package charite.christo;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.lang.reflect.Method;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/*
  See  BasicScrollBarUI AquaScrollBarUI WindowsScrollBarUI SynthScrollBarUI
*/
public class ChScrollBar extends JScrollBar {
    final java.util.Map MAPCP=new java.util.HashMap();
    public final static String KOPT_METAL="CSB$$M";
    private boolean _hasSelection, _myModel;
    private int _selII[], _selScale, _selBoxH, _warnTrack, _warnNoBut, _lock, _val, _diff;
    private Color _selColor[];
    final Collection SPANES=new ArrayList();
    public ChScrollBar(int v_or_h) {
        super(v_or_h);
        addMoli(MOLI_LEFT_TO_MIDDLE,this);
        for(Component b : childs(this)) addMoli(MOLI_SB_UNLOCK, b);
    }
    public @Override int getUnitIncrement() {
        final int ui=super.getUnitIncrement();
        final Component view=scrllpnChild(getParent());
        if (view!=null) {
            if (ChTextComponents.tools(view)!=null) return maxi(1, charH(view));
            if (getOrientation()==VERTICAL) {
                if (view instanceof JList || view instanceof JTable) {
                    final int h=hght(view);
                    final int n=view instanceof JTable ? ((JTable)view).getRowCount() : sze(view);
                    return maxi(1, n==0 ? ICON_HEIGHT : h/n);
                }
                if (view instanceof JTable) {
                    final int rh=((JTable)view).getRowHeight();
                    return maxi(1,rh<1 ? ICON_HEIGHT : rh);
                }
            }
        }
        return ui==1 ? EX : ui;
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> set selection >>> */
    /** to be set in paint hook */
    public void setHasSelection(boolean b) { _hasSelection=b; }
    public void setSelectedY(int[] ii, int scale, Color cc[], int boxHeight) {
        _selII=ii;
        _selColor=cc;
        _selScale=scale;
        _selBoxH=boxHeight;
        repaintC(this);
    }
    /* <<< set selection <<< */
    /* ---------------------------------------- */
    /* >>> paint >>> */
    @Override public void paint(Graphics g) {
        if (!paintHooks(this, g, false)) return;
        final int ii[]=_selII, max=getMaximum();
        final boolean hasSel=_hasSelection=sze(ii)>0;
        final boolean vertical=getOrientation()==VERTICAL;
        final Rectangle track=_hasSelection || _lock!=0 ? getTrack() : null;
        if (hasSel) {
            final Rectangle thumb=getThumb();
            g.setColor(C(0xFFffFF));
            g.fillRect(x(track),y(track), wdth(track), hght(track));
            final int rx=x(thumb)+1, ry=y(thumb)+1, rw=wdth(thumb), rh=maxi(1, hght(thumb)-2);
            ((Graphics2D)g).setPaint(new GradientPaint(0, 0, C(0x666688), vertical?rw/2F:0, vertical?0:rh/2F, C(0xDDddEE), true));
            g.fillRect(rx,ry,rw, rh);
            g.setColor(C(0x444444));
            g.drawRect(rx,ry,rw, rh);
        } else try { super.paint(g); } catch(Exception e){}
        if (hasSel && max>0) {
            final int th=vertical?hght(track):wdth(track), boxH=maxi(1, th*maxi(_selBoxH,_selBoxH)/max-1);
            final Color color1=sze(_selColor)!=1 ? null : get(0, _selColor, Color.class);
            for(int i=0; i<ii.length; i++) {
                if (ii[i]<0) continue;
                final Color c=color1!=null ? color1 : get(i, _selColor, Color.class);
                g.setColor(c!=null ? c : bgSelected());
                final int d=ii[i]* th*_selScale/max;
                if (vertical) g.fillRect(4,y(track) + d, wdth(track)-5, boxH);
                else g.fillRect(x(track) + d, 4, boxH,hght(track)-5);
            }
        }
        final int h=hght(this);
        if (_lock==1 && vertical && h>EX) {
            final int y=mini(h-3, y2(track));
            g.setColor(C(0xFFffFF));
            g.fillRect(0, y, 99, 99);
            g.setColor(C(0));
            g.fillRect(0, y, 99, 4);
        }
        paintHooks(this, g, true);
    }

    /* <<< paint <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    { this.enableEvents(ENABLE_EVT_MASK);}
    @Override public void processEvent(AWTEvent ev) {
        final int id=ev.getID(), modi=modifrs(ev), y=y(ev);
        if (id==MOUSE_RELEASED || id==MOUSE_ENTERED || id==MOUSE_DRAGGED && x(ev)%32==0) {
            if (_hasSelection && getValueIsAdjusting()) repaintC(this);
        }
        if (id==MOUSE_DRAGGED) {
            final int v=getValue();
            if (v!=_val) {
                final int diff=v<_val?-1:1;
                _val=v;
                if (diff!=_diff) {
                    if (_diff!=0) {
                        for(int i=sze(SPANES); --i>=0;) repaintC(scrllpnChild(getRmNull(i,SPANES)));
                    }
                    _diff=diff;
                }
            }
        }
        if (id==MOUSE_RELEASED || id==MOUSE_PRESSED) {
            _val=getValue();
            _diff=0;
        }
        if (id==MOUSE_ENTERED && super.getUnitIncrement()!=getUnitIncrement()) setUnitIncrement(getUnitIncrement());
        if ((id==MOUSE_DRAGGED || id==MOUSE_PRESSED) && getOrientation()==VERTICAL) {
            if (y>hght(this)-y(getTrack())) lock(1,this);
            else if (y>0) lock(0,this);
        }
        if (shouldProcessEvt(ev)) {
            if (0==(modi&CTRL_MASK) || wheelRotation(ev)==0) super.processEvent(ev);
        }
        if (id==MOUSE_RELEASED || id==MOUSE_ENTERED || id==MOUSE_EXITED) repaintC(deref(scrllpnChild(getParent()), ChTextView.class));
    }

    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Workaround for protected methods >>> */
    private final Rectangle _rTrack=new Rectangle(), _rThumb=new Rectangle();
    private final Object _method[]={null,null};
    private Object _mUI;
    public Rectangle rect(int thumb) {
        final Object ui=getUI();
        if (ui==null) return null;
        if (_mUI!=ui) {
            _mUI=ui;
            _method[0]=null;
        }
        if (_method[0]==null) {
            for(int i=2;--i>=0;) {
                _method[i]=orO(findMthd(RFLCT_SET_ACCESSIBLE, i==0?"getTrackBounds":"getThumbBounds",ui.getClass(),null), "");
            }
        }
        return deref(invokeMthd(RFLCT_SET_ACCESSIBLE, deref(_method[thumb],Method.class), ui, NO_OBJECT), Rectangle.class);
    }

    @Override public void updateUI() {
        final UIDefaults defaults=UIManager.getDefaults();
        final Object ui=defaults.get(super.getUIClassID());
        Object myUI=null;
        try {
            myUI=
                "javax.swing.plaf.metal.MetalScrollBarUI".equals(ui)  || gcp(KOPT_METAL,getClass())!=null ? new ChMetalScrollBarUI() :
                                                                                                                       
                                                                                                                                                   
                null;
        } catch(Exception ex){}
        if (myUI instanceof javax.swing.plaf.ScrollBarUI) setUI((javax.swing.plaf.ScrollBarUI)myUI);
        else super.updateUI();
    }

     public Rectangle getTrack() {
         Rectangle r;
         if ((r=rect(0))!=null) return r;
         else if (0==_warnTrack++) putln(RED_WARNING+"Failed ChScrollBar.getTrackBounds");

         final int W=wdth(this), H=hght(this);
         final int incrGap=atoi(UIManager.get("ScrollBar.incrementButtonGap"));
         final int decrGap=atoi(UIManager.get("ScrollBar.decrementButtonGap"));
         final Component b1, b2;
         {
             Component c1=null,c2=null;
             for(Component c : getComponents()) {
                 if (c instanceof AbstractButton) {
                     if (c1==null) c1=c;
                     else c2=c;
                 }
             }
             if (c2==null) b1=b2=null;
             else if (y(c1)<y(c2) || x(c1)<x(c2)) { b1=c1; b2=c2; }
             else { b1=c2; b2=c1; }
         }
         r=_rTrack;
         if (getOrientation()==JScrollBar.HORIZONTAL) {
             if (b2!=null) {
                 r.x=x(b1)+wdth(b1)+incrGap;
                 r.width=x(b2)-decrGap-x(r);
             } else {
                 r.x=EX;
                 r.width=W-2*EX;
                 if (0==_warnNoBut++) putln(RED_WARNING+"ChScrollBar no buttons");
             }
             r.height=H;
         } else {
             r.width=W;
             if (b2!=null) {
                 r.y=y(b1)+hght(b1)+incrGap;
                 r.height=y(b2)-decrGap-y(r);
             } else {
                 r.y=EX;
                 r.height=H-2*EX;
             }
         }
         return r;
     }

    public Rectangle getThumb() {
        Rectangle r;
        if ((r=rect(1))!=null) return r;
        r=_rThumb;
        final Rectangle track=getTrack();
        final int v=getValue(), m=maxi(1,getMaximum()), e=getVisibleAmount();
        if (getOrientation()==JScrollBar.HORIZONTAL) {
            r.x=x(track)+wdth(track)*v/m;
            r.width=wdth(track)*e/m;
            r.height=hght(track);
        } else {
            r.y=y(track)+hght(track)*v/m;
            r.height=hght(track)*e/m;
            r.width=wdth(track);
        }
        return r;
    }

    /* <<< Workaround <<< */
    /* ---------------------------------------- */
    /* >>>  Lock End position >>> */
    // BasicScrollBarUI
    public void myModel() {
        if (!_myModel) {
            _myModel=true;
            final DefaultBoundedRangeModel model=new DefaultBoundedRangeModel(getValue(), getVisibleAmount(), 0, getMaximum()) {
                    @Override public int getValue() {
                        return _lock==1 ? super.getMaximum()-super.getExtent() : _lock==-1 ? 0 : super.getValue();
                    }
                };
            setModel(model);
        }
    }

    public static void lock(int upOrDown, JScrollBar sbar) {
        final ChScrollBar sb=deref(sbar, ChScrollBar.class);
        if (sb!=null) {
            if (upOrDown!=0) sb.myModel();
            if (sb._lock!=upOrDown) repaintC(sb);
            sb._lock=upOrDown;
        }
    }
}

