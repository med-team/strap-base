package charite.christo;
import javax.swing.*;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class ChPanel extends JPanel implements Scrollable {
    { if (!withGui()) assrt(); }

    public final static String KEY_REPRESENT_COMPONENT="CP$$RC", KOPT_UPDATE_SIZE="CP$$US";
    final java.util.Map MAPCP=new java.util.HashMap();
    private long _duration[];
    int _progressPercent=INT_NAN;
    private final static int MARGIN=2;
    private String _progressTxt;
    final Object WEAK_REF=newWeakRef(this);
    private boolean _painted;
    public ChPanel() {
        super();
        setBG(DEFAULT_BACKGROUND,this);
        setForeground(C(0));
        setOpaque(true);
        pcp(KEY_NOT_YET_PAINTED,"",this);
    }
    public ChPanel setProgress(int percent, String txt) {
        _progressTxt=txt;
        _progressPercent=percent;
        return this;
    }
    private String _labelText;
    public ChPanel t(CharSequence s) { _labelText=toStrg(s); return this; }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> fixSize >>> */
    private int _pw, _ph;
    public void updateSize() {
        final boolean debug=gcp("D",this)!=null;

        final Dimension ps=getPreferredSize();
        final int pw=wdth(ps), ph=hght(ps);
       
        if (ps!=null && (pw!=_pw||ph!=_ph)) {
            setPreferredSize(ps);
            revalidate();
            final Component par=parentC(parentC(this));
            if (par instanceof JScrollPane) {
                if (pw!=_pw) revalidateC(getSB('W',par));
                if (ph!=_ph) revalidateC(getSB('H',par));
            }
            _pw=pw;
            _ph=ph;
        }
    }
    /* <<< fixSize <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    public void paintChildren(Graphics g) {
        if (!isEnabled() && gcp(KOPT_NOT_PAINTED_IF_DISABLED,this)!=null) return;
        final JScrollBar sb=gcp(KEY_NO_PAINT_IF_ADJUSTING, this, JScrollBar.class);
        if (sb!=null && sb.getValueIsAdjusting()) return;
        try {
            super.paintChildren(g);
        } catch (Throwable e){ putln(RED_CAUGHT_IN+"ChPanel paintChildren ",e); stckTrc(e); }
    }
    @Override public boolean isValidateRoot() { return gcp(KOPT_IS_VALIDATE_ROOT, this)!=null; }

    public long[] getWhenPainted() {
        if (_duration==null) _duration=new long[ARRAY_DURATION];
        return _duration;
    }

    @Override public void paintComponent(Graphics g) {
        if (gcp(KOPT_UPDATE_SIZE,this)!=null) updateSize();
        final int w=getWidth(), h=getHeight();
        {
            final Component c=gcp(KEY_REPRESENT_COMPONENT,this,Component.class);
            if (c!=null) {
                SwingUtilities.paintComponent(g,c,getParent(),0,0,w,h);
                return;
            }
        }

        final long time=System.currentTimeMillis();
        if (paintHooks(this, g, false)) {
            try { super.paintComponent(g);}catch(Throwable t){}
            final int progress=_progressPercent;

            if (progress!=INT_NAN) {
                final String txt=_progressTxt;
                g.setColor(C(0x8888FF));
                g.fillRect(1,1, maxi(0,w*progress/100-1),h-MARGIN);
                g.setColor(C(0));
                g.drawRect(0,0,w-1,h-1);
                if (sze(txt)!=0) {
                    g.setFont(getFont());
                    g.drawString(txt, (w-strgWidth(g,txt))/2, charA(g)+MARGIN);
                }
            }
            final String labelText=_labelText;
            if (labelText!=null) {
                g.setFont(getFont());
                g.setColor(getForeground());
                g.drawString(labelText, 2, charA(g)+MARGIN);
            }
            final JTabbedPane tp=deref(getParent(), JTabbedPane.class);
            final Color bg=tp==null?null:tp.getBackground();
            if (bg!=null) {
                g.setColor(bg);
                fillBigRect(g,0,0,getWidth(),getHeight());
            }

            pcp(KEY_NOT_YET_PAINTED,null,this);
            final long d[]=_duration;
            if (d!=null) d[DURATION]=(d[WHEN]=System.currentTimeMillis())-time;
        }
        paintHooks(this, g, true);

    }

    @Override public void paint(Graphics g){
        _painted=true;
        final int H=getHeight(), W=getWidth();
        try {
            if (H>0 && W>0) super.paint(g);
        } catch(Throwable ex){putln(RED_CAUGHT_IN," ChPanel.paint(g) "); stckTrc(ex);}
        if (null==gcp(KOPT_NOT_INDICATE_TOO_SMALL,this)) {
            final Component cc[]=getComponents();
            int smallH=-1, smallW=-1;
            for(int i=cc.length; --i>=0;) {
                final JComponent c=derefJC(cc[i]);
                if (c==null) continue;
                final Dimension ps=c.getPreferredSize();
                if (wdth(ps)==0 || hght(ps)==0)continue;
                final int x=c.getX(), y=c.getY(), h=c.getHeight(), w=c.getWidth();
                if (smallH<0 && (h<1 || y>=H-h/2)) smallH=mini(y,H-2);
                if (smallW<0 && (w<1 || x>=W-w/2)) smallW=mini(x,W-2);
            }
            final boolean fl=getLayout() instanceof FlowLayout;
            if (smallH>=0 && !fl) ChRenderer.randomStrikeThrough(0L, hashCode(), g, 0, W, smallH);
            if (smallW>=0 || smallH>=0 && fl) ChRenderer.randomStrikeThrough(ChRenderer.STRIKE_THROUGH_V, hashCode(), g, 0, H, W-2);
        }
    }

    @Override public boolean isShowing() { return super.isShowing() || gcp(KOPT_JUST_WRITING_PNG,this)!=null;  }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Dimension >>> */
    @Override public Dimension getMaximumSize() { return prefSze0(this) ? dim(0,0) : super.getMaximumSize(); }

    @Override public Dimension getPreferredSize() {
        {
            final Component c=gcp(KEY_REPRESENT_COMPONENT,this,Component.class);
            if (c!=null) {
                Dimension d=isVisbl(c) ? c.getSize() : null;
                if (wdth(d)==0 || hght(d)==0) d=c.getPreferredSize();
                return d;
            }
        }
        final Dimension d=prefSze(this);
        final String labelText=_labelText;
        try { // =Fri.08.09.06 42 linux .NullPointerException
            if (d!=null) return d;
            if (labelText!=null) {
                final Font f=getFont();
                return dim(2*MARGIN+strgWidth(f, labelText), charH(f)+2*MARGIN);
            }
            return super.getPreferredSize();
        } catch(Exception e) { return dim(333,333);}
    }
    /* <<< Dimension <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    @Override public String getToolTipText(java.awt.event.MouseEvent ev) {
        return ballonMsg(ev, super.getToolTipText(ev));
    }

    @Override public Point getToolTipLocation(java.awt.event.MouseEvent event) {
        final Point p=(Point)gcp(KEY_TIP_LOCATION,this);
        if (p!=null) {
            final JViewport parent=parentC(false,this,JViewport.class);
            final Point vp=  parent!=null ? parent.getViewPosition() : null;
            return vp!=null ? new Point(x(vp)+x(p), y(vp)+y(p)) : p;
        }
        return null;
    }
    /* <<< Tip <<< */
    /* ---------------------------------------- */
    /* >>> Scroll >>> */
     @Override public Dimension getPreferredScrollableViewportSize() { return getPreferredSize(); }
     @Override public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
         return visibleRect==null ? 10 : orientation==SwingConstants.VERTICAL ? hght(visibleRect)-EX : wdth(visibleRect)-EM;
    }
    @Override public boolean getScrollableTracksViewportHeight() { return gcp(KOPT_TRACKS_VIEWPORT_HEIGHT,this)!=null;}
    @Override public boolean getScrollableTracksViewportWidth() { return gcp(KOPT_TRACKS_VIEWPORT_WIDTH,this)!=null;}
    @Override public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) { return EX;}

    /* <<< Scroll <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    { this.enableEvents(ENABLE_EVT_MASK);}
    @Override public void processEvent(AWTEvent ev) {
        mayClosePopupMenu(ev);
        if (shouldProcessEvt(ev)) {
            try {
                super.processEvent(ev);
            } catch(Throwable e){}
        }
    }
    /* <<< Scroll <<< */

    public Object getDndDateien() { return dndV(false,this);}
    @Override public void removeAll() {
        if (!isEDT()) inEDT(thrdM("removeAll", this));
        else try {
                super.removeAll();
            } catch(Exception ex){ stckTrc(ex);}
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    @Override public boolean isEnabled() {  return isEnabld(this,super.isEnabled());  }
    @Override public void invalidate() { if (null==gcp(KOPT_DO_NOT_INVALIDATE,this)) super.invalidate(); }
    public ChPanel cp(Object key, Object value) { pcp(key,value,this); return this;}

}

