package charite.christo;
import java.io.File;
import java.util.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/*
  @author Christoph Gille
*/
public class ChCodeViewer {

    private final static byte CLASS=1, COMMENT=2, KEYWORD=3, STRING=4;
    private final static String ERROR_CLASS="ERROR_CLASS",
        KEY_WORDS[]="implements package abstract do inner public var boolean continue for int return void break else interface short volatile extends long static while case final naive super transient cast float new rest catch for null synchronized char finally operator this class generic outer switch const goto package throw double if private true default import protected try import".split(" ");
    private ChCodeViewer(){}
    public static BA highlightSyntax(long options, BA src, String[] packages) {

        final Map<String,String> mapFullClassName=new HashMap();
        final int E=src.end();
        final byte[] T=src.bytes(), aa=new byte[E];
        final String[] imported;
        {
            final String pp[]=getImportedPackages(src);
            if (packages!=null) {
                imported=new String[pp.length+packages.length];
                System.arraycopy(pp,0,imported,0,pp.length);
                System.arraycopy(packages,0,imported,pp.length,packages.length);
            } else imported=pp;
        }
        boolean update=false;
        int multiComment=-1, singleComment=-1, string=-1, aClass=-1, keyword=-1, aClassP=-1, countSlash=0;
        for(int i=-1; i<E; i++) {
            final int c0= i<0 ? '\n' : T[i], c1=i+1==E?'\n':T[i+1];
            final boolean isW=is(LETTR_DIGT_US,c0);
            if (multiComment>=0 && c0=='*' && T[i+1]=='/') {
                Arrays.fill(aa, multiComment,i+2, COMMENT);
                multiComment=-1;
            } else if (singleComment>=0 && c0=='\n') {
                Arrays.fill(aa, singleComment,i, COMMENT);
                singleComment=-1;
            } else if (string>=0 && c0=='"') {
                if ( (countSlash&1)==0) {
                    Arrays.fill(aa, string,i+1, STRING);
                    string=-1;
                    continue;
                }
            } else {
                if ( (aClass>=0 || aClassP>=0) && !isW) {
                    final int start=aClassP>=0 && cntainsOnly(LETTR_DIGT_US_DOT, T, aClassP,i) ? aClassP :
                        aClass>=0 && cntainsOnly(LETTR_DIGT_US_DOT, T, aClass,i) ? aClass : -1;
                    if (start>=0 && strstr("..", T, start,i)<0 && T[i-1]!='.') {
                        final String sClass=bytes2strg(T,start,i);
                        String cn=mapFullClassName.get(sClass);
                        if (cn==null) {
                            cn=fullClassNam(update,sClass,imported); update=false;
                            if (cn==null) cn=ERROR_CLASS;
                            mapFullClassName.put(sClass,cn);
                        }
                        //putln(sClass," = ",cn);
                        if (cn!=ERROR_CLASS) Arrays.fill(aa, start, i, CLASS);
                    }
                }
                if (keyword>=0 && !isW) {
                    final int len=i-keyword;
                    if (len>1) {
                        final int k0=T[keyword];
                        for (String KW : KEY_WORDS) {
                            if (KW.length()==len && KW.charAt(0)==k0 && strEquls(KW, T, keyword)) {
                                Arrays.fill(aa, keyword,i, KEYWORD);
                                break;
                            }
                        }
                    }
                }
            }
            if (!isW) {
                aClass=-1;
                if (c0!='.') aClassP=-1;
            }
            if (!is(LOWR,c0)) keyword=-1;
            if (multiComment<0 && singleComment<0 && string<0) {
                if (!isW && is(LOWR,c1)) keyword=i+1;
                if (c0=='/') {
                    if (c1=='*') {multiComment=i; continue;}
                    if (c1=='/') {singleComment=i; continue;}
                }
                if (c0=='"' && (countSlash&1)==0) { string=i; continue;}
                if (!isW && is(LETTR,c1)) {
                    if (c0!='.') aClassP=i+1;
                    if (c1<='Z') aClass=i+1;
                }
            }
            if (c0=='\\') countSlash++; else countSlash=0;
        }
        //putln("highlightSyntax ",System.currentTimeMillis()-time);
        return insertAttributes(options,T,0,E,aa, mapFullClassName);
    }

    public final static long HTML=1<<1, ANSI=1<<2, HTML_BODY=HTML|1<<3;
    private static BA insertAttributes(long options, byte T[], int B, int E, byte aa[], Map<String,String> mapFullClassName) {
        final BA sb=new BA( (E-B)*2);
        if (0!=(options&HTML_BODY)) sb.a("<html><body>\n");
        int lastA=0;
        String closeTag="";
        for(int i=B;i<E;i++) {
            final int a=aa[i];
            if (lastA!=a) {
                if (0!=(options&ANSI)) {
                    if (a==0) sb.a(ANSI_RESET);
                    if (a==STRING) sb.a(ANSI_FG_RED);
                    if (a==KEYWORD) sb.a(ANSI_BOLD);
                    if (a==CLASS) sb.a(ANSI_FG_BLUE+ANSI_UL);
                    if (a==COMMENT) sb.a(ANSI_FG_MAGENTA);
                } else {
                    if (a==0) { sb.a(closeTag); closeTag=""; }
                    if (a==STRING)  { sb.a("<font color=\"#00AA00\">"); closeTag="</font>"; }
                    if (a==KEYWORD) { sb.a("<b>"); closeTag="</b>"; }
                    if (a==COMMENT) { sb.a("<font color=\"#4444AA\">"); closeTag="</font>"; }
                    if (a==CLASS)   {
                        int to=i;
                        while( to<E && aa[to]==CLASS) to++;
                        final CharSequence javadoc=ChJavadoc.javadocUrl(mapFullClassName.get(bytes2strg(T,i,to)));
                        if (javadoc!=null) { sb.a("<a href=\"").a(javadoc).a("\">"); closeTag="</a>"; }
                        else { sb.a("<font color=\"#AA0000\">"); closeTag="</font>"; }
                    }
                }
                lastA=a;
            }
            sb.a((char)T[i]);
        }
        if (0!=(options&HTML_BODY)) sb.a("\n</body></html>\n");
        return sb;
    }

    public static String[] getImportedPackages(BA ba) {
        String pp[]=null;
        while(true) {
            final int  ends[]=ba.eol();
            final byte[] T=ba.bytes();
            int count=0;
            nextLine:
            for(int iL=0; iL<ends.length; iL++) {
                final int b= iL==0 ? 0 : ends[iL-1]+1;
                final int e=ends[iL];
                if (e-b<6) continue;
                final int c0=T[b];
                if (c0!='i' && c0!='p') {
                    if (c0!=' ' && c0!='\t')  break nextLine;
                    continue;
                }
                final int semicolon=strchr(';', T,b,e);
                if (semicolon<0) continue;
                if (c0=='p' && strstr("package ",T,b,e)==b) {
                    int next=b+8; while(next<e && ( T[next]==' ' || T[next]=='\t')) next++;
                    if (semicolon>next) {
                        if (pp!=null) pp[count]=bytes2strg(T,next,semicolon)+'.';
                        count++;
                    }
                }
                if (c0=='i' && strstr("import ",T,b,e)==b) {
                    int next=b+7; while(next<e && T[next]==' ') next++;
                    if (strstr("static ",T,next,e)!=next && semicolon>next) {
                        final int to=T[semicolon-1]=='*' ? semicolon-1 : semicolon;
                        if (to>next) {
                            if (pp!=null) pp[count]=bytes2strg(T,next,to);
                            count++;
                        }
                    }
                }
            }
            if (pp==null) pp=new String[count]; else break;
        }
        return pp;
    }

    public static BA fontifyXmlDAS(BA xml) {
        if (xml==null) return xml;
        if (strstr("><",xml)>=0) {
            xml.replace(0L,"><FEATURE", ">\n\n  <FEATURE")
                .replace(0L,"><TYPE ",  ">\n   <TYPE ")
                .replace(0L,"><START>", ">\n   <START>")
                .replace(0L,"><START>", ">\n   <START>")
                .replace(0L,"</FEATURE><","</FEATURE>\n\n  <");
        }
        final int B=xml.begin(), E=xml.end();
        final byte[] T=xml.bytes();
        final BA ba=new BA(sze(xml)*3/2);
        for(int i=B; i<E-5; i++) {
            if (T[i]=='<') {
                final int k=i+(T[i+1]=='/' ?2:1);
                final String ansi=
                    strEquls("FEATURE",T,k) ? ANSI_FG_RED :
                    strEquls("TYPE",T,k)    ? ANSI_YELLOW :
                    strEquls("START",T,k)  || strEquls("END",T,k)  ? ANSI_FG_CYAN :
                    strEquls("METHOD",T,k) || strEquls("SCORE",T,k) || strEquls("NOTE",T,k)
                    || strEquls("SEGMENT",T,k) || strEquls("GFF",T,k) || strEquls("DASGFF",T,k)? ANSI_BOLD :
                    null;

                final int gt=ansi==null?-1: strchr(STRSTR_E,'>',T,k,E);
                if (gt>0) {
                    ba.a(ansi).a(T,i,gt+1).a(ANSI_RESET);
                    i=gt;
                    continue;
                }
            }
            ba.a((char)T[i]);
        }
        return ba;
    }

    // Java6 charite.christo.ChCodeViewer ChButton.java
    public static void main(String... argv) {
        for(String arg :argv) {
            final File f=file(arg);
            final BA src=readBytes(f);
            if (src==null) continue;
            final BA sb=ChCodeViewer.highlightSyntax(HTML_BODY, src,(String[])null);
            wrte(file(arg+".html"),sb);
        }
    }

}
