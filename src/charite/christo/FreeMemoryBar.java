package charite.christo;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   Displays the free memory
   @author Christoph Gille
*/
public class FreeMemoryBar extends ChButton implements Runnable {
    public FreeMemoryBar() {
        super(ChButton.HEIGHT_I|ChButton.ON_PRESS_GC,"     ");
        monospc(this);
        i(IC_BLANK);
        addMoli(MOLI_REFRESH_TIP,this);
        rtt(this);
    }
    private long free,max,total;
    private boolean _started;
    @Override public void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);
        this.total=Runtime.getRuntime().totalMemory()>>10;
        this.max=Runtime.getRuntime().maxMemory()>>10;
        final long totalFree=max-total+free;
        final float freeByMax=totalFree/(float)max;
        final String t=baTip().format10(totalFree,8).toString();
        final int h=getHeight();
        g.setColor(C(0));
        g.drawString(t,0,(h-charH(g))/2+charA(g));
        g.setColor(blueYellowRed(1-freeByMax));
        final int h0=(int)(h*(1-Math.sqrt(freeByMax)));
        g.fill3DRect(3,h-h0,4,h0,true);
        if (!_started) {
            _started=true;
            ChThread.callEvery(0, 333, this,"FreeMemoryBar");
        }
    }
    @Override public void run() {
            final long f=Runtime.getRuntime().freeMemory()>>10;
            if ((f/32)!=(free/32)) { free=f; repaint(); }
    }
    @Override public String getToolTipText(java.awt.event.MouseEvent ev) {
        return baTip()
            .a("<html><body><pre>How much memory [kilobyte] is left? ( max-total+free): ")
            .format10(max-total+free,8)
            .a(" kb\n\n total Memory: ").format10(total,8)
            .a(" kb\n  free Memory: ").format10(free,8)
            .a(" kb\n   max Memory: ").format10(max,8)
            .a(
               " kb\n\n"+
               "Click to run garbage collection to find out how much memory there is left after unsused data is removed.\n"+
               "The max memory  can be increased in the Java control panel or with the command line option -Xmx.\n"+
               "Example for -Xmx:\n"+
               "  java -Xmx100M -jar strap.jar \n"+
               "</pre></body></html>")
            .toString();
    }
}
