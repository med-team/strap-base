package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
public class PrgParas {
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> GUI >>> */
    private PrgParasGUI _gui;
    public String _guiDescription;
    private long _guiOptions;
    public PrgParasGUI getGUI(boolean createOne) {
        if (createOne && _gui==null) {
            if ( 0!=(_guiOptions&HasPrgParas.FURTHER_PARAMETERS) || _guiDescription!=null) {
                _gui=new PrgParasGUI(_guiOptions, _guiDescription);
            }
        }
        return _gui;
    }

    public void defineGUI(long options, String name_descript_initVal) {
        _guiDescription=name_descript_initVal;
        _guiOptions=options;
    }
    public boolean hasGUI() { return _guiDescription!=null;}
    /* <<< GUI <<< */
    /* ---------------------------------------- */
    /* >>> Non GUI >>> */

    public Map<String,String> getMapVariables() {
        final PrgParasGUI gui=getGUI(_guiDescription!=null);
        final Map<String,String> mapGui=gui!=null ? gui.getMap() : null;
        if (mapGui==null || _mapVariable==null) return _mapVariable;
        final Map<String,String> m=new HashMap();
        if (mapGui!=null) m.putAll(mapGui);
        if (_mapVariable!=null) m.putAll(_mapVariable);
        return m;
    }

    public PrgParas addVariable(String name, String value) { return addVariable(name,value,false); }
    public PrgParas addVariableE(String name, String value) { return addVariable(name,value,true); }
    private PrgParas addVariable(String name, String value, boolean atEnd) {
        if (_vVariable==null) _vVariable=new Vector[]{new Vector(),new Vector()};
        _vVariable[atEnd?1:0].add(name);
        if (_mapVariable==null) _mapVariable=new HashMap();
        _mapVariable.put(name,value);
        _asA=null;
        return this;
    }

    private HashMap _mapVariable;
    private List<String> _vOpt[], _vVariable[];
    public void addCommandLineOption(String para, boolean atEnd) {
        if (_vOpt==null) _vOpt=new Vector[]{new Vector(),new Vector()};
        _vOpt[atEnd?1:0].add(para);
        _asA=null;
    }
    /* <<< Non GUI <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    private final static List<String> V=new Vector();
    private String[] _asA;
    private int _asArrMC;
    public String[] asStringArray() {
        final PrgParasGUI gui=getGUI(false);
        final int mc=gui==null?0:gui.mc();
        if (_asA==null || _asArrMC!=mc) {
            _asArrMC=mc;            
            synchronized(V) {
                V.clear();
                for(int iCollect=0; iCollect<5; iCollect++) {
                    final Object collect=
                        iCollect==0 ? get(0,_vOpt) :
                        iCollect==1 ? get(0,_vVariable) :
                        iCollect==2 ? (gui!=null ? gui.getMap() : null) :
                        iCollect==3 ? get(1,_vVariable) :
                        iCollect==4 ? get(1,_vOpt) : null;
                    final Map<String,String> map=deref(collect, Map.class);
                    if (map!=null) {
                        for(Map.Entry<String,String> e : entryArry(map)) {
                            final String k=e.getKey(), v=e.getValue();
                            if (nxt(-SPC,v)<0) continue;
                            if (lstChar(k)=='=') V.add(k+v);
                            else {
                                V.add(k);
                                if (v!=PrgParasGUI.BLANK) V.add(v);
                            }
                        }
                    }
                }
                if (gui!=null) adAll(gui.getFurtherOptions(),V);
                _asA=strgArry(V);

            }
        }
        return _asA;
    }

 }
