package charite.christo;
import java.util.*;
import javax.swing.Icon;
import java.net.URL;
import java.io.File;
import java.util.zip.ZipFile;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
public class UniqueList<T> implements List<T>,  HasRenderer, ChRunnable, HasMap, HasMC {
    public final static int UNIQUE=1<<1, USE_HASH_SET=1<<2, ASSERT_EDT=1<<3, TO_STRING_SUPER=1<<4;
    private int _mc, _asArray_mc, _options=UNIQUE;
    private final Class _clazz;
    private Object _refArray, _nodeId, _rendererComp;
    public Object _ic;
    private String _label, _hooksID[][];
    private Set _hashSet;
    private List _v;
    private Map _clientO;
    private final boolean _ref=false; /* For future method addRef(Reference r) */
    public List<T> v() {
        if (_v==null) _v=new MyVect();
        return _v;
    }
    public UniqueList(Class c) { _clazz=c; }
    public Class getClazz() { return _clazz;}
    public UniqueList rmOptions(long opt) {  _options&=~opt; return this; }
    public UniqueList addOptions(long opt) { _options|=opt;  return this; }
    @Override public int mc() { return _mc;}
    @Override public boolean equals(Object o) { return o==this;}
    @Override public Map map(boolean create) {return _clientO==null&&create?_clientO=new ChMap() : _clientO;}

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> indexOf >>> */
    public int indexOf(Object o, int from0, int to0) {
        final List v=_v;
        final Object[] elDat=v instanceof MyVect ? ((MyVect)v).getData() : null;
        final int size=_v==null ? 0 : elDat!=null ? mini(elDat.length,size()) : size();
        if (size==0) return -1;
        final boolean fwd=from0<to0;
        final int to=fwd?mini(to0,size):to0;
        final boolean equals2=o instanceof File || o instanceof URL || o instanceof ZipFile || o instanceof CharSequence;
        for(int i=mini(size-1,from0); fwd?i<to:to<i; i+=fwd ? 1 : -1) {
            final Object el=elDat!=null ? elDat[i]:v.get(i);
            if (equals2 ? equals2(el,o) : el==o) return i;
        }
        return -1;
    }
    private boolean equals2(Object o1, Object o2) {
        if (o1 instanceof CharSequence) return o1.equals(o2);
        else if (o1 instanceof ChFile && o2 instanceof ChFile) return o1.hashCode()==o2.hashCode() && o1.toString().equals(o2.toString());
        else if (o1 instanceof URL    && o2 instanceof URL) return o1.toString().equals(o2.toString());
        else if (o1 instanceof File && o2 instanceof File) return o1.equals(o2);
        else if (o1 instanceof ZipFile && o2 instanceof ZipFile) return ((ZipFile)o1).getName().equals(((ZipFile)o2).getName());
        return o1==o2;
    }
    public UniqueList ensureCapacity(int n) {
        final List v=v();
        if (v instanceof Vector) ((Vector)v).ensureCapacity(n);
        return this;
    }
    /* <<< indexOf <<< */
    /* ---------------------------------------- */
    /* >>> Override  >>> */
    @Override public List<T> subList(int from, int to) { return v().subList(from,to);}
    @Override public int size() {
        runHooks(HOOK_ACCESS);
        return _v==null ? 0 : _v.size();
    }
    @Override public boolean isEmpty() { return size()>0; }
    @Override public ListIterator<T> listIterator() {return listIterator(0); }
    @Override public ListIterator<T> listIterator(int i) {return v().listIterator(i); }
    @Override public Iterator<T> iterator() { return v().iterator(); }
    @Override public Object[] toArray() { return size()>0?v().toArray() : emptyArray(_clazz);}
    @Override public <T> T[] toArray(T[] a) { return v().toArray(a); }
    @Override public boolean containsAll(Collection<?> c) { return _v==null ? false : sze(c)==0 ? true :  v().containsAll(c); }
    @Override public synchronized boolean contains(final Object o) {
            final List v=_v;
            final int size=v==null ? 0 : size();
            if (size==0) return false;

            if (0!=(_options&USE_HASH_SET)) {
                if (_hashSet==null) (_hashSet=new HashSet()).addAll(this);
                if (_hashSet.size()!=size) {
                    assrt();
                    _hashSet.retainAll(this);
                    _hashSet.addAll(this);
                }
                return _hashSet.contains(o);
            }

            return indexOf(o)>=0;
        }
    @Override public int indexOf(Object o) { return indexOf(o, 0,MAX_INT); }
    @Override public int lastIndexOf(Object o) { return indexOf(o, MAX_INT, -1); }

    @Override public synchronized T get(int i) {
            if (i<0 || i>=size()) return null;
            try{ return v().get(i);} catch(Throwable e){ return null;}
        }

    /* *** Primary Methods that change *** */

    /* --- Remove List --- */
    @Override synchronized public T remove(int i)  {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            final T removed=i>=0 && i<size() ? v().remove(i) : null;
            if (removed!=null) {
                _changed();
                if (_hashSet!=null) _hashSet.remove(removed);
            }
            return removed;
        }
   @Override synchronized public boolean retainAll(Collection c)  {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            if (v().retainAll(c)) {
                _changed();
                if (_hashSet!=null) _hashSet.retainAll(c);
                return true;
            } else return false;
        }
    @Override public boolean removeAll(Collection<?> c) {
        if (0!=(_options&ASSERT_EDT)) assrtEDT();
        if (_hashSet!=null) _hashSet.removeAll(c);
        final boolean changed= _v==null ? false : v().removeAll(c);
        if (changed) _changed();
        return changed;
    }

     @Override synchronized public final void clear() {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            if (size()>0) {
                clr(v());
                _changed();
                clr(_hashSet);
            }
        }
    /* --- Add --- */
    @Override synchronized public boolean add(T s) {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            if (s!=null && !(0!=(_options&UNIQUE) && contains(s)) && v().add(s)) {
                _changed();
                if (_hashSet!=null) _hashSet.add(s);
                return true;
            } else return false;
        }

    @Override public boolean addAll(Collection<? extends T> c) { return addAll(MAX_INT,c); }

    @Override public boolean addAll(int pos, Collection<? extends T> c) {
        final int L=sze(c), size=size();
            if (L==0) return false;
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            Collection cAdd=c;
            if (0!=(_options&UNIQUE)) (cAdd=c instanceof Set ? c : new HashSet(c)).removeAll(v());
            if (pos>size) v().addAll(cAdd); else v().addAll(pos, cAdd);
            final boolean changed=size!=size();
            if (changed) {
                _changed();
                if (_hashSet!=null) _hashSet.addAll(c);
            }
            return changed;
        }

    @Override synchronized public final void add(int idx, T s) {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            if (s!=null && !(0!=(_options&UNIQUE)&&contains(s))) {
                final int i=idx<0 ? 0 : idx;
                if (i<size()) try { v().add(i,s); } catch(Throwable e){ v().add(s);}
                else v().add(s);
                _changed();
                if (_hashSet!=null) _hashSet.add(s);
            }
        }
    /* --- Mixed add and remove --- */

    @Override synchronized public boolean remove(Object o) {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            if (v().remove(o)) {
                 _changed();
                 if (_hashSet!=null) _hashSet.remove(o);
                return true;
            } else return false;
        }

    @Override synchronized public T set(int i,T o) {
            if (0!=(_options&ASSERT_EDT)) assrtEDT();
            if (i>=size() || i<0) return null;
            if (o==null || get(i)==o) return o; /* no check, otherwise Collections.sort does not work */
            _changed();
            if (_hashSet!=null) _hashSet.remove(get(i));
            return v().set(i,o);
        }

    /* <<< implements List <<< */
    /* ---------------------------------------- */
    /* >>> asArray >>> */
    public synchronized T[] asArray() {
            final List v=v();
            final int size=size();
            final Class c=getClazz();
            if (size==0) return (T[])emptyArray(c);
            Object[] a=(Object[])deref(_refArray);
            if (a==null || a.length!=size || _asArray_mc!=_mc) {
                _asArray_mc=_mc;
                a=(Object[])java.lang.reflect.Array.newInstance(c,size);
                if (_ref) {
                    for(int i=size; --i>=0;) a[i]=getRmNull(i,this);
                } else {
                    try { v().toArray(a); } catch(Throwable ex){ putln("Caught in UniqueList ",c); putln(ex); }
                }
                final Object[] noNull=rmNullA(a,c);
                if (a!=noNull) while(v.remove(null));
                _refArray=wref(a=noNull);
            }
            return (T[]) a;
        }
    public Object[] elementData() {
        final List v=_v;
        final Object[] oo=v==null ?  NO_OBJECT : v instanceof MyVect ? ((MyVect)v).getData() : null;
        return oo!=null ? oo : asArray();
    }
    /* <<< asArray <<< */
    /* ---------------------------------------- */
    /* >>> Convenience >>> */

    public boolean addAllA(T...oo) {
        boolean changed=false;
        if (oo!=null ) for(T o : oo) changed|=add(o);
        return changed;

    }
    /* <<< Convenience <<< */
    /* ---------------------------------------- */
    /* >>>  Appearance >>> */

    public String getText() { return _label;}
    public UniqueList<T> t(String text) { _label=text;return this;}
    public UniqueList<T> i(Icon icon) { _ic=icon; return this;}
    public UniqueList<T> i(String icon) { _ic=icon; return this;}
    public UniqueList like(Object l) { return t(getTxt(l)).i(getIcn(l)); }

    @Override public String toString() {
        return
            0!=(_options&TO_STRING_SUPER) ? v().toString() :
            _label!=null ? _label+"#"+sze(this) : v().toString();
    }
    @Override public Object getRenderer(long options, long rendOptions[]) { return _label+" #"+size(); }

    /* <<< Appearance <<< */
   //========================================
    /* >>> Hooks >>> */
    public final static int HOOK_ACCESS=1, HOOK_CHANGE=0;
    private ChRunnable[][] _hooks;
    public UniqueList addHook(int type, ChRunnable r, String s) {
        if (r!=null && s!=null) {
            if (_hooks==null) { _hooks=new ChRunnable[2][]; _hooksID=new String[2][]; }
            _hooksID[type]=adToStrgs(s,_hooksID[type],0);
            _hooks[type]=adToArray(r, _hooks[type],0, ChRunnable.class);
        }
        return this;
    }
    private void runHooks(int type) {
        if (_hooks==null || _hooks[type]==null) return;
        for(int i=_hooks[type].length; --i>=0;) _hooks[type][i].run(_hooksID[type][i],this);
    }
    private void _changed() {
        _mc++;
        if (_hooks!=null && _hooks[HOOK_CHANGE]!=null) runHooks(HOOK_CHANGE);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    @Override public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_GET_ICON) return _ic;
        if (id==ChRunnable.RUN_GET_ITEM_TEXT) return _label;
        if (id==PROVIDE_TEXT_FOR_SORTING) return _label;
        if (id==PROVIDE_JTREE_NODE_ID)  return sze(_label)>0 ? _label :  _nodeId==null ? _nodeId=intObjct(hashCode()) : _nodeId;
        if (id==ChRunnable.RUN_SET_TREE_VALUE) _rendererComp=arg;
        if (id==MAY_PROVIDE_TREE_VALUE) return _rendererComp;
        return null;
    }

    private static class MyVect extends Vector {
        static boolean error;
        public Object[] getData() {
            try {
                return error ? null : super.elementData;
            } catch(Throwable ex){ error=true; assrt(); }
            return null;
        }
    }
}
//http://java.sun.com/j2se/1.5.0/docs/guide/collections/reference.html  Collections
