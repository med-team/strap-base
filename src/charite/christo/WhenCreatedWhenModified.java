package charite.christo;
public interface WhenCreatedWhenModified {
    long whenModified();
    long whenCreated();
}
