package charite.christo;
/**
  @author Christoph Gille
*/
public interface IsEnabled {
   boolean isEnabled(Object o);

}
