package charite.christo;
import java.awt.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ListDataEvent;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.lang.System.currentTimeMillis;
/**
This utility class manages threads.
It fulfills same task as SwingWorker in JRE6
Methods in  Swing are not thread safe.  Exceptions: #repaint() #revalidate()
   @author Christoph Gille
*/
public class ChDelay extends Thread implements ChRunnable {
    public final static byte
        ENABLED=1, REPAINT=2,
        CONTENTS_CHANGED=4,
        SET_PREF_SIZE=5,
        VISIBLE=6,
        SET_JLIST_II=11,
        FG_COLOR=14, BG_COLOR=15,
        UPDATEUI=17, ADDTAB=19, REVALIDATE=20,RUN=21,
        RUNNABLE_ARG=28, ADD_COMPONENT=29,
        MAX_VARIABLE=34;
    public final static int DONT_SET_DURATION=4;
    static { assert EDT!=DONT_SET_DURATION && PAUSE!=DONT_SET_DURATION;}
     /* ---------------------------------------- */
    /* >>> The Job instance >>> */
    public final long LAST[];
    private final Object _object, _data;
    private final byte _actionID;
    private long _options, _nextInvoke;

    public ChDelay(long options, int id, Object o, Object data) { this(options,id,o,0,data,(long[])null);}
    public ChDelay(long options, int id, Object o, long delay, Object data, long[] duration) {
        _actionID=(byte)id;
        _object=o;
        _nextInvoke=currentTimeMillis()+delay;
        _data=data;
        this.LAST=duration;
        _options=options;
        setName("ChDelay");
    }
    @Override public String toString() {
        final String[] names=finalStaticInts(ChDelay.class, MAX_VARIABLE);
        final Object runID=_actionID!=RUNNABLE_ARG ? "" : _data instanceof String ? _data : get(0,_data);
        return super.toString()+shortClNam(_object)+"."+get(_actionID, names)+" "+runID;
    }
    private static String shortClNam(Object o0) {
        final Object o=o0 instanceof ChDelay ? ((ChDelay)o0)._object : o0;
        if (o==null) return "";
        final Class c= o instanceof Class ? (Class)o : o.getClass();
        final String cn=c.getName();
        return cn.substring(cn.lastIndexOf('.')+1);
    }
    /* <<< The Job instance <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    //static int countDebug;
    @Override public void run() {
        final long time=currentTimeMillis();
        try {
            runJob(_options, _actionID, _object, _data);
        } catch(Throwable ex) {
            putln("ChDelay actionID="+_actionID+"  options="+_options+(_data instanceof String ? "data="+_data : ""));
            stckTrc(ex,System.err);
        }
        final long[] d=this.LAST;
        if ((_options&DONT_SET_DURATION)==0 && d!=null && d.length>0) d[DURATION]=(d[WHEN]=currentTimeMillis())-time;
    }
    private final static java.util.List<ChDelay> vQueue=new Vector();
    private static ChDelay _inst;
    private static ChDelay instance() {if (_inst==null) _inst=new ChDelay(0,0,null,0,null,null); return _inst;}
    private static boolean _isRunning;
    private static void runJob(long opt, int actionID, Object object0,Object data) {
        final Object object=deref(object0), d0, d1;
        if (object==null) return;
        if (data instanceof Object[]) {
            final Object dd[]= (Object[])data;
            d0=dd.length>0?dd[0]:null;
            d1=dd.length>1?dd[1]:null;
        }  else { d0=d1=null; }
        switch(actionID) {
        case ADD_COMPONENT:
            final Component o=object instanceof Component ? (Component)object : pnl(object);
            final Container c=(Container)d0;
            final Object constraint=d1;
            if (c==null || o==null) break;
            if (constraint!=null) c.add(o,constraint); else c.add(o);
            revalAndRepaintC(o);
            break;
        case RUN:((Runnable)object).run(); break;
        case RUNNABLE_ARG:
            ((ChRunnable)object).run((String) (d0!=null?d0: data), d1);
            break;
        }
        if (actionID==VISIBLE) {
            setVisblC(data==Boolean.TRUE, object, 0);
        } else {
            if (object instanceof Component) runJob(actionID,(Component)object,data);
            if (object instanceof Object[]) {
                for(Object o : ((Object[])object)) {
                    o=deref(o);
                    if (o instanceof Component) runJob(actionID,(Component)o,data);
                }
            }
        }
    }

    private static void runJob(int actionID, Component c, Object data) {
        final JComponent jc=c instanceof JComponent ? (JComponent)c : null;
        switch(actionID) {
        case CONTENTS_CHANGED:
            updateListTableCombo(jc);
            break;
        case SET_PREF_SIZE:
            final Dimension d=data instanceof Dimension ? (Dimension)data : null;
            if (d!=null) jc.setPreferredSize(d);
            break;
        case ENABLED:
          setEnabld(data==Boolean.TRUE?true : data==Boolean.FALSE?false: c.isEnabled(), c);
            break;
        case REVALIDATE:
            pcp(KEY_NEEDS_REVALIDATE,null,c);
            revalAndRepaintC(c);
            break;
        case REPAINT:
            c.repaint();
            break;
        // case RESTORE_BACKGROUND:
        //     Color col=(Color)gcp(KEY_BACKGROUND,c);
        //     if (col!=null) c.setBackground(col);
        //     break;
        case UPDATEUI:
            jc.updateUI();
            break;
        case ADDTAB:
            final JTabbedPane tp=(JTabbedPane)data;
            if (tp!=null) {
                adTab(0, null, c, tp);
                if (tp.getSelectedIndex()==tp.getTabCount()-2 || tp.getSelectedIndex()==0) tp.setSelectedComponent(c);
                pcp(JTabbedPane.class,null,c);
            }
            break;
        case SET_JLIST_II:
            final int ii[]=(int[])data;
            ((ChJList)c).setSelII(ii);
            break;
        case FG_COLOR: {
            c.setForeground((Color)data);
            break;
        }
        case BG_COLOR: {
            if (data!=null) c.setBackground((Color)data); else LAFChooser.setDefaultBG(c);
            break;
        }
        }
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> Public API  >>> */
    public static void afterMS(long options, int id, Object object, long delay,Object data) {
        if (object==null) return;
        if (delay==0 && isEDT()) {
            runJob(options, id, object, data);
        } else if (id==RUN && object instanceof ChDelay) {
            final ChDelay j=(ChDelay)object;
            j._options=options;
            j._nextInvoke=Long.MAX_VALUE;
            enqueue(j);
            final long time=currentTimeMillis();
            final long[] d=j.LAST;
            if ((options&PAUSE)!=0 && d!=null && d.length>0 && time-d[WHEN]>delay) {
                if ((options&EDT)!=0) inEDT(j); else j.run();
                vQueue.remove(j);
            } else {
                j._nextInvoke=time+delay;
            }
        } else  {
            afterMS(options, id, object, delay, data, null);
        }
    }
    public static void afterMS(long options, int id, final Object object, long delay, Object data, long duration[]) {
        if (object==null)  return;
        final ChDelay j=getJob(object,id, data);
        final long time=currentTimeMillis();
        if (duration!=null && duration.length>0 && time-duration[WHEN]>delay) {
            if (j!=null) j._nextInvoke=Long.MAX_VALUE;
            if (isEDT()|| (options&EDT)==0)  runJob(options, id, object, data);  else inEDT(new ChDelay(options, id,object,delay,data,duration));

            if (j!=null) vQueue.remove(j);
        } else {
            if (j!=null  && !(id==RUNNABLE_ARG && data==j._data) ) {
                if (j._nextInvoke>time+delay) j._nextInvoke=time+delay;
            } else {
                enqueue(new ChDelay(options, id,object,delay,data,duration));
            }
        }
    }
    private static void enqueue(ChDelay j) {
        if (!_isRunning) {
            _isRunning=true;
            startThrd(thrdCR(instance(),"LOOP"));
        }
        adUniq(j,vQueue);
    }
    public static void toContainer(Object o,Container c, Object constraint,long delay) { afterMS(EDT,ADD_COMPONENT, o, delay,new Object[]{c,constraint},(long[])null); }
    public static void runAfterMS(long options,Runnable r, long delay) { afterMS(options,RUN,r,delay, (Object)null);}
    public static void revalidate(Object c, long delay) { afterMS(DONT_SET_DURATION,REVALIDATE,c,delay,null, (long[])null);}
    public static void repaint(Object c,long delay) { afterMS(DONT_SET_DURATION,REPAINT,c,delay,null,(long[])null);}
    public static void repaint(Object c,long delay, long duration[]) { afterMS(0,REPAINT,c,delay,null, duration);}
    public static void updateUI(Object c,long delay) { afterMS(EDT,UPDATEUI,c,delay,null,null); }
    public static void setSelII(ChJList c, int ii[],long delay) { afterMS(EDT,SET_JLIST_II,c,delay,ii,(long[])null);}
    public static void fgBg(boolean bg, Object c,Color col,long delay) { afterMS(EDT,bg ? BG_COLOR : FG_COLOR,c,delay,col,(long[])null);}
    public static void addTab(JComponent c, JTabbedPane tp, long delay) { afterMS(EDT,ADDTAB,c,delay,tp,(long[])null);}
    public static void contentsChanged(JComponent c, long delay) {  afterMS(EDT,CONTENTS_CHANGED,c,delay,null,(long[])null);}
    public static void setEnabled(Component c, boolean b,long delay) { if (c!=null) afterMS(EDT,ENABLED,c,delay, boolObjct(b),(long[])null);}
    public static void setVisible(Object c, boolean b,long delay) { if (c!=null) afterMS(EDT,VISIBLE,c,delay, boolObjct(b),(long[])null);}

    public static void parentWindowToFront(Component c,int afterMs){  startThrd(thrdCR(instance(),"TO_FRONT",new Object[]{intObjct(afterMs),c})); }
    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="HLB") highlightButton( (Component)argv[0], atoi(argv[1]));
        if (id=="TO_FRONT") {
            try {
                sleep(atoi(argv[0]));
                final Window d=parentWndw(argv[1]);
                if (d!=null && d.isVisible()) { d.toFront(); d.setVisible(true);}
            } catch(Exception e){}
        }
        if (id=="LOOP") {
            while(true) {
                final long ct=currentTimeMillis();
                for(int i=vQueue.size(); --i>=0;) {
                    ChDelay job=null;
                    try {
                        job=get(i,vQueue, ChDelay.class);
                        if (job==null||job._nextInvoke>ct) continue;
                        if ( (job._options&EDT)!=0) SwingUtilities.invokeAndWait(job); else job.run();
                    } catch(Throwable ex){
                        stckTrc(ex);
                    }

                    vQueue.remove(job);
                }
                ChUtils.sleep(55);
            }
        }
        return null;
    }
    // ++++++++++++++++++++++++++++++++++++++++
    /* <<< Public API <<< */
    /* ---------------------------------------- */
    /* >>> Static Utils >>> */
    private static ChDelay getJob(Object object,int id, Object data) {
        for(int i=0; i<vQueue.size(); i++) {
            final ChDelay j=get(i,vQueue,ChDelay.class);
            if (j!=null && object==j._object && (id<1 || id==j._actionID) && (data==null || data==j._data)) {
                return j;
            }
        }
        return null;
    }
    public static boolean contains(Object c) { return getJob(c,0,null)!=null;}
    public static void remove(Object o,int id) {
        while(true) {
            final ChDelay j=getJob(o,id,null);
            if (j==null) return;
            vQueue.remove(j);
        }
    }
    private static ListDataEvent listDataEvent;
    private static void updateListTableCombo(JComponent c) {
        if (c==null) return;
        final ListModel lm=c instanceof JList ? ((JList)c).getModel() : c instanceof JComboBox ? ((JComboBox)c).getModel() : null;
        if (lm instanceof AbstractListModel) {
            final AbstractListModel alm=(AbstractListModel)lm;
            if (listDataEvent==null) listDataEvent=new ListDataEvent(ChDelay.class, ListDataEvent.CONTENTS_CHANGED,0, MAX_INT);
            for(javax.swing.event.ListDataListener l : alm.getListDataListeners()) {
                l.contentsChanged(listDataEvent);
            }
        }
        if (c instanceof JTable) {
            final JTable jt=(JTable)c;
            final javax.swing.table.TableModel tm=jt.getModel();
            if (tm!=null) jt.tableChanged(new javax.swing.event.TableModelEvent(tm));
        }
        revalAndRepaintC(c);
        pcp(KEY_NEEDS_REVALIDATE,null,c);
    }

    public static void highlightButton(Component b, int duration) {
        if (myComputer()) return;
        if (b==null) return;
        if (!isEDT()) inEdtLater(thrdCR(instance(), "HLB", new Object[]{b,intObjct(duration)}));
        else {
            setEnabld(true,b);
            revalAndRepaintCs(b);
            remove(b,0);
            //   setBG(0xFF00,b);
            fgBg(true,b,  (Color)null ,duration);
            /*ChJMenu*/
            pcp(KEY_BACKGROUND, C(0xFFFF), b);
            runAfterMS(0L, thread_pcp(KEY_BACKGROUND, null, b), duration);
            /*Childs*/
            for(Object o : oo(gcp(ChButton.KEY_MENU_ITEMS,b)))  highlightButton(derefC(o), duration);
        }
    }

}
