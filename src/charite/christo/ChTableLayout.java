package charite.christo;
import static charite.christo.ChUtils.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**
   Container that arranges components horizontally.
   A header allows dragging of columns and resizing like in a JTable.
   @author Christoph Gille
   // BasicTableHeaderUI JScrollPane BasicScrollPaneUI
   */
public final class ChTableLayout extends ChPanel implements ChRunnable, ProcessEv, javax.swing.table.TableCellRenderer, TooltipProvider, PaintHook {
    public final static String COLUMN_ADDED="CTL$$CA", COLUMN_REMOVED="CTL$$CRM",   KEY_OFFSET_COMPONENT="CTL$$OC", KEY_HEIGHT_SOUTH_PANEL="$CTL$$HSP";
    private final static Object KEY_ITEM=new Object();
    private final static int MIN_SIZE=EM, DIVIDER=EM, SQUEEZE=2*EM;
    private final static Rectangle RECT_ERROR=new Rectangle();
    private final static String[] _msgUndock={null};
    private final ChButton LABEL=labl();
    private boolean  _dragged, likeDivider, likeDividerWhenMoved, _resizing;
    private int _resizingCol=-1, _selCol=-1, _labelCol, _colUnderMouse=-1;
    private Runnable _tDivider;
    private final ArrayList<Object> _v=new ArrayList();
    private final ChJTable _t;
    private final long _opt;
    public ChTableLayout(long options) {
        _opt=options;
        setLayout(null);
        (_t=new ChJTable(tm,options)).setPreferredSize(dim(0,0));
        final JTableHeader th=th();
        addActLi(evAdapt(this).addTo("mM", th).addTo("c",this), _t);
        if (0!=(options&ChJTable.CHJTABLE_FOCUSED_BLACK)) th.setDefaultRenderer(this);
        th.setPreferredSize(dim(9999,EX));
        add(th);
        add(_t);
        monospc(th);
        setTip(this, LABEL);
        th.setResizingAllowed(true);
    }
    public ChJTable t() { return _t;}
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Event  >>> */
    private final javax.swing.border.Border H_BORDER=hBorder(), BORDER=hBorder();
    private javax.swing.border.Border hBorder() {
        try { return new javax.swing.plaf.metal.MetalBorders.TableHeaderBorder(); } catch(Throwable t){}
        return null;
    }
    private void recursiveAddMoli(Component c, Object item) {
        evAdapt(this).addTo("mf",c);
        if (c instanceof Container) {
            for(Component child : ((Container)c).getComponents()) recursiveAddMoli(child,item);
        }
    }
    private static int xButton(int type, int rw) {
        final int
            xClose=-EM-(rw>4*EM?EM:EM/2),
            xMaxim=xClose-EM-(rw>3*EM?EM/2:2);
        return type==ChRenderer.SMALL_CLOSE ? xClose : type==ChRenderer.SMALL_MAXIM ? xMaxim : 0;
    }

    public int getSelectedIndex() { return _selCol; }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void processEv(AWTEvent ev) {
        final int id=ev.getID(), modi=modifrs(ev), x=x(ev), y=y(ev), nCol=sze(_v);
        final Object q=ev.getSource(),  item=orO(gcp(KEY_ITEM,q),q);
        final JComponent comp=derefJC(q);
        final String cmd=actionCommand(ev);
        final boolean ownRenderer=0!=(_opt&ChJTable.CHJTABLE_FOCUSED_BLACK);
        if (id==ActionEvent.ACTION_PERFORMED) {
            if (cmd==ChJTable.COLUMN_MARGIN_CHANGED ||cmd==ChJTable.COLUMN_MOVED) {
                inEDTms(THREAD_LOCATION,99);
                THREAD_LOCATION.run();
            }
        }
        final JTableHeader th=th();
        if (q==th) {
            Object removeColumn=null;
            final int hw=th.getWidth(), h=th.getHeight();
            if (id==MOUSE_EXITED || id==MOUSE_RELEASED) {
                th.repaint();
            }

            for(int col=getColumnCount(); --col>=0;) {
                final Object component=get(col,_v);
                final boolean disposable=null!=gcp(KEY_CLOSE_OPT,component);
                final Object headerCanResizeV=gcp(KEY_LOOKS_LIKE_DIVIDER, th);
                final Rectangle rectCol=rectangleAt(col);
                if (rectCol==RECT_ERROR) continue;
                final boolean isInColumn=x(rectCol)<x && x<x2(rectCol);
                 if (isInColumn && (id==MOUSE_MOVED && col!=_colUnderMouse || id==MOUSE_ENTERED || id==MOUSE_PRESSED || id==MOUSE_CLICKED)) {
                     if (headerCanResizeV!=null || !ownRenderer) {
                         if (_tDivider==null) _tDivider=thrdCR(this,"DSB");
                         inEDTms( _tDivider, 50);
                     }
                     for(int iType=2; --iType>=0;) {
                        int type=0;
                        switch(iType) {
                        case 0:
                            type=ChRenderer.SMALL_CLOSE;
                            if (!disposable) continue;
                            break;
                        case 1:
                            if (0==(_opt&ChJTable.CHJTABLE_SHOW_BUT_MAXIM) || nCol<2) continue;
                            type=ChRenderer.SMALL_MAXIM;
                            break;
                        }
                        final int xBut=x2(rectCol)+xButton(type,rectCol.width);
                        if (xBut<0) continue;
                        if (!ownRenderer) {
                            ChRenderer.setSmallButtonX(type, xBut, q);
                            ChRenderer.drawSmallButtons(th, null);
                        }
                        if (xBut<x && x<xBut+EM && id==MOUSE_CLICKED) {
                            if (type==ChRenderer.SMALL_CLOSE) removeColumn=component;
                            else narrowAllOthers(component, 3*EM);
                        }
                    }
                }
                if (id==MOUSE_EXITED && (headerCanResizeV!=null || disposable)) th.repaint();

                if (id==MOUSE_DRAGGED && q==th && 0==undockEnabld() && gcp(KOPT_UNDOCKABLE,component)!=null ) {
                    final Component pnl=getPnl(component);
                    _msgUndock[0]=ANSI_W_ON_B+"Hint: Panels can be dragged out, so-called undocking";
                    if (!(pnl instanceof ChPanel))  drawMsg(DRAW_MSG_SET_COLOR,_msgUndock,pnl,(Graphics)null,0);
                }
                if (_dragged
                    && id==MOUSE_RELEASED
                    && gcp(KOPT_UNDOCKABLE,component)!=null
                    && col==_colUnderMouse
                    && !_resizing
                    && null==th.getResizingColumn()
                    && (y<-EX || y>EX+h || x<-EX || x>EX+hw)) {
                  inEDTms( thread_pnlUndock(component, xScreen(ev)-EX, yScreen(ev)-EX/2, (modi&CTRL_MASK)==0), 333);
                }
                if (id==MOUSE_MOVED) {
                    if (x(rectCol)+EM<x && x<x2(rectCol)-3*EM && headerCanResizeV!=null) {
                        final Cursor curs=deref(headerCanResizeV, Cursor.class);
                        th.setCursor(curs!=null?curs :cursr('n'));
                    }

                    if (isInColumn && _colUnderMouse!=col) {
                        _colUnderMouse=col;
                        th.repaint();
                    }
                }
            }
            if (removeColumn!=null) {
                final int opt=((modi&CTRL_MASK)==0?CLOSE_ASK:0) | closeOpts(removeColumn);
                inEDTms(thrdCR(this,"X",new Object[]{intObjct(opt),  removeColumn}),99);
            }
        }
        /* >>> Focussed gets a Border >>> */
        final int col=item==null ? -1 : _v.indexOf(item);
        if (id==MOUSE_PRESSED || id==FocusEvent.FOCUS_GAINED) {
            if (col>=0 && this._selCol!=col) {
                this._selCol=col;
                if (ownRenderer) {
                    if (BORDER!=null) {
                        for(int i=nCol; --i>=0;) {
                            final JComponent pnl=derefJC(getPnl(get(i,_v)));
                            if (pnl!=null) pnl.setBorder(i==_selCol?BORDER:null);
                        }
                    }
                }
                final JComponent pnlOffset=gcp(KEY_OFFSET_COMPONENT,q,JComponent.class);
                if (pnlOffset!=null && col>0) {
                    pnlOffset.setPreferredSize(dim(  (int)(EX*Math.log((col*8+1)))    ,0));
                    revalAndRepaintC(pnlOffset);
                }
                th.repaint();
            }
        }
        /* >>> Begin Divider >>> */
        final JViewport viewPort=deref(parentC(comp), JViewport.class);
        int yMax=atoi(gcp(KEY_HEIGHT_SOUTH_PANEL,q));
        final int colConv=_t.convertColumnIndexToView(col);
        if ( (yMax==0 || yMax>y) &&
             viewPort==null &&
             (!(comp instanceof JScrollPane)) &&
             (!(comp instanceof JViewport)) &&
              colConv>=0 && colConv<nCol-1 && comp!=null) {

            final Rectangle rectCol=rectangleAt(col);
            final int w=rectCol.width;
            if (yMax==0) yMax=comp.getHeight();
            final boolean resizeCursorOld=likeDivider || _resizingCol==col;
            final int vpX=0;
            likeDivider=w-DIVIDER<=x-vpX && x-vpX<=w;
            if (id==MOUSE_DRAGGED) {

                if (!_dragged && (likeDividerWhenMoved || likeDivider)) _resizingCol=col;
            }
            if (id==MOUSE_MOVED) {
                _resizingCol=-1;
                likeDividerWhenMoved=likeDivider;
            }
            if (_resizingCol==col) {
                pcp(KEY_IS_RESIZING,"",comp);
                pcp(KEY_IS_RESIZING,"",q);
                chColSize(col,x, w);
            } else if (id==MOUSE_MOVED) {
                for(int iCol=getColumnCount(); --iCol>=0;) {
                    final Object component=get(iCol,_v);
                    pcp(KEY_IS_RESIZING,null,component);
                    pcp(KEY_IS_RESIZING,null,q);
                }
            }
            Rectangle rDiv=gcp(KEY_LOOKS_LIKE_DIVIDER,q,Rectangle.class);
            if (likeDivider && !_dragged && id!=MOUSE_EXITED) {
                if (rDiv==null) { rDiv=new Rectangle(); comp.repaint(); }
                setRect(w-EM,0,EM,yMax,rDiv);
                pcp(KEY_LOOKS_LIKE_DIVIDER,rDiv,comp);
                pcp(KEY_LOOKS_LIKE_DIVIDER,rDiv,item);
            } else {
                pcp(KEY_LOOKS_LIKE_DIVIDER,null,comp);
                pcp(KEY_LOOKS_LIKE_DIVIDER,null,item);
                if (rDiv!=null)comp.repaint();
            }
            if ( likeDivider || _resizingCol>=0 ) {
                if (!(comp instanceof ChPanel)) drawDivider(comp,null,w-EM,0,EM,yMax);
                comp.setCursor(cursr('e'));
            } else if (resizeCursorOld ) comp.setCursor(null);
        } /* End Divider */

        if (id==ComponentEvent.COMPONENT_RESIZED) {
            inEDTms(THREAD_LOCATION,333);
        }
        if (id==MOUSE_DRAGGED) _dragged=true;
        if (id==MOUSE_MOVED || id==MOUSE_RELEASED) {
            _dragged=_resizing=false;
            _msgUndock[0]=null;
        }
    }

    /* <<< Event  <<< */
    /* ---------------------------------------- */
    /* >>> Header >>> */
    public int columnWidth(int i) { return rectangleAt(i).width;}
    private Rectangle rectangleAt(int i) {
        try {
            if (i>=0) return th().getHeaderRect(t().convertColumnIndexToView(i));
        } catch(Throwable t){}
        return RECT_ERROR;
    }

    {
        addPaintHook(this,LABEL);
        setMinSze(0,0, LABEL);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        final boolean sel=column==this._selCol;
        LABEL.bg(sel ? 0xffFFff : DEFAULT_BACKGROUND).t(titleForObject(get(column,_v))).setBorder(H_BORDER);
        _labelCol=column;
        return LABEL;
    }
    public String provideTip(Object  objOrEv) {
        final Object c=evtSrc(objOrEv);
        if (c==LABEL) {
            final String t=LABEL.getText();
            if (strgWidth(LABEL,t)>columnWidth(_labelCol)) return t;
        }
        return null;
    }
    public boolean paintHook(JComponent c, Graphics g, boolean after) {
        if (after) {
            if (c==LABEL) {
                if (_t.convertColumnIndexToModel(_labelCol)==_colUnderMouse && null==th().getResizingColumn()) {
                    for(int iType=2; --iType>=0;) {
                        int type=0;
                        switch(iType) {
                        case 0:
                            type=ChRenderer.SMALL_CLOSE;
                            if (null==gcp(KEY_CLOSE_OPT, get(_labelCol,_v))) continue;
                            break;
                        case 1:
                            if (0==(_opt&ChJTable.CHJTABLE_SHOW_BUT_MAXIM) || sze(_v)<2) continue;
                            type=ChRenderer.SMALL_MAXIM;
                            break;
                        }
                        final int xBut=xButton(type,c.getWidth());
                        if (xBut<0) {
                            ChRenderer.setSmallButtonX(type,xBut,c);
                        }
                    }
                    ChRenderer.drawSmallButtons(c, g);
                }
            } else {
                final Rectangle divider=gcp(KEY_LOOKS_LIKE_DIVIDER,c,Rectangle.class);
                if (divider!=null && !_dragged) drawDivider(c,g, x(divider), y(divider), divider.width, divider.height);
                if (_msgUndock[0]!=null && c==getPnl(get(_colUnderMouse, _v))) drawMsg(DRAW_MSG_SET_COLOR, _msgUndock,c,g,0);
            }
        }
        return true;
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Header >>> */
    public JTableHeader th() { return t().getTableHeader();}
    public ChTableLayout setTitle(int col, String title) {
        final TableColumn c=colV(col);
        if (c!=null) {
            c.setHeaderValue(toStrgN(title));
            th().repaint();
        }
        return this;
    }
    private TableColumn colV(int col0) {
        if (col0<0 || col0>=getColumnCount()) return null;
        return t().column(t().convertColumnIndexToView(col0));
    }
    /* <<< Header  <<< */
    /* ---------------------------------------- */
    /* >>> add remove get Columns >>> */
    public ChTableLayout addCol(int opt, Object p) {
        final Component panel=getPnl(p);
        if (panel!=null) {
            if (0!=(_opt&ChJTable.CHJTABLE_FOCUSED_BLACK)) {
                evAdapt(this).addTo("mM",panel);
                addPaintHook(this,derefJC(panel));
            }
            recursiveAddMoli(panel,wref(p));
        }
        if (adUniq(p,_v)) {

            fixColumnModel();
            handleActEvt(this,COLUMN_ADDED,0);
        }
        pcpAddOpt(KEY_CLOSE_OPT,opt&CLOSE_MASK,p);
        return this;
    }
    private void fixColumnModel() {
        _t.createDefaultColumnsFromModel();
        layoutColumns();
        repaint();
        t().tableChanged(new javax.swing.event.TableModelEvent(tm));
    }
    public void removeColumn(Object... pp) {
        if (pp==null) return;
        boolean changed=false;
        for(Object p: pp) {
            if (_v.remove(p)) {
                changed=true;
                if (p instanceof Component) ((Component)p).removeMouseListener(evAdapt(this));
            }
        }
        if (changed) {
            fixColumnModel();
            handleActEvt(this,COLUMN_REMOVED,0);
        }

    }
    public Object[] getAllComponents() { return _v.toArray();}
    public void layoutColumns() {
        for(Component c : getComponents()) {
            if (c!=_t && c!=th()) remove(c);
        }
        for(Object o : _v.toArray()) {
            final Component c=getPnl(o);
            add(c!=null ? c :  LABEL.fg(0).bg(0x0FFffFF).t("error"));
        }
    }
    /* <<< add remove get Columns <<< */
    /* ---------------------------------------- */
    /* >>> Thread  >>> */
    private final Runnable THREAD_LOCATION=thrdCR(this,"B");
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="DSB") {
            final JComponent th=th();
            if (null!=gcp(KEY_LOOKS_LIKE_DIVIDER, th)) drawDivider(th,null,0,0,-2*EM, EM);
            if (0==(_opt&ChJTable.CHJTABLE_FOCUSED_BLACK)) ChRenderer.drawSmallButtons(th, null);
        }
        if (id=="B") {
            final int n=sze(_v);
            int iDragged=-1;
            final TableColumn col=th().getDraggedColumn();
            if (col!=null)  iDragged=col.getModelIndex();
            final JTableHeader header=th();
            final int height=getHeight(), width=getWidth(), headerH=prefH(th());
            header.setBounds(0,0,width,headerH);
            _t.setBounds(0,headerH, width,1);
            for(int iC=0;iC<n;iC++) {
                final Component c=getPnl(get(iC,_v));
                final Rectangle r=c==null?null:rectangleAt(iC);
                if (r==RECT_ERROR || r==null) continue;
                final int x= x(r) + ( iC==iDragged ? header.getDraggedDistance() : 0 );
                final int x2=iC==n-1?width : x2(r);
                c.setBounds(x,y(r)+headerH, maxi(0,x2-x), maxi(0,height-headerH));

            }
            _t.revalidate();
            revalAndRepaintC(this);
        }
        if (id=="X") clos(atoi(argv[0]),   argv[1]);
        return null;
    }
    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> TableModel >>> */
    private final javax.swing.table.TableModel tm=new javax.swing.table.DefaultTableModel() {
            @Override public int getRowCount(){ return 0;}
            @Override public int getColumnCount() { return ChTableLayout.this.getColumnCount(); }
        };
    public int getColumnCount() { return sze(_v); }
    /* <<< TableModel <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    public void narrowAllOthers(Object c, int narrowWidth) {
        if (!isEDT()) {
            inEdtLater(thrdM("narrowAllOthers",this,new Object[]{c,intObjct(narrowWidth)}));
        } else {
            final int column=_v.indexOf(c), N=getColumnCount();
            final TableColumn tCol=colV(column);
            if (column<0 || column>=N || tCol==null || N<2) return;
            final int prefW=maxi(narrowWidth, th().getWidth()-narrowWidth*(N-1));
            for(int col=N; --col>=0;) {
                final TableColumn tc=colV(col);
                if (tc==null) continue;
                tc.setMaxWidth(30000);
                tc.setMinWidth(MIN_SIZE);
                tc.setResizable(true);
                tc.setPreferredWidth(tc==tCol ? prefW : narrowWidth);
            }
        }
    }
    public void chColSize(int column, int newWidth0, int oldWidth) {
        final TableColumn tCol=colV(column);
        final int N=getColumnCount();
        if (N<2 || tCol==null) return;
        final int newWidth=mini(newWidth0, th().getWidth()-(N-1)*SQUEEZE);
        final int nW=maxi(newWidth,SQUEEZE);
        int reduce=nW-oldWidth;
        for(int col=0; col<N; col++) {
            final TableColumn tc=colV(col);
            if (tc==null) continue;
            tc.setMaxWidth(30000);
            tc.setMinWidth(MIN_SIZE);
            final int oldW=tc.getWidth(), prefW;
            if (col>column) {
                prefW=maxi(SQUEEZE,oldW-reduce);
                reduce-=(oldW-prefW);
            } else prefW=oldW;
            tc.setPreferredWidth(prefW);
        }
        tCol.setPreferredWidth(nW-reduce);
        if (0!= (reduce+=nW-newWidth)) {
            tCol.setPreferredWidth(nW);
            for(int col=column; --col>=0 && reduce!=0;) {
                final TableColumn tc=colV(col);
                if (tc==null) continue;
                final int oldW=tc.getWidth(), newW=maxi(SQUEEZE,oldW-reduce);
                reduce-=(oldW-newW);
                tc.setPreferredWidth(newW);
            }
        }
    }
    private final Dimension _d=new Dimension(EX,0);
    public Dimension getPreferredSize() {
        int h=0;
        for(Object o :  getAllComponents()) h=maxi(h, prefH(o));
        _d.height=h+ EX+prefH(th());
        return _d;
    }

}
