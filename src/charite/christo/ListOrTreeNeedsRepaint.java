package charite.christo;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;

import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/*

Sieh BasicTreeUI
 */
public class ListOrTreeNeedsRepaint {

    private final static Object  KEY_HC=new Object();
    private ListOrTreeNeedsRepaint(){}
    public static boolean listNeedsRepaint( JList jl) {
        final ListModel m=jl!=null?jl.getModel() : null;
        final int L=sze(m);
        if (L==0) return false;

        long[] hash=gcp(KEY_HC, jl, long[].class);
        if (sze(hash)<L) pcp(KEY_HC, hash=new long[L+10], jl);
        final ListCellRenderer rend=jl.getCellRenderer();
        final int from=jl.getFirstVisibleIndex();
        final int to=jl.getLastVisibleIndex();
        boolean diff=false;
        for(int row=from; row<=to; row++) {
            final Object item=m.getElementAt(row);
            if (item==null) continue;
            final long hc;
            if (item instanceof HasRendererMC) hc=((HasRendererMC)item).getRendererMC();
            else {
                final Component c=rend!=null ? rend.getListCellRendererComponent(jl, item, row, false, false) : null;
                hc=rendCompHC(c);
            }
            if (hash[row]!=hc) {
                diff=true;
                hash[row]=hc;
                //putln("diff at "+row+"  hc="+hc+ "  item="+item);
            }
        }
        return diff;
    }

    /*
      final Object a=combo.getUI().getAccessibleChild(jc, 0);
      if (a instanceof javax.swing.plaf.basic.ComboPopup) {
      popupList=((javax.swing.plaf.basic.ComboPopup)a).getList();
      }
    */

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    public static boolean treeNeedsRepaint(JTree jt) {
        final int L=jt==null?0:jt.getRowCount();
        if (L==0) return false;
        long[] hash=gcp(KEY_HC, jt, long[].class);
        if (sze(hash)<L) pcp(KEY_HC, hash=new long[L+10], jt);
        final Rectangle visible=jt.getVisibleRect();
        final TreePath path0=jt.getClosestPathForLocation(0,visible.y);
        if (path0==null) return false;
        final int fromRow=jt.getRowForPath(path0);
        final int rowHeight=jt.getRowHeight();
        boolean diff=false;
        final TreeCellRenderer rend=jt.getCellRenderer();
        int y=jt.getPathBounds(path0).y, row=fromRow;
        for(; row<L; row++) {
            final TreePath tp=jt.getPathForRow(row);
            final Object item=tp.getLastPathComponent();
            if (item==null) continue;

            final long hc;
            if (item instanceof HasRendererMC) {
                hc=((HasRendererMC)item).getRendererMC();
            }
            else {
                final Component c=rend==null?null: rend.getTreeCellRendererComponent(jt, item, false, false, false,  row,false);
                hc=rendCompHC(c);
            }
            if (hash[row]!=hc) {
                diff=true;
                hash[row]=hc;
            }
            final int height=rowHeight>0 ? rowHeight : jt.getPathBounds(tp).height;
            if ((y+=height)>y2(visible)) break;
        }
        return diff;
    }

    /* <<< repaint <<< */
    /* ---------------------------------------- */
    /* >>> mayUpdate >>> */
    private static final Object keyListSize=new Object(), keyComboHC=new Object();
    static void mayUpdateList(JList c) {
        if (c==null) return;
        final ListModel m=c.getModel();
        final Object oSize=gcp(keyListSize,c);
        final int size=sze(m);
        if (oSize==null || atoi(oSize)!=size) {
            pcp(keyListSize,intObjct(size), c);
            revalAndRepaintC(c);
        }
        if (listNeedsRepaint(c)) c.repaint();
    }

    static void mayUpdateCombo(JComboBox c) {
        final ListModel m=c==null ? null : c.getModel();
        if (m==null) return;
        final Object item=c.getSelectedItem();
        boolean contains=false;
        for(int i=sze(m); !contains &&  --i>=0;) {
            final Object el=m.getElementAt(i);
            contains|=item==el || item instanceof CharSequence && item.equals(el);
        }
        if (!contains || item==null) {
            if (sze(m)>0) c.setSelectedItem(m.getElementAt(0));
        } else {
            final Object oHC=gcp(keyComboHC,c);
            final long hc=ListOrTreeNeedsRepaint.rendCompHC(item);
            if (oHC==null || atol(oHC)!=hc) pcp(keyComboHC, longObjct(hc), c);
        }
    }

    /* <<< mayUpdate  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    private static long rendCompHC(Object o) {
        if (o instanceof HasRendererMC) return ((HasRendererMC)o).getRendererMC();
        int hc=0;
        if (o instanceof Component) {
            final Component comp=(Component)o;
            if (comp!=null) {
                for(int iProp=3;--iProp>=0;) hc+=ChRenderer.hashC(iProp==0?getTxt(comp):iProp==1?comp.getForeground() : comp.getBackground());
            }
        }
        return hc;
    }

}
