package charite.christo;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   Proteins read from a PDB-file are selected.
   When a protein has more than one chain a new protein file is written for each chain.
   An underscore and the chain identifier letter is appended to the file name.
   @author Christoph Gille
*/
public class PDB_separateChains extends AbstractInteractiveFileProcessor {
     {
        setTextNorth("The following list shows all PDB  files with more than one chain.<br>"+
                     "Select the files to be split into one-chain- PDB-files.");
        setErrorNonSelected("none of these files are PDB files with more than one chain");
    }

    private final static int CHAIN_ID=21;
    @Override public String fileEntry(BA ba,File f) {
        final boolean alsoIfOnlyOneChain=true;
        if (f==null || ba==null) return null;
        final boolean chains[]=chainsInPDBfile('*',ba);
        if (!alsoIfOnlyOneChain && countTrue(chains)<2) return null;
        final BA sb=new BA(99).a(f).a(' ');
        final int last=lstTrue(chains);
        for(int i=0;i<sze(chains);i++) {
            if (chains[i])sb.a((char)i).a(i==last-1 ? " and " : " ");
        }
        return sb.toString();
    }

    @Override public File[] processFile(BA ba, File f, boolean overwrite) {
        final File[] vCreated=new File[128];
        int count=0;
        if (ba==null) {
            final int l=sze(f);
            if (l>0) ba=readBytes(f,ba4CurrThrd(PDB_separateChains.class));
        }
        if (ba!=null) {
            final boolean chains[]=chainsInPDBfile('*', ba);
            for(char iChain=' '; chains!=null && iChain<='z';iChain++) {
                if (chains[iChain]) {
                    final File fNew=file(newName(f,(char)iChain));
                    if (sze(fNew)==0 || overwrite) {
                        delFile(fNew);
                        writePdbOnlyChain(fNew, iChain, ba);
                    }
                    if (sze(fNew)>0) vCreated[count++]=fNew;
                }
            }
        }
        return chSze(vCreated,count,File.class);
    }
    public static boolean[] chainsInPDBfile(char type, BA ba) { /* type=[*|P] */
        final boolean het=type=='*';
        final boolean pep=type=='*' || type=='P';
        final int ends[]=ba.eol();
        final byte[] T=ba.bytes();

        boolean rr[]=null;
        for(int iL=0; iL<ends.length; iL++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1;
            if (b+CHAIN_ID>=ends[iL] || rr!=null && rr[T[b+CHAIN_ID] & 127]) continue;
            final int c0=T[b], c1=T[b+1], c2=T[b+2], c3=T[b+3], c4= T[b+4], c5=T[b+5];
            final boolean success=
                het && c0=='H'&&c1=='E'&&c2=='T'&&c3=='A'&&c4=='T'&&c5=='M' ||
                c0=='A'&&c1=='T'&&c2=='O'&&c3=='M'&&c4==' '&&c5==' ' && (pep && T[b+12]==' '&&T[b+13]=='C'&&T[b+14]=='A'&&T[b+15]==' ' || het);
            if (success) {
                if (rr==null) rr=new boolean[128];
                rr[T[b+CHAIN_ID] & 127]=true;
            }
        }
        return rr;
    }
    private static boolean writePdbOnlyChain(OutputStream fos, char chain, BA ba) throws IOException {
        final int
            ATOM=('A')+('T'<<8)+('O'<<16)+('M'<<24),
            CONE=('C')+('O'<<8)+('N'<<16)+('E'<<24),
            TER =('T')+('E'<<8)+('R'<<16)+(' '<<24),
            HETA=('H')+('E'<<8)+('T'<<16)+('A'<<24),
            HELI=('H')+('E'<<8)+('L'<<16)+('I'<<24),
            SHEE=('S')+('H'<<8)+('E'<<16)+('E'<<24),
            SEQR=('S'<<0)+('E'<<8)+('Q'<<16)+('R'<<24),
            ANIS=('A')+('N'<<8)+('I'<<16)+('S'<<24);  // ANISOU  OU

        if (ba==null) return false;
        final int ends[]=ba.eol();
        final byte[] T=ba.bytes();
        if (T==null) return false;
        boolean success=false;
        //putln(ba.toString());

        nextLine:
        for(int iL=0; iL<ends.length; iL++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1;
            final int l=ends[iL]-b;
            if (l>6) {
                final int first32bits= charsTo32bit(T,b);
                final int c4= T[b+4], c5=T[b+5];
                int idxChain=-1;
                boolean isHetNoChain=false;
                switch(first32bits){
                case ANIS: if (c4=='O'&& c5=='U')
                    continue nextLine;
                    break;
                case CONE:
                    if (c4=='C'&& c5=='T') continue nextLine;
                    break;
                case TER:
                    if (c4==' '&& c5==' ') continue nextLine;
                    break;
                case ATOM:
                    if (c4==' '&& c5==' ') idxChain=CHAIN_ID;
                    break;
                case HETA:
                    if (c4=='T'&& c5=='M' && l>19) {
                        idxChain=CHAIN_ID;
                        final int resName=charsTo32bit(T,17);
                        if (T[b+idxChain]==' ' &&
                            resName!=HETERO_HOH&&resName!=HETERO_HHO&&resName!=HETERO_OHH&&resName!=HETERO_H2O&&
                            resName!=HETERO_DOD&&resName!=HETERO_DDO&&resName!=HETERO_ODD&&resName!=HETERO_D2O) {
                            isHetNoChain=true;
                        }
                    }
                    break;
                case HELI:
                    if (c4=='X'&& c5==' ') idxChain=19;
                    break;
                case SHEE:
                    if (c4=='T'&& c5==' ') idxChain=21;
                    break;
                case SEQR:
                    if (c4=='E'&& c5=='S') idxChain=11;
                    break;
                }
                if (idxChain>0 && idxChain<l) {
                    final char c=(char)T[b+idxChain];
                    if (c==chain || isHetNoChain) success=true;
                    else continue nextLine;
                    if (isHetNoChain) putln(ba.newString(b, b+l));
                }
            }
            fos.write(T,b,l);
            fos.write('\n');
        }
        fos.flush();
        return success;
    }
    public static boolean writePdbOnlyChain(File f, char chain, BA ba) {
        if (ba==null) return false;
        OutputStream fos=null;
        boolean success=false;
        try {
            fos=fileOutStrm(f);
            success=writePdbOnlyChain(fos,chain,ba);
            return success;
        } catch(IOException e) { putln("Caught PDB_separateChains#writePdbOnlyChain ",e); return false;}
        finally {
            closeStrm(fos);
            if (success)  {
                for(int i=0;i<3; i++) if (sze(f)==0) sleep(99);
            } else delFile(f);
        }
    }
    private static String newName(File  f, char chain) {
        if (f==null) return null;
        final String fn=delSfx(".zip",delSfx(".Z",delSfx(".bz2",delSfx(".gz", f.getName()))));
        final char ch=is(LETTR_DIGT,chain)?chain:'_';
        final int i=fn.lastIndexOf('.');
        if (i<0 || i<fn.lastIndexOf('/') || i<fn.lastIndexOf('\\')) return new BA(99).a(fn).a('_').a(ch).a(".pdb").toString();
        return new BA(99).a(fn,0,i).a('_').a(ch).a(fn,i,MAX_INT).toString();
    }
}
