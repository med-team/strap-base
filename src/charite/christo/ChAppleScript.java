package charite.christo;
import java.io.*;
import static charite.christo.ChUtils.*;
public class ChAppleScript {
    static final String[] TITLE_DOWNLOADS={"Downloads", "Geladene Dateien", "Download Manager", "Descargas", "T\u00E9l\u00E9chargements"};
    public final static int MINIMIZE_DOWNLOADS=1;
    public final static File ff[]=new File[9];
    private ChAppleScript(){}
    public static File getScript(int type) {
        File f=ff[MINIMIZE_DOWNLOADS];
        if (null==f) {
            final BA sb=new BA(3333);
            String fn=null;
            if (type==MINIMIZE_DOWNLOADS) {
                fn="minimizeDownloads";
                for(String app : new String[]{"Firefox", "Safari"}) {
                    sb.a("try\ntell application \"").a(app).a("\"\n");
                    for(String window : TITLE_DOWNLOADS) {
                        sb.a(" try\n"+
                             "  tell window \"").a(window).a("\"\n")
                            .a("   set miniaturized to true\n"+
                               "  end tell\n"+
                               " end try\n\n");
                    }
                    sb.a("end tell\n"+
                         "end try\n\n");
                }

            }
            f=ff[MINIMIZE_DOWNLOADS]=file("~/@/applescript/"+fn+".applescript");
            wrte(f,sb);
        }
        return ff[MINIMIZE_DOWNLOADS];
    }

}
