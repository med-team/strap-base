package charite.christo;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Opens a JColorChooser.
   @author Christoph Gille
*/
public class ButColor extends ChButton {
    public final static int NO_TRANSPARENCY=1<<0, SHOW_PREVIEW_PANEL=1<<1, TRANSPARENCY_CHANGE_EVT=1<<2, NO_EVENT_IF_EQUALS=1<<3;
    private final Object[] _colored;
    private final Color _default, _reset=new Color(0x01020304,true);
    private final long _opt;
    private JSlider _slider;
    private JColorChooser _cs;
    private JComponent _panel;
    public ButColor(int opt, Color col, Object c) {
        _colored=oo(c);
        cp(KEY_COLOR, _default=col!=null ? col: Color.MAGENTA);
        _opt=opt;
        i(IC_COLOR).tt("Change color");
    }

    @Override public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        setAotEv(ev);
        if (_cs==null || !(q==_slider || q==_cs.getSelectionModel())) {
            super.processEv(ev);
        } else {
            final int alpha=_slider==null?-1 : ((255-_slider.getValue())&255);
            final Color c0=_cs.getColor();
            if (c0==_reset || c0==null || _panel==null) return;
            final Color c=
                alpha<0 ? c0 :
                new Color( (c0.getRGB()&0xFFffFF)| (alpha<<24), true);
            if (isVisbl(this)) {
                i(ChIcon.newFiltered(ChIcon.newFilterMonochrome(c.getRGB()),iicon(IC_COLOR)));
                repaint();
            }
            for(Object o : _colored) {            
                if (o instanceof Colored) ((Colored)o).setColor(c);
                if (o instanceof ActionListener && _panel!=null && !(q==_slider && 0==(_opt&TRANSPARENCY_CHANGE_EVT))) {
                    cp(KEY_COLOR,c);
                    final ActionEvent newEv=new ActionEvent(this,ActionEvent.ACTION_PERFORMED,ACTION_COLOR_CHANGED);
                    ((ActionListener)o).actionPerformed(newEv);
                    handleEvt(this, newEv);
                }
            }
            if (q==_cs.getSelectionModel() && 0==(_opt&NO_EVENT_IF_EQUALS)) {
                _cs.getSelectionModel().setSelectedColor(_reset);
            }
        }
    }
    @Override public Object run(String id, Object argv) {
        super.run(id,argv);
        if (id==RUN_PUSHED) run(ChRunnable.RUN_SHOW_IN_FRAME,null);
        if (id==ChRunnable.RUN_GET_PANEL) {
            if (_cs==null) {
                final EvAdapter li=evAdapt(this);
                setDragMovesWindow(true, _cs=new JColorChooser());
                for(JComponent c : childsR(_cs, JComponent.class)) li.addTo("mk",c);
                _cs.getSelectionModel().addChangeListener(li);
                if (0==(_opt&SHOW_PREVIEW_PANEL)) _cs.setPreviewPanel(new JLabel());
                final int alpha=_default!=null? _default.getRGB()>>>24 : 255;
                Object translucent=null;
                if (0==(_opt&NO_TRANSPARENCY)) {
                    final JSlider sl=_slider=new JSlider(0,255,255-alpha&255);
                    translucent=pnl(HB,"Transparency ",sl);
                    sl.addChangeListener(li);
                }
                _panel=pnl(CNSEW,_cs, translucent, gcp(KEY_SOUTH_PANEL, this));
            }
            return _panel;
        }
        if (id==ChRunnable.RUN_SHOW_IN_FRAME) {
            final Object pnl=run(ChRunnable.RUN_GET_PANEL,null);
            ChFrame.frame(orS(gcps(KEY_FRAME_TITLE,this), "color"), pnl, ChFrame.PACK|CLOSE_CtrlW_ESC).shw(ChFrame.AT_CLICK);
        }
        return null;
    }
}
