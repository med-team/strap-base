package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.mini;
// java charite.christo.protein.DNA_Util actacgatgac
/**
   @author Christoph Gille
*/
public class DNA_Util {
    private DNA_Util(){}
    // ----------------------------------------
    /*
    public static byte[] translateTriplet2aa(byte[] dna,boolean[] isTranslated) {
        return translateTriplet2aa(dna,isTranslated,false);
    }
    */
    // ----------------------------------------
    /** translates the nucleotide sequence this.dna into the amino acid sequence this.residueType
        depending on the array dnaDeletionsAndInsertions.
        With the variable dnaDeletionsAndInsertions one can compensate for sequenceing errors.
        A positive value in dnaDeletionsAndInsertions indicates a missing nucleotide,
        a negative value may compansate a nucleotide which shouldn't be there
*/
    public static byte[] translateTriplet2aaCompact(byte[] dna,boolean[] isTranslated) {
        return translateTriplet2aa(dna,isTranslated,true);
    }
    public static byte[] firstCodonToUpperCase(byte nuclSequ[]) {
        final int n=nuclSequ.length;
        for(int i=0,i3=0;i<n;i++) {
            if (i3==0) nuclSequ[i]&=~32; else nuclSequ[i]|=32;
            if (++i3==3) i3=0;
        }
        return nuclSequ;
    }
    private static byte[] translateTriplet2aa(byte[] dna,boolean[] isTranslated,boolean isCompact) {
        if (dna==null) return null;
        final int n=isTranslated!=null ? mini(dna.length,isTranslated.length) : dna.length;
        final int countTrue=isTranslated!=null ? ChUtils.countTrue(isTranslated) : dna.length;
        final byte[] aa=new byte[isCompact ?  countTrue/3 : n];
        if (!isCompact) Arrays.fill(aa,(byte)' ');
        int i=0,iAa=0;
        try {
            while(true) {
            byte trip0,trip1,trip2;
            while(isTranslated!=null && !isTranslated[i]) i++;
            final int i0=i;
            trip0=dna[i++];
            while(isTranslated!=null && !isTranslated[i]) i++; trip1=dna[i++];
            while(isTranslated!=null && !isTranslated[i]) i++; trip2=dna[i++];
            final byte a=triplet2aa[ (XACTG01234[trip0&255]<<6) + (XACTG01234[trip1&255]<<3) + (XACTG01234[trip2&255]) ];
            if (isCompact) aa[iAa++]=a;
            else aa[i0]=a;
            }
        } catch(Exception e){}
        return aa;
    }
    public final static byte triplet2aa(int trip0,int trip1, int trip2) {
        return triplet2aa[ (XACTG01234[trip0&255]<<6)  + (XACTG01234[trip1&255]<<3) + XACTG01234[trip2&255] ];
    }
    /** standard codon translation table */
    private final static String aa2triplet[][]={
        {"GCT","GCC","GCA","GCG"}, //A
        {}, //B
        {"TGT","TGC"}, // C
        {"GAT","GAC"}, // D
        {"GAA","GAG"}, //E
        {"TTT","TTC"},//F
        {"GGT","GGC","GGA","GGG"},//G
        {"CAT","CAC"},//H
        {"ATT","ATC","ATA"},//I
        {},//J
        {"AAA","AAG"},//K
        {"TTG","TTA","CTT","CTC","CTA","CTG"},//L
        {"ATG"},//M
        {"AAT","AAC"},//N
        {},//O
        {"CCT","CCC","CCA","CCG"},//P
        {"CAA","CAG"},//Q
        {"CGT","CGC","CGA","CGG","AGA","AGG"},//R
        {"TCT","TCC","TCA","TCG","AGT","AGC"},//S
        {"ACT","ACC","ACA","ACG"},//T
        {},//U
        {"GTT","GTC","GTA","GTG"},//V
        {"TGG"},//W
        {},//X
        {"TAT","TAC"},//Y
        {}//Z
    };
    // ----------------------------------------
    private final static int XACTG01234[];
    static {
        int nn[]=new int[256];
        nn['A']=nn['a']=1;  nn['C']=nn['c']=2; nn['T']=nn['t']=3; nn['G']=nn['g']=5;
        XACTG01234=nn;
    }
    // ----------------------------------------
    /** standard codon translation table  triplet2aa['T']['G']['G'] returns W for Tryptophan */

    private final static byte[] triplet2aa=triplet2aa();
    private static byte[] triplet2aa() {
        byte r[]=new byte[(7<<6) + (7<<3) +7];
        Arrays.fill(r,(byte)'X');
        for(char iAa='A';iAa<='Z';iAa++) {
            final String triplets[]=aa2triplet[iAa-'A'];
            for(String s:triplets){
                final byte  trip[]=s.getBytes();
                final int n0=XACTG01234[trip[0]],n1=XACTG01234[trip[1]],n2=XACTG01234[trip[2]];
                final int idx=(n0 << 6) + (n1 <<3) + n2;
                r[idx]=(byte)iAa;
            }
        }
        return r;
    }
    // ----------------------------------------
    public final static byte COMPLEMENT[]=new byte[256];
    static {
        COMPLEMENT['a']=(byte)'t'; COMPLEMENT['t']=(byte)'a'; COMPLEMENT['c']=(byte)'g'; COMPLEMENT['g']=(byte)'c';
        COMPLEMENT['A']=(byte)'T'; COMPLEMENT['T']=(byte)'A'; COMPLEMENT['C']=(byte)'G'; COMPLEMENT['G']=(byte)'C';
    }
    public static byte complement(byte b) { return b>0?COMPLEMENT[b]:0;}
    public static byte[] complement(byte[] dna,int len) {
        final int n=mini(dna.length,len);
        final byte compl[]=new byte[n];
        for(int i=n; --i>=0;) {
            final byte c=COMPLEMENT[dna[i]];
            compl[i]=c!=0 ? c : dna[i];
        }
        return compl;
    }
    // ----------------------------------------
    public static byte[] reverse(byte[] dna,int len) {
        final int n=mini(dna.length,len);
        final byte rev[]=new byte[n];
        for(int i=0;i<n;i++)  rev[i]=dna[n-i-1];
        return rev;
    }

    // error GGCACCCCTGAGGGGCTGTATCTCTAA
    public static byte[] reverseComplementDestructive(byte[] dna,int to) {

        for(int i=0,iRev=mini(dna.length,to)-1; i<iRev; i++,iRev--) {
            byte bTemp1=COMPLEMENT[dna[iRev]];
            if (bTemp1==0) bTemp1= dna[iRev];
            byte bTemp2=COMPLEMENT[dna[i]];
            if (bTemp2==0) bTemp2= dna[i];
            dna[iRev]=bTemp2; dna[i]=bTemp1;
        }
        return dna;
    }

    public static byte[] onlyTranslated(byte[] dna,boolean isTranslated[]) {
        if (isTranslated==null || dna==null) return dna;
        final int n=mini(isTranslated.length,dna.length);
        final byte bb[]=new byte[ChUtils.countTrue(isTranslated)];
        for(int i=0,j=0;i<n;i++) if (isTranslated[i]) bb[j++]=dna[i];
        return bb;
    }

    public static int actg2int(byte bb[], int from) {
        if (bb==null || bb.length<from+3) return -1;
        for(int i=0;i<3;i++) {
            final int c=bb[i];
            if (c!='A' && c!='C' && c!='T' && c!='G' && c!='U' && c!='a' && c!='c' && c!='t' && c!='g' && c!='u') return -1;
        }
        final int c0=bb[from]|32, c1=bb[from+1]|32, c2=bb[from+2]|32;
        return
            (c0=='a' ? 0 :   c0=='c' ? 1<<0 :   c0=='t'||c0=='u' ?  2<<0 :   3<<0)+
            (c1=='a' ? 0 :   c1=='c' ? 1<<2 :   c1=='t'||c1=='u' ?  2<<2 :   3<<2)+
            (c2=='a' ? 0 :   c2=='c' ? 1<<4 :   c2=='t'||c2=='u' ?  2<<4 :   3<<4);
    }

    public static byte[]  int2actg(int i, byte bb0[], boolean uracil) {
        final byte[] bb=bb0!=null ? bb0 : new byte[3];
        if (i<0 || i>=4*4*4) {
            bb[0]=bb[1]=bb[2]=(byte)'X';
            return bb;
        }
        final int i0=i&3, i1=(i>>2)&3, i2=(i>>4)&3;
        final char UT=uracil ?'U':'T';
        bb[0]=(byte) (  i0==0 ? 'A' : i0==1 ? 'C' : i0==2 ? UT : 'G' );
        bb[1]=(byte) (  i1==0 ? 'A' : i1==1 ? 'C' : i1==2 ? UT : 'G' );
        bb[2]=(byte) (  i2==0 ? 'A' : i2==1 ? 'C' : i2==2 ? UT : 'G' );
        return bb;
    }
}
