package charite.christo;
/**
@author Christoph Gille
*/
public interface HasNativeExec {
    BasicExecutable getNativeExec(boolean createNew);
}
