package charite.christo;
import java.util.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**
   @author Christoph Gille

Test: TestStrings2Tree.java
*/
public class Strings2Tree implements ChRunnable {
    public final static int SKIP_LINES_WITH_HASH=1<<0, LEAFS_CONTAIN_ENTIRE_PATH=1<<2, DIRS_FIRST=1<<3;

    private final Object _list;
    private final int _opt;
    private final boolean[] _delim;
    public Strings2Tree(int options, Object v, boolean delim[]) {
        _list=v;
        _opt=options;
        _delim=delim;

    }

    private int _mc, _nLines;
    public UniqueList getTree() {
        final Object list=_list;
        final int mc=modic(list), L=sze(list);
        if (_root==null || _mc!=mc || _nLines!=L) {
            _mc=mc;
            _nLines=L;
            if (_root==null) _root=new UniqueList(Object.class).t("Root");
            _vAllNodes.clear();
            for(int iLine=0; iLine<L; iLine++) {
                final String line=toStrgTrim(get(iLine,list));
                if (sze(line)==0 || 0!=(_opt&SKIP_LINES_WITH_HASH) && chrAt(0,line)=='#') continue;
                final String fields[]=split(line);
                final String leaf=0!=(_opt&LEAFS_CONTAIN_ENTIRE_PATH)?line:fields[fields.length-1];
                _vAllNodes.add(leaf);
                insert(fields).add(leaf);
            }
            removeUnusedNodes(_root);
        }
        return _root;
    }

    private final static Map<String,String[]> mapSplit=new HashMap();
    private String[] split(String s) {
        String ss[]=mapSplit.get(s);
        if (ss==null) {
            ss=splitTokns(SPLIT_ALLOW_EMPTY_TOKENS, s,0,MAX_INT, _delim);
            mapSplit.put(s,ss);
        }
        return ss;
    }

    private UniqueList<Object> _root;
    private final Collection _vAllNodes=new HashSet();
    private UniqueList insert(String[] fields) {
        UniqueList<Object> v=_root;
        for(int i=0;i<fields.length-1; i++) {
            final Map<String,UniqueList> m=map(v);
            final String f=fields[i];
            UniqueList<Object> child=m.get(f);
            if (child==null) {
                m.put(f, child=new UniqueList(Object.class).t(f));
                if (0!=(_opt&DIRS_FIRST)) v.add(0,child); else v.add(child);
            }
            _vAllNodes.add(child);
            v=child;
        }
        return v;
    }

    private void removeUnusedNodes(UniqueList v) {
        for(Object o : v.asArray()) {
            final UniqueList v2=o instanceof UniqueList ? (UniqueList)o : null;
            if (v2!=null) removeUnusedNodes( v2);
            if (!_vAllNodes.contains(o)) {
                v.remove(o);
                if (v2!=null) map(v).remove(v2.getText());
            }
        }
    }

    private final static Object KEY=new Object();
    private final static Map<String,UniqueList> map(UniqueList v) {
        Map m=gcp(KEY,v,Map.class);
        if (m==null) pcp(KEY, m=new HashMap(), v);
        return m;

    }

    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id==PROVIDE_JTREE_CHILDS) return getTree();
        return null;
    }

}
