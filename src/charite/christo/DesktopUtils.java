package charite.christo;
import java.io.*;
import java.awt.*;
import java.awt.dnd.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**   @author Christoph Gille
 */
public final class DesktopUtils implements DropTargetListener {

    private static DesktopUtils _inst;
    private static DesktopUtils instance() { if (_inst==null) _inst=new DesktopUtils(); return _inst;}
    public final static String[] OPTS={"-viewTextFiles", "-pubmed"};
    public static boolean main(String...argv) {
        final int iView=maxi(idxOfOption("-viewTextFiles",argv), idxOfOption("-viewTextFile",argv));
        final int iPubmed=idxOfOption("-pubmed",argv);
        if (iView>=0) {
            for(int i=iView+1;i<argv.length && chrAt(0,argv[i])!='-'; i++) {
                final File f=file(argv[i]);
                if (sze(f)>0) {
                    new ChTextView(f).tools().showInFrame(ChFrame.STAGGER|ChFrame.SCROLLPANE, argv[i]);
                }
            }
            ChFrame.setOptionsForAll(ChFrame.SYSTEM_EXIT_WHEN_ALL_CLOSED);
            final JComponent
                bHelp=ChButton.doView(_CC+"/textViewer.html").t("Help"),
                pan=pnl(CNSEW,"Drag text file here ","<h2>Text viewer</h2>",C(0xFF00),bHelp,null);
            for(JComponent c : childsR(pan, JComponent.class))  new DropTarget(c,instance());
            new ChFrame("Text viewer").ad(pan).shw(ChFrame.PACK|ChFrame.SHUT_DOWN_ON_CLOSE|ChFrame.ALWAYS_ON_TOP);
            return true;
        }
        if (iPubmed>=0) {
            Insecure.setFileModificationControl(false);
            final String s=delPfx("-pubmed=",argv[iPubmed]);
            openPmidManager(nxt(DIGT,s)>=0?s:null, ChFrame.SHUT_DOWN_ON_CLOSE);
            return true;
        }
        return false;
    }
    private static ChFrame _fPmid;
    public static void openPmidManager(String txt, long options) {
        if (_fPmid==null) {
            final String msg="Type a list of Pubmed or Uniprot references. \n"+
                "Move the mouse over these references.\n"+
                "Abstracts appear below.\n"+
                "Also try context-menu\n",
                example="\nPMID:20004206 PMID:16469097 PMID:11301311 PMID:16322575\n UNIPROT:Q9H936\n";
            final ChTextArea taPMID=new ChTextArea(msg+(sze(txt)>0? rplcToStrg(" PMID","\nPMID",txt):example)+"\n"), taAbstract=new ChTextArea("");
            rtt(taAbstract); rtt(taPMID);
            final ChTextComponents tPMID=taPMID.tools(), tAbstract=taAbstract.tools();
            taPMID.tools().setReplaceWhenTextChanges(new ChTextReplacement("PMID: ","PMID"));
            if (sze(txt)==0) tPMID.saveInFile("DesktopUtils_PMID_LIST");
            tAbstract.enableUndo(true).underlineRefs(0);
            ChTextComponents.setTextAreaForAbstract(tAbstract);
            tPMID.enableUndo(true).underlineRefs(ULREFS_GO|ULREFS_PMID_DOWNLOAD);
            pcp(KEY_IF_EMPTY,msg,taPMID);
            pcp(KEY_IF_EMPTY,"The Abstract of the PMID\nunder the mouse\nwill be shown here",taAbstract);
            final Dimension d=dim(1,150);
            final Object
                pNorth=pnl(HB,"Manage Pubmed abstracts","#",smallHelpBut(ChPubmed.class)),
                pMain=pnl(CNSEW,new JSplitPane(JSplitPane.VERTICAL_SPLIT,scrllpn(0,taPMID,d),scrllpn(0,taAbstract,null)),pNorth);
            _fPmid=new ChFrame("Pubmed").ad(pMain).size(444,444);
        }
        _fPmid.shw(options);
    }

    private java.awt.datatransfer.Transferable _transferable;
    private File[] _files;
    public void dropActionChanged(java.awt.dnd.DropTargetDragEvent event) {  }
    public void dragEnter(java.awt.dnd.DropTargetDragEvent ev) { ev.acceptDrag(java.awt.dnd.DnDConstants.ACTION_COPY);}
    public void dragOver(java.awt.dnd.DropTargetDragEvent event) {
        final java.awt.datatransfer.Transferable t=event.getTransferable();
        if (t!=_transferable) {
            _transferable=t;
            _files=ChTransferable.getFiles(t,0);
        }
    }
    public void drop(java.awt.dnd.DropTargetDropEvent event) {
        event.acceptDrop(java.awt.dnd.DnDConstants.ACTION_COPY);
        final java.awt.datatransfer.Transferable t=event.getTransferable();
        final File ff[]=sze(_files)>0 ? _files : ChTransferable.getFiles(t,0);
        for(int i=sze(ff); --i>=0;) {
            final File f=ff[i];
            if (sze(f)>0) new ChTextView(f).tools().showInFrame(ChFrame.STAGGER|ChFrame.PACK_SMALLER_SCREEN|ChFrame.SCROLLPANE,f.getName());
        }
    }
    public void dragExit(java.awt.dnd.DropTargetEvent ev) { }

}
