package charite.christo;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;
import javax.swing.*;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   <h3>External programs</h3>

   External programs are installed in the directory ~/.StrapAlign/bin/.
   <br><br>

   Since your computer may not be prepared for installing scientific programs from Source code,
   ClustalW is also included ready to run on Windows, Macintosh and Linux.

   For all other external programs, compilers and the utility make are required.

   <h3>Compilers</h3>

   Two program packages are required on your PC to  install software
   from source code:

   <br><br>XXX<br><br>

   <h3>Testing, whether compilers are already installed</h3>
   Usually, the C-compiler is started with the command "cc" and the C++ compiler with the command "c++".
   Fortran compilers are started with "gfortran" or "f77" or "f95".
   If  C++ is installed then typing into the command shell
   "c++" produces the output
   <pre>c++: no input files</pre>
   But the message
   <pre>c++: command not found</pre>
   indicates that either C++ is not installed yet or that the PATH-variable is not set correctly.

   OS_DEBIAN
   To install the compilers on a Debian system run the following in a terminal:
   <pre class="terminal">
   sudo apt-get install g++ gfortran make
   </pre>
   OS_SUSE
   To install the compilers for SUSE Linux run the following in a terminal:
   <pre class="terminal">
   sudo /sbin/yast2 --install gcc-c++ gcc-fortran make
   </pre>
   OS_MAC
   For Apple, C++-Compiler is included in Xcode.
   <ol>
   <li>Insert Mac OS X Installation DVD. Navigate to the Optional Installs: Xcode Tools folder.</li>
   <li>Double-click XcodeTools.mpkg. Follow the prompts, but at Installation Type, click the Customize button.</li>
   <li>To save disk space, deactivate all but Unix Development Support.</li>
   </ol>
   Unfortunately, Fortran which is required for TM-align, is not included in Xcode and must be obtained from other sources.

   OS_WINDOWS
   The compilers are provided by Cygwin. <i>BUTTON:charite.christo.Microsoft#newCygwinButton("")!</i>
   OS_OTHER
   To install the compilers on your system open the software manager and activate the packages
   <ul>
   <li>C++-compiler</li>
   <li>Fortran compiler</li>
   <li>make</li>
   </ul>

   @author Christoph Gille
   The abstract class <i>BasicExecutable</i> is used to make a Java wrapper for a
   program that is written in  C or C++ or any other compatible computer language other than Java.
   <br>
   Conventions:
   <ul>
   <li>There is only one executable.</li>
   <li>The filename of the executable is getName() +  ".exe".</li>
   <li>
   </ul>

*/
public class BasicExecutable implements CanBeStopped,Disposable,ChRunnable,HasControlPanel  {
    public final static String
        RPLC_WIN_DOT_EXE="AE$$RWDE",
        PFX_DPKG="\nPFX_DPKG ",
        PFX_FIX_INCLUDE="\nPFX_STRGH ",
        PFX_ADD_INCLUDE_LIMITS="\nPFX_INCLUDE_LIMITS",
        DEFAULT_BINARY="*",
        REPLACEME_INLCUDE_STRING[][]={{"#include <string>","#include <string.h>"}};

    /* >>> Instance >>> */

    private String _name,_instMessage, _binURLs[], _srcURLs[], _srcInstScript;
    private boolean _toldInstFailed, _version;
    private File _dirBinaries,_fileExecutable,_dirTmp,_fileIn,_fileOut;
    private static int _countInstances;
    private static Component _myComponent;
    private ChExec _execMake;
    private Collection _vSrcRplc, _vAddInclude;

    public BasicExecutable setName(String n) {
        _name=n;
        if (nxt(SLASH,n)>=0) {
            putln(RED_ERROR, "BasicExecutable.setName(\""+n+"\"):  Wrong character in name");
            System.exit(1);
        }
        return this;
    }
    public BasicExecutable(String n) { setName(n); }
    public BasicExecutable() {}
    public final String getName() {return _name;}
    public final void initExecutable() {
        if (!Insecure.EXEC_ALLOWED) return;
        final ChExec ex=_myExec;
        if(ex!=null) ex.dispose();
        if (sze(fileExecutable())==0 && _instMessage!=null) {
            final String msg=
                "The required software can be installed by running the following shell command as root: \n\n  "+
                _instMessage+"  \n";
            ChMsg.msgDialog(0L, new ChTextView(msg));
        }
        installPackage();
        delFile(fileOut());
        delFile(fileIn());
        CanBeStopped.vALIGNMENTS.add(wref(this));
    }
    @Override public String toString() {
        return dItem(this) +
            (sze(getPackageURLs(false))>0 ? " The program file will be installed automatically." : "" )+
            (_srcInstScript!=null ?  " It will be installed from source. " : "");
    }
    public void dispose() {
        if (_myExec!=null) try { _myExec.dispose();} catch(Throwable e){}
        _myExec=null;
    }
/* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Files and Directories >>> */

    /** temporary directory for transient data. Will be deleted after the program session. */
    public final File dirTemp() {
        if (_dirTmp==null) {
            deleteTree(_dirTmp);
            mkdrs(_dirTmp=file(ChUtils.dirTmp()+"/"+getName()+"/"+ _countInstances++ ));
            delFileOnExit(_dirTmp);
        }
        return _dirTmp;
    }
    /** You can use this as your input file. */
    public final File fileIn() { if (_fileIn==null) _fileIn=file(dirTemp(),"input"); return _fileIn;}
    /** You can use this as your output file. */
    public final File fileOut() {
        if (_fileOut==null) _fileOut=file(dirTemp(),"output");
        return _fileOut;
    }
    /** The directory of the installed program. */
    public final File dirBinaries() {
        final String n=getName();
        if (n!=null) {
            if (_dirBinaries==null) mkdrs(_dirBinaries=file(dirBin(),n));
            if (!_dirBinaries.isDirectory()) delFile(_dirBinaries);
        }
        return _dirBinaries;
    }
    /** The File of the executable.  is name() plus  dot "exe"  */
    public File fileExecutable() {
        if (_fileExecutable==null) _fileExecutable=file(dirBinaries(),getName()+".exe");
        return _fileExecutable;
    }
    public void setFileExecutable(File f) { _fileExecutable=f;}
    /* <<< Files and Directories <<< */
    /* ---------------------------------------- */
    /* >>> Installation >>> */
    /**
       Set URL of the zip-archive for the program binary.
       An array of alternative URLs can be provided.
       Each String may contain a list of required zip files.
       Each URL may be followed by a file path denoting a file inside the zip archive that is the exe file.
    */
    public void setBinaryPackageURLs(String...uu) { _binURLs= sze(uu)==0  ? null : uu; }
    // See SubcellularLocationSherLoc
    /**
       Set URL of the zip-archive for the program source.
       An array of alternative URLs can be provided.
       Each String may contain a list of required zip files.
    */
    public final void setSourcePackageURLs(String...urls) { _srcURLs=urls; }
    private final String[] getPackageURLs(boolean isSource) {
        String[] uu=null;
        if (isSource) {
            uu=_srcURLs;
            if (sze(uu)==0 && _srcInstScript!=null) uu=_srcURLs=new String[]{URL_STRAP_SRC+getName()+".zip.gz"};
        } else {
            uu=_binURLs;
            for(int i=sze(uu); --i>=0;) {
                if (uu[i]==DEFAULT_BINARY) uu[i]=urlOfBinariesForThisOS()+getName()+".zip.gz";
            }
        }
        return uu;
    }

    /** @return a shell script, usually a compile command. Type the string "C_COMPILER" instead of "gcc" and "C_PLUS_PLUS_COMPILER" instead of "g++". */
    public void setSourceInstallationScript(String shellScript) {
        _srcInstScript=shellScript==null ? null : rplcToStrg(RPLC_WIN_DOT_EXE, isWin() ? ".exe" : "", shellScript)+"\n";
    }

    private boolean extract(String urlsPipeExe[], boolean isSource) {
        if (!Insecure.EXEC_ALLOWED) return false;
        boolean success=false;
        for(int iTry=0; iTry<sze(urlsPipeExe); iTry++) {
            final String urls[]=splitTokns(urlsPipeExe[iTry]);
            for(int iU=0; iU<urls.length; iU++) {
                final File fZip=zipFile(urls[iU],isSource);
                if (fZip==null) continue;
                mkParentDrs(fZip);
                if (!looks(LIKE_EXTURL,urls[iU])) {
                    try {
                        cpy(CLASSLOADER.getResourceAsStream(urls[iU]),fZip);
                    } catch(Exception e){putln("caught in BasicExecutable#extract ",e);}
                } else {
                    final URL url=url(delLstCmpnt(urls[iU],'|'));

                    if (ChMsg.askPermissionInstall(url,null)) InteractiveDownload.downloadFiles(new URL[]{url},new File[]{fZip});

                    if (sze(fZip)==0) { error("Did not download <br>"+fZip); continue; }
                }
                final boolean zipOK=new ChTarPROXY().extractAll(ChZip.REPORT_ERROR|ChZip.SUGGEST_DEL, fZip, dirBinaries());
                final String exeInZip=lstCmpnt(urls[iU],'|');
                if (sze(fileExecutable())==0 && sze(exeInZip)>0) {
                    final File fE=file(dirBinaries()+"/"+exeInZip);
                    if (sze(fE)==0) putln(ANSI_RED+"Error in BasicExecutable "+ANSI_RESET+" no such file ",fE);
                    else cpy(fE,fileExecutable());
                }
                if (lstDir(dirBinaries()).length>0) success=true;
                else if (sze(fZip)>0 && zipOK) error("Unable to extract "+fZip);
            }
            if (success) break;
        }
        return success;
    }
    private static File zipFile(String u, boolean isSource) {
        if (!Insecure.EXEC_ALLOWED) return null;
        final File dir=file(dirBin()+ (isSource ? "/downloadedSourcePackages" : "/downloadedBinaryPackages"));
        final String s=u.substring(u.lastIndexOf('/')+1);
        return file(dir,delSfx(".bz2",delSfx(".gz",s)));
    }
    public void installPackage() {
        if (!Insecure.EXEC_ALLOWED) {
            Insecure.tellNoPermission('X',"");
            return;
        }
        final String n=getName();
        if (n==null) {
            putln(RED_WARNING+"BasicExecutable calling installPackage() before setting name",shrtClasNam(this));
            return;
        }
        if (!isEDT()) inEDT(thrdM("installPackage",this));
        else if (sze(fileExecutable())==0) {
            if (!withGui()) putln("\n"+ANSI_FG_GREEN+"Going to install "+ANSI_RESET,n);
            if (!_version) _version=currVers(getName());
            extract(getPackageURLs(true), true);
            for(Object[] rplc_f : toArry(_vSrcRplc,Object[].class)) {
                final File f=file(dirBinaries()+"/"+rplc_f[1]);
                //putln(ANSI_BLUE+rplc_f[1]+"     ",f,ANSI_RESET);
                rplcInTextFile(' '|STRPLC_FILL_RIGHT, (String[][])rplc_f[0], f );
            }
            for(Object[] rplc_f : toArry(_vAddInclude,Object[].class)) {
                final File f=file(dirBinaries()+"/"+rplc_f[1]);
                //putln(ANSI_BLUE+rplc_f[1]+"     ",f,ANSI_RESET);
                final BA ba=readBytes(f);
                if (sze(ba)>0) wrte(f,ba.insert(0,rplc_f[0]));
            }
            if (sze(_srcInstScript)>0) make(_srcInstScript);
            if (sze(fileExecutable())==0) extract(getPackageURLs(false),false);
        }

        for(String f : lstDir(dirBinaries())) if (f.endsWith(".exe")) mkExecutabl(file(dirBinaries(),f));
    }
    /* <<< Installation <<< */
    /* ---------------------------------------- */
    /* >>> Execute >>> */
    /** Show a message telling that the program could not be started */
    public final void failedToLaunch() {
        if (_myExec==null || !_myExec.couldNotLaunch()||_toldInstFailed) return;
        final String urlHome=dHomePage(this);
        final Component msg=
            pnl(VBHB,
                 pnl("<h2>Failed to run </h2>",getName()),
                 " ",
                 ctrlPnl(_myExec),
                 pnl(HBL,"Please install ",fileExecutable()),
                 " ",
                 urlHome!=null ? pnl(HB,"Web:",urlHome) : null
                 );
        error(null,msg);
    }
    /** For internal use. If override please make a call super.run(...) */
    public Object run(String id,Object arg) {
        if (!Insecure.EXEC_ALLOWED) return null;
        if (id=="AFTER_COMPILATION") {
            final Container pSouth=(Container)arg;
            final boolean success=sze(fileExecutable())>0;
            pSouth.add(pnl( C(success ? 0x00b200:0xFF0000), success ? "compilation finished successfully" : "compilation failed"));
            if (!success) {
                if (sze(getPackageURLs(false))>0) pSouth.add(pnl(HBL,"As a workaround I am going to install the pre-compiled package"));
                pSouth.add(pnl(HBL,"For Installing compilers click ", ChButton.doView(msgInstallCompilers()).t("Installation from source code ...")));
            }
        }
        if (id=="COMPILATION") {
            final Object pSouth=get(0,arg);
            final ChExec exec=(ChExec)get(1,arg);
            exec.run();
            inEdtCR(this,"AFTER_COMPILATION",pSouth);
            final java.awt.Window d=parentWndw(_myComponent);
            if (d instanceof JDialog) d.dispose();
        }
        return null;
    }
    private void make(String commands) {
        if (!Insecure.EXEC_ALLOWED || commands==null) return;
        String cmd=rplcToStrg("C_PLUS_PLUS_COMPILER",  custSetting(Customize.CplusPlus_compiler), commands);
        cmd=rplcToStrg("C_COMPILER",custSetting(Customize.C_compiler),cmd);
        cmd=rplcToStrg("FORTRAN_COMPILER", custSetting(Customize.fortran_compiler),cmd);
        cmd=rplcToStrg("MNO_CYGWIN_FLAG",isWin() ? "-mno-cygwin" : "",cmd);
        if (isWin()) cmd=ChExec.WINDOWS_addBinToPath+cmd;
        final File fScript=file(dirBinaries(),"makeByStrap.sh");
        wrte(fScript,cmd);
        _execMake=new ChExec(withGui() ? ChExec.WITHOUT_ASKING|ChExec.STDOUT|ChExec.STDERR : ChExec.TO_STDOUT).dir(dirBinaries());
        _execMake.setCommandLine(((isWin() ? dirCygwin()+"\\bin\\sh.exe " : "sh ")+toCygwinPath(fScript)));
        if (withGui()) {
            final Component log=pnl(VBHB,
                                    pnl(HBL,"Installing ",getName()," from source code. This may take some minutes"),
                                    fScript,
                                    ctrlPnl(_execMake),
                                    BasicExecutable._myComponent=new JSeparator()
                                    );
            startThrd(thrdCR(this,"COMPILATION",new Object[]{log, _execMake}));
            InteractiveDownload.addTab(null, -1);
            if (0==ChMsg.option(log, new String[]{"Abort installation"})) _execMake.kill();
            final boolean success=sze(fileExecutable())>0;
            final int success1;
            if (!success && sze(_binURLs)>0) {
                success1=0;
                toContainr(0,pnl("<sub>Installation from source code failed.<br>Therefore going to install binary.</sub>"),log);
            } else success1=success ? 1 : -1;
            InteractiveDownload.addTab(log, success1);
        } else _execMake.run();
    }
    /* <<< Execution <<< */

    /* ---------------------------------------- */
    /* >>> Command line options >>> */
    private long _paraOpts;
    private Object _shared;
    private String _paras;
    private PrgParas _prgPara;
    public final PrgParas getPrgParas() {
        final BasicExecutable si= ((BasicExecutable)orO(getSharedInstance(), this));

        if (si._prgPara==null) {
            si._prgPara=new PrgParas();
            if (si._paras==null) si._prgPara.defineGUI(si._paraOpts, si._paras);
        }
        return si._prgPara;
    }
    public final void setSharedInstance(Object shared) { _shared=shared;}
    public final Object getSharedInstance() { return _shared; }
    public void defineParameters(long opt, String s) { _paraOpts=opt; _paras=s;}

    /* <<<  Command line options <<< */
    /* ---------------------------------------- */
    /* >>> Process >>> */
    private boolean _isStopped;
    private ChExec _myExec;
    public ChExec exec(long options) { final ChExec ex=exec(true);  return ex==null?null: ex.setOptions(options); }
    public ChExec exec(boolean create) {
        if (_myExec==null && Insecure.EXEC_ALLOWED && create) {
            _myExec=new ChExec(0);
        }
        return _myExec;
    }
    public boolean isStopped() { return _isStopped;}
    public void stop() {
        _isStopped=true;
        final ChExec ex=_myExec;
        if (ex!=null) ex.kill();
    }

    /* <<< Process <<< */
    /* ---------------------------------------- */
    /* >>> Static tools >>> */
    public static Class[] checkForUpdates(Class classes[]) {
        if (!Insecure.EXEC_ALLOWED) return NO_CLASS;
        final List<Class> v=new ArrayList();
        for(Class c : classes) {
            if (isAssignblFrm(BasicExecutable.class, c)) {
                final BasicExecutable ex= mkInstance(c, BasicExecutable.class);
                for(int src=0; src<2; src++) {
                    final String[] urlsPipeExe=ex!=null ?  ex.getPackageURLs(src!=0) : null;
                    for(int iTry=sze(urlsPipeExe); --iTry>=0;) {
                        final String urls[]=splitTokns(urlsPipeExe[iTry]);
                        for(int iU=0; iU<urls.length; iU++) {
                            final String url=delLstCmpnt(urls[iU],'|');
                            final File fZip=zipFile(url,src!=0);
                            if (sze(fZip)>0 &&  0==isUpToDate(fZip,url(url))) adUniq(c,v);
                        }
                    }
                }
            }
        }
        return toArryClr(v,Class.class);
    }
    public static String msgInstallCompilers() {
        if (!Insecure.EXEC_ALLOWED) return "";
        final String os=
            isSystProprty(IS_DEBIAN) ? "DEBIAN" :
            isSystProprty(IS_SUSE) ? "SUSE" :
            isMac() ? "MAC" :
            isWin() ? "WINDOWS" :
            "OTHER";
        final CharSequence txt=getHlp(BasicExecutable.class);
        final int
            osFrom=strstr(STRSTR_AFTER|STRSTR_w, "OS_"+os,txt),
            osTo=strstr(STRSTR_w_L|STRSTR_E, "OS_",txt, osFrom+1,MAX_INT),
            hlpEnd=strstr(STRSTR_w_L, "OS_",txt);
        return txt==null?null:rplcToStrg("XXX", txt.subSequence(osFrom,osTo), txt.subSequence(0, hlpEnd));
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Control Panel >>> */

    private Object _ctrl;
    public Object getControlPanel(boolean real) {
        final ChExec e=exec(false);
        if (e!=null) return !real ? CP_EXISTS : e.getControlPanel(true);
        final Object cached=gcp(KEY_CACHE_TEXT,this);
        if (!real) return cached!=null ? CP_EXISTS : null;
        if (_ctrl==null && cached!=null) _ctrl=ChButton.doView(cached).t("Text in cache");
        return _ctrl;
    }

    private final static String pfxFix=PFX_FIX_INCLUDE.substring(1), pfxDpkg=PFX_DPKG.substring(1), pfxLimits=PFX_ADD_INCLUDE_LIMITS.substring(1);
    public static BasicExecutable init(BasicExecutable e0, String names, String srcUrls, String instScript) {
        final BasicExecutable e=e0!=null ? e0 : new BasicExecutable();
        final String nn[]=splitLnes(names);
        e.setName(nn[0]);
        if (!Insecure.EXEC_ALLOWED) {
            Insecure.tellNoPermission('X',"");
            return null;
        }
        e.setSourcePackageURLs(splitLnes(srcUrls));
        e.setSourceInstallationScript(instScript);
        File f=null, f0=null;
        for(int i=1; i<nn.length;i++) {
            final String s=nn[i];

            if (isSystProprty(IS_USE_INSTALLED_SOFTWARE)) {
                if (chrAt(0,s)=='/' && sze(f0=file(s))>9) f=f0;
                final String packages=delPfx(pfxDpkg,s);
                if (s!=packages) e._instMessage=installCmdForPckgs(packages);
            }
            final String files=delPfx(pfxFix,s);
            if (s!=files) {
                for(String file : splitTokns(files)) {
                    e._vSrcRplc=adNotNullNew(new Object[]{REPLACEME_INLCUDE_STRING,file}, e._vSrcRplc);
                }
            }
            final String fixes=delPfx(pfxLimits,s);
            if (s!=fixes) {
                for(String file : splitTokns(fixes)) {
                    e._vAddInclude=adNotNullNew(new Object[]{"#include \"limits.h\"\n#include <limits>\n",file}, e._vAddInclude);
                }
            }
        }
        if (f!=null) e.setFileExecutable(f);
        else e.initExecutable();
        return e;
    }

}

