package charite.christo;
public interface TooltipProvider {
   String provideTip(Object evOrComp);
}
