package charite.christo;
import java.net.URL;
import javax.swing.*;
import java.io.File;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/*
AbstractProxy is used by classes that depend on jar files which are download when the class is used.
These classes cannot be loaded with the boot class loader because the boot classpath does not yet contain these jar files.
Instead they are loaded with a class loader which is part of STRAP.
*/
public abstract class AbstractProxy implements HasMap {
    public final static String PFX_INSTALLED="AP$$PI";
    public final static int TRY_BOOT_CLASSLOADER=1<<0;
    private int _options;
    private String _lastKey;
    private Object _po;
    public void setOptions(int o) { _options=(int)o;}
    private final static Map mapProxy=new WeakHashMap();
    private final static Map<String,Class> MAP=new HashMap();
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Jar-files >>> */
    public abstract String getRequiredJars();
    private final static int REQU200=0, REQU=1, REMOTE=2, REMOTE200=3, LOCAL200=4, LOCAL=5, LOCAL_ALT_INSTALLED=6, USR_SHARE_JAVA=7;
    private final static Map<Class,Object[]> _map=new HashMap();

    private Object[] mapped() {
        final Object[] mapped;
        {
            Object oo[]=_map.get(getClass());
            if (oo==null) _map.put(getClass(), oo=new Object[8]);
            mapped=oo;
        }

        String[] ss200=(String[])mapped[REQU200];
        if (ss200==null) {
            final String tt[]=splitTokns(getRequiredJars());
            final int N=tt.length;
            final String[] ss=new String[N],usrShareJava=new String[N];
            final File[] ff200=new File[N], ff=new File[N],  ffAlt=new File[N];
            ss200=new String[N];

            for(int i=N; --i>=0;) {
                for(String s : splitTokns(tt[i], chrClas1('|'))) {
                    final String alt=delPfx(PFX_INSTALLED,s);
                    if (alt!=s) {
                        ffAlt[i]=installdFile(alt+".jar");
                        usrShareJava[i]=alt;
                    } else ss200[i]=endWith(".pack.gz",s) ? s : s+".pack.gz";
                }
            }
            mapped[REQU200]=ss200=rmNullS(ss200);
            for(int i=N; --i>=0;) {
                final String s=delSfx(".pack.gz",ss200[i]);
                ss[i]=s;
                ff[i]=file(dirJars()+"/"+delToLstChr(s,'/'));
                ff200[i]=file(dirJars()+"/"+delToLstChr(ss200[i],'/'));
            }
            final URL u=url(URL_STRAP_JARS);
            mapped[LOCAL]=ff;
            mapped[LOCAL_ALT_INSTALLED]=ffAlt;
            mapped[USR_SHARE_JAVA]=usrShareJava;
            mapped[LOCAL200]=ff200;
            mapped[REQU]=ss;
            mapped[REMOTE]=   Web.urls(u,ss);
            mapped[REMOTE200]=Web.urls(u,ss200);

        }
        return mapped;
    }
    public String[] usrShareJava() { return (String[])mapped()[USR_SHARE_JAVA]; }
    public URL[]      remoteURLs() { return (URL[])   mapped()[REMOTE]; }
    private URL[]  remoteURLs200() { return (URL[])   mapped()[REMOTE200]; }

    /* <<< Jar File <<< */
    /* ---------------------------------------- */
    /* >>> Native DLLs  >>> */
    public String jarsContainingNativeLibraries() { return null;}
    /** Space separated list of obsolete jar-files, which need to be deleted. Regular expressions are allowed. */
    private static void extractDlls(File dir, String files, String fileExt[]) {
        if (dir==null || files==null) return;
        for(String t : splitTokns(files)) {
            final java.util.regex.Pattern pattern=java.util.regex.Pattern.compile(t);
            for(String file : lstDir(dir)) {
                if (pattern.matcher(file).matches()) {
                    final File f=new File(dir,file);
                    if (fileExt==NO_STRING) {
                        delFile(f);
                        if (f.length()>0) {
                            final String fn=f.getAbsolutePath();
                            final String del=getPrpty(AbstractProxy.class,"DEL_FILES", "");
                            if (del==null || del.indexOf(" "+fn)<0) setPrpty(AbstractProxy.class,"DEL_FILES",del+" "+fn);
                        }
                    } else  {
                        try {
                            ChZip.extractAllFilesWithEnding( new java.util.zip.ZipFile(f),null, fileExt,dir);
                        } catch(Throwable e){ stckTrc(e); }
                    }
                }
            }
        }
    }
    {
        final String del=getPrpty(AbstractProxy.class,"DEL_FILES", "");
        for(String t : splitTokns(del)) delFile(new File(t));
        setPrpty(AbstractProxy.class,"DEL_FILES","");
    }
    /* <<< Native DLLs <<< */
    /* ---------------------------------------- */
    /* >>> proxyObject >>> */
    private boolean _successDownload, _needAdapter, _denyAdapters;
    private static File _fAdapters;
    public Object proxyObject() {
        if (_fAdapters==null) _fAdapters=file(dirJars()+"/strapAdapterClasses1.jar");
        if (_po==null) {
            if (_needAdapter && sze(_fAdapters)==0) return null;
            if (!isEDT()) {
                inEDT(thrdM("proxyObject", this));
                return _po;
            } else {
                boolean go=true;
                final String cn=delSfx("PROXY",nam(getClass())), rsc=cn.replace('.','/')+".class";
                final boolean needAdapter=null==CLASSLOADER.getResource(rsc);
                if (needAdapter) {
                    putln(RED_WARNING+" not found: ", rsc);
                    final URL urlAdapters=url(URL_STRAP+"debian/"+nam(_fAdapters));
                    if (sze(_fAdapters)==0 && !_denyAdapters) {
                        _denyAdapters=_denyAdapters || !ChMsg.askPermissionInstall(urlAdapters,null);
                        if (!_denyAdapters) InteractiveDownload.downloadFilesAndUnzip(new URL[]{urlAdapters}, dirJars());
                    }
                     _needAdapter=true;
                     if (sze(_fAdapters)==0) return null;
                }
                final boolean loadDirectly= !Insecure.CLASSLOADING_ALLOWED || prgOptT("-noClassloading");
                if (go && _po==null && (loadDirectly || 0!=(_options&TRY_BOOT_CLASSLOADER))) {
                    Class c=null;
                    try {
                        c=Class.forName(cn);
                    } catch(Exception e) {
                        putln(RED_CAUGHT_IN+" AbstractProxy Class.forName ",cn);
                        putln(_error=e);
                    }
                    try {
                        if (c!=null) _po=c.newInstance();
                    } catch(Exception e) {
                        putln(RED_CAUGHT_IN+" AbstractProxy newInstance() ",c);
                        putln(_error=e);
                    }
                    if (loadDirectly) go=false;
                }
                if (go && _po==null) {
                    final Object mm[]=mapped();
                    final File
                        dirJars=dirJars(),
                        ff200[]=(File[]) mm[LOCAL200],
                        ff[]=   (File[]) mm[LOCAL],
                        ffAlt[]=(File[]) mm[LOCAL_ALT_INSTALLED];
                    final String key=!Insecure.CLASSLOADING_ALLOWED ? "" : ChClassLoader1.computeKey(ff,(File)null);
                    if (key.equals(_lastKey)) return null;
                    _lastKey=key;
                    final boolean p200=ChZip.supportForPack200();
                    boolean missing=false;
                    for(int i=ff200.length; !missing && --i>=0;) {
                        if (sze(ff200[i])==0 && sze(ff[i])==0 && sze(ffAlt[i])==0) missing=true;
                    }

                    if (missing && ChMsg.askPermissionInstall(url(URL_STRAP_JARS), remoteURLs())) {
                        if (!InteractiveDownload.downloadFilesAndUnzip(p200 ? remoteURLs200() : remoteURLs(), dirJars)) {
                            return null;
                        }
                    }

                    if (p200) {
                        for(File f : ff) {
                            if (sze(f)==0) {
                                ChZip.unpack200(dirJars,ChZip.PACK_IF_NEWER);
                                break;
                            }
                        }
                    }
                    final String obsolete="jortho.jar  neobio.jar";
                    extractDlls(dirJars,obsolete,NO_STRING);
                    { /* Native */
                        final String s=jarsContainingNativeLibraries();
                        if (s!=null) {
                            final String ln=System.mapLibraryName("libname"),  SUFFIX_DLL[]={ln.substring(ln.lastIndexOf('.'))};
                            extractDlls(dirJars,s.substring(s.lastIndexOf('/')+1),SUFFIX_DLL);
                        }
                    }
                    boolean success=true;
                    for(int i=ff.length; --i>=0;) {
                        if (sze(ff[i])==0 && sze(ffAlt[i])==0) {
                            success=false;  putln("Missing: "+ff[i]+"  "+ffAlt[i]);
                        }
                    }
                    _successDownload=success;
                    try {
                        Class c=MAP.get(cn);
                        File ffC[]=ff;
                        if (isSystProprty(IS_USE_INSTALLED_SOFTWARE)) {
                            ffC=new File[ff.length];
                            for(int i=ff.length; --i>=0;) ffC[i]= (sze(ffAlt[i])>0?ffAlt:ff)[i];
                        }
                        if (_needAdapter && sze(_fAdapters)>0) ffC=adToArray(_fAdapters, ff, 0, File.class);

                        //debugExit("xxxxxxxxxxx "+_needAdapter+"  "+_fAdapters+" xxxxxxxxxxxxxx ", ffC);

                        if (c==null) c=findClByN(cn, ffC);
                        MAP.put(cn,c);
                        try {
                            if (c!=null) _po=c.newInstance();
                        } catch(Exception er) { stckTrc(_error=er);}
                    } catch(Exception e){ stckTrc(_error=e); }
                    if (_po==null) {
                        putln("Error in AbstractProxy.proxyObject ",getClass());
                        if (myComputer()) shwTxtInW(ChClassLoader1.log());
                    }
                }
                if (_po!=null) mapProxy.put(_po, wref(this));
            }
        }
        return _po;
    }
    private Throwable _error;
    private Class findClByN(String cn, File[] ff) {
        adUniq(cn, ChClassLoader1.getInstance(false, ff,(File)null).vNO_PARENT_DELEGATION);
        final Class c=Insecure.findClassByName(cn,ff);
        if (c==null) {
            if (_successDownload) {
                final int n=sze(ff);
                final BA sb=new BA(3333)
                    .a("Error loading class ").aln(cn).a('\n')
                    .a(n==1 ? "T":"One of t").a("he following file").a('s', n==1 ? 1:0)
                    .a(" from ").aln(URL_STRAP_JARS)
                    .a("may be corrupt and may need to be deleted: "),
                    sbDetails=new BA(3333).a('\n',2).a(ChClassLoader1.log());
                for(int i=0; i<n; i++) {
                    sb.a(ff[i]).a(' ').a(sze(ff[i])).aln(" bytes");
                    final List vEntries=new ArrayList(9999);
                    if (!ChZip.checkIntegrityOfZipFile(ff[i], vEntries)) {

                        if (sze(ff[i])<10*1000 && looks(LIKE_HTML, readBytes(ff[i]))) {
                            sb.aln("Possibly, the web proxy settings are wrong!");
                            sb.aln("Going to delete ").aln(ff[i]);
                            delFile(ff[i]);
                            delFileOnExit(ff[i]);
                        } else {
                        sb.aln("checkIntegrityOfZipFile returns false\n");
                        sbDetails.a("\nfile=").aln(ff[i]).join(vEntries);
                        }
                    }
                }
                sb.a(sbDetails);
                if (_error!=null) sb.a('\n',2).a(stckTrcAsStrg(_error));
                ChStdout.uploadErrorReport(toStrg(sb.a('\n',2)),true, null);
                ChMsg.msgDialog(0L, "Close Strap and remove all files in the directory "+dirJars()+".<br>Restart Strap.<br>If the problem still occurs, please contact the author.");
                viewFile(drctry(dirJars()), 0);
            }
            return null;
        }
        return c;
    }
    /* <<< proxyObject <<< */
    /* ---------------------------------------- */
    /* >>> Various Methods >>> */
    public Object proxyObjectRun(String id, Object para) {
        final ChRunnable r=deref(proxyObject(),ChRunnable.class);
        return r==null?null:r.run(id,para);
    }
    public float getScore() {
        final HasScore s=deref(proxyObject(),HasScore.class);
        return s==null?Float.NaN:s.getScore();
    }

    public Object getControlPanel(boolean real) {
        final HasControlPanel c=deref(proxyObject(),HasControlPanel.class);
        return c==null?null:c.getControlPanel(real);
    }
    public Object getSharedControlPanel() {
        final HasSharedControlPanel c=deref(proxyObject(),HasSharedControlPanel.class);
        return c==null?null:c.getSharedControlPanel();
    }
    public void setSharedInstance(Object shared) {
        final NeedsSharedInstance c=deref(proxyObject(),NeedsSharedInstance.class);
        if (c!=null) setSharedInstance(shared);
    }
    public Object getSharedInstance() {
        final NeedsSharedInstance c=deref(proxyObject(),NeedsSharedInstance.class);
        return c==null?null:c.getSharedInstance();
    }

    public PrgParas getPrgParas() {
        final HasPrgParas p=deref(proxyObject(),HasPrgParas.class);
        return p==null?null: p.getPrgParas();
    }
    public void interpret(long options, String c) {
        final CommandInterpreter i=deref(proxyObject(),CommandInterpreter.class);
        if (i!=null) i.interpret(options, c);
    }
    private int _disposed;
    public void dispose() {
        if (0==_disposed++) {
            final Disposable d=deref(proxyObject(), Disposable.class);
            if (d!=null) d.dispose();
        }
    }
    /* <<<  Various Methods <<< */
    /* ---------------------------------------- */
    /* >>> Static  Utils >>> */
    public final static short COMMONS_COLLECTIONS=1, JCOMMON=3, LOG4J=4, ACTIVATION=7, JOGL=10, GLUGEN=11, BIOJAVA_3D=12;
    /**
       In the method getRequiredJars() frequently used jar-libraries should be referred to by a constant.
       For example jarFile(COMMONS_COLLECTIONS) returns "commons-collections-3.2.jar "
    */
    public static String jarFile(short id) {
        if (id==JOGL || id==GLUGEN) {
            final boolean isLinux= isSystProprty(IS_LINUX), isMac= isSystProprty(IS_MAC), isWin= isSystProprty(IS_WINDOWS);
            final String
                OS_ARCH=systProprty(SYSP_OS_ARCH),
                OS_NAME=systProprty(SYSP_OS_NAME),
                osSpecificFile=
                isWin && (OS_ARCH=="amd64"||OS_ARCH=="x86_64")  ? "windows-amd64" :
                isWin  ? "windows-i586" :
                OS_NAME=="sunos" && OS_ARCH=="sparc" ? "solaris-sparc" :
                OS_NAME=="sunos" && OS_ARCH=="sparcv9" ? "solaris-sparcv9" :
                OS_NAME=="sunos" && OS_ARCH=="x86" ? "solaris-i586" :
                OS_NAME=="sunos" && (OS_ARCH=="amd64"||OS_ARCH=="x86_64") ? "solaris-amd64" :
                isLinux && (OS_ARCH=="amd64"||OS_ARCH=="x86_64") ? "linux-amd64" :
                isLinux && isSystProprty(IS_86)  ? "linux-i586" :
                isMac && OS_ARCH=="ppc" ? "macosx-ppc" :
                isMac ? "macosx-universal" : "";
            return
                id==JOGL ? "http://download.java.net/media/jogl/builds/archive/jsr-231-webstart-current/jogl-natives-"+osSpecificFile+".jar "+
                "http://download.java.net/media/jogl/builds/archive/jsr-231-webstart-current/jogl.jar " :
                id==GLUGEN ? "http://download.java.net/media/gluegen/webstart/gluegen-rt-natives-"+osSpecificFile+".jar "+
                "http://download.java.net/media/gluegen/webstart/gluegen-rt.jar " :
                null;
        }

        return

            id==BIOJAVA_3D ? "Structure_biojava.jar" :
            id==COMMONS_COLLECTIONS ? PFX_INSTALLED+"commons-collections3|commons-collections-3.2.jar " :
            id==LOG4J ?               PFX_INSTALLED+"log4j-1.2|log4j-1.2.8r.jar " :
            id==JCOMMON ?             PFX_INSTALLED+"jcommon|jcommon-0.9.3.jar " :
            id==ACTIVATION ?          PFX_INSTALLED+"activation|activation.jar " :
            null;
    }
    public static <T extends Object> T getProxy(Object o, Class<T> clas) {
        return o!=null ? (T)deref(mapProxy.get(o), clas) : null;
    }
    public static void disposeProxy(Object o) {
        final Disposable d=getProxy(o,Disposable.class);
        if (d!=null) d.dispose();
    }

    private Map _props;
    public Map map(boolean create) {return _props==null&&create?_props=new ChMap() : _props;};
    /* <<< Static Utils <<< */
    /* ---------------------------------------- */

}

