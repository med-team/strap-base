package charite.christo;
import java.util.*;
import java.awt.Color;
import static charite.christo.ChUtils.*;
public class AnsiEscape {
    /* ---------------------------------------- */
    /* >>> Attribut instance >>> */
    private Color _fg,_bg;
    private int _style;
    public void reset() {_fg=_bg=null; _style=0;}
    public Color fg() { return _fg;}
    public Color bg() { return _bg;}
    public int style() { return _style;}
    public String toString() { return " fg="+_fg+" bg="+_bg; }
    /* <<< Attribut instance <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    public final static int SHIFT_LEN=32, SHIFT_CODE=40;
    public final static long MASK_POS=0xFFFFffff, MASK_POS_INV=0xffFFffFF00000000L;
    public static long newEscape(int code, int pos, int len) {
        return pos+ (((long)len)<<SHIFT_LEN)+(((long)code)<<SHIFT_CODE);
    }
    /*
    public static String escapeToString(long e) {
        final int len= (int)((e>>SHIFT_LEN)&255);
        final int code= (int)((e>>SHIFT_CODE)&255);
        final int pos=(int)(e&MASK_POS);
        return "\u001B["+code+"m"+code+"@"+pos+"."+len+"\u001B[m";
    }
    */

    public static int escapeEndsAt(int pos, byte T[], int maxPos) {
        final int E=mini(maxPos, T==null ? 0: T.length);
        if (E<1+pos) return -1;
        final byte c=T[pos];
        if (!('0'<=c && c<='9' || c=='m' )) return -1;
        final int noDigt=nxt(-DIGT,T,pos,E);
        return noDigt>0 && (T[noDigt]=='m' || T[noDigt]==';' ) ? noDigt+1 : -1;
    }

    private static Color colors[];
    public static Color ansiColor(int code) {
        if (colors==null) {
            final Color cc[]=colors=new Color[48];
            cc[30]= cc[40]=C(0x0);
            cc[31]= cc[41]=C(0xff0000);
            cc[32]= cc[42]=C(0x00FF00);
            cc[33]= cc[43]=C(0xffff00);
            cc[34]= cc[44]=C(0x0000ff);
            cc[35]= cc[45]=C(0xff00ff);
            cc[36]= cc[46]=C(0x3344ff);
            cc[37]= cc[47]=C(0xffFFff);
        }
        return code>=0 && code<colors.length ? colors[code] : null;
    }
    public final static int BOLD=1<<1, FAINT=1<<2, STANDOUT=1<<3, UNDERLINE=1<<4, BLINK=1<<5, REVERSE=1<<7, INVISIBLE=1<<8,
         NO_STANDOUT=2<<23, NO_UNDERLINE=24, NO_BLINK=25, NO_REVERSE=26;

    public static void inferIntoAttribute(long escape, AnsiEscape attr) {
        final int
            code= (int)(escape>>SHIFT_CODE&255),
            no=
            code==NO_UNDERLINE ? UNDERLINE :
            code==NO_STANDOUT ? STANDOUT :
            code==NO_REVERSE ? REVERSE :
            0;

        if (code==0) { attr._bg=attr._fg=null; attr._style=0; }
        else if (no!=0) attr._style&=(~no);
        else if (code<30) attr._style|=(1<<code);

        final Color color=ansiColor(code);
        if (color!=null) {
            if (code<40)  attr._fg=color;
            else attr._bg=color;
        }
    }
    public static void extractColors(String s, List v) {
        if (sze(s)==0) return;
        int from=0, ooB1=-1, m;
        while((ooB1=s.indexOf("\u001B[",ooB1+1))>=0 &&  (m=s.indexOf('m',ooB1+2))>=0) {
            if (from<ooB1) v.add(s.substring(from,ooB1));
            final Color color=ansiColor(atoi(s,ooB1+2));
            if (color!=null) v.add(color);
            from=m+1;
        }
        if (from<s.length()) v.add(s.substring(from));
    }
}
