package charite.christo;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/*
  @author Christoph Gille
*/
public class ChCompilerJavacPROXY extends AbstractProxy {
    final static String RUN_COMPILE="CJC$$RC"; //OPTION_CLASSPATH="-classpath", 
    @Override public String getRequiredJars() {
        final int v=javaVsn();
        return toStrg(new BA(22).a("tools_").a(maxi(14,v)).a(".jar"));
    }

    public boolean compile(File[] classpath, File sourceFile) {
        final File classFile=file(new BA(99).a(delSfx(".java",sourceFile)).a(".class"));
        final long ageClassFile=classFile!=null ? classFile.lastModified() : 0L;
        final String[] para=derefArray(new Object[]{"-classpath", toStrg(new BA(0).a('"').join(classpath, systProprty(SYSP_PATH_SEP)).a('"')), toStrg(sourceFile)}, String.class);
        final ByteArrayOutputStream baos=new ByteArrayOutputStream();
        proxyObjectRun(RUN_COMPILE, new Object[]{para, new PrintWriter(baos)});
        final boolean success= classFile!=null && classFile.lastModified()>ageClassFile;
        log().a(ANSI_YELLOW+"Compiler parameters="+ANSI_RESET).join(para,"   ").a('\n').aRplc('\t',' ', baos);
        if (!success) log().aln(" "+ANSI_RED+"failed"+ANSI_RESET+"\n");
        else log().a(" "+GREEN_SUCCESS).a(classFile).a(classFile.length(),6).aln(" bytes\n");
        log().send();
        return success;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Log  >>> */
    private static BA _log;
    public static BA log() {
        if (_log==null) _log=new BA(9999);
        return _log;
    }
}

