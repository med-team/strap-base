package charite.christo;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import javax.swing.JSlider;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
/**
   convenient extension of JSlider
   @author Christoph Gille
*/
public final class ChSlider extends JSlider implements javax.swing.event.ChangeListener,MouseWheelListener, HasMC {
    private final String command;
    /* >>> Constructor  >>> */
    public ChSlider(String cmd,int mi,int ma,int ini ) {
        super(mi,ma,maxi(mi,ini));
        command=cmd;
        addChangeListener(this);
        addMouseWheelListener(this);
    }
    public ChSlider tt(CharSequence tt) {
        setTip(tt,this);
        return this;
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Event  >>> */
    public void stateChanged(javax.swing.event.ChangeEvent ev) {
        setPrpty(_clas,_id, getValue());
        handleActEvt(this,command,0);
        ChDelay.repaint(this,1111);
        modi++;
    }
    @Override public void processEvent(AWTEvent e) {
        try{ super.processEvent(e);} catch(Exception ex){}
    }
    public void mouseWheelMoved(MouseWheelEvent ev) {
        final int ma=getMaximum(),mi=getMinimum();
        int i=ev.getWheelRotation()*(ma-mi)/ (isCtrl(ev) ? 200:50)+getValue();
        if (i<mi)i=mi;
        if (i>ma)i=ma;
        setValue(i);
        modi++;
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Value >>> */
    private int modi;
    public int mc() { return modi;}
    public JSlider cln() {
        return new JSlider(getModel());
    }
    /* <<< Value <<< */
    /* ---------------------------------------- */
    /* >>> Label >>> */
    private String[] _labels;
    public ChSlider endLabels(String... labels) {
        _labels=labels;
        if (countNotNull(labels)>0) {
            final Hashtable ht=new Hashtable();
            ht.put(intObjct(0),new JLabel(" "));
            setLabelTable(ht);
            try { setPaintLabels(true); } catch(Exception e){}
        }
        return this;
    }
    /* <<< Label <<< */
    /* ---------------------------------------- */
    /* >>> paint >>> */
    private boolean lastIsEnabled=true;
    public void paintComponent(Graphics g) {
        if (lastIsEnabled!=isEnabled()) { revalidate(); lastIsEnabled=isEnabled();}
        if (!isOpaque()) setBackground(C(0xFFffFF)); /* Nimbus bug */
        try { super.paintComponent(g);} catch(Throwable e){ stckTrc();}

        Font f=null;
        Rectangle cb=null;
        final String[] labels=_labels;
        int xPrev=MAX_INT;
        for(int i=sze(labels); --i>=0;) {
            if (sze(labels[i])==0) continue;
            if (f==null) {
                g.setFont(f=getFnt(11,false,0));
                cb=chrBnds(f);
                g.setColor(C(0));
            }
            final int sw=strgWidth(f,labels[i]);
            final int x=mini(getWidth()-sw,maxi(0,getWidth()*i/(labels.length-1)-sw/2));
            final int y=getHeight()-cb.height+y(cb);
            if (x+sw<xPrev) g.drawString(labels[i],x,y);
            xPrev=x;
        }
    }
    /* <<< paint <<< */
    /* ---------------------------------------- */
    /* >>> layout >>> */
    @Override public Dimension getPreferredSize() {
        Dimension d=prefSze(this);
        try {
            if (d==null) d=super.getPreferredSize();
        } catch(Exception e){ d=dim(32,32);}
        return d;
    }
    /* <<< layout <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    private Class _clas;
    private String _id;
    public ChSlider save(Class clas,String id_str) {
        setValue(getPrpty(_clas=clas,_id="slider_"+id_str, getValue()));
        return this;
    }

 }
