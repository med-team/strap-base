package charite.christo;
import static charite.christo.ChUtils.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseEvent;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChConstants.*;
/**HELP
<h3>Text-Pane short-cuts</h3>

The multi-line text-pane is derived from the standard Java text-pane
and provides additional functionality (http://www.bioinformatics.org/strap/straputils/).

<b>Shortcuts with the Ctrl-key:</b>

The control key is located at the left on the keyboard is labeled
Control, Ctrl or Strg (Steuerung).

The text-pane receives the key-strokes only if it has the input focus. Click to set focus.

<ul>
<li>X  Cut Text and copy</li>
<li>C  Copy text.</li>
<li>V  Paste text.</li>
<li>F  Find text pattern. Visit  http://www.bioinformatics.org/straputils/ for a complete description of search and highlighting options.</li>
<li>L  Toggle line numbers. Or toggle truncate/fold lines</li>
<li>+  Enlarge font.</li>
<li>-  Smaller font.</li>
<li>*  Toggle text quality (see WIKI:Antialiasing ).</li>
<li>T  Toggle frame is always floating on top. Indicated in frame title.</li>
<li>P  If it is a HTML document a printable html document will be opened in the Web-Browser.</li>
<li>ESC   Disable links until the mouse exits and re-enters.</li>
</ul>
Function keys:
<ul>
<li>F1  Help</li>
<li>F6  Settings: Specify Web-Browser, File-Browser and Standard URLs</li>
<li>F7  Spell check</li>
<li>F10 Toggle WIKI:Line_wrap</li>
</ul>

For editable text fields  the following features may or may not be active:
<ul>

<li>ALT-slash or Ctrl+ENTER and sometimes also Tab  &nbsp;
WIKI:Word_completion.
File path completion  if the word at the cursor looks like a  file path.
Also Environment variables are completed.</li>

<li>Ctrl+Z WIKI:Undo </li>
<li>Ctrl+Y WIKI:Redo </li>
</ul>

<h3>Hyperlinks</h3>

In most text views
URLs like http://www.google.com or database references like
PUBMED:12166070 or PUBMED{fructose glucose}  or EC classes like "1.2.2.1" can be clicked to open the
selected item in the browser or document viewer.

To specify another web browser hold the Ctrl-key while clicking.

The list of database links can be customized: <i>CUSTOMIZE:Customize#webLinks!</i>,
<i>CUSTOMIZE:Customize#databases!</i>, <i>CUSTOMIZE:Customize#proteinDatabases!</i>.

<h3>Context Menus</h3>

The context menu is opened by right-clicking a word.
Pubmed links like PMID2840859 have a specific context menu.

<h3>Auto-word-completion</h3>

WIKI:Word_completion available for certain text views.
Typing the beginning of word and hitting the Tab-key or alt-/ completes the word.
In case of ambiguity, the desired word may come up by pressing the key several times.

<i>INCLUDE_DOC:ChCodeViewer</i>

   @author Christoph Gille
*/
public class ChJTextPane extends JTextPane {
    final static Object KEY_TEXT_CSS_JS=new Object();
    public final static String KEYBINDINGS="F1 Help   Ctrl-key actions:   C Copy   V Paste   F Find   P Print  +/- Zoom   * Font quality   R shell script";

    public ChJTextPane(CharSequence txt) {
        rtt(this);
        setFont(getFnt(14,true,0));
        setEditable(false);
        setBG(0xFFffFF, this);
        t(txt);
        pcp(KOPT_TRACKS_VIEWPORT_WIDTH,"",this);
    }
    private ChTextComponents _tools;
    public final ChTextComponents tools() {
        if (_tools==null) _tools=new ChTextComponents(this);
        return _tools;
    }
    @Override public void paintComponent(Graphics g){
        if (tools().paintHook(this,g,false))
            try {super.paintComponent(g);} catch(Throwable e){}
        tools().paintHook(this,g,true);
    }
    { this.enableEvents(MOUSE_EVENT_MASK|KEY_EVENT_MASK|MOUSE_WHEEL_EVENT_MASK);}
    @Override public void processEvent(AWTEvent ev) {
        final AWTEvent newEv=tools().pEv(ev);
        if (newEv!=null) super.processEvent(newEv);
    }
    @Override public String getToolTipText(MouseEvent mev) {
        final String tt=(String)tools().run(ChTextComponents.TT,mev);
        return tt!=null ? tt : ballonMsg(mev, super.getToolTipText(mev));
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> setText >>> */
    public ChJTextPane t(CharSequence txt){
        final ChTextComponents tools=tools();
        tools.setChanged();
        final String aClass=gcps(KEY_CLASS_NAME,this);
        final int L=sze(txt);
        if (containsHtmlTags(txt)) {
            setOpaque(false);
            final BA sb=new BA(999+L).a("<!DOCTYPE HTML><html><head>");
            final int posCss=sze(sb);
            sb.aln("<title>Strap</title></head>\n<body bgcolor=\"#FFFFFF\">");
            final int posJS=sze(sb);
            sb.aln(txt).aln("</body></html>");
            String name=dTab(this);
            if (aClass!=null && name==null) name=shrtClasNam(aClass);
            final String imported[]=HtmlDoc.getImportedPackages(sb,aClass);
            HtmlDoc.process(sb,name, DOCTYPE_TUTORIAL,imported);
            tools.setJavaPackages(imported);
            try {
                setEditorKit(new HtmlDocEditorKit(imported));
            } catch(Exception e){ stckTrc(e); }
            setContentType("text/html");
            addHyperlinkListener(HtmlDoc.instance());
            final String viewedText=toStrg(sb);
            tools.setTextTS(viewedText);
            htmlTidy(viewedText);
            pcp(KEY_TEXT_CSS_JS, new Object[]{viewedText,intObjct(posCss),intObjct(posJS)}, this);
            String css=HtmlDoc.cssOrJS('c');
            final int media=css==null?-1:css.indexOf("@media");
            if (media>0) ((javax.swing.text.html.HTMLDocument)getDocument()).getStyleSheet().addRule(css.substring(0,media)); else assrt();
        } else {
            setText(toStrg(txt));
        }
        tools.underlineRefs(0);
        return this;
    }
    /* <<< setText  <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    @Override public boolean getScrollableTracksViewportWidth() { return gcp(KOPT_TRACKS_VIEWPORT_WIDTH,this)!=null; }
    public int getFixedRowHeight() { return -1;}
    @Override public Dimension getPreferredSize() {
        final Dimension ps=prefSze(this);
        return ps!=null ? ps : super.getPreferredSize();
    }

    /* <<< Layout <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public Object getDndDateien() { return dndV(false,this); }

}

