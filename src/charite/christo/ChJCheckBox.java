package charite.christo;
import javax.swing.*;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   the extended button class
   @author Christoph Gille
*/
public class ChJCheckBox extends JCheckBox {
    private final Object _but0;
    public ChJCheckBox(AbstractButton but) {
        pcp(KEY_CLONED_FROM,this, but);
        setModel(but.getModel());
        _but0=wref(but);
        setText(getTxt(but));
        setToolTipText(but.getToolTipText());
        setOpaque(false);
        pcp(KEY_CLONED_FROM,but,this);
    }
    //  @Override public void setBackground(Color c) {stckTrc();}
    @Override public void paintComponent(Graphics g) {
        try {

            final ChButton chbut=deref(_but0,ChButton.class);
            final long opts=chbut==null?0L : chbut.getOptions();
            if (!isEnabled() && 0!=(opts&ChButton.PAINT_IF_ENABLED)) return;
            super.paintComponent(g);

        } catch(Throwable ex){}
        ChRenderer.drawSmallButtons(this,g);
    }
    @Override public Dimension getMaximumSize() { return prefSze0(this) ? dim(0,0) : super.getMaximumSize(); }

    @Override public Dimension getPreferredSize() {
        final Dimension ps=prefSze(this);
        return ps!=null ? ps : super.getPreferredSize();
    }
    @Override public Cursor getCursor() {
        final AbstractButton b=deref(_but0,AbstractButton.class);
        return b!=null && b.isEnabled() ? cursr('D') : super.getCursor();
    }
    @Override public void processEvent(AWTEvent ev) {
        if (shouldProcessEvt(ev)) super.processEvent(ev);
    }
    private boolean _radio;
    public ChJCheckBox radio() { _radio=true; return this;}
    @Override public String getUIClassID() {
        return _radio ? "RadioButtonUI" : super.getUIClassID();

    }

}
