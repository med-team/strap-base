package charite.christo;
import static charite.christo.ChUtils.*;
/*
hex2bin("4142") => AB
bin2hex("AB") => 4142
 */
public class Hex2bin {
    private Hex2bin(){}
    private final static byte[] HEX=new byte[16];
    private final static int[] FROM_HEX_LO=new int[256],FROM_HEX_HI=new int[256];
    static {
        for(int i=16;--i>=0;) {
            final int h=HEX[i]=(byte)((i>=10?'A'-10 : '0')+i);
            FROM_HEX_HI[h]=i*16;
            FROM_HEX_LO[h]=i;
        }
    }

    public static byte[] hex2bin(byte hh[], int from, int to) {
        final int t=mini(to, sze(hh));
        if (from>=t) return NO_BYTE;
        byte bb[]=new byte[(t-from)/2];
        for(int i=0,i2=from; i<bb.length; i++,i2+=2) {
            bb[i]=(byte)(FROM_HEX_HI[hh[i2]]+FROM_HEX_LO[hh[i2+1]]);
        }
        return bb;
    }
    public static byte[] bin2hex(byte[] bb,int from,int to) {
        if (bb==null) return null;
        to=mini(to,bb.length);
        final int n=to-from;
        byte hh[]=new byte[2*n];
        for(int i=from,i2=0;i<to;i++) {
            int b=bb[i]; if (b<0) b+=256;
            hh[i2++]=HEX[b>>4];
            hh[i2++]=HEX[b&0xf];
        }
        return hh;
    }
}
