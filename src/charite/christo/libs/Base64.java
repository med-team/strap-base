package charite.christo.libs;
import charite.christo.BA;
/*
 * @author Robert Harder
 * @author rob@iharder.net
 * @version 2.1
 */
public class Base64 {

    /**
     * Translates a Base64 value to either its 6-bit reconstruction value
     * or a negative number indicating some other meaning.
     **/
    private final static byte DECODABET[]={
        -9,-9,-9,-9,-9,-9,-9,-9,-9,                 // Decimal  0 -  8
        -5,-5,                                      // Whitespace: Tab and Linefeed
        -9,-9,                                      // Decimal 11 - 12
        -5,                                         // Whitespace: Carriage Return
        -9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,-9,     // Decimal 14 - 26
        -9,-9,-9,-9,-9,                             // Decimal 27 - 31
        -5,                                         // Whitespace: Space
        -9,-9,-9,-9,-9,-9,-9,-9,-9,-9,              // Decimal 33 - 42
        62,                                         // Plus sign at decimal 43
        -9,-9,-9,                                   // Decimal 44 - 46
        63,                                         // Slash at decimal 47
        52,53,54,55,56,57,58,59,60,61,              // Numbers zero through nine
        -9,-9,-9,                                   // Decimal 58 - 60
        -1,                                         // Equals sign at decimal 61
        -9,-9,-9,                                      // Decimal 62 - 64
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,            // Letters 'A' through 'N'
        14,15,16,17,18,19,20,21,22,23,24,25,        // Letters 'O' through 'Z'
        -9,-9,-9,-9,-9,-9,                          // Decimal 91 - 96
        26,27,28,29,30,31,32,33,34,35,36,37,38,     // Letters 'a' through 'm'
        39,40,41,42,43,44,45,46,47,48,49,50,51,     // Letters 'n' through 'z'
        -9,-9,-9,-9                                 // Decimal 123 - 126
    };
    private Base64(){}

    /* ********  D E C O D I N G   M E T H O D S  ******** */
    /**
     * Decodes four bytes from array <var>source</var>
     * and writes the resulting bytes (up to three of them)
     * to <var>destination</var>.
     * The source and destination arrays can be manipulated
     * anywhere along their length by specifying
     * <var>srcOffset</var> and <var>destOffset</var>.
     * This method does not check to make sure your arrays
     * are large enough to accomodate <var>srcOffset</var> + 4 for
     * the <var>source</var> array or <var>destOffset</var> + 3 for
     * the <var>destination</var> array.
     * This method returns the actual number of bytes that
     * were converted from the Base64 encoding.
     *
     *
     * @param source the array to convert
     * @param srcOffset the index where conversion begins
     * @param destination the array to hold the conversion
     * @param destOffset the index where output will be put
     * @return the number of decoded bytes converted
     * @since 1.3
     */
    private static int decode4to3( byte[] source, int srcOffset, byte[] destination, int destOffset ) {
        // Example: Dk==
        if( source[ srcOffset + 2] == '=' ) {
            // Two ways to do the same thing. Don't know which way I like best.
            //int outBuff=  ( ( DECODABET[ source[ srcOffset    ] ] << 24 ) >>>  6 )
            //              | ( ( DECODABET[ source[ srcOffset + 1] ] << 24 ) >>> 12 );
            final int outBuff=  ( ( DECODABET[ source[ srcOffset    ] ] & 0xFF ) << 18 )
                | ( ( DECODABET[ source[ srcOffset + 1] ] & 0xFF ) << 12 );
            destination[ destOffset ]=(byte)( outBuff >>> 16 );
            return 1;
        }
        // Example: DkL=
        else if( source[ srcOffset + 3 ] == '=' ) {
            // Two ways to do the same thing. Don't know which way I like best.
            //int outBuff=  ( ( DECODABET[ source[ srcOffset     ] ] << 24 ) >>>  6 )
            //              | ( ( DECODABET[ source[ srcOffset + 1 ] ] << 24 ) >>> 12 )
            //              | ( ( DECODABET[ source[ srcOffset + 2 ] ] << 24 ) >>> 18 );
            final int outBuff=  ( ( DECODABET[ source[ srcOffset     ] ] & 0xFF ) << 18 )
                | ( ( DECODABET[ source[ srcOffset + 1 ] ] & 0xFF ) << 12 )
                | ( ( DECODABET[ source[ srcOffset + 2 ] ] & 0xFF ) <<  6 );
            destination[ destOffset     ]=(byte)( outBuff >>> 16 );
            destination[ destOffset + 1 ]=(byte)( outBuff >>>  8 );
            return 2;
        }
        // Example: DkLE
        else {
                try{
                    // Two ways to do the same thing. Don't know which way I like best.
                    //int outBuff=  ( ( DECODABET[ source[ srcOffset     ] ] << 24 ) >>>  6 )
                    //              | ( ( DECODABET[ source[ srcOffset + 1 ] ] << 24 ) >>> 12 )
                    //              | ( ( DECODABET[ source[ srcOffset + 2 ] ] << 24 ) >>> 18 )
                    //              | ( ( DECODABET[ source[ srcOffset + 3 ] ] << 24 ) >>> 24 );
                    final int outBuff=  ( ( DECODABET[ source[ srcOffset     ] ] & 0xFF ) << 18 )
                        | ( ( DECODABET[ source[ srcOffset + 1 ] ] & 0xFF ) << 12 )
                        | ( ( DECODABET[ source[ srcOffset + 2 ] ] & 0xFF ) <<  6)
                        | ( ( DECODABET[ source[ srcOffset + 3 ] ] & 0xFF )      );
                    destination[ destOffset     ]=(byte)( outBuff >> 16 );
                    destination[ destOffset + 1 ]=(byte)( outBuff >>  8 );
                    destination[ destOffset + 2 ]=(byte)( outBuff       );
                    return 3;
                }catch( Exception e){
                    System.out.println(""+
                                       source[srcOffset]+ ": " + ( DECODABET[ source[ srcOffset     ] ]  ) +"\n"+
                                       source[srcOffset+1]+  ": " + ( DECODABET[ source[ srcOffset + 1 ] ]  ) +"\n"+
                                       source[srcOffset+2]+  ": " + ( DECODABET[ source[ srcOffset + 2 ] ]  ) +"\n"+
                                       source[srcOffset+3]+  ": " + ( DECODABET[ source[ srcOffset + 3 ] ]  ) );
                    return -1;
                }   //e nd catch
            }
    }   // end decodeToBytes
    /**
     * Very low-level access to decoding ASCII characters in
     * the form of a byte array. Does not support automatically
     * gunzipping or any other "fancy" features.
     *
     * @param source The Base64 encoded data
     * @param off    The offset of where to begin decoding
     * @param len    The length of characters to decode
     * @return decoded data
     * @since 1.3
     */
    public static BA decode(byte[] source, int off, int to ) {
        final byte WHITE_SPACE_ENC=-5; // Indicates white space in encoding
        final byte EQUALS_SIGN_ENC=-1; // Indicates equals sign in encoding
        final int len=to-off;
        int    len34  =len * 3 / 4;
        final byte[] outBuff=new byte[ len34 ]; // Upper limit on size of output
        int    outBuffPosn=0;
        byte[] b4       =new byte[4];
        int    b4Posn   =0;
        int    i        =0;
        byte   sbiCrop  =0;
        byte   sbiDecode=0;
        for( i=off; i < to; i++ ) {
            sbiCrop=(byte)(source[i] & 0x7f); // Only the low seven bits
            sbiDecode=DECODABET[ sbiCrop ];
            if( sbiDecode >= WHITE_SPACE_ENC ) { // White space, Equals sign or better
                    if( sbiDecode >= EQUALS_SIGN_ENC ) {
                        b4[ b4Posn++ ]=sbiCrop;
                        if( b4Posn > 3 ) {
                            outBuffPosn += decode4to3( b4, 0, outBuff, outBuffPosn );
                            b4Posn=0;
                            // If that was the equals sign, break out of 'for' loop
                            if( sbiCrop == '=' )
                                break;
                        }   // end if: quartet built
                    }   // end if: equals sign or better
                }   // end if: white space, equals sign or better
            else {
                    System.err.println( "Bad Base64 input character at " + i + ": " + source[i] + "(decimal)" );
                    return null;
                }   // end else:
        }   // each input character
        return new BA(outBuff,0,outBuffPosn);
    }   // end decode
}
