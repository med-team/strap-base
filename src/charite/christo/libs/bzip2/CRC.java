/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/*
 * This package is based on the work done by Keiron Liddle, Aftex Software
 * <keiron@aftexsw.com> to whom the Ant project is very grateful for his
 * great code.
 */

package charite.christo.libs.bzip2;

/**
 * A simple class the hold and calculate the CRC for sanity checking
 * of the data.
 *
 */
final class CRC {

    CRC() {
        initialiseCRC();
    }

    void initialiseCRC() {
        globalCrc = 0xffffffff;
    }

    int getFinalCRC() {
        return ~globalCrc;
    }
    /*
    int getGlobalCRC() { return globalCrc; }
    void setGlobalCRC(int newCrc) { globalCrc = newCrc; }
    */
    void updateCRC(int inCh) {
        final int[] crc32Table=BZip2Constants.crc32Table();
        int temp = (globalCrc >> 24) ^ inCh;
        if (temp < 0) {
            temp = 256 + temp;
        }
        globalCrc = (globalCrc << 8) ^ crc32Table[temp];
    }
    /*
    void updateCRC(int inCh, int repeat) {
        final int[] crc32Table=BZip2Constants.crc32Table();
        int globalCrcShadow = this.globalCrc;
        while (repeat-- > 0) {
            int temp = (globalCrcShadow >> 24) ^ inCh;
            globalCrcShadow = (globalCrcShadow << 8) ^ crc32Table[(temp >= 0)
                                                      ? temp
                                                      : (temp + 256)];
        }
        this.globalCrc = globalCrcShadow;
    }
    */

    int globalCrc;
}

