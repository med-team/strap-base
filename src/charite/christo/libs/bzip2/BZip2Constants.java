/*
This package is based on the work done by Keiron Liddle, Aftex Software
<keiron@aftexsw.com> to whom the Ant project is very grateful for his
great code.
*/
package charite.christo.libs.bzip2;
import java.io.*;
import static java.lang.System.*;
public class BZip2Constants {
    final static int
        baseBlockSize = 100000,
        MAX_ALPHA_SIZE = 258,
        MAX_CODE_LEN = 23,
        RUNA = 0,
        RUNB = 1,
        N_GROUPS = 6,
        G_SIZE = 50,
        N_ITERS = 4,
        MAX_SELECTORS = (2 + (900000 / G_SIZE)),
        NUM_OVERSHOOT_BYTES = 20;

    private static int[] _rNums, _crc32Table;
    public static int[] rNums() {
        if (_rNums==null) _rNums=read(512,"rnum.rsc");
        return _rNums;
    }
    public static int[] crc32Table() {
        if (_crc32Table==null) _crc32Table=read(256,"crc.rsc");
        return _crc32Table;
    }

    public static int[] read(int n, String file) {
        try {
            final int ii[]=new int[n];
            StreamTokenizer t=new StreamTokenizer(BZip2Constants.class.getResourceAsStream(file));
            for(int i=0;i<n; i++) {
                t.nextToken();
                ii[i]=(int)t.nval;
            }
            return ii;
        } catch(Exception e) {out.println("caughtIn BZip2Constants"); out.println(e); return null;}
    }

}
