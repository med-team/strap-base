package charite.christo;


import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Some modale message dialogs
   @author Christoph Gille
*/
public class ChMsgDia implements java.awt.event.ActionListener, Runnable {
    public final static int YES_NO=1, OK=2;
    private final long _options;
    private final String _buttons[];
    private String _value;
    private int iButton=MIN_INT;

    private Object _msg;
    public ChMsgDia(int type, Object msg, String[] buttons0,  long options) {
        _msg=msg;
        _options=options;
        final String[] buttons=buttons0!=null?buttons0:
            type==YES_NO?new String[]{"yes","No"} :
        type==OK ? new String[]{"OK"} :
        new String[0];
        _buttons=buttons;
        //putln("ChMsgDia constructor ");
        inEDT(this);

    }
    public void run() {
        final ChButton bb[]=new ChButton[sze(_buttons)];
        for(int i=bb.length; --i>=0;) {
            if (_buttons[i]==null) continue;
            bb[i]=new ChButton(_buttons[i]).li(this);
        }
        final Object pMain=pnl(CNSEW,_msg, null,pnl(bb),null,pnl(  iicon("javax/swing/plaf/metal/icons/ocean/question.png") ));
        final ChFrame f=new ChFrame("Strap-Dialog").ad(pMain).shw(_options|ChFrame.ALWAYS_ON_TOP|ChFrame.TO_FRONT);
         addActLi(this,f);
         _msg=null;
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        for(int i=sze(_buttons); --i>=0;) {
            if (eqNz(cmd, _buttons[i])) {
                iButton=i;
                _value=_buttons[i];
                closeW(CLOSE_DISPOSE, q);
            }
        }
        if (cmd==ACTION_WINDOW_CLOSING) {
            iButton=-1;
            closeW(CLOSE_DISPOSE, q);
        }
    }
    public int i() {
        while(iButton==MIN_INT) {
            //puts("*");
            sleep(99);
        }
        //putln(GREEN_DONE);
        return iButton;
    }
    public String value() { i(); return _value;}

}
