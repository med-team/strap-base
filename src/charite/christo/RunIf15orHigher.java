package charite.christo;
import java.awt.*;
import static charite.christo.ChUtils.*;
class RunIf15orHigher implements ChRunnable {

    public final static String RUN_BIG_DECI="R15$$RBD", RUN_GET_MOUSE_LOCATION="R15$$M", RUN_GET_SOCKET="R15$$S",
        SET_AOT="R15$$sAOT", GET_AOT="R15$$gAOT";

    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id==RUN_BIG_DECI) {
            final Object num=argv[0];
            final int precision=atoi(argv[1]);
            if (mCtx==null) mCtx=new java.math.MathContext[999];
            if (mCtx[precision]==null) mCtx[precision]=new java.math.MathContext(precision);
            return new java.math.BigDecimal(num.toString(), mCtx[precision]);
        }
        if (id==RUN_GET_MOUSE_LOCATION) {
            final PointerInfo pi=dtkt()==null?null : MouseInfo.getPointerInfo();
            return pi==null?null:pi.getLocation();
        }
        if (id==RUN_GET_SOCKET) {
            final java.net.Proxy p=(java.net.Proxy)arg;
            return p!=null ? new java.net.Socket(p) : new java.net.Socket();
        }
        if (id==SET_AOT) {
            final Window f=(Window)argv[1];
            if (f!=null) f.setAlwaysOnTop(isTrue(argv[0]));
        }
        if (id==GET_AOT) {
            final Window f=(Window)arg;
            return  f!=null && f.isAlwaysOnTop() ?"":null;
        }

        return null;
    }
    private static java.math.MathContext mCtx[];

}
