package charite.christo;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class RenameFile extends javax.swing.JPanel implements ActionListener {

    private final ChTextArea _ta=new ChTextArea(""), _taHeader=new ChTextArea("");
    private final ChTextView _labMsg=new ChTextView("");
    public RenameFile() {
        final Object
            msg2="<sub>Change the file paths in the right column and press the button &lt;Rename&gt;.<br>"+
            "Hit the tab key for file path completion.</sub>",
            pSouth=pnl(new GridLayout(2,1), msg2, pnl(new ChButton("R").t("Rename").li(this)," ",_labMsg));
        _ta.tools().enableUndo(true).enableWordCompletion(dirWorking()).underlineRefs(ULREFS_NOT_CLICKABLE);
        addActLi(this,_ta);
        _taHeader.setEditable(false);
        pnl(this,CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE,_ta), setBG(DEFAULT_BACKGROUND,_taHeader), pSouth);
    }

    public void setFiles(File ff[]) {
        final int N=countNotNull(ff);
        final String ss[]=new String[N], ofn="Old file name";
        final boolean isWin=isWin();
        for(int i=0, j=0; i<N; i++) {
            if (ff[i]==null) continue;
            final File f=ff[j++];
            ss[i]=(isWin ? f.getAbsolutePath() : fPathUnix(f)).replaceAll(" ","%20");
        }
        final int len=maxi(1+sze(ofn), sze(longestName(ss)));
        final BA sb=new BA(999).a('\n');
        for(String s : ss)  sb.a(' ',len-sze(s)).a(s).a(' ',4).a(s).a('\n',2);
        _ta.t(sb);
        _taHeader.t(sb.clr().a(' ',len-sze(ofn)).a(ofn).a(' ',4).a("New file name"));
    }

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final BA baMsg=_labMsg.byteArray();
        if (cmd=="R") {
            final BA txt=_ta.tools().byteArray();
            final byte[] T=txt.bytes();
            final int ends[]=txt.eol();
            final ChTokenizer TOK=new ChTokenizer();
            int success=0, failed=0, violation=0;
            final File vViolation[]=new File[ends.length];
            for(int i=0; i<ends.length; i++) {
                final int b=i==0 ? 0 : ends[i-1]+1;
                TOK.setText(T,b,ends[i]);
                final String alt=TOK.nextAsString(), neu=TOK.nextAsString();
                if (alt!=null && neu!=null && chrAt(0,alt)!='#') {
                    final File fAlt=file(alt), fNeu=file(neu);
                    if (fAlt.exists() && !fAlt.equals(fNeu)) {
                        delFile(fNeu);
                        if (fNeu.exists()) failed++;
                        else {
                            if (!Insecure.canModify(fAlt)) vViolation[violation++]=fAlt;
                            else if (!Insecure.canModify(fNeu)) vViolation[violation++]=fNeu;
                            else {
                                renamFile(fAlt,fNeu);
                                if (fNeu.exists()) success++;
                                else failed++;
                            }
                        }
                    }
                }
                if (countNotNull(vViolation)>0) error(Insecure.msgNotModify(new BA(0).a('\n').join(vViolation).a('\n')));
            }
            _ta.tools().run('H').run();

            if (success+failed>0) {
                baMsg.clr().a(plrl(success, GREEN_SUCCESS+"Renamed %N file%S"));
                if (failed>0) baMsg.a(plrl(failed, "   "+RED_WARNING+"Failed %N file%S"));
            } else baMsg.clr().a(RED_WARNING+"No file renamed");
            revalAndRepaintCs(_labMsg);
        }
        if (cmd==ACTION_TEXT_CHANGED) {
            baMsg.clr();
            revalAndRepaintCs(_labMsg);
        }
    }
    private static RenameFile _inst;
    public static ChFrame openFrame(long frameOpts, File ff[]) {
        if (_inst==null) _inst=new RenameFile();
        _inst.setFiles(ff);
        return ChFrame.frame("Rename Files", _inst, 0L).shw(frameOpts);
    }

}
