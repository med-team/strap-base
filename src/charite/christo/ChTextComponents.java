package charite.christo;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.undo.UndoManager;
import javax.swing.event.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.KeyEvent.*;
import static java.awt.event.MouseEvent.*;
/**
   Utilities for TextComponents
   @author Christoph Gille
*/
public final class ChTextComponents implements DocumentListener, ChRunnable, PaintHook, UndoableEditListener, ProcessEv, HasMC {
    public final static byte[] WRONG_CHARS_IN_FILEN={(byte)'x'}, MOVIE="MOVIE:".getBytes();
    public final static String
        COMPL_HYPERREFS="CTC$$WCHR",
        KOPT_F1_HELP="CTC$$f1",
        KOPT_CLASS_AT_CURSOR="CTCs$$_2",
        ACTION_INSERTED_TEXT_LONGER_ONE="CTCs$$I",
        RANGE_SUFX_XML="_VIEW_XML_",
        FILE_RANGE_SUFX[]={"_VIEW_TEXT_", RANGE_SUFX_XML},
        TT="CTC$$TT";
    final static String PDF4PMID="&PDF4PMID", DOCID_TMP="ChTextComponents_tmp";

    private boolean _saveNeeded, _saveLoaded, _painted,
        _byteArrayValid, _segmentValid, _alreadyTyped, _alreadUlRefs,
        _hlAuto=true, _hlMouseEnter,
        _changedSinceGainedFocus,
        _evEnabled=true, _doingDnd;

    private final static String
        LEFT_CLASS_DELIM=" >\t\n\r()=+,*/&|!;>{}:", CLASS_DELIM=LEFT_CLASS_DELIM+".[]",
        _imageRefsS[], _imageRefsI[];
    private final static int _imageRefsStrLen[], _region[]={0,0};// STYLE_UL_MAX=4;
    static {
        String[] ss=null, ii=null;
        final String[] all="WIKI{=wikipedia WIKI:=wikipedia PUBMED:=pubmed PUBMED{=pubmed MOVIE:=movie  ~ / file:/ 1 2 3 4 5 6 7 8 9 http:// ftp://".split(" ");
        /* Digits are for ec-class */
        int ssL[]=null;
        for(int i=all.length; --i>=0;) {
            final int eq=all[i].indexOf('=');
            if (ss==null) ss=new String[i+1];
            if (eq<0)  ss[i]=all[i];
            else {
                if (ii==null) ii=new String[i+1];
                if (ssL==null) ssL=new int[i+1];
                ss[i]=all[i].substring(0,eq);
                ii[i]=all[i].substring(eq+1);
                ssL[i]=ss[i].length();
            }
        }
        _imageRefsS=ss;
        _imageRefsI=ii;
        _imageRefsStrLen=ssL;
    }

    private final Rectangle _RECT=new Rectangle();
    private final EvAdapter LI=new EvAdapter(this);
    private static boolean _escKey;
    private int _modifiers;
    /* ---------------------------------------- */
    /* >>> Instance >>> */
    final Object _ref;
    private static ChTextComponents _inst;
    private static ChTextComponents instance() {if (_inst==null) _inst=new ChTextComponents(null); return _inst;}
    public ChTextComponents(JComponent jc) {
        _ref=new java.lang.ref.WeakReference(jc);
        if (tc()!=null) {
            tc().getDocument().addDocumentListener(this);
            tc().setSelectionColor(C(0xFF, 0x55));
            tc().setHighlighter(new ChJHighlighter(this));
        }
        if (!(jc instanceof ChTextField)) ChThread.preventGC(jc);
        if (jc instanceof ChTextArea || jc instanceof ChTextView) addActLi(LI,ChPubmed.class);
        scrollByWheel(jc);
    }
    public JComponent jc() { return (JComponent)deref(_ref);}
    public JTextComponent tc() { return deref(_ref,JTextComponent.class);}

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Global settings >>> */
    private static Collection<ActionListener> _vLiDb;
    private static DialogFindInText _associateFind[]={null,null};
    private static ChRunnable _ctxtProvider;
    private static ChTextComponents _taForAbstract;
    public static void setContextMenuProvider(ChRunnable r) { _ctxtProvider=r;}
    public static void setTextAreaForAbstract(ChTextComponents t) { _taForAbstract=t;}
    public static void addLiDb(ActionListener li) { _vLiDb=adUniqNew(li, _vLiDb); }
    public static void setAssociateFind(boolean focus, DialogFindInText d) { _associateFind[focus?0:1]=d; }
    /* <<< Global settings <<< */
    /* ---------------------------------------- */
    /* >>> Undo/Redo >>> */
    private Object _undoM;
    private UndoManager undoManager(boolean instantiate) {
        UndoManager m=(UndoManager) deref(_undoM);
        if (m==null && instantiate) { m=new UndoManager(); _undoM=_undoSoft ? newSoftRef(m) : m; }
        return m;
    }
    private boolean _undoSoft;
    public void undoableEditHappened(javax.swing.event.UndoableEditEvent e) {
        undoManager(true).addEdit(e.getEdit());
    }
    public ChTextComponents enableUndo(boolean soft) {
        _undoSoft=soft;
        if (tc()!=null) tc().getDocument().addUndoableEditListener(this);
        return this;
    }
    /* <<< Undo/Redo <<< */
    /* ---------------------------------------- */
    /* >>> Tab-Completion >>> */
    public final static int COMPL_TAB=1<<1, COMPL_IC=1<<2, COMPL_INFER_CASE=1<<3, COMPL_ALLOW_SPC_IN_FILE=1<<4;
    private Set<String> _complOffered;
    private Object _vComplFirst, _vComplLast, _complDirListing[];
    private long _complOptions;
    private boolean[] _complDelimL, _complDelimR;
    private int _complCaret, _complNxtIdx[]=NO_INT;
    private FileFilter _complFileFilter;
    public ChTextComponents setWordCompletionSetFirst(Object collection) { _vComplFirst=collection; return this; }
    public ChTextComponents enableWordCompletion(Object collection) {
        return enableWordCompletion(COMPL_TAB, collection,null,null);
    }
    public ChTextComponents enableWordCompletion(long options, Object collection, boolean lDelim[], boolean rDelim[]) {
        if (collection==null || tc()==null) return this;
        if (collection instanceof FileFilter) _complFileFilter=(FileFilter)collection;
        else {
            _vComplLast=_vComplLast==null ? collection : new Object[]{_vComplLast,collection};
        }
        _complDelimL=lDelim!=null ? lDelim : chrClas(TAB_COMPL_DELIMl);
        _complDelimR=rDelim!=null ? rDelim : chrClas(TAB_COMPL_DELIMr);
        _complOptions=options;
        final JComponent jc=jc();
        if (jc!=null && (_complOptions&COMPL_TAB)!=0) jc.setFocusTraversalKeys( KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, Collections.EMPTY_SET);
        return this;
    }
    private void complClear() {
        clr(_complOffered);
        _complDirListing=null;
        Arrays.fill(_complNxtIdx,0);
        _complCaret=0;
    }
    private static Object complObject(Object o) {
        Object c=null!=tools(o) ? tools(o).byteArray() : o;
        while(!(c instanceof BA) && !(c instanceof List) && c instanceof ChRunnable)  c=((ChRunnable)c).run(PROVIDE_WORD_COMPLETION_LIST, null);
        if (c instanceof HasName || c instanceof File) c=nam(c);
        else if (c instanceof JTextComponent) c=getTxt(c);
        else if (c==COMPL_HYPERREFS) c=Hyperrefs.getKeysToHighlight();
        return c;
    }
    private boolean completeRecursive(Object collection0, int recursion, long option) {
        if (collection0==null) return false;
        final boolean evEnabled=_evEnabled;
        enableTextChangeEv(false);
        final Document doc=tc().getDocument();
        try {
            final long strstr=(option&COMPL_IC)!=0 ? STRSTR_IC : 0L;
            if (_complOffered==null) _complOffered=new HashSet();
            final Object collection=complObject(collection0);
            if (sze(_complNxtIdx)<=recursion) _complNxtIdx=chSze(_complNxtIdx,recursion+5);
            if (_complCaret>0) {
                try {
                    doc.remove(_complCaret, tc().getCaretPosition()-_complCaret);
                } catch (Exception e){ putln(RED_CAUGHT_IN+"ChTextComponents",".complete()",e);}
            }
            final BA byteArray=byteArray();
            final byte T[]=byteArray.bytes();
            final int caret=tc().getCaretPosition(), wordStart=prevE(_complDelimL,T,caret-1,-1)+1;
            String complWord=null;
            final String strg=collection instanceof String ? (String)collection : null;
            if (collection instanceof BA || collection instanceof byte[]) {
                final byte[] colT;
                final int B,E;
                if (collection instanceof BA) {
                    final BA ba=(BA)collection;
                    colT=ba.bytes();
                    E=ba.end();
                    B=ba.begin();
                } else { B=0; E=(colT=(byte[])collection).length; }
                for(int jT=maxi(B, _complNxtIdx[recursion]); jT<E; jT++) {
                    final int iCol=collection==byteArray && jT<caret ? caret-jT-1 : jT;
                    if (wordStart!=iCol && !isTrue(_complDelimL,chrAt(iCol,colT)) && (iCol==0 || isTrue(_complDelimL,chrAt(iCol-1,colT))) && strEquls(strstr,colT, iCol, iCol+caret-wordStart,  T, wordStart)) {
                        final int wordEnd=nxtE(_complDelimR, colT,iCol+caret-wordStart,E);
                        if (wordEnd==iCol+caret-wordStart) continue;
                        final String s=toStrgIP(colT, iCol,wordEnd);
                        if (_complOffered.add(s)) {
                            complWord=s;
                            _complNxtIdx[recursion]=jT+1;
                            break;
                        }
                    }
                }
            } else if (strg!=null || collection instanceof List || collection instanceof Object[]) {
                final int N=strg!=null ? 1 : sze(collection);
                for(int i=_complNxtIdx[recursion]; i<N; i++) {
                    final Object
                        o0=strg!=null ? strg : complObject(deref(get(i,collection))),
                        o=collection==_complDirListing && 0==(_complOptions&COMPL_ALLOW_SPC_IN_FILE) ? rplcToStrg(" ","%20",toStrg(o0)) : o0;
                    if (o instanceof List || o instanceof BA || o instanceof byte[] || o instanceof Object[]) {
                        if (recursion>10) putln(RED_WARNING+"ChTextComponents"," word completion: Recursion=",intObjct(recursion));
                        else if (completeRecursive(o, recursion+1, option)) return true;
                    } else if (o instanceof CharSequence) {
                        if (sze(o)>caret-wordStart && strEquls(strstr, T, wordStart, caret, o, 0)) {
                            final String s=toStrg(o);
                            if (_complOffered.add(s)) {
                                complWord=s;
                                _complNxtIdx[recursion]=i+1;
                                break;
                            }
                        }
                    } else if (o!=null) putln(RED_WARNING+"ChTextComponents"," unsupported word completion collection ",shrtClasNam(o));
                }
            }
            if (complWord!=null) {
                try {
                    if (0==(option&COMPL_INFER_CASE)) doc.insertString(caret, complWord.substring(caret-wordStart), null);
                    else {
                        doc.remove(wordStart, caret-wordStart);
                        doc.insertString(wordStart, complWord, null);
                    }
                } catch (Exception e){ putln(RED_CAUGHT_IN+"ChTextComponents", ".complete() ",e);
                }
                _complCaret=caret;
                return true;
            }
            return false;
        } finally {
            enableTextChangeEv(evEnabled);
        }
    }
    /* <<< Tab-Completion <<<  */
    /* ---------------------------------------- */
    /* >>> Packages >>> */
    private String[] _javaPackages;
    void setJavaPackages(String[] pp) { _javaPackages=pp;}
    /* <<< Packages <<< */
    /* ---------------------------------------- */
    /* >>> Word under Mouse >>> */
    private String _protDBColonId, _class, _url, _plugin, _word, _mMovedWord, _id;
    private static File _file;
    private static Range _range;
    private static Range range() { return _range==null?_range=new Range() : _range;}
    private int _mouseTextPos=-99999, _wordFrom=-99999, _wordTo=-99999, _mMovedModi;
    private void setMousePosition(int pos) {
        _mouseTextPos=pos;
        final String lastW=_word;
        final JComponent jc=jc();
        final BA ba=byteArray();
        final byte[] T=ba.bytes();
        /* --- java class --- */
        if (gcp(KOPT_CLASS_AT_CURSOR,jc)!=null) {
            if (_javaPackages==null)  _javaPackages=ChCodeViewer.getImportedPackages(ba);
            final Range r=getWordAt(pos,ba, chrClas(LEFT_CLASS_DELIM), chrClas(CLASS_DELIM), range());
            _class=r==null?null:fullClassNam(true, ba.newString(r.from(),r.to()),_javaPackages);
        }
        final Range r=getWordAt(pos,ba, chrClas(" \t\n\r\"(>"), chrClas(" \t\n\r\");"), range());
        if (r!=null) {
            final int
                from=r.from(),
                to0=r.to(),
                to=(get(to0,T)==')' && strEquls("WIKI",T, from)) ? to0+1 : to0;
            final boolean semicolonSpace=from>8 && T[from-1]==' ' && T[from-2]==';';
            if (_wordFrom!=from || _wordTo!=to || lastW==null || lastW.indexOf(':')>0 || semicolonSpace) {
                final String w=ba.newString(from,to);
                final int colon=w.indexOf(':');
                if (colon<0 && looks(LIKE_MAILADDR,w)) _url="mailto:"+(_word=w);
                else if (!w.equals(lastW) || colon>0 || semicolonSpace) {
                    _word=w;
                    _url=_protDBColonId=_plugin=null;
                    _file=null;
                    if (sze(w)>2) {
                        {
                            final int fFrom=T[from]=='[' ? from+1 : from;
                            final int fTo=fileEndsAt(T,fFrom,ba.end());
                            final File f=fTo<0 ? null :  myFile(ba.newString(fFrom,fTo));
                            _file=f!=null?f:cntainsOnly(FILENM,w)?file(w) : null;
                        }
                        _id=
                            looks(LIKE_EC,w, colon+1,MAX_INT) ? w.substring(colon+1, nxtE(-DIGT_DOT,w,colon+1,MAX_INT)) :
                            w.startsWith("PDB:") || w.startsWith("UNIPROT:") || guessDbFromId(w)=="UNIPROT:" ? w :
                            null;
                        String emblDB=null, url=Hyperrefs.toUrlString(delSfx('.',w),Hyperrefs.WEB_LINK|Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE);
                        if (endWith("?plugins",url)) _plugin=url;
                        EMBL_links:
                        if (semicolonSpace) { /* --- EMBL; U13634; AAB03370.1;  --- */
                            final int from0=maxi(0,from-72);
                            for(int semicolon=from-1; --semicolon>from0;) {
                                if (T[semicolon]!=';' || T[semicolon+1]!=' ') continue;
                                for(int space=semicolon; --space>from0;) {
                                    if  (T[space]!=' ') continue;
                                    final int L=semicolon-space;
                                    nextKey:
                                    for(String key : Hyperrefs.getDatabases(Hyperrefs.WEB_LINK|Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE)[0]) {
                                        if (sze(key)!=L || lstChar(key)!=':') continue;
                                        for(int i=L-1; --i>=0;) if (key.charAt(i)!=T[space+1+i]) continue nextKey;
                                        emblDB=key+delSfx('.',delSfx(';',w));
                                        url=Hyperrefs.toUrlString(emblDB,Hyperrefs.WEB_LINK|Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE);
                                        break EMBL_links;
                                    }
                                }
                            }
                        }
                        final int middle=emblDB!=null ? sze(w)/2 : colon;
                        _url=url;
                        final String ew=orS(emblDB, w);
                        if (middle>=0
                            && pos-from>middle
                            && gcp(ACTION_DATABASE_CLICKED,jc)!=null
                            && w.indexOf("://")<0
                            && ( Hyperrefs.toUrlString(ew,Hyperrefs.PROTEIN_FILE)!=null || ew.startsWith("PDB:"))
                            ) {
                            _protDBColonId= ew;
                        }
                        _protDBColonId=rplcToStrg("DIP:DIP:","DIP:",_protDBColonId);
                    }
                }
            }
        } else {
            _word=_url=_protDBColonId=_plugin=_id=null;
            _file=null;
            _wordFrom=_wordTo=-99999;
        }
    }
    public String getWordUnderMouse(Range r) {
        final String w=_word;
        if (r!=null) r.set(w!=null ? _wordFrom:0, w!=null?_wordTo:0);
        return w;
    }
    /* <<< Word under Mouse <<< */
    /* ---------------------------------------- */
    /* >>> Word at position >>> */
    private final static Rectangle RECTf=new Rectangle(), RECTt=new Rectangle();
    private final static Point POINTv=new Point();
    private static Range getWordAt(Point p, JComponent tc, boolean[] lDelimiters, boolean[] rDelimiters,Range range) {
        final int iCaret=viewToModel(p,tc);
        final ChTextComponents tools=tools(tc);
        if (tools==null) return null;

        getWordAt(iCaret, tools.byteArray(),lDelimiters,rDelimiters,range);
        final int f=range==null ? 0:range.from(), t=range==null ? 0:range.to();
        if (t-f==0) return null;
        modelToView(f,tc, RECTf);
        modelToView(t,tc, RECTt);
        final int rH=RECTf.height, tH=RECTt.height>0?RECTt.height:rH;
        if (rH>0) {
            final boolean contains=  x(RECTf)<x(RECTt) ?
                contains(p, x(RECTf), y(RECTf), x2(RECTt),  y(RECTt)+tH-1) :
                contains(p, x(RECTf), y(RECTf),     99999,  y(RECTf)+rH-2) ||
                contains(p,        0, y(RECTt),  x(RECTt),  y(RECTt)+tH-2);
            return !contains ? null : range;
        }
        return null;
    }
    public static String getWordAt(Point p, JComponent tc, boolean[] lDelimiters, boolean[] rDelimiters) {
        final Range r=getWordAt(p,tc,lDelimiters,rDelimiters,range());
        final ChTextComponents tools=tools(tc);
        if (r==null || tools==null) return null;
        final int f=r.from(), t=r.to();
        final BA ba=tools.byteArray();
        return f>=0 && f<t && t<ba.end() ? ba.newString(f,t) : null;
    }
    public static String getWordAt(final int iCaret, BA ba, boolean[] lDelimiters, boolean[] rDelimiters) {
        final Range r=getWordAt(iCaret,ba,lDelimiters,rDelimiters,range());
        if (r==null) return null;
        final int f=r.from(), t=r.to();
        return f>=0 && f<t && t<ba.end() ? ba.newString(f,t) : null;
    }
    public static Range getWordAt(final int iCaret, BA ba, boolean[] lDelimiters, boolean[] rDelimiters, Range range) {
        if (ba==null || iCaret<0) return null;
        final boolean[] lD=lDelimiters!=null?lDelimiters: chrClas(" \t\n\r(");
        final boolean[] rD=rDelimiters!=null?rDelimiters: chrClas(" \t\n\r)");
        final byte[] bb=ba.bytes();
        final int L=ba.end();
        int l,r;
        for(l=iCaret; l> 0 && l<=L && !(bb[l-1]>=0 && lD[bb[l-1]]); l--);
        for(r=iCaret; r>=0 && r< L && !(bb[r  ]>=0 && rD[bb[r  ]]); r++);
        l=mini(maxi(0,l),L);
        r=mini(maxi(0,r),L);
        if (strchr('{',bb,l,r)>0) {
            for(String key: Hyperrefs.getKeysWithBrace()) {
                if ( strEquls(key,bb,l)) {
                    r=strchr('}',bb,l,L);
                    if (r<0) return null;
                    break;
                }
            }
        }
        if (l>=r || r>L || l<0) return null;
        range.set(l,r);
        return range;
    }
    /* <<< Word at position <<< */
    /* ---------------------------------------- */
    /* >>> Cursor >>> */
    private Cursor _origCursor;
    public Cursor getCursor() {
        final JComponent jc=jc();
        if (jc==null) return cursr(Cursor.DEFAULT_CURSOR);
        if (_origCursor==null) _origCursor=jc.getCursor();
        final String w=_word;
        Cursor curs=null;
        final int m=_modifiers;
        final boolean shift=(m&SHIFT_MASK)!=0, ctrl=(m&CTRL_MASK)!=0, alt=(m&ALT_MASK)!=0;
        if (w!=null) {
            if (pmid(w)>0) curs=cursr(Cursor.DEFAULT_CURSOR);
            else {
                final Object c=
                    (_uRefsOptions&ULREFS_NOT_CLICKABLE)!=0?null:
                    _class!=null ? ( !(ctrl||alt) ? "Source\nJavadoc" : shift ? "Edit" : ctrl ? "Source" : "Javadoc" ):
                    sze(_file)>0 && looks(LIKE_FILEPATH,w) ? ( isExeFileName(_file) ? "run" : systProprty(SYSP_ICON_FILE_BROWSER)):
                    looks(LIKE_MAILADDR, w) ? "Mail" :
                    _protDBColonId!=null ? gcp(ACTION_DATABASE_CLICKED,jc) :
                    _plugin!=null ? "Load" :
                    _url!=null || _id!=null || strEquls("MOVIE:",w) || strEquls("Image:",w)? IC_WWW16 :
                    null;
                curs=ChIcon.getCursor(toStrg(c),jc);
            }
        }
        return curs!=null ? curs : _origCursor;
    }
    /* <<< Cursor <<< */
    /* ---------------------------------------- */
    /* >>> Contextmenu >>> */
    private String _textForContextMenu;
    private JPopupMenu _contextMenu;
    private Object[] newContextMenuObjects(String title) {
        return new Object[]{
            title,
            new ChButton("W_CPY").li(LI).t("Copy text").i(IC_CLIPBOARD),
            new ChButton("W_WEB").li(LI).t("Web-Search text").i(IC_WWW),
            "-",
            new ChButton("W_HL").li(LI).t("Highlight text").i(IC_UNDERLINE),
            new ChButton("W_RMHL").li(LI).t("Remove Highlight").i(IC_CLOSE),
            "-",
            new ChButton("EXEC").li(LI).t("Run native program, script or web service ...").i(IC_BATCH),
            "For opening the control panel, click with Ctrl-key pressed",
            "-",
            Customize.newButton(Customize.webBrowser, Customize.webLinks, Customize.fileBrowsers, Customize.pdfViewer),
            new ChButton("H").t("Help on text components").li(LI)
        };
    }
    public static String[] webLinks(String id, String[] customSettings) {
        final String cc[]=customSettings, ss[]=new String[sze(cc)];
        for(int i=cc.length; --i>=0;) {
            final String c=cc[i].trim();
            if (c.indexOf('*')>0) ss[i]=rplcToStrg("*", id,c);
            else if (sze(c)>0) ss[i]=c+id;
        }
        return ss;
    }
    private final static ChTextArea _taWebLinks[]=new ChTextArea[Customize.MAX_ID+1];
    public static void showFrameWeblinks(int type, CharSequence txt, Object pSouth) {

        ChTextArea ta=_taWebLinks[type];
        if (ta==null) {
            final Object pS=
                pSouth instanceof Customize[] ? Customize.newButton((Customize[])pSouth) :
                pSouth=="B" ? Customize.newButton(type).i(null) : pSouth;
            ta=_taWebLinks[type]=new ChTextArea("");
            pcp("PNL",pnl(CNSEW,ta,null, pnl(CNSEW,null,null,null,pS)), ta);
        }
        tools(ta.t("\n"+txt+"\n")).underlineRefs(ULREFS_GO|ULREFS_NO_ICON);
        final JComponent pnl=gcp("PNL",ta,JComponent.class);
        final ChFrame w=ChFrame.frame("Links",pnl,ChFrame.ALWAYS_ON_TOP).shw(ChFrame.PACK|ChFrame.AT_CLICK|ChFrame.TO_FRONT|ChFrame.DRAG);
        inEDTms(thread_setWndwState('T',w), 222);
    }
    /* <<< Context <<< */
    /* ---------------------------------------- *
    /* >>> Frame >>> */
    public final static long FRAME_OPTS=ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN|CLOSE_CtrlW|CLOSE_DISPOSE;
    public ChTextComponents showInFrame(String t) { return showInFrame(FRAME_OPTS|ChFrame.SCROLLPANE, t); }
    public ChTextComponents showInFrame(long options,String title) {
        final JComponent jc=jc();
        if (jc!=null) {
            if (!isEDT()) {
                inEdtLater(thrdM("showInFrame", this, new Object[]{longObjct(options), title}));
                cp(KEY_AVOID_GARBAGE_COLLECT,jc);
            } else {
                cp(KEY_AVOID_GARBAGE_COLLECT,null);
                 final File f=jc instanceof ChTextView ? ((ChTextView)jc).getFile() : null;
                final String t=title!=null ? title : fPathUnix(f);
                pcp(KEY_TITLE,t,jc);
                ChFrame.frame(t,jc, options|(f!=null?(CLOSE_CtrlW|CLOSE_DISPOSE|ChFrame.SCROLLPANE):0)).shw(options);
                //ChDelay.revalidate(jc,333);
            }
        }
        return this;
    }
    /* <<< Frame <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    private static int pmid(String w) {
        final int pos=strEquls("PMID",w) ? 4 : strEquls("PUBMED",w) ? 6 : -1;
        return pos<0 ? -1 : atoi(w, pos+(chrAt(pos,w)==':' ? 1:0));
    }
    private static String _ttW, _tt;
    private static int _ttModi;
    /* <<< Tip <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
     private void he(AWTEvent ev) {
        handleEvt(this,ev);
        handleEvt(jc(),ev);
    }
    private Cursor _cursor;
    private AWTEvent _actEvt;
    private MouseEvent _mPressEv;
    public void processEv(AWTEvent ev) { pEv(ev); }
    AWTEvent pEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final JComponent jc=jc();
        final int id=ev.getID(), modi=_modifiers=modifrs(ev), iButton=buttn(ev), x=x(ev), y=y(ev), zoom=scaleUpDown(ev);
        final JTextComponent tc=tc();
        final ChTextView ascii=deref(jc,ChTextView.class);
        final ChTextArea ta=deref(jc,ChTextArea.class);
        final JTextField tf=deref(jc,JTextField.class);
        final Object contentType= jc instanceof ChTextField ? ((ChTextField)jc).getContentType() : null;
        final MouseEvent mev=deref(ev,MouseEvent.class);
        final BA ba=byteArray();
        final int end=ba.end();
        final byte[] T=ba.bytes();
        final boolean
            shift=(modi&SHIFT_MASK)!=0, ctrl=isCtrl(ev), alt=(modi&ALT_MASK)!=0,
            shortCut=isShrtCut(ev),
            isHtml=tc!=null && tc.getDocument() instanceof javax.swing.text.html.HTMLDocument;
        if (q==jc && q!=null && !shouldProcessEvt(ev)) return null;
        if (zoom!=0) {
            final double f=isHtml ? 1.05 : 1.1;
            enlargeFont(jc,_scale*= (zoom<0 ? 1/f : f));
            _mc++;
            return null;
        }
        if (tf==null) {
            final int i=id==MOUSE_PRESSED ? 1 : id==FocusEvent.FOCUS_GAINED ?0 : -1;
            if (i>=0 && null!=_associateFind[i]) _associateFind[i].setTextComponent(jc);
        }
        if (id==MOUSE_MOVED) getSelection(_region);
        if (id==MOUSE_DRAGGED && !shift) {
            if (!_doingDnd && x<0 || y<0 || x>EX+wdth(jc) || y>EX+hght(jc)) {
                final Object export=orO(_file,url(_url));
                if (export!=null) {
                    setSelection(_region[0], _region[1]);
                    _doingDnd=true;
                    dndV(true,jc).add(export);
                    exportDrg(jc, mev, TransferHandler.COPY);
                }
            }
            if (_doingDnd) return null;
        } else _doingDnd=false;
        final int kcode=keyCode(ev);
        final char kchar=keyChar(ev);
        final boolean searchDialog=q==jc && id==KEY_PRESSED && !shift && (kcode==VK_F&&shortCut || kcode==VK_F3&& ctrl);
        if (id==MOUSE_PRESSED) {
            final Object jpm=gcp("S",q);
            if (jpm instanceof JPopupMenu) inEDTms( thread_shwPopupMenu((JPopupMenu)jpm), 333);
            if (jpm instanceof File) processFile(0,  "", toStrg(jpm));
        }
        if (q==jc && jc!=null) {
            if (id==KEY_PRESSED) {
                _alreadyTyped=true;
                if (setAotEv(ev)) return null;
                _changedSinceGainedFocus=true;
                if ( (kcode==VK_V || kcode==VK_X) && shortCut) changed(null);
                if ( kcode==VK_I && shortCut && tc!=null) {
                    try {
                        tc.getDocument().insertString(maxi(0,tc.getCaretPosition()),"\t",null);
                    } catch(Exception ex){}

                }
                if (kcode==VK_ESCAPE) _escKey=true;

                final String spelling="Word lists can be added with the program option -spellcheckWords=\"urls or file paths\"<br>Start spell Check?";
                if (!ctrl && !shift && kcode==VK_F7 && !_jOrtho && tc!=null && (_jOrthoInit || ChMsg.yesNo(spelling)) ) {
                    _jOrthoInit=_jOrtho=true;
                    new ChJOrthoPROXY().register(tc);
                }
                if (!ctrl && !shift && kcode==VK_F6) {
                    for(int i=3; --i>=0;) Customize.addDialog(Customize.customize(i==0?Customize.webLinks:i==1?Customize.webBrowser:Customize.fileBrowsers));
                }
                if (kcode==VK_F1) {
                    if(shift && tc!=null) {
                        final File f=newTmpFile("");
                        wrte(f,toStrg( get(0,gcp(ChJTextPane.KEY_TEXT_CSS_JS,jc))));
                        edFile(-1, f, modi);
                    } else showHelp(ChJTextPane.class);
                    return null;
                }
                if (_undoM!=null && shortCut) {
                    final UndoManager um= undoManager(false);
                    if (um!=null) {
                        try {
                            if (kcode==VK_Y) um.redo();
                            if (kcode==VK_Z) if (shift) um.redo(); else um.undo();
                        } catch(Exception e) {}
                    }
                }
                if (((_complOptions&COMPL_TAB)!=0 && kcode==VK_TAB || (kcode==VK_ENTER && ctrl && !alt && !shift) || kcode==VK_SLASH && alt) && tc!=null) {
                    final Object cmpl=_vComplLast;
                    final JFileChooser jfs=deref(cmpl,JFileChooser.class);
                    final File currentDir;
                    {
                        File d=cmpl instanceof File ? (File) cmpl : null;
                        if (d==null && (cmpl instanceof List || cmpl instanceof Object[])) {
                            for(int i=sze(cmpl); --i>=0;) if ( (d=get(i,cmpl,File.class))!=null) break;
                        }
                        if (fExists(d)) currentDir=d;
                        else if (jfs!=null) {
                            final String t=toStrgTrim(getTxt(jc));
                            currentDir=looks(LIKE_FILEPATH_EVALVAR,t) ? null : file(jfs.getCurrentDirectory()+"/"+toStrg(t,0,prev(SLASH,t,MAX_INT,0)));
                        } else currentDir=null;
                    }
                    final int
                        caret=tc.getCaretPosition(),
                        from= 0!=(_complOptions&COMPL_ALLOW_SPC_IN_FILE) && tf!=null ? nxt(-SPC, T,0, end) : prevE(SPC, T,caret-1,-1)+1;
                    final boolean isPath=looks(LIKE_FILEPATH_EVALVAR,T,from);
                    Object ff[]=_complDirListing;
                    if (ff==null) {
                        if (currentDir!=null || isPath) {
                            final int
                                to=nxtE(SPC, T,caret, end),
                                slash=prevE(SLASH, T, to, from-1),
                                endDir=slash>=0 ? slash+1 : to;
                            final File dir=isPath ? file(ba.newString(from,endDir)) : currentDir;
                            if (isDir(dir)) {
                                ff=_complDirListing= _complFileFilter!=null ? dir.listFiles(_complFileFilter) : lstDirF(dir);
                                sortArry(ff, comparator(COMPARE_ALPHABET));
                            }
                        }
                    }

                    final long ignCase= _complOptions&COMPL_IC;
                    final boolean notOnlyDir=(!isPath || sze(_complDirListing)==0) &&
                        !( (cmpl instanceof File || jfs!=null || cmpl==null) && _vComplFirst==null && nxt(SLASH,ba)>=0);
                    if (!(completeRecursive(_complDirListing, 0, isWin() ? (COMPL_INFER_CASE|COMPL_IC) : 0)  ||
                          notOnlyDir && (
                                         completeRecursive(_vComplFirst,2, ignCase) ||
                                         (tf==null && completeRecursive(ba,3, ignCase)) ||
                                         _vComplFirst!=cmpl && completeRecursive(cmpl,4,ignCase) ||
                                         completeRecursive(ChEnv.getNames(ChEnv.WITH_DOLLAR),5, 0)  )
                          )
                        ) complClear();
                    return null;
                } else if (kcode!=VK_SPACE) complClear();
                if (ctrl)  {
                    switch(kcode) {
                    case VK_P:
                        final String clazz=gcps(KEY_CLASS_NAME, jc);
                        File fOut=clazz!=null ? HtmlDoc.main1(clazz) : null;
                        if (jc instanceof ChTextView) fOut=((ChTextView)jc).getFile();
                        if (fOut==null) {
                            fOut=newTmpFile(isHtml ? ".html":".txt");
                            if (isHtml) {
                                final Object t_css_js=gcp(ChJTextPane.KEY_TEXT_CSS_JS,jc);
                                final BA txt=toBA(get(0,t_css_js));
                                if (txt==null) {assrt(); return null;}
                                txt.replace(' '|STRPLC_FILL_RIGHT,"<CAPTION><BR>","<CAPTION>");
                                final int posCSS=atoi(get(1,t_css_js)), posJS=atoi(get(2,t_css_js));
                                if (posJS >0) txt.insert(posJS,  HtmlDoc.cssOrJS('J'));
                                if (posCSS>0) txt.insert(posCSS, HtmlDoc.cssOrJS('C'));
                                HtmlDoc.process(txt, DOCID_TMP, DOCTYPE_HTML, _javaPackages);
                                HtmlDoc.rplcWIKI(txt);
                                addHtmlTags(txt);
                                wrte(fOut, txt);
                            } else wrte(fOut, ba);
                        }
                        if (sze(fOut)>0) {
                            if (isHtml) inEDTms(thrdRRR(threadWritePng(), thread_visitURL(fOut, 0)), 555);
                            else edFile(-1, fOut, 0);
                        }
                        return null;
                    case VK_U:
                        if (jc!=null) jc.updateUI();
                        if (ascii!=null && ascii.getFile()!=null) ascii.setFile(ascii.getFile());
                        underlineRefs(0);
                        return null;
                    }
                }
                if (ctrl && kcode==VK_L || !ctrl && kcode==VK_F10) {
                    if (ta!=null) ta.setLineWrap(!ta.getLineWrap());
                    else {
                        pcp(KOPT_TRACKS_VIEWPORT_WIDTH, gcp(KOPT_TRACKS_VIEWPORT_WIDTH,jc)!=null ? null: "", jc);
                        revalAndRepaintC(jc);
                    }
                }
            }
            {
                if (id==MOUSE_PRESSED && shift && !ctrl) {
                    _mPressEv=mev;
                    return null;
                }
                final MouseEvent le=_mPressEv;
                _mPressEv=null;
                if (id==MOUSE_RELEASED && shift && le!=null) return le;
            }
            //if (id==MOUSE_MOVED)  { evLstnr(MOMOLI_DRAG_WITH_ALT).mouseMoved(mev); } !!!!!!!!!!!!!!!!!!!!!!!!!!11
            final int iP=x>=0 && y>=0 ?  viewToModel(setXY(x,y,POINTv),jc) : -1;
            final boolean isNewCharacter;
            if (id==MOUSE_MOVED || id==MOUSE_PRESSED || id==MOUSE_CLICKED) {
                modelToView(iP+1,jc,RECTf);
                final int pos=RECTf.height==0 || x(RECTf)<x || y>y2(RECTf) ? -1 : iP;

                if (isNewCharacter=_mouseTextPos!=pos) setMousePosition(pos);
            } else isNewCharacter=false;
            final String w=_word;

            final int colon=strchr(':',w);
            if ((id==MOUSE_MOVED || id==MOUSE_PRESSED || id==MOUSE_CLICKED) && isNewCharacter && (_uRefsOptions&ULREFS_NOT_CLICKABLE)==0 && !_escKey) {
                if (w!=null && (_uRefsOptions&ULREFS_PMID_DOWNLOAD)!=0 && !shift && !(w.equals(_mMovedWord)&&modi==_mMovedModi)) {
                    final String d= colon<0 && guessDbFromId(w)=="UNIPROT:" ? "UNIPROT:"+w : w;
                    final boolean isPM=pmid(d)>0;
                    if (isPM || d.startsWith("SWISS:") || d.startsWith("UNIPROT") || d.startsWith("ENSG")) {
                        final File f=ChPubmed.maybeDownload(d);
                        if (f!=null) {
                            final ChTextComponents t=_taForAbstract;
                            if (t!=null) ChPubmed.showAbstract(f,t, (modi&ALT_MASK)!=0 ? ChPubmed.SHOWABSTRACT_FULL_TEXT : 0L);
                        }
                        if (isPM && d.indexOf('{')<0) ChPubmed.queueForDownload(d,true);
                    }
                }
                _mMovedWord=w;
                _mMovedModi=modi;
            }
            if (isPopupTrggr(true,ev) || id==MOUSE_CLICKED && null!=fullClassNam(true, _class, null)) {
                _textForContextMenu=w;
                if (pmid(w)>0) ChPubmed.showPubmedPopup(w);
                else {
                    JPopupMenu menu=_contextMenu;
                    if (menu==null) {
                        menu=_contextMenu=jPopupMenu(0, "Clicked", newContextMenuObjects("This menu acts on a selected text region or on the word under the mouse pointer"));
                    }
                    for(Object c : menu.getComponents()) if (gcp("S", c)!=null) rmFromParent(c);
                    final ChRunnable hasCM=gcp(PROVIDE_JPopupMenu, jc, ChRunnable.class);

                    final JPopupMenu
                        special=_class!=null ? ChJavadoc.menu(_class,(String[])null) :
                        !shift&&!alt&&!ctrl ? (JPopupMenu) runCR(hasCM!=null?hasCM:_ctxtProvider,PROVIDE_JPopupMenu,ev) : null;

                    if (special!=null && !isPopupTrggr(true,ev) && id==MOUSE_CLICKED) menu=special;
                    else {
                        int count=0;
                        if (special!=null) {
                            final Icon icn=dIcon(special);
                            final Component itm=new JMenuItem(orS(getTxt(special),"Special"), icn==null?iicon(IC_BLANK) : icn);
                            LI.addTo("m", pcpReturn("S", special, itm));
                            menu.add(itm,0);
                            count++;
                        }
                        if (fExists(_file)) {
                            final Component itm=new JMenuItem("Process File ...", iicon(IC_BLANK));
                            LI.addTo("m", pcpReturn("S", _file, itm));
                            menu.add(itm,0);
                            count++;
                        }
                        if (count>=0) menu.add((Component)pcpReturn("S","",new JSeparator()) ,count);
                    }
                    shwPopupMenu(menu);
                }
                return null;
            }
            if (id==MOUSE_CLICKED && !isPopupTrggr(false,ev)) {
                _textForContextMenu=w;
                if ((_uRefsOptions&ULREFS_NOT_CLICKABLE)==0) {
                    final String urlMouse0=_url, idMouse=_id, dbE=_protDBColonId;
                    final int pmidStart=strstr(STRSTR_AFTER,PDF4PMID,urlMouse0), iPmid=atoi(urlMouse0, pmidStart);
                    final String urlMouse=iPmid>0 ? rplcToStrg(".pdf+html",".pdf",delLstCmpnt(urlMouse0,'&')) : urlMouse0;
                    final boolean
                        dbClicked=urlMouse!=null && dbE!=null && (iPmid<=0 || gcp(ACTION_DATABASE_CLICKED,jc)!=null && pmid(dbE)<=0),
                        isAfterColon=colon>0 && iP-range().from()>colon,
                        isPDB=strEquls("PDB:",idMouse);
                    final int db=
                        idMouse==null || isAfterColon ? 0 :
                        isPDB ? Customize.pdbLinks :
                        idMouse.startsWith("UNIPROT:") || guessDbFromId(idMouse)=="UNIPROT:" ? Customize.uniprotLinks :
                        looks(LIKE_EC,idMouse) ? Customize.ecClassLinks : 0;
                    if (db!=0 && !shift && !ctrl && !alt) {
                        String id_noDB=delPfx("PDB:",delPfx("UNIPROT:",idMouse));
                        if (isPDB) id_noDB=delLstCmpnt(delLstCmpnt(id_noDB,':'),'_');
                        showFrameWeblinks(db, new BA(99).join(webLinks(id_noDB, custSettings(db))),"B");
                    } else if (_plugin!=null) {
                        InteractiveDownload.downloadFilesAndUnzip(new java.net.URL[]{url(_plugin)},dirPlugins());
                        he(new ActionEvent(this,ActionEvent.ACTION_PERFORMED, ACTION_DOWNLOAD_PLUGIN,0));
                    } else if (urlMouse!=null) {
                        final BA log=ctrl?new BA(99):null;
                        if (dbClicked){
                            final String command=ACTION_DATABASE_CLICKED+delSfx(',',delSfx('.',dbE));
                            handleEvt(_vLiDb, new ActionEvent(orO(jc,this), ActionEvent.ACTION_PERFORMED,command,0));
                        } else if (idxOfStrg(urlMouse, custSettings(Customize.testProxySelector))>=0) {
                            Web.testConnectionForURL(urlMouse);
                        } else if (Web.localhost_sendData(urlMouse,log)) {
                            shwTxtInW(FRAME_OPTS|ChFrame.ALWAYS_ON_TOP, "localhost", log);
                        } else if (!shift) {
                            processFile(0, w, delSfx('.',urlMouse));
                            ChPubmed.maybeAssociatePdf(iPmid, false);
                        }
                    } else {
                        for(ChTextArea taL : _taWebLinks) if (jc!=null && jc==taL) ChFrame.frame("",taL,0).setVisible(false);
                    }
                    if (!shift) {
                        final File f=_file;
                        if (f!=null&&w!=null) {
                            boolean success=false;
                            int viewR=-1;
                            for(String V : FILE_RANGE_SUFX) {
                                if (viewR<0) viewR=strstr(STRSTR_AFTER,V,w);
                            }
                            if (viewR>0) {
                                final int from=atoi(w,viewR), to=mini(atoi(w, w.indexOf('-',viewR)+1), sze(f));
                                if (from>0 && to>from) {
                                    InputStream is=null;
                                    try {
                                        (is=new FileInputStream(f)).skip(from);
                                        final byte[] buf=new byte[to-from];
                                        final BA txt=new BA(buf,0,is.read(buf));
                                        if (sze(txt)>0) {
                                            new ChTextView(txt).tools().showInFrame(fPathUnix(f));
                                            success=true;
                                        }
                                    } catch(IOException ex) { stckTrc(ex); }
                                    closeStrm(is);
                                }
                            }
                            if (!success && fExists(f) && looks(LIKE_FILEPATH,w)) processFile(0, "", toStrg(f));
                        }
                    }
                    if (iButton==2)  changed(null);
                }
            }
            if (id==MOUSE_PRESSED || searchDialog) {
                int count=0;
                for(Map.Entry<DialogFindInText,Object> e : entryArry(DialogFindInText.getMap())) {
                    final DialogFindInText d=e.getKey();
                    final Object v=deref(e.getValue());
                    if (v==null || d==null) DialogFindInText.getMap().entrySet().remove(e);
                    else if (v==jc) {
                        if (id==MOUSE_PRESSED) d.setPosition(iP);
                        if (searchDialog) {
                            d.showInFrame(count==0?ChFrame.AT_CLICK:ChFrame.STAGGER);
                            count++;
                        }
                    }
                }
                if (searchDialog && count==0) new DialogFindInText().setTextComponent(jc).showInFrame(ChFrame.AT_CLICK);
                if (iButton==2) inEDTms(run('M'), 333);
            }
            if (id==MOUSE_ENTERED && q==jc) {
                _escKey=false;
                if (0!=(_uRefsOptions&ULREFS_DISABLE_UNTIL_MOUSE) && !_hlAuto) { _hlAuto=true; run('h').run(); }
                if (_hlMouseEnter) run('h').run();
            }
            if (id==FocusEvent.FOCUS_GAINED) _changedSinceGainedFocus=false;
            if (id==FocusEvent.FOCUS_LOST || id==MOUSE_EXITED || (tf!=null && id==ActionEvent.ACTION_PERFORMED) ) {
                if (_saveNeeded)  {
                    if (_saveLoaded) saveInFile(true);
                    if (contentType==KEY_EMAIL) setPrpty(ChUtils.class, KEY_EMAIL, jc.toString());
                }
                if (gcp(KEY_IF_EMPTY,jc)!=null) jc.repaint();
            }
            final String act=
                _alreadyTyped && id==FocusEvent.FOCUS_LOST && _changedSinceGainedFocus ? ACTION_FOCUS_LOST :
                gcp(ACTION_FOCUS_GAINED,jc)!=null && id==FocusEvent.FOCUS_GAINED ? ACTION_FOCUS_GAINED :
                tf!=null && id==KEY_PRESSED && kchar=='\n' ? ACTION_ENTER : null;
            if (act!=null && System.currentTimeMillis()-_timeEnter>99)  {
                he(new ActionEvent(jc, ActionEvent.ACTION_PERFORMED,act,0));
                _timeEnter=System.currentTimeMillis();
            }
            if (act!=null) {
                if (contentType==Integer.class) setTextTS(toStrg(atoi(tf)));
                if (contentType==Float.class)   setTextTS(toStrg(atof(tf)));
            }
            if ((id==MOUSE_MOVED || id==KEY_PRESSED || id==KEY_RELEASED) && _cursor!=getCursor()) jc.setCursor(_cursor=getCursor());
            if ((id==MOUSE_PRESSED || id==MOUSE_CLICKED) && tc!=null) tc.getCaret().setVisible(true);
        }
        if (id==ActionEvent.ACTION_PERFORMED) {
            final int iB=idxOf(q,_ofB);
            final boolean tfChanged=q==_ofTf && cmd==ACTION_TEXT_CHANGED;
            final String path=iB<0&&!tfChanged?null:toStrgTrim(getTxt(_ofTf));
            if (sze(path)>0) {
                final File f=file(path);
                if (f!=null) {
                    if (iB==OFopenFile) viewFile(f, modi);
                    if (iB==OFopenDir) viewFile(drctry(f), modi);
                    if (iB==OFmenu) shwPopupMenu(FilePopup.instance().menu(f));
                }
                _ofDnD();
                if (iB==OFU) visitURL(path, modi);
                if (iB==OFu) visitURL(delLstCmpnt(path,'/'), modi);
                if (iB==OFpdf) ChPubmed.showPubmedPopup(gcps("W", q));
                if (iB>=0 && iB!=OFmenu && iB!=OFpdf && isSelctd(_ofClose)) closeW(0,q);
            }

            if (cmd=="H") showHelp(ChJTextPane.class);
            if (cmd==ChPubmed.ACTION_FULL_TEXT_ASSOCIATED) run('H').run();
            if (cmd=="W_WEB" || cmd=="W_HL" || cmd=="W_RMHL" || cmd=="W_CPY" || cmd=="EXEC" || kcode==VK_R && shortCut) {
                final Range r=new Range();
                final String wrd=getSelection(r)  && r.from()<=_mouseTextPos && _mouseTextPos<r.to() ?
                    byteArray().newString(r.from(), r.to()) : _textForContextMenu;
                if (wrd!=null) {
                    if (cmd=="EXEC" || kcode==VK_R && shortCut) {
                        ChFrame.frame("Execute shell script",ExecByRegex.panel(Customize.scriptByRegex, wrd),ChFrame.PACK|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.TO_FRONT|ChFrame.AT_CLICK).i(IC_BATCH);
                    }
                    if (cmd=="W_CPY") toClipbd(wrd);
                    if (cmd=="W_WEB") showFrameWeblinks(Customize.webSearches, new BA(99).join(webLinks(wrd, custSettings(Customize.webSearches))),"B");
                    if (cmd=="W_RMHL" || cmd=="W_HL") {
                        for(TextMatches h:toArry(HIGHLIGHTS[1], TextMatches.class)) {
                            if (wrd.equals(h.needle())) { removeHighlight(h); jc.repaint();}
                        }
                        if (cmd=="W_HL") highlightOccurrence(wrd,null,null,HIGHLIGHT_REPAINT|HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(0xFF00ff));
                    }
                }
            }
            if (cmd==CMD_CLEAR) {
                setTxt("",tc);
                final BA text=jc instanceof ChTextView ? ((ChTextView)jc).byteArray() : null;
                if (text!=null) { text.clr(); jc.repaint(); }
            }
            if (cmd=="SAVE_AND_EDIT") {
                if (ascii!=null) {
                    final File f=ascii.getFile();
                    if (f!=null) edFile(-1, f, modi);
                } else {
                    File f=file(gcp("F",q));
                    if (f==null) f=newTmpFile("");
                    wrte(f,byteArray());
                    edFile(-1, f, modi);
                }
            }
        }
        {
            final BA append=gcp(BA.KEY_SEND_TO, jc(), BA.class);
            if (append!=null) append.send();
        }
        return searchDialog ? null : ctrl2apple(ev);
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Run >>> */
    private ChDelay _run[];
    public ChDelay run(char type) {
        if (_run==null) _run=new ChDelay['z'];
        if (_run[type]==null) _run[type]=(ChDelay)thrdCR(this,("RUN_"+type).intern(),null, new long[ARRAY_DURATION]);
        return _run[type];
    }
    private long _timeEnter;
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        final JComponent jC=jc();
        if (id==TT) {
            final MouseEvent ev=(MouseEvent)arg;
            final int modi=modifrs(ev);
            final String w=_word;
            if (_ttW!=w || _ttModi!=modi) {
                _ttW=w;
                _ttModi=modi;
                CharSequence tt=null;
                final int pmid=pmid(w);
                if (strEquls("MOVIE:",w)) tt="Open movie in browser";
                if (strEquls("Image:",w)) tt="Open image in browser";
                else if (pmid>0) {
                    tt=
                        orS(ChPubmed.pubmedTitle(pmid),"")+
                        ("<br>Left-Click: Abstract in Browser<br>Right-click: PDF<br>"+
                         "Colors: blue indicates presence of Abstract, red presence of PDF<br>")+
                        ((_uRefsOptions&ULREFS_PMID_DOWNLOAD)!=0?"Alt+mouse-hover: view PDF as plain text" : "");
                }
                else if (_url!=null) tt=_url;
                else tt=ballonMsg(ev, null);
                if (tt==null && w!=null) {
                    if (w.startsWith("$CTIME=")||w.startsWith("$MTIME=")) {
                        final long t=atol(w,7,99);
                        tt=day(t<MAX_INT ?t*1000 :t);
                    }
                }
                if (tt==null) {
                    final HelpCommands hc=gcp(HelpCommands.class, jc(), HelpCommands.class);
                    if (hc!=null) tt=hc.tt(w, 0!=(modi&SHIFT_MASK));
                }
                _tt=addHtmlTagsAsStrg(tt);
            }
            return "null".equals(_tt) ? null : _tt;
        }
        if (id=="ADD_OPT2H") {
            final TextMatches h=(TextMatches)arg;
            removeHighlight(h);
            h.setOptions(h.options()|HIGHLIGHT_REMOVE_IF_TEXT_CHANGES);
            addHighlight(h);
        }
        if (id=="underlineRefs") underlineRefs(atol(arg));
        if (id=="getTextAsSegment") ((Segment[])arg)[0]=getTextAsSegment();
        if (id=="getText") ((String[])arg)[0]=getTxt(jC);
        if (id=="setTextTS") setTextTS(toStrg(argv[0]),argv[1]!=null);
        if (id=="RUN_U") underlineRefs(0);
        if (id=="RUN_R") {
            final ChTextReplacement rplcMnt[]=_replacements;
            final BA ba=byteArray();
            if (sze(rplcMnt)>0 && sze(ba)>0) {
                final int mc=ba.mc();
                for(ChTextReplacement r : rplcMnt) ba.replace(r);
                if (mc!=ba.mc())  setTextTS(ba,true);
            }
        }
        if (jC!=null) {
            if (id=="RUN_H" || id=="RUN_h") {
                clr(HIGHLIGHTS[0]);
                if ( (_uRefsOptions&ULREFS_GO)==0) underlineRefs(0);
                for(List<TextMatches> vH : HIGHLIGHTS) {
                    for(int iH=sze(vH);--iH>=0;) {
                        final TextMatches h=vH.get(iH);
                        if (h!=null && h.needle()!=null) {
                            if ((h.options()&HIGHLIGHT_UPDATE_IF_TEXT_CHANGES)!=0) {
                                hlo(h.needle(),h.delimiterL(),h.delimiterR(), h.options(), h.color(),h);
                            }
                        }
                    }
                }
                if ((_uRefsOptions&ULREFS_WEB_COLORS)!=0) highlightRGB();
                if (_RECT.height>0) jC.repaint(_RECT);
                _RECT.setBounds(0,0,0,0);
                if (id=="RUN_H") jc().repaint();
            }

            if (id=="RUN_M") he(new ActionEvent(jC,ActionEvent.ACTION_PERFORMED, CMD_MIDDLE_MOUSE_BUTTON,0));
            if (id=="RUN_I") he(new ActionEvent(jC,ActionEvent.ACTION_PERFORMED, ACTION_INSERTED_TEXT_LONGER_ONE,0));
            if (id=="RUN_C") he(_actEvt==null ? _actEvt=new java.awt.event.ActionEvent(jC,ActionEvent.ACTION_PERFORMED, ACTION_TEXT_CHANGED,0) : _actEvt);
        }
        return null;
    }
    /* <<< Run <<< */
    /* ---------------------------------------- */
    /* >>> Buttons >>> */
    public ChButton newClearButton() { return new ChButton(CMD_CLEAR).t("Clear").li(LI);}
    public ChButton butSave(File f) { return new ChButton("SAVE_AND_EDIT").t("save").li(LI).cp("F",f); }
    /* <<< Buttons <<< */
    /* ---------------------------------------- */
    /* >>> References >>> */
    private static int[] _sharedFromTo=new int[1000], _sharedStyle=new int[500];
    private static byte[] _lookupTable[][];
    private static int _lookupTableMC;
    private long _uRefsOptions;
    private static boolean[] refsDelimL, refsDelimR;
    public ChTextComponents underlineRefs(long mode) {
        if (!isEDT()) {
            inEdtLaterCR(this,"underlineRefs",longObjct(mode));
            return this;
        }
        if (refsDelimL==null) refsDelimL=chrClas("\00"+" \t\n\r>,(){}[]\"");
        if (refsDelimR==null) refsDelimR=chrClas("\00"+" \t\n\r>,(){}[]\"");
        _alreadUlRefs=true;
        _uRefsOptions|=mode;
        if ( (mode&ULREFS_WEB_COLORS)!=0)  highlightRGB();
        if ( (mode&ULREFS_DISABLE_UNTIL_MOUSE)!=0) { _hlAuto=false; return this; }
        if ( (mode&ULREFS_BG)!=0) { _alreadUlRefs=true;  startThrd(thrdCR(this,"RUN_U")); return this;}
        else if ( (mode==0 || (mode&(ULREFS_GO|ULREFS_NOT_CLICKABLE))!=0) && 0==(_uRefsOptions&ULREFS_NEVER)) {
            final JComponent jc=jc();
            final int STYLE_PDF=_imageRefsS.length, STYLE_ABSTRACT=STYLE_PDF+1, STYLE_FILE=STYLE_PDF+2, STYLE_REF=STYLE_PDF+3, MAX_STYLE=STYLE_REF;
            final int transparentDependingOnJC=jc instanceof ChJTextPane ? 44 : 255;
            final int found[]={-1,-1};
            final BA ba=byteArray();
            final byte T[]=ba!=null ? ba.bytes() : null;
            final int E=T!=null ? mini(T.length,ba.end()) : 0;
            if (E==0) return this;
            final boolean debug=sze(ba)>333*1000;
            final int mc=Hyperrefs.mc();
            byte[][] lookup[]=_lookupTable;
            if (lookup==null || _lookupTableMC!=mc) {
                lookup=OccurInText.addToLookupTable(Hyperrefs.getKeysToHighlight(),null);
                _lookupTable=lookup=OccurInText.addToLookupTable(_imageRefsS, lookup);
                if (isWin()) lookup=OccurInText.addToLookupTable("C: D: E: c: d: e:".split(" "),lookup);
                _lookupTableMC=mc;
            }
            int fromTo[]=_sharedFromTo, style[]=_sharedStyle, count=0, pos=ba.begin()-1;
            while( pos<E-1 && (pos=OccurInText.idxOfNeedles(lookup, refsDelimL, T, pos+1, E, found)) >=0 ) {
                final byte hit[]=lookup[found[0]][found[1]];
                if (hit==null || hit.length==0) { assrt(); continue; }
                final byte h0=hit[0], h1=hit.length>1 ? hit[1] : 0;
                final boolean isEC;
                if ('1'<=h0 && h0<='9') {
                    if (pos>E-6) continue;
                    final byte p1=T[pos+1], p2=T[pos+2];
                    isEC=('0'<=p1 && p1<='9' && p2=='.' || p1=='.' && '0'<=p2 && p2<='9' ) && looks(LIKE_EC,T,pos,E);
                    if (!isEC) continue;
                } else isEC=false;

                int stle=-1;
                final boolean
                    isMovie=h0=='M' && strEquls("MOVIE:",T,pos),
                    isImage=h0=='I' && strEquls("Image:",T,pos),
                    looksLikeURL=(h0=='f' || h0=='h') && looks(LIKE_EXTURL,T,pos),
                    looksLikeFile=(h1==':' || h0=='/' || h0=='\\' || h0=='~' || h0=='f'&&h1=='i') && looks(LIKE_FILEPATH_EVALVAR,T,pos),
                    isHyperref=Hyperrefs.isHyperref(T,pos,E);
                if (isHyperref || looksLikeFile || looksLikeURL || isEC || isMovie || isImage) {
                    int toIdx=pos+hit.length;
                    while(toIdx<E && !refsDelimR[127&T[toIdx]]) toIdx++;
                    if (looksLikeFile) {
                        final int toFile=fileEndsAt(T,pos,toIdx);
                        if (toFile>0) stle=STYLE_FILE;
                        else continue;
                    }
                    if (stle<0) {
                        if (hit[hit.length-1]=='{') {
                            toIdx--;
                            while(toIdx+1<E && T[toIdx+1]!='}' && T[toIdx+1]!='\n') toIdx++;
                        }
                        stle=idxOfStrg(isMovie? MOVIE : hit, _imageRefsS,0, _imageRefsStrLen.length, _imageRefsStrLen);
                        if (stle>=0 && !isEC && !looksLikeFile) {
                            final int
                                numStart,
                                pmid=
                                E>pos+6 && hit.length==4 && h0=='P'&&h1=='M'&&hit[2]=='I'&&hit[3]=='D' &&
                                is(DIGT,T,numStart=pos+ (T[pos+4]==':'?5:4)) ? atoi(T,numStart,E) : 0;
                            if (pmid>0) {
                                final String sPmid=toStrg(pmid);
                                stle=
                                    ChPubmed.hasPdf(sPmid) ? STYLE_PDF :
                                    sze(ChPubmed.getFile(sPmid, ChPubmed.ABSTRACT))>0 ? STYLE_ABSTRACT : stle;
                            }
                            toIdx=pos+hit.length;
                        } else stle=STYLE_REF;
                    }
                    if (count+1>=fromTo.length) {
                        _sharedStyle=style=chSze(style, (count+1000)/2);
                        _sharedFromTo=fromTo=chSze(fromTo, count+1000);
                    }
                    style[count/2]=stle;
                    fromTo[count++]=pos;
                    fromTo[count++]=toIdx;
                }
            }
            if (count>0) {

                for(int iStyle= MAX_STYLE+1; --iStyle>=0;) {
                    int cnt=0;
                    for(int i=count/2; --i>=0;) if (style[i]==iStyle) cnt++;
                    final int[] ft=new int[2*cnt];
                    cnt=0;
                    for(int i=0; i<count; i++) {
                        if (style[i/2]==iStyle) ft[cnt++]=fromTo[i];
                    }

                    if (cnt>0) {

                        final long hlStyle=
                            iStyle==STYLE_FILE || iStyle==STYLE_REF ? HIGHLIGHT_STYLE_UL :
                            0;
                        final Object color=
                            iStyle==STYLE_FILE || iStyle==STYLE_REF ? null  :
                            iStyle==STYLE_PDF ? C(0xff0000,transparentDependingOnJC) :
                            iStyle==STYLE_ABSTRACT ? C(0x8888ff,transparentDependingOnJC) :
                            (String)get(iStyle,_imageRefsI),
                            color1=color!=null ? color : C((_uRefsOptions&ULREFS_NOT_CLICKABLE)!=0 ? COLOR_REF_GRAY : COLOR_REF);
                        addHighlight(new TextMatches(HIGHLIGHT_NOT_IN_SCROLLBAR|HIGHLIGHT_REMOVE_IF_TEXT_CHANGES|hlStyle, ft, count/2, color1));

                    }
                }
            }
            if ( (mode&HIGHLIGHT_REPAINT)!=0) { ChDelay.repaint(jc,99); ChDelay.repaint(getSB('V',jc),99); }
        }
        return this;
    }
    /* <<< References <<< */
    /* ---------------------------------------- */
    /* >>> Scroll >>> */
    public static Runnable thread_setScrollPositionV(int pos, Component jc) {
        return thrdM("setScrollPositionV",ChTextComponents.class, new Object[]{intObjct(pos),jc});
    }
    public static void setScrollPositionV(int pos, Component jc) {
        final JScrollBar sb=getSB('V',jc);
        if (sb!=null) {
            try {
                if (jc instanceof JTextComponent) ((JTextComponent)jc).setCaretPosition(pos);
            }catch(Exception ex){}
            final Rectangle r=new Rectangle();
            modelToView(pos, jc, r);
            ChScrollBar.lock(0, sb);
            ((JComponent)jc).scrollRectToVisible(r);
        }
    }
    /* <<< Scroll <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private boolean _wasEmpty;
    public void repaintLater(int delay) {
        final JComponent jC=jc(), vSB=getSB('V',jC);
        if (delay==0) {
            if (jC!=null) jC.repaint();
            if (vSB!=null) vSB.repaint();
        } else {
            ChDelay.repaint(jC,delay);
            ChDelay.repaint(vSB,delay);
        }
    }
    public boolean isEmpty() { return nxt(-SPC, byteArray())<0; }
    final static Rectangle clipr=new Rectangle();
    public boolean paintHook(JComponent component, Graphics g, boolean after) {
        if (!_jOrtho) antiAliasing(g);
        final JComponent jc=jc();
        final ChScrollBar vSB=deref(getSB('V',jc), ChScrollBar.class);
        if (jc==null) return false;
        if (!after && component==vSB) {
            paintScrollbar:
            for(List<TextMatches> vH : HIGHLIGHTS) {
                for(int iH=sze(vH);--iH>=0;) {
                    final TextMatches h=vH.get(iH);
                    if (h!=null && h.yy(jc).length>0 && (h.options()&HIGHLIGHT_NOT_IN_SCROLLBAR)==0) {
                        vSB.setHasSelection(true);
                        break paintScrollbar;
                    }
                }
            }
        }
        if (after && component==vSB) {
            Rectangle track=null;
            final int jcHeight=hght(jc);
            for(List<TextMatches> vH : HIGHLIGHTS) {
                for(int iH=sze(vH);--iH>=0;) {
                    final TextMatches h=vH.get(iH);
                    if (h==null || (h.options()&HIGHLIGHT_NOT_IN_SCROLLBAR)!=0) continue;
                    g.setColor(h.color());
                    final int[] yy=h.yy(jc);
                    for(int i=yy.length; --i>=0;) {
                        if (track==null) track=vSB.getTrack();
                        final int y=y(track)+yy[i]*track.height/jcHeight;
                        fillBigRect(g, 1,y,track.width-3,2);
                    }
                }
            }
        }
        if (component==jc) {
            final Rectangle clip=g.getClipBounds(clipr);
            final int
                pos1=boundsToPos(true,clip),
                pos2=boundsToPos(false,clip),
                w=wdth(jc),
                h=hght(jc);
            if (!after) {
                loadSavedText();
                if (!paintHooks(jc, g, false)) return false;
                if (jc.isOpaque()) {
                    g.setColor(jc.getBackground());
                    fillBigRect(g,0,0,w,h);
                }
                if (!(jc instanceof JTextComponent)) paintHighlights(pos1-1,pos2,false,g, clip);
            } else {
                final String txtEmpty=toStrg(gcpa(KEY_IF_EMPTY,jc));
                final boolean isE=txtEmpty!=null && isEmpty() && !jc.hasFocus();
                if (_wasEmpty !=isE) jc.repaint();
                _wasEmpty=isE;
                if (isE) {
                    g.setClip(0,0,w, h);
                    g.setColor(jc.getBackground());
                    fillBigRect(g, 0,0,w,h);
                    g.setColor(C(0x888888));
                    drawMsg(0L,splitStrg(txtEmpty,'\n'), jc, g,  y(jc.getInsets()));
                } else {
                    paintHighlights(pos1-1,pos2,true,g,clip);
                    paintTabs(pos1-1,pos2, g);
                    paintHooks(jc, g, true);
                    if (!_alreadUlRefs && _hlAuto) {
                        _alreadUlRefs=true;
                        _RECT.setBounds(0,0,w, h);
                        inEDTms(run('h'),333);
                    }
                    if (gcp(KOPT_F1_HELP, jc)!=null) {
                        final int prefH=prefH(jc);
                        if (h-prefH>EX*3/2) {

                            g.setFont(getFnt(12, false, 0));
                            g.setColor(C(0x4455FF));
                            g.drawString("F1 Help", w-7*EM,h-EX);
                        }
                    }
                }
            }
            _painted=true;
        }
        return true;
    }
    private void shiftHighlights(int from, int len) {
        for(List<TextMatches> vH : HIGHLIGHTS) {
            for(int iH=sze(vH);--iH>=0;) {
                final TextMatches h=vH.get(iH);
                final int[] fromTo=h!=null ? h.fromToTrim() : null;
                if (fromTo==null || fromTo.length==0) continue;
                final int binSearch=Arrays.binarySearch(fromTo,from);
                final int insPoint=binSearch<0 ? -(binSearch+1) : binSearch;
                for(int i=insPoint; i<fromTo.length; i++)  fromTo[i]+=len;
            }
        }
    }

    void paintTabs(int from, int to, Graphics g) {
        final JTextComponent tc=tc();
        final BA ba=byteArray();
        if (tc==null || sze(ba)==0) return;
        final byte T[]=ba.bytes();
        final int t=mini(to, ba.end(), T.length), charH=charH(g)+1, charA=charA(g);
        g.setColor(C(0xFF));
        for(int i=maxi(0,from); i<t; i++) {
            if (T[i]=='\t') {
                Rectangle r=null;
                try { r=tc.modelToView(i); } catch(Exception ex){}
                if (r!=null) g.drawString("\u00bb", EX/2+r.x, r.y+charA+ (r.height-charH)/2);
            }
        }
    }

    void paintHighlights(int from, int to, boolean after, Graphics g, Rectangle clip) {
        final JComponent jc=jc();
        final int
            clipX0=clip!=null ? x(clip) : 0,
            clipY0=clip!=null ? y(clip) : 0,
            clipX1=clip!=null ? clipX0+clip.width : MAX_INT,
            charW=charW(jc()),
            charA=charA(jc());
        final boolean
            isEditableFocus=tc()!=null && tc().isEditable() && jc.hasFocus(),
            isHtml=tc()!=null && tc().getDocument() instanceof javax.swing.text.html.HTMLDocument;
        int fstPxlTilde=-1;
        for(List<TextMatches> vH : HIGHLIGHTS) {
            for(int iH=sze(vH);--iH>=0;) {
                final TextMatches h=vH.get(iH);
                if (h==null) continue;
                final long style=h.options()&HIGHLIGHT_STYLE_MASK, opts=_uRefsOptions;
                final int[] fromTo=h.fromToTrim();
                int fromToL=0;
                if (fromTo!=null) for(int i=0; i<fromTo.length; i++) if (fromTo[i]>=0) fromToL=i;
                if (fromToL==0) continue;
                final Object c_or_i=h.colorOrIcon();
                final ImageIcon ic=c_or_i instanceof ImageIcon && (opts&ULREFS_NO_ICON)==0  ? (ImageIcon)c_or_i : null;
                final Image im=img(ic);
                final Color col=c_or_i instanceof Color ? (Color)c_or_i :  C((opts&ULREFS_NOT_CLICKABLE)!=0?COLOR_REF_GRAY:COLOR_REF);
                if (after!=(im!=null || style==HIGHLIGHT_STYLE_WAVE || style==HIGHLIGHT_STYLE_UL)) continue;
                final int binSearch=Arrays.binarySearch(fromTo,from), insPoint=binSearch<0 ? -(binSearch+1) : binSearch;
                final boolean ul=style==HIGHLIGHT_STYLE_UL || im!=null && isEditableFocus;
                for(int i=insPoint&~1; i<fromToL; i+=2) {
                    final int iFrom=fromTo[i], iTo=fromTo[i+1];
                    if (iFrom>to) break;
                    final Rectangle r0=RECTf, r1=RECTt;
                    modelToView(iFrom,jc,r0);
                    modelToView(iTo-1,jc,r1);
                    final int
                        r1Width=r1.width, w1=r1Width<2 ? charW : r1Width,
                        y0= y(r0),  x0=x(r0),  x1=x(r1), y1=y(r1), w0=r0.width, h0=r0.height, h1=r1.height;
                    if (x0+w0<clipX0 && x1+w1<clipX0 || x0>clipX1 && x1>clipX1) continue;
                    if (y1>=0 && y0>=0 && col!=null) {
                        g.setColor(col);
                        if (y0==y1) {
                            if (style==HIGHLIGHT_STYLE_WAVE) {
                                if (fstPxlTilde<0) fstPxlTilde=ChFontMetrics.getCharBounds('~', g.getFont()).y;
                                g.drawString(toStrg('~',iTo-iFrom), x0,y0+charA+fstPxlTilde+1);
                            }
                            else if (ul) g.drawLine(x0,y1+h1,  x1+w1,y1+h1 );
                            else if (im==null) {
                                final int yf=maxi(clipY0, y0), wf=x1+w1-x0, hf=y1+h0-y0-1;
                                if (style==HIGHLIGHT_STYLE_RRECT) g.fillRoundRect(x0, yf+1,  wf, hf-1, charW, RECTf.height);
                                else fillBigRect(g, x0, yf, wf , hf);
                            }
                        } else {
                            if (ul) {
                                g.drawLine(x0, y(r0)+h0,  99999, y(r0)+h0);
                                g.drawLine(0,y1+h1, x1+w1,y1+h1 );
                            } else if (im==null) {
                                fillBigRect(g, x0,y0+1, wdth(jc)-x0,h0-2);
                                if (y1>y0+h0) fillBigRect(g, 0,y0+h0,wdth(jc),y1-y0-h0); g.setColor(col);
                                fillBigRect(g, 0,y1+1,x1+w1,h1-2);
                            }
                        }
                    }
                    if (im!=null && x0<x1 && (opts&ULREFS_NO_ICON)==0 && !isEditableFocus) {
                        final int colon=nxt(-LETTR, byteArray(), iFrom, iTo), cColon=chrAt(colon,byteArray());
                        final boolean flushr=cColon==':' || cColon=='|';
                        final int xr;
                        if (flushr) {
                            modelToView(colon+1, jc, RECTt);
                            xr=x(RECTt)-1;
                        } else xr=x1;
                        if (xr>x0) {
                            g.setColor(jc.getBackground());
                            fillBigRect(g, x0, y0,xr-x0, h0);
                            final int icw=wdth(ic), ich=hght(ic),  h2=ic==iicon("wikipedia") ? mini(EX,h0) : h0;
                            if (icw>0&&ich>0) {
                                if (icw*h2<ich*(xr-x0)) {
                                    final int d=icw*h2/ich;
                                    final int destX=maxi(x0, flushr?xr-d :  x0 + (xr-x0- d)*2/3);
                                    final int y=y0+(h0-h2)/2;
                                    g.drawImage(im, destX, y, destX+d, y+h2, 0, 0, icw, ich,jc);
                                } else {
                                    final int d=ich*(xr-x0)/icw;
                                    final int destY=maxi(y0,y0 + (y0+h0-y0- d)/2);
                                    g.drawImage(im, x0, destY , xr, destY+d, 0, 0, icw, ich,jc);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Dimension >>> */
    private float _scale=1;
    public static int getFixedRowHeight(Object o) {
        return
            o instanceof ChTextArea && !((JTextArea)o).getLineWrap() ? ((ChTextArea)o).getFixedRowHeight() :
            o instanceof ChTextView ? ((ChTextView)o).getFixedRowHeight() :
            -1;
    }
    public static int getLength(Object o) {
        if (o instanceof JTextComponent) return ((JTextComponent)o).getDocument().getLength();
        final ChTextView a=deref(o,ChTextView.class);
        if (a!=null) return sze(a.getFile()!=null ? a.getFile() : a.byteArray());
        return 0;
    }
    /* <<< Run <<< */
    /* ---------------------------------------- */
    /* >>> TextMatches >>> */
    public final List<TextMatches> HIGHLIGHTS[]=new ArrayList[3];
    public void addHighlight(TextMatches h) {
        final long opt=h.options();
        final int i=(opt&HIGHLIGHT_REMOVE_IF_TEXT_CHANGES)!=0 ? 0 : (opt&HIGHLIGHT_NEVER_REMOVE)!=0? 2 : 1;
        HIGHLIGHTS[i]=(List)adUniqNew(h, HIGHLIGHTS[i]);
    }
    public void rmAllHighlights() { for(Collection v : HIGHLIGHTS) clr(v); }
    public boolean removeHighlight(TextMatches h) {
        boolean success=false;
        for(int i=0; i<2; i++)  success|=sze(HIGHLIGHTS[i])>0 && HIGHLIGHTS[i].remove(h);
        return success;
    }
    private void highlightRGB() {
        final List<TextMatches> v=HIGHLIGHTS[0]==null ? HIGHLIGHTS[0]=new ArrayList() : HIGHLIGHTS[0];
        final BA ba=byteArray();
        final byte[] T=ba.bytes();
        final int end=ba.end();
        for(int i=ba.begin()+1; i<end; i++) {
            if (T[i-1]!='#' || !is(HEX_DIGT, T,i)) continue;
            final int to=nxtE(-HEX_DIGT,T,i+1,end);
            if (to-i!=6 && to-i!=8) continue;
            final long rgb=hexToInt(T,i,MAX_INT);
            if (rgb<0) continue;
            v.add(new TextMatches(HIGHLIGHT_NOT_IN_SCROLLBAR|HIGHLIGHT_REMOVE_IF_TEXT_CHANGES, new int[]{i-1,i}, 1,  C((int)rgb)));
        }
    }
    /* <<< TextMatches <<< */
    /* ---------------------------------------- */
    /* >>> highlightOccurrence >>> */
    public TextMatches highlightOccurrence(Object needle, boolean[] delimiterL,boolean[] delimiterR, long mode, Color color) {
        return hlo(needle,delimiterL,delimiterR,mode,color,null);
    }
    private TextMatches hlo(Object oNeedle, boolean[] delimL,boolean[] delimR, long mode, Color color, TextMatches highlight) {
        final Object provided= !(oNeedle instanceof BA || oNeedle instanceof Collection) && oNeedle instanceof ChRunnable ? ((ChRunnable)oNeedle).run(PROVIDE_HIGHLIGHTS, jc()) : null;
        final int[] buffer=highlight!=null ? highlight.fromToTrim() : null, fromTo;
        final byte[] lookup[][];
        if (oNeedle instanceof Object[] || provided!=null) {
            lookup=
                provided!=null ? (byte[][][]) provided :
                oNeedle instanceof byte[][][] ? (byte[][][]) oNeedle:
                OccurInText.addToLookupTable((Object[])oNeedle, null);
            fromTo=OccurInText.findLU(lookup,byteArray(),delimL,delimR,(IsEnabled)null,buffer);
        } else {
            final byte[] needle= oNeedle instanceof byte[] ? (byte[])oNeedle : toBytsCached(toStrg(oNeedle));
            fromTo=OccurInText.find(mode, needle, delimL, delimR, byteArray(),  buffer);
            lookup=null;
        }
        if ((mode&HIGHLIGHT_UPDATE_ON_MOUSE_ENTER)!=0) { _hlMouseEnter=true; /*return null; */}
        final boolean update=(mode&HIGHLIGHT_UPDATE_IF_TEXT_CHANGES)!=0;
        _hlAuto|=update;
        final int L=idxOfNotPositive(fromTo),  ft[]=chSze(fromTo,L);
        final TextMatches h=highlight==null && (ft.length>0 || update) ? new TextMatches(mode, ft, L/2, color) : highlight;
        if (h!=null) {
            h.setNeedle( provided==null&&lookup!=null ? lookup : oNeedle);
            h.setDelimiters(delimL,delimR);
            h.setFromTo(ft, MAX_INT);
            if (h!=highlight) addHighlight(h);
            if ( (mode&HIGHLIGHT_REPAINT)!=0){
                ChDelay.repaint(jc(),99);
                ChDelay.repaint(getSB('V', jc()),999);
            }
        }
        return h;
    }
    public void highlightBlanks() {
        highlightOccurrence(" ", null,null,HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(DEFAULT_BACKGROUND));
        highlightOccurrence("\t",null,null,HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(0xFF00FF));
    }
    /* <<< highlightOccurrence <<< */
    /* ---------------------------------------- */
    /* >>> Model View >>> */
    public static void modelToView(int m, Object o, Rectangle r) {
        final JTextComponent tc=deref(o, JTextComponent.class);
        if (tc!=null) {
            try {
                r.setBounds(tc.modelToView(m));
            } catch(Exception e){  r.setBounds(m<0?0: wdth(tc),m<0?0:hght(tc),0,0); }
        } else if (o instanceof ChTextView) ((ChTextView)o).modelToView(m,r);
    }
    /* <<< Model View <<< */
    /* ---------------------------------------- */
    /* >>> Tools >>> */
    public static ChTextComponents tools(Object o0) {
        final Object o=deref(o0);
        return
            o instanceof ChTextArea ?   ((ChTextArea)o).tools() :
            o instanceof ChTextField ? ((ChTextField)o).tools() :
            o instanceof ChJTextPane ? ((ChJTextPane)o).tools() :
            o instanceof ChTextView ?   ((ChTextView)o).tools() :
            null;
    }
    /* <<< Tools <<< */
    /* ---------------------------------------- */
    /* >>> Save >>> */
    public final static String DIR_SAVED_TEXT="~/@/savedText/";
    private File _saveFile;
    public ChTextComponents saveInFile(String fileName) {
        if (fileName!=null) saveInFile(file(DIR_SAVED_TEXT+ filtrS('_'|FILTER_NO_MATCH_TO, FILEP, fileName)+".txt"));
        return this;
    }
    public void saveInFile(File file) {_saveFile=file;  }
    public static int viewToModel(Point p,Object o) {
        if (o instanceof JTextComponent && p!=null) {
            try { return ((JTextComponent)o).viewToModel(p); } catch(Exception e){}
        } else  if (o instanceof ChTextView) return ((ChTextView)o).viewToModel(x(p),y(p));
        return -1;
    }
    private static File myFile(String n) {
        return sze(n)==0 ? null : file(FILE_NO_ERROR, isWin() && n.startsWith("~/.StrapAlign") ? rplcToStrg("~/.StrapAlign", dirSettings().toString(), n) : n);
    }
    public void loadSavedText() {
        final JComponent jc=jc();
        if (jc instanceof JTextComponent && _saveFile!=null && !_saveLoaded) {
            _saveLoaded=true;
            setTextTS(readBytes(_saveFile));
            _saveNeeded=false;
        }
    }
    public void saveInFile(boolean always) {
        if (_saveNeeded || always) wrte(_saveFile,byteArray());
        _saveNeeded=false;
    }
    /* <<< Save <<< */
    /* ---------------------------------------- */
    /* >>> Analyze Text >>> */
    private boolean _jOrtho;
    private static boolean _jOrthoInit;

    public static int nxtEC(byte T[], int from, int end) {
        final int dgt=nxt(DIGT,T,from, end);
        if ( (dgt==0 || dgt>0 && is(SPC,T[dgt-1])) && looks(LIKE_EC,T,dgt,end)) return dgt;
        return -1;
    }

    private static int fileEndsAt(byte[] T, int pos, int end) {
        if (pos+3>end || !looks(LIKE_FILEPATH_EVALVAR,T,pos)) return -1;
        final boolean cc[]=chrClas(FILENM);
        int to=pos+3;
        nextI:
        while(to<end) {
            final byte c=T[to];
            if (c<=0) break nextI;
            if (!(cc[c] || c=='%'||c=='/'||c=='$'||c=='\\')) break nextI;
            for(String V : FILE_RANGE_SUFX)  if (strEquls(V,T,to)) break nextI;
            to++;
        }
        File f=myFile(toStrg(T,pos,to));
        if (!fExists(f) && chrAt(to-1,T)=='.') f=myFile(toStrg(T,pos,--to));
        return fExists(f) ? to : -1;
    }
    /* <<< Analyze Text <<< */
    /* ---------------------------------------- */
    /* >>> Text >>> */

    private transient Object _refBA, _segment;
    private transient Object _getText;
    public String getText() {
        String s=(String)deref(_getText);
        if (s==null) {
            if (isEDT()) s=getTxt(jc());
            else {
                final String ss[]={null};
                inEDT(thrdCR(this, "getText", ss));
                s=ss[0];
            }
            _getText=newSoftRef(s);
        }
        return s;
    }
    public void setTextTS(CharSequence txt) { setTextTS(txt,false); }
    public void setTextTS(CharSequence txt, boolean keepCaretPosition) {
        final String s=toStrg(txt);
        if (s!=null) {
            if (!isEDT()) inEDT(thrdCR(this,"setTextTS",new Object[]{s,keepCaretPosition?"":null}));
            else {
                final JTextComponent tc=tc();
                final ChJScrollPane sp=getSP(tc);
                final JScrollBar sb=keepCaretPosition&&sp!=null?sp.getVerticalScrollBar() : null;
                final int scrollV=sb!=null?sb.getValue(): -1;
                final int caret=keepCaretPosition ? tc.getCaretPosition() : -1;
                _getText=newSoftRef(s);
                final boolean save=_evEnabled;
                enableTextChangeEv(false);
                _alreadUlRefs=false;
                setChanged();
                tc.setText(s);
                try {
                    if (caret>=0) tc.setCaretPosition(caret);
                } catch(Exception ex){}
                if (scrollV>=0)  sb.setValue(scrollV);
                enableTextChangeEv(save);
            }
            _saveNeeded=true;
        }
    }
    public BA byteArray() {
        loadSavedText();
        final JComponent jc=jc();
        final ChTextView ascii=jc instanceof ChTextView ? (ChTextView)jc : null;
        if (ascii!=null) return ascii.byteArray();
        BA ba=(BA)deref(_refBA);
        if (ba==null) { _refBA=newSoftRef(ba=new BA(NO_BYTE));_byteArrayValid=false;}
        if (!_byteArrayValid) {
            ba.setTextSegment(getTextAsSegment());
            _byteArrayValid=true;
        }
        return ba;
    }
    public Segment getTextAsSegment() {
        if (!isEDT()) {
            final Segment[] ss={null};
            inEDT(thrdCR(this,"getTextAsSegment",ss));
            return ss[0];
        }
        final JTextComponent tc=tc();
        if (tc!=null) {
            Segment segm=(Segment)deref(_segment);
            if (!_segmentValid || segm==null) {
                if (segm==null) _segment=newSoftRef(segm=new Segment());
                _segmentValid=true;
                final Document doc=tc.getDocument();
                try {
                    segm.array=null;
                    doc.getText(0, doc.getLength(), segm);
                } catch(Exception e){ putln(RED_CAUGHT_IN+"ChTextComponents"," getText(int,int,Segment)", e);}
            }
            return segm;
        }
        return null;
    }
    /* <<< Text<<< */
    /* ---------------------------------------- */
    /* >>> Text Change >>> */
    private ChTextReplacement[] _replacements;
    private int _mc;
    public void setReplaceWhenTextChanges(ChTextReplacement...replacements) { _replacements=replacements; }
    public int mc() { return _mc; }
    public void enableTextChangeEv(boolean b) { _evEnabled=b; }
    public void insertUpdate(DocumentEvent ev){ changed(ev); }
    public void removeUpdate(DocumentEvent ev){ changed(ev); }
    public void changedUpdate(DocumentEvent ev){}
    public void setChanged() {
        _mc++;
        _getText=null;
        _byteArrayValid=_segmentValid=false;
    }
    private void changed(DocumentEvent ev) {
        setChanged();
        if (_painted) {
            final JComponent jc=jc();
            boolean doRunHL=sze(HIGHLIGHTS[0])>0 || _hlAuto;
            for(int i=sze(HIGHLIGHTS[1]); --i>=0 && !doRunHL;) {
                final TextMatches h=(TextMatches)get(i,HIGHLIGHTS[1]);
                if (h!=null && (h.options()&HIGHLIGHT_UPDATE_IF_TEXT_CHANGES)!=0 ) doRunHL=true;
            }
            final boolean immediately=false; // ???
            _saveNeeded=true;
            if (doRunHL && _hlAuto && ev!=null) {
                boolean runThread=true;
                final boolean
                    ins=ev.getType()==DocumentEvent.EventType.INSERT,
                    del=ev.getType()==DocumentEvent.EventType.REMOVE;
                if (!ins && !del || ev.getLength()>1) {
                    _RECT.add(0,0);
                    _RECT.add(wdth(jc),hght(jc));
                } else {
                    shiftHighlights(ev.getOffset(), ev.getLength()*(ins?1:-1));
                    if (immediately) {
                        runThread=false;
                        run('h').run();
                    } else {
                        modelToView(ev.getOffset(),jc,RECTf);
                        final int charH=charH(jc), y=y(RECTf);
                        if (_RECT.height==0) _RECT.setBounds(0,y,0,0);
                        _RECT.add(0,y-charH);
                        _RECT.add(wdth(jc),y+2*charH);
                    }
                }
                if (runThread) inEDTms(run('h'),333+10*run('h').LAST[DURATION]);
            }
            if (gcp(KOPT_NO_EVENT,jc)==null && _evEnabled && _alreadyTyped && ev!=null) {
                /* --- Event dispatching needs delay because the caret position is adjusted later --- */
                if (ev.getType()==DocumentEvent.EventType.INSERT && ev.getLength()>2) inEDTms(run('I'),333);
                inEDTms(run('C'), run('C').LAST[DURATION]*2+111);
            }
            if (sze(_replacements)>0) inEDTms(run('R'),333);
        }
    }

    /* <<< Text  Change<<< */
    /* ---------------------------------------- */
    /* >>> Selection >>> */
    public void marchingAnts(int id, int from, int to) {
        final JComponent jc=jc();
        modelToView(from,jc,RECTf);
        modelToView(to,jc,RECTt);
        if (RECTf.height>0 && RECTt.height>0) {
            final Rectangle r=new Rectangle(x(RECTf), y(RECTf),  x2(RECTt)-x(RECTf),  y2(RECTt)-y(RECTf));
            ChScrollBar.lock(0, getSB('V', jc));
            jc.scrollRectToVisible(new Rectangle(x(r)-8, y(r)-8, r.width+16, r.height+16));
            setMarchingAnt(ANTS_SEARCH, jc,r, C(0xFF00ff), C(0xFF0000));
        }
    }
    public boolean getSelection(Object range) {
        final int f,t;
        if (tc()!=null) {
            f=tc().getSelectionStart();
            t=tc().getSelectionEnd();
        } else if (jc() instanceof ChTextView) {
            final int[] ft=((ChTextView)jc())._regionFT;
            f=ft[0];
            t=ft[1];
        } else f=t=-1;
        if (range instanceof Range) ((Range)range).set(f,t);
        final int ft[]=deref(range, int[].class);
        if (ft!=null) { ft[0]=f; ft[1]=t;}
        return f>=0 && f<t;
    }
    public void setSelection(int f, int t) {
        if (tc()!=null) {
            tc().setSelectionStart(f);
            tc().setSelectionEnd(t);
        } else if (jc() instanceof ChTextView) ((ChTextView)jc()).setSelection(f,t);
    }

    /* <<< Selection <<< */
    /* ---------------------------------------- */
    /* >>> static utility >>> */
    private static boolean contains(Point p, int x0, int y0, int x1, int y1) {
        return x(p)>x0 && x(p)<x1 && y(p)>y0 && y(p)<y1;
    }
    public final static long INSERT_IF_NOT_CONTAINS=1<<0;
    public static void insertAtCaret(long options, CharSequence ins, Component jc) {
        final String s=toStrg(ins);
        final JTextComponent tc=deref(jc,JTextComponent.class);
        final int L=sze(s);
        if (L==0 || tc==null) return;
        final String t=getTxt(tc);
        if ( (options&INSERT_IF_NOT_CONTAINS)!=0 && t.indexOf(s)>=0) return;
        final int cp=maxi(0,tc.getCaretPosition()), after=cp+L;
        try { tc.getDocument().insertString(cp, s, null); } catch(Exception e){ return; }
        tc.setCaretPosition(after);
        tc.requestFocus();
        final ChTextComponents tools=tools(jc);
        if (tools!=null) {
            final TextMatches h=new TextMatches(HIGHLIGHT_NOT_IN_SCROLLBAR, new int[]{cp,after}, 1, C(0xFF00,99));
            tools.addHighlight(h);
            inEDTms( thrdCR(tools,"ADD_OPT2H", h),444);
        }
    }
    private static JComponent _ofDia, _ofClose;
    private static ChButton _ofB[]=new ChButton[9];
    private final static int OFopenFile=0, OFopenDir=1, OFU=2, OFu=3, OFpdf=4, OFmenu=5;
    private static ChTextField _ofTf;

    private static void _ofDnD() {
        final String path=toStrg(_ofTf);
        final File f=file(FILE_NO_ERROR,path);
        _ofB[OFopenFile].addDnD(fExists(f)?f:null).tt("");
        _ofB[OFU].addDnD(looks(LIKE_EXTURL,path)?url(path):null).tt("");
    }

    private static void processFile(int opt, String word, String path) {
        if (nxt(-SPC,path)<0) return;
        if (_ofDia==null) {
            final String ss[]="Open file;Open directory;In web browser;Parent in web browser;Manage publication PDF full text ...;File menu \u25bc".split(";");
            for(int i=0;i<ss.length;i++) {
                _ofB[i]=new ChButton(ss[i]).li(instance().LI).cp(KOPT_HIDE_IF_DISABLED,"");
            }
            final Object
                pBut=pnl(VBHB,_ofTf=new ChTextField("",COMPLETION_FILES_IN_WORKING_DIR).li(instance().LI),
                         pnl(HB, _ofB[0], " ", _ofB[1], " ", _ofB[2], " ", _ofB[3], "#"), " ",
                         _ofB[4]," ",
                         _ofB[5]," ",
                         Insecure.EXEC_ALLOWED ? "Hint: For details, hold Ctrl-key while pressing buttons." : null
                         ),
                pDetail=pnl(VBHB,
                            Insecure.EXEC_ALLOWED ? Customize.newButton(Customize.buttonFileViewers) : null,
                            "#JS",
                            buttn(NEW_BUT_EXEC_TRUSTED),
                            buttn(TOG_NASK_EXEC).cb(),
                            "#JS",
                            _ofClose=cbox(true,"Close this dialog afterwards", "",null),
                            "#JS", " "
                            );
            final String msgAvoid="<sub>Note: Opening this dialog can be avoided by holding shift while clicking into the text.</sub>";
            _ofDia=pnl(VB,pBut,  pnlTogglOpts("*Options",pDetail),pnl(msgAvoid),pDetail);
        }
        _ofTf.tools().underlineRefs(ULREFS_NOT_CLICKABLE);
        _ofTf.setText(path);
        final File f=path.indexOf(':')>=0&&!looks(LIKE_FILEPATH_EVALVAR,path)?null:file(path), fParent=drctry(f);
        final Object u=url(path), bb[]=_ofB;
        final boolean isD=isDir(f), isF=fExists(f) && !isD, isP=!isD && fExists(fParent);
        setEnabld(isF, bb[OFmenu]);
        setEnabld(u!=null, bb[OFU]);
        setEnabld( Insecure.EXEC_ALLOWED && (isF || isD), bb[OFopenDir]);
        setEnabld( Insecure.EXEC_ALLOWED && isF, bb[OFopenFile]);
        setEnabld(!Insecure.EXEC_ALLOWED && isP, bb[OFu]);
        setEnabld(pmid(word)>0, bb[OFpdf]);
        pcp("W", word,_ofB[OFpdf]);
        _ofDnD();
        final ChFrame w=ChFrame.frame("Process file", _ofDia, ChFrame.ALWAYS_ON_TOP).shw(ChFrame.AT_CLICK|ChFrame.PACK);
        inEDTms( thread_setWndwState('T',w), 222);
    }
    private final static Point pointTmp=new Point();
    public int boundsToPos(boolean first, Rectangle clip) {
        if (clip==null) return first?0:MAX_INT/2;
        pointTmp.move(first? x(clip) : x2(clip), first?y(clip) : y2(clip));
        return ChTextComponents.viewToModel(pointTmp,jc());
    }

    public ChTextComponents cp(Object key, Object value) { pcp(key,value,jc()); return this;}
    public ChTextComponents tt(Object tt) {  setTip(tt,jc()); return this; }
   final Map MAPCP=new java.util.HashMap();

}
