package charite.christo;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import javax.swing.JComponent;
import java.io.File;
import java.lang.reflect.Member;
/**
   For the generation of the documentation of STRAP
   @author Christoph Gille
*/
public class HtmlDocElement implements HasRenderer {
    private final String _vPackages[], _id, _documentId, _parameter, _className, _fieldOrMethod;
    private final boolean _isActive;
    private final Range _range;
    private final Class _clazz;
    private JComponent _jComponent;
    private File _imageFile;
    private String _urlImageFile, _style;
    private Object _value;
    private static ChButton _label;
    public Object getRenderer(long options, long rendOptions[]) {
        if (_jComponent!=null) return _jComponent;
        return (_label==null ? _label=labl() : _label).t(_parameter);
    }
    @Override public String toString() {
        return new BA(0)
            .a("HtmlDocElement: id=").a(_id).a(" documentId=").aln(_documentId)
            .a(" fieldOrMethod=").aln(_fieldOrMethod)
            .a(" par=").aln(getParameter())
            .a(" class=").aln(getClazz())
            .a(" value=").aln(_value)
            .a(" vPackages=").join(_vPackages,",")
            .toString();
    }
    public String getFieldOrMethod() { return _fieldOrMethod;}
    public String getParameter() { return _parameter;}
    public void setJComponent(JComponent c) { _jComponent=c;}
    public String urlImageFile() {
        imageFile();
        return _urlImageFile;
    }

    public File imageFile() {
        if (_imageFile==null) {
            final String
                name=
                _clazz!=null ? (_clazz.getName().replace('.','-')+("+"+_fieldOrMethod)).replaceAll(REGEX_NO_FILE_NAME,"_")+".png" :
                ((getValue()!=null ? getValue() : getParameter())+".png").replaceAll(REGEX_NO_FILE_NAME,"_");
            _imageFile=new File(dirDocumentation(),name!=null ? name : toStrg(getValue()));
            _urlImageFile=_documentId==ChTextComponents.DOCID_TMP ? toStrg(url(_imageFile)) : name;
        }
        return _imageFile;
    }
    public Range getRange() { return _range;}
    public Class getClazz() { return _clazz;}
    public JComponent getJComponent() { return _jComponent;}
    public boolean isActive() { return _isActive;}
    public String id() { return _id;}
    public String getDocumentID() { return _documentId;}
    public String getStyle() {return _style;}

    public HtmlDocElement(String id,String param,Range r,String[] packages,String docId) {
        final String para=delSfx('!',param);
        _isActive=param!=para;
        _documentId=docId;
        _id=id;
        _range=r;
        _vPackages=packages;
        _parameter=para;
        final int len=sze(para);
        final boolean isConstructor=para.startsWith("new ");
        if (chrAt(0, para)=='"' && chrAt(len-1,para)=='"') {
            _value=rplcToStrg("&LT;","<",rplcToStrg("&GT;",">",para.substring(1,len-1)));
            _className=_fieldOrMethod=null;
            _clazz=null;
            return;
        }
        final int iSharp=para.indexOf('#');
        {
            final String cn=delLstCmpnt(iSharp>=0 ? para.substring(0,iSharp) : isConstructor ? para.substring(para.lastIndexOf(' ')+1,para.indexOf('(')) : para, ',');
            _clazz=findClas(cn, packages);
            _className=  _clazz!=null ? cn: null;
        }
        if (iSharp>=0) {
            final int iColon=para.indexOf(':',iSharp);
            _fieldOrMethod=iColon<0 ? para.substring(iSharp+1) : para.substring(iSharp+1,iColon);
            if (iColon>0) _style=para.substring(iColon+1);
        } else if (isConstructor) {
            _fieldOrMethod=para;
        } else _fieldOrMethod=null;
    }
    private Member _member;
    public final Member getMember() { return _member;}
    public final Object getValue() {
        if (_value==null) {
            _value=ERROR_OBJECT;
            final String sMember=_fieldOrMethod;
            if (sze(sMember)>0) {
                Member mem=null;
                //final Class cl=sze(_className)>0 ? _clazz=findClas(_className,vPackages) : null;
                final Class cl=_clazz;
                if (cl!=null) {
                    final Object[] returnValue={null};
                    _member=mem=ChJavadoc.findMember(cl, sMember, _vPackages, returnValue);
                    _value=returnValue[0];

                }
                if (_value==null) HtmlDoc.log().a(_documentId).a(RED_ERROR+" HtmlDocElement:"+ANSI_RESET+" id=").a(_id)
                                      .a(" class=").a(cl).a(cl==null?_className:null)
                                      .a(" sMember=").a(sMember).a(" mem!=null: ").a(mem!=null?'t':'f').a('\n');
            }
        }
        return _value!=ERROR_OBJECT?_value:null;
    }

}

