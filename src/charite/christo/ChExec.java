package charite.christo;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.util.List;
import java.util.*;
import java.lang.ref.Reference;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   execute external programs
   Several alternative command lines can be set.
   They are probed until one succeeds.
   The trying of command lines stops at an empty command line.
   The method error(Exception) is called when non of the alternative command lines succeeds.
   @author Christoph Gille
*/
public final class ChExec implements Runnable,HasControlPanel,java.awt.event.ActionListener, ChRunnable {
    public final static boolean SUCCESS[]=new boolean['z'+1];
    private static void recordSuccess(String prg, boolean success) {
        final int id=
            prg==null ? 0 :
            prg.indexOf("clustalw")>=0 ? 'W' :
            prg.indexOf("pymol")>=0 ? 'P' :
            prg.endsWith("tex") ? 'X' :
            0;
        SUCCESS[success?id : (id|32)]=true;
    }
    public final static int STDOUT=1<<0, STDERR=1<<1,IGNORE_ERROR=1<<2, CYGWIN_DLL=1<<3, SHOW_STREAMS=1<<4,
        LOG=1<<5, STDOUT_IN_TEXT_BOX=1<<6, PLEASE_INSTALL_MANUALLY=1<<7, WITHOUT_ASKING=1<<8, NOT_TO_DENY_LIST=1<<9, DEBUG=1<<10, TO_STDOUT=1<<11;
    final static String WINDOWS_addBinToPath=
        "export PATH=/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin:/usr/local/bin:/usr/bin:/bin:/usr/X11R6/bin; "+
        "export DISPLAY=localhost:0;\n";

    public final static String
        ACTION_ERROR="ACTION_ERROR", ACTION_TERMINATED="ACTION_TERMINATED", CYGWINSH="CYGWINSH",
        EXE_TM_ALIGN="TMalign2";
    private final static int
        OUT=0, ERR=1, STATUS=2, KILL=3,
        OUTandERR=3;

    private String _icon, _label="Status",   _commandLinesV[][], _inString;
    private Object _process, _center, _ctrl;
    private boolean _finished, _launched, _failedDisplay, _running[];
    private long _opt;
    public ChExec(long mode) {
        setOptions(mode);
    }
    public ChExec setOptions(long m0) {
        final long m=_opt= 0!=(_opt&STDOUT_IN_TEXT_BOX) ? m0|STDOUT : m0;
        if ( (m&STDERR)!=0) _vBytes[ERR]=WANT_DATA;
        if ( (m&STDOUT)!=0) _vBytes[OUT]=WANT_DATA;
        if ( (m&SHOW_STREAMS)!=0) showStdoutAndStderr(null);

        return this;
    }
    public boolean finished() { return _finished;}
    public boolean couldNotLaunch(){ return _finished && !_launched;}
    public boolean failedOpenDisplay() { return _failedDisplay;}
    private int _exitV=Integer.MIN_VALUE;
    public int exitValue() { return  _exitV;}
    public void kill() {
        final Process p=(Process)deref(_process);
        if (p!=null) try {
                p.destroy();
                handleActEvt(this,ACTION_TERMINATED,0);
            } catch(Exception e){ }
    }
    public boolean isRunning() { return get(0, _running); }
    public void dispose() {
        closeStrm(_bufStdin);
        kill();
        _bufStdin=null;
        _userDir=null;
        _sbStatus.clr();
        _vBytes[0]=_vBytes[1]=null;
        inEdtLaterCR(this,"DISPOSE",null);
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Log  >>> */
    private static int _countInst;
    private final String PFX=(!withGui()?"[Exec ":"[")+(++_countInst)+"] ";
    private static BA _log;
    private final BA _sbStatus=new BA(222);
    private ChTextView _outAndErr;
    public static BA log() {
        if (_log==null) {
            _log=new BA(99);
            if (withGui()) _log.sendToLogger(0*ChLogger.NO_LINE_NUMBERS, "Execute binaries",IC_BATCH, 99*1000);
            else _log.setSendTo(BA.SEND_TO_STDOUT);
        }
        return _log;
    }
    public void setCustomize(Customize c) {
        if (withGui()) {
            _customize=c;
            if (c!=null) {
            _opt|=OUTandERR;
            showStdoutAndStderr(null);
            }
        }
    }
    public ChExec showStdoutAndStderr(ChFrame f) {
        _opt=_opt|STDOUT|STDERR;
        if (!isEDT()) inEDT(thrdM("showStdoutAndStderr", this,new Object[]{f}));
        else {
            if (f!=null) _fOutErr=f;
            ChTextView ta=get(OUTandERR,_refTA, ChTextView.class);
            if (ta==null) _refTA[OUTandERR]=wref(ta=_outAndErr=new ChTextView(999));
            if (_fOutErr!=null) _fOutErr.shw(0);
            else ta.run(ChRunnable.RUN_SHOW_IN_FRAME,null);
        }
        return this;
    }
    /* <<< Log <<< */
    /* ---------------------------------------- */
    /* >>> Environment  >>> */
    private List<String> _vEnv;
    public ChExec addToEnvironement(Object o) {
        if (o!=null) {
            List<String> v=_vEnv;
            if(v==null) v=_vEnv=new Vector();
            if (o instanceof Map) {
                final Map m=(Map)o;
                for(Map.Entry e : entryArry(m)) {
                    if (e.getValue()!=null) adUniq(e.getKey()+"="+e.getValue(),v);
                }
            } else adUniq(toStrg(o), v);
        }
        return this;
    }
    /* <<< Environment <<< */
    /* ---------------------------------------- */
    /* >>> Command Lines >>> */

    private Customize _customize;
    public ChExec setCommandLine(Object s) { return setCommandLines(new String[]{toStrg(s)}); }
    public ChExec setCommandLines(String[] lines) {
        _commandLinesV=new String[lines.length][];
        for(int i=0;i<lines.length;i++) {
            final String ss[]=splitTokns(lines[i]);
            if (ss.length>0) ss[0]=toStrg(rplcEnvInPath(ss[0]));
            for(int j=ss.length; --j>=0;) ss[j]=rplcToStrg("%20"," ",ss[j]);
            _commandLinesV[i]=ss;
        }
        return this;
    }
    public ChExec setCommandsAndArgument(String[] commands,Object argument0) {
        Object argument=argument0;
        if (argument==null) return setCommandLines(commands);
        final String lines[]=new String[commands.length];
        final boolean win=isWin();
        int count=0;
        for(int i=0;i<commands.length;i++) {
            String s=toStrg(rplcEnvInPath(commands[i]));
            if (sze(s)==0) continue;
            if (chrAt(0,s)=='/' && sze(file(fstTkn(s)))==0) {
                continue;
            }

            final boolean isCygwin=s.startsWith(CYGWINSH);
            if (isCygwin && argument instanceof File) argument=toCygwinPath(argument);
            if (isCygwin && !win) {
                s=s.substring(nxt(-SPC, s, CYGWINSH.length(), MAX_INT));
            }
            String sArgument=toStrg(argument);
            if (sArgument!=null && sArgument.startsWith("file:/") && !sArgument.startsWith("file://")) sArgument="file://"+sArgument.substring(5);

            lines[count++]=s.indexOf('*')>=0 ?  rplcToStrg("*",sArgument,rplcToStrg("$*",sArgument,s)) : s+" "+sArgument;
        }

        return setCommandLines(lines);
    }
    /**
       null is skipped, for List and Arrays the elements are inserted.
       If arg[0] is   CYGWINSH all files are written in the style /cygdrive/c/...
    */

    public void setCL(List v, Object o, boolean cygw) {
        if (o==null) return;
        else if (o instanceof PrgParas) adAll( ((PrgParas)o).asStringArray(), v);
        else if (o instanceof Collection) v.addAll((Collection)o);
        else if (o instanceof Object[]) adAll( (Object[])o, v);
        else v.add(o instanceof File && cygw ? toCygwinPath(o) : o);
    }
    public ChExec setCommandLineV(Object... objects) {
        final boolean cygw;
        if (CYGWINSH.equals(get(0,objects))) {
            if (!isWin()) objects[0]=null;
            cygw=true;
        } else cygw=0!=(_opt&CYGWIN_DLL);
        final int n=sze(objects);
        final List v=new Vector(n);
        for(int i=0; i<n; i++) {
            setCL(v, objects[i], i>0&&cygw);
        }
        _commandLinesV=new String[][]{toStrgArray(v.toArray())};
        return this;
    }
    /* <<< Command Lines <<< */
    /* ---------------------------------------- */
    /* >>> Streams >>> */
    private final static byte NL[]={(byte)'\n'};
    private final static List<byte[]> WANT_DATA=new Vector();
    private final List<byte[]> _vBytes[]=new Vector[2];
    private ChRunnable _printlnStdout;
    private final boolean _stdoutDone[]={false};
    private BufferedOutputStream _bufStdin;
    public ChExec printlnStdout(ChRunnable a) { _printlnStdout=a;return this;}
    private void readStream(InputStream is,boolean closed[], boolean isRunning[],
                                   List<byte[]> vBytes[], Reference<ChTextView> refLog[], ChRunnable lines,
                                   Reference<ChButton> refButs[], int outOrErr) {
        if (is==null) return;
        final ChInStream br=lines!=null ? new ChInStream(is,1024)  : null;
        final BA LINE=lines!=null ? new BA(99) : null;

    stop:
        while(true) {
            try {
                final String line;
                final byte[] appnd;
                if (lines!=null) {
                    appnd=null;
                    if (!br.readLine(LINE.clr())) break stop;
                    lines.run(ChRunnable.RUN_INTERPRET_LINE,line=LINE.toString());
                } else {
                    line=null;
                    int available=0;
                    while ((available=is.available())==0) {
                        if (!isRunning[0] && is.available()==0) break stop;
                        Thread.sleep(22);
                    }
                    final byte bb[]=new byte[available];
                    final int nRead=is.read(bb,0,available);
                    if (nRead==0) {
                        try { Thread.sleep(22); } catch(Exception e) {}
                        continue;
                    }
                    if (0!=(_opt&TO_STDOUT)) System.out.write(bb,0,nRead);
                    _failedDisplay|=strstr(0L,"failed to open display",bb,0,nRead)>0;
                    appnd=nRead==bb.length ? bb:chSze(bb,nRead);
                }
                if (sze(appnd)>0 || line!=null) {
                    setEnabld(true, get(outOrErr,refButs,JComponent.class));
                    if (vBytes[outOrErr]==WANT_DATA) vBytes[outOrErr]=new Vector(2);
                    final List<byte[]> vB=vBytes[outOrErr];
                    if (vB!=null) {
                        if(appnd!=null) vB.add(appnd);
                        if (line!=null) { vB.add(line.getBytes()); vB.add(NL); }
                    }
                    for(int i=refLog.length; --i>=0;) {
                        final ChTextView ta=get(i,refLog, ChTextView.class);
                        if (ta!=null && (i==OUTandERR || i==outOrErr)) {
                            ta.byteArray().a(appnd).aln(line);
                            if (appnd!=null||line!=null) ChDelay.revalidate(ta,333);
                        }
                    }
                }
            } catch(Exception ex) {
                log().a(PFX).a(RED_CAUGHT_IN+"ChExec#readStream() ").a(ex).send();
                stckTrc(ex);
                if (!isRunning[0])  break stop;
            }
            try { Thread.sleep(22); } catch(Exception e) { break stop; }
        }
        if (closed!=null) closed[0]=true;
        closeStrm(is);
        closeStrm(_bufStdin);
    }
    public ChExec setStdin(String s) { _inString=s; return this; }
    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        final BA log=_sbStatus;
        if (id=="SHW_F") {
            final ChTextView ta=_outAndErr;
            if (ta!=null) {
                ChFrame f=get(OUTandERR,_frames, ChFrame.class);
                if (f==null) {
                    if (_fOutErr!=null) { (f=_fOutErr).getContentPane().removeAll();}
                    else addActLi(this, f=new ChFrame("Execute external program"));
                    _frames[OUTandERR]=wref(f);
                    f.ad(pnl(CNSEW,scrllpn(ta), _customize==null?null:pnl(Customize.newButton(_customize)), getControlPanel(true))).shw(CLOSE_CtrlW_ESC|ChFrame.STAGGER);
                }
                f.size(600,300).shw(0);
            }
        }
        if (id=="SAY_FAILED") {
            log().a(PFX).aln(ANSI_RED+"Failed"+ANSI_RESET).send();
            disableKill("Failed",IC_UNHAPPY);
            handleActEvt(this,ACTION_ERROR,0);
        }

        if (id=="SHOW_OUT") shwTxtInW(getStdoutAsBA());
        if (id=="DISPOSE") dispos(_frames);
        if (id=="STREAM") {
            final Process pp[]=(Process[])argv[0];
            final boolean[] closed=(boolean[])argv[1], running=(boolean[])argv[2];
            final List[] vBytes=(List[]) argv[3];
            final Reference<ChTextView>[] refLogs=(Reference[]) argv[4];
            final ChRunnable lines=(ChRunnable)argv[5];
            final Reference<ChButton> refB[]=(Reference[])argv[6];
            final int O_or_E="O"==argv[7] ? OUT : ERR;
            while(pp[0]==null) {
                try { Thread.sleep(99); } catch(Exception e) {}
                if (couldNotLaunch()) return null;
            }
            final Process p=pp[0];
            if (p!=null) {
                InputStream is=O_or_E==OUT ? p.getInputStream() : p.getErrorStream();
                if (!(is instanceof BufferedInputStream)) is=new BufferedInputStream(is);
                readStream(is,closed, running,vBytes,refLogs,lines, refB, O_or_E);
            }
        }
        if (id=="WRITE") threadWriteToProcess((String)argv[0],(boolean[])argv[1]);
        return null;
    }
    private void threadWriteToProcess(String inString, boolean isRunning[]) {
        Process p=null;
        while(p==null) {
            try { Thread.sleep(99); } catch(Exception e) {}
            if (!isRunning[0]) return;
            p=(Process)deref(_process);
        }
        if (inString==null || p==null) return;
        final BufferedOutputStream os=new BufferedOutputStream(p.getOutputStream());
        try{
            Thread.sleep(99);
            os.write(inString.getBytes());
        } catch(Exception e){ log().a(PFX).a(RED_CAUGHT_IN).a("writeToProcess ").aln(e).send(); }
        closeStrm(os);
    }
    public ChExec write(String s) throws IOException {
        BufferedOutputStream os=_bufStdin;
        final Process p=(Process)deref(_process);
        if (p!=null && isRunning()) {
            if (os==null) os=_bufStdin=new BufferedOutputStream(p.getOutputStream());
            os.write(s.getBytes());
            os.flush();
        }
        return this;
    }
    public byte[] getStderr() { return bytesToBytes(_vBytes[ERR]);}
    public byte[] getStdout() {
        if (couldNotLaunch()) return null;
        final List v=waitForStdout();
        return couldNotLaunch() ? null : bytesToBytes(v);
    }
    public BA getStdoutAsBA() {
        final byte[] stdout=getStdout();
        return stdout==null?null: new BA(getStdout());
    }
    private List waitForStdout() {
        if (_vBytes[OUT]==null) return null;
        try {
            while(!_stdoutDone[0] && !couldNotLaunch()) { Thread.sleep(55); }
        } catch(InterruptedException e){}
        return _vBytes[OUT];
    }
    /* <<< Streams <<< */
    /* ---------------------------------------- */
    /* >>> Files >>> */
    private File _userDir;
    public ChExec dir(File f) { _userDir=f; return this;}
    /* <<< Files <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    public void run() {
        if (_outAndErr!=null) inEdtLater(thrdCR(this,"SHW_F"));
        _launched=_finished=false;
        final File dir=_userDir!=null ? _userDir : dirWorking();
        boolean ok=false;
        for(int iTry=0; !ok && iTry<sze(_commandLinesV); iTry++) {
            final String[] cmd=_commandLinesV[iTry];
            if (sze(cmd)==0) continue;
            if (ok=runLine(cmd,dir)) handleActEvt(this,ACTION_TERMINATED,0);
            else try { Thread.sleep(222); } catch(Exception e) {}
        }
        if (!ok) inEdtLaterCR(this,"SAY_FAILED",null);
        if (0!=(_opt&STDOUT_IN_TEXT_BOX)) inEdtLaterCR(this,"SHOW_OUT",null);
    }
    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> Exec >>> */
    private boolean runLine(String execv0[],File dir) {
        final boolean win=isWin();
        final long opts=_opt;
        String[] execv=execv0;
        final BA baTmp=new BA(99), log=_sbStatus;
        if ( sze(execv)==0 || chrAt(0,execv[0])=='#') return false;
        if (CYGWINSH.equals(execv[0])) {
            if (win) {
            final String[] execvNew={dirCygwin()+"\\bin\\sh.exe","-c",""};
            final BA sb=new BA(111).a(WINDOWS_addBinToPath);
            for(int i=1; i<execv.length; i++) sb.a(execv[i]).a(' ');
            execvNew[2]=sb.toString();
            execv=execvNew;
            } else execv[0]=null;
        }
        if ("SH".equals(execv[0])) {
            final String[] execvNew={win ? Microsoft.cmd() : "sh" , win ? "/c" : "-c", ""};
            final BA sb=new BA(111);
            for(int i=1;i<execv.length;i++) sb.a(execv[i]).a(' ');
            execvNew[2]=sb.toString();
            execv=execvNew;
        }
        final ChTextView ta=_outAndErr;
        boolean success=false;
        try {
            int commandLength=0;
            for(int i=0;i<execv.length;i++) {
                String s=execv[i];
                s=rplcToStrg("STRAP_COLOR_DEPTH",toStrg(dtkt().getColorModel().getPixelSize()),s);
                if (win) {
                    if (s.startsWith("?:")) s=processFileName(s);
                    final File dc=dirCygwin();
                    if (dc!=null) s=rplcToStrg("DIR_CYGWIN",toStrg(dc),s);
                }
                execv[i]=s;
                commandLength+=s.length();
            }
            if (commandLength==0) return false;
            try {
                for(int i=2; --i>=0;) {
                    if ((i==0 ? "ECLIPSE":"J_JAVA_EDITOR").equals(execv[0])) {
                        for(int iF=execv.length;--iF>=1;) success=success||(i==0 ? new FileEditorEclipse() : new J_JavaEditor()).editFile(file(execv[iF]),-1);
                        return success;
                    }
                }
            } catch(Exception e){ stckTrc(e);}

            if (win && 0!=(opts&CYGWIN_DLL)) {
                if (_vEnv==null) _vEnv=new Vector();
                final int i=idxOfPfx("PATH=",_vEnv);
                if (i>=0) _vEnv.set(i,_vEnv.get(i)+";"+dirCygwin());
                else _vEnv.add("PATH="+dirCygwin()+"\\bin");
            }
            final String env[]=strgArry(_vEnv);
            final Process pp[]={null};
            if (_running!=null) _running[0]=false;
            _running=new boolean[]{false};
            startThrd(thrdCR(this,"STREAM",new Object[]{pp,       null, _running, _vBytes, _refTA,         null,  _refButtons, "E"}));
            startThrd(thrdCR(this,"STREAM",new Object[]{pp,_stdoutDone, _running, _vBytes, _refTA, _printlnStdout,_refButtons, "O"}));
            if (_inString!=null) { startThrd(thrdCR(this,"WRITE",new Object[]{_inString,_running})); _inString=null;}
            final String envNull[]=env.length>0?env:null;
            final String explain=ChExecSecurity.describeExec(opts,execv,envNull,dir);
            log().a("\n").a(PFX).aln(explain).send();
            if (ta!=null) ta.a(explain);
            final boolean
                stdSoftware=strEquls("/usr/bin/", execv[0]) || isMac() && strEquls("/Applications/", execv[0]),
                ask=withGui() && !stdSoftware && withGui() && 0==(opts&ChExec.WITHOUT_ASKING);
            final Process process=pp[0]=
                ask ? ChExecSecurity.runtimeExecS(opts, execv,envNull,dir) :
                Insecure.runtimeExec(execv,envNull,dir);
            if (myComputer() || 0!=(DEBUG&opts) || !withGui()) {
                final BA sb=new  BA(99).a(ANSI_FG_GREEN+"ChExec"+ANSI_RESET+"  cd ").a(dir).a(';');
                for(String s : execv) sb.a(' ').aQuoteIfNecessary(s);
                putln(sb);
            }
            _process=wref(process);
            if (process==null) return false;
            _running[0]=_launched=true;
            log().a(PFX).aln("Running ...").send();
            _icon=IC_RUNNING;
            final ChButton butStatus=get(STATUS,_refButtons,ChButton.class);
            if (butStatus!=null) butStatus.i(IC_RUNNING);
            try { process.waitFor(); } catch(Exception e) { log().a(PFX).a(RED_CAUGHT_IN).a("waitFor() ").a(e).send();}
            _running[0]=false;
            _exitV=process.exitValue();
            rmFromParent(deref(_center));
            baTmp.clr().a("Exit_status=").a(_exitV).a('\n');
            log().a(PFX).a(baTmp).send();
            if (ta!=null) ta.a(baTmp);
            if (_exitV!=0) {
                if (!endWith("dialign-t.exe", execv[0]) && !endWith("dialign-t.exe", get(1,execv))) return false;
                disableKill("terminated != 0","");
                if (win && (opts&CYGWIN_DLL)!=0 && !file(dirCygwin()+"\\bin").isDirectory()) {
                    if (_msgCygwin==null) {
                        _msgCygwin=pnl(VBHB,"To run this external program, you need to install cygwin!","WIKI:Cygwin for details ",Microsoft.newCygwinButton(null));
                    }
                    error("installCygwin",_msgCygwin);
                }
            }
            disableKill("success",IC_HAPPY);

            return success=true;
        } catch(IOException e){
            final String nr="Failed to run the program ";
            baTmp.clr().aln("\nterminated").aln(e);
            if (ta!=null) ta.a(baTmp);
            log.a(PFX).a(baTmp).send();
            if (0!=(opts&PLEASE_INSTALL_MANUALLY)) error(nr+execv[0]+"<br>Please install manually.");
            return false;
        }
        finally {
            final boolean isR[]=_running;
            if (isR!=null) isR[0]=false;
            _finished=true;
            closeStrm(_bufStdin);
            recordSuccess(execv[0], success);
        }
    }
    public ChExec waitFor(){
        try {
            while(!isRunning() && !finished() && !couldNotLaunch()) { Thread.sleep(99); }
        } catch(InterruptedException ex){}
        return this;
    }
    /* <<< Exec <<< */
    /* ---------------------------------------- */
    /* >>> GUI >>> */
    private final Object[] _refTA=new Reference[4], _refButtons=new Reference[4],  _frames=new Reference[4];
    private ChFrame _fOutErr;
    private static JComponent _msgCygwin;
    private void disableKill(String status,String icon) {
        final ChButton bK=get(KILL,_refButtons,ChButton.class), butStatus=get(STATUS,_refButtons,ChButton.class);
        if (bK!=null) {
            rmFromParent( derefArray(gcp(KEY_CLONES,bK), ChButton.class));
            rmFromParent(bK);
            setEnabld(false,bK);
        }
        _label=status;
        _icon=icon;
        if (butStatus!=null) butStatus.t(status).i(icon);
    }
    /* <<< GUI <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        for(int i=0; i<4;i++) {
            if (_refButtons[i]==null || ev.getSource()!=get(i,_refButtons)) continue;
            if (i==KILL) kill();
            else if (i==OUT || i==ERR || i==STATUS) {
                ChTextView ta=(ChTextView)get(i,_refTA);
                if (ta==null) {
                    ta=new ChTextView(i==STATUS ? _sbStatus : new BA(999));
                    _refTA[i]=wref(ta);
                    if (i==OUT || i==ERR)   ta.byteArray().join(_vBytes[i]);
                }
                final JComponent ctrlPnl=derefJC(_ctrl);
                final Window window= parentWndw(ctrlPnl);
                if (isRunning() && window!=null) {
                    if (window.getHeight()<500) window.setSize(window.getWidth(),500);
                    window.show();
                    JComponent center=derefJC(_center);
                    if (center!=null) ctrlPnl.remove(center);
                    _center=wref(center=scrllpn(ta));
                    ctrlPnl.add(center,BorderLayout.CENTER);
                    revalidateC(center);
                    addMoli(MOLI_REPAINT_ON_ENTER_AND_EXIT,center);
                    center.paintImmediately(0,0,99999,99999);
                } else {
                    ChFrame f=(ChFrame)get(i,_frames);
                    if (f==null) {
                        f=new ChFrame(i==OUT ? "Stdout" : i==ERR ? "Stderr" : "Status");
                        _frames[i]=wref(f);
                        addActLi(this, f);
                        f.ad(scrllpn(SCRLLPN_INHERIT_SIZE,ta)).shw(CLOSE_CtrlW_ESC|ChFrame.DISPOSE_ON_CLOSE|ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN);
                    }
                    f.shw();
                }
            }
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Control Panel >>> */
    @Override public Object getControlPanel(boolean real) {
        if (!real) return CP_EXISTS;
        JComponent ctrl=derefJC(_ctrl);
        if (ctrl==null) {
            final ChButton bb[]=new ChButton[4];
            for(int i=0;i<4;i++) {
                if (i==KILL && finished() || (i==OUT || i==ERR) && _vBytes[i]==null) continue;
                bb[i]=get(i,_refButtons,ChButton.class);
                if (bb[i]==null) {
                    final ChButton b=bb[i]=new ChButton().li(this).cp(KEY_AVOID_GARBAGE_COLLECT,this);
                    _refButtons[i]=wref(b);
                    if (i==STATUS) b.t(_label).i(_icon).tt("Status of the process");
                    else if (i==KILL) b.t("kill").tt("Kill the process");
                    else if (i==OUT)  b.t("stdout").tt("Standard output");
                    else if (i==ERR)  b.t("stderr").tt("Standard error output");
                    if (i==OUT || i==ERR) setEnabld(0<sze(_vBytes[i]), b);
                } else bb[i]=bb[i].cln();
            }
            ctrl=pnl(CNSEW,null,pnl(bb));
            _ctrl=wref(ctrl);
        }
        return ctrl;
    }
    /* <<< Control Panel <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    private static byte[] bytesToBytes(List v) {
        if (v==null) return null;
        if (v==WANT_DATA) return NO_BYTE;
        byte BB[]=null;
        final Object oo[]=v.toArray();
        if (oo.length==1 && oo[0] instanceof byte[]) return (byte[])oo[0];
        while(true) {
            int count=0;
            for(Object o : oo) {
                if (o instanceof byte[]) {
                    final byte[] bb=(byte[])o;
                    if (BB!=null) System.arraycopy(bb,0,BB,count,bb.length);
                    count+=bb.length;
                }
            }
            if (BB!=null) break; else BB=new byte[count];
        }
        return BB;
    }
    /* <<< Static utils <<< */
    /* ---------------------------------------- */
    @Override protected void finalize() throws Throwable{
        logFinalize(ANSI_GREEN+" ChExec ","");
        closeStrm(_bufStdin);
        super.finalize();
    }

}
