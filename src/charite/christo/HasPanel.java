package charite.christo;
/**
   Implemented by objects that have a  GUI.
   @author Christoph Gille
*/
public interface HasPanel {
    int NEW_PANEL=1, NO_NEW_PANEL=2;
    Object getPanel(int mode);

}
