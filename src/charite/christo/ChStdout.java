package charite.christo;
import java.io.*;
import java.util.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   <h3>Exceptions and the streams stdout and stderr </h3>

   Stdout and stderr are collected in files residing in
   ".StrapAlign/log/" unless Strap was started with
   the command line parameter "-stderr" and "-stdout".

   The Java-console can be switched on with <i>STRING:ChUtils#jControl()</i> in
   <i>STRING:ChUtils#dirJavaws()</i>.

   @author Christoph Gille

   OutOfMemoryError see http://www.javaspecialists.eu/archive/Issue092.html
*/
public class ChStdout extends PrintStream implements ActionListener {
    public final static String ACTION_DATA_TRANSFER="CS$$dqf";
    private final static byte[] OFME=nam(OutOfMemoryError.class).getBytes();
    private final static Set<String> _vMsgs=new HashSet();
    private final static File[] _logFile=new File['R'+1];
    private final static ChStdout _stream[]=new ChStdout['R'+1];
    private static BA _outOfMem;
    private static Object _taX,_cbSend;
    private static String _clas;
    private static int _line=0, _sent, _memReserve[]=new int[8*1024];
    private static ChStdout _inst;
    private static ChStdout inst() { if (_inst==null) _inst=printStream(' '); return _inst; }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> PrintStream >>> */
    private static ChStdout printStream(char type) {
        if (_stream[type]==null) _stream[type]=new ChStdout(type, type=='R');
        return _stream[type];
    }

    public static void addMsgOutOfMemoryError(String s) {
        if (_outOfMem==null) _outOfMem=new BA(999);
        if ( strstr(s,_outOfMem)<0) _outOfMem.a(s);
    }

    /* <<< PrintStream <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    private static File logFile(char O_or_R) {
        File f=_logFile[O_or_R];
        if (f==null) mkParentDrs(f=_logFile[O_or_R]=file(dirLog(), O_or_R=='O' ? "stdout.log" :  "stderr.log"));
        return f;
    }
    public static void redirect(char O_or_R){
        if (_stream[O_or_R]==null) {
            if (O_or_R=='O') {
                final BA msg=new BA(99).a("\nRedirecting stdout to ").a(logFile(O_or_R)).a(".\nThis can be turned off with the program option -stdout=t.");
                putln(msg);
                System.setOut(_stream[O_or_R]=printStream('O'));
                putln(msg);
            } else if (O_or_R=='R') System.setErr(_stream[O_or_R]=printStream('R'));
            else assrt();
            if (withGui()) inEdtLater(thread_toLogMenu(ChStdout.newButton(O_or_R), null, null) );
        }
    }

    public static void isErrorInMyCode(File f) {
        InputStream is=null;
        try { is=new FileInputStream(f); } catch(Exception e){putln(RED_CAUGHT_IN+"ChStdout#","isErrorInMyCode",e);}
        if (is==null) return;
        final ChInStream chis=new ChInStream(is,1024);
        final BA line=new BA(99);
        boolean trigger=false, sendReport=false;
        while( chis.readLine(line.clr())) {
            if (!is(SPC,line,0))  {
                trigger=strstr("Exception",line)>=0;
                continue;
            }
            if (trigger) {
                if (strstr("jPlugs",line)>0 || strstr("astex.MoleculeRenderer",line)>0) trigger=false;
                else if (strstr(_CC,line)>0) {
                    sendReport=true;
                    break;
                }
            }
        }
        if (sendReport) uploadErrorReport(readBytes(f),false, null);
        closeStrm(is);
    }

    public static ChButton newButton(char t) {
        final String tt="Standard error stream stdout and stderr unless started with the command line options -stderr or -stdout.";
        if ((t=='O'||t=='R') && _stream[t]!=null) return new ChButton(t=='O'?"Stdout":"Stderr").i(IC_CONSOLE).doViewFile(logFile(t)).tt(tt);
        return null;
    }

    private static BA getSourceLine(String s) {
        final int idx=strstr("at charite.christo",s)+3;
        final BA sb=new BA(5555);
        if (idx<3) return sb.a("lineNumber: -1");
        final int idxDollar=strchr('$',s,idx,MAX_INT), idxOpen=strchr('(',s,idx,MAX_INT);
        int idxEndClassName=-1;
        if (idxDollar>0 && idxDollar<idxOpen) idxEndClassName=idxDollar;
        else
            for(idxEndClassName=idxOpen;  idxEndClassName>idx && chrAt(idxEndClassName,s)!='.'; idxEndClassName--);
        if (idxEndClassName<0) return null;
        final String name=s.substring(idx,idxEndClassName);
        _clas=null;
        try { _clas=nam(Class.forName(name)); }catch(Throwable e){ putln(RED_CAUGHT_IN+"ChStdout#","getSourceLine ",e); return sb;}
        sb.a(ANSI_RED+"CLASS"+ANSI_RESET+"=").aln(_clas).a("EDT=").aln(_sent==1?'t':'f');
        sb.a("java.util.Arrays.useLegacyMergeSort").a('=').a(sysGetProp("java.util.Arrays.useLegacyMergeSort")).a(' ').aln(legacyMergeSort());
        final int idxColon=strchr(':',s,idx,MAX_INT);
        if (idxColon<0) return sb;
        sb.a("line number=").a(_line=atoi(s,idxColon+1)-1).a("\njava version=").aln(systProprty(SYSP_JAVA_VERSION)).a("compiled=").aln(dateOfCompilation());
        final BA ba=getJavaSrc(_clas);
        if (ba!=null) {
            final int ends[]=ba.eol();
            if (_line>0 && _line<ends.length) sb.a(ba.bytes(),ends[_line-1],ends[_line]).a('\n');
        }
        return sb;
    }
    public static void uploadErrorReport(CharSequence text, boolean isModal, boolean[] isUploading) {
        final String txt=toStrg(text);
        if (txt==null || _vMsgs.contains(txt)) return;
        final BA sb=new BA(9999).aln("Upload this text or mail it to christophgil@googlemail.com !\n").aln(txt)
            .a(day(0))
            .a(" OS_ARCH=").aln(systProprty(SYSP_OS_ARCH))
            .a(" OS_NAME=").aln(systProprty(SYSP_OS_NAME))
            .a(" DateOfCompilation=").aln(dateOfCompilation())
            .aln(getSourceLine(txt))
            .a("\nprgParas()= ").join(prgParas(),"  ").a('\n');
        _taX=new ChTextArea(7,70).t(sb);
        _cbSend=cbox(true, "Yes, send  this bug report", null, inst());
        wrte(file(dirLog(),"bugReport.log"),sb);
        final Object pUpload=pnl(VBHB,
                                 "<h2>An error has occured</h2>Please upload this report.<br>"+
                                 "The following text is transmitted to the Strap server:",
                                 scrllpn(_taX),
                                 pnl(HBL,_cbSend, isModal ? null : new ChButton("OK").li(inst()))
                                 );
        if(isModal) {
            ChMsg.msgDialog(0L, pUpload);
            if (isSelctd(_cbSend)) {
                startThrd(thread_uploadBug(toStrg(_taX), isUploading));
                _vMsgs.add(txt);
            }
        } else addActLi(inst(), new ChFrame("error").ad(pUpload).size(800,500).shw(0));
    }

    public static Runnable thread_uploadBug(CharSequence txt, boolean isUploading[]) {
        return thrdM("uploadBug",ChStdout.class, new Object[]{txt,isUploading});
    }
    public static CharSequence uploadBug(CharSequence txt, boolean isUploading[]) {
        final BA response=Web.getServerResponse(0L,URL_STRAP+"error.php"+DO_NOT_ASK,new Object[][]{{"error",txt}});
        putln(ANSI_GREEN+"SEND"+ANSI_RESET,response);
        if (isUploading!=null) isUploading[0]=false;
        return response;
    }

    public static void mayUploadSecurityViolations() {
        final String[] securityViolations=strgArry(Insecure.VIOLATIONS.values());
        if (securityViolations.length>0) {
            final boolean isUploading[]={true};
            uploadErrorReport(new BA(0).join(securityViolations,"\n-------------\n"),true, isUploading);
            Insecure.VIOLATIONS.clear();
            for(int i=0; i<30&&isUploading[0]; i++) sleep(100);
        }
    }

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        if (q==_cbSend) setEnabld(isEnabld(q), _taX);
        if ((cmd=="OK" || cmd==ACTION_WINDOW_CLOSING) && _taX!=null) {
            if (isSelctd(_cbSend)) startThrd(thread_uploadBug(toStrg(_taX), null));
            parentWndw(ev.getSource()).dispose();
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> PrintStream >>> */
    private final boolean _super;
    private final OutputStream _os;
    private final char _type;
    public ChStdout(char type, boolean dup) {
        super(type=='R'?System.err : System.out);
        _type=type;

        OutputStream os=null;
        if (type!=' ') try { os=new FileOutputStream(logFile(type)); } catch(Exception ex){}
        _os=os;
        _super=dup||_os==null;
    }
    @Override public void write(int b) {
        if (_super) super.write(b);
        if (_os!=null) try { _os.write(b); } catch (Exception ex) {}
    }
    @Override public void write(byte bb[],int offset,int len) {
        if (_super) super.write(bb,offset,len);
        if (_os!=null) try {  _os.write(bb,offset,len); } catch (Exception ex) {}
        testException(bb,offset,len);
    }
    @Override public void write(byte bb[]) {
        if (_super) super.write(bb,0,bb.length);
        if (_os!=null) try { _os.write(bb,0,bb.length); } catch (Exception ex) {}
        testException(bb,0,bb.length);
    }
    private void testException(byte bb[],int offset,int len) {
        if (_type!='R') return;
        if (strstr(OFME,bb,offset,offset+len)>=0) {
            _memReserve=null;
            error(new BA(999).a(OutOfMemoryError.class).aln(':').aln(_outOfMem).a("See ").a(URL_STRAP).aln("index2.html#HEAP\n for solution."));
        } else {
            if (_sent==0 && strstr(_CC,bb,offset,offset+len)>=0) {
                _sent=isEDT()?1:2;
                printStream('R').flush();
                inEDTms(thrdM("isErrorInMyCode",ChStdout.class, new Object[]{logFile('R')}),333);
            }
            if (strstr("CDataTransferer_dragQueryFile",bb,offset,offset+len)>=0) {
                handleActEvt(ChStdout.class,ACTION_DATA_TRANSFER,0);
            }
        }
    }

}
