package charite.christo;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

   The user can override default program settings by typing into the text area. The changes are persistently stored in the directory ~/.StrapAlign/.
   Text lines can be deactivated by inserting a "#" at the beginning of the line.

   <br><br>
   <b>Try until success strategy:</b> Usually there are different possible applications for a specific file type.
   For example PDF files may be opened with Evince Viewer, Acrobat Reader, Foxit Reader or Xpdf.

   Strap cannot know which applications are available on the
   computer. Therefore all applications in the list are tried one after the other until one succeeds.

   <br><br>
   <i>OS:W,<b>Windows: </b>
   The environment variable %PATH% is not systematically used in Windows.
   There are two workarounds:
   <ul>
   <li>Entering the full path to the exe file. White space in file paths needs replaced by <b>"%20"</b>.</li>
   <li>The prefix "<b>start</b>" is a mechanism
   to start applications without knowing the exact location on the file system.
   Note that the start command does not
   return a valid exit value and Strap cannot detect failure to start.
   </li>
   </ul>
   </i>

   <br><br>

   <b>Advanced:</b>  For using complex shell expressions, the
   command line should be started with <i>OS:W,CYGWINSH</i> <i>OS:OML,SH</i>

    <br><br>
    <sub>
    <b>For system administrators: Setting values by command line option or web variable:</b>
    Command-line options can be added by program parameters.
    These lines are added to the default or current text.
    The command line option is formed by appending the name of the
    customize dialog to the prefix "customizeAdd". For example to add rules for processing text patterns
    the following command line parameter is used
    -customizeAddScriptByRegex=<i>file or URL</i>.  If the program is started by Java Webstart, then
    "&customizeAddScriptByRegex=<i>URL-encoded web address</i>" is added to the Java-webstart link.
    </sub>
   @author Christoph Gille
*/
public final class Customize implements ActionListener, HasPanel, ChRunnable, HasMC {

    public final static int
        databases=Hyperrefs.PLAIN_TEXT,
        proteinDatabases=Hyperrefs.PROTEIN_FILE,
        webLinks=Hyperrefs.WEB_LINK,
        C_compiler=11,
        CplusPlus_compiler=12,
        latexEditors=13,
        dviPsConverter=14,
        dviViewer=15,
        fileBrowsers=16,
        fortran_compiler=18,
        htmlEditors=19,
        javaSourceEditor=20,
        laf=21,
        pdfViewer=22,
        proteinFileExtensions=23,
        psPoster=24,
        psViewer=25,
        textEditors=27,
        watchLogFile=28,
        webBrowser=29,
        error=33,
        pdbSite=34,
        pdflatex=35,
        latex=37,
        openFileWith=38,
        scriptByRegex=39,
        execAminoSeq=40,
        execResidueSelection=41,
        ecClassLinks=42,
        webSearches=43,
        pdbLinks=44,
        uniprotLinks=45,
        testProxySelector=46,
        keggOrganisms=47,
        speciesIcons=48,
        cssAlignBrowser=49,
        seqFeatureColors=50,
        trustExe=51,
        refuseExe=52,
        strapApps=53,
        proteinInfoRplc=54,
        preferedDasSources=55,
        MAX_ID=56,
        buttonFileViewers=101,
        buttonWeb=102,

        EXECUTABLES=1002,  LATEX=1003, LIST=1005, JAVA_CLASSES=1006, SHELL_SCRIPT=1011,
        MAX_VARIABLE=1111,
        FLAG_SKIP_COMMENTS=1<<17, FLAG_INTERN=1<<18, FLAG_REPLACE=1<<19, FLAG_NOTIFY_IMMEDIATELY=1<<20;

    private final int _type, _opt;
    private final String _name, _default[],  _help;
    private final boolean _notifyImmediately;
    private final static Customize CUSTOMIZE[]=new Customize[MAX_ID+1];
    private static volatile String[] _names;
    private int _mc, _mcHidden;
    public final static Customize[] NONE={};
    public int mc() { return _mc;}

    private static String[] names() {
        if (_names==null) _names=finalStaticInts(Customize.class, MAX_VARIABLE);
        return _names;
    }
    @Override public String toString() { return _name+"."+ get(_type,names());}
    private List<String> _vHidden, _vSettings;
    /* ---------------------------------------- */
    /* >>> Instance >>> */
    final static Map<String,Customize> mapInstances=new HashMap();
    public Customize(String name, String[] defaultSetting, String help, int type) {
        _notifyImmediately= (type&FLAG_NOTIFY_IMMEDIATELY)!=0;
        _name=name;
        _type=type&0xffff;
        _opt=type&~0xffff;
        _help=help;
        if ((type&FLAG_REPLACE)!=0) { addHiddenEntries(false, defaultSetting); _default=NO_STRING; }
        else _default=defaultSetting;
        mapInstances.put(name,this);
        downloadFiles(new String[]{name});
    }

    private boolean _needUpdate, _needSave;
    private void myUpdate() {
        getSettingsV();
        handleActEvt(this, ACTION_CUSTOMIZE_CHANGED,0);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Parsing text >>> */
    private static String cmdOpt(String name) {
        return toStrg(new BA(99).a("-customizeAdd").a((char)(name.charAt(0)&~32)).a(name,1,MAX_INT).a('='));
    }
    private final static Set<String>  _vDL=new HashSet();
    public static void downloadFiles(String namesOfCustomize[]) {
        final String names[]=namesOfCustomize!=null ? namesOfCustomize : names();
        if (names==null) {
            stckTrc();
            return;
        }
        for(String name : names) {
            if (_vDL.contains(name)) continue;
            _vDL.add(name);
            final String opt=cmdOpt(name);
            for(String arg : prgOpts()) {
                final String param=delPfx(opt, arg);
                if (param!=arg && looks(LIKE_EXTURL,param)) {
                    final File f=file(param);
                    final java.net.URL url=url(param);
                    if (f!=null && url!=null && 0==isUpToDate(f,url)) {
                        f.delete();
                        startThrd(thrdRRR(thread_urlGet(url,0), thread_afterDL(name)));
                    }
                }
            }
        }
    }

    private static Runnable thread_afterDL(String name) {
        return thrdM("afterDL", Customize.class,new Object[]{name});
    }
    public static void afterDL(String name) {
        final Customize c=mapInstances.get(name);
        if (c!=null) {
            c._mcHidden++;
            handleActEvt(c, ACTION_CUSTOMIZE_CHANGED,0);

        }
    }
    private String[] linesFromParams() {
        Collection<String> v=null;
        final String opt=cmdOpt(_name);
        for(String a : prgOpts()) {
            final String param=delPfx(opt,a);
            if (param==a) continue;
            final String[] lines=readLines(file(param));
            if (sze(lines)>0) adAll(lines,v==null ? v=new ArrayList() : v);
        }
        return strgArry(v);
    }

    public synchronized List<String> getSettingsV() {
        final BA txt=toBA(_ta);
        final int mc=_mcHidden+modic(txt);
        List<String> v=_vSettings;
        if (v==null || _mc!=mc) {
            _mc=mc;
            _txt4completion=null;
            _highlights=null;
            _settings=null;
            final BA taOrSaved=txt!=null?txt : readTxt();
            final String[]
                add1=strgArry(_vHidden),
                add2=linesFromParams(),
                add3=taOrSaved!=null ? splitTokns(SPLIT_TRIM, taOrSaved,0,MAX_INT, chrClas1('\n')) : _default;
            if (v==null) _vSettings=v=new Vector(sze(add1)+sze(add2)+sze(add3)); else v.clear();

            for(int iTxt=3;--iTxt>=0; ) {
                final String[] lines=iTxt==0 ? add1 : iTxt==1 ? add2 : add3;
                if (sze(lines)==0) continue;
                for(String s : lines) {
                    if (s==null) continue;
                    s=s.trim();
                    if ((_opt&FLAG_INTERN)!=0) s=s.intern();
                    if ((_opt&FLAG_SKIP_COMMENTS)!=0 && chrAt(0,s)=='#') continue;
                    if (sze(s)>0) v.add(s);
                }
            }
        }
        return v;
    }
    public void addHiddenEntries(boolean uniq, String... ss) {
        if (_vHidden==null) _vHidden=new Vector(ss.length);
        if ( adAll(uniq, ss, _vHidden)) _mcHidden++;
    }
    private String[] _settings;
    public synchronized String[] getSettings() {
        final List v=getSettingsV();
        String ss[]=_settings;
        if (ss==null) _settings=ss=strgArry(v);
        return ss;
    }
    public String getSetting() {
        for(String s:getSettings()) if (sze(s)>0) return s;
        return "";
    }
    /* <<< Parsing text <<< */
    /* ---------------------------------------- */
    /* >>> GUI >>> */
    private JComponent _pMain, _butSave, _butReset;
    private ChTextArea _ta;
    private JFileChooser _fs;
    private synchronized ChTextArea ta() {
        if (_ta==null) {
            _ta=new ChTextArea("");
            final BA saved=readTxt();
            _ta.t(saved!=null?saved:new BA(0).join(_default).a('\n')).tools().underlineRefs(ULREFS_NOT_CLICKABLE|ULREFS_WEB_COLORS);
        }
        return _ta;
    }
    public synchronized JComponent getPanel(int options) {
        if (_pMain==null && options!=NO_NEW_PANEL || options==NEW_PANEL) {
            final ChTextArea ta=ta();
            pcp(ACTION_FOCUS_GAINED,"",ta);
            ta.setLineWrap(false);
            addActLi(this, ta.tools().enableWordCompletion("").enableUndo(true));
            final int t=_type;
            final Object
                pButtons=pnl(HB,
                            helpBut(Customize.class).t("Help"),
                            new ChButton("H").li(this).t("About "+_name),
                            new ChButton("DS").li(this).t("See default settings"),
                            _butSave=new ChButton("Save").li(this).tt("Write to file"),
                            _butReset=new ChButton("Reset ...").li(this),
                            t==SHELL_SCRIPT||t==EXECUTABLES ?
                            new ChButton("S").t("File browser").li(this).tt("Search on HD") : null
                            );
            setEnabld(false,_butSave);

            final String type=
                t==EXECUTABLES ? "Program application. If more than one is given, then the first one that is installed, is taken." :
                t==SHELL_SCRIPT ? "List of command lines that are executed one after the other." :
                t==LATEX ? "LaTeX code" :
                t==JAVA_CLASSES ? "List of Java classes" :
                "List of text lines.";
            _pMain=pnl(CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE,ta())," Type: "+type, pButtons);
            if (_parent!=null) _pMain=pnl(CNSEW, _pMain,null,pnl(REMAINING_VSPC1));
            addActLi(this,ta);
            TabItemTipIcon.set(_name,_name,_help,null, _pMain);
        }
        return _pMain;
    }
    public static ChButton newButton(int id) { return newButton(new int[]{id}); } /* Required for Html embedded */
    public static ChButton newButton(int... IDs) {
        final List<Customize> v=new ArrayList();
        String t=null;
        for(int id : IDs) {
            int ids[]=null;
            switch(id) {
            case buttonWeb:
                ids=new int[]{databases, webLinks, proteinDatabases, webSearches, uniprotLinks, ecClassLinks};
                t="Web resources";
                break;
            case buttonFileViewers:
                ids=new int[]{webBrowser, fileBrowsers, pdfViewer, textEditors, watchLogFile};
                t="File viewers";
                break;
            }

            if (ids==null) adUniq(customize(id),v);
            else for(int i : ids) adUniq(customize(i),v);
        }
        return newButton(toArry(v,Customize.class)).t(t!=null?t:"Customize");
    }

    public static ChButton newButton(Customize... cc) {
        if (_inst==null) _inst=new Customize();
        final ChButton b=new ChButton("DIALOG").t("Customize").li(_inst).cp("CC",cc);
        final BA sb=new BA("Customize dialog for<ul>");
        for(int i=0; i<sze(cc);i++) {
            if (cc[i]!=null) sb.a("<li>").a(cc[i]._name).a("</li>");
        }
        return b.tt(sb.a("</ul>")).i(IC_CUSTOM);
    }
    public ChCombo newComboBox() {
        return new ChCombo(_type==JAVA_CLASSES ? ChJTable.CLASS_RENDERER:0, getSettingsV() ).li(this);
    }
    /* <<< GUI <<< */
    /* ---------------------------------------- */
    /* >>> static GUI >>> */
    private static JComponent _panel;
    private static ChTabPane _parent, _tabP;
    private static ActionListener _li;
    private Customize(){ _opt=_type=0; _name=_help=""; _default=null; _notifyImmediately=false;}
    private static Customize _inst;
    public static void setParent(ChTabPane tp, ActionListener li) { _parent=tp; _li=li;  }
    public static void addDialog(Customize... cc) {
        if (cc==null) return;
        for(Customize cus : cc) {
            if (cus==null) continue;
            if (_panel==null) {
                final Object pTop=pnl(HB,"#","<h2>Preferences</h2>","#",panHS(Customize.class));
                addActLi(_li,_tabP=new ChTabPane(ChTabPane.RIGHT));
                _panel=pnl(CNSEW,_tabP,pTop);
                TabItemTipIcon.set(null,null,null,IC_CUSTOM,_panel);
            }
            if (_parent==null) ChFrame.frame("Customize",_panel,0).size(600,400).shw(0);
            else _parent.addTab(CLOSE_ALLOWED, _panel);
            _tabP.addTab(CLOSE_CtrlW, cus.getPanel(0));
        }
    }
    static Map mapSettings() { return _map;}
    private final static Map<Object,Object> _map=new WeakHashMap();
    private final static List<Customize> _v=new Vector();
    public static Customize[] cc(Object o) {
        if (o==null) return NONE;
        _v.clear();
        Object settings=mapSettings().get(o);
        final Class clas=o instanceof Class ? (Class)o : o.getClass();
        if (settings==null && o!=clas) settings=mapSettings().get(clas);
        for(int i=sze(settings); --i>=0;) {
            final Customize c=
                settings instanceof Customize[] ? ((Customize[])settings)[i] :
                settings instanceof int[] ? customize(((int[])settings)[i]) : null;
            adUniq(c,_v);
        }
        for(int i=CLASS_Example; i<=CLASS_LateCommands; i++) {
            adUniq(getCustomizableForClass(clas,i),_v);
        }
        return toArryClr(_v,Customize.class);
    }
    /* <<< static GUI <<< */
    /* ---------------------------------------- */
    /* >>> ActionEvent >>> */
    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        if (cmd=="H") {
            if (_help=="C") new ChJTextPane(BasicExecutable.msgInstallCompilers()).tools().showInFrame("Installation from source code");
            else new ChJTextPane("<h2>Help for </h2>\n"+_help).tools().showInFrame("").underlineRefs(ULREFS_NO_ICON);
        }
        if (cmd=="DIALOG") Customize.addDialog((Customize[]) gcp("CC",q));
        if (_ta!=null && q!=null) {
            if (cmd=="DS") {
                final BA sb=new BA(0).join(_default);
                if (sze(_default)==0) sb.join(_vHidden);
                new ChTextView(sb).tools().showInFrame("Default settings").underlineRefs(ULREFS_NOT_CLICKABLE);
            }
            if (q==_butReset) {
                if (!ChMsg.yesNo("Do you really want to revert to original settings and discard your changes?")) return;
                _ta.t(new BA(0).join(_default).a('\n'));
                delFile(f());
                setEnabld(false, _butSave);
                setEnabld(false, _butReset);
                myUpdate();
                if (isCtrl(ev)) {
                    final BA txt=toBA(_ta);
                    final byte[] bb=txt.bytes();
                    final int end=txt.end();
                    int i=_ta.getCaretPosition();
                    while( --i>=0 && bb[i]!='\n'){}
                    int to=i;
                    while( ++to<end && bb[to]!='\n'){}
                    startThrd(new ChExec(0).setCommandLine(bytes2strg(bb,i+1,to-1)).showStdoutAndStderr(null));
                }
            }
            if (cmd=="S") {
                if (_fs==null) _fs=new ChFileChooser(ChFileChooser.CLOSE, "Customize");
                _fs.setMultiSelectionEnabled(false);
                if (_fs.showOpenDialog(SwingUtilities.getRootPane(getPanel(0)))!=JFileChooser.APPROVE_OPTION) return;
                ChTextComponents.insertAtCaret(0L, rplcToStrg(" ","%20",toStrg(_fs.getSelectedFile())), _ta);
            }
            if (q==_ta) {
                if (cmd==ACTION_TEXT_CHANGED) {
                    _needUpdate=true;
                    setEnabld(true, _butSave);
                    setEnabld(true, _butReset);
                }
                if (_needUpdate) {
                    if (_notifyImmediately  || (cmd==ACTION_FOCUS_LOST || cmd==ACTION_FOCUS_GAINED)) {
                        _needUpdate=false;
                        myUpdate();
                    }
                }
            }
            if (q==_butSave) save(true);
        }
    }
   /* <<< ActionListener <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */

    private BA readTxt() { return readBytes(f());}
    private File f() { return file("~/@/customize/"+_name);}
    public void save(boolean always) {
        if (always || _needSave) {
            final ChTextArea ta=_ta;
            wrte(f(), ta!=null ? ta.getText() : new BA(0).join(getSettings()).a('\n'));
            setEnabld(false, _butSave);
        }
    }

    public synchronized void addLine(String s) {
        if (adUniq(s,getSettingsV())) {
            _settings=null;
            if (_ta!=null) inEDT(thrdCR(this, "A",s));
            _needSave=true;
        }
    }
    // }
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> Create defaults >>> */
    public static Customize customize(int id) {
        if (id<1) return null;
        if (CUSTOMIZE[id]!=null) return CUSTOMIZE[id];
        final boolean isWin=isSystProprty(IS_WINDOWS), isMac=isMac(), isWin95=isSystProprty(IS_WINDOWS_95_FAMILY);
        final String
            notepadPP="%ProgramFiles%/Notepad++/Notepad++\n",
            INFO_J="<br>J_JAVA_EDITOR denotes an editor with syntax highlighting which will be automatically installed.<br>ECLIPSE denotes the Eclipse IDE.",
            J_ED="J_JAVA_EDITOR\n";
        final boolean addUsrBin=!isWin();
        final String h;
        final Object ss;
        final int type;
        final String ms_fph="\nrundll32 url.dll,FileProtocolHandler\n", ms_start=isWin95 ? "\nstart " : "cmd.exe /c start ";
        if (id==openFileWith|| id==execResidueSelection||id==execAminoSeq) {
            ss=adToStrgs("SH echo $* > /tmp/t;xterm -e less /tmp/t",customize(textEditors).getSettings(),0);
            final String what= id==execAminoSeq ? "amino acid sequences" : id==execResidueSelection ? "residue selections" : " files";
            h="Shell programs to process "+what+". Used for context menu of "+what+".";
            type=LIST;
        } else if (id==scriptByRegex) {
            h="Three columns: <ol>"+
                "<li>Display name</li>"+
                "<li>Regular expression, that can match or not matchShell programs to process </li>"+
                "<li>Shell command/Web URL which can be executed if regular expression matches.</li>"+
                "</ol>";
            ss=
                "#Tab-separated Columns (Type \t or Ctrl+I for tab):\n"+
                "#  1st Column: Display name *\n"+
                "#  2nd Column: A regular expression *\n"+
                "#  3rd Column: A shell script *\n\n"+
                "demo/Example: Matches any\t.*\t echo Hello, this matches any *\n"+
                "demo/Example google\t.*\thttp://www.google.com/search?q=* \n"+
                "demo/Example: Matches starting with M\t^M.*$\t echo starts with letter M *\n"+
                "demo/Example: Matches numbers\t^.*[0-9]$\t echo ends with a digit *\n"+
                "demo/Example: local host port\t.*\t http://localhost:1234?blabla_*\n"+
                "util/GEO_Datasets\t^ENSG[0-9][0-9].*\t fgrep 'ARGUMENT' ~/testMetannogenExpr/GEO_Datasets.txt\n";
            type=LIST|FLAG_REPLACE;
        } else if (id==strapApps) {
            final BA html=readBytes(rscAsStream(0L, _CCS, "appsResponse.html"));
            final int pre=strstr("<pre",html), erp=strstr("</pre>",html);
            final String DP=URL_STRAP+"plugins/demoApplication.php";
            final BA hlp=new BA(999)
                .aln("#Tab-separated Columns (Type Ctrl+I to insert a tab):")
                .aln("#  1st Column: Display name *")
                .aln("#  2nd Column: A shell script or URL *\n")
                .tab("demo/Web/Simple Example").aln(DP)
                .tab("demo/Web/Example with PDB-file content").a(DP).aln("?-CALPHA_COORDINATES_IF_SELECTED-")
                .tab("demo/Web/Example with server cache").a(DP).aln("?-SERVER_CACHE+CALPHA_COORDINATES_IF_SELECTED-");

            if (pre<0 || erp<0) assrt();
            else {
                for(int i=0; i<3; i++) {
                    final CharSequence script;
                    if (i==2) script="echo aa_sequence ThisIsTheSequence , myProtein\n";
                    else {
                        final File f=file(dirTmp()+"/demo/script"+(1+i)+".html");
                        wrte(f, html);
                        script=new BA(999).a(isWin ? "@type " : "cat ").aln(f);
                        Arrays.fill(html.bytes(),pre,erp+6, (byte)' ');
                    }
                    final File app=file(dirTmp()+"/demo/script"+(i+1)+(isWin?".bat":".sh"));
                    wrte(app,script);
                    mkExecutabl(app);
                    hlp.a("demo/Scripts/Example ").tab(i==0?"1 - HTML page and script" : i==1?"2 - Only HTML page" : "3 - Only script").tab(app).a('\n');
                }
            }

            h="Two columns: <ol>"+
                "<li>Display name</li>"+
                "<li>Shell command/Web URL which can be executed with the status file as argument.</li>"+
                "</ol>";
            ss=hlp;

            type=LIST;
        } else if (id==pdflatex || id==latex) {
            final String P=id==latex ? "latex" : "pdflatex", NS=" --interaction=nonstopmode\n";
            ss=isWin ? P+NS+"CYGWINSH "+P+NS :
                isMac ?
                "/usr/texbin/"+P+NS+"\n"+
                P+NS+
                "/usr/local/teTeX/bin/powerpc-apple-darwin-current/"+P+"\n":
                P+NS;
            type=EXECUTABLES;
            h="<b>The command "+P+" takes  a latex-file (ending .tex) as input and produces a "+(id==latex ?"PostScript":"PDF")+" file. <br>LaTeX home page: http://www.latex-project.org";
        } else if (id==latexEditors || id==textEditors) {
            ss=isWin ? notepadPP+ms_start+"wordpad\n"+ms_start+"winword\n"+ms_start+"edit.com\n"+J_ED :
                isMac ? "open -e\n"+J_ED :
                "gnuclient\nkwrite\nkedit\ngedit\nemacs\nxemacs\nnedit\n# xterm -e vi\n# xterm -e pico\n"+J_ED;
            h="Text editor. <br>"+INFO_J;
            type=EXECUTABLES;
        } else if (id==databases || id==proteinDatabases || id==webLinks) {
            type=LIST|FLAG_REPLACE;
            h="Expressions of the form DATABASE:identifier will be translated into URLS.<br>"+
                "E.g. PDB:1RYP is translated into a link for the PDB-database.<br>"+
                "If an asterisk is present, then it is replaced by the ID otherwise the ID is appended at the end.\n"+
                "The user can enter new URLs or override the default settings.\n";
            ss=readLines(rscAsStream(0L, _CC, get(id,names())+".rsc"));
        } else if (id==speciesIcons) {
            type=LIST|FLAG_REPLACE;
            h=
                "Mapping of species names to icon files.<br>"+
                "The text contains lines with base URLs.<br>"+
                "The other lines contain two columns separated by space: "+
                "<ol><li>scientific-species-name</li><li>icon file</li></ol>\n";
            ss=readLines(rscAsStream(0L, _CC,"speciesIcons.rsc"));
        } else if (id==proteinInfoRplc) {
            type=LIST;
            h="Text replacement lines to render the protein information such as organsim name and protein name more compact.<br>"+
                "Each text line  contains a regular expression and an optional replacement text, separated by tab (Type Ctrl-I for tabulator).<br>"+
                "If the regular expression matches a part of the text, it is deleted or replaced with the text right from the tab.";
            ss=" subunit \t ";
        } else if (id==cssAlignBrowser) {
            type=LIST;
            ss=readLines(rscAsStream(0L, _CCP,"toHTML.css"));
            h="Modify the text to get the optimal layout for the browser / Textprocessor.";
        } else if (id==seqFeatureColors) {
            type=Customize.LIST|Customize.FLAG_NOTIFY_IMMEDIATELY;
            h="# Web_colors of features for black and white background.";
            ss=readLines(rscAsStream(0L, _CCS,"SequenceFeatures.rsc"));
        } else if (id==trustExe) {
            type=Customize.LIST;
            h="Trusted native programs (i.e. executables or exe's)";
            ss=new String[]{
                //                "~/@/bin/"+ChExec.EXE_CLUSTALW+"/"+ChExec.EXE_CLUSTALW+".exe",
                "~/@/bin/"+ChExec.EXE_TM_ALIGN+"/"+ChExec.EXE_TM_ALIGN+".exe"
            };
        } else if (id==refuseExe) {
            type=Customize.LIST;
            h="Native programs (i.e. executables or exe's) which should not be run because you do not trust them.";
            ss=NO_STRING;
        } else switch(id) {
            case keggOrganisms:
                type=LIST|FLAG_REPLACE;
                h="Kegg Organisms";
                ss=readLines(rscAsStream(0L, _CCM,"keggOrganisms.rsc"));
                break;
            case psPoster:
                ss="poster -mA4";
                h="Scale and tile a postscript image to print on multiple pages\nDownload poster at http://www.geocities.com/SiliconValley/5682/poster.html";
                type=EXECUTABLES;
                break;
            case pdfViewer:
                ss=isWin ? ms_fph+"cmd /C\"start c:\\document.doc\"\ncommand /C\"start c:\\document.doc\"\nacroread" :
                    isMac ? "open" : "evince\nxpdf\nacroread";
                h="PDF-viewer.";
                type=EXECUTABLES;
                break;
            case dviViewer:
                ss=isWin ? ms_fph : "xdvi\nkdvi";
                h="DVI is a document format generated by latex and  converted  to PostScript with dvips.";
                type=EXECUTABLES;
                break;
            case psViewer:
                ss=isWin ? "CYGWINSH gv\n"+ms_fph+ms_start+"psview\n" : "gv\nkgv\nggv\nkghostview\nevince\n" ;
                h="PostScript is printable document format.\n";
                type=EXECUTABLES;
                break;
            case dviPsConverter:
                ss="dvips $* -o $*.ps\n"+(isWin ? "CYGWINSH dvips $* -o $*.ps" : "");
                h="Conversion dvi to PostScript ";
                type=SHELL_SCRIPT;
                break;
            case fileBrowsers:
                ss=isWin ? ms_start+"explorer \"$*\"" :
                    isMac ? "open -a Finder" :
                    "rox\nnautilus\npcmanfm\nthunar\nkonqueror\nkbear\nxfe\ngcmd\nemacs\ngnuclient\n"+
                    "sh -c xterm%20-title%20*%20-e%20'cd%20*;ls%20-l;/bin/sh'\n"+
                    "xfce4-terminal --title * --working-directory=*\n"+
                    "gnome-terminal --title * --working-directory=*\n"+
                    "konsole -T * --workdir *\n";

                h="File browser ";
                type=EXECUTABLES;
                break;
            case htmlEditors:
                ss=isWin ? ms_start+"winword\n"+ms_start:
                isMac ? "open -a Microsoft%20Word\n"+
                    "/Applications/KompoZer.app/Contents/MacOS/kompozer\n"+
                    "open -e \n"+
                    "# open -a pages" :
                    // SEEdit, rapidweaver, sandvox nicht mit html file als argument
                    // amaya CSS probleme
                    // dreamweaver (adobe) zu gross
                    "gwrite\nlibreoffice\nooffice3\nooffice2\nooffice\nopenoffice\nsoffice\nabiword"+"\n# do not know";
                h="Word-Processor like Openoffice or MS-Word";
                type=EXECUTABLES;
                break;
            case javaSourceEditor:
                ss=isMac||isWin ? notepadPP+"#ECLIPSE\n"+J_ED :
                "#ECLIPSE\ngnuclient\nkwrite\ngedit\nkedit\n"+J_ED;
                h="Editor for  Java source text.<br>"+INFO_J;
                type=EXECUTABLES;
                break;
            case webBrowser:
                final String expl="EXPLORER.EXE \"$*\"\n";
                ss=isWin ? (isWin95?expl:ms_fph)+(isWin95?ms_fph:expl)+
                    ms_start+"netscape \"$*\"\n"+
                    ms_start+"chrome \"$*\"\n"+
                    ms_start+"firefox \"$*\"\n"+
                    "cmd.exe /c %ProgramFiles%\\Opera\\Opera.exe  \"$*\"\n"+
                    ms_start+"mozilla \"$*\"" :
                    isMac ? "open\nopen -a Safari":
                    "chromium\nfirefox\niceweasel\nicecat\ngnome-www-browser\nkonqueror\nmozilla\nopera\nnetscape\nepiphany";
                h="A web browser.";
                type=EXECUTABLES;
                break;

            case laf:
                final UIManager.LookAndFeelInfo lafs[]=UIManager.getInstalledLookAndFeels();
                final BA sb=new BA(333);
                for(int i=lafs.length; --i>=0;) sb.aln(lafs[i].getClassName());
                ss=sb.toString();
                h="Look and Feel";
                type=JAVA_CLASSES;
                break;
            case proteinFileExtensions:
                ss=
                    ".pdb .ent .dssp   # Structure file\n"+
                    ".swiss .swissprot .sw .swp   # Swissprot sequence file\n"+
                    ".embl   # EMBL sequence file\n"+
                    ".genbank .gb   # Genbank (see menu 'translate dna ..')\n"+
                    ".dna .nt   # Nucleotides a,c,t,g\n"+
                    ".fa .fasta .mfa   # Fasta-format\n"+
                    ".ascii .txt .text .seq  # Plain text\n"+
                    ".msf .clustalw .aln .pir   # Multiple sequence file containing more than one sequence\n"+
                    "PD* PF*   # Prodom and PFAM alignment files\n"+
                    "other # Any other extension";
                h="File extensions do not influence the way <br>the proteins are parsed !!";
                type=LIST;
                break;
            case watchLogFile:
                ss=isWin ? ms_start+"Firefox \"$*\"\n"+ms_fph+ms_start+"EXPLORER \"$*\"" :
                    isMac ? "/Applications//Utilities/Console.app/Contents/MacOS/Console " :
                    "SH (xterm -title less_$* -e less -r $*&);(xterm -title tail_-f_$* -e tail -n 999 -f $*)" ;
                h="shell command to watch a log file";
                type=EXECUTABLES;
                break;
            case C_compiler:
                ss="cc -lm -O2";
                h="C";
                type=SHELL_SCRIPT;
                break;
            case CplusPlus_compiler:
                ss="c++ -lm -O2";
                h="C";
                type=SHELL_SCRIPT;
                break;
            case fortran_compiler:
                ss=(!isWin && cmdExists("gfortran") ? "gfortran" : !isWin && cmdExists("f95") ? "f95" : "f77")+" -O2";
                h="C";
                type=SHELL_SCRIPT;
                break;
            case pdbSite:
                final String dir= isWin ? "\\myPDB\\pdb\\??\\pdb????.ent" : "/pdb/??/pdb????.ent";
                ss=
                    "http://www.rcsb.org/pdb/files/????.pdb.gz\n"+
                    "ftp://pdb.protein.osaka-u.ac.jp/v3/pub/pdb/data/structures/divided/pdb/??/pdb????.ent.gz\n"+
                    "ftp://ftp.wwpdb.org/pub/pdb/data/structures/divided/pdb/??/pdb????.ent.gz\n"+
                    "ftp://ftp.ebi.ac.uk/pub/databases/rcsb/pdb-remediated/data/structures/divided/pdb/??/pdb????.ent.gz\n"+
                    dir+"\n"+
                    dir+".Z";
                h="the location of the pdb-file collections.<br>Files and URLs are allowed.";
                type=LIST;
                break;
            case ecClassLinks:
                h="A list of URLs for creation external links by EC number. Asterisk is place-holder for EC number.";
                ss="STRAPec{*}\nMETACYCec{*}\nNISTec{*}\nBRENDAec{*}\nREACTOMEec{*}\nEXPASYec{*}\nKEGGec{*}\nIUBMBec{*}\nUMBBDec{*}\nUNIPROT{*}\nENSG{*}\nSABIORKec{*}";
                type=LIST;
                break;
            case webSearches:
                h="A list of URLs for looking up a word or phrase. Used in context menu of text components.";
                ss=" WIKIPEDIA{*}\n GOOGLE{*}\n PUBMED{*}\n UNIPROT{*}\n TDB{*}\n ENSG{*}\n ISI{*}\n CITEXPLORE{*}\n METACYC{*}";
                type=LIST;
                break;
            case pdbLinks:
                h="A list of PDB links. Asterisk is place-holder for PDB id.";
                ss="PDBj:*\nPDB_SUM:*\nPDB_RCSB:*\nJENA:*\nPDBe:*\nVIPERDB:*";
                type=LIST;
                break;
            case uniprotLinks:
                h="A listUniprot links. Asterisk is place-holder for UNIPROT id.";
                ss="UNIPROT_EBI:* \n EXPASY:* \n BLAST{UNIPROT:*} \n LOC{UNIPROT:*} \nPFAM_PROTEIN:* \n DASTY:*";
                type=LIST;
                break;
            case preferedDasSources:
                h="List of default sources for loading sequence features.";
                ss="CSA - extended\nnetphos\nnetoglyc\ncbs_total\nuniprot\nFireDB";
                type=LIST;
                break;
            case testProxySelector:
                h="A list of Web-addresses to test what web proxy is assigned.";
                ss="http://www.google.de\n"+"http://localhost/\n";
                type=LIST;
                break;
            default:
                if (CUSTOMIZE[error]==null) CUSTOMIZE[error]=new Customize("error",NO_STRING,"",LIST);
                return CUSTOMIZE[error];
            }

        String ss0[]=ss instanceof String[] ? (String[])ss : splitLnes(ss);
        if (type==EXECUTABLES) {
            if (!isWin()) ss0=ChEnv.executablesReorder(ss0);
            if (addUsrBin) ss0=addUsrBin(ss0);
        }
        final Customize c=CUSTOMIZE[id]=new Customize(get(id,names()),ss0,h,type);
        if (id==webLinks) {
            final BA txt=readBytes(rscAsStream(0L, _CC,"webLinksH.rsc"));
            if (txt!=null) c.addHiddenEntries(false, splitLnes(txt.replace(0, "SWF/"," http://www.bioinformatics.org/strap/swf/")));
            else if (myComputer()) {
                stckTrc();
                System.exit(99);
            }
        }
        return c;
    }
    /* <<< Create defaults <<< */
    /* ---------------------------------------- */
    /* >>> Provide services >>> */
    private byte[][][] _highlights;
    private String[][] _txt4completion;
    public Object run(String id, Object arg) {

        if (id=="A") {
            final Object save=gcp(KOPT_NO_EVENT,_ta);
            pcp(KOPT_NO_EVENT,"",_ta);
            _ta.setText(toStrg(toBA(_ta).delBlanksR().a('\n').aln(arg)));
            pcp(KOPT_NO_EVENT,save,_ta);
        }
        if (id==PROVIDE_WORD_COMPLETION_LIST) {
            String tokens[][]=_txt4completion;
            if (tokens==null) {
                final String ss[]=getSettings();
                tokens=_txt4completion=new String[ss.length][];
                for(int i=ss.length; --i>=0;) tokens[i]=splitTokns(ss[i]);
            }
            return tokens;
        }
        if (id==PROVIDE_HIGHLIGHTS) {
            if (_highlights==null) _highlights=OccurInText.addToLookupTable(getSettings(),null);
            return _highlights;
        }
        return null;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Class specific using reflection  >>> */
    private final static Map<String,Customize> mapClass2Customize=new HashMap();
    public static Customize getCustomizableForClass(Class clazz, String id, String help, int type) {
        final String key=clazz.getName()+" "+id;
        Customize c=mapClass2Customize.get(key);
        if (c==null) {
            final String lines=deref(invokeMthd("getCustomizable"+id, clazz), String.class);
            if (lines!=null) {
                c=new Customize(id+"_"+niceShrtClassNam(clazz),splitStrg(lines,'\n') , help, type);
                mapClass2Customize.put(key,c);
            }
        }
        return c;
    }
    public final static int  CLASS_Example=10001, CLASS_ExecPath=10002, CLASS_InitCommands=10003, CLASS_LateCommands=10004;
    public static Customize getCustomizableForClass(Class c,int id) {
        switch(id) {
        case CLASS_Example: return getCustomizableForClass(c, "Examples", "Example commands", LIST);
        case CLASS_ExecPath: return  Customize.getCustomizableForClass(c, "ExecPath", "file path of Executable",EXECUTABLES);
        case CLASS_InitCommands: return Customize.getCustomizableForClass(c, "InitCommands", "Rendering commands executed at program start",LIST);
        case CLASS_LateCommands: return Customize.getCustomizableForClass(c, "LateCommands", "Rendering commands executed after all proteins loaded.",LIST);
        default: { assrt(); return null;}
        }
    }
    /* <<< Class specific using reflection <<< */
    private static String[] addUsrBin(String ss[]) {
        if (ss==null || isWin()) return ss;
        final String rr[]=new String[ss.length*2];
        final boolean mac=isMac();
        for(int i=0; i<ss.length; i++) {
            rr[2*i+1]=ss[i];
            final String w0=wordAt(0,ss[i]);
            final char c0=chrAt(0,w0);
            if (c0!='/' && c0!='#' && mac && !"open".equals(w0)) rr[2*i]="/usr/bin/"+ss[i];
        }
        return rmNullS(rr);
    }
    protected ChFileFilter[] toFileFilters() {
        final String ss[]=getSettings();
        final ChFileFilter ff[]=new ChFileFilter[ss.length];
        for(int i=0; i<ss.length; i++) {
            final String s=ss[i].trim(), fst=fstTkn(s);
            if (sze(s)>0 && !"all".equals(fst)) {
                ff[i]=new ChFileFilter(s.indexOf('.')>=0 ? "-iz "+s:s);
            }
        }
        return ff;
    }

}
//http://www.biostat.wisc.edu/bcg/sup/macsup/unixtips.html
