package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class HelpCommands {
    public final static int DEFINITION=0, EXAMPLE=1, HINT=2;
    private BA _ba;
    private final String _rsc;
    private HelpCommands(String resource) {
        _rsc=resource;
    }
    private Map<String,String> _maps[];
    public Map<String,String>[] maps() {
        if (_maps==null) {
            _maps=new Map[]{new HashMap(), new HashMap(), new HashMap(), new HashMap()};
            _ba=readBytes(rscAsStream(0L,"",_rsc));

            if (_ba==null) _ba=new BA(0);
            final byte T[]=_ba.bytes();
            final int[] ends=_ba.eol();
            String command=null;
            for(int iL=0; iL<ends.length; iL++) {
                final int b=iL==0 ? _ba.begin() : ends[iL-1]+1;
                final int e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                if (e-b<3 || T[b]=='<') continue;
                if (is(LETTR_DIGT,T,b)) command=_ba.newString(b,nxtE(SPC,T,b,e));
                if (command!=null) {

                    int start=0, type=0;
                    if (T[b]==' ') {
                        start=2;
                        type= T[b+1]=='E' ? EXAMPLE : T[b+1]=='H' ? HINT : DEFINITION;
                    }
                    final String
                        prev=_maps[type].get(command),
                        line=_ba.newString(b+start, e),
                        value=prev!=null ? new BA(999).aln(prev).a(line).toString() : line;
                    _maps[type].put(command, value);
                }
            }
        }
        return _maps;
    }
    private BA _sb;
    public String get(int type, String c) {
        return maps()[type].get(c);
    }

    public BA alphabetically(String commands[]) {
        final String names[]=commands!=null?commands.clone():strgArry(maps()[0].keySet());
        Arrays.sort(names);
        final BA sb=new BA(3333);
        for(String n : names) {
            final String d=get(DEFINITION,n), xx[]=splitLnes(get(EXAMPLE,n)), h=get(HINT,n);
            if (d==null) continue;
            final int spc=strchr(STRSTR_E, ' ', d);
            sb.a(ANSI_BOLD).a(d,0,spc).a(ANSI_RESET).a(d,spc,MAX_INT).a('\n');
            if (xx.length>0) {
                sb.a(plrl(xx.length," "+ANSI_UL+"Example%S:"+ANSI_RESET)).a('\n', xx.length>1?1:0);
                for(String x : xx) sb.a("   ").aln(x);
            }
            sb.aln(h).a('\n');
        }
        return sb;
    }
    private static Map<Object,HelpCommands> _map;
    public static HelpCommands getInstance(String resource) {
        if (_map==null) _map=new HashMap();
        HelpCommands h=_map.get(resource);
        if (h==null) _map.put(resource,h=new HelpCommands(resource));
        return h;
    }

    public BA tt(String w, boolean verbose) {
        final Object d=get(HelpCommands.DEFINITION,w);
        if (d==null) return null;
        final BA ba=new BA(333).a("<pre>").aln(d).a("</pre>");
        if (verbose) ba.and("Examples:<pre>",get(HelpCommands.EXAMPLE,w),"</pre>").a(rplcToStrg("\n","<br>", get(HelpCommands.HINT,w)));
        else ba.a("<sub>Shift-key for details</sub>");
        return ba;
    }
}

