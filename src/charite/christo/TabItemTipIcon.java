package charite.christo;
import charite.christo.hotswap.ProxyClass;
import javax.swing.Icon;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Obtains  texts for tabs of JTabbedPane, menu item texts, tool=tips and tabs.
   @author Christoph Gille
*/
public class TabItemTipIcon {
    private final byte[] _T;
    private final int _f, _t;
    private Icon _icon;
    private String[] _get;
    public final static int TAB=0, ITEM=1, TIP=2, ICON=3, HOME=4, FIELDS=5;

    public TabItemTipIcon(byte[] txt, int from, int to) { _T=txt; _f=from; _t=prev(-SPC, txt, to-1, from-1)+1; }
     String g(int n) {
        if (_get==null) _get=new String[FIELDS];
        if (_get[n]==null) {
            final byte[] T=_T;
            final int f=_f;
            if (_T==null||_f<0) return null;
            final int t=mini(T.length,_t);
            int b=f, count=0;
            for(int i=f; i<=t; i++) {
                if (i==t || T[i]==';') {
                    if (count++==n) {
                        _get[n]=toStrg(T,b,i);
                        break;
                    }
                    b=i+1;
                }
            }
        }
        return _get[n];
    }

    public static Object set(String tab, CharSequence item, CharSequence tip, Object icon,  Object o) {
        if (o==null) return null;
        final String s=deref(sKey(o), String.class);
        TabItemTipIcon t=s!=null ? MAP.get(s) : WEAK_MAP.get(o);
        if (t==null) setTiti(t=new TabItemTipIcon(null,0,0), o);
        String[] ff=t._get;
        if (ff==null) ff=t._get=new String[FIELDS];
        ff[TAB]=toStrg(tab);
        ff[ITEM]=toStrg(item);
        ff[TIP]=toStrg(tip);
        ff[ICON]=toStrg(deref(icon,CharSequence.class));
        t._icon=deref(icon, Icon.class);
        return o;
    }
    final static Map<String,TabItemTipIcon> MAP=new HashMap();
    private final static Map<Object,TabItemTipIcon> WEAK_MAP=new WeakHashMap();

    public static String g(int i, Object o, java.awt.AWTEvent ev) {
        String v=null;
        if (isInstncOf(ChRunnable.class, o)) {
            final ChRunnable r=(ChRunnable)o;
            final Object a=r.run(i==TAB?ChRunnable.RUN_GET_TAB_TEXT : i==ITEM?ChRunnable.RUN_GET_ITEM_TEXT : i==TIP?ChRunnable.RUN_GET_TIP_TEXT : i==ICON?ChRunnable.RUN_GET_ICON : null, ev);
            v=
                a==null ? null :
                a instanceof Class ? g(i,a,ev) :
                a instanceof CharSequence ? toStrg(a) :
                null;
        }
        if (sze(v)==0){
            final TabItemTipIcon t=getTiti(o);
            v=t==null?null : t.g(i);
        }
        return v;
   }
    public static Icon icn(Object o) {
        if (o==null) return null;
        final TabItemTipIcon t=getTiti(o);

        Icon icon=null;
        if (t!=null) {
            final String sIcon=t.g(ICON);
            if (sIcon!=null) {
                if (t._icon==null) t._icon=iicon(sIcon);
                if (t._icon==null) t._icon=iicon(IC_ERROR_ICON);
            }
            icon=t._icon;
        }
        if (icon==null) {
            final Object a=runCR(o, ChRunnable.RUN_GET_ICON, null);
            icon=a instanceof String ? iicon((String)a) : deref(a,Icon.class);
        }
        if (icon==null) {
            final String cn=clasNam(o);
            if (sze(cn)>0 && !(cn.charAt(0)=='j' && strEquls("java.",cn,0)||strEquls("javax.",cn,0))) {
                icon=iicon(cn.replace('.','/'));
            }
            if (icon==null) icon=iicon(IC_ERROR_ICON);
            set(null,null,null,icon,cn);
        }
        return icon==iicon(IC_ERROR_ICON)?null : icon;
    }
    private static int _b;

    public static void ad(String path, String rsc) {
        final BA txt=readBytes(rscAsStream(0L,path, rsc));
        if (txt==null) return;
        final byte T[]=txt.bytes();
        final BA ba=new BA(99);
        for(int e=0, E=txt.end(); e<=E; e++) {
            if (e==T.length || T[e]=='\n') {
                final int b=_b, c0=chrAt(b,T);
                if (c0!='#') {
                    for(int i=b;i<e;i++) {
                        if (T[i]==';') {
                            String a="";
                            final String pck=
                                c0!='C' ? "" :
                                strEquls(a="CCSE.",T,b) ? _CCSE :
                                strEquls(a="CCM.", T,b) ? _CCM  :
                                strEquls(a="CCS.", T,b) ? _CCS  :
                                strEquls(a="CCP.", T,b) ? _CCP  :
                                strEquls(a="CCB.", T,b) ? _CCB  :
                                strEquls(a="CC.",  T,b) ? _CC   :
                                "";
                            ba.clr().a(pck).a(T,b+sze(a),i);
                            final TabItemTipIcon titi= new TabItemTipIcon(T,i+1,e);
                            MAP.put(ba.toString(), titi);
                            /*
                            if (myComputer()) {
                                try { Class.forName(ba.toString()); } catch(Exception ex) {putln(RED_ERROR+"TabItemTipIcon ",ba); }
                                final String ic=titi.g(ICON);
                                if (sze(ic)>0 && iicon(ic)==null) debugExit("Missing icon ",ic,"  ", ba);
                            }
                            */
                            break;
                        }
                    }
                }
                _b=e+1;
            }
        }
    }
    private static String sKey(Object o) {
        final Class c=o instanceof Class ? (Class)o : o instanceof ProxyClass ? ((ProxyClass)o).getClassInstance() : null;
        return c!=null ? c.getName() : o instanceof CharSequence ?  o.toString() : null;
    }

    public static void setTiti(TabItemTipIcon titi, Object o) {
        if (o==null) return;
        final String s=sKey(o);
        if (s!=null) MAP.put(s,titi);
        else WEAK_MAP.put(o,titi);
    }
    public static TabItemTipIcon getTiti(Object o) {
        if (o==null) return null;
        final String key=sKey(o);
        TabItemTipIcon t= key!=null ? MAP.get(key) : WEAK_MAP.get(o);

        if (t==null && o.getClass()!=ChPanel.class) t=MAP.get(o instanceof String ? o : o.getClass().getName());
        if (t==null && o instanceof ChJScrollPane) return getTiti(((ChJScrollPane)o).jc());
        return t;
    }

}
