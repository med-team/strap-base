package charite.christo;
import static charite.christo.ChUtils.*;

public class NativeTools extends BasicExecutable {
    private static int _countTried, _installed;
    public NativeTools() {
        final boolean isW=isSystProprty(IS_WINDOWS), isM=isSystProprty(IS_MAC);
        final String n="nativeTools_"+(isW ? "windows2" : isM ? "mac" : "unix2");
        setName(n);
        if (ChConstants.LINUX_PACKAGE) setFileExecutable(file("/usr/lib/strap/nativeTools_unix2"));
        else {
            setSourceInstallationScript(isW || isM ? null :  "C_COMPILER -lm -lX11 -o "+n+".exe "+n+".c");
            if (isSystProprty(IS_LINUX386) || isW) setBinaryPackageURLs("charite/christo/files/bin/"+n+".zip");
            setSourcePackageURLs("charite/christo/files/src/"+n+".zip");
        }
    }

    public static void setWindowState(char m, int x, int y, String ...title) {
        new NativeTools().sws(m,x,y,title);
    }

    @Override public void installPackage() {
        if (0==_installed++) super.installPackage();
    }
    private void sws(char m, int x, int y, String ...title) {
        if (!isSystProprty(IS_MAC)) {
            final java.io.File bin=fileExecutable();
            if (bin.length()==0 && _countTried++<3) installPackage();
            if (bin.length()==0) return;
            startThrd(new ChExec(ChExec.DEBUG|ChExec.IGNORE_ERROR|ChExec.WITHOUT_ASKING).setCommandLineV(bin, toStrg(m),toStrg(x),toStrg(y), title));
        }
    }
}
/*
  xorg-x11-libx11-devel (Suse)
  libx11-dev (Debian)
*/
