CCS.StrapDAS;;DAS features

CCS.ActivateDeactivate;Activate; Activate / deactivate annotations in residue selections ...;;IC_SELECT
CCS.AddAnnotation;Add anno;Add annotations to annotated residue selections
#CCS.Balibase;bali;BAliBASE  - benchmark ...;;align;http://bips.u-strasbg.fr/fr/Products/Databases/BAliBASE/
CCS.Dialog3DViewer;3D;Protein ^viewers, Pymol, Jmol ...;A protein is displayed in a protein viewer <br>and selections and commands are passed <br>from Strap to the viewer;IC_JMOL
CCS.DialogAlign;;^Align ...;Aligning proteins with computational alignment methods e.g. CLUSTALW.;IC_ALIGN
CCS.DialogSuperimpose3D;;^3D-Superimpose structures ...;Superimpose protein structures;superimpose

CCS.DialogAlignOneToAll;1;Align many sequences to ^one reference ...;Try alignment of one protein to all other proteins recommended when one protein should be added<br> to an existing alignment.<br>This way you find the most similar protein;IC_ALIGN
CCS.DialogNonRedundantSequenceSet;Set;Make non ^redundant protein set ...
#CCS.DialogAlignWithBalibase;BALi;Evaluate alignment methods with BAliBASE ...


CCS.DialogBarChart;;^Bar-charts  ...;A bar chart for all proteins is drawn.<br> The method to obtain a value for each protein <br>can be selected;barchart
CCS.DialogBlast;Blast;^BLAST -  Find similar sequences ...;identify sequences that are similar to the query sequence;IC_SEARCH

CCS.DialogSimilarStructure;Find 3D;Associate similar ^3D-structure ...;Find 3D-structure with a similar amino acid sequence;IC_3D

CCS.DialogCompareProteins;;Pairwise ^comparison of proteins ...;A comparison method <br>e.g. % sequence identity is selected. <br>Distances as table or graph;IC_COMPARE
CCS.DialogCopyAnnotations;Copy;Cop^y residue selections to another protein ...;Map ResidueSelections onto another protein ...;IC_COPY
CCS.DialogDifferentResidues;Homol; Create script for sequence positions that differ between two proteins ...;<ul><li>substitution commands <br>for homology modeling</li><li>Creating residue selections <br>using the different positions</li></ul>
CCS.DialogDotPlot;;^Dotplot ...;;IC_DOTPLOT


CCS.DialogExportAlignment;;Export ^alignment ... ;export alignment as<ul><li>Multiple sequences file</li><li>HTML</li><li>HSSP</li></ul>;IC_EXPORT

CCS.DialogExportWord;;To word-processor, web-^Browser or Jalview ...;;IC_WORD



CCS.DialogExportProteins;;^Export protein files ...;Protein files are written into the directory strapTmp;IC_EXPORT

CCS.DialogFetchPdb;;Download ^3D structures ...;A text may contain pdb-IDs. These IDs are identified and the respective PDB-files loaded.;IC_3D
CCS.DialogFetchSRS;;^Download sequence files ...;A text my contain sequence-IDs. These IDs are identified and the respective files loaded.;IC_DOWNLOAD
CCS.DialogGenbank;Genbank;Translate ^Genbank nucleotide files ...;translate nucleotide sequences<br> into amino acid sequences<br>assuming the file is a Genbank file;IC_DNA
CCS.DialogHighlightPattern;;Underline sequence patterns ...;;IC_UNDERLINE
CCS.DialogImportMFA;Import;^Import multiple fasta ...;A multiple fasta file contains several sequences;IC_IMPORT
CCS.DialogManyInOneRow;Stack;Stacking proteins into a single ^line ...;many sequences stacked in one line<br>and manipulated <b>simultaneously</b>
CCS.DialogNeighbors;Neighbors;Find proteins with a similar ^fold ...;Structurally similar proteins in the PDB. The search is based on 3D-structur. <br>AA sequence is not regarded.;IC_SEARCH
CCS.DialogNewProtein;; ^Enter protein in text-box ...;;IC_ADD
CCS.V3dUtils;; 3D-visualization in Strap ...;;IC_JMOL

CCS.DialogPhylogeneticTree;Tree;Phylogenetic tree ...;;IC_TREE


CCS.DialogPlot;;^Plot along alignment ...;;IC_PLOT
CCS.DialogInferCDS;Nt;^Translate nucleotide sequence to amino acids to match a given aa sequence ...;;dna_helix
CCS.DialogPredictCoiledCoil;;^Coiled coils;;IC_COILEDCOIL
CCS.DialogPredictSecondaryStructures;;^Secondary structure;;IC_HELIX
CCS.DialogPredictTmHelices2;;^Transmembrane helices;;IC_TM_HELIX
CCS.DialogPredictTmHelicesMulti;;Compare several methods to predict TM-helices ...;;IC_TM_HELIX
#CCS.Strap;;Alignment Projects in Strap

CCS.DialogPublishAlignment;;Publish ^interactive alignment ...;;globe
CCS.DialogResidueAnnotationChanges;Change;^Change many annotations ...;add/change many residue annotations;IC_EDIT
CCS.ActivateDeactivate; Activate;Activate/Deactivate entries in annotations of residues;;IC_EDIT
CCS.DialogResidueAnnotationList;SNP;From ^list of sequence positions e.g. mutations, SNPs ...;lists of mutations e.g. `R333K L3443I delF508 .. ;import
CCS.DialogRestoreFromBackup;Backup;Restore from ^backup ...;Gaps, annotations, nucleotide reading frame <br>can be saved in a backup directory.<br>These data can be restored ...
CCS.DialogSelectionOfResiduesMain;;Select residues by [structural] ^features ...;;ruler
CCS.DialogSubcellularLocalization;;Subcellular ^localization;;IC_MITOCHONDRION

CCS.Dialog_bl2seq;;Compare two sequences with Blast ^2 Sequences ...;;dotplot;http://www.ncbi.nlm.nih.gov/BLAST
CCS.Distance3DToCursor;Dist;Highlight spatially ^neighboured  residues ...;All amino acids are highlighted where the C-Alpha atom is structurally close to the cursor position.<br>It can be used to identify equivalent positions in previously superimposed proteins.;ruler

CCSE.Distance3DToHetero;;Distances of Residues to hetero groups;All amino acids are highlighted, that are  adjacent to a certain hetero compound;flavin


CCS.DnaExonStart;;Underline start of e^xons;;dna_helix
CCS.EditDna;Define CDS;Manually set reading ^frame and coding nucleotides ...;select reading frame. <br>affects the translation of the DNA-sequence <br>;dna_helix
CCS.FilesFetchedFromServer;Fetch;Downloaded protein files

CCS.IntermediateSeq;Intermed;Intermediate sequences ...;;IC_ALIGN
CCS.ProteinPopup;;The context menu of proteins
CCS.ProteinViewerPopup;;;;IC_JMOL
CCS.ResidueAnnotation;;Annotated residue selections;;IC_UNDERLINE
CCS.ResidueAnnotationView;Edit Anno;Modifying annotations;;IC_UNDERLINE
CCS.ResultCompareProteinPairs;;Compare proteins
CCS.Strap;;Project folders;;IC_DIRECTORY
CCS.StrapAlign;;Introduction;;
CCS.StrapHotswap;;^Create Java plugins by yourself ...;;IC_PLUGIN
CCS.StrapKeyStroke;;Keyboard;;IC_KEYBOARD
CCS.StrapPlugins;;Plugins
CCS.StrapTree;Objects;Selecting objects and using context menus;;IC_SELECT
CCS.StrapView;;The sequence alignment panel
CCS.Texshade;;Export &PDF ...;uses LaTeX/TeXshade to render multiple sequence alignments;pdf
CCS.science.FlavinePROXY;;Flavin Proteins;;flavin
CCS.science.Flavine;;Flavin Proteins;;flavin
CCS.science.Prione;;Prion
CCS.science.Proteasome;;Proteasome;;proteasome
CCSE.AlignmentEntropy;;Alignment entropy gives the variability at each alignment positions

CCSE.AlignmentWriterHSSP;;HSSP is not frequently used

CCB.Blaster_SOAP_ebi;;Blast at EBI;;;http://www.ebi.ac.uk/blast2/
CCB.Blaster_local_NCBI;;This requires local installation of the NCBI Blast package
CCB.Blaster_local_NCBI_gapped_PSI;;blastpgp is more sensitive than blastp
CCB.Blaster_local_Wu;;This requires local installation of the Blast program and the sequence database
CCS.Aligner3D;;This universal method can align sequences and 3D structure combining ClustalW ot T-coffee with a 3D-superposition method.;;IC_SUPERIMPOSE
CCSE.Aligner3D_Mustang;;Multiple structural alignment algorithm.;;IC_SUPERIMPOSE
CCSE.Aligner3D_Matt;;Multiple sequence alignment by 3D-coordinates allowing little bends.;;IC_SUPERIMPOSE
CCSE.Aligner3D_Mapsci;;Multiple sequence alignment by 3D-coordinates.;;IC_SUPERIMPOSE
CCSE.Aligner3D_Smolign;;Under construction - flexible multiple 3D-alignment.;;IC_SUPERIMPOSE
CCSE.CoiledCoil_PredictorRobinson;;Prediction of coiled coils;;IC_COILEDCOIL
CCSE.ConsensusSequence;;Consensus Sequence
CCSE.CountResidues;;the number of amino acids in the protein
CCSE.DemoAssociateObjectsToProteins;;Attach information at proteins

CCSE.Hydrophobicity;;High values for hydrophobic amino acids

CCSE.MultipleAlignerAlign_m;;align-m: Still under construction. by Ivo Van Walle. Is accurate and therefore  slow.  Non-comercial users only.;;;http://bioinformatics.vub.ac.be/software/software.html
CCSE.MultipleAlignerAmap;;Amap is  an accurate alignment program by A.S. Schwartz, E.W. Myers and L. Pachter
CCSE.MultipleAlignerClustalW;;Multiple sequence alignment with ClustalW;;;http://www.ebi.ac.uk/Tools/clustalw/index.html

CCSE.MultipleAlignerDialign;;Multiple sequence alignment by Burkhard Morgenstern and Said Abdeddaim;;;http://dialign.gobics.de/

CCSE.MultipleAlignerDialignT;;ultiple sequence alignment by Amarendran R. Subramanian;;;http://bibiserv.techfak.uni-bielefeld.de/dialign/
CCSE.MultipleAlignerKalign;;Multiple sequence alignment by Timo Lassmann;;;http://msa.cgb.ki.se/
CCSE.MultipleAlignerMafft;;Multiple sequence alignment with Mafft
CCSE.MultipleAlignerMuscle;;Multiple sequence alignment TabItemTipIcon.jav;;;http://www.drive5.com/muscle/
CCSE.MultipleAlignerProbcons2;;Multiple sequence alignment Probcons
CCSE.MultipleAlignerT_Coffee;;$ Multiple sequence alignment with many features
CCSE.MultipleAligner_SPEM;;Multiple sequence alignment Spem
CCSE.PairAlignerJAligner;;Local alignment procedure in Java
CCSE.PairAlignerNeoBio;;Global and local pair alignments. Implementation in Java.
CCSE.PhylogeneticTree_Archaeopteryx;;Phylogenetic Dendrogram Archaeopteryx
CCSE.PhylogeneticTree_TreeTop;;TreeTop - Phylogenetic Tree in web browser
CCSE.PhylogeneticTree_Jalview;;Jalview - After start open tree in Jalview

CCSE.ProteinsSorter_with3D_without3D;;first those with 3D coordinates, then those without 3D;IC_SORT

CCSE.ResidueHydrophobicity_TOPPRED;;TOPPRED, Hydrophobicity plot Toppred
CCSE.ResidueSolventAccessibility;;Residue Solvent Accessibility;;IC_UMBRELLA
CCSE.ResidueValues_from_textfile;;residue values from a text file;values are expected in a file DATA/proteinName.?

CCSE.SecondaryStructure_Jnet;;Jnet
CCSE.SecondaryStructure_NNPREDICT;NNPREDICT;Nnpredict 
CCSE.SecondaryStructure_Sopma;;Sopma

CCSE.Superimpose_native_CE;;CE is slow but robust;;IC_SUPERIMPOSE
CCSE.Superimpose_CEPROXY;;Java port of CE;;IC_SUPERIMPOSE
CCSE.Superimpose_GangstaPlus;;3D-Superposition from the Free University Berlin;;IC_SUPERIMPOSE
CCSE.Superimpose_LajollaProteinPROXY;;Lajolla;;IC_SUPERIMPOSE
CCSE.Superimpose_TM_align;;TM_align - a rapid and robust 3D-alignment method;;superimpose;
CCSE.Superimpose_GoedePROXY;;Goede-method;Superimposes two sets of unconnected points. Neglects connectivity and therefore not optimal for superimposing homologous proteins;IC_SUPERIMPOSE
CCSE.Toppred;;TOPPRED, Hydrophobicity plot, by Theodore D. Liakopoulos, Claude Pasquier and Stavros J. Hamodrakas;;;http://bioweb.pasteur.fr/seqanal/interfaces/toppred.html
CCSE.TransmembraneHelix_DAS;TM-hx;DAS-TMfilter server by  Miklos Cserzo;;;http://mendel.imp.ac.at/sat/DAS/DAS.html
CCSE.TransmembraneHelix_HMMTOP;TM-hx;HMMTOP, by Gabor E. Tusnady, at the Institute of Enzymology, Hungary;;;http://www.enzim.hu/hmmtop/html/adv_submit.html
CCSE.TransmembraneHelix_MEMSAT;TM-hx;$ MEMSAT:  by  David T. Jones's Group;;;http://saier-144-37.ucsd.edu/memsat.html
CCSE.TransmembraneHelix_ORIENTM;TM-hx;ORIENTM, by Theodore D. Liakopoulos, Claude Pasquier and Stavros J. Hamodrakas;;;http://athina.biol.uoa.gr/orienTM/submit.html
CCSE.TransmembraneHelix_PRED_TMR;TM-hx;PRED_TMR prediction by  Claude Pasquier;;;http://o2.biol.uoa.gr/PRED-TMR/input.html
CCSE.TransmembraneHelix_Phobius;;Phobius, by Lukas Kall;;;http://phobius.sbc.su.se/
CCSE.TransmembraneHelix_SOSUI;;SOSUI: Mitaku Group Department of Biotechnology, Tokyo;;;http://sosui.proteome.bio.tuat.ac.jp/sosuiframe0.html
CCSE.TransmembraneHelix_SPLIT3;;SPLIT: method by  Davor Juretic, Server by  Damir Zuci;;;http://split.pmfst.hr/split/
CCSE.TransmembraneHelix_SPLIT4;;SPLIT: method by  Davor Juretic, Server by  Damir Zuci;;;http://split.pmfst.hr/split/
CCSE.TransmembraneHelix_THUMBUP;;A TM helical-protein topology predictor using burial propensity of amino acids;;;http://sparks.informatics.iupui.edu/Softwares-Services_files/thumbup.htm
CCSE.TransmembraneHelix_TMAP;;TMAP: consensus prediction from alignment  by B Persson and P Argos. The proteins must be aligned !!;;;http://bioweb.pasteur.fr/seqanal/interfaces/tmap.html
CCSE.TransmembraneHelix_TMAP_MULTI;;TMAP: consensus prediction from alignment  by B Persson and P Argos. The proteins must be aligned !!;;;http://bioweb.pasteur.fr/seqanal/interfaces/tmap.html
CCSE.TransmembraneHelix_TMHMM2;;TMHMM2 server by  Anders Krogh and Kristoffer Rapacki;;;http://www.cbs.dtu.dk/services/TMHMM/
CCSE.TransmembraneHelix_TMPRED;;TM PREDICT  by K Hofmann and  W Stoffel;;;http://www.ch.embnet.org/software/TMPRED_form.html
CCSE.TransmembraneHelix_TOPPRED;;TOPPRED, by Theodore D. Liakopoulos, Claude Pasquier and Stavros J. Hamodrakas;;;http://biophysics.biol.uoa.gr
CCSE.TransmembraneHelix_WaveTM;;WaveTM: takes about 3min per seq.  Authors  Evanthia E. Pashou, Zoi I. Litou, Theodore D. Liakopoulos and Stavros J. Hamodrakas;;;http://athina.biol.uoa.gr/bioinformatics/waveTM/



CCSE.AlignTwoProteinsAndGetScore;;Alignment score after  aligning  the two given sequences;The alignment is computed for the given pair of proteins;IC_ALIGN

CCSE.SuperimposeTwoStructuresAndGetScore;;3D similarity Score or RMSD value after superimposing the two structures; The superposition is computed for the given pair of proteins;IC_SUPERIMPOSE


CCSE.AlignmentScoreCountIdentical;;The relative number of identical residues of two aligned sequences
CCSE.AlignmentScoreBlosume62;Blosum62;Similarity score for two aligned sequences based on Blosum62


Tutorial_BLAST_PDB;3D;Identifying protein structures for a given AA sequence (basic)
Tutorial_INTRO;Intro;Introduction (basic)
Tutorial_3D;3D;Structure alignments are better than sequence based methods (advanced)
Tutorial_3DVis;3D Visualization;3D Structure Visualization (basic)
Tutorial_MUTATIONS;SNP;View mutations in 3D (advanced)
Tutorial_ALIG_ALIGNMENTS;Ali;Join two sequence alignments (advanced)
Tutorial_TEXSHADE;PDF;Highlighting Residues in Printable PDF-Output (advanced)
Tutorial_WOBBLEBASE;nt/aa;Comparing nucleotide and amino acid sequence alignments (advanced)
Tutorial_SUPERPOSITION;Superpose;Superposition of proteins (basic)
Tutorial_copy_DNA;Copy;Copy DNA or other ligands from one Structure to another (advanced)
Tutorial_DnD;DnD;Drag-and-Drop (basic)
Tutorial_WELCOME;Welcome;Welcome
Tutorial_UNDOCKI;Undock;Fast access to often used menu items (basic)

 
