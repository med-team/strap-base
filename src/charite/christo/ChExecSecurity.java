package charite.christo;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
     @author Christoph Gille
*/
public final class ChExecSecurity {
    private ChExecSecurity(){}
    static Process runtimeExecS(long options, String arg[], String[] env, File dir) throws IOException {
        if (checkPermission(options, arg, env, dir)) {
            return Runtime.getRuntime().exec(arg, env,dir);
        }
        return null;
    }
    private static boolean inList(String prgName, Customize cust) {
        final boolean debug=cust==Customize.customize(Customize.trustExe);
        if (looks(LIKE_FILEPATH,prgName)) {
            final File fp=file(prgName);
            for(String s : cust.getSettings()) {
                final File f=file(s);
                if (eqNz(f,fp)) return true;
            }
        } else {
            final long options=isWin() ? STRSTR_ANY_SLASH|STRSTR_IC : 0L;
            for(String s : cust.getSettings()) {
                if (endWith(options, prgName,s)) return true;
                if (endWith(options, s,prgName)) return true;
            }
        }
        return false;
    }

    public static boolean checkPermission(long options, String arg[], String[] env, File dir) {
        final Customize
            cRefus=Customize.customize(Customize.refuseExe),
            cTrust=Customize.customize(Customize.trustExe);
        final String a0=get(0,arg), a1=get(1,arg), a2=get(2,arg);
        final boolean cmdExe=isWin() && "cmd.exe".equalsIgnoreCase(a0) && "/c".equalsIgnoreCase(a1);
        final String prgName=
            cmdExe && "start".equals(a2) ? a0+" "+a1+" "+a2+" "+wordAt(0,get(3,arg)) :
            cmdExe || 0<=strstr(STRSTR_w_R, "sh", a0) && "-c".equals(a1) ? a0+" "+a1+" "+wordAt(0,a2)  :
            a0;

        if (!Insecure.EXEC_ALLOWED || a0==null || inList(prgName, cRefus)) return false;
        synchronized(mkIdObjct("CES$$SYNC",prgName)) {
            final ChButton tog=buttn(TOG_NASK_EXEC);
            if (!isSelctd(tog) && !inList(prgName,cTrust)) {
                final Object
                    msg=pnl(VBHB,
                            tog.cb(),
                            "<br>Also see the menu \"Security\" in \"Preferences\"!<br>"+
                            "<br>Strap wants to start the external program "+
                            "<pre>\n "+prgName+"\n</pre>Do you allow starting this program?<br>"
                            );
                if (!ChMsg.yesNoSync(false, tog, ChFrame.CENTER|ChFrame.PACK, msg)) {
                        if (0==(options&ChExec.NOT_TO_DENY_LIST)) cRefus.addLine(prgName);
                        return false;
                    }
                    cTrust.addLine(prgName);
                }
        }
        return true;
    }

    public static String describeExec(long opts, String argv[], String[] env, File dir) {
        final BA sb=new BA(999);
        if (!isWin() || 0==(opts&ChExec.CYGWIN_DLL)) sb.a(" cd ").aQuoteIfNecessary(dir).a('\n');
        if (isWin()) sb.a(" cd ").aQuoteIfNecessary(toCygwinPath(dir)).a('\n');
        for(int i=0; i<sze(env);i++) {
            final String e=env[i];
            final int eq=strchr('=',e);
            if (eq<0) continue;
            sb.a(isWin() ? " set ":" export ").a(e,0,eq).a('=').aQuoteIfNecessary(e.substring(eq+1)).a('\n');
        }
        for(String a:argv) sb.aQuoteIfNecessary(a).a(' ',3);
        return sb.a('\n').toString();
    }

}
