package charite.christo;
import java.net.URL;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Selections of Residues (sors) can refere to URLs or database entries.
   Database IDs are translated into URLs.
   @author Christoph Gille
*/
//http://www.expasy.org/expasy_urls.html
//http://www.chem.qmul.ac.uk/iubmb/enzyme/EC1/1/1/3.html

public class Hyperrefs  {
    private Hyperrefs(){}
    public final static int  WEB_LINK=1, PLAIN_TEXT=2, PROTEIN_FILE=4;
    private final static int COUNT=3, MODIC[]=new int[(1<<COUNT)+1];
    private final static String[][][] DATABASES=new String[(1<<COUNT)+1][][];
    private static String pubmedOption, keysWithBrace[], keysToHighlight[];
    public final static String PAIR_ALIGNMENT="PAIR_ALIGNMENT:";

    public static void setPubmedOption(String s) { pubmedOption=s;}     // holding=idecuhmlib_fft&

    public static boolean hasNumericID(String w) {
        final char c=chrAt(0,w);
        return ( ((c|32)=='p' && w.startsWith("PMID") || w.startsWith("PUBMED") || w.startsWith("pmid")) || c=='G' && w.startsWith("GO:")) && w.indexOf('{')<0;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Databases >>> */
    public static int mc() {
        int mc=0;
        for(int i=0;i<COUNT; i++) {
            final Customize c=Customize.customize(1<<i);
            c.getSettings();
            mc+=c.mc();
        }
        return mc;
    }
    public static synchronized String[] getKeysWithBrace() {
        final String[][] key_url=getDatabases(WEB_LINK);
        if (keysWithBrace==null) {
            final List<String> vK=vClr(0,VTEMP);

            vK.clear();
            for(String key:key_url[0]) {
                if (lstChar(key)=='{') vK.add(key);
            }
            keysWithBrace=strgArry(vK);
        }
        return keysWithBrace;
    }
    public static synchronized String[] getKeysToHighlight() {
        final String[][] key_url=getDatabases(WEB_LINK|PLAIN_TEXT|PROTEIN_FILE);
        if (keysToHighlight==null) {
        final List<String> vK=vClr(0,VTEMP);
            for(String key: key_url[0]) {
                if (!vK.contains(key)) {
                    vK.add(key);
                    if ("EMBL:".equals(key) || "PIR:".equals(key) || "DIP:".equals(key) || "PDB:".equals(key) ) vK.add(" "+delSfx(':',key)+"; ");
                }
            }
            vK.add("ftp://");
            vK.add("http://");
            vK.add(PAIR_ALIGNMENT);
            keysToHighlight=strgArry(vK);
        }
        return keysToHighlight;
    }
    private final static Object VTEMP[]={null,null};
    public static synchronized String[][] getDatabases(int id) {
        boolean isSingle=false;
        for(int i=0;i<COUNT; i++) isSingle|= (id==1<<i);
        if (isSingle) {
            final Customize cust=Customize.customize(id);
            final String lines[]=cust.getSettings();
            final int mc=cust.mc();
            Map vars=null;
            if (DATABASES[id]==null || MODIC[id]!=mc) {
                MODIC[id]=mc;
                keysToHighlight=keysWithBrace=keysToHighlight=null;
                final List<String> vK=vClr(0,VTEMP), vU=vClr(1,VTEMP);
                for(int i=DATABASES.length; --i>=0;) if ( (i&id)!=0) DATABASES[i]=null;
                for(String line:lines) {
                    if (chrAt(0,line)=='#') continue;
                    if (chrAt(0,line)=='$') {
                        final int eq=line.indexOf('=');
                        if (eq>0) (vars==null?vars=new ChMap() : vars).put(line.substring(0,eq),line.substring(eq+1).trim());
                        continue;
                    }
                    final String[] tt= splitTokns(line);
                    final int nT=sze(tt);
                    for(int i=0;i<nT-1;i++) {
                        if (vars!=null) tt[nT-1]=toStrg(strplc(STRSTR_w_R, vars,null,tt[nT-1]));
                        vK.add(tt[i]);
                        vU.add(toStrg(strplc(STRSTR_w_R, vars,null,tt[nT-1])));
                    }
                }
                final int n=sze(vK);
                final String[][] key_url=new String[2][n];
                for(int i=0;i<n;i++) {
                    key_url[0][i]=vK.get(i);
                    key_url[1][i]=vU.get(i);
                }
                DATABASES[id]=key_url;
            }
        } else {
            for(int i=0;i<COUNT; i++) getDatabases(1<<i);
            if (DATABASES[id]==null) {
                final List<String> vK=vClr(0,VTEMP), vU=vClr(1,VTEMP);
                for(int i=0;i<COUNT; i++) {
                    if ( ( (1<<i) & id )==0) continue;
                    final String[] dd[]=getDatabases(1<<i);
                    adAll(dd[0],vK);
                    adAll(dd[1],vU);

                }
                DATABASES[id]=new String[][]{strgArry(vK),strgArry(vU)};

            }
        }
        return DATABASES[id];
    }
    /* <<< getDatabases  <<< */
    /* ---------------------------------------- */
    /* >>> toUrlString >>> */

    public static boolean isHyperref(byte[] T, int pos,int end0) {
        final int end=mini(sze(T),end0),  L=end-pos;
        if (end==0 || L<4) return false;
        final byte t0=T[pos];
        if (L>10 && (t0|32)=='u' && strEquls(STRSTR_IC,"uniref",T,pos) && get(nxt(-DIGT,T,pos+6,end),T)==':') return true;
        nextKey:
        for(String key : getKeysToHighlight()) {
            final int len=sze(key);
            if (len==0) continue;
            final char k0=key.charAt(0), kz=key.charAt(len-1);
            if (k0!=t0 || pos+len>=T.length) continue;
            if (L<len+4 && kz!='{' && !(kz=='e' && key.endsWith("database"))) continue;
            for(int k=len;--k>=0;)  if (key.charAt(k)!=T[pos+k]) continue nextKey;
            if (T.length>pos+len && t0=='P' && T[pos+1]=='D' && T[pos+2]=='B') {
                if (!is(LETTR_DIGT,T[pos+len])) continue;
            }
            return true;
        }
        return false;
    }
    public static String toUrlString(String url_or_DBreference, int mode) {
        if (url_or_DBreference==null) return null;
        final String trim=url_or_DBreference.trim();
        final int l_trim=trim.length();
        if (l_trim<5) return null;
        final char c0=trim.charAt(0);
        String txt=ensemblUrl(url_or_DBreference,mode);
        if (txt!=null) return txt;
        if (c0=='P' && url_or_DBreference.startsWith(PAIR_ALIGNMENT))
            return toStrg(url(file(dirTmp()+"/sequFeature/align/"+delPfx(STRAPTMP,url_or_DBreference.replace(':','_'))+".txt")));
        if (trim.length()>10 && (c0|32)=='u' && strEquls(STRSTR_IC,"uniref",trim)) {
            final int colon=nxt(-DIGT,trim,6,MAX_INT);
            if (chrAt(colon,trim)!=':') return null;
            txt= (strEquls(STRSTR_IC,"UPI",trim,colon+1) ? "UNIPARC" : "UNIPROT") + url_or_DBreference.substring(colon);
        } else txt=trim;
        if ((c0=='h' || c0=='f') && looks(LIKE_EXTURL,txt) ) {
            final URL u=url(txt);
            if (u!=null) return u.toString();
        }

        //final boolean debug="SpiceBrowser".equals(url_or_DBreference);
        final int L=txt.length();
        final String[] dd[]=getDatabases(mode), keys=dd[0], urls=dd[1];
        for(int iK=0; iK<keys.length; iK++) {
            final String key=keys[iK];
            if (key==null) continue;
            String url=urls[iK];
            final int lKey=key.length();
            if (lKey==0) continue;
            final char k0=key.charAt(0), kz=key.charAt(lKey-1);
            if (c0==k0 && txt.startsWith(key) && ( L>=lKey+1  || kz=='*' ||  /*L>=lKey &&*/ kz=='{' || (kz=='e' && key.endsWith("database"))) ){
                final char cz=txt.charAt(L-1);
                final String v0=delSfx(',',txt.substring(lKey,cz=='}'?L-1:L));
                String v= kz=='{'  || v0.indexOf(':')>0 ? filtrS(FILTER_URL_ENCODE,0, v0.trim()) : v0;
                if (k0=='P' && "PDB:".equals(key)){
                    if (v.length()>4)  v= delPfx("pdb",v);
                    if (v.length()>4)  v= v.substring(0,4);
                }
                if (k0=='H' && "HSSP:".equals(key) || k0=='F' && "FSSP:".equals(key)) new AnonymousFTPPROXY().proxyObject();

                if (pubmedOption!=null)  url=rplcToStrg("entrez/query.fcgi?","entrez/query.fcgi?"+pubmedOption,url);
                if (hasNumericID(txt)) v=toStrg( atoi(txt, nxt(DIGT,txt)));
                {
                    final int iRplc=url.indexOf("REPLACE_CHAR./");
                    if (iRplc>0) { url=url.substring(0,iRplc); v=v.replace('.','/'); }
                }
                final String rplc=rplcToStrg("*",v,url);

                String sUrl=rplc!=url?rplc:url+v;
                sUrl=delSfx(',',sUrl);
                if (k0=='W' && "WIKI:".equals(key) && txt.indexOf('(')<0) sUrl=delSfx(')',sUrl);
                if (sUrl!=null && sUrl.indexOf("NCBI_PROTEIN_OR_NUCLEOTIDE")>0) {
                    final char db=ncbiType(v);
                    sUrl=rplcToStrg(STRSTR_w, "NCBI_PROTEIN_OR_NUCLEOTIDE", db==0 || db=='p' ? "protein" : "nucleotide",sUrl);
                }
                //sUrl=rplcNcbiDB(v,sUrl);
                return sUrl;
            }
        }
        /*
          w3m 'http://www.ensembl.org/Gene?g=ENSBTAG00000011829'
          http://www.ensembl.org/Homo_sapiens/Export/Output/Transcript?db=core;flank3_display=0;flank5_display=0;g=ENSG00000120087;output=genbank;r=17:46684590-46710934;strand=feature;t=ENST00000239165;param=similarity;param=repeat;param=genscan;param=contig;param=variation;param=marker;param=gene;param=vegagene;param=estgene;_format=HTML
        */
        if (url_or_DBreference.startsWith("ENSEMBL:")) return  toUrlString(url_or_DBreference.substring(8),mode);
        return null;
    }
    /* <<< toUrlString <<< */
    /* ---------------------------------------- */
    /* >>> NCBI >>> */
    /*
      http://www.ncbi.nlm.nih.gov/Sequin/acc.html
      http://www.ncbi.nlm.nih.gov/RefSeq/key.html#accessions
      http://www.ncbi.nlm.nih.gov/Sitemap/sequenceIDs.html
    */

  private static char ncbiType(String id) {
      char db=0;
        if (strEquls(id,0,3, "AC_ NC_ NG_ NM_ NR_ NS_ NT_ NW_ NZ_ XM_ XR_", 0)) db='p';
        else if (strEquls(id,0,3, "AP_ NP_ XP_ YP_ ZP_", 0)) db='n';
        else if (looks(LIKE_GB_PROTEIN_ID, id)) db='p';
        else if (looks(LIKE_GB_NUCLEOTIDE_ID, id)) db='n';
        return db;
    }
    /* <<< NCBI <<< */
    /* ---------------------------------------- */
    /* >>> Ensembl >>> */
    private static String[][] _ensOrg;
    private static String[][] mapEnsemblOrganisms() {
        if (_ensOrg==null) {
            final BA txt=readBytes(tryRscAsStream(_CCP+"/ensemblOrgs.rsc"));
            _ensOrg=sze(txt)==0?new String[0][] : toDoubleArry(txt, chrClas1('\n'),SPLIT_SKIP_WHITE_SPC, chrClas1('\t'), SPLIT_SKIP_WHITE_SPC);
        }
        return _ensOrg;
    }
    public static String ensemblOrganism(String ensID) {
        if (sze(ensID)<6) return null;
        final char c0=ensID.charAt(0), c1=ensID.charAt(1);
        if (!(c0=='E' && c1=='N' || c0=='O' && c1=='T')) return null;
        for(String[] id_org : mapEnsemblOrganisms()) {
            final int L=sze(id_org[0]);
            if (!ensID.startsWith(id_org[0]) || ensID.length()<L+5) continue;
            final char gtp=ensID.charAt(L);
            if (gtp!='P' && gtp!='T' && gtp!='G') continue;
            if (!cntainsOnly(DIGT, ensID, L+1,MAX_INT)) continue;
            return id_org[1];
        }
        return null;
    }
    public static String ensemblUrl(String id, int mode) {
        final String organism=ensemblOrganism(id);
        if (organism==null) return null;
        final char gtp=chrAt(nxt(DIGT,id)-1, id);
        final String url=
            gtp=='G' ? "Gene?g=" :
            gtp=='T' ? organism+"/Export/Output/Transcript?db=core;output=genbank;strand=feature;param=gene;param=vegagene;_format=Text;t=":
            null;
        return url==null?null:"http://www.ensembl.org/"+url+id;
    }

    /* <<< Ensembl <<< */
    /* ---------------------------------------- */
    /* >>> toDbColonID >>> */

   public final static int idxAW(String haystack, String... needles) {
        for(String s : needles) {
            final int idx=iaw(s,haystack);
            if (idx>0) return idx;
        }
        return -1;
    }
    public static String toDbColonID(String u) {
        final int L=sze(u);
        if (L==0) return null;
        final String lc=u.toLowerCase();
        final int
            equals1=u.indexOf('='),
            equals=u.lastIndexOf('='),
            slash=u.lastIndexOf('/'), quest=u.indexOf('?',slash+1),
            id=maxi(ia("list_uids=",u),ia("&id=",u),ia("?id=",u));
        String pf=null, idList=null;
        int idx, iUniprot=-1;

        if (u.indexOf("strap")>0 && u.indexOf("load=")>0) {
            for(String key : getDatabases(PROTEIN_FILE)[0]) {
                if ((idx=iaw("load="+key,u))>0) {
                    pf=key+wordAt(idx,u,LETTR_DIGT_US);
                    break;
                }
            }
        }
        if (pf!=null) {}
        else if (u.indexOf(".php?align=")>0) pf=urlDecode(u.substring(equals+1,e(equals,u)));
        else if (u.indexOf(".ensembl.org")>0 && u.indexOf("/Align")<0) {
            for(int i=L-5; --i>9;) {
                if (u.charAt(i)=='=' && "ptg".indexOf(u.charAt(i-1))>=0 && !is(LETTR_DIGT_US,u,i-2)) {
                    pf=u.substring(i+1,e(i,u));
                    break;
                }
            }
        } else if (u.indexOf("genome.jp/dbget-bin/")>0 || u.indexOf("genome.ad.jp/dbget-bin/")>0 ) {
            int colon=-1;
            {
                int i=quest+2;
                for(; i<L; i++) if (u.charAt(i)=='+' && is(LETTR_DIGT_US,u,i-1) && is(LETTR_DIGT_US, u,i-2)) { colon=i; break;}
            }
            if (colon<0) colon=u.indexOf(':',quest);
            if (colon>0) pf="KEGG_AA:"+u.substring(prevE(-LETTR_DIGT_US, u,colon-1,-1)+1,colon)+"+"+u.substring(colon+1,nxtE(-LETTR_DIGT_US, u,colon+1,L));
        }
        else if (
                 (idx=idxAW(lc,"http://www.rcsb.org/pdb/files/","entryid=pdb:","pdbid=", "pdb-id:", "db=structure&orig_db=structure&term=",
                            "pdb=","pdb_code=","pdbcode=","get-pdb.pl?","/pdbsum/",
                            "pfam.sanger.ac.uk/structure?id=","macmol.pl?filename="))>0
                 || iaw("PDBsend",u)>0 &&  (idx=iaw("code=",u))>0
                 || iaw("pdbid_search",lc)>0 && (idx=iaw("query=",lc))>0
                 || iaw("entry_pdb.sh",lc)>0 && (idx=iaw("entry=",lc))>0
                 || u.indexOf("dx.doi.org/")>0 && (idx=ia("/pdb",u))>0 && idx+4<L
                 || u.indexOf(".ebi.ac.uk")>0 && u.indexOf("/msd")>0 &&  ( (idx=ia("/summary/",u))>0 || (idx=id)>0)
                 || (idx=ia("/pdbsum/",u))>0
                 || (idx=ia("/ocashort?id=",u))>0
                 || (idx=ia("/pdb/files/",u))>0
                 || (idx=iaw("load=PDB:",u))>0
                 ) {
            pf="PDB:"+ wordAt(idx,u,LETTR_DIGT);
        }
        else if ( (idx=ia("/pdb.cgi?sid=d",u))>0 && u.indexOf("scop")>0 && 5+idx<L) {
            pf="PDB:"+u.substring(idx,idx+4)+":"+u.substring(idx+4,idx+5).toUpperCase();
        } else if ((u.indexOf(".rcsb.org/")>0 || u.startsWith("http://www.pdb.org/") || u.startsWith("http://www.fli-leibniz.de/cgi-bin/ImgLib.pl?CODE=")) && equals>0) {
            if ((idx=ia("accessionIdList=",u))>0) idList=u.substring(idx);
            else if ( (idx=ia("structureId=",u))>0) {
                pf="PDB:"+u.substring(idx,e(idx,u));
                final int c=ia("chainId=",u);
                if (c>0) pf+=":"+u.charAt(c);
            } else  pf="PDB:"+u.substring(equals+1,e(equals,u));
        } else if ( u.indexOf("/pdbe-srv/")>0 && (idx=ia("/entry/",u))>0) { pf="PDB:"+wordAt(idx,u,LETTR_DIGT);
        } else if ( u.indexOf("uk/pdbe")>0 && u.indexOf("searchResults")>0 && (idx=ia("term=",u))>0) { idList=u.substring(idx);
        } else if (  (idx=u.indexOf("UniRef90_"))>0)  { pf="UR090:"+wordAt(idx,u,LETTR_DIGT_US);
        } else if (  (idx=u.indexOf("UniRef50_"))>0)  { pf="UR050:"+wordAt(idx,u,LETTR_DIGT_US);
        } else if (  (idx=u.indexOf("UniRef100_"))>0) { pf="UR100:"+wordAt(idx,u,LETTR_DIGT_US);
        } else if (
                   (u.indexOf("UniProt")>0 && u.indexOf("biomyn.de")>0 && (idx=ia("mid=",u))>0)
                   || (u.indexOf("http://www.tcdb.org/")>=0 && (idx=ia("anum=",u))>0)
                   || (lc.indexOf(".expasy.")>0 &&  (idx=ia("/niceprot.pl?",u))>0)
                   || (idx=idxAW(lc,"uniprot+",
                                 "uniprot:", "uniprot.org/entry/", "uniprot.org/uniref/",
                                 "[swiss_prot-id:'", "[trembl-id:'", "[uniprot-acc:(", "[uniprot-proteinid:",
                                 "sptr_ac=", "uniprot.org/blast/?about=",  "swissprotrelease-accnumber:%27"))>0
                   || (idx=ia("/uniprot/",u))>0
                   || (idx=ia("[UNIPROT-acc:",u))>0
                   ) iUniprot=idx;
        else if ((idx=ia("/uniparc/UPI",u))>0) pf="UPI:"+wordAt(idx-3,u,LETTR_DIGT);
        else if (lc.indexOf("/get-sprot")>0) iUniprot=quest+1;
        else if (strstr(".uniprot.",u)>0 && (idx=strstr(STRSTR_AFTER|STRSTR_w_L,"id=",u))>0) iUniprot=idx;
        else if ((idx=idxAW(u,"[EMBL:'","[emblidacc-id:","emblfetch?id=","EMBL_features-id:","www.ebi.ac.uk/cgi-bin/expasyfetch?"))>0) pf="EMBL:"+wordAt(idx,u,LETTR_DIGT_US);
        else if (u.indexOf("howdy.jst.go.jp")>0 && u.indexOf("GenBank")>0 && (idx=iaw("Val=",u))>0)  pf="GB:"+u.substring(idx,e(idx,u));
        else if ( lc.indexOf(".ncbi.")>0) {
            if (u.indexOf("/cdd/")>0 && (idx=ia("uid=",u))>0) pf="CDD:"+wordAt(idx,u, LETTR_DIGT_US);
            else if ( (idx=ia(".gov/nuccore/",u))>0 || (idx=ia("/nucleotide/",u))>0)  pf="NCBI_NT:"+wordAt(idx,u, LETTR_DIGT_US);
            else if ( (idx=ia(".gov/protein/",u))>0 ) pf="NCBI_AA:"+wordAt(idx,u, LETTR_DIGT_US);
            else if ((u.indexOf("/vast")>0  || u.indexOf("Op=VastSum")>0) && equals>0) pf="NCBI_NEIGHBOR:"+u.substring(equals+1);
            else if (u.indexOf("/mmdb")>0 && equals>0) pf="NCBI_3D:"+wordAt(equals+1,u,LETTR_DIGT_US);
            else if (u.indexOf("cddsrv.cn3")>0 && (idx=ia("uid=",u))>0)  pf="NCBI_3D:"+wordAt(idx,u,LETTR_DIGT_US);
            else {
                final String db=strstr("=protein",lc)>0 ? "NCBI_AA:" :
                    iaw("db=n",lc)>0 || iaw("db=gene",lc)>0 ? "NCBI_NT:" :
                    "GB:";
                int idOrVal=id>0 ? id : iaw("val=",lc);
                if (idOrVal<0) idOrVal= iaw("term=",lc);
                if (idOrVal>0) pf= db+u.substring(idOrVal,e(idOrVal,u));
            }
        }
        else if (u.indexOf("pfam.sanger.")>0  && ( (idx=ia("acc=",u))>0 || (idx=ia("entry=",u))>0 )
                 || (idx=ia("&fam=PF",u)-2)>0 || (idx=ia("/family/PF",u)-2)>0
                 || (idx=ia("&fam=DUF",u)-3)>0 || (idx=ia("/family/DUF",u)-3)>0
                 )
            pf= (u.indexOf("=full")>0? "PFAM_FULL:":"PFAM:")+u.substring(idx,e(idx,u));
        else if (u.indexOf("pfam.sanger.")>0 && (idx=ia("/protein/",u))>0 && cntainsOnly(LETTR_DIGT_US, u, idx, MAX_INT)) pf="UNIPROT:"+u.substring(idx,e(idx,u));
        else if (u.indexOf("prodom")>0 && u.indexOf(".fr/")>0 && (idx=u.indexOf("=PD"))>0) pf="PRODOM:"+u.substring(idx+1,e(idx,u));
        else if (u.indexOf("viperdb.")>0 && (idx=ia("VDB=",u))>0) pf="VIPERDB:"+u.substring(idx,e(idx,u));
        else if ((idx=equals1+1)>0 &&  u.startsWith("http://www.ebi.ac.uk/interpro/") || (idx=ia("[interpro-AccNumber:",u))>0)  {
            pf="INTERPRO:"+wordAt(idx,u,LETTR_DIGT);
        }
        else if (u.indexOf("systers.molgen.mpg.de")>0 && ( (idx=ia("CLNR=",u))>0 || (idx=iaw("nr=",u))>0 ))  pf="SYSTERS_CLUSTER:"+u.substring(idx,e(idx,u));
        else if ( (idx=ia("UNIPARC-refdbsv:",u))>0) pf="UNIPARC:"+wordAt(idx,u,LETTR_DIGT);
        else if ( (idx=ia("[emblcds-id:",u))>0) pf="EMBLCDS:"+wordAt(idx,u,LETTR_DIGT);

        else if ( strstr("strap.php?",u)>0 && ( (idx=ia("align=",u))>0  || (idx=ia("load=",u))>0  || (idx=ia("alignAndRearange=",u))>0)) {
            final int bar=mini( strchr(STRSTR_E,'|',u,idx, MAX_INT), strchr(STRSTR_E,'&',u,idx, MAX_INT), strchr(STRSTR_E,'%',u,idx, MAX_INT));
            if (bar>0) pf=u.substring(idx,bar);
        }
        else if ((idx=iaw("hgnc_id=",u))>0) pf="HGNC:"+wordAt(idx,u,LETTR_DIGT);
        else {
            final int pos=1+prev(-LETTR_DIGT_US,u, L-1, 0);
            if (chrAt(pos-1, u)=='/' || chrAt(pos-1, u)=='=') {
                if (looks(LIKE_UNIPROT_ID, u,pos, MAX_INT)) return "UNIPROT:"+u.substring(pos);
            }
        }

        if (idList!=null) {
            final BA sb=new BA(99);
            for(String uid : splitTokns(strplc(0L, ",", " ", strplc(0L, "%20", "   ", idList)))) {
                if (looks(LIKE_UNIPROT_ID,uid)) sb.a("UNIPROT:").aln(uid);
            }
            pf=toStrg(sb);
        }

        if (pf==null && iUniprot>0) pf="UNIPROT:"+wordAt(iUniprot,u,LETTR_DIGT_US);
        if (pf==null && u.startsWith("javascript")) {
            int i=0;
            while( (i=u.indexOf('\'',i)+1)>1) {
                final int noLDUS=nxt(-LETTR_DIGT_US,u,i,L);
                if (chrAt(noLDUS,u)!='\'') continue;
                final int noLD=nxt(-LETTR_DIGT,u,i,L), lttr=nxt(LETTR,u,i,noLDUS), digt=nxt(DIGT,u,i,noLDUS);
                String db=null;
                if (noLD==noLDUS) {
                    if (noLD==i+LEN_PDB_ID) db="PDB:";
                    if (noLD==noLDUS && noLDUS-i==LEN_UNIPROT  && digt>0 && lttr>0) db="SWISS:";
                } else if (lttr>0) {
                    int countUS=0;
                    boolean goodPos=false;
                    for(int k=i; k<noLDUS; k++) {
                        if (u.charAt(k)=='_') {
                            countUS++;
                            goodPos=k-i>2 && k-i<5 && L-i>2;
                        }
                    }
                    if (goodPos && countUS==1) db="SWISS:";
                }
                if (db!=null) {
                    pf=db+u.substring(i,noLDUS);
                    break;
                }
            }
        }
        return rplcToStrg("%20","",pf);
    }

    /* <<< toDbColonID <<< */
    /* ---------------------------------------- */
    /* >>> References in HTML >>> */
    public static char isHtmlOrXml(BA ba) {
        final byte T[]=ba.bytes();
        final int E=ba.end();
        int  b=nxt(-SPC, ba);
        if (b>=0 && strEquls("<!-- ",T, b) &&  (b=ia(" -->",T,b,E))>0) b=nxtE(-SPC, T,b,E);
        return
            looks(LIKE_HTML,ba) ? 'H' :
            strEquls(STRSTR_IC,"<?xml",T,b) ? 'X' :
            ' ';
    }
    public static CharSequence getProteinReferencesInHtmlFile(BA ba) {
        final byte T[]=ba.bytes();
        final int ends[]=ba.eol(),  B=ba.begin(), E=ba.end();
        final char htmlOrXml=isHtmlOrXml(ba);
        BA ret=null;
        int idx=0;
        if (htmlOrXml=='H' || htmlOrXml=='X') {
            if (strstr(STRSTR_IC,"<title>Ensembl ",T,B,E)>0) {
                for(int transcript=-1; (transcript=strstr("/Transcript/",T,transcript+1,E))>0; ) {
                    final int t=ia(";t=",T,transcript+1,E);
                    if (t<0) break;
                    final String tid=wordAt(t,ba,UPPR_DIGT);
                    (ret==null?ret=new BA(999) : ret).a(tid).a('\t');
                }
                return toStrg(ret);
            }
        }
        if (htmlOrXml=='H') {
            final int body=strstr(STRSTR_IC|STRSTR_w,"<body",ba);
            if (strstr(STRSTR_IC,"<title>SCOP: ",T,B,body)>0) {
                final List<String> v=new ArrayList();
                for(int pdb=0;  (pdb=ia(".cgi?pd=",T,pdb,E))>0; ) {
                    final int gt=strchr('>',T,pdb,E);
                    if (gt>0) {
                        final int chain=ia(";pc=",T,pdb,gt);
                        if (chain>0) adUniq("PDB:"+ba.newString(pdb,pdb+4)+"_"+(char)(get(chain,T)&~32), v);

                    }
                }
                return toStrg(new BA(0).join(v," "));
            } else if (strstr(STRSTR_IC,"<title>Vast",T,B,body)>0) {
                final int iUid=ia("uid=",T,B,E), iLF=strchr('\n',T,iUid+1,E), iQuotes=strchr('"',T,iUid+1,iLF);
                if (iUid>0 && iQuotes>0 && iLF>0) {
                    final int iChain=ia(" sequence ",T,iQuotes,iLF);
                    (ret==null?ret=new BA(999):ret).a("PDB:").a(T,iUid,iQuotes);
                    if (iChain>0) ret.a(':').a((char)lCase(T[iChain]));
                }
            } else if (strstr(STRSTR_IC,"Structure Summary,",ba,0,3333)>0 && (idx=strstr(STRSTR_IC|STRSTR_AFTER,"pdbID=",ba))>0) {
                (ret==null?ret=new BA(999):ret).a("PDB:").filter(FILTER_TO_LOWER, 0, T,idx, nxt(-LETTR_DIGT, T, idx,E));

            }
        }
        if (htmlOrXml!='H') {
            boolean isUniprotList=true;
            for(int iL=0; iL<ends.length; iL++) {
                final int b=iL==0?B:ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                if (e-b>0 && !looks(LIKE_UNIPROT_ID,T,b,e)) {
                    isUniprotList=false;
                    break;
                }
            }
            if (isUniprotList) {
                for(int iL=0; iL<ends.length; iL++) {
                    final int b=iL==0?B:ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                    if (e>b) (ret==null?ret=new BA(16*ends.length) : ret).a("UNIPROT:").a(T,b,e).a(' ');
                }
            }

        }

        if ((htmlOrXml=='H' || htmlOrXml=='X') && sze(ret)==0) {
            String url=null;
            for(int i=B; i<E-2; i++) {
                if (T[i]=='h' && T[i+1]=='t') {
                    if (strEquls(url="http://www.uniprot.org/uniprot/", T, i) || strEquls(url="http://www.uniprot.org/entry/", T, i)) {
                        final int e=nxt(-LETTR_DIGT,T,i+=url.length(),E);
                        if (looks(LIKE_UNIPROT_ID, T,i,e)) (ret==null?ret=new BA(999):ret).a(" UNIPROT:").a(T,i,e).a('\n');
                    }
                    if (strEquls(url="http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?val=", T, i)) {
                        final int e=nxt(-LETTR_DIGT_US,T, i+=url.length(), E);
                        if (e>0) {
                            final String v=toStrg(T,i,e);
                            final char db=ncbiType(v);
                            (ret==null?ret=new BA(999):ret).a(db==0 || db=='p' ? "NCBI_AA:" : "NCBI_NT:").aln(v);
                        }
                    }
                }
            }
        }
        return ret;
    }

    private final static int e(int i, String u) {
        final int amp=u.indexOf('&',i);
        return amp>0 ? amp : u.length();
    }

    private final static int  ia(String needle, Object haystack) {  return strstr(STRSTR_AFTER, needle,haystack); }
    private final static int iaw(String needle, Object haystack) { return strstr( STRSTR_w_L|STRSTR_AFTER, needle,haystack); }
    private final static int  ia(String needle, Object haystack, int haystackFrom, int haystackTo) {  return strstr(STRSTR_AFTER, needle,haystack, haystackFrom,haystackTo); }

    public static void toSeqvista(java.io.File ff[]) {
        java.io.File fJar=InteractiveDownload.downloadFileIfNewer("http://zlab.bu.edu/SeqVISTA/ws/SeqVISTA.jar");
        if (sze(fJar)==0) fJar=InteractiveDownload.downloadFileIfNewer(ChConstants.URL_STRAP_JARS+"/SeqVISTA.jar");
        if (sze(fJar)>0) {
            startThrd(new ChExec(0).setCommandLineV(file(systProprty(SYSP_BIN_JAVA)),"-jar", fJar, ff));
        }
    }
}
