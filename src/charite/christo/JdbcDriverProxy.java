package charite.christo;
import java.sql.*;

class JdbcDriverProxy implements Driver {
	private final Driver _d;
	public JdbcDriverProxy(Driver d) { _d=d;}
	public boolean acceptsURL(String u) throws SQLException { return _d.acceptsURL(u); }
	public Connection connect(String u, java.util.Properties p) throws SQLException { return _d.connect(u, p);}
	public int getMajorVersion() { return _d.getMajorVersion();}
	public int getMinorVersion() { return _d.getMinorVersion(); }
	public DriverPropertyInfo[] getPropertyInfo(String u, java.util.Properties p) throws SQLException { return _d.getPropertyInfo(u, p); }
	public boolean jdbcCompliant() { return _d.jdbcCompliant();	}
}

