package charite.christo;
public class Range {
    public Range() {}
    public Range(int from,int to) { _f=from; _t=to;}
    private int _f, _t;
    public int from(){return _f;}
    public int to()  {return _t;}

    public void set(int f, int t) { _f=f; _t=t;}
    @Override public String toString(){return "["+_f+".."+_t+"]";}
}
