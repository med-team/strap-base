package charite.christo;
import java.io.File;

/**
   @author Christoph Gille
   f.toString()==f.toString()
   f.getAbsolutePath()!=f.getAbsolutePath()
   f.getName()==f.getName()
*/
public class ChFile extends File {
    private final boolean _unknownDrive;
    private static boolean _win;
    private String _toS, _name;
    private int _hc;

    public ChFile(String fn, boolean isWindows) {
        super( (_win=isWindows) && fn.startsWith("?:") ? "C:"+fn.substring(2) : fn);
         _unknownDrive=_win && fn.startsWith("?:");
     }
     public ChFile(File f, String s, boolean windows) {
         super(f,s);
         _win=windows;
         _unknownDrive=false;
     }

    @Override public String toString(){
        if (_toS==null) {
            String s=null;
            try {
                s=getCanonicalPath();
            } catch(Throwable iox) { }
            try {
                if (s==null) s=getAbsolutePath();
            } catch(Throwable iox) { s=iox.toString();}
            _toS= _unknownDrive ? "?:"+s.substring(2) : s;
        }
        return _toS;
    }

     @Override public int hashCode() {
         if (_hc==0) _hc=super.hashCode();
         return _hc;
     }

     @Override public String getName() {
         if (_name==null) _name=super.getName();
         return _name;
     }
}
