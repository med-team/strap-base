package charite.christo;
import java.util.*;
import java.io.*;
import java.util.zip.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class FullClassName  {
    private static long _mc;
    private final static Object[] baHC={null}, vJars={null};
    private FullClassName(){}
    public static String fullClassName(boolean update, String shortCN, String packages[]) {
        if (shortCN==null) return null;
        if (update) {
            long mc=0;
            final BA ba=baClr(baHC);
            for(File f:jarFilesNotInCP()) {
                mc+=((long)ba.clr().a(f.lastModified()).hashCode()<<32)+f.toString().hashCode();
            }
            if (_mc!=mc) {
                _mc=mc;
                ALL[1]=null;
            }
        }
        for(int ext=2;--ext>=0;) {
            final Map<String,BA> byP=allClassesByPackage(ext);
            for(int iP=sze(packages); --iP>=-1;) {
                final String p=iP<0?"java.lang":packages[iP];
                final BA ba=byP.get(p);
                if (ba!=null && strstr(STRSTR_w,shortCN, ba)>=0) {
                    return p+ (p.endsWith(".")?"":".")+shortCN;
                }
            }
            final int dot=strchr(STRSTR_PREV, '.',shortCN);
            if (dot>0) {
                final BA ba=byP.get(shortCN.substring(0,dot));
                if (strstr(STRSTR_w, shortCN.substring(dot+1),ba)>=0) return shortCN;
            }
        }
        return null;
    }
    public static InputStream javaSourceAsStream(String cn0) {
        if (cn0==null) return null;
        final String cn=delSfx(".java",cn0).replace('.','/')+".java";
        InputStream is=CLASSLOADER.getResourceAsStream(cn);
        if (is==null) {
            for(File f : jarFilesNotInCP()) {
                try {
                    final ZipFile jf=new ZipFile(f);
                    final ZipEntry je=jf.getEntry(cn);
                    if (je!=null) is=jf.getInputStream(je);
                    break;
                } catch(Exception ex){ putln(RED_CAUGHT_IN+"sourceCodeAsStream ",ex,"\n",f); }
            }
        }
        return is;
    }
    private final static Map ALL[]={null,null};
    private static Map<String,BA> allClassesByPackage(int i) {
        if (ALL[i]==null) {
            final long time=System.currentTimeMillis();
            ALL[i]=new HashMap();
            if (i==0) {
                final BA baTmp=new BA(99);
                for(File f : classpth(true,null)) {
                    if (f.toString().endsWith(".jar")) allInZip(f,ALL[0]);
                    if (f.isDirectory()) allInTree(f.getAbsolutePath().length()+1, f, ALL[0], baTmp);
                }
            } else {
                for(File f : jarFilesNotInCP()) allInZip(f,ALL[1]);
            }
            if (myComputer()) {
                debugTime(i+" allClassesByPackage time=", time);
            }
        }
        return ALL[i];
    }
    private static String _pck="";
    private static BA _ba;
    private static void addClass(BA cn, Map m) {
        final int dot=strchr(STRSTR_PREV, '.',cn);
        final boolean old=dot<0 ? _pck=="" : dot==_pck.length() && strEquls(_pck, cn, 0);
        BA ba=null;
        if (old) ba=_ba;
        else {
            _pck=dot<0?"" : cn.newString(0,dot);
            ba=(BA)m.get(_pck);
            if (ba==null) {
                m.put(_pck, ba=new BA(333));
                m.put(_pck+".", ba);
            }
            if (_ba!=null) {
                _ba.trimSize();
            }
            _ba=ba;
        }
        ba.a(cn,dot+1,MAX_INT).a(' ');
    }
    private final static void allInZip(File f, Map m) {
        final BA baTmp=new BA(99);
        try {
            final Enumeration entries=new ZipFile(f).entries();
            while(entries.hasMoreElements()) {
                final String name=((ZipEntry)entries.nextElement()).getName();
                if (name.indexOf('$')>=0 || !name.endsWith(".class")) continue;
                baTmp.clr().a(name, 0, name.length()-6).replaceChar('/','.');
                addClass(baTmp, m);
            }
        } catch(Exception ex){ putln(RED_CAUGHT_IN+"allClassInZip ",f); putln(ex);}
    }
    private final static void allInTree(int base, File dir, Map m, BA baTmp) {
        for(String f : lstDir(dir)) {
            final int L=f.length();
            if (L>0 && f.charAt(L-1)=='s' && f.indexOf('$')<0 && f.endsWith(".class")) {
                baTmp.clr()
                    .a(dir,base,MAX_INT).a('.')
                    .a(f, 0, L-6).replaceChar('/','.').replaceChar('\\','.');
                addClass(baTmp, m);
            } else if (f.indexOf('.')<0) {
                final File dir2=new File(dir,f);
                if (dir2.isDirectory()) allInTree(base, dir2, m, baTmp);
            }
        }
    }
    private final static File[] jarFilesNotInCP() {
        final List v=vClr(vJars);
        for(int d= JAVADIR_MAX+1; --d>=0;) {
            final File dir=dirJava(d);
            for(String fn : lstDir(dir)) {
                if (fn.endsWith(".jar")) v.add(file(dir,fn));
            }
        }
        return toArry(v,File.class);
    }

}
