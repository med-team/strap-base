package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
public class ChSet<E> implements Set<E>, HasMC {

    public final static long  WEAK_REFERENCE=1<<0, IDENTITY=1<<1;
    private int _mc=0;
    public int mc() { return _mc;}
    private Set<E> _set;
    private Map<E,String> _map;
    private final Class _clazz;
    private final long _options;
    public ChSet(long options, Class clazz) {
        _options=options;
        _clazz=clazz;
    }

    private Set<E> set() { return _set==null ? _set=new HashSet() : _set; }

    private Set<E> theSet() { return 0!=((IDENTITY|WEAK_REFERENCE)&_options) ? _map().keySet() : set(); }

    private Map _map() {
        if (_map==null) _map=0!=(_options&IDENTITY) ? new IdentityHashMap() : new WeakHashMap();
        return _map;
    }
    public Iterator iterator() {
        return theSet().iterator();
    }
    public int size() {
        return _map==null && _set==null ? 0 : theSet().size();
    }
    public boolean isEmpty() { return size()==0;}
    public boolean contains(Object o) {
        return _map!=null ? _map.containsKey(o) : _set!=null ?_set.contains(o) : false;
    }
    public boolean add(E o) {
        if (o==null) return false;
        final boolean changed= 0!=(_options&WEAK_REFERENCE) ? null==_map().put(o, "") : set().add(o);

        if (changed) _mc++;
        return changed;
    }
    public boolean remove(Object o) {
        final boolean changed=_map!=null ?  _map.remove(o) == "" : _set!=null ? _set.remove(o) : false;
        if (changed) _mc++;
        return changed;
    }
    public void clear() {
        boolean changed=false;
        if (_map!=null) { changed=_map.size()>0; _map.clear(); }
        if (_set!=null) { changed=_set.size()>0; _set.clear(); }
        if (changed) _mc++;
    }
    public boolean removeAll(Collection<?> c) {
       final boolean changed=_map!=null ? _map.keySet().removeAll(c) : _set!=null?_set.removeAll(c) : false;
       if (changed) _mc++;
       return changed;
    }
    public boolean retainAll(Collection<?> c) {
        final boolean changed=_map!=null ? _map.keySet().retainAll(c) : _set!=null ? _set.retainAll(_set) : false;
        if (changed) _mc++;
        return changed;
    }
    public boolean addAll(Collection<? extends E> c) {
        if (c==null) return false;
        boolean changed=false;
        if (0!=(_options&WEAK_REFERENCE)) {
            for(Object o : c.toArray()) {
                changed|=_map().put((E)o,"")!="";
            }
        } else changed=_set.addAll(c);
        if (changed) _mc++;
        return changed;
    }
    public boolean containsAll(Collection<?> c) {
        return _map!=null ? _map.keySet().containsAll(c) : _set!=null ? _set.containsAll(c) : false;
    }
    public Object[] toArray() {
        return _map!=null ? _map.keySet().toArray() : _set!=null ? _set.toArray() : NO_OBJECT;
    }
    public <T> T[] toArray(T[] a) {
        return _map!=null ? _map.keySet().toArray(a) :  set().toArray(a);
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Arrays >>> */
    private final static int C_ARRAYv=1;
    private Object refCached;
    private int lastSize;
    private Object[] cached() {
      Object oo[]=(Object[])deref(this.refCached);
        final int L=size();
        if (oo==null) refCached=newSoftRef(oo=new Object[2]);
        else if (this.lastSize!=L) for(int i=oo.length; --i>=0;) oo[i]=null;
        this.lastSize=L;
        return oo;
    }

    public E[] asArray() { return asArray(null,null);}
    public E[] asArray(ChRunnable run, String runID) {
        final Object cached[]=cached();
        E[] vv=(E[])cached[C_ARRAYv];
        if (vv==null) {
            cached[C_ARRAYv]=vv=(E[])toArray(emptyArray(_clazz));
            if (run!=null) run.run(runID,vv);
        }
        return vv;
    }

}

