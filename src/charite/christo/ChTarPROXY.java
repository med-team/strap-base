package charite.christo;
import java.io.*;
/**HELP
  <b>Home:</b> http://www.gjt.org/servlets/JCVSlet/list/ice/com/ice/tar/ <br>
  <b>License of tar.zip:</b> tar/doc/LICENSE says: "...You are free to use this work in any way you wish..."
*/

/* libgeronimo-activation-1.1-spec-java */
public class ChTarPROXY extends AbstractProxy {
    @Override public String getRequiredJars() {  return PFX_INSTALLED+"javatar|tar.jar " +jarFile(ACTIVATION);}
    public boolean extractAll(int opt, File fTar, File dir) {
        final String fn=fTar.toString();
        if (fn.endsWith(".jar") || fn.endsWith(".zip")) return ChZip.extractAll(opt,fTar,dir);
        else {
            final ChRunnable r=((ChRunnable)proxyObject());
             return null!=r && null!=r.run(ChTar.EXTRACT, new Object[]{new Integer(opt),fn,dir});
        }
    }
}
