package charite.christo;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.ref.*;
import java.net.URL;
import java.util.*;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.util.List;
import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.border.*;
import javax.swing.event.*;
import charite.christo.hotswap.*;
import javax.swing.tree.TreePath;
import static java.lang.reflect.Array.newInstance;
import static charite.christo.ChConstants.*;
import static java.awt.event.KeyEvent.*;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.FocusEvent.*;
import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.FALSE;
import static java.lang.System.currentTimeMillis;
import static javax.swing.BorderFactory.*;
/**
   Static utility methods.

   http://docs.oracle.com/javase/6/docs/api/java/util/concurrent/locks/Lock.html
   @author Christoph Gille
*/
public final class ChUtils implements ChRunnable, ProcessEv {
    private final static Object
        KOPT_DRAG_ALTKEY[]={new Object(),new Object()},
        SYNC_FASTEST_P=new Object(),
        SYNC_SPLIT=new Object(),
        KOPT_DISABL_EVNTS=new Object(),
        KEY_ORIG_FONT_SIZE=new Object(),
        KEY_RESCUE_MENUB=new Object(),
        KEY_DRAGW=new Object(),
        KEY_FOCUS_LOST_WHEN=new Object();
    final static Object
        KEY_EVENT_SOURCE=new Object(),
        KEY_TOGGLE_COLLAPSE[]={new Object(),new Object()};
    public final static long REQUEST_IMAGE_PACK=1<<1, REQUEST_IMAGE_REMOVE_TEXT=1<<2, REQUEST_IMAGE_REMOVE_BORDER=1<<3;

    public final static String
        KEY_REQUEST_IMAGE_MAX_WIDTH="CU$$RIMW", KEY_REQUEST_IMAGE_OPTIONS="CU$$RIO",
        TTContextAndDnD="Right-click for context menu.  Mouse drag for copying files.",
        USER_DOT_HOME="user.home",
        KEY_DRAG="CUs$$_26",
        KOPT_JUST_WRITING_PNG="CUs$$_27",
        PROP_NMR_ONLY_FIRST_MODEL="NMR_ONLY_FIRST_MODEL",
        STRAP_PACKAGES[]={_CC,_CCP,_CCS, _CCB, _CCSSC, _CCSE, _CCM},
        TRUSTED_PLUGINS[]={"http://www.bioinformatics.org/strap/plugins/"},
        COMPRESS_SUFFIX[]={".gz",".z",".GZ",".Z",".bz2"},
        JPG_PNG_GIF_BMP[]=".jpeg .jpg .png .gif .bmp .JPEG .JPG .PNG .GIF .BMP".split(" "),
        NO_STRING[]={},
        ERROR_STRING=new String(),
        ACTION_BECOME_INVISIBLE="CU$$ABI",
        KOPT_POPUP_MENU="CU$$RM",
        REMAINING_VSPC1="CU$$RVS1", REMAINING_VSPC2="CU$$RVS2",
        CNSEW="CNSEW",
        FLOWRIGHT="CU$$FR", FLOWLEFT="CU$$FL", HB="hB", VB="vB", VBHB="vBhB", HBL="HBL", VBPNL="VBPNL",
        SOME_HTML_TAGS[]="</span>;</div>;<br>;<hr>;<h1>;<h2>;<h3>;<pre>;<ul>;<li>;<b>;<del>;<u>;<i>;<I>;<center>;<sup>;<sub>;</font>;</a>;</pre>;<img src=\";<head>;<pre class=\"terminal\">".split(";"),
        SOME_CLOSING_HTML_TAGS[]="</h1> </h2> </h3> </li> </ul> </b> </u> </i> </I> </pre> </center> </sup> </sub> </head>".split(" ");
    public final static Object
        NO_OBJECT[]={}, ERROR_OBJECT="CUs$$ERROR_OBJECT",
        CNTXT_OBJCTS[]={null},
        SYNC_CPY=new Object();

    public final static int
        SYSP_OS_ARCH=1,
        SYSP_OS_NAME=2,
        SYSP_OS_VERSION=3,
        SYSP_JAVA_VERSION=4,
        SYSP_JAVA_HOME=5,
        SYSP_JAVA_CLASS_PATH=6,
        SYSP_USER_NAME=7,
        SYSP_USER_HOME=8,
        SYSP_USER_DIR=9,
        SYSP_PATH_SEP=10,
        SYSP_ICON_FILE_BROWSER=11,
        SYSP_NAME_FILE_BROWSER=12,
        SYSP_BIN_JAVA=13,
        SYSP_SYS_LAF=14,
        IS_WINDOWS=19,
        IS_LINUX=20,
        IS_MAC=21,
        IS_MAC7=22,
        IS_86=23,
        IS_LINUX386=24,
        IS_WINDOWS_95_FAMILY=25,
        IS_WEBSTARTED=26,
        IS_OPENJDK=27,
        IS_KNOW_PACKAGE_MANAGER=28,
        IS_DEBIAN=29,
        IS_SUSE=30,

        IS_USE_INSTALLED_SOFTWARE=33,

        IS_UNIX_SHELL=34;
    public final static Class[] NO_CLASS={};
    public final static Map[] NO_MAP={};
    public final static Map.Entry[] NO_MAP_ENTRY={};
    public final static URL NO_URL[]={}, ERROR_URL;
    static { URL u=null; try { u=new URL("http://www.sun.com");} catch(Exception e){assrt();} ERROR_URL=u; }
    public final static byte[] NO_BYTE={}, NO_BYTE_ARRAY[]={};
    public final static int[] NO_INT={};
    public final static long NO_LONG[]={}, TIME_AT_START=currentTimeMillis();
    public final static boolean[] NO_BOOLEAN={}, ERR_BOOLEAN={},  CAN_BE_STOPPED={}, FALSE_TRUE={false,true};
    public final static File NO_FILE[]={}, ERROR_FILE=new File("___");
    public final static Map.Entry[] NO_Entry={};
    public final static ClassLoader CLASSLOADER=ChUtils.class.getClassLoader();
    private ChUtils() {}
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Msg  >>> */
    private final static Set vErrorAlreadyReported=new HashSet();
    public static void error(CharSequence s) {
        if (!withGui()) putln(s);
        else {
        if (chrAt(0,s)=='@') putln(s);
        ChMsg.showError(s,null);
        }
    }
    public static void error(String s, Component jc) { ChMsg.showError(s,jc);}
    public static void error(Throwable err,String msg) {
        if (withGui()) {
            if (err instanceof OutOfMemoryError) {
                System.gc();
                System.gc();
                error("Caught OutOfMemoryError <br>"+
                      "Increase memory of the Java Runtime Environment and try again.<br><br>DETAILS:<br>"+msg+"<br><br>"+
                      stckTrcAsStrg(err)
                      );
            } else error(toStrgN(msg)+"<pre>\n"+stckTrcAsStrg(err)+"</pre>");
        } else putln(RED_ERROR,msg,"\n",err);
    }
    private static BA _logUnavail, _logInf3D;
    public static BA logUnavailable() {
        if (_logUnavail==null) toLogMenu(_logUnavail=new BA(999), "Unavailable services", IC_UNHAPPY);
        return _logUnavail;
    }
    public static BA logInfer3D() {
        if (_logInf3D==null) _logInf3D=new BA(99*1000).sendToLogger(0, "Infer 3D coordinates",IC_3D, 99*1000);
        return _logInf3D;
    }

    /* <<< Msg <<< */
    /* ---------------------------------------- */
    /* >>> Debug >>> */
    public static String dateOfCompilation() { return orS( toStrgTrim(readBytes(rscAsStream(0L,_CC, "dateOfCompilation.rsc"))), "?????"); }
    private static int _myComp, _totalClicks;
    public static boolean myComputer() {
        if (_myComp==0) {
            try {
                _myComp=new File("/tmp/meine_IP_Nummer.txt").exists() ? CTRUE : CFALSE;
            } catch(Throwable ex) {_myComp=CFALSE;}
        }
        return _myComp==CTRUE;
    }
    public static void debugTime(CharSequence msg, long time) {
        if (myComputer()) {
            putln(ANSI_CYAN+ANSI_FG_WHITE+"debugTime "+ANSI_RESET);
            puts(msg);
            System.out.print(' ');
            System.out.print(currentTimeMillis()-time);
            System.out.println(" ms\n");
        }
    }
    public static void stckTrc() { stckTrc(new Exception("My stack trace"));}
    private final static Set _vMsg1=new HashSet();
    public static void stckTrc1(Throwable ex) {
        final String s=stckTrcAsStrg(ex!=null?ex:new Exception("My stack trace"));
        if (s==null) return;
        int at=0;
        for(int i=7; --i>=0;) {
            final int at2=s.indexOf("at ",at+1);
            if (at2<0) break;
            at=at2;
        }
        if (at>0 && _vMsg1.add(new Integer(hashCd(s,0,at)))) {
            putln("\n",s);
        }
    }
    public static void stckTrc(Throwable e) { stckTrc(e,System.out);}
    public static void stckTrc(Throwable e, PrintStream stream) {
        if (e==null) return;
        stream.println(ANSI_GREEN+"Caught stckTrc currentThread()="+ANSI_RESET+Thread.currentThread()+"\n"+e+"\n");
        e.printStackTrace(stream);
        stckTrc(e.getCause(), stream);
        stream.print("\n\n");
        if (mapStckTrace!=null) {
            final AWTEvent ev=Toolkit.getDefaultToolkit().getSystemEventQueue().getCurrentEvent();
            java.lang.reflect.Field f=null;
            if (ev!=null) {
                Runnable r=null;
                try {
                    f=ev.getClass().getDeclaredField("runnable");
                    if (f!=null) {
                        f.setAccessible(true);
                        r=(Runnable)f.get(ev);
                    }
                } catch(Throwable ex) { ex.printStackTrace(System.out);}
                stream.println(BLUE_DEBUG+"getSystemEventQueue().getCurrentEvent().runnable="+r);
                stckTrcPrint(r);
            }
            stckTrcPrint(Thread.currentThread());
        }
    }

    private static void stckTrcPrint(Runnable r) {
        if (r==null || mapStckTrace==null) return;
        final Exception[] xx=mapStckTrace.get(r);
        putln(BLUE_DEBUG+"stckTrc r="+r+"   #"+sze(xx));
        for(int k=0; k<sze(xx); k++) {
            if (xx[k]!=null) xx[k].printStackTrace();
        }
        if (r instanceof Thread) {
            java.lang.reflect.Field f=null;
            try{
                f=Thread.class.getDeclaredField("target");
                f.setAccessible(true);
                final Runnable target=(Runnable)f.get(r);
                if (r!=target) stckTrcPrint(target);
                putln(BLUE_DEBUG+"stckTrcPrint Thread.target="+target);
            } catch(Throwable ex) {}
        }
    }
    public static String stckTrcAsStrg(Throwable t) {
        final OutputStream os=new ByteArrayOutputStream();
        stckTrc(t!=null?t:new Throwable(), new PrintStream(os));
        return os.toString();
    }
    private static boolean _logFinalize;
    public static void logFinalize(String txt,Object o){
        if (txt=="TRUE") _logFinalize=true;
        if (!_logFinalize) return;
        System.out.print(txt);
        System.out.print(ANSI_RESET);
        if (o!=null) { puts(" "); puts(o instanceof Class ? shrtClasNam((Class)o) : o); puts(" "); }
    }
    /* <<< Debug <<< */
    /* ---------------------------------------- */
    /* >>> Properties >>> */
    private final static String[] _systProprty=new String[99];
    public static String sysGetProp(String key) {
        ChClassLoader1.setSecurityPolicy();
        try {
            return orS(toStrgIntrn(System.getProperty(key)),"");
        } catch(Throwable e){ putln(e);}
        return "";
    }
    public static boolean isWin() { return isSystProprty(IS_WINDOWS);}
    public static boolean isMac() { return isSystProprty(IS_MAC);}
    public static boolean isMacLaf() { return  isMac() && LAFChooser.isSystemLAF(); }
    public static boolean isScreenMenuBar() { return isMacLaf() && isTrue(sysGetProp("apple.laf.useScreenMenuBar")); }

    public static String installCmdForPckgs(Object packages) {
        final boolean isD=isSystProprty(IS_DEBIAN), isS=isSystProprty(IS_SUSE);
        if (!isSystProprty(IS_KNOW_PACKAGE_MANAGER) || sze(packages)==0) return null;
        final BA sb= new BA(99).a(isD ? "apt-get install " : isS ? "/sbin/yast2 --install ": "");
        if (packages instanceof CharSequence) sb.a(packages);
        else if (packages instanceof Object[]) sb.join(packages, " ");
        else assrt();
        return sb.toString();
    }
    public static String systProprty(int i) {
        String p=_systProprty[i];
        if (p==null) {
            switch(i){
            case SYSP_OS_ARCH:      p=sysGetProp("os.arch"); break;
            case SYSP_OS_NAME:      p=sysGetProp("os.name"); break;
            case SYSP_OS_VERSION:   p=sysGetProp("os.version"); break;
            case SYSP_JAVA_VERSION: p=sysGetProp("java.version"); break;
            case SYSP_JAVA_HOME:    p=sysGetProp("java.home"); break;
            case SYSP_BIN_JAVA:     p=sysGetProp("java.home")+"/bin/java"+(isWin() ? ".exe":""); break;
            case SYSP_USER_NAME:    p=sysGetProp("user.name"); break;
            case SYSP_USER_DIR:     p=sysGetProp("user.dir"); break;
            case SYSP_PATH_SEP:     p=orS(sysGetProp("path.separator"),isWin()?";":":"); break;
            case SYSP_ICON_FILE_BROWSER: p=isMac() ? IC_MAC_FINDER : IC_DIRECTORY; break;
            case SYSP_NAME_FILE_BROWSER: p=isMac() ? "Finder" : isWin() ? "Explorer" : "native file browser"; break;
            case SYSP_JAVA_CLASS_PATH: p=sysGetProp("java.class.path"); break;
            case SYSP_USER_HOME:
                synchronized(USER_DOT_HOME) {
                    p=sysGetProp(USER_DOT_HOME);
                    if (sze(p)==0 || p.indexOf('%')>=0) {
                        p=ChMsg.askUserHome(p);
                        if (p==null) System.exit(1);
                        mkdrs(new File(toStrgTrim(p)));
                    }
                }
                break;

            case SYSP_SYS_LAF:
                try {
                    p=UIManager.getSystemLookAndFeelClassName();
                    if (p==null) p="";
                } catch(Exception ex) {}
                break;
            }
            _systProprty[i]=p;
        }
        return toStrgN(p);
    }
    public static String urlOfBinariesForThisOS() {
        final String dir=isSystProprty(IS_LINUX386) ? "linux386" : isWin() ? "windows" : isMac() ? (isSystProprty(IS_86) ? "macIntel" : "mac") : null;
        return dir!=null ? URL_STRAP+"Binaries/"+dir+"/" : null;
    }
    public static String dirJavaws() { return sysGetProp("java.home")+File.separatorChar+(javaVsn()==14 ? "javaws" : "bin")+File.separatorChar;  }
    public static String jControl() { return (isWin() ? (javaVsn()==14 ? "javaws.exe" : "javacpl.exe") : (javaVsn()==14 ? "javaws" : "ControlPanel"));}
    public static String fileJcontrol() { return dirJavaws()+jControl(); }
    public static String msgJcontrol() {
        return
            "The tool "+fileJcontrol()+" in "+dirJavaws()+
            (" allows three important settings:\n"+
             "1. The Web-proxy. A wrong setting may be the reason why Strap cannot download files.\n"+
             "2. Activation of the text console for the standard output.\n"+
             "3. Javaw verion. A wrong setting can be the reason why still 1.4 is used even though a higher version is installed.\n");
    }
    private static byte[] _isSystProprty=new byte[99];
    public static boolean isSystProprty(int i) {
        int is=_isSystProprty[i];
        if (is==0) {
            final boolean b;
            switch(i) {
            case IS_USE_INSTALLED_SOFTWARE: b=LINUX_PACKAGE; break;
            case IS_WINDOWS: b= File.separatorChar=='\\'; break;
            case IS_LINUX: b=systProprty(SYSP_OS_NAME).toLowerCase().indexOf("linux")>=0; break;
            case IS_MAC7:
                b=isMac() && 6<atoi(delPfx("10.",systProprty(SYSP_OS_VERSION)));
                break;
            case IS_MAC: b=systProprty(SYSP_OS_NAME).toLowerCase().indexOf("mac")>=0; break;
            case IS_86: b=systProprty(SYSP_OS_ARCH).indexOf("86")>=0 || systProprty(SYSP_OS_ARCH).toLowerCase().indexOf("amd64")>=0; break;
            case IS_LINUX386: b=isSystProprty(IS_LINUX) && isSystProprty(IS_86); break;
            case IS_WINDOWS_95_FAMILY:
                final String v=isWin() ? systProprty(SYSP_OS_VERSION) : "";
                b=v.indexOf("95")>=0 || v.indexOf("98")>=0 || v.indexOf("ME")>=0; break;
            case IS_WEBSTARTED:
                ClassLoader cl=CLASSLOADER;
                boolean webstart=false;
                while(cl!=null) {
                    if (cl.getClass().getName().indexOf("JNLP")>=0) {
                        webstart=true;
                        break;
                    }
                    try { cl = cl.getParent(); } catch(Exception ex){ break;}
                }
                b=webstart;
                break;
            case IS_OPENJDK: b=strEquls(STRSTR_IC, "OpenJDK", sysGetProp("java.vm.name")); break;
            case IS_KNOW_PACKAGE_MANAGER: b=isSystProprty(IS_DEBIAN) || isSystProprty(IS_SUSE); break;
            case IS_DEBIAN: b=fExists("/etc/apt/sources.list"); break;
            case IS_SUSE: b=fExists("/sbin/yast2"); break;
            case IS_UNIX_SHELL:
                b=!isWin() || strstr("bash",ChEnv.get("SHELL"))>=0;
                break;
            default: b=false;
            }
            is=_isSystProprty[i]=(byte)(b?'t':'f');
        }
        return 't'==is;
    }
    private static int _javaVsn=-1;
    public static int javaVsn() {
        if (_javaVsn<0) _javaVsn=atoi(new BA(4).filter(0,DIGT, sysGetProp("java.specification.version") ));
        return _javaVsn;
    }
    private static ChRunnable[] _runHigher;
    public static ChRunnable forJavaVersion(int version) {
        if (_runHigher==null) _runHigher=new ChRunnable[18];
        ChRunnable r=_runHigher[version];
        if (r==null) {
            if (javaVsn()>=version) {
                try {
                    r=(ChRunnable)Class.forName(_CC+"RunIf"+version+"orHigher").newInstance();
                } catch(Throwable e){stckTrc();}
                if (null==r) assrt();
            }
            if (r==null) r=instance(DO_NOTHING_ChRunnable);
            _runHigher[version]=r;
        }
        return r;
    }

    public static File[] classpth(boolean cpAtStart, File dir) {
        if (_cpAtStart==null) {
            final String ss[]=splitTokns(systProprty(SYSP_JAVA_CLASS_PATH), chrClas(systProprty(SYSP_PATH_SEP)));
            final List<File> v=new ArrayList(ss.length);
            for(String s : ss) adUniq(file(s),v);
            final String url=toStrg(urlThisJarFile());
            adUniq( strEquls("file:",url) ?   file(url) : thisJarFile(), v);
            _cpAtStart=toArry(v,File.class);
        }
        if (dir==null) return _cpAtStart;
        return derefArray(new Object[]{ChZip.getInstance(dir).jarFiles(), dir, cpAtStart? _cpAtStart:null}, File.class);
    }
    /* <<< Properties <<< */
    /* ---------------------------------------- */
    /* >>> Object pools >>>  */
    private final static Object _cObj[]=new Object[256], _map4CurrThrd[]={new WeakHashMap(),null};
    public static BA ba4CurrThrd(Object id) {
        final Map m=map4CurrThrdSoftRef();
        BA ba=(BA)m.get(id);
        if (ba==null) m.put(id, ba=new BA(999));
        else ba.clr();
        return ba;
    }

    public static Map map4CurrThrd() { return _map4CurrThrd(false);}
    public static Map map4CurrThrdSoftRef() { return _map4CurrThrd(true);}
    private static Map _map4CurrThrd(boolean soft) {
        synchronized(_map4CurrThrd) {
            Map m0=get(soft?1:0,_map4CurrThrd,Map.class);
            if (m0==null) _map4CurrThrd[1]=newSoftRef(m0=new WeakHashMap());
            Map m=deref(m0.get(Thread.currentThread()),Map.class);
            if (m==null) m0.put(Thread.currentThread(), m=new HashMap());
            return m;
        }
    }

    private final static Dimension[] _dim=new Dimension[1000];
    public static Dimension dim(int x, int y) {
        int i=(x+y>=0?x+y:-x-y)%_dim.length;
        if (i<0) i=0;
        Dimension d=_dim[i];
        if (d==null || d.width!=x || d.height!=y) {
            //puts(ANSI_CYAN+" dim "+ANSI_RESET);
            _dim[i]=d=new Dimension(x,y);
        }
        return d;
    }
    private final static Integer _int[]=new Integer[1000];
    public static Object intObjct(int num) {
        int i=(num>=0?num:-num)%_dim.length;
        if (i<0) i=0;

        Integer d=_int[i];
        if (d==null || d.intValue()!=num) {
            _int[i]=d=new Integer(num);
        }
        return d;
    }
    private final static Long _long[]=new Long[1000];
    public static Object longObjct(long num) {
        int i=(int)((num>=0?num:-num)%_dim.length);
        if (i<0) i=0;
        Long d=_long[i];
        if (d==null || d.longValue()!=num) {
            _long[i]=d=new Long(num);
        }
        return d;
    }
    public static Object boolObjct(boolean b) { return b?TRUE:FALSE;}
    public static Object charObjct(char i) { return i>255 ? new Character(i) : _cObj[i]==null ? _cObj[i]=new Character(i) : _cObj[i]; }
    /* <<< Object pools <<< */
    /* ---------------------------------------- */
    /* >>> JMenu >>> */
    public static JComponent findMenuItem(String label, JMenu menu) {
        for(Component item : childs(menu)) {
            if (item instanceof JMenu  && label.equalsIgnoreCase(getTxt(item))) return (JComponent)item;
        }
        return null;
    }
    public static void noMenuIconsOnMac(Component m) {
        if (!isScreenMenuBar() || m==null || !(m  instanceof JMenuBar) && gcp(KOPT_NO_ICON,m)!=null) return;
        pcp(KOPT_NO_ICON,"",m);
        if (m instanceof JMenu || m instanceof JMenuBar) {
            for(Component c : childs(m)) noMenuIconsOnMac(c);
        }
    }

    public static JMenuBar menuBar2j(MenuBar mb, String[][] rplcMenuItems) {
        if (mb==null) return null;
        final JMenuBar jmb=new ChJMenuBar();
        for(int i=0;i<mb.getMenuCount(); i++) jmb.add(new ChJMenu("").setAwtMenu(mb.getMenu(i),rplcMenuItems));
        return jmb;
    }
    public static void setHeavy(Component c) {
        final JPopupMenu pop=deref(c, JPopupMenu.class);
        final JMenu jm=deref(c, JMenu.class);
        if (pop!=null) pop.setLightWeightPopupEnabled(false);
        else if (jm!=null) {
            for(int i=jm.getItemCount(); --i>=0;) setHeavy(deref(jm.getItem(i),JMenu.class));
            setHeavy( ((JMenu)c).getPopupMenu());
        } else if (c instanceof JMenuBar) {
            final JMenuBar mb=(JMenuBar)c;
            for(int i=mb.getMenuCount(); --i>=0;) setHeavy(mb.getMenu(i));
        } else for(Component child : childs(c)) setHeavy(child);
    }
    public static void removeMenuItems(String[] menus, String items[], JMenuBar menubar) {
        if (menubar==null) return;
        for(String menu : menus) {
            final JMenu m=child(menubar,JMenu.class,menu);
            for(String item : items) {
                final JComponent mi=findMenuItem(item,m);
                if (mi!=null) m.remove(mi);
            }
        }
    }
    private final static List _vAllMenuitems=new ArrayList(100);
    public static AbstractButton getEquivalentMenuButton(Object buttonOrString, Object menuID) {
        final AbstractButton b=buttonOrString instanceof AbstractButton ? ((AbstractButton)buttonOrString) :null;
        final String txt=b!=null ? b.getText() : toStrg(buttonOrString);
        if (txt==null) return null;
        for(int i=sze(_vAllMenuitems); --i>=0;) {
            final AbstractButton mi=get(i,_vAllMenuitems,AbstractButton.class);
            if (mi==null || !txt.equals(mi.getText())) continue;
            if (b!=null && gcp(KEY_MENU_ID,b)!=gcp(KEY_MENU_ID,mi)) continue;

            if (menuID!=null && menuID!=gcp(KEY_MENU_ID,mi)) continue;
            return mi;
        }
        return null;
    }
    public static void setLi4CntxtMenu(EvAdapter a) { _defaultPopLi=a; }
    private static EvAdapter _defaultPopLi;
    public static void addLi4CntxtMenu(Component c) { if (_defaultPopLi!=null) _defaultPopLi.addTo("m",c); }
    public final static int MENU_NO_ICON=1<<0, MENU_HAS_ACCELERATOR=1<<1;
    public static JMenu jMenu(Object oo[]) { return jMenu(0,oo,"");}
    public static JMenu jMenu(int options, Object oo[], String parent) {
        final ChJMenu jMenu=new ChJMenu("");
        if (0==(options&MENU_NO_ICON)) jMenu.i(iicon(IC_BLANK));
        adToMenu(options, jMenu, oo,parent);
        addMoli(0,jMenu);
        return jMenu;
    }
    public static JComponent[] jMenus(int options, Object oo[], ActionListener li, String parent) {
        final List v=new ArrayList();
        for(Object o : oo) {
            final JComponent m=o instanceof Object[] ? jMenu(options,(Object[])o,parent) : derefJC(o);
            if (m!=null) {
                v.add(m);
                addActLi(li,m);
            }
        }
        return toArryClr(v,JComponent.class);
    }
    public final static String KEY_BEFORE_MENU_ITEM="PVU$$BI";
    public static JMenuBar toMenuBar(int options, Component mm[], JMenuBar mb) {
        for(Component m : mm) {
            if (m==null) continue;
            final ChJMenu jm=deref(m,ChJMenu.class);
            if (jm!=null) jm.i(null).setIcon(null);
            if ("Help".equals(getTxt(jm))) {
                setTxt(" Help",m);
                mb.add(Box.createHorizontalGlue());
            } else {
                final Component cBefore=gcp(KEY_BEFORE_MENU_ITEM, m, Component.class);
                if (cBefore!=null) mb.add(cBefore);
            }
            if (gcp(KOPT_NOT_FOR_MENUBAR, m)==null) mb.add(m);
        }
        return mb;
    }
    public static JMenuBar arryToMenuBar(int options, Object oo[][], JMenuBar mb) {
        final JMenu mm[]=new JMenu[oo.length];
        for(int i=mm.length; --i>=0; ) mm[i]=jMenu(options,oo[i],"");
        return toMenuBar(options,mm,mb);
    }
    public static JComponent _jmCtrl;
    public static JComponent jMenuAccellerator() {
        if (_jmCtrl==null) _jmCtrl=new JMenu("");
        return _jmCtrl;
    }
    static void adToMenu(int options, JComponent menu, Object oo[], String parent0) {
        int startAt=0;
        final JMenu jMenu=deref(menu,JMenu.class);
        if (sze(oo)==0) return;
        final BA sbParent=new BA(99).a(parent0);
        if (jMenu!=null) {
            Icon icon=null;
            String label=null, tip=null;
            for(int k=0; k<oo.length; k++) {
                final Object o=oo[k];
                if (icon==null && o instanceof Icon) {
                    jMenu.setIcon(icon=((Icon) o));
                    startAt=k+1;
                }
                if (icon==null && o=="i" && k+1<oo.length) {
                    k++;
                    if (jMenu instanceof ChJMenu) ((ChJMenu)jMenu).i(oo[k]);
                    else jMenu.setIcon(icon=iicon(toStrg(oo[k])));
                    startAt=k+1;
                } else if (o instanceof CharSequence)  {
                    final String s=toStrg(o);
                    if (tip==null && chrAt(0,s)=='?') { jMenu.setToolTipText(tip=s.substring(1)); startAt=k+1;}
                    else if (label==null) {
                        final int acc=s.indexOf('^');
                        label=s;
                        if (acc>=0) {
                            final int mnem=chrAt(acc+1,s);
                            if (is(LETTR_DIGT,mnem)) {
                                jMenu.setMnemonic(uCase(mnem));
                                label=rmMnemon(s);
                            }
                        }
                        final char MENU_SEP='\u0000';
                        sbParent.a(MENU_SEP).a(label);
                        jMenu.setText(rmMnemon(label));
                        startAt=k+1;
                    }
                }
            }
        }
        int accelerator=0;
        String toolTip=null;
        Object icon=null;
        boolean waitIcon=false, waitHelp=false;
        long mnemUsed=0;
        final int shortCut=shortCutMask();
        for(int j=startAt; j<oo.length; j++) {
            final Object o=oo[j];
            final String stg=deref(o, String.class);
            final char c0=chrAt(0,stg);
            if (o instanceof ImageIcon || waitIcon) { icon=o; waitIcon=false;}
            else if (o=="h") waitHelp=true;
            else if (o=="i") waitIcon=true;
            else if (c0=='?') toolTip=addHtmlTagsAsStrg(stg.substring(1));
            else if ((c0=='&' || c0=='^') && stg.length()<3) accelerator=mnemon(stg);
            else if (!toContainr(0, o, menu)) {
                int mnem=0;
                final JComponent c;
                AbstractButton cMnemon=null;
                if (waitHelp) {
                    waitHelp=false;
                    Object ic=dIIcon(o);
                    if (ic==null) ic=TabItemTipIcon.g(TabItemTipIcon.ICON, o,null);
                    c=helpBut(o).i(ic!=null ? ic : "32x10").mi(dItem(o));
                } else if (o instanceof Object[]) {
                    c=cMnemon=jMenu(options, oo(o), toStrg(sbParent));
                } else c=derefJC(o);
                if (c!=null) {
                    if (toolTip!=null) c.setToolTipText(toolTip);
                    final ChButton cb=c instanceof ChButton ? (ChButton)c : null;
                    if (icon!=null && cb!=null) cb.i(icon);
                    final JMenuItem mi=deref(cb!=null ? cb.mi(null) : c, JMenuItem.class);
                    if (mi!=null) {
                        cMnemon=mi;
                        _vAllMenuitems.add(wref(mi));
                        pcp(JMenu.class,jMenu,c);
                        if (accelerator==0 && cb!=null) accelerator=cb.mnemonic();
                        if (accelerator!=0) {
                            int a=accelerator&255;
                            if (a==0) a=uCase(chrAt(0,getTxt(mi)));
                            final int ua=uCase(a);
                            if (is(LETTR_DIGT,ua)) {
                                if (((accelerator>>>8)&255)=='^') mnem=ua;
                                else {
                                    final int shft=is(LOWR,a)?SHIFT_MASK:0;
                                    mi.setAccelerator(KeyStroke.getKeyStroke(ua, shft|shortCut));
                                    final String KEY_included="CU$$included";
                                    final boolean debug=false;
                                    if ((CTRL_MASK!=shortCut || debug) && cb!=null && gcp(KEY_included,cb)==null) {
                                        pcp(KEY_included,"",cb);
                                        final JMenuItem mi2=(JMenuItem)cb.mi(debug ? null : "");
                                        mi2.setAccelerator(KeyStroke.getKeyStroke(ua, shft|CTRL_MASK));
                                        if (!debug) pcp(KOPT_NO_ICON,"",mi2);
                                        jMenuAccellerator().add(mi2);
                                    }
                                }
                            }
                        }
                    }
                    menu.add(mi!=null?mi:c);
                    accelerator=0;
                    icon=toolTip=null;
                }
                if (cMnemon!=null) {
                    if (mnem==0) mnem=cMnemon.getMnemonic();
                    if (mnem!=0) {
                        final long bit=1L<<(mnem-'0');
                        if (0!=(mnemUsed&bit)) {
                            mnem=0;
                        } else mnemUsed|=bit;
                        cMnemon.setMnemonic(mnem);
                    }
                }
            }
        }
    }
    public static String[] getMenuPaths(String txt, Object menuID) {
        Collection v=null;
        for(Object o : ancestrs(getEquivalentMenuButton(txt,menuID))) {
            String a=null;
            if (o instanceof JMenuBar) a="Menu-bar";
            if (o instanceof JMenu) {
                final String s=getTxt(o);
                if ("help".equalsIgnoreCase(s)) return NO_STRING;
                a=s;
            }
            if (o instanceof JPopupMenu) a=getTxt(o);
            v=adUniqNew(a,v);
        }
        return toArryClr(v,String.class);
    }
    /* Rescue Menubar */
    public static void setUpdateMenubarOnce(JMenuBar mb) {
        if (isSystProprty(IS_LINUX)) {
            final Object ref=wref(mb);
            for(Component c : childs(mb)) {
                addMoli(MOLI_RESCUE_MENUBAR,c);
                pcp(KEY_RESCUE_MENUB,ref,c);
            }
        }
    }
    public final static int MENUITMS_LOG=1, MENUITMS_SECURITY=3, MENUITMS_WEB=4;
    private final static List[] MENUITMS=new ArrayList[9];
    private final static JPopupMenu[] POPMENUS=new JPopupMenu[9];
    public static JPopupMenu popMenu(int t) {
        if (POPMENUS[t]==null) POPMENUS[t]=jPopupMenu(0,   (String)get(0, menuItms(t)), menuItms(t).toArray());
        return POPMENUS[t];
    }

    public static Runnable thread_toLogMenu(Object obj, String title, Object icon) {
        return thrdM("toLogMenu", ChUtils.class, new Object[]{obj,title, icon});
    }
    private final static Set _vLM=new HashSet();
    public static void toLogMenu(Object obj, String title, Object icon) {
        if (obj==null || !withGui()) return;
        if (!isEDT()) inEdtLater(thread_toLogMenu(obj,title, icon));
        else if (_vLM.add(obj)) {
            final ChButton b=obj instanceof BA || obj instanceof ChLogger ? ChButton.doView(obj).t(title).i(icon) : deref(obj,ChButton.class);
            final Component c=b!=null?b.mi(title):derefC(obj);
            if (obj instanceof ChLogger) pcp(KEY_TITLE, title, obj);
            popMenu(MENUITMS_LOG).add(c!=null?c: new JLabel(toStrg("Error toLogMenu "+shrtClasNam(obj)+" "+title)));
        }
    }
    public static List menuItms(int t) {
        if (MENUITMS[t]==null) {
            final Object
                oo[]=
                t==MENUITMS_LOG ? new Object[] {
                "Messages", "i", IC_EDIT,
                new ChButton("Log files").doViewFile(dirLog()).i(IC_DIRECTORY)
            } : t==MENUITMS_SECURITY ? new Object[] {
                "Security & Privacy", "i", IC_SECURITY,
                buttn(NEW_BUT_EXEC_TRUSTED),
                buttn(TOG_NASK_EXEC),
                buttn(TOG_NASK_UPLOAD),
                buttn(TOG_NASK_DOWNLOAD)
            } : t==MENUITMS_WEB ? new Object[] {
                "Internet","i",IC_WWW,
                buttn(BUT_TEST_PROXY),
                Customize.newButton(Customize.webBrowser),
                Customize.newButton(Customize.buttonWeb)
            } : null;
            adAll(oo, MENUITMS[t]=new ArrayList());
        }
        return MENUITMS[t];
    }

    public static void jMenu2html(int depth, Component c, BA sb) {
        final Object txt=rmHtmlHeadBody(txtForTitle(c));

        if (nxt(LETTR_DIGT,txt)>=0 || c instanceof JPopupMenu) {
            if (c instanceof JMenu || c instanceof JPopupMenu) {
                final BA sb2=new BA(88);
                if (depth>0) sb.aln("<li>");
                for(Component child: childs(c)) jMenu2html(depth+1, child, sb2);
                if (sb2.length()>0) {
                    if (c instanceof JMenu) sb.a("\n<div class=\"submenu\"><span class=\"menuname\">").a(depth>0?null:"<b>").a(txt).a(depth>0?null:"</b>").aln("</span>");
                    sb.aln("<ul>").aln(sb2).aln("</ul>");
                    if (c instanceof JMenu) sb.aln("</div>");
                }
                if (depth>0) sb.aln("</li>");
            } else if (c instanceof JMenuItem) {
                Object link=gcp(ChButton.KEY_CLASS,c);
                if (link !=null && !(link instanceof String || link instanceof Class)) link=link.getClass();
                final boolean hasDocu=getHlp(link)!=null;
                sb.a("<li>");
                if (hasDocu) sb.a("<a href=\"").a(URL_STRAP_DOC).a(link instanceof Class ? nam(link) : link).a(".html\">");
                if (c instanceof JCheckBoxMenuItem) sb.a("&lt;x&gt;");
                sb.a(txt);
                if (hasDocu) sb.a("</a>");
                sb.aln("</li>");
            }
        }
    }

    /* <<< JMenu <<< */
    /* ---------------------------------------- */
    /* >>> Button >>> */
    public final static ChButton[] BUTTN=new ChButton[60];
    public final static int TOG_WHITE_BG=1, TOG_ANTIALIASING=2,
        TOG_CACHE=3, TOG_MENUICONS=4,
        TOG_ALI_NOT_COMMUT=6,
        TOG_COLLAPSE=8,
        TOG_NASK_UPLOAD=9,
        TOG_NASK_DOWNLOAD=12,
        TOG_LOCK_JMENU=13,
        TOG_CTRL_WHEEL=14,
        TOG_NASK_EXEC=15,
        TOG_EDTFTP=16,

        TOG_JAVA_SRC=18,
        TOG_COMBINE_SEQ_FEATURES=19,
        TOG_SOUND=20,
        TOG_DND_TRANSFORM_HETERO=39,
        TOG_DEACT_PICR=40,
        TOG_DEACT_DAS_PDB2U=41,
        TOG_KEEP_TMP_FILES=42,
        BUT_TEST_PROXY=51,
        BUT_LAF=52,
        BUT_LOG=53,
        BUT_PREV_MSG=54,
        NEW_BUT_EXEC_TRUSTED=55;
    public static boolean isWhiteBG() { return isSelctd(BUTTN[TOG_WHITE_BG]);}
    public static ChButton buttn(final int i) {
        if (i==NEW_BUT_EXEC_TRUSTED) {
            return Customize.newButton(Customize.trustExe, Customize.refuseExe).t("Edit list of trusted and refused native programs");
        }

        ChButton but=BUTTN[i];
        if (but==null) {
            String s=null, t=null, b=null,tt=null, ic=null;
            boolean li=false, set=false;
            if (i==BUT_TEST_PROXY)   { tt=b="Network settings"; ic=IC_WWW_SETTINGS; }
            if (i==BUT_PREV_MSG)        b="Previous messages";
            if (i==BUT_LAF)             b="Look and Feel ...";
            if (i==BUT_LOG)           { b="Messages ..."; tt="Open messages menu";}
            if (i==TOG_CTRL_WHEEL)    { t="Reverse zoom with Ctrl+Mouse-Wheel."; s="ReverseZoom";}
            if (i==TOG_NASK_EXEC)     { t="Run native programs without asking"; s="AskExec"; set=true; }
            if (i==TOG_EDTFTP)        { t="Use edtFTPj for addresses starting with \"ftp://\""; s="-edtftp"; }
            if (i==TOG_LOCK_JMENU)    { t="Keep sub-menus open after click"; s="keepSubmenusOpen"; set=true; }
            if (i==TOG_NASK_DOWNLOAD) { t="Download files without asking"; set=true; }
            if (i==TOG_NASK_UPLOAD)   { t="Upload data without asking"; set=true; s="notAskUploadData"; }
            if (i==TOG_WHITE_BG)        t="White background (3D-view, dotplot)";
            if (i==TOG_ANTIALIASING)  { t="Enhanced graphics quality, reduced speed (Short-cut Ctrl-*)"; li=true; tt="Anti-aliasing"; }
            if (i==TOG_MENUICONS)     { t="Icons in menus"; set=true; s="iconsInMenus";  }

            if (i==TOG_JAVA_SRC)      { t="Provide links to source code"; li=true;  s="JavaSrc"; tt="Tool buttons appear which open the source code of the current part of the program."; }
            if (i==TOG_COMBINE_SEQ_FEATURES) { t="Suppress those features for which there is a more specific one"; set=true; }
            if (i==TOG_SOUND) { t="Sound on"; set=true; }
            if (i==TOG_DND_TRANSFORM_HETERO) {
                t="For export of hetero compounds use current 3D transformation";
                set=true;
                tt="If the protein is transformed by 3D-superposition<br>then write out the transformed coordinates<br>It is disabled if the protein is still in the original coordinate system.";
            }
            if (i==TOG_DEACT_PICR) {
                t="Deactivate EBI Picr service which returns the Uniprot ID for the uploaded sequence";
                tt="Picr is used to get the Uniprot ID for a sequence. The check-box will be deactivated if the service is unavailable";
            }
            if (i==TOG_DEACT_DAS_PDB2U) {
                t="Deactivate DAS service pdb_uniprot";
                tt="pdb_uniprot is used to get the Uniprot ID for a pdb ID. The check-box will be deactivated if the service is unavailable";
            }
            if (i==TOG_KEEP_TMP_FILES) { t="Keep temporary files"; tt="Normally, temporary files and log files are deleted when the Strap terminates"; }
            if (i==TOG_COLLAPSE)      { t="Collapse menubar to save space on screen"; s="collapse"; }
            if (i==TOG_ALI_NOT_COMMUT){ t="Sequence and 3D-alignments are not commutative"; tt="If not activated then computation time is saved since \nA with B gives the same sequence alignment and the inverse 3D matrix of B with A."; }
            if (i==TOG_CACHE)         { t="Use cached results";
                li=true;
                set=CacheResult.isEnabled();
                tt="The results of time-consuming calculations are stored on hard disk.";
            }
            if (t!=null) but=toggl(t).s(set);
            if (b!=null) but=newButtn(0,b).i(ic);
            if (s!=null) but.save(ChUtils.class,s);
            if (tt!=null) but.tt(tt);
            if (i==TOG_WHITE_BG) pcp(HtmlDoc.KEY_DO_NOT_CLONE,"",but);
            if (i==BUT_LOG) pcp(KEY_SMALL_TEXT,"Msg",but);
            if (li || b!=null) but.li(evLstnr(0));
            synchronized("SYNC$$BUTTN") {
                if (BUTTN[i]!=null) but=BUTTN[i];
                else BUTTN[i]=but;
            }
        }
        return but;
    }
    /* <<< Button <<< */
    /* ---------------------------------------- */
    /* >>> Undockable Menu Items >>> */
    public final static String KEY_UNDOCK_TARGET="CU$11";
    private final static List vUndockTargets=new ArrayList();
    private static ChJList[] _jlUndock;
    private static ChJList[] jlUndock01() { return _jlUndock==null ?  _jlUndock=new ChJList[2] : _jlUndock; }
    public static ChJList jlUndock(boolean create) {
        final ChJList jj[]=jlUndock01();
        if (jj[0]==null && create) {
            final ChJList jl=jj[0]=new ChJList(new Vector(), ChJTable.BACKSPACE_DEL_ITEM|ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT);
            final JFrame f=_undockW=new JFrame();
            f.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
            f.setUndecorated(true);
            setDragMovesWindow(false, jl);
            jl.setBorder(createRaisedBevelBorder());
            f.getContentPane().add(jl);
            evLstnr(0).addTo("kmM",jl);
            vUndockTargets.add(wref(jl));
            jl.setFixedCellHeight(ICON_HEIGHT);
        }
        return jj[0];
    }
    private static JFrame _undockW;
    public static void undockSetTarget(ChJList L) {
        adUniq(wref(jlUndock01()[1]=L), vUndockTargets);
    }
    public static ChJList undockNewTarget(ChJList L) {
        final ChJList target=new ChJList(new Vector(),ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT);
        pcp(KEY_UNDOCK_TARGET,wref(target),L);
        pcp(KEY_UNDOCK_TARGET,wref(target),target);
        pcp(KEY_CONTEXT_OBJECTS,wref(L),L);
        pcp(KEY_CONTEXT_OBJECTS,wref(L),target);
        addMoli(MOLI_UNDOCK_UPDATE,target);
        addMoli(MOLI_UNDOCK_UPDATE,L);
        addActLi(evLstnr(MOLI_UNDOCK_UPDATE), L);
        return target;
    }
    public static void undockEnableDisable(ChJList target0) {
        final ChJList target=target0!=null ? target0 : jlUndock(false);
        if (target!=null) {
            updateEnabled(target.getList());
            repaintC(target);
        }
    }
    private static void undockSave() {
        final ChJList jL=jlUndock(false);
        final List v=jL!=null ? jL.getList() : null;
        final int L=sze(v);
        final File f=file("~/@/tmp/undockedItems.txt");
        if (L>0) {
            BA sb=null;
            for(int i=0; i<L; i++) {
                final String s=getTxt(v.get(i));
                if (s!=null) (sb==null ? sb=new BA(999) : sb).aln(s);
            }
            if (sb!=null) wrte(f,sb);
        } else delFile(f);
    }
    private static String _undockItms[];
    public static int undockSaved(Object o) {
        String[] ss=_undockItms;
        if (ss==null) {
            final File f=file("~/@/tmp/undockedItems.txt");
            ss=readLines(f);
            if (ss==null) ss=NO_STRING;
            _undockItms=ss;
        }
        if (o==null || ss.length==0) return ss.length;
        final JComponent jc=derefJC(o);
        final Object childs[]=o instanceof JMenu || o instanceof JPopupMenu ? childs(jc) : o instanceof Object[] ?(Object[])o : null;
        if (childs!=null) {
            for(Object o2 : childs) undockSaved(o2);
        } else if (jc!=null) {
            final String s=getTxt(o);
            final int i=s==null ? -1 : idxOfStrg(s,ss);
            if (i>=0) {
                ss[i]=null;
                _undockItms=ss=rmNullS(ss);
                undockItm(false, jc, (AWTEvent)null, null);
            }
        }
        return ss.length;
    }
    public static void undockAdd(Object button) { undockItm(true, button, null,null); }
    public static void undockRemove(Object button) {
        if (remov(button, jlUndock(true).getList())) revalAndRepaintCs(jlUndock(true));
    }

    private static void undockItm(boolean direct, Object buttons, AWTEvent ev, ChJList jList) {
        if (buttons==null) return;
        final ChJList jl=jList!=null?jList:jlUndock(true);
        final List v=jl.getList();
        final Point pm=mouseLctn();
        for(Object button0 : oo(buttons)) {
            final Container button=deref(button0, Container.class);
            if (button instanceof JMenu || button==null) continue;
            final String MI="CU$$KEY_MI";
            final Component
                bFather=gcp(KEY_CLONED_FROM, button,AbstractButton.class),
                bAdd,
                bAdd0=bFather!=null ? bFather : button,
                prevBut=gcp(MI,bAdd0,AbstractButton.class);

            if (isChLabl(bAdd0)) continue;
            if (prevBut!=null) bAdd=prevBut;
            else if (direct) bAdd=button;
            else if (bAdd0 instanceof ChButton)  {
                final ChButton b=(ChButton)bAdd0;
                bAdd=b.isToggle() ? b.cb() : b.mi(getTxt(button));
            }
            else if (bAdd0 instanceof JCheckBoxMenuItem) bAdd=new ChJCheckBox((JCheckBoxMenuItem)bAdd0);
            else if (bAdd0 instanceof AbstractButton)    bAdd=new ChButton().cp(ChButton.KEY_DO_CLICK,bAdd0).mi(null);
            else bAdd=bAdd0;
            if (bAdd!=bAdd0) {
                pcp(MI,bAdd, bAdd0);
                final AbstractButton b=deref(bAdd, AbstractButton.class);
                if (b!=null) {
                    if (b instanceof JMenuItem) ((JMenuItem)b).setAccelerator(null);
                    b.setOpaque(true);
                    pcpAddOpt(KEY_CLOSE_OPT,CLOSE_ALLOWED,b);
                    if (b instanceof JCheckBox) pcp(ChRenderer.KOPT_NO_CHANGE_FG_AND_BG_IF_SELECTED,"",b);
                    else {
                        b.setIconTextGap(-3);
                        if (!direct) b.setIcon(iicon("1x1"));
                    }
                }
            }
            final Point pButton=!button.isShowing()?null: button.getLocationOnScreen();
            v.remove(bAdd);
            if (pm==null||pButton==null||pm.y<pButton.y) v.add(bAdd); else v.add(0,bAdd);
        }
        if (jl==jlUndock(false)) {
            final JFrame f=_undockW;
            if (f!=null) {
                if (pm!=null&&ev!=null) f.setLocation( pm.x, pm.y);
                undockWinSize(true, f, jl);
                setAOT(true,f);
                setWndwState('T',f);
                f.show();
            }
        } else revalAndRepaintC(jl);
    }
    private static void undockWinSize(boolean fullSize, Window f, ChJList jl) {
        final List v=jl.getList();
        final int N=sze(v);
        if (N==0) f.setVisible(false);
        else {
            int w=7*EM;
            for(int i=N; fullSize && --i>=0;) w=maxi(w, prefW(get(i,v)));
            f.setSize(w+EM, N*ICON_HEIGHT);
            revalAndRepaintC(jl);
        }
    }
    public static AbstractButton getUndockedButton(AbstractButton button) {
        if (button==null) return null;
        for(int iL=sze(vUndockTargets); --iL>=0;) {
            final ChJList jl=get(iL,vUndockTargets,ChJList.class);
            final List v=jl!=null ? jl.getList() : null;
            for(int i=sze(v); --i>=0;) {
                final AbstractButton b=get(i,v, AbstractButton.class);
                if (b!=null && b.getModel()==button.getModel() || gcp(KEY_CLONED_FROM, b)==button) return b;
            }
        }
        return null;
    }
    /* <<< Undockable Menu Items <<< */
    /* ---------------------------------------- */
    /* >>> Detach  panels >>> */
    private static int _undockEnabled;
    private static JComponent _pnlUndockMsg;
    final static Object KEY_detach_frame=new Object(), KEY_detach_parent=new Object();
    public final static Object KEY_detach_component=new Object(), KEY_UNDOCK_MESSAGE=new Object();
    public static int undockEnabld() { return _undockEnabled; }
    public static Runnable thread_pnlUndock(Object component, int xScreen, int yScreen, boolean ask) {
        return thrdM("pnlUndock",ChUtils.class, new Object[]{component, intObjct(xScreen),intObjct(yScreen), boolObjct(ask)});
    }
    public static void pnlUndock(Object component, int xScreen, int yScreen, boolean ask) {
        final Component panel=getPnl(component);
        if (panel!=null && gcp(KEY_detach_component,parentWndw(panel))==null) {
            final Object
                tp=gcp(ChTabPane.KEY_PARENT_CTP,panel),
                parent=tp!=null ? tp: panel.getParent();
            pcp(KEY_detach_parent, wref(parent), panel);
            if (parent==null)  { error("ButOwnWindow: p==null"); return;}
            if (ask && _undockEnabled==0) {
                if (_pnlUndockMsg==null) {
                    final Object
                        msg=pnl("Un-docked panels appear in a separate frame and can be moved around and even moved on another monitor.<br>"+
                            "They snap back to the original container by clicking the close button."),
                        pNorth=pnlTogglOpts("*Explain",msg),
                        tog=toggl("E_UNDOCK").li(evLstnr(0)).t("Enable undocking of panels").cb();
                    _pnlUndockMsg=pnl(CNSEW,pnl(VBHB,msg,"-", hintAOT(true)),pNorth, tog);
                }

                addActLi(evLstnr(0), ChFrame.frame("Undocking?",_pnlUndockMsg,ChFrame.PACK|ChFrame.ALWAYS_ON_TOP).shw());
            }
            if (ask && _undockEnabled!=1) {
                pcp("R", thread_pnlUndock(component, xScreen,yScreen,ask), _pnlUndockMsg);
                return;
            }
            rmFromParent(panel);
            final String title=titleForObject(component);
            final ChFrame f=new ChFrame(title==null?"":sze(title)<20?title+hintAOT(false) : title).size(EM*80,EX*30);
            pcp(KEY_detach_component,wref(component),f);
            pcp(KEY_detach_frame,f, component);
            addActLi(evLstnr(0), f);
            final ChRunnable run=gcp(KEY_GET_MENUBAR,component,ChRunnable.class);
            final JMenuBar mb=(JMenuBar) (run!=null ? run.run(KEY_GET_MENUBAR,component) : gcp(KEY_GET_MENUBAR,component));
            if (mb!=null) {
                setHeavy(mb);
                f.setJMenuBar(mb);
            }
            f.ad(panel).i(dIIcon(component)).size(wdth(panel)+EM, hght(panel)+EX).shw(ChFrame.TO_FRONT);
            if (xScreen>=0 && yScreen>=0) f.setLocation(xScreen,yScreen);
        }
    }
    public static String titleForObject(Object o) {
        ChRunnable run=gcp(ChRunnable.RUN_GET_COLUMN_TITLE,o, ChRunnable.class);
        if (run==null) run=deref(o , ChRunnable.class);
        String title=run==null?null: toStrg(run.run(ChRunnable.RUN_GET_COLUMN_TITLE,o));
        if (title==null) title=dItem(o);
        if (title==null) title=nam(o);
        return title;
    }
    public static void pnlDock(Object component) {
        if (component==null) return;
        final ChFrame f=gcpClear(KEY_detach_frame, component, ChFrame.class);
        f.superDispose();
        final Object parent=gcp(KEY_detach_parent, component);
        final Component panel=getPnl(component);
        if (panel!=null) {
            if (parent instanceof JTabbedPane) {
                final Object title=gcp(KEY_tabLabel,component);
                adTab(0,orS(deref(title,String.class), getTxt(title)), component, (JTabbedPane)parent);
            }
            else if (parent instanceof ChTabPane) ((ChTabPane)parent).addTab(0, (JComponent)panel);
            else if (parent instanceof ChTableLayout) ((ChTableLayout)parent).addCol(0, panel);
        }
    }
    /* <<< Detach <<< */
    /* ---------------------------------------- */
    /* >>> HTML >>> */
    public static long hexToInt(Object text) { return hexToInt(text,0,MAX_INT); }
    public static long hexToInt(Object text, int from, int max) {
        final int L=mini(max,sze(text));
        if (L<=0) return -1;
        assrtIsTxt(text);
        final boolean hash=chrAt(from,text)=='#';
        final int firstDigit=from+(hash?1:0);
        long ret=0;
        for(int pos=firstDigit, digit;  pos<L && (digit=HEX2INT[255&chrAt(pos,text)])>=0; pos++) {
            ret=(ret<<4)|digit;
        }
        return ret;
    }
    public static String shortcutAsHtml(String keySequence) { return "<font size=\"-2\" color=\"8866FF\">"+keySequence+"</font>"; }

    public static String rgbHex(long rgb) { return new BA(6).aHex(rgb, 6).toString(); }
    public static String rgbHex(Color color, boolean alpha) { return color==null?null : new BA(8).a(color,0,alpha?8:6).toString(); }

    private static String[] _rmHtmlHeadBody;
    public static CharSequence rmHtmlHeadBody(CharSequence s) {
        if (_rmHtmlHeadBody==null) _rmHtmlHeadBody=splitTokns("<html> <head> <body> </html> </head> </body>");
        CharSequence sb=s;
        if (sb!=null) for(String tag : _rmHtmlHeadBody) sb=strplc(STRPLC_FILL_RIGHT, tag, "",sb);
        return rmAllChars((char)0, sb, 0, MAX_INT);
    }
    public static String xmlAttribute(String id, byte T[], int B,int E) {
        for(int pos=B; (pos=strstr(STRSTR_AFTER,id,T,pos,E))>0; ) {
            if (!is(LETTR_DIGT_US,T,pos-id.length()-1) && get(pos,T)=='=' && get(pos+1,T)=='"') {
                final int endQuote=strchr('"', T,pos+2,E);
                if (endQuote>0) return bytes2strg(T, pos+2,  endQuote);
            }
        }
        return null;
    }
    public static String toStrg(byte[] T, int from, int to, String[] stringList, byte[][] stringListBB) {
        if (T==null || from<0 || to>T.length || from>=to) return "";
        if (stringList!=null && stringListBB!=null) {
            final byte c0=T[from];
            for(int i=stringList.length; --i>=0;) {
                final byte[] sl=stringListBB[i];
                if (sl.length==0 || c0!=sl[0]) continue;
                if (strEquls(sl,T,from)) return stringList[i];
            }
        }
        return bytes2strg(T, from,to);
    }
    public static String xmlAttribute(String id, byte T[], int B,int E,String[] stringList, byte[][] stringListBB) {
        for(int pos=B; (pos=strstr(STRSTR_AFTER,id,T,pos,E))>0; ) {
            if (!is(LETTR_DIGT_US,T,pos-id.length()-1) && get(pos,T)=='=' && get(pos+1,T)=='"') {
                final int endQuote=strchr('"', T,pos+2,E);
                if (endQuote>0) {
                    return toStrg(T,pos+2,  endQuote, stringList, stringListBB);
                }
            }
        }
        return null;
    }
    public static String addHtmlTagsAsStrg(Object cs) {
        if (cs instanceof String) {
            final String s=(String)cs;
            if ( s.indexOf('<')<0 || s.indexOf('>')<0 || s.startsWith("<html>")) return s;
        }
        return cs==null ? null : toStrg(addHtmlTags(toBA(cs)));
    }
    public static CharSequence addHtmlTags(CharSequence cs) { return _addHtm(cs,false);}
    public static boolean containsHtmlTags(CharSequence cs) { return ""==_addHtm(cs,true);}
    private static CharSequence _addHtm(CharSequence cs, boolean test) {
        if (cs==null) return null;
        final int L=sze(cs);
        boolean hasTag=false;
        int sHead=-1,sBody=-1, sHtml=-1;
        for(int i=L; --i>=0;) {
            if (cs.charAt(i)!='<') continue;
            int to=-1;
            for(int j=i; j<i+10 && j<L; j++) if (cs.charAt(j)=='>') { to=j+1; break;}
            if (to>0) {
                for(String tag :  SOME_HTML_TAGS) {
                    hasTag=hasTag|| tag.length()==to-i && strEquls(STRSTR_IC, tag,cs,i);
                }
                if (to-i==6) {
                    if (strEquls(STRSTR_IC, "<body>",cs,i)) sBody=i;
                    if (strEquls(STRSTR_IC, "<head>",cs,i)) sHead=i;
                    if (strEquls(STRSTR_IC, "<html>",cs,i)) sHtml=i;
                }
            }
        }
        if (test) return hasTag?"":null;
        if (hasTag) {
            ckHtml(cs);
            final String insert=
                sHead<0 && sHtml<0 ? "<html>\n<body>\n<head>\n</head>\n" :
                sHead>0 && sHtml<0 ? "<html>\n<body>\n" :
                null;
            if (insert!=null) {
                cs=toBA(cs).insert(0, insert);
                if (sHtml<0) cs=toBA(cs).aln("\n</body>\n</html>");
            } else {
                if (sBody>0 && sHead<0) cs=toBA(cs).insert(sBody, "\n<head>\n</head>\n" );
            }
        }
        return cs;
    }
    public static void setCheckHtml(boolean onOff) {
        if (onOff) { if (_debugHtml==null) _debugHtml=new HashSet(); }
        else _debugHtml=null;
    }

    public static String ckHtml(Object txt) {
        final String s=toStrg(txt);
        if (_debugHtml!=null) _debugHtml.add(s);
        return s;
    }
    private static Set<CharSequence> _debugHtml;
    public static void htmlTidy(Object fOrTxt) {
        if (_debugHtml==null) return;
        final BA ba=
            fOrTxt instanceof File ? readBytes((File)fOrTxt) :
            fOrTxt instanceof BA ? new BA(((BA)fOrTxt).newBytes(0,((BA)fOrTxt).end()+3333), 0, ((BA)fOrTxt).end())  :
            fOrTxt instanceof CharSequence ? new BA((CharSequence)fOrTxt) :
            null;
        if (ba==null) return;
        ba.replace(' '|STRPLC_FILL_RIGHT,"<body","<DIV class=\"body\" ")
            .replace(' '|STRPLC_FILL_RIGHT,"</body>","</DIV>")
            .replace(' '|STRPLC_FILL_RIGHT,"<html","<DIV class=\"html\" ")
            .replace(' '|STRPLC_FILL_RIGHT,"</html>","</DIV>")
            .replace(' '|STRPLC_FILL_RIGHT,"<head","<DIV class=\"head\" ")
            .replace(' '|STRPLC_FILL_RIGHT,"</head>","</DIV>")
            .replace(' '|STRPLC_FILL_RIGHT,"<!DOCTYPE","<br  ")
            .replace(' '|STRPLC_FILL_RIGHT,"<title>","")
            .replace(' '|STRPLC_FILL_RIGHT,"</title>","")
            .insert(0,"<!DOCTYPE HTML>\n<html><head><title>T</title></head><body>\n")
            .a("\n\n</body></html>");
        final File f=file("~/@/log/ckHtml.html");
        wrte(f,ba);
        putln("tidy ",f," > /dev/null");
    }
    public static String[] hrefsInHtml(BA ba) {
        if (ba==null) return NO_STRING;
        final byte[] T=ba.bytes();
        final int E=ba.end();
        Collection v=null;
        for(int i=ba.begin(); (i=strstr(STRSTR_AFTER,"href=\"",T,i,E))>0;) {
            final int e=strchr('\"',T,i,E);
            if (e>0) v=adUniqNew(ba.newString(i,e),  v);
        }
        return strgArry(v);
    }
    /* <<< HTML <<< */
    /* ---------------------------------------- */
    /* >>> Font >>> */
    public static String cssH1H2H3(double scale) {
        return
            "body {font: normal "+(int)(100*scale)+"% Helvetica, Arial, sans-serif;}"+
            "li   {font: normal "+(int)(100*scale)+"% Helvetica, Arial, sans-serif;}"+
            "h3   {font: normal "+(int)(120*scale)+"% Helvetica, Arial, sans-serif;}"+
            "h2   {font: normal "+(int)(140*scale)+"% Helvetica, Arial, sans-serif;}"+
            "h1   {font: normal "+(int)(160*scale)+"% Helvetica, Arial, sans-serif;}"+"\n";
    }
    public static JComponent monospc(Object o) {
        if (o instanceof JComponent) {
            final JComponent c=(JComponent)o;
            final Font f=c.getFont();
            if (f==null) return c;
            c.setFont(getFnt(f.getSize(),true,f.getStyle()));
            return c;
        } else if (o!=null) {
            final BA sb=new BA(99);
            final File f=deref(o,File.class);
            if (f!=null) sb.a(' ').aRplc(0L, " ","%20",(isWin() ? f.getAbsolutePath() : fPathUnix(f))).a(' ');
            else sb.a(o);
            if (looks(LIKE_EXTURL,sb)) sb.insert(0," ").a(' ');
            final JComponent tf=new ChTextView("").a(sb).bg(null);
            setMaxSze(-1,-1, tf);
            return tf;
        }
        return null;
    }
    final static Object[][] FONTS=new Reference[8][];
    public static Font getFnt(int size0, boolean monospace, int style) {
        final int size=maxi(size0,1);
        final int type=
            ( (style&Font.BOLD)!=0 ? 1:0) |
            ( (style&Font.ITALIC)!=0 ? 2:0) |
            ( monospace ? 4:0);
        if (sze(FONTS[type])<=size) FONTS[type]=chSze(FONTS[type],size+10,Reference.class);
        Font f=(Font)deref(FONTS[type][size]);
        if (f==null) FONTS[type][size]=newSoftRef(f=new Font(monospace ? "Monospaced" :"", style, size));
        return f;
    }
    //  public static JComponent enlargeFont(CharSequence c,double k) { return enlargeFont(pnl(c),k);}
    private final static Map<String,Font> mapEnlargeFont=new HashMap();
    public static Component enlargeFont(Component c,final double k) {
        if (c==null) return null;
        final Font f=c.getFont();
        if (f!=null) {
            final int style=f.getStyle();
            Object oSize=gcp(KEY_ORIG_FONT_SIZE,c,Integer.class);
            if (oSize==null) pcp(KEY_ORIG_FONT_SIZE,oSize=intObjct(f.getSize()),c);
            final int size=maxi(1,(int)(k*atoi(oSize)));
            final String key=f.getName()+" "+style+" "+size;
            Font newFont=mapEnlargeFont.get(key);
            if (newFont==null) mapEnlargeFont.put(key,newFont=new Font(f.getName(),style,size));
            c.setFont(newFont);
            if (c instanceof ChJTextPane) {
                final javax.swing.text.Document doc=((ChJTextPane)c).getDocument();
                if (doc instanceof javax.swing.text.html.HTMLDocument) ((javax.swing.text.html.HTMLDocument)doc).getStyleSheet().addRule(cssH1H2H3(k));
            }
        }
        for(Component co: childs(c)) enlargeFont(derefJC(co),k);
        return c;
    }
    public static int strgWidth(Object o, CharSequence s) {
        if (o==null || sze(s)==0) return 0;
        final FontMetrics fm=fntMet(o);
        return fm==null?0:  fm.stringWidth(toStrg(s));
    }
    public static int charA(Object o) {
        final FontMetrics fm=fntMet(o);
        return fm==null?0: fm.getAscent();
    }
    public static int charW(Object o) {
        final FontMetrics fm=fntMet(o);
        return fm==null?0: fm.charWidth('H');
    }
    public static int charH(Object o) {
        final FontMetrics fm=fntMet(o);
        return fm==null?0: fm.getAscent()+fm.getDescent();
    }
    public static FontMetrics fntMet(Object o) {
        if (o instanceof FontMetrics) return (FontMetrics)o;
        final Font f= o instanceof Font ? (Font)o : o instanceof Graphics ? ((Graphics)o).getFont() : o instanceof Component ? ((Component)o).getFont() : null;
        if (f==null) assrt();
        return f==null || dtkt()==null ? null : dtkt().getFontMetrics(f);
    }

    private static Toolkit _dTK;
    private static int _isGEA;
    public static Toolkit dtkt() {
        if (_isGEA==0) {
            try {
                _isGEA=GraphicsEnvironment.getLocalGraphicsEnvironment().isHeadless() ? CFALSE : CTRUE;
            } catch(Error ex) {_isGEA=CFALSE;}
        }
        if (_dTK==null && _isGEA==CTRUE) _dTK=Toolkit.getDefaultToolkit();
        return _dTK;
    }
    public static Rectangle chrBnds(Font f) { return ChFontMetrics.getCharBounds((char)0,f);}
    public static String[] allFnts() {
        final List<String> v=new ArrayList();
        final String SKIP[]="Webdings;Wingdings;Symbol;MS Outlook;StarMath;StarBats;Standard Symbol".split(";");
        nextFont:
        for(Font f:GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts()){
            final String n=f.getName();
            for(String not:SKIP) if (n.startsWith(not)) continue nextFont;
            v.add(n);
        }
        return strgArry(v);
    }
    /* <<< Font <<< */
    /* ---------------------------------------- */
    /* >>> Dimension  >>> */
    private final static Object KEY_ps=new Object();
    private static int prefWH(boolean width, Object comp) {
        int w=0;
        if (comp instanceof Object[]) {
            for(Object o : (Object[]) comp) w=maxi(w, prefWH(width, o));
        }
        Dimension dim=null;
        try { dim=comp instanceof Component ? ((Component)comp).getPreferredSize() : null; } catch(Exception ex){}
        if (dim!=null) w=width?dim.width:dim.height;
        return w;
    }
    public static int prefW(Object comp) { return prefWH(true,  comp); }
    public static int prefH(Object comp) { return prefWH(false, comp); }
    public static void setPrefSze(PreferredSize ps, Object o) { pcp(KEY_ps, ps, o); }

    public static boolean prefSze0(Object c) {
        return
            !Insecure.EXEC_ALLOWED && gcp(KEY_HIDE_IF_EXEC_FORBIDDEN,c)!=null ||
            notSelctd(gcp(KEY_TOGGLE_COLLAPSE[0],c)) || isSelctd(gcp(KEY_TOGGLE_COLLAPSE[1],c)) ||
            null!=gcp(KOPT_HIDE_IF_DISABLED,c) && !isEnabld(c);
    }
    public static Dimension prefSze(Object c) {
        if (c==null) return null;
        Dimension d=prefSze0(c) ? dim(0,0) : gcp(KEY_PREF_SIZE,c,Dimension.class);
        if (d==null) {
            final PreferredSize ps=(PreferredSize)gcp(KEY_ps, c);
            d= ps!=null ? ps.preferredSize(c) : null;
        }
        return d;
    }

    public static void setPrefSze(boolean reval, int w0, int h0, Component c) {
        if (c==null) return;
        if (!isEDT()) inEdtLater(thrdCR(instance(0), "setPrefSze",new Object[]{boolObjct(reval), new int[]{w0,h0},c}));
        else {
            final Dimension d=c.getPreferredSize();
            final int w=w0==MIN_INT ? prefW(c) :w0;
            final int h=h0==MIN_INT ? prefH(c) :h0;
            if (d==null || d.width!=w || d.height!=h) {
                c.setPreferredSize(dim(w,h));
                adaptSmallSizeParent(c);
                if (reval) revalAndRepaintC(c);
            }
        }
    }
    /* <<< Dimension <<< */
    /* ---------------------------------------- */
    /* >>> Progress >>> */
    public final static int PRGRSS_TXT=1, PRGRSS_SRC=2;
    public static ChPanel prgrssBar() {
        final Font f=getFnt(9,false,0);
        final ChPanel pan=(ChPanel)pnl(dim(1,charH(f)+4), KOPT_HIDE_IF_DISABLED);
        pan.setFont(f);
        return pan;
    }
    public static Object[] prgrss(Object src, int v, CharSequence msg) { return new Object[] { intObjct(v),msg,src};  }
    public static void prgrss2bar(ChPanel bar, Object prgReport[]) {
        if (bar!=null) {
            final String txt=toStrg(prgReport[1]);
            final int p=atoi(prgReport[0]);
            bar.setProgress(p,txt).repaint();
        }
    }

    private static ChPanel _prgrssShutDwn;
    public static JComponent prgrssShutDwn() {
        if (_prgrssShutDwn==null)  _prgrssShutDwn=prgrssBar();
        return _prgrssShutDwn;
    }
    public static void tellShutDown(String msg) {
        final String s="Shut down - Please wait: "+msg;
        if (_prgrssShutDwn!=null) _prgrssShutDwn.setProgress(0,s).paintImmediately(0,0,999,99);
        //putln(s);
    }
    /* <<< Progress <<< */
    /* ---------------------------------------- */
    /* >>> AWT >>> */
    private static Rectangle _clip;
    private static boolean _gui=true;
    public static boolean withGui() { return _gui && dtkt()!=null;}
    public static void setNoGui() { _gui=false; }
    public static Rectangle clipBnds(Graphics g) { return clipBnds(g, MAX_INT/2, MAX_INT/2); }
    public static Rectangle clipBnds(Graphics g, int w, int h) {
        if (_clip==null) _clip=new Rectangle();
        else setRect(0,0,w,h,_clip);
        if (g!=null) g.getClipBounds(_clip);
        return _clip;
    }

    public static void setMaxSze(int w, int h, Object c) {
        final JComponent jc=derefJC(c);
        if (jc!=null) jc.setMaximumSize(w<0 || h<0 ? jc.getPreferredSize() : dim(w,h));
    }
    public static void setMinSze(int w, int h, Component c) {
        final JComponent jc=derefJC(c);
        if (jc!=null) {
            jc.setMinimumSize(w<0 || h<0 ? jc.getPreferredSize() : dim(w,h));
        }
    }

    public static Point mouseLctn() {
        final Point p=(Point) forJavaVersion(15).run(RunIf15orHigher.RUN_GET_MOUSE_LOCATION, null);
        return p!=null?p:!isMac()?null: deref(invokeMthd("getMouseLocationOnScreen","com.apple.eawt.Application"),Point.class);
    }
    public static void revalidateC(Object component) {
        if (dtkt()==null) return;
        final Object o=deref(component);
        for(int i=szeVA(o); --i>=0; ) repaintC(get(i,o));
        if (o instanceof JComponent) ((JComponent)o).revalidate();
    }
    public static void repaintC(Object component) {
        if (dtkt()==null) return;
        final Object o=deref(component);
        for(int i=szeVA(o); --i>=0; ) repaintC(get(i,o));
        if (o instanceof Component) ((Component)o).repaint();
    }
    public static Runnable thread_repaint(Component c, Rectangle r) {
        return c==null?null : r==null ? thrdM("repaint",c) : thrdM("repaint", c, new Object[]{intObjct(r.x), intObjct(r.y), intObjct(r.width), intObjct(r.height)});
    }
    public static void revalAndRepaintC(Object c) {
        revalidateC(c);
        repaintC(c);
    }
    public static void setVisblC(boolean b, Object object, int afterMS) {
        final Component c=derefC(object);
        if (dtkt()!=null && c!=null) {
            if (afterMS==0) {
                c.setVisible(b);
                if (b && c instanceof Frame) ((Frame)c).setExtendedState(0);
            } else ChDelay.setVisible(c,b,afterMS);
        }
    }
    public static Runnable thread_revalAndRepaintCs(Object o) {
        return thrdM("revalAndRepaintCs",ChUtils.class, new Object[]{o});
    }
    public static void revalAndRepaintCs(Object o) {
        if (o==null) return;
        final Object key="CU$$dimRR", keyP="CU$$dimRRp";
        final Component c=derefC(o), parent=parentC(c);
        if (parent==null || !c.isVisible() || dtkt()==null) return;
        final Dimension ps=c.getPreferredSize(), d=parent.getSize();
        if (!c.isValid() || !eqNz(ps, gcp(key,c)) || !eqNz(d, gcp(keyP,c))) {
            pcp(keyP,d,c);
            pcp(key,ps,c);
            revalidateC(c);
        }
        repaintC(c);
    }
    public static Cursor cursr(int d) {
        final int t=
            d=='W' ? Cursor.WAIT_CURSOR:
            d=='M' ? Cursor.MOVE_CURSOR:
            d=='T' ? Cursor.TEXT_CURSOR:
            d=='H' ? Cursor.HAND_CURSOR:
            d=='n' ? Cursor.N_RESIZE_CURSOR:
            d=='s' ? Cursor.S_RESIZE_CURSOR:
            d=='e' ? Cursor.E_RESIZE_CURSOR:
            d=='w' ? Cursor.W_RESIZE_CURSOR:
            d=='n'+'w' ? Cursor.NW_RESIZE_CURSOR:
            d=='n'+'e' ? Cursor.NE_RESIZE_CURSOR:
            d=='s'+'w' ? Cursor.SW_RESIZE_CURSOR:
            d=='s'+'e' ? Cursor.SE_RESIZE_CURSOR :
            Cursor.DEFAULT_CURSOR;
        return Cursor.getPredefinedCursor(t);
    }
    public static JComponent emptyBordr(int l, int r, int o, int u, JComponent c) {
        if (c!=null) c.setBorder(createEmptyBorder(o,l,u,r));
        return c;
    }
    public static boolean isContained(Component parent,Object comp) {
        if (parent==comp || parent.getClass()==comp) return true;
        for(Component c: childs(parent)) if (isContained(c,comp)) return true;
        return false;
    }
    /* ---------------------------------------- */
    public static void pushDivider(Component c,double pos) {
        if (c==null) return;
        final JSplitPane sp=parentC(false,c,JSplitPane.class);
        if (sp!=null) {
            final int
                max=sp.getMaximumDividerLocation(),
                div=sp.getDividerLocation();
            if (sp.getBottomComponent()==c && div> (int)(max*(1-pos))) { sp.setDividerLocation(1-pos); sp.revalidate(); }
            if (sp.getTopComponent()==c && div< (int)(max*pos)) { sp.setDividerLocation(pos); sp.revalidate();}
        }
    }
    public static void toClipbd(CharSequence txt) {
        final String s=toStrg(txt);
        if (s!=null && dtkt()!=null) {
            final java.awt.datatransfer.StringSelection sel=new java.awt.datatransfer.StringSelection(s);
            try{ dtkt().getSystemClipboard().setContents(sel,null);}catch(Throwable ex){}
            try{ dtkt().getSystemSelection().setContents(sel,null);}catch(Throwable ex){}
        }
    }
    public static void setSelctd(boolean b, Object o) {
        if (o instanceof AbstractButton) {
            if (!isEDT()) inEDT(thrdM("setSelctd",ChUtils.class,new Object[]{boolObjct(b),o}));
            else  ((AbstractButton)o).setSelected(b);
        }
    }
    public static boolean isSelctd(Object o) { return o instanceof AbstractButton ? ((AbstractButton)o).isSelected() : false; }
    public static boolean notSelctd(Object o) { return o instanceof AbstractButton ? !((AbstractButton)o).isSelected() : false; }

    private static Object _hasFocus;
    public static Component focusedC(int lostNotLongerThanMs) {
        final Component c=derefC(_hasFocus);
        return c==null || c.hasFocus() || currentTimeMillis()-atol(gcp(KEY_FOCUS_LOST_WHEN,c))<lostNotLongerThanMs ?  c : null;
    }

    public final static String KEY_SMALL_TEXT="CC$$KST", KEY_ADAPTED_SIZES="CC$$KSZ",  KOPT_ADAPT_SMALL_SIZE="OPT$$KUS";
    private final static Object KEY_LONG_TEXT=new Object(),  KOPT_ADAPT_SMALL_SIZE_NEEDED=new Object();

    static void adaptSmallSizeParent(Object c) {
        Object par=c;
        for(int i=5;--i>=0 && par!=null;) {
            par=parentC(c);
            if (null!=gcp(KOPT_ADAPT_SMALL_SIZE, par)) pcp(KOPT_ADAPT_SMALL_SIZE_NEEDED,"",par);
        }
    }

    public static void adaptSmallSize(Container c) {
        final LayoutManager lm=c==null?null:c.getLayout();
        if (lm==null || !isVisbl(c)) return;
        final Component cc[]=c.getComponents();
        final Dimension dim=c.getSize();
        for(Component b : cc) {
            final String tLong=gcps(KEY_LONG_TEXT,b);
            if (gcps(KEY_SMALL_TEXT,b)!=null) {
                if (tLong==null) pcp(KEY_LONG_TEXT, getTxt(b),b);
                else setTxt(tLong,b);
            }
            final Dimension[] dd=(Dimension[])gcp(KEY_ADAPTED_SIZES,b);
            if (dd!=null) ((JComponent)b).setPreferredSize(dd[0]);
        }
        for(int i=cc.length; --i>=0;) {
            final Component b=cc[i];
            final String tShort=gcps(KEY_SMALL_TEXT,b);
            final Dimension[] dd=(Dimension[])gcp(KEY_ADAPTED_SIZES, b);
            if (tShort!=null || dd!=null) {
                if (wdth(c)>=wdth(lm.preferredLayoutSize(c))) break;
                if (tShort!=null) setTxt(tShort,b);
                if (dd!=null) ((JComponent)b).setPreferredSize(dd[1]);
            }
        }
    }
    /* <<< AWT <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public static EvAdapter evAdapt(ProcessEv pe) {
        if (pe==null) return null;
        EvAdapter ea=(EvAdapter)gcp(EvAdapter.KEY, pe);
        if (ea==null)  ea=new EvAdapter(pe);
        return ea;
    }

    /* <<< AWT <<< */
    /* ---------------------------------------- */
    /* >>> update >>> */
    public final static int
        CHANGED_CHILDS_OF_PROTEIN=1,
        CHANGED_PROTEIN_LABEL=2,
        CHANGED_PROTEIN_ORDER=3,
        CHANGED_PROTEIN_AT_CURSOR=4,
        CHANGED_COMBOBOX=5,
        CHANGED_SELECTED_OBJECTS=6,
        CHANGED_PLUGINS_ADDED_OR_REMOVED=7,

        UPDATE_DO_REPAINT=1<<16,
        UPDATE_DO_UPDATE_LISTS=1<<17,
        UPDATE_DO_UPDATE_TREES=1<<18,
        UPDATE_DO_UPDATE_ENABLED=1<<19;
    public static void updateOn(int id, JComponent c) {
        final Object ref=wref(c);
        if (!cntains(ref,_updateOn[id])) _updateOn[id]=adToArray(ref,_updateOn[id], 33, Object.class);
    }
    private final static Runnable[] _updateR=new Runnable[9];
    private final static Object _updateOn[][]=new Object[9][];
    public static void updateAllNow(int typeAndAction) {
        final int type=typeAndAction&0xff;
        if (_updateR[type]==null) _updateR[type]=thrdM("_updateAllNow",ChUtils.class,new Object[]{intObjct(type)});
        inEDTms(_updateR[type],333);
    }

    public static void _updateAllNow(int typeAndAction) {
        final int type=typeAndAction&0xff;
        final int action=(typeAndAction&0xffFF0000)==0 ? 0xFFff0000 : (typeAndAction&0xffFF0000);
        final Object oo[]=_updateOn[type], keyDim=_updateR;
        for(int i=sze(oo);--i>=0;) {
            final Object o=getRmNull(i,oo);
            if (o instanceof JComponent) {
                final JComponent jc=(JComponent)o;
                if (!isVisbl(jc)) continue;

                boolean
                    enable=0!=(action&UPDATE_DO_UPDATE_ENABLED),
                    repaint=0!=(action&UPDATE_DO_REPAINT);

                if (0!=(action&UPDATE_DO_UPDATE_LISTS)) {
                    if (o instanceof JComboBox)  {
                        ListOrTreeNeedsRepaint.mayUpdateCombo((JComboBox)o);
                        enable=false;
                        repaint=true;
                    } else if (o instanceof JList) {
                        ListOrTreeNeedsRepaint.mayUpdateList((JList)o);
                        enable=false;
                        repaint=true;
                    }
                }
                if (0!=(action&UPDATE_DO_UPDATE_TREES)) {
                    if (o instanceof JTree) repaint=true;
                }
                if (enable) {
                    updateEnabled(jc);
                }
                if (gcp(KOPT_HIDE_IF_DISABLED,jc)!=null) {
                    final Dimension d=jc.getPreferredSize();
                    final int dim=d==null ? 0 : d.width+d.height;
                    if (dim!=atoi(gcp(keyDim,jc))) {
                        revalAndRepaintC(jc);
                        pcp(keyDim,intObjct(dim),jc);
                    }
                }
                if (repaint) jc.repaint();
            }
        }
    }
    public static void updateEnabled(Object o) {
        final JComponent jc=derefJC(o);
        if (jc!=null) jc.setEnabled(jc.isEnabled());
        else {
            for(int i=szeVA(jc); --i>=0;) updateEnabled(getRmNull(i,o));
        }
    }
    /* <<< Update <<< */
    /* ---------------------------------------- */
    /* >>> Shape >>> */
    private final static Rectangle _rect=new Rectangle();
    public static boolean rectEquals(int x,int y, int w, int h, Rectangle r) {
        return r!=null && r.x==x&&r.y==y&&r.width==w&&r.height==h;
    }
    public static boolean setRect(int x,int y, int w, int h, Rectangle r) { return setRectAndRepaint(x,y,w,h, r, null);  }
    public static boolean setRectAndRepaint(int x,int y, int w, int h, Rectangle r, Component c) {
        if (r!=null && !(r.x==x&&r.y==y&&r.width==w&&r.height==h)) {
            if (c!=null) {
                final int rx=r.x, ry=r.y, rw=r.width, rh=r.height;
                if (rw>0 && rh>0 && rx+rw>0 && ry+rh>0 && c.getWidth()>0 && c.getHeight()>0) c.repaint(rx,ry, rw,rh);
            }
            r.setBounds(x,y,w,h);
            return true;
        }
        return false;
    }
    /*<<< Shape <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    private static void stckTrcSaveThrd(Runnable r, String name) {
        if (mapStckTrace!=null) {
            final Exception stcktr=new Exception(name);
            mapOneToMany(stcktr, r, mapStckTrace, 2, Exception.class);
        }
    }
    public static void startThrd(Runnable r) {
        final String n=r instanceof Thread ? ((Thread)r).getName() : null;
        startThrd(r,orS(n,"startThrd"));
    }
    public static void startThrd(Runnable r, String name) {
        if (r==null) return;
        final Thread t=r instanceof Thread ? (Thread)r : new Thread(r);
        if (mapStckTrace!=null) {
            final Exception
                ee[]=mapStckTrace.get(Thread.currentThread()),
                stcktr=new Exception("startThrd_"+ (name!=null?name:t.getName()));
            for(int i=0;i<sze(ee);i++) {
                mapOneToMany(stcktr, t, mapStckTrace, 2, Exception.class);
                mapOneToMany(ee[i],  t, mapStckTrace, 2, Exception.class);
            }
            stckTrcSaveThrd(t,"startThrd_"+ (name!=null?name:t.getName()));
        }
        t.start();
    }
    public static Map<Runnable,Exception[]> mapStckTrace;
    public static void assrtEDT() {
        if (!isEDT()) { puts(ANSI_RED+" EDT! "+ANSI_RESET); stckTrc1(null); }
    }
    public static boolean isEDT() { return dtkt()==null || EventQueue.isDispatchThread();}
    public static void inEDT(Runnable r) {
        if (r==null) return;
        if (isEDT()) r.run();
        else try {
                stckTrcSaveThrd(r,"inEDT");
                SwingUtilities.invokeAndWait(r);
                //if (mapStckTrace!=null) mapStckTrace.put(r,null);
            } catch(Exception e){e.printStackTrace(System.out);}
    }

    public static void inEDTms(Runnable r, long afterMS) {
        if (afterMS==0) inEDT(r);
        else ChDelay.runAfterMS(EDT,r, afterMS);
    }

    public static void inEdtCR(ChRunnable cr, String id, Object o) {
          if (cr==null) return;
          if (isEDT()) cr.run(id,o);
        else inEDT(thrdCR(cr,id,o));
    }
    public static void inEdtLaterCR(ChRunnable cr, String id, Object o) {
        if (cr==null) return;
        if (isEDT()) cr.run(id,o);
        else try {
                final Runnable r=thrdCR(cr,id,o);
                stckTrcSaveThrd(r,"inEDT");
                SwingUtilities.invokeLater(r);
            } catch(Exception e) { stckTrc(e);}
    }
    public static void inEdtLater(Runnable r) {
        if (r==null) return;
        if (!isEDT()) try { SwingUtilities.invokeLater(r); } catch(Exception e) { stckTrc(e);}
        else r.run();
    }
    public static Thread thrdBG(Runnable r) { return thrdCR(instance(0),"thrdBG",r); }
    public static Thread thrdEdtLater(Runnable ...rr) {  return thrdCR(instance(0),"runnableEdtLater",rr); }
    public static Thread thrdRRR(Runnable ...rr) {  return thrdCR(instance(0),"runnable",rr); }
    public static Thread thrdCR(ChRunnable r,String id) { return thrdCR(r,id, null, (long[])null);}
    public static Thread thrdCR(ChRunnable r,String id,Object arg) { return thrdCR(r,id,arg,(long[])null);}
    public static Thread thrdCR(ChRunnable r,String id,Object arg, long[] duration) {
        if (arg instanceof long[]) assrt();
        return new ChDelay(EDT, ChDelay.RUNNABLE_ARG, r,0, arg!=null ? new Object[]{id,arg} : id, duration);
    }
    public static void runAftrMS(int msg, Runnable r) { if (r!=null)  startThrd(thrdRRR(thrdSleep(999), r)); }
    public static Runnable thrdSleep(int msec) { return thrdCR(instance(0), "WAIT_MS", intObjct(msec));}
    public static Runnable thrdM(String methodName, Object instOrClass) {
        return thrdMR(methodName,instOrClass,(Object[]) null,(Object[]) null);
    }
    public static Runnable thrdM(String methodName, Object instOrClass, Object para[]) {
        return thrdMR(methodName,instOrClass, para, (Object[]) null);
    }
    public static Runnable thrdMR(String methodName, Object instOrClass, Object para[], Object ret[]) {
        if (instOrClass==null) return null;
        assert ret==null || ret.length==1;
        final Method m=findMthd(0L, methodName, instOrClass, para);
        if (m==null && myComputer()) {
            stckTrc();
            debugExit("\n methodName="+methodName+"\n instOrClass="+instOrClass+"\n para=",para);
        }
        return m==null ? null : thrdCR(instance(0), "INVOKE_MTHD",  new Object[]{m,instOrClass, para, ret});
    }

    /*<<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> ToolTipManager  JComponent >>> */
    private static int  initialDelay, reshowDelay;
    public static void setTipTiming(int initial, int reshow) {
        if (initialDelay==0) {
            initialDelay=ToolTipManager.sharedInstance().getInitialDelay();
            reshowDelay=ToolTipManager.sharedInstance().getReshowDelay();
        }
        if (initial<0) {
            ToolTipManager.sharedInstance().setInitialDelay(initialDelay);
            ToolTipManager.sharedInstance().setReshowDelay(reshowDelay);
        } else {
            ToolTipManager.sharedInstance().setInitialDelay(initial);
            ToolTipManager.sharedInstance().setReshowDelay(reshow);
        }
    }
    public static Component rtt(Component c) {
        if (c instanceof JComponent ) {
            ToolTipManager.sharedInstance().registerComponent((JComponent)c);
            ToolTipManager.sharedInstance().setEnabled(true);
        }
        return c;
    }
    /* <<< ToolTipManager <<< */
    /* ---------------------------------------- */
    /* >>> JTabbedPane  BasicTabbedPaneUI  >>> */
    {
        final Insets ins=UIManager.getInsets("TabbedPane.tabInsets");
        if (ins!=null) { ins.left=2; ins.right=1; }
    }
    final static String KEY_tabComponent="CU$$KTC";
    private final static Object KEY_tabLabel=new Object(), KEY_JTabbedPane=new Object(), KEY_undockableComponent=new Object();
    public static void adMainTab(Object comp, JTabbedPane tp, Class c0) {
        if (comp==null || tp==null) return;
        final Component pnl=comp instanceof Component ? (Component)comp : getPnl(comp);
        final Class c=c0!=null ? c0 : tp.getClass();
        pcp(KEY_TOOLTIP, dTip(c),pnl);
        TabItemTipIcon.setTiti( TabItemTipIcon.getTiti(c),pnl);
        final Component tabComp=adTab(0, null, pnl, tp);
        if (gcp(KOPT_UNDOCKABLE,tp)!=null) setTabUndocksPanel(tabComp,tp);
    }

    public static JComponent adTab(int opt, String title0, Object component, JTabbedPane tp) {
        JComponent tabComp=null;
        if (tp!=null && component!=null) {
            final Component getPnl=component instanceof Component ? (Component)component : getPnl(component);
            final Icon icon=dIcon(component);
            final String title=addHtmlTagsAsStrg(title0!=null?title0: dTab(component)), tip=dTip(component);
            final int n=tp.getTabCount();
            final JComponent oldSelected=derefJC(tp.getSelectedComponent());
            tp.addTab(title, icon, getPnl,  tip);
            try { tp.setSelectedIndex(n); } catch(Exception ex) { putln(RED_CAUGHT_IN,ex); }
            pcp(KEY_NEEDS_REVALIDATE, "",tp);
            ChButton lab=null;
            pcp(KEY_tabLabel,title,component);
            if (javaVsn()>15) {
                lab=labl(title).i(icon).tt(tip);
                tabComp=lab.cp(ChButton.KEY_JTabbedPane, tp).cp(KEY_tabComponent,wref(component));
                invokeMthd(RFLCT_PRINT_EXCEPTIONS, "setTabComponentAt",tp,new Object[]{intObjct(n),lab} );
                pcp(KEY_tabLabel,lab,component);
                pcp(KEY_JTabbedPane,wref(tp), tabComp);
                addMoli(MOLI_TAB, lab);
                if (gcp(KOPT_UNDOCKABLE,component)!=null) setTabUndocksPanel(lab,getPnl);
                lab.setHorizontalAlignment(JLabel.RIGHT);
                ChDelay.revalidate(lab,999);
                revalidate_whenMouseEnter1(true, lab);
            }
            ChDelay.revalidate(component,999);
            ChDelay.revalidate(oldSelected,999);
            if (0!=(opt&CLOSE_MASK) || null!=gcp(KEY_CLOSE_HOOKS,component)) {
                pcpAddOpt(KEY_CLOSE_OPT, opt&CLOSE_MASK, getPnl);
                closeOnKey((opt&CLOSE_MASK)|CLOSE_CHILDS, getPnl, getPnl);
                ChRenderer.setSmallButtonXc( -EM, lab);
            }
        }
        return tabComp;
    }
    public static void setTtleAt(Component comp, String title, JTabbedPane tp) {
        final int i=tp!=null ? tp.indexOfComponent(comp):-1;
        final String t=i<0?null:addHtmlTagsAsStrg(title);
        if (t!=null && !t.equals(tp.getTitleAt(i))) {
            tp.setTitleAt(i,t);
            final JComponent lab=gcp(KEY_tabLabel,comp ,JComponent.class);
            if (lab!=null) { setTxt(t,lab); lab.invalidate(); lab.repaint(); }
        }
    }
    public static void setTabUndocksPanel(Component tab, Component panel) {
        if (tab!=null && panel!=null) {
            evLstnr(MOMOLI_PANEL_UNDOCK).addTo("M", tab);
            pcp(KEY_undockableComponent,wref(panel), tab);
        }
    }
    /* <<< JTabbedPane <<< */
    /* ---------------------------------------- */
    /* >>> Parents  >>> */
    public static Container parentC(Object o) {
        if (o instanceof File) assert(false);
        return o instanceof Component ? ((Component)o).getParent() : null;
    }
    public static <T extends Object> T parentC(boolean recurs, Object o, Class<T> clas) {
        Object comp=o;
        while(recurs) {
            if ( comp==null  || clas.isAssignableFrom(comp.getClass())) return (T)comp;
            comp=parentC(comp);
        }

        final Object par=parentC(o);
        return  par!=null && clas.isAssignableFrom(par.getClass()) ? (T)par : null;
    }
    public static boolean isVisbl(Component c) {
        if (c==null||!c.isShowing() || !c.isVisible()) return false;
        final Window w=parentWndw(c);
        if (w==null)  return false;
        if (w instanceof JFrame && ((JFrame)w).getState()==JFrame.ICONIFIED) return false;
        return true;
    }
    public static Window parentWndw(Object c) {  //  static Window | getWindowAncestor(Component c)
        if (c instanceof Window) return (Window)c;
        if (c instanceof Component) {
            final Component w=SwingUtilities.getRoot((Component)c);
            if (w instanceof Window) return (Window)w;
            return  parentC(false, SwingUtilities.getRootPane((Component)c),Window.class);
        } else return null;
    }
    public static Runnable thread_rmFromParent(Object obj){
        return thrdM("rmFromParent", ChUtils.class, new Object[]{obj});
    }
    public static void rmFromParent(Object obj) {
        if (obj==null) return;
        if (!isEDT()) {
            inEdtLater(thread_rmFromParent(obj));
            return;
        }
        for (int i=0; i<szeVA(obj); i++) rmFromParent(get(i,obj));
        final Component c=getPnl(obj), parent=parentC(c);
        if (c==null) return;
        if (c instanceof JComponent) {
            final Object tp=gcpa(ChTabPane.KEY_PARENT_CTP,c);
            if (tp instanceof ChTabPane) {
                ((ChTabPane)tp).removeTab((JComponent)c);
                return;
            }
        }
        if (parent instanceof ChTableLayout) {
            ((ChTableLayout)parent).removeColumn(obj);
            return;
        }
        final Container con=c.getParent();
        if (con!=null) {
            final Window parentW=parentWndw(c);
            if (parentW instanceof JFrame && ((JFrame)parentW).getContentPane()==c) {
                parentW.setVisible(false);
            }
            try {

                con.remove(c);
            } catch(Throwable e) { stckTrc(e); }
            try {
                con.repaint();
                ChDelay.revalidate(con,99);
            } catch(Exception e) {}
        }
    }
    private final static Object ANC[]={null};
    public static Component[] ancestrs(Component c) { return toArryClr(ancestrs(c,vClr(ANC)), Component.class);}
    private static List ancestrs(Component c,List<Component> v) {
        if (c!=null) {
        Object parent=gcp(JMenu.class,c);
        if (parent==null) parent=c.getParent();
        if (parent==null && c instanceof JPopupMenu) { parent=((JPopupMenu)c).getInvoker();}
        if (parent instanceof Component) {
            ancestrs((Component)parent,v);
            v.add((Component)parent);
        }
        }
        return v;
    }
    /*<<< Parents  <<< */
    /* ---------------------------------------- */
    /* >>>Children  >>> */
    private final static Object CHILDS[]={null};
    public static <T extends Object> T[] childsR(Component comp, Class<T> clas) {
        return toArryClr(vChildsR(comp,clas,vClr(CHILDS)),clas);
    }
    public static Collection vChildsR(Component comp,Class clas,Collection v) {
        if (comp==null) return v;
        if (clas==null || isInstncOf(clas,comp)) { v.add(comp); }
        if (comp instanceof JMenu) {
            final JMenu m=(JMenu)comp;
            final int N=m.getItemCount();
            for(int i=0;i<N;i++) vChildsR(m.getItem(i),clas,v);
        } else {
            for(Component c : childs(comp)) vChildsR(c,clas,v);
        }
        return v;
    }
    public static <T extends Object> T child(Component comp,Class<T> clas) { return child(comp,clas,null); }
    public static <T extends Object> T child(Component comp,Class<T> clas, String label) {
        if (comp==null) return null;
        if (isInstncOf(clas,comp)) {
            final String txt= label!=null && comp instanceof AbstractButton ? getTxt(comp) : null;
            if (txt==null || txt.equalsIgnoreCase(label) ||
                label!=null && label.endsWith(".*") && strEquls(label,0,label.length()-2,txt,0)
                ) return (T)comp;
        }
        for(Component c : childs(comp) ) {
            final Object ret=child(c,clas,label);
            if (ret!=null) return (T)ret;
        }
        return null;
    }
    /* ---------------------------------------- */
    private final static Object[] CLONES={null};
    public static Object[] getClones(Component c) {
        return toArryClr( getClones(c,vClr(CLONES),0), Object.class);
    }
    private static List getClones(Object c, List v, int recursion) {
        final Reference rr[]=gcp(KEY_CLONES,c, Reference[].class);
        if (rr!=null) {
            for(Reference r : rr) {
                final Object o=r!=null ? r.get() : null;
                if (o!=null) {
                    v.add(o);
                    if (recursion<4) getClones(o,v,recursion+1);
                }
            }
        }
        return v;
    }
    public static Runnable thread_setEnabld(boolean b, Object o) { return thrdCR(instance(0), b? "ENABLE":"DISABLE", o); }
    public static void setEnabld(boolean b, Object compOrArray) {
        final Component c=derefC(compOrArray);
        if (c!=null && c.isEnabled()==b) return;
        if (!isEDT()) {
            inEdtLater(thread_setEnabld(b, compOrArray));
            return;
        }

        if (c!=null) {
            c.setEnabled(b);
            repaintC(getClones(c));
            revalAndRepaintC(c);
            if (c instanceof JComponent && c.isVisible() && gcp(KOPT_HIDE_IF_DISABLED, c)!=null) {
                final Container parent=c.getParent();
                if (isVisbl(parent)) {
                    invalTree(parent);
                    revalAndRepaintC(parent);
                }
                adaptSmallSizeParent(c);
            }
        } else {
            for(int i=szeVA(compOrArray); --i>=0;) setEnabld(b,get(i,compOrArray));
        }
    }
    public static boolean isEnabld(JComponent c, boolean superIsEnabled) {
        final Component deleg=gcp(KEY_ENABLED_IF_ENABLED, c, Component.class);
        if (deleg!=null) return deleg.isEnabled();
        boolean enabled=superIsEnabled;
        Object o;
        if ( (o=gcp(KEY_ENABLED,c)) instanceof IsEnabled) enabled=((IsEnabled)o).isEnabled(c);
        return enabled;
    }
    public static boolean isEnabld(Object o) {
        if (o==null) return false;
        if (o instanceof JComponent) return ((JComponent)o).isEnabled();
        if (runCR(o,ChRunnable.RUN_IS_DISABLED)!=null) return false;
        return true;
    }

    public static void invalTree(Component parent) {
        if (parent==null) return;
        if (!isEDT()) {
            inEdtLater(thrdM("invalTree",ChUtils.class, new Object[]{parent}));
            return;
        }
        for(Component c : childs(parent))  invalTree(c);
        if (parent.isValid()) parent.invalidate();
    }
    public static void setEnabledIncludingChildren(Component parent,boolean b) {
        if (parent==null) return;
        for(Component c : childs(parent)) setEnabledIncludingChildren(c,b);
        parent.setEnabled(b);
    }
    public static void rmAllChilds(Component parent) {
        if (!isEDT()) {
            inEdtCR(instance(0),"rmChilds",parent);
            return;
        }
        if (parent==null) return;
        if (parent instanceof JComponent) rmAllListnrs(false, parent);
        if (parent instanceof Container) {
            Component cc[]=childs(parent);
            final JTabbedPane tp=deref(parent,JTabbedPane.class);
            if (tp!=null) {
                cc=new Component[tp.getTabCount()];
                for(int i=cc.length;--i>=0;) cc[i]=tp.getComponentAt(i);
            }
            for(Component c:cc) rmAllChilds(c);
            try { ((Container)parent).removeAll(); } catch(Throwable ex) {}
        }
    }

    public static int closeOpts(Object o) { return CLOSE_MASK&atoi(gcp(KEY_CLOSE_OPT,o)); }
    public static void closeW(int opt, Object o) { clos(opt,parentWndw(o)); }
    public static void dispos(Object objects) { clos(CLOSE_DISPOSE, objects); }
    public static void clos(int opt0, Object objects) {
        final String ask="Really close this component? <br><br><sub>Note: You can avoid this confirmation dialog by holding ctrl.</sub>";
        if (objects==null) return;
        final Object oo[]=oo(objects);
        final int opt=opt0&~CLOSE_ASK;
        if (countNotNull(oo)==0 || (opt0&CLOSE_ASK)!=0 && !ChMsg.yesNo(ask)) return;
        for(Object object0 : oo) {
            final Object o=deref(object0);
            if (o==null) continue;
            if (0!=(opt&CLOSE_DISPOSE_TABS)) {
                final Object[] childs=
                    o instanceof ChTabPane ? ((ChTabPane)o).tabComponents() :
                    o instanceof JTabbedPane ? childs((Component)o) :
                    o instanceof ChTableLayout ? ((ChTableLayout)o).getAllComponents() :
                    null;
                for(Disposable d : derefArray(childs, Disposable.class)) d.dispose();
            }

            if (0!=(opt&CLOSE_DISPOSE)) {
                final Disposable d=deref(o,Disposable.class);
                if (d!=null) d.dispose();
                else invokeMthd("dispose",o);
                if(o instanceof ChJList) clr(((ChJList)o).getList());
            }
            for(Object o2 : oo(gcp(KEY_CLOSE_HOOKS,o))) {
                final Disposable d=deref(o2,Disposable.class);
                if (d!=null && d!=o) d.dispose();
                else runR(deref(o2,Runnable.class));
            }
            if (0==(opt&CLOSE_NO_REMOVE)) {
                final Window w=deref(o,Window.class);
                if (w!=null) w.setVisible(false);
                final Component container=getPnl(o);
                if (container!=null) {
                    if ( (opt&CLOSE_RM_CHILDS)!=0) {
                        for(Component child : childs(container)) if (child!=container) clos(CLOSE_DISPOSE|CLOSE_RM_CHILDS,child);
                        rmAllChilds(container);
                    }
                    revalAndRepaintC(parentC(container));
                }
                rmFromParent(o);
            }

        }
    }
    /*<<< Children <<< */
    /* ---------------------------------------- */
    /* >>> JScrollPane >>> */
    private final static Map<ListSelectionModel,Object>  mapLisSelModel2Comp=new WeakHashMap();
    public static void adSelListener(Component jc) {
        final ListSelectionModel lsm=
            jc instanceof JList  ? ((JList) jc).getSelectionModel() :
            jc instanceof JTable ? ((JTable)jc).getSelectionModel() : null;
        if (lsm!=null) {
            lsm.removeListSelectionListener(chEvLi());
            lsm.addListSelectionListener(chEvLi());
            mapLisSelModel2Comp.put(lsm,wref(jc));
        }
        if (jc instanceof JTree) {
            ((JTree)jc).removeTreeSelectionListener(chEvLi());
            ((JTree)jc).addTreeSelectionListener(chEvLi());
        }
    }
    public final static int SCRLLPN_EBORDER=1<<1, SCRLLPN_INHERIT_SIZE=1<<2, SCRLLPN_ORIG_SB=1<<3, SCRLLPN_TOOLS=1<<5,
        SCRLLPN_CLR_BUT=(1<<6)|SCRLLPN_TOOLS, SCRLLPN_NO_VSB=1<<8, SCRLLPN_NO_HSB=1<<9, SCRLLPN_TOP=1<<10, SCRLLPN_LOCK_BOTTOM=1<<11;
    public static ChJScrollPane scrllpnT(Object c) { return scrllpn(SCRLLPN_TOOLS, c,null);}
    public static ChJScrollPane scrllpn(Object c) { return scrllpn(0, c,null);}
    public static ChJScrollPane scrllpn(int opt, Object c) { return scrllpn(opt, c,null);}
    public static ChJScrollPane scrllpn(int options, Object object, Dimension d) {
        final Object c=object instanceof BA ? ((BA)object).textView(true) : object;
        final int opt=options|atoi(gcp(KEY_OPTS_SCROLLPANE,c));
        ChJScrollPane sp=getSP(c);
        final ChTextComponents tools=ChTextComponents.tools(c);
        if (sp==null && c instanceof JComponent) {
            sp=new ChJScrollPane(opt, (JComponent)c);
            if (d!=null) sp.setPreferredSize(d);
            if (0!=(opt&SCRLLPN_EBORDER)) sp.setBorder(createEtchedBorder());
            final boolean isList=c instanceof JList || c instanceof JTable || c instanceof JTree;
            final String fileName=gcps(DialogStringMatch.KEY_SAVE,c);
            if (0!=(opt&SCRLLPN_NO_VSB)) sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
         if (0!=(opt&SCRLLPN_NO_HSB)) sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            if (0!=(opt&SCRLLPN_TOOLS) || fileName!=null)  {
                if (fileName!=null || isList) sp.addMenuItem(ChButton.dialogStringMatch(atol(gcp(DialogStringMatch.KEY_OPTIONS,c)), new Object[]{wref(c)}, orS(fileName,"save")));
                if (isList) {
                    sp.addMenuItem(new int[]{ChJScrollPane.AS_TXT});
                    if (c instanceof JList) sp.addMenuItem(new int[]{ChJScrollPane.INV_SELECTION});
                }
                if (tools!=null && 0!=(SCRLLPN_CLR_BUT&opt)) sp.addMenuItem(tools.newClearButton());
            }
        }
        if (0!=(opt&(SCRLLPN_LOCK_BOTTOM|SCRLLPN_TOP))) {

            ChScrollBar.lock(0!=(opt&SCRLLPN_LOCK_BOTTOM)?1:-1, getSB('V',sp));

        }
        if (c instanceof JTextComponent || c instanceof ChTextView) setDragScrolls('b', (Component)c, sp);
        return sp;
    }
    public static Component scrllpnChild(Object scrollPane) {
        final JScrollPane sp=deref(scrollPane,JScrollPane.class);
        final JViewport vp=sp==null?null:sp.getViewport();
        return vp==null?null:vp.getView();
    }
    /** @ param hv: (H)ORIZONTAL (V)ERTICAL (B)OTH  or with alt h v b */
    public static void setDragScrolls(char hv, Component c, Component sp) {
        final int id=(hv|32)=='h' ? MOMOLI_SBH : (hv|32)=='v' ? MOMOLI_SBV : MOMOLI_SBB;
        if (hv=='h' || hv=='b') pcp(KOPT_DRAG_ALTKEY[0],"", c);
        if (hv=='v' || hv=='b') pcp(KOPT_DRAG_ALTKEY[1],"", c);
        evLstnr(id).addTo("M", c);
        if (sp!=null) {
            if (sp instanceof JScrollPane || sp instanceof JScrollBar) pcp(instance(id),wref(sp),c);
            else assrt();
        }
    }
    public static ChJScrollPane getSP(Object c) { return c==null?null:gcp(ChJScrollPane.KEY_SP,c, ChJScrollPane.class);}
    public static JScrollBar getSB(char v_or_h, Object c) {
        JScrollPane sp=c instanceof JScrollPane ? (JScrollPane)c : getSP(c);
        return (sp==null?null:v_or_h=='V' ? sp.getVerticalScrollBar() : sp.getHorizontalScrollBar());
    }
    /* <<< JScrollPane <<< */
    /* ---------------------------------------- */
    /* >>> JTable JList >>> */
    public static int[] selIndices(Object oo[], ListModel m) {
        boolean bb[]=null;
        for(int i=m==null ? 0 : m.getSize(); --i>=0;) {
            if (cntains(get(i,m),oo)) {
                if (bb==null) bb=new boolean[i+1];
                bb[i]=true;
            }
        }
        final int N=countTrue(bb);
        if (N>0) {
            final int ii[]=new int[N];
            int count=0;
            for(int i=0;i<bb.length;i++) {
                if (bb[i]) ii[count++]=i;
            }
            return ii;
        }
        return NO_INT;
    }
    public static void revalidateIfModified(JComponent c, int mc) {
        if (c!=null && atoi(gcp(KEY_MC,c))!=mc) {
            pcp(KEY_MC, intObjct(mc),c);
            revalAndRepaintC(c);
        }
    }
    public static void selectAllItems(Object list) {
        if (list instanceof JList) {
            final JList jl=(JList)list;
            final int L=jl.getModel().getSize();
            if (L>0) jl.getSelectionModel().setSelectionInterval(0,L-1);
        }
    }
    private static void selectionChanged(EventObject ev) {
        final Object q=ev.getSource(), refTable=mapLisSelModel2Comp.get(q);
        selectionToSb((JComponent)(q instanceof JComponent ? q: deref(refTable)));
    }
    public static Runnable thread_selectionToSb(JComponent jc) {
        return thrdM("selectionToSb", ChUtils.class, new Object[]{jc});
    }
    public static void selectionToSb(JComponent jC) {
        if (jC==null) return;
        if (strEquls(_CC,jC.getClass()) && gcp(KEY_NOT_YET_PAINTED,jC)==null && gcp(KEY_IGNORE_SELECTION_EVT,jC)==null) {
            handleActEvt(jC, orS(gcps(KEY_ACTION_COMMAND,jC), ACTION_SELECTION_CHANGED),0);
        }
        final JScrollPane sp=parentC(true,jC, JScrollPane.class);
       ChScrollBar
           vsb=deref(getSB('V',sp), ChScrollBar.class),
           hsb=deref(getSB('H',sp), ChScrollBar.class);

       if (!isVisbl(vsb) || hght(vsb)<2*EX) vsb=null;
       if (!isVisbl(hsb) || wdth(hsb)<2*EX) hsb=null;

        if (vsb!=null || hsb!=null) {
            int[] xx=null, yy=null;
            int boxH=2, boxW=2;
            if (jC instanceof JList) {
                final JList jl=(JList)jC;
                final int ori=jl.getLayoutOrientation();
                final int fixed=jl.getFixedCellHeight();
                final int ii[]=jl.getSelectedIndices();
                final boolean wrap=ori==JList.HORIZONTAL_WRAP || ori==JList.VERTICAL_WRAP;
                if (ii!=null) {
                    yy=new int[ii.length];
                    if (hsb!=null && wrap) xx=new int[ii.length];
                    for(int i=ii.length; --i>=0;)  {
                        if (fixed>0 && !wrap) yy[i]=fixed*ii[i]+fixed/2;
                        if (xx!=null) {
                            final Point p=jl.indexToLocation(ii[i]);
                            xx[i]=x(p);
                            yy[i]=y(p);
                        }
                    }
                }
                if (fixed>0 && !wrap) boxH=fixed;
                else if (jl.getModel().getSize()>0) {
                    final Rectangle r=jl.getCellBounds(0,0);
                    boxH=r.height;
                    boxW=r.width;
                }
            } else if (jC instanceof JTable) {
                final JTable jt=(JTable)jC;
                final int ii[]=jt.getSelectedRows();
                final int fixed=jt.getRowHeight();
                boxH=fixed;
                if (ii!=null) {
                    yy=new int[ii.length];
                    for(int row=ii.length; --row>=0;) {
                        yy[row]=fixed>0 ? fixed*ii[row]+fixed/2 : y(jt.getCellRect(row,0, false));
                    }
                }
            } else if (jC instanceof JTree) {
                final JTree jl=(JTree)jC;
                final TreePath ii[]=jl.getSelectionPaths();
                if (ii!=null) {
                    yy=new int[ii.length];
                    for(int i=ii.length; --i>=0;) {
                        final Rectangle r=jl.getPathBounds(ii[i]);
                        if (r!=null) yy[i]=y(r)+r.height/2;
                    }
                }
            }
            if (vsb!=null) vsb.setSelectedY(yy,1,(Color[])null, boxH);
            if (hsb!=null) hsb.setSelectedY(xx,1,(Color[])null, boxW);
        }
    }
    private static long _whenNxtPrev;
    public static void selectNxtPrev(JComboBox choice, int upDown) {
        if (upDown!=0 && choice!=null && (System.currentTimeMillis()-_whenNxtPrev)>33) {
            _whenNxtPrev=System.currentTimeMillis();
            final int idx=choice.getSelectedIndex() + (upDown<0 ? -1 : 1);
            if (idx>=0 && idx<choice.getItemCount()) {
                choice.setSelectedIndex(idx);
                choice.repaint();
            }
        }
    }
    public static File[] getSelFiles(Object jl) {
        final Object oo[]=getSelValues(jl);
        final File ff[]=new File[sze(oo)];
        for(int i=ff.length; --i>=0;) {
            if (oo[i] instanceof File || oo[i] instanceof java.net.URL) ff[i]=file(oo[i]);
        }
        return rmNullA(ff,File.class);
    }
    public static Object[] getSelValues(Object ref) {
        final Object o=deref(ref);
        if (o instanceof JFileChooser) return ((JFileChooser)o).getSelectedFiles();
        if (o instanceof JList) return ((JList)o).getSelectedValues();
        if (o instanceof JComboBox) return new Object[]{((JComboBox)o).getSelectedItem()};
        if (o instanceof Object[]) return (Object[])o;
        if (o instanceof Collection) return ((Collection)o).toArray();
        if (o instanceof JTree) {
            final TreePath pp[]=((JTree)o).getSelectionPaths();
            if (sze(pp)==0) return NO_OBJECT;
            final Set v=new HashSet(pp.length);
            for(int i=sze(pp); --i>=0;) v.add(pp[i].getLastPathComponent());
            return v.toArray();
        }
        return NO_OBJECT;
    }
    private final static Object[] vTP_ref={null};
    public static void selectObjectsIE(Collection v, JTree c) {
        final Object save=gcp(KEY_IGNORE_SELECTION_EVT,c);
        pcp(KEY_IGNORE_SELECTION_EVT,"",c);
        selectObjectsT(v,c);
        pcp(KEY_IGNORE_SELECTION_EVT, save, c);
    }
    private static void selectObjectsT(Collection v,JTree jTree) {
        final Collection<TreePath> vTP=fromSoftRef(0,vTP_ref, HashSet.class);
        vTP.clear();
        for(int i=jTree.getRowCount(); --i>=0;) {
            final TreePath tp=jTree.getPathForRow(i);
            final Object o=tp.getLastPathComponent();
            if (v.contains(o)) vTP.add(tp);
        }
        jTree.setSelectionPaths(toArry(vTP,TreePath.class));
    }
    public static void scrollToVis(Object o, Component c) {
        if (o==null) return;
        if (c instanceof JTree) {
            final JTree t=(JTree)c;
            final TreePath tp=getTreePath(o, t);
            if (tp!=null) t.scrollPathToVisible(tp);
        }
    }
    /* <<< JTable JList <<< */
    /* ---------------------------------------- */
    /* >>> JTree >>> */
    public static TreePath getTreePath(Object o, JTree jTree) {
        for(int i=jTree.getRowCount();--i>=0;) {
            final TreePath tp=jTree.getPathForRow(i);
            if (tp!=null && tp.getLastPathComponent()==o) return tp;
        }
        return null;
    }
    /* <<< JTree <<< */
    /* ---------------------------------------- */
    /* >>> JPopupMenu >>> */
    public static void setCntxtObj(Object value) { CNTXT_OBJCTS[0]=oo(value); }
    public static Object getCntxtObj() { return CNTXT_OBJCTS[0];}
    private static Object _popup;
    public static Runnable thread_shwPopupMenu(JPopupMenu m) {
        return thrdM("shwPopupMenu",ChUtils.class,new Object[]{m});
    }
    public static boolean shwPopupMenu(JPopupMenu pop) {
        if (pop==null) return false;
        pcp(KOPT_POPUP_MENU,"",pop);
        pop.addPopupMenuListener(chEvLi());
        final MouseEvent ev=lstMouseEvt();
        final Component c=ev==null?null:(Component)ev.getSource();
        try { /* IllegalComponentState/NullPointer */
            if (c!=null&&c.isShowing())  pop.show(c, maxi(1, x(ev)-EX), maxi(1, y(ev)-EX));
        } catch(Exception ex) {
            stckTrc(ex);
            final ChFrame f=ChFrame.anyFrame();
            Point p=mouseLctn();
            if (f!=null && p!=null) {
                SwingUtilities.convertPointFromScreen(p=new Point(p), f.getRootPane());
                pop.show(f.getRootPane(), maxi(1, x(p)-EX), maxi(1, y(p)-EX));
            }
        }
        _popup=wref(pop);
        return true;
    }
    public static void mayClosePopupMenu(AWTEvent ev) {
        if (ev==null || ev.getID()==MOUSE_PRESSED) {
            final JPopupMenu p=deref(_popup, JPopupMenu.class);
            if (p!=null && p.isVisible()) p.setVisible(false);
        }
    }
    public static Object objectAt(Point point, Object obj) {
        Point p=point;
        Object o=obj;
        JPopupMenu jpm=deref(obj,JPopupMenu.class);
        if (obj instanceof EventObject) {
            final Object q=((EventObject)obj).getSource();
            if (q instanceof JPopupMenu) jpm=(JPopupMenu)q;
            if (obj instanceof MouseEvent) {
                o=q;
                p=point(obj);
            }
        }
        if (jpm!=null) o=jpm.getInvoker();
        if (p==null && o instanceof Component) {
            final Point pScreen=mouseLctn();
            if (pScreen!=null) SwingUtilities.convertPointFromScreen(p=new Point(pScreen), (Component)o);
        }
        if (p==null||o==null) return null;
        if (o instanceof JTree) {
            final TreePath tp= ((JTree)o).getPathForLocation(x(p), y(p));
            return tp!=null ? tp.getLastPathComponent() : null;
        }
        if (o instanceof JList) return get(((JList)o).locationToIndex(p),o);
        if (o instanceof JTable) {
            final JTable t=(JTable)o;
            try { return t.getValueAt( t.rowAtPoint(p),t.columnAtPoint(p)); } catch(Exception e){}
        }
        return null;
    }
    public static Object[] contextObjectsAt(AWTEvent ev) {
        final Object q=ev!=null ? ev.getSource() : null;
        return q instanceof Component ? contextObjectsAt(point(ev), q) : NO_OBJECT;
    }
    public static Object[] contextObjectsAt(Point p, Object q) {
        return contextObjects(objectAt(p,q), getSelValues(q));
    }
    public static Object[] contextObjects(Object oMouse, Object oo[]) {
        if (oMouse==null) return NO_OBJECT;
        final int i=idxOf(oMouse,oo);
        if (i<0) return new Object[]{oMouse};
        if (oo[0]==oMouse) return oo;
        final Object ooReorder[]=oo.clone();
        ooReorder[0]=oMouse;
        ooReorder[i]=oo[0];
        return ooReorder;
    }
    public static JPopupMenu jPopupMenu(int opt, String parent, Object[] oo) {
        final JPopupMenu m=new JPopupMenu(parent);
        adToMenu(opt,m,oo,parent);
        return m;
    }
    /* <<< JPopupMenu <<< */
    /* ---------------------------------------- */
    /* >>> Own GUI elements >>> */
    public static void radioGrpSet(int idx,Object[] radioGrp) {
        for(int i=sze(radioGrp); --i>=0;) {
            final AbstractButton b=deref(radioGrp[i],AbstractButton.class);
            b.setSelected(i==idx);
        }
    }
    public static int radioGrpIdx(Object evtSource, Object[] radioGrp) {
        if (radioGrp!=null && evtSource!=null) {
            for(Object r : radioGrp) {
                if (evtSource==r || evtSource==gcp(KEY_CLONED_FROM,r)) return radioGrpIdx(evtSource);
            }
        }
        return MIN_INT;
    }
    public static int radioGrpIdx(Object o) {
        if (o==null) return -1;
        if (o instanceof Object[]) {
            for(int i=sze(o); --i>=0;)
                if (isSelctd(get(i,o))) return i;
        }
        return radioGrpIdx(gcp(instance(ALI_RADIO),o));
    }
    public static AbstractButton[] radioGrp(String[] ss, int idx, ActionListener ali) {
        final AbstractButton bb[]=new AbstractButton[ss.length];
        final Object[] rr=new Object[ss.length];
        for(int i=bb.length; --i>=0;) {
            final ChButton t=toggl(ss[i]).li(ali).s(idx==i);
            evLstnr(ALI_RADIO).addTo("a",t);
            pcp(instance(ALI_RADIO),rr,t);
            rr[i]=wref(t);
            bb[i]=t.radio();
        }
        return bb;
    }
    public static void radioGrp(int idx, AbstractButton...bb) {
        final Object[] rr=new Object[bb.length];
        for(int i=bb.length; --i>=0;) {
            final AbstractButton b=bb[i];
            if (b==null) continue;
            if (i==idx) b.setSelected(true);
            evLstnr(ALI_RADIO).addTo("a",b);
            pcp(instance(ALI_RADIO),rr,b);
            rr[i]=wref(b);
        }
    }
    public static boolean isChLabl(Object q) { return q instanceof ChButton && 'L'==((ChButton)q)._typ; }

    public static ChButton newButtn() { return newButtn(0L,null); }
    public static ChButton newButtn(long opt, String butLabel) { return _buttn('B', opt, butLabel); }

    public static ChButton labl() { return labl(null);}
    public static ChButton labl(String s) { return _buttn('L', 0L, s); }

    public static ChButton toggl() { return toggl(0,null); }
    public static ChButton toggl(String butLabel) { return toggl(0,butLabel); }
    public static ChButton toggl(long options, String butLabel) { return _buttn('T', options, butLabel); }

    public static ChButton _buttn(char type, long opt, String butLabel) {
        if (!isEDT()) {
            final Object para[]={null, intObjct(type), longObjct(opt), butLabel};
            inEDT(thrdCR(instance(0),"BUTTON",para));
            return (ChButton)para[0];
        }
        return new ChButton(type,butLabel).setOptions(opt);
    }

    public static AbstractButton cbox(boolean selected, String butLabel, String tip, ActionListener li) {
        final AbstractButton but=new JCheckBox(butLabel);
        setTip(tip,but);
        but.setSelected(selected);
        but.addActionListener(li);
        return but;
    }
    public static AbstractButton cbox(String butLabel) { return cbox(false,butLabel,null,null); }
    public static JComponent pnlTogglOpts(Object pan) { return pnlTogglOpts("Options", pan); }
    public static JComponent pnlTogglOpts(String msg, Object pan) {
        final ChButton b=toggl().doCollapse(pan);
        if (chrAt(0,msg)=='*') b.doPack();
        return pnl(FLOWLEFT, OPAQUE_FALSE,  b, delPfx('*',msg));
    }

    /* <<< Own GUI elements <<< */
    /* ---------------------------------------- */
    /* >>> Border >>> */
    private static Border emptyBorderEM() { return createEmptyBorder(EM,EM,EM,EM); }

    public static void noBrdr(JComponent c) {
        /* Avoid setBorder(null) because  LookAndFeel.installBorder(...) */
        if (c!=null && !(c instanceof JViewport)) try{ c.setBorder(createEmptyBorder()); } catch(Exception ex){}
    }
    /* <<< Border <<< */
    /* ---------------------------------------- */
    /* >>> Containers >>> */
    public final static int TOCONTAINR_REVAL=1<<0;
    public static Runnable thread_toContainr(int opt, Object o, Component c, String where) {
        return thrdM("toContainr", ChUtils.class, new Object[]{intObjct(opt), o,c,where});
    }
    public static boolean toContainr(int opt, Object o0, Component c) { return toContainr(0, o0, c, "");}
    public static boolean toContainr(int opt, Object o0, Component container, String where) {
        final Container c=deref(container,Container.class);
        Object o=o0;
        Component added=null;
        if (c==null || o==null) return false;
        if (!isEDT()) {
            inEdtLater(thread_toContainr(opt, o,c,where));
            return true;
        }
        final Object array[]=o instanceof Object[] ? (Object[])o : null;
        final int arrayN=sze(array);
        if (arrayN>0 && array[0]==ARRAY_CP) {
            for(int i=1; i<array.length; i+=2) pcp(array[i], array[i+1],c);
            return true;
        }
        final JMenu jMenu=c instanceof JMenu ? (JMenu)c:null;
        final JPopupMenu jPopup=c instanceof JPopupMenu ? (JPopupMenu)c:null;
        if (array!=null) {
            if (jMenu==null && jPopup==null) {
                for(Object o1 : array) toContainr(0, o1,c,where);
                return true;
            }
        }
        final String stg=o instanceof String ? (String)o : null;
        final char c0=chrAt(0,stg);
        final JComponent jc=derefJC(c);
        if (jc!=null) {
            final Border b=
                o instanceof Border ? (Border)o : c0!='#' ? null :
                stg.startsWith("#TB ") ? createTitledBorder(createEtchedBorder(), stg.substring(4), TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, null, null) :
                stg.startsWith("#TBem ") ? createCompoundBorder(emptyBorderEM(),createTitledBorder(stg.substring(6))) :
                o=="#ETB" ? createEtchedBorder() :
                o=="#RBB" ? createRaisedBevelBorder() :
                o=="#LBB" ? createLoweredBevelBorder() :
                o=="#ETBem" ? createCompoundBorder(emptyBorderEM(), createEtchedBorder()) :
                o=="#EBem" ? emptyBorderEM() :
                null;
            if (b!=null)  { jc.setBorder(b); return true; }
            if (o==REMAINING_VSPC1 || o==REMAINING_VSPC2 || o==HB || o==HBL || o==VB || o==VBHB  || o==VBPNL) return true;
            if (o==FLOWLEFT || o==FLOWRIGHT) {
                final FlowLayout fl=deref(c.getLayout(),FlowLayout.class);
                if (fl!=null) fl.setAlignment(o==FLOWLEFT ? FlowLayout.LEFT : FlowLayout.RIGHT);
                return true;
            }
            if (o==OPAQUE_FALSE || o==OPAQUE_TRUE) { jc.setOpaque(o==OPAQUE_TRUE); return true;}
            if (o instanceof Dimension) { jc.setPreferredSize((Dimension)o); return true;}
            if (o instanceof Color) { c.setBackground((Color)o); jc.setOpaque(true); return true;}
        }
        if (o instanceof LayoutManager) { c.setLayout((LayoutManager)o); return true; }
        if (strEquls(WATCH_MOVIE+"MOVIE:",stg)) {
            final String ss[]=splitTokns(stg);
            final JComponent pnl=ss.length>1 ? pnl() : null;
            for(String s0 : ss) {
                final String s=delPfx(WATCH_MOVIE,s0);
                final ChButton b=ChButton.doOpenURL(s)
                    .t("Watch Movie "+ delPfx("MOVIE:",delSfx('*', s)).replace('_',' '))
                    .tt("Watch movie in web-browser").i(IC_MOVIE);
                if (pnl!=null) {
                    pnl.add(b,where);
                    o=pnl;
                } else o=b;
            }
        }
        if (o instanceof File || o instanceof URL ||
            stg!=null && !containsHtmlTags(stg) &&
            (looks(LIKE_EXTURL,stg) || looks(LIKE_FILEPATH_EVALVAR,stg) || stg.startsWith("PUBMED:") || stg.startsWith("WIKI:") || stg.startsWith("?:/"))) {
            o=rtt(monospc(o));
        }
        if (added==null && stg!=null) {
            if (o=="#") c.add(added=Box.createHorizontalGlue());
            else if (o=="##") c.add(added=Box.createVerticalGlue());
            else if (o=="#JS") c.add(added=new JSeparator(),where);
            else if (stg.startsWith("OPT$$")) {
                pcp(o,"",c);
                if (o==KOPT_ADAPT_SMALL_SIZE) evLstnr(COMPLI_SHORT_OR_LONG).addTo("c", container);
                return true;
            } else if (o==" " && (jMenu!=null || jPopup!=null)) {
                c.add(added=new ChButton().enabled(false).mi(" "));
                added.setFocusable(false);
            }
        }
        if (added==null && jMenu==null && jPopup==null) {
            Component comp=
                o instanceof Icon ? new JLabel((Icon)o) :
                o instanceof Component ? (Component)o :
                null;
            if (o instanceof CharSequence) {
                final String s=rmTrailingImport(addHtmlTagsAsStrg(o));
                if (strstr("\u001B[",s)>=0) comp=new ChTextView("").a((CharSequence)o);
                else comp=new JLabel(rmTrailingImport(addHtmlTagsAsStrg(o)));
            }
            if (comp!=null) {
                if ((where=="p" || where=="b") && !(comp instanceof JPanel)) c.add(added=(where=="p" ? pnl(comp) : pnl(HBL,comp)));
                else c.add(added=comp,where);
            }
        } else if (added==null) {
            final JPopupMenu jpm=jPopup!=null?jPopup:jMenu.getPopupMenu();
            if (o instanceof JMenu && jMenu!=null) jMenu.add(added=(JMenu)o);
            else if (o=="-") jpm.add(added=new JPopupMenu.Separator());
            else if (stg!=null && !stg.startsWith(_CC)) jpm.add(added=menuInfo(0,stg));
        }
        if (0!=(opt&TOCONTAINR_REVAL)) revalAndRepaintC(added);
        return added!=null;
    }

    public final static int MI_HEADLINE=1<<1, MI_LONG=1<<2;
    public static Component menuInfo(int opt, String t) {
        if (t==null) return null;
        final JComponent ta;
        if (0!=(opt&MI_LONG)) {
            pcp(KEY_PREF_SIZE, dim(10*EX,EX), ta=new ScrollLabel(t));
            ta.setPreferredSize(dim(10*EX,EX));
        } else {
            ((JTextField)(ta=new JTextField(t))).setEditable(false);
        }
        setFG(0x6464ff,ta);

        if (0!=(opt&MI_HEADLINE)) ta.setFont(getFnt(14,false,Font.BOLD));
        else enlargeFont(ta,0.85);

        if (myComputer() && t.indexOf('<')>=0) putln(RED_WARNING,"menuInfo ",t);
        emptyBordr(EM, 0, 0, 0, ta).setOpaque(false);
        return ta;
    }

    private static String rmTrailingImport(String s) {
        final int i=s.indexOf("<i>"+HtmlDoc.PACKAGE);
        return i>0 ? s.substring(0,i) : s;
    }

    public static <T extends Object> JComponent pnl(T... oo) {
        final Object o0=get(0,oo);
        final boolean cnsew0=get(1,oo)==CNSEW;
        final JComponent p=
            idxOf(REMAINING_VSPC1,oo)>=0 ? new RemainingSpc(1) :
            idxOf(REMAINING_VSPC2,oo)>=0 ? new RemainingSpc(2) :
            cnsew0 ? (JComponent)oo[0] :
            new ChPanel();
        if (o0==VB || o0==VBHB || o0==VBPNL || o0==HB|| o0==HBL) p.setLayout(new BoxLayout(p, o0==HB || o0==HBL ? BoxLayout.X_AXIS:BoxLayout.Y_AXIS));
        if (cnsew0 || get(0,oo)==CNSEW) {
            p.setLayout(new BorderLayout());
            for(int i=cnsew0?2:1, k=0; i<oo.length; i++,k++) {
                final String cnsew=
                    k==0 ? BorderLayout.CENTER :
                    k==1 ? BorderLayout.NORTH :
                    k==2 ? BorderLayout.SOUTH :
                    k==3 ? BorderLayout.EAST :
                    BorderLayout.WEST;
                toContainr(0,oo[i],p, cnsew);
            }
        } else toContainr(0, oo,p, o0==VBPNL?"p" : o0==VBHB ? "b" : "");
        if (o0==HBL) p.add(Box.createHorizontalGlue());
        return p;
    }
    public static JComponent remainSpcS(Object o) { return pnl(CNSEW, o, null, new RemainingSpc(1)); }
    public static void remainSpcS(Object pnl, Object o) { pnl(pnl, CNSEW, o, null, new RemainingSpc(1)); }
    public static Component getPnl(Object o) {
        Component gcp=gcp(KEY_PANEL,o,Component.class);
        if (gcp==null) gcp=derefC(runCR(o,ChRunnable.RUN_GET_PANEL));
        try {
            final Object ret=
                gcp!=null ? gcp :
                o instanceof JPanel || o instanceof JScrollPane ? o :
                isInstncOf(HasPanel.class,o) ? ((HasPanel)o).getPanel(0) :
                o;
            return derefC(ret);
        } catch(Exception e){return null;}
    }
    /* <<< Containers <<< */
    /* ---------------------------------------- */
    /* >>> PrgParas >>> */
    public static Component sharedCtrlPnl(Object objectOrClass, boolean createOne) {
        final Object o= instanceFromClassOrCombo(objectOrClass);
        final HasSharedControlPanel hasSC=deref(o,HasSharedControlPanel.class);
        Object cp=hasSC==null?null:hasSC.getSharedControlPanel();
        while( !(cp instanceof Component) && isAssignblFrm(HasSharedControlPanel.class, cp)) cp=((HasSharedControlPanel)cp).getSharedControlPanel();
        HasPrgParas hasPP=deref(o,HasPrgParas.class);
        if (hasPP==null) hasPP=deref(cp,HasPrgParas.class);
        Component pan=derefC(cp);
        if (pan==null && hasPP!=null) {
            final PrgParas paras=hasPP.getPrgParas();
            final PrgParasGUI gui=paras==null?null : paras.getGUI(createOne);
            pan=gui==null?null: gui.getPanel();
        }
        if (pan==null && isAssignblFrm(HasNativeExec.class,o)) pan=sharedCtrlPnl( ((HasNativeExec)o).getNativeExec(false), createOne);
        return pan;
    }
    private static Map<Object,Object> mapInst=new WeakHashMap();
    public static boolean isClassOrPC(Object c) { return c  instanceof Class || c instanceof ProxyClass; }
    private static Object instanceFromClassOrCombo(Object o) {
        if (o==null) return null;
        Object inst=o;
        if (isClassOrPC(o))  {
            inst=mapInst.get(o);
            if (inst==null) mapInst.put(o,inst=mkInstance(o));
        } else if (o instanceof ChCombo) inst=((ChCombo)o).instanceOfSelectedClass(null);
        return inst;
    }
    public static boolean hasSharedControlPanel(Object objectOrClass) {
        if (isAssignblFrm(HasSharedControlPanel.class,objectOrClass) || isAssignblFrm(HasPrgParas.class, objectOrClass)) return true;
        final Object o=instanceFromClassOrCombo(objectOrClass);
        final HasPrgParas hasPP=deref(o,HasPrgParas.class);
        if (hasPP!=null && hasPP.getPrgParas()!=null && hasPP.getPrgParas().hasGUI()) return true;
        if (isAssignblFrm(HasNativeExec.class, o)) return hasSharedControlPanel( ((HasNativeExec)o).getNativeExec(false));
        return false;
    }
    public static void setSharedParas(Object shared, Object inst) {
        if (inst!=null && shared!=null) {
            if (isAssignblFrm(NeedsSharedInstance.class, inst)) ((NeedsSharedInstance)inst).setSharedInstance(shared);

            if (isAssignblFrm(HasNativeExec.class, inst) && isAssignblFrm(HasNativeExec.class, shared) ) {
                setSharedParas(((HasNativeExec)shared).getNativeExec(false), ((HasNativeExec)inst).getNativeExec(false));
            }
        }
    }
    /* <<< HasPrgParas <<< */
    /* ---------------------------------------- */
    /* >>> getControlPanel >>> */
    public static Component ctrlPnl(Object o) { return derefC(_ctrlPnl(true,o)); }
    public static boolean hasCtrlPnl(Object o) { return null!=_ctrlPnl(false,o); }
    private static Object _ctrlPnl(boolean real, Object o) {
        if (o==null) return null;
        Object pan=null;
        if (isAssignblFrm(HasControlPanel.class, o)) {
             Object cp=((HasControlPanel)o).getControlPanel(real);
             while( !(cp instanceof Component) && isAssignblFrm(HasControlPanel.class, cp)) cp=((HasControlPanel)cp).getControlPanel(real);
             pan=deref(cp);
        }
        if (pan==null && isAssignblFrm(HasNativeExec.class, o)) {
            pan=_ctrlPnl(real,((HasNativeExec)o).getNativeExec(false));
        }
        if (pan==null) {
            final Object deleg=gcp(KEY_CTRL_PNL,o);
            return deleg instanceof Component ? (Component)deleg : _ctrlPnl(real,deleg);
        }
        return pan;
    }
    public static boolean shwCtrlPnl(long options, String title, Object o) {
        final Object pnl=ctrlPnl(o);
        if (pnl!=null) {
            ChFrame.frame(orS(title,"Control panel"), pnl, options).shw(options);
            return true;
        } else return false;
    }
    /* <<< getControlPanel <<< */
    /* ---------------------------------------- */
    /* >>> EventListener >>> */
    private final static int
        MOMOLI_DRAG_MENU_ITEM=4, MOMOLI_AUTO_SCROLL=6, MOMOLI_SBH=7, MOMOLI_SBV=8, MOMOLI_SBB=9, MOMOLI_DRAG_WIN=10, MOMOLI_PANEL_UNDOCK=11,
        ALI_RADIO=22, ALI_REPAINT_UNDOCKED=23,
        MOLI_OVER_PRINT=34,
        MOLI_RESCUE_MENUBAR=37, MOLI_REVALIDATE1=39,
        MOLI_UNDOCK_UPDATE=40,
        KLI_CTRLW_DISPOSE=41,
        WLI_SCROLL=50,
        MOLI_TIP_DELAY=51;
    public final static int
        MOLI_LEFT_TO_MIDDLE=31,
        MOLI_REFRESH_TIP=32,
        MOLI_REPAINT_ON_ENTER_AND_EXIT=33,
        KLI_SHIFT_TIP=44,
        KLI_ALWAYS_ON_TOP=45, MOLI_TAB=38, FOLI_FOCUS=35,
        MOLI_SB_UNLOCK=36,
        COMPLI_SHORT_OR_LONG=37,
        DO_NOTHING_ChRunnable=99,
        ADD_MOLI_CHILDS=80000000;

    private EvAdapter _li;
    private EvAdapter listener() { if (_li==null) _li=new EvAdapter(this); return _li; }
    public static EvAdapter evLstnr(int id) { return instance(id).listener();}
    private final static ChUtils[] _inst=new ChUtils[100];
    private static ChUtils instance(int id) { if (_inst[id]==null) _inst[id]=new ChUtils(); return _inst[id];}
    public static void addMoli(int id, Component c) {
        evLstnr(id&0xff).addTo("m",c);
        if (0!=(id&ADD_MOLI_CHILDS)) {
            for(Component child : childs(c)) addMoli(id,child);
        }
    }
    public static Component setUndockble(Component c) { return setUndockble(false,c); }
    public static Component setUndockble(boolean recursive, Component c) {
        evLstnr(MOMOLI_DRAG_MENU_ITEM).addTo("Mm",c);
        if (c instanceof AbstractButton) evLstnr(ALI_REPAINT_UNDOCKED).addTo("a", c);
        pcp(KOPT_UNDOCKABLE,"",c);
        if (recursive) {
            for(Component child : childs(c))  setUndockble(true, child);
        }
        return c;
    }
    private static Component[] _noComp;
    public static Component[] childs(Component c) {
        if (_noComp==null) _noComp=new Component[0];
        final Object oo[]=
            c instanceof JViewport ? new Component[]{((JViewport)c).getView()} :
            c instanceof ChTabPane ? ((ChTabPane)c).tabComponents() :
            c instanceof JMenu ? ((JMenu)c).getMenuComponents() :
            c instanceof Container ? ((Container)c).getComponents() :
            null;
        return oo==null ? _noComp : oo instanceof Component[] ? (Component[])oo : derefArray(oo, Component.class);
    }

    public static void setAutoscr(JComponent c) { evLstnr(MOMOLI_AUTO_SCROLL).addTo("M", c); c.setAutoscrolls(true);}
    public static void setDragMovesWindow(boolean recursive, Component p) {
        if (p==null) return;
        if (p.getClass().getName().startsWith(_CC)) pcp(KOPT_DRAGS_PARWINDOW,"",p);
        else if (!(p instanceof JSlider || p instanceof Adjustable)) {
            evLstnr(MOMOLI_DRAG_WIN).addTo("M", p);
        }
        if (recursive) for(Component c  : childs(p)) setDragMovesWindow(true,c);
    }
    public static void revalidate_whenMouseEnter1(boolean childs, Component c) {
        if (childs)  for(Component child : childs(c))  revalidate_whenMouseEnter1(true, child);
        if (!(c instanceof JPanel)) addMoli(MOLI_REVALIDATE1,c);
    }
    public static void scrollByWheel(Component c) { evLstnr(WLI_SCROLL).addTo("w", c); }
    private static Object _closeWithCtxtMenu;
    public static void closeWithCtxtMenu(Object o) { _closeWithCtxtMenu=o; }
    private static boolean _popupShown;
    private static EvLi _chEvLi;
    private static long _popupInvisibleWhen;
    public static long popupInvisibleWhen() { return _popupInvisibleWhen;}
    public static EvLi chEvLi() { if (_chEvLi==null) _chEvLi=new EvLi(); return _chEvLi; }
    public static class EvLi implements ListSelectionListener, TreeSelectionListener, PopupMenuListener {
        public void valueChanged(ListSelectionEvent ev) { ChUtils.selectionChanged(ev); }
        public void popupMenuCanceled(PopupMenuEvent e) { ChUtils._popupShown=false; }
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            if (gcp(KOPT_POPUP_MENU,e.getSource())!=null) {
                _popupInvisibleWhen=currentTimeMillis();
                ChUtils._popupShown=false;
                handleActEvt(gcp(ACTION_BECOME_INVISIBLE,e.getSource()), ACTION_BECOME_INVISIBLE, 0);
                setVisblC(false, _closeWithCtxtMenu,333);
                _closeWithCtxtMenu=null;
            }
        }
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            _popupInvisibleWhen=currentTimeMillis();
            ChUtils._popupShown=true;
            ChUtils.callSetEnabled(e);
        }
        public void valueChanged(TreeSelectionEvent ev) { ChUtils.selectionChanged(ev);}
    }
     static void callSetEnabled(EventObject e) {
         for(Component c : childs(derefC(e.getSource()))) c.setEnabled(c.isEnabled());
     }
    /* <<< EventListener <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    private static Component _compAtMouse;
    private static MouseEvent _lastMEvt, _lastMEvtMove;
    public static Component compAtMouse() { return _compAtMouse;}
    public static MouseEvent lstMouseEvt() { return _lastMEvt;}
    private static void lstMouseEvt(Object ev) {
        if (ev instanceof MouseEvent) {
            _lastMEvt=(MouseEvent)ev;
            if (((AWTEvent)ev).getID()==MOUSE_MOVED) _lastMEvtMove=_lastMEvt;
        }
    }
    public static int buttn(AWTEvent ev) {
        return ev instanceof MouseEvent ? ((MouseEvent)ev).getButton() : -1;
    }
    public static int modifrs(Object event) {
        if (event instanceof Integer) return atoi(event);
        LAFChooser.mayRestoreLaf();
        lstMouseEvt(event);
        return
            event instanceof InputEvent ? ((InputEvent)event).getModifiers() :
            event instanceof ActionEvent ? ((ActionEvent)event).getModifiers() :
            0;
    }
    public static Object evtSrc(Object objOrEv) {
        return objOrEv instanceof EventObject ?  ((EventObject)objOrEv).getSource() : deref(objOrEv);
    }

    public static int wheelRotation(AWTEvent ev) {
        return ev instanceof MouseWheelEvent ? ((MouseWheelEvent)ev).getWheelRotation() : 0;
    }
    public static String actionCommand(AWTEvent ev) {
        if (!(ev instanceof ActionEvent)) return null;
        final String cmd=((ActionEvent)ev).getActionCommand();
        assert ((AWTEvent)ev).getID()==ActionEvent.ACTION_PERFORMED;
        return toStrgN(cmd);
    }
    public static char keyChar(AWTEvent ev) { return ev instanceof KeyEvent ? ((KeyEvent)ev).getKeyChar():0;}
    public static int keyCode(AWTEvent ev) {
        if (ev instanceof KeyEvent) {
            final char c=((KeyEvent)ev).getKeyChar();
            return c=='+' ? VK_PLUS : c=='-' ? VK_MINUS :  c=='*' ? VK_ASTERISK : ((KeyEvent)ev).getKeyCode();
        }
        return 0;
    }
    public static int hght(Object o) {
        return
            o instanceof Dimension ? ((Dimension)o).height :
            o instanceof Rectangle ? ((Rectangle)o).height :
            o instanceof Component ? ((Component)o).getHeight() :
            o instanceof Image ? ((Image)o).getHeight(ChIcon.imageObserver()) :
            o instanceof ImageIcon ? ((ImageIcon)o).getIconHeight() :
            0;
    }
    public static int wdth(Object o) {
        return
            o instanceof Dimension ? ((Dimension)o).width :
            o instanceof Rectangle ? ((Rectangle)o).width :
            o instanceof Component ? ((Component)o).getWidth() :
            o instanceof Image ? ((Image)o).getWidth(ChIcon.imageObserver()) :
            o instanceof ImageIcon ? ((ImageIcon)o).getIconWidth() :
            0;
    }
    public static int x(Object o) {
        if (o instanceof Component) return ((Component)o).getX();
        if (o instanceof AWTEvent) return o instanceof MouseEvent ? ((MouseEvent)o).getX() : 0;
        if (o instanceof Point) return ((Point)o).x;
        if (o instanceof Rectangle) return ((Rectangle)o).x;
        if (o instanceof Insets) return ((Insets)o).left;
        if (o!=null && !(o instanceof Shape)) assrt();
        return 0;
    }
    public static int y(Object o) {
        if (o instanceof Component) return ((Component)o).getY();
        if (o instanceof AWTEvent) return o instanceof MouseEvent ? ((MouseEvent)o).getY() : 0;
        if (o instanceof Point) return ((Point)o).y;
        if (o instanceof Rectangle) return ((Rectangle)o).y;
        if (o instanceof Insets) return ((Insets)o).top;
        if (o!=null && !(o instanceof Shape)) assrt();
        return 0;
    }
    public static int x2(Rectangle r) { return r!=null?r.x+r.width:0;}
    public static int y2(Rectangle r) { return r!=null?r.y+r.height:0;}
    public static Point setXY(int x, int y, Point p) { if (p!=null) { p.x=x; p.y=y; } return p;}
    public static Point point(Object ev) {
        return ev instanceof Point ? (Point)ev : ev instanceof MouseEvent ? ((MouseEvent)ev).getPoint() : null;
    }
    public static boolean doubleClck(AWTEvent ev) { return ev instanceof MouseEvent && ev.getID()==MOUSE_CLICKED && ((MouseEvent)ev).getClickCount()>1; }
    private static Point pointScreen(AWTEvent ev) {
        final Point p=new Point();
        if (ev instanceof MouseEvent) SwingUtilities.convertPointToScreen(setXY(x(ev),y(ev),p), (Component)ev.getSource());
        else setXY(0,0,p);
        return p;
    }
    public static int xScreen(AWTEvent ev) { return pointScreen(ev).x; }
    public static int yScreen(AWTEvent ev) { return pointScreen(ev).y; }
    private static int _ev_masks[];
    public static int shortCutMask() {
        /* Unix: 2 Mac: 4 (META_MASK)  Toolkit */
        if (dtkt()==null) return 2;
        if (_ev_masks==null) {
            final int shortCut=dtkt().getMenuShortcutKeyMask();
            _ev_masks=new int[]{shortCut};
        }
        return _ev_masks[0];
    }
    static AWTEvent ctrl2apple(AWTEvent ev) {
        if (isMac()) {
            final int kcode=keyCode(ev), modi=modifrs(ev);
            if ((kcode==VK_C||kcode==VK_X||kcode==VK_V| kcode==VK_A) && 0==(modi&(SHIFT_MASK|ALT_MASK)) && 0!=(modi&(isMacLaf()?CTRL_MASK:shortCutMask()))) {
                try {
                    return new KeyEvent((Component)ev.getSource(),ev.getID(),System.currentTimeMillis(), isMacLaf()?META_MASK:CTRL_MASK,kcode,keyChar(ev));
                } catch(IllegalArgumentException e) {}
            }
        }
        return ev;
    }

    private static int _typedNum=MIN_INT;
    public static int typedNumber(AWTEvent ev) {
        final int id=ev.getID(), kcode=keyCode(ev);
        if (id==MOUSE_PRESSED || id==MOUSE_RELEASED || id==FOCUS_GAINED || id==FOCUS_LOST) {
            _typedNum=MIN_INT;
        } else if (id==KeyEvent.KEY_PRESSED) {
            if (!is(DIGT,kcode) && kcode!=VK_SHIFT) _typedNum=MIN_INT;
            if (is(DIGT,kcode)) {
                if (_typedNum==MIN_INT) _typedNum=0;
                _typedNum=_typedNum*10+kcode-'0';
            }
        }
        return _typedNum;
    }

    public static String idAsStrg(AWTEvent ev) {
        if (ev==null) return null;
        final int id=ev.getID();
        final Class c=ev.getClass();
        final String pfx=
            c==MouseEvent.class ? "MOUSE_" :
            c==KeyEvent.class ? "KEY_" :
            null;
        final Map m=pfx==null?null : finalStaticInts(c, pfx);
        return m==null?null:(String)m.get(intObjct(id));
    }
    public static boolean  isCtrl(AWTEvent ev) { return (modifrs(ev)&CTRL_MASK)!=0;}
    public static boolean isShift(AWTEvent ev) { return (modifrs(ev)&SHIFT_MASK)!=0;}
    public static boolean isShrtCut(AWTEvent ev) { return (modifrs(ev)&(CTRL_MASK|shortCutMask()))!=0;}
    public static boolean isTogglSelect(AWTEvent ev) { return isShrtCut(ev);}

    public static int scaleUpDown(AWTEvent ev) {
        if (!isShrtCut(ev)) return 0;
        final int r=wheelRotation(ev), c=keyCode(ev);
        if (r!=0) return r>0==isSelctd(BUTTN[TOG_CTRL_WHEEL]) ?1:-1;
        return c==VK_ADD || c==VK_PLUS  ? 1 : c==VK_MINUS ? -1 : 0;
    }
    private Point _dragFromS, _dragFrom;
    private static boolean _undocking;

    public void processEv(AWTEvent ev) {

        final Object q=ev.getSource();
        final MouseEvent mev=deref(ev, MouseEvent.class);
        final JComponent jcq=derefJC(q);
        final Component cq=derefJC(q);
        final int
            ID=ev.getID(), kc=keyCode(ev),
            x=x(ev), y=y(ev),
            modi=modifrs(ev),
            w=wdth(jcq), h=hght(jcq),
            inst=idxOf(this, _inst);
        final boolean ctrl=isCtrl(ev), shift=isShift(ev), isSelctd=isSelctd(q), shortCut=isShrtCut(ev);
        if (inst==MOLI_TAB && ID==MOUSE_CLICKED && buttn(ev)==1) {
            final JComponent comp=gcp(KEY_tabComponent,q, JComponent.class);
            final JTabbedPane tp=gcp(KEY_JTabbedPane,q, JTabbedPane.class);
            if (ChRenderer.inButClose(q, x, y)) {
                clos(closeOpts(comp)|(ctrl?0:CLOSE_ASK),comp);
            } else if (tp!=null) {
                tp.setSelectedComponent(comp);
                if (null!=gcp(KEY_NEEDS_REVALIDATE,tp)) ChDelay.revalidate(tp,99);
            }
        }
        if (inst==FOLI_FOCUS) {
            if (ID==FOCUS_GAINED) _hasFocus=wref(q);
            if (ID==FOCUS_LOST) pcp(KEY_FOCUS_LOST_WHEN,longObjct(currentTimeMillis()),q);
        }
        if (ID==MOUSE_DRAGGED && inst==MOMOLI_PANEL_UNDOCK && !_undocking && (x<0 || y<0 || x>w || y>h) && gcp(KEY_undockableComponent,q)!=null) {
            _undocking=true;
            pnlUndock(gcp(KEY_undockableComponent,q),xScreen(ev)-EM,yScreen(ev)-EM, !ctrl);
        }
        if (ID==MOUSE_MOVED) _undocking=false;
        if (inst==KLI_ALWAYS_ON_TOP) setAotEv(ev);
            if (inst==MOLI_SB_UNLOCK && ID==MOUSE_PRESSED) ChScrollBar.lock(0, deref(parentC(q), JScrollBar.class));
        if (cq!=null && (ID==MOUSE_ENTERED||ID==MOUSE_EXITED)) {
            if (inst==MOLI_REFRESH_TIP) { rtt(jcq); ToolTipManager.sharedInstance().setEnabled(true); }
            if (inst==MOLI_REPAINT_ON_ENTER_AND_EXIT) repaintC(q);
            if (inst==MOLI_OVER_PRINT) {
                final Object para[]=gcp(this,q,Object[].class), strg=para[0];
                final int dx=atoi(para[1]);
                final Component comp=(Component)deref(para[3]);
                if (comp!=null && strg!=null) {
                    final Point p=SwingUtilities.convertPoint(cq,  x,  y, comp);
                    if (p.y>=0 && p.y<=comp.getHeight()) {
                        final Graphics g=comp.getGraphics();
                        final int compW=comp.getWidth();
                        final Font f=(Font)para[4];
                        if (g!=null) {
                            if (f!=null) g.setFont(f);
                            int dy=atoi(para[2]);
                            for(Object o:oo(strg)) {
                                final String s=toStrg(o);
                                if (s==null) continue;
                                final int sw=strgWidth(g,toStrg(s));
                                final int X=dx+ (dx<0?compW-sw:0);
                                g.setColor(Color.WHITE);
                                fillBigRect(g,X,dy,sw,charH(g));
                                g.setColor(Color.BLUE);
                                g.drawString(s, X,   charA(g)+dy);
                                dy+=charH(g);
                            }
                        }
                    } else repaintC(comp);
                }
            }
            if (inst==MOLI_TIP_DELAY) setTipTiming(ID==MOUSE_EXITED ? -1:atoi(gcp(this,q)),0);
        }
        if (cq!=null) {
            if (ID==MOUSE_ENTERED && inst==MOLI_REVALIDATE1) {
                cq.removeMouseListener(listener());
                if (inst==MOLI_REVALIDATE1) revalAndRepaintC(cq);
            }
            if (inst==MOLI_LEFT_TO_MIDDLE && ID==MOUSE_PRESSED && buttn(ev)==1) {
                processEvt(new MouseEvent(cq, ID, mev.getWhen(), 0, x,  y, 1,false,BUTTON2),q);
            }
            if (ID==MOUSE_MOVED) {
                _dragFromS=_dragFrom=null;
                Point lp=gcp(KEY_DRAG,cq,Point.class);
                if (lp==null) pcp(KEY_DRAG,lp=new Point(),cq);
                SwingUtilities.convertPointToScreen(setXY(x,y,lp),cq);
            }
            if ((ID==MOUSE_CLICKED||ID==MOUSE_PRESSED) && inst==KLI_CTRLW_DISPOSE) cq.requestFocus();
            if (inst==MOMOLI_DRAG_MENU_ITEM /*strEquls(_CC,clasNam(q),0) &&*/) {
                final int w1=w>0?w:prefW(q); /* Mac menu */
                if ((ID==MOUSE_DRAGGED || ID==MOUSE_EXITED && 0!=(modi&BUTTON1_MASK) ) && (x<1||x>w1-2)) undockItm(false, cq, ev, jlUndock01()[1]);
            }
            if (ID==MOUSE_PRESSED && parentC(false,cq,JMenuBar.class)!=null) undockSetTarget(null);
            if (ID==KEY_PRESSED && inst==KLI_SHIFT_TIP && kc==VK_SHIFT) toolTipFakeEv();
            if (ID==KEY_PRESSED && inst==KLI_CTRLW_DISPOSE) {
                final Object[] pp=(Object[]) gcpa(this, q);
                final int opt=atoi(pp[0]);
                for(int i=1; i<pp.length; i++) {
                    Object toBeClosed= get(i,pp);
                    if (toBeClosed==REP_PARENT_WINDOW) toBeClosed=parentWndw(jcq);
                    if (toBeClosed==null) continue;
                    if ((opt&CLOSE_CtrlW)!=0 && kc==VK_W && (shortCut||ctrl)    ||
                        (opt&CLOSE_ESC)  !=0 && kc==VK_ESCAPE) {
                        clos(opt,toBeClosed);
                    }
                }
            }
        }
        if (inst==MOMOLI_DRAG_WIN) dragWindow(ev);
        if ((ID==ComponentEvent.COMPONENT_RESIZED || ID==ComponentEvent.COMPONENT_SHOWN) && inst==COMPLI_SHORT_OR_LONG) {
            adaptSmallSize(jcq);
        }
        if (ID==MOUSE_DRAGGED) {
            final Container container=q instanceof Container ? (Container)q : null;
            final boolean alt=0!=(modi&ALT_MASK);
            if (inst==MOMOLI_AUTO_SCROLL && jcq!=null) {
                setRect(x,y,1,1, _rect);
                jcq.scrollRectToVisible(_rect);
            }
            if (inst==MOMOLI_SBH || inst==MOMOLI_SBV || inst==MOMOLI_SBB) {
                final Object spOrSb=gcp(this,q);
                final Point lp=gcp(KEY_DRAG, container, Point.class), np=new Point(x,y);
                pcp(KEY_DRAG,np,container);
                SwingUtilities.convertPointToScreen(np,container);
                if (_dragFromS==null) { _dragFromS=np; _dragFrom=new Point(x,y); }
                final ChRunnable r=gcp(ChRunnable.RUN_IS_DRAG4XY,jcq, ChRunnable.class);
                if (r!=null && isTrue(r.run(ChRunnable.RUN_IS_DRAG4XY, _dragFrom))) return;
                for(int isH=0; isH<2; isH++) {
                    if (!alt && gcp(KOPT_DRAG_ALTKEY[isH],q)!=null) continue;
                    final char hv=isH!=0?'H':'V';
                    if (hv=='V' && inst==MOMOLI_SBH) continue;
                    if (hv=='H' && inst==MOMOLI_SBV) continue;
                    final JScrollBar sb=getSB(hv, parentC(true, orO(spOrSb, q), JScrollPane.class));
                    if (sb==null || !(sb.isVisible() || gcp(KEY_SCROLL_EVEN_IF_INVISIBLE,sb)==null)) continue;
                    if (_dragFromS==np) pcp(this,intObjct(sb.getValue()),sb);
                    if( lp!=null) {
                        final int d=hv=='H' ? _dragFromS.x-np.x : _dragFromS.y-np.y;
                        sb.setValue( maxi(0,mini(sb.getMaximum(),atoi(gcp(this,sb))+ d)));
                    }
                }
            }
        }
        if (ID==MouseWheelEvent.MOUSE_WHEEL) {
            final int rotation=wheelRotation(ev);
            if (rotation!=0 && !ctrl) {
                if (inst==WLI_SCROLL) {
                    final JScrollPane sp= parentC(true,parentC(jcq),JScrollPane.class);
                    final JScrollBar sV=getSB('V',sp), sH=getSB('H',sp),  sb=!shift && isVisbl(sV) ? sV : sH!=null ? sH : sV;
                    if (sb!=null) {
                        final int d=maxi(sb.getUnitIncrement(), charH(jcq));
                        sb.setValue(sb.getValue() + (rotation<0 ? -d : d));
                    }
                }
            }
        }
        final String cmd=actionCommand(ev);
        if (ID==ActionEvent.ACTION_PERFORMED && q!=null) {
            final int iButton=idxOf(q, BUTTN);
            if (iButton>=0) pcp(KEY_BACKGROUND, null, q);
            if (iButton==TOG_CACHE) CacheResult.setEnabled(isSelctd);
            if (iButton==TOG_ANTIALIASING) ChFrame.repaintAll();
            if (iButton==TOG_JAVA_SRC) {
                for(int i=sze(_vSrcBut); --i>=0;) revalAndRepaintC(getRmNull(i,_vSrcBut));
            }
            if (iButton==TOG_EDTFTP) { if (isSelctd) new AnonymousFTPPROXY().proxyObject(); else mapFTP.clear(); }
            if (iButton==BUT_LOG) shwPopupMenu(popMenu(MENUITMS_LOG));
            if (iButton==BUT_PREV_MSG) ChMsg.showAllPreviousMsgs();
            if (iButton==BUT_LAF) {
                ChFrame f=gcp("F",q,ChFrame.class);
                if (f==null) pcp("F", f=new ChFrame(getTxt(q)).ad(LAFChooser.instance().panel()),q);
                f.shw();
            }
            if (iButton==BUT_TEST_PROXY) Web.showProxyInfo();
            if (cmd==ACTION_WINDOW_CLOSING && _undockEnabled==0 && q==parentWndw(_pnlUndockMsg)) _undockEnabled=-1;
            if (cmd=="E_UNDOCK") {
                _undockEnabled=isSelctd ? 1 : -1;
                if (isSelctd) runR(gcpClear("R",_pnlUndockMsg, Runnable.class));
                final Frame f=(Frame)parentWndw(_pnlUndockMsg);
                if (f!=null) f.dispose();
            }
            if (q==Customize.customize(Customize.testProxySelector)) setTxt(testProxySelector(), gcp(Web.KEY_TA_PROXY, Web.pnlProxyInfo()));
            if (inst==ALI_RADIO) {
                final Object o=gcp(this,q);
                for(int i=sze(o); --i>=0;) {
                    final AbstractButton but=get(i,o,AbstractButton.class);
                    if (but!=null && but!=q) but.setSelected(false);
                }
            }
            if (inst==ALI_REPAINT_UNDOCKED) repaintC(jlUndock01());
            if (cmd==ACTION_WINDOW_CLOSING) pnlDock(gcp(KEY_detach_component,q));
        }
        final ChJList jl=deref(q,ChJList.class);
        if (jl!=null && cntains(jl, jlUndock01()) && _undockW!=null) {
            final List v=jl.getList();
            final int N=sze(v);
            if (N==0) _undockW.setVisible(false);
            else {
                if (ID==MOUSE_ENTERED || ID==MOUSE_PRESSED) {
                    for(int i=N; --i>=0;) {
                        final Object depends=gcpa(KEY_DEPENDS_ON, get(i,v));
                        if (depends instanceof Object[] && get(0,depends)==null) {
                            v.remove(i);
                            jl.revalidate();
                        }
                    }
                }
                if (jl==jlUndock(false) && (ID==MOUSE_ENTERED||ID==MOUSE_EXITED) && 0==(modi&(BUTTON1_MASK|ALT_MASK|SHIFT_MASK|CTRL_MASK|META_MASK))) {
                    undockWinSize(ID==MOUSE_ENTERED, _undockW, jl);
                }
            }
        }
        final Object cntxtObj=gcp(KEY_CONTEXT_OBJECTS,q);
        if (jl!=null && mev!=null && (/*!ChUtils._popupShown && */cntxtObj!=null || q==jlUndock(false))) {
            if (ID==MOUSE_ENTERED && !_popupShown) {
                /* This is for undocked menu items. */
                /* A JList can refer to a collection via KEY_CONTEXT_OBJECTS. */
                /* On MOUSE_ENTERED this collection will replace the context objects */
                final ChJList jl2=deref(cntxtObj, ChJList.class);
                if (jl2!=null) {
                    /* Proteins or selections in extra list.
                       jl may be the drop target below  or jl==cntxtObj */
                    final Object oo[]=jl2.getSelectedValues();
                    setCntxtObj(sze(oo)>0?oo: jl2.getList());
                    final ChJList target=gcp(KEY_UNDOCK_TARGET,jl2, ChJList.class);
                    if (target!=null) undockEnableDisable(target);
                } else {
                    /* List of undocked Menu items */
                    if (cntxtObj!=jl) setCntxtObj(cntxtObj);
                    //updateAllNow(CHANGED_SELECTED_OBJECTS);
                }

            }
            final Object lastBut=gcp("L",jl);
            final int row=y/ICON_HEIGHT;
            final AbstractButton but=get(row,jl,AbstractButton.class);
            if (but!=null) {
                if (but!=lastBut) {
                    for(int iL=sze(vUndockTargets); --iL>=0;) {
                        final ChJList jList=get(iL,vUndockTargets,ChJList.class);
                        final List v=jList!=null ? jList.getList() : null;
                        for(int i=sze(v); --i>=0;) ChRenderer.setSmallButtonXc(row==i?-1:MIN_INT, v.get(i));
                    }
                }
                if (ID==MOUSE_CLICKED) {
                    if (gcp(KEY_CLOSE_OPT,but)!=null && x>w-EM && 1+y%ICON_HEIGHT<EM) {
                        remov(but,jl.getList());
                        if (jl==jlUndock(false)) undockWinSize(true, _undockW, jl);
                        else jl.revalidate();
                    } else {
                        ChButton cbut=gcp(KEY_CLONED_FROM,but,ChButton.class);
                        if (cbut==null) cbut=deref(but,ChButton.class);
                        if (cbut!=null) {
                            if (cbut.isToggle()) cbut.s(!cbut.s());
                            else cbut.processEv(new ActionEvent(cbut,ActionEvent.ACTION_PERFORMED,cmd,modi));
                        }
                        else but.doClick();
                    }
                    repaintC(jl);
                }

                if (ID==MOUSE_MOVED && but!=lastBut) {
                    pcp("L",but,jl);
                    final String tt, btt=but.getToolTipText(), t0=but.getText();
                    if (btt==null) tt=t0;
                    else {
                        final BA sb=new BA(btt);
                        if (strstr("<body>",sb)<0) sb.a("</body></html>").insert(0,"<html><body>");
                        if (t0!=null) {
                            String t=t0.indexOf("<body>")>0?t0.substring(t0.indexOf("<body>")+6) : t0;
                            if (t.indexOf("</body>")>0) t=t.substring(0,t.indexOf("</body>"));
                            sb.insert(strstr(STRSTR_AFTER,"<body>",sb),t);
                        }
                        tt=sb.toString();
                    }
                    jl.setToolTipText(tt);
                    rtt(jl);
                    jl.setCursor(cursr('D'));
                    jl.clearSelection();
                    repaintC(jl);
                }
                if (ID==MOUSE_EXITED || ID==MOUSE_MOVED) {
                    if (ChRenderer.setSmallButtonXc(ID==MOUSE_EXITED ? MIN_INT : w-EM-3,  but)) repaintC(jl);
                }
            }
        }
    }
    public static void closeOnKey(int option, Component evtSrc, Object toBeClosed) {
        final Object toBeC[]=oo(toBeClosed);
        if (evtSrc==null || countNotNull(toBeC)==0) return;
        final Object key=instance(KLI_CTRLW_DISPOSE), refs[]=new Object[toBeC.length+1];
        for(int i=toBeC.length; --i>=0;) refs[i+1]=toBeC[i]==REP_PARENT_WINDOW?toBeC[i] : wref(toBeC[i]);
        refs[0]=intObjct(option);
        pcp(key, refs, evtSrc);
        evLstnr(KLI_CTRLW_DISPOSE).addTo("km",evtSrc);
        if (0!=(option&CLOSE_CHILDS)) {
            for(Component child : childs(evtSrc)) closeOnKey(option, child, toBeClosed);
        }
    }
    public static Component rmAllListnrs(boolean childs, Component c) {
        if (c==null) return null;
        if (childs) for(Component child :childs(c)) rmAllListnrs(true,child);
        pcp(KOPT_DISABL_EVNTS,"",c);
        // if (c instanceof JTable || c instanceof JList) c.setEnabled(false);
        for(Class listenerType : new Class[]{MouseListener.class,MouseMotionListener.class,ActionListener.class, ChangeListener.class}) {
            for(EventListener l : c.getListeners(listenerType)) {
                invokeMthd(0L,"remove"+shrtClasNam(listenerType),c,oo(l));
            }
        }
        return c;
    }
    public static boolean isSimplClick(AWTEvent ev) {
        return ev!=null && ev.getID()==MOUSE_CLICKED &&
            0==(modifrs(ev)&(CTRL_MASK|SHIFT_MASK|ALT_MASK)) &&
            buttn(ev)==1;
    }
    public static boolean isPopupTrggr(boolean specific, AWTEvent ev) {
        if (!(ev instanceof MouseEvent)) return false;
        lstMouseEvt(ev);
        final int id=ev.getID();
        final boolean ctrl=isCtrl(ev), alt=0!=(modifrs(ev)&ALT_MASK), shift=isShift(ev), b3=buttn(ev)==3, prc=id==MOUSE_PRESSED || id==MOUSE_CLICKED || id==MOUSE_RELEASED;
        if (isMac() ?  b3&&!shift&&!ctrl&&!alt&&id==MOUSE_PRESSED : ((MouseEvent)ev).isPopupTrigger()) return true;
        if ( (specific ? id==MOUSE_PRESSED : prc) && alt && ctrl && !shift) return true;
        if (ctrl && !alt && isMacLaf())  ChMsg.savedMsg(readBytes(rscAsStream(0L,_CC, "Macintosh.html")),"MacMouse",0);
        return b3 && !specific && prc && !doubleClck(ev) && !shift;
    }
    /* <<< Event <<< */
     /* ---------------------------------------- */
    /* >>> ChRunnable >>> */
    public static void runR(Object r0) {
        final Object r=deref(r0);
        if  (!(r instanceof Collection) && isInstncOf(Runnable.class, r)) ((Runnable)r).run();
        else for(int i=0; i<szeVA(r); i++) runR(get(i,r));
    }
    public static Object runCR(Object runnable, String id) { return runCR(runnable, id,null);}
    public static Object runCR(Object runnable, String id, Object arg) {
        if (!(runnable instanceof Collection) && isInstncOf(ChRunnable.class, runnable)) return  ((ChRunnable)runnable).run(id,arg);
        Object ret=null;
        for(int i=0; i<szeVA(runnable); i++) {
            final Object o=runCR(get(i,runnable), id, arg);
            if (ret==null) ret=o;
        }
        return ret;

    }

    public Object run(String id,Object arg) {
        if (this==_inst[DO_NOTHING_ChRunnable]) return null;
        final Object argv[]=arg instanceof Object[] ? (Object[])arg:null;
        if (id=="BUTTON") argv[0]=_buttn((char)atoi(argv[1]), atol(argv[2]), (String)argv[3]);
        if (id=="setPrefSze") {
            final boolean reval=isTrue(argv[0]);
            final int wh[]=(int[])argv[1];
            final Component c=(Component)argv[2];
            setPrefSze(reval, wh[0], wh[1], c);
        }
        if (id=="ENABLE")  setEnabld(true, arg);
        if (id=="DISABLE") setEnabld(false,arg);
        if (id=="setIcn") setIcn((Icon)argv[0],argv[1]);
        if (id=="setTxt") setTxt((String)argv[0], argv[1]);

        if (id=="EDT") inEDT((Runnable)arg);
        if (id=="WAIT_MS") sleep(atoi(arg));
        if (id=="INVOKE_MTHD") {
            final Method m=(Method)argv[0];
            final Object instOrClass=argv[1], para[]=(Object[])argv[2], ret[]=(Object[])argv[3];
            final long opt=argv.length>4 ? atol(argv[4]) : RFLCT_PRINT_EXCEPTIONS;
            final Object r=invokeMthd(opt, m,instOrClass,para);
            if (ret!=null) ret[0]=r;
        }
        if (id=="WATCH_DOWNLOAD") {
            final File
                f0=(File)argv[0],
                ff[]={f0,file(f0+".gz"),file(f0+"TMP"),file(f0+".gzTMP")};
            final ChRunnable log=(ChRunnable)argv[1];
            String msg=null;
            final BA ba=new BA(99);
            while(argv[0]!=null && ff!=null) {
                for(File f : ff) {
                    if (sze(f)>0) {
                        _canDownload=true;
                        msg=ba.clr().a(f.getName()).a(' ').format10(sze(f),10).a(" bytes downloaded").toString();
                        break;
                    }
                }
                if (msg==null && f0!=null) msg="Downloading: "+f0.getName();
                sleep(222);
                sayDownloading(log, msg!=null ? msg+(sze(f0)>0 ? " done" : " ...") : null);
            }
        }
        if (id=="runnable") for(Runnable r : (Runnable[])arg) runR(r);
        if (id=="runnableEdtLater") { final Runnable rr[]=(Runnable[])arg; inEdtLater(sze(rr)==1 ? rr[0] : thrdRRR(rr)); }
        if (id=="thrdBG") startThrd(deref(arg,Runnable.class));
        if (id=="MARCHING_ANT") marchingAnts();
        if (id=="CURR_VERS") putln(readBytes("http://www-intern.charite.de/bioinf/strap/currentVersion?n="+arg+"&os="+systProprty(SYSP_OS_NAME)+systProprty(SYSP_OS_VERSION)));
        if (id=="POST_BROWSER") {
            final String sUrl=(String)argv[0];
            final BA postData=(BA)argv[1];
            final File fHtml=newTmpFile(".html");
            final BA ba=readBytes(Web.getServerResponseAsStream(0L, sUrl,postData));
            if (ba!=null) {
                Web.insertBaseTag(url(sUrl),fHtml,ba);
                wrte(fHtml,ba);
                visitURL(fHtml,0);
            }
        }
        if (id=="getServerResponseTime") {
            final String url=(String)argv[0];
            final int[] time=(int[])argv[1];
            final long t0=currentTimeMillis();
            final InputStream is=inStreamT(99,url,0L);
            if (is!=null) {
                time[0]=(int)(currentTimeMillis()-t0);
                closeStrm(is);
            }
        }
        if (id=="addHelpSource") addHelpSource((String)argv[0], (ChTabPane)argv[1], argv[2]==TRUE);
        if (id=="rmChilds") rmAllChilds((Component)arg);
        if (id=="INPUT_STREAM") if ((argv[2]=inStream((String)argv[0], atol(argv[1])))==null) argv[2]="";
        if (id=="LM_HTTP") if ( (argv[1]=lastModifiedMsHttpHeader((URL)argv[0]))==null) argv[1]="";
        //if (id=="gHF_") argv[2]=getHeaderFld((String)argv[0],(URL)argv[1]);
        if (id=="gHF") {
            try {
                final java.net.URLConnection c=((URL)argv[1]).openConnection();
                argv[3]=c;
                if (c!=null) {
                    c.setUseCaches(false);
                    argv[2]=c.getHeaderField((String)argv[0]);
                }
            } catch(Exception ex) { putln("caught gHF ", ex);}
        }
        if (id=="EVENT") {
            final Object[] oo=(Object[])arg;
            final Object vListener= oo[0];
            final EventObject ev=(EventObject) oo[1];
            if (vListener==null || ev==null) return null;
            for(int i=0; i<sze(vListener); i++) {
                final Object listener=get(i,vListener);
                if (listener instanceof ActionListener && ev instanceof ActionEvent) ((ActionListener)listener).actionPerformed((ActionEvent)ev);
                if (listener instanceof ChangeListener && ev instanceof ChangeEvent) ((ChangeListener)listener).stateChanged((ChangeEvent)ev);
            }
        }
        return null;
    }
    /* <<< ChRunnable <<< */
    /* ---------------------------------------- */
    /* >>> ImageIcon >>> */
    final static String IC_ERROR_ICON="CU$$EI";
    private static ImageIcon _errIc;
    final static Map<String,ImageIcon> mapIcon=new HashMap();

    public static ImageIcon iicon(String id) {
        String s=id;
        if (s!=null && s.startsWith("IC_")) {
            try {
                s=(String)ChConstants.class.getField(s).get(null);
            } catch(Exception ex) { debugExit(RED_ERROR, "getField ",s);}
        }

        if (sze(s)==0 || s==ERROR_STRING || dtkt()==null) return null;

        if (_errIc==null) _errIc=new ImageIcon();

        if (s==IC_ERROR_ICON) return _errIc;
        ImageIcon icon=mapIcon.get(s);
        if (icon==null) {
            final int esc=s.indexOf('\u001B');
            if (esc>0) {
                int fnSize=atoi(s);
                if (fnSize<3) fnSize=12;
                final BA ba=new BA(s.substring(esc));
                final long[] escape=ba.getAnsiEscapes();
                final int escapeL=ba.countAnsiEscapes();
                final AnsiEscape attr=new AnsiEscape();
                for(int i=0;i<escapeL;i++) AnsiEscape.inferIntoAttribute(escape[i],attr);
                final Color fg=attr.fg(),bg=attr.bg();

                final Font font=getFnt(fnSize,true,Font.BOLD);
                icon=new ImageIcon(ChIcon.text2image(ba.toString(), font, fg!=null?fg:Color.BLACK,bg));
            } else if (s.startsWith("javax/")) {
                final URL u=rscAsURL(0L, s);
                icon=u==null?null: new ImageIcon(u);
            } else {
                icon=ChIcon.findIcon(s);
            }
            mapIcon.put(s,icon!=null ? icon : _errIc);
        }
        return icon!=_errIc ? icon : null;
    }
    public static Image img(Object o) {
        return o instanceof ImageIcon ? ((ImageIcon)o).getImage() :
            o instanceof Image ? (Image)o :
            o instanceof String ? img(iicon((String)o)) :
            o instanceof HasImage ? ((HasImage)o).getImage() :
            null;

    }
    /* <<< ImageIcon <<< */
    /* ---------------------------------------- */
    /* >>> Color >>> */
    public final static int FG_SELECTED=0xFFffFF, BG_SELECTED=0xFF;
    private static Color _bgSelected;
    private final static Color[] _blueYellowRed=new Color[10000], _color=new Color[1000];
    public static Color C(int rgb) { return C(rgb, 255); }
    public static Color C(int rgb, int a) {
        final int rgba=rgb|(a<<24);
        switch(rgba) {
        case 0xFF000000:return Color.BLACK;
        case 0xffFFffFF: return Color.WHITE;
        case 0xff808080: return Color.GRAY;
        case 0xff0000FF: return Color.BLUE;
        case 0xff00FF00: return Color.GREEN;
        case 0xffFF0000: return Color.RED;
        case 0xffFFAFAF: return Color.PINK;
        case 0xff00FFFF: return Color.CYAN;
        case 0xffFF00FF: return Color.MAGENTA;
        }
        int i=(rgba>=0?rgba:-rgba)%_color.length;
        if (i<0) i=0;
        Color c=_color[i];
        if (c==null || c.getRGB()!=rgba) {
            _color[i]=c=new Color(rgba,true);
            //puts(ANSI_CYAN+" color "+ANSI_RESET);
        }
        return c;
    }

    public static Color bgSelected() {
        Color c=_bgSelected;
        if (c==null) {
            c=deref(UIManager.getDefaults().get("List.selectionBackground"), Color.class);
            if (c==null) c=C(0xFF);
            final int rgb=c.getRGB();
            if (((rgb>>>16)&255)+((rgb>>>8)&255)+(rgb&255)>0xFF) c=C(0xFF); /* too bright */
            _bgSelected=c;
        }
        return c;
    }

    public static JComponent setBG(Color bg, JComponent comp) {
        if (bg!=null && comp!=null) {
            if (!bg.equals(comp.getBackground())) comp.setBackground(bg);
        }
        return comp;
    }
    public static Object setFG(int rgb, Object comp) { setFgBg(false, rgb,comp);  return comp;}
    public static Object setBG(int rgb, Object comp) { setFgBg(true, rgb,comp); return comp;}
    public static void setFgBg(boolean bg, int rgb, Object comp) {
        if (comp instanceof Component) {
            final Component c=(Component)comp;
            final Color col=bg?c.getBackground() : c.getForeground();
            if (col==null || (0xFFffFF&col.getRGB())!=rgb) {
                if (isEDT()) {
                    if (bg) c.setBackground(C(rgb));
                    else c.setForeground(C(rgb));
                } else ChDelay.fgBg(bg, comp, C(rgb),10);
            }
        }
    }
    public static void setFgBgMb(JMenuBar mb, int fgRgb, int bgRgb) {
        if (mb==null) return;
        final String key="CC$$mbbg";
        Color origBG=gcp(key,mb, Color.class);
        if (origBG==null) pcp(key, origBG=mb.getBackground(), mb);
        final Color fg=C(fgRgb), bg=C(bgRgb);
        mb.setForeground(fg);
        mb.setBackground(bgRgb==DEFAULT_BACKGROUND && origBG!=null ? origBG : bg);
        for(Component c : childs(mb)) {
            c.setForeground(fg);
            if (c instanceof JMenu) ((JMenu)c).setContentAreaFilled(false);
        }
    }
    public static Color colr(Object o0) {
        Object o=gcp(KEY_COLOR,o0);
        if (o==null) o=o0;
        return
            o instanceof Colored ? ((Colored)o).getColor() :
            o instanceof Color ? (Color)o :
            o instanceof Integer ? C(atoi(o)) :
            null;
    }
    public static int rgb(Color c) { return c!=null ? c.getRGB() : 0xFFffFF;}
    public static int rgbDarkr(int rgb) {
        final int
            r0=(rgb>>16)&255, g0=(rgb>>8)&255, b0=rgb&255,
            r=r0*45875/65536, g=g0*45875/65536, b=b0*45875/65536;
        return  (r<<16)|(g<<8)|b;
    }

    private final static Object _mapDarkerr[]={null};
    public static Color[] darkerColors(Color cc0[]) {
        if (cc0==null) return null;
        final Map<Color[],Color[]> m=fromSoftRef(0,_mapDarkerr,IdentityHashMap.class);
        Color[] cc=m.get(cc0);
        if (cc==null) {
            m.put(cc0, cc=new Color[cc0.length]);
            for(int i=cc0.length; --i>=0;) cc[i]=C(rgbDarkr(rgb(cc0[i])));
        }
        return cc;
    }
    public static Color str2color(Object s) { return str2color(s, 0, MAX_INT); }
    public static Color str2color(Object s, int from, int to) {
        final int L=sze(s), noSpc=nxt(-SPC,s, from, L);
        if (chrAt(noSpc,s)=='#') {
            final int noHex=nxtE(-HEX_DIGT, s, noSpc+1,L),  nDigits=noHex-noSpc-1, num=(int)hexToInt(s,1,99);
            return nDigits>6 ? C(num&0xFFffFF, (num>>>24)&255) : C(num);
        }
        final int
            c1=strchr(',',s, noSpc, L),
            c2=strchr(',',s, c1+1,  L),
            c3=strchr(',',s, c2+1,  L);
        if (c2>0) {
            final int
                r=atoi(s,0),
                g=atoi(s,c1+1),
                b=atoi(s,c2+1),
                a=c3>0?atoi(s,c3+1) : 255;
            return C( (r<<16) + (g<<8)+b,a);
        }
        return null;
    }
    /**
     * a value 0<=x<=1 is turned into a color blue ... yellow ... red
     */
    public static Color blueYellowRed(float x) {
        final Color cc[]=_blueYellowRed;
        if (x>1) x=1f;
        if (x<0) x=0;
        final int L=cc.length;
        int idx=(int)(x* L);
        if (idx>=L) idx=L-1;
        if (cc[idx]!=null) return cc[idx];
        float r=0,g=0,b=0;
        if (x< 0.5f) { b=1-x-x; r=x+x; g=x+x; }
        if (x>=0.5f) { r=1; g=2-x-x; }
        return cc[idx]=new Color(r,g,b);
    }
    public static Color float2blueRed(double value) {
        float x=(float)value;
        if (Float.isNaN(x)) return Color.WHITE;
        if (x>1) x=1f;
        if (x<0) x=0;
        float r=0,g=0,b=0;
        if (x< 0.5f) { b=1-x-x; r=x+x; g=x+x; }
        if (x>=0.5f) { r=1; g=2-x-x; }
        return new Color(r,g,b);
    }
    /* <<< Color <<< */
    /* ---------------------------------------- */
    /* >>> LAF >>> */
    public static void makeTranslucent(Component parent) {
        for(Component c : childs(parent)) {
            c.setBackground(C(0,0));
            makeTranslucent(c);
        }
        if (parent instanceof JComponent) ((JComponent)parent).setOpaque(false);
    }
    public static void antiAliasing(Graphics g) {
        final ChButton b=BUTTN[TOG_ANTIALIASING];
        final Object a=b!=null && b.s() ?  RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF;
        if (g!=null) ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, a);
    }
    /* <<< LAF <<< */
    /* ---------------------------------------- */
    /* >>> Date  >>> */
    private static java.text.DateFormat dateFmt0, dateFmt1;
    public static String day(long when) {
        if (dateFmt0==null) dateFmt0=java.text.DateFormat.getDateInstance(java.text.DateFormat.LONG,Locale.ENGLISH);
        try {
            return dateFmt0.format(new Date(when==0 ? currentTimeMillis() : when));
        } catch(Exception ex) { return "Error: Wrong date";}
    }
    public static String fmtDate(long when) {
        if (dateFmt1==null) dateFmt1=new java.text.SimpleDateFormat("yyyy_MM_dd-HH_mm");
        try {
            return dateFmt1.format(new Date(when==0 ? currentTimeMillis() : when));
        } catch(Exception ex) { return "Error: Wrong date";}
    }
    /* <<< Date <<< */
    /* ---------------------------------------- */
    /* >>> String Array >>> */
    public static int findSfx(String suffices[], String s) {
        for(int i=0;i<suffices.length; i++) {
            if (s!=null && s.endsWith(suffices[i])) return i;
        }
        return -1;
    }
    public static int idxOfPfx(String pfx, Object o) {
        if (0==sze(pfx) || o==null) return -1;
        final int L=szeVA(o);
        for(int i=0; i<L; i++) {
            if (strEquls(pfx, get(i,o,CharSequence.class))) return i;
        }
        return -1;
    }
    /* <<< String Array <<< */
    /* ---------------------------------------- */
    /* >>> CharSequence >>> */
    public static String partDotDotDot(String s, int maxLen) {
        return sze(s)<=maxLen ? s : s.substring(0,maxLen)+" ...";
    }
    public static String substrg(Object txt,int from,int to) {
        if (txt==null) return null;
        final int f=maxi(0,from), t=mini(to,sze(txt));
        if (f>=t) return "";
        if (txt instanceof CharSequence) return toStrg(((CharSequence)txt).subSequence(f,t));
        if (txt instanceof byte[]) return bytes2strg((byte[])txt,f, t);
        assrt();
        return null;
    }
    public static byte[] substrgB(byte bb[], int from ,int to) {
        if (sze(bb)==0) return NO_BYTE;
        if (from<=0 && to>=bb.length) return bb;
        final int f=maxi(0,from), t=mini(bb.length,to);
        if (f>=t) return NO_BYTE;
        final byte rr[]=new byte[t-f];
        System.arraycopy(bb,f,rr,0,t-f);
        return rr;
    }
    public static byte[] concat(byte[] txt1,int from1,int to1,   byte[] txt2,int from2,int to2,  byte buffer[]) { return concat( txt1, from1, to1,    txt2, from2, to2,  null,0,0, buffer);}
    public static byte[] concat(byte[] txt1,int from1,int to1,   byte[] txt2,int from2,int to2,  byte[] txt3,int from3,int to3, byte buffer[]) {
        final int f1=maxi(0,from1), t1=mini(sze(txt1),to1);
        final int f2=maxi(0,from2), t2=mini(sze(txt2),to2);
        final int f3=maxi(0,from3), t3=mini(sze(txt3),to3);
        final int L=maxi(0,t1-f1)+maxi(0,t2-f2)+maxi(0,t3-f3);
        final byte txt[]=sze(buffer)>=L ? buffer : new byte[L];
        if (t1-f1>0) System.arraycopy(txt1,f1,txt,0,t1-f1);
        if (t2-f2>0) System.arraycopy(txt2,f2,txt,t1-f1,t2-f2);
        if (t3-f3>0) System.arraycopy(txt3,f3,txt,t1-f1+t2-f2,t3-f3);
        return txt;
    }
    private final static Object refImage[]=new Reference['v'+1];
    public static BufferedImage sharedImage(char h_or_v, int n) {
        assert (h_or_v|32)=='v' || (h_or_v|32)=='h';
        final boolean isVertical= (h_or_v|32)=='v';
        BufferedImage i=(BufferedImage)deref(refImage[h_or_v]);
        if (i==null|| (isVertical ? i.getHeight() : i.getWidth())<n ) {
            i=new BufferedImage(isVertical?1:n, isVertical?n:1,(h_or_v|32)==h_or_v ? BufferedImage.TYPE_USHORT_GRAY : BufferedImage.TYPE_INT_RGB);
            refImage[h_or_v]=newSoftRef(i);
        }
        return i;
    }
    public static String strgTrim(byte[] txt, int from,int to) {
        final int f=nxtE(-SPC,txt,from,to);
        final int t=prevE(-SPC, txt, mini(txt.length,to)-1, from)+1;
        return bytes2strg(txt,f,t);
    }
    public static String[] collectIDs(String search,  BA ba, String pfx) {
        return collectIDs(search,ba.bytes(), ba.begin(), ba.end(), pfx);
    }
    private final static Object[] COLLECTIDS={null,null};
    public static String[] collectIDs(String search,  byte txt[], int b, int e, String pfx) {
        synchronized(COLLECTIDS) {
            final List v=vClr(1,COLLECTIDS);
            final boolean prefix=sze(pfx)>0;
            int i=b;
            boolean added=false;
            while( (i=strstr(STRSTR_AFTER,search,txt,i,e))>0) {
                final int t=nxtE(SPC,txt,i,e);
                v.add(prefix ? baClr(COLLECTIDS).a(pfx).a(txt,i,t).toString() : bytes2strg(txt, i,t) );
                added=true;
            }
            return added ? toArryClr(v,String.class) : NO_STRING;
        }
    }
    public static boolean cntainsOnly(int charClass, Object txt) { return cntainsOnly(chrClas(charClass),txt,0,MAX_INT);}
    public static boolean cntainsOnly(int charClass, Object txt, int f, int t) { return cntainsOnly(chrClas(charClass),txt,f,t);}
    public static boolean cntainsOnly(boolean[] charClass, Object txt, int from, int to0) {
        final int to=mini(sze(txt), to0);
        if (txt==null || to-from<=0) return false;
        if (txt instanceof CharSequence) {
            final CharSequence T=(CharSequence)txt;
            for(int i=maxi(0,from); i<to; i++) {
                final char c=T.charAt(i);
                if (c>127 || !charClass[c])  return false;
            }
        } else  {
            final byte[] T=(byte[])txt;
            for(int i=maxi(0,from); i<to; i++) {
                final byte c=T[i];
                if (c<0 || !charClass[c])  return false;
            }
        }
        return true;
    }
    /* <<< CharSequence <<< */
    /* ---------------------------------------- */
    /* >>> HashCode and Compare >>> */
    public static int hashCd(Object o) {
        return o==null ? 0 :
            o instanceof String ? o.hashCode() :
            hashCd(o, 0, MAX_INT, false);
    }
    public static int hashCd(Object bytesOrCharSeq, int from, int to) { return hashCd(bytesOrCharSeq, from, to, false); }
    public static int hashCd(Object bytesOrCharSeq, int from, int to,boolean toUpper) {
        final int L=mini(to,sze(bytesOrCharSeq));
        if (bytesOrCharSeq instanceof BA) {
            final BA ba=(BA)bytesOrCharSeq;
            return hashCd(ba.bytes(), ba.begin()+from, ba.begin()+L,  toUpper);
        }
        final byte bb[]=bytesOrCharSeq instanceof byte[] ? (byte[]) bytesOrCharSeq : null;
        final CharSequence cs=bytesOrCharSeq instanceof CharSequence ? (CharSequence)bytesOrCharSeq : null;
        if (bb==null && cs==null) { assert bytesOrCharSeq==null;  return -1; }
        int h=0;
        if (toUpper) {
            for (int i=from; i<L; i++) {
                final int b=(bb!=null?bb[i] : cs.charAt(i))&~32;
                if (b==0) break;
                h= 31*h + b;
            }
        } else {
            for (int i=from; i<L; i++) {
                final byte b=(byte)(bb!=null?bb[i] : cs.charAt(i));
                if (b==0) break;
                h= 31*h + b;
            }
        }
        return h;
    }
    public static long hashCd(File f) {
        if (f==null) return -1;
        InputStream is=null;
        try {
            is=new BufferedInputStream(new FileInputStream(f));
            long h=0;
            for(int c;  (c=is.read())>=0;) {
                h= 31*h + c;
            }
            return h;
        } catch (IOException iox) {
            return -1;
        }
        finally { closeStrm(is); }
    }
    public static int hashCdOnlyLetters(byte bb[],int from, int to,boolean toUpper) {
        if (bb==null) return -1;
        final int iMax=mini(bb.length,to);
        int h=0;
        for (int i=from; i<iMax; i++) {
            final byte c=bb[i];
            if ('a'<=c && c<='z' || 'A'<=c && c<='Z') {
                final int b=toUpper ? c&~32 : c;
                if (b==0) break;
                h= 31*h + b;
            }
        }
        return h;
    }
    public static long hashCdFloats(float xyz[], int len) {
        if (xyz==null) return 0;
        long sum=0;
        final int n=mini(len, xyz.length/3);
        for(int i=0,j=0,count=0;i<n;i++,j+=3) {
            sum+=(long)( (xyz[j]+xyz[j+1]*2+xyz[j+2]*3)*100)*count++;
        }
        return sum;
    }
    /* <<< CharSequence <<< */
    /* ---------------------------------------- */
    /* >>> Looks like ... >>> */
    public final static int LIKE_EC=1, LIKE_EXTURL=2, LIKE_EXTURL_S=3, LIKE_MAILADDR=4, LIKE_ENSGhm=5,
        LIKE_FILEPATH=6, LIKE_FILEPATH_EVALVAR=7,
        LIKE_KEGG=8, LIKE_KEGGc=9, LIKE_KEGGr=10, LIKE_NUM=11, LIKE_DRIVE_LETTER=12, LIKE_HTML=13,
        LIKE_UNIPROT_ID=14, LIKE_GB_PROTEIN_ID=15, LIKE_GB_NUCLEOTIDE_ID=16;
    public static boolean looks(int like, Object T) { return looks(like,T,0,MAX_INT); }
    public static boolean looks(int like, Object T, int from) {
        return looks(like, T, from,  like==LIKE_HTML?MAX_INT:nxtE(SPC,T,from,MAX_INT));
    }
    public static boolean looks(int like, Object T, int from0, int to0) {
        if (T==null) return false;
        assrtIsTxt(T);
        final int to=mini(sze(T),to0), from=maxi(0,from0), L=to-from;
        if (L<=0) return false;
        if (like==LIKE_HTML) {
            final int i=nxt(-SPC, T, from, to);
            if (chrAt(i,T)!='<') return false;
            if (strEquls(STRSTR_IC, "<html>", T, i)) return true;
            if (strEquls(STRSTR_IC, "<!DOCTYPE ", T, i) && strEquls(STRSTR_IC, "html", T, nxt(-SPC,T,i+10,to))) return true;
            return false;
        }
        // ENSG00000154358
        if (like==LIKE_NUM) {
            final char c0=chrAt(from,T);
            return '0'<=c0 && c0<='9' || c0=='-' && L>1 && is(DIGT,T,from+1);
        }
        if (like==LIKE_EC) {
            final int dot1=nxt(-DIGT,T, from,to-5);
            if (dot1<=from || chrAt(dot1,T)!='.')  return false;
            final int dot2=nxt(-DIGT,T,dot1+1,to-3);
            if (dot2<dot1+2 || chrAt(dot2,T)!='.')  return false;
            final int dot3=nxt(-DIGT,T,dot2+1,to-1);
            if (dot3<dot2+2 || chrAt(dot3,T)!='.')  return false;
            final int end=nxt(-DIGT,T,dot3+1,to);
            return !is(LETTR_DIGT_US,T,end) && chrAt(end,T)!='.';
        }
        final char c0=chrAt(from,T), prev=chrAt(from-1,T);
        if (like==LIKE_EXTURL || like==LIKE_EXTURL_S) {
            if ((c0=='h' || c0=='f') && (strEquls("http://",T,from) || like==LIKE_EXTURL_S && strEquls("https://",T,from) || strEquls("ftp://",T,from))) {
                return !is(LETTR_DIGT_US,prev) && !is(SLASH,prev);
            } else  return false;
        }
        final char  c1=chrAt(from+1,T), c2=chrAt(from+2,T);
        if (like==LIKE_DRIVE_LETTER) return is(LETTR, c0) && c1==':' && (to==2 || is(SLASH,c2));
        if (like==LIKE_ENSGhm) {
            if (L<15) return false;
            if (c0!='E' || c1!='N' || c2!='S') return false;
            if (L==15 && chrAt(from+3,T)=='G'   && cntainsOnly(DIGT,T,from+4, to)) return true;
            if (L==18 && strEquls("MUSG",T,from+3) && cntainsOnly(DIGT,T,from+7, to)) return true;
            return false;
        }
        if (like==LIKE_FILEPATH || like==LIKE_FILEPATH_EVALVAR) {
            if (c0=='%' || c0=='$') {
                if (strEquls("$HOME/",T,from)) return true;
                if (like==LIKE_FILEPATH_EVALVAR) {
                    for(String s : ChEnv.getNames(ChEnv.WITH_FILEPATH)) {
                        if (strEquls(s,T,from) && !is(LETTR_DIGT_US,T, from+s.length()) ) return true;
                    }
                }
            }
            final boolean win=isWin(), slash0=is(SLASH,c0);
            if (c0=='f' && strEquls("file:/",T,from) ||
                (c0=='.'||c0=='~') && is(SLASH,c1) || slash0&&is(LETTR_DIGT_US,c1) ||
                win && slash0 && is(SLASH,c1) && is(LETTR_DIGT,c2) ||
                win && c1==':' && is(LETTR,c0) && is(SLASH,c2)) {
                return prev!=':' && !is(SLASH,prev) && !is(LETTR_DIGT_US,prev);
            }
        }
        if (like==LIKE_KEGG || like==LIKE_KEGGc || like==LIKE_KEGGr) {
            if (L!=6 ||
                like==LIKE_KEGG  && c0!='R' && c0!='C' && c0!='G' ||
                like==LIKE_KEGGr && c0!='R' ||
                like==LIKE_KEGGc && c0!='C' && c0!='G') return false;
            return cntainsOnly(DIGT,T,from+1,to);
        }
        if (like==LIKE_MAILADDR) {
            final int at=strchr('@',T,from,to);
            //if (at==6 && w.charAt(0)=='R') return false; /* R00658@any4.2.1.11 */
            if (at<1 || strchr('.',T,at,to)<0) return false;
            int i=0;
            for(i=to; --i>=from;) {
                final char c=chrAt(i,T);
                int atRel=i-at;
                if (atRel<0) atRel=-atRel;
                if (c=='.' && (atRel==1 || i>0 && chrAt(i-1,T)=='.')) return false;
                if (c=='@' && i!=at) return false;
                if (!is(LETTR_DIGT_US,c) && c!='.' && c!='@') return false;
            }
            return true;
        }

        if (like==LIKE_UNIPROT_ID && L>=6) { /* E.g.  A0RZB7 */
            return is(UPPR_DIGT,T,from) && cntainsOnly(UPPR_DIGT, T,from,from+6) && (L==6 || !is(LETTR_DIGT_US,T,from+6));
        }
        if (like==LIKE_GB_NUCLEOTIDE_ID || like==LIKE_GB_PROTEIN_ID) {
            /* http://www.ncbi.nlm.nih.gov/Sequin/acc.html */
            int countL=0, countD=0;
            for(int i=from; i<to; i++) {
                final char c=chrAt(i,T);
                if ('0'<=c && c<='9') countD++;
                else if (is(LETTR,c)) {
                    if (countD>0) return false;
                    countL++;
                } else {
                    if (c=='_') return false;
                    break;
                }
            }
            //putln("letters="+countL+"  digits="+countD);
            final int c=countL*10+countD;
            return  like==LIKE_GB_PROTEIN_ID ? c==35 : c==15 || c==26 || c==42 || c>=46 && c<=48 || c==57;

        }
        return false;
    }
    /* <<< Looks like ... <<< */
    /* ---------------------------------------- */
    /* >>> Directory >>> */
    public static void setFolderIcon(File dir, String icon, String tip) {
        final String fn=isWin() ? delLstCmpnt(icon,'.')+".ico" : icon;
        final File fIco=file(dir+"/"+fn);
        if (sze(fIco)==0) {
            cpy(rscAsStream(0L,_CC+"myIcons",fn), fIco);
            if (sze(fIco)>0) {
                if (isWin()) {
                    final File desktopIni=file(dir+"/Desktop.ini");
                    wrte(desktopIni,"[.ShellClassInfo]\r\nConfirmFileOp=0\r\nNoSharing=1\r\nIconFile="+fn+"\r\nIconIndex=0\r\nInfoTip="+tip);
                    new ChExec(0).setCommandLineV("attrib", "+s",dir.toString()).run();
                    new ChExec(0).setCommandLineV("attrib", "+s",dir).run();
                    new ChExec(0).setCommandLineV("attrib", "+H",desktopIni).run();
                    new ChExec(0).setCommandLineV("attrib", "+H",fIco).run();
                }
            }
        }
    }
    private static File
        _dirHome, _dirBin, _dirData, _dirWorking, _dirTmp, _dirTmpTime, _dirUser, _dirLog, _dirDnd, _dirSettings, _dirDoc, _dirWeb, _dirStrap,
        _fLogDL, _dragFiles[],  _cpAtStart[], _thisJarFile, _dirHome2;

    public static ChFile newTmpFile(String suffix) {
        if (_dirTmpTime==null || _dirTmp==null) mkdrs(_dirTmpTime=file(dirTmp()+"/time"+TIME_AT_START));
        final ChFile f=(ChFile)file(_dirTmpTime+"/"+_iTempFile++ +suffix);
        delFileOnExit(f);
        return f;
    }

    public static File dirTmp() {
        final File dw=_dirWorking;
        if (_dirTmp==null) _dirTmp=file(dw!=null ? dw+"/"+STRAPTMP : "~/@/"+STRAPTMP);
        return _dirTmp;
    }

    public static void setWorkingDir(File f) {
        _refMapFiles[0]=null;
        _dirTmp=null;
        _dirWorking=f;
    }
    public static boolean dirWorkingSet() { return _dirWorking!=null; }
    public static File dirWorking() { return _dirWorking!=null ? _dirWorking : dirUser(); }

    public static File dirWeb() {
        File f=_dirWeb;
        if (f==null) mkdrsErr(_dirWeb=f=file("~/@/web"),"Cache for files loaded from the web.");
        return f;
    }
    public static File dirStrapAnno() {
        File f=_dirStrap;
        if (f==null) mkdrsErr(f=_dirStrap=file("~/@/strap"),"Protein annotations");
        return f;
    }
    public static File dirHome() {
        if (_dirHome==null) _dirHome= new ChFile(systProprty(SYSP_USER_HOME),isWin());
        return _dirHome;
    }

    /**   %HOMEDRIVE%\%HOMEPATH% may not be the same as user.home */
    public static File dirHome2() {
        if (!isWin()) return dirHome();
        if (_dirHome2==null) {
            final File f=file(new BA(99).and(ChEnv.get("HOMEDRIVE"),"/").a(ChEnv.get("HOMEPATH")));
            _dirHome2=isDir(f) && !strEquls(toStrg(f),toStrg(dirHome())) ? f : dirHome();
        }
        return _dirHome2;
    }
    public static File dirDndData() {
        File f=_dirDnd;
        if(f==null) {
            mkdrsErr(f=_dirDnd=file("~/@/tmp/dnd"),"Tmp files for Drag'n Drop");
            delFileOnExit(f);
        }
        return f;
    }
    public static File dirUser() {
        if(_dirUser==null) _dirUser=new ChFile(systProprty(SYSP_USER_DIR),isWin());
        return _dirUser;
    }
    public static File dirLog() {
        File f=_dirLog;
        if(f==null) {
            mkdrsErr(new ChFile(dirSettings()+"/log", isWin()), "Log-messages\nYou can delete this folder");
            delFileOnExit(f=_dirLog=new ChFile(dirSettings()+"/log/"+fmtDate(0),isWin()));
        }
        return f;
    }
    public static File dirDataFiles() {
        if(_dirData==null) mkdrsErr(_dirData=new ChFile(dirSettings(),"dataFiles",isWin()),"This directory contains downloaded data files of\n");
        return _dirData;
    }
    public static File dirSettings(){
        if (_dirSettings==null) {
            final boolean isWin=isWin();
            final File fRelocate=new File(dirHome(),"StrapAlign"+File.separatorChar+"location_of_StrapAlign.ini");
            if (0==sze(fRelocate)) renamFile(new File(dirHome(),".location_of_StrapAlign.ini"),fRelocate);
            final String
                dotStrapAlign=isWin ? "StrapAlign" : ".StrapAlign",
                dirHomeEnv=ChEnv.get("STRAP_USER_HOME"),
                dirHome=dirHomeEnv!=null && new File(dirHomeEnv).isDirectory() ?  dirHomeEnv : toStrg(dirHome());
            final BA ba=readBytes(fRelocate);
            File d=null;
            if (ba!=null && ba.delBlanksL().setEnd(nxtE(SPC,ba)).length()>0) {
                if (strstr("StrapAlign",ba)<0) ba.delSuffix("/").delSuffix("\\").a("/StrapAlign");
                (d=new File(ba.toString())).mkdirs();
            }
            if (d==null) {
                if (isWin && dirHome.indexOf(' ')>0) {
                    for(char root='C'; root<='E'; root++) {
                        final File dTry=new File((char)root+":\\"+dotStrapAlign), dJars=new File(dTry+"\\jars");
                        if (!dJars.exists()) mkdrs(dJars);
                        if (dJars.isDirectory()) {
                            d=dTry;
                            final String txt=
                                "\nThis file contains the location of Strap's  application data folder.\r\n"+
                                "To change the location modify the first line.\r\n"+
                                "The file path must not contain white space.\r\n";
                            wrte(fRelocate,d+txt);
                            final BA msg=new BA(999)
                                .aln("The directory for the application data for Strap is")
                                .aln(d.getAbsolutePath())
                                .aln("You may specify a different location by modifying")
                                .aln(fRelocate)
                                .aln("The directory path must not contain white space.")
                                .trimSize();
                            toLogMenu(msg, "Info on Strap directory ...", IC_DIRECTORY);
                            break;
                        }
                    }
                }
            }
            if (d==null) d=new File(dirHome+File.separatorChar+dotStrapAlign);
            _dirSettings=d;
            if (d.toString().indexOf(' ')>0) {
                error("The application data folder for Strap\n"+d+
                      "\ncontains white space which might cause problems.\nPlease specify a suitable location in\n\n"+fRelocate);
            }
            final String readme=
                "This directory contains application data of Strap.\n"+
                "To free hard disk space you might delete the directories \"web\" and \"cache\".";
            mkdrsErr(d,readme);
        }
        return _dirSettings;
    }
    public static File dirBin() {
        if (_dirBin==null) {
            mkdrsErr(_dirBin=new ChFile(dirSettings(),"bin",isWin()),"This directory contains external programs which have been installed automatically.\n"+
                     "Packages are installed when they are needed for the first time\n"+
                     "You can remove the directory and its files to force reinstallation of all packages.");
        }
        return _dirBin;
    }
    public static File dirDocumentation() {
        if (_dirDoc==null) mkdrsErr(_dirDoc=file("~/@/doc"),"This directory contains generated HTML.\nThey can be viewed in web browsers.");
        return _dirDoc;
    }
    /* <<< Directory <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */

    public static boolean isNamedPipe(File f) {
        if (f==null || isWin()) return false;
        if (!Insecure.EXEC_ALLOWED) {
            puts(onlyOnce(RED_WARNING+" Strap light cannot identify named pipes"));
            return false;
        }
        final ChExec ex=new ChExec(ChExec.STDOUT).setCommandLineV("ls","-l",f);
        ex.run();
        return chrAt(0,ex.getStdout())=='p';
    }

    public static String relativFilePath(File dir, String path) {
        if (dir==null||path==null) return path;
        final String path0=toStrg(dir);
        final int L;
        if (strEquls((isWin() ? STRSTR_IC:0) | STRSTR_ANY_SLASH, path0,path) && is(SLASH, path, L=sze(path0))) return path.substring(L+1);
        return path;

    }
    public static boolean fExists(String fn) { return fn!=null && fExists(file(fn));}
    public static boolean fExists(File f) { return f!=null && f.exists();}
    public static boolean isDir(File f) { return f!=null && f.isDirectory();}
    public static boolean isDirNotEmpty(File f) {
        for(String s : lstDir(f)) {
            if (!"README.txt".equals(s)) return true;
        }
        return false;
    }
    public static File[] lstDirF(File f) {
        File ff[]=null;
        try {
            if (isDir(f)) ff=f.listFiles();
        } catch(Exception ex){stckTrc(ex);}
        return ff==null?NO_FILE : ff;
    }

    public static String[] lstDir(File f) {
        String ff[]=null;
        try {
            if (isDir(f)) ff=f.list();
        } catch(Exception ex){stckTrc(ex);}
        return ff==null?NO_STRING : ff;
    }

    private static Set<String> vDelOnExit;
    private static int _iTempFile, _countCache;
    private final static Object _refMapFiles[]={null};
    private static List<File> _vRoots;
    /**
       Sometimes  Windows says " ... Please insert disk ... " when checking for C: D: E: ...
       This is a workaround. The user is confronted with the windows message only once.
    */
    public static File[] listExistingFileRoots() {
        if (_vRoots==null) {
            _vRoots=new ArrayList();
            final File rr[]=File.listRoots();
            for (File f : rr) {
                final String s=f.getAbsolutePath();
                if (isWin() && chrAt(1,s)==':') {
                    if (s.charAt(0)=='A' || s.charAt(0)=='B') continue;
                    if (!f.canRead()) continue;
                    if (false && Insecure.EXEC_ALLOWED) {
                        final ChExec ex=new ChExec(0L).setCommandLineV("SH","VOL",s.substring(0,2));
                        ex.run();
                        if (ex.exitValue()!=0) continue;
                        ex.dispose();
                    }
                }
                adUniq(f,_vRoots);
            }
            if (sze(_vRoots)==0) adAllUniq(rr,_vRoots);
        }
        final File ff[]=toArry(_vRoots, File.class);
        for(int i=ff.length; --i>=0;) if (ff[i]!=null && !ff[i].exists()) ff[i]=null;
        return rmNullA(ff,File.class);
    }
   public static String fPathUnix(Object file) {
        if (file==null) return null;
        final String path= toStrg(file), home=sysGetProp(USER_DOT_HOME);
        final int L=home.length();
        return (sze(home)>4 && path.startsWith(home) ? "~"+path.substring(is(SLASH,home,L-1) ? L-1 : L) :  path).replace('\\','/');
    }
    public static void delFileOnExit(File f) { delFileOnExit(toStrg(f)); }
    public static void delFileOnExit(String s) {
        if (s!=null) (vDelOnExit==null ? vDelOnExit=new HashSet() : vDelOnExit).add(s);
    }
    private static int _stckTrcDel;
    public static boolean delFile(File f) { return delFile(true, f); }
    public static boolean delFile(boolean reportError, File f) {
        if (!fExists(f)) return false;
        for(int iTry=0; iTry<3; iTry++) {
            if (!f.exists()) break;
            Insecure.delFile(f);
            sleep(99);
        }
        if (sze(f)>0) {

            if (reportError) {
                final String msg="Deletion of file refused\n";
                error(msg+Insecure.msgNotModify(f));
                if (0==_stckTrcDel++) { putln(RED_WARNING, msg,f); stckTrc(); }
            }
            delFileOnExit(f);
            return false;
        }
        return !f.exists();
    }

    public static boolean canModifyNiceMsg(File f) {
        if (Insecure.canModify(f)) return true;
        if (f!=null) error(Insecure.msgNotModify(f));
        return false;
    }

    public static void deleteTree(File f) {
        if (f==null) return;
        for(String child : lstDir(f)) deleteTree(new File(f,child));
        delFile(false, f);
    }
    public static String toCygwinPath(Object f) {
        final String p=toStrg(f);
        return chrAt(1,p)!=':'||!isWin()?p : "/cygdrive/"+chrAt(0,p)+p.substring(2).replace('\\','/');
    }
    public static File mostRecentFile(File dirs[], String extensions[]) {
        long lm=0;
        File fLast=null;
        for(File dir : dirs) {
            nextFile:
            for(String fn : lstDir(dir)) {
                for(String e : extensions) {
                    if (e!=null && fn.indexOf(e)>=0 && ( fn.endsWith(e) || fn.endsWith(delLstCmpnt(e,'-')))) {
                        final File f=new File(dir,fn);
                        final long modified=f!=null ?  f.lastModified() : 0;
                        if (lm<modified && f.length()>0 ) { lm=modified; fLast=f; }
                        continue nextFile;
                    }
                }
            }
        }
        return fLast;
    }
    public static void mkExecutabl(File f) {
        if (f==null ||isWin()) return;
        if (javaVsn()<16) new ChExec(0).setCommandLineV("chmod", "a+x", f).run();
        else forJavaVersion(16).run(RunIf16orHigher.RUN_setExecutable,f);
    }
    public static File file(File dir, String filename) { return filename==null||dir==null ?  null : new ChFile(dir,filename,isWin());}
    public static File file(Object filename) { return filename instanceof File ? (File)filename : file(0L, filename);}
    static String processFileName(String filename) {
        if (filename==null || looks(LIKE_EXTURL,filename)) return null;
        final String fn=rplcToStrg("/./","/",_processFileName(filename));
        return fn!=null && isWin() ? fn.replace('/',File.separatorChar) :  fn;
    }
    public final static long FILE_NEW_INSTANCE=1<<1, FILE_NO_ERROR=1<<2;
    public static File file(long options, Object filename) {
        final boolean newInstance=(options&FILE_NEW_INSTANCE)!=0;
        final String toString0=toStrg(filename);
        if (toString0==null) return null;
        final boolean isURL=looks(LIKE_EXTURL,toString0);
        final String toString;
        if (isURL) {
            String s=delPfx("ftp://",delPfx("http://",delSfx(".gz",delSfx(".Z",toString0))));
            final int amp=s.lastIndexOf('&');
            if (amp>0 && cntainsOnly(DIGT,s,amp+1,MAX_INT)) s=s.substring(0,amp);
            final BA ba=new BA(99).a(dirWeb()).a('/').filter('_'|FILTER_NO_MATCH_TO, FILEP, s);
            toString=isWin() && ba.length()>240 ? dirWeb()+"/exceedsPathLen/"+sze(s)+"/"+s.hashCode() : ba.toString();
        } else toString=toString0;
        if (sze(toString)==0) return null;
        if ((options&FILE_NO_ERROR)==0 && toString.indexOf(':',2)>1 && !toString.startsWith("file:")) {
            putln(RED_ERROR+" file ",toString);
            stckTrc();
        }
        if (!newInstance && filename instanceof File) return (File)filename;
        final boolean lookup=!newInstance && toString.charAt(0)!='?';
        Map<String,File> map=null;
        if (lookup) {
            synchronized(map=fromSoftRef(0,_refMapFiles, HashMap.class)) {
                final File f=map.get(toString);
                if (f!=null) return f;
            }
        }

        final String fn=filename.equals(systProprty(SYSP_USER_DIR)) ? toString : processFileName(toString);
        if (fn==null) return null;
        if (lookup) {
            File f=map.get(fn);
            if (f==null) {
                f=new ChFile(fn,isWin());
                if ((_countCache++&32)==0) {
                    synchronized(map) {
                        map.put(toString,f);
                        map.put(fn,f);
                    }
                }
            }
            return f;
        }
        return new ChFile(fn,isWin());
    }
    public static String _processFileName(String filename0) {
        final String filename=filename0!=null ? filename0.trim() : null;
        if (filename==null || filename.length()==0) return null;
        String pattern;
        final char c0=chrAt(0,filename);
        final boolean win=isWin();
        if (c0=='f') {
            final boolean
                lh=filename.startsWith(pattern="file://localhost/") || filename.startsWith(pattern="file:///"),
                host=!lh && filename.startsWith(pattern="file://");
            if (lh || host || filename.startsWith(pattern="file:/") )  {
                /* rfc1738 file:/tmp/ file:///tmp/ file:/C:/temp/ C:\temp   \\debianserver\SAMBA\aa    */
                int start=pattern.length()-(host?2:1);
                if (/*win && */is(LETTR,filename,start+1) && ':'==chrAt(start+2,filename)) start++;
                return urlDecode(filename.substring(start));
            }
        }
        final String fn=rplcToStrg("%20"," ",filename);
        final char c1=chrAt(1,fn);
        if (c0=='/' && c1=='c' && fn.startsWith("/cygdrive/") && chrAt(11,fn)=='/') {
            return (char)(fn.charAt(10)&~32)+":\\"+ fn.substring(12);
        }
        if (win && c1==':') {
            if (c0=='?' && fn.length()>2 ) {
                final String fn0= fn.substring(2);
                for(File driveLetter: listExistingFileRoots()) {
                    if (driveLetter.exists() && chrAt(1,driveLetter.getAbsolutePath())==':') {
                        final File f=new File(driveLetter,fn0.replace('/',File.separatorChar));
                        if (f.exists()) return f.getAbsolutePath();
                    }
                }
                return fn;
            }
            if (is(LETTR,c0) && (is(SLASH,fn,2) || fn.length()==2)) return fn;
        }
        if (c0=='/' || c0=='\\') return fn;
        if (c0=='~' && is(SLASH,c1)) return fn.startsWith("~/@/") ? dirSettings()+(fn.substring(3)) : dirHome()+fn.substring(1);
        if (c0=='$' || c0=='%' && win) {
            final String fnNew=toStrg(rplcEnvInPath(fn));
            if (fnNew!=fn) return fnNew;
        }
        return dirWorking()+"/"+fn;
    }
    public static boolean mkdrs(File dir) {
        if (dir==null) return false;
        for(int iTry=2;--iTry>=0 && !isDir(dir);) {
            Insecure.makeDirectories(dir, iTry==0);
            sleep(111);
        }
        return isDir(dir);
    }
    private final static Set<String> vMkdrsErr=new HashSet();
    public static boolean mkdrsErr(File dir) { return mkdrsErr(dir,"");}
    public static boolean mkdrsErr(File dir, String readme) {
        if (dir==null) return false;
        if (!dir.exists()) {
            mkdrs(dir);
            if (!dir.isDirectory()) {
                if (!vErrorAlreadyReported.contains(dir)) error("Could not create directory "+dir+"<pre class=\"data\">"+stckTrcAsStrg(null)+"</pre>");
                vErrorAlreadyReported.add(dir);
            } else if (!dir.canWrite()) error("No permission to create files in directory "+dir);
            final String path=dir.getAbsolutePath();
            if (sze(readme)>0 && !vMkdrsErr.contains(path)) {
                vMkdrsErr.add(path);
                final CharSequence t=isWin() ? filtrS(FILTER_UNIX_TO_DOS,0, readme) : readme;
                final File f=new File(dir,"README.txt");
                if (f.length()!=t.length()) wrte(f,t);
            }
        } else if(!dir.isDirectory()) error("Error: Cannot create directory\n"+dir+"\nIt is a regular file.");
        return dir.isDirectory();
    }
    public static File mkParentDrs(File f) {
        final File parent=drctry(f);
        return mkdrsErr(parent) ? parent : null;
    }
    public static CharSequence rplcEnvInPath(CharSequence txt) {
        CharSequence s=txt;
        final char c0=chrAt(0,s);
        final boolean win=isWin();
        if (c0=='$' || c0=='%' && isWin()) {
            for(String env : ChEnv.getNames(ChEnv.WITH_FILEPATH)) {
                final long opt=(win && lstChar(env)=='%' ? 0L : STRSTR_w_R)|(win?STRSTR_IC:0L);
                if (strstr(opt, env,s)>=0) s=strplc(opt,env,ChEnv.get(env),s);
            }
        }
        return s;
    }
    public static boolean rplcInTextFile(long options, String[][] replacements, File f) {
        InputStream is=null;
        OutputStream os=null;
        try {
            try {
                final File fTmp=newTmpFile(".txt");
                os=fileOutStrm(fTmp);
                is=new FileInputStream(f);
                final ChInStream cis=new ChInStream(is,1024);
                final BA buf=new BA(99);
                while( cis.readLine(buf.clr())) {
                    for(String[] replacement : replacements) buf.replace(options,replacement[0],replacement[1]);
                    buf.write(os);
                    os.write('\n');
                }
                delFile(f);
                renamFileOrCpy(fTmp,f);
                return true;
            } catch(IOException iox) {}
            return false;
        } finally{ closeStrm(is); }
    }
    public static boolean fileEquls(File f, BA ba) {
        if (f==null || ba==null) return false;
        final int B=ba.begin(), E=ba.end();
        if (f.length()!=E-B) return false;
        InputStream is=null;
        final byte[] buf=new byte[4096], T=ba.bytes();
        try {
            try {
                int pos=B;
                is=new BufferedInputStream(new FileInputStream(f));
                while(true) {
                    final int n=is.read(buf,0, 4096);
                    if (n==0) sleep(111);
                    if (n<0) break;
                    assert n<=T.length-pos;
                    for(int i=mini(n,T.length-pos);--i>=0;) if (buf[i]!=T[i+pos]) return false;
                    pos+=n;
                }
            } catch(IOException ex) {stckTrc(ex);  return false;}
            return true;
        } finally { closeStrm(is); }
    }
    public static boolean fileEquls(File f1,File f2) {
        if (f1==null||f2==null) return false;
        final long L1=f1.length(), L2=f2.length();
        if (L1!=L2)return false;
        if (!f1.equals(f2)) return false;
        InputStream is1=null, is2=null;
        final byte buf1[]=new byte[4096], buf2[]=new byte[4096];
        try {
            try {
                is1=new BufferedInputStream(new FileInputStream(f1));
                is2=new BufferedInputStream(new FileInputStream(f2));
                while(true) {
                    final int n=maxi(1,mini(4096, is1.available(), is2.available()));
                    final int n1=is1.read(buf1,0,n), n2=is2.read(buf2,0,n);
                    if (n1==0) sleep(111);
                    if (n1!=n2) assrt();
                    if (n1<0) break;
                    for(int i=n1;--i>=0;) if (buf1[i]!=buf2[i]) return false;
                }
            } catch(IOException ex) {stckTrc(ex);  return false;}
            return true;
        } finally { closeStrm(is1); closeStrm(is2); }
    }
    public static File aFileNotZ(Object...path) {
        for(int i=0;i<path.length;i++) {
            final Object o=path[i];
            final File f=o instanceof CharSequence ? file(o) : (File)o;
            if (sze(f)!=0) return f;
        }
        return null;
    }
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> cp File >>> */
    public static boolean renamFile(File src, String dest) { return renamFile(src,file(dest)); }
    public static boolean renamFile(File src,File dest) {
        if (!fExists(src) || dest==null) return false;
        mkParentDrs(dest);
        delFile(dest);
        return Insecure.renameFile(src,dest);
    }
    public static boolean renamFileOrCpy(File src,File dest) {
        if (sze(src)==0  || dest==null) return false;
        renamFile(src,dest);
        if (sze(dest)==0 && !mkLink(src,dest,false)) {
            try {
                if (!cpy(new FileInputStream(src),dest)) return false;
            } catch(Exception iox) { return false;}
            if (dest.exists()) {
                delFileOnExit(src.getAbsolutePath());
                delFile(src);
            }
            return false;
        }
        return sze(dest)>0;
    }
    public static void hardLinkOrCopy(File src, File dest) {
        if (src==null || dest==null || src.equals(dest) || dest.length()>0) return;
        mkParentDrs(dest);
        synchronized(SYNC_CPY) {
            mkLink(src,dest,false);
            if (dest.length()==0) cpy(src,dest);
        }
    }
    public static boolean mkLink(File src, File dest, boolean soft) {
        if (src==null || dest==null || isWin() || !Insecure.EXEC_ALLOWED) return false;
        mkParentDrs(dest);
        final String cmd[]={"ln", soft?"-s":null,  toStrg(src), toStrg(dest)};
        try { runtimeExec(rmNullS(cmd)); } catch(IOException iox) {}
        return dest.exists();
    }
    public static void cpyIfDiffers(File src,File dst) {
        if (sze(src)!=0 && dst!=null && (dst.length()==0 || !fileEquls(src,dst))) cpy(src,dst);
    }
    public static boolean cpy(File src, File dest) {
        if (src!=null && dest!=null && src.length()>0) {
            try {
                return cpy(new FileInputStream(src),dest);
            } catch(IOException iox) {}
        }
        return false;
    }
    public static boolean cpy(InputStream from, File dest) {
        if (from==null || dest==null) return false;
        mkParentDrs(dest);
        OutputStream to=null;
        delFile(dest);
        try {
            if (!copyNoClose(from,to=fileOutStrm(dest), (IsEnabled)null, (byte[][])null, (boolean[]) null)) return false;
        } catch(IOException e) {
            return false;
        } finally{
            closeStrm(from);
            closeStrm(to);
        }
        return sze(dest)>0;
    }
    /* <<< cp File <<< */
    /* ---------------------------------------- */
    /* >>> Read >>> */
    public static BA readBytes(Object fileOrUrl) {
        if (fileOrUrl==null) return null;
        else if (fileOrUrl instanceof CharSequence) return readBytes(toStrg(fileOrUrl),null);
        else if (fileOrUrl instanceof File) return readBytes((File)fileOrUrl,null);
        else if (fileOrUrl instanceof InputStream) return readBytes((InputStream)fileOrUrl,null);
        else if (fileOrUrl instanceof URL) try { return readBytes(((URL)fileOrUrl).openStream());} catch(Exception ex) {}
        else stckTrc();
        return null;
    }
    public static BA readBytes(String fileOrUrl, BA buf) {
        final InputStream fi=inStream(fileOrUrl,STREAM_UNZIP);
        if (fi==null) return null;
        final boolean isZip=fi instanceof java.util.zip.InflaterInputStream;
        int fLen=0;
        if (!looks(LIKE_EXTURL,fileOrUrl)) {
            final File file=file(fileOrUrl);
            if (file!=null && !file.isDirectory()) fLen=(int)file.length();
        }
        final BA ba=buf!=null ? buf : new BA(0);
        ba.ensureCapacity(isZip ? fLen*3 : fLen);
        return readBytes(fi, ba);
    }
    /**
       @param buffer buffer to load file contents into.
       If buffer is large enough to hold the data then  buffer is also the  returned value.
       If buffer is null or too small then a new byte array with the exact size is returned.
    */
    public static BA readBytesMX(final File f, long maxBytes, BA buffer) {
        final long L=Math.min(maxBytes,f==null ? 0:f.length());
        if (L==0) return null;
        try {
            final InputStream is=deflaterAccordingToExtension(new FileInputStream(f),f.getName());
            final boolean
                isUnzip=is instanceof java.util.zip.InflaterInputStream,
                isBA=is instanceof ByteArrayInputStream;
            final int sze= (int) (isUnzip ? L*5 : L);
            final BA ba= readBytes(is,buffer!=null ? buffer : new BA(sze));
            if (!isUnzip && !isBA && myComputer() && L>sze(ba)) putln(RED_ERROR," in readBytes: Read only "+ba.end()+" File: "+L+" Bytes");
            return ba;
        } catch (Exception e){ return null;}
    }
    public static BA readBytes(File f, BA buffer) { return readBytesMX(f,MAX_INT,buffer);}
    public static BA readBytes(InputStream inStr, BA origBuffer) {
        final boolean debug=myComputer();
        final int SIZE=1024;
        if (inStr==null) return null;
        final InputStream fi=inStr instanceof java.util.zip.InflaterInputStream && !(inStr instanceof BufferedInputStream) ? new BufferedInputStream(inStr) : inStr;
        try {
            int available=0, countAllBytes=0, bufferEnd=0;
            try {available=fi.available();}catch(Exception e){}
            final BA buffer=origBuffer!=null ? origBuffer : new BA(maxi(available,4444));
            byte bb[]=buffer.bytes();
            try {
                if (sze(bb)==0) bb=new byte[available>0 ? available : SIZE];
                try{
                    final int read=fi.read(bb);
                    bufferEnd=maxi(0,read);
                    if (read<0) return buffer;
                    countAllBytes+=read;
                } catch(IOException e) { return null;}
                int nextByte=-1;
                try {nextByte=fi.read();} catch(IOException e){}
                if (nextByte!=-1){
                    final List<byte[]> v=new ArrayList();
                    v.add(chSze(bb,countAllBytes));
                    v.add(new byte[]{(byte)nextByte}); countAllBytes++;
                    try {
                        while(true) {
                            available=fi.available();
                            final byte[] ba=new byte[available>0 ? available:SIZE];
                            final int read=fi.read(ba);
                            if (read<0) break;
                            if (debug && read!=available && available!=0) putln("ChUtils#readBytes read!=available "+read+" != "+available+( read==1?" !!!!!!!!!! ":" "));
                            countAllBytes+=read;
                            v.add(chSze(ba,read));
                        }
                    } catch(IOException e){ putln("caught ChUtils#readBytes ",e);}
                    bb=new byte[countAllBytes];
                    final int N=sze(v);
                    for(int i=0,nextIdx=0; i<N; i++) {
                        final byte[] ba=v.get(i);
                        System.arraycopy(ba,0, bb, nextIdx, ba.length);
                        nextIdx+=ba.length;
                        bufferEnd=countAllBytes;
                    }
                }
                buffer.set(bb,0,bufferEnd);
                return buffer;
            } catch(OutOfMemoryError oome) {
                buffer.set(NO_BYTE,0,0);
                final String msg="!Caught: ChUtils#readBytes: OutOfMemoryError \n"+fi+"\nIncrease maxHeapSize "+stckTrcAsStrg(oome);
                error(msg);
                return new BA(msg);
            }
        } finally{ closeStrm(fi);closeStrm(inStr);}
    }
    public static InputStream deflaterAccordingToExtension(InputStream is,String s) {
        try {
            if (endWith(STRSTR_IC, ".doc", s) && ( strchr(':',s)<0 || looks(LIKE_FILEPATH,s))) {
                final File f=file(s);
                final BA ba=sze(f)==0?null:new ChCatdoc().docAsBytes(f);
                if (sze(ba)>0) return new ByteArrayInputStream(ba.bytes(), ba.begin(), sze(ba));
            }
            if (endWith(STRSTR_IC,".gz",s)||s.endsWith(".tgz")) return new java.util.zip.GZIPInputStream(is);
            if (endWith(STRSTR_IC,".z",s)) return new charite.christo.libs.UncompressInputStream(is);
            if (endWith(STRSTR_IC,".bz2",s)) {
                if ('B'!=is.read() || 'Z'!=is.read()) {
                    putln("Error in ChUtils.deflaterAccordingToExtension bz2. ",s);
                }
                return new charite.christo.libs.bzip2.CBZip2InputStream(is);
            }
        } catch(Exception e){ putln("caught ChUtils#deflaterAccordingToExtension ",e);}
        return is;
    }
    public final static long STREAM_UNZIP=1<<1;
    public static InputStream inStream(String fileNameOrUrl, long options) {
        if (options>STREAM_UNZIP) assrt();
        if (fileNameOrUrl!=null)
            try {
                final String fn=fileNameOrUrl.trim();
                final InputStream is;
                if (looks(LIKE_EXTURL,fn)) {
                    is=new URL(fn).openStream();
                } else {
                    final File f=file(fn);
                    if (!f.exists()) return null;
                    is=new FileInputStream(f);
                }
                _canDownload=true;
                return (STREAM_UNZIP&options)!=0 ? deflaterAccordingToExtension(new BufferedInputStream(is),fn) : is;
            } catch(Exception e){
                putln("Caught ChUtils#inputStream ",fileNameOrUrl, e);
                /* throws .StringIndexOutOfBoundsException due to holding=idecuhmlib_ */
            }
        return null;
    }
    public static InputStream inStreamT(int timeOutSec, String fn,long options ) {
        final Object params[]={fn, longObjct(options), null};
        final Thread t=thrdCR(instance(0),"INPUT_STREAM",params);
        startThrd(t);
        int time=0;
        while(params[2]==null) {
            sleep(1000);
            if (time++>timeOutSec) {t.interrupt();break;}
        }
        return params[2] instanceof InputStream ? (InputStream)params[2] : null;
    }
    public static String readLne(InputStream is) throws IOException {
        BA sb=null;
        for(int b; (b=is.read())>=0;) {
            if (sb==null) sb=new BA(99);
            if (b=='\n') break;
            if (b!='\r') sb.a((char)b);
        }
        return sb!=null ? sb.toString() : null ;
    }
    public static String[] readLines(File f) { return readLines(inStream(toStrg(f),STREAM_UNZIP)); }
    public static String[] readLines(InputStream is) {
        if (is==null) return NO_STRING;
        final List<String> v=new ArrayList(999);
        final ChInStream chIs=new ChInStream(is,1024);
        final BA LINE=new BA(999);
        try {
            while(chIs.readLine(LINE.clr())) {
                final String line=LINE.toString();
                v.add(line);
                //if (untilStartsWith!=null && line.startsWith(untilStartsWith)) break;
                //if (isEnabled!=null && i%10==0  && !isEnabled.isEnabled(null)) return null;
            }
        }
        finally{closeStrm(is);}
        return strgArry(v);
    }
    /* <<< Read <<< */
    /* ---------------------------------------- */
    /* >>> Write >>> */
    public static boolean copyIsOs(InputStream from, OutputStream to) throws IOException {
        return copyNoClose(from,to, null,null,null);
    }
    private static boolean copyNoClose(InputStream from, OutputStream to,IsEnabled isEnabled, byte stop[][],boolean[] returnIsTruncated) throws IOException {
        if (from==null || to==null) return false;
        if (isEnabled!=null && !isEnabled.isEnabled(ChUtils.class)) return false;
        final byte[][] saved= stop!=null ? new byte[stop.length][] : null;
        final byte[] buffer=new byte[4096];
        int bytes_read;
        boolean success=true;
    readBlock:
        while((bytes_read=from.read(buffer))!=-1) {
            Thread.yield();
            if (isEnabled!=null && !isEnabled.isEnabled(ChUtils.class)) { success=false; break;}
            if (stop!=null) {
                for(int iS=stop.length; --iS>=0;) {
                    final byte[] stp=stop[iS], sav=saved[iS];
                    final int l=sze(stp);
                    if (l==0) continue;
                nextH:
                    for (int h=sav!=null ? -l : 0; h<bytes_read-l; h++) {
                        boolean equ=true;
                        for(int n=0; n<l ; n++) {
                            final byte ch= h+n>=0 ?  buffer[h+n] : sav[l+h+n];
                            if (stp[n]!=ch) { equ=false; break;}
                        }
                        if (equ) {
                            to.write(buffer,0,h+l);
                            if (returnIsTruncated!=null) returnIsTruncated[0]=true;
                            break readBlock;
                        }
                    }
                }
                for(int iS=stop.length; --iS>=0;) {
                    if (saved[iS]==null) saved[iS]=new byte[stop[iS].length];
                    final byte[] sav=saved[iS];
                    for(int n=0; n<sav.length; n++) {
                        final int i=bytes_read-(sav.length-n), i2=n+bytes_read;
                        if (bytes_read>buffer.length) assrt();
                        sav[n]= 0<=i && i<bytes_read ?  buffer[i] : 0<=i2 && i2<sav.length ? sav[i2] : 0;
                    }
                }
            }
            to.write(buffer,0,bytes_read);
        }
        return success;
    }
    public static boolean wrte(File f,byte bb[],int from,int to) {
        OutputStream fo=null;
        final int L=mini(sze(bb),to)-from;
        if (f==null || bb==null || L<0) return false;
        try {
            mkParentDrs(f);
            fo=fileOutStrm(f);
            if (fo!=null) fo.write(bb,from,L);
        } catch(Exception e) { return false; }
        finally{
            closeStrm(fo);
        }
        if (fo!=null) for(int i=0;i<3; i++) if (sze(f)==0) sleep(99);
        wrteCheck(f,L);
        return true;
    }
    private static void wrteCheck(File f, int tL) {
        final int fL=sze(f);
        if (fL!=tL) putln(RED_ERROR," writing file ",f," size is "+fL+" instead of "+tL);
    }
    public static boolean wrte(File f,CharSequence cs) {
        if (f==null || cs==null) return false;
        OutputStream fw=null;
        mkParentDrs(f);
        try{
            fw=fileOutStrm(f);
            if (!wrte(fw,cs)) return false;
        } catch(IOException ex){ return false;}
        finally {
            closeStrm(fw);
            if (fw!=null && sze(cs)>0) afterWrte(f);
        }
        wrteCheck(f,sze(cs));
        return true;
    }
    public static boolean wrte(File f, CharSequence... ss) {
        if (f==null || ss==null) return false;
        OutputStream fw=null;
        mkParentDrs(f);
        int len=0;
        try{
            fw=fileOutStrm(f);
            for(CharSequence cs : ss) {
                if (cs!=null) {
                    wrte(fw,cs);
                    len+=cs.length();
                }
            }
        } catch(IOException ex){ return false;}
        finally {
            closeStrm(fw);
            if (fw!=null && len>0) afterWrte(f);
        }
        return true;
    }
    public static boolean wrte(OutputStream os,CharSequence cs) throws IOException {
        if (os==null||cs==null) return false;
        if (cs instanceof BA)  ((BA)cs).write(os);
        else {
            final String s=toStrg(cs);
            if (s!=null) os.write(toByts(s));
        }
        return true;
    }

    public static void appndToFile(CharSequence s, File f) {
        if (sze(s)==0 || f==null) return;
        OutputStream w=null;
        try {
            mkParentDrs(f);
            toBA(s).write(w=fileAppendStream(f));
        } catch(IOException iox) { puts(onlyOnce(RED_ERROR+"appndToFile "+f+"\n"));}
        closeStrm(w);
    }
    public static OutputStream fileAppendStream(File f) throws IOException { return Insecure.fileOutputStream(f,true); }

    public static OutputStream fileOutStrm(File f) throws IOException {
        mkParentDrs(f);
        return Insecure.fileOutputStream(f,false);
    }
    private static void afterWrte(File f) {
        if (isWin()) {
            for(int i=0;i<3; i++) if (sze(f)==0) sleep(99);
        } else {
            final String name=f.toString();
            if (name.endsWith(".sh") || name.endsWith(".exe") || name.endsWith(".bat") || name.endsWith(".command")) mkExecutabl(f);
        }
    }
    public static boolean closeStrm(Object str) {
        if (str==null) return false;
        try {
            if (str instanceof InputStream) ((InputStream)str).close();
            else if (str instanceof Reader) ((Reader)str).close();
            else if (str instanceof java.net.Socket) ((java.net.Socket)str).close();
            else if (str instanceof OutputStream) {
                try { ((OutputStream)str).flush();}catch(IOException e){}
                ((OutputStream)str).close();
            } else if (str instanceof Writer) {
                try { ((Writer)str).flush();}catch(IOException e){}
                ((Writer)str).close();
            } else assrt();
        }catch(IOException e){ return false; }
        return true;
    }
    /* <<< write File <<< */
    /* ---------------------------------------- */
    /* >>> Jar File >>> */
    private static URL _urlThisJar;
    public static URL urlThisJarFile() {
        URL u=_urlThisJar;
        if (u==null) {
            String res=toStrg(ChUtils.class.getResource("ChUtils.class"));
            res=delLstCmpnt(delPfx("jar:",res),'!');
            res=delSfx("charite/christo/ChUtils.class",res);
            if (res!=null) u=url(res);
            //if (res!=null) try { u=new URL(res); } catch(Exception ex) {} /* Method url() leads to problems in case of Umlaute */
            if (u==null) {
                try {
                    u=ChUtils.class.getProtectionDomain(). getCodeSource().getLocation();
                } catch(Throwable ex) { error(ex,"");}
            }
            if (u==null) u=ERROR_URL;
            _urlThisJar=u;
        }
        return u!=ERROR_URL ? u : null;
    }
    public static File thisJarFile() {
        if (_thisJarFile==null) {
            final  URL u=urlThisJarFile();
            File f=file(FILE_NO_ERROR,u);
            if (sze(f)==0 && endWith(".jar",u)) {
                f=newTmpFile(".jar");
                try {cpy(u.openStream(),f);} catch(Exception e){stckTrc(e);}
            }
            _thisJarFile=sze(f)>0 && endWith(".jar",f) ? f : ERROR_FILE;
        }
        return _thisJarFile!=ERROR_FILE ? _thisJarFile : null ;
    }
    private final static Map<Object,Boolean> _isPCU=new HashMap();
    public static boolean isProxyClassUnavailable(Object classOrClassName) {
        if (!(classOrClassName instanceof String || classOrClassName instanceof Class)) return false;
        Boolean unavailable=_isPCU.get(classOrClassName);
        if (unavailable==null) {
            final String cn;
            final Class c;
            if (classOrClassName instanceof Class) {
                c=((Class)classOrClassName);
                cn=c.getName();
            } else {
                Class c2=null;
                cn=(String)classOrClassName;
                try { c2=Class.forName(cn);} catch(Exception ex) { }
                c=c2;
            }
            final String shrt=shrtClasNam(c);
            unavailable=
                cn!=null && c!=null && shrt!=null &&
                c.getResource(shrt+".class")!=null &&
                c.getResource(delSfx("PROXY",shrt)+".class")==null ? TRUE : FALSE;
            _isPCU.put(cn,unavailable);
            _isPCU.put(c,unavailable);
        }
        return unavailable==TRUE;
    }
    public static File toolsDotJar() {
        final File f=file(delSfx("jre",systProprty(SYSP_JAVA_HOME))+"/lib/tools.jar");
        return sze(f)>0?f:null;
    }
    /* <<< Jar  <<< */
    /* ---------------------------------------- */
    /* >>> ZipFile  >>> */
    private final static Map<String,Object> mapZip=new HashMap();
    public static java.util.zip.ZipFile zipFle(String fPath) {
        final File f=file(fPath);
        if (sze(f)==0) return null;
        synchronized(mapZip) {
            Object zf=deref(mapZip.get(fPath));
            if (zf==ERROR_OBJECT) return null;
            if (zf==null) {
                try {
                    mapZip.put(fPath,newSoftRef(zf=new java.util.zip.ZipFile(f)));
                } catch(Exception ex) {zf=ERROR_OBJECT; return null;}
            }
            return deref(zf,java.util.zip.ZipFile.class);
        }
    }
    public static InputStream zipEntryStrm(String entry, String fPath) {
        return ChZip.inputStream(entry, zipFle(fPath));
    }
    public static File unzip(File f) {
        if (f==null) return null;
        final String fn=f.getAbsolutePath();
        final int i=findSfx(COMPRESS_SUFFIX,fn);
        final String fnDest= i>=0 ? delSfx(COMPRESS_SUFFIX[i],fn) : fn.endsWith(".tgz") ? delSfx(".tgz",fn)+".tar" : null;
        if (fnDest==null) return f;
        final File fDest=file(fnDest);
        cpy(inStream(fn,STREAM_UNZIP),fDest);
        return sze(fDest)>0 ? fDest : null;
    }
    /* <<< ZipFile <<< */
    /* ---------------------------------------- */
    /* >>> Resource >>> */
    public final static int JAVADIR_JARS=1, JAVADIR_PLUGINS=2, JAVADIR_HOTSWAP=3, JAVADIR_RELOADABLE=4, JAVADIR_MAX=4;
    public static File dirJars() { return dirJava(JAVADIR_JARS);}
    public static File dirHotswap() { return dirJava(JAVADIR_HOTSWAP);}
    public static File dirPlugins() { return dirJava(JAVADIR_PLUGINS);}
    private final static File[] DIR_JAVA=new File[JAVADIR_MAX+1];
    public static File dirJava(int type) {
        File f=DIR_JAVA[type];
        if (f==null) {
            final String n=type==JAVADIR_JARS?"jars": type==JAVADIR_PLUGINS?"plugins":type==JAVADIR_HOTSWAP?"hotswap" : null;
            DIR_JAVA[type]=f=n==null ? dirSettings() : new ChFile(dirSettings(),n,isWin());
            if (type==JAVADIR_JARS) {
                mkdrsErr(f,
                         "This directory contains jar-files of downloaded external software.\n"+
                         "If the files are deleted they are downloaded again when required.\n\n"+
                         "You can delete all *.pack.gz. Once converted into .jar they are not needed any more.\n");
            }
        }
        return f;
    }
    public static URL rscAsURL(long options, String s) {
        URL url=CLASSLOADER==null? ClassLoader.getSystemResource(s) : CLASSLOADER.getResource(s);
        for(int i=1;i<=JAVADIR_MAX; i++) {
            if (url==null && 0!=(options& (1<<i))) url=ChZip.getInstance(dirJava(i)).resource(s);
        }
        return url;
    }
    public static InputStream rscAsStream(long options, String path, String fileName) {
        if (path==null || fileName==null) return null;
        final String p=path.replace('.','/').replaceAll("//","/");
        final String s=p.endsWith("/") || sze(p)==0 ? p+fileName : p+"/"+fileName;
        return rscAsStream(options, s);
    }
    public static InputStream rscAsStream(long options, String s) {
        if (CLASSLOADER==null) {
            return ClassLoader.getSystemResourceAsStream(s);
        }
        InputStream is=CLASSLOADER.getResourceAsStream(s);
        for(int i=1; i<=JAVADIR_MAX; i++) {
            if (is==null && 0!=(options&(1<<i))) is=ChZip.getInstance(dirJava(i)).resourceAsStream(s);
        }
        return is;
    }
    public static InputStream tryRscAsStream(Object object) {
        final String s=deref(object, String.class);
        if (strEquls(_CC,s) && nxt(SPC,s)<0 && countChr('/',s, 0, MAX_INT)==1) {
            final String ss[]=splitTokns(s, chrClas1('/'));
            for(int i=ss.length-1; --i>=0;)  ss[i]= ss[i].replace('.','/');
            final String path=new BA(99).join(ss, "/").replace(0L,"//","/").toString();
            return rscAsStream(0L, path);
        }
        return null;
    }
    private final static Map<String,Boolean> map_hasSource=new HashMap();
    public static boolean hasJavaSrc(Object o) {
        final String cn=toStrgIntrn(clasNam(o));
        if (cn==null) return false;
        final Boolean b=map_hasSource.get(cn);
        if (b==TRUE || b==FALSE && cn.startsWith(_CC)) return b==TRUE;
        final String path=cn.replace('.', '/')+".java";
        final URL u=rscAsURL(0XffFF, path);
        map_hasSource.put(cn,u!=null ? TRUE : FALSE);
        return u!=null;
    }
    public static File getJavaSrcFile(Class c) {
        if (c==null) return null;
        final File f=file(rscAsURL(0L, clasNamWithSlash(c)+".java"));
        return fExists(f) ? f : null;
    }
    public static boolean hasHlp(Object o) {
        final String cn=toStrgIntrn(clasNam(o));
        return  cn!=null &&
            (
             CLASS_DesktopUtils==cn ||
             CLASS_EditDna==cn ||
             CLASS_ChJmolPROXY==cn ||
             cn.endsWith("ResidueAnnotationView") ||
             cn.endsWith("StrapAlign") ||
             cn.endsWith("StrapTree") ||
             cn.endsWith("StrapView") ||
             cn.endsWith("Protein3d") ||
             getHlp(o)!=null);
    }
    public static BA getJavaSrc(String cn) {
        final InputStream is=FullClassName.javaSourceAsStream(cn);
        return is!=null ? readBytes(is) :  null;
    }
    public static String fullClassNam(boolean update, String shortOrLongClassName, String[] packages) {
        return shortOrLongClassName==null?null:FullClassName.fullClassName(update, shortOrLongClassName,packages);
    }
    /* <<< Resource <<< */
    /* ---------------------------------------- */
    /* >>> Tokens >>> */
    public static String tokn(Object txt, int from, int to0, boolean[] separators) {
        final byte[] T=txt instanceof byte[] ? (byte[])txt : null;
        final CharSequence cs=T!=null ? null: toCharSeq(txt);
        final boolean sep[]=separators!=null?separators:chrClas(SPC);
        final int t= mini(sze(txt),to0);
        int b=maxi(0,from);
        for( ; b<t; b++) {
            final int c=T!=null?T[b]:cs.charAt(b);
            if (!(c>=0 && c<128 && sep[c])) break;
        }
        int e=b;
        for( ; e<t; e++) {
            final int c=T!=null?T[e]:cs.charAt(e);
            if (c>=0 && c<128 && sep[c]) break;
        }
        return toStrg(txt,b,e);
    }

    public static String[] splitStrg(Object text, char delimiter) {
        final Object txt=text==null || text instanceof CharSequence || text instanceof byte[] ? text : toCharSeq(text);
        if (txt==null) return NO_STRING;
        final int n=countChr(delimiter,txt, 0, MAX_INT)+1, L=sze(txt);
        final String lines[]=new String[n];
        Arrays.fill(lines,"");
        int iCR=-1,count=0,lastCR=-1;
        while(true) {
            iCR=strchr(delimiter,txt,iCR+1,MAX_INT);
            if (iCR<0 && lastCR<L-1) iCR=L; // last line no CR
            if (iCR<0) break;
            lines[count]=toStrg(txt,lastCR+1,iCR);
            count++;
            lastCR=iCR;
        }
        return lines;
    }
    private static Object _bufSplit;
    public final static long  SPLIT_ALLOW_EMPTY_TOKENS=1<<0, SPLIT_TRIM_L=1<<1, SPLIT_TRIM_R=1<<2,SPLIT_TRIM=SPLIT_TRIM_R|SPLIT_TRIM_L, SPLIT_SKIP_WHITE_SPC=1<<3;
    public static String[] splitLnes(Object text)  { return splitTokns(0L, text, 0, MAX_INT, chrClas1('\n'));}
    public static String[] splitTokns(Object text, boolean[] delim) { return splitTokns(0L, text, 0, MAX_INT,  delim);}
    public static String[] splitTokns(Object text) { return splitTokns(0L, text, 0, MAX_INT, chrClas(SPC));}
    public static String[] splitTokns(long options, Object text, int from, int to, boolean[] delim) {
        final int L=mini(to,sze(text));
        if (L==0) return NO_STRING;
        if (text instanceof BA) {
            final BA ba=(BA)text;
            return splitTokns(options, ba.bytes(), ba.begin()+from, ba.begin()+L,  delim);
        }
        final byte[] bb=text instanceof byte[] ? (byte[])text : null;
        final String s= text instanceof String ? (String)text : null;
        final CharSequence cs= text instanceof CharSequence ? (CharSequence)text : null;
        if (bb==null && cs==null && null==s) { assrt(); return NO_STRING;}
        int begin=from;
        int count=0;
        synchronized(SYNC_SPLIT) {
            final String buf0[]=deref(_bufSplit,String[].class);
            String[] buf=buf0;
            for(int i=from; i<L+1;i++) {
                final char c= i==L ? '?' : cs!=null ? cs.charAt(i) : (char)bb[i];
                if (i==L || (delim!=null && c<delim.length && delim[c])) {
                    int end=i;
                    if (0!=(options&SPLIT_TRIM_R)) while(chrAt(end-1,text)==' ') end--;
                    if (0!=(options&SPLIT_TRIM_L)) while(chrAt(begin,text)==' ') begin++;
                    if ( (0!=(options&SPLIT_ALLOW_EMPTY_TOKENS) || end-begin>0) &&
                         (0==(options&SPLIT_SKIP_WHITE_SPC) || nxt(-SPC, text, begin,end)>=0)) {
                        if (buf==null || buf.length<=count) buf=chSze(buf,count*2+100);
                        buf[count++]=substrg(text,begin,end);
                    }
                    begin=i+1;
                }
            }
            if (count==0) return NO_STRING;
            if (buf!=buf0) _bufSplit=newSoftRef(buf);
            {
                final String ss[]=new String[count];
                System.arraycopy(buf,0,ss,0,count);
                return ss;
            }
        }
    }
    public static String[][] toDoubleArry(CharSequence txt, boolean delimLines[], long optsL, boolean delimFields[], long optsF) {
        if (txt==null) return null;
        final String[] lines=splitTokns(optsL, txt, 0, MAX_INT, delimLines);
        final String[][] fff=new String[lines.length][];
        for(int i=lines.length; --i>=0;) fff[i]=splitTokns(optsF, lines[i], 0, MAX_INT, delimFields);
        return fff;
    }
    /* <<< Tokens <<< */
    /* ---------------------------------------- */
    /* >>> Exec >>> */
    public static boolean cmdExists(String...cmd) {
        try { Insecure.runtimeExec(cmd,null,null);} catch(IOException e){ return false;}
        return true;
    }
    public static Process runtimeExec(String arg[]) throws IOException{ return runtimeExec(arg, null,null);}
    public static Process runtimeExec(String arg[], String[] env, File dir) throws IOException{
        if (!Insecure.EXEC_ALLOWED) return null;
        try {
            putln("ChUtils.runtimeExec ",arg);
            return Insecure.runtimeExec((String[])arg, env, dir);
        } catch(Exception e){ putln("ChUtils caught",e," ",arg);}
        return null;
    }
    /* <<< Exec <<< */
    /* ---------------------------------------- */
    /* >>> Count >>> */
    public static int countLettrs(byte text[]) { return countLettrs(text,0,MAX_INT);}
    public static int countLettrs(byte text[],int start,int end) {
        if (text==null) return 0;
        int count=0;
        for(int i=mini(text.length,end);--i>=start;) {
            final int  b=text[i];
            if ('a'<=b && b<='z' || 'A'<=b && b<='Z' ) count++;
        }
        return count;
    }
    public static int[] count01234(int n) {
        final int r[]=new int[n];
        for(int i=n; --i>=0;) r[i]=i;
        return r;
    }
    /*
    public static int[] count43210(int n) {
        final int r[]=new int[n];
        for(int i=n; --i>=0;) r[i]=(n-i-1);
        return r;
    }
    */
    public static int countChr(char c, Object txt, int from, int to) {
        if (txt==null) return 0;
        int iCR=from-1, count=0;
        while(( iCR=strchr(c,txt,iCR+1, to))>=0) count++;
        return count;
    }
    public static int sumOf(int[] num, int toIndex) {
        int sum=0;
        for(int i=mini(toIndex,num.length); --i>=0;) sum+=num[i];
        return sum;
    }
    /* <<< Count  <<< */
    /* ---------------------------------------- */
    /* >>> String >>> */
    public static String plrl(int n, CharSequence txt) {
        if (txt==null) return null;
        CharSequence s=txt;
        final String num=n== 0 ? "no" : n==1 ? "one" : n==2 ? "two" : n==3 ? "three" : toStrg(n);
        if (strEquls("%N",s)) s=toStrg(capitalize(num))+s.subSequence(2, s.length());
        s=strplc(0L,"%N", num, s);
        s=strplc(0L,"%A", n>1?"":"a", s);
        s=strplc(0L,"%S", n>1?"s":"", s);
        s=strplc(0L,"%H", n>1?"have":"has", s);
        return toStrg(s);
    }
    public static String orS(String a, String b) { return sze(a)>0?a : b;}
    public static Object orO(Object a, Object b) { return a!=null?a : b;}
    public static int ftoa(double number,int wInt,int wFrac, byte buffer[], int pos0) {
        final boolean minus=number<0;
        final double f= minus ? -number : number;
        final long integ=(long)f;
        int pos=pos0;
        for(int i=wInt-stringSizeOfInt(integ)-(minus ? 1 : 0); --i>=0;) buffer[pos++]=' ';
        if (minus) buffer[pos++]='-';
        pos+=itoa(integ,buffer,pos);
        if (wFrac>0) {
            final double frac=f- integ;
            buffer[pos++]='.';
            for(int i=1; i<=wFrac; i++) {
                final int dig=((int)(frac*power10(i)))%10;
                buffer[pos++]=(byte)('0'+dig);
            }
        }
        return pos-pos0;
    }
    public static int log2(long n) {
        int r=0;
        for(int i=0;i<64;i++) {
            if (0!=((1L<<i)&n)) r=i;
        }
        return r;
    }
    public static int power10(int exp) {
        switch(exp) {
        case 0: return 1;
        case 1: return 10;
        case 2: return 100;
        case 3: return 1000;
        case 4: return 10000;
        case 5: return 100000;
        case 6: return 1000000;
        case 7: return 10000000;
        case 8: return 100000000;
        case 9: return 1000000000;
        default: return 1000000000;
        }
    }
    public static int stringSizeOfInt(long x0) {
        final long x=x0<0 ? -x0 : x0;
        return
            (x!=x0 ? 1 : 0)+
            (
             x<10 ? 1 :
             x<100 ? 2 :
             x<1000 ? 3 :
             x<10000 ? 4 :
             x<100000 ? 5 :
             x<1000000 ? 6 :
             x<10000000 ? 7 :
             x<100000000 ? 8 :
             x<1000000000 ? 9 :
             x<10000000000L? 10 :
             x<100000000000L? 11 :
             x<1000000000000L? 12 :
             x<10000000000000L? 13 :
             x<100000000000000L? 14 :
             x<1000000000000000L? 15 :
             x<10000000000000000L? 16 :
             x<100000000000000000L? 17 :
             x<1000000000000000000L? 18 :
             19
             );
    }
    /* ---------------------------------------- */
    public static String uCase(String s) { return s!=null ? s.toUpperCase() : null;}
    public static String lCase(String s) { return s!=null ? s.toLowerCase() : null;}

    public final static Map<String,String> _lCase=new HashMap();
    public static String lCaseOnlyLettersDigits(String s, Map<String,String> map) {
        if (s==null) return null;
        final Map<String,String> m=map!=null?map:_lCase;
        String lc=m.get(s);
        if (lc==null) m.put(s,lc=new BA(s.length()).filter(FILTER_TO_LOWER, LETTR_DIGT, s).toString());
        return lc;
    }

    public static char uCase(int c) { return (char)(is(LOWR,c) ? c&~32 : c); }
    public static char lCase(int c) { return (char)(is(UPPR,c) ? c|32 : c); }
    public static CharSequence capitalize(CharSequence s) {
        final int L=sze(s);
        if (L==0) return s;
        final char c0=chrAt(0,s);
        if (is(UPPR,c0) && cntainsOnly(LOWR,s,1,MAX_INT)) return s;
        return uCase(c0)+  substrg(s,1,MAX_INT).toLowerCase();
    }
    private static Object toStrgUF(Object o) {
        return o==null || o instanceof CharSequence || o instanceof byte[] ? o : o instanceof Class ? nam(o) : toStrg(o);
    }
    public static String delAfter(String pattern,Object haystack, boolean last) {
        final String h=toStrg(haystack);
        final int i=h==null||pattern==null ? -1 : last ? h.lastIndexOf(pattern) : h.indexOf(pattern);
        return i>=0 ? h.substring(0,i) : h;
    }
    public static String delSfx(char suffix, Object haystack) {
        final Object h=toStrgUF(haystack);
        if (h==null) return null;
        final int L=sze(h);
        return L==0 ? "" : chrAt(L-1,h)!=suffix ? toStrg(h) : toStrg(h, 0, L-1);
    }
    public static String delPfx(char suffix, Object haystack) {
        final Object h=toStrgUF(haystack);
        if (h==null) return null;
        final int L=sze(h);
        return L==0 ? "" : chrAt(0,h)!=suffix ? toStrg(h) : toStrg(h,1,MAX_INT);
    }
    public static String delSfx(String suffix, Object haystack) {
        final Object h=toStrgUF(haystack);
        return h!=null && suffix!=null && endWith(suffix,h) ? toStrg(h,0,sze(h)-suffix.length()) : toStrg(h);
    }
    public static String delPfx(String prefix, String haystack) {
        final Object h=toStrgUF(haystack);
        return h!=null && prefix!=null && strEquls(prefix,h) ? toStrg(h,prefix.length(), MAX_INT) : toStrg(h);
    }
    public static String addPfx(String prefix, Object txt) {
        if (txt==null) return null;
        return strEquls(prefix, txt) ? toStrg(txt) : prefix+txt;
    }
  public static String addSfx(String suffix, Object txt) {
        if (txt==null) return null;
        return endWith(suffix, txt) ? toStrg(txt) : txt+suffix;
    }

    public static String getSfx(Object text,char sep) {
        final String s=toStrg(text);
        if (s==null) return null;
        final int i= s.lastIndexOf(sep);
        return i<0 ? "" : s.substring(i);
    }
    public static String delFromChar(String s, char c) {
        final int i=s!=null ? s.indexOf(c) : -1;
        return i>=0 ? s.substring(0,i) : s;
    }
    public static String delToLstChr(String s, char c) {
        return s==null?null : s.substring(s.lastIndexOf(c)+1);
    }
    public static String delLstCmpnt(Object text,char sep) {
        final String s=toStrg(text);
        if(s==null) return s;
        final int i=s.lastIndexOf(sep);
        return i<0 ? s: s.substring(0,i);
    }
    public static String lstCmpnt(Object text, char sep) {
        final String s=toStrg(text);
        final int i=s==null ? -1 : s.lastIndexOf(sep);
        return i<0 ? "" :  s.substring(i+1);
    }
    public static String lstPathCmpnt(Object text) {
        final Object s=toStrgUF(text);
        final int i= prev(SLASH,s, sze(s)-1, -1); //==null ? -1 : maxi(s.lastIndexOf('/'),s.lastIndexOf('\\'));
        return i<0 ? "" :  toStrg(s,i+1,MAX_INT);
    }
    public static String delDotSfx(Object o) { return delLstCmpnt(o,'.');}
    public static String dotSfx(Object file) {
        final String n= file!=null ? file.toString() : null;
        if (n==null) return null;
        final int dot=n.lastIndexOf('.');
        return dot>=0 ? n.substring(dot).intern() : null;
    }
    public static String fstTkn(String s) { return wordAt(nxt(-SPC,s),s);}
    public static String lstTkn(CharSequence s) {
        final int L=s==null?0:s.length();
        final int noSpc=L==0?-1 :prev(-SPC,s, L-1, -1);
        if (noSpc<0) return null;
        final int spc=prevE(SPC,s, noSpc, -1);
        return s.subSequence(spc+1,noSpc+1).toString();
    }
    public static String wordAt(int from, Object u) { return wordAt(from,u, -SPC);}
    public static String wordAt(int from, Object u, int charClass) {
        if (u==null || from<0) return null;
        int to=from;
        while(is(charClass,u,to)) to++;
        return  from>=to ? "" :  toStrg(u,from,to);
    }
    public static int idxOfLetters(byte needle[], byte haystack[]) { return idxOfLetters(needle,haystack,0,MAX_INT);}
    public static int idxOfLetters(byte needle[], byte haystk[], int hFrom, int hTo0) {
        final int hTo=mini(hTo0,sze(haystk));
        final int nFrom=nxt(LETTR,needle,0,MAX_INT);
        final int nTo=prev(LETTR,needle,needle.length-1,-1)+1;
        if (nFrom<0 || nFrom>=nTo) return -1;
        final int n0=needle[nFrom]|32;
    nextH:
        for(int iH0=hFrom; iH0<hTo; iH0++) {
            if ( (32|haystk[iH0]) != n0) continue nextH;
            for(int iN=nFrom+1, iH=iH0+1; iN<nTo; iN++, iH++) {
                if (iH>=hTo) continue nextH;
                while(  !is(LETTR, needle,iN) ) if (++iN>=nTo) continue nextH;
                while(  !is(LETTR, haystk,iH) ) if (++iH>=hTo) continue nextH;
                if ((haystk[iH]|32) != (needle[iN]|32) ) continue nextH;
            }
            return iH0;
        }
        return -1;
    }
    public static double variableValueF(String varName, Object haystack, int  hFrom, int hTo, char equalsSign, boolean spaceBeforeEquals, boolean ignoreCase) {
        for(int i=hFrom; (i=strstr(STRSTR_w|(ignoreCase?STRSTR_IC:0L),varName, haystack, i, hTo))>=0; i++) {
            i+=varName.length();
            if (spaceBeforeEquals) i=nxt(-SPC,haystack,i,hTo);
            if (chrAt(i,haystack)==equalsSign) {
                i++;
                if (spaceBeforeEquals) i=nxt(-SPC,haystack,i,hTo);
                final double f=atof(haystack,i,hTo);
                if (!Double.isNaN(f)) return f;
            }
        }
        return Double.NaN;
    }
    /* <<< String <<< */
    /* ---------------------------------------- */
    /* >>> StringBuffer >>> */
    private static char[] appndSync(Object src, int from, int to,  char buf[], Object[] sync, StringBuffer sb) {
        if (src instanceof char[])  sb.append((char[])src,from,to-from);
        else if (src instanceof String && from==0 && to==((String)src).length()) sb.append(src);
        else {
            if (buf==null||buf.length<to-from) buf=ccSoftRef(0,sync,to-from+999);
            toChrs(src, from,to, buf);
            sb.append(buf,0, to-from);
        }
        return buf;
    }
    private final static Object[] SYNC_aRplc={null};
    private static void sbAppndRplc(Object txt, Object replacement,  int[] fromTo, int count, StringBuffer dest) {
        final int tL=sze(txt), rL=sze(replacement);
        final int N=mini(count,sze(fromTo)/2);
        char ccTmp[]=null;
        synchronized(SYNC_aRplc) {
            for(int i=0, lastTo=0; i<=N; i++) {
                final int from=i<N?fromTo[i*2]:tL;
                ccTmp=appndSync(txt, lastTo, from, ccTmp, SYNC_aRplc, dest);
                if (i<N) {
                    ccTmp=appndSync(replacement, 0,rL, ccTmp, SYNC_aRplc, dest);
                    lastTo=fromTo[i*2+1];
                }
            }
        }
    }
    public static String mapId(String id, Map<String,String> m) {
        return m==null ? id : orS(m.get(id),id);
    }
    /* <<< StringBuffer <<< */
    /* ---------------------------------------- */
    /* >>> String-Conversions >>> */
    public static CharSequence toCharSeq(Object object) {
        final Object o=deref(object);
        final ChTextComponents tools=o instanceof JComponent ? ChTextComponents.tools(o) : null;
        if (tools!=null) return tools.byteArray();
        return o instanceof CharSequence ? (CharSequence)o : toStrg(o);
    }
    public static BA toBA(Object object) {
        final Object o=deref(object);
        final ChTextComponents tools=ChTextComponents.tools(o);
        return
            tools!=null ? tools.byteArray() :
            o==null ? null :
            o instanceof BA ? (BA)o :
            o instanceof byte[] ? new BA((byte[])o) :
            new BA(0).a(o);
    }
    public static byte[][] strgs2bytes(Object[] ss) {
        if (ss==null) return null;
        final byte[][] bb=new byte[ss.length][];
        for(int i=ss.length; --i>=0;) {
            final String s=toStrg(ss[i]);
            if (s!=null) bb[i]=toByts(s);
        }
        return bb;
    }
    public static void toChrs(Object src, int from, int toPos,  char dest[]) {
        if (src==null) return;
        if (src instanceof byte[]) {
            final byte[] bb=(byte[])src;
            final int to=mini(toPos, bb.length);
            for(int i=from; i<to; i++) dest[i]=(char)bb[i-from];
        } else if (src instanceof String) {
            final String s=(String)src;
            s.getChars(from, mini(toPos, s.length()), dest, 0);
        } else if (src instanceof StringBuffer) {
            final StringBuffer s=(StringBuffer)src;
            s.getChars(from,  mini(toPos, s.length()), dest, 0);
        } else if (src instanceof BA) {
            final BA ba=(BA)src;
            final byte[] bb=ba.bytes();
            final int B=ba.begin();
            final int to=mini(toPos, ba.end()-B);
            for(int i=from; i<to; i++) dest[i]=(char)bb[i-B-from];
        } else if (src instanceof CharSequence) {
            final CharSequence s=(CharSequence)src;
            final int to=mini(toPos, s.length());
            for(int i=from; i<to; i++) dest[i]=s.charAt(i-from);
        } else {
            assrt();
        }
    }
    private static Object[] mapS2B={null};
    public static byte[] toBytsCached(String s) {
        if (s==null) return null;
        byte bb[];
        synchronized(mapS2B) {
            final Map<String,byte[]> m=(Map)fromSoftRef(0,mapS2B,HashMap.class);
            bb=m.get(s);
            if (bb==null)  m.put(s,bb=toByts(s));
        }
        return bb;
    }
    public static String bytes2strg(byte[] bb) { return bytes2strg(bb,0,MAX_INT);}
    public static String bytes2strg(byte[] bb, int from, int to) {
        final int t=mini(bb!=null ? bb.length:0,to), f=maxi(0,from), N=t-f;
        if (N<=0) return "";
        return new String(bb,0, f, N);
    }
    private final static Object[] SYNC_B2S={null};
    public static byte[] toByts(Object txt) {
        if (txt==null) return null;
        final int L=sze(txt);
        if (L==0) return NO_BYTE;
        if (txt instanceof byte[]) return (byte[])txt;
        if (txt instanceof char[]) {
            final char cc[]=(char[])txt;
            final byte[] bb=new byte[L];
            for(int i=L; --i>=0;) bb[i]=(byte)cc[i];
            return bb;
        }
        if (txt instanceof String) {
            final String s=(String)txt;
            final byte bb[]=new byte[L];
            synchronized(SYNC_B2S) {
                final char cc[]=ccSoftRef(0,SYNC_B2S,L);
                s.getChars(0,L,cc,0);
                for(int i=L; --i>=0;) bb[i]=(byte)cc[i];
            }
            return bb;
        }
        if (txt instanceof BA) {
            final BA ba=(BA)txt;
            final byte T[]=ba.bytes();
            final int B=ba.begin(), E=ba.end();
            if (B!=0 || T.length!=E) {
                final byte[] bb=new byte[E-B];
                System.arraycopy(T,B, bb,0,E-B);
                return bb;
            }
            return T;
        }
        if (txt instanceof CharSequence) {
            final CharSequence s=(CharSequence)txt;
            final byte bb[]=new byte[L];
            for(int i=L; --i>=0;) bb[i]=(byte)s.charAt(i);
            return bb;
        }
        return toByts(new BA(0).a(txt));
    }
    /* <<< String-Conversions <<< */
    /* ---------------------------------------- */
    /* >>> Bit-Sets >>> */
    public static boolean isTrue(boolean[] bb, int i) {  return bb!=null && i>=0 && i<bb.length && bb[i];}
    public static boolean isTrue(String s,final int i) {
        final int i4=i/4, L=sze(s);
        if (L==3 && "all".equals(s)) return true;
        if (L<=i4) return false;
        final char c=s.charAt(i4);
        final int d= c>='a' ? c-('a'-10) : c>='A' ? c-('A'-10) : c>='0' ? c-'0' : 0;
        return (d&(1<<(i%4)))!=0;
    }
    public static boolean trueOrEmpty(Object o) {
        final String s=o instanceof String ? (String)o:null;
        return TRUE.equals(o) || "t".equalsIgnoreCase(s) || "true".equalsIgnoreCase(s);
    }
    public static boolean isTrue(Object o) {
        return TRUE.equals(o) || o instanceof String && ("true".equalsIgnoreCase((String)o) || "t".equalsIgnoreCase((String)o));
    }
    public static boolean[] withoutOffset(boolean bb[], int offset) {
        if (offset==0 || bb==null) return bb;
        if (bb.length+offset<0) return NO_BOOLEAN;
        final boolean selected[]=new boolean[bb.length+offset];
        if (offset>0) System.arraycopy(bb,0,selected,offset,bb.length);
        else System.arraycopy(bb,0,selected,0,bb.length+offset);
        return selected;
    }
    public static boolean looksLikeSet(byte T[], int from, int to, boolean allowColonChain, boolean allowDotAtom) {
        if (nxt(DIGT,T,from,to)<0) return false;
        for(int i=from; i<to;i++) {
            final char c=(char)T[i];
            if (!is(DIGT,c) && c!=',' && c!='-'  &&  !(allowColonChain&&c==':')  &&  !(allowDotAtom&&c=='.') ) {
                if (allowColonChain &&  (is(LETTR,c) || c=='?')  && i>from && T[i-1]==':') continue;
                if (allowDotAtom) {
                    if (c=='*') continue;
                    if (is(UPPR,c) && chrAt(prev(-LETTR_DIGT,T,i, from), T)=='.') continue;
                }
                return false;
            }
        }
        return true;
    }

    public static boolean[] parseSet(Object charSeq, int maxIdx, int returnMinIdx[]) { return _parseSet(charSeq, maxIdx, 0, returnMinIdx);  }
    public static boolean[] parseSet(Object charSeq, int maxIdx, int offset) { return _parseSet(charSeq, maxIdx, offset, null);  }
    private static boolean[] _parseSet(Object charSeq, int maxIdx, int offset0, int returnMinIdx[]) {
        final BA ba=toBA(charSeq);
        if (ba==null) return null;
        final byte T[]=ba.bytes();
        boolean bb[]=null;
        int maxIndex=0, minIndex=MAX_INT, offset=offset0;
        final ChTokenizer st=new ChTokenizer().setDelimiters(chrClas(", "));
        for(int pass=0; pass<2; pass++) {
            int count=0;
            st.setText(ba);
            while(st.nextToken()) {
                final int b=st.from(), e=st.to();
                if (strstr("NaN",T,b,e)>=0) continue;
                if(0==count++ && T[b]=='+') {
                    offset=offset0+atoi(T,b+1,e);
                    continue;
                }
                final int
                    dash=strchr('-',T,b+1,e),
                    from=offset+atoi(T,b,e),
                    to=dash<0?from: '-'==T[e-1] ? maxIdx :  offset+atoi(T,dash+1,e);
                if (from>to) continue;
                if (from<minIndex) minIndex=from;
                if (to>maxIndex) maxIndex=to;
                if (bb!=null) {
                    final int f=maxi(0, returnMinIdx==null?from:from-minIndex);
                    final int t=mini(bb.length-1, returnMinIdx==null?to:to-minIndex);
                    if (f==t) bb[f]=true;
                    else if (f<t) Arrays.fill(bb, f,t+1,true);
                }
            }
            if (bb==null) {
                if (minIndex==MAX_INT) return NO_BOOLEAN;
                bb=new boolean[mini(returnMinIdx!=null ? maxIndex-minIndex : maxIndex, maxIdx)+1];
            }
        }
        if (returnMinIdx!=null) returnMinIdx[0]=minIndex;
        return bb!=null ? bb : NO_BOOLEAN;
    }
    public static int[] parseRange(Object txt,  int from, int to, int buf[]) {
        final int t=mini(sze(txt),to);
        int i1=0,i2=0;
        boolean succsess=false;
        if (from>=t) {}
        else if (succsess=chrAt(from,txt)=='-' && cntainsOnly(DIGT,txt,from+1,t)) i2=atoi(txt,from+1);
        else {
            final char c0=chrAt(from,txt);
            final int dash=strchr('-',txt, from+(c0=='-'?2:1),t);
            if (succsess=dash>0 && looks(LIKE_NUM, txt,from,dash) && (dash==t-1 || looks(LIKE_NUM,txt,dash+1,t))) {
                i1=atoi(txt,from);
                i2=dash==t-1? MAX_INT : atoi(txt,dash+1);
            }
        }
        int[] ft=null;
        if (succsess) {
            ft=buf!=null?buf:new int[2];
            ft[0]=i1;
            ft[1]=i2;
        }
        return ft;
    }
    public static void reverseArray(boolean bb[],int from,int to) {
        for(int i=from,iRev=mini(bb.length,to)-1; i<iRev; i++,iRev--) {
            final boolean bTemp=bb[iRev];
            bb[iRev]=bb[i]; bb[i]=bTemp;
        }
    }
    /* <<< Bit-Sets <<< */
    /* ---------------------------------------- */
    /* >>> Boolean >>> */
    public static boolean[] orBB(boolean aa[], boolean bb[], boolean buf[], int reserve) {
        final int N=maxi(lstTrue(aa),lstTrue(bb));
        final boolean rr[]=sze(buf)>N ? buf: N==0 ? NO_BOOLEAN : new boolean[N+reserve];
        for(int i=rr.length; --i>=0;) {
            rr[i]=i<aa.length && aa[i] || i<bb.length &&bb[i];
        }
        return rr;

    }

    public static int lstTrue(boolean bb[]) {
        if (bb==null) return -1;
        for(int i=bb.length;--i>=0;) if (bb[i]) return i;
        return -1;
    }
    public static int fstTrue(boolean bb[]) { return nxtTrue(bb,0,MAX_INT);}
    public static int nxtTrue(boolean bb[], int from, int to) {
        final int t=mini(sze(bb),to);
        for(int i=from;i<t;i++) if (bb[i]) return i;
        return -1;
    }

    public static int countTrue(boolean bb[]) {
        if (bb==null) return 0;
        int count=0;
        for(int i=bb.length;--i>=0;) if (bb[i]) count++;
        return count;
    }
    public static int countTrue(boolean bb[], int from, int to) {
        if (bb==null) return 0;
        int count=0;
        for(int i=mini(to,bb.length);--i>=from;) if (bb[i]) count++;
        return count;
    }
    public static int nThTrue(int n, boolean bb[]) {
        final int L=sze(bb);
        int count=0;
        for(int i=0; i<L; i++) {
            if (bb[i] && count++==n) return i;
            if (L-i+count<n) return -1;
        }
        return -1;
    }
    public static boolean[] trimArray(boolean bb[],  int first) {
        if (bb==null) return null;
        final int last=lstTrue(bb)+1;
        if (first==0 && last==bb.length) return bb;
        if (first<0 || first>=last) return NO_BOOLEAN;
        final boolean[] bbNew=new boolean[last-first];
        System.arraycopy(bb,first,bbNew,0,last-first);
        return bbNew;
    }
    public static boolean[] setTrue(int from, int to, boolean boolArray[]) {
        final boolean[] bb=sze(boolArray)<to?new boolean[to] : boolArray;
        Arrays.fill(bb, from, mini(bb.length, to), true);
        return bb;
    }
    public static String boolToTxt(boolean bb[]) {
        return new BA(22).boolToText(bb,0,",", "-").toString();
    }
    public static long boolToLong(boolean bb[]) {
        long r=0;
        for(int i=sze(bb); --i>=0;) if (bb[i]) r|= (1L<<i);
        return r;
    }

    public static boolean boolEquls(boolean[] bb1, int from1, int to1,  boolean[] bb2, int from2, int to2) {
        final int t1=mini(sze(bb1),to1), t2=mini(sze(bb2), to2), mi=mini(from1,from2);
        int i1=from1, i2=from2;
        if (mi<0) {
            if (nxtTrue(bb1,0,i1-=mi)>=0) return false;
            if (nxtTrue(bb2,0,i2-=mi)>=0) return false;
        }
        while(i1<t1 && i2<t2) if (bb1[i1++]!=bb2[i2++]) return false;
        if (nxtTrue(bb1,i1,t1)>=0) return false;
        if (nxtTrue(bb2,i2,t2)>=0) return false;
        return true;
    }
    public static boolean boolOffstEquls(boolean[] bb1, int o1, boolean[] bb2, int o2) {
        final int ma=mini(o1,o2);
        if (!boolEquls(bb1,ma-o1, MAX_INT, bb2, ma-o2, MAX_INT)) return false;
        return nxtTrue(bb1,0,ma-o1)<0 && nxtTrue(bb1,0,ma-o1)<0;
    }

    /* <<< Boolean <<< */
    /* ---------------------------------------- */
    /* >>> Properties >>> */
    public static void prprtsToFile(Properties p, File f) {
        if (f!=null && p!=null) {
            OutputStream fOut=null;
            try {
                mkParentDrs(f);
                p.store(fOut=fileOutStrm(f),null);
            } catch(Exception e){ putln("caught ChUtils#store properties ",e); }
            finally { closeStrm(fOut);}
        }
    }
    public final static String PROPERTY_FILE="~/@/myProperties.txt";
    private static Properties _prpties;
    private static Properties properties() {
        if (_prpties==null) {
            _prpties=new Properties();
            InputStream is=null;
            final File f=file(PROPERTY_FILE);
            if (sze(f)>0) {
                try {
                    _prpties.load(is=new FileInputStream(f));
                } catch(Exception e){ putln("caught ChUtils#properties() ",e); }
                finally{ closeStrm(is);}
            }
        }
        return _prpties;
    }
    private static int _prptyMC;
    public static void setPrpty(Class clas,String key,boolean bool) { setPrpty(clas,key,bool ? "true":"false");}
    public static void setPrpty(Class clas,String key,int v) { setPrpty(clas,key,toStrg(v));}
    public static void setPrpty(Class clas,String key,String defaulT) {
        if (clas!=null && key!=null)  {
            properties().setProperty(clas.getName()+"."+key,defaulT);
            _prptyMC++;
        }
    }
    public static String getPrpty(Class clas,String key,String defaulT) { return properties().getProperty(clas.getName()+"."+key,defaulT); }
    public static int getPrpty(Class clas,String key,int defaulT) {
        return atoi(properties().getProperty(clas.getName()+"."+key,toStrg(defaulT)),0);
    }
    public static boolean getPrpty(Class clas,String key,boolean defaulT) {
        return isTrue(properties().getProperty(clas.getName()+"."+key,defaulT ? "true":"false"));
    }
    /* <<< Properties <<< */
    /* ---------------------------------------- */
    /* >>> Encoding >>> */
    /**
       32 bit given as an int value are converted to a String of 4 characters.
       In protein model Strings of 4 characters are encoded as an int and can be decoded with this method.
    */
    public static char[] bit32To4chars(int bit32,char[] cc4) {
        if (cc4==null) cc4=new char[4];
        final int l=cc4.length;
        if (l>3) cc4[3]=(char)(((bit32 & 0xff000000)>>24));
        if (l>2) cc4[2]=(char)(((bit32 & 0x00ff0000)>>16));
        if (l>1) cc4[1]=(char)(((bit32 & 0x0000ff00)>>8));
        if (l>0) cc4[0]=(char)(( bit32 & 0x000000ff));
        return cc4;
    }
    public final static byte[] HEX2INT=new byte[256], INT2HEX=new byte[256];
    static {
        Arrays.fill(HEX2INT,(byte)-1);
        Arrays.fill(INT2HEX,(byte)-1);
        for(byte i= 0;i<10;i++) {
            HEX2INT['0'+i]=i;
            INT2HEX[i]=(byte)('0'+i);
        }
        for(byte i=10;i<16;i++) {
            HEX2INT['A'-10+i]=HEX2INT['a'-10+i]=i;
            INT2HEX[i]=(byte)('A'-10+i);
        }
    }
    public final static int
        FILTER_FIRST_OPTION=1<<8,
        FILTER_NO_MATCH_TO=1<<8,
        FILTER_TO_UPPER=1<<9, FILTER_TO_LOWER=1<<10, FILTER_UNIX_TO_DOS=1<<11,
        FILTER_HTML_DECODE=1<<12, FILTER_HTML_ENCODE=1<<13, FILTER_BACKSPACE=1<<14, FILTER_URL_ENCODE=1<<15, FILTER_FOLD=1<<16, FILTER_TRIM=1<<17,
        FILTER_NO_HTML_BODY=1<<18, FILTER_QUOTE_TO_HTML=1<<19, FILTER_LT_GT_4TT=1<<20,
        FILTER_DECODE_PSQL_OCTAL=1<<21;
    private final static Object[] SYNC_FILTR={null};
    public static String filtrS(long filter,  int match, Object txt) { return toStrg(filtr(filter,match, txt, 0, MAX_INT));}
    public static byte[] filtrBB(long filter, int match, Object txt) { return toByts(filtr(filter,match, txt, 0, MAX_INT));}
    public static Object filtr(long filtr, int match, Object txt, int from, int to) {
        final int L=sze(txt);
        if (L==0) return txt;
        synchronized(SYNC_FILTR) {
            if (txt instanceof String) {
                final String s=(String)txt;
                if (match==0 && filtr==FILTER_HTML_DECODE && s.indexOf('&')<0) return txt;
            }
            final BA ba=baSoftClr(SYNC_FILTR).filter(filtr,match,txt,from,to);
            return sze(ba)!=sze(txt) || 0!=(ba.getOptions()&BA.OPTION_IS_FILTERED) ? ba.toString() : txt;
        }
    }
 private static String[] _rplcToUnicode;
    public static CharSequence greekToUnicode(CharSequence txt) {
        if (_rplcToUnicode==null) {
            final String s2=
                "\u03B1 alpha \u03B2 beta \u03B3 gamma \u03B4 Delta \u03B5 epsilon \u03B6 zeta \u03B7 eta \u03B8 Theta \u03B9 iota "+
                "\u03BA kappa \u03BB Lamda \u03BE Xi \u03BF omicron \u03C0 Pi \u03C1 rho \u03C3 Sigma \u03C4 tau \u03C5 upsilon "+
                "\u03C6 Phi \u03C7 Chi \u03C8 Psi \u03C9 Omega";
            final String ss[]=splitTokns(s2);
            final ArrayList v=new ArrayList(222);
            for(int i=0; i<ss.length; i+=2) {
                final String s=ss[i+1], sLc=s.toLowerCase(), gUC=ss[i].toUpperCase();
                v.add(ss[i]);
                v.add(sLc);
                if (s!=sLc) {
                    v.add(gUC);
                    v.add(s.toUpperCase());
                    v.add(gUC);
                    v.add(s);
                }
            }
            _rplcToUnicode=strgArry(v);
        }
        CharSequence s=txt;
        if (s!=null) {
            final String[] ss=_rplcToUnicode;
            for(int i=0; i<ss.length; i+=2) s=strplc(STRPLC_FILL_RIGHT|STRSTR_w|STRPLC_16BIT, ss[i+1], ss[i], s);
        }
        s=rmAllChars((char)0, s, 0, MAX_INT);
        return s;
    }
    /* <<< Encoding <<< */
    /* ---------------------------------------- */
    /* >>> Download >>> */
    private final static Map<String,int[]> _countDownloads=new HashMap();
    private static ChRunnable downloadLogger;
    public static void setDefaultDownloadLogger(ChRunnable r) { downloadLogger=r;}
    public static void sayDownloading(ChRunnable log, String msg) {
        final ChRunnable r=log!=null ? log : downloadLogger;
        if (r!=null) r.run(ChRunnable.RUN_SAY_DOWNLOADING, msg);
    }
    public static Runnable thread_urlGet(URL url, int ageDays) { return thread_urlGet(url, ageDays,  (File[])null, downloadLogger); }
    public static Runnable thread_urlGet(URL url, int ageDays, File ffEquivalent[],  ChRunnable log) {
        return thrdM("urlGet",ChUtils.class, new Object[]{url,intObjct(ageDays), ffEquivalent, log});
    }
    public static File urlGet(URL url, int ageDays) { return urlGet(url,ageDays, (File[])null, downloadLogger);   }
    public static File urlGet(URL url, int ageDays, File ffEquivalent[],  ChRunnable log) {
        if (url==null) return null;
        final File f=_urlGet(url,ageDays,ffEquivalent,log);
        final String sUrl=toStrg(url);
        if (sze(f)==0) {
            for(String s : getAlternativeUrls(sUrl)) {
                if (sUrl.equals(s)) continue;
                final File f2=_urlGet(url(s),ageDays,ffEquivalent,log);
                if (sze(f2)>0) return f2;
            }
        }
        return f;
    }
    private static File _urlGet(URL url, int ageDays, File ffEquivalent[], ChRunnable log) {
        final File f=file(url);
        if (f==null || (sze(f)>0 && (currentTimeMillis()-f.lastModified())/(1000*60*60*24)<ageDays)  ) return f;
        for(int i=sze(ffEquivalent); --i>=0;) if (sze(ffEquivalent[i])>0) return ffEquivalent[i];
        int count[]=null;
        try {
            final String s=toStrg(url);
            synchronized(_countDownloads) {
                count=_countDownloads.get(s);
                if (count==null) _countDownloads.put(s,count=new int[1]);
                sleep(99*count[0]*count[0]);
                count[0]++;
            }
            synchronized(toStrgIntrn(f)) {
                delFile(f);
                mkParentDrs(f);
                final Object ff[]={f,log};
                if (log!=null) startThrd(thrdCR(instance(0),"WATCH_DOWNLOAD",ff));
                downloadAndDecompress(s,f);
                ff[0]=null;
            }
            if (log!=null && sze(f)>0) log.run(ChRunnable.RUN_DOWNLOAD_FINISHED,f);
            return f;
        } finally { count[0]=maxi(count[0]--,0); }
    }

    public static void logDownload(URL src, File f, String comment) {
        if (src==null || f==null) return;
        if (_fLogDL==null) _fLogDL=file(dirLog()+"/downloads.log");
        final BA sb=new BA(333);
        if (sze(f)==0) sb.aln("Files downloaded from the Internet\n");
        sb.a(comment).a('\t').a(src).a('\t').aln(fPathUnix(f));
        appndToFile(sb, _fLogDL);
    }
    public static File unattendedDownload(URL url) {
        if (url==null) return null;
        final String sURL=toStrg(url);
        final int slash=sURL.indexOf('/',10);
        final String server=slash>0 ? sURL.substring(0,slash) : sURL;
        final File f=file(sURL), fTmp=file(f+"TMP");
        logDownload(url,fTmp,"unattendedDownload");
        synchronized(mkIdObjct("CU$$US",server)) { cpy(inStreamT(15,sURL, 0L),fTmp); }
        renamFileOrCpy(fTmp,f);
        return f;
    }
    private final static Map<String,AnonymousFTPPROXY> mapFTP=new HashMap();
    private final static byte ENDMDL[][]={"\nENDMDL".getBytes()}; // Beispiel 2JQU 1AXJ
    static boolean useEdtFTP(String src) {
        return Insecure.CLASSLOADING_ALLOWED && strEquls("ftp://",src) && (isSelctd(buttn(TOG_EDTFTP)) || /* src.startsWith("ftp://ftp.genome.jp") || */ src.startsWith("ftp://ftp.embl-heidelberg.de"));
    }
    public static void downloadAndDecompress( String src, File fDest) { downloadAndDecompress(src,fDest,(BA)null,(IsEnabled)null);}
    public static void downloadAndDecompress( String src,final File fDest, BA log,IsEnabled isEnabled) {
        if (fDest==null || src==null || fDest.length()>0 || (isEnabled!=null && !isEnabled.isEnabled(null))) return;
        if (log!=null) log.a("ChUtils.downloadAndDecompress ").a(src).a(" ==> ").aln(fDest).send();
        final String fnDest=fDest.getName();
        mkParentDrs(fDest);
        String srcSuffix=null,destSuffix=null;
        for(String suffix:COMPRESS_SUFFIX) {
            if (fnDest.endsWith(suffix)) destSuffix=suffix;
            if (src.endsWith(suffix)) srcSuffix=suffix;
        }
        final File fDownloadTmp=file(fDest+TMP_DOWNLOAD_SFX);
        delFile(fDownloadTmp);
        final URL url=url(src);
        logDownload(url,fDest,"downloadAndDecompress ");
        if (url==null) return;
        if (src.startsWith("ftp://")) {
            AnonymousFTPPROXY ftp=null;
            if (useEdtFTP(src)) {
                if (log!=null) log.a("Going to initialize edtftpj: ").send();
                ftp=mapFTP.get(url.getHost());
                if (ftp==null) {
                    final AnonymousFTPPROXY ftp0=new AnonymousFTPPROXY();
                    if (ftp0.proxyObject()!=null) mapFTP.put(url.getHost(),ftp=ftp0);
                }
                if (log!=null) log.a("edtftpj ").aln(ftp!=null?GREEN_SUCCESS:"failed").send();
            }
            if (ftp!=null) {
                final File fDownload= (srcSuffix!=destSuffix)  ? file(fDest+srcSuffix) : fDest;
                delFile(fDownload);
                ftp.download(url,fDownloadTmp,log);
                if (fDownloadTmp.length()>0) {
                    if (log!=null) log.a("AnonymousFTPPROXY ").a(GREEN_SUCCESS).aln(fDownloadTmp.getName()).send();
                    renamFileOrCpy(fDownloadTmp,fDownload);
                    if (fDownload!=fDest) unzip(fDownload);
                }
            }
        }
        if (fDest.length()+fDownloadTmp.length()==0) {
            if (log!=null) log.a("Using ChUtils.inputStream for ").aln(src).send();
            final byte[][] STOP=
                !getPrpty(ChUtils.class,PROP_NMR_ONLY_FIRST_MODEL,true) ? null :
                looks(LIKE_EXTURL,src) && (fnDest.endsWith(".ent") || fnDest.endsWith(".pdb")) ? ENDMDL :
                null;
            final InputStream is=inStreamT(99, src,  srcSuffix!=destSuffix ? STREAM_UNZIP : 0L);
            _canDownload=true;
            OutputStream fo=null;
            if (is!=null) {
                final boolean[] isTruncated= STOP==ENDMDL ? new boolean[1]: null;
                boolean success=false;
                try {
                    fo=fileOutStrm(fDownloadTmp);
                    success=copyNoClose(is,fo,isEnabled,STOP,isTruncated);
                } catch(IOException e){ putln("ChUtils#downloadAndDecompress ",e);}
                finally {
                    if (isTruncated!=null && isTruncated[0]) {
                        final String msg="\nREMARK   only the first model has been downloaded!\n";
                        putln("\n",fDest," ",msg);
                        try {fo.write(msg.getBytes());} catch(Exception e){}
                    }
                    closeStrm(is);
                    closeStrm(fo);
                    if (fDownloadTmp.length()>0) _canDownload=true;
                    if (success && sze(fDownloadTmp)>0) renamFileOrCpy(fDownloadTmp,fDest);
                }
            }
        }
        final String ex=dotSfx(fDest);
        if (sze(fDest)>0 && (".zip".equalsIgnoreCase(ex) || ".jar".equalsIgnoreCase(ex))) {
            if (!ChZip.checkIntegrityOfZipFile(fDest, null))
                error("The zip file "+fDownloadTmp+" <br>is corrupt.<br>Please delete it!");
        }
    }
    private static boolean _canDownload;
    public static boolean isHttpResponse404(File f) {
        if (sze(f)==0 || sze(f)>30000) return false;
        InputStream is=null;
        try {
            is=new FileInputStream(f);
            final byte bb[]=new byte[2048];
            is.read(bb);
            final int idx=strstr(STRSTR_IC,"not found</title>",bb);
            return bb[0]=='<' && idx>0;
        } catch(IOException e) { return false;}
        finally{closeStrm(is);}
    }
    /* <<< Downloading <<< */
    /* ---------------------------------------- */
    /* >>> Networking >>> */
    private static Map<String,String[]> altUrl;
    public static void addAlternativeUrl(String alternative, String url) {
        if (sze(alternative)*sze(url)==0 || alternative.equals(url)) return;
        if (altUrl==null) altUrl=new HashMap();
        mapOneToMany(alternative, url, altUrl, 0, String.class);
    }
    public static String[] getAlternativeUrls(String url) {
        String aa[]=null;
        if (url!=null && altUrl!=null) aa=altUrl.get(url);
        return aa!=null?aa:NO_STRING;
    }
    public static String getHeaderFld(String field, URL u, int timeOut_ms) {
        final Object params[]={field,u,null,null};
        final Thread t=thrdCR(instance(0),"gHF",params);
        t.setDaemon(true);
        startThrd(t);
        final long time=currentTimeMillis();
        while(params[2]==null) {
            sleep(3);
            if (currentTimeMillis()-time>timeOut_ms) {
                t.interrupt();
                break;
            }
        }
        return (String)params[2];
    }
    public static String getHeaderFld(String field, URL u) {
        if (u!=null) {
            try {
                final java.net.URLConnection c=u.openConnection();
                if (c!=null) {
                    c.setUseCaches(false);
                    return c.getHeaderField(field);
                }
            } catch(Exception ex) { putln("caught getHeaderField url=",u, ex);}
        }
        return null;
    }
    public static int getContentLen(URL url) { return url==null ? 0 : atoi(getHeaderFld("Content-Length", url)); }
    public static int isUpToDate(File f, URL u) {
        if (f==null || u==null) return -1;
        if (f.length()==0) return 0;
        final long[] time=lastModifiedMsHttpHeader(u,5);
        if (time==null || time[0]<=0) {
            return -1;
        }
        final long ageServer=time[1]-time[0];
        final long ageFile=currentTimeMillis()-f.lastModified();
        /*
        if (myComputer()) {
            final BA sb=new BA(99)
                .a("\n isUpToDate ").a(f).a(' ').a(u)
                .a("\n    ageServer = ").format10(time[1]/1000,8).a(" - ").format10(time[0],12).a(" - ").format10(ageServer,12)
                .a("\n age-local=").format10(ageFile,12)
                .a("\n   http-Date         =").format10(time[1],12)
                .a("\n   http-Last-modified=").format10(time[0],12)
                .a("\n ageFile-ageServer =").format10(ageFile-ageServer,8);
            putln(sb);
            putln("ageFile-ageServer"+(ageFile-ageServer));
        }
        */

        return ageFile<=ageServer ? 1 : 0;
    }
    public static long[] lastModifiedMsHttpHeader(URL u, int timeOutSec) {
        final Object params[]={u,null};
        final Thread t=thrdCR(instance(0),"LM_HTTP",params);
        startThrd(t);
        int time=0;
        while(params[1]==null) {
            sleep(1000);
            if (time++>timeOutSec) {t.interrupt();break;}
        }
        return params[1] instanceof long[] ? (long[])params[1] : null;
    }
    public static long[] lastModifiedMsHttpHeader(URL u) {
        if (u==null) return null;
        try{
            final java.net.URLConnection c=u.openConnection();
            c.setUseCaches(false);
            final String lm=c.getHeaderField("Last-Modified");
            final String date=c.getHeaderField("Date");
            final long ms_lm=sze(lm)>0 ? Date.parse(lm) : -1;
            final long ms_st=sze(date)>0 ? Date.parse(date) : -1;
            if (ms_lm>ms_st && myComputer()) {
                putln(RED_ERROR," in lastModifiedMsHttpHeader url=",u);
                putln(" lm=",  lm);
                putln(" date=",date);
            }
            return new long[]{ms_lm,  ms_st};
        } catch(Exception ex) { putln("caught lastModifiedMsHttpHeader ",u, ex);}
        return null;
    }
    public static URL url(Object txt) {
        if (txt==null || txt instanceof URL) return (URL)txt;
        if (txt instanceof File) try { return ((File)txt).toURL(); }catch(Exception e){}
        final String s0=rplcToStrg(DO_NOT_ASK, "", toStrgTrim(txt));
        if (s0!=null) {
            final int daysCache=s0.indexOf(KEEP_DAYS_IN_CACHE);
            final String s=daysCache>0?s0.substring(0,daysCache):s0;
            final File f=!strEquls("file:/",s) && !looks(LIKE_EXTURL,s) && looks(LIKE_FILEPATH_EVALVAR,s) ? file(s) : null;
            try { return f!=null ?  f.toURL() : new URL(s); }catch(Exception e){}
        }
        return null;
    }
    public static int keepDaysInCache(CharSequence url) {
        final int i=strstr(STRSTR_AFTER,KEEP_DAYS_IN_CACHE,url);
        return i>0 ? atoi(url, i) : 99999;
    }
    public static String urlDecode(String s) {
        if (s!=null && (s.indexOf('+')>=0 || s.indexOf('%')>=0)) {
            try { return java.net.URLDecoder.decode(s);} catch(IllegalArgumentException e){}
        }
        return s;
    }
    public static CharSequence jnlpDecode(CharSequence s) {
        return strplc(0L,"JNLPAMP","&", strplc(0L,"JNLPGT",">", strplc(0L,"JNLPLT","<",strplc(0L,"JNLPLINEBREAK","            \n",strplc(0L,"JNLPFRAGEZEICHEN","?",s)))));
    }
    public static URL[] urls(String txt) {
        Collection v=null;
        for(String s : splitTokns(txt)) v=adUniqNew(url(delLstCmpnt(s,'|')),v);
        return toArryClr(v,URL.class);
    }
    /* <<< Network <<< */
    /* ---------------------------------------- */
    /* >>> Proxy >>> */
    private static Object _proxyS;
    public static java.net.InetSocketAddress proxyIsaForUrl(URL url) {
        if (javaVsn()==14) return null;
        final java.net.Proxy p= (java.net.Proxy)proxyForUrl(url);
        return p!=null && p.type()!=java.net.Proxy.Type.DIRECT ? (java.net.InetSocketAddress)p.address() : null;
    }
    public static Object proxyForUrl(URL url) {
        if (_proxyS==null) {
            _proxyS=invokeMthd("getDefault","java.net.ProxySelector");
            if (_proxyS==null) _proxyS=ERROR_OBJECT;
        }
        java.net.URI uri=null;
        try {
            uri=url!=null && _proxyS!=ERROR_OBJECT ? new java.net.URI(toStrg(url)) : null;
        } catch(Exception e){}
        if (uri!=null) {
           final List v=(List)invokeMthd(0L,"select",_proxyS, oo(uri));
            final int n=sze(v);
            for(int i=0; i<n; i++) {
                final java.net.Proxy proxy=(java.net.Proxy)get(i,v);
                if (proxy!=null && proxy.type()!=java.net.Proxy.Type.DIRECT) return proxy;
                // ?  null : (java.net.InetSocketAddress)proxy.address();
            }
        }
        return null;
    }
    public static String testProxySelector() {
        final BA sb=new BA(999);
        for(String u: custSettings(Customize.testProxySelector)){
            final java.net.InetSocketAddress p=proxyIsaForUrl(url(u));
            sb.a(u).a(" ==> ").aln(p!=null?p:"None");
        }
        return sb.toString();
    }
    public static String[] commandLineParametersForProxy() {
        final java.net.InetSocketAddress  proxyGoogle=proxyIsaForUrl(url("http://www.google.com"));
        if (proxyGoogle==null) return NO_STRING;
        final String[] vPara=new String[3];
        vPara[0]="-DproxyHost="+proxyGoogle.getHostName();
        vPara[1]="-DproxyPort="+proxyGoogle.getPort();
        final Set<String> v=new HashSet();
        for(String url : Hyperrefs.getDatabases(Hyperrefs.WEB_LINK|Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE)[1]) {
            final int to=mini(strchr(STRSTR_E,'/',url,7,MAX_INT),strchr(STRSTR_E,'?',url));
            v.add(url.substring(0,to));
        }
        BA sb=null;
        for(Object[] urls : new Object[][]{v.toArray(), custSettings(Customize.testProxySelector)}) {
            for(Object o : urls) {
                final String u=toStrgTrim(o);
                if (u!=null && u.startsWith("http://")) {
                    final URL url=url(u);
                    if (url!=null && proxyIsaForUrl(url)==null) {
                        (sb==null?sb=new BA("-Dhttp.nonProxyHosts="):sb.a('|')).a(url.getHost());
                    }
                }
            }
        }
        if (sb!=null) vPara[2]=sb.toString();
        return rmNullS(vPara);
    }
    /* <<< Proxy <<< */
    /* ---------------------------------------- */
    /* >>> flow control >>> */
    public static boolean sleep(long i) {
        return sleep(i,"sleep");
    }
    public static boolean sleep(long i,String msg) {try {Thread.sleep(i);} catch (Exception e) { return false;} return true; }
    private static Collection<Runnable> _vShutDown;
    public static void addShutdownHook1(Runnable r) { _vShutDown=adUniqNew(r,_vShutDown);}
    public static void shutDwn(int forceAfterMs) {
        if (forceAfterMs>0) ChDelay.runAfterMS(0L, thrdM("exit",System.class,new Object[]{intObjct(0)}), forceAfterMs);
        tellShutDown("undockSave ...");
        undockSave();
        tellShutDown(null);
        if (_prptyMC>0) prprtsToFile(properties(), file(PROPERTY_FILE));
        for(Runnable r : toArry(_vShutDown, Runnable.class)) {
            if (r==null) continue;
            tellShutDown(r.getClass()+".run()");
            r.run();
        }
        if (sze(_appName)>0) {
            final int on=(int)((currentTimeMillis()-TIME_AT_START)/1000);
            final File f=file(dirTmp()+"/last_"+appName()+".txt");
            tellShutDown("write "+f+"...");
            wrte(f,(on/60)+':'+(on%60)+"."+_totalClicks);
        }
        if (!isSelctd(BUTTN[TOG_KEEP_TMP_FILES])) {
            int i=0;
            final int N=sze(vDelOnExit);
            for(String path : strgArry(vDelOnExit)) {
                if ( (i++ % 20)==0) tellShutDown("del "+i+"/"+N);
                deleteTree(new File(path));
            }
        }
        Customize.customize(Customize.trustExe).save(false);
        if (_debugHtml!=null) htmlTidy(new BA(99999).join(_debugHtml,"\n<br>\n"));
        System.exit(0);
    }
    public static void stopAllAlignments() { stopAll(CanBeStopped.vALIGNMENTS);}
    private static void stopAll(List v) {
        for(Object ref : v.toArray()) {
            final Object o=deref(ref);
            if (o instanceof CanBeStopped) ((CanBeStopped)o).stop();
            else if (o instanceof boolean[]) {
                final boolean isInterrupted[]=(boolean[])o;
                if (isInterrupted.length>0) isInterrupted[0]=true;
            }
        }
        v.clear();
    }
    /* <<< flow control <<< */
    /* ---------------------------------------- */
    /* >>> Program parameters >>> */
    public final static String OPT_WITHOUT_EQ=new String();
    public final static Map<String,String> MAP_ARGV=new HashMap();
    private static String _prgrParam[], _prgrOpts[], _appName, _forgotPara=" Forgot setPrgParameters()";
    public final static int PARA_INTRPRT=1<<0, PARA_RDIRCT=1<<1;
    public static String[] prgParas() {
        if (_prgrParam==null) {
            if (_forgotPara!=null) putln(RED_ERROR,_forgotPara);
            _forgotPara=null;
            return NO_STRING;
        }
        return _prgrParam;
    }

    public static String[] prgOpts() {
        final String ss[]=prgParas();
        if (ss.length==0) return NO_STRING;
        if (_prgrOpts==null) {
            final String[] oo=new String[ss.length];
            int n=0;
            for(String s : ss) if (chrAt(0,s)=='-') oo[n++]=s;
            _prgrOpts=chSze(oo,n);
        }
        return _prgrOpts;
    }
    public static void setPrgParameters(long options, String applicationName, String[] argv) {
        _appName=applicationName;
        if (_prgrParam!=argv) {
            _prgrParam=argv;
            _prgrOpts=null;
            for(String a : argv) {
                if (chrAt(0,a)!='-') continue;
                final int eq=a.indexOf('=');
                final String afterEqu=eq<0?null:toStrgTrim(jnlpDecode(a.substring(eq+1)));
                MAP_ARGV.put(eq<0?a:a.substring(0,eq), eq<0?OPT_WITHOUT_EQ:afterEqu);
            }
        }
        if (0!=(options&PARA_INTRPRT)) {
            for(int earlyLate=2; --earlyLate>=0;) {
                for(String a0 : argv) {
                    if (a0==null) continue;
                    final String a=a0.trim();
                    if (earlyLate!=0  && CacheResultJdbc.processPara(a)) continue;
                    interpretPrgPara(earlyLate!=0, a);
                }
            }
        }
        if (0!=(options&PARA_RDIRCT) && withGui()) {
            if (!prgOptT("-stderr")) ChStdout.redirect('R');
            if (!prgOptT("-stdout")) ChStdout.redirect('O');
        }
    }
    static String appName() { return _appName;}
    public final static String PRG_OPTS[]={
        "-noDisclaimer", "-noCache", "-noSound", "-noClassloading", "-customizeAdd",
        "-edtftp", "-laf=", "-spellcheckWords=", "-sysProxies", "-probeWebProxy", "-cp=", "-useInstalledSoftware",
        "-askUpload", "-askExec", "-stackTrace", "-log3d", "-logFinalize", "-debug=", "-stdout", "-stderr", "-allowFileModification"};

    public static boolean isPrgOpt(String a) {
        final int aL=a==null?0:a.length();
        if (aL!=0) {
            for(String p : PRG_OPTS) {
                final int L=p.length(), eq=p.charAt(L-1)=='=' ? L-1 : L;
                if ((L==aL && L==eq || aL>eq+1 && a.charAt(eq)=='=') && strEquls(p,a,0)) return true;
            }
        }
        return false;
    }
    public static int idxOfOption(String needle, String haystack[]) {
        if (haystack==null || needle==null) return  idxOf(needle,haystack);
        for(int i=0; i<haystack.length; i++) {
            final String h=haystack[i];
            if (needle.equals(h) || chrAt(strstr(STRSTR_AFTER,needle,h),h)=='=') return i;
        }
        return -1;
    }
    public static boolean interpretPrgPara(boolean early, String a) {
        int B=0;
        if (a==null || a.length()==0 || a.charAt(0)=='J' && a.matches("JVM[0-9]*_")) return true;
        if (a.charAt(0)=='-') {
            final int eq=a.indexOf('=');
            final String a0=toStrgIntrn(eq<0?a:a.substring(0,eq+1));
            final String afterEqu=eq<0?null:toStrgTrim(jnlpDecode(a.substring(eq+1)));
            final boolean isT=optT(a);
            if (a.equals("-debug=H")) setCheckHtml(true);
            else if (a0=="-keepFiles") buttn(TOG_KEEP_TMP_FILES).s(isT);
            else if (a0=="-noSound")   buttn(TOG_SOUND).s(!isT);
            else if (a0=="-askExec")   buttn(TOG_NASK_EXEC).s(!isT);
            else if (a0=="-askUpload") buttn(TOG_NASK_UPLOAD).s(!isT);
            else if (a0=="-stackTrace") mapStckTrace=new HashMap();
            else if (a0=="-noCache") CacheResult.setEnabled(!isT);
            else if (a0=="-useInstalledSoftware") {if (isT) _isSystProprty[IS_USE_INSTALLED_SOFTWARE]='t'; }
            else if (a0=="-logFinalize") {         if (isT) logFinalize("TRUE",""); }
            else if (a0=="-probeWebProxy") {       if (isT && early) startThrd(Web.thread_autoProbeProxy()); }
            else if (a0=="-sysProxies") {          if (isT && early) sysSetPrptrty("java.net.useSystemProxies", "true"); }
            else if (a0=="-edtftp") buttn(TOG_EDTFTP).s(isT);
            else if (a0=="-mlock") buttn(TOG_LOCK_JMENU).s(isT);
            else if (eq>0 && a0=="-laf") LAFChooser.setLAF(afterEqu,false);
            else if (eq>0 && a0=="-user.home") _systProprty[SYSP_USER_HOME]=afterEqu;
            else if (eq>0 && a0=="-spellcheckWords") startThrd(ChJOrthoPROXY.thread_addDictionaries(splitTokns(afterEqu)));
            else if (eq>0 && a0=="-benchmark") { for(int i=sze(afterEqu); --i>=0;) BENCHMARKS[afterEqu.charAt(i)]=true; }
            else if (eq>0 && a0=="-proxycfgOutput") {
                if (early) {
                    final BA ba=readBytes(file(FILE_NO_ERROR, delPfx('"',delSfx('"',afterEqu))));
                    if (sze(ba)==0) {
                        putln(RED_ERROR, "Option ",a);
                        putln("Cannot read file ",afterEqu);
                    } else Web.proxycfgOutput(ba);
                }
            }
            else return false;
            return true;
        }
        return false;
    }
    public static boolean prgOptT(String opt) {
        final int L=opt==null?0:opt.length();
        if (L<2) return false;
        final String[] argv=prgOpts();
        final char c1=opt.charAt(1);
        for(int i=sze(argv); --i>=0;) { /* Backward Loop allows overriding */
            final String s=argv[i];
            final int l=s==null?0:s.length();
            if (l<2 || s.charAt(1)!=c1 || !s.startsWith(opt)) continue;
            if (l==L || opt.charAt(L-1)=='=' || s.charAt(L)=='=') return optT(s);
        }
        return false;
    }

    public static boolean optT(String s) {
        if (s==null) return false;
        final int eq=s.indexOf('='), L=s.length(), c1=L>eq+1 ? s.charAt(eq+1) : 0;
        return eq<0 || L==eq+2  && c1=='t' ||  L==eq+5  && (32|c1)=='t' && strEquls(STRSTR_IC,"true",s,eq+1);
    }

    /* <<< PrgParas <<< */
    /* ---------------------------------------- */
    /* >>> Installed Software >>> */

    private final static String LOCAL_DIRS[]={"/usr/share/java/","/usr/share/strap/", "/usr/lib/strap/"};
    public static File installdFile(String s) {
        if (s==null || !isSystProprty(IS_USE_INSTALLED_SOFTWARE)) return null;
        for(String dir : LOCAL_DIRS) {
            File f;
            if ( 0<sze(f=file(dir+s))) {
                //putln("installdFile"+ANSI_GREEN+" Nehme "+f+ANSI_RESET);
                return f;
            }
        }
        return null;
    }
    public static boolean sysSetPrptrty(String key, String val) {
            try {
                System.setProperty(key, val);
                return true;
            } catch(Exception ex){}
        return false;
    }

    /* <<< Installed Software <<< */
    /* ---------------------------------------- */
    /* >>> Collections >>> */
    private final static Object[] vRmAll={null};
    public static boolean rmAll(Object remove[], List v) {
        if (remove==null || v==null) return false;
        synchronized(vRmAll) {
            synchronized(v) {
                final List vRm=vClr(vRmAll);
                adAllUniq(remove,vRm);
                return v.removeAll(vRm);
            }
        }
    }
    public static <T extends Object> T lstEl(Object collection, Class<T> c) {
        return get(sze(collection)-1,collection,c);
    }
    public static String applyDict(String s0, Map<String,String> m) {
        final String s= m!=null && s0!=null ? m.get(s0) : null;
        return s!=null ? s : s0;
    }
    public static boolean remov(Object o, Collection v) {
        if (v==null) return false;
        synchronized(v) { return v.remove(o);}
    }
    public static boolean cntains(Object o, Collection v) {
        if (v==null || v.size()==0) return false;
        synchronized(v) { return v.contains(o); }
    }
    public static void clr(Collection v) {
        if (v!=null && v.size()>0) synchronized(v) {v.clear();}
    }
    public static Object remov(int i, List v) {
        if (v!=null && i>=0 && i<v.size()) {
            synchronized(v) {
                try { return v.remove(i); } catch(Exception ex){}
            }
        }
        return null;
    }
    public static String getS(int i, Object collection) { return get(i,collection, String.class); }
    public static Object getRmNull(int i, Object collection) { return getRmNull(i,collection, Object.class);}
    public static <T extends Object> T getRmNull(int i, Object collection, Class<T> c) {
        final T o=get(i,collection,c);
        if (o==null && i>=0 && i<sze(collection)) {
            try {
            if (collection instanceof List) ((List)collection).remove(i);
            else if (collection instanceof Object[]) ((Object[])collection)[i]=null;
            } catch(Exception ex) {}
        }
        return o;
    }
    public static <T extends Object> T get(int i, Object collection, Class<T> c) {
        if (i<0 || collection==null) return null;
        final Object o=deref(get(i,collection));
        return o==null || c==null || c==Object.class || c==String.class && o instanceof String || isInstncOf(c,o) ? (T)o : null;
    }
    public static boolean adAllUniq(Object[] oo, Collection v) { return adAll(true, oo,v); }
    public static boolean adAll(Object[] oo, Collection v) { return adAll(false, oo,v); }
    static boolean adAll(boolean uniq, Object[] oo, Collection v) {
        boolean changed=false;
        if (oo!=null && v!=null) {
            synchronized(v) {
                if (v instanceof ArrayList) ((ArrayList)v).ensureCapacity(oo.length);
                for(Object o : oo) {
                    if (o!=null && (!uniq || !v.contains(o))) changed|=v.add(o);
                }
            }
        }
        return changed;
    }
    public static boolean adNotNull(Object o, Collection v) {
        if (v==null||o==null) return false;
        synchronized(v) { return v.add(o); }
    }
    public static boolean adUniq(Object o, Collection v) {
        if (v==null||o==null) return false;
        synchronized(v) { return v instanceof Set || !v.contains(o) ? v.add(o) : false; }
    }
    public static <T> void adUniqR(T o, int row, List<T> v) {
        if (o==null || v==null) return;
        synchronized(v) {
            final int iSrc=v.indexOf(o), iDst=mini(sze(v), maxi(0,row));
            if (iSrc!=iDst) {
                v.remove(o);
                v.add(mini(sze(v),iDst),o);
            }
        }
    }
    public static Collection adUniqNew(Object o, Collection v0) {
        if (o==null) return null;
        final Collection v=v0!=null?v0: new ArrayList();
        if (!v.contains(o)) v.add(o);
        return v;
    }
    public static Collection adNotNullNew(Object o, Collection v0) {
        if (o==null) return null;
        final Collection v=v0!=null?v0: new ArrayList();
        v.add(o);
        return v;
    }

    public static int modic(Object o) { return o instanceof HasMC ? ((HasMC)o).mc() : 0; }
    /* >>> Collections >>> */
    /* ---------------------------------------- */
    /* >>> Sort >>> */
    public final static int COMPARE_ALPHABET=1,  COMPARE_ALPHABET_IC=2, COMPARE_SIZE=4, COMPARE_NAME=5, COMPARE_ID=6, COMPARE_REVERT=7,
        COMPARE_WHEN_CREATED=8,
        COMPARE_WHEN_MODIFIED=9,
        COMPARE_RENDERER_TEXT=10;
    public static int compareAlphabetically(Object o1, Object o2, boolean ignoreCase) { return ChComparator.compareAlphabetically(o1,o2,ignoreCase);}
    private final static Comparator[] COMPARATOR=new Comparator[99];
    public static Comparator comparator(int type) {
        if (COMPARATOR[type]==null) COMPARATOR[type]=new ChComparator(type);
        return COMPARATOR[type];
    }
    public static  <T extends Object> T[] inferOrdr(T[] toBeChanged, Class<T> clazz, Object current[], Object ordered[]) {
        final int n=sze(toBeChanged);
        if (n==0) return emptyArray(clazz);
        if (n!=sze(current) || n!=sze(ordered)) {
            assrt();
            return toBeChanged;
        }
        final T[] newArray=(T[])newInstance(clazz,n);
        for(int i=0; i<n; i++) {
            for(int j=0; j<n; j++) {
                if (current[i]==ordered[j] && newArray[j]==null) {
                    newArray[j]=toBeChanged[i];
                    break;
                }
            }
        }
        return newArray;
    }
    /* <<< Sort <<< */
    /* ---------------------------------------- */
    /* >>> Arrays  >>> */

    public final static Object[] shuffl(Object aa[]) {
        final int N=sze(aa);
        for(int i=N; --i>=0;) {
            final int j=(int)(Math.random()*N);
            if (i!=j && j<N) {
                final Object o=aa[i];
                aa[i]=aa[j];
                aa[j]=o;
            }
        }
        return aa;
    }
    public static String[] wrdsIntTxt(BA txt, boolean delim[]) {
        final Set<String> v=new HashSet();
        final ChTokenizer T=new ChTokenizer().setText(txt).setDelimiters(delim);
        while(T.nextToken()) v.add(T.asString());
        return strgArry(v);

    }

    public static byte[] nNull(byte bb[]) { return bb==null?NO_BYTE:bb; }
    public static String[] nNull(String bb[]) { return bb==null?NO_STRING:bb; }
    public static boolean arraysEqul(Object aa[], Object bb[]) {
        final int n=sze(aa);
        if (n!=sze(bb)) return false;
        for(int i=n; --i>=0;) if (aa[i]!=bb[i]) return false;
        return true;
    }
    public static boolean arraysEqul(Class clazz, Object mm1[], Object mm2[]) {
        int i1=0, i2=0;
        while(i1<mm1.length || i2<mm2.length) {
            Object m1=null, m2=null;
            while((m1=get(i1,mm1,clazz))==null && i1<mm1.length) i1++;
            while((m2=get(i2,mm2,clazz))==null && i2<mm2.length) i2++;
            i1++;
            i2++;
            if (m1!=m2) return false;
        }
        return true;
    }
    public static void sortArry(Object o) { sortArry(o,null);}
    public static void sortArry(Object o, Comparator comp) {
        final List v=o instanceof List ? (List)o : null;
        final Object oo[]=o instanceof Object[] || v!=null ? oo(o) : null;
        if (sze(oo)==0) return;
        if (comp!=null) Arrays.sort(oo,comp); else Arrays.sort(oo);
        if (v!=null) {
            for(int i=oo.length; --i>=0;) {
                try {v.set(i,oo[i]);} catch(Exception ex) {putln(RED_ERROR," caught in sortArry",ex);}
            }
        }
    }
    public static String[] adToStrgs(String item, String collection[], int reserve) {
        final int L=sze(collection);
        for(int i=0;i<L;i++) {
            if (collection[i]==null) {
                collection[i]=item;
                return collection;
            }
        }
        final String ss[]=chSze( L==0 ? NO_STRING:collection, L+1+reserve);
        ss[L]=item;
        return ss;
    }
    public static int[] adToArray(int item, int collection[], int reserve) {
        final int L=sze(collection);
        for(int i=0;i<L;i++) {
            if (collection[i]==-1) {
                collection[i]=item;
                return collection;
            }
        }
        final int ss[]=chSze(collection, L+1+reserve);
        Arrays.fill(ss,L+1,ss.length,-1);
        ss[L]=item;
        return ss;
    }
    public static <T extends Object> T[] adToArray(T item, T collection[], int reserve, Class<T> clazz) {
        final int L=sze(collection);
        for(int i=0;i<L;i++) {
            if (collection[i]==null) {
                collection[i]=item;
                return collection;
            }
        }
        final Object ss[]=chSze(collection, L+1+reserve, clazz);
        ss[L]=item;
        return (T[])ss;
    }

   public static int maxSze(Object oo[]) {
        int len=0;
        for(int i=sze(oo) ;--i>=0;) len=maxi(len,sze(oo[i]));
        return len;
    }
    public static int countNull(Object[] oo) {
        int count=0;
        if (oo!=null) for(Object o : oo) if (o==null) count++;
        return count;
    }
    public static int countNotNull(Object[] oo) { return oo==null ? 0 : oo.length-countNull(oo); }
    private final static Map<Class, Object> mapEmpty=new HashMap();
    public static <T extends Object> T[] emptyArray(Class<T> clazz) {
        final Object empty=
            clazz==String.class ? NO_STRING :
            clazz==File.class ? NO_FILE :
            clazz==URL.class ? NO_URL :
            clazz==Map.class ? NO_MAP :
            clazz==Map.Entry.class ? NO_MAP_ENTRY :
            clazz==Class.class ? NO_CLASS :
            clazz==Object.class ? NO_OBJECT :
            null;
        if (empty!=null) return (T[])empty;
        Object oo=mapEmpty.get(clazz);
        if (oo==null) mapEmpty.put(clazz,oo=newInstance(clazz,0));
        return (T[])oo;
    }
    final static int C_ARRAYe=1, C_ARRAYks=2, C_ARRAYkb=3, C_ARRAYk=4, C_ARRAYv=5, C_MAP_name2o=6;

    public static String[] toStrgArray(Object oo[]) {
        if (oo instanceof String[]) return (String[]) oo;
        final int count=countNotNull(oo);
        if (count==0) return NO_STRING;
        final String ss[]=new String[count];
        int j=0;
        for(Object o : oo) {
            final String s=toStrg(o);
            if (s!=null) ss[j++]=s;
        }
        return ss;
    }
    public static String[] strgArry(Object o) {
        if (o!=null) {
            if (o instanceof Collection) synchronized(o) {
                    if (((Collection)o).size()==0) return NO_STRING;
                    if (o instanceof UniqueList) return (String[])((UniqueList)o).asArray();
                    return (String[])((Collection)o).toArray(NO_STRING);
                }
            if (o instanceof String[]) return (String[])o;
            if (o instanceof CharSequence || o instanceof byte[]) return new String[]{toStrg(o)};
            assrt();
        }
        return NO_STRING;
    }

    public static <T extends Object> T[] toArryClr(Collection v, Class<T> clazz) { final T[] tt=toArry(v,clazz); clr(v); return tt;}

    public static <T extends Object> T[] toArry(Collection v, T[] nullArray) {
        if (v==null||v.size()==0) return nullArray;
        synchronized(v) {
            final Class c=nullArray.getClass().getComponentType();
            return (T[])toArry(v,c);
        }
    }

    public static <T extends Object> T[] toArry(Collection v, Class<T> clazz) {
        if (v==null) return emptyArray(clazz);
        synchronized(v) {
            if (v instanceof UniqueList) {
                final Class c=((UniqueList)v).getClazz();
                if (c==null || clazz==null || clazz.isAssignableFrom(c)) return ((UniqueList<T>)v).asArray();
            }
            return (T[]) (v.size()==0 ? emptyArray(clazz) :  v.toArray(emptyArray(clazz)));
        }
    }
    public static Map.Entry[] entryArry(Map m) {
        if (m==null) return NO_Entry;
        final Object[] cached= m instanceof ChMap ? ((ChMap)m).cached() : null;
        Object vv=cached!=null ? cached[C_ARRAYe]:null;
        if (vv==null) vv=toArry(m.entrySet(),Map.Entry.class);
        if (cached!=null) cached[C_ARRAYe]=vv;
        return (Map.Entry[])vv;
    }
    public static void rmKeyNullMappings(Map m) {
        if (m==null || m instanceof ChMap) return;
        final Set es=m.entrySet();
        for(Map.Entry e : entryArry(m)) {
            if (e.getValue()==null) es.remove(e);
        }
    }
    public static String[] keyStrgArry(Map m) {
        rmKeyNullMappings(m);
        if (sze(m)==0) return NO_STRING;
        final Object[] cached= m instanceof ChMap ? ((ChMap)m).cached() : null;
        Object vv=cached!=null ? cached[C_ARRAYks]:null;
        if (vv==null) vv=strgArry(m.keySet());
        if (cached!=null) cached[C_ARRAYks]=vv;
        return (String[])vv;
    }
    public static byte[][] keyByteArry(Map m) {
        if (m==null) return NO_BYTE_ARRAY;
        final Object[] cached= m instanceof ChMap ? ((ChMap)m).cached() : null;
        Object vv=cached!=null ? cached[C_ARRAYkb]:null;
        if (vv==null) vv=strgs2bytes(keyStrgArry(m));
        if (cached!=null) cached[C_ARRAYkb]=vv;
        return (byte[][])vv;
    }
    private static <K extends Object>  K[] k_v_Arry(Map m, Class<K> c, char k_or_v) {
        if (sze(m)==0) return emptyArray(c);
        if (m instanceof ChMap) return (K[]) ((ChMap)m).toArrayKV(k_or_v=='k', (ChRunnable)null, "");
        return (K[]) rmNullA(toArry(k_or_v=='k' ? m.keySet():m.values(),c));
    }
    public static Object[] keyArry(Map m) { rmKeyNullMappings(m); return keyArry(m, Object.class); }
    public static Object[] valueArry(Map m) { return valueArry(m,Object.class);}
    public static <K extends Object, K2 extends K> K[]   keyArry(Map<K2,?> m, Class<K> c) { return  k_v_Arry(m, c, 'k');}
    public static <V extends Object, V2 extends V> V[] valueArry(Map<?,V2> m, Class<V> c) { return  k_v_Arry(m, c, 'v');}

    public static boolean setToNull(Object needle, Object[] haystack) {
        boolean changed=false;
        if (haystack!=null && needle!=null) {
            for(int i=haystack.length; --i>=0;) {
                if (haystack[i]==needle) {
                    haystack[i]=null;
                    changed=true;
                }
            }
        }
        return changed;
    }
    public static Object[] rmNullA(Object oo[]) { return rmNullA(oo, Object.class);}
    public static String[] rmNullS(String oo[]) { return rmNullA(oo, String.class);}
    public static <T extends Object> T[] rmNullA(T[] tt, Class<T> clazz) {
        final int count=countNull(tt);
        if (count>0) {
            final T[] newArray=(T[])newInstance(clazz,tt.length-count);
            int j=0;
            for(T t : tt) if (t!=null) newArray[j++]=t;
            return newArray;
        }
        return tt;
    }
    public static <T extends Object> T[] rmNullNewInst(T[] tt, Class<T> clazz) {
        final T[] nn=rmNullA(tt,clazz);
        return nn==null ? null : nn!=tt ? nn : nn.clone();
    }
    public static void rmNull(List v) {
        for(int i=v!=null ? v.size() : 0; --i>=0;) {
            try {
                Object o=v.get(i);
                while(o instanceof Reference) o=((Reference)o).get();
                if (o==null) v.remove(i);
            } catch(Exception ex){}
        }
    }
    public static boolean rplcA(Object needle, Object rplc,  Object haystack[]) {
        if (rplc==needle) return false;
        boolean changed=false;
        for(int i=sze(haystack); --i>=0;) if (haystack[i]==needle) { changed=true; haystack[i]=rplc; }
        return changed;
    }
    public static <T extends Object> T[] set1stElement(T item, T collection[], int reserve, Class<T> clazz ) {
        if (item==null || collection!=null && collection.length>0 && collection[0]==item) return collection;
        T[] coll=collection;
        int idx=idxOf(item,coll);
        if (idx<0) idx=idxOf(item,coll=ad(item,collection, reserve, clazz));
        if (idx<0) assrt();
        final T item0=coll[0];
        coll[0]=coll[idx];
        coll[idx]=item0;
        return coll;
    }
    public static <T extends Object> T[] ad(T item, T collection[], int reserve, Class<T> clazz ) {
        final int L=sze(collection);
        for(int i=0;i<L;i++) {
            if (collection[i]==null) {
                collection[i]=item;
                return collection;
            }
        }
        final T ss[]=chSze( L==0 ? NO_STRING:collection, L+1+reserve,clazz);
        ss[L]=item;
        return ss;
    }
    public static void addRef(String key, Object ref, Object target) {
        if (!(ref instanceof Reference)) assrt();
        else pcp(key, ad((Reference)ref, gcp(key,target,Reference[].class),4,  Reference.class), target);
    }
public static String rplcByListElement(String s, Object collection) {
        if (collection==null) return s;
        else if (collection instanceof String[]) {
            final String ss[]=(String[])collection;
            final int i=idxOfStrg(s,ss);
            return i>=0 ? ss[i] : s;
        } else if (collection instanceof List) {
            final List v=(List)collection;
            final int i=v.indexOf(s);
            return i>=0 ? (String)v.get(i) : s;
        }
        return s;
    }
    public final static long IDX_OF_INST_NOT=1<<1;
    public static int idxOfInst(long options, Class clazz, Object haystack) {
        final Object hs=deref(haystack);
        if (hs==null || clazz==null) return -1;
        if (hs instanceof Object[] || hs instanceof List) {
            final int L=sze(hs);
            final boolean not=0!=(options&IDX_OF_INST_NOT);
            for(int i=0; i<L; i++) {
                final Object h=get(i,hs);
                if (h==null) continue;
                if (!not  == isAssignblFrm(clazz, h.getClass())) return i;
            }
            return -1;
        }
        return isAssignblFrm(clazz,hs) ? 0:-1;
    }
    public static <T extends Object> T fstInstOf(Class<T> clazz, Object haystack) {
        if (haystack==null || clazz==null) return null;
        if (clazz.isAssignableFrom(haystack.getClass())) return (T)haystack;
        final int L=haystack instanceof Object[] || haystack instanceof List ? sze(haystack):0;
        for(int i=0; i<L;i++) {
            final Object o=get(i,haystack);
            if (o!=null && isAssignblFrm(clazz,o.getClass())) return (T)o;
        }
        return null;
    }
   public static int idxOf(Object o,Object oo[]) { return idxOf(o,oo,0,MAX_INT);}
    public static int idxOf(Object o,Object oo[],int idx, int to) {
        if (oo!=null) {
            if (o instanceof Integer || o instanceof Character) assrt();
            final int toIndex=mini(oo.length,to);
            for(int i=idx;i<toIndex; i++)  if (o==(oo[i])) return i;
        }
        return -1;
    }
    public static int idxOf(int o,int oo[]) { return idxOf(o,oo, 0,MAX_INT); }
    public static int idxOf(int o,int oo[], int from, int to) {
        if (oo!=null) {
            final int t=mini(oo.length,to);
            for(int i=from;i<t;i++) if (o==(oo[i])) return i;
        }
        return -1;
    }

    public static int idxOfStrg(String needle, String haystack[]) { return idxOfStrg(0L, needle,haystack); }
    public static int idxOfStrg(long options, String needle, String haystack[]) {
        if (haystack==null || needle==null) return -1;
        final int hc=needle.hashCode(), nL=needle.length();
        for(int i=0; i<haystack.length; i++) {
            final String h=haystack[i];
            if (h==null) continue;
            if (0!=(options&STRSTR_BEGIN)) {
                if (strEquls(options, needle, h, 0)) return i;
            } else if (0!=(options&STRSTR_END)) {
                if (strEquls(options, needle, h, h.length()-nL)) return i;
            } else if (0!=(options&STRSTR_IC)) {
                if (needle.equalsIgnoreCase(h)) return i;
            } else {
                if (h.hashCode()==hc && needle.equals(h)) return i;
            }
        }
        return -1;
    }
    public static int idxOfStrg(byte[] needle, String haystack[], int from, int to0, int strLen[]) {
        final int to=mini(to0,haystack.length);
        for(int i=from; i<to; i++) {
            final String h=haystack[i];
            if (h!=null && strLen[i]==needle.length && strEquls(h,needle,0)) return i;
        }
        return -1;
    }
    public static boolean cntainsRef(Object o, Object oo[]) {
        if (o==null) return false;
        for(int i=sze(oo); --i>=0;) if (oo[i]==o || deref(oo[i])==o) return true;
        return false;
    }
    public static boolean cntains(Object o, Object oo[]) { return idxOf(o,oo,0,MAX_INT)>=0;}
    public static boolean cntainsAll(Object needles[], Object haystack[]) {
        if (haystack==null) return false;
        if (needles!=null) for(Object o : needles)  if (!cntains(o,haystack)) return false;
        return true;
    }
    public static boolean cntainsAll(Object needles, Object haystack[]) {
        if (haystack==null) return false;
        for(int i=sze(needles); --i>=0;) {
            final Object o=get(i,needles);
            if (o!=null && !cntains(o, haystack)) return false;
        }
        return true;
    }
    public static boolean cntainsAtLeastOne(Object needles[], Object haystack) {
        if (haystack==null || haystack==null || needles==null) return false;
        final Collection v=deref(haystack, Collection.class);
        final Object hh[]=deref(haystack, Object[].class);
        if (haystack!=null &&  hh==null && v==null) assrt();
        for(Object o : needles)  {
            if (o!=null && (v!=null && v.contains(o) || hh!=null && cntains(o,hh))) return true;
        }
        return false;
    }
    public static <T extends Object> T[] delet(Object element, Object[] aa, Class<T> clazz) { return delet(element,aa,clazz,false); }
    public static <T extends Object> T[] delet(Object element, Object[] aa, Class<T> clazz, boolean extended) {
        if (aa==null) return null;
        int count=aa.length;
        for(Object a:aa) if (a==element) count--;
        if (count==aa.length) return (T[])aa;
        final Object bb[]=(Object[])newInstance(clazz,count);
        count=0;
        for(Object a:aa) if (a!=element && count<bb.length) bb[count++]=a;
        return (T[])bb;
    }
    public static int szeVA(Object o) { return o instanceof List ? ((List)o).size() : o instanceof Object[] ? ((Object[])o).length : 0; }
    public static int sze(Object o) {
        final int ret=
            o==null ? 0 :
            o instanceof int[] ? ((int[])o).length :
            o instanceof byte[] ? ((byte[])o).length :
            o instanceof boolean[] ? ((boolean[])o).length :
            o instanceof short[] ? ((short[])o).length :
            o instanceof long[] ? ((long[])o).length :
            o instanceof float[] ? ((float[])o).length :
            o instanceof double[] ? ((double[])o).length :
            o instanceof char[] ? ((char[])o).length :
            o instanceof Object[] ? ((Object[])o).length :
            o instanceof CharSequence ? ((CharSequence)o).length() :
            o instanceof Collection ? ((Collection)o).size() :
            o instanceof Map ? ((Map)o).size() :
            o instanceof File ? (isDir((File)o) ? 0 : (int)((File)o).length()) :
            o instanceof JList ? sze(((JList)o).getModel()) :
            o instanceof JComboBox ? sze(((JComboBox)o).getModel()) :
            o instanceof ListModel ? ((ListModel)o).getSize() :
            o instanceof JTextComponent ? ((JTextComponent)o).getDocument().getLength() :
            -1;
        if (ret<0 && myComputer()) {
            putln(RED_ERROR,clazz(o));
            assrt();
        }
        return maxi(0,ret);
    }
    public static int[] chSze(int[] a, int n) {
        if (a!=null && n==a.length) return a;
        final int[] b=new int[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static long[] chSze(long[] a,int n) {
        if (a!=null && n==a.length) return a;
        final long[] b=new long[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static String[] chSze(String[] a,int n) {
        if (a!=null && n==a.length) return a;
        final String[] b=new String[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static short[] chSze(short[] a,int n) {
        if (a!=null && n==a.length) return a;
        final short[] b=new short[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static byte[] chSze(byte[] a,int n0) {
        final int n=n0>=0?n0:0;
        if (a!=null && n==a.length) return a;
        final byte[] b=new byte[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static byte[][] chSze(byte[][] a,int n) {
        if (a!=null && n==a.length) return a;
        final byte[][] b=new byte[n][];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static boolean[] chSze(boolean[] a,int n) {
        if (a!=null && n==a.length) return a;
        final boolean[] b=new boolean[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static float[] chSze(float[] a,int n) {
        if (a!=null && n==a.length) return a;
        final float[] b=new float[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static double[] chSze(double[] a,int n) {
        if (a!=null && n==a.length) return a;
        final double[] b=new double[n];
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static <T extends Object> T[] chSze(Object[] a,int n, Class<T> clazz) {
        if (a!=null && n==a.length) return (T[])a;
        final T[] b=(T[])newInstance(clazz,n);
        if (a!=null) System.arraycopy(a,0,b,0,mini(a.length,n));
        return b;
    }
    public static boolean[] redim(boolean[] ii,int n,int reserve) {
        boolean jj[]=ii;
        if (jj==null || jj.length<=n) jj=new boolean[n+reserve];
        return jj;
    }
    public static float[] redim(float[] ii,int n,int reserve) {
        float jj[]=ii;
        if (jj==null || jj.length<=n) jj=new float[n+reserve];
        return jj;
    }
    public static double[] redim(double[] ii,int n,int reserve) {
        double jj[]=ii;
        if (jj==null || jj.length<=n) jj=new double[n+reserve];
        return jj;
    }
    public static byte[] redim(byte[] ii,int n,int reserve) {
        byte jj[]=ii;
        if (jj==null || jj.length<=n) jj=new byte[n+reserve];
        return jj;
    }
    public static int[] redim(int[] ii,int n,int reserve) {
        int jj[]=ii;
        if (jj==null || jj.length<=n) jj=new int[n+reserve];
        return jj;
    }
    public static <T extends Object> T[] subArry(int from, int to, T[] oo,  int reserve, Class c) {
        if (oo==null) return null;
        final int f=maxi(0,from), t=mini(oo.length,to);
        final T[] tt=(T[])newInstance(c,t-f+reserve);
        System.arraycopy(oo,f, tt,0,tt.length);
        return tt;
    }
    private final static Object[] JOINA={null,null};
    public static  <T extends Object> T[] joinArrays(T[] a1, T[] a2, Class<T> c) {
        final int n1=sze(a1), n2=sze(a2);
        if (n1+n2==0) return (T[])newInstance(c,0);
        if (cntainsAll(a1,a2)) return a2;
        if (cntainsAll(a2,a1)) return a1;
        synchronized(JOINA) {
            final Collection v=n1*n1+n2*n2<1000 ? vClr(0,JOINA) : hashSetClr(1,JOINA);
            for(Object a : a1) adUniq(a,v);
            for(Object a : a2) adUniq(a,v);
            return toArryClr(v,c);
        }
    }

    public static Object[] oo(Object o) {
        if (o instanceof Collection) {
            synchronized(o) { return o instanceof UniqueList ? ((UniqueList)o).asArray() : ((Collection)o).toArray(); }
        }
        return o==null ? NO_OBJECT : o instanceof Object[] ? (Object[]) o : new Object[]{o};
    }
    private final static Object[] FLATTEN={null};
    public static <T extends Object> T[] flattenA(Class<T>clazz, boolean uniq, Object objects) {
        synchronized(FLATTEN) {
            return toArryClr(flattenA(clazz,vClr(FLATTEN),uniq, oo(objects)),clazz);
        }
    }
    public static Object[] flattenA(Object objects) {
        synchronized(FLATTEN) {
            return toArryClr(flattenA(null, vClr(FLATTEN), false, oo(objects)), Object.class);
        }
    }
    public static List flattenA(Class clazz, List v, boolean uniq, Object[] oo) {
        if (v!=null) {
            synchronized(v) {
                for(Object o : oo) {
                    if (o==null) continue;
                    if (o instanceof Object[]) flattenA(clazz, v, uniq, (Object[])o);
                    else if (!uniq || !v.contains(o)) {
                        if (clazz==null||isAssignblFrm(clazz,o)) v.add(o);
                    }
                }
            }
        }
        return v;
    }
    public static int idxOfNotPositive(int nn[]) {
        if (nn==null) return 0;
        for(int i=0; i<nn.length; i++) if (nn[i]<0) return i;
        return nn.length;
    }
    public static String intArrayToTxt(int ii[]) {
        return intArrayToTxt(ii,0,MAX_INT);
    }
    public static String intArrayToTxt(int ii[], int from, int to) {
        final int t=mini(to,sze(ii));
        int w=0;
        for(int i=from; i<t;i++) w=maxi(w, stringSizeOfInt(ii[i]));
        final BA sb=new BA(t*w+t);
        for(int i=from; i<t;i++) {
            if (i>0) sb.a(',');
            sb.a(ii[i],w);
        }
        return sb.toString();
    }
    /* <<< Arrays <<< */
    /* ---------------------------------------- */
    /* >>> Array Element Accessor >>> */
    public static <T extends Object> T getV(Object key, Map m, Class<T> c) {
        if (key==null || m==null) return null;
        final Object o=deref(m.get(key));
        return isInstncOf(c,o) ? (T)o : null;
    }
    public static float get(int i, float[] a) { return a!=null && a.length>i && i>=0  ? a[i] : Float.NaN;}
    public static int   get(int i, int[] a) { return a!=null && a.length>i && i>=0  ? a[i] : INT_NAN;}
    public static long  get(int i, long[] a) { return get(i,a,Long.MIN_VALUE);}
    public static long  get(int i, long[] a, long outOfRange) { return a!=null && a.length>i && i>=0  ? a[i] : outOfRange;}
    public static short get(int i, short[] a) { return a!=null && a.length>i && i>=0  ? a[i] : Short.MIN_VALUE;}
    public static byte  get(int i, byte[] a) { return a!=null && a.length>i && i>=0 ? a[i] : 0;}
    public static String  get(int i, String[] a) {  return a!=null && a.length>i && i>=0 ? a[i] : null;}
    public static boolean get(int i, boolean[] a) {  return a!=null && a.length>i && i>=0 ? a[i] : false;}
    public static Object  get(int i, Object a) {
        if (a instanceof Reference || a instanceof Set) assrt();
        if (i<0 || i>=sze(a)) return null;

        final ListModel m=a instanceof JList ? ((JList)a).getModel() : a instanceof  ListModel ? (ListModel)a : null;
        Object o;
        try {
            o=a instanceof Object[] ? ((Object[])a)[i] :
                //a instanceof ListModel ?  ((ListModel)a).getElementAt(i) :
                a instanceof List ? ((List)a).get(i) :
                m!=null ? m.getElementAt(i) : null;
        } catch(Exception e){ putln("Caught ChUtils#get ",e); return null; }
        return deref(o);
    }
    public static <T extends Object> T get(Object collection0, Class<T> c) {
        if (collection0==null) return null;
        final Object a=c==Reference.class ? collection0: deref(collection0);
        if (isInstncOf(c,a)) return (T)a;
        final int N=sze(a);
        for(int i=0; i<N; i++) {
            final Object o=get(i,a);
            if (isInstncOf(c,o)) return (T)o;
        }
        return null;
    }
    public static JComponent derefJC(Object o) { return deref(o, JComponent.class);}
    public static Component derefC(Object o) { return deref(o, Component.class);}

    public static <T extends Object> T  deref(Object object, Class<T> c) {
        if (object==null) return null;
        final Object o=deref(object);
        return isInstncOf(c,o) ?  (T)o : null;
    }
    public static Object deref(Object ref0) {
        Object r=ref0;
        while(r instanceof Reference) r=((Reference)r).get();
        return r;
    }
    private final static Object[] DEREFARRAY={null};
    public static <T extends Object> T[] derefArray(Object objects, Class<T> clazz) {
        synchronized(DEREFARRAY) {
            return toArryClr(  derefArrayV(false,objects,clazz, vClr(DEREFARRAY)), clazz);
        }
    }
    private static <T extends Object> List derefArrayV(boolean uniq, Object objects, Class<T> clazz, List v) {
        final Object oo= objects instanceof List ? objects : oo(objects);
        final int N=szeVA(oo);
        if (N>0) {
            for(int i=0; i<N; i++) {
                final Object o=deref(get(i,oo));
                if (szeVA(o)>0) derefArrayV(uniq, o, clazz, v);
                else if (o!=null && clazz.isAssignableFrom(o.getClass())) {
                    if (uniq)adUniq(o,v);
                    else v.add(o);
                }
            }
        }
        return v;
    }
    /* <<< Array Element Accessor <<< */
    /* ---------------------------------------- */
    /* >>> Needle Haystack >>> */
    public static void assrtIsTxt(Object o) {
        if (!( o==null || o instanceof CharSequence || o instanceof byte[])) {
            putln("ChUtils o=",shrtClasNam(o));
            assrt();
        }
    }
    public static void assrt() { assert false; }
    /* >>> Character >>> */
    public final static int
        SPC=2, DIGT=3, LETTR=4, UPPR=5, LOWR=6, LETTR_DIGT=7, UPPR_DIGT=8, LOWR_DIGT=9, LETTR_DIGT_US=10, FILENM=11, ACTGUN=12, SPC_PIPE=13,
        WORD_DELIM_L=21, WORD_DELIM_R=22,
        LETTR_DASH=24, SLASH=26, DIGT_DASH=27, DIGT_SPC=28,LETTR_DASH_TILDE_DOT=29, LETTR_DIGT_US_COLON=30,
        EOL=31, HEX_DIGT=32, FILEP=34,
        DIGT_DOT=36, LETTR_DIGT_US_DASH=38, UPPR_DIGT_COLON=40, UPPR_DIGT_US=41, URLENCOD=42,  LETTR_DIGT_US_DOT=43, LETTR_DIGT_US_DOT_SLASH=44,
        LETTR_DIGT_DOT=45,
        LT_GT_AMP=46,
        LT_GT=47,
        DIGT_DOT_DASH=48,
        TAB_COMPL_DELIMr=50,TAB_COMPL_DELIMl=51,

        MAXCHARCLASS=66;
  public static boolean is(int charType, Object txt, int i) {
        if (txt==null || i<0) return false;
        if (txt instanceof byte[]) {
            final byte[] T=(byte[])txt;
            return i<T.length && T[i]>=0 && chrClas(charType)[T[i]];
        } else {
            final CharSequence T=(CharSequence)txt;
            return i<T.length() && is(charType, T.charAt(i));
        }
    }
    public static boolean isSpc(int c) { return ' '==c || '\r'==c || '\n'==c || '\t'==c || '\f'==c;}
    /* loss of speed due to additional parameter: 20%  Compared to direct comparison: nearly half the speed */
    private final static Map<String,boolean[]>mapDelim=new HashMap();
    public static boolean[] chrClas(String s) {
        final int L=s!=null ? s.length() : 0;
        boolean[] d=null;
        if (L==1) d=chrClas1(s.charAt(0));
        else if (L>1) {
            d=mapDelim.get(s);
            if (d==null) {
                mapDelim.put(s,d=new boolean[128]);
                for(int i=s.length(); --i>=0;) d[s.charAt(i)]=true;
            }
        }
        return d;
    }
    private final static boolean[][] CHCL1=new boolean[128][], CHCL=initCharClasses();
    public static boolean[] chrClas(int type) {return CHCL[type>0?type:(MAXCHARCLASS-type)];}
    public static boolean[] chrClas1(char c) {
        if (CHCL1[c]==null) (CHCL1[c]=new boolean[128])[c]=true;
        return CHCL1[c];
    }
    private static boolean[][] initCharClasses() {
        final boolean cc[][]=new boolean[MAXCHARCLASS*2][128];
        for(int t=MAXCHARCLASS; --t>=0;) {
            final boolean c[]=cc[t];
            final boolean
                letterDigitUs=t==LETTR_DIGT_US || t==LETTR_DIGT_DOT ||t==LETTR_DIGT_US_DASH || t==LETTR_DIGT_US_DOT || t==LETTR_DIGT_US_DOT_SLASH || t==LETTR_DIGT_US_COLON ||  t==FILENM || t==FILEP || t==URLENCOD,
                letterDigit=letterDigitUs || t==LETTR_DIGT,
                upperDigit=t==UPPR_DIGT || t==UPPR_DIGT_COLON || t==UPPR_DIGT_US;
            if (letterDigitUs) c['_']=true;
            if (t==SPC|| t==SPC_PIPE || t==DIGT_SPC || t==WORD_DELIM_L || t==WORD_DELIM_R || t==TAB_COMPL_DELIMr || t==TAB_COMPL_DELIMl) {
                c[' ']=c['\t']=c['\r']=c['\n']=c['\f']=true;
            }
            if (letterDigit || upperDigit || t==DIGT || t==HEX_DIGT || t==LOWR_DIGT || t==DIGT_DASH || t==DIGT_SPC || t==DIGT_DOT || t==DIGT_DOT_DASH ) {
                for(int j='0';j<='9'; j++) c[j]=true;
            }
            if (letterDigit || t==LETTR || t==LETTR_DASH || t==LETTR_DASH_TILDE_DOT) {
                for(int j='A'; j<='Z'; j++) c[j]=c[32|j]=true;
            }
            if (t==LOWR || t==LOWR_DIGT) for(int j='a'; j<='z'; j++) c[j]=true;
            if (t==UPPR || upperDigit)  for(int j='A';j<='Z'; j++) c[j]=true;
            if (t==HEX_DIGT) for(int j='A'; j<='F'; j++) c[j]=c[32|j]=true;
            if (t==LETTR_DASH_TILDE_DOT) c['-']=c['~']=c['.']=true;
            if (t==ACTGUN) c['a']=c['c']=c['t']=c['g']=c['n']=c['u']=c['A']=c['C']=c['T']=c['G']=c['N']=c['U']=true;
            if (t==FILENM || t==FILEP) c['-']=c['.']=c['+']=true;
            if (t==FILEP) c['/']=c['\\']=true;
            if (t==TAB_COMPL_DELIMr || t==TAB_COMPL_DELIMl ) c['/']=c['\\']=c['=']=c['#']=true;
            if (t==WORD_DELIM_L || t==WORD_DELIM_R) c['=']=c['/']=c['\\']=true;
         }
        cc[LETTR_DASH]['-']=
            cc[DIGT_DASH]['-']=
            cc[LETTR_DIGT_US_DASH]['-']=
            cc[LETTR_DIGT_DOT]['.']=
            cc[SLASH]['/']=cc[SLASH]['\\']=
            cc[EOL]['\n']=cc[EOL]['\r']=
            cc[LT_GT_AMP]['<']= cc[LT_GT_AMP]['>']= cc[LT_GT_AMP]['&']=
            cc[LT_GT]['<']= cc[LT_GT]['>']=
            cc[URLENCOD]['-']=cc[URLENCOD]['.']=cc[URLENCOD]['*']=
            cc[LETTR_DIGT_US_DOT]['.']=
            cc[LETTR_DIGT_US_DOT_SLASH]['.']=cc[LETTR_DIGT_US_DOT_SLASH]['/']=
            cc[DIGT_DOT]['.']=
            cc[DIGT_DOT_DASH]['.']=cc[DIGT_DOT_DASH]['-']=
            cc[WORD_DELIM_R]['.']=
            cc[UPPR_DIGT_COLON][':']=
            cc[UPPR_DIGT_US]['_']=
            cc[LETTR_DIGT_US_COLON][':']=
            cc[SPC_PIPE]['|']=true;
        for(int i=MAXCHARCLASS; --i>=0;) {
            final boolean c[]=cc[i];
            if (c==null) continue;
            final boolean inverse[]=cc[MAXCHARCLASS+i]=new boolean[128];
            for(int j=128; --j>=0; ) inverse[j]=!c[j];
        }
        return cc;
    }
    public static boolean is(int charType, int c) {   return c>=0 && c<128 && chrClas(charType)[c];}
    public static char chrAt(int i, Object txt) {
        if (txt==null ||i<0) return (char)0;
        else if (txt instanceof CharSequence) return i<((CharSequence)txt).length() ? ((CharSequence)txt).charAt(i) : (char)0;
        else if (txt instanceof byte[]) return (char)get(i,(byte[])txt);
        else { assrt(); return (char)0; }
    }
    public static char lstChar(Object txt) {
        final int L=sze(txt);
        return L==0?0:chrAt(L-1,txt);
    }
    /* <<< Character <<< */
    /* ---------------------------------------- */
    /* >>> Next/Previous >>> */
    public static int nxt(boolean[] charClass, Object txt, int from,int to) {
        if (txt instanceof byte[]) {
            final byte[] T=(byte[]) txt;
            final int t=mini(to,  T.length);
            for(int i=maxi(0,from); i<t; i++) {
                if (T[i]<0) continue;
                if (T[i]>=0 && charClass[T[i]]) return i;
            }
        } else if (txt instanceof BA) {
            final BA ba=(BA)txt;
            final int B=ba.begin();
            return maxi(-1,  nxt(charClass, ba.bytes(), B+from, B+mini(sze(ba),to)) -B);
        } else if (txt!=null) {
            final CharSequence T=(CharSequence) txt;
            final int t=mini(to, T.length());
            int cPrev=chrAt(from-1,T);
            for(int i=maxi(0,from); i<t; i++) {
                final char c=T.charAt(i);
                if (c<128 && charClass[c]) return i;
                cPrev=c;
            }
        }
        return -1;
    }
    public static int nxt(int charType, Object txt) { return nxt(charType,txt,0,MAX_INT);}
    public static int nxt(int charType, Object txt, int from) { return nxt(charType,txt,from,MAX_INT); }
    public static int nxt(int charType, Object txt, int from,int to) {
        if ( (charType==SPC || charType==-SPC) && txt instanceof byte[]) {
            final byte[] T=(byte[]) txt;
            final int t=mini(to, T.length), f=maxi(0,from);
            if (charType==SPC) {
                for(int i=f; i<t; i++) if (  T[i]==' ' || T[i]=='\t' || T[i]=='\n' || T[i]=='\r')  return i;
            } else  {
                for(int i=f; i<t; i++) if (!(T[i]==' ' || T[i]=='\t' || T[i]=='\n' || T[i]=='\r')) return i;
            }
            return -1;
        }
        return nxt(chrClas(charType), txt,from,to);
    }
    public static int prev(int charType, Object txt) { return prev(charType, txt, MAX_INT, -1); }
    public static int prev(int charType, Object txt,int from, int to) {
        //assert from>=to;
        if ( (charType==SPC || charType==-SPC) && txt instanceof byte[]) {
            final byte[] T=(byte[]) txt;
            final int t=maxi(-1,to), f=mini(from, T.length-1);
            if (charType==SPC) {
                for (int i=f; i>t; --i) if ( isSpc(T[i])) return i;
            } else {
                for (int i=f; i>t; --i) if (!isSpc(T[i])) return i;
            }
            return -1;
        }
        return prev(chrClas(charType), txt,from,to);
    }
    public static int prev(boolean charClass[], Object txt,int from, int to) {
        if (from<to) assrt();
        final int t=maxi(-1,to);
        if (txt instanceof CharSequence) {
            final CharSequence T=(CharSequence) txt;
            for (int i=mini(from, T.length()-1); i>t; --i) {
                final char c=T.charAt(i);
                if (c<128 && charClass[c])  return i;
            }
        } else if (txt!=null) {
            final byte[] T=(byte[]) txt;
            for (int i=mini(from, T.length-1); i>t; --i) {
                if (T[i]>=0 && charClass[T[i]])  return i;
            }
        }
        return -1;
    }
    // ----------------------------------------
    public static int prevE(boolean charClass[], Object T,int from, int to) {
        final int i=prev(charClass,T,from,to);
        return i>=0 ? i : maxi(-1,to);
    }
    public static int prevE(int charClass, Object T,int from, int to) {
        final int i=prev(charClass,T,from,to);
        return i>=0 ? i : maxi(-1,to);
    }
    // ----------------------------------------
    public static int nxtE(boolean[] charType, Object txt, int from,int to) {
        final int i=nxt(charType,txt, from,to);
        return i>=0 ? i : mini(to, sze(txt));
    }
    public static int nxtE(int charType, Object txt, int from,int to) {
        final int i=nxt(charType,txt, from,to);
        return i>=0 ? i : maxi(from, mini(to, sze(txt)));
    }
    public static int nxtE(int charType, Object txt) { return nxtE(charType, txt, 0,MAX_INT);}
    /* <<< Next/Previous <<< */
    /* ---------------------------------------- */
    /* >>> Equals >>> */
    public static boolean eqNz(Object o1, Object o2) { return o1!=null && o1.equals(o2);}
    public static boolean eq(Object o1, Object o2) { return  o1==null ? o2==null : o1.equals(o2); }
    public static boolean strEquls(Object needle, int from, int to, Object haystack, int fromH) {  return strEquls(0L, needle,from,to, haystack,fromH);}
    public static boolean strEquls(Object needle,  Object haystack, int fromH) {  return strEquls(0L, needle,0,MAX_INT, haystack,fromH);}
    public static boolean strEquls(Object needle,  Object haystack) {  return strEquls(0L, needle,0,MAX_INT, haystack,0);}
    public static boolean strEquls(long options, Object needle,  Object haystack) {  return strEquls(options, needle,0,MAX_INT, haystack,0);}
    public static boolean strEquls(long options, Object needle,  Object haystack, int i) {  return strEquls(options, needle,0,MAX_INT, haystack,i);}
    public static boolean strEquls(long options, Object oNeedle, int needleFrom0, int needleTo0, Object text, int fromH) {
        if (oNeedle==null || text==null || needleFrom0<0 || fromH<0) return false;
        final Object oHaystack= toStrgUF(text);
        if (oNeedle instanceof Object[]) {
            for( Object oN : (Object[])oNeedle) {
                if (strEquls(options, oN, needleFrom0, needleTo0, oHaystack, fromH)) return true;
            }
            return false;
        }
        assrtIsTxt(oHaystack);
        final int t=mini(sze(oNeedle),needleTo0), f=maxi(0,needleFrom0), hL=sze(oHaystack);
        final byte[] hsbb=oHaystack instanceof byte[] ? (byte[]) oHaystack:null;
        final CharSequence hss=hsbb!=null ? null : toCharSeq(oHaystack);
        final byte[] nbb=oNeedle instanceof byte[] ? (byte[])oNeedle : null;
        final CharSequence ns=oNeedle instanceof CharSequence ? (CharSequence)oNeedle : null;
        if (hL<t-f+fromH) return false;
        if (0!=(options&STRSTR_EVALOPTIONS) && !strstrEvalOpts(options, fromH, fromH+t-f,  null,null, oHaystack, 0, hL)) return false;
        final boolean ignoreCase=0!=(options&STRSTR_IC), anySlash=0!=(options&STRSTR_ANY_SLASH);
        for(int i=t-f; --i>=0;) {
            final int cn=nbb!=null?nbb[f+i]:ns.charAt(f+i);
            final int ch=hsbb!=null?hsbb[fromH+i]:hss.charAt(fromH+i);
            if (!(ch==cn || ignoreCase && (ch|32)==(cn|32) && is(LETTR,ch))) {
                if (anySlash && (cn=='/' || cn=='\\') && (ch=='/' || ch=='\\')) continue;
                return false;
            }
        }
        return true;
    }

    public static boolean bytsEquls(byte[] bb1, int from1, int to1,  byte[] bb2, int from2) {
        if (bb1==null || bb2==null || from1<0 || from2<0) return false;
        final int t1=mini(bb1.length,to1);
        if (bb2.length-from2 < t1-from1) return false;
        for(int i1=from1, i2=from2; i1<t1; ) {
            if (bb1[i1++]!=bb2[i2++]) return false;
        }
        return true;
    }
    public static boolean endWith(CharSequence ending, Object txt) { return endWith(0L,ending,txt);}
    public static boolean endWith(long options, Object ending, Object text) {
        if (ending instanceof Object[]) {
            for(Object n : (Object[])ending) if (endWith(options,n, text)) return true;
            return false;
        }
        final Object t= toStrgUF(text);
        final int sL=sze(ending), tL=sze(t);
        return tL*sL==0 || tL<sL  ? false : strEquls(options, ending,t, tL-sL);
    }

   public static boolean strEqulsWithFilter(long opt, boolean[] clazz, Object t1, int to1,  Object t2, int to2) {
        Object o1=t1, o2=t2;
        int i1=mini(to1, sze(o1))-1, i2=mini(to2,sze(o2)-1);

        if (o1 instanceof BA && ((BA)o1).begin()==0) { o1=((BA)o1).bytes(); i1=mini(i1,((BA)o1).end()-1, sze(o1)); }
        if (o2 instanceof BA && ((BA)o2).begin()==0) { o2=((BA)o2).bytes(); i2=mini(i2,((BA)o2).end()-1, sze(o2)); }

        while(i1>=0 || i2>=0) {
            char c1, c2;
            do {
                c1=i1<0?0: chrAt(i1--,o1);
            } while(i1>=0 && (c1>255 || !clazz[c1]));
            do {
                c2=i2<0?0: chrAt(i2--,o2);
            } while(i2>=0 && (c2>255 || !clazz[c2]));
            if (0!=(opt&STRSTR_IC) ?  ((c1|32)!=(c2|32)) :  (c1!=c2)) return false;
        }
        return true;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> STRSTR >>> */
    public final static long
        STRPLC_FILL_RIGHT=1L<<8, STRPLC_FILL_RM=1L<<10,
        STRPLC_CACHED_SB=1L<<13, STRPLC_16BIT=1L<<14,
        STRSTR_BEGIN_OF_LINE=1L<<20, STRSTR_EOL=1L<<21,
        STRSTR_w_L=1L<<22, STRSTR_w_R=1L<<23, STRSTR_w=STRSTR_w_L|STRSTR_w_R,
        STRSTR_W_L=1L<<24, STRSTR_W_R=1L<<25, STRSTR_W=STRSTR_W_L|STRSTR_W_R,
        STRSTR_E=1L<<30, STRSTR_AFTER=1L<<31, STRSTR_PREV=1L<<32,
        STRSTR_BEGIN=1L<<33, STRSTR_END=1L<<34,
        STRSTR_ANY_SLASH=1L<<40,STRSTR_IC=1L<<41, STRCHR_NOT=1L<<42;
    private final static long
        STRSTR_EVALOPTIONS=STRSTR_w|STRSTR_BEGIN_OF_LINE|STRSTR_EOL;
    private static boolean strstrEvalOpts(long options, int hitFrom, int hitTo, boolean[] lDelim, boolean[] rDelim, Object haystack,  int haystackFrom,  int haystackTo) {
        final int cL,cR;
        if (haystack instanceof byte[]) {
            final byte bb[]=(byte[])haystack;
            cL=hitFrom>haystackFrom && hitFrom<=haystackTo ?  (int)(char)bb[hitFrom-1] : MAX_INT;
            cR=hitTo<haystackTo ? (int)(char)bb[hitTo] : MAX_INT;
        } else {
            final CharSequence s=(CharSequence)haystack;
            cL=hitFrom>haystackFrom && hitFrom<=haystackTo ? s.charAt(hitFrom-1) : MAX_INT;
            cR=hitTo<haystackTo ? s.charAt(hitTo) : MAX_INT;
        }
        if (cL!=MAX_INT && (options&STRSTR_w_L)!=0 && is(LETTR_DIGT_US,cL)) return false;
        if (cR!=MAX_INT && (options&STRSTR_w_R)!=0 && is(LETTR_DIGT_US,cR)) return false;
        if (cL!=MAX_INT && (options&STRSTR_BEGIN_OF_LINE)!=0 && cL!=0 && '\n'!=cL) return false;
        if (cR!=MAX_INT && (options&STRSTR_EOL)!=0 && cR!=0 && '\n'!=cR && '\r'!=cR) return false;
        if (lDelim!=null && cL!=MAX_INT && !lDelim[cL]) return false;
        if (rDelim!=null && cR!=MAX_INT && !lDelim[cR]) return false;
        return true;
    }
    public static int strstr(          Object needle,                 Object hs)                     { return strstr(0L,  needle,0,MAX_INT, null,null,  hs,0,MAX_INT, null); }
    public static int strstr(          Object needle,                 Object hs, int hFrom, int hTo) { return strstr(0L,  needle,0,MAX_INT, null,null,  hs,hFrom,hTo, null); }
    public static int strstr(long opt, Object needle,                 Object hs)                     { return strstr(opt, needle,0,MAX_INT, null,null,  hs,0,MAX_INT, null); }
    public static int strstr(long opt, Object needle,                 Object hs, int from,  int to)  { return strstr(opt, needle,0,MAX_INT, null,null,  hs,from,to,   null); }
    public static int strstr(long opt, Object needle, int nF, int nT, Object hs, int from,  int to)  { return strstr(opt, needle,nF,nT,     null,null,  hs,from,to,   null); }
    public static int strstr(long opts,Object needle, int nFrom, int nTo, boolean delimL[], boolean delimR[], Object haystack0, int hFrom, int hTo, TextMatches matches) {
        if (needle==null || haystack0==null) return -1;
        final Object haystack=haystack0 instanceof File || haystack0 instanceof URL ? haystack0.toString() : haystack0;
        final int offset, hF,hT, hL;
        final byte[] hBB;
        final CharSequence hSt;
        final Object txt;
        final boolean prev=0!=(opts&STRSTR_PREV);
        if (haystack instanceof BA) {
            final BA ba= (BA)haystack;
            offset=ba.begin();
            txt=hBB=ba.bytes();
            hL=ba.length();
            hSt=null;
        } else {
            offset=0;
            if (haystack instanceof byte[]) {
                txt=hBB=(byte[])haystack;
                hL=hBB.length;
                hSt=null;
            } else if (haystack instanceof CharSequence) {
                txt=hSt=(CharSequence)haystack;
                hL=hSt.length();
                hBB=null;
            } else {
                assrt();
                return -1;
            }
        }
        hF=offset+ (prev ? mini(hL-1,hFrom) : maxi(0,hFrom));
        hT=offset+ (prev ? maxi(-1, hTo)    : mini(hL,hTo));
        /* needle */
        final int nF, nT;
        final byte[] nBB;
        final CharSequence nSt;
        int c0=0,c1=0,c2=0,c3=0,c9=0;
        if (needle instanceof byte[]) {
            nSt=null;
            nBB=(byte[])needle;
            if (nBB.length==0) return -1;
            nT=mini(nBB.length,nTo);
            nF=mini(nT, maxi(0,nFrom));
            if (nF>=nT) return -1;
            c0=nBB[nF];
            c9=nBB[nT-1];
            if (nT>nF+1) {
                c1=nBB[nF+1];
                if (nT>nF+2) {
                    c2=nBB[nF+2];
                    if (nT>nF+3) c3=nBB[nF+3];
                }
            }
        } else if (needle instanceof CharSequence) {
            nBB=null;
            nSt=(CharSequence)needle;
            if (nSt.length()==0) return -1;
            nT=mini(nSt.length(),nTo);
            nF=mini(nT, maxi(0,nFrom));
            if (nF>=nT) return -1;
            c0=nSt.charAt(nF);
            c9=nSt.charAt(nT-1);
            if (nT>nF+1) {
                c1=nSt.charAt(nF+1);
                if (nT>nF+2) {
                    c2=nSt.charAt(nF+2);
                    if (nT>nF+3) c3=nSt.charAt(nF+3);
                }
            }
        } else {
            assrt();
            return -1;
        }
        final int nL=nT-nF;

        final boolean ic=0!=(opts&STRSTR_IC), neval=0==(opts&STRSTR_EVALOPTIONS), anySlash=0!=(opts&STRSTR_ANY_SLASH);
        int result=-1;
        if (nL==0) return -1;

        if (nL==1 && matches==null && !anySlash && !ic) {
            final boolean not=0!=(opts&STRCHR_NOT);
            if (prev) {
                if (hBB!=null) for(int i=hF;i>hT;i--) if (not==(c0!=hBB[i])       && (neval||strstrEvalOpts(opts,i,i+1, null,null, txt,hF,hT))) {result=i; break;}
                if (hSt!=null) for(int i=hF;i>hT;i--) if (not==(c0!=hSt.charAt(i))&& (neval||strstrEvalOpts(opts,i,i+1, null,null, txt,hF,hT))) {result=i; break;}
            } else {
                if (hBB!=null) for(int i=hF; i<hT;i++) if (not==(c0!=hBB[i])&&        (neval||strstrEvalOpts(opts,i,i+1,null,null, txt,hF,hT))) {result=i; break;}
                if (hSt!=null) for(int i=hF; i<hT;i++) if (not==(c0!=hSt.charAt(i))&& (neval||strstrEvalOpts(opts,i,i+1,null,null, txt,hF,hT))) {result=i; break;}
            }
        } else {
            if (prev) { putln(RED_ERROR," STRSTR_PREV not yet supported "); assrt(); }
            final int c032=c0|32, c932=c9|32;
            final boolean c0Slash=anySlash && is(SLASH,c0), c9Slash=anySlash && is(SLASH,c0);
            final int hMax=hT-nL + 1;
            nextH:
            for(int h=hF;   h<hMax; h++) {
                final int h0=hBB!=null ? hBB[h] : hSt.charAt(h);

                if (!c0Slash && (h0|32)!=c032) continue;
                final int h9=hBB!=null ? hBB[h+ nL -1] : hSt.charAt(h+ nL -1); // !!!!!!!!!!!!!!!!!!!!!!
                if (!c9Slash && (h9|32)!=c932) continue;
                for(int n=nL; --n>=0; ) {
                    final int cn=nBB!=null ? nBB[nF+n] : n==nL-1?c9:n==0?c0: n==1?c1: n==2?c2 : n==3?c3 : nSt.charAt(nF+n);
                    final int ch=hBB!=null ? hBB[h+n] : hSt.charAt(h+n);
                    if (!(ch==cn || ic && (ch|32)==(cn|32) && is(LETTR,ch))) {
                        if (anySlash && (cn=='/' || cn=='\\') && (ch=='/' || ch=='\\')) continue;
                        continue nextH;
                    }
                }
                if (neval || strstrEvalOpts(opts, h, h+nL, delimL, delimR, txt, hF, hT)) {
                    result=h;
                    if (matches==null) break;
                    else {
                        matches.add(h-offset,h+nL-offset);
                        h+=nL-1;
                    }
                }
            }
        }
        // putln("result="+result+" offset="+offset);
        // putln("hBB=",hBB,"<");
        // putln("haystack=",haystack,"<");
        if (0!=(opts&STRSTR_AFTER) && result>=0) result+=nL;
        return (opts&STRSTR_E)!=0 && result<0 ? hT-offset : maxi(-1,result-offset);
    }
    public static int strchr(char ch, Object txt) { return strchr(0L, ch,txt,0,MAX_INT); }
    public static int strchr(long options, char ch, Object txt) {
        final boolean prev=0!=(options&STRSTR_PREV);
        return strchr(options, ch,txt, prev?MAX_INT:0, prev?-1:MAX_INT);
    }
    public static int strchr(char ch, Object txt,int from, int to) { return strchr(0L, ch,txt,from,to); }
    public static int strchr(long options, char ch, Object haystack0, int from, int to) {
        return strstr(options,toStrg(ch),0,1, null,null, haystack0,from,to, null);
    }
    /* <<< Search String <<< */
    /* ---------------------------------------- */
    /* >>> Replace String >>> */
    public static String rplcToStrg(Object needle, Object replacement, CharSequence haystack) {
        return rplcToStrg(0L, needle, replacement,haystack);
    }
    public static String rplcToStrg(long options, Object needle, Object replacement, CharSequence haystack) {
        return toStrg(strplc(options, needle, null,null, replacement, haystack, 0, MAX_INT));
    }
    public static CharSequence strplc(long options, Object pattern, Object replacement,  CharSequence txt) {
        return strplc(options,pattern, null,null, replacement, txt, 0,MAX_INT);
    }
    public static CharSequence strplc(long options, Object pattern, Object replacement,  CharSequence txt,  int from,  int to) {
        return strplc(options,pattern, null,null, replacement, txt, from, to);
    }
    private final static Object SYNC_STRPLC[]=new Object[512];
    public static CharSequence strplc(long options, Object pattern, boolean[] delimL, boolean[] delimR, Object replacement,  CharSequence txt,  int from,  int to) {
        if (pattern instanceof Map) {
            CharSequence cs=txt;
            for(Map.Entry e: entryArry((Map)pattern)) cs=strplc(options,e.getKey(),delimL,delimR,e.getValue(),cs, from, to);
            return cs;
        }

        final int txtL=sze(txt);
        if (txtL==0 || pattern==null) return txt;
        final int iTextMatch=Thread.currentThread().hashCode()&0xFF;
        final TextMatches matches=fromSoftRef(iTextMatch, SYNC_STRPLC, TextMatches.class);
        synchronized(matches) {
            final boolean fillR=0!=(options&STRPLC_FILL_RIGHT);
            strstr(options, pattern, 0, MAX_INT,  delimL,delimR, txt, from, to, matches.clear());
            final int n=matches.count(),  fromTo[]=matches.fromTo(), Lr=sze(replacement);
            if (n==0) return txt;
            boolean keepSize=true, notMore=true;
            int newSize=txtL, effort=0;
            for(int i=n; --i>=0;) {
                final int diff=fromTo[2*i]-fromTo[2*i+1]+Lr;
                newSize+=diff;
                if (diff!=0) { keepSize=false; if (diff>0) notMore=false; }
                effort+=newSize-fromTo[2*i+1];
            }
            if (keepSize || (fillR&&notMore)) {
                final StringBuffer txtSb=deref(txt, StringBuffer.class);
                final BA txtBa=deref(txt, BA.class);
                final String rS=txtSb==null?null:toStrg(replacement);
                if (Lr>0 && (txtBa!=null || txtSb!=null)) {
                    final byte fillRc=(byte)(options&255);
                    for(int i=n; --i>=0;) {
                        final int f=fromTo[2*i], t0=fromTo[2*i+1];
                        final int t=fillR  && notMore ? f+Lr : t0;
                        if (txtSb!=null) {
                            txtSb.replace(f,t, rS);
                            for(int k=t; k<t0; k++) txtSb.setCharAt(k,(char)fillRc);
                        } else {
                            txtBa.replaceRange(f,t, replacement);
                            if (t<t0) txtBa.setCharsAt(t,t0, fillRc);
                        }
                    }
                    return txt;
                }
            }
            final CharSequence dest=
                0!=(options&STRPLC_CACHED_SB) ? sbSoftClr(256+iTextMatch, SYNC_STRPLC) :
                0!=(options&STRPLC_16BIT) ? new StringBuffer() :
                new BA(0);
            if (dest instanceof BA) {
                ((BA)dest).ensureCapacity(newSize);
                ((BA)dest).aRplc(txt, replacement, matches);
            } else {
                ((StringBuffer)dest).ensureCapacity(newSize);
                sbAppndRplc(txt, replacement, fromTo, n, (StringBuffer)dest);
            }
            if (dest.length()!=newSize) putln(RED_ERROR,"rplc dest.length()="+dest.length()+" newSize="+newSize);
            if (0!=(options&STRPLC_FILL_RM)) rmAllChars( (int)(options&255), dest,0,MAX_INT);
            return dest;
        }
    }
    public final static int RM_CHARS_KEEP_ONE=1<<24;
    private final static Object[] SYNC_rmCC=new Object[2];
    public static CharSequence rmAllChars(int character, CharSequence cs, int from, int to) {
        final boolean keepOne=0!=(RM_CHARS_KEEP_ONE&character);
        final int c=character&0xFFff;
        final int L=sze(cs);
        if (cs instanceof BA) {
            final BA ba=(BA)cs;
            final byte[] T=ba.bytes();
            final int B=ba.begin(), E1=B+L-1, f=from+B, t=mini(to,L)+B;
            int dest=f;
            for(int src=f; src<t; src++) if (T[src]!=c || keepOne && (src>=E1 || T[src+1]!=c)) T[dest++]=T[src];
            return ba.setEnd(dest);
        } else {
            synchronized(SYNC_rmCC) {
                final char[] T=ccSoftRef(0, SYNC_rmCC,L);
                toChrs(cs,0,L, T);
                final int f=from, t=mini(to,L), E1=L-1;
                int dest=f;
                for(int src=f; src<t; src++) if (T[src]!=c || keepOne && (src>=E1 || T[src+1]!=c)) T[dest++]=T[src];
                final StringBuffer sb=cs instanceof StringBuffer ? (StringBuffer)cs :new StringBuffer();
                if (sb==cs) sb.setLength(0);
                return sb.append(T,0,dest);
            }
        }
    }
    /* <<< String Replace <<< */
    /* ---------------------------------------- */
    /* >>> atoi >>> */
    public static boolean isFloat(final byte[] T,final int from, final int to) {
        if (T==null) return false;
        final int t=mini(to,T.length);
        int f=maxi(0,from);
        if (to>f && T[f]=='+') f++;
        if (to>f && T[f]=='-') f++;
        boolean prevExp=false;
        for(int i=f, dots=0, countE=0; i<t; i++) {
            final byte c=T[i];
            final boolean exp=c=='E' || c=='e';
            if (!(exp&&++countE<2 || c=='.' && ++dots<2 || '0'<=c&&c<='9' || prevExp&&(c=='+'||c=='-'))) return false;
            prevExp=exp;
        }
        return true;
    }
    public static double atof(Object o) { return atof(o,0,MAX_INT);}
    public static double atof(Object o, int position, int endPosition) {
        if (o==null) return 0;
        if (o instanceof Number) return
                                     o instanceof Double ? ((Double)o).doubleValue() :
                                     o instanceof Float ? ((Float)o).floatValue() :
                                     atol(o);
        final byte bb[]=o instanceof byte[] ? (byte[])o : null;
        final CharSequence s=bb!=null ? null : o instanceof CharSequence ? (CharSequence)o : toStrg(o);
        final int L=bb!=null ? bb.length : sze(s);
        if (L==0) return Double.NaN;
        int pos=position;
        while(pos<L && (bb!=null ? bb[pos] : s.charAt(pos))==' ') pos++;
        while(pos<L && (bb!=null ? bb[pos] : s.charAt(pos))=='+') pos++;
        if (L<=pos) return Double.NaN;
        final int b0=bb!=null ? bb[pos] : s.charAt(pos);
        final boolean minus=b0=='-';
        if (minus) pos++;
        int end=pos, e=-1, dot=-1;
        boolean skip=false;
        for(int c=b0,c1 ; end<L; end++,c=c1) {
            c1=L>end+1 ? (bb!=null?bb[end+1]:s.charAt(end+1)) :0;
            if (skip) { skip=false; continue;}
            if (end==pos && c=='-') continue;
            if (c=='E' || c=='e') {
                if (e!=-1) break;
                e=end;
                if (c1=='-' || c1=='+') skip=true;
            } else if (c=='.') {
                if (e+dot!=-2) break;
                dot=end;
            } else if ('0'>c || c>'9') break;
        }
        if (pos==end) return Double.NaN;
        _atoiDebug=false;
        double v= bb!=null ? atol(bb,pos,MAX_INT) : atol(s,pos,MAX_INT);
        _atoiDebug=true;
        if (dot!=-1) {
            final int exp=mini(19,(e!=-1 ? e : end)-dot-1);
            if (exp>0) {
                final double f=exp<FRAC.length ? FRAC[exp] : Math.pow(10,-exp);
                final long frac= (bb!=null ? atol(bb,dot+1,dot+1+exp): atol(s,dot+1,dot+1+exp)) ;
                v+=frac*f;
            }
        }
        if (e!=-1) {
            final int exp=bb!=null ? atoi(bb,e+1) : atoi(s,e+1);
            final double f= exp>=0 && exp<DEC.length ? DEC[exp] : -exp>=0 && -exp<DEC.length ? FRAC[-exp] : Math.pow(10,exp);
            v*=f;
        }
        return minus ? -v : v;
    }
    private static int _atoi[][];
    private static float[][] _atof;
    public static float[][] atofLookup() { atoiLookup();  return _atof;}
    public static int[][] atoiLookup() {
        if (_atoi==null) {
            _atoi=new int[7][];
            _atof=new float[7][];
            for(int decpos=7; --decpos>=0;) {
                final float ff[]=_atof[decpos]=new float[256];
                final int dd[]=_atoi[decpos]=new int[256];
                final int fac=power10(decpos);
                for(int i=10; --i>0; ) { dd[i+'0']=fac*i; ff[i+'0']=((float)i)/fac; }
            }
        }
        return _atoi;
    }
    public static boolean _atoiDebug=true;
    private final static double[] FRAC=new double[18], DEC=new double[18];
    static {
        long d=1;
        for(int i=0; i<FRAC.length; i++) {
            FRAC[i]=1.0/d;
            DEC[i]=d;
            d*=10;
        }
    }
    public static long atolDecHex(Object s, final int position) {
        assert s instanceof CharSequence || s instanceof byte[];
        final int pos=nxt(-SPC,s,position, MAX_INT);
        if (pos<0 || sze(s)<=pos) return 0L;
        final int c1=chrAt(pos+1,s)|32;
        if (chrAt(pos,s)=='0' && (c1=='x' || c1=='b')) {
            try {
                return Integer.parseInt(toStrg(s).substring(pos+2).trim(),c1=='x'?16:2);
            } catch(Exception ex){return 0L;}
        }
        return atol(s,pos,MAX_INT);
    }

    public static int atoiOrDefault(Object o, int defaultValue) {  return atoiOrDefault(o,0,defaultValue); }
    public static int atoiOrDefault(Object o, int pos, int defaultValue) {
        final int noSpc=nxt(-SPC,o,pos,MAX_INT);
        final char c0=chrAt(noSpc,o);
        if (is(DIGT,c0) || is(DIGT,o,noSpc+1)&&(c0=='-'||c0=='+')) return (int)atol(o,pos,MAX_INT);
        return defaultValue;
    }
    public static int trailInt(Object o) {
        final int L=sze(o);
        final int pos=is(DIGT,o,L-1) ? prev(-DIGT,o) : -1;
        return pos<0?INT_NAN : atoi(o,pos+1);
    }
    public static int intEndsAt(Object txt, int start) {
        assrtIsTxt(txt);
        final char sgn=chrAt(start,txt);
        final int digits=start+ (sgn=='-'||sgn=='+'?1:0);
        final int end=nxtE(-DIGT,txt,digits,MAX_INT);
        return end<=digits ? -1 : end;
    }

    public static int  atoi(Object o, int pos) { return (int)atol(o,pos,MAX_INT);}
    public static int  atoi(Object o, int pos, int end) { return (int)atol(o,pos,end);}
    public static int  atoi(Object o) { return (int)atol(o,0,MAX_INT); }
    public static long atol(Object o) { return atol(o,0,MAX_INT); }
    public static long atol(Object text, int fromPosition, int toPosition) {
        final Object o;
        final int position, end;
        if (text instanceof BA) {
            final BA ba=(BA)text;
            o=ba.bytes();
            position=fromPosition+ba.begin();
            end=mini(ba.bytes().length,toPosition)+ba.begin();
        } else { o=text; position=fromPosition; end=toPosition;}
        final byte[] bb=o instanceof byte[] ? (byte[])o : null;
        final CharSequence cs;
        if (bb==null) {
            if (o==null) return 0;
            if (o instanceof Object[] || o instanceof List) return atol(get(position, o), 0,end);
            if (o instanceof long[]) return position>=0 && ((long[])o).length>position ? ((long[])o)[position] : 0;
            if (o instanceof  int[]) return position>=0 && (( int[])o).length>position ? (( int[])o)[position] : 0;
            if (o instanceof Long) return ((Long)o).longValue();
            if (o instanceof Integer) return ((Integer)o).intValue();
            if (o instanceof Byte) return ((Byte)o).byteValue();
            if (o instanceof Short) return ((Short)o).shortValue();
            if (o instanceof Character) return ((Character)o).charValue()-'0';
            if (o instanceof char[]) assrt();
            cs=toCharSeq(o);
        } else cs=null;
        final int L=mini(bb!=null?bb.length : cs!=null?cs.length() : 0,end);
        if (L==0 || position<0) return 0;
        final String s=cs instanceof String ? (String)cs : null;
        int pos=position;
        while(pos<L && (bb!=null?bb[pos]:s!=null?s.charAt(pos):cs.charAt(pos))==' ') pos++;
        while(pos<L && (bb!=null?bb[pos]:s!=null?s.charAt(pos):cs.charAt(pos))=='+') pos++;
        final boolean minus=L>pos && (bb!=null?bb[pos]:s!=null?s.charAt(pos):cs.charAt(pos))=='-';
        long ret=0;
        for(int i=minus ? pos+1 : pos; i<L; i++) {
            final int b=bb!=null ? bb[i] : s!=null?s.charAt(i):cs.charAt(i);
            if (b<'0' || b>'9') break;
            ret=ret*10+ (b-'0');
        }
        return minus ? -ret : ret;
    }
    public static int itoa(int number, byte[] T) { return itoa(number,T, 0); }
    public static int itoa(long number, byte[] T, int textPosition) {
        long i= number<0?-number:number;
        final int sz=stringSizeOfInt(number);
        int idx=textPosition+sz;
        while(i>=0) {
            T[--idx]= (byte) ('0'+i%10);
            if ( (i/=10) ==0) break;
        }
        if (number<0)  T[idx-1]=(byte)'-';
        return sz;
    }
    /* <<< atoi <<< */
    /* ---------------------------------------- */
    /* >>> Math  >>> */
    public static int maxi(int a, int b) {  return a>=b?a:b; }
    public static int maxi(int a, int b, int c) {  return (b >= c) ?  (a>=b?a:b) : (a>=c?a:c);    }
    public static int mini(int a, int b) {  return a<=b?a:b; }
    public static int mini(int a, int b, int c) {  return (b <= c) ?  (a<=b?a:b) : (a<=c?a:c);    }
    public static int mini(int[] aa) {
        int min=MAX_INT;
        for(int a :aa ) if (min>a)min=a;
        return min;
    }
    public static int maxi(int[] aa) {
        int maxi=MIN_INT;
        for(int a :aa ) if (maxi<a)maxi=a;
        return maxi;
    }
    /* <<< Math <<< */
    /* ---------------------------------------- */
    /* >>> Customize >>> */
    public static void setSettings(Object dialog, int...c)       {  Customize.mapSettings().put(dialog,c); if (!(dialog instanceof Class))  Customize.mapSettings().put(dialog.getClass(),c); }
    public static void setSettings(Object dialog, Customize...cc0) {
        final Customize[] cc=rmNullA(cc0,Customize.class);
        Customize.mapSettings().put(dialog,cc);
        if (!(dialog instanceof Class))  Customize.mapSettings().put(dialog.getClass(),cc);
    }
    public static String custSetting(int id) { final Customize c=Customize.customize(id); return c!=null ? c.getSetting() : null;}
    public static String[] custSettings(int id) { final Customize c=Customize.customize(id); return c!=null ? c.getSettings() : NO_STRING;}
    public static void edFile(int lineNumber,Object fileObject, int awtModifiers) {
        if (fileObject==null) return;
        setNextProcessHasLogPanel((awtModifiers&CTRL_MASK)!=0);
        final File f=fileObject instanceof File ? (File)fileObject : file(fileObject.toString());
        mkParentDrs(f);
        if (!f.exists()) wrte(f,"");
        final String fn=f.getAbsolutePath();
        final Customize c=Customize.customize(fn.endsWith(".java") ?  Customize.javaSourceEditor : Customize.textEditors);
        final ChExec ex=new ChExec(0).setCommandsAndArgument(c.getSettings(),fn);
        if (_logNxtX) ex.setCustomize(c);
        startThrd(ex);
        _logNxtX=false;
    }
    public static String[] custSettings(Class clazz,int id) {
        final Customize c=Customize.getCustomizableForClass(clazz,id);
        return c!=null?c.getSettings() : NO_STRING;
    }
    /* <<< Customize <<< */
    /* ---------------------------------------- */
    /* >>> OS -dependent services >>> */
    private static boolean _logNxtX;
    private static long _logNxtXTime;
    public static boolean isNextProcessHasLogPanel() { return _logNxtX; }
    public static void setNextProcessHasLogPanel(boolean b) {
        if (currentTimeMillis()-_logNxtXTime>999) _logNxtX=b;
    }
    public static void setNextProcessHasLogPanel(long time) { _logNxtXTime=time;}
    public static boolean isExeFileName(Object o) {
        final String s=toStrg(o);
        return s==null ? false : s.endsWith(jControl());
    }
    public static void setViewFileWith(Object fileAbsPath, char type) {
        (viewFileWith[type]==null ? viewFileWith[type]=new HashSet() : viewFileWith[type]).add(toStrg(fileAbsPath));
    }
    public static File drctry(File f) { return f!=null && !f.isDirectory() ? file(f.getParent()) : f; }
    private final static Set<String>[] viewFileWith=new HashSet['z'+1];
    public static void viewFile(File file, int awtModifiers) {
        File f=file;
        final String path=toStrg(f);
        if (!fExists(f)) return;
        if (!Insecure.EXEC_ALLOWED /*&& name2class(ChJnlp.BASIC_SERVICE)!=null*/) { ChJnlp.showURL(url(f)); return; }
        int c;
        {
            boolean compressed=false;
            for(String s : COMPRESS_SUFFIX) compressed|=path.endsWith(s);
            char type=0;
            for(char iT='z'+1; --iT>='A';) if (viewFileWith[iT]!=null && viewFileWith[iT].contains(path)) { type=iT; break;}
            c=type=='v' || compressed && type=='e' ? MAX_INT : type=='e' ? Customize.textEditors : -1;
        }
        if (c<0) {
            final String ext=lstCmpnt(path,'.').toLowerCase().intern();
            if (f.isDirectory()) c=Customize.fileBrowsers;
            else if (ext=="tex") c=Customize.latexEditors;
            else if (ext=="java")c=Customize.javaSourceEditor;
            else if (ext=="pdf") c=Customize.pdfViewer;
            else if (ext=="ps")  c=Customize.psViewer;
            else if (ext=="log") c=Customize.watchLogFile;
            else if (strstr(STRSTR_w_R,ext, "html htm jpg jpeg gif png bmp")>=0) visitURL(f, awtModifiers);
            else if (strstr(STRSTR_w_R,ext, "txt msf aln fasta sbml xml")>=0 || path.indexOf("_segment=")>0) c=Customize.textEditors;
            else {
                c=Customize.fileBrowsers;
                f=drctry(f);
            }
        }
        if (c==MAX_INT) { new ChTextView(f).tools().showInFrame(path); }
        else if (c>=0) {
            final Customize cust=Customize.customize(c);
            setNextProcessHasLogPanel( (awtModifiers&CTRL_MASK)!=0);
            final ChExec ex=new ChExec(ChExec.LOG).setCommandsAndArgument(cust.getSettings(),f);
            if (_logNxtX) ex.setCustomize(cust);
            startThrd(ex);
        }
        _logNxtX=false;
    }
    private static long _visitUrlWhen;
    private static String _visitURL;
    public final static String VISIT_URL_JAVAWS="&USEJAVAWS";
    private static boolean _jnlpServiceBrowser;
    public static Runnable thread_visitURL(Object o, int evtMask) {
        return thrdM("visitURL",ChUtils.class, new Object[]{o,intObjct(evtMask)});
    }
    public static void visitURL(Object o, int evtMask) {
        if (o==null) return;
        final List<String> v=new ArrayList();
        if (o instanceof CharSequence && strEquls("mailto:",o,0)) v.add(toStrg(o));
        for(URL url : o instanceof URL[] ? (URL[])o : new URL[]{url(o)}) {
            final String sUrl=toStrg(url);
            if (sUrl==null || sUrl.equals(_visitURL) && currentTimeMillis()-_visitUrlWhen<444) continue;
            _visitURL=sUrl;
            adUniq(strEquls(URL_STRAP,sUrl) && endWith(".html",sUrl)? sUrl+"?"+ChFrame.startCount() : sUrl ,v);
        }
        final boolean jnlpService=!Insecure.EXEC_ALLOWED && name2class(ChJnlp.BASIC_SERVICE)!=null;
        for(String sUrl: strgArry(v)) {
            final int postData=sUrl.indexOf("??");
            final String sUrl0=postData>0?sUrl.substring(0,postData) : sUrl;
            if (postData>0) startThrd(thrdCR(instance(0),"POST_BROWSER", new Object[]{sUrl0, new BA(sUrl.substring(postData+2))}));
            else if (postData<0 || sUrl.indexOf('?')<postData) {
                ChExec ex=null;
                Customize c=null;
                if (jnlpService) {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                } else if (sUrl.indexOf(VISIT_URL_JAVAWS.substring(1))>0 || sUrl.indexOf(".jnlp?")>0 || sUrl.endsWith(".jnlp")) {
                    ex=new ChExec(ChExec.LOG).setCommandLineV(dirJavaws()+"javaws",sUrl);
                } else {
                    c=Customize.customize(Customize.webBrowser);
                    ex=new ChExec(ChExec.LOG).setCommandsAndArgument(c.getSettings(),sUrl0);
                }
                if (ex!=null && (evtMask&CTRL_MASK)!=0) ex.setCustomize(c);
                startThrd(ex);
            }
        }
        _visitUrlWhen=currentTimeMillis();
        _logNxtX=false;
    }
    public static File dirCygwin() { return file("?:\\cygwin\\");}
    static boolean currVers(String s) { startThrd(thrdCR(instance(0),"CURR_VERS",s)); return true;}
    /* <<< OS-dependent services <<< */
    /* ---------------------------------------- */
    /* >>> Write Image >>> */
    private static JTabbedPane _pngTabbed;
    private static CellRendererPane _pngPane;
    public final static int PNG_NOT_NOW=1;
    private static Runnable _pngR;
    public static Runnable threadWritePng() {
        if (_pngTabbed!=null) {
            ChFrame.frame(null, _pngTabbed, 0).size(99,99).shw(0);
        }
        if (_pngR==null) _pngR=thrdM("writePng", ChUtils.class, new Object[]{intObjct(0),"A",null});
        return _pngR;
    }

    public static void writePng(int opt, Object component, File f) {
        final Object KEY_PNG_FILE="CU$$PNGF";
        assrtEDT();
        if (component=="A" && _pngTabbed!=null) {
            for(int i=_pngTabbed.getTabCount(); --i>=0;) {
                final Component jc=scrllpnChild(child(_pngTabbed.getComponentAt(i),JScrollPane.class));
                final File file=gcp(KEY_PNG_FILE,jc,File.class);
                if (file!=null && (file.length()==0 || (currentTimeMillis()-file.lastModified()>60*1000))) {
                    writePngNow(jc, file);
                    pcp(KEY_PNG_FILE,null,jc);

                }
            }
            _pngTabbed.removeAll();
             ChFrame.frame(null, _pngTabbed,0).setExtendedState(JFrame.ICONIFIED);
        }
        final Component c=derefC(component);
        if (c!=null) {
            final boolean complex=!(c instanceof JComboBox || c instanceof AbstractButton || c instanceof JLabel);
            if (complex && null==c.getParent()) {
                if (_pngTabbed==null)  ChFrame.frame("writePng: you can close this frame", _pngTabbed=new JTabbedPane(), ChFrame.ALWAYS_ON_TOP);
                if (_pngTabbed!=scrllpn(c).getParent()) {
                    pcp(KEY_PNG_FILE,f,c);
                    adTab(0, nam(f),  scrllpn(c),  _pngTabbed);
                }
            } else {
                writePngNow(c,f);
                if (opt!=PNG_NOT_NOW) inEDTms(threadWritePng(),999);
            }
        }
    }
    private static void writePngNow(Component c, File f) {
        if (c==null || f==null) return;
        if (_pngPane==null) _pngPane=new CellRendererPane();
        pcp(KOPT_JUST_WRITING_PNG,"",c);
        int w=wdth(c),h=hght(c);
        final Dimension prefSize;
        if (w<=0 || h<=0) {
            prefSize=HtmlDoc.prefSize(c);
            w=maxi(1,wdth(prefSize));
            h=maxi(1,hght(prefSize));
        } else prefSize=null;
        final BufferedImage im=new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
        final Graphics g=im.getGraphics();
        try {
            g.setColor(Color.WHITE);
            fillBigRect(g, 0,0,99999,99999);
            final boolean complex=!(c instanceof JComboBox || c instanceof AbstractButton || c instanceof JLabel);
            if (complex || prefSize==null) c.paint(g);
            else if (prefSize!=null) _pngPane.paintComponent(g, c, pnl(), 0, 0, w, h, true);
            mkParentDrs(f);
            javax.imageio.ImageIO.write(im,"png",f);
        } catch(Throwable e) { stckTrc(e); }
        pcp(KOPT_JUST_WRITING_PNG,null,c);
    }
    /* <<< Write Image <<< */
    /* ---------------------------------------- */
    /* >>> Help and Soruce code  >>> */
    public final static boolean[] BENCHMARKS=new boolean['z'+1];
    /* >>> Help and Source >>> */
    private static byte _allHelp[];
    final static Object[] MAP_HLP={null};
    public static BA getHlp(Object object) {
        final String cnSlash=clasNamWithSlash(object);
        if (cnSlash==null||cnSlash.startsWith("java/")) return null;
        final HashMap<String,Object> mapHelp=fromSoftRef(0,MAP_HLP,HashMap.class);
        final Object oHelp=mapHelp.get(cnSlash);
        if (oHelp==ERROR_OBJECT) return null;
        BA sbHelp=deref(oHelp,BA.class);
        if (sbHelp==null) sbHelp=readBytes(rscAsStream(0L, cnSlash+"Help.html"));
        if (sbHelp!=null && sze(sbHelp)==0) return null;
        byte all[]=_allHelp;
        if (all==null) all=_allHelp=toByts(readBytes(rscAsStream(0L,_CC,"allHelp"),new BA(260*1000)));
        if (all==null) all=_allHelp=NO_BYTE;
        if (sbHelp==null && sze(all)>0) {
            final int from=strstr("\t"+cnSlash.replace('/','.')+"\n",all);
            if (from>=0) {
                final int cr=strchr('\n',all,from,MAX_INT);
                final int to=strchr('\t',all,cr,MAX_INT);
                if (to>0) {
                    sbHelp=new BA(to-cr+3333).a(all,cr+1,to);
                    if (strstr(0L,"<i>",all,cr,to)>=0) {
                        for(int i=4;--i>=0;) sbHelp.a("\n<i>"+HtmlDoc.PACKAGE).a(i==0?_CC:i==1?_CCS:i==2?_CCSE:_CCP).aln("</i>");
                    }
                }
            }
        }
        if (sbHelp==null) {
            final BA ba=getJavaSrc(cnSlash);
            if (ba!=null) {
                final byte[] src=ba.bytes();
                final int end=ba.end();
                int from=strstr(0L,"/"+"**HELP",src,0,end);
                if (from>=0) {
                    from+=7; while(isSpc(src[from])) from++;
                    final int author=strstr(0L,"@author",src,from,end);
                    final int to=author>0 ? author : strstr(0L,"\n*/",src,from,end);
                    if (to>from) {
                        sbHelp=new BA(to+999).a(src,from,to);
                        if (strstr(0L,"<i>",src,from,to)>=0) {
                            sbHelp.a("\n<i>PACKAGE:").a(clasNam(object)).aln(".</i>");
                            for (Object i : ChCodeViewer.getImportedPackages(ba)) sbHelp.a("<i>"+HtmlDoc.PACKAGE).a(i).aln("</i>");
                        }
                    }
                }
            }
        }
        mapHelp.put(cnSlash, sbHelp==null?ERROR_OBJECT : sbHelp.trimSize());
        return sbHelp;
    }
    public static boolean showHelp(Object o) {
        if (o instanceof URL) {
            final BA ba=readBytes(toStrg(o));
            if (sze(ba)==0) return false;
            if (looks(LIKE_HTML,ba)) new ChJTextPane(ba).tools().showInFrame("Help");
            else shwTxtInW("Help",ba);
            return true;
        } else return addHelpSource(clasNam(o), PANELS_JH['H'],true);
    }
    public static boolean showJavaSource(String shortClassName, String[] packages) {
        final String cn=fullClassNam(true, shortClassName,packages);
        return cn!=null ? addHelpSource(cn,PANELS_JH['J'],false) : false;
    }
    private static boolean addHelpSource(String className, ChTabPane tabbed, boolean helpOrSource) {
        if (className==null) return false;
        if(!isEDT()) {
            inEdtLaterCR(instance(0),"addHelpSource",new Object[]{className,tabbed, boolObjct(helpOrSource)});
            return false;
        }
        if (tabbed!=null) {
            for(JComponent h : tabbed.tabComponents()) {
                if (className.equals(gcp(KEY_CLASS_NAME,h))) {
                    tabbed.setSelectedComponent(h);
                    return true;
                }
            }
        }
        final String
            shortCn=className.substring(className.lastIndexOf('.')+1).replaceAll("PROXY$","*"),
            tip=txtForTitle(dItem(className))+"<br><sup>"+className+"</sup>",
            key=(helpOrSource ? "" : "class ")+className;
        Object tabComp=deref(mapTextViewer.get(key));
        if (tabComp==null) {
            JComponent ta=null;
            if (helpOrSource) {
                final BA sb=getHlp(className);
                if (sb==null) return false;
                HtmlDoc.insertHeadline(className,'2',sb, DOCTYPE_TUTORIAL);
                if (className.endsWith("PROXY")) {
                    if (!helpOrSource) addHelpSource(delSfx("PROXY",className),tabbed,false);
                    else sb.a("\n<br><sub><b>Source code:</b> ").a(URL_STRAP_JARS).a("src</sub><br>");
                }
                ta=new ChJTextPane("").t(sb);
                pcp(KEY_CLASS_NAME,className,ta);
            } else {
                final BA sb=ChCodeViewer.highlightSyntax(ChCodeViewer.ANSI, getJavaSrc(className), NO_STRING);
                if (sb==null) return false;
                ta=new ChTextView(sb.trimSize());
                pcp(ChTextComponents.KOPT_CLASS_AT_CURSOR, "", ta);
                setBG(0xFFffFF, ta);
            }
            if (ta!=null) {
                if (tabbed==null) {
                    ChTextComponents.tools(ta).showInFrame(ChFrame.SCROLLPANE|CLOSE_CtrlW|ChFrame.AT_CLICK,shortCn);
                }
                else tabComp=scrllpn(ta);
                closeOnKey(CLOSE_CtrlW, ta, tabComp);
            }
        }
        if (tabComp!=null) {
            mapTextViewer.put(key,wref(tabComp));
            String tab=null;
            if (className.indexOf(".Dialog")>0) tab=dItem(className);
            else if (className.endsWith(_CCSE)) tab=shortCn;
            else tab=orS(dTab(className), dItem(className));
            TabItemTipIcon.set(orS(tab,shortCn),className,tip,dIcon(className),tabComp);
            tabbed.addTab(CLOSE_ALLOWED, (JComponent)tabComp);
            try { ((ChTabPane)tabbed.getParent() ).setSelectedComponent(tabbed);} catch(Exception e) {}
        }
        return true;
    }
    private static Map<String,Object> mapTextViewer=new HashMap();
    public final static Object KOPT_PANHS_NO_CTRL=new Object();
    public static JComponent panHS(Object oo) {
        final JComponent c=pnl(HB, OPAQUE_FALSE);
        boolean ctrl=true;
        for(Object o : oo(oo)) {
            if (o==null) continue;
            if (gcp(KOPT_PANHS_NO_CTRL,o)!=null) ctrl=false;
            for(int i=0; i<6; i++) {
                final ChButton b=
                    i==0 ? ChButton.doCustomize(o) :
                    i==1 && ctrl ? ChButton.doSharedCtrl(o) :
                    i==2 ? ChButton.doCtrl(o) :
                    i==3 ? ChButton.doWebSettings(o) :
                    i==4 ? smallSourceBut(oo) :
                    i==5 ? smallHelpBut(oo) :
                    null;
                final String ic=i==0 ? IC_CUSTOM24 : i==1 || i==2 ? IC_CONTROLPANEL: null;
                if (b!=null) {
                    if (ic!=null) b.rover(ic);
                    c.add(b.cp(KOPT_HIDE_IF_DISABLED,""));
                }
            }
        }
        c.add(Box.createHorizontalGlue());
        return c;
    }
    public final static ChTabPane PANELS_JH[]=new ChTabPane['J'+1];
    private final static List _vSrcBut=new ArrayList();
    public static ChButton smallHelpBut(Object o) { return helpBut(o).rover(IC_HELP); }
    public static ChButton smallSourceBut(Object o) {final ChButton b=ChButton.doHelpOrJava(oo(o),'J').rover(IC_JAVA);   _vSrcBut.add(b); return b;}
    public static ChButton helpBut(Object o) { return ChButton.doHelpOrJava(o,'H'); }
    public static ChButton sourceBut(Object o) { return ChButton.doHelpOrJava(o,'J').t("See java source code"); }
    public static Object dialogHead(Object obj) {
        final String t=dTitle(clas(obj));
        final JComponent title=new ScrollLabel(t!=null?t : shrtClasNam(obj)), pEast=panHS(obj);
        enlargeFont(title,1.5);
        if (obj instanceof JComponent && gcp(KEY_PRINTABLE_COMPONENTS,obj)!=null) pEast.add(ChButton.doPrint(obj),0);
        final Object p= pnl(CNSEW,title,null,null,pEast);
        setMaxSze(9999,ICON_HEIGHT+2,p);
        return p;
    }
    /* <<< Help and Source <<< */
    /* ---------------------------------------- */
    /* >>> ClientProperty >>> */
    static Map map(Object o) {
        return
            o instanceof ChTextArea || o instanceof ChTextView || o instanceof ChJTextPane
            || o instanceof ChTextField ? ChTextComponents.tools(o).MAPCP :
            o instanceof ChScrollBar ? ((ChScrollBar)o).MAPCP :
            o instanceof ChButton ? ((ChButton)o).MAPCP :
            o instanceof ChButton.MItem ? ((ChButton.MItem)o).MAPCP :
            o instanceof ChPanel ? ((ChPanel)o).MAPCP :
            o instanceof ChCombo ? ((ChCombo)o).MAPCP :
            o instanceof ChJList ? ((ChJList)o).MAPCP :
            o instanceof HasMap ? ((HasMap)o).map(true) :
            null;
    }
    private static Map<Object,Map> _mapCP=new WeakHashMap();
    private static Map mapcp(Object o, boolean create) {
        Map m=map(o);
        if (m==null) m= _mapCP.get(o);
        if (m==null && create) _mapCP.put(o,m=new HashMap());
        return m;
    }
    public static Runnable thread_pcp(Object key, Object value,  Object c) {
        return thrdM("pcp",ChUtils.class, new Object[]{key,value,c});
    }
    public static Object pcpReturn(Object key, Object value,  Object c) { pcp(key,value,c); return c; }
    public static void pcp(Object key, Object value, Object c) {
        if (c==null || key==null) return;
        final Map m=c instanceof Map ? (Map)c : mapcp(c,true);
        synchronized(m) { m.put(key,value); }
    }

    public static void pcpAddOpt(Object key, long opt, Object c) { if (opt!=0) pcp( key, longObjct(atol(gcp(key,c))|opt),c);}
    public static void pcp2(boolean weak, Object key1, Object key2, Object value,  Object c) {
        if (c==null || key1==null || key2==null) return;
        Map m=gcp(key1, c, Map.class);
        if (m==null && value!=null) pcp(key1,m=weak ? new WeakHashMap() : new HashMap(), c);
        if (m!=null) {
            synchronized(m) {
            m.put(key2,value);
            if (sze(m)==0) pcp(key1, null, c);
            }
        }
    }
    public static void addCP(Object key, Object value,  Object container) { addCP(key, value,container, 4); }
    public static void addCP(Object key, Object value,  Object container, int reserve) {
        if (container==null || value==null || key==null) return;
        final Object[] collection=oo(gcp(key,container));
        if (!cntains(value,collection)) {
            final Object[] oo=ad(value, collection, reserve, Object.class);
            if (oo!=collection) pcp(key,oo,container);
        }
    }
    public static String gcps(Object key, Object c) { return gcp(key,c,String.class); }
    public static <T extends Object>T gcp(Object key, Object c, Class<T> clazz) {
        final Object o=gcp(key,c);
        return isInstncOf(clazz,o) ? (T)o : null;
    }
    public static <T extends Object>T gcp2(Object key1, Object key2,  Object c, Class<T> clazz) {
        if (c==null || key1==null || key2==null) return null;
        final Map m=gcp(key1,c, Map.class);
        if (m==null) return null;
        final Object o;
        synchronized(m) { o=deref(m.get(key2)); }
        return isInstncOf(clazz,o) ? (T)o : null;
    }
    public static <T extends Object>T gcpClear(Object key, Object c, Class<T> clazz) {
        final Object o=gcp(key,c);
        if (o!=null) pcp(key,null,c);
        return isInstncOf(clazz,o) ? (T)o : null;
    }
    public static Object gcpa(Object key, Object c0) {
        Object c=c0, ret=null;
        for(int i=10; c!=null && --i>=0;) {
            ret=gcp(key,c);
            if (ret!=null) break;
            c=gcp(KEY_CLONED_FROM,c);
        }
        return ret;
    }
    public static Object gcp(Object key, Object c) {
        if (c==null || key==null) return null;
        final Map m=c instanceof Map ? (Map)c : mapcp(c,false);
        if (m!=null) synchronized(m) { return deref(m.get(key)); }
        return null;
    }

    public static Object mkIdObjct(String key, Object o) {
        Object k=gcp(key,o);
        if (k==null) pcp(key, k=new Object(),o);
        return k;
    }
    /* <<< ClientProperty <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */

    private static long _dragFilesWhen;
    private static TransferHandler _fth;
    public final static String KEY_DND_NOT_ITSELF="CU$$DND_NI", KEY_DND_OBJ="CC$$KDO";
    public static boolean jListDoDnD(AWTEvent mev) {
        if (mev.getID()==MouseEvent.MOUSE_DRAGGED) {
            final JComponent comp=derefJC(mev.getSource());
            if (comp==null) return false;
            final List dndFile=dndV(true,comp);
            adAllUniq(oo(gcp(KEY_DND_OBJ,comp)),dndFile);
            for(Object o: ChUtils.contextObjectsAt(point(mev),comp))  {
                if (gcp(KEY_DND_NOT_ITSELF,o)==null) adUniq(o,dndFile);
                for(Object f : oo(runCR(o,PROVIDE_DND_FILES))) {
                    if (f==null || (f instanceof File && !((File)f).exists())) continue;
                    adUniq(f,dndFile);
                }
            }
            if (sze(dndFile)>0 ) {
                exportDrg(comp, mev, TransferHandler.COPY);
                return true;
            }
        }
        return false;
    }
    public static int tipDnd(Object but, BA sb) {
        int count=0;
        final int N=sb==null?0:tipDnd(but, null);
        for(Object f : oo(gcp(KEY_DND_OBJ, but))) {
            if (f instanceof File && ((File)f).exists() || f instanceof URL) {
                if (count++==0 && sb!=null) {
                    sb.a("To copy the file").a('s', maxi(N,1))
                        .a(", drag this ").a(but instanceof AbstractButton ? "button":"label")
                        .aln(" with the mouse to the drop target.\n<pre>");
                }
                if (sb!=null) sb.aln(f);
            }
        }
        if (count>0 && sb!=null) sb.aln("</pre>");
        return count;
    }

    public static void resetDragFiles() { _dragFiles=null;}
    public static File[] getDragFiles() {
        final File ff[]=_dragFiles;
        return ff!=null && currentTimeMillis()-_dragFilesWhen < 180*1000 ? ff:  NO_FILE;
    }
    public static Point dndScreenLocation() { return _dndScreen; }
    private static Point _dndScreen;
    public static void exportDrg(JComponent c, AWTEvent ev, int copy_move) {
        if (c==null) return;
        {
            /*final Object oo=*/
            //invokeMthd(RFLCT_ASSERT_EXISTS, "getDndDateien",c);
        }
        if (javaVsn()==14) {
            error("Drag-drop not working for Java 1.4. Please update Java!");
            return;
        }
        if (ev instanceof MouseEvent) SwingUtilities.convertPointToScreen(_dndScreen=new Point(x(ev), y(ev)), (Component)ev.getSource());
        if (_fth==null) _fth=new TransferHandler("dndDateien") {
                @Override protected java.awt.datatransfer.Transferable createTransferable(JComponent c) { return ChUtils.createTransf(c);}
            };
        if (c.getTransferHandler()!=_fth) c.setTransferHandler(_fth);
        _fth.exportAsDrag(c,(MouseEvent)ev,copy_move);
    }
    private static java.awt.datatransfer.Transferable createTransf(JComponent c) {
        final Object[] oo=oo(invokeMthd(RFLCT_ASSERT_EXISTS, "getDndDateien",c));
        if (sze(oo)==0) return null;
        _dragFilesWhen=currentTimeMillis();
        _dragFiles=derefArray(oo,File.class);
        return new ChTransferable(oo, ChTransferable.FILE | ChTransferable.OBJECT );
    }
    public static File toFile(Object object) {
        if (object==null) return null;
        Object o=deref(object);
        if (o instanceof EventObject || o instanceof JPopupMenu) o=objectAt(null,o);
        Object f=o instanceof File || o instanceof URL ? file(o) : invokeMthd("getFile",o);
        if (o instanceof String) { final File f1=file(o); if (f1!=null && f1.exists()) f=f1;}
        return deref(f,File.class);
    }
    private final static Object KEY_dndF=new Object();
    public static List dndV(boolean clear, Object c) {
        List v=(List)gcp(KEY_dndF, c);
        if (v==null) pcp(KEY_dndF,v=new ArrayList(),c);
        if (clear) v.clear();
        return v;
    }
    /* <<< DnD <<< */
    /* ---------------------------------------- */
    /* >>> Window >>> */

    public static Runnable thread_setWndwState(char m, Object f) {
        return f==null?null:thrdM("setWndwState", ChUtils.class, new Object[]{charObjct(m), f});
    }
    public static void setWndwState(char state, Object windowOrChildOrArray) {
        if (windowOrChildOrArray==null) return;
        if (!isEDT()) {
            inEdtLater(thread_setWndwState(state,windowOrChildOrArray));
            return;
        }
        if (windowOrChildOrArray instanceof Object[]) {
            for(Object o : (Object[])windowOrChildOrArray) setWndwState(state,o);
            return;
        }
        final Window f=parentWndw(derefC(windowOrChildOrArray));
        if (f!=null) {
            if (javaVsn()==14) {
                if (state=='F' || state=='T') {
                    f.show();
                    if (f instanceof JFrame) ((JFrame)f).setExtendedState(0);
                    f.toFront();
                }
            } else {
                if ((state|32)=='t') setAOT(state=='T', f);
                if (state=='F') f.toFront();
                if (state=='F' || state=='T') {
                    final boolean aot=isAot(f);
                    setAOT(true,f);
                    if (state=='F' && !aot) inEDTms(thread_setWndwState('t', f), 55);
                }
            }
        }
    }
    private static long _whenAot;
    private static boolean _hintFloating=true;
    private final static String HINT_FLOATING="   Hint: Ctrl+T toggles frames \"always floating on top\" ";
    public static boolean setAotEv(AWTEvent ev) {
        if (javaVsn()>14 && isCtrl(ev) && ev.getID()==KEY_PRESSED && keyCode(ev)=='T') {
            final JFrame f=deref(parentWndw(ev.getSource()), JFrame.class);
            if (f==null) return true;
            final boolean b=!isAot(f);
            final long now=System.currentTimeMillis();
            if (now-_whenAot>222) setAOT(b, f);
            _whenAot=now;
            _hintFloating=false;
            return true;
        } else return false;
    }

    public static boolean isAot(Window w) {
        return w!=null && javaVsn()>14 && null!=forJavaVersion(15).run(RunIf15orHigher.GET_AOT,w);
    }
    public static void setAot(boolean b, Window w) {
        if (javaVsn()>14 && w!=null) forJavaVersion(15).run(RunIf15orHigher.SET_AOT,new Object[]{boolObjct(b), w});
    }
    public static void setAOT(boolean b, Window w) {
        setAot(b,w);
        final JFrame f=deref(w,JFrame.class);
        if (f!=null && javaVsn()>14) {
            final String
                t0=f.getTitle(),
                t=(b ? "^ " : "")+rplcToStrg(HINT_FLOATING, "", delPfx("^ ", t0));
            if (t0!=null && !t0.equals(t)) f.setTitle(t);
        }
    }

    public static String hintAOT(boolean always) { return always || _hintFloating ? HINT_FLOATING : null; }

    public static void packW(Object o) {
        if (o==null) return;
        if (!isEDT()) inEdtLater(thrdM("packW",ChUtils.class, new Object[]{o}));
        else {
            final Window w=parentWndw(o);
            if (w!=null) {
                try { w.pack(); } catch(Throwable ex){}
            }
        }
    }
    public static Runnable thread_setWndwStateT(char m, int x, int y, String ...title) {
        return thrdM("setWndwStateT", ChUtils.class, new Object[]{charObjct(m), intObjct(x),intObjct(y), title});
    }
    public static void setWndwStateT(char m, int x, int y, String ...title) {
        if (sze(title)==0) return;
        if (title.length==1 && title[0]=="D") title=ChAppleScript.TITLE_DOWNLOADS;
        if (isMac()) {
            if (m=='I') {
                startThrd(new ChExec(ChExec.WITHOUT_ASKING|ChExec.IGNORE_ERROR).setCommandLineV("osascript",ChAppleScript.getScript(ChAppleScript.MINIMIZE_DOWNLOADS)));
            }
            return;
        } else if (Insecure.EXEC_ALLOWED) new NativeTools().setWindowState(m,x,y,title);
    }
    public static void setWndwStateLaterT(char m, int sleep, String ...title) {
        startThrd(thrdRRR(thrdSleep(sleep), thread_setWndwStateT(m, -1, -1,title)));
    }
    public static void shwTxtInW(BA txt) { shwTxtInW((String)null,txt);}
    public static void shwTxtInW(String title, BA txt) { shwTxtInW(CLOSE_CtrlW|ChFrame.SCROLL_TOP|ChFrame.AT_CLICK|ChFrame.PACK,title,txt);}
    public static void shwTxtInW(long opt, String title, BA ba) {
        if (sze(ba)==0) return;
        if (!isEDT()) {
            inEdtLater(thrdM("shwTxtInW",ChUtils.class,new Object[]{longObjct(opt),title,ba}));
            return;
        }
        final String t=title!=null?title:toStrg(gcp(KEY_TITLE,ba));
        ChTextView tv=ba.textView(false);
        if ( (tv=ba.textView(false))==null) {
            final Object v=ba.getSendTo();
            if (v instanceof File) tv=new ChTextView((File)v);
            else if (v instanceof ChRunnable) {
                setTitl(t, ((ChRunnable)v).run(ChRunnable.RUN_SHOW_IN_FRAME, longObjct(opt)));
                return;
            }
        }
        if (tv==null) tv=ba.textView(true);
        tv.tools().showInFrame(opt,t);
    }
    /* <<< Window <<< */
    /* ---------------------------------------- */
    /* >>> Reflection >>> */

    public static String legacyMergeSort() {
        if (javaVsn()<17) return systProprty(SYSP_JAVA_VERSION);
        try {
            final java.lang.reflect.Field f=Class.forName("java.util.Arrays$LegacyMergeSort").getDeclaredField("userRequested");
            f.setAccessible(true);
            return toStrg(f.get(null));
        } catch(Throwable ex) { return "?"; }
    }

    private static void rflctPrintEx(long opt, Throwable ex, Object txt) {
        if ((opt&(RFLCT_VERBOSE|RFLCT_PRINT_EXCEPTIONS))!=0 && ex!=null)  {
            putln(RED_WARNING, txt, "\n",ex);
            if ((opt&RFLCT_PRINT_EXCEPTIONS)!=0) stckTrc(ex);
        }
    }
    private static void rflctSetAcc(long opt, Object member) {
        if (member!=null && 0!=(opt&RFLCT_SET_ACCESSIBLE)) {
            try{
                if (member instanceof Method) ((Method)member).setAccessible(true);
                else ((java.lang.reflect.Field)member).setAccessible(true);
            } catch(Throwable ex){rflctPrintEx(opt, ex, "setAccessible "); }
        }
    }
    public static Object rflctFieldVal(long opts, String field, Object instanceOrClassOrClassname) {
        final Object instOrClass=instOrClass(opts,instanceOrClassOrClassname);
        if (instOrClass==null || field==null) return null;
        final Class c= instOrClass instanceof Class ? (Class)instOrClass : instOrClass.getClass();
        Field f=null;
        try {
            f=c.getDeclaredField(field);
        } catch(NoSuchFieldException ex) {
            rflctPrintEx(opts, ex, "rflctField");
            return null;
        }
        rflctSetAcc(opts, f);
        if (0!=(RFLCT_RETURN_MEMBER&opts)) return f;
        try {
            return f==null?null:f.get(instOrClass instanceof Class ? null : instOrClass);
        } catch(Exception ex){ rflctPrintEx(opts, ex, "rflctFieldVal"); }
        return null;
    }

    public static Object invokeMthd(String methodName, Object instanceOrClassOrClassname) {
        return invokeMthd(0L, methodName,instanceOrClassOrClassname,NO_OBJECT);
    }
    public final static long RFLCT_ASSERT_EXISTS=1<<0, RFLCT_PRINT_EXCEPTIONS=1<<1, RFLCT_VERBOSE=1<<2,
        RFLCT_RETURN_ERROR=1<<3, RFLCT_SET_ACCESSIBLE=1<<4, RFLCT_RETURN_MEMBER=1<<5;

    public final static Object RFLCT_ERROR=new Object();
    private final static Set<String> _vReportedErrors=new HashSet();

    private static Object instOrClass(long opts,Object ioc) {
        if (ioc instanceof String) {
            try {
                return Class.forName((String)ioc);
            } catch(Exception e){
                rflctPrintEx(opts,e,"invokeMthd ");
                if ( (opts&RFLCT_ASSERT_EXISTS)!=0) assrt();
                return null;
            }
        }
        return ioc;
    }

    public static Method findMthd(long opts, String methodName, Object instanceOrClass, Object para[]) {
        final Object instOrClass=instOrClass(opts,instanceOrClass);
        if (instOrClass==null || methodName==null) return null;

        final Class c= instOrClass instanceof Class ? (Class)instOrClass : instOrClass.getClass();
        final int nPara=sze(para);
        final boolean alsoPrivate=0!=(opts&RFLCT_SET_ACCESSIBLE);
        if (nPara==0) {
            if (alsoPrivate) {
                Class c2=c;
                do {
                    try { return c2.getDeclaredMethod(methodName); } catch(Exception ex){}
                    c2=c2.getSuperclass();
                } while(c2!=null && c2!=Object.class);
            }

            try {
                return c.getMethod(methodName);
            } catch(Exception ex){ rflctPrintEx(opts, ex, "invokeMthd");
            }
        } else {
            nextMethod:
            for(Method m : alsoPrivate ? c.getDeclaredMethods() : c.getMethods()) {
                if (!m.getName().equals(methodName)) continue;
                if (!alsoPrivate && (m.getModifiers()&java.lang.reflect.Modifier.PUBLIC)==0) continue;
                final Class[] cc=m.getParameterTypes();
                if (cc.length!=nPara) continue;

                for(int iP=nPara; --iP>=0; ) {
                    if (para[iP]==null) continue;
                    final Class ci=cc[iP], pi=para[iP].getClass();
                    if ( !(ci==boolean.class&&pi==Boolean.class) &&
                         !(ci==int.class&&pi==Integer.class) &&
                         !(ci==long.class&&pi==Long.class) &&
                         !(ci==short.class&&pi==Short.class) &&
                         !(ci==float.class&&pi==Float.class) &&
                         !(ci==double.class&&pi==Double.class) &&
                         !(ci==byte.class&&pi==Byte.class) &&
                         !(ci==char.class&&pi==Character.class) &&

                         !cc[iP].isAssignableFrom(pi)) continue nextMethod;
                }
                return m;
            }
        }
        if ((opts&RFLCT_ASSERT_EXISTS)!=0) { putln("methodName=",methodName); assrt(); }
        if (myComputer() && !"getText".equals(methodName) && !strEquls("getCustomizable", methodName) && _vReportedErrors.add(c+"."+methodName)) {
            putln(RED_WARNING,"No such method ",c+" "+methodName);
            _vReportedErrors.add(methodName);
        }
        return null;
    }
    public static Object invokeMthd(long opts, String methodName, Object instOrClass) {
        return invokeMthd(opts, methodName, instOrClass, null);
    }
    public static Object invokeMthd(long opts, String methodName, Object instOrClass,  Object para[]) {
        return invokeMthd(opts, findMthd(opts,methodName, instOrClass, para), instOrClass, para);
    }
    public static Object invokeMthd(long opts, Method m, Object instOrClass, Object para[]) {
        final Object objectError= 0!=(opts&RFLCT_RETURN_ERROR)?RFLCT_ERROR : null;
        if (m==null) return null;
        rflctSetAcc(opts,m);
        if ((opts&RFLCT_VERBOSE)!=0) putln(YELLOW_DEBUG+"invokeMthd method=", m);
        try {
            return m.invoke(instOrClass,para!=null ? para : NO_OBJECT);
        } catch(Exception e){ rflctPrintEx(opts, e, "Invokemthd method="+m); stckTrc(e); }
        return objectError;
    }
    public static boolean setFld(long opts, Object o, String name, Object value) {
        if (o==null) return false;
        try{
            Class c=o instanceof Class ? (Class)o : o.getClass();
            java.lang.reflect.Field f=null;
            while(c!=null) {
                if ((opts&RFLCT_VERBOSE)!=0) putln("setFld c=", c," name=",name);
                try { f=c.getDeclaredField(name); break; } catch(Exception ex){}
                c=c.getSuperclass();
            }
            if ((opts&RFLCT_VERBOSE)!=0 || f==null) putln("setFld f=", f);
            if (f==null) {
                if ( (opts&RFLCT_PRINT_EXCEPTIONS)!=0) { stckTrc(); }
            } else {
                rflctSetAcc(opts,f);
                f.set(o==c?null:o,value);
                return true;
            }
        } catch(Exception ex){ rflctPrintEx(opts, ex, "setFld="+name); }
        return false;
    }
    private final static Map<Class,String[]> mapFieldName=new HashMap();
    private final static Map<Class,Map<Integer,String>> mapFieldNameM=new HashMap();
    public static String[] finalStaticInts(Class clas, int MAX) { return finalStaticInts(clas, "", MAX); }
    public static String[] finalStaticInts(Class clas, String startsWith, int MAX) {
        String[] nn=mapFieldName.get(clas);
        if (nn==null) mapFieldName.put(clas,nn=_staticInts(clas,"",MAX,null));
        return nn;
    }
    public static Map<Integer,String> finalStaticInts(Class clas, String startsWith) {
        Map<Integer,String> m=mapFieldNameM.get(clas);
        if (m==null) {
            _staticInts(clas, startsWith, 0,  m=new HashMap());
            mapFieldNameM.put(clas,m);
        }
        return m;
    }
    public static boolean processEvt(EventObject ev,Object component) {
        return ERROR_OBJECT!=invokeMthd(RFLCT_RETURN_ERROR, "processEvent", component, oo(ev));
    }

    public static String rmMnemon(String txt) {
        if (txt==null) return null;
        CharSequence s=txt;
        for(int t=2; --t>=0;) {
            final char c=t==0?'^':'&';
            final int i=strchr(c,txt);
            if (i>=0 && is(LETTR_DIGT, s, i+1)) s=new BA(99).a(s,0,i).a(s,i+1,MAX_INT);
        }
        return toStrg(s);
    }

    public static int mnemon(String t) {
        if (t==null) return 0;
        int acc=t.indexOf('^');
        if (acc<0) acc=t.indexOf('&');
        // if (chrAt(acc,t)=='&') putln(RED_WARNING+"t="+t);
        return acc<0 ? 0 : ((chrAt(acc,t)<<8)|chrAt(acc+1,t));
    }

    public static String txtForTitle(Object button) {
        return rmMnemon(rplcToStrg("<br>","",delSfx(" as text",delSfx('\u25bc' ,delSfx("...", button instanceof CharSequence ? button : getTxt(button))))));
    }
    public static String getTxt(Object button) {
        final Object b=deref(button);
        return
            b instanceof JTextComponent ? ((JTextComponent)b).getText() :
            b instanceof AbstractButton ? ((AbstractButton)b).getText() :
            b instanceof JLabel ? ((JLabel)b).getText() :
            b instanceof JPopupMenu ? ((JPopupMenu)b).getLabel() :
            b instanceof Frame ? ((Frame)b).getTitle() :
            toStrg(invokeMthd("getText",b));
    }
    public static void setTitl(String txt, Object frame) {
        final Object b=deref(frame);
        if (b instanceof Frame)  ((Frame)b).setTitle(txt);
        else if (b!=null) invokeMthd(0L,"setTitle",b, oo(txt));
    }
    public static void setTxtAndReval(String txt0, Object b, int msAfter) {
        final String old=getTxt(b), txt=txt0!=null?txt0:"";
        if (b==null || sze(old)+sze(txt)==0 || txt.equals(old)) return;
        setTxt(txt,b);
        if (strgWidth(b,old)!=strgWidth(b,txt)) ChDelay.revalidate(parentC(b), msAfter);
    }
    public static Object setIcn(Icon i, Object button) {
        final Object b=deref(button);
         if (b==null) return null;
         else if (b instanceof JComponent && !isEDT())  inEdtLater(thrdCR(instance(0), "setIcn", new Object[]{i,button}));
         else if (b instanceof AbstractButton)  ((AbstractButton)b).setIcon(i);
         else if (b instanceof JLabel) ((JLabel)b).setIcon(i);
         else invokeMthd(0L,"setIcon",i, oo(i));
         return b;
    }
    public static void setTxt(String txt, Object button) {
        final Object b=deref(button);
        if (b==null) return;
        else if (b instanceof JComponent && !isEDT())  inEdtLater(thrdCR(instance(0), "setTxt", new Object[]{txt,button}));
        else if (b instanceof AbstractButton)  ((AbstractButton)b).setText(txt);
        else if (b instanceof JLabel) ((JLabel)b).setText(txt);
        else if (b instanceof ScrollLabel) ((ScrollLabel)b).t(txt);
        else if (b instanceof JTextComponent)  ((JTextComponent)b).setText(txt);
        else invokeMthd(0L,"setText",b, oo(txt));
    }
    public static Icon getIcn(Object button) {
        final Object b=deref(button);
        final Object i=
            b==null ? null :
            b instanceof AbstractButton ?  ((AbstractButton)b).getIcon() :
            b instanceof JLabel ? ((JLabel)b).getIcon() :
            (Icon)invokeMthd("getIcon",b);
        return i instanceof Icon ? (Icon)i : null;
    }
    private static String[] _staticInts(Class clas, String startsWith, int MAX, Map<Integer,String>map ) {
        final java.lang.reflect.Field[] fields=clas.getDeclaredFields();
        final String[] nn=MAX>0 ? new String[MAX] : null;
        for(java.lang.reflect.Field field:fields) {
            final int modi=field.getModifiers();
            if ( (modi & java.lang.reflect.Modifier.FINAL)==0 || (modi & java.lang.reflect.Modifier.PUBLIC)==0 || (modi & java.lang.reflect.Modifier.STATIC)==0) continue;
            try {
                final Object o=field.get(null);
                if (!(o instanceof Integer)) continue;
                final int value=atoi(o);
                final String name=field.getName();
                if (sze(startsWith)>0 && !name.startsWith(startsWith)) continue;
                if (clas==ChUtils.class && !name.startsWith("SYSP_") && !name.startsWith("IS_")) continue;
                if (nn!=null && value>=0 && value<nn.length && nn[value]==null) nn[value]=name.intern();
                if (map!=null) {
                    final Object key=intObjct(value);
                    map.put((Integer)key, field.getName().intern());
                }
            } catch(Exception e){ }
        }
        return nn;
    }
    public static String[] finalStaticStrings(boolean values, String namePfx, Class clazz) {
        final List v=new ArrayList(55);
        for(java.lang.reflect.Field f:  clazz.getFields()) {
            final int m=f.getModifiers();
            if ( (m&java.lang.reflect.Modifier.FINAL)==0 || (m&java.lang.reflect.Modifier.PUBLIC)==0 || (m&java.lang.reflect.Modifier.STATIC)==0) continue;
            if (!f.getName().startsWith(namePfx)) continue;
            if (f.getType()!=String.class) continue;
            try{v.add(values ? f.get(null) : f.getName());} catch(Exception ex){ stckTrc(ex);}
        }
        final String ss[]=strgArry(v);
        sortArry(ss);
        return ss;
    }
    public static boolean isInstncOf(Class interf, Object o) {
        if (o==null || interf==null) return false;
        if (o instanceof HotswapProxy) {
            final ProxyClass pc=((HotswapProxy)o).hotswap_getProxyClass();
            return pc!=null && idxOf(interf,pc.getProxyInterfaces())>=0;
        }
        return  interf.isAssignableFrom(o.getClass());
    }
    public static String shrtClasNam(Object o) {
        final String cn=clasNam(o);
        return cn==null?null:cn.substring(cn.lastIndexOf('.')+1);
    }

     public static Class clas(Object o0) {
        Object o=ChCombo.selItem(o0);
        if (o instanceof JList) o=((JList)o).getSelectedValue();
        if (o==null) return null;
        final ProxyClass pc=o instanceof ProxyClass ? (ProxyClass)o : o instanceof HotswapProxy ?  ((HotswapProxy)o).hotswap_getProxyClass() : null;
        if (pc!=null) return pc.getClassInstance();
        if (o instanceof String )  try { return Class.forName((String)o); } catch(Throwable ex){}
        else if (o instanceof Class) return (Class)o;
        return o.getClass();
    }
    public static String clasNam(Object o) {
        if (o instanceof String) return (String)o;
        final Class c=clas(o);
        if (c==null) return null;
        final String name=c.getName();
        final int iDollar=name.indexOf('$');
        return iDollar>0 ? name.substring(0,iDollar) : name;
    }
    public static String clasNamWithSlash(Object o) { final String s=clasNam(o); return s!=null ? s.replace('.','/') :null;}
    public static String niceShrtClassNam(Object viewer) {
        return delSfx("PROXY",delPfx("Ch",delPfx("PairAligner",delPfx("Superimpose_",delPfx("SubcellularLocation_",shrtClasNam(viewer))))));
    }
    public static boolean isAssignblFrm(Class interf, Object o) {
        final Class c=clas(o);
        return interf!=null && c!=null && interf.isAssignableFrom(c);
    }
    public static boolean cntainsAssignblFrm(Class c, Object oo[]) {
        if (c!=null && oo!=null) for(Object o : oo) if (isAssignblFrm(c,o)) return true;
        return false;
    }
    public static Object mkInstance(Object o0) { return mkInstance(o0, (Class)null,false); }
    public static <T extends Object>T mkInstance(Object o0, Class<T> clazz) { return mkInstance(o0,clazz,false); }
    public static <T extends Object>T mkInstance(Object o0, Class<T> clazz, boolean error) {
        if (o0==null) return null;
        final ChCombo combo=o0 instanceof ChCombo ? (ChCombo)o0 : null;
        Object o=o0, inst=null;
        if (combo!=null) o=combo.getSelectedItem();
        else if (o instanceof JList) o=((JList)o).getSelectedValue();
        if (o instanceof String) o=clas(o);
        try {
            if (o instanceof ProxyClass) inst=((ProxyClass)o).newInstance();
        } catch(Throwable e){ stckTrc(e);}
        if (inst==null && o instanceof Class) {
            try {
                inst=  ((Class)o).newInstance();
            }catch(Exception e){ if (error) putln(RED_CAUGHT_IN," mkInstance ",e); }
        }
        if (inst!=null) {
            if (combo!=null) setSharedParas(combo.instanceOfSelectedClass(null), inst);
            if (clazz!=null && !isAssignblFrm(clazz,inst)) inst=null;
        }
        if (inst==null && error) error("Could not instantiate "+o0+" "+clazz);
        return (T)inst;
    }
    public static Class name2class(String name) {
        if (name==null) return null;
        Class c=null;
        try {
            try {
                try {
                    c=Class.forName(name.replace('/','.'));
                } catch(NoClassDefFoundError er){}
            } catch(ClassNotFoundException ex){}
        } catch(ExceptionInInitializerError ex) { stckTrc(ex);}
        return c;
    }
    private static Map<String,Class> mapFindC;
    public static Class findClas(String s,String[] packages) {
        if (!is(LETTR,s,0)) return null;
        Map<String,Class> m=mapFindC;
        if (m==null) {
            m=mapFindC=new HashMap();
            final BA sb=new BA(99);
            for(byte b : "ISJBC".getBytes()) {
                for(int brace=5;--brace>0;) {
                    sb.clr().a(b=='B'?"byte": b=='S'?"short": b=='I'?"int": b=='J'?"long": b=='C'?"char":null);
                    for(int i=brace;--i>=0;) sb.a("[]");
                    m.put(sb.toString(), name2class(sb.clr().a('[',brace).a((char)b).toString()));
                }
            }
            m.put("byte",  byte.class);
            m.put("short",short.class);
            m.put("int",    int.class);
            m.put("long",  long.class);
            m.put("char",  char.class);
        }
        Class c=m.get(s);
        if (c!=null) return c;
        int brace=0;
        final int L=sze(s);
        for(int i=L; strEquls("[]",s,i-=2); ) brace++;
        if (!cntainsOnly(LETTR_DIGT_US_DOT_SLASH, s,0,L-2*brace)) return null;
        final BA sb=new BA(33);
        for(int i=s.indexOf('.')>=0?-1:sze(packages); --i>=-2;) {
            final String p=i==-2 ? "" : i==-1 ? "java.lang." : packages[i];
            if (i>=0 && (sze(p)==0 || "java.lang.".equals(p) || "java/lang/".equals(p))) continue;
            sb.clr().a('[',brace).a('L',brace==0?0:1).a(p).a(s,0,L-2*brace).replaceChar('/','.').a(';',brace==0?0:1);
            if ((c=name2class(sb.toString()))!=null) {
                if (i<0) m.put(s,c);
                return c;
            }
        }
        return null;
    }
    /** get the file from a javadoc file generated with the JDK javadoc tool.*/
    public static Class javadocFile2class(CharSequence txt) {
        final int idxContent=strstr(" CONTENT=\"",txt);
        if (idxContent>0 && idxContent< strstr("</HEAD>",txt)) {
            final int idxSpace=strstr(0L," ",txt,idxContent+1, MAX_INT);
            if (idxSpace>0 && (substrg(txt,idxSpace,idxSpace+12).equals(" interface\">") || substrg(txt,idxSpace,idxSpace+8).equals(" class\">"))) {
                return name2class(substrg(txt,idxContent+10,idxSpace).trim());
            }
        }
        return null;
    }
    public static Class clazz(Object o) { return o!=null ? o.getClass() : null;}
    /* <<< Reflection <<< */
    /* ---------------------------------------- */
    /* >>> Name >>> */
        public static <T extends Object> T findWithName(String name, Object oo[],Class<T> clazz ) {
        if (oo==null||name==null) return null;
        for(Object s : oo) {
            if (isAssignblFrm(clazz,s) && name.equals(nam(s))) return (T)s;
        }
        return null;
    }
    public static String getID(Object o) { return o instanceof HasID ? ((HasID)o).getID() : null; }
    public static String nam(Object o) {
        return
            o==null ? null :
            o instanceof String ? (String)o :
            o instanceof Class ?     ((Class)o).getName() :
            o instanceof File ?       ((File)o).getName() :
            o instanceof HasName ? ((HasName)o).getName() :
            toStrg(invokeMthd("getName",o));
    }
    public static Object longestName(Object oo[]) {
        int len=0;
        Object longest=null;
        for(int i=sze(oo) ;--i>=0;) {
            final String s=oo[i] instanceof String ? (String)oo[i] : nam(oo[i]);
            if (sze(s)>len)  {
                len=s.length();
                longest=oo[i];
            }
        }
        return longest;
    }
    public static float scor(Object o) {
        return o!=null && isAssignblFrm(HasScore.class,o) ? ((HasScore)o).getScore() : Float.NaN;
    }
    /* <<< Name  <<< */
    /* ---------------------------------------- */
    /* >>> Reference >>> */
    private final static Map<Object,Object> mapWRef=new WeakHashMap(999);
    private final static Object REF_NULL=newWeakRef(null);
    public static Object wref(Object o) { return wref(o,true); }
    public static Object wref(Object o, boolean create) {
        if (o==null) return REF_NULL;
        if (o instanceof ChTextArea || o instanceof ChTextView || o instanceof ChJTextPane || o instanceof ChTextField) {
            return ChTextComponents.tools(o)._ref;
        }
        if (o instanceof HasWeakRef) return ((HasWeakRef)o).wRef();
        if (o instanceof ChButton) return ((ChButton)o).WEAK_REF;
        if (o instanceof ChJScrollPane) return ((ChJScrollPane)o).WEAK_REF;
        if (o instanceof ChPanel) return ((ChPanel)o).WEAK_REF;
        synchronized(mapWRef) {
            Object r;
            final Map m=map(o);
            if (m!=null) {
                final String key="CC$$KWR";
                r=m.get(key);
                if (r==null && create) m.put(key,r=newWeakRef(o));
            } else {
                r=mapWRef.get(o);
                if (r==null && create) mapWRef.put(o,r=newWeakRef(o));
            }
            return r;
        }
    }
    public static Object[] weakRefArry(final Object object) {
        final Object oo=object instanceof Set ? oo(object) : object;
        assert oo instanceof Object[] || oo instanceof List;
        final Object rr[]=new Reference[sze(oo)];
        for(int i=rr.length; --i>=0;) {
            final Object o=get(i,oo);
            rr[i]=o==null?null:o instanceof Reference ? o : wref(o);
        }
        return rmNullA(rr);
    }
    public static Object newSoftRef(Object o) { return new SoftReference(o); }
    public static Object newWeakRef(Object o) { return new WeakReference(o); }
    private static char[] ccSoftRef(int i, Object[] oo, int size) {
        char[] cc=deref(oo[i], char[].class);
        if (cc==null||cc.length<size) oo[i]=newSoftRef(cc=new char[size]);
        return cc;
    }
    public static BA baSoftClr(Object[] oo) { return baSoftClr(0,oo);}
    public static BA baSoftClr(int idx, Object[] oo) {
        BA ba=deref(oo[idx], BA.class);
        if (ba==null) oo[idx]=newSoftRef(ba=new BA(0));
        else ba.clr();
        return ba;
    }
    public static StringBuffer sbSoftClr(int i, Object[] oo) {
        StringBuffer sb=deref(oo[i], StringBuffer.class);
        if (sb==null) oo[i]=newSoftRef(sb=new StringBuffer(99));
        else sb.setLength(0);
        return sb;
    }
    public static StringBuffer sbClr(Object[] oo) {
        StringBuffer sb=(StringBuffer)oo[0];
        if (sb==null) oo[0]=sb=new StringBuffer(99);
        else sb.setLength(0);
        return sb;
    }
    public static List alSoftClr(int idx, Object[] oo) {
        ArrayList al=deref(oo[idx], ArrayList.class);
        if (al==null) oo[idx]=newSoftRef(al=new ArrayList(0));
        else al.clear();
        return al;
    }
    public static <T extends Object> T fromSoftRef(int i, Object[] oo, Class<T> clazz) {
        T t=deref(oo[i],clazz);
        if (t==null) {
            Object o=
                clazz==TextMatches.class ? new TextMatches() :
                clazz==Map.class ? new HashMap() :
                null;
            if (o==null) {
                try { o=clazz.newInstance(); } catch(Exception ex) { debugExit(clazz);}
            }
            oo[i]=t=(T)o;
        }
        return t;
    }
    public static List vClr(Object[] oo) {return vClr(0,oo); }
    public static List vClr(int i, Object[] oo) {
        List v=deref(oo[i], List.class);
        if (v==null) oo[i]=v=new ArrayList();
        else v.clear();
        return v;
    }
    public static List vSoftClr(int i, Object[] oo) {
        List v=deref(oo[i], List.class);
        if (v==null) oo[i]=newSoftRef(v=new ArrayList());
        else v.clear();
        return v;
    }
    public static Set hashSetClr(int i, Object[] oo) {
        Set v=deref(oo[i], Set.class);
        if (v==null) oo[i]=v=new HashSet();
        else v.clear();
        return v;
    }
    public static BA baClr(Object BUF[]) { return baClr(0,BUF);}
    public static BA baClr(int i, Object BUF[]) {
        BA ba=(BA)BUF[i];
        if (ba==null) BUF[i]=ba=new BA(99); else ba.setEnd(0);
        return ba;
    }
    private static int _baTip;
    private final static Object BYTE_ARR[]=new Object[16];
    public static BA baTip() { return baSoftClr(_baTip++%16,BYTE_ARR);}
    /* <<< Reference <<< */
    /* ---------------------------------------- */
    /* >>> Standard Streams >>> */
    public static void debugExit(Object...oo) {
        if (myComputer()) {
            putln(oo);
            //stckTrc();
            System.exit(0);
        }
    }
    public static void putln(boolean b) { outOrErr().println(b); }
    public static void putln(int n) { outOrErr().println(n); }
    public static void putln(Object o) {
        puts(o);
        outOrErr().print('\n');
        _stderr=false;
    }
    public static void putln(Object o0, Object o1) {  puts(o0);  putln(o1); }
    public static void putln(Object o0, Object o1, Object o2) {  puts(o0); putln(o1, o2); }
    public static void putln(Object o0, Object o1, Object o2, Object o3) {  puts(o0); putln(o1,o2,o3); }
    public static void putln(Object o0, Object o1, Object o2, Object o3, Object o4) {  puts(o0); putln(o1,o2,o3, o4); }
    private static int _printAnsi;
    public static boolean printAnsiColors() {
        if (_printAnsi==0)  _printAnsi=!isSystProprty(IS_UNIX_SHELL) || isSystProprty(IS_WEBSTARTED) ? CFALSE : CTRUE;
        return _printAnsi==CTRUE;
    }
    /** For reading manpages */
    public static BA decodeEscChrs(BA ba, char mode) {
        final int L=sze(ba);
        if (L<3) return ba;
        final byte T[]=ba.bytes();
        final BA out=new BA(L);
        final String
            boldf=mode=='A'?ANSI_BOLD : "",
            under=mode=='A'?ANSI_UL : "",
            clear=mode=='A'?ANSI_RESET : "";
        String lastCode=null;
        for(int i=0; i<L;i++) {
            final char c=(char)T[i], c1=i<L-1 ? (char)T[i+1] :0, c2=i<L-2 ? (char)T[i+2] :0;
            final String code=c1!=8 ? null : c=='_' ? under : boldf;
            if (c==0xffe2 && c1==0xff80 && c2==0xff90) { /* hyphen */
                out.a('-');
                i+=2;
            } else if (code!=null) {
                if (code!=lastCode) out.a(code);
                out.a(c2);
                i+=2;
            } else {
                if (lastCode!=null) out.a(clear);
                if (c<0xff) out.a(c);
            }
            lastCode=code;
        }
        return out;
    }
    private static boolean _stderr;
    private static PrintStream outOrErr() { return _stderr?System.err:System.out;}
    public static String onlyOnce(String msg) {
        return msg==null || _vMsg1.add(msg) ? msg : null;
   }
    public static boolean puts(Object o) {
        if (o==null) return false;
        if (o=="MY_COMPUTER") _myComp=CTRUE;
        final byte bb[]=deref(o,byte[].class);
        try {
            if (o==TOERR) _stderr=true;
            else if (!printAnsiColors() && (bb!=null || o instanceof CharSequence) && 0<=strchr('\u001B', o)) {
                final CharSequence cs=deref(o,CharSequence.class);
                final int L=sze(o);
                for(int i=0; i<L; i++) {
                    final char c=bb!=null?(char)bb[i] : cs.charAt(i);
                    if (c=='\u001B') i=strchr('m',o,i,L);
                    else outOrErr().print(c);
                }
            } else if (bb!=null) {
                outOrErr().write(bb);
            } else if (o instanceof char[]) {
                puts(new String((char[])o));
            } else if (o instanceof boolean[]) {
                puts(boolToTxt((boolean[])o));
            } else if (o instanceof int[]) {
                int count=0;
                for(int i : (int[])o) {
                    if (0<count++) outOrErr().print(',');
                    outOrErr().print(i);
                }
            } else if (o instanceof Object[]) {
                int count=0;
                for(Object o2 : (Object[])o) {
                    if (0<count++) puts(", ");
                    puts(o2);
                }
            } else if (o instanceof File) {
                puts(fPathUnix((File)o));
            } else outOrErr().print(o);
        } catch(IOException iox){}
        /*  if ((o instanceof CharSequence || o instanceof byte[]) && strstr("No such method",o)>=0) stckTrc(); */
        return true;
    }
    /* <<< Standard Streams <<< */
    /* ---------------------------------------- */
    /* >>> Columns >>> */
    public static int tabulatrs(char ch09, byte txt[], int from, int end, int[] TABS) {
        int count=0;
        Arrays.fill(TABS,-1);
        final int E=mini(end,txt.length);
        for(int i=maxi(0,from); i<E; i++) {
            if (txt[i]==ch09) TABS[count++]=i;
            if (TABS.length==count || txt[i]=='\n') break;
        }
        return count;
    }
    public static int nthColumn(int n, char separator, byte[] txt, int from, int to) {
        int pos=maxi(0,from);
        for(int i=n; --i>=0;) if (  (pos=1+strchr(separator,txt,pos,to)) <1) return -1;
        return pos;
    }
    /* <<< Columns <<< */
    /* ---------------------------------------- */
    /* >>> HashMaps >>> */
    public final static int MAP_VALUES_AS_INTERN=1<<17,
        MAP_RM_TERMINAL_QUOTES=1<<18,
        MAP_ALLOW_EMPTY_STRING=1<<19,
        MAP_KEYS_LETTR_DIGT_LC=1<<20;
    public static void mapKey2Array(long options, BA ba, Map<String, String[]> map) {
        if (ba==null) return;
        final ChTokenizer tok=new ChTokenizer();
        final List<String> v=new ArrayList(), vVal=new ArrayList();
        final int B=ba.begin(), ends[]=ba.eol();
        final byte[] T=ba.bytes();
        for(int iL=0; iL<ends.length; iL++) {
            final int b= iL==0?B : ends[iL-1]+1, e=strchr(STRSTR_E,'#',T,b,ends[iL]);
            final int tab=strchr('\t',T,b,e);
            if (tab<0) continue;
            v.clear();
            tok.setText(T,tab+1, e);
            while(tok.nextToken()) {
                final String s=tok.asString();
                adUniq((options&MAP_VALUES_AS_INTERN)!=0 ? s.intern() : s,v);
            }
            if (v.size()>0) {
                tok.setText(T,b, tab);
                while(tok.nextToken()) {
                    final String k=tok.asString();
                    final String vv[]=map.get(k);
                    vVal.clear();
                    for(int i=v.size(); --i>=0;) adUniq(v.get(i),vVal);
                    adAllUniq(vv,vVal);
                    if (vv==null || vv.length!=vVal.size())map.put(k, strgArry(vVal));
                }
            }
        }
    }
    public static void mapColumns012345to9(long options, BA ba, Map<String,String> map) {
        if (ba==null) return;
        final ChTokenizer TOK=new ChTokenizer();
        final int B=ba.begin(), ends[]=ba.eol();
        final byte[] T=ba.bytes();
        final List<String> v=new ArrayList();
        final BA sbLettrDigt=(options&MAP_KEYS_LETTR_DIGT_LC)!=0 ? new BA(33) : null;
        for(int iL=0; iL<ends.length; iL++) {
            final int b= iL==0?B : ends[iL-1]+1, e=strchr(STRSTR_E,'#',T,b,ends[iL]);
            if (e-b<3) continue;
            String rep=null;
            TOK.setText(T,b,e);
            v.clear();
            while(TOK.nextToken()) v.add(rep=TOK.asString());
            final int n=sze(v);
            if (rep!=null && n>1) {
                if ((options&MAP_VALUES_AS_INTERN)!=0) rep=rep.intern();
                for(int i=sbLettrDigt!=null ? n : n-1; --i>=0;) {
                    String val=v.get(i);
                    if (sbLettrDigt!=null) val=sbLettrDigt.clr().filter(FILTER_TO_LOWER, LETTR_DIGT, val).toString();
                    map.put(val,rep);
                }
            }
        }
    }
    public static <VALUE extends Object, KEY extends Object> VALUE[] mapOneToMany(VALUE item, KEY key, Map<KEY,VALUE[]> map, int reserve, Class<VALUE> clazz ) {
        if (map!=null && item!=null && key!=null && (!(key instanceof String) || ((String)key).length()>0)) {
            final VALUE[]  dd=map.get(key);
            if (!(clazz==String.class ? idxOfStrg((String)item, (String[])dd)>=0 : cntains(item,dd) )) {
                final VALUE[] newDD=ad(item, dd, reserve, clazz);
                map.put(key, newDD);
                return newDD;
            }
            return dd;
        }
        return null;
    }
    public static <T extends Object> void rmNulls(Map<? extends Object,T[]> map, Class<T> clazz ) {
        for(Map.Entry<String,T[]> e : entryArry(map)) {
            final T[] tt=e.getValue();
            if (tt==null) continue;
            final T[] tt2=rmNullA(tt, clazz);
            if (tt!=tt2) e.setValue(tt2);
        }
    }
    public static void mapColumn1Column2(BA ba,int column1, int column2, char separator, Map<String,String> map, long mode) {
        if (ba==null) return;
        final int B=ba.begin(), ends[]=ba.eol();
        final byte[] T=ba.bytes();
        final boolean emptyStrg=(mode&MAP_ALLOW_EMPTY_STRING)!=0;
        for(int iL=0; iL<ends.length; iL++) {
            final int b= iL==0?B : ends[iL-1]+1, e=ends[iL];
            if (e-b<3 || T[b]=='#') continue;
            final int col1=nthColumn(column1,separator,T,b,e);
            if (col1<0) continue;
            final int col2=nthColumn(column2,separator,T,b,e);
            if (col2<0) continue;
            final int end2=strchr(STRSTR_E,separator,T,col2,e);
            final int end1=strchr(STRSTR_E,separator,T,col1,e);
            if (col1>=0 && col2>=0 && end1>col1 && (emptyStrg && end2==col2 ||  end2>col2) ) {
                String v= ba.getString(mode&(BA.STRING_TRIM|BA.STRING_POOL), col2,end2);
                String k=ba.newString(col1,end1);
                if ((mode&MAP_RM_TERMINAL_QUOTES)!=0) {
                    v=delSfx('"',delPfx('"',v));  k=delSfx('"',delPfx('"',k));
                    if ((mode&BA.STRING_TRIM)!=0) { v=v.trim();  k=k.trim();  }
                }
                if ((mode&MAP_VALUES_AS_INTERN)!=0) v=v.intern();
                map.put(k, v);
            }
        }
    }
    /* <<< HashMaps <<< */
    /* ---------------------------------------- */
    /* >>> toString >>> */
    public static String[] intrn(String ss[]) {
        if (ss!=null) for(int i=ss.length; --i>=0;) ss[i]=ss[i]!=null?ss[i].intern() : null;
        return ss;
    }
    public static String toStrgIntrn(Object o) { final String s=toStrg(o); return s!=null ? s.intern() : null;}
    public static String toStrgTrim(final Object o) {
        final String s=toStrg(o);
        return s!=null ? s.trim() : null;
    }
    public static String toStrg(Object s) {
        if (s==null) return null;
        if (s instanceof JList) {
            final Object oo[]=getSelValues(s);
            return oo==null ? "" : toStrg(oo.length==1 ? oo[0] : new BA(0).join(oo));
        }
        if (s instanceof File && !(s instanceof ChFile)) { /* --- In Javaws5 s instanceof File ? ((File)s).getAbsolutePath() : (java.util.PropertyPermission user.dir read) --- */
            try{
                return ((File)s).getCanonicalPath();
            } catch(Exception e) { return ((File)s).getAbsolutePath(); }
        }
        final String str=
            s instanceof byte[] ? bytes2strg((byte[])s,0,MAX_INT) :
            s instanceof String ? (String)s :
            s.toString();
        return "".equals(str) ? "" : str;
    }
    public static String toStrg(double f) { return String.valueOf(f); }
    public static String[] toStrgs(Object[] ss) {
        if (ss==null) return null;
        if (ss instanceof String[]) return (String[])ss;
        final String st[]=new String[ss.length];
        for(int i=0;i<ss.length;i++) st[i]=toStrg(ss[i]);
        return st;
    }
    public static String toStrg(Object o, int from, int to) {
        if (o instanceof BA) return ((BA)o).newString(from,to);
        final byte bb[]=o instanceof byte[] ? (byte[])o : null;
        final String s=bb!=null ? null : toStrg(o);
        if (bb==null && s==null) return "";
        final int L=bb!=null ? bb.length : s.length();
        if (from>=L || from>=to) return "";
        final int f=from>=0?from:0, t=to<L?to:L;
        return bb!=null ? bytes2strg(bb,f, t) : s.substring(f,t);
    }
    private static String[] _strgLen1;
    public static String[] strgsOfLen1() {
        if (_strgLen1==null) {
            _strgLen1=new String[256];
            for(int i=256; --i>=0;) _strgLen1[i]=String.valueOf((char)i);
        }
        return _strgLen1;
    }
    public static String toStrg(char c) { return c>255 ? String.valueOf(c) : strgsOfLen1()[c];  }
    public static String toStrg(long i) { return 0<=i && i<=9 ? toStrg((char)(i+'0')) : String.valueOf(i); }

    private final static String _timesC[][]=new String[128][];
    public static String toStrg(char c,int times) {
        if (times<1) return "";
        if (times==1) return toStrg(c);
        String ss[]=_timesC[c];
        if (sze(ss)<=times) ss=_timesC[c]=chSze(ss, times+9);
        if (ss[times]==null) ss[times]=new BA(0).a(c,times).toString();
        return ss[times];
    }
    /*
      http://forums.sun.com/thread.jspa?forumID=256&threadID=265010
      Hashtable HashMap
      Length 13: 463ns instead of  911
      Length 58: 465ns instead of  1211
    */
    public static int sPoolCountRecycled, sPoolCountNotRecycled;
    private static String[] sPoolIP, sPoolP;
    public static String toStrgIP(byte[] bb, int from, int to) {
        if (sPoolIP==null) sPoolIP=new String[256*256];
        return toStrg_(bb,from,to,sPoolIP);
    }
    public static String toStrgP(byte[] bb, int from, int to) {
        if (sPoolP==null) sPoolP=new String[256*256];
        return toStrg_(bb,from,to,sPoolP);
    }
    private static String toStrg_(byte[] bb, int from, int to, final String sPool[]) {
        if (from>=bb.length || from>=to) return "";
        final int f=from>=0?from:0, t=to<bb.length ? to : bb.length;
        final int hashCd=hashCd(bb,f,t);
        final String s=sPool[hashCd&0xffff];
        if (s!=null && s.length()==t-f && s.hashCode()==hashCd && strEquls(s,bb,f)) { sPoolCountRecycled++; return s; }
        sPoolCountNotRecycled++;
        return sPool[hashCd&0xffff]= sPool==sPoolIP ? bytes2strg(bb,f,t).intern() : bytes2strg(bb,f,t);
    }
    public static String toStrgN(Object o) {
        final String s=o==null?"": o instanceof String ? (String)o : toStrg(o);
        return s==null?"":s;
    }
    /* <<< toString <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private static Object refRGB;
    public static int[] sharedRGB(int n) {
        int[] ii=(int[])deref(refRGB);
        if (ii==null||ii.length<n) refRGB=newSoftRef(ii=new int[n]);
        return ii;
    }
    private static Stroke _strokeDivider, _stroke[][];
    public static void drawDivider(Component c,Graphics g,int x0,int y0,int dx0,int dy0) {
        if (_strokeDivider==null) _strokeDivider=new BasicStroke(1f,BasicStroke.CAP_BUTT,BasicStroke.CAP_BUTT,1f,new float[]{1f,2f},0);
        if (g==null) g=c.getGraphics();
        final Graphics2D g2=(Graphics2D)g;
        if (g==null) return;
        final int dx=dx0>0?dx0:c==null?0:c.getWidth()+dx0;
        final int dy=dy0>0?dy0:c==null?0:c.getHeight()+dy0;
        final Stroke stroke0=g2.getStroke();
        g2.setStroke(_strokeDivider);
        final long time=currentTimeMillis();
        for(int pass=2;--pass>=0 && currentTimeMillis()-time<99;) {
            g.setColor(C(pass==1?0xFF:0xFFffFF));
            if (dx>dy) {
                for(int y=y0; y<y0+dy; y+=3) g.drawLine(x0-(y/3)%3,   y+pass,  x0+dx,y+pass);
            } else {
                for(int x=x0; x<x0+dx; x+=3) g.drawLine(x,  y0-(x/3)%3+pass,x,y0+dy+pass);
            }
        }
        g2.setStroke(stroke0);
    }
    public static void mouseOverPrintMsg(Component c, String ss[], Component target, int x, int y,Font f) {
        final Object para[]={ss,intObjct(x),intObjct(y), wref(target),f};
        if (c==null || ss==null) pcp(instance(MOLI_OVER_PRINT),null,c);
        else {
            evLstnr(MOLI_OVER_PRINT).addTo("M",c);
            pcp(instance(MOLI_OVER_PRINT),para,c);
        }
    }

    public static Stroke dashedStroke(int type) {
        if (_stroke==null) _stroke=new Stroke[][]{new Stroke[12],new Stroke[3]};
        final Stroke[] S=_stroke[type];
        final int nStroke=S.length, iStroke=(int) ((currentTimeMillis()/100)%nStroke);
        Stroke s=S[iStroke];
        if (s==null) s=S[iStroke]=new BasicStroke(1f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND,1f,new float[]{nStroke/3f,nStroke-nStroke/3f},iStroke);
        return s;
    }

    public static Runnable thread_drawMsg(String txt, JComponent c) {
        return thrdM("drawMsg",ChUtils.class,  new Object[]{txt,c});
    }
    public static void drawMsg(String msg0, Component c) {
        if (c!=null) {
            final String msg;
            final int y;
            if (chrAt(0,msg0)=='@') {
                final int charH=charH(c)+1;
                y=(charH+1)*atoi(msg0,1);
                msg=substrg(msg0,nxt(-DIGT,msg0,1),MAX_INT);
            } else {
                msg=msg0;
                y=0;
            }
            if (sze(msg)==0) c.repaint(0,y,9999,EX);
            else drawMsg(DRAW_MSG_SET_COLOR, splitStrg(msg, '\n'),c,null,y);
        }
    }
    public final static int DRAW_MSG_SET_COLOR=1<<0;
    public static void drawMsg(long options, Object[] msgs, Component c, Graphics g0, int y0) {
        if (c==null) return;
        if (sze(msgs)==0) {
            repaintC(c);
            return;
        }
        final Graphics g=g0!=null ? g0:c.getGraphics();
        if (g!=null) {
            Color fg=null, bg=null;
            final int charH=charH(g)+1, charA=charA(g);
            int row=0;
            for(Object oMsg : msgs) {
                if (oMsg instanceof Color) {
                    bg=(Color)oMsg;
                    continue;
                } else  if (oMsg instanceof String) {
                    final String msg=(String)oMsg;
                    String txt=msg;
                    if (txt.indexOf('\u001B')>=0)  {
                        final BA ba=new BA(txt);
                        final long[] escape=ba.getAnsiEscapes();
                        final int escapeL=ba.countAnsiEscapes();
                        final AnsiEscape attr=new AnsiEscape();
                        for(int i=0;i<escapeL;i++) AnsiEscape.inferIntoAttribute(escape[i],attr);
                        txt=ba.toString();
                        fg=attr.fg();
                        bg=attr.bg();
                    }
                    if (fg==null) fg=Color.BLACK;
                    if (bg==null) bg=c.getBackground();
                    if (bg==null || bg.equals(fg)) bg=Color.WHITE;
                    final String key="CU$$W"+row;
                    final int w=strgWidth(g,txt)+EM, y=y0+row*charH;
                    if (0!=(DRAW_MSG_SET_COLOR&options)) {
                        g.setColor(bg);
                        g.fillRect(0,y, maxi(w,atoi(gcp(key,c))),charH);
                        g.setColor(fg);
                    }
                    g.drawString(txt, 0,charA+y);
                    pcp(key,intObjct(w),c);
                    row++;
                }
            }
        }
    }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Hooks >>> */
    private static Map<Object,Map> _mapForService=new HashMap();
    public static List listServices(boolean create, Object o, Class clazz) { return _listS(create,o,clazz); }
    public static List<ChRunnable> listServicesR(boolean create, Object o, String runID) { return _listS(create,o,runID); }
    private static List _listS(boolean create, Object o, Object clazz) {
        if (o==null || clazz==null) return null;
        Map<Object,List<Reference>> m=_mapForService.get(clazz);
        if (m==null) _mapForService.put(clazz,m=new WeakHashMap());
        List v=m.get(o);
        if (create && v==null) m.put(o, v=new ArrayList());
        rmNull(v);
        return v;
    }

    public static void rmService(Object service, Object client, Class clazz) {
        final List v=service==null?null:listServices(false, client,clazz);
        if (v!=null) v.remove(wref(service));
    }
    public static void addService(Object service, Object client, Class clazz) {
        if (service!=null && client!=null) adUniq(wref(service), listServices(true,client,clazz));
    }
    public static void addServiceR(ChRunnable service, Object client, String runId) {
        if (service!=null && client!=null) adUniq(wref(service), listServicesR(true,client,runId));
    }
    public final static List<JPopupMenu> vPopupCloseOnClick=new ArrayList();
    private static boolean _dragW;

    private static boolean dragWindow(AWTEvent ev) {
        final int id=ev.getID(), modi=modifrs(ev);
        if (id==MOUSE_DRAGGED && 0==(modi&(SHIFT_MASK|CTRL_MASK))) {
            final Component c=(Component)ev.getSource();
            final int x=x(ev), y=y(ev);
            final Window w=parentWndw(c);
            if (w!=null) {
                final Point np=new Point(x,y);
                if (!_dragW) pcp(KEY_DRAGW,SwingUtilities.convertPoint(c, np, w),c);
                else {
                    final Point point0=gcp(KEY_DRAGW,c,Point.class);
                    if (point0!=null) {
                        SwingUtilities.convertPointToScreen(np,c);
                        w.setBounds(x(np)-x(point0), y(np)-y(point0), w.getWidth(), w.getHeight());
                    }
                }
            }
            _dragW=true;
        } else if (id==MOUSE_MOVED || id==MOUSE_RELEASED)  _dragW=false;
        return _dragW;
    }
    private final static Map _setClrEvt=new WeakHashMap();
    public static void clrEvt(AWTEvent ev) { _setClrEvt.put(ev,""); }
    public static boolean shouldProcessEvt(AWTEvent ev) {
        final Object q=ev==null?null:ev.getSource(), drags=gcp(KOPT_DRAGS_PARWINDOW,q);
        if (q==null) return false;
        final int id=ev.getID();
        if (id==KEY_PRESSED && keyCode(ev)==VK_SHIFT) toolTipFakeEv();
        final Class clazz=q.getClass();
        final JComponent qjc=derefJC(q);
        if (drags!=Boolean.FALSE) {
            /* Warning:  Protein3d extends ChPanel */
            if (drags!=null || clazz==RemainingSpc.class ||
                (clazz==ChPanel.class || isChLabl(q)) && listServices(false, q, PaintHook.class)==null) {
                if (dragWindow(ev)) return false;
            }
        }

        if (gcp(KOPT_DISABL_EVNTS,q)!=null || gcp(KOPT_DISABLE_EV,q)!=null ||setAotEv(ev)) return false;
        if (id==MOUSE_PRESSED) {
            _totalClicks++;
            for(int i=sze(vPopupCloseOnClick); --i>=0;)  setVisblC(false, get(i,vPopupCloseOnClick), 0);
            vPopupCloseOnClick.clear();
        }
        if (id==KEY_TYPED && keyCode(ev)==VK_ASTERISK) {
            buttn(TOG_ANTIALIASING).s(!buttn(TOG_ANTIALIASING).s());
            ChFrame.repaintAll();
        }
        if (ev instanceof MouseEvent) {
            lstMouseEvt(ev);
            final JComponent cam=id==MOUSE_EXITED?null:qjc;
            if (cam!=_compAtMouse) {
                if (_compAtMouse instanceof ChButton) repaintC(_compAtMouse); /* Close buttons */
                if (cam instanceof ChButton) cam.repaint();
                _compAtMouse=cam;
            }
        }
        if (ChJScrollPane.openMenu(ev, qjc))  return false;
        return _setClrEvt.get(ev)==null;
    }
    public static Component addPaintHook(PaintHook hook, Component o) { addService(hook,o,PaintHook.class); return o; }
    public static Object addActLi(ActionListener li, Object o) { addService(li,o, ActionListener.class); return o; }
    public static void handleEvt(Object hasListeners, AWTEvent ev) {
        Object v=listServices(false,hasListeners,ActionListener.class);
        if (v==null && (hasListeners instanceof List || hasListeners instanceof Object[])) v=hasListeners;
        if (sze(v)>0) {
            final Object arg[]={v,ev};
            if (isEDT()) instance(0).run("EVENT",arg);
            else inEdtCR(instance(0),"EVENT",arg);
        }
    }
    public static void handleActEvt(Object hasListeners, String cmd, int modifiers) {
        if (hasListeners!=null && cmd!=null) {
            Object v=listServices(false,hasListeners,ActionListener.class);
            if (v==null) v=derefArray(oo(hasListeners), ActionListener.class);
            if (sze(v)>0) {
                final Object source=gcp(KEY_EVENT_SOURCE,hasListeners);
                handleEvt(hasListeners, new ActionEvent(orO(source, hasListeners),ActionEvent.ACTION_PERFORMED,cmd,modifiers));
            }
        }
    }
    public static boolean paintHooks(JComponent c, Graphics g, boolean after) {
        pcp(KOPT_ALREADY_PAINTED,"",c);
        final JScrollBar sb=gcp(KEY_NO_PAINT_IF_ADJUSTING,c, JScrollBar.class);
        if (c==null || sb!=null && sb.getValueIsAdjusting()) return false;
        final int W=c.getWidth(), H=c.getHeight();
        final List v=listServices(false, c,PaintHook.class);
        boolean ret=true;
        for(int i=0; i<sze(v); i++) {
            final PaintHook hook=get(i,v,PaintHook.class);
            if (hook!=null && !hook.paintHook(c, g,after)) ret=false;
        }
        if (ret && after) {
            if (c==_compAtMouse || !(c instanceof ChButton && !isChLabl(c))) ChRenderer.drawSmallButtons(c,g);
            if (tipDnd(c,null)>0) {
                final int w=24;
                drawDivider(c,g,W-w*5/4, 0, w*5/4,H);
                g.drawImage(iicon(IC_COPY).getImage(), W-w-6, -14,c);
            }
        } else {
            if (!c.isEnabled() && gcp(KOPT_NOT_PAINTED_IF_DISABLED,c)!=null) {
                final Container par=c.getParent();
                final Color bg=par==null?null:par.getBackground();
                g.setColor(bg!=null?bg:C(DEFAULT_BACKGROUND));
                g.fillRect(0,0,W,H);
                return false;
            }
        }
        if (true && null!=gcp(KOPT_ADAPT_SMALL_SIZE_NEEDED,c)) {
            pcp(KOPT_ADAPT_SMALL_SIZE_NEEDED,null,c);
            adaptSmallSize(c);
        }
        return ret;
    }
    /* <<< Hooks <<< */
    /* ---------------------------------------- */
    /* >>> TabItemTipIcon >>> */

    private final static Object BMSG[]={null};
    public static String ballonMsg(AWTEvent ev, String superTT) {
        final Object q=ev==null ? null : ev.getSource();
        if (q==null) return null;
        final BA sb=appndTooltips(ev, superTT, baClr(BMSG));
        sb.delBlanksL().delBlanksR();
        if (sb.length()==0 && (q instanceof JTable || q instanceof JTree || q instanceof JList)) {
            final Object cell=objectAt(null,ev);
            appndTooltips(cell,null,sb);
            if (sb.length()==0) sb.a(dTip(cell));
        }
        return sb.delBlanksL().delBlanksR().length()==0?null:addHtmlTagsAsStrg(sb);
    }
    public static BA appndTooltips(Object evOrComp, String superTT, BA sb) {
        final AWTEvent ev=deref(evOrComp, AWTEvent.class);
        final Object q=evtSrc(evOrComp);
        if (q==null) return null;

        sb.and(superTT,"<br>");
        final JComponent cloned=gcp(KEY_CLONED_FROM, q, JComponent.class);
        if (cloned!=null) sb.filter(FILTER_NO_HTML_BODY, cloned.getToolTipText()).a("<br>");
        final List v=listServices(false, q, TooltipProvider.class);
        for(int i=0; i<sze(v); i++) {
            final TooltipProvider t=get(i,v,  TooltipProvider.class);
            if (t!=null && q instanceof JComponent) sb.and(t.provideTip(evOrComp),"<br>");
        }
        sb.join2("", deref(gcpa(KEY_TOOLTIP, q), CharSequence.class),"<br>").and(dTip(ev), "<br>");
        tipDnd(q,sb);
        return sb;
    }
    public static Icon dIcon(Object o) { return TabItemTipIcon.icn(o); }
    public static ImageIcon dIIcon(Object o)  { return deref(dIcon(o), ImageIcon.class);}
    public static String dHomePage(Object o)  { return TabItemTipIcon.g(TabItemTipIcon.HOME,o, null); }
    public static String     dItem(Object o)  { return TabItemTipIcon.g(TabItemTipIcon.ITEM,o, null); }
    public static String      dTab(Object o)  { return TabItemTipIcon.g(TabItemTipIcon.TAB,o, null);  }
    public static String     dTitle(Object o) { return txtForTitle(dItem(o)); }
    public static String      dTip(Object objOrEv) {
        final AWTEvent ev=deref(objOrEv,AWTEvent.class);
        final Object q=evtSrc(objOrEv);
        String tt=TabItemTipIcon.g(TabItemTipIcon.TIP,q, ev);
        if (tt==null) tt=toStrg(gcp(KEY_TOOLTIP, q, CharSequence.class));
        if (tt==null) tt=toStrg(gcp(KEY_TOOLTIP, scrllpnChild(q), CharSequence.class));
        return tt;
    }
    public static CharSequence rendererTxt(Object v) {
        final Object o=isInstncOf(HasRenderer.class,v) ? ((HasRenderer)v).getRenderer(0L,null) : null;
        return o instanceof CharSequence ? (CharSequence)o : null;
    }
    public static Component setTip(Object tt, Component jc) {
        if (jc instanceof JComponent) {
            if (tt==null) { ((JComponent)jc).setToolTipText(null); rtt(jc); }
            else if (tt instanceof TooltipProvider) addService(tt, rtt(jc), TooltipProvider.class);
            else ((JComponent)jc).setToolTipText(addHtmlTagsAsStrg(toCharSeq(tt)));
        }
        return jc;
    }

    public static void setTipDelay(JComponent c, int delayMS) { addMoli(MOLI_TIP_DELAY,c);  pcp(instance(MOLI_TIP_DELAY),intObjct(delayMS),c); }
    private static void toolTipFakeEv() {
        final MouseEvent lstEv=_lastMEvtMove;
        if (lstEv!=null) ToolTipManager.sharedInstance().mouseMoved(new MouseEvent((Component)lstEv.getSource(),MOUSE_MOVED,currentTimeMillis(),SHIFT_MASK, x(lstEv)-1, y(lstEv),0, false));
    }

    /* <<< TabItemTipIcon <<< */
    /* ---------------------------------------- */
    /* >>> 3D >>> */
    public static int charsTo32bit(int c0,int c1,int c2,int c3) { return c0+(c1<<8)+(c2<<16)+(c3<<24);}
    public static int charsTo32bit(byte T[], int i) { return T[i+0]+(T[i+1]<<8)+(T[i+2]<<16)+(T[i+3]<<24);}
    public static String pdbID(String s) {
        return delLstCmpnt(delLstCmpnt(delLstCmpnt(delPfx("pdb",delPfx("PDB:",delSfx("__",s))),':'),'_'),'.');
    }
    public static char pdbChain(String s) {
        if (sze(s)<LENGTH_PDBIDS) return 0;
        final int start=(s.charAt(0)|32)!='p'||(s.charAt(1)|32)!='d'||(s.charAt(2)|32)!='b' ? 0 : s.charAt(3)==':' ?4 : 3;
        final char colon=chrAt(start+LENGTH_PDBIDS,s);
        final char chain= colon==':' || colon=='_' ? chrAt(start+(LENGTH_PDBIDS+1), s) : 0;
        return is(LETTR_DIGT,chain) ? chain : (char) 0;
    }
    private static byte[] _pdbModels;
    private static boolean isPdbModel(String id4) {
        if (_pdbModels==null) {
            final BA ba=readBytes(rscAsStream(0L,_CCP,"PDBmodels.rsc" ));
            _pdbModels= ba==null ?  NO_BYTE : toByts(ba);
        }
        return strstr(STRSTR_IC,id4,_pdbModels)>=0;
    }
    public static String urlForPDB(String id0) {
        final String id=pdbID(id0);
        for(String mask : custSettings(Customize.pdbSite)) {
            if (sze(mask)>0 && !mask.startsWith("\\\\")) {
                final String url=urlForPdbMask(id,mask);
                final File f=file(url);
                if (sze(f)>0) return url;
            }
        }
        final String mask= isPdbModel(id) ?
            "ftp://ftp.wwpdb.org/pub/pdb/data/structures/models/current/pdb/??/pdb????.ent.gz" :
            choseFastestPDBServer(custSettings(Customize.pdbSite));
        return urlForPdbMask(id,mask!=null ? mask : "http://www.rcsb.org/pdb/files/????.pdb.gz");
    }
    private static String urlForPdbMask(String pdbId, String mask) {
        final String id=lCase(pdbID(pdbId));
        return id!=null && mask!=null ? rplcToStrg("??",id.substring(1,3),rplcToStrg("????",id,mask)) : null;
    }
    public static File localPdbFile(String pdbID, char chain) {
        return pdbID!=null ? file(chain==0 ? urlForPDB(pdbID) : dirSettings()+"/pdbChains/"+pdbID.substring(1,3)+"/"+pdbID+"_"+chain+".pdb") : null;
    }
    public static File downloadPdbChain(String pdbId_chain,  char chain) {
        final String id=pdbID(pdbId_chain);
        File f=localPdbFile(id,chain);
        if (sze(f)==0 && id!=null) {
            downloadAllPdbChains(id, chain);
            f=localPdbFile(id,chain);
        }
        return f;
    }
    private final static Map<String,File[]> mapSingleChain=new HashMap();
    public static File[] downloadAllPdbChains(String id4, char needThisChain) {
        File f=file(urlForPDB(id4));
        if (sze(f)==0) {
            if(_fastestPDB==null) sayDownloading(null,"@0 Obtaining fastest PDB-repository");
            f=urlGet(url( urlForPDB(id4)),999);
        }
        if (sze(f)==0) f=urlGet(url("http://www.rcsb.org/pdb/files/"+id4+".pdb.gz"), 999);
        final File dir=file(dirSettings()+"/pdbChains/"+id4.substring(1,3));
        mkdrs(dir);
        File ff[];
        synchronized(SYNC_CPY) {
            ff=alreadyExtractedChains(dir,id4);
            if (sze(f)>0 && (ff.length==0 || needThisChain!=0 && sze(localPdbFile(id4,needThisChain))==0)) {
                for(File fChain : new PDB_separateChains().processFile(null, f,false)) {
                    final String n=fChain.getName();
                    final int us=n.indexOf('_');
                    if (us>0) renamFile(fChain, localPdbFile(id4, chrAt(us+1,n)));
                }
            }
            ff=mapSingleChain.get(id4);
        }
        if (ff==null)  mapSingleChain.put(id4,ff=alreadyExtractedChains(dir,id4));
        return ff;
    }
    private static File[] alreadyExtractedChains(File dir, String id4) {
        File[] v=null;
        int count=0;
        final String[] files=lstDir(dir);
        if (files.length!=0){
            sortArry(files);
            for(int i=0; i<files.length; i++) {
                if (files[i].startsWith(id4)) {
                    (v==null?v=new File[files.length-i] : v)[count++]=file(dir+"/"+files[i]);
                }
            }
        }
        return chSze(v,count,File.class);
    }
    private final static Map<String,int[]> _mapSRT=new HashMap();
    private static int serverResponseTime(String url, boolean testOnlyHost, boolean saveOnlyForHost) {
        if (!looks(LIKE_EXTURL,url)) return MAX_INT;
        synchronized(_mapSRT) {
            int[] time=_mapSRT.get(url);
            if (time==null) {
                final int slash=url.indexOf('/',8);
                final String base=slash>0 ? url.substring(0,slash+1) : url;
                time=_mapSRT.get(base);
                if (time==null) {
                    time=new int[]{-1};
                    _mapSRT.put(base,time);
                    startThrd(thrdCR(instance(0),"getServerResponseTime",new Object[]{testOnlyHost?base:url,time}));
                }
                if (!saveOnlyForHost) _mapSRT.put(url,time);
            }
            return time[0];
        }
    }
    private static String _fastestPDB;
    private static String choseFastestPDBServer(String[] urls) {
        synchronized(SYNC_FASTEST_P) {
            if (_fastestPDB==null) {
                String best=null;
            loop:
                while(true) {
                    for(String s : urls) {
                        final String u=urlForPdbMask("3rec",s);
                        if (u==null) continue;
                        final int time=serverResponseTime(u,false,false);
                        if (time!=MAX_INT && time>=0) {
                            best=s;
                            break loop;
                        }
                    }
                    sleep(222);
                    sayDownloading(null, "@0 Looking for closest PDB mirror");
                }
                _fastestPDB=best;
                sayDownloading(null, "@0 Closest PDB mirror "+best);
            }
        }
        return _fastestPDB;
    }
    public static float squareDistance(float x, float y, float z,  float[] xyz, int fromIdx, int toIdx) {
        final int to3=mini(toIdx*3,xyz.length);
        float distance=Float.MAX_VALUE;
        for(int i= 3*(0<fromIdx?fromIdx:0);  i<to3; i+=3) {
            final float dx=xyz[i]-x, dy=xyz[i+1]-y, dz=xyz[i+2]-z,  d=dx*dx + dy*dy + dz*dz;
            if (distance>d) distance=d;
        }
        return distance;
    }
    /* <<< 3D <<< */
    /* ---------------------------------------- */
    /* >>> Databases >>> */
    public static String guessDbFromId(String id) { return guessDbFromId(id,false);}
    public static String guessDbFromId(String id, boolean onlyNT) {
        final int L=id==null ? 0 : id.length();
        if (L==0) return "";
        final char c0=id.charAt(0);
        final boolean d1= cntainsOnly(DIGT, id,1, MAX_INT);
        final boolean ud= cntainsOnly(UPPR_DIGT, id);
        final boolean u0=is(UPPR,c0);
        return
            id.indexOf(':')>=0 ? "" :
            L==6 && c0=='C' && d1 ? "KEGGc:" :
            L==6 && c0=='R' && d1 ? "KEGGr:" :
            ud && u0 && L==6 ?  (onlyNT ?  "EMBL:" : "UNIPROT:") :
            ud && L==11  && c0=='I' && id.startsWith("IPI") ? "IPI:" :      // IPI00000021
            ud && L==13  && c0=='U' && id.startsWith("UPI") ? "UNIPARC:" :  // UPI00000017EA
            ud && L==15  && c0=='E' && id.startsWith("ENSP")&& cntainsOnly(DIGT,id,4,15) ? "ENSEMBL:" : //  ENSP00000329399
            "";
    }
    /* <<< Protein <<< */
    /* ---------------------------------------- */
    /* >>> marchingAnts >>> */
    private static Map<Component,Object[][]> mapMarchingAnts[]=new WeakHashMap[2];
    public final static int ANTS_SEARCH=0;
    private static Runnable _threadAnts;
    public static void setMarchingAnt(int id, Component jc, Shape r, Color fg,Color bg) {
        addMarchingAnt(id,jc,null, fg,bg);
        final Map<Component,Object[][]> m=mapMarchingAnts[id];
        if (m!=null && jc!=null) m.remove(jc);
        addMarchingAnt(id,jc,r  ,fg,bg);
    }
    public static void addMarchingAnt(int id, Object refJc, Shape r, Color fg,Color bg) {
        final JComponent jc=derefJC(refJc);
        if (jc==null) return;
        Map<Component,Object[][]> m=mapMarchingAnts[id];
        if (m==null) m=mapMarchingAnts[id]=new WeakHashMap();
        final Object[][] lastRR=m.get(jc);
        if (r==null) {
            if (lastRR!=null) {
                for(Object[] o : lastRR) {
                    if (o==null) continue;
                    final Rectangle b=((Shape)o[0]).getBounds();
                    jc.repaint(b.x-1, b.y-1, b.width+2, b.height+2);
                }
            }
            m.remove(jc);
        } else {
            if (_threadAnts==null) {
                _threadAnts=thrdCR(instance(0),"MARCHING_ANT");
                ChThread.callEvery(EDT, 333, _threadAnts, "MARCHING_ANT");
            }
            m.put(jc, ad(new Object[]{r,fg,bg}, lastRR, 10, Object[].class));
        }
    }
    private static void marchingAnts() {
        final Stroke dashedStroke0=dashedStroke(0);
        try {
            for(Map<Component,Object[][]> m : mapMarchingAnts) {
                if (m!=null) {
                    for(Map.Entry<Component,Object[][]> e: entryArry(m)) {
                        final Component jc=e.getKey();
                        if (!jc.isShowing()) continue;
                        final Object[][] vvv=e.getValue();
                        if (vvv==null) continue;
                        for(Object[] vv : vvv) {
                            if (vv==null) continue;
                            final Shape r=(Shape)vv[0];
                            final Color fg=(Color)vv[1], bg=(Color)vv[2];
                            final Graphics2D g=(Graphics2D)jc.getGraphics();
                            if (g==null) continue;
                            g.translate(BUGFIX_OPENJDK_G2,BUGFIX_OPENJDK_G2);
                            final Object scale=gcp(KEY_SCALE_FACTOR,jc);
                            final java.awt.geom.AffineTransform transform0=scale!=null ? g.getTransform() : null;
                            if (scale!=null) {final double s=atof(scale); g.scale(s,s);}
                            final Stroke stroke0=g.getStroke();
                            g.setColor(jc.getBackground());
                            g.translate(1,1); g.draw(r);
                            g.translate(-2,-2); g.draw(r);
                            g.translate(1,1);
                            for(boolean isDashed:FALSE_TRUE) {
                                g.setColor(isDashed ? fg : bg);
                                g.setStroke(isDashed ? dashedStroke0 : stroke0);
                                g.draw(r);
                            }
                            g.setStroke(stroke0);
                            if (transform0!=null) g.setTransform(transform0);
                            g.translate(-BUGFIX_OPENJDK_G2,-BUGFIX_OPENJDK_G2);
                        }
                    }
                }
            }
        } catch(Throwable ex) {stckTrc(ex);}
    }
    /* <<< mapMarchingAnts <<< */
    /* ---------------------------------------- */
    /* >>> Bugs workaround >>> */
    public static void fillBigRect(Graphics g, int x, int y, int w0, int h0) {
        if (g==null) return;
        final int w=mini(w0,0x2fFFffFF), h=mini(h0,0x2fFFffFF);
        if (x+w>0x7fff || y+h>0x7fff) {
            synchronized("CU$$SYNCfr") {
                g.translate(x,y);
                g.fillRect(0,0,w,h);
                g.translate(-x,-y);
            }
        } else g.fillRect(x,y,w,h);
    }
}
