package charite.christo;
import static charite.christo.ChUtils.*;
import java.util.regex.*;
import java.util.*;

/**
   putln(ChRegex.replace(ChRegex.pattern("(34)"), " $1$1 ", "12345678"));
   @author Christoph Gille
*/
public class ChRegex {
    private ChRegex(){}
    private final static Object[] MAP={null};
    private final static StringBuffer _sb=new StringBuffer(99);
    public static Pattern pattern(String s) throws PatternSyntaxException {
        if (s==null) return null;
        final java.util.Map<String,Pattern> map=fromSoftRef(0,MAP,Map.class);
        Pattern p=map.get(s);
        if (p==null) map.put(s,p=Pattern.compile(s));
        return p;
    }
    public static CharSequence replace(Pattern pattern,String replacement, String haystack ) {
        if (pattern==null || haystack==null || replacement==null) return haystack;
        try {
          final Matcher matcher=pattern.matcher(haystack);
           synchronized(_sb){
               _sb.setLength(0);
               boolean found=false;
               while (matcher.find()) {
                   try{
                       matcher.appendReplacement(_sb, replacement);
                       found=true;
                   } catch(Exception e){ stckTrc(e);}
               }
               if (!found) return haystack;
               matcher.appendTail(_sb);
               return _sb;
           }
        } catch(IllegalStateException ex) { stckTrc(ex); }
        return haystack;
    }
}
