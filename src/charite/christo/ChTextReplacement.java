package charite.christo;

public class ChTextReplacement {

    private final String _needle,_replacement;
    public ChTextReplacement(String needle, String replacement) {
        _needle=needle;
        _replacement=replacement;
    }
    public String getNeedle() { return _needle;}
    public String getReplacement() { return _replacement;}

}
