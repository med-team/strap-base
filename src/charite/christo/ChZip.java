package charite.christo;
import java.util.*;
import java.io.*;
import java.util.zip.*;
import java.net.URL;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   to extract jar packages
   @author Christoph Gille
*/
public class ChZip  {
    public final static long PACK_IF_NEWER=1<<1;
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Zip-Files >>> */
    private long _fileHash;
    private File[] _jarFiles;
    private int _jfMC;

    public int jarFiles_mc() { return _jfMC;}
    public File[] jarFiles() {
        long sumModi=0;
        final File dir=_parent;
        sumModi+=dir.lastModified();
        if (isWin()) {
            for(File f : lstDirF(dir)) {
                final String s=f.toString();
                if (s.endsWith(".jar"))  sumModi+=f.lastModified()+ ( (long)s.hashCode() << 32);
            }
        }
        if (_fileHash!=sumModi) {
            _fileHash=sumModi;
            final List v=new ArrayList();
            for(String fn : lstDir(dir)) if (fn.endsWith(".jar")) v.add(file(dir,fn));
            _jarFiles=toArry(v,File.class);
            _jfMC++;
            _jarFilesJF=null;
        }
        if (_jarFiles!=null) sortArry(_jarFiles,comparator(COMPARE_NAME));
        return _jarFiles!=null ? _jarFiles : NO_FILE;
    }

    private ZipFile[] _jarFilesJF;
    public ZipFile[] jarFilesJF() {
        final File ff[]=jarFiles();
        if (_jarFilesJF==null) {
            Collection v=null;
            for(File f : ff) {
                try{ v=adUniqNew(new ZipFile(f),v); } catch(Exception e){}
            }
            _jarFilesJF=toArry(v, ZipFile.class);
        }
        return _jarFilesJF;
    }
    /* <<< Jar-Files <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    public String[] joinFiles(String fn, Map<String, ZipFile> map) {
        final List<String> v=new ArrayList(99);
        for(ZipFile jf : jarFilesJF()) {
            final String lines[]=readLines(inputStream(fn,jf));
            if (lines==null) continue;
            for(String s : lines) {
                final String line=delLstCmpnt(s,'#').trim();
                v.add(line);
                if (map!=null) map.put(line,jf);
            }
        }
        return strgArry(v);
    }

    public URL resource(String s) {
        if (s==null) return null;
        final ZipFile jj[]=jarFilesJF();
        for(ZipFile zf:jj) {
            final ZipEntry e=zf.getEntry(s);
            if (e!=null) return  url("jar:file://"+zf.getName()+"!/"+s);
        }
        return null;
    }
    public InputStream resourceAsStream(String s) {
        if (s==null) return null;
        final ZipFile jj[]=jarFilesJF();
        for(ZipFile zf:jj) {
            final InputStream is=inputStream(s,zf);
            if (is!=null) return is;
        }
        return null;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> related to a directory containing Jars  >>> */
    private final File _parent;
    private ChZip(File parent){ _parent=parent;}
    private static Map<String,ChZip> mapInst=new HashMap();
    public static ChZip getInstance(File parent) {
        final String key=toStrg(parent);
        ChZip z=mapInst.get(key);
        if (z==null) {
            mapInst.put(key, z=new ChZip(parent));
        }
        return z;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> pack200 >>> */
    // pack200 -Hfalse tt.pack.gz S.jar; time unpack200 tt.pack.gz t.jar; ls -lh t.jar
    private static Set<String> setAlreadyTried=new HashSet();
    public static boolean supportForPack200() { return javaVsn()!=14;}
    public static File unpack200(File fPack, long options) {
        if (fPack==null) return null;
        if (isDir(fPack)) {
            BA sbError=null;
            for(File f: lstDirF(fPack)) {
                final String fn=f.getAbsolutePath();
                if (!fn.endsWith(".jar.pack.gz") && !fn.endsWith(".jar.pack.bz2")) continue;
                if (sze(new File(delSfx(".pack.gz",fn)))>0) continue;
                final String key=f.getAbsolutePath()+f.lastModified();
                if (setAlreadyTried.contains(key)) continue;
                setAlreadyTried.add(key);
                final File fJar=unpack200(f, options);
                if (fJar==null && !htmlMeansProxyProblem(f)) {
                    if (sbError==null) sbError=new BA(999);
                    sbError.a("unpack200 ").a(f).a(" failed\n").a(MSG_CORRUPT).a(f);
                }
            }
            if (sbError!=null) error(sbError);
            return null;
        }
        final String fnPack=fPack.getAbsolutePath();
        final File fJar=new File(delSfx(".pack.gz",fnPack));
        try {
            final java.util.jar.JarOutputStream jostream = new java.util.jar.JarOutputStream(fileOutStrm(fJar));
            java.util.jar.Pack200.newUnpacker().unpack(deflaterAccordingToExtension(new FileInputStream(fPack),fnPack), jostream);
            // Must explicitly close the output.
            jostream.close();
            return fJar;
        } catch (IOException ioe) {
            if (!htmlMeansProxyProblem(fPack) && !htmlMeansProxyProblem(fJar) && !htmlMeansProxyProblem(fPack)) {
                error(MSG_CORRUPT+fnPack);
                stckTrc(ioe);
            }
            return null;
        }
    }

    static boolean htmlMeansProxyProblem(File f) {
        if (f!=null && sze(f)<10*1000 && looks(LIKE_HTML,readBytes(inStream(toStrg(f), 0L),null))) {
            error("The web proxy might not be set correctly");
            if (endWith("pack.gz", f) || endWith(".jar", f)) {
                putln(RED_ERROR+" Wrong web proxy. Deleting ",f);
                delFile(f);
                delFileOnExit(f);
            }
            return true;
        }
        return false;
    }
    private final static String MSG_CORRUPT="The following file could not be unpacked.\nIt may be corrupt.\nYou should consider to delete it.\n";
    /* <<< pack200  <<< */
    /* ---------------------------------------- */
    /* >>> static Utils >>> */
    public final static int REPORT_ERROR=1<<0, SUGGEST_DEL=1<<1;
    static void errorMsgExtract(int opt, Exception e, String f, File dest) {

        putln(RED_CAUGHT_IN," extractAll f=",f );
        putln(e); 
        if (0!=(opt&REPORT_ERROR)) {
            error("Problem extracting\n "+f+"\n to "+dest+
                  (0==(opt&SUGGEST_DEL)?"" : "\nConsider to delete the file. A new file will then be downloaded.")
                  );
        }
    }
    public static boolean extractAll(int opt, File jf,File dest) {
        if (sze(jf)==0) return true;
        try {
            return extractAll(opt,new ZipFile(jf), dest);
        } catch(Exception e) {
            errorMsgExtract(opt, e, toStrg(jf), dest);
            return false;
        }
    }
    public static boolean extractAll(int opt, ZipFile jf,File dest) {
        if (!isDir(dest)) {
            error("Error in ChZip.extractAll: "+dest+" is not a directory");
            return false;
        }
        try {
            final List<ZipEntry> vE=new ArrayList(1000);
            final Enumeration entries=jf.entries();
            while(entries.hasMoreElements()) vE.add((ZipEntry)entries.nextElement());
            String lastDir=null;
            long lastTime=0;
            int count=0;
            final int nE=sze(vE);
            for(int iE=0; iE<nE; iE++) {
                final ZipEntry e=vE.get(iE);
                final File f=file(dest,e.getName());
                if (e.isDirectory()) mkdrs(f);
                else {
                    final File dir=f.getParentFile();
                    final String dirS=dir.toString();
                    if (lastDir==null || !lastDir.equals(dirS)) mkdrs(dir);
                    lastDir=dirS;
                    delFile(f);
                    cpy(jf.getInputStream(e),f);
                    final long time=System.currentTimeMillis();
                    if (time-lastTime>999) {
                        sayDownloading(null,ANSI_MAGENTA+"Extracted "+jf.getName()+" #"+count+"/"+nE);
                        lastTime=time;
                    }
                    count++;
                }
            }
            sayDownloading(null,ANSI_GREEN+"Extracted "+jf.getName()+" #"+count+" done");
        } catch(Exception e) { errorMsgExtract(opt, e, toStrg(jf), dest);  return false;}
        return true;
    }
    public static String[] extractAllFilesWithEnding(ZipFile zf, String sRoot, String fileExtension[], File dirDest) {
        final ArrayList<String> vEntries=new ArrayList();
        final Enumeration entries=zf.entries();
        while(entries.hasMoreElements()) {
            final ZipEntry e=(ZipEntry)entries.nextElement();
            final String sE=e.getName();
            if (sze(sRoot)==0 || sE.startsWith(sRoot)) {
                for(String ext:fileExtension) {
                    if(ext==null || sE.endsWith(ext)) {
                        adUniq(sE,vEntries);
                        final File f= file(dirDest+"/"+sE.substring(sE.lastIndexOf('/')+1));
                        mkdrs(f.getParentFile());
                        if (f.lastModified()<e.getTime() || sze(f)!=e.getSize()) {
                            InputStream is=null;
                            try {
                                cpy(is=zf.getInputStream(e),f);
                            }catch(Exception ex){ putln(RED_CAUGHT_IN+"ChZip", ".extractAllFilesWithEnding ", ex);}
                            closeStrm(is);
                        }
                    }
                }
            }
        }
        mkdrsErr(dirDest);
        return strgArry(vEntries);
    }

    public static boolean checkIntegrityOfZipFile(File f, List<String>listOfEntries) {
        final byte[] buffer=new byte[4096];
        try {
            final ZipFile zf=new ZipFile(f);
            final Enumeration entries=zf.entries();
            while(entries.hasMoreElements()) {
                final ZipEntry e=(ZipEntry)entries.nextElement();
                if (listOfEntries!=null) listOfEntries.add(e+" "+e.getSize());
                InputStream is=null;
                try {
                    is=new BufferedInputStream(zf.getInputStream(e));
                    while(is.read(buffer)!=-1) {}
                } catch(IOException ioex) { putln(RED_CAUGHT_IN+"ChZip",".checkIntegrity ", e); return false;}
                closeStrm(is);
            }
        } catch(Exception e){putln(RED_CAUGHT_IN+"ChZip",".checkIntegrity ",e); return false;}
        return true;
    }

    public static InputStream inputStream(String entry, ZipFile zf) {
        final ZipEntry je=zf==null || entry==null ? null : zf.getEntry(entry);
        try { if (je!=null) return zf.getInputStream(je); } catch(Exception ex){}
        return null;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    public static void makeZipFile(File zipFile, Map<File,File[]> data, FileFilter filter){
        ZipOutputStream z=null;
        try {
            z=new ZipOutputStream(fileOutStrm(zipFile));
        } catch(Exception ioe){ putln(RED_CAUGHT_IN+"ChZip",".makeJarFile ",ioe);}
        for(Map.Entry<File,File[]> e : entryArry(data)) {
            final File dir=e.getKey(), ff[]=e.getValue();
            mkParentDrs(zipFile);
            addToZipFile(z, dir,ff,filter);
        }
        closeStrm(z);
    }
    private static void addToZipFile(ZipOutputStream zipOut, File base, File[] files, FileFilter filter){
        try {
            for(File f:files) {
                if (isDir(f)) addToZipFile(zipOut, base,  lstDirF(f),filter);
                else if (sze(f)>0 && (filter==null || filter.accept(f))) {
                    final InputStream from=new FileInputStream(f);
                    final String name=relativFilePath(base, f.getAbsolutePath()).replace('\\','/');
                    final ZipEntry entry=new ZipEntry(chrAt(1,name)==':' ? name.substring(2) : name);
                    zipOut.putNextEntry(entry);
                    try {
                        final byte[] buffer=new byte[4096];
                        int bytes_read;
                        while((bytes_read=from.read(buffer))!=-1) {
                            Thread.yield();
                            zipOut.write(buffer,0,bytes_read);
                        }
                    } catch(Exception e){ stckTrc(e);}
                    finally { closeStrm(from);}
                }
            }
        } catch(IOException ioe){ stckTrc(ioe); }
    }

}
