package charite.christo;
import java.awt.*;
/**
Implemented by colored objects.
Usually the user can change the color of such objects using a tool button
<i>ICON:IC_COLOR</i>.
@see ButColor ButColor
@author Christoph Gille
*/
public interface Colored {
 void setColor(Color c);
 Color getColor();
}
