package charite.christo;
/**
   For the Tree and List renderers
   @author Christoph Gille
*/
public interface HasRenderer {
    Object getRenderer(long options, long rendOptions[]);
    int JLIST=1<<1;

    long STRIKE_THROUGH=1<<0;
}
