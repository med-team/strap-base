package charite.christo;
import java.io.*;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

public class ChLogger implements ChRunnable, HasPanel, ProcessEv {
    public final static int SAVE_DISPLAY_FILE=1<<1, NO_LINE_NUMBERS=1<<2;
    public final static String RUN_PREPROCESS="ChLog$$prepr";
    private final BA _ba=new BA(999);
    private ChTextView _ta;
    private final int _opt, _maxLen;
    private int _countF, _countL;
    private File _swapDir, _saveFile;
    private final Vector<File> _vFF=new Vector();
    private ChJList _jlFF;
    private ChRunnable _preprocessor;
    private JComponent _panel;
    public ChLogger(int options, int maxLen) {
        _opt=options;
        _maxLen=maxLen;
    }
    public ChLogger saveFile(File f) { _saveFile=f; return this; }
    public void setPreprocessor(ChRunnable p) { _preprocessor=p;}
    public JComponent getPanel(int mode) {
        if (_panel==null) {
            if (!isEDT()) {
                inEDT(thrdM("getPanel",this,new Object[]{intObjct(mode)}));
                return _panel;
            }
            _ta=new ChTextView(_ba);

            if (0==(_opt&NO_LINE_NUMBERS)) _ta.setLineNumberStart(0);
            final Object pBut=pnl(
                                  _saveFile==null?null:new ChButton("S").t("Save ...").li(evAdapt(this)),
                                  new ChButton("D").t("Insert separator").li(evAdapt(this)),
                                  new ChButton("C").t("Clear").li(evAdapt(this))
                                  );
            _panel=pnl(CNSEW,scrllpn(SCRLLPN_LOCK_BOTTOM,_ta, dim(99,222)), null,pBut);
            evAdapt(this).addTo("mM",_ta);
            evAdapt(this).addTo("mM",scrllpn(_ta));
        }
        return _panel;
    }
    /* <<< Pane <<< */
    /* ---------------------------------------- */
    /* >>> append >>> */
    private final Object para[]={null,null,null};
    public synchronized ChLogger a(Object line) {
        if (line instanceof Color || line==null || dtkt()==null) return this;
        if (line instanceof Object[]) return a(new BA(0).join(line, " "));
        if (line instanceof Throwable) return a(stckTrcAsStrg(((Throwable)line)));
        if (_preprocessor!=null) {
            para[0]=line;
            para[1]=_ba;
            para[2]=this;
            if (_preprocessor.run(RUN_PREPROCESS, para)==null) return this;
        }
        _ba.a(line);
        if (isVisbl(_ta)) _ta.repaint();
        if (_ba.end()>_maxLen) saveTmp();
        return this;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Save >>> */
    private synchronized void saveTmp() {
        final int E=_ba.end();
        final byte T[]=_ba.bytes();
        int e=E;
        for(int i=E, count=4; --i>=0;) {
            if (T[i]=='\n' && count--==0) {
                e=i;
                break;
            }
        }
        if (e>0) {
            final File f=swapFile(_countF++);
            wrte(f, T, 0, e);
            _countL+=countChr('\n', T,0,e);
            System.arraycopy(T,e,T,0,E-e);
            _ba.setEnd(E-e);
            if (_ta!=null) _ta.setLineNumberStart(_countL);
        }
    }
    final File swapFile(int i) {
        if (_swapDir==null) {
            mkdrs(_swapDir=newTmpFile(""));
            inEDT(thrdCR(this,"NORTH"));
        }
        final File f=file(_swapDir, i+".txt");
        _vFF.add(f);
        revalAndRepaintC(_jlFF);
        return f;
    }

    /* <<< Save <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    private ChFileChooser _fs;
    public void processEv( AWTEvent ev) {
        final String cmd=actionCommand(ev);
        final int kcode=keyCode(ev);
        final BA sb=gcp(BA.KEY_SEND_TO, _ta, BA.class);
        if (sb!=null) sb.send();
        if (kcode==java.awt.event.KeyEvent.VK_ENTER) a("\n");
        if (cmd!=null) {
            if (cmd=="C") {
                deleteTree(_swapDir);
                _swapDir=null;
                _countF=0;
                _ta.t("");
            }
            if (cmd=="D") a("#--------------------\n");
            if (cmd=="S") {
                if (_fs==null) {
                    evAdapt(this).addTo("a", _fs=new ChFileChooser(0, "ChLogger"));
                    _fs.setCurrentDirectory(mkParentDrs(_saveFile));
                    final String n=_saveFile.getName(), fileExt=n.substring(n.indexOf('.')+1);
                    _fs.setSelectedFile(_saveFile);
                    _fs.setDialogType(JFileChooser.SAVE_DIALOG);
                    if (sze(fileExt)>0) _fs.addFilter(fileExt);
                }
                ChFrame.frame("Choose a script output file",_fs,ChFrame.ALWAYS_ON_TOP|ChFrame.PACK|CLOSE_CtrlW).shw(ChFrame.CENTER);

            }
            if (cmd==JFileChooser.APPROVE_SELECTION) {
                final File f=_fs.getSelectedFile();
                if (isDir(f)) error("Error writing script: <pre> "+f+"</pre>is a directory.");
                else if (sze(f)==0 || ChMsg.yesNo("The file<pre> "+f+"</pre>already exists.<br>Overwrite?")) {
                    if (canModifyNiceMsg(f)) save(SAVE_DISPLAY_FILE, f);
                }
            }
        }
    }
    public void save(long options, File f) {
        try {
            final OutputStream os=fileOutStrm(f);
            for(int i=0; i<_countF;i++) {
                final File fi=swapFile(i);
                final InputStream is=new FileInputStream(fi);
                copyIsOs(is,os);
                closeStrm(is);
            }
            _ba.write(os);
            closeStrm(os);
            if (0!=(options&SAVE_DISPLAY_FILE)) {
                new ChFrame("Written file").ad(new ChJList(oo(f),ChJList.OPTIONS_FILES)).shw(ChFrame.ALWAYS_ON_TOP|ChFrame.AT_CLICK|ChFrame.PACK);
            }
        } catch(IOException iox) {
            error(iox, "Error writing "+f);
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_APPEND) a(arg);
        if (id==ChRunnable.RUN_SHOW_IN_FRAME) return ChFrame.frame( gcps(KEY_TITLE,this), getPanel(0), CLOSE_CtrlW_ESC).shw(atol(arg));

        if (id=="NORTH") {
            _jlFF=new ChJList(_vFF, ChJList.OPTIONS_FILES);
            final Component pnl=pnl(CNSEW, scrllpn(_jlFF),  pnlTogglOpts("Externalized: ", scrllpn(_jlFF)));
            getPanel(0).add(pnl, BorderLayout.NORTH);
            revalAndRepaintC(getPanel(0));
        }
        return null;
    }

}
