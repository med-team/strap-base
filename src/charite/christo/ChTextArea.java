package charite.christo;
import java.awt.*;
import java.util.*;
import javax.swing.*;
import java.util.List;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public final class ChTextArea extends JTextArea implements ChRunnable, Runnable {
    private boolean _isPainting, _isPainted;
    /* >>> Constructor >>> */
    public ChTextArea(CharSequence s) {
        super(sze(s)==0 ? "" : toStrg(s));
        setWrapStyleWord(true);
        monospc(this);
        setBG(0xFFffFF, this);
    }
    public ChTextArea(int rows,int cols) { this(""); setColumns(cols); setRows(rows);}
    @Override public String toString() { tools().loadSavedText();   return getText();    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> set >>> */
    public ChTextArea t(Object text) {
        final String s=toStrg(text);
        if (text!=null) {
            tools().setTextTS(s);
        }
        return this;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    @Override public boolean isOpaque() {
        return _isPainting ? false : super.isOpaque();
    }
    @Override public void paintComponent(Graphics g) {
        final ChTextComponents tools=tools();
        final int w=getWidth(), h=getHeight();
        final Color bg=getBackground();
        if (bg!=null && isOpaque()) {
            g.setColor(bg);
            fillBigRect(g,0,0,w,h);
        }
        if (tools.paintHook(this,g,false)) {
            try {
                _isPainting=true;
                super.paintComponent(g);
                _isPainting=false;
            } catch(Throwable e){}
        }
        if (!_isPainted) {
            _isPainted=true;
            run();
        }
        tools.paintHook(this,g,true);
    }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>>  Append >>> */
    private final List _v=new ArrayList();
    boolean _isRunning;
    public void run() {
        if (sze(_v)>0) {
            if (!isEDT()) inEDT(this);
            else synchronized(_v) {
                    for(Object o : _v.toArray()) appendNow(o);
                    _v.clear();
                    if (!_isRunning) {
                        _isRunning=true;
                        ChThread.callEvery(ChThread.WEAK_REFERENCE, 555,this,"ChTextArea");
                    }

                }
        }
    }

    public ChTextArea a(Object o) {
        if (o==null || !(o instanceof Color) && sze(o)==0) return this;
        if (o instanceof Object[]) {
            for(Object o2 : (Object[])o)  a(o2);
            return this;
        }
        if (isEDT()) appendNow(o);
        else {
            synchronized(_v) {
                _v.add(o instanceof CharSequence ? o.toString() : o);
            }
            if (!_isRunning) run();
        }
        return this;
    }
    private int _colorPos;
    private Color _color;
    private final static Object[] ANOW={null};
    private void aColor(Color c0) {
        assrtEDT();
        final Color bg=getBackground();
        Color c=c0==null?bg : c0,  lastC=_color;
        if (c==null) c=Color.WHITE;
        if (lastC==null) lastC=bg;
        if (lastC==null) lastC=Color.WHITE;
        if (!c.equals(lastC)) {
            final int len=getDocument().getLength();
            //putln("addColoredBg "+_colorPos+" "+len+"  "+lastC);
            if (!lastC.equals(bg)) ChJHighlighter.addColoredBg(_colorPos, len-1, lastC, this);
            _color=c;
            _colorPos=len;
        }
    }
    public void appendNow(Object o) {
        if (o==null) return;
        if (o instanceof Color) aColor((Color)o);
        else if ((o instanceof byte[] || o instanceof CharSequence) && sze(o)>0) {
            final String s=toStrg(o);
            final List v=vClr(ANOW);
            AnsiEscape.extractColors(s,v);
            final int N=sze(v);
            for(int i=0;i<N; i++) {
                final Object t=v.get(i);
                if (t instanceof String) append((String)t);
                else appendNow(t);
            }
            v.clear();
            if (isVisible())  tools().underlineRefs(ULREFS_DISABLE_UNTIL_MOUSE);
        }
        tools().setChanged();
    }
    /* <<< Append  <<< */
    /* ---------------------------------------- */
    /* >>> Threading  >>> */
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_APPEND && arg!=null) a(arg);
        if (id==ChRunnable.RUN_SHOW_IN_FRAME) {
            pcp(KEY_OPTS_SCROLLPANE, intObjct(SCRLLPN_CLR_BUT),this);
            return ChFrame.frame("",this,ChFrame.SCROLLPANE|CLOSE_CtrlW).shw(atol(arg));
        }
        return null;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    { this.enableEvents(ENABLE_EVT_MASK);}
    @Override public void processEvent(AWTEvent ev) {
        final AWTEvent newEv=tools().pEv(ev);
        if (newEv!=null) try { super.processEvent(newEv); } catch(Throwable e){}
    }
    private final ChTextComponents _tools=new ChTextComponents(this);
    public ChTextComponents tools() { return _tools; }

    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    public int getFixedRowHeight() { return getFontMetrics(getFont()).getHeight();}

    @Override public String getToolTipText(java.awt.event.MouseEvent mev) {
        final String tt=(String)tools().run(ChTextComponents.TT,mev);
       return tt!=null ? tt : ballonMsg(mev, super.getToolTipText(mev));
    }
    @Override public Dimension getPreferredSize() {
        final Dimension ps=prefSze(this);
        return ps!=null ? ps : super.getPreferredSize();
    }
    /* <<< Layout <<< */
    /* ---------------------------------------- */
    public Object getDndDateien() { return dndV(false,this); }

}
