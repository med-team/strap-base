package charite.christo;
import java.awt.event.*;
import java.net.URL;
import java.io.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

<br><b>Text Editor:</b> WIKI:Notepad does not work properly.  WIKI:Wordpad or WIKI:Notepad_plus_plus is used.


<br><br><b>The directory for program settings: </b>

Since WIKI:Dot_files cause problems the Strap directory is named "StrapAlign" instead of  "<b>.</b>StrapAlign".
<br>
To avoid white space in file paths on Windows XP,  the home directory is "C:\StrapAlign\"  instead of "%HOMEPATH\StrapAlign".

An alternative directory can be provided in the file %HOMEPATH%\StrapAlign\location_of_StrapAlign.ini.


<br><br><b>Installing Bioinformatics programs: </b> Strap uses the WIKI:Compiler of WIKI:Cygwin if no other specified.


@author Christoph Gille

But file:///C:/ is fine but failes file:/C:/...
Do not use quotes. rundll32.exe shell32.dll,Control_RunDLL "file:///C:/....html" does not work. Without quotes it works.

*/
public class Microsoft implements ActionListener,ChRunnable {
    public final static String CYGWIN_CORE="gcc-g++ gcc-g77 make python grep coreutils wget findutils ", CYGWIN_LATEX="tetex tetex-extra gv ";
    private Microsoft(){}
    private static Microsoft inst;
    private static Microsoft instance() {if (inst==null) inst=new Microsoft(); return inst;}
    public static String cmd() { return isSystProprty(IS_WINDOWS_95_FAMILY)  ? "command.com" : "cmd.exe";}

    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource(), roots=gcp("R",q);
        final String cmd=ev.getActionCommand(), packages=gcps("P", q);
        if (cmd=="CW") {
            final boolean log=isCtrl(ev) || setupFailed || cbCygwinUnattended!=null&&!cbCygwinUnattended.s();
            if (ChMsg.yesNo("Going to install/update the cygwin packages<pre>"+packages+"</pre>This will take some time.")) {
                startThrd(thrdCR(instance(),"INST_CW", new Object[]{packages, longObjct(log ? ChExec.SHOW_STREAMS : 0L), roots}));
            }
        }
        if (cmd=="CWD") new ChFrame("Install Cygwin").ad(panInstallCygwin(packages)).shw(ChFrame.AT_CLICK|ChFrame.PACK|ChFrame.ALWAYS_ON_TOP);
    }

    public static ChButton newCygwinButton(String packages) {
        return new ChButton("CWD").t("Install cygwin packages ...").i(IC_CYGWIN).li(instance()).cp("P",packages);
    }
    private static ChButton cbCygwinUnattended;
    private static JComponent panInstallCygwin(String packages0) {
        final String packages=sze(packages0)>0 ? packages0 : CYGWIN_CORE;
        if (cbCygwinUnattended==null) cbCygwinUnattended=toggl("Run Cygwin setup.exe unattended and automatically ").s(true);
        Object roots=null;
        {
            final File dirCygwin=dirCygwin();
            roots=dirCygwin;
            if (!dirCygwin.isDirectory()) {
                final Object ff[]=listExistingFileRoots();
                if (sze(ff)>0) {
                    final String rr[]=new String[ff.length];
                    for(int i=sze(ff); --i>=0;) rr[i]=delSfx("\\",ff[i])+"\\cygwin";
                    final ChCombo c=new ChCombo(rr);
                    roots=c;
                    for(String r:rr) {
                        if ((32|chrAt(0,toStrg(r)))=='c') c.s(r);
                    }
                }
            }
        }
        final String ICOP="In case of problems with Cygwin installation";
        final JComponent
            panProblem=pnl(VBHB,"#TB "+ICOP,
                            " Unselect the following check-box and try installation again. ",
                            cbCygwinUnattended.cb()
                            ),
            b=new ChButton("CW").t(ChButton.GO).li(instance()).doClose(0,REP_PARENT_WINDOW).cp("P",packages).cp("R",roots),
            pan=pnl(VBHB,
                     pnl(HBL,"WIKI:Cygwin"," is required for Bioinformatics programs embedded in Strap."),
                     " ",
                     "Installation of Cygwin requires 200 to 500 MBytes.",
                     pnl(HBL,"Root directory: ", roots),
                     " ",
                     pnl(HBL,"Install the Cygwin packages  ",packages, " ",b),
                     " ",

                    pnlTogglOpts("*"+ICOP, panProblem),
                    panProblem
                    );

        return pan;
    }

    private static boolean setupFailed;
    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id=="INST_CW") {
            final String packages=(sze(argv[0])>0 ? argv[0].toString() : CYGWIN_CORE).replaceAll("  *",",");
            final long options=atol(argv[1]);
            final File dirCygwin=file(argv[2]);
            final File fSetup=file(dirBin()+"/cygwinSetup/setup.exe");
            if ((System.currentTimeMillis()-fSetup.lastModified())/1000 > 60*60*24* 2) delFile(fSetup);
            InteractiveDownload.downloadFiles(new URL[]{url("http://www.cygwin.com/setup.exe")},new File[]{fSetup});
            final File fBash=file(dirCygwin()+"/bin/bash.exe");
            final java.net.InetSocketAddress proxy=proxyIsaForUrl(url("http://www.cygwin.org/"));
            new ChExec(options)
                .setCommandLineV(cmd(),"/c",fSetup,
                                 cbCygwinUnattended==null || cbCygwinUnattended.s()?"-q":null,
                                 "-R",dirCygwin,"-l",dirCygwin,
                                 proxy==null?null:"-p",
                                 proxy==null?null:proxy.getHostName()+":"+proxy.getPort(),
                                 "-d",
                                 "-P",packages
                                 ).run();
            if (sze(fBash)==0) { setupFailed=true; error("Can not find <br>"+fBash); }
            BA sb=null;
            for(File f : lstDirF(dirCygwin())) {
                if (f.toString().indexOf("%3a%2f%2f")>0) {
                    if (sb==null) sb=new BA("To save memory you may delete the following folders containing downloaded cygwin packages:\n\n");
                    sb.aln(f);
                }
            }
            shwTxtInW(ChTextComponents.FRAME_OPTS|ChFrame.ALWAYS_ON_TOP,"You can delete files",sb);
        }
        return null;
    }

}
