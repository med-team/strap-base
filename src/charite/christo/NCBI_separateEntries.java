package charite.christo;
import java.io.*;
import java.util.Collection;
import static charite.christo.ChUtils.*;
/**HELP
This utility allows to split a multiple sequences file where the sections are separated
by a line containing only two slashes.

<i>SEE_DIALOG:charite/christo/strap/DialogFetchSRS</i>
   @author Christoph Gille
*/
public class NCBI_separateEntries extends AbstractInteractiveFileProcessor {
    {
    setTextNorth(
            "The following list shows all multipart NCBI files.<br>"+
            "The entries are separated by lines starting with two slash \"//\".<br>"+
            "Select the files  to be split into single files.");
    setErrorNonSelected("non of these files are multiple NCBI files");
    }
    @Override public String fileEntry(BA byteArray,File f) {
        final File ff[]=processFile(byteArray,null,true);
        return ff.length>0 ? f+"  entries: "+ff.length : null;
     }
    @Override public File[] processFile(BA byteArray,File f, boolean overwrite) {
        Collection v=null;
        final BA ba=byteArray!=null ? byteArray : readBytes(f);
        if (ba!=null) {
            final int ends[]=ba.eol();
            final byte[] T=ba.bytes();
            int count=0;
            int lastStart=0;
            for(int iL=0; iL<ends.length; iL++) {
                final int b= iL==0 ? ba.begin() : ends[iL-1]+1, l=ends[iL]-b;
                if (l>1 && T[b]=='/' && T[b+1]=='/') {
                    try {
                        final File fo=ChUtils.file(f+"."+count);
                        final OutputStream fos=fileOutStrm(fo);
                        fos.write(T,lastStart,b-lastStart);
                        fos.flush();
                        fos.close();
                        v=adNotNullNew(fo, v);
                    } catch(Exception e){ putln("Caught NCBI_separateEntries#separate ",e);}
                    lastStart=b+l;
                    count++;
                }
            }
        }
        return toArryClr(v,File.class);
    }
}
