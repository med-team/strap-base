package charite.christo;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.MouseEvent.*;
public class ScrollLabel extends JPanel  {
    { setOpaque(false);}
    public ScrollLabel(CharSequence s){
        t(s);
    }
    @Override public Cursor getCursor() { return cursr(realWidth()<strgWidth(this,getText()) ? 'M':'D'); }
    private String _txt;
    public String getText() { return _txt;}
    public final ScrollLabel t(CharSequence t) { _txt=toStrg(t); ChDelay.repaint(this,333);return this;}
    private int getPreferredHeight() {
        final Insets i=getInsets();
        return  charA(this)+charH(this)+y(i)+i.bottom;
    }
    @Override public Dimension getMinimumSize() {
        return new Dimension(9,getPreferredHeight());
    }

    @Override public Dimension getPreferredSize() {
        final Dimension d=gcp(KEY_PREF_SIZE,this,Dimension.class);
        return d!=null ? d : new Dimension( strgWidth(this,getText()),getPreferredHeight());
    }

    @Override public void paintComponent(Graphics g) {
        ChUtils.antiAliasing(g);
        final int width=realWidth();

        final String t=getText();
        if (t!=null) {
            _scrollX=mini(0, maxi(_scrollX, width-strgWidth(this,getText())));
            final Color bg=getBackground(), fg=getForeground();
            if (isOpaque()) { g.setColor(bg); g.fillRect(0,0,22222,999); }
            g.setColor(fg);
            final int charA=charA(g);
            final Insets i=getInsets();
            g.drawString(t,_scrollX+x(i),charA+y(i));
            /* --- dot dot dot --- */
            final int wDots=strgWidth(this," ... "), wText=strgWidth(this,t),xDots=_scrollX+wText>width ? width-i.right-wDots : _scrollX<0 ? x(i) : -1;
            if (xDots!=-1) {
                if (isOpaque()) { g.setColor(bg);g.fillRect(xDots,0,wDots,999); }
                g.setColor(fg);g.drawString(" ... ",xDots,charA+y(i));
            }
        }
        //else super.paintComponent(g);
        //ChRenderer.paint(this, g); //4350
    }
    private int _lastX, _scrollX;
    private long _lastWhen;
    private int realWidth() {
        int width=22222;
        Container parent=this;
        do {
            width=mini(width,parent.getWidth());
            parent=parent.getParent();
        } while(parent!=null);
        return width;
    }

    { this.enableEvents(MOUSE_EVENT_MASK | MOUSE_WHEEL_EVENT_MASK | MOUSE_MOTION_EVENT_MASK);}
    @Override public void processEvent(AWTEvent ev) {

        final int id=ev.getID();
        if (ev instanceof MouseEvent) {
            final MouseEvent mev=(MouseEvent)ev;
            final long when=mev.getWhen();
            final int x=mev.getX(),prefWidth= strgWidth(this,getText()),width=realWidth();
            if (id==MOUSE_MOVED || id==MOUSE_DRAGGED) {
                if (x==0 || x==width) return;
                if (when-_lastWhen<222) {
                    final double k=x-_lastX<0 ? -_scrollX/(double)x : (prefWidth-width+_scrollX)/(width-(double)x);
                    _scrollX+=(x-_lastX)*(k>1 ? k:1);
                    //putln(k+" "+_scrollX);
                }
                _lastX=x;
                _lastWhen=when;
                repaint();
            }
        }
        final int rotation=wheelRotation(ev);
        if (rotation!=0) {
            _scrollX+=rotation*realWidth()/3;
            repaint();
        }
    }
}
