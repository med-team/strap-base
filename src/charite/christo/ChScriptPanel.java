package charite.christo;
import javax.swing.JComponent;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

public class ChScriptPanel implements java.awt.event.ActionListener, javax.swing.event.CaretListener {
    public final static String RUN_SCRIPT="CSC$$RS";
    private ChTextArea _ta;
    private final String _id;
    private final Object _wordCompl;
    private ChFrame _frame;
    private static BA _list;
    private final ChRunnable _run;
    private final Object _helpBut;
    private ChButton _butSelection;
    public ChScriptPanel(String id, Object wordCompl, Object helpBut, ChRunnable run) {
        _id=id;
        _wordCompl=wordCompl;
        _helpBut=helpBut;
        _run=run;
    }
    public ChTextArea textPane() {
        if (_ta==null) {
            (_ta=new ChTextArea("")).addCaretListener(this);
            (_butSelection=new ChButton("Run selected lines").li(this)).setEnabled(false);
            final Object b1=new ChButton("L").li(this).t("Run script line at cursor");
            _ta.tools()
                .cp(KEY_SOUTH_PANEL,pnl(HB,b1, " ", _butSelection,"#", _helpBut))
                .saveInFile("scriptpanel_"+_id)
                .enableWordCompletion(_wordCompl)
                .enableUndo(true)
                .underlineRefs(ULREFS_NOT_CLICKABLE);
            _ta.tools().underlineRefs(ULREFS_NO_ICON);
        }
        return _ta;
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();

       
        final boolean line=cmd=="L";
        if (line || q==_butSelection) {
            final BA ba=toBA(_ta);
            final int from, to;
            if (line) {
                final int c=_ta.getCaretPosition();
                if (c>=0) {
                    final int b=strchr(STRSTR_PREV,'\n',ba,c-1,-1);
                    from=b>0?b+1:0;
                    to=strchr(STRSTR_E,'\n',ba,c,MAX_INT);
                } else from=to=-1;
            } else { from=_ta.getSelectionStart(); to=_ta.getSelectionEnd(); }
            if (from>=0 && from<to) {
                final BA txt=new BA(ba, from, to);
                //if (myComputer()) putln(ChScriptPanel.class,"\n",txt);
              _run.run(RUN_SCRIPT,new Object[]{_id, txt, this});
            }

        }
    }
    public void caretUpdate(javax.swing.event.CaretEvent ev) {
        final int from=_ta.getSelectionStart(), to=_ta.getSelectionEnd();
        _butSelection.setEnabled(from>=0 && from<to);
    }
    public ChFrame showFrame(String title) {
        if (_frame==null) {
            final JComponent
                tp=textPane(),
                p=pnl(CNSEW,scrllpn(tp),null,gcp(KEY_SOUTH_PANEL,tp));
            _frame=new ChFrame("").ad(p).size(EM*80,EX*30);
        }
        _frame.shw(ChFrame.CENTER|CLOSE_CtrlW).setTitle(title);
        return _frame;
    }

}
