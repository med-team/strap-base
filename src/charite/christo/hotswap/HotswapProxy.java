package charite.christo.hotswap;
public interface HotswapProxy {
    /**
       Returns the parent <code>ProxyClass</code>.
    **/
    ProxyClass hotswap_getProxyClass();
    /**
       Explicitly enqueues this <code>Proxy</code> as a participant in
       the next hotswap transaction in the parent
       <code>ProxyClass</code>.  This will be done automatically after
       each commit or rollback if the <code>isAutoEnqueue</code>
       property is set to <code>true</code>.  The
       <code>proxy.hotswap()</code> calls this method as well, so it
       is not necessary to use unless you want to queue up several
       <code>Proxy</code> instances to be hotswapped in the same
       transaction.
    **/
     void hotswap_enqueue();
    /**
       Explicitly dequeues this <code>Proxy</code> as a participant in
       the next hotswap transaction in the parent
       <code>ProxyClass</code>.
    **/
    void hotswap_dequeue();
    /**
       Returns <code>true</code> if this <code>Proxy</code> instance
       is registered as a participant in the next hotswap transaction.
    **/
    boolean hotswap_isEnqueued();
    /**
       Explicitly releases any resources associated with this
       <code>Proxy</code>.  It usually not necessary to worry about
       having to explicitly release a <code>Proxy</code>; it is
       required only if one of the following two conditions are true:
       *
       <li>The <code>Object</code> <code><b>po</b></code> returned by
       the <code>hotswap_getInstance()</code> method is known to be an
       <code>instanceof</code> <code>ProxyObject</code> AND it is
       known that the implementation of
       <code><b>po</b>.hotswap_onRelease()</code> requires explicit
       release (even when not within the context of a hotswap
       transaction).  This implementation behavior is not recommended.
       Your object should have a different method that is exposed on
       the object itself to do any non-hotswap-transaction related
       cleanup.
       *
       <li><code><b>po</b>.isAutoEnqueue()</code> returns
       <code>true</code>.  If a <code>Proxy</code> instance is
       <I>autoenqueueing</I>, it is always registered with the parent
       <code>ProxyClass</code> as a hotswap transaction participant.
       Of course, if the parent <code>ProxyClass</code> always
       contains a reference to this object, it will never be garbage
       collected.  Calling this method ensures that the parent
       <code>ProxyClass</code> does not hold any references to this
       <code>Proxy</code> and hence will presumably be
       garbage-collectable.
    **/
    void hotswap_release();
}
