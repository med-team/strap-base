package charite.christo.hotswap;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;

class ChInvocationHandler implements HotswapProxy, java.lang.reflect.InvocationHandler {
    private final ProxyClass _cls;
    private Object _obj, _tmp;

    // ================================
    // Constructors
    // ================================
    private static String _error;
    public Object invoke(Object src, java.lang.reflect.Method method, Object[] args) throws Throwable {
        if (_obj==null || !(_obj.getClass().equals(_cls.getClassInstance()))) _obj=hotswap_newInstance();
        hotswap_enqueue();
        _cls.mayHotswap();
        try {
            final Object result=method.getName().startsWith("hotswap")
                ? method.invoke(this,args)
                : method.invoke(hotswap_getInstance(), args);
            _error=null;
           return result;
        }catch(Throwable e) {
          putln("caught in ChInvocationHandler: ",e);
            Throwable cause=e;
            while( (cause=cause.getCause())!=null) {
                final String msg=cause.getMessage();
                if (msg==null || msg.equals(_error)) {
                    putln("ChInvocationHandler cause=",cause);
                    _error=msg;
                    cause.printStackTrace(System.out);
                }
            }
            return null;
        }
    }
    ChInvocationHandler(ProxyClass cls){
        _cls=cls;
    }
    @Override public String toString(){
        return "(Proxy of class "+_cls.getClassInstance().getName()+": "+_obj+")";
    }
    @Override public boolean equals(Object other){
        if (other==this) return true;
        if (!(other instanceof ChInvocationHandler))  return false;
        final ChInvocationHandler that=(ChInvocationHandler)other;
        return (this._cls.getClassInstance().equals(that._cls.getClassInstance()));
    }

    public ProxyClass hotswap_getProxyClass(){
        return _cls;
    }

    public synchronized Object hotswap_getInstance() {
        // If the object is null, this is the first time the object
        // has been constructed or the arguments have been updated.
        if (_obj==null)  _obj=hotswap_newInstance();
        return _obj;
    }
    public synchronized void hotswap_enqueue(){
        if (!_cls.isEnqueued(this)) _cls.enqueue(this);
    }
    public synchronized void hotswap_dequeue(){
        if (_cls.isEnqueued(this))  _cls.dequeue(this);
    }
    public synchronized boolean hotswap_isEnqueued(){
        return _cls.isEnqueued(this);
    }
    public synchronized void hotswap_release(){
        // Release from the proxy class
        _cls.dequeue(this);
        _tmp=null;
    }
    synchronized boolean hotswap_prepare(Class newClass){
        try {
            _tmp=newClass.getDeclaredConstructor().newInstance();
        } catch (Throwable ex) {
            ex.printStackTrace(System.out);
            return false;
        }
        return true;
    }
    synchronized void hotswap_commit(){
        // Swap the old instance with the new.
        final Object old=_obj;
        _obj=_tmp;
        _tmp=old;
    }
    synchronized void hotswap_rollback(){
        _tmp=null;
    }
    synchronized Object hotswap_newInstance(){
        try {
            return _cls.getClassInstance().getDeclaredConstructor(NO_CLASS).newInstance();
        } catch (Exception ex) {
            putln("caught in ChInvocationHandler ",_cls);
            putln(ex);
            return null;
        }
    }
}
