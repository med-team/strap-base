package charite.christo.hotswap;
import java.util.*;
import java.io.File;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   These hotswap classes are adapted from the inxar.hotswap library by Paul Cody Johnston.
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
*/
public class ProxyClass implements HasRenderer{
    private final String _className;
    private final File _classfile, _sourcefile;
    private Class _cls, _interfaces[];
    private boolean _needsHotswap, _isHotswapping;
    private final Stack<ChInvocationHandler> _proxies;
    private final static UniqueList<ProxyClass> _vProxyClass=new UniqueList(ProxyClass.class);
    private final int id=_count++;
    private static int _count;
    private final static Object SB[]={null};

    private ProxyClass(File f,String className){
        _className=className;
        final String noExt=ChUtils.delDotSfx(f.getAbsolutePath());
        _classfile=new File(noExt+".class");
        _sourcefile=new File(noExt+".java");
        _proxies=new Stack();
    }
    public Class[] getProxyInterfaces() {
        if (_interfaces==null) {
            final List set=new ArrayList();
            Class cls=getClassInstance();
            adUniq(HotswapProxy.class, set);
            while (cls!=null) {
                adAllUniq(cls.getInterfaces(), set);
                cls=cls.getSuperclass();
            }
            _interfaces= toArry(set,Class.class);
        }
        return _interfaces;
    }
    public HotswapProxy newInstance(){
       return newProxy(new ChInvocationHandler(this));

    }
    private HotswapProxy newProxy(ChInvocationHandler proxy){
        try {
            final Class ii[]=getProxyInterfaces();
            final ClassLoader cl=Insecure.CLASSLOADING_ALLOWED ? classLoader() : CLASSLOADER;
            return (HotswapProxy) java.lang.reflect.Proxy.newProxyInstance(cl,ii, (java.lang.reflect.InvocationHandler)proxy );
        } catch (Exception ex) { putln(RED_CAUGHT_IN,ex); return null; }
    }

    public File getSourcefile(){ return _sourcefile; }
    public File getClassfile(){ return _classfile;  }
    public String getName(){ return _className; }
    public Class getClassInstance() {
        if (_cls==null) _cls=newClassInstance();
        return _cls;
    }
    public void hotswap(){
        _interfaces=null;
        _cls=null;
        _needsHotswap=true;
        mayHotswap();
    }
    public boolean mayHotswap(){
        synchronized(this) {
            if (sze(_proxies)==0) return false;
            // Disallow recursive hotswaps.
            if (_isHotswapping) return false;
            _isHotswapping=true;
        }
        boolean result=false;
        try {
            result=hotswapInternal();
        } finally { _isHotswapping=false; }
        return result;
    }
    private synchronized boolean hotswapInternal(){
        if (!_needsHotswap) return false;
        _needsHotswap=false;
        Class newClass=null;
        try {
            newClass=newClassInstance();
            //whenModified=_classfile.lastModified();
        } catch (Exception ex) {
            putln(RED_CAUGHT_IN+"ProxyClass#hotswapInternal() ",_className," ",ex);
            /* Remove all _proxies, the hotswap died. */
            _proxies.clear();
            return false;
        }
        final UniqueList<ChInvocationHandler> prepared=new UniqueList(ChInvocationHandler.class);
        boolean commit=true;
        try {
            // First prepare all proxies.
            while (!_proxies.isEmpty()) {
                final ChInvocationHandler p=_proxies.pop();
                if (p.hotswap_prepare(newClass)) prepared.add(p);
                else commit=false;
            }
        } catch (Throwable ex) {
            putln(RED_CAUGHT_IN+"ProxyClass#hotswapInternal() ");
            stckTrc(ex);
            commit=false;
        }
        if (commit) {
            // Do the commit
            for(ChInvocationHandler p : prepared.asArray()) p.hotswap_commit();
            // The transaction has succeeded. Drop the old class and replace it with the new.
            _cls=newClass;
        } else {
            for(ChInvocationHandler p : prepared.asArray()) p.hotswap_rollback();
        }
        return commit;
    }
    synchronized void enqueue(ChInvocationHandler proxy){
        if (!_proxies.contains(proxy)) _proxies.add(proxy);
    }
    synchronized void dequeue(ChInvocationHandler proxy){
        _proxies.remove(proxy);
    }
     synchronized boolean isEnqueued(ChInvocationHandler proxy){
        return _proxies.contains(proxy);
    }
    static ChClassLoader1 classLoader() {
        final ChZip chZip=ChZip.getInstance(dirHotswap());
        return ChClassLoader1.getInstance(true, chZip.jarFiles(),dirHotswap());
    }
     synchronized Class newClassInstance(){
        try {
            _interfaces=null;
            return classLoader().loadClass(_className);
        } catch(Exception e){ putln(RED_CAUGHT_IN+"ProxyClass#newInstance ",_className+" ",e); stckTrc(e); return null;}
    }
    public static synchronized ProxyClass getProxyClass(String className){
        if (className==null) return null;
        for(ProxyClass pc : _vProxyClass.asArray()) {
            if (className.equals(pc.getName())) return pc;
        }
        return null;
    }
    public static synchronized ProxyClass getProxyClass(File f, String className){
        ProxyClass pc=getProxyClass(className);
        if (pc==null) {
            pc=new ProxyClass(f, className);
            _vProxyClass.add(pc);
        }
        return pc;
    }
    public static synchronized void remove(ProxyClass cls){
        _vProxyClass.remove(cls);
    }
    @Override public boolean equals(Object o) {
        return o instanceof ProxyClass && ((ProxyClass)o).getSourcefile()==getSourcefile();
    }

    public static ProxyClass[] getProxyClasses() { return _vProxyClass.asArray();}
    @Override public String toString() { return toStrg(deref(getRenderer(0, null),CharSequence.class));}

    public Object getRenderer(long options, long rendOptions[]) {

        final File fS=getSourcefile(), fC=getClassfile();
        return baSoftClr(SB).a(getName()).a(" #").a(id)
            .a(
               sze(fS)==0 ? " (no sourcefile)" :
               sze(fC)==0 ? " (no classfile)" :
               fC.lastModified()<fS.lastModified() ? " (no classfile)" : " (ok)" )
            .toString();
    }
}
