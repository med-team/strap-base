package charite.christo;
import static charite.christo.ChUtils.*;
import java.awt.*;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.text.html.*;
import javax.swing.text.ViewFactory;
import javax.swing.text.Element;
import javax.swing.text.View;
import static charite.christo.ChConstants.*;
public class HtmlDocEditorKit extends HTMLEditorKit implements ViewFactory {
    private final String[] _packages;
    public HtmlDocEditorKit(String[] packages) {
        _packages=packages;
    }
    private ViewFactory _origFactory;
    public View create(Element elem) {
        if (elem.isLeaf()) {
            try {
                final int start=elem.getStartOffset(),end=elem.getEndOffset();
                final String txt=elem.getDocument().getText(start,end-start).trim();
                for(String id:HtmlDoc.TAGS_FOR_EDITOR_KIT) {
                    if (txt.startsWith(id)) {
                        final String text=txt.substring(id.length());
                        final HtmlDocElement el=new HtmlDocElement(id,text,new Range(start,end),_packages,"unknown");
                        HtmlDoc.jComponent(id,el);
                        final JComponent jComponent=el.getJComponent();
                        if (jComponent==null) return _origFactory.create(elem);
                        final View view=new MyView(elem,jComponent);
                        addRef(KEY_CLONES, wref(view), jComponent);
                        return view;
                    }
                }
            } catch(Exception e){}
        }
        return _origFactory.create(elem);
    }
    private static class MyView extends ObjectView {
        private final Object _c;
        private Dimension _d;
        @Override public float getPreferredSpan(int axis) { return _d==null ? 32 : axis==X_AXIS ? _d.width:_d.height;}
        @Override public float getAlignment(int axis) { return axis==X_AXIS ? 0f: .8f; }
        @Override public float getMaximumSpan(int axis) { return getPreferredSpan(axis);}
        public MyView(Element el,Object component) {
            super(el);
            _c=component;
        }

        @Override public void paint(Graphics g, Shape a) {
            if (a!=null && _c instanceof ChButton && isEnabld(_c)) {
                g.setColor(C(0xff));
                g.drawRect(x(a)+1,y(a)+1, wdth(a)-2, hght(a)-2);
            }
            super.paint(g,a);
        }

        @Override protected Component createComponent() {
            final Component c=_c instanceof Component ? (Component)_c: new JLabel(orS(toStrg(_c),"null"));
            _d=HtmlDoc.prefSize(c);
            if (c instanceof ChButton) {
                final ChButton b=(ChButton)c;
                b.setContentAreaFilled(true);
                b.setBorderPainted(true);
                b.setOpaque(true);

            }
            return c;
        }
    }
    @Override public ViewFactory getViewFactory() {
        if (_origFactory==null) _origFactory=super.getViewFactory();
        return this;
    }
}
