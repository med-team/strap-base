# Strap
ALIGN{ http://www.bioinformatics.org/strap/strap.php?USEJAVAWS&align=
LOC{ http://www.bioinformatics.org/strap/strap.php?USEJAVAWS&dialog=L&align=
BLAST{  http://www.bioinformatics.org/strap/strap.php?USEJAVAWS&dialog=B&align=
STRAPec{ http://www.bioinformatics.org/strap/strap.php?USEJAVAWS&rename=sp&align=UNIPROT_EC:




# Trasnmembrane Transport
TCDB{  http://www.tcdb.org/simple_search.php/??QUERY=
TDB{ http://www.membranetransport.org/search.php??type=%&family=All+Families+&protein=All+Transporters&search=S&substrate1=
TCDB: http://www.tcdb.org/tcdb/index.php?tc=
MEMTRANS_HSAP: http://www.membranetransport.org/protein.php?pOID=hsap2&pSynonym=

# Sequence Alignments
CDD: http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=*
PFAM: Pfam: PfamA:  http://pfam.sanger.ac.uk/family?acc=
PF0 http://pfam.sanger.ac.uk/family?acc=PF0
PRODOM: http://prodom.prabi.fr/prodom/current/cgi-bin/request.pl?question=SPTR&query=        
PD0 http://prodom.prabi.fr/prodom/current/cgi-bin/request.pl?question=SPTR&query=PD0        
INTERPRO: http://www.ebi.ac.uk/interpro/IEntry?ac=
IPR0 http://www.ebi.ac.uk/interpro/IEntry?ac=IPR0
FSSP: http://jura.ebi.ac.uk:8765/holm/qz?find=
SYSTERS: http://systers.molgen.mpg.de/cgi-bin/nph-fetchcluster.pl?CLNR=*&LINES=3

# Protein/ NT Sequences
ENST http://www.ensembl.org/Homo_sapiens/transview?transcript=ENST
ENSP http://www.ensembl.org/Homo_sapiens/protview?peptide=ENSP
UBIC: http://www.bioinformatics.ubc.ca/resources/tools/
ENSG{ http://www.ensembl.org/Homo_sapiens/Search/Summary?species=Homo_sapiens;idx=;q=
ENSEMBL{ http://www.ensembl.org/Homo_sapiens/Search/Summary?species=all;idx=;q=
ENSG http://www.ensembl.org/Homo_sapiens/geneview?gene=ENSG
ENSMUSG http://www.ensembl.org/Mus_musculus/geneview?gene=ENSMUSG
IPI http://www.ebi.ac.uk/cgi-bin/dbfetch?db=IPI&style=raw&id=IPI
UPI http://www.ebi.ac.uk/cgi-bin/dbfetch?db=UNIPARC&style=raw&id=UPI
UNIPROT: UNIPROT_EBI: UNIPROTKB:  SWISS: UR050: UR090: UR100:   http://www.ebi.uniprot.org/entry/
EMBL: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=EMBL&id=
EMBLCDS: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=EMBLCDS&id=

NCBI_AA: http://www.ncbi.nlm.nih.gov/protein/ 
NCBI_NT: http://www.ncbi.nlm.nih.gov/nuccore/


PIR: http://pir.georgetown.edu/cgi-bin/nbrfget?uid=
INTACT: http://www.ebi.ac.uk/intact/search/do/search?searchString=
HMDB: http://hmdb.ca/scripts/show_card.cgi?METABOCARD=*.txt
PRINTS: http://soap.genome.ad.jp/dbget-bin/www_bget?prints+
DIP: http://dip.doe-mbi.ucla.edu/dip/Search.cgi?SM=3&AC=DIP:*&Search2=Query+DIP&GE=&DS=&PIR=&GB=&TX=&SF=&FN=&LO=&KW=
PDOC: http://www.expasy.org/cgi-bin/nicedoc.pl?
PROSITE: http://www.expasy.org/cgi-bin/nicesite.pl? 


# Uniprot ID
PFAM_PROTEIN: http://pfam.sanger.ac.uk//protein/
DASTY:UNIPROT: http://www.ebi.ac.uk/dasty/client/ebi.php?label=BIOSAPIENS&q=
DASTY: http://www.ebi.ac.uk/dasty/client/ebi.php?label=BIOSAPIENS&q=
PDB_CATALYTIC_SITE: http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/CSA/CSA_Site_Wrapper.pl?pdb=
ARCHSCHEMAu: http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/archschema/RunArchSchema.jnlp?source=STRAP&seqId=
ARCHSCHEMApf: http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/archschema/RunArchSchema.jnlp?source=STRAP&pfamId=


# Pdb ID
PDBj: http://service.pdbj.org/mine/Detail?PAGEID=Summary&PDBID=
PDBe: http://www.ebi.ac.uk/pdbe-srv/view/entry/
PDB: PDB_RCSB: http://www.rcsb.org/pdb/cgi/explore.cgi?pdbId=
VIPERDB: http://viperdb.scripps.edu/info_page.php?USEJAVAWS&VDB=
PDB_3DEE: http://www.compbio.dundee.ac.uk/3Dee/c_page/*.html
PDB_SUM: http://www.ebi.ac.uk/pdbsum/
JENA: http://www.fli-leibniz.de/cgi-bin/ImgLib.pl?EXPAND=interactive,tool&CODE=
PDB_DALI: http://ekhidna.biocenter.helsinki.fi/dali/daliquery?find=
PDB_COLUMBA: http://wbi.informatik.hu-berlin.de/columba/columba.cgi?action=single_result&pdbid=
PDB_PQS: http://pqs.ebi.ac.uk/pqs-bin/pqs-hits?px=



#Expression
EXPR{  http://olli.charite.de/Forschung/HepatoNet/data/expressionByEnsg/*.html 
NCBI_GEO: http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=
GSE http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE
GDS http://www.ncbi.nlm.nih.gov/sites/GDSbrowser?acc=GDS
GSM http://www.ncbi.nlm.nih.gov/sites/GDSbrowser?acc=GSM



#Metabolism
KMAP: http://www.genome.jp/kegg/pathway/map/*.html
KEGGr: http://www.genome.jp/dbget-bin/www_bget?rn+
KEGGpw: http://www.genome.jp/dbget-bin/get_pathway?org_name=ko&mapno=
KEGGc: http://www.genome.jp/dbget-bin/www_bget?compound:
KLIGAND: http://www.genome.jp/dbget-bin/www_bget?cpd:
REACTOME: http://www.reactome.org/cgi-bin/eventbrowser_st_id?ST_ID=
BIOCYC_PW: http://biocyc.org/META/NEW-IMAGE?type=PATHWAY&object=
COMPOUND_CHEBI: http://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:
COMPOUND_3DMET: http://www.3dmet.dna.affrc.go.jp/bin2/show_data.e?acc=
COMPOUND_PUBCHEM: http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=

#disease
DISEASESDATABASE: http://www.diseasesdatabase.com/*.htm


# EC Class
BRENDAec{  BRENDAec: http://www.brenda-enzymes.info/php/result_flat.php4?ecno=
KEGGec{ http://www.genome.jp/dbget-bin/www_bget?enzyme+
METACYC{ http://metacyc.org/META/substring-search?type=NIL&object=
UMBBDec{ http://umbbd.msi.umn.edu/servlets/pageservlet?ptype=e&ECcode=
METACYCec{ http://metacyc.org/META/substring-search?type=NIL&object=
INTENZec{ http://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=
EXPASYec{ http://www.expasy.org/cgi-bin/nicezyme.pl?
REACTOMEec{ http://www.reactome.org/cgi-bin/link?SOURCE=EC&ID=
NISTec{ http://xpdb.nist.gov/enzyme_thermodynamics/enzyme_compose_query.pl?EC=
IUBMBec{ http://www.chem.qmul.ac.uk/iubmb/enzyme/EC*.html?REPLACE_CHAR./
SABIORKec{ SABIORKec: http://sabiork.h-its.org/index2.jsp?EC=
# Classification
GO: http://www.ebi.ac.uk/ontology-lookup/?termId=GO%3A
NCBI_TAXONOMY: http://www.ncbi.nlm.nih.gov:80/htbin-post/Taxonomy/wgetorg?id=


# Wiki
WIKI: WIKIPEDIA{ http://en.wikipedia.org/wiki/
WIKIde: http://de.wikipedia.org/wiki/
PDBWIKI: PDBWIKI{ http://pdbwiki.org/index.php/
BIOPERL: http://www.bioperl.org/wiki/
BIOWIKI: BIOWIKI{ http://biowiki.org/
WIKTION: http://en.wiktionary.org/wiki/
BioinfWIKI:  http://wiki.bioinformatics.org/
BioinfOrg: http://www.bioinformatics.org/
WWONLINE: http://www.wordwebonline.com/en/
BIONITY: http://www.bionity.com/lexikon/e/
MOL_TOOLKIT: http://www.vivo.colostate.edu/molkit/*/index.html


# Free text search
PUBMED{ http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?CMD=search&DB=pubmed&term=
ISI{ http://apps.isiknowledge.com/UA_GeneralSearch.do??fieldCount=1&SID=newSession&action=search&product=UA&search_mode=GeneralSearch&value(select1)=TS&value(input1)=
CITEXPLORE{  http://www.ebi.ac.uk/citexplore/performQuery.do??queryString=
UNIPROT{ http://www.uniprot.org/uniprot/?sort=score&query=
GOOGLE{ http://www.google.com/search?q=


# Lit          
MRANKER{ http://cbdm.mdc-berlin.de/tools/medlineranker/cgi-bin/ranker.pl??trset=allmesh&bgset=medline&tset=trset&trset_text=
MRANKERR{ http://cbdm.mdc-berlin.de/tools/medlineranker/cgi-bin/ranker.pl??trset=allmesh&bgset=medline&tset=recent&trset_text=
PMID: PMID  PUBMED: pmid: PubMed=  MEDLINE= http://www.ncbi.nlm.nih.gov:80/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=
PUBMEDCENTRAL: http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=
OVIDWEB: http://ovidsp.tx.ovid.com/spa/ovidweb.cgi?T=JS&PAGE=fulltext&D=ovft&AN=


# Specifications
MolEvolFormats* http://www.molecularevolution.org/resources/fileformats
EBI_Formats*  http://www.ebi.ac.uk/help/formats.html
cache*  http://www.bioinformatics.org/strap/cache.html
/FTId= FTId= http://www.expasy.ch/cgi-bin/get-sprot-variant.pl?




DEMO_VIA_PORT_1234: http://localhost:1234?The+id+is+*.+This+demonstrates+how+other+programs+can+be+controled

