package charite.christo;
import java.io.*;

public class ChInStream  {
    private final byte[] BUF;
    private int bufFrom,bufTo;
    private final InputStream _is;

    public ChInStream(InputStream is, int bufSize) { _is=is; BUF=new byte[bufSize]; }
    public boolean readLine(BA ba) { return _r(false,ba);}
    public boolean readLines(BA ba) { return _r(true,ba);}
    public boolean _r(boolean multipleLines, BA ba) {
        boolean success=false;
        try {
            next:
            for(;;) {
                if (bufFrom>=bufTo) {
                    final int read=_is.read(BUF);
                    bufFrom=0;
                    bufTo=read;
                    if (read==-1) break next;
                }
                int cr=-1;
                if (multipleLines) {
                    for(int i=bufTo; --i>=bufFrom; ) if (BUF[i]=='\n') { cr=i; break;}
                } else {
                    for(int i=bufFrom; i<bufTo; i++) if (BUF[i]=='\n') { cr=i; break;}
                }

                final int to=cr<0?bufTo : multipleLines ? cr+1 : cr;
                if (bufFrom<to || cr==bufFrom) {
                    success=true;
                    ba.a(BUF,bufFrom, multipleLines ? to : to>0&&BUF[to-1]=='\r'?to-1:to);
                }
                bufFrom=to+1;
                if (cr>=0) break next;
            }
            return success;
        } catch(IOException ex){ return false;}
    }
}
