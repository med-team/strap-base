package charite.christo;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**
   A graphical representation of a graph
   adjusting the node distances dynamically.
   @author Christoph Gille
   @author Sun Microsystems
*/
public class GraphSun extends JPanel implements Disposable,Runnable, java.awt.event.ActionListener {
    private transient IsEnabled _enabled;
    private double _max, _min;
    private transient GraphNode _closest, _pick;
    private final static Color _color=new Color(250, 220, 100);
    private final static int SCALE_MAX=400;
    private final ChSlider _sliderScale=new ChSlider("sliderScale",0,SCALE_MAX,100);
    private final JComponent
        _butLines=toggl("connect").s(true).cb(),
        _butImage=toggl().t("icons").tt("show images rather protein names<br>when defined").cb(),
        _buttonBar=pnl(HB,"#",new ChButton("SCRAMBLE").t("scramble").li(this), _butLines,_butImage);
    private int _w=333, _h=333;
    private final UniqueList<GraphNode> _vNodes=new UniqueList(GraphNode.class);
    private final UniqueList<GraphEdge> _vEdges=new UniqueList(GraphEdge.class);
    public GraphSun() {
        _sliderScale.setOrientation(JSlider.VERTICAL);
        _sliderScale.setToolTipText("Scale");
        pnl(this,CNSEW,null,null,_buttonBar,null,_sliderScale);
        makeTranslucent(this);
    }
    public void setEnabled(IsEnabled e) { _enabled=e; }
    private void addNode(GraphNode n) {
        final int w2=200/2,h2=200/2;
        n.x = w2*Math.random();
        n.y = h2*Math.random();
        n.x=200;
        n.y=200;
        _vNodes.add(n);
    }
    public void addEdge(GraphEdge e) {
        _vEdges.add(e);
        addNode(e.from());
        addNode(e.to());
    }
    public void run() {
        if (_enabled!=null && !_enabled.isEnabled(this)) return;
        /* preventFixedNodesFromfloatingOutTheScreen */
        final int w=_w,h=_h;
        for(GraphNode n : _vNodes.asArray()) {
            if (n.isFixed()) {
                if (n.x<0) n.x=0;
                if (n.y<40) n.y=40;
                if (n.x+20>w) n.x=w-20;
                if (n.y+20>h) n.y=h-20;
            }
        }
        relax();
        repaint();
    }
    private double scale() {
        double mv=_max-_min; if (mv==0) mv=1;
        return _sliderScale.getValue()/300f/mv*_w;
    }
    private void relax() {

        final double thisScale=scale();
        double max=Double.MIN_VALUE, min=Double.MAX_VALUE;
        for(GraphEdge e:_vEdges.asArray()) {
            final double v=e.len();
            if (max<v) max=v;
            if (min>v) min=v;
        }
        _max=max;
        _min=min;

        for(GraphEdge e:_vEdges.asArray()) {
            final GraphNode to=e.from(),from=e.to();
            final double

                v=(e.len()-min),
                distance= e.isDistanceScore() ? v : max-min-v;
            if (Double.isNaN(distance)) continue;

            final double
                vx=to.x-from.x, vy=to.y-from.y,
                len=Math.sqrt(vx * vx + vy * vy),
                f=len<2 ? 0 : distance*thisScale/len-1 ;
            if (Double.isNaN(f)) continue;
            final double dx=f*vx, dy=f*vy;
            to.dx+=dx; to.dy+=dy;
            from.dx+=-dx; from.dy+=-dy;
        }
        for(GraphNode n1 : _vNodes.asArray()) {
            double dx=0,dy=0;
            for(GraphNode n2 : _vNodes.asArray()) {
                if (n1==n2)continue;
                final double
                    vx=n1.x-n2.x,
                    vy=n1.y-n2.y,
                    len=vx * vx + vy * vy;
                if (len==0) {dx+=Math.random();dy+=Math.random();
                } else if (len < 100*100) {dx+=vx / len;dy+=vy / len;}
            }
            double dlen=dx*dx + dy*dy;
            if (dlen>0) {
                dlen=Math.sqrt(dlen) / 2;
                n1.dx+=dx/dlen;
                n1.dy+=dy/dlen;
            }
        }
        final int w=_w, h=_h;
        for(GraphNode n : _vNodes.asArray()) {
            if (!n.isFixed() && n!=_pick) {
                n.x+=Math.max(-5, Math.min(5, n.dx))- n.x/(w+1)*10;
                n.y+=Math.max(-5, Math.min(5, n.dy))- n.y/(h+1)*10;
                //if (n.x < 0) n.x=0; else if (n.x > d.width) n.x=d.width;
                //if (n.y < 0) n.y=0; else if (n.y > d.height) n.y=d.height;
            }
            n.dx /= 2;
            n.dy /= 2;
        }

    }
    private void paintNode(Graphics g, GraphNode n, FontMetrics fm) {
        final int x=(_w=getWidth())/2 + (int)n.x, y=(_h=getHeight())/2 + (int)n.y;
        Image image=null;
        if (isSelctd(_butImage)) { image= n.image(); }
        g.setColor((n==_pick) ? C(0xFFAFAF) : (n.isFixed() ? C(0xFF0000) : _color));
        if (image==null) {
            final String s=n.text();
            final int w=fm.stringWidth(s) + 10, h=fm.getHeight() + 4;
            g.fill3DRect(x - w/2, y - h / 2, w, h,n!=_closest);
            g.setColor(C(0));
            g.drawString(s, x - (w-10)/2, (y - (h-4)/2) + fm.getAscent());
        } else {

            g.drawImage(image,x-ICON_HEIGHT/2, y-ICON_HEIGHT/2,ICON_HEIGHT,ICON_HEIGHT, this);
        }
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        final long time=System.currentTimeMillis();
        final int w=_w,h=_h;
        final boolean isLines=isSelctd(_butLines);
        if (h<10 || w<10) return;
        final double max=_max, min=_min;
        final int xm=w/2,ym=h/2;
        for(GraphEdge e:_vEdges.asArray()) {
            final GraphNode from=e.from(),to=e.to();
            final double x1=from.x, y1=from.y;
            final double x2=to.x, y2=to.y;
            final double v=e.len();
            if (Double.isNaN(v)) continue;
            g.setColor(max>min ? blueYellowRed((float)( (v-_min)/(max-min))) : C(0xFF0000));
            if (isLines) g.drawLine( (int)(xm+x1), (int)(ym+y1), (int)(xm+x2), (int)(ym+y2));
        }
        final FontMetrics fm=g.getFontMetrics();
        for(GraphNode n:_vNodes.asArray()) {
            paintNode(g, n, fm);
        }
        ChThread.callEvery(0, 50+(int)(System.currentTimeMillis()-time),this,"GraphSun");
    }
    @Override public void paintChildren(Graphics g) {
        if (gcp(KEY_IS_PRINTING,this)==null)
            try {
                super.paintChildren(g);
            } catch(Throwable e){}
    }
    public void processEvent(AWTEvent ev) {
        final int
            id=ev.getID(),
            x=x(ev)-_w/2,
            y=y(ev)-_h/2;
        if (id==MOUSE_MOVED) {
            _closest=closestNode(x,y);
            setCursor(cursr(_closest !=null ? 'H':'D'));
        }
        if (_pick!=null && (id==MOUSE_DRAGGED || id==MOUSE_PRESSED || id==MOUSE_RELEASED )) {
            _pick.x=x;
            _pick.y=y;
        }
        if (id==MOUSE_DRAGGED) setCursor(cursr('M'));
        if (id==MOUSE_PRESSED) _pick=closestNode(x,y);
        if (id==MOUSE_RELEASED)_pick=null;
        final int wheel=wheelRotation(ev);
        if (wheel!=0) {
            final int v=_sliderScale.getValue();
            _sliderScale.setValue(maxi(0, mini(SCALE_MAX, v+wheel*10)));
        }
        super.processEvent(ev);
    }
    { this.enableEvents(ENABLE_EVT_MASK);}

    private GraphNode closestNode(int x,int y) {
        double bestdist=9999;
        GraphNode closest=null;
        for(GraphNode n : _vNodes.asArray()) {
            final double dx=n.x-x, dy=n.y-y, dist=dx*dx+dy*dy;
            if (dist < bestdist) {
                closest=n;
                bestdist=dist;
            }
        }
        return closest;
    }
    public void start() { ChThread.callEvery(0, 999,this,"GraphSun");}
    @Override public void dispose() { ChThread.callEvery(0, -1,this,"");}
    public void addButton(JComponent c,int pos) { _buttonBar.add(c,pos);}
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final int w=_w,h=_h;
        final double w2=w/2.0,h2=h/2.0;
        for(GraphNode n : _vNodes.asArray()) {
            if (!n.isFixed()) {
                if (cmd=="SHAKE") {
                    n.x+=w*(Math.random()-0.5);
                    n.y+=h*(Math.random()-0.5);
                }
                if (cmd=="SCRAMBLE") {
                    n.x= w2*Math.random();
                    n.y= h2*Math.random();
                }
            }
        }
    }

}
