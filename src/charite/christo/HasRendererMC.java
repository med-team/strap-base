package charite.christo;
/**
   @author Christoph Gille
*/
public interface HasRendererMC extends HasRenderer {
    long getRendererMC();
}
