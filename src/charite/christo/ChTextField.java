package charite.christo;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.awt.*;
import javax.swing.JTextField;
import javax.swing.text.Document;
import java.awt.event.*;
import java.io.File;
/**
   @author Christoph Gille

*/
public final class ChTextField extends JTextField {
    private String _defaultText;
    private boolean _painting, _painted;
    private Object _contentType;

    public ChTextField() { this("",0L);}
    public ChTextField(Object defaultText) { this(defaultText,0L);}

    public ChTextField(Object defaultText, long option) {
        Object defaultT=defaultText;
        long opt=option;
        if (defaultT instanceof File) {
            defaultT=toStrg(defaultT);
            opt|=COMPLETION_FILES_IN_WORKING_DIR;
        }
        if (!(defaultT instanceof Document || defaultT instanceof JTextField || defaultT instanceof CharSequence || defaultT==null)) assrt();
        final Document doc=defaultT instanceof Document ? (Document)defaultT :
            defaultT instanceof JTextField ? ((JTextField)defaultT).getDocument() :null;
        if (doc!=null) setDocument(doc);
        else {
            if ((_defaultText=toStrg(defaultT))!=null) t(_defaultText);
        }
        monospc(this);
        if (opt!=0) {
            final ChTextComponents tools=tools();
            final boolean  complFiles= (opt&COMPLETION_FILES_IN_WORKING_DIR)!=0;
            if ( (opt&COMPLETION_DIRECTORY_PATH)!=0 || complFiles) {
                tools.enableUndo(true)
                    .enableWordCompletion(ChTextComponents.COMPL_TAB, complFiles ? dirWorking() : new ChFileFilter("-d"),null,null)
                    .underlineRefs(ULREFS_NOT_CLICKABLE)
                    .highlightOccurrence(ChTextComponents.WRONG_CHARS_IN_FILEN, null,null, HIGHLIGHT_UPDATE_IF_TEXT_CHANGES|HIGHLIGHT_STYLE_WAVE, C(0xFF0000));
            }
            tools.underlineRefs(opt);
        }
    }
    @Override public void processEvent(AWTEvent ev) {
        if (!_painted) return;
        final AWTEvent newEv=tools().pEv(ev);
        if (newEv!=null) try { super.processEvent(newEv);} catch(Throwable ex){ if (myComputer()) stckTrc(ex);}
    }
    { this.enableEvents(MouseEvent.MOUSE_EVENT_MASK|KeyEvent.KEY_EVENT_MASK|MouseEvent.MOUSE_WHEEL_EVENT_MASK|FocusEvent.FOCUS_EVENT_MASK);}
    public Object getContentType() { return _contentType;}
    public ChTextField ct(Object c) { _contentType=c;  return this; }
    @Override public String getText() { if (isDisplayable() && !isEDT()) assrt(); return super.getText();}
    public ChTextField t(Object o) {
        final String s=toStrg(o);
        if (s!=null) tools().setTextTS(s);
        return this;
    }
    public ChTextField cols(int columns,boolean minimumSize, boolean maximize){
        setColumns(columns);
        if (maximize)    setMaxSze(-1,-1, this);
        if (minimumSize) setMinSze(-1,-1, this);
        return this;
    }

    @Override public Dimension getPreferredSize() {
        final Dimension ps=prefSze(this);
        return  ps!=null ? ps : super.getPreferredSize();
    }
    public ChTextField tt(CharSequence toolTip) {
        setTip(toolTip,this);
        return this;
    }

    @Override public String getToolTipText(MouseEvent mev) {
        final String tt=(String)tools().run(ChTextComponents.TT,mev);
        return tt!=null ? tt : ballonMsg(mev, super.getToolTipText(mev));
    }

     @Override public boolean isOpaque() { return _painting ? false : super.isOpaque(); }

    @Override public void paintComponent(Graphics g) {

       _painted=true;
       final int w=getWidth(), h=getHeight();
       final Color bg=getBackground();

        if (bg!=null && isOpaque()) {
            g.setColor(bg);
            fillBigRect(g,0,0,w,h);
        }
        if (tools().paintHook(this,g,false)) {
            _painting=true;
            super.paintComponent(g);
            _painting=false;
        }

        if (_contentType!=null) {
            final String txt=toString();
            final int L=sze(txt);
            Color colorValid=C(0x00b200);
            String textValid=null;
            if (_contentType==File.class && L>0)   {
                final File d=file(txt);
                if (fExists(d) && !d.isDirectory()) { textValid="exists";}
                else if (d.isDirectory()) { textValid="directory"; colorValid=C(0xFF00FF);}
                else { textValid="no such file"; colorValid=C(0xFF0000);}
            }
            {
                final Object o=gcpa(KEY_IF_EMPTY,this);
                if (L==0 && o!=null) { textValid=o.toString(); colorValid=C(0x808080);}
            }

            if (textValid!=null) {
                g.setColor(colorValid);
                g.drawString(textValid,w- strgWidth(g,textValid),charA(g));
            }
        }
        tools().paintHook(this,g,true);
    }

    public ChTextField li(ActionListener l) { addActLi(l,this); return this;}
    @Override public String toString() {
        tools().loadSavedText();
        final String s=toStrgTrim(isEDT() ? getText() : tools().getText());
        return sze(s)==0 ? "" : s;
    }
    private ChTextComponents _tools;
    public synchronized ChTextComponents tools() {
        if (_tools==null) _tools=new ChTextComponents(this);
        return _tools;
    }
    public ChTextField saveInFile(String fileName) {tools().saveInFile(fileName); return this;}

    public Object getDndDateien() { return dndV(false,this); }
}
