package charite.christo;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
        UIManager.getDefaults().put("Menu.selectionForeground", C(0));
        Menu.selectionBackground
        http://lists.apple.com/archives/java-dev/2010/Jul/msg00133.html
*/
public final class ChJMenu extends JMenu implements ProcessEv, MenuListener {

    private ChButton _b;
    public ChJMenu(String label) {
        super(label);
        addMouseListener(evAdapt(this));
        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
        addMenuListener(this);
        setFG(0,this);
        getPopupMenu().addPopupMenuListener(chEvLi());
        setOpaque(false);
    }

    public ChJMenu(ChButton b) {
        this(b.getText());
        _b=b;
        setDelay(0);
    }
    public ChJMenu cp(Object key, Object value) { pcp(key,value,this); return this;}
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Late Initialization  >>> */
    private boolean _isInit, _isSel;
    @Override public void menuDeselected(MenuEvent e) {
        _isSel=false;
        getModel().setRollover(false); /* BugFix for WindowsMenuUI */
    }
    @Override public void menuCanceled(MenuEvent e) { _isSel=false; }
    private static long _actEvtWhen;
    @Override public void menuSelected(MenuEvent e) {
        ChUtils.handleActEvt(e.getSource(), ACTION_MENU_SELECTED, 0);
        ChUtils.callSetEnabled(e);
        if (!_isSel) _isSel=true; else return; /* Otherwise 2 events under Windows */
        getPopupMenu().setLightWeightPopupEnabled(false);

        if (!_isInit) {
            _isInit=true;
            for(int i=getMenuComponentCount(); --i>=0;) {
                final Object o=getMenuComponent(i);
                if (o instanceof ChButton.MItem) ((ChButton.MItem)o).paintComponent(null);/* Late init icon */
            }
        }
        final Menu awtM=_awtMenu;
        if (awtM!=null) {
            long hc=0;
            for(int i=awtM.getItemCount(); --i>=0;) {
                hc+=awtM.getItem(i).hashCode()+ ( (long)awtM.getItem(i).getLabel().hashCode()<<32);
            }
            if (_awtMenuHC!=hc) {
                removeAll();
                _awtMenuHC=hc;
                menu2jAddItems(awtM,this);
                _awtMenuHC=awtM.getItemCount();
            }
        }
        if (_b!=null) {
            final long time=System.currentTimeMillis();
            if (time-_actEvtWhen>333) {
                _actEvtWhen=time;
                pcp(KEY_VISIBLE_COMPONENT, this, _b);
                String cmd=gcps(KEY_ACTION_COMMAND,this);
                if (cmd==null) cmd=gcps(KEY_ACTION_COMMAND, _b);
                if (cmd==null) cmd=CMD_MENU_SELECTION_CHANGED;
                handleActEvt(_b!=null ? _b : this, cmd,0);
                inEDTms( thrdM("setSelected", this, new Object[]{boolObjct(false)}),99);
            }
        }
    }
    @Override public boolean isEnabled() {
        if (gcp(KOPT_DISABLE_IF_CHILDS_DISABLED,this)!=null) {
            boolean oneEnabled=false;
            for(Component c : childs(this)) {
                oneEnabled|=c instanceof AbstractButton && c.isEnabled();
            }
            if (!oneEnabled) return false;
        }
        if (!super.isEnabled()) return false;
        if (_b!=null) return _b.isEnabled();
        final IsEnabled ise=gcp(KEY_ENABLED,this,IsEnabled.class);
        if (ise!=null) return ise.isEnabled(this);
        return true;
    }
    /* <<< Late Initialization <<< */
    /* ---------------------------------------- */
    /* >>> Layout  >>> */
    @Override public Dimension getPreferredSize() {
        if (gcp(KOPT_DISABLE_IF_CHILDS_DISABLED,this)!=null && !isEnabled()) return dim(0,0);
        {
            final Dimension ps=prefSze(this);
            if (ps!=null) return ps;
        }
        final boolean noIcons=gcp(KOPT_NO_ICON,this)!=null;
        if (getText()==KEY_ACCELERATOR) return dim(0,0);
        if (_oIcon!=null && !(getParent() instanceof JMenuBar) && !noIcons) _oIcon=ChButton.mkIcon(_oIcon, this);
        final Dimension d=super.getPreferredSize();
        if (d!=null && _oIcon!=null && !noIcons)  d.height=ICON_HEIGHT;
        return d;
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Event JMenu   BasicMenuUI  >>> */
    /* http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4403182
     */

    private static Object  _lockedParent;
    static AWTEvent[] lockSubmenuOnClick(AWTEvent ev) {
        final int id=ev.getID();
        final AWTEvent ret[]={null,null};
        final Object c=ev.getSource(), parent=parentC(c);
        if (id!=MOUSE_ENTERED && id!=MOUSE_PRESSED || !isSelctd(buttn(TOG_LOCK_JMENU)) || parent instanceof JMenuBar) {
            ret[0]=ev;
        } else if (c!=null) {
            if (id==MOUSE_PRESSED) {
                if (c instanceof JMenu) {
                    _lockedParent=parent;
                }
            } else if (_lockedParent!=parent) {
                _lockedParent=null;
                ret[0]=ev;
            }
            if (id==MOUSE_PRESSED) {
                ret[0]=new MouseEvent((Component)c, MOUSE_ENTERED, ((MouseEvent)ev).getWhen()-1, 0, 1, 1,  0, false,0);
                ret[1]=ev;
            }
        }
        return ret;
    }
    { enableEvents(ENABLE_EVT_MASK);}

    public void processEvent(AWTEvent ev) {
        try {
            for(AWTEvent ev2 : lockSubmenuOnClick(ev)) {
                if (ev2!=null)  super.processEvent(ev2);
            }
        } catch(Throwable ex){ stckTrc(); }
    }

    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final MenuItem mi=(MenuItem)gcp(KEY_AWT_MI,q);
        if (mi!=null) {
            if (mi instanceof CheckboxMenuItem) ((CheckboxMenuItem)mi).setState(isSelctd(q));
            final ActionEvent aev=new ActionEvent(mi, ActionEvent.ACTION_PERFORMED, mi.getActionCommand(), modifrs(ev));
            for(ActionListener al : mi.getActionListeners()) al.actionPerformed(aev);
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Layout  >>> */
    @Override public void paintComponent(Graphics g) {
        final Container mb=getParent();
        {
            /* Bug fix Windows ignoring setOpaque and setContentAreaFilled */
            if (isSelected()) {
                final Color bg=UIManager.getDefaults().getColor("MenuItem.selectionBackground");
                if (bg!=null) { g.setColor(bg); g.fillRect(0,0,999,99); }
                super.paintComponent(g);
            } else {
                final Color bg0=gcp(KEY_BACKGROUND, this, Color.class);
                final Color bg=mb==null?null:mb.getBackground();
                if (bg==C(0) || bg0!=null) {
                    if (bg0!=null) { g.setColor(bg0); g.fillRect(0,0,999,99); }
                    final Color fg=mb.getForeground();
                    g.setColor(bg0!=null?C(0) : fg!=null?fg:C(0xFFffFF));
                    g.drawString(getText(), 0, (getHeight()-charH(g))/2+charA(g));
                } else {
                    super.paintComponent(g);
                }
            }
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> awt-Menu >>> */
    private final static Object KEY_AWT_MI=new Object();
    private Menu _awtMenu;
    private long _awtMenuHC;
    private String _rplcMenuItems[][];
    ChJMenu setAwtMenu(Menu m, String[][] rplc) {
        _awtMenu=m;
        _rplcMenuItems=rplc;
        String label=m.getLabel();
        for(int j=sze(rplc); --j>=0;) {
            if (label.equalsIgnoreCase(rplc[j][0]) && get(1,rplc[j])!=null) label=get(1,rplc[j]);
        }
        setText(label);
        setHeavy(this);
        return this;
    }
    void menu2jAddItems(Menu m, JMenu jm) {
        nextItem:
         for(int i=0; i<m.getItemCount(); i++) {
            final MenuItem it=m.getItem(i);
            final String label0=it.getLabel(), rplc[][]=_rplcMenuItems;
            String label=label0;
            for(int j=sze(rplc); --j>=0;) {
                if (label0.equalsIgnoreCase(rplc[j][0])) label=get(1,rplc[j]);
            }
            if (label==null) continue;
            if ("-".equals(label)) jm.addSeparator();
            else if (it instanceof Menu) jm.add(new ChJMenu("").setAwtMenu((Menu)it,_rplcMenuItems));
            else {
                final CheckboxMenuItem cb=it instanceof CheckboxMenuItem ? ((CheckboxMenuItem)it) : null;
                final ChButton b=new ChButton(cb!=null ? 'T':'B',label).li(evAdapt(this)).cp(KEY_AWT_MI,it);
                if (cb!=null) b.s(cb.getState());
                setUndockble(b);
                jm.add(b.mi(null));
            }
        }
    }
    /* <<< awt-Menu <<< */
    /* ---------------------------------------- */
    /* >>> Icon >>> */
    private Object _oIcon=null;
    public ChJMenu i(Object o) { _oIcon=o; return this;}
    /* <<< Icon <<< */
    /* ---------------------------------------- */
    @Override public Icon getIcon() {
        if (gcp(KOPT_NO_ICON,this)!=null) return null;
        return !notSelctd(BUTTN[TOG_MENUICONS]) ?  super.getIcon() : getParent() instanceof JMenuBar ? null : iicon(IC_BLANK);
    }
    public static void hideChildlessMenus(Component m) {
        final Component cc[]=childs(m);
        if (m instanceof JMenu) {
            boolean e=false;
            for(Component c : cc) if (c.isEnabled()) { e=true; break; }
            ((JMenu)m).setPreferredSize(e?null:dim(0,0));
            if (!e) return;
        }
        for(Component c : cc) hideChildlessMenus(deref(c, JMenu.class));
    }
}
/*
http://hintsforums.macworld.com/showthread.php?p=489302#post489302
*/
