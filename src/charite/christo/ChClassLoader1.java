package charite.christo;
import java.net.URL;
import java.util.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public final class ChClassLoader1 extends java.net.URLClassLoader implements HasWeakRef {
    private final Map<String,Object> mapClasses=new HashMap();
    private static URL _urlAspectjrt;
    /* ---------------------------------------- */
    /* >>> Instance >>> */
    private static int nextID=1;

    ChClassLoader1(URL[] urls,ClassLoader parent) {
        super(urls,parent);
        log().a(ANSI_GREEN+" Constructor ChClassLoader1 "+ANSI_RESET).join(urls," ").a('\n').send();

    }
    private final Object weakRef=newWeakRef(this);
    public Object wRef() {return weakRef;}
    private static Map<String, Object> mapInstances=new HashMap();

    public static String computeKey(File[] files, File dir) {
        String ss[]=new String[files.length+1];
        for(int i=0; i<ss.length; i++) {
            final File f=i<files.length? files[i] : dir;
            if (isDir(f)||sze(f)>0) {
                ss[i]=f.toString();
                if (i>0 && ss[i].equals(ss[i-1])) ss[i]=null;
            }
        }
        ss=rmNullS(ss);
        Arrays.sort(ss,comparator(COMPARE_ALPHABET));
        final BA sbKey=new BA(444);
        for(String s : ss) {
            sbKey.a(s).a(' ').a(sze(file(s))).a(',');
        }
        return sbKey.toString();
    }

    public static ChClassLoader1 getInstance(boolean newInstance, File[] files, File dir) {
        final String key=computeKey(files,dir);
        ChClassLoader1 cl=newInstance ? null : deref(mapInstances.get(key),ChClassLoader1.class);
        if (cl==null) {
            final URL urls[]=new URL[files.length+2+(dir!=null?1:0)];
            log().a('#').a(nextID).a(" "+ANSI_GREEN+" new ChClassLoader1 "+ANSI_RESET).aln(dir);
            int count=0;
            for(int i=files.length; --i>=0;) {
                log().a(' ',2).aln(files[i]);
                urls[count++]=url(files[i]);
            }
            log().a('\n').send();
            if (dir!=null) urls[count++]=url(dir);
            urls[count++]=urlThisJarFile();
            if (myComputer()) {
                if (_urlAspectjrt==null) _urlAspectjrt=url(file("/usr/share/java/aspectjrt.jar"));
                urls[count++]=_urlAspectjrt;
            }
            cl=new ChClassLoader1(rmNullA(urls,URL.class), ChClassLoader1.class.getClassLoader());
            if (!newInstance) mapInstances.put(key,cl.weakRef);
        }
        return cl;
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> loadClass >>> */
    @Override public synchronized Class loadClass(String cn) throws ClassNotFoundException {
        if (!Insecure.CLASSLOADING_ALLOWED) return null;
        final char c0=cn.charAt(0);
        boolean findClass=
            !(c0=='c'&&cn.startsWith("charite.")   || c0=='j'&&cn.startsWith("java.")       ||
              c0=='j'&&cn.startsWith("javax.")     || c0=='c'&&cn.startsWith("com.sun.")    ||
              c0=='a'&&cn.startsWith("apple.awt.") || c0=='s'&&cn.startsWith("sun.")        ||
              c0=='o'&&cn.startsWith("org.xml.sax")|| c0=='o'&&cn.startsWith("org.w3c.dom") ||
              c0=='o'&&cn.startsWith("org.ietf")   || c0=='o'&&cn.startsWith("org.omg"));
        //&& !cn.startsWith("com.sun.opengl.") ;
        if (!findClass && cn.indexOf("PROXY")<0) {
            for(int i=sze(vNO_PARENT_DELEGATION); --i>=0;) {
                if (cn.startsWith(vNO_PARENT_DELEGATION.get(i))) {
                    findClass=true;
                    break;
                }
            }
        }
        if (cn.startsWith(_CCS) && cn.indexOf("Biojava")>=0) findClass=true;
        log().a(findClass).a(" loadClass  ").a(cn).a("==>  ").aln(findClass ? "findClass" : "super.loadClass").send();
        Class cRet=null;
        //putln(ANSI_GREEN+" going to loadClass="+ANSI_RESET+findClass+" cn="+cn);

        try {
            cRet=findClass ? findClass(cn): super.loadClass(cn);
        } catch(ClassNotFoundException t) {
            if(!findClass) cRet=findClass(cn);
            if (cRet==null) throw t;
        }
        //putln(ANSI_RED+"findClass="+ANSI_RESET+findClass+" cRet="+cRet);
        return cRet;

    }
    @Override public synchronized Class findClass(String cn0) throws ClassNotFoundException{
        if (!Insecure.CLASSLOADING_ALLOWED) { assrt(); return null; }
        //final boolean debug=cn0.startsWith("com.inet.jortho.DictionaryFactory");

        final String cn=cn0!=null && cn0.startsWith("com.sun.tools.javac.resources.compiler_") ?
            "com.sun.tools.javac.resources.compiler" : cn0;
        Class c=deref(mapClasses.get(cn), Class.class);
        if (c!=null) return c;
        //putln(ANSI_YELLOW+"going to findClass "+cn, getURLs(),ANSI_RESET+" "+cn+" "+getParent());
        if (cn.startsWith("org.openscience.jmol.Properties")) throw(new ClassNotFoundException());
        log().a(cn.indexOf('$')<0 ? ANSI_CYAN : ANSI_RESET).a("ChClassLoader1#findClass "+ANSI_RESET).a(cn);
        try {
            try {
                c=super.findClass(cn);

                if (c!=null) mapClasses.put(cn,wref(c));
                log().aln(GREEN_SUCCESS).send();
                return c;
            } catch(ClassNotFoundException ex) {
                log().a(ANSI_RED+" findClass "+ANSI_RESET).aln(ex).send();
                throw(ex);
            }
        } catch(NoClassDefFoundError er) {
            log().a(ANSI_RED+"findClass "+ANSI_RESET).aln(er).send();
            throw(new ClassNotFoundException());
        }

    }
    /* <<< loadClass <<< */
    /* ---------------------------------------- */
    /* >>> findLibrary >>> */
    @Override public String findLibrary(String libname) {
        if (!Insecure.CLASSLOADING_ALLOWED) { assrt(); return null; }
        for(boolean lib : new boolean[]{true,false}) {
            final java.io.File f=file(dirJars(),System.mapLibraryName(libname));
            if (f.exists()) {
                log().a(ANSI_CYAN+"ChClassLoader1.findLibrary("+ANSI_WHITE).a(libname).a(") ==> ").aln(f).send();
                return f.toString();
            }
        }
        return super.findLibrary(libname);
    }
    // ----------------------------------------
    public final static List<String> vNO_PARENT_DELEGATION=new ArrayList();
    static {
        adUniq(_CCP+"ChJmol",vNO_PARENT_DELEGATION);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Log  >>> */
    private static BA _log;
    public static BA log() {
        if (_log==null) toLogMenu(_log=new BA(999), "Strap ClassLoader", null);
        return _log;
    }
    /* <<< Log <<< */
    /* ---------------------------------------- */
    /* >>> Security  >>> */
    static {
        if (Insecure.CLASSLOADING_ALLOWED) try { setSecurityPolicy(); } catch(Throwable t){}
    }
    private static java.security.Policy policy;
    public static void setSecurityPolicy() {
        if (Insecure.CLASSLOADING_ALLOWED && policy==null) {
            policy=new java.security.Policy() {
                    @Override public java.security.PermissionCollection getPermissions(java.security.CodeSource codesource) {
                        final java.security.Permissions perms = new java.security.Permissions();
                        perms.add(new java.security.AllPermission());
                        return perms;
                    }
                    @Override public void refresh(){}
                };
            try {  java.security.Policy.setPolicy(policy); } catch(Throwable e) {
                putln(RED_CAUGHT_IN+"ChClassLoader1 ",e);
            }
        }
    }
}
/*
http://docs.oracle.com/javase/7/docs/technotes/guides/net/ClassLoader.html
*/
