package charite.christo;
import java.awt.event.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

With this dialog shell programs
can be run on files or amino acid sequences.

The program can be chosen from the choice menu or can be typed.

Dollar signs followed by an asterisk are replaced by the
command arguments. Otherwise the arguments are placed after the
expression.

<br><br><b>New shell process:</b>

Complex command lines involving pipes, redirection, variable assignments or loops need to be interpreted by a shell.

This is achieved by starting the command with "SH"  (or "CYGWINSH" on Windows PCs).

<br><br><b>White space:</b>

White space within arguments must be written as <b>"%20"</b>.

<br><br><b>Details:</b>
For details of the running process hold the Ctrl-key while pressing the button <i>LABEL:ChButton#BUTTON_GO</i> or watch the stdout.

   @author Christoph Gille
*/
public class DialogExecuteOnFile extends JPanel implements ActionListener {
    private String _argv[];
    private ChTextField _tf;
    private ChCombo _combo;
    private Object[] _radioTf;
    private final Customize _cust;
    public DialogExecuteOnFile(Customize c) { _cust=c;  }
    private ChFrame _f;

    public void show(String[] ff) {
        _argv=ff;
        for(int i=ff.length; --i>=0;) ff[i]=rplcToStrg(" ","%20",ff[i]);
        if (_f==null) {
            _combo=_cust.newComboBox();
            _radioTf=radioGrp(new String[]{"",""},0,this);
            _tf=new ChTextField().saveInFile(getClass()+"_tf");
            _tf.setEnabled(false);
            pcp(KEY_IF_EMPTY,"enter command line",_tf);
            if (""==_tf.toString()) _tf.t("SH echo $* > /tmp/t;xterm -e less /tmp/t");
            final JComponent
                pViewFile=this,
                panCombo=pnl(HB,_radioTf[0],"#",_combo," ",Customize.newButton(_cust).setOptions(ChButton.NO_FILL|ChButton.NO_BORDER|ChButton.ICON_SIZE)),
                pMain=pnl(VB,
                         pnl(HB,pnl("<h2>Shell command</h2>"),"#",panHS(this)),

                            pnl(HBL,new ChButton("A").t("See argument list").li(this)),
                            pViewFile,
                            " ",
                         pnl(CNSEW,pnl(new ChButton("C").t("\u2193").li(this).tt("Copy the selected string into the text-field")),panCombo,pnl(HB,_radioTf[1],"#",_tf)),
                            pnl(new ChButton("GO").t(ChButton.GO).li(this))
                            );
            _f=new ChFrame("execute").ad(pMain);
        }
        _f.shw(ChFrame.PACK|ChFrame.AT_CLICK);
    }

    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (cmd=="A") shwTxtInW("Argument list",new BA(99).join(_argv).a('\n'));
        if (cmd=="C") _tf.t(_combo.toString());
        if (cmd=="GO") {
            final BA command=toBA(radioGrpIdx(_radioTf)==0 ? _combo : _tf);
            final BA args=new BA(0).join(_argv," ");
            final BA sb=new BA(99).a(command);
            args.getAnsiEscapes();
            if (strstr("$*",command)>=0) sb.replace(0L,"$*",args);
            else sb.a(' ').a(args);
            final ChExec ex=new ChExec(ChExec.STDOUT_IN_TEXT_BOX|ChExec.LOG).setCommandLine(sb);
            if (isCtrl(ev)) ex.setCustomize(_cust);
            startThrd(ex,"DialogExecuteOnFile");
        }
        final int iRadio=radioGrpIdx(q);
        if (iRadio>=0) {
            _combo.setEnabled(iRadio==0);
            _tf.setEnabled(iRadio!=0);
        }
    }
}
