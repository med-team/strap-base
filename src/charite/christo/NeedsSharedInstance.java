package charite.christo;
/**
   @author Christoph Gille
*/
public interface NeedsSharedInstance {
    void setSharedInstance(Object shared);
    Object getSharedInstance();
}
