package charite.christo;
import static charite.christo.ChUtils.*;
import java.util.*;
/**
   A fast Tokenizer
   @author Christoph Gille
*/
public class ChTokenizer  {
    private byte[] _text;
    private int _end, _pos, _f, _t;
    private boolean _delimiters[];
    private String  _asStr;
    public int countTokens() {
        final int oldPos=_pos;
        int count=0;
        while(nextToken()) count++;
        _pos=oldPos;
        return count;
    }

    public ChTokenizer setText(BA ba) { if (ba!=null) setText(ba.bytes(), ba.begin(), ba.end()); return this;}
    public ChTokenizer setText(byte bb[],int begin, int endPosition) {
        _asStr=null; _f=_t=-1; _text=bb; _pos=begin; _end=endPosition;
        if (_text!=null && _text.length<_end) _end=_text.length;
        return this;
    }
    public ChTokenizer setDelimiters(boolean[] charClass) {_delimiters=charClass; return this;}

    public ChTokenizer trim() {
        if (_f>=0 && _t>=0 && _text!=null) {
            _t=prevE(-SPC,_text,_t-1,_f-1)+1;
            _f=nxtE(-SPC,_text,_f,_t);
        }
        return this;
    }

    public boolean containsOnly(int charClass) {
        final int f=from(), t=to();
        if (f<0 || t-f<1) return false;
        return cntainsOnly(charClass, _text, f,t);
    }

    public boolean eq(String s) {
       final int f=from(), t=to();
       return s!=null && f>=0 && t-f==s.length() &&  strEquls(s,_text,f);
    }
    public int from(){ return _f;}
    public int to(){ return _t;}
    public boolean nextToken() {
        if (_delimiters==null) _delimiters=chrClas(SPC);
        _asStr=null;
        final byte[] T=_text;
        if (T==null || _pos<0) { _f=_t=0; return false; }
        final int end=mini(T.length, _end);
        while(_pos<end && T[_pos]>=0 && _delimiters[T[_pos]]) _pos++;
        if (_pos>=end) { _f=_t=0; return false; }
        _f=_pos++;
        while(_pos<end && !(T[_pos]>=0 && _delimiters[T[_pos]]) ) _pos++;
        _t=_pos;
        if ( _f>=0 && _f<_t && _t<=end) {
            if (T[_f]=='"' && T[_t-1]=='"') { _f++; _t--; }
            return true;
        }
        return false;
    }
    public String nextAsString() { return nextToken() ? asString() : null;}
    public String remainingAsString(boolean trim) {
        if (!nextToken()) return null;
        final byte[] T=_text;
        int f=from(), e=mini(sze(T), _end);
        if (trim) {
            f=nxtE(-SPC,T,f,e);
            e=prevE(-SPC,T, e-1,f-1)+1;
        }
        return f>0 && f<e ? bytes2strg(T,f,e) : null;
    }
    public String nextAsStringTrim() {
        if (!nextToken()) return null;
        final byte[] T=_text;
        final int F=from(), E=to();
        final int f=nxtE(-SPC,T,F,E), e=prev(-SPC,T, E-1,f-1)+1;
        return T!=null && f>=0 && f<=e && e<=_end ?  bytes2strg(T,f,e) : null;
    }
    public String asString() {
        String s=_asStr;
        if (s==null) s=_asStr=toStrg(_text, from(), to());
        return s;
    }
    public String asStringIP() { return toStrgIP(_text, from(), to()); }
    public byte[] asBytes(byte[] buffer) {
        final byte[] t=_text;
        final int f=from(), e=to();
        if (t==null || f<0 || e<f) return NO_BYTE;
        final byte[] buf=buffer!=null ? buffer : new byte[e-f];
        Arrays.fill(buf,(byte)0);
        System.arraycopy(t,f,buf,0,mini(e-f,buf.length));
        return buf;
    }
    public int asInt() { return (int)asLong();}
    public long asLong() {
        final byte[] T=_text;
        int f=nxt(-SPC,T,from(),to());
        final int t=prev(-SPC, T, to()-1, f-1)+1;
        if (f<0 || f>=t) return ChConstants.INT_NAN;
        if (T[f]=='+') f++;
        if ( f>=t || !is(DIGT_DASH, T, f) || f+1<t && !(cntainsOnly(DIGT,T, f+1, t))) return ChConstants.INT_NAN;
        return atol(T,f,t);
    }

    public double asFloat() {
        final byte[] T=_text;
        final int f=nxt(-SPC,T,from(),to()), t=prev(-SPC, T, to()-1, f-1)+1;
        return f>=0 && isFloat(T,f,t) ? atof(T,f,t) : Double.NaN;
    }

    public byte asChar() {
        final int f=from();
        final byte[] t=_text;
        return t!=null && f>=0 ? t[f] : -1;
    }

}
