package charite.christo;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;

/**
   extended choice box
   @author Christoph Gille
*/
public class ChCombo extends JComboBox implements ChRunnable, IsEnabled, HasMC, ProcessEv {
    final Map MAPCP=new HashMap();
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    private int _mc, _mcModel, _count=-1;
    private Object _run, _data;
    private long _options;
    public ChCombo(String...oo) { super(oo); ini(0); }
    public ChCombo(long options, Object data) {
        _data=data;
        _options=options;
        if (_data instanceof ComboBoxModel) setModel((ComboBoxModel)_data);
        else if (_data instanceof List) setModel(new JListModel((List)_data));
        else if (_data instanceof Object[]) setModel(new DefaultComboBoxModel((Object[])_data));
        ini(_options);
    }
    public Object data() { return _data;}
    private void ini(long options){
        setMaximumRowCount(30);
        if ( (options&ChJTable.DEFAULT_RENDERER)!=0) {
            final ChRenderer r=new ChRenderer();
            setRenderer(r);
            rtt(this);
            if ((options&(ChJTable.ICON_ROW_HEIGHT))!=0) setMinSze(ICON_HEIGHT,ICON_HEIGHT, r.label());
        }
        if ( (options&ChJTable.CLASS_RENDERER)!=0) {
            setRenderer(new ChRenderer().options(ChRenderer.CLASS_NAMES));
            rtt(this);
            _classes=true;
        }
        if ( (options&(ChJTable.ICON_ROW_HEIGHT))!=0) {
            setMinSze(ICON_HEIGHT,ICON_HEIGHT, this);
        }
        if ( (options&ChJTable.RTT)!=0) rtt(this);
        addItemListener(evAdapt(this));
        mayUpdateMC(false);
    }
    public ChCombo cln() {
        final ChCombo c=new ChCombo(_options,getModel());
        c.setRenderer(getRenderer());
        c._clas=_clas;
        c._id=_id;
        pcp(KEY_CLONED_FROM, wref(this),c);
        rtt(c);
        return c;
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private boolean _painted, _handleEvent=true;
    /* paintComponent(Graphics g) wird unter Mac nicht angesprungen !*/
    @Override public void paint(Graphics g){
        final int L=getModel().getSize();
        if (!_painted) {
            _painted=true;
            if (_classes && isProxyClassUnavailable(getSelectedItem())) {
                final ComboBoxModel m=getModel();
                final int N=sze(m);
                for(int i=0; i<N; i++) {
                    if (!isProxyClassUnavailable(get(i,m))) {
                        s(i);
                        break;
                    }
                }
            }
            selNextEnabled();
        }
        try {
            super.paint(g);
            if (L==0) {
                final String msg=gcps(KEY_IF_EMPTY,this);
                if (msg!=null)  {
                    g.clearRect(0,0,99999,999);
                    g.setColor(C(0xff));
                    g.drawString(msg,0,charA(g));
                }
            }
            final int lc=_count;
            _count=L;
            if (L>=0 && lc!=L) ChDelay.updateUI(this,3333); // !!!!!!!
        } catch(Exception e){}
        mayUpdateMC(true);
    }
    /* <<<Paint  <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    private JComponent _pan, _panHSnf;
    public JComponent panel() {
        if (_classes && _panHSnf==null)  (_panHSnf=panHS(this)).add(this,0);
        if (_pan==null) _pan=pnl(VB, _panHSnf, labToString());
        return _pan;
    }
    @Override public Dimension getMinimumSize() { try { return super.getMinimumSize();} catch(Throwable e){return dim(32,32);}}
    @Override public Dimension getMaximumSize() {
        final Dimension d=getPreferredSize();
        if (d!=null && d.width+d.height==0) return dim(0,0);
        try { return super.getMaximumSize();} catch(Throwable e){return dim(32,32);}
    }
    @Override public Dimension getPreferredSize() {
        final Dimension d=prefSze(this);
        return d!=null ? d : super.getPreferredSize();
    }
    /* <<< Layout <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    { this.enableEvents(MOUSE_EVENT_MASK|MOUSE_WHEEL_EVENT_MASK);}
    private void mayUpdateMC(boolean reval) {
        final int mc=modic(_data);
        if (_mcModel!=mc) {
            _mcModel=mc;
            if (reval) revalAndRepaintC(this);
        }
    }
    @Override public void processEvent(AWTEvent ev) {
        if (!shouldProcessEvt(ev)) return;
        final int id=ev.getID();
        try{super.processEvent(ev);}catch(Throwable ex){}
        if (id==MOUSE_ENTERED) mayUpdateMC(true);
        if (gcp(HtmlDocEditorKit.class,this)==null) selectNxtPrev(this, wheelRotation(ev));
    }
    public void processEv(AWTEvent ev) {
        final String cmd=actionCommand(ev);
        if (!_painted || !_handleEvent) return;
        if (cmd==ACTION_CUSTOMIZE_CHANGED) revalAndRepaintC(this);
        if (cmd==ACTION_SELECTION_CHANGED) {
            _mc++;
            inEDTms(thrdCR(this,"HSNF"),99);
            final ChCombo clone= (ChCombo) gcp(KEY_CLONED_FROM,this);
            final String cmd2=gcps(KEY_ACTION_COMMAND,this);
            final ActionEvent event=new ActionEvent(orO(clone, this), ActionEvent.ACTION_PERFORMED, cmd2!=null?cmd2 : ACTION_SELECTION_CHANGED, 0);
            handleEvt(clone,event);
            handleEvt(this,event);
            /* --- In Windows the selected Item is not drawn if it has the focus. --- */
            if (isWin() && getParent()!=null) getParent().requestFocus();
            runR(_run);
        }
    }
    public ChCombo li(ActionListener l) { addActLi(l,this); return this;}
    public ChCombo r(Runnable r) {_run=r; return this; }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> selected index >>> */
    public int mc() { return _mc;}
    @Override public Object run(String id, Object arg) {
        if (id=="s") s(arg);
        if (id=="HSNF") {
            setLabToString();
            storeInProperties();
            revalAndRepaintC(_panHSnf);
            updateAllNow(CHANGED_COMBOBOX|UPDATE_DO_REPAINT);
        }
        return null;
    }
    public ChCombo s(int i) {
        if (i>=0 && i<sze(getModel())) {
            _handleEvent=false;
            super.setSelectedIndex(i);
            _handleEvent=true;
        }
        return this;
    }
    protected void selNextEnabled() {
        final Object o=getSelectedItem();
        if (o==null || !isEnabled(o)) {
            final ComboBoxModel m=getModel();
            for(int i=sze(m); --i>=0;) {
                final Object item=m.getElementAt(i);
                if (isEnabled(item)) {
                    s(item);
                    break;
                }
            }
        }
    }
    public ChCombo s(Object o) {
        if (o==null) return null;
        if (!isEDT()) inEdtLaterCR(this,"s",o);
        else {
            final ComboBoxModel m=getModel();
            final String cn=o instanceof Class ? nam(o) : o instanceof String ? (String)o : null;
            for(int i=sze(m); --i>=0;) {
                final Object item=m.getElementAt(i);
                if (item instanceof charite.christo.hotswap.ProxyClass && o==((charite.christo.hotswap.ProxyClass)item).getClassInstance() ||
                    item instanceof Class && nam(item).equals(cn) ||
                    eq(cn, item) || o==item)  {
                    if (o!=m.getSelectedItem()) {
                        m.setSelectedItem(item);
                        _mc++;
                        if (_painted) repaint();
                    }
                    return this;
                }
            }
        }
        return this;
    }
    public int i() { return getSelectedIndex();}
    @Override public void setSelectedItem(Object o) {
        if (o!=getSelectedItem()) {
            _mc++;
            super.setSelectedItem(o);
        }
    }
    /* <<<  selected index <<< */
    /* ---------------------------------------- */
    /* >>> save >>> */
    private Class _clas;
    private String _id;
    public ChCombo save(Class clas,String id_str) {
        _clas=clas;
        _id="combobox_"+id_str;
        final int savedIdx=getPrpty(clas, _id,-1);
        if (savedIdx>=0 && sze(getModel())>savedIdx) s(savedIdx);
        return this;
    }
    private void storeInProperties() {
        if (_clas!=null && _id!=null) setPrpty(_clas,_id, i());
    }
    /* <<< save <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    public ChCombo tt(Object o) { pcp(KEY_TOOLTIP,o,this); rtt(this); return this; }
    private boolean _classes;
    private Class _interface;
    public boolean containsClasses() { return _classes;}
    public ChCombo classRenderer(Class anInterface) {
        _interface=anInterface;
        _classes=true;
        setRenderer(new ChRenderer().options(ChRenderer.CLASS_NAMES).setEnabled(this));
        rtt(this);
        return this;
    }
    private ScrollLabel labToString;
    @Override public String toString() { return toStrgN(getSelectedItem()); }
    private void setLabToString() {
        if (labToString!=null) {
            String s=dItem(getSelectedItem());
            final Object inst=instanceOfSelectedClass(null);
            if (s==null) s=toStrg(inst);
            s=rplcToStrg("\n"," ",s);
            s=rplcToStrg("<br>"," ",s);
            if (s==null) s="";
            s=rmMnemon(s);
            final String txt=s.indexOf('@')>0 && s.startsWith(nam(inst.getClass())) ?  "" : s;
            final boolean license=chrAt(0,s)=='$';
            labToString.t(license ? "Read the license and conditions of use. "+txt : txt);
            if (license) setBG(0xFF,labToString);
            setFG(license?0xffFFff :0,  labToString);
        }
    }
    private JComponent labToString() {
        if (labToString==null) labToString=new ScrollLabel("");
        setLabToString();
        return labToString;
    }
    @Override public String getToolTipText(){
        ChCombo combo=(ChCombo)gcpa(KEY_CLONED_FROM,this);
        if (combo==null) combo=this;
        final Object tt=gcpa(KEY_TOOLTIP,combo);
        final int i=i();
        if (tt instanceof CharSequence) return addHtmlTagsAsStrg(tt);
        if (tt instanceof CharSequence[] && i>=0 && i<sze(tt)) return addHtmlTagsAsStrg(get(i,tt));
        if (_classes) return dTip(instanceOfSelectedClass(null));
        return super.getToolTipText();
    }
    /* <<< Renderer  <<< */
    /* ---------------------------------------- */
    /* >>> isEnabled >>> */
    public boolean isEnabled(Object o) {
        final IsEnabled ie=gcp(KEY_ENABLED, this, IsEnabled.class);
        return ie!=null ? ie.isEnabled(o) : _interface!=null ? isAssignblFrm(_interface,o) : true;
    }
    /* <<< isEnabled <<< */
    /* ---------------------------------------- */
      /* >>> Choice of Class  >>> */
    private Map _mapSharedInst;
    public <T extends Object>T instanceOfSelectedClass(Class<T> clazz) {
        if (_mapSharedInst==null) _mapSharedInst=new HashMap();
        final Object selected=getSelectedItem();
        Object inst=_mapSharedInst.get(selected);
        if (inst==null) _mapSharedInst.put(selected, inst=mkInstance(selected));
        return clazz==null || isAssignblFrm(clazz,inst) ? (T)inst : null;
    }
    /* <<< Choice of Class <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    public final static Object selItem(Object o) {
        return
            o instanceof ChCombo ? ((ChCombo)o).getSelectedItem() :
            o;
    }
}
