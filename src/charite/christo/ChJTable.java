package charite.christo;
import java.awt.*;
import javax.swing.*;
import java.util.List;
import java.util.*;
import javax.swing.table.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**
   @author Christoph Gille
*/
public class ChJTable extends JTable implements ProcessEv, Comparator{
    public final static String COLUMN_MOVED="ChJTable_COLUMN_MOVED", COLUMN_MARGIN_CHANGED="ChJTable_COLUMN_MARGIN_CHANGED", KEY_FILE_EXT_DND="JT$$PK";
    public final static int
        DEFAULT_RENDERER=1<<0, ICON_ROW_HEIGHT=1<<1, EX_ROW_HEIGHT=1<<2, CLASS_RENDERER=1<<3, RTT=1<<4, FILE_POPUP=1<<5,
        EXX_ROW_HEIGHT=1<<6, TABLE_ROW_SORTER=1<<7,
        FILE_TRANSFER_HANDLER=1<<10, SINGLE_SELECTION=1<<11,  SET_VALUE_SELECTED_ROWS=1<<12,
        DRAG_ENABLED=1<<13, NO_REORDER=1<<14, CHJTABLE_FOCUSED_BLACK=1<<15, CHJTABLE_SHOW_BUT_MAXIM=1<<16,
        CHJTABLE_BACKSPACE_DEACTIVATED=1<<17, CHJTABLE_CLEAR=1<<18, BACKSPACE_DEL_ITEM=1<<20,
        COLUMN_EXTRA_WIDTH=8, COLUMN_WIDTH_4_DIGITS=5*EX;
    private String _headerTT[];
    private boolean _initialized;
    private final long _opt;
    private Object _dragRowSrc;
    private int _dragRowCols[], _mouseHeaderCol=-1;
    public long getOptions() { return _opt;}
    public ChJTable(TableModel m, long options) {
        _opt=options;
        if (m!=null) {
            setModel(m);
            if ((TABLE_ROW_SORTER&options)!=0) setRowSorter(null);
            if (m instanceof ChTableModel) ((ChTableModel)m).setJTable(this);
        }
        if ((DEFAULT_RENDERER&options)!=0) setDefaultEditor(Object.class, (ChRenderer)(_renderer=new ChRenderer()));
        if ((ICON_ROW_HEIGHT&options)!=0) setRowHeight(32);
        if ((EX_ROW_HEIGHT&options)!=0) setRowHeight(EX);
        if ((EXX_ROW_HEIGHT&options)!=0) setRowHeight(EX*3/2);
        evAdapt(this).addTo("M",getTableHeader());
        if ( (options&NO_REORDER)!=0) getTableHeader().setReorderingAllowed(false);
        adSelListener(this);
        setShowHorizontalLines(true);
        setShowVerticalLines(true);
        setBG(DEFAULT_BACKGROUND, this);
        setGridColor(C(0));
        setIntercellSpacing(dim(1,1));
        scrollByWheel(this);
    }

     @Override public void columnMoved(javax.swing.event.TableColumnModelEvent ev) {
         super.columnMoved(ev);
         handleActEvt(this,COLUMN_MOVED,0);
     }
    @Override public void columnMarginChanged(javax.swing.event.ChangeEvent ev) {
        super.columnMarginChanged(ev);
        handleActEvt(this,COLUMN_MARGIN_CHANGED,0);
    }
    { pcp(KOPT_TRACKS_VIEWPORT_WIDTH,"",this);}
    @Override public boolean getScrollableTracksViewportWidth() {  return gcp(KOPT_TRACKS_VIEWPORT_WIDTH,this)!=null; }

    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> JTable */
    {pcp(KEY_NOT_YET_PAINTED,"",this);}
    @Override public void paintComponent(Graphics g) {

        if (false || paintHooks(this, g, false)) {
            try{
                super.paintComponent(g);
                if (!_initialized) {
                    _initialized=true;
                    noBrdr(getSP(this));
                    pcp(KEY_NOT_YET_PAINTED,null,this);
                }
            } catch(Throwable e){}
        }
        paintHooks(this, g, true);
    }

    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    private TableCellRenderer _renderer;
    public ChJTable renderer(TableCellRenderer r) { _renderer=r; return this;}
    public ChRenderer getChRenderer() { return deref( _renderer, ChRenderer.class); }
    public ChJTable headerTip(String...tt) {
        final int N=sze(tt);
        _headerTT=new String[N];
        for(int i=N;--i>=0;) _headerTT[i]=addHtmlTagsAsStrg(tt[i]);
        return this;
    }
    @Override public TableCellRenderer getCellRenderer(int row, int column) {
        return (_renderer!=null) ? _renderer : super.getCellRenderer(row,column);
    }
    /* <<< Renderer <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent  >>> */
    { this.enableEvents(ENABLE_EVT_MASK);}
    @Override public void processEvent(AWTEvent ev) {
        if (!shouldProcessEvt(ev)) return;
        final int id=ev.getID();
        final Object q=ev.getSource();
        final AWTEvent ev2=pEv(ev);/*EEEE*/
        if (ev2!=null) {
            try {
                super.processEvent(ev2);
            } catch(Throwable e){}
        }
        if (id==MOUSE_DRAGGED && q==this && (_dragRowSrc!=null || _dragRowCols!=null)) {
            final int row=rowAtPoint(point(ev)), col=columnAtPoint(point(ev)), nC=getColumnCount();
            if (col>=0 && row>=0 && (_dragRowCols==null || idxOf(col, _dragRowCols)>=0)) {
                if (isEditing()) getCellEditor().stopCellEditing();
                final int[]
                    selRows=getSelectedRows(),
                    rows=sze(selRows)>=0 && idxOf(row, selRows)>=0 ? selRows : new int[]{row};
                final BA sb=new BA(99);
                final Object srcID=gcp(KEY_DRAG_SRC_ID,this);
                for(int r : rows) {
                  for(int c=0; c<nC; c++) sb.a(getValueAt(r,c)).a('\t');
                  if (srcID!=null) sb.a(srcID); else sb.a(hashCode());
                  sb.a('\t').a(r).a('\n');
                }
                if (sze(sb)>0) {
                    final String fileExt=gcps(KEY_FILE_EXT_DND,this);
                    if (fileExt!=null) {
                        final java.io.File f=file(dirTmp(),"tableRow."+fileExt);
                        wrte(f,sb);
                        dndV(true,this).add(f);
                    } else dndV(true,this).add(toStrg(sb));
                    exportDrg(this, ev, TransferHandler.COPY);
                }
            }
        }
    }

    public void processEv(AWTEvent ev) { pEv(ev); }
    public AWTEvent pEv(AWTEvent ev) {
      final int id=ev.getID();
        final Point p=point(ev);
        final Object q=ev.getSource();
        final JTableHeader h=deref(q,JTableHeader.class);
        if (p!=null && h!=null) {
            final int col=columnAtPoint(p);
            if (_mouseHeaderCol!=col) {
                _mouseHeaderCol=col;
                setTip(get(col,_headerTT), h);
            }
        }
        if (q==this && (id==MOUSE_CLICKED||id==MOUSE_PRESSED)) {
            boolean clearSelection=false;
            final int row=rowAtPoint(p), col=columnAtPoint(p);
            if (col>=0 && row>=0 && isCellEditable(row,col)) { /* Workaround: under windows difficult to get the cursor into a cell */
                editCellAt(row,col);
                final TableCellEditor tce=getCellEditor(row,col);
                final Component comp=tce!=null ? tce.getTableCellEditorComponent(this, getValueAt(row,col),false, row, col) : null;
                final ChTextField tf=deref(comp, ChTextField.class);
                final AbstractButton but=deref(comp, AbstractButton.class);
                if (tf!=null) {
                    final Rectangle r=getCellRect(row,col,false);
                    if (r!=null) tf.processEvent(new java.awt.event.MouseEvent(comp, id,0,0, x(p)-x(r), y(p)-y(r), 1,false));
                    tf.requestFocus();
                }
                if (but!=null && id==MOUSE_PRESSED && 0==(modifrs(ev)&(CTRL_MASK|SHIFT_MASK|ALT_MASK)) && buttn(ev)==1) but.doClick();
            } else if (id==MOUSE_PRESSED) {
                final int[] sel=getSelectedRows();
                if (sze(sel)==1 && sel[0]==row) clearSelection=true;
                if (clearSelection) { clearSelection(); return null;}
            }
        }
        return ev;
    }

    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */

    public void enableDragRows(Object src, int[] fromCols) {
        _dragRowSrc=wref(src);
        _dragRowCols=fromCols;
    }
    public Object getDndDateien() { return dndV(false,this);}
    /* <<< DnD <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    @Override public String getToolTipText(java.awt.event.MouseEvent ev) {
        return ballonMsg(ev, super.getToolTipText(ev));
    }

    public TableColumn column(int col) {
        if (col<0 || col>=getColumnCount()) return null;
        try { return getColumnModel().getColumn(col); } catch(Exception e) {return null;}
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    public void setColumnWidthC(boolean resizable, int col, Object comp) {
        if (comp!=null) {
            if (!(comp instanceof JComponent || comp instanceof Object[])) assrt();
            setColWidth(resizable, col, wdth(getIntercellSpacing())+prefW(comp)+COLUMN_EXTRA_WIDTH);
        }
    }

    public ChJTable setW(int pref, int min, int max, int col) {
        final TableColumn c=column(col);
        if (c!=null) {
        c.setPreferredWidth(pref);
        c.setMinWidth(min==-1?pref:min);
        c.setMaxWidth(max==-1?pref:max);
        }
        return this;
    }
    public void setColWidth(boolean resizable, int col, int w) {
        final TableColumn c=column(col);
        if (c==null) return;
        if (w>=0) {
            c.setPreferredWidth(w);
            if (!resizable) { c.setMaxWidth(w); c.setMinWidth(w); }
        }
    }

    /* <<< Layout <<< */
    /* ---------------------------------------- */
    /* >>> Row sorter >>> */
    public int compare(Object o1, Object o2) {
        if (o1==o2) return 0;
        if (o1 instanceof Boolean && o2 instanceof Boolean) return isTrue(o1) ? -1 : 1;
        if (o1 instanceof Object[] && get(0,o1)==ChRenderer.KEY_TOGGLE_SELECTED && o2 instanceof Object[] && get(0,o2)==ChRenderer.KEY_TOGGLE_SELECTED) {
            final Color c1=get(1,o1,Color.class);
            final Color c2=get(1,o2,Color.class);
            if (c1!=null && c2!=null) {
                final int rgb1=c1.getRGB(),rgb2=c2.getRGB();
                return rgb1<rgb2?-1 : rgb1==rgb2?0 : 1;
            }
        }
        if (o1 instanceof Comparable)  return ((Comparable)o1).compareTo(o2);
        return 0;
    }
    public static void setRowSorter(Comparator cc[], JTable t) {
        if (javaVsn()>15) {
            forJavaVersion(16).run(RunIf16orHigher.RUN_setRowSorter,new Object[]{t,cc});
        }
    }
   public static int[] selectedRowsConverted(JTable t) {
        final int[] rr0=t!=null ? t.getSelectedRows() : null, rr;
        if (rr0==null) return NO_INT;

        if (javaVsn()>15) {
            rr=rr0.clone();
            for(int i=rr.length; --i>=0;) rr[i]=rowIndexToModel(rr[i],t);
        } else rr=rr0;
        return rr;
    }

    private final static int[] rowIndexRet={0};
    private final static Object[] rowIndexPar={null,rowIndexRet};
    public static int rowIndexToModel(int row, JTable t) {
        if (t instanceof ChJTable && javaVsn()>15) {
            rowIndexRet[0]=row;
            rowIndexPar[0]=t;
            forJavaVersion(16).run(RunIf16orHigher.RUN_convertI2M,rowIndexPar);
            return rowIndexRet[0];
        }
        return row;
    }
}
