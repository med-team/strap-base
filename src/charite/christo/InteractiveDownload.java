package charite.christo;
import java.awt.*;
import java.io.File;
import java.net.URL;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP
<h2>Proxy Server</h2>
<i>INCLUDE_DOC:Web</i>

<h2>Privacy</h2>

If you work with sensitive data you might  deactivate the check-boxes
<i>CB:ChUtils#buttn(TOG_NASK_UPLOAD)</i> or <i>CB:ChUtils#buttn(TOG_NASK_DOWNLOAD)</i>.
Each time, data is uploaded to a remote server or downloaded from the internet, a confirmation dialog will appear.

@author Christoph Gille

*/
public class InteractiveDownload implements ChRunnable {
    private final static String
        SUFFICES[]={"","TMP",".gzTMP",".ZTMP",".zipTMP",".bz2TMP"},
        DL_ADL_M_C[]={"Download","More info","Cancel"},
        NEEDS_DL="The program needs to download some file[s] in order to proceed";
    private static JComponent _butLogDL;
    private final static Object SYNC_IaW=new Object(), SYNC=new Object();
    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="WATCH") {
            final File[] FILES=(File[])argv[0];
            final ChTextView taDownloaded=(ChTextView)argv[1];
            final boolean[] isEnabled=(boolean[])argv[2];
            final BA sbFileSize=new BA(99);
            while(isEnabled[0]) {
                sleep(222);
                sbFileSize.clr();
                for(File f0:FILES) {
                    if (f0==null) continue;
                    boolean exists=false;
                    for(String suffix : SUFFICES) {
                        for(int noPackGz=2; --noPackGz>=0;) {
                            if (noPackGz!=0 && !endWith(".jar.pack.gz", f0)) continue;
                            final File f1=file(new BA(99).a(f0).a(suffix)), f=noPackGz!=0 ? withoutPackGz(f1) : f1;
                            if (f.exists()) {
                                sbFileSize.a(' ',2).a(f.getName()).format10(sze(f),11).a(' ',15).a('\n');
                                exists=true;
                            }
                        }
                    }
                    if (!exists) sbFileSize.a(' ').a(f0.getName()).aln(": not yet");
                }
                taDownloaded.t(sbFileSize);
                revalAndRepaintC(taDownloaded);
            }
        } else assrt();
        return null;
    }

    private static File withoutPackGz(File f) {
        return !endWith(".pack.gz",f) ? f : file(delSfx(".pack.gz",toStrg(f)));
    }

    public static boolean downloadFiles(URL urls[],File files[]) {return _dlEDT(urls,files,null);}
    public static boolean downloadFilesAndUnzip(URL urls[], File directory) { return _dlEDT(urls,null,directory); }

    public static boolean _dlEDT(URL urls[], File files[], File directory) {
       final boolean isGui=withGui();
       if (isGui && !isEDT()) {
           final Object[] retValue={null};
           synchronized(SYNC_IaW) {
               inEDT(thrdMR("_dlEDT",InteractiveDownload.class, new Object[]{urls,files,directory}, retValue));
           }
           return isTrue(retValue[0]);
       }
        final File[] vFILE=new File[urls.length];
        final URL[]  vURL=new URL[urls.length], srcParents=new URL[urls.length];
        final String[] destParents=new String[urls.length];
        final BA log=log(), baSrc=isGui?new BA(999):null, baDst=isGui?new BA(222):null;
        final ChTextView taSrc=isGui?new ChTextView(baSrc):null, taDst=isGui?new ChTextView(baDst):null;
        boolean isPlugin=false, useEdtFTP=false;
        int count=0;
        for(int i=0; i<urls.length; i++){
            final URL url=urls[i];
            if (url==null) continue;
            final String
                sUrl=toStrg(url),
                fName=delLstCmpnt(sUrl.substring(sUrl.lastIndexOf('/')+1),'?'),
                fNameNoCompress;
            isPlugin|= sUrl.endsWith("?plugins");
            useEdtFTP=useEdtFTP || useEdtFTP(sUrl);
            if (!isPlugin && fName.endsWith(".pack.gz")) {
                if (sze(file(directory, delSfx(".pack.gz",fName)))>0) continue;
                fNameNoCompress=fName;
            } else fNameNoCompress=delSfx(".Z",delSfx(".bz2",delSfx(".gz",fName)));

            final File file=files!=null ? files[i] : directory!=null ? file(directory, fNameNoCompress) : null;
            if (file==null || (!isPlugin && sze(file)>0)) continue;
            destParents[count]=toStrg(mkParentDrs(file));
            vFILE[count]=file;
            vURL[count]=url;
            if (isGui) {
                baSrc.aln(url).send();
                baDst.aln(fPathUnix(file)).send();
            }
            srcParents[count]=url(delLstCmpnt(delSfx('/',url),'/'));
            count++;
        }
        if (count==0) return true;
        if (useEdtFTP) new AnonymousFTPPROXY().proxyObject();
        if (directory==dirPlugins()) {
            final BA sb=new BA(999).a("Going to download the following plugin data into ").a(directory).a('\n',2);
            for(URL u:vURL) { sb.a(' ',2).aln(u); currVers(toStrg(u)); }
            sb.aln("\n "+RED_WARNING+" Plugins can potentially contain WIKI:Malware.\n\n"+
                 "Do not download, unless you fully trust the plugin.\n"+
                 "You may want to protect yourself in a sandbox environment (See "+URL_STRAP+"security.html).\n\n"+
                 "Trusted URLs are highlighted in green");
            final ChTextView ta=new ChTextView(sb);
            for(String needle:TRUSTED_PLUGINS) ta.tools().highlightOccurrence(needle, null,null, 0, C(0xaaffaa));
            if (ChMsg.optionSync(SYNC, sb,new String[]{"I trust the plugin","I do not want to load the plugin"})!=0) return false;
        }

        final BA sb=new BA(333);
        for(int i=2; --i>=0;) {
            Object last=null;
            sb.a("Download ").aln(i==0 ? "to:" : "from:");
            for(Object p: i==0 ? destParents : srcParents)
                if (!eqNz(p,last)) sb.a(' ').aln(last=p);
        }
        Object pSouth=null;
        if (isGui) {
            final ChButton togAsk=buttn(TOG_NASK_DOWNLOAD);
            pSouth=monospc(sb);
            final Object
                pCenter=pnl(VBHB, "Loading the following files:", pnl(HBL,taSrc," ==> ",taDst),null),
                msg1=pnl(CNSEW,scrllpn(0,pCenter, dim(666,222)), togAsk.cb(),pSouth);
            final String DC[]={"Download","Cancel"};
            if (isPlugin && ChMsg.optionSync(SYNC, msg1,DC)!=0) return false;

            if (!togAsk.s()) {
                addTab(null, -1);
                final int option=ChMsg.optionSync(SYNC, pnl(VBHB,NEEDS_DL,togAsk.cb()) ,DL_ADL_M_C);
                if (option==2 || option==1 && ChMsg.optionSync(SYNC, msg1,DC)!=0) return false;
            }
        }
        final ThreadDownload thread=new ThreadDownload(vFILE,vURL);
        if (isGui) {
            startThrd(thread);
            final ChTextView taDownloaded=new ChTextView("");
            final String wget=isMac() ? "curl -O  " : "wget -N ";
            if (log!=null) log.a("# If download fails you can try ").aln(wget);
            for(int i=0; i<vURL.length; i++) {
                if (vURL[i]!=null) log.a(wget).a(vURL[i]).a(" -O '").a(vFILE[i]).aln("'");
            }
            thread._msgDL=pnl(VBHB, thread.pNorth, " ", pSouth, " ", monospc("Downloaded bytes:"), pnl(HB,taDownloaded,"#", ChButton.doView(log()).t("Log")));

            taDownloaded.setPreferredSize(dim(1,count*(EX+4)));
            final boolean isRunning[]={true};
            startThrd(thrdCR(new InteractiveDownload(),"WATCH",new Object[]{vFILE,taDownloaded.a("\n"),isRunning}));
            addTab(null, -1);
            ChMsg.optionSync(SYNC, thread._msgDL,new String[]{"Abort download"});
            isRunning[0]=false;
        } else thread.run();
        final boolean success=thread.finished && thread.success;
        thread.interrupted=true;
        return success;
    }

    private static BA _log;
    public static BA log() {
        if (_log==null) {
            _log=new BA(99);
            if (withGui()) _log.sendToLogger(ChLogger.NO_LINE_NUMBERS, "Log interactive downloads",IC_DOWNLOAD, 50*1000);
            else _log.setSendTo(BA.SEND_TO_STDOUT);
        }
        return _log;
    }
    private static class ThreadDownload extends Thread implements ChRunnable,IsEnabled {
        @Override public void interrupt() { success=false;  super.interrupt();}
        boolean success=true, interrupted, finished;
        final URL[] urls;
        final File files[];
        JComponent _msgDL;
        final JComponent pNorth=!withGui()?null:pnl();
        ThreadDownload(File[] vF, URL[] vU) {
            urls=vU;
            files=vF;
            if (pNorth!=null) pNorth.add(new JLabel("loading ... "));
        }
        public boolean isEnabled(Object o){return !interrupted;}
        @Override public void run() {
            final BA log=log();
            for(int i=0; i<urls.length;i++) {
                final File f=files[i], fNoPackGz=withoutPackGz(f);
                final URL url=urls[i];
                if (f==null||url==null || sze(f)+sze(fNoPackGz)>0) continue;
                delFile(f);
                final String sUrl=toStrg(url), urlNoPackGz=delSfx(".pack.gz",sUrl);
                if (sze(f)+sze(fNoPackGz)==0) downloadAndDecompress(sUrl,f,log,((IsEnabled)this));
                if (sze(f)+sze(fNoPackGz)==0 && sUrl!=urlNoPackGz) downloadAndDecompress(urlNoPackGz,fNoPackGz,log,((IsEnabled)this));
                log.a("Download: ").a(f);
                if (sze(f)+sze(fNoPackGz)==0) {
                    log.aln(RED_ERROR).send();
                    success=false;
                } else {
                    if (endWith(STRSTR_IC, ".exe", f)) {
                        log.a(" Make executable");
                        mkExecutabl(f);
                    }
                    log.aln(GREEN_SUCCESS).send();
                }
            }
            finished=true;
            if (withGui()) inEdtLaterCR(this,"AFTER_DOWNLOAD",null);
        }
        public final Object run(String id, Object arg) {
            if (id=="AFTER_DOWNLOAD") {
                if (pNorth!=null) {pNorth.removeAll();
                    if (success) pNorth.add(pnl(C(0x00b200),"download succeeded"));
                    else pNorth.add((pnl(C(0xFF0000),"download failed",smallHelpBut(InteractiveDownload.class))));
                    final Window w=parentWndw(_msgDL);
                    if (w instanceof JDialog) w.dispose();
                    addTab(_msgDL, success?1:-1);
                }
            }
            return null;
        }
    }
    public static void addTab(Object c, int success) {
        if (_frame==null)  {
            final String t="Downloads and installations";
            _frame=new ChFrame(t).ad(_tabbed=new ChTabPane(ChTabPane.LEFT)).size(555,222);
            toLogMenu(_butLogDL=ChButton.doView(_frame).i(IC_DOWNLOAD), t,  null);
        }
        if (c==null)  { _frame.setVisible(false); return; }
        final ChJScrollPane sp=scrllpn(c);
        TabItemTipIcon.set(toStrg(++_iDownload),null,null,null,sp);
        TabItemTipIcon.set(null,null,null,success==1 ? IC_HAPPY: success==-1?IC_UNHAPPY : IC_UNSURE, sp);
        _tabbed.addTab(0, sp);
        ChDelay.highlightButton(buttn(BUT_LOG),4444);
        ChDelay.highlightButton(_butLogDL,9999);
        if (success>0) _frame.setExtendedState(JFrame.ICONIFIED);
        else _frame.shw();

    }
    private static ChTabPane _tabbed;
    private static ChFrame _frame;
    private static int _iDownload;

    public static File downloadFileIfNewer(Object urls) {
        return urls==null?null : get(0,downloadFilesIfNewer(oo(urls)), File.class);
    }

    public static File[] downloadFilesIfNewer(Object urls[]) {
        final URL uu[]=new URL[urls.length];
        final File ff[]=new File[urls.length];
        int count=0;
        for(int i=urls.length; --i>=0;) {
            if (urls[i]==null || urls[i] instanceof File || urls[i] instanceof String && !looks(LIKE_EXTURL,urls[i])) continue;
            final URL u=url(urls[i]);
            if (u==null) continue;
            ff[i]=file(u);
            if (ff[i]==null) continue;
            if (isUpToDate(ff[i],u)==0) {
                if (sze(ff[i])>0) {
                    delFile(ff[i]);
                    puts(new BA(99).a("downloadFilesIfNewer: Going to ").a("delete ").a(ff[i]).aln(sze(ff[i])>0?GREEN_SUCCESS:" failed"));
                }
                uu[i]=u;
                count++;
            }
        }
        if (count>0) {
            puts(new BA(99).a(ANSI_FG_GREEN).a('>',10).aln(ANSI_RESET).a("downloadFilesIfNewer: Going to ").a("download "));
            puts(uu);
            downloadFiles(uu,ff);
            puts(GREEN_DONE);
        }
        return ff;
    }
    public final static Object docuAskDownload() {
        return pnl(VBPNL,
                   NEEDS_DL,
                   pnl(new ChButton(DL_ADL_M_C[0]),new ChButton(DL_ADL_M_C[1]),new ChButton(DL_ADL_M_C[2]))
                   );
    }
}

