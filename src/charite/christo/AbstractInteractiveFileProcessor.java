package charite.christo;
import java.io.File;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public abstract class AbstractInteractiveFileProcessor implements java.awt.event.ActionListener  {
    private final java.util.Vector vector=new java.util.Vector();
    private ChJList _jList;
    private ChJList jList() {
        if (_jList==null) _jList=new ChJList(vector,0L);
        return _jList;
    }
    private ChFrame frame;
    private File[] ffCreated={};
    public File[] getCreatedFiles() { return ffCreated;}
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev==null ? "A":ev.getActionCommand();
        if (cmd=="APPLY" || cmd=="A") {
            final Object oo[]=cmd=="APPLY" ? jList().getSelectedValues() : vector.toArray();
            if (sze(oo)==0) error("You must select files");
            else {
                for(Object o: oo) {
                    final String s=o.toString();
                    final File f=file(delFromChar(s,' '));
                    if (sze(f)==0 && myComputer()) putln("error  in AbstractInteractiveFileProcessor: f=null");
                    ffCreated=processFile(null,f,true);
                }
                handleActEvt(this,"CREATED",0);
                new ChFrame(shrtClasNam(this)).ad(scrllpn(new ChJList(ffCreated, ChJList.OPTIONS_FILES))).shw();
            }
        }
    }

    public void interactiveProcess(File ...ff) {
        vector.clear();
        for(File f:ff){
            if (f==null) continue;
            final BA ba=readBytes(f);
            final Object fileEntry=fileEntry(ba,f);
            if (fileEntry!=null) vector.add(fileEntry);
        }
        if (sze(vector)==0) error(errorNonSelected);
        else if (sze(vector)==1) actionPerformed(null);
        else {
            if (frame==null) {
                final Object
                    butHelp=hasHlp(getClass()) ? smallHelpBut(getClass()) : null,
                    butApply=new ChButton("APPLY").t("Process selected files").li(this);
                frame=new ChFrame("").ad(pnl(CNSEW, scrllpn(jList()),textNorth,pnl(butApply,butHelp)));
            }
            jList().repaint();
            jList().setSelectedIndex(0);
            frame.shw(0);
        }
    }

    public abstract String fileEntry(BA byteArray,File f);
    public void setTextNorth(String s) { textNorth=s;}
    private String textNorth,errorNonSelected;
    public void setErrorNonSelected(String s) { errorNonSelected=s;}
    public abstract File[] processFile(BA byteArray,File file,boolean overwrite);
}
