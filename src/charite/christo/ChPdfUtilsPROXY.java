package charite.christo;
import java.io.*;
import static charite.christo.ChUtils.*;
/**HELP
<b>Home:</b> http://pdfbox.apache.org/ <br>
<b>License of JPdf:</b> Apache License v2.0
*/
public class ChPdfUtilsPROXY extends AbstractProxy {
    @Override public String getRequiredJars() {
        return "checkstyle-all-4.2.jar  FontBox-0.1.0.jar PDFBox-0.7.3.jar bcprov-jdk15-140.jar";
    }
    public final static String STRIP_TEXT="TXT";
    public void pdf2txt(File fPdf,File fTxt) {
        final ChRunnable r=(ChRunnable)proxyObject();
        if (r!=null) r.run(STRIP_TEXT,new Object[]{fPdf,fTxt});
    }

}


