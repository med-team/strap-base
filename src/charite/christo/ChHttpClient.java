package charite.christo;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.net.*;
import org.apache.commons.httpclient.ProxyClient;

public class ChHttpClient implements ChRunnable {
    final static String RUN_newSocket="newSocket";
    public final Object run(String id, Object arg) {
        if (id==RUN_newSocket) {
            try {
                final String host=(String)arg;
                final ProxyClient proxyclient=new ProxyClient();
                proxyclient.getHostConfiguration().setHost(host);
                final Object proxy= proxyForUrl(url("http://"+host));
                putln("ChHttpClient host="+host+" proxy="+proxy);
                if (proxy==null) return null;

                    final InetSocketAddress sa=(InetSocketAddress)((Proxy)proxy).address();
                    proxyclient.getHostConfiguration().setProxy(sa.getHostName(), sa.getPort());
                    final ProxyClient.ConnectResponse response=proxyclient.connect();
                    return response.getSocket();

            } catch(java.io.IOException iox) { putln(RED_CAUGHT_IN+" ChHttpClient ",iox); }
        }
        return null;
    }

}
