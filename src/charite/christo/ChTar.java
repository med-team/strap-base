package charite.christo;
import java.io.*;
import static charite.christo.ChUtils.*;
import com.ice.tar.TarArchive;
public class ChTar implements ChRunnable {

    public final static String EXTRACT="E";

    public Object run(String id, Object arg) {
        if (id==EXTRACT) {
            final Object[] argv=(Object[])arg;
            final int opt=atoi(argv[0]);
            final String fTar=(String)argv[1];
            final File dir=(File)argv[2];
            mkdrsErr(dir);
            try{

                new TarArchive(inStream(fTar,STREAM_UNZIP)).extractContents(dir);
                return id;
            } catch(Exception e) {
                ChZip.errorMsgExtract(opt, e, fTar, dir);
            }
        }
        return null;
    }
}
