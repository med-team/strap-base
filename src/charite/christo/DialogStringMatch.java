package charite.christo;
import java.util.regex.Pattern;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   Items in graphical list components can be selected using string pattern.
   See WIKI:Regular_expression.
   @author Christoph Gille
*/
public class DialogStringMatch implements java.awt.event.ActionListener, HasPanel {
    public final static String
        KEY_OPTIONS="DSM$$O",
        KEY_SAVE="DSM$$S",
        PROVIDE_HAYSTACK="DSM$$PH", PROVIDE_STRINGS="DSM$$PS",
        ACTION_FINISHED="DSM$$F", ACTION_BEFORE="DSM$$B",
        METHODS[]={"Contains string","Starts with","Ends with","Regul expr"};
    public final static int NEVER_FILE_CONTENT=1<<0, FROM_RENDERER_COMPONENT=1<<2;
    private final static Object[] BA={null};
    private final long __opts;
    private JComponent __vB, __panFile, __panel;
    private Object __components[], __haystacks, __tfSize, __labCount, __butOr, __butAnd;
    private ChFrame __frame;
    private Collection __vMatching;
    private String __fileName;
    private List<DialogStringMatch> __vRows;

    public DialogStringMatch(long options, String fileName) {
        __opts=options;
        __fileName=fileName;
    }

    public DialogStringMatch(long options, Object haystacks, Collection vMatching, String fileName) {
        __haystacks=haystacks;
        __vMatching=vMatching;
        __opts=options;
        __fileName=fileName;
    }
    public DialogStringMatch setListComponent(Object[] c) {
        __components=c;
        return this;
    }

    /* <<< Parent Instance <<< */
    /* ---------------------------------------- */
    private void enableDisable(boolean mayAddRow) {
        int countTxt=0, countEmpty=0, countF=0;
        final int R=sze(__vRows);
        if (R==0) new DialogStringMatch(this);
        for(int i=0; i<R; i++) {
            final DialogStringMatch  m=__vRows.get(i);
            final boolean empty= sze(m._tf.getText())==0;
            if (!empty) countTxt++; else countEmpty++;
            setEnabld(!empty, m._butSelect);
            if (m._cbFile.s()) countF++;

        }
        setEnabld(countTxt>1, __butOr);
        setEnabld(countTxt>1, __butAnd);
        setEnabld(countF>0,   __tfSize);
        setEnabld(countF>0,   __panFile);
        if (mayAddRow && countEmpty==0) {
            new DialogStringMatch(this);
            if (__frame!=null) __frame.size(prefW(__panel)+32, prefH(__panel)+32);
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> selectElements >>> */
    private static int selectJListElements(Component c, DialogStringMatch isEnabled) {
        int count=0;
        final long options=isEnabled.__opts;
        if (c instanceof ChJList) {
            final ChJList l=(ChJList)c;
            final ListModel m=l.getModel();
            final int N=m.getSize();
            final int iiOld[]= l.getSelectedIndices();
            final int ii[]=chSze(iiOld, N+iiOld.length);
            for(int j=0;j<N;j++) {
                ii[j+iiOld.length]=-1;
                if (isEnabled.isEnabled(m.getElementAt(j))) {
                    ii[j+iiOld.length]=j;
                    count++;
                }
            }
            l.setSelII(ii);
        } else if (c instanceof JTree) {
            final JTree t=(JTree)c;
            final Set set=new HashSet();
            final int N=t.getRowCount();
            for(int iRow=0;iRow<N;iRow++) {
                final javax.swing.tree.TreePath tp=t.getPathForRow(iRow);
                final Object o=tp.getLastPathComponent();
                if (o!=null && isEnabled.isEnabled(o)) set.add(tp);
            }
            count=sze(set);
            t.setSelectionPaths(toArry(set,javax.swing.tree.TreePath.class));
        } else if (c instanceof JTable) {
            final JTable t=(JTable)c;
            final int COLS=t.getColumnCount();
            for(int row=t.getRowCount(); --row>=0;) {
                for(int col=COLS; --col>=0;) {
                    Object o=t.getValueAt(row, col);
                    final javax.swing.table.TableCellRenderer tcr= o==null || 0==(options&FROM_RENDERER_COMPONENT) ? null : t.getDefaultRenderer(o.getClass());
                    if (tcr!=null) o=getTxt(tcr.getTableCellRendererComponent(t, o, false, false, row,  col));
                    if (o!=null && isEnabled.isEnabled(o)) {
                        t.addRowSelectionInterval(row, row);
                        count++;
                        break;
                    }
                }
            }
        } else if (c instanceof ChFileChooser) {
            final ChFileChooser jfc=(ChFileChooser)c;
            final javax.swing.filechooser.FileFilter fil=jfc.getFileFilter();
            final File ss[]=lstDirF(jfc.getCurrentDirectory());
            if (ss.length>0) {
                File ff[]=new File[ss.length];
                int i=0;
                for(File s : ss) {
                    if (isEnabled.isEnabled(s)) {
                        final File f=s;
                        if (fil==null || fil.accept(f)) ff[i++]=f;
                    }
                }
                ff=rmNullA(ff,File.class);
                jfc.setSelectedFiles(ff);

                inEDTms(thread_selectionToSb(gcp(ChFileChooser.KEY_FPANE,jfc,JComponent.class)), 222);
                jfc.saveSelection(ff);
                count=ff.length;
            }
        }
        return count;
    }
    private static void recursiveSelect(Object haystacks, DialogStringMatch isEnabled, Collection matching) {
        if (haystacks==null) return;
        if (haystacks instanceof Object[]) {
            for(Object h : ((Object[])haystacks)) recursiveSelect(h,isEnabled, matching);
        } else {
            if (haystacks instanceof List) {
                final List v=(List)haystacks;
                for(int i=sze(v); --i>=0;) recursiveSelect(v.get(i),isEnabled,matching);
            }
            if (haystacks instanceof ChRunnable) {
                recursiveSelect(((ChRunnable)haystacks).run(PROVIDE_HAYSTACK,null),isEnabled,matching);
            }
            if(isEnabled.isEnabled(haystacks)) matching.add(haystacks);
        }
    }
    private void selectUnselect(DialogStringMatch isEnabled) {
        handleActEvt(this, ACTION_BEFORE,0);
        int count=0;
        final JComponent jc=get(0,__components, JComponent.class);
        if (__haystacks!=null) {
            __vMatching.clear();
            recursiveSelect(__haystacks,isEnabled, __vMatching);
            count=sze(__vMatching);
        } else if (jc!=null) {
            count=selectJListElements(jc, isEnabled);
        }
        setTxt(count+" items matched",__labCount);
        ChDelay.revalidate(__labCount,333);
        handleActEvt(this, ACTION_FINISHED,0);
    }
    /* <<< selectElements <<< */
    /* ---------------------------------------- */
    /* >>> Here starts the former inner class >>> */
    private ChTextField _tf;
    private ChButton _cbNot ,_cbLC, _cbWr, _cbWl, _cbReg, _cbFile, _butSelect;
    private DialogStringMatch _parent;
    private boolean _isAnd;

    private DialogStringMatch(DialogStringMatch parent) {
        _parent=parent;
        __opts=parent.__opts;

        (_tf=new ChTextField().li(this).tt("Enter a search String")).requestFocus();
        if (sze(parent.__fileName)>0) _tf.saveInFile("DialogStringMatch_"+parent.__fileName+"_"+sze(parent.__vRows));
        _tf.tools()
            .enableWordCompletion(parent.__haystacks)
            .cp(KEY_IF_EMPTY,"Enter search string")
            .highlightOccurrence(" ", null,null,HIGHLIGHT_UPDATE_IF_TEXT_CHANGES,C(DEFAULT_BACKGROUND));
        _tf.tools().highlightOccurrence("\t",null,null,HIGHLIGHT_UPDATE_IF_TEXT_CHANGES,C(0x444444));
        _cbWl=toggl("Begin of word");
        _cbWr=toggl("End of word");
        _cbReg=toggl("Regular expression");
        _cbNot=toggl("Not").tt("Entries that do not match");
        _cbLC=toggl("Ignore case").tt("Ignore upper/lower case of letters").li(this).s(true);
        _cbFile=toggl("File content").li(this).tt("Tests the file content rather than the file name.");
        _butSelect=new ChButton(parent.__haystacks!=null ? "Find<br>matching" : "Select<br>matching").li(this);
        final Object
            pSouth=pnl(_cbNot.cb()," ",_cbReg.cb(),_cbLC.cb(), _cbWl.cb(), _cbWr.cb(), (parent.__opts&NEVER_FILE_CONTENT)==0 ? _cbFile.cb():null),
            pCenter=pnl(new GridLayout(2,1),_tf,pSouth);
        __panel=pnl(CNSEW,pCenter,null,null,null,pnl(_butSelect));
        parent.__vRows.add(this);
        parent.__vB.add(__panel);
        parent.enableDisable(false);
    }
    public boolean isEnabled(Object object0) {
        if (_parent==null) { /* Is the parent */
            boolean success=false;
            for(int i=0; i<sze(__vRows); i++) {
                final DialogStringMatch m=__vRows.get(i);
                if (m==null || sze(m._tf.getText())==0) continue;
                final boolean b=m.isEnabled(object0);
                if ( b && !_isAnd)  return true;
                if (!b &&  _isAnd) return false;
                success|=b;
            }
            return success;
        } else  { /* Single fields */
            final String txt=_tf.getText();
            Pattern pattern=null;
                if (_cbReg.s()) {
                    try {
                        pattern=Pattern.compile(txt, (_cbLC.s()?Pattern.CASE_INSENSITIVE : 0)|Pattern.MULTILINE|Pattern.DOTALL);
                    } catch(Exception e) { error(toStrg(e)); }
                }
            final long opt=
                (_cbLC.s() ? STRSTR_IC  : 0) |
                (_cbWl.s() ? STRSTR_w_L : 0) |
                (_cbWr.s() ? STRSTR_w_R : 0);
            final int fileKB=!_cbFile.s() ? 0 : atoi(_parent.__tfSize);
            return !_cbNot.s() == recursiveMatches(txt, object0,  pattern, opt, fileKB);
        }
    }

    private static boolean recursiveMatches(String needle, Object o, Pattern pattern, long strOpt, int fileKB) {
        if (o==null) return false;
        if (o instanceof Object[] || o instanceof List) {
            for(int i=sze(o); --i>=0;) {
                final Object o2=get(i,o);
                if (o2!=null && recursiveMatches(needle, o2, pattern, strOpt, fileKB)) return true;
            }
        }
        if (o instanceof ChRunnable && recursiveMatches(needle, runCR(o,PROVIDE_STRINGS,null),pattern, strOpt, fileKB)) return true;
        CharSequence txt=o instanceof CharSequence ? (CharSequence)o : null;
        if (txt==null && o instanceof File && fileKB>0) txt=readBytesMX((File)o, 1024L*fileKB, baSoftClr(BA));
        if (txt==null && (o instanceof HasName || o instanceof File)) txt=nam(o);
        if (txt!=null) {
            if (pattern!=null) {
                final java.util.regex.Matcher m=pattern.matcher(txt);
                while(m!=null && m.find()) {
                    final char prev=chrAt(m.start()-1, txt), nxt=chrAt(m.end(),txt);
                    if (
                        (0==(strOpt&STRSTR_w_R) || !is(LETTR_DIGT_US,nxt)) &&
                        (0==(strOpt&STRSTR_w_L) || !is(LETTR_DIGT_US,prev))
                        ) return true;
                }
                return false;
            }
            return strstr(strOpt,needle,txt)>=0;
        }
        return false;
    }
    /* <<< Match <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private static int _count;
    public Object getPanel(int mode) {
        if (__panel==null && _parent==null) {
            __tfSize=new ChTextField("10").ct(Integer.class).cols(9,true,false);
            __labCount=new JLabel();
            __butAnd=new ChButton("Logical \"AND\"").li(this);
            __butOr=new ChButton("Logical \"OR\"").li(this).tt("combine all searches by boolean \"OR\"");
            final Object
                msg=new ScrollLabel("With this dialog you can select items  using string patterns"),
                pNorth=pnl(CNSEW,msg,null,null,smallSourceBut(DialogStringMatch.class),smallHelpBut(DialogStringMatch.class));
            if (0==(__opts&NEVER_FILE_CONTENT)) {
                __panFile=pnl(HB,"   Limit search within files to ",__tfSize,"kByte");
                pcp(KOPT_HIDE_IF_DISABLED,"",__panFile);
            }
            __vB=pnl(VB,pNorth);
            __vRows=new ArrayList();
            __panel=pnl(CNSEW,__vB,gcp(KEY_NORTH_PANEL,this), pnl(__butAnd,__butOr,__labCount, __panFile));
            enableDisable(true);
        }
        return __panel;
    }

    public void showInFrame() {
         if (__frame==null) {
             final Object north=gcp(KEY_NORTH_PANEL,this);
             __frame=new ChFrame(orS(gcps(KEY_TITLE,this), "Select by text pattern #"+ ++_count)).ad(north==null?getPanel(0) : pnl(CNSEW, getPanel(0),north));
             final ChTextField tf=child(__vB,ChTextField.class);
             if (tf!=null) tf.requestFocus();
         }
         __frame.shw(ChFrame.TO_FRONT|ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN|CLOSE_CtrlW_ESC);
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();

        if (_parent!=null) {
            if (q==_butSelect || q==_tf && cmd==ACTION_ENTER) { // Matcher
                setTxt("Computing ...",_parent.__labCount);
                _parent.selectUnselect(DialogStringMatch.this);
            }
            if (q==_tf || q==_cbFile) _parent.enableDisable(q==_tf);
        } else {
            if (q==__butAnd || q==__butOr) {
                _isAnd= q==__butAnd;
                selectUnselect(this);
            }
        }
    }
}
