package charite.christo;
import java.awt.*;
import static charite.christo.ChUtils.*;
public class RemainingSpc extends ChPanel {
    private final int _parent;
    public RemainingSpc(int parentDepth) {
        super();
        _parent=parentDepth;
        setMinSze(0,0, this);
    }

    private int _initialized;
    private boolean _dimZero;
    @Override public Dimension getPreferredSize() {
        final Dimension d=super.getPreferredSize();
        //putln("d="+d);
        if (_dimZero) return d;
        Component parent=this;
        for(int i=_parent; --i>=0 && parent!=null;) parent=parent.getParent();
        if (parent==null) {
            return dim(0,0);
        }
        if (_initialized++==0) {
            for(ChJScrollPane sp : childsR(parent, ChJScrollPane.class)) {
                sp.addOption(SCRLLPN_INHERIT_SIZE);
            }

        }
        _dimZero=true;
        final int add=parent.getHeight()-prefH(parent);
        //putln("RemainingSpc "+parent.getHeight()+"  "+prefH(parent));
        _dimZero=false;
        D.width=d.width;
        D.height=d.height+maxi(0,add-2);
        return D;
    }
    private final Dimension D=new Dimension();
}
