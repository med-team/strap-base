package charite.christo;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class RendererNumberAsOval extends JComponent implements TableCellRenderer {
    public final static int STYLE_COLORED_OVAL=0, STYLE_OVAL=1, STYLE_NUMBER=2;
    public final static String TIP="RNO$$T";
    private final ChRunnable _r;
    private double _scale, _offset, _v=Double.MIN_VALUE;
    private int _type;
    private Object _o;

    private final static BA BA[]={null};
    public RendererNumberAsOval(ChRunnable r) {
        _r=r;
        rtt(this);
    }
    public void set(int type, double scale, double offset) {
        _scale=scale;
        _offset=offset;
        _type=type;
    }
    public Component getTableCellRendererComponent(JTable t,Object o,boolean isSel,boolean hasFocus,int row,int col) {
        final double dd[]=(double[])_r.run(ChRunnable.RUN_GET_DISSIMILARITY_VALUE, _o=o);
        _v=dd==null ? Double.MIN_VALUE : dd[0];
        return this;
    }
    public String getToolTipText() { return (String) _r.run(TIP, _o); }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        final BA ba= Double.isNaN(_v) || _type==STYLE_NUMBER || Double.isNaN(_scale) ? baClr(BA).aFloat(_v,5) : null;
        final int w=getWidth()-2;
        final int h=getHeight()-2;

        if (_v==Double.MIN_VALUE) {
            g.drawImage(iicon(IC_HOURGLASS).getImage(),  w/2-ICON_HEIGHT/2, h/2-ICON_HEIGHT/2, this);
        } else if (ba!=null) {
            g.drawString(ba.toString(),0,charA(g));
        } else {
            final double v2=(_offset+_v)*_scale;
            float v1=1-(float)v2;
            if (v1>1) v1=1;
            if (v1<0) v1=0;
            g.setColor(_type==STYLE_COLORED_OVAL ? ChUtils.float2blueRed((float)v2) : new Color(v1,v1,v1));
            final int radius=(int)(w*v2)/2,x=1+w/2-radius;
            g.fillOval(x,1,2*radius,h);
            g.setColor(C(0));
            g.drawOval(x,1,2*radius,h);
        }
    }
}
