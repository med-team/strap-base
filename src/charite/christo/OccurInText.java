package charite.christo;
import java.util.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**
   Find places of patterns in text
   @author Christoph Gille
*/
public final class OccurInText {
    private OccurInText(){}
    public static int[] find(long mode, byte needle[], boolean[] delimL, boolean[] delimR, BA ba, int[] buffer) {
        if (needle==null || needle.length==0 || ba==null) {
            if (sze(buffer)==0) return NO_INT;
            buffer[0]=-1;
            return buffer;
        }
        final boolean
            ignoreCase= (mode & HIGHLIGHT_IGNORE_CASE)!=0,
            skipBlanks= (mode & HIGHLIGHT_SKIP_BLANKS)!=0,
            ignoreLenOfSpc= (mode & HIGHLIGHT_IGNORE_LEN_OF_BLANK)!=0,
            dotMatchesAny= (mode & HIGHLIGHT_DOT_MATCHES_ANY)!=0;
        final byte c0=needle[0], T[]=ba.bytes();
        final int
            lc0='A'<=c0 && c0<='Z' ? c0|32 : c0,
            L=needle.length,
            end=ba.end(),
            begin=ba.begin();
        int fromTo[]=buffer!=null?buffer:new int[900], count=0;
        if (needle==ChTextComponents.WRONG_CHARS_IN_FILEN) {
            final boolean[] charClassFN=chrClas(FILENM);
            final int b=nxtE(-SPC,T,begin,end);
            final int e=prev(-SPC,T,end-1, b-1);
            if (!looks(LIKE_EXTURL,T, b,e)) {
                for(int i=b; i<e; i++) {
                    final char c=(char)T[i];
                    if (c>127 || !charClassFN[c]  && c!='/' && c!='\\' && (i!=b+1||c!=':') && (i>0||c!='~')  ) {
                        if (count+1>=fromTo.length) fromTo=chSze(fromTo, 2*count+100);
                        fromTo[count++]=i;
                        fromTo[count++]=i+1;
                    }
                }
            }
        } else {
            if (L==1) {
                nextPosition:
                for(int i=begin; i<end; i++) {
                  if (T[i]==c0 || ignoreCase && (T[i]|32)==lc0 && is(LETTR,lc0)) {
                        final int hitStart=i;
                        while (i+1<end && (T[i+1]==c0 || ignoreCase && (T[i+1]|32)==lc0) ) i++;
                        if (2*count>=fromTo.length) fromTo=chSze(fromTo,2*count+100);
                        if (i>begin && delimL!=null  && !delimL[127&T[i-1]]) continue nextPosition;
                        if (delimR!=null  && i+1<end && !delimR[127&T[i+1]]) continue nextPosition;
                        fromTo[count++]=hitStart;
                        fromTo[count++]=i+1;
                        if (count>9999) break;
                    }
                }
            } else {
                nextPosition:
                for(int i=begin, endL1=end-L+1; i<endL1; i++) {
                  if (ignoreCase ? (T[i]|32)==lc0 : T[i]==c0 ) {
                        int iQ=1,iH=i+1;
                        for(; iQ<L && iH<end; iQ++,iH++) {
                            byte cQ= needle[iQ];
                            byte cH=T[iH];
                            if (ignoreLenOfSpc && iQ>0 && iQ!=needle.length-1 && isSpc(cH) && isSpc(cQ)) {
                                cQ=get(iQ=nxtE(-SPC,needle,iQ, L),needle);
                                cH=get(iH=nxtE(-SPC,T,iH,end),T);
                            }
                            while (skipBlanks && (cH<'0' && isSpc(cH) || cH=='-') && cQ!=cH && iH<end-1) cH=T[++iH];
                            if (!(dotMatchesAny&&cQ=='.' || ignoreCase && (cQ|32)==(cH|32) && is(LETTR,cQ) || cQ==cH)) continue nextPosition;
                        }
                        if (i>begin && delimL!=null  && !delimL[127&T[i-1]]) continue nextPosition;
                        if (delimR!=null && iH<end-1 && !delimR[127&T[iH]]) continue nextPosition;
                        if (count+1>=fromTo.length) fromTo=chSze(fromTo, 2*count+100);
                        fromTo[count++]=i;
                        fromTo[count++]=iH;
                        if (count>9999) break;
                    }
                }
            }
        }
        if (count<fromTo.length) fromTo[count]=-1;
        return fromTo;
    }
    public static int[] findLU(byte[] lookup[][], BA ba, boolean[] delimL, boolean[] delimR, IsEnabled isEnabled, int[] buffer) {
        final byte T[]=ba!=null ? ba.bytes() : null;
        final int E=T!=null ? mini(T.length,ba.end()) : 0;
        if (E==0) return NO_INT;
        final int B=ba.begin();
        int fromTo[]=buffer!=null?buffer : new int[100];
        final int found[]={-1,-1};
        int pos=B-1, count=0;
        while( pos<E-1 && (pos=idxOfNeedles(lookup, delimL, T, pos, E, found)) >=0 ) {
            if (count+1>=fromTo.length) fromTo=chSze(fromTo,2*count+1000);
            final byte hit[]=lookup[found[0]][found[1]];
            final int n=hit!=null ? hit.length : 0;
            if (n==0) continue;
            if (delimR!=null && pos+n<E && !delimR[127&T[pos+n]]) continue;
            fromTo[count++]=pos;
            fromTo[count++]=pos+n;
        }
        if (count<fromTo.length) fromTo[count]=-1;
        return fromTo;
    }

    static int idxOfNeedles(byte[][][] lookupTable, boolean[] delimL, byte[] T, int from, int to, int[] found) {
        final int toIndex=mini(T.length,to), fromIndex=maxi(0,from);
        for(int i=fromIndex; i<toIndex;i++) {
            final byte[][] words=lookupTable[T[i]&127];
            if (words!=null) {
                final int c=T[i];
                if (c<0 || 127<c) continue;
                if (delimL!=null && i>fromIndex && !delimL[127&T[i-1]]) continue;
                final int c1=i+1<toIndex ? T[i+1] : 0;
                for(int iK=maxi(0,c==found[0] ? found[1]+1 : 0); iK<words.length; iK++) {
                    final byte[] word=words[iK];
                    if (word==null) break;
                    if ( (word.length==1 || c1==word[1]) &&  i+word.length<=toIndex && strEquls(0L, word,T,i)) {
                        found[0]=c;
                        found[1]=iK;
                        return i;
                    }
                }
            }
            found[0]=found[1]=-1;
        }
        return found[0]=found[1]=-1;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    public static byte[][][] addToLookupTable(Object ss[], byte[][][] lookupTable) {
        final byte[][][] ttt=lookupTable!=null?lookupTable : new byte[128][][];
        nextO:
        for(Object o : ss) {
            if (o instanceof Object[]) addToLookupTable((Object[])o,ttt);
            else if (o instanceof Collection) addToLookupTable(((Collection)o).toArray(),ttt);
            else {
                final byte[] T=o instanceof byte[] ? (byte[])o : o instanceof String ? toBytsCached((String)o) : null;
                if (T==null && o!=null) assrt();
                if (T!=null && T.length>0) {
                    final byte c=T[0];
                    if (ttt[c]!=null) {
                        for(byte[] tt : ttt[c]) {
                            if (tt==null) break;
                            if (bytsEquls(T,0,MAX_INT, tt,0) && T.length==tt.length) continue nextO;
                        }
                    }
                    ttt[c]=ad(T, ttt[c], sze(ttt[c])+5, byte[].class);
                }
            }
        }
        return ttt;
    }

}
