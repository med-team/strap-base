package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class ChMap<K,V>  implements Map<K,V>, ChRunnable, HasMC {
    public final static int TYPE_TreeMap=1<<1, TYPE_WeakHashMap=1<<2, WORD_COMPLETION_K=1<<3, WORD_COMPLETION_V=1<<4;

    private transient volatile int _mc;
    private final Class _clazzK,_clazzV;
    private final int _options, _initialCapacity;
    public ChMap() { this(0L, Object.class,Object.class,99); }
    public ChMap(long options, Class cK, Class cV, int capacity) {
        _clazzK=cK;
        _clazzV=cV;
        _options=(int)options;
       _initialCapacity=capacity;
    }

    private Map _map;
    private Map<K,V> m() {
        if (_map==null) {
            _map=
                0!=(_options&TYPE_TreeMap) ? new TreeMap() :
                0!=(_options&TYPE_WeakHashMap) ? new WeakHashMap(_initialCapacity) :
                new HashMap(_initialCapacity);

        }
        return _map;
    }
    public int size(){ return m().size();}
    public boolean isEmpty(){ return m().isEmpty();}
    public boolean containsKey(Object key){ return m().containsKey(key);}
    public boolean containsValue(Object value){ return m().containsValue(value);}
    public V get(Object key){

        final Class keyClass=key==null?null:key.getClass();
        if (keyClass==Vector.class || keyClass==ArrayList.class) { /* hashCode is variant */
            for(Map.Entry e : entryArry(m())) {
                if (e.getKey()==key) return (V)e.getValue();
            }
            return null;
        } else return m().get(key);
    }
    public Set<K> keySet(){ return m().keySet();}
    public Collection<V> values(){ return m().values();}
    public Set<Map.Entry<K, V>> entrySet(){ return m().entrySet();}

    public V put(K key, V value) {
        final V old=value==null ? m().get(key) : m().put(key,value);
        if (value==null) m().remove(key);
        if (old!=value) { _mc++; _cached=null; }
        return old;
    }
     public void clear() {
        _mc++;
         _cached=null;
        m().clear();
    }

     public void putAll(Map<? extends K,? extends V> m) {
        _mc++;
        _cached=null;
        m().putAll(m);
    }
     public V remove(Object o) {
        _mc++;
        _cached=null;
        return m().remove(o);
    }

    public int mc() { return _mc;}

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Arrays >>> */

    private Object _cached;
    private int _lastSize;
    Object[] cached() {
        Object oo[]=(Object[])deref(_cached);
        final int L=size();
        if (oo==null) _cached=newSoftRef(oo=new Object[9]);
        else if (_lastSize!=L) for(int i=oo.length; --i>=0;)oo[i]=null;
        _lastSize=L;
        return oo;
    }

    public V[] vArray(ChRunnable run, String runID) { return (V[]) toArrayKV(false, run, runID); }
    public K[] kArray(ChRunnable run, String runID) { return (K[]) toArrayKV(true,  run, runID); }
    public V[] vArray() { return (V[]) toArrayKV(false, null,""); }
    public K[] kArray() { return (K[]) toArrayKV(true,  null,""); }
    Object toArrayKV(boolean isKey, ChRunnable run, String runID) {
        final Object cached[]=cached();
        Object[] vv=(Object[])cached[isKey ? C_ARRAYk : C_ARRAYv];
        if (vv==null) {
            vv=(isKey ? keySet() : new HashSet(values())).toArray(emptyArray(isKey ? _clazzK : _clazzV));
            vv=rmNullA(vv,isKey ? _clazzK : _clazzV);
            cached[isKey ? C_ARRAYk : C_ARRAYv]=vv;
            if (run!=null) run.run(runID,vv);
        }
        return vv;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Derived >>> */
    public Map<String,V[]> getMapName2objects(int reserve) {
        final Object cached[]=cached();
        Map<String, V[]> m=(Map)cached[C_MAP_name2o];
        if (m==null) {
            cached[C_MAP_name2o]=m=new HashMap();
            for(V v : vArray()) mapOneToMany(v, nam(v), m, 2, _clazzV);
        }
        return m;
    }
    @Override public boolean equals(Object o){ return m().equals(o);}
    @Override public int hashCode(){return m().hashCode(); }
    @Override public String toString() { 
        // return super.toString()+"#"+size();
        return m().toString();
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> As List >>> */

    private int vList_mc, kList_mc;
    private UniqueList vList,kList;
    public List list(boolean isKey, ChRunnable run ,String runID) {
        UniqueList v= isKey ? kList : vList;
        if ( (isKey ? kList_mc : vList_mc)!=_mc || v==null) {
            if (isKey) kList_mc=_mc;
            else vList_mc=_mc;
            if (v==null) {
                v=new UniqueList(Object.class).rmOptions(UniqueList.UNIQUE);
                v.addHook(UniqueList.HOOK_ACCESS, this, "UPDATE_v");
                if (isKey) kList=v; else vList=v;
            } else v.clear();
            if (run!=null) run.run(runID,vList);
        }
        return v;
    }
    public List<V> vList(ChRunnable run ,String runID) { return list(false, run, runID); }
    public List<K> kList(ChRunnable run ,String runID) { return list(true, run, runID); }
    public List<V> vList() { return list(false, null, ""); }
    public List<K> kList() { return list(true, null, ""); }

    /* <<< As List <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    private String _labelText;
    private Object _ic;

    public ChMap t(String title) { _labelText=title; return this;}
    public ChMap i(String icon)  { _ic=icon; return this;}

   /* <<< Renderer  <<< */
    /* ---------------------------------------- */
    /* >>> Word completion >>> */
    private Object[] wordCompl;

    private int mc_vList, mc_kList;
    public Object run(String id,Object arg) {
        if (id==ChRunnable.RUN_GET_ICON) return _ic;
        if (id==ChRunnable.RUN_GET_ITEM_TEXT) return _labelText;
        if (id=="UPDATE_v") {
            final UniqueList v=(UniqueList)arg;
            final int mc=mc();
            Object[] addAll=null;
            if (v==vList && mc_vList!=mc) {
                mc_vList=mc;
                addAll=vArray();
            }
            if (v==kList && mc_kList!=mc) {
                mc_kList=mc;
                addAll=kArray();
            }
            if (addAll!=null) {
                v.clear();
                v.addAllA(addAll);
            }
        }
        if (id==PROVIDE_WORD_COMPLETION_LIST) {
            final boolean k=0!=(_options&WORD_COMPLETION_K), v=0!=(_options&WORD_COMPLETION_V);
            if (!v && !k) return null;
            if (v && k) {
                if (wordCompl==null) wordCompl=new Object[2];
                wordCompl[0]=vArray();
                wordCompl[1]=kArray();
                return wordCompl;
            }
            return v ? vArray() : kArray();
        }
        if (id==DialogStringMatch.PROVIDE_HAYSTACK || id==PROVIDE_JTREE_CHILDS) {
            return vArray();
        }

        return null;
    }
}
