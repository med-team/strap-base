package charite.christo;
import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.MouseEvent.*;
/**
   A reimplementation of tabbed pane since JTabbedPane does not work very well
   @author Christoph Gille
*/
public class ChTabPane extends ChPanel implements ProcessEv, PaintHook, PreferredSize, Comparator {
    public final static String
        ACTION_REMOVED="TP_A_RM", ACTION_ADDED="TP_A_ADD", ACTION_SELECTION_CHANGED="TP_A_SC",
        KEY_SORT="CTB$$S", KEY_PARENT_CTP="CTP$$P", KEY_UNIQUE_ID="CTP$$U", KEY_PANE1="CTP$$1", KEY_PANE2="CTP$$2";
    public final static int LEFT=1<<1, RIGHT=1<<2, COLLAPSE=1<<3;
    private final Object KEY_WHEN=new Object(), KEY_TAB=new Object();
    private final List<Component> _v=new ArrayList(), _vFocus=new ArrayList();
    private final JComponent
        _panTabs, _pTabs,
        _butPrev=new ChButton(ChButton.DISABLED|ChButton.MAC_TYPE_ICON|ChButton.HIDE_IF_DISABLED, "\u25C4")
        .li(evAdapt(this))
        .tt("selects the previously focused dialog");
    private final boolean _leftSide;
    private JComponent _panTabsPar, _jlHisto, _bSelected;
    private Object _pCenter;
    public void addSouth(JComponent c) { _panTabsPar.add(c,BorderLayout.SOUTH);}
    public ChTabPane(long options) {
        _leftSide=0!=(options&LEFT);
        _insets=new Insets(0,_leftSide?0:EX*3/2,0,_leftSide?EX*3/2:0);
        setPrefSze(this, _panTabs=pnl());
        setMinSze(0,0, _panTabsPar=_pTabs=pnl(_panTabs));
        if (_leftSide) {
            addPaintHook(this,_panTabsPar=pnl(CNSEW,_pTabs,pnl(_butPrev)));
            setMinSze(3,3, this);
        }
        pnl(this,CNSEW,null,null,null, _leftSide?null:_panTabsPar, _leftSide?_panTabsPar:null);
        pcp(KEY_PANE1, wref(_pTabs), this);
        pcp(KEY_PANE2, wref(_panTabs), this);
    }
    private void updateGUI() {
        _panTabs.removeAll();
        _panTabs.setLayout(new GridLayout(sze(_v),1));
        for(JComponent jc: tabComponents())  {
            final ChButton tog=gcp(KEY_TAB,jc,ChButton.class);
            if (tog!=null) {
                _panTabs.add(_leftSide ? tog:tog.radio1());
            }
        }
        ChDelay.revalidate(_jlHisto,222);
        for(int i=sze(_vFocus);--i>=0;) {
            if (!_v.contains(_vFocus.get(i))) _vFocus.remove(i);
        }
        _bSelected=gcp(KEY_TAB, selectedComponent(), ChButton.class);

        setEnabld(sze(_vFocus)>2, _butPrev);
    }

    public void setSelectedComponent(Component o) { addTab(0, o);}
    public JComponent selectedComponent() {  return (JComponent)deref(_pCenter); }

    public JComponent[] tabComponents() { return toArry(_v,JComponent.class);}
    public JComponent componentWithTabtext(String s) {
        for(JComponent c : tabComponents()) {
            if (s!=null && s.equals(dTab(c))) return c;
        }
        return null;
    }
    private final static int xx[]=new int[4], yy[]=new int[4];
    public boolean paintHook(JComponent c,Graphics g, boolean after) {
        final int h=c.getHeight(),w=c.getWidth();
        if ("TAB"==gcp(KEY_ACTION_COMMAND,c)) {
            if (!after && _leftSide) ((ChButton)c).setTabSelected(g,c==_bSelected);

            if (c==_bSelected && after) {
                final int hi=h/3, xTip=_leftSide?w:0, xBase=_leftSide?w-EX/2:EX/2;
                xx[0]=xBase; yy[0]=(h-hi)/2;
                xx[1]=xBase; yy[1]=(h-hi)/2+hi;
                xx[2]=xTip;  yy[2]=h/2;
                xx[3]=xx[0]; yy[3]=xx[0];
                g.setColor(C(c==_bSelected?0xFFffFF : 0));
                g.fillPolygon(xx,yy,3);
            }
        }
        return true;
    }
    public void removeAllTabs() {
        _v.clear();
        final Component pC=derefC(_pCenter);
        if (parentC(pC)==this) remove(pC);
        updateGUI();
        revalAndRepaintC(this);
        handleActEvt(this,ACTION_REMOVED,0);
    }

    public boolean removeTab(Component... cc) {
        boolean success=false;
        for(Component c:cc) {
            if (c!=null && _v.remove(c)) {
                pcp(KEY_PARENT_CTP,null,c);
                pcp(KEY_TAB,null,c);
                success=true;
            }
        }
        if (success) {
            previousTab();
            handleActEvt(this,ACTION_REMOVED,0);
        }
        return success;
    }
    private void previousTab() {
        if (sze(_vFocus)>0) _vFocus.remove(0);
        _selC(get(0, sze(_vFocus)>0 ? _vFocus : _v, Component.class));

        updateGUI();
        revalAndRepaintC(this);
        _butPrev.repaint();
    }

    public void addTab(int opt, Component c) {
        if (c==null || c==selectedComponent()) {
            handleActEvt(this,ACTION_SELECTION_CHANGED,0);
            return;
        }
        pcpAddOpt(KEY_CLOSE_OPT, opt&CLOSE_MASK, c);

        if (gcp(KEY_WHEN,c)==null) pcp(KEY_WHEN,intObjct((int)(System.currentTimeMillis()&0x7fFFffFF)),c);
        final Object noDuplicates=gcp(KEY_UNIQUE_ID,c);
        if (noDuplicates!=null) {
            for(int i=sze(_v); --i>=0;) {
                final JComponent jc=get(i,_v,JComponent.class);
                if (noDuplicates.equals(gcp(KEY_UNIQUE_ID,jc))) {
                    _selC(jc);
                    return;
                }
            }
        }
        handleActEvt(this,ACTION_ADDED,0);
        adUniq(c,_v);
        if (gcp(KEY_TAB,c)==null) pcp(KEY_TAB,newTabButton(c),c);
        pcp(KEY_PARENT_CTP,wref(this),c);
        _selC(c);
        addMoli(MOLI_REFRESH_TIP,c);
        final JComponent sorted[]=tabComponents();
        sortArry(sorted,this);
        _v.clear();
        adAllUniq(sorted,_v);
        updateGUI();
        revalAndRepaintC(this);
    }
    private void _selC(Component c) {
        final JComponent center=selectedComponent();
        if (center!=null) remove(center);
        _pCenter=wref(c);
        if (c==null) return;
        add(c,BorderLayout.CENTER);
        if (center!=c)  handleActEvt(this,ACTION_SELECTION_CHANGED, 0);
        _bSelected=gcp(KEY_TAB,c,ChButton.class);

        for(Object o : tabComponents()) {
            final ChButton b=gcp(KEY_TAB, o, ChButton.class);
            if (b!=null) b.s(o==c);
        }
        _vFocus.remove(c);
        _vFocus.add(0,c);
        revalAndRepaintC(this);
        _butPrev.repaint();
    }
    private final Insets _insets;
    private ChButton newTabButton(Component comp) {
        final String tab=dTab(comp);
        final Icon ic=dIcon(comp);
        final ChButton tog=
            new ChButton(_leftSide?'B':'T', "TAB")
            .setOptions(_leftSide ? 0 : (ChButton.NO_FILL|ChButton.NO_BORDER))
            .li(evAdapt(this))
            .t(tab).i(_leftSide?ic:null)
            .tt(dTip(comp))
            .cp(KEY_tabComponent, wref(comp));
        tog.setFocusable(false);
        if (_leftSide) evLstnr(MOLI_TAB).addTo("m", tog);
        Colored colored=deref(comp,Colored.class);
        if (colored==null) colored=deref(gcp(KEY_MODEL,comp),Colored.class);
        tog.cp(_leftSide ? ChButton.KEY_COLOR_BOX_L : ChButton.KEY_COLOR_BOX_R, colored);
        if (null!=gcp(KEY_CLOSE_OPT, comp)) {
            ChRenderer.setSmallButtonX(ChRenderer.SMALL_CLOSE, _leftSide?0:-ChRenderer.BUT_WIDTH, tog);
        }
        if (_leftSide && undockEnabld()!=-1) pcp(KOPT_DRAW_UNDOCKABLE,"", tog);

        evAdapt(this).addTo("m",tog);
        tog.setHorizontalAlignment(_leftSide?AbstractButton.TRAILING:AbstractButton.LEFT);
        if (_leftSide) tog.setHorizontalTextPosition(AbstractButton.LEFT);

        tog.setMargin(_insets);
        addPaintHook(this,tog);
        setTabUndocksPanel(tog,comp);
        return tog;
    }

    public void updateTabLabels() {
        for(JComponent c:tabComponents()) {
            final ChButton tog=gcp(KEY_TAB,c, ChButton.class);
            if (tog!=null) tog.t(dTab(c));
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> ActionListener >>> */
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);

        if (cmd=="TAB" && q!=_bSelected) {
            final JComponent pc=selectedComponent();
            if (pc!=null) remove(pc);
            _bSelected=(ChButton)q;
            final JComponent c=gcp(KEY_tabComponent, q, JComponent.class);
            if (_v.contains(c)) addTab(0, c);
        }
        if (q==_butPrev) {
            if (isCtrl(ev)) {
                if (_jlHisto==null) _jlHisto=new ChJList(_vFocus,ChJTable.ICON_ROW_HEIGHT|ChJTable.CLASS_RENDERER);
                ChFrame.frame("History",_jlHisto,ChFrame.SCROLLPANE).shw(0);
            } else previousTab();
            ChDelay.revalidate(_jlHisto,333);
        }
    }
    /* <<< ActionListener <<< */
    /* ---------------------------------------- */
    /* >>> getPreferredSize >>> */
    @Override public Dimension preferredSize(Object c) {
        final Component jc=derefC(c);
        int height=0;
        final Component cc[]=childs(jc);
        final int width=maxi(ICON_HEIGHT, prefW(cc));
        if (c==_panTabs) height=mini(_pTabs.getHeight()-ICON_HEIGHT,sze(_v)*ICON_HEIGHT);
        if (c==_pTabs)   height=mini(sze(cc)*ICON_HEIGHT,getHeight()-_butPrev.getHeight());
        return dim(width,height);
    }

    /* <<< getPreferredSize <<< */
    /* ---------------------------------------- */
    /* >>> order >>> */

    public int compare(Object o1,Object o2) {
        final String k1=gcps(KEY_SORT,o1), k2=gcps(KEY_SORT,o2);
        final int w1=(atoi(gcp(KEY_WHEN,o1))<<16) | _v.indexOf(o1), w2=(atoi(gcp(KEY_WHEN,o2))<<16) | _v.indexOf(o2);
        final int comp=k1!=null && k2!=null  ? k1.compareTo(k2) : 0;
        return comp!=0 ? comp : w1<w2 ? -1 : 1;
    }

}
