package charite.christo;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public final class Archive {
    private final Object SYNC_FP=new Object();
    private List<Entry> _vEntries;
    private final File _file, _fileHeader;
    private RandomAccessFile _fp;
    private long _lastModifiedH;
    private static class Entry implements Comparable {
        long _time;
        int _from, _to, _hashCd;
        String _name;
        //@Override public String toString() { return _name+" {"+_from+","+_to+"}";}
        public int compareTo(Object o) {
            final Entry e=(Entry)o;
            return e._from<_from ? 1 : e._from==_from ? 0 : -1;
        }
    }

    public Archive(File f,File fHeader) {
        _fileHeader=fHeader;
        mkParentDrs(_file=f);
    }
    private RandomAccessFile raf(boolean write) throws IOException {
        RandomAccessFile raf=_fp;
        if (raf==null) _fp=raf=Insecure.randomAccessFile(_file, "rw");
        return raf;
    }

    private List<Entry> vEntries() {
        if (_vEntries==null) {
            _vEntries=new java.util.ArrayList();
            _lastModifiedH=_fileHeader.lastModified();
            final BA ba=readBytes(_fileHeader,(BA)null);
            if (ba!=null) {
                final long len=_file.length();
                final byte[] bb=ba.bytes();
                final int ends[]=ba.eol();
                for(int i=0;i<ends.length;i++) {
                    final int begin= i==0? 0 : ends[i-1]+1, end=ends[i];
                    int tab1=-1, tab2=-1, tab3=-1, tab4=-1;
                    for(int pos=begin;pos<end;pos++) {
                        if (bb[pos]=='\t') {
                            if (tab1<0) tab1=pos;
                            else if (tab2<0) tab2=pos;
                            else if (tab3<0) tab3=pos;
                            else if (tab4<0) tab4=pos;
                        }
                    }
                    if (tab3<0) continue;
                    final int to=atoi(bb,tab3+1, end);
                    if (len<to) continue;
                    final Entry e=new Entry();
                    e._name=bytes2strg(bb, begin, tab1);
                    e._time=atol(bb,tab1+1,MAX_INT);
                    e._from=atoi(bb,tab2+1,end);
                    e._to=atoi(bb,tab3+1,end);
                    if (tab4>0) e._hashCd=atoi(bb,tab4+1,end);
                    _vEntries.add(e);
                }
            }
            if (_lastModifiedH !=_fileHeader.lastModified()) {
                _vEntries.clear();
            }
        }
        return _vEntries;
    }
    private Entry getEntry(String entryName) {
        final String name=entryName.replace('\n','_');
        final List<Entry> v=vEntries();
        final int L=name.length();
        for(int i=v.size(); --i>=0;) {
            final Entry e=v.get(i);
            if (L==e._name.length() && name.equals(e._name)) return e;
        }
        return null;
    }
    public boolean contains(String s) { return getEntry(s)!=null;}
    public long getLastModified(String s) {
        final Entry e=getEntry(s);
        return e!=null ? e._time : 0;
    }
    public BA get(String name0, BA buffer) {
        final String name=name0.replace('\n','_');
        final Entry e=getEntry(name);
        if (e==null) return null;
        final BA ba=buffer!=null ? buffer : new BA(0);
        final int len=e._to-e._from;
        byte bb[]=ba.bytes();
        if (len>bb.length) bb=new byte[len];
        try {
            final RandomAccessFile fp=raf(false);
            if (fp==null) return null;
            fp.seek(e._from);
            fp.readFully(bb,0,len);
            ba.set(bb,0,len);
            if (e._hashCd!=0 && e._hashCd!=hashCd(bb,0,len)) {
                putln(RED_WARNING+"Archive: wrong hashcode", _file);
                return null;
            }
        } catch(IOException ex){stckTrc(ex); return null;}
        return ba;
    }
    /* <<< Get  <<< */
    /* ---------------------------------------- */
    /* >>> Write  >>> */

    private boolean modified;
    public void write(String name,BA ba) {
        if (ba==null || name==null) return;
        final byte[] T=ba.bytes();
        final int B=ba.begin(), E=ba.end();
        final String nam=name.replace('\n','_');
        if (_vEntries!=null) _vEntries.remove(getEntry(nam));
        modified=true;
        sortArry(vEntries());
        final int len=E-B;
        final int nE=vEntries().size();
        int insertAt=-1, insertGap=MAX_INT, lastTo=0,maxTo=0;
        for(int i=0; i<nE; i++) {
            final Entry e=vEntries().get(i);
            final int gap=e._from-lastTo;
            if (gap>=len && gap<insertGap) {
                insertAt=lastTo;
                insertGap=gap;
            }
            lastTo=e._to;
            if (maxTo<lastTo) maxTo=lastTo;
        }
        try {
            Entry newE=getEntry(nam);
            final boolean contains=newE!=null;
            if (!contains) (newE=new Entry())._name=nam;
            synchronized(SYNC_FP) {
            final RandomAccessFile fp=raf(true);
            newE._from=insertAt<0 ? maxTo : insertAt;
            newE._to=newE._from+len;
            newE._time=System.currentTimeMillis();
            newE._hashCd=hashCd(T,B,E);
            fp.seek(newE._from);
            fp.write(T, B, E);
            }
            if (!contains) vEntries().add(newE);
        } catch(IOException ex){stckTrc(ex);}
    }
    private boolean _dirty;
    void deleteBecauseError() {
        putln(RED_ERROR+" Archive.deleteBecauseError()  ",_file);
        _dirty=true;
        delFile(_file);
        delFile(_fileHeader);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Close >>> */
    void close() {
        synchronized(SYNC_FP) {
        final RandomAccessFile fp=_fp;
        if (fp!=null) try { fp.close();} catch(IOException e){}
        _fp=null;
        }
    }
    void dispose() {
        if (modified && !_dirty) {
            if (myComputer()) puts("Archive save "+_fileHeader+ "  "+sze(_fileHeader)+" ");
            final List<Entry> v=vEntries();
            final int nE=v.size();
            int size=0;
            BA sb=null;
            while(true) {
                for(int i=0; i<nE; i++) {
                    final Entry e=(Entry)ChUtils.get(i,v);
                    if (e==null) break;
                    if (sb!=null) sb.a(e._name).a('\t').a(e._time).a('\t')
                                      .a(e._from).a('\t').a(e._to).a('\t')
                                      .a(e._hashCd).a('\n');
                    else size+=e._name.length()+(4+30);
                }
                if (sb==null) sb=new BA(size); else break;
            }
            wrte(_fileHeader,sb);
            if (myComputer()) putln(GREEN_DONE);
        }
    }
    @Override protected void finalize() throws Throwable {
        //logFinalize(ANSI_RED+ANSI_FG_WHITE+"Archive","");
        dispose();
        close();
        super.finalize();
    }
}
