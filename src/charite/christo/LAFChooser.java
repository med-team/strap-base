package charite.christo;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.net.URL;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class LAFChooser implements java.awt.event.ActionListener, java.beans.PropertyChangeListener {
    public final static boolean METAL=false;
    public final static String IF_NOT_ALREADY="LC$$INA";
    private static LookAndFeel _current;
    private static boolean _restoreLaf, _initalized;

    /* ---------------------------------------- */
    /* >>>  Instance >>> */
    private final ChJList jList=new ChJList(null,ChJTable.ICON_ROW_HEIGHT).li(this);
    private JComponent _pnl;
    private static LAFChooser _inst;
    public static LAFChooser instance() { if (_inst==null) _inst=new LAFChooser(); return _inst; }
    private LAFChooser() {}
    public JComponent panel() {
        if (_pnl==null) {
            setListData();
            final Object
                pS=pnl(VBHB," ","#JS","#JS"),
                pE=pnl(new Dimension(2*ICON_HEIGHT,EX*8), Customize.newButton(Customize.laf).t(null).setOptions(ChButton.ICON_SIZE));
            _pnl=pnl(CNSEW, jList,null,pS, pE);
            addActLi(this,Customize.customize(Customize.laf));
        }
        return _pnl;
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        if (q instanceof Customize && jList!=null) setListData();
        if (q==jList) {
            final Object v=jList.getSelectedValue();
            if (v!=null) {
                final String s=v.toString();
                for(String laf : custSettings(Customize.laf)) {
                    if (laf.endsWith(s) || laf.endsWith(s+"LookAndFeel")) {
                        setLAF(laf, true);
                        break;
                    }
                }
                jList.clearSelection();
            }
        }
    }
    private void setListData() {
        final String ss[]=custSettings(Customize.laf) , ll[]=new String[ss.length];
        for(int i=ss.length; --i>=0;) {
            ll[i]=delSfx("LookAndFeel",ss[i].substring(ss[i].lastIndexOf('.')+1));
        }
        jList.setListData(ll);
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
    public static Object getLaf(){ return dtkt()==null?null: UIManager.getLookAndFeel(); }
    public static boolean isSystemLAF() {
        return systProprty(SYSP_SYS_LAF).equals(clasNam(getLaf()));
    }
    public static boolean isCrossPlatform() {
        return eqNz(clasNam(getLaf()), UIManager.getCrossPlatformLookAndFeelClassName());
    }
    public static void setDefaultBG(Component c) {
        if (c!=null) {
            final Color bg=UIManager.getDefaults().getColor("Button.background");
            if (bg!=null) c.setBackground(bg);
        }
    }
    public static boolean setLAF(Object classOrClassName, boolean updateTree) {
        if (dtkt()==null) return false;
        Object s=classOrClassName;
        if (s==IF_NOT_ALREADY) {
            if (_initalized) return false;
            else s=METAL ? "M" : "S";
        }
        try {
            if (s==null) s=_current;

             if ("G".equals(s)) s="com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
             if ("m".equals(s)) s="com.sun.java.swing.plaf.motif.MotifLookAndFeel";
             if ("N".equals(s)) s="com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
             if ("M".equals(s)) s=UIManager.getCrossPlatformLookAndFeelClassName();

            if (s=="S") {
                final String pref=
                    isSystProprty(IS_LINUX)   ? "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel" :
                    isSystProprty(IS_WINDOWS) ? "com.sun.java.swing.plaf.windows.WindowsLookAndFeel" :
                    null;
                if (pref!=null) {
                    final Class c=name2class(pref);
                    if (c!=null) s=c.newInstance();
                }
                if (s=="S") s=systProprty(SYSP_SYS_LAF);
            }
            final String cn=deref(s,String.class);
            final int exclam=strchr('!',cn);
            Class cl=null;
            if (exclam>0) { /* not working */
                final URL u=url(cn.substring(0,exclam));
                final String cn1=cn.substring(exclam+1);
                try {
                    final ChClassLoader1 loader=new ChClassLoader1(new URL[]{u},ChUtils.class.getClassLoader());
                    cl=loader.findClass(cn1);
                } catch(Exception ex){}
            } else cl=name2class(cn);
            _current=(LookAndFeel) (s instanceof LookAndFeel ? s : cl!=null ? cl.newInstance() : null);
            if (_current!=null) UIManager.setLookAndFeel(_current);
            if (UIManager.get("ScrollBar.width")==null) UIManager.put("ScrollBar.width",intObjct(17));
            /* Java 1.6 NullPointerException at MetalScrollBarUI.installDefaults(MetalScrollBarUI.java:73) */
            if (updateTree) SwingUtilities.updateComponentTreeUI(ChFrame.mainFrame());
            mapIcon.put(IC_INFO,null);

        } catch(Throwable e){
            putln(RED_CAUGHT_IN," setLAF");
            stckTrc(e);
            return false;
        }
        putln("LAFChooser.setLAF now: ", getLaf());
        init();
        return true;
    }

    static void mayRestoreLaf() {
        if (_restoreLaf) {
            _restoreLaf=false;
            setLAF(null,false);
        }

    }
    @Override public void propertyChange(java.beans.PropertyChangeEvent evt) {
        if (UIManager.getLookAndFeel()!=_current && _current!=null) {
            putln(RED_WARNING,"LookAndFeel changed outside Strap. Revert ");
            _restoreLaf=true;
        }
    }
    public static void init() {
        if (_initalized) return;
        _initalized=true;
        UIManager.addPropertyChangeListener(instance());
        if (!isSystProprty(IS_MAC)) UIManager.put("Button.margin", new Insets(0, 2, 0, 2) );
        UIManager.put("ScrollBar.squareButtons",Boolean.FALSE);
        UIManager.put("ScrollBar.allowsAbsolutePositioning",Boolean.TRUE); /* See BasicScrollBarUI */
        UIManager.put("TextAreaUI",javax.swing.plaf.basic.BasicTextAreaUI.class.getName()); /* Sonst immer opaque */
        UIManager.put("TextFieldUI",javax.swing.plaf.basic.BasicTextFieldUI.class.getName()); /* Sonst immer opaque */
        UIManager.put("TextPaneUI",javax.swing.plaf.basic.BasicTextPaneUI.class.getName()); /* Sonst immer opaque */
        UIManager.put("TextField.border", BorderFactory.createEtchedBorder());
    }

    /* <<< static Utils <<< */
    /* ---------------------------------------- */
    /* >>> Testing UI  >>> */
    public static void showUiDefaults() {
        final UIDefaults defaults = UIManager.getDefaults();
        final ArrayList set=new ArrayList(999);
        final ArrayList vIcons=new ArrayList();
        for(Map.Entry e : entryArry(defaults)){
            final Object v=e.getValue(), k=e.getKey();
            if (v!=null) set.add(k+"    "+v+"   "+clasNam(v));
            if (k instanceof String && endWith("Icon",k)) vIcons.add(pnl(k, v));
        }
        final Object ss[]=set.toArray();
        Arrays.sort(ss);
        new ChFrame("UIManager.getDefaults()")
            .ad(pnl(CNSEW, scrllpn(new BA(0).join(ss,"\n\n")), null,null,scrllpn(pnl(VB,vIcons.toArray())) ))
            .shw(0);
    }
  

/* http://nadeausoftware.com/node/87 */
    /* http://filibeto.org/unix/macos/lib/dev/documentation/Java/Conceptual/Java14Development/Java14Development.pdf */
    /* http://www.java2s.com/Tutorial/Java/0120__Development/SystemsetPropertyapplelafuseScreenMenuBartrue.htm */

}

