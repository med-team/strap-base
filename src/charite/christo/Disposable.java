package charite.christo;
/**
Implemented by objects such as dialogs or plugins that can be disposed.
The dispose method should contain the cleanup.
@author Christoph Gille
*/
public interface Disposable {
    void dispose();
    Disposable[] NONE={};
}
