package charite.christo;
import java.awt.AWTEvent;
import java.io.File;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.KeyEvent.*;
/*
  @author Christoph Gille
*/
public class FilePopup implements TooltipProvider, ProcessEv, PopupMenuListener {
    public final static long NO_RENAME=1<<1, PROTEIN_FILE=1<<2;
    private File _fFocus;
    private final long _opt;
    private JPopupMenu _jpm;
    private Object _oo[];
    final Object JC_FOCUS[]={null};
    private static DialogExecuteOnFile _execFile;
    private static boolean _alreadyCM;
    @Override public void popupMenuCanceled(PopupMenuEvent e) {}
    @Override public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {}
    @Override public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        _fFocus=toFile(e);
        setLabel();
        _alreadyCM=true;
        for(int i=sze(_vTT); --i>=0;) {
            final JComponent c=getRmNull(i,_vTT, JComponent.class);
            if (c!=null && c.getToolTipText()==TTContextAndDnD) c.setToolTipText(null);
        }
    }
    private void setLabel() {
        boolean noDir=false;
        final BA ba=new BA(99);
        int n=0;
        for(File f : getFiles()) {
            if (f==null) continue;
            ba.a(f.getName()).a(' ',3);
            noDir=noDir || !isDir(f);
            n++;
        }
        setTxt(plrl(n,"Menu for %N "+(noDir?"file":"folder")+"%S"), LAB_TITLE[0]);
        setTxt(toStrg(ba), LAB_TITLE[1]);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Instance >>> */
    public FilePopup(long options) {
        _opt=options;
    }
    public void manageTT(JComponent c) {
        if (c!=null && !_alreadyCM && sze(c.getToolTipText())==0) {
            c.setToolTipText(TTContextAndDnD);
            adUniq(wref(c),_vTT);
        }
    }
    private static FilePopup _inst;
    public static FilePopup instance() { if (_inst==null) _inst=new FilePopup(0L); return _inst;}

    public void register(JComponent q) {
        evAdapt(this).addTo("km",q);
        setTip(this,q);
    }
    private static List _vTT=new ArrayList();
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> JPopupMenu >>> */
    private final Object LAB_TITLE[]={menuInfo(MI_HEADLINE,""), menuInfo(MI_LONG,"")};

    public JPopupMenu menu(File f) {
        if (f!=null) _fFocus=f;
        return popMenu();
    }
    public JPopupMenu popMenu() {
        if (_jpm==null) _jpm=jPopupMenu(0,"File-contextmenu",menuObjects(null,null));
        return _jpm;
    }

    public Object[] menuObjects(String icn, String title) {
        if (_oo==null) {
            final EvAdapter LI=evAdapt(this);
            final Object q=deref(JC_FOCUS[0]);

            _oo=new Object[]{
                icn==null?null:"i", icn,

                title,
                ChButton.dialogStringMatch(0L, JC_FOCUS,"FilePopup").i(IC_SEARCH),
                q instanceof ChJList ? toggl("FP").li(LI).t("Display full file ^path") : null,
                "-",
                LAB_TITLE[0],
                LAB_TITLE[1],
                new ChButton("L").li(LI).t("Display list").i(IC_LIST),
                " ",
                new ChButton("C").li(LI).t("Copy file path").i(IC_CLIPBOARD),
                0!=(_opt&NO_RENAME) ? null : new ChButton("MV").li(LI).t("^Rename ..."),
                0==(_opt&PROTEIN_FILE) ? null:"&v",
                new ChButton("V").li(LI).t("^View text").i(IC_SHOW),
                new Object[]{
                    "Open", "i", IC_DIRECTORY,
                    !Insecure.EXEC_ALLOWED ? null : new ChButton( "O").li(LI).t("^Open according to file type"),
                    !Insecure.EXEC_ALLOWED ? null : new ChButton( "X").li(LI).t("Open ^with ..."),
                    !Insecure.EXEC_ALLOWED ? null : new ChButton("FB").li(LI).t("File ^browser").i(systProprty(SYSP_ICON_FILE_BROWSER)),
                    new ChButton("ED").li(LI).t("Text-^editor").i(IC_EDIT),
                },
                " ",
                new Object[] {
                    "^Bioinformatics", "i", IC_3D,
                    new ChButton("CHAINS").li(LI).t("Split multi chain PDB file ...").i(IC_SCISSOR),
                    new ChButton("NCBI").li(LI).t("Split multipart NCBI file ...").i(IC_SCISSOR)
                },
                Customize.newButton(Customize.fileBrowsers,Customize.textEditors,Customize.pdfViewer,Customize.psViewer,Customize.javaSourceEditor,Customize.latexEditors,Customize.webBrowser),
                " ",
                new ChButton("RM").li(LI).t("File to ./^trash/ ...").i(IC_KILL)

            };

        }
        return _oo;
    }

    /* <<< JPopupMenu <<< */
    /* ---------------------------------------- */
    /* >>> getFiles >>> */

    private ChRunnable _hasFF;
    public FilePopup setFiles(ChRunnable hasFiles) { _hasFF=hasFiles; return this; }
    public File[] getFiles() {
        if (_hasFF!=null) return (File[])_hasFF.run(PROVIDE_FILES,null);
        final File ff[]=getSelFiles(get(0,JC_FOCUS)), f=_fFocus;
        return f!=null && idxOf(f,ff)<0 ? new File[]{f} : ff;
    }
    /* <<< getFiles <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final int id=ev.getID(), kcode=keyCode(ev), modi=modifrs(ev);
        final String cmd=actionCommand(ev);
        final File fMouse=toFile(ev);
        if (fMouse!=null) {
            if (isPopupTrggr(false,ev)) {
                if (isPopupTrggr(true,ev)) {
                    _fFocus=fMouse;
                    JC_FOCUS[0]=wref(q);
                    setLabel();
                    shwPopupMenu(menu(null));
                }
            } else if (doubleClck(ev)) {
                if (fMouse.isDirectory()) viewFile(drctry(fMouse), modi);
                else {
                    _fFocus=fMouse;
                    handleActEvt(this,ACTION_CLICKED_DOUBLE, modi);
                }
            }
        }
        final Object jc=deref(JC_FOCUS[0]);
        if (id==KEY_PRESSED && (kcode==VK_BACK_SPACE || kcode==VK_DELETE)) {
            final File[] ff=getSelFiles(q);
            if (ff.length>0 && ChMsg.yesNo(plrl(sze(ff),"Delete %N files?"))) {
                for(File f:ff) delFile(f);
                revalAndRepaintC(q);
            }
        }
        if (cmd!=null) {
            final File ff[]=getFiles();
            if (cmd=="C") toClipbd(new BA(999).join(ff));
            else if (cmd=="CHAINS") new PDB_separateChains().interactiveProcess(ff);
            else if (cmd=="NCBI") new NCBI_separateEntries().interactiveProcess(ff);
            else if (cmd=="MV") RenameFile.openFrame(ChFrame.AT_CLICK|ChFrame.PACK, ff);
            else if (cmd=="FP") {
                pcp(ChRenderer.KOPT_FULL_FILE_PATH, isSelctd(q)?"":null,jc);
                repaintC(jc);
            } else if (cmd=="X") {
                int count=0;
                String nn[]=new String[ff.length];
                for(File f : ff) nn[count++]=f.toString();
                if (_execFile==null) _execFile=new DialogExecuteOnFile(Customize.customize(Customize.openFileWith));
                _execFile.show(nn);
            } else if (q instanceof AbstractInteractiveFileProcessor) {
                final File ffCreated[]=((AbstractInteractiveFileProcessor)q).getCreatedFiles();
                if (sze(ffCreated)>0) new ChFrame("Created files").ad(scrllpn(new ChJList(ffCreated, ChJList.OPTIONS_FILES))).shw(0);
            } else if (cmd=="L") {
                new ChFrame("Files")
                    .ad(pnl(CNSEW,scrllpn(new ChJList(ff, ChJList.OPTIONS_FILES)),null,"<sub>Right click for context-menu</sub>"))
                    .shw(ChFrame.AT_CLICK);
            } else if (cmd=="FB") {
                final ArrayList<String> v=new ArrayList();
                for(File f :ff) adUniq(f.isDirectory() ? toStrg(f) : f.getParent(), v);
                for(String fn: strgArry(v)) viewFile(drctry(file(fn)), modi);
            }

            long fOpt=ChFrame.AT_CLICK;
            for(File f :ff) {
                if (cmd=="V") {
                    final boolean p=0!=(_opt&PROTEIN_FILE);
                    new ChTextView(f).tools().showInFrame(fOpt|ChFrame.STAGGER|ChFrame.SCROLLPANE, toStrg(f))
                        .cp(KOPT_CONTAINS_AA, p?"":null)
                        .cp(ACTION_DATABASE_CLICKED,p?"Load":null)
                        .underlineRefs(0);
                }
                if (cmd=="ED") edFile(-1, f, modi);
                if (cmd=="O") viewFile(f, modi);
                if (cmd=="RM") {
                    final String path=dirWorkingSet() ? "trash/" : dirSettings()+"/trash/";
                    final File trashed=file(path+f.getName());
                    mkdrsErr(file(path), "DELETED FILES");
                    if (canModifyNiceMsg(f) && canModifyNiceMsg(trashed)) {
                        if (trashed.exists()) delFile(trashed);
                        renamFile(f, trashed);
                        revalAndRepaintC(get(0,JC_FOCUS));
                    }
                }
                fOpt=ChFrame.STAGGER;
            }

            _fFocus=null;
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    private static Object _lastF;
    private static String _tip;
    public static BA tip(File f) {
        final BA ba=baTip();
        if (f==null) {}
        else if (f.isDirectory()) ba.a("Directory");
        else ba.format10(sze(f),10).a(" bytes <br>Modified: ").a(day(f.lastModified()));
        return ba.a("<br><sub>").a(TTContextAndDnD).a("</sub>");
    }
    public String provideTip(Object evOrComp) {
        final File f=toFile(evOrComp);
        if (_tip==null || f!=_lastF)  { _lastF=f; _tip=addHtmlTagsAsStrg(tip(f)); }
        return _tip;
    }

}

