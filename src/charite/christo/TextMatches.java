package charite.christo;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import java.awt.Color;
import java.awt.Rectangle;

public class TextMatches {
    private boolean _delimiterL[], _delimiterR[];
    private Object _needle, _colorOrIcon;
    private long _options;
    private int _fromTo[], _yy[],_mcYY, _count;
    public TextMatches() {}
    public TextMatches(long options, int[] fromTo, int count, Object c_or_i) {
        _colorOrIcon=
            c_or_i instanceof String ? iicon((String)c_or_i) :
            c_or_i;
        setFromTo(fromTo, count);
        _options=options;

    }
    public void setFromTo(int[] fromTo, int count) {
        clear();
        _fromTo=fromTo;
        _count=mini(sze(fromTo)/2, count);
    }

    public void add(int from, int to) {
        int ft[]=_fromTo;
        final int L=count();
        if (ft==null || ft.length<=L*2+1) _fromTo=ft=chSze(ft, L*4+100);
        ft[L*2]=from;
        ft[L*2+1]=to;
        _count=L+1;
    }

    public TextMatches clear() {
        _count=0;
        _yy=null;
        return this;
    }

    public void setDelimiters(boolean dL[], boolean dR[]) { _delimiterL=dL; _delimiterR=dR;}
    public void setNeedle(Object n) { _needle=n; }
    public void setOptions(long opt) { _options=opt; }
    public int[] fromTo() {
        final int[] ft=_fromTo;
        return ft==null ? NO_INT : ft;
    }
    public int[] fromToTrim() {
        int ft[]=fromTo();
        final int L=count();
        if (ft.length>L*2) _fromTo=ft=chSze(_fromTo, L*count());
        return ft;
    }
    public int count() { return mini(_count, sze(_fromTo)/2);}
    public boolean[] delimiterL() { return _delimiterL;}
    public boolean[] delimiterR() { return _delimiterR;}
    public long options() { return _options;}
    public Object needle() { return _needle;}
    public Object colorOrIcon() { return _colorOrIcon; }
    public Color color() {
        final Object o=_colorOrIcon;
        return o instanceof Color ? (Color)o : null;
    }
    private final static Rectangle R=new Rectangle();

    public int[] yy(Object jc) {
        final ChTextComponents tools=ChTextComponents.tools(jc);
        if (tools==null) return NO_INT;
        final BA ba=tools.byteArray();
        final int mc=tools.mc()+ba.mc();
        int[] yy=_yy;
        if (yy==null || _mcYY!=mc) {
            _mcYY=mc;
            final int rowH=ChTextComponents.getFixedRowHeight(jc);
            final int fromTo[]=fromTo();
            final int L=idxOfNotPositive(fromTo)/2;
            _yy=yy=new int[L];
            for(int i=0; i<L;  i++) {
                final int f=fromTo[2*i];
                if (f<0) break;
                if (rowH>=0) yy[i]=rowH*ba.idxToRow(f);
                else {
                    ChTextComponents.modelToView(f,jc,R);
                    yy[i]=y(R);
                }
            }
        }
        return yy;
    }
}
