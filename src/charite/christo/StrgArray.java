package charite.christo;

public class StrgArray /* implements Comparable */ {
    private final String[] _ss;
    public StrgArray(String...ss) {
        _ss=ss;
    }

    public String[] ss() {  return _ss; }
    @Override public int hashCode() {
        int hc=0;
        for(String s : _ss) if (s!=null) hc+=s.hashCode();
        return hc;
    }
    @Override public boolean equals(Object o) {
        final StrgArray sa=o instanceof StrgArray ? (StrgArray)o : null;
        final String[] ss=ss(), ss2=sa==null?null:sa.ss();
        final int L=ss().length;
        if (ss2==null||ss2.length!=L) return false;
        for(int i=L; --i>=0;) {
            if (ss[i]==null && ss2[i]!=null || !ss[i].equals(ss2[i])) return false;
        }
        return true;
    }
    @Override public String toString() { return new BA(99).join(ss()," ").toString();}

    /*
    @Override public int compareTo(Object o) {
        final StrgArray sa=o instanceof StrgArray ? (StrgArray)o : null;
        final String[] ss1=ss(), ss2=sa==null?null:sa.ss();
        if (ss2==null) return -1;
        final int L=ss1.length, L2=ss2.length;
        for(int i=0;i<L||i<L2; i++) {
            final String s1=i>=ss1.length?"":ss1[i];
            final String s2=i>=ss2.length?"":ss2[i];
            final int c=s1.compareTo(s2);
            if (c!=0) return c;
        }
        return 0;
    }
    */
}
