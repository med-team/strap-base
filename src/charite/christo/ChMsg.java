package charite.christo;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static javax.swing.JOptionPane.*;
/**
   Some modale message dialogs
   @author Christoph Gille
*/
public class ChMsg implements ChRunnable {
    public final static String
        KEY_RESULT_LABEL="CM$$KRL", KEY_RESULT_ICON="CM$$KRLI",
        SFX_COMPUTING=" - computing ...";

    private static String _title="Strap-dialog";
    public final static int SOUND_SUCCESS=3, SOUND_FAILURE=4;

    public static void setDefaultTitle(String s) {_title=s;}

    private ChMsg(){}
    private static ChMsg _inst;
    private static ChMsg instance() { if (_inst==null) _inst=new ChMsg(); return _inst;}

    private static Window mainF() {
        final ChFrame f=ChFrame.mainFrame();
        return f!=null && f.isVisible() ? f : null;
    }

    public static void msgDialog(long options, Object o) {
        if (o!=null) showMessageDialog(mainF(), addTags(options, o),_title,INFORMATION_MESSAGE);
    }

    public static boolean yesNo(Object o){ return yesNoSync(true, null, ChFrame.AT_CLICK, o);}
    public static boolean yesNoSync(boolean selectedReturnsTrue, AbstractButton sync, long options, Object msg){
        if (isSelctd(sync)==selectedReturnsTrue) return true;
        if (isEDT()) {
            nativeToFront(_title);
            return 0==JOptionPane.showConfirmDialog(mainF(), new Object[]{addTags(options,msg)},_title,YES_NO_OPTION,QUESTION_MESSAGE);
        }
        synchronized(sync!=null?sync:new Object()) {
            boolean ret;
            ret=isSelctd(sync) || new ChMsgDia(ChMsgDia.YES_NO, msg, null, options).i()==0;
            return ret;
        }
    }

    public static String askUserHome(String old) {
        String msg="<sub>";
        final String varHome=isWin() ? "HOMEPATH":"HOME";
        if (ChUtils.javaVsn()!=14) msg+="Environment $"+varHome+"="+ChEnv.get(varHome)+"<br>";
        msg+="System.getProperty("+USER_DOT_HOME+")="+sysGetProp(USER_DOT_HOME)+"<br>";
        msg+="</sub><br>Problem: System.getProperty(user.home) is not defined. <br>Please enter the user home path";
        return (String)showInputDialog(mainF(),addTags(0L,msg),_title,QUESTION_MESSAGE,null,null,old);
    }

    public static String input(Object msg, int size, String old) {
        nativeToFront(_title);
        return (String)showInputDialog(mainF(), addTags(ChFrame.AT_CLICK,msg),_title,QUESTION_MESSAGE,null,null,old);
    }

    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="RESULT_SHW") {
            ChFrame.frame("Computed results", pnlResults(), ChFrame.SCROLLPANE)
                .size(prefW(_resultPnl)+2*EM,prefH(_resultPnl)+4*ICON_HEIGHT)
                .shw(ChFrame.AT_CLICK);
        }
        if (id=="RESULT_L") toListOfResults(arg);
        if (id=="RESULT_D") computationDone((String)get(0,arg), get(1,arg));
        if (id=="AT_CLICK") {
            sleep(222);
            ChFrame.setLocationAtMouse(parentWndw(arg));
        }
        if (id=="SOUND" && isSelctd(buttn(TOG_SOUND)) && System.currentTimeMillis()-_whenSound>999) { sound(atoi(arg)); }
        if (id=="SPEAK") if (!speakNow((String)argv[0])) sound(atoi(argv[1]));

        return null;
    }

    public static int optionSync(Object sync, Object msg, String[] choice) {
        if (isEDT()) {
            nativeToFront(_title);
            return showOptionDialog(mainF(),addTags(ChFrame.AT_CLICK,msg),_title, DEFAULT_OPTION,QUESTION_MESSAGE,null,choice,null);
        }
        synchronized(sync!=null?sync:new Object()) {
            return new ChMsgDia(0, msg,choice,ChFrame.AT_CLICK).i();
        }
    }
    public static int option(Object msg, String[] oo) {
        return optionSync(null, msg, oo);
    }

    public static Object optionVal(Object msg,String[] oo) {
        return get(option(msg,oo), oo);
    }
    private static Object addTags(long options, final Object o) {
        if (o==null) return null;
        final Component c=o instanceof CharSequence ? pnl(o) : o instanceof Component ? (Component)o : null;
        if (c!=null) {

            addMoli(MOLI_REPAINT_ON_ENTER_AND_EXIT,c);
            ChDelay.repaint(c,2222);
            revalidate_whenMouseEnter1(true,c);
            for(int sleep=3; --sleep>=0;) startThrd(thrdRRR(thrdSleep(sleep*99+99),thread_setWndwState('F', c)));
        }
        if (c!=null) {
            final Runnable t=thread_setWndwState('T',c);
            startThrd(thrdRRR(thrdSleep(3333), t, thrdSleep(3333),t));
            if (0!=(options&ChFrame.AT_CLICK)) startThrd(thrdCR(instance(),"AT_CLICK",c));
            return c;
        }

        return o;
    }

    private static File _dirMsg;
    private static File dirMsgs() {
        if (_dirMsg==null)
            mkdrsErr(_dirMsg=file("~/@/savedMsgs"),"Here are messages that had been shown previously.");
        return _dirMsg;
    }

    private static Collection _vMsgShown=new HashSet();
    public static void savedMsg(CharSequence msg, String id, int version) {
        if (dtkt()==null||sze(msg)==0||!_vMsgShown.add(id)) return;
        final File dir=dirMsgs(), f=file(dir,id);
        for(int i=0; i<version; i++) delFile(file(dir,id+version));
        if (sze(f)>0) return;
        CharSequence sCR=strplc(0L,"\n","<br>",msg);
        wrte(f,sCR);
        sCR=toBA(sCR).a("<br><br>All messages can be reviewed by typing <font color=\"RED\"> Ctrl+M </font><br><br>&nbsp;");
        new ChJTextPane(sCR).tools().cp(KEY_WEST_PANEL,pnl(C(0xFFffFF),iicon(IC_INFO))).showInFrame("");
    }
    public static void showAllPreviousMsgs() {
        for(int i=3; --i>=0;) {
            delFile(file(dirMsgs(),i==0?"scrollV":i==1?"alignmentMouseWheel":i==2?"strapWeb":"FileBrowser"));
        }
        final String files[]=dirMsgs().list();
        if (sze(files)>0) {
            final BA sb=new BA(9999);
            for(String f:files) {
                sb.a(readBytes(file(dirMsgs(),f))).a("<br><hr>");
            }
            addHtmlTags(sb);
            new ChJTextPane(sb).tools().cp(KEY_WEST_PANEL,pnl(C(0xFFffFF),iicon(IC_INFO))).showInFrame("Previous messages");
        }
        if (_fErr!=null) { _fErr.show(); _fErr.setExtendedState(JFrame.NORMAL);}
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Error >>> */
    private static ChTabPane _tabbedErr;
    private static ChFrame _fErr;
    private final static Map<String,Component> MAP_ERROR=new HashMap();
    private static int iError;
    private static Runnable _runErrorF, _runMsgF;
    static Runnable thread_showError(CharSequence s,Component jc) {
        return thrdM("showError", ChMsg.class, new Object[]{s,jc});
    }
    public static void showError(CharSequence cs, Component cError) {
        final String s=toStrg(cs);
        if (!isEDT()) {
            inEdtLater(thread_showError(s, cError));
            return;
        }
        if (s==null && cError==null) {
            if (_fErr!=null) _fErr.show();
            return;
        }
        final String title="Strap warnings and errors. ";
        if (_fErr==null) _fErr=new ChFrame(title).ad(_tabbedErr=new ChTabPane(ChTabPane.LEFT)).shw(ChFrame.CENTER|ChFrame.ALWAYS_ON_TOP);
        if (_runErrorF==null) _runErrorF=thread_setWndwState('F', _fErr);
        inEDTms(_runErrorF,555);
        Component jc=s!="" && s!=null ? MAP_ERROR.get(s) : null;
        if (jc==null) {
            final Component cErrorSP=(cError!=null && child(cError,JScrollPane.class)==null) ? scrllpn(cError) : cError;
            final CharSequence html=addHtmlTags(s);
            jc= cError!=null ? cErrorSP : html!=s ? new ChJTextPane(html) : new ChTextView(html);
            TabItemTipIcon.set(toStrg(++iError),null,null,null,jc);
            _tabbedErr.addTab(0, jc);
            if (s!=null) MAP_ERROR.put(s.intern(),jc);
        }
        _tabbedErr.setSelectedComponent(jc);
        setWndwState('F',_fErr.shw(CLOSE_CtrlW_ESC|ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN));
        //_fErr.setSizeAccordingTo(jc,EM,2*EX);
    }
    /* <<< Error <<< */
    /* ---------------------------------------- */
    /* >>> Sound  >>> */
    public final static File[] _soundF=new File[5];

    private static long _whenSound;
    public static void sound(int id) {
        if (id==0 || !withGui()) return;
        File f=_soundF[id];

        if (f==null) {
            final URL u=url(URL_STRAP_DATAFILES+ (id==SOUND_FAILURE ? "failed" : "finished")+".wav");
            f=file(u);
            if (sze(f)<999) unattendedDownload(u);
            if (sze(f)<999) f=ERROR_FILE;
            _soundF[id]=f;
        }
        if (f!=ERROR_FILE) {
            try {
                java.applet.Applet.newAudioClip(url(f)).play();
                _whenSound=System.currentTimeMillis();
            } catch(Exception e){
                putln(RED_CAUGHT_IN,"ChMsg.sound ",e);
                delFile(f); _soundF[id]=ERROR_FILE;
            }
        }
    }
    public static void soundBG(int id) { if (withGui() && isSelctd(buttn(TOG_SOUND))) startThrd(thrdCR(instance(), "SOUND",intObjct(id))); }

    private static int _countSpeak;
    private static boolean speakNow(String txt) {
        String prg=isMac()?"/usr/bin/say" : "/usr/bin/espeak";
        if (!fExists(prg)) prg="/usr/bin/recite";
        if (fExists(prg)) {
            final ChExec ex=new ChExec(0L).setCommandLineV(prg,txt);
            _countSpeak++;
            ex.run();
            _countSpeak--;
            return ex.finished() && ex.exitValue()==0;
        }
        return false;
    }
    public static void speak(String txt) { speakBG(txt,0);}
    public static void speakBG(String txt, int soundFallBack) {
        if (isSelctd(buttn(TOG_SOUND)) && sze(txt)>0) {
            if (!Insecure.EXEC_ALLOWED) sound(soundFallBack);
            else if (_countSpeak<3) startThrd(thrdCR(instance(), "SPEAK",new Object[]{txt,intObjct(soundFallBack)}));
        }
    }
    /* <<< Sound <<< */
    /* ---------------------------------------- */
    /* >>> Msg >>> */
    private final static Map<String,ChFrame> mapMSG_FRAME=new HashMap();
    public static void msg(String m) {
        ChFrame f=mapMSG_FRAME.get(m);
        if (f==null) {
            final ChJTextPane tp=new ChJTextPane(m);
            tp.tools().showInFrame("Strap messgs ");
            mapMSG_FRAME.put(m, f=gcp(KEY_FRAME, tp, ChFrame.class));
        }
        if (_runMsgF==null) _runMsgF= thread_setWndwState('F',f);
        inEDTms(_runMsgF,555);
        f.shw(0);
    }

    private static void nativeToFront(String t) {
        if (Insecure.EXEC_ALLOWED && sze(new NativeTools().fileExecutable())>0) {
            setWndwStateLaterT('T', 999, t);
            setWndwStateLaterT('F', 999, t);
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Special Messeges  >>> */
    public static boolean noProteinsSelected(Object pp[]) {
        if (sze(pp)==0) {
            showError("No proteins selected",null);
            return true;
        }
        return false;
    }
    public static boolean noProtsSelectedButWantTakeAll(int nAll) {
        return ChMsg.yesNo("No proteins selected! \nDo you want to take all "+nAll+" proteins?");
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Disclaimer >>> */
    private static boolean _dShown;
    public static void disclaimerMsg(String title, String warning, String fPath) {
        if (!_dShown && !prgOptT("-noDisclaimer") ) {
            _dShown=true;
            final JFrame fAOT=new JFrame("");
            fAOT.getContentPane().add(new JLabel(title));
            fAOT.pack();
            setAot(true,fAOT);
            fAOT.show();

            final File f=new File( isWin() ? fPath.replace('/','\\'):fPath);
            Insecure.makeDirectories(new File(f.getParent()), true);
            if (sze(f)==0) {
                final AbstractButton cb=cbox("Do not ask again");
                final JTextArea ta=new JTextArea(warning+"\n\nThis message will be written to\n"+f);
                ta.setEditable(false);
                final Object msg=pnl(CNSEW,ta,"<h2>Disclaimer</h2>",cb);
                if (0!=showConfirmDialog(fAOT,msg,"Disclaimer",YES_NO_OPTION, QUESTION_MESSAGE)) {
                    System.exit(1);
                }
                if (cb.isSelected()) wrte(f,warning);
            }
            fAOT.dispose();
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> BlastResult >>> */
    private static Container _resultPnl;
    private static int _resultCount;
    private static Object[] _results=new Object[30];
    private final static ChButton[] _resultBb=new ChButton[_results.length];
    private static JComponent _butCompRes;

    public static Runnable runShowResult() {
        return thrdCR(instance(),"RESULT_SHW");
    }
    public final static ChButton butShowComputedResults() {
        return new ChButton("Computed results").r(runShowResult());
    }
    private static Container pnlResults() {
        final String title="Computation results";
        if (_resultPnl==null) {
            _resultPnl=pnl(VB,
                          title,
                           "The last  "+_results.length+" results will be listed here.",
                           " "
                          );
            toLogMenu(_butCompRes=butShowComputedResults(), null, null);
        } else revalAndRepaintCs(_resultPnl);
        ChDelay.highlightButton(buttn(BUT_LOG), 5555);
        ChDelay.highlightButton(_butCompRes, 5555);
        return _resultPnl;
    }

    public static void toListOfResults(Object blaster) {
        final String title=gcps(KEY_RESULT_LABEL,blaster);
        if (title==null || cntains(blaster,_results) || dtkt()==null) return;
        if (!isEDT()) inEdtLater(thrdCR(instance(),"RESULT_L",blaster));
        else {
            if (cntains(blaster,_results)) return;
            final ChButton butCtrl=ChButton.doCtrl(blaster);
            if (butCtrl!=null) {
                final Container pnl=pnlResults();
                final int i=_resultCount++ % _results.length;
                final Object icn=gcp(KEY_RESULT_ICON,blaster);
                butCtrl.setOptions(ChButton.HEIGHT_I).t(_resultCount+" "+title+SFX_COMPUTING).i(icn).tt("Started "+new Date()).fg(0x8800);
                pnl.add(butCtrl);
                if (_resultBb[i]!=null) pnl.remove(_resultBb[i]);
                _results[i]=blaster;
                _resultBb[i]=butCtrl;
            }
        }
    }
    public static void computationDone(String txt, Object blaster) {
        final String title=gcps(KEY_RESULT_LABEL,blaster);
        if (dtkt()==null || title==null || !cntains(blaster,_results)) return;
        if (!isEDT()) inEdtLater(thrdCR(instance(),"RESULT_D",new Object[]{txt,blaster}));
        else {
            final int i=idxOf(blaster,_results);
            final ChButton b=i<0?null:_resultBb[i];
            if (b!=null) {
                b.t( delSfx(SFX_COMPUTING,getTxt(b))+" "+toStrgN(txt)).tt("<pre>"+b.getToolTipText()+"\nFinished  "+new Date()+"</pre>");
                setFG(txt!=null?0:0x880000, b);
            }
        }
    }

    private final static Map<String,String> _askPermIns=new HashMap();
    public static boolean askPermissionInstall(Object what1, Object what2) {
        if (!LINUX_PACKAGE || what1==null&&what2==null) return true;
        final String key=toStrgIntrn(new BA(99).and(what1," ").a(what2));
        synchronized(key) {
        String prev=_askPermIns.get(key);
        if (prev==null) {
            final Object
                msg=pnl(VBHB,
                        "Warning: Strap asks for permission to install the external Software.",
                        " ",
                        !LINUX_PACKAGE?null:
                        pnl(VBHB,
                            "Say \"no\" unless you know the risks.",
                            fExists(GUI_BINARY)? "It is risky to install software from a location other than the "+(DEBIAN_PACKAGE?"Debian":"Suse")+" repository." :
                                "Better install the package \"strap\" and restart Strap with "+GUI_BINARY
                            ),
                        " ",
                        pnl(HBL, "Without this software, some non-essential functionality is missing."),
                        " ",
                        what1,
                        what2
                        );
            _askPermIns.put(key,prev=yesNo(msg)?"y":"n");
        }
        return prev=="y";
        }
    }
}
