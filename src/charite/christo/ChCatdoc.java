package charite.christo;
import static charite.christo.ChUtils.*;
import java.io.File;
/*
Source: http://blogstatic.micropledge.com/2009/09/catdoc-0.94.2-win32.zip
http://poi.apache.org/
*/
public class ChCatdoc  {

    private  static File _path[];
    public BA docAsBytes(File fWord) {
        final String PATHS="/usr/bin/catdoc /bin/catdoc /usr/local/bin/catdoc";
        Object fExe="catdoc";
        if (isWin()) {
            final BasicExecutable aex=new BasicExecutable("catdoc");
            aex.setBinaryPackageURLs(BasicExecutable.DEFAULT_BINARY);
            aex.initExecutable();
            fExe=aex.fileExecutable();
        } else {
            if (_path==null) {
                final String pp[]=splitTokns(PATHS);
                _path=new File[pp.length];
                for(int i=pp.length; --i>=0;)  _path[i]=file(pp[i]);
            }

        }
        final ChExec ex=new ChExec(ChExec.STDOUT);
        ex.setCommandLineV(fExe,fWord).run();
        BA stdout=null;
        if (ex.couldNotLaunch()) {
            if (!isWin()) {
                final String msg=
                    isMac() ?
                    "Either save the Winword file (ending .doc)  as .txt or install CatDoc.\n\n"+
                    "The program CatDoc is looked for in the following locations: "+PATHS :
                    "Please install the program CatDoc for reading Winword files.\n\n"+installCmdForPckgs("catdoc");
                error(msg);
            }
        } else stdout=ex.getStdoutAsBA();
        ex.dispose();
        return stdout;
    }
}

