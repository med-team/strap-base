package charite.christo;

/**
Provides a GUI where the user can change objects.
The user can open the panel by pressing a tool button <i>ICON:ChUtils#IC_CONTROLPANEL</i>
Compare {@link HasSharedControlPanel HasSharedControlPanel} which provides a control panel for a group of objects.

@author Christoph Gille
*/
public interface HasControlPanel {
    String CP_EXISTS="";
    /**  For !real return CP_EXISTS. Else returns a java.awt.Container. */
    Object getControlPanel(boolean real);

}
