package charite.christo;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import java.util.List;
/**
   @author Christoph Gille
*/
public class JListModel implements ComboBoxModel {
    private final List _list;
    public JListModel(List li) { _list=li;}
    public int getSize() { return _list.size();}
    public Object getElementAt(int i) { try { return _list.get(i); } catch(IndexOutOfBoundsException ex){return null;} }

    public void addListDataListener(ListDataListener l) { }
    public void removeListDataListener(ListDataListener l) { }

    private Object _selItem;
    public Object getSelectedItem() {
        if (_selItem==null || !_list.contains(_selItem)) {
            if (getSize()>0) _selItem=getElementAt(0);
        }
        return _selItem;
    }
    public void setSelectedItem(Object o) { _selItem=o;}

}
