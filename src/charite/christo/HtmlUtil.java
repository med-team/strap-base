package charite.christo;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class HtmlUtil {

    /* frequenently changing tags first */
    public final static int  S=1<<2, U=1<<4, B=1<<5, I=1<<6;
    private final static int FONT=1<<3, A=1<<7,  TAGS=8;
    private final static String TAG[]={null,null , "S","FONT", "U","B","I", "A" };
    private String _tt;
    private int _fg, _bg=0xFFffFF, _current, _ttTag;

    public BA reset(BA sb) {
        setStyles(-1,-1,-1, "", "", null, sb);
        return sb;
    }
    public void setStyles(int style0, int foregnd, int backgnd, String url, String attribute, String tt, BA sb) {
        int current=_current;
        final int fg=foregnd&0xFFffFF, bg=backgnd;
        final boolean colorChange=fg!=_fg || bg!=_bg && bg!=-1;
        final int style=style0==-1 ? 0 : style0 | (sze(url)>0?A:0) |  (colorChange ? FONT:0) | (_current&FONT);

        // final boolean debug=foregnd==0xff01;
        final boolean debug=false;
        if (debug) {
            putln(Integer.toBinaryString(_current)+" style="+Integer.toBinaryString(style)+" setStyles "+style0+" foregnd="+Integer.toHexString(foregnd)+"  "+Integer.toHexString(backgnd)+" url="+url+"  attribute="+attribute+" tt="+tt);
        }
        for(int i=0;i<TAGS; i++) {
            final int bit=1<<i;

            if ( (current&bit)!=(style&bit) ||  (bit==FONT && colorChange) || bit==_ttTag && !eq(tt,_tt)) {
                if (debug) putln("change "+colorChange+"  "+Integer.toHexString(bg)+" "+Integer.toHexString(_bg));
                for(int j=0; j<=i; j++) {
                    if ( (current&(1<<j))!=0) {
                        final int spc=TAG[j].indexOf(' ');
                        sb.a("</").a(TAG[j],0, spc>0?spc:99).a('>');
                        current&=~(1<<j);
                        if ((1<<j)==FONT) _bg=_fg=-1;
                        if ((1<<j)==_ttTag) _tt=null;
                    }
                }
            }
        }
        if (style0==-1) {
            _current=0;
            return;
        }
        boolean tipAdded=false;
        for(int i=TAGS+1; --i>0;) {
            final int bit=1<<i;
            if ( ((style&bit)!=0) && (current&bit)==0) {
                sb.a('<').a(TAG[i]).and(" ",attribute);
                if (bit==FONT) {
                    sb.a(" color=\"#").aHex(_fg=fg,6).a('\"');
                    if (bg!=0xFFffFF && bg!=-1 || 0!=((style|_current)&A)) sb.a(" style=\"background-color: #").aHex(bg,6).a(";\"");
                    _bg=bg;
                }
                if (bit==A) {
                    final boolean js=!url.startsWith("javascript");
                    sb.a(" class=\"aPlain\" href=\"").a(!js?null:"javascript:shw('").a(url).a(!js?null:"');").a("\" ");
                }
                if (sze(tt)>0 && !tipAdded && !tt.equals(_tt)) {
                    attributeTooltip(_tt=tt,bg,sb);
                    _ttTag=bit;
                    tipAdded=true;
                }
                sb.a('>');
                current|=bit;
            }
        }
        _current=current;
        //putln(" style="+Integer.toBinaryString(style)+" _current="+Integer.toBinaryString(_current)+"  _fg="+_fg+"  fg="+fg+ANSI_GREEN+"   sb="+ANSI_RESET+sb);
    }

    private static BA attributeTooltip(String tooltip, int colorRGB, BA sb) {
        return sb.a(" onmouseover=\"tip(this,").a(tooltip).a(",'#").aHex(colorRGB,6).a("')\" ");
    }

}

