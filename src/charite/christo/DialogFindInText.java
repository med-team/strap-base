package charite.christo;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   views html and plain text
   @author Christoph Gille
*/
public class DialogFindInText implements Disposable, ProcessEv {
    private final static int
        COLORS[]={0x00ffff,0xffff00,0xff0000,0x00ff00,0xc0c0c0,0x0000ff,0xff00ff},
        N=7;

    private final EvAdapter LI=new EvAdapter(this);

    private final boolean _noAutoActivation[]=new boolean[N];
    private Object _textComponent;
    private JComponent _pMain, _pMsgOther;
    private ChButton _butSearch[]=new ChButton[N], _togDetails;
    private ChTextField[]
        _textField=new ChTextField[N],
        _labCount=new ChTextField[N],
        _tfDelim={new ChTextField(" \\t\\r\\n"), new ChTextField(" \\t\\r\\n")};

    private int _antsPos, _antsWidth=-1;
    private ChFrame _frame;
    private final TextMatches[][] _highlight=new TextMatches[N][7];

    private final ChButton
        bNext[]=new ChButton[N], bPrev[]=new ChButton[N], bFrst[]=new ChButton[N], bLast[]=new ChButton[N],
        CB_SKIP_BLANKS=toggl("Ignore space and dash").li(LI).tt("i.e. \"website\" is found in \"web site\" because the space is ignored"),
        CB_IGNORE_LEN_OF_BLANK=toggl("Ignore length of white space").li(LI).s(true),
        CB_DOT=toggl("Dots match any").li(LI).s(true),
        CB_CASE=toggl("Case sensitive").li(LI),
        CB_nucRC=toggl("DNA-RevCompl").li(LI),
        CB_nuc2aaRC=toggl("Aminos from DNA-RevCompl").li(LI),
        CB_nuc2aaFwd=toggl("Aminos from DNA").li(LI),
        CB_Delim=toggl("").li(LI);

    {
        final String tt="Entering a nucleotide sequence as search String also searches for the translated  AA sequences of the 3 reading frames";
        CB_nuc2aaFwd.tt(tt);
        CB_nuc2aaRC.tt(tt);
    }

    public DialogFindInText setTextComponent(JComponent tc) {
        final Object ref=wref(tc);
        if (_textComponent!=ref) {
            rmHL(_textComponent);
             getMap().put(this,_textComponent=ref);
            for(int i=N; --i>=0;) highlight(i);
        }
        if (_togDetails!=null) {
            _togDetails.s(false);
            ChTextComponents.setAssociateFind(false, null);
            if (parentC(_pMsgOther)!=null) {
                rmFromParent(_pMsgOther);
                _frame.ad(_pMain).setVisible(true);
            }
        }
        return this;
    }
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource(), tc=deref(_textComponent);
        final String cmd=actionCommand(ev);
        final int id=ev.getID();
        final ChTextComponents tools=ChTextComponents.tools(tc);
        //putln((id==ActionEvent.ACTION_PERFORMED)+"DialogFindInText tc="+tc);
        if (id==ActionEvent.ACTION_PERFORMED) {
            final boolean isDelim=CB_Delim.s();
            _tfDelim[0].setEnabled(isDelim);
            _tfDelim[1].setEnabled(isDelim);
            boolean enableDisable=false;
            for (int row=N; --row>=0;) {
                final ChButton tog=_butSearch[row];
                if (q==_textField[row] && !_noAutoActivation[row] && !tog.s() && 1<sze(_textField[row].getText())) { tog.s(_noAutoActivation[row]=true);}
                if (q==tog) _noAutoActivation[row]=true;
                if (q==tog || tog.s() && ( q==_textField[row] || q==CB_DOT || q==CB_CASE || q==CB_SKIP_BLANKS || q==CB_IGNORE_LEN_OF_BLANK || q==CB_nuc2aaFwd || q==CB_nuc2aaRC || q==CB_nucRC || q==CB_Delim || q==CB_IGNORE_LEN_OF_BLANK || q==_tfDelim[0]||q==_tfDelim[1] )) {
                    highlight(row);
                }
                if (q==bLast[row] || q==bFrst[row] || q==bNext[row] || q==bPrev[row]) {
                    final int pos=q==bLast[row] ? MAX_INT : q==bFrst[row] ? -1 :  _antsPos;
                    int thisFrom=-1, thisTo=-1, thisIdx=-1;
                    final TextMatches[][] highlight=_highlight;
                    if (highlight!=null) {
                    for(TextMatches  h : highlight[row]) {
                        if (h==null) continue;
                        final boolean fwd=q==bFrst[row] || q==bNext[row];
                        final int fromTo[]=h.fromTo();
                        /* otherwise, (-(insertion point) - 1) */
                        final int idx0=Arrays.binarySearch(fromTo, pos + (fwd?2*maxi(_antsWidth, 1):-2));
                        final int idx= (idx0<0 ? maxi(0, -idx0-(fwd?1:2)) : idx0)&~1;
                        if (idx<fromTo.length) {
                            thisFrom=fromTo[idx];
                            thisTo=fromTo[idx|1];
                            thisIdx=idx;
                        }
                    }
                    }
                    if (thisFrom>=0 && thisTo>=0) {
                        _labCount[row].t((1+thisIdx/2)+"/"+count(row));
                        tools.marchingAnts(ANTS_SEARCH, _antsPos=thisFrom, thisTo);
                        _antsWidth=thisTo-thisFrom;
                    }
                    enableDisable=false;
                }
            }
            if (cmd=="O") {
                if (_pMsgOther==null) LI.addTo("km", _pMsgOther=pnl("Click a text component.<br>The search dialog will then be applied to this component."));
                rmFromParent(_pMain);
                _frame.ad(_pMsgOther).setVisible(true);
                _pMsgOther.requestFocus();
                ChTextComponents.setAssociateFind(false,this);
            }
            if (cmd=="FOCUS") ChTextComponents.setAssociateFind(true,isSelctd(q)?this:null);

            if (enableDisable) enableDisable();
        }
        if (q==_pMsgOther && (id==KeyEvent.KEY_PRESSED || id==MouseEvent.MOUSE_PRESSED)) setTextComponent((JComponent)tc);
    }

    void setPosition(int pos) { _antsPos=pos; _antsWidth=-1; }
    private void highlight(int row) {
        if (_butSearch[row]==null) return;
        final Object tc=deref(_textComponent);
        final ChTextComponents tools=ChTextComponents.tools(tc);
        if (tools==null) return;
        final TextMatches[] hh=_highlight[row];
        for(int j=hh.length; --j>=0;) {
            tools.removeHighlight(hh[j]);
            hh[j]=null;
        }

        if (isSelctd(_butSearch[row])) {
            final String needle=_textField[row].getText();
            if (needle!=null && needle.length()==0) {
                repaintC(tc);
                repaintC(getSB('V', tc));
            } else if (needle!=null) {
                final long mode=(CB_CASE.s()?0:HIGHLIGHT_IGNORE_CASE) | (CB_SKIP_BLANKS.s()?HIGHLIGHT_SKIP_BLANKS:0) | (CB_IGNORE_LEN_OF_BLANK.s()?HIGHLIGHT_IGNORE_LEN_OF_BLANK:0) | (CB_DOT.s()?HIGHLIGHT_DOT_MATCHES_ANY:0);
                final String dL=CB_Delim.s() ? rplcToStrg("\\t","\t", rplcToStrg("\\r","\r", rplcToStrg("\\n","\n", _tfDelim[0].getText()))) : "";
                final String dR=CB_Delim.s() ? rplcToStrg("\\t","\t", rplcToStrg("\\r","\r", rplcToStrg("\\n","\n", _tfDelim[1].getText()))) : "";
                hh[0]=tools.highlightOccurrence(needle, chrClas(dL) , chrClas(dR),mode|HIGHLIGHT_REPAINT|HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(COLORS[row]));
                if (CB_DOT.s() || CB_SKIP_BLANKS.s()) {
                    final String trim=needle.trim();
                    if (nxt(-ACTGUN,trim)<0) {
                        final byte[] seq=trim.getBytes();
                        final boolean isTranslated[]=new boolean[seq.length];
                        final long m=HIGHLIGHT_IGNORE_CASE|HIGHLIGHT_SKIP_BLANKS|HIGHLIGHT_UPDATE_IF_TEXT_CHANGES;
                        for(int frame=3; --frame>=0;) {
                            for(int j=seq.length; --j>=0;) isTranslated[j]= (j>=frame);
                            if (CB_nuc2aaFwd.s()) hh[1+frame]=tools.highlightOccurrence(DNA_Util.translateTriplet2aaCompact(seq,isTranslated), null,null,m, C(COLORS[row]));
                            if (CB_nuc2aaRC.s() || CB_nucRC.s()) {
                                final byte[]
                                    revCompl=DNA_Util.reverse(DNA_Util.complement(seq,MAX_INT),MAX_INT),
                                    txt=CB_nuc2aaRC.s() ? DNA_Util.translateTriplet2aaCompact(revCompl,isTranslated) : revCompl;
                                hh[1+3+frame]=tools.highlightOccurrence(txt, null,null,m, C(COLORS[row]));
                            }
                        }
                    }
                }
            }
            enableDisable();
        }

        tools.repaintLater(0);
    }
    public void showInFrame(long options) {
        final Object tc=deref(_textComponent);
        if (tc==null) return;
        final boolean isAA=gcp(KOPT_CONTAINS_AA,tc)!=null;
        if (gcp(ChTextView.KOPT_SEARCH_SKIP_BLANKS,tc)!=null) CB_SKIP_BLANKS.s(true);

        final long opts=ChFrame.PACK|CLOSE_CHILDS|CLOSE_CtrlW|options|(_frame==null?ChFrame.ALWAYS_ON_TOP:0);
        if (_frame==null) {
            final JComponent
                panel=pnl(new GridLayout(N,1)),
                pDetails=pnl(VB,
                            pnl(HBL,CB_SKIP_BLANKS.cb(), CB_IGNORE_LEN_OF_BLANK.cb()),
                            pnl(HBL,CB_CASE.cb(),CB_DOT.cb()),
                            isAA?pnl(CB_nucRC.cb(), CB_nuc2aaFwd.cb(),CB_nuc2aaRC.cb()):
                            pnl(HB,"#TB Left and right delimiter characters", CB_Delim.cb(), _tfDelim[0], _tfDelim[1]),
                            pnl(HBL,new ChButton("O").t("Apply to another text component").li(LI),
                                toggl("FOCUS").t("Apply to text component that has the focus").li(LI).cb()
                                )
                            ),
                pSouth=pnl(VB,pnl(HBL,_togDetails=toggl().doCollapse(pDetails),"Options"), pDetails);
            _pMain=pnl(CNSEW,panel,null,pSouth);
            _frame=new ChFrame("Find").ad(_pMain);
            for(int i=0; i<N;i++) {
                final ChTextField tf=_textField[i]=new ChTextField("", ULREFS_DISABLE).cols(30,false,false).li(LI);
                final ChTextComponents tools=tf.tools();
                tools.saveInFile("DialogFindInText_"+gcp(KEY_TITLE,tc)+"_"+i);
                if (i==N-1) pcp(KEY_IF_EMPTY,"Enter search String here",tf);
                _butSearch[i]=toggl("highlight").li(LI).s("".equals(tf.toString()));
                (_labCount[i]=new ChTextField().cols(9,true,true)).setEditable(false);
                final long opt=ChButton.MAC_TYPE_ICON|ChButton.DISABLED;
                bNext[i]=new ChButton(opt, ">" ).li(LI);
                bPrev[i]=new ChButton(opt, "<" ).li(LI);
                bFrst[i]=new ChButton(opt, "|<").li(LI);
                bLast[i]=new ChButton(opt, ">|").li(LI);
                panel.add(pnl(CNSEW,tf,null,null,pnl(HB,bFrst[i],bPrev[i],monospc(_labCount[i]),bNext[i],bLast[i]),_butSearch[i].cb()));
                tf.tools().highlightBlanks();
            }
            for(ChTextField tf:_tfDelim) {
                tf.tools().highlightBlanks();
                tf.li(LI)
                    .tt("Enter character sets   left / right of the match.<br>The \\t denotes  the tabulator and  \\r and \\n denote different line separators.")
                    .setEnabled(false);
            }
        }
        enableDisable();
        _frame.shw(opts);
        _textField[N-1].requestFocus();

    }
    private int count(int row) {
        int count=0;
        for(TextMatches h : _highlight[row]) if (h!=null)  count+=h.fromTo().length;
        return count/2;
    }
    public void enableDisable() {
        int sum=0;
        for(int row=0;row<N;row++) {
            if (_textField[row]==null) continue;
            final int c=count(row);
            _labCount[row].t(c==0 ? "" : baTip().format10(c,8).toString()).setBackground(C(COLORS[row]));
            bNext[row].enabled(c>0);
            bPrev[row].enabled(c>0);
            bFrst[row].enabled(c>0);
            bLast[row].enabled(c>0);
            sum+=c;
        }
        if (sum==0) setMarchingAnt(ANTS_SEARCH, derefC(_textComponent),(Rectangle)null,(Color)null,(Color)null);
    }
    public void dispose() {
        rmHL(_textComponent);
        if (_frame!=null) _frame.dispose();
        _butSearch=null;
        _textField=_labCount=_tfDelim=null;
        getMap().put(this,null);
    }

    private void rmHL(Object textComponent) {
        final ChTextComponents tools=ChTextComponents.tools(deref(textComponent));
        if (tools!=null) {
            boolean doRepaint=false;
            for(int i=0;i<N;i++) {
                for(int j=_highlight[i].length; --j>=0;) doRepaint|=tools.removeHighlight(_highlight[i][j]);
            }
            if (doRepaint) tools.repaintLater(0);
        }
    }
    private static Map<DialogFindInText, Object> _map;
    public static Map<DialogFindInText, Object> getMap() {
        if (_map==null) _map=new ChMap(0L, DialogFindInText.class, Object.class, 33);
        return _map;
    }
}
