package charite.christo;
import javax.swing.*;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
public class ChJScrollPane extends JScrollPane implements ProcessEv, PaintHook {
    private static ChButton _butTools;
    private int _opt;
    private final Object _c;
    private int _prefH, _countPaintHook;
    public final static String KEY_RUN_AS_TEXT="CJSP$$AT";
    final static Object KEY_SP=new Object();
    final Object WEAK_REF=newWeakRef(this);
    public ChJScrollPane(int opt, Component c) {
        super(c);
        _opt=opt;
        _c=wref(c);
        pcp(KEY_SP, WEAK_REF,c);
        if (0==(opt&SCRLLPN_ORIG_SB)) {
            final ChScrollBar
                vsb=new ChScrollBar(JScrollBar.VERTICAL),
                hsb=new ChScrollBar(JScrollBar.HORIZONTAL);
            addPaintHook(ChTextComponents.tools(c), vsb);
            setVerticalScrollBar(vsb);
            setHorizontalScrollBar(hsb);
        }
        adSelListener(c);
        getViewport().setBackground(c.getBackground());
        addPaintHook(this,derefJC(c));
        if (c instanceof ProcessEv) evAdapt((ProcessEv)c).addTo("kmw",getViewport());
        final TabItemTipIcon titi=TabItemTipIcon.getTiti(c);
        TabItemTipIcon.setTiti(titi,this);
    }

    public JComponent jc() { return derefJC(_c);}
    public void addOption(int o) { _opt|=o;}
    @Override public void setVerticalScrollBar(JScrollBar sb) {
        pcp(KEY_SP, WEAK_REF, sb);
        if (sb instanceof ChScrollBar) adUniq(wref(this), ((ChScrollBar)sb).SPANES);
        evAdapt(this).addTo("m", sb);
        super.setVerticalScrollBar(sb);
    }
    @Override public void setHorizontalScrollBar(JScrollBar sb) {
        pcp(KEY_SP, WEAK_REF, sb);
        if (sb instanceof ChScrollBar) adUniq(wref(this), ((ChScrollBar)sb).SPANES);
        evAdapt(this).addTo("m", sb);
        super.setHorizontalScrollBar(sb);
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Dimension >>> */
    private final Dimension _d=new Dimension();
    @Override public Dimension getPreferredSize() {
        final JComponent c=jc();
        Dimension d=prefSze(this);
        if (d==null && c!=null && 0!=(SCRLLPN_INHERIT_SIZE&_opt)) {
            final Dimension dim=c.getPreferredSize();
            d=_d;
            d.width=wdth(dim)+2*EX;
            d.height=hght(dim)+EX + (c instanceof JTable ? prefH(((JTable)c).getTableHeader()) : 0);
        }
        return d!=null ? d : super.getPreferredSize();
    }
    /* <<< Dimension <<< */
    /* ---------------------------------------- */
    /* >>> Paint  HashMap >>> */
    @Override public void paint(Graphics g) {
        super.paint(g);
      if (_jPopup!=null && isVisible()) {
            final int[] xywh=xywh();
            if (xywh!=null) SwingUtilities.paintComponent( g,_butTools,this, xywh[0], xywh[1], xywh[2], xywh[3]);
        }
    }
    private final static int _xywh[]=new int[4];
    private int[] xywh() {
        final JComponent c=jc(), vp=getViewport();
        if (vp==null || c==null) return null;
        final int xywh[]=_xywh, w=prefW(_butTools), h=prefH(_butTools);
        xywh[0]=mini(wdth(vp),  wdth(c)) -w-2;
        xywh[1]=mini(hght(vp),  hght(c))-h-2;
        xywh[2]=w;
        xywh[3]=h;
        return xywh;
    }

    public boolean paintHook(JComponent c, Graphics g, boolean after) {
        if (after && 0!=(_opt&SCRLLPN_INHERIT_SIZE) && _countPaintHook++>0 && prefH(c)!=_prefH) {
            _prefH=prefH(c);
            ChDelay.revalidate(new Object[]{getParent(),this}, 99);
        }
        return true;
    }
    @Override public void validateTree(){ try { super.validateTree(); } catch(Exception e){ stckTrc(e); } }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */

    static boolean openMenu(AWTEvent ev, Component c) {
        final int id=ev.getID();
        final Object q=ev.getSource();
        final ChJScrollPane sp=getSP(c);
        if (sp==null || sp._jPopup==null) return false;
        final JTable jt=deref(c, JTable.class);
        final boolean
            doRepaint= (id==MOUSE_MOVED)!=(sp._point!=null),
            clicked= id==MOUSE_CLICKED || id==MOUSE_PRESSED;

        if (doRepaint || clicked || id==MOUSE_MOVED) {
            final JViewport vp=sp.getViewport();
            final Point p=clicked || id==MOUSE_MOVED ? SwingUtilities.convertPoint((Component)q, point(ev), vp) : null;
            if (jt!=null && p!=null) p.y+=hght(jt.getTableHeader());
            sp._point=id==MOUSE_MOVED?p:null;
            if (doRepaint || clicked) {
                final int[] xywh=sp.xywh();
                if (xywh!=null) {
                    if (doRepaint) vp.repaint(xywh[0]-9,xywh[1]-9,9+xywh[2],9+xywh[3]);
                    if (clicked && xywh[0]<=x(p) && xywh[1]<=y(p)) {
                        if (id==MOUSE_CLICKED) shwPopupMenu(sp._jPopup);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private Point _point;
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final JComponent c=jc();
        final int id=ev.getID();
        final String cmd=actionCommand(ev);
        if (id==MOUSE_RELEASED && c!=null) c.repaint();

        if (cmd!=null) {
            if (c==null) return;
            final ChJTree jTree=deref(c, ChJTree.class);
            final JTable jTable=deref(c, JTable.class);
            final JList jl=deref(c, JList.class);
            if (cmd=="SORT" && jTree!=null) jTree.sort(atoi(gcp("SORT",q)));
            final boolean onlySelected=cmd=="TS";
            if (cmd=="TA" || onlySelected) {
                final BA ba=new BA(9999);
                if (jTree!=null) {
                    final int rc=jTree.getRowCount();
                    for(int iRow=0;iRow<rc;iRow++) {
                        final javax.swing.tree.TreePath tp=jTree.getPathForRow(iRow);
                        final Object o=tp.getLastPathComponent();
                        final boolean isSel=jTree.isPathSelected(tp);
                        if (onlySelected && !isSel) continue;
                        final int pc=tp.getPathCount();
                        final CharSequence rendTxt=rendererTxt(o);
                        ba.a('\n', pc<3?1:0)
                            .a(isSel && cmd=="TA" ? "[x]" : "   ")
                            .a(' ',1+pc*2)
                            .aln(rendTxt!=null ? rendTxt : getTxt(jTree.getCellRenderer().getTreeCellRendererComponent(jTree, o,false,false,false, iRow, false)));
                    }
                }

                if (jl!=null) {
                    final Object oo[];
                    if (onlySelected) oo=jl.getSelectedValues();
                    else {
                        final ListModel m=jl.getModel();
                        oo=new Object[sze(m)];
                        for(int i=0; i<oo.length; i++) oo[i]=m.getElementAt(i);
                    }
                    for(int i=0; i<oo.length; i++) {
                        if (oo[i]==null) continue;
                        final CharSequence rendTxt=rendererTxt(oo[i]);
                        ba.a(" - ").aln(rendTxt!=null ? rendTxt : getTxt(jl.getCellRenderer().getListCellRendererComponent(jl, oo[i], i, false,false)) );
                    }
                }

                if (jTable!=null) {
                    final int selected[]=jTable.getSelectedRows();
                    final Object oo[];
                    final int COLS=jTable.getColumnCount(), ROWS=jTable.getRowCount();
                    for(int col=0; col<ROWS; col++) {
                        if (col>0) ba.a('\t');
                        ba.a(jTable.getColumnName(col));
                    }
                    for(int col=0; col<COLS; col++) {
                        if (col>0) ba.a('\t');
                        ba.a('-',sze(jTable.getColumnName(col)));
                    }
                    ba.a('\n');
                    for(int row=0; row<ROWS; row++) {
                        if (onlySelected && idxOf(row, selected)<0) continue;
                        for(int col=0; col<COLS; col++) {
                            final Object v=jTable.getValueAt(row,col);
                            if (col>0) ba.a('\t');
                            final CharSequence rendTxt=rendererTxt(v);
                            ba.a(rendTxt!=null?rendTxt:
                                 getTxt(jTable.getCellRenderer(row,col).getTableCellRendererComponent(jTable, v, false, false, row,  col))
                                 );
                        }
                        ba.a('\n');
                    }
                }
                final ChRunnable r=gcp(KEY_RUN_AS_TEXT, this, ChRunnable.class);
                if (sze(ba)==0) error("No rows");
                else if (r!=null) r.run(KEY_RUN_AS_TEXT, ba);
                else shwTxtInW(CLOSE_CtrlW_ESC, "As text",ba);
            }
            if (cmd=="UUI") {
                if (isCtrl(ev))  c.revalidate();
                else if (isShift(ev)) c.doLayout();
                else ChDelay.updateUI(c,333);
            }
            if (jTree!=null) {
                if (cmd=="UT") jTree.updateKeepStateLater(0L, 99);
                if (cmd=="EN" || cmd=="CN") {
                    final javax.swing.tree.TreePath pp[]=jTree.getSelectionPaths();
                    if (sze(pp)==0) error("No rows selected");
                    else if (pp.length<999 || cmd=="CN" || ChMsg.yesNo("Really expand that many rows?")) {
                        for( javax.swing.tree.TreePath p:pp) {
                            if (cmd=="CN" && !jTree.isCollapsed(p)) jTree.collapsePath(p);
                            if (cmd=="EN" && !jTree.isExpanded(p))  jTree.expandPath(p);
                        }
                    }
                }
            }
            if (cmd=="INV" && jl!=null) {
                final int sel[]=jl.getSelectedIndices(), neu[]=new int[sze(jl)];
                for(int i=neu.length;--i>=0;) neu[i]=idxOf(i,sel)<0?i:MAX_INT;
                jl.setSelectedIndices(neu);
            }
        }
    }
    /* <<<  Event <<< */
    /* ---------------------------------------- */
    /* >>> Button  >>> */
    public final static int EXPAND_NODES=1, INV_SELECTION=2, UPDATE=4, AS_TXT=6;
    public ChJScrollPane addMenuItem(Object object) {
        if (object instanceof Object[]) for(Object o: ((Object[])object)) addMenuItem(o);
        JComponent but=null;
        if (object instanceof int[]) {
            final Object mmUpdate[]=new Object[sze(object)+1];
            for(int i=0; i<sze(object); i++) {
                final int id=((int[])object)[i];
                final Object add=
                    id==EXPAND_NODES ? new Object[]{
                    "Expand nodes",
                    new ChButton("EN").t("Expand tree nodes").tt("Show subnodes of tree-nodes that are not leafs"),
                    new ChButton("CN").t("Collapse nodes")
                }:
                id==INV_SELECTION ? new ChButton("INV").t("Invert selection ") :
                id==UPDATE ? new Object[]{
                    "Update",
                    new ChButton("UT").t("Update tree"),
                    new ChButton("UUI").t("Call updateUI")
                }:
                id==AS_TXT ? new Object[]{
                    "As text",
                    new ChButton("TA").t("All rows"),
                    new ChButton("TS").t("Selected rows")
                } : null;
                //new ChButton(toStrg(id)).t(t).tt(tt);
                for(Object o : oo(add)) if (o instanceof ChButton) ((ChButton)o).li(evAdapt(this));
                addMenuItem(add instanceof Object[] ? jMenu(oo(add)) : add);
            }
            if (countNotNull(mmUpdate)>0) { mmUpdate[0]="Update"; but=jMenu(mmUpdate); }
        } else but=derefJC(object);
       if (but!=null) {
           menu().add(but instanceof ChButton ? ((ChButton)but).mi(null) : but);
       }
       return this;
    }
    public ChJScrollPane addMenuItemSort(int...types) {
        final Object mm[]=new Object[types.length+1];
        mm[0]="Sort";
        for(int i=0; i<types.length; i++) {
            final int t=types[i];
            final String txt=
                t==COMPARE_SIZE ? "By size" :
                t==COMPARE_ID ? "By id" :
                t==COMPARE_NAME ? "By name" :
                t==COMPARE_WHEN_MODIFIED ? "By modification time" :
                t==COMPARE_WHEN_CREATED ? "By creation time" :
                t==COMPARE_ALPHABET ? "Alphabetically" :
                t==COMPARE_ALPHABET_IC ? "Alphabetically case insensitive"  :
                t==COMPARE_REVERT ? "Revert order"  :
                "Sort";
            mm[i+1]=new ChButton("SORT").t(txt).li(evAdapt(this)).cp("SORT",intObjct(t));
        }
        addMenuItem(jMenu(mm));
        return this;
    }
    private JPopupMenu _jPopup;
    private JPopupMenu menu() {
        if (_butTools==null)  (_butTools=new ChButton(ChButton.MAC_TYPE_ICON,"\u25bc").tt("Menu of tools")).setMargin(new Insets(0,0,0,0));
        if (_jPopup==null) {
            final JComponent c=jc();
            _jPopup=new JPopupMenu("Scrollpane");
            if (c instanceof JList || c instanceof JTree) _jPopup.add(menuInfo(0,"Tools for item list"), 0);
            evAdapt(this).addTo("Mkmw",c);
        }
        return _jPopup;
    }
    /* <<< Button  <<< */

}
