package charite.christo;
import java.net.URL;
import java.io.File;
import java.io.InputStream;
import static charite.christo.ChUtils.*;
/**HELP
Using standard methods of Java, certain FTP-addresses do not work.
These problems are solved with  http://www.enterprisedt.com/products/edtftpj/.
<br>
<b>License of edtFTPj/Free:</b>  WIKI:LGPL
*/
public class AnonymousFTPPROXY extends AbstractProxy {
    final static String FTP="Ftp";
    {
        setOptions(TRY_BOOT_CLASSLOADER);

    }
    @Override public String getRequiredJars() { return "edtftpj-1.5.2.jar" ;}
    public boolean download(URL url, File f, ChRunnable log) {
        final ChRunnable i=(ChRunnable)proxyObject();
        final boolean success;
        if (i==null) success=false;
        else {
            final boolean[] bb={false};
            i.run(FTP, new Object[]{url,f,log, bb});
            success=bb[0];
        }
        if (!success) {
            try {
                if (log!=null) log.run(ChRunnable.RUN_APPEND,"AnonymousFTPPROXY: going to use Java API since  edtftpj failed\n");
                final InputStream is=url.openStream();
                if (f==null) { closeStrm(is); return is!=null;}
                cpy(is,f);
            } catch(Exception e){ if (log!=null) log.run(ChRunnable.RUN_APPEND, new Object[]{C(0xFF0000),e,C(0xFFffFF),"\n"}); return false;}
        }
        return f==null || f.length()>0;
    }

}

// http://www.nsftools.com/tips/JavaFtp.htm
