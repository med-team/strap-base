package charite.christo;
import java.awt.*;
import javax.swing.*;
import static javax.swing.SwingUtilities.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public final class ChSprite implements Runnable {
    public final static int SPEED_SINUS=1<<1;
    private final static int TEXT_MARGIN=4;
    private final Point _pDest;
    private final Object _txt, _canvasDest;
    private final Font font=getFnt(12, true, 0);
    private final int _w, _h, _options, _screenX0, _screenY0;
    private final float _mseconds;
    private int _lastScrX=MIN_INT, _lastScrY, _screenX, _screenY;
    public ChSprite(long options, Component cSrc, Point pSrc, Component cDest, Point pDest, String txt, float mseconds) {
        _canvasDest=wref(cDest);
        _pDest=pDest;
        _mseconds=mseconds;
        _txt=txt;
        _options=(int)options;
        if (txt instanceof String) {
            _h=2*TEXT_MARGIN+charH(font);
            _w=2*TEXT_MARGIN+strgWidth(font, (String)txt);
        } else _w=_h=10;
        final Point p=new Point(pSrc);
        convertPointToScreen(p, cSrc);
        _screenX0=x(p);
        _screenY0=y(p);
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> canvas >>> */
    private Component _prevCanvas;
    private Component canvas() {
        final Component
            c0=derefC(_canvasDest),
            cRoot=c0==null?null : getRootPane(c0),
            cDeep=cRoot==null?null : getDeepestComponentAt(cRoot, _screenX,_screenY);
        final Component c=cDeep instanceof JComponent || cDeep==null ? cRoot : cDeep;
        if (c==null) return null;
        _pCanvas.move(_screenX, _screenY);
        convertPointFromScreen(_pCanvas,c);
        if (_prevCanvas!=null && _prevCanvas!=cDeep) {
            ChDelay.repaint(_prevCanvas,99);
            _prevX=INT_NAN;
        }
        _prevCanvas=cDeep;
        return c;
    }
    /* <<<  canvas <<< */
    /* ---------------------------------------- */
    /* >>>  offImage >>> */
    private Image _im, _im2;
    private int _offX, _offY, _offW, _offH;
    private final Point _pCanvas=new Point(), pDestS=new Point();
    private int _prevX=INT_NAN, _prevY;
    private Image offImage() {
        final Component c=canvas();
        if (c==null) return null;
        Image im=_im;
        final int x=x(_pCanvas), dx=_prevX==INT_NAN?0:x-_prevX;
        final int y=y(_pCanvas), dy=                  y-_prevY;
        if (_prevX==INT_NAN || im==null ||
            x<=_offX || x+_w >=_offX+_offW ||
            y<=_offY || y+_h>=_offY+_offH) {
            if (im==null) {
                _im=im=c.createImage(_offW=3*_w, _offH=3*_h);
                _im2=c.createImage(_offW, _offH);
            }
            if (im==null) return null;
            _prevX=x;
            _prevY=y;
            final int BORDER=4;
            _offX=x + (dx>0?-BORDER : dx<0 ? _w- _offW+BORDER  : (_w-_offW )/2);
            _offY=y + (dy>0?-BORDER : dy<0 ? _h-_offH+BORDER  : (_h-_offH)/2);
            final Graphics g=im.getGraphics();
            g.translate(-_offX, -_offY);
            c.paint(g);
            g.translate(_offX, _offY);
            //if (myComputer()) g.setColor(C(0xFF0000)); g.drawRect(2,2,_offW-4,_offH-4);
        }
        return im;
    }
    /* <<< offImage <<< */
    /* ---------------------------------------- */
    /* >>>  offImage >>> */
    private void stroke() {
        final Component c=canvas();
        final Image im=offImage(), im2=_im2;
        final Graphics g=c==null?null:c.getGraphics(),g2=im2==null?null:im2.getGraphics();
        if (g==null || im==null || im2==null) return;

        g2.drawImage(im,0,0,c);
        final int x=x(_pCanvas)-_offX, y=y(_pCanvas)-_offY;
        if (_txt instanceof String) {
            final int ax=_screenX0-_screenX, ay=_screenY0-_screenY, transp=32+(int)(2*Math.sqrt(ax*ax+ay*ay));
            g2.setColor(transp>255 ? C(0xFFffFF): C(0xFFffFF,transp));
            g2.fillRoundRect(x+1, y+1, _w-2, _h-2, TEXT_MARGIN/2, TEXT_MARGIN/2);
            g2.setColor(C(0));
            g2.setFont(font);
            g2.drawString((String)_txt, x+TEXT_MARGIN, y+TEXT_MARGIN+charA(font));
        }
        g.drawImage(im2, _offX, _offY,   c);
    }
    public void run() {
        if (isEDT()) {
            stroke();
        } else {
            final Component cDest=derefC(_canvasDest);
            final long time0=System.currentTimeMillis();
            for(;;) {
                final long time=System.currentTimeMillis();
                pDestS.move(x(_pDest), y(_pDest));
                convertPointToScreen(pDestS, cDest);
                final double dx=x(pDestS)-_screenX0, dy=y(pDestS)-_screenY0, dist=Math.sqrt(  dx*dx+dy*dy);
                final long time1=(long)(time0+dist*_mseconds);
                if (time>time1) break;
                double part=(time-time0)/(time1-(double)time0);
                if (0!=(_options&SPEED_SINUS)) {
                    final double sin=Math.sin(Math.PI*(part-0.5));
                    part=0.5+sin/2;
                    part*=part;
                }
                _screenX=(int)(_screenX0+  dx*part)-_w/2;
                _screenY=(int)(_screenY0+  dy*part)-_h/2;
                if ( _lastScrX!=_screenX || _lastScrY!=_screenY || _prevX==INT_NAN) {
                    inEDT(this);
                    _lastScrX=_screenX;
                    _lastScrY=_screenY;
                }
                sleep(10);
            }
            ChDelay.repaint(canvas(),2222);
        }
    }
}

