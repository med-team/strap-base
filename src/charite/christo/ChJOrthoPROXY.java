package charite.christo;
import java.util.ArrayList;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
<b>Home:</b>http://www.inetsoftware.de/other-products/jortho<br>
<b>License of JOrtho:</b>  GPL<br>
*/
public class ChJOrthoPROXY extends AbstractProxy implements ChRunnable {
    public final static String JTEXTCOMPONENT="CJO$$jtc";
    public final static String DICT_FILE="dictionary_en.ortho", RUN_DOWNLOAD_WORDS="CJOP$$DW";
    private static ArrayList vCustomCollection=new ArrayList();
    @Override public String getRequiredJars() { return "jortho2.jar "+DICT_FILE;}
    public void register(javax.swing.text.JTextComponent tc) {
        final ChRunnable r=(ChRunnable)proxyObject();
        if (r!=null) {
            r.run("ADD", vCustomCollection);
            r.run(JTEXTCOMPONENT, tc);
        }
    }
    /**
       Argument:  An   String or HasName or ChRunnable
       or an Array or Collection of them.
    */
    public static void addWords(Object o) { vCustomCollection.add(o); }
    public static Runnable thread_addDictionaries(String ss[]) { 
        return thrdM("addDictionaries",ChJOrthoPROXY.class, new Object[]{ss}); 
    }
    public static void addDictionaries(String ss[]) {
        InteractiveDownload.downloadFilesIfNewer(ss);
        for(int i=sze(ss); --i>=0;) {
            addWords(splitTokns(readBytes(file(ss[i]))));
        }
    }

    private static String[] wordList;
    public Object run(String id, Object arg) {

        if (id==PROVIDE_WORD_COMPLETION_LIST || id==RUN_DOWNLOAD_WORDS) {
            final String url=URL_STRAP_DATAFILES+"english_words.txt.gz";
            final File fWords=file(url);
            if (id==PROVIDE_WORD_COMPLETION_LIST) {
                if (sze(wordList)==0 && sze(fWords)>0) wordList=readLines(fWords);
                //putln("wordList"+sze(wordList));
                //putln(wordList);
                return wordList;
            }
            if (id==RUN_DOWNLOAD_WORDS && wordList==null && sze(fWords)==0) {
                wordList=new String[0];
                putln("Going to download "+url);
                urlGet(url(url),99999);
            }
        }
        return null;
    }

}
