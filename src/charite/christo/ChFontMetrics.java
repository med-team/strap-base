package charite.christo;
import java.awt.*;
import java.util.*;
import java.awt.image.BufferedImage;
class ChFontMetrics {
    private ChFontMetrics(){}
    private final static Map[] MAPS=new Map[128];
    private final static byte[] TEST_BYTES="g2MDVQW".getBytes(); //3204
    public static Rectangle getCharBounds(char chr, final Font f) {
        Rectangle rect=null;
        if (chr<MAPS.length) {
            if (MAPS[chr]==null) MAPS[chr]=new HashMap();
            rect=(Rectangle)MAPS[chr].get(f.toString());
        }
        if (rect==null) {
            final FontMetrics fm=ChUtils.fntMet(f);
            final int
                charW=fm.charWidth('X'),
                charH=fm.getHeight(),
                charH2=2*charH+1, charW2=2*charW+1;
            final BufferedImage im=new BufferedImage(charW2,charH2,BufferedImage.TYPE_INT_ARGB);
            final Graphics g=im.getGraphics();
            g.setFont(f);
            int rgb[]=new int[charW2*charH2];
            Arrays.fill(rgb,0);
            im.setRGB(0,0, charW2,charH2, rgb, 0,charW2);
            g.setColor(Color.WHITE);
            if (chr==0) {
                for(int i=TEST_BYTES.length; --i>=0;) g.drawBytes(TEST_BYTES,i,1,0,charH);
            } else g.drawBytes(new byte[]{(byte)chr},0,1,0,charH);
            rgb=im.getRGB(0,0, charW2,charH2, rgb, 0,charW2);
            int top=0,bottom=0,right=0;
            for(int line=0,iRgb=0;line<charH2;line++) {
                for(int c=0; c<charW2; c++,iRgb++) {
                    if ((rgb[iRgb]&255)!=0) {
                        bottom=line;
                        if (top==0) top=line;
                        if (right<c) right=c;
                    }
                }
            }
            right=fm.charWidth('X');
            rect=new Rectangle(0,charH-top,right,bottom-top+1);
            if (chr<MAPS.length) MAPS[chr].put(f.toString(),rect);
            // if (chr=='\u2197') {
            //     final javax.swing.JLabel l=new javax.swing.JLabel(rect.toString());
            //     l.setIcon(new javax.swing.ImageIcon(im));
            //     l.setBackground(Color.GREEN);
            //     l.setOpaque(true);
            //     new ChFrame("ChFontMetrics")
            //         .ad(l)
            //         .shw(ChFrame.PACK|ChFrame.ALWAYS_ON_TOP);
            // }
        }
        return rect;
    }
}
