package charite.christo;
import java.lang.reflect.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Utility classes for Javadoc.
   @author Christoph Gille
*/
public class ChJavadoc implements ActionListener {
    private ChJavadoc(){}
    private static int count;
    private final static String[] PACKAGES=new String[100], URLS=new String[100];
    /** Classes with package have a Javadoc at url */
    public static void addJavadocUrl(String packagE, String url) {
        if (count<PACKAGES.length) {
            final String p=packagE.replace('.','/');
            int i=idxOfStrg(p,PACKAGES);
            if (i<0) i=count++;
            PACKAGES[i]=p;
            URLS[i]=new BA(99).a1('/').toString();
        }
    }
    static {
        addJavadocUrl("charite/", URL_STRAP+"javadoc/");
        addJavadocUrl("org/biojava/", "http://www.biojava.org/docs/api/");
        for(String s: new String[]{"java/","javax/","org/ietf/","org/omg/","org/w3c/","org/xml/"}) addJavadocUrl(s,"http://download.oracle.com/javase/6/docs/api/");
    }

    static String javadocUrl(String className) {
        if (className==null) return null;
        final String name=className.replace('.','/').replace('$','.');
        for(int i=0;i<count;i++) {
            final String p=PACKAGES[i];
            if (name.startsWith(p)) {
                final String base=URLS[i];
                return new BA(222).a(base).a(name).a(".html").toString();
            }
        }
        return null;
    }
    public static String javadocUrl(Member m) {
        final Class c=m!=null ? m.getDeclaringClass() : null;
        final String url=javadocUrl(nam(c));
        if (url!=null) {
            final BA sb=new BA(99).a(m).replace(0L,",", ", ");
            final int iThrows=strstr(") throws ", sb);
            if (iThrows>0) sb.replaceRange(iThrows+1,MAX_INT,"");
            int dot=-1;
            final int L=sb.length();
            for(int i=0; i<L; i++) {
                final char ch=sb.charAt(i);
                if (ch=='.') dot=i;
                if (ch=='(') break;
            }
            if (dot>0) sb.replaceRange(0,dot+1,"");
            sb.replaceChar('$','.'); // inner classes
            return sb.insert(0,"#").insert(0,javadocUrl(nam(c))).toString();
        }
        return null;
    }
    public static void javadocLink(String className,BA sb) {
        String javadoc=javadocUrl(className);
        if (javadoc!=null)  sb.a("<a href=\"").a(javadoc).a("\">").a(className, className.lastIndexOf('.')+1, MAX_INT).a("</a>");
    }

    final static Member findMember(Class cl, String sMember,String[] packages,Object[] returnValue) {
        if (sMember==null||cl==null) return null;
        final int open=sMember.indexOf('('),  close=sMember.indexOf(')');
        Exception exception=null;
        Member mem=null;
        if (open>0 && open<close) {
            final Object arg[];
            final Class cc[];
            if (chrAt(open+1, sMember)=='"') {
                arg=new Object[]{sMember.substring(open+2,close-1)};
                cc=new Class[]{String.class};
            } else if (open+1<close) {
                final String ss[]=sMember.substring(open+1,close).split(",");
                cc=new Class[ss.length];
                arg=new Object[ss.length];
                boolean classDescriptor=false;
                for(int i=cc.length; --i>=0;) {
                    final String s=ss[i];
                    if ("null".equals(s)) continue;
                    final boolean isF="false".equals(s), isT=isTrue(s);
                    if (isF || isT) { cc[i]=boolean.class; arg[i]=isF?Boolean.FALSE:Boolean.TRUE; }
                    else if ( (cc[i]=findClas(s,packages))!=null) classDescriptor=true;
                    else if (s.indexOf('(')<0) {
                        try {
                            final Field f=cl.getField(s);
                            if (f!=null) {
                                cc[i]=f.getType();
                                arg[i]=f.get(null);
                            }
                        } catch(Exception ex) {}
                    }
                    if (cc[i]==null) {
                        putln(" cl=",cl);
                        putln(" sMember=",sMember);
                        putln(" packages=",packages);
                        assrt();
                    }
                }
                if (classDescriptor && returnValue!=null) assrt();
            } else arg=cc=NO_CLASS;
            if (sMember.startsWith("new ")) {
                for(Constructor constr : cl.getConstructors()) {
                    if (isAssignableFrom(constr.getParameterTypes(),cc)) {
                        if (returnValue!=null) try {returnValue[0]=constr.newInstance(arg); } catch(Exception ex) {exception=ex;}
                        mem=constr;
                        break;
                    }
                }
            } else {
                final String name=sMember.substring(0,open);
                for(Method method : cl.getMethods()) {
                    if (method.getName().equals(name) && isAssignableFrom(method.getParameterTypes(),cc)) {
                        mem=method;
                        if (returnValue!=null)  {
                            try { returnValue[0]=method.invoke(null,arg); } catch(Exception ex) {exception=ex;}
                        }
                        break;
                    }
                }
            }
        } else {
            Field field=null;
            try {
                field=cl.getField(sMember);
                if (field!=null && returnValue!=null) returnValue[0]=field.get(null);
            } catch(Exception ex){ exception=ex;}
            mem=field;
        }
        if (exception!=null) {
            putln(RED_CAUGHT_IN+"ChJavadoc:\n  class=",cl);
            putln("  sMember=",sMember);
            putln("  Packages=",packages);
            stckTrc(exception);
        }
        return mem;
    }
    private static boolean isAssignableFrom(Class cc[], Class ccFrom[]) {
        if (cc.length!=ccFrom.length) return false;
        for(int i=cc.length; --i>=0;) {
            if (cc[i]!=null && ccFrom[i]!=null && !cc[i].isAssignableFrom(ccFrom[i])) return false;
        }
        return true;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  Popup Menu >>> */
    private static ChButton _bDoc, _bSrc, _bEdt;
    private static JPopupMenu _popup;
    private static JLabel _title=new JLabel();
    private static String _clazz, _vPackages[];
    private static ActionListener _inst;
    public static JPopupMenu menu(String clazz, String[] packages) {
        _clazz=clazz;
        _vPackages=packages;
        if (_popup==null) {
            _inst=new ChJavadoc();
            final Object oo[]={
                _title,
                _bDoc=new ChButton("Javadoc in Web-Browser").li(_inst),
                _bSrc=new ChButton("Source code").li(_inst),
                _bEdt=new ChButton("Edit source code").li(_inst)
            };
            _popup=jPopupMenu(0, "Menu for Java class",oo);
        }
        final File f=getJavaSrcFile(clas(clazz));
        _bSrc.setEnabled(hasJavaSrc(clazz));
        final String javadocUrl=ChJavadoc.javadocUrl(clazz);
        _bDoc.setEnabled( javadocUrl!=null);
        _bEdt.setEnabled(sze(f)>0);
        _title.setText(_clazz);
        return _popup;
    }
    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        if (q==_bDoc) visitURL(ChJavadoc.javadocUrl(_clazz),0);
        if (q==_bSrc) showJavaSource(_clazz, null);
        if (q==_bEdt) edFile(-1, getJavaSrcFile(clas(_clazz)), 0);

    }
}

