package charite.christo;
import java.io.File;
import java.net.URL;
import java.util.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP
<h2>Showing Abstracts</h2>

When the mouse hovers over a Pubmed reference of the form
\"PMID:19762348\", the abstract is downloaded automatically in the
background and shown in a certain text area.  The same works for
Uniprot identifiers of the form UNIPROT:Q9H936 or human Ensembl
references of the form ENSG00000103064.

<h2>Showing Full-text</h2>

The user might associate a full PDF text with a Pubmed ID. Those
Pubmed links are indicated by red background. The PDF can be looked at
by using the context menu (right click).  If the ALT-key is hold down
while the mouse is moving over a Pubmed ID, then not the abstract, but
the full text (without images) is shown.

<h2>The download directory</h2>
PDF-files are downloaded with the web browser into a certain directory.
The program needs to know this directory.
The  PDF file that has been created at last in this directory
is identified by the program and associated to the respective PMID.

<h2>Customizing the Browser</h2>

Many web browsers display PDF documents within the
Browser's frame.  With this behavior, the mechanism is not working.
The browser  must be configured such that
PDF files are downloaded into a specific directory and
displayed in an external PDF viewer such as WIKI:Evince or WIKI:Xpdf
or WIKI:Acroread.
<br><br>
In Firefox browser go to Menu-bar&gt;Edit&gt;Preferences.  The
preferences frame will open.  Click the Icon "Applications". A list
with file types appear.  Find the line "PDF document" and specify an
action. A good choice which gives maximum control is "Always ask".
<br><br>
In other web browsers customization works in a similar way.

@author Christoph Gille

 */
public final class ChPubmed implements ChRunnable, java.awt.event.ActionListener {
    public final static String ACTION_FULL_TEXT_ASSOCIATED="CPM$$FTA";
    public final static int LIST=0, PDF=1, ABSTRACT=2, UNIPROT=3, PDF2TXT=4, GO=5, AUTO=6, PDF_TITLE=7;
    private final static BA baTmp=new BA(999);
    private static Map<String,Runnable> mapThread=new HashMap();
    private static Map<String,Vector<String> > mapQueue=new HashMap();
    private static Map<String,HashSet<String> > mapRequests=new HashMap();
    private static ChButton bPdf2txt, togShared;
    private final static Map<String,File>[] mapPMID_Pdf=new Map[GO+1];
    private static File[] dirL=new File[GO+1];
    public static void queueForDownload(String id0,  boolean isListPdf) {
        final String id;
        if (isListPdf) {
            final int digit=nxt(DIGT,id0);
            if (digit<0) return;
            id=id0.substring(digit);
            if (sze(getFile(id,LIST))>0) return;
        } else id=id0;
        final String T=isListPdf ? "LIST" : "ABSTRACT";
        Runnable t=mapThread.get(T);
        if (t==null) {
            mapThread.put(T,t=thrdCR(instance(),T));
            mapRequests.put(T, new HashSet());
            mapQueue.put(T, new Vector());
            startThrd(t);
        }
        if (!mapRequests.get(T).contains(id)) {
            mapRequests.get(T).add(id);
            mapQueue.get(T).add(id);
        }
    }
    /* <<< queue <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    public Object run(String id, Object arg ) {
        final boolean bySfx=id=="ABSTRACT";
        if (bySfx || id=="LIST") {
            while(true) {
                final Vector<String> vQ=mapQueue.get(id);
                if (sze(vQ)==0) { sleep(500); continue;}
                final String dbId=getS(0,vQ);
                if (dbId==null) continue;
                vQ.remove(dbId);
                final File fDest=getFile(dbId, bySfx ? AUTO : LIST);
                if (sze(fDest)==0) {
                    if (bySfx) {
                        final String sURL=Hyperrefs.toUrlString(dbId,Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE);
                        //putln(dbId+" sURL="+sURL);
                        final File fSrc=file(sURL);
                        if (fSrc!=null && sze(fSrc)==0)  {
                            //putln("ChPubmed.unattendedDownload ",url(sURL),"  ",fSrc);
                            unattendedDownload(url(sURL));
                            handleActEvt(ChPubmed.class, ACTION_FULL_TEXT_ASSOCIATED, 0);
                        }
                        if (sze(fSrc)>0 && !fSrc.equals(fDest)) renamFileOrCpy(fSrc,fDest);
                    } else  collectPdfs(atoi(dbId,nxt(DIGT,dbId)));
                }
            }
        }
        if (id=="SHARED") {
            _listSharedPdf=true;
            if (_setSharedPdf==null) _setSharedPdf=new HashSet();
            synchronized(_setSharedPdf) {_setSharedPdf.clear();}
            for(String f : lstDir(dirSharedPdf())) {
                if (endWith(".pdf",f)) {
                    synchronized(_setSharedPdf) { _setSharedPdf.add(delSfx(".pdf",f));}
                }
            }
            handleActEvt(ChPubmed.class, ACTION_FULL_TEXT_ASSOCIATED, 0);
            _listSharedPdf=false;
        }
        if (id=="CPY_PDF") {
            final File fPdf=(File)arg;
            if (dirSharedPdf().isDirectory()) {
                final File fCopy=file(dirSharedPdf()+"/"+fPdf.getName()), fTmp=file(fCopy+"TMP");
                //putln("Going to copy "+fPdf+" to "+fCopy);
                cpy(fPdf,fTmp);
                renamFileOrCpy(fTmp, fCopy);
                mkExecutabl(fCopy);
                if (sze(fCopy)==0) error("could not copy "+fPdf+" to "+fCopy);
                //putln(" fCopy "+sze(fCopy)+" Bytes");
            }
        }

        if (id=="PDF2TXT") {
           setEnabld(false,bPdf2txt);
           viewFile(getDir(PDF2TXT), 0);
           for(File dir : new File[]{getDir(PDF),dirSharedPdf()}) {
               for(String s : lstDir(dir)) {
                   if(!endWith(".pdf",s)) continue;
                   final File fPdf=new File(dir,s);
                   if (sze(fPdf)==0) continue;
                   final File fTxt=getFile(delDotSfx(s),PDF2TXT);
                   if (sze(fTxt)==0 || fTxt.lastModified()<fPdf.lastModified()) {
                       new ChPdfUtilsPROXY().pdf2txt(fPdf,fTxt);
                       ChUtils.sleep(111);
                   }
               }
           }
           setEnabld(true,bPdf2txt);
        }
        return null;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    private static Runnable threadPdfRenameUseTitle(File dirP, String fileSuffix) {
        return thrdM("pdfRenameUseTitle", ChPubmed.class, new Object[]{dirP, fileSuffix});
    }
    public static void pdfRenameUseTitle(File dirP, String fileSuffix) {
        //putln("pdfRenameUseTitle "+dirP+" "+fileSuffix);
        BA sbSkip=null;
        final BA sbSuccess=new BA(999);
        for(String fn : lstDir(dirP)) {
            final File fP=file(dirP,fn);
            final int iPmid=atoi(fn);
            if (sze(fP)==0 || !endWith(fileSuffix,fn) || iPmid==0) continue;
            final String title=pubmedTitle(iPmid);
            if (sze(title)==0) {
                if (sbSkip==null) sbSkip=new BA(999).a("Skipped because abstract not yet downloaded: \n");
                sbSkip.a("PUBMED:").a(iPmid).a('\n');
                continue;
            }
            String t=filtrS('_'|FILTER_NO_MATCH_TO, FILENM, title);
            t=rplcToStrg(".","_",t);
            t=rplcToStrg("__","_",t);
            t=rplcToStrg("--","-",t);
            t=rplcToStrg("_-","-",t);
            t=rplcToStrg("-_","-",t);
            while (sze(t)>0 && !is(LETTR_DIGT,t,0)) t=t.substring(1);
            while (sze(t)>0 && !is(LETTR_DIGT,t,sze(t)-1)) t=t.substring(0,sze(t)-1);
            if (sze(t)==0) continue;
            final File fNew=file(dirP+"Title/"+t+fileSuffix);
            if (!fNew.exists()) {
                hardLinkOrCopy(fP,fNew);
                sbSuccess.a(fP).a(" ==> ").a(fNew).a(fNew.exists() ? " success\n\n":" failed\n\n");
            }
        }
        shwTxtInW("Rename using Pubmed title",sbSuccess.and("\n", sbSkip));

    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */
    public static File getFile(String w0, int type0) {
        final int type;
        if (type0==AUTO) {
            type=
                w0==null || w0.indexOf('{')>=0 ? -1 :
                w0.startsWith("PMID") || w0.startsWith("PUBMED") || w0.startsWith("pmid") ? ABSTRACT :
                w0.startsWith("GO:") ? GO :
                w0.startsWith("UNIPROT:") || w0.startsWith("SWISS:")? UNIPROT : -1;
        } else type=type0;
        if (type<0) return file(Hyperrefs.toUrlString(w0,Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE));
        Map<String,File> m=mapPMID_Pdf[type];
        if (m==null) m=mapPMID_Pdf[type]=new HashMap();
        File f=m.get(w0);
        if (f==null) {
            final String w;
            if (type==ABSTRACT || type==PDF2TXT || type==PDF ||  Hyperrefs.hasNumericID(w0)) {
                final int num=nxt(DIGT,w0);
                if (num<0) return null;
                w=w0.substring(num, nxtE(-DIGT,w0,num, MAX_INT));
            } else w=w0.replace(':','_');
            m.put(w0,f=file(getDir(type)+"/"+w+ getFileSuffix(type)));
        }
        return f;
    }
    private static String getFileSuffix(int type) {
        return type==LIST ? ".list" : type==PDF ? ".pdf" : type==UNIPROT ? ".swiss" : type==PDF2TXT ? ".txt" : type==ABSTRACT ? ".M" : "";
    }
    private static boolean _listSharedPdf;
    private static HashSet _setSharedPdf;
    public static boolean hasPdf(String pmid) {
        if (_setSharedPdf==null) mayUpdateShared(true);
        return pmid!=null && _setSharedPdf!=null && (_setSharedPdf.contains(pmid) || sze(getFile(pmid,PDF))>0);
    }
    /* <<< Files <<< */
    /* ---------------------------------------- */
    /* >>> Directories >>> */
    public static File getDir(int type) {
        if (dirL[type]==null){
            final String s=type==LIST?"listPdfs": type==PDF ? "pdfs" : type==PDF_TITLE ? "pdfTitle" : type==UNIPROT ? "uniprot" : type==PDF2TXT ? "pdf2txt" : type==GO ? "GO" : "abstracts";
            mkdrs(dirL[type]=file(dirSettings()+"/pubmed/"+ s));
        }
        return dirL[type];
    }
    private static ChTextField _tfDirPdf[], _tfDirSharedPdf;
    private static ChTextField tfDirSharedPdf() {
        if (_tfDirSharedPdf==null) {
            final String s=(isSystProprty(IS_WINDOWS) ? "//olli.charite.de" : "/nfs")+"/sysbio/Forschung/pdf4pmid";
            _tfDirSharedPdf=new ChTextField(s).li(instance()).saveInFile("dirSharedPdf");
            pcp(ACTION_FOCUS_LOST,"SHARED",_tfDirSharedPdf);
        }
        return _tfDirSharedPdf;
    }
    private static File _dirSharedPdf;
    public static File dirSharedPdf() {
        File d=_dirSharedPdf;
        if (d==null) d=_dirSharedPdf=file(tfDirSharedPdf());
        return d;
    }
    private static ChTextField[] tfDirDownloadedPdf() {
        ChTextField tf[]=_tfDirPdf;
        if (tf==null) {
            final String ss[]={"~/","~/Desktop/","~/Downloads/",ChEnv.get("TEMP"),"C:/temp/","D:/temp/","/tmp/"};
            tf=_tfDirPdf=new ChTextField[ss.length];
            for(int i=ss.length; --i>=0;) {
                final File f= ss[i]==null || ss[i].indexOf(':')>0 && !isSystProprty(IS_WINDOWS) ? null : file(ss[i]);
                if (!isDir(f)) continue;
                tf[i]=new ChTextField(rplcToStrg(" ","%20",toStrg(f)), COMPLETION_DIRECTORY_PATH).saveInFile("ChPubmed_dirDownloadedPdf"+i);
            }
        }
        return tf;
    }
    /* <<< Directories <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    private static void mayUpdateShared(boolean always) {
        if (always || !_listSharedPdf) startThrd(thrdCR(instance(),"SHARED"));
    }
    private final static String[] EE={".pdf",".PDF","fulltext","PDFSTART"};
    private static Object _dialog;
    private static int _dialog_pmid;
    static boolean maybeAssociatePdf(int iPmid, boolean longDialog) {
        if (iPmid<=0) return false;
        if (_dialog==null) {
            togShared=toggl("Save a copy of the PDF in the shared directory").s(true);

            final Object
                panShared=pnl(VBHB,"#ETB",
                              "Sharing downloaded PDFs with colleagues.",
                              togShared.cb(),
                              pnl(HB,"Shared directory: ",tfDirSharedPdf()),
                              " "
                              );

            _dialog=pnl(VBHB,
                        "<h2>Associating PDF with the Pubmed abstract</h2>",
                        " ",
                        "<h3>1. Preparation</h3>",
                        "Type the directory or directories where web browsers normally save PDF files.",
                        tfDirDownloadedPdf(),
                        " ",
                        "<h3>2. Downloading</h3>",
                        pnl(HBL,"Download the PDF full text using the web browser ", new ChButton("OPEN_PMID").li(instance()).t(null).i(IC_WWW)," ."),
                        " ",
                        pnlTogglOpts("*Options:", panShared),
                        panShared,
                        "<h3>3. Associate</h3>",
                        pnl(HBL,"By pressing the button ",  new ChButton("ASSOC_PDF").t("Associate ...").li(instance()),
                            ", the last created or modified PDF in above directories will be associated with the Pubmed entry.")
                        );
        }
        _dialog_pmid=iPmid;
        ChFrame.frame("", _dialog, ChFrame.ALWAYS_ON_TOP|ChFrame.PACK).shw(ChFrame.AT_CLICK).setTitle("Associate PDF PMID "+iPmid);
        return true;
    }
    public static File maybeDownload(String w0) {
        final String w=delPfx("ALIGN:",delPfx("EXPR:",delPfx("LOC:",w0)));
        final File f=getFile(w,AUTO);
        if (f!=null && sze(f)==0) {
            final String sURL=Hyperrefs.toUrlString(w, w.startsWith("ENSG") ? Hyperrefs.WEB_LINK : Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE);
            final File fSrc=file(sURL);
            if (sze(fSrc)>0 && !fSrc.equals(f)) { renamFileOrCpy(fSrc,f);  }
            if (sze(f)==0) queueForDownload(w,false);
        }
        return sze(f)>0?f:null;
    }
    private static ChPubmed _inst;
    private static ChPubmed instance() { return _inst==null ? _inst=new ChPubmed() : _inst;}
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final int modi=modifrs(ev);
        if (gcp(ACTION_FOCUS_LOST,q)=="SHARED" && (cmd==ACTION_ENTER || cmd==ACTION_FOCUS_LOST)) {
            if (eqNz(file(_dirSharedPdf), file(_tfDirSharedPdf))) {
                _dirSharedPdf=null;
                mayUpdateShared(false);
            }
        }
        if (cmd=="OPEN_PMID") {
            putln("dialog_pmid="+_dialog_pmid+"  u="+Hyperrefs.toUrlString("PUBMED:"+_dialog_pmid, Hyperrefs.WEB_LINK));
            visitURL(Hyperrefs.toUrlString("PUBMED:"+_dialog_pmid, Hyperrefs.WEB_LINK), modi );
        }
        if (cmd=="ASSOC_PDF") {
            final int iPmid=_dialog_pmid;
            final File dirs[]=new File[tfDirDownloadedPdf().length];
            for(int i=dirs.length; --i>=0;) dirs[i]=file(tfDirDownloadedPdf()[i]);
            final File pdfLastModi=mostRecentFile(dirs,EE);
            if (sze(pdfLastModi)>0) {
                final File fFullText=newTmpFile(".txt");
                new ChPdfUtilsPROXY().pdf2txt(pdfLastModi,fFullText);
                final Object msg2=
                    pnl(CNSEW,sze(fFullText)>0?scrllpn(0,new ChTextView(fFullText),dim(99,444)) : "Unable to convert pdf to txt",
                             pnl(HB,"Associate PDF file ",pdfLastModi," modified = ",day(pdfLastModi.lastModified())),
                             pnl(VBPNL," ",pnl(HB,"with ",monospc("PMID"+iPmid), monospc(pubmedTitle(iPmid))))
                             );
                if (ChMsg.yesNo(msg2)) {
                    final File fPdf=ChPubmed.getFile(toStrg(iPmid),PDF);
                    cpy(pdfLastModi,fPdf);
                    if(sze(fPdf)>0) {
                        if (isSelctd(togShared)) startThrd(thrdCR(instance(),"CPY_PDF",fPdf));
                        handleActEvt(ChPubmed.class, ACTION_FULL_TEXT_ASSOCIATED, 0);
                        if (Insecure.canModify(pdfLastModi) && ChMsg.yesNo("Successfully associated.<br>Delete the downloaded file?<pre>"+pdfLastModi+"</pre>")) {
                            delFile(pdfLastModi);
                        }
                        mayUpdateShared(true);
                    }
                }
            } else error(new BA(99).a("Could  not find PDF in <pre>").join(dirs).a("</pre>"));
        }

        if (strEquls("TITLE_", cmd)) {
            final int type= cmd=="TITLE_ABSTRACT" ? ABSTRACT : PDF;
            final File dir= cmd=="TITLE_PDF_SHARED" ?  dirSharedPdf() : getDir(type);
            startThrd(threadPdfRenameUseTitle(dir,getFileSuffix(type)));
        }
        if (cmd=="DIR") {

            final BA
                msg=new BA(999).a("<ul>")
                .a("<li><b>WIKI:Pubmed abstracts</b> ").a(fPathUnix(getDir(ABSTRACT))).a("</li>")
                .a("<li><b>WIKI:Gene_ontology</b> ").a(fPathUnix(getDir(GO))).a("</li>")
                .a("<li><b>WIKI:Plain_text  Pdf2Txt</b> ").a(fPathUnix(getDir(PDF2TXT))).a("</li>")
                .a("</ul>")
                .a("<ul>")
                .a("<li><b>WIKI:PDF</b> ").a(fPathUnix(getDir(PDF))).a("</li>")
                .a("<li><b>Shared PDF directory</b> ").a(dirSharedPdf()).a("</li>")
                .a("</ul>");
            shwTxtInW("",msg);
        }
        if (cmd=="PDF2TXT") {
            Runnable t=mapThread.get(cmd);
            if (t==null) mapThread.put(cmd,t=thrdCR(instance(),cmd,q));
            startThrd(t);
        }
        final File fPdf=pmidPopup!=null ? getFile(pmidPopup,PDF) : null;
        if (fPdf!=null) {
            if (cmd=="PDF_A") maybeAssociatePdf( atoi(fPdf.getName()),true);
            if (cmd=="PDF_0") {
                final File fBackup=file(fPdf+"_");
                delFile(fBackup);
                Insecure.renameToIgnoreSecurity(fPdf, fBackup);
                delFile(getFile(pmidPopup,PDF2TXT));
                handleActEvt(ChPubmed.class, ACTION_FULL_TEXT_ASSOCIATED, 0);
                final File fShared=file(dirSharedPdf()+"/"+fPdf.getName());
                if (sze(fShared)>0 && Insecure.canModify(fShared) && ChMsg.yesNo("Also delete <pre>"+fShared+"</pre>")) delFile(fShared);
                mayUpdateShared(false);
            }
            if (cmd=="PDF_V") {
                viewFile(sze(fPdf)>0 ? fPdf : file(dirSharedPdf(),fPdf.getName()), modi);
            }
            pmidPopup=null;
        }
    }    /* <<< ActionListener <<< */
    /* ---------------------------------------- */
    /* >>> JPopupMenu >>> */
    private static javax.swing.JPopupMenu pmidMenu;
    private static String pmidPopup;
    private static java.awt.event.ActionListener li;
    private static ChButton buttons[]={null,null};
    public static void showPubmedPopup(String pmid) {
        ChPubmed.pmidPopup=pmid;
        final ChTextField tfShared=new ChTextField(tfDirSharedPdf().getDocument()).cols(80,true,true);
        tfShared.li(instance());
        pcp(ACTION_FOCUS_LOST,"SHARED",tfShared);
        if (pmidMenu==null) {
            li=instance();
            final Object[] items={
                "A recently (1h) downloaded PDF can be associated to the Pubmed ID.",
                "The program needs to know the directory of downloaded  PDFs.",
                "-",
                new ChButton("PDF_A").li(li).t("Associate PDF ...").i(IC_PDF),
                buttons[0]=new ChButton("PDF_V").li(li).t("View PDF ( Shift+Left-Click )"),
                buttons[1]=new ChButton("PDF_0").li(li).t("Delete PDF").i(IC_KILL),
                "-",
                new Object[]{
                    "Utils",
                    new ChButton("DIR").li(li).t("List important folders ...").i(systProprty(SYSP_ICON_FILE_BROWSER)),
                    "-",
                    new ChButton("TITLE_ABSTRACT")   .t("All Abstract files   - Create nice file names using Pubmed title").li(li),
                    new ChButton("TITLE_PDF_SHARED") .t("All shared PDF files - Create nice file names using Pubmed title").li(li),
                    new Object[]{
                        "All PDF-files",
                        new ChButton("TITLE_PDF").t("Create nice file names using Pubmed title").li(li),
                        bPdf2txt=new ChButton("PDF2TXT").t("Extract plain texts from PDFs ").li(li)
                    }
                },
                new Object[]{
                    "Customize",
                    ChButton.doView(tfShared).t("Change shared PDF directory..."),
                    Customize.newButton(Customize.databases, Customize.proteinDatabases, Customize.fileBrowsers, Customize.pdfViewer, Customize.webBrowser).t("Customize urls...")
                },
                new Object[]{
                    "Source code and licenses",
                    ChButton.doOpenURL(URL_STRAP_JARS+"src").t("Java source codes"),
                    "For source code of Strap download strap.jar from home page"
                },
                "-",
                helpBut(ChPubmed.class).t("Help")
            };
            pmidMenu=jPopupMenu(0, "",  items);
        }
        for(ChButton b : buttons) {
            final String cmd=gcps(KEY_ACTION_COMMAND,b);
            if (cmd=="PDF_0" || cmd=="PDF_V") setEnabld(sze(getFile(pmid,PDF))>0, b);
        }
        shwPopupMenu(pmidMenu);
    }
    /* <<< JPopupMenu <<< */
    /* ---------------------------------------- */
    /* >>> Collect PDF Links >>> */
    public static String viaTest(URL u) {
        final String via=getHeaderFld("Via",u);
        if (via==null) return null;
        final int b=nxt(LETTR,via);
        final int e=b>=0? via.indexOf(',',b) : -1;
        if  (e<0) return null;
        final String ret=via.substring(b,e);
        //if (myComputer())  wrte(file("~/var/log/http_field_via/"+filtrS('_'|FILTER_NO_MATCH_TO, FILENM, u)),u+"\t"+via);

        return ret.indexOf(' ')<0 && ret.indexOf(':')<0 ? ret : null;
    }
    private static File collectPdfs(int pmid) {
        final File f=getFile(toStrg(pmid),LIST);
        final String oId=toStrg(pmid);
        final String urlPubmed=Hyperrefs.toUrlString("PMID:"+pmid,Hyperrefs.WEB_LINK);
        final BA ba=readBytes(sze(file(oId))>0 ? file(oId) : urlPubmed);
        if (ba==null) { putln("Error in ChPubmed: cannot read ",urlPubmed); return null;}
        final byte[] T=ba.bytes();
        final int E=ba.end();
        int linkout=0;
        final HashSet listPDF=new HashSet();
        while(  (linkout=strstr("\"linkout-icon",T,linkout+1,E))>0) {
            int href=linkout;
            while(href>0 && !strEquls("href=\"", T,href)) href--;
            if (href<=0) continue;
            final int urlB=href+6;
            final int urlE=strchr('"',T,urlB+1,E);
            baTmp.clr();
            if (T[urlB]=='/') baTmp.a("http://www.ncbi.nlm.nih.gov");
            baTmp.a(T,urlB,urlE);
            try {
                final URL u=url(new BA(0).filter(FILTER_HTML_DECODE,baTmp));
                final BA txtWithPdf=readBytes(u.openStream());
                if (txtWithPdf!=null) collectPdfs(txtWithPdf,u,listPDF,0,new boolean[1]);
                else putln(" ChPubmed# cannot read ",u);
            } catch(Exception ex){putln(ex);}
        }
        collectPdfs(ba,url(urlPubmed),listPDF,999,new boolean[1]);
        baTmp.clr().a('\n');
        final Object ss[]= listPDF.toArray();
        for(Object o : ss) baTmp.a(o).a(ChTextComponents.PDF4PMID).a(pmid).a('\n');
        wrte(f,baTmp);
        return f;
    }
    private static void collectPdfs(BA ba, URL url, HashSet result,int depth,boolean foundSdarticle[]) {
       final byte[] T=ba.bytes();
        final int E=ba.end();
        final boolean isSpri=strstr("title>SpringerLink",ba)>0;
        final boolean isPnas=strstr("content=\"http://www.pnas.org/",ba)>0;
        if (DEBUG && myComputer()) {
            final BA baV=new BA(E+9999).aln(url).a("depth=").a(depth).a('\n');
            try{
                final java.net.URLConnection c=url!=null ? url.openConnection() : null;
                if (c!=null) {
                    baV.aln(c.getHeaderFields());
                    for(int i=0;i<1000;i++) {
                        final String f=c.getHeaderField(i);
                        if (f==null && i>0) break;
                        baV.a(i).a(' ').aln(f);
                    }
                }
            } catch(Exception ex) { }
            baV.a('\n').a('=',80).a('\n').a(ba);
            shwTxtInW(toStrg(url),baV);
        }
        for(int search=2; --search>=0;) {
            int urlPdf=0;
            while(true) {
                urlPdf=search==0 ? strstr(STRSTR_AFTER,"href=",T,urlPdf+1,E) : strstr("http://",T,urlPdf+1,E);
                if (urlPdf<0) break;
                if (T[urlPdf]=='"') urlPdf++;
                final int gt=strchr('>',T,urlPdf,E);
                final int quote=strchr('"',T,urlPdf,gt);
                final int endAddr=quote>0 ? quote : gt;
                if (endAddr<0) continue;
                final int  debug=  strstr(STRSTR_IC,"pdf",T,0,E);
                if (DEBUG && debug>0) putln("urlPdf="+urlPdf+" quote="+quote+"  debug="+debug+"  "+strstr(STRSTR_IC,"pdf",T,urlPdf,endAddr+4));
                final int end=(T[endAddr-1]=='"' ? 1:0)+endAddr;
                final String lnk=toStrg(filtr(FILTER_HTML_DECODE,0,ba,urlPdf,end));
                if (  ( strstr(STRSTR_IC,"pdf",T,urlPdf,endAddr+4)+strstr(STRSTR_IC,"PDF",T,urlPdf,endAddr+4)>0 )  &&
                      lnk.indexOf("jpg")+lnk.indexOf("jpeg")+lnk.indexOf("png")+lnk.indexOf("gif")<-2) {
                    if (lnk.indexOf("sdarticle")>0) {
                        if (foundSdarticle[0])  break;
                        foundSdarticle[0]=true;
                    }
                    final String absoluteLink;
                    if (T[urlPdf]=='/') {
                        String host=viaTest(url);
                        if (isSpri) host="http://www.springerlink.com/";
                        else if (isPnas) host="http://www.pnas.org/";
                        if (host==null) url.getHost();
                        absoluteLink="http://"+host+lnk;
                    } else absoluteLink=lnk;
                    if (DEBUG) putln(" lnk=",lnk);
                    if (absoluteLink!=null && absoluteLink.indexOf("/null/")<0) result.add(absoluteLink);
                } else  if (!foundSdarticle[0] && depth==0 && lnk.indexOf(".htm")>0) {
                    if (lnk.startsWith("http://www.sciencedirect.com") || lnk.startsWith("http://journals.elsevierhealth.com/")) {

                        collectPdfs(readBytes(lnk), url(lnk),result,depth+1,foundSdarticle);
                    }
                }
            }
       }
    }
    /* <<< Collect PDF Links <<< */
    /* ---------------------------------------- */
    /* >>> Title/Abstract >>> */
    private final static BA BA[]={null,null};
    public final static long SHOWABSTRACT_FULL_TEXT=1<<1;
    public static String pubmedTitle(int pmid) {
        final BA ba=readBytes(getFile(toStrg(pmid),ABSTRACT) ,baTmp.clr());
        if (ba==null) return null;
        ba.insidePreTags(null);
        final int b=strstr(STRSTR_AFTER,"\nTI ", ba), e=b>=0 ? strchr('\n',ba,b,MAX_INT) : -1;
        return e>0 ? ba.newString(b,e) : null;
    }
    public static void showAbstract(File f, ChTextComponents dest, long options) {
        final String noExt=delDotSfx(f.getName());
        final BA baTmp1=baClr(0,BA),  baTmp2=baClr(1,BA);
        if ( (options&SHOWABSTRACT_FULL_TEXT)!=0) {
            final File fPdf=ChPubmed.getFile(noExt,ChPubmed.PDF), fTxt=ChPubmed.getFile(noExt,ChPubmed.PDF2TXT);
            //putln("fPdf="+fPdf);
            if (sze(fPdf)>0 && (sze(fTxt)==0 || fPdf.lastModified()>fTxt.lastModified())) {
                //putln("SHOWABSTRACT_FULL_TEXT neu laden");
                new ChPdfUtilsPROXY().pdf2txt(fPdf,fTxt);
            }
            readBytes(fTxt,baTmp1);
        } else {
            if (noExt.startsWith("Summary_g_")) {
                readBytes(f ,baTmp1);
                final int from=strstr("<h2>Gene:",baTmp1);
                final int to=strstr("</p>",baTmp1,from,MAX_INT);
                final byte[] T=baTmp1.bytes();
                int lt=0;
                if (from>0 && to>from) {
                    for(int i=from; i<to; i++) {
                        if (T[i]=='<') lt++;
                        else if (T[i]=='>') lt--;
                        else if (lt==0) baTmp2.a((char)T[i]);
                    }
                } // debug("ChPubmed: from="+from+" to="+to+"\n f="+f+"\n");
                baTmp2
                    .replace(STRPLC_FILL_RIGHT,"Source: UniProtKB/Swiss-Prot ","\nUNIPROT:")
                    .replace(STRPLC_FILL_RIGHT,"\n      ","\n")
                    .replace(STRPLC_FILL_RIGHT,"(EC ","\n")
                    .replace(STRPLC_FILL_RIGHT,")(", " ")
                    .replace(STRPLC_FILL_RIGHT|STRPLC_FILL_RM,"\n\n","\n")
                    .a('\n');

                final int uniprot=strstr("UNIPROT:",baTmp2);
                baTmp1.clr();
                if (uniprot>0) {
                    final int uniprotE=nxtE(SPC,baTmp2,uniprot,MAX_INT);
                    final String unip=baTmp2.newString(uniprot,uniprotE);
                    final File fU=ChPubmed.getFile(unip,ChPubmed.UNIPROT);
                    if (sze(fU)==0) ChPubmed.maybeDownload(unip);
                    readBytes(fU,baTmp1);
                }
            } else {
                if (endWith(".M",f)) baTmp2.a(readBytes(ChPubmed.getFile(noExt, ChPubmed.LIST) ,baTmp1));
                readBytes(f,baTmp1.clr()).insidePreTags(null);
                if(strstr("not available</title>",baTmp1)>0) delFile(f);
                final int iTi=strstr("\nTI ", baTmp1);
                if (iTi>0) baTmp1.setBegin(iTi);
            }
        }
        if (!strEquls("<?xml ",baTmp1.bytes(),baTmp1.begin())) {
            dest.setTextTS(baTmp2.a(baTmp1));
            final int ti=strstr(STRSTR_BEGIN_OF_LINE, "TI ",baTmp2);
            ChTextComponents.setScrollPositionV(ti>0?ti:0,dest.jc());
        }
    }
    /* <<< Title/Abstract <<< */

    private final static boolean DEBUG=false;

}

