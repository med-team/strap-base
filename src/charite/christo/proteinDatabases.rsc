

#Alignments
CDD: http://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=*&seqout=1&maxaln=-1
#PFAM: Pfam: http://pfam.sanger.ac.uk/family/alignment/download/gzipped?acc=*&alnType=seed&.gz
#PFAM_FULL: http://pfam.sanger.ac.uk/family/alignment/download/gzipped?acc=*&alnType=full&.gz
PFAM_FULL: http://pfam.sanger.ac.uk/family/*/alignment/full/gzipped?.gz
PFAM: http://pfam.sanger.ac.uk/family/*/alignment/seed/gzipped?.gz

#Sequences

#soap-uniprot: UNIPROTKB:
UNIPARC: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=uniparc&style=raw&id=
UR090: UNIREF090: http://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=UR090&style=raw&id=
UR050: UNIREF050: http://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=UR090&style=raw&id=
UR100: UNIREF100: http://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=UR100&style=raw&id=

EMBL: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=EMBL&style=raw&id=
EMBLCDS: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=emblcds&style=raw&id=
SWISS: EXPASY: Swiss-Prot:  http://www.uniprot.org/uniprot/*.txt
UNIPROT: UNIPROTKB: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=uniprotkb&style=raw&id=
IPI: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=IPI&style=raw&id=
UPI: http://www.ebi.ac.uk/cgi-bin/dbfetch?db=UNIPARC&style=raw&id=
EMBLCDS http://www.ebi.ac.uk/cgi-bin/dbfetch?db=EMBLCDS&style=raw&id=

UNIPROT_EC: http://www.uniprot.org/uniprot/?sort=score&format=list&query=ec:*
UNIPROT_QUERY: http://www.uniprot.org/uniprot/?sort=score&format=list&query=

$EFETCH=http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?rettype=gp&retmode=text&id=

REFSEQ: NCBI: ENTREZ: GB: GENBANK: $EFETCH*&db=NCBI_PROTEIN_OR_NUCLEOTIDE
NCBI_NT: Gi: GI:  $EFETCH*&db=nucleotide
NCBI_AA: $EFETCH*&db=protein
# The id will be analyzed and NCBI_PROTEIN_OR_NUCLEOTIDE will be replaced by protein or nucleotide, accordingly. 
# http://www.ncbi.nlm.nih.gov/Sitemap/sequenceIDs.html
# GI: can be NT and AA. But the db=protein/nucleotide does not matter
# http://www.ncbi.nlm.nih.gov/protein/153266841?report=genbank&log$=protalign&blast_rank=1&RID=U55FHHT301N

INTERPRO:   http://www.ebi.ac.uk/interpro/ISpy?mode=list&ipr=
NCBI_NEIGHBOR: http://structure.ncbi.nlm.nih.gov/Structure/vast/vastsrv.cgi?sdid=
KEGG_AA: http://www.genome.jp/dbget-bin/www_bget?-f+-n+a+
KEGG_NT: http://www.genome.jp/dbget-bin/www_bget?-f+-n+n+
PRODOM: http://prodomweb.univ-lyon1.fr/prodom/current/cgi-bin/request.pl?db_ent0=
FSSP: ftp://ftp.embl-heidelberg.de/pub/databases/protein_extras/fssp/*.fssp.Z
#HSSP: http://bioinfo.hku.hk/db/hssp/HSSP/*.hssp
HSSP: ftp://ftp.embl-heidelberg.de/pub/databases/protein_extras/hssp/*.hssp
#http://www.cbi.cnptia.embrapa.br/cgi-bin/SMS/STINGm/mostra_completo_1_hssp.cgi?1ryp

SYSTERS_CLUSTER: http://systers.molgen.mpg.de/cgi-bin/nph-fileout.pl?method=m&nr=


#Indirekt See Hyperrefs.getProteinReferencesInHtmlFile
NCBI_3D: http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=
HGNC: http://www.genenames.org/data/hgnc_data.php?hgnc_id=


#soap-embl: EMBL:
#soap-uniparc: UNIPARC:

#Structure:
SCOP: http://scop.mrc-lmb.cam.ac.uk/scop/data/scop.*.html
SCOP_SUN: http://scop.mrc-lmb.cam.ac.uk/scop/search.cgi?sunid=
SCOP_SCCS: http://scop.mrc-lmb.cam.ac.uk/scop/search.cgi?sccs=
VIPERDB: http://viperdb.scripps.edu/API2/api_get_structure.php?VDB=
