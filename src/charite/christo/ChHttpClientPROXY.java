package charite.christo;
import java.net.Socket;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class ChHttpClientPROXY extends AbstractProxy {
    @Override public String getRequiredJars() {
        return
          PFX_INSTALLED+"commons-codec|commons-codec-1.4.jar "+
            PFX_INSTALLED+"commons-httpclient|commons-httpclient-3.1.jar "+
            PFX_INSTALLED+"commons-logging|commons-logging-1.1.1.jar "+
            PFX_INSTALLED+"httpclient|httpclient-4.1.1.jar "+
            PFX_INSTALLED+"httpclient|httpclient-cache-4.1.1.jar "+
            PFX_INSTALLED+"httpcore|httpcore-4.1.jar "+
            PFX_INSTALLED+"httpmime|httpmime-4.1.1.jar ";

   }
    /* libhttpmime-java libhttpclient-java libcommons-httpclient */

    public Socket newSocket(String host) {
        final ChRunnable po=(ChRunnable)proxyObject();
        final Socket socket=po==null?null : (Socket)po.run(ChHttpClient.RUN_newSocket, host);
        putln(RED_WARNING+" ChHttpClientProxy po==null");
        return socket;
    }
}
