package charite.christo;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.awt.datatransfer.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

<div class="floatR">
    <table summary="Dag-and-Drop actions"    rules="groups" border="1"  >
        <caption>Actions which are performed when objects are dropped on certain targets.
        </caption>
      <colgroup>
        <col width="1*">
          <col width="1*">
            <col width="1*">
              <col width="1*">
                <col width="1*">
                  <col width="1*">
      </colgroup>
      <thead style="border-bottom: 3px solid black;" >
        <tr><th style="border-right: 3px solid black;"></th><th colspan=4 >D r o p p e d &nbsp;  O b j e c t s &nbsp; &nbsp; (Also see WIKI:Drag_and_drop and  Drop_Web_Link*)</th></tr>
        <tr>
          <th style="border-right: 3px solid black;"> Drop target</th>
          <th style="border-right: 2px solid gray;"> Proteins</th>
          <th style="border-right: 2px solid gray;"> Residue selections</th>
          <th style="border-right: 2px solid gray;"> Hetero groups</th>
          <th>Image files (png, gif, jpg)</th>
        </tr>
      </thead>

      <tbody>
        <tr style="border-bottom: 2px solid gray;">
          <th style="border-right: 3px solid black;"> <br>&nbsp;</th>
          <td style="border-right: 2px solid gray;"></td>
          <td style="border-right: 2px solid gray;" nowrap> Copy residue selection to target protein</td>
          <td style="border-right: 2px solid gray;" nowrap> Add heteros to target protein </td>
          <td nowrap>Set icon image</td>
        </tr>

        <tr style="border-bottom: 2px solid gray;">
          <th style="border-right: 3px solid black;">Drop on residue selections <br>&nbsp;</th>
          <td style="border-right: 2px solid gray;"></td>
          <td style="border-right: 2px solid gray;"></td>
          <td style="border-right: 2px solid gray;"></td>
          <td nowrap>Set background image</td>
        </tr>

        <tr style="border-bottom: 2px solid gray;">
          <th style="border-right: 3px solid black;">Drop in 3D-View<br>&nbsp;</th>
          <td style="border-right: 2px solid gray;" nowrap> Add 3D-structure to view</td>
          <td style="border-right: 2px solid gray;">Highlight atoms/amino acids in 3D</td>
          <td style="border-right: 2px solid gray;">Add hetero structure to view</td>
          <td></td>
        </tr>

        <tr style="border-bottom: 2px solid gray;">
          <th style="border-right: 3px solid black;">Drop in alignment panel<br>&nbsp;</th>
          <td style="border-right: 2px solid gray;" nowrap>Load protein</td>
          <td style="border-right: 2px solid gray;" nowrap>Add residue selection to protein</td>
          <td style="border-right: 2px solid gray;" nowrap></td>
          <td></td>
        </tr>

        <tr>
          <th style="border-right: 3px solid black;" nowrap>Drop in desktop or file browser <br>&nbsp;</th>
          <td style="border-right: 2px solid gray;" nowrap>Copy protein file </td>
          <td style="border-right: 2px solid gray;"></td>
          <td style="border-right: 2px solid gray;" nowrap>Export as single PDB-file</td>
          <td></td>
        </tr>
      </tbody>
    </table>
    See Drop_Web_Link*
    <br>
</div>
*/

public class ChTransferable implements Transferable {
    public final static String KOPT_RENDERER_TEXT="CT$$KRT";
    public final static int FILE=1, OBJECT=2;
    private final Object[] _data;
    private final List<File> _vFiles=new ArrayList();
    private final String _dataS;
    private final int _opt;
    private DataFlavor[] _df;

    public ChTransferable(Object[] alist, int options) {
        _opt=options;
        _data = alist;
        final BA sb=new BA(99);
        for(Object o : _data) {
            if (o instanceof URL) sb.a(o).a("\r\n");
            if (o instanceof File) sb.a(((File)o).toURI()).a("\r\n");
            if (gcp(KOPT_RENDERER_TEXT, o)!=null || gcp(KOPT_RENDERER_TEXT, clazz(o))!=null) {
                sb.or(rendererTxt(o), toStrg(o)).a("\r\n");
            }
        }
        _dataS=toStrg(sb);
        for(Object o : _data) adUniq(deref(o,File.class),_vFiles);
    }
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
        //        putln(YELLOW_DEBUG+" getTransferData "+flavor);
        return
            flavor==DataFlavor.stringFlavor || flavor==flavor('U') ? _dataS :
            flavor==DataFlavor.javaFileListFlavor ? _vFiles :
            _data;
    }
    public DataFlavor[] getTransferDataFlavors() {
        if (_df==null) {
            final List<DataFlavor> v=new ArrayList();
            adUniq(flavor('O'),v);
            adUniq(DataFlavor.stringFlavor,v);
            boolean isF=(_opt&FILE)!=0;
            for(Object o : _data) {
                if (o instanceof URL) {

                    adUniq(flavor('U'),v);
                    adUniq(DataFlavor.stringFlavor,v);
                }
                isF|= (o instanceof File);
            }
            if (isF) {
                adUniq(DataFlavor.javaFileListFlavor,v);
                adUniq(flavor('U'), v);
            }
            _df=toArry(v,DataFlavor.class);
        }
        return _df;
    }
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        //putln("isDataFlavorSupported ",flavor+"   "+(idxOf(flavor,getTransferDataFlavors())>=0));
        return idxOf(flavor,getTransferDataFlavors())>=0;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Static Utils  >>> */
    private final static HashSet _vReported=new HashSet();
    private static DataFlavor _flavors[];
    private static DataFlavor flavor(char t) {
        if (_flavors==null) {
            _flavors=new DataFlavor['U'+1];
            try {_flavors['U']=new DataFlavor("text/uri-list;class=java.lang.String"); } catch(Exception e){ assrt(); }
            try {_flavors['O']=new DataFlavor(Object.class,"Any"); } catch(Exception e){ assrt();}
        }
        return _flavors[t];
    }
    public static Object getData(String type, Transferable t) {
        final DataFlavor df=
            type=="S"  ? DataFlavor.stringFlavor:
            type=="URI"? flavor('U'):
            type=="FF" ? DataFlavor.javaFileListFlavor:
            type=="OO" ? flavor('O'):
            null;
        if (null==df) assrt();
        if (t.isDataFlavorSupported(df)) {
            Object o=null;
            try { o=t.getTransferData(df); } catch(Exception e){ }
            if (o instanceof String && strEquls("Unsupported ",o)) {
                //putln(RED_WARNING+"ChTransferable  Unsupported ",o,"\n type=",type);
                o=null;
            }
           return df==DataFlavor.stringFlavor && t instanceof ChTransferable ? ((ChTransferable)t)._dataS : o;
        }
        return null;
    }
    public static String reportDataFlavors(Transferable t) {
        if (t==null) return null;
        final String key=toStrg(new BA(99).a(t).a(' ').a(t.getTransferDataFlavors()));
        if (_vReported.contains(key)) return null;
        _vReported.add(key);
        try {
            final BA sb=new BA(333).a("DataFlavors of ").aln(t);
            for(DataFlavor f : t.getTransferDataFlavors()) {
                sb.a(' ',4).aln(shrtClasNam(f))
                    .a("    getHumanPresentableName()=").aln(f.getHumanPresentableName())
                    .a("    toString()=").aln(f)
                    .a("    getTransferData()=");
                try {
                    sb.a(shrtClasNam(t.getTransferData(f))).a(' ').aln(t.getTransferData(f));
                } catch(Throwable ex) {sb.a(ex);}
            }
            return toStrg(sb);
        } catch(Throwable ex) {
            stckTrc(ex);
            return stckTrcAsStrg(ex);
        }
    }

    static File[] getFiles(Transferable t, int options) {
        Collection<File> vF=null;
        final List dataFileLst=(List)getData("FF",t);
        final String dataUriLst=(String)getData("URI",t);
        final String dataString=(String)getData("S",t);
        /*
        putln("\n type=",t);
        putln("\n dataFileLst=", dataFileLst);
        putln("\n dataUriLst=", dataUriLst);
        putln("\n dataString=", dataString);
        putln("\n dataObject=", dataObject);
        */
        for(String lines : new String[]{dataString , dataUriLst}) {
            if (lines==null) continue;
            for(String line :  lines.split("\n")) {
                final String sData=line.trim();
                vF=adUniqNew(nxt(LETTR_DIGT,sData)>=0 ? file(sData) : null, vF);
            }
        }
        for(int i=sze(dataFileLst); --i>=0;) {
            final File f=get(i,dataFileLst,File.class);
            if (sze(f)>0) vF=adUniqNew(f,vF);
        }
        //putln(" ChTransferable#getFiles ",vF);
        return toArryClr(vF,File.class);
    }

}

