package charite.christo;
import java.sql.*;
import java.net.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/*
  apt-get install libeditline-dev libreadline-dev

  http://www.kfu.com/~nsayer/Java/dyn-jdbc.html
  http://zetcode.com/db/postgresqljavatutorial/

  -cacheUrl=jdbc:postgresql://localhost/strap_cache

   createdb strap_cache
   mkdir ~/.postgresql/data
   initdb -D ~/.postgresql/data
   postmaster -D ~/.postgresql/data

bytea_output
http://grokbase.com/t/postgresql/pgsql-general/0365fsmz55/problem-with-bytea
http://www.asciitable.com/

*/

public class CacheResultJdbc {
    private Connection _con;
    private boolean _conFailed;
    private final static String TABLE="cache";
    private static CacheResultJdbc _inst;
    public static CacheResultJdbc instance() { if (_inst==null) _inst=new CacheResultJdbc(); return _inst;}

    private final static String _urlUserPwd[]=new String[3];
    //    @Override public String getRequiredJars() { return "postgresql-9.1-902.jdbc3.jar";}

    public static boolean processPara(String arg) {
        if (arg==null || !arg.startsWith("-cache")) return false;
        
        for(int i=3; --i>=0;) {
            final String p=i==0?"-cacheUrl=" : i==1?"-cacheUser=" : "-cachePassword=";
            if (arg.startsWith(p)) {
                _urlUserPwd[i]=delPfx(p,arg);
                return true;
            }
        }
        return false;
    }
    /* <<< Options <<< */
    /* ---------------------------------------- */
    /* >>> Connection >>> */
    private  Connection con() {
        if (_conFailed || _urlUserPwd[0]==null) return null;
        if (_con==null) {
            try {
                final String driver="postgresql-9.1-902.jdbc3.jar";
                if (InteractiveDownload.downloadFilesAndUnzip(new URL[]{new URL(URL_STRAP_JARS+driver+".pack.gz")}, dirJars())) ChZip.unpack200(dirJars(),ChZip.PACK_IF_NEWER);
                final File f=file(dirJars(),driver);
                if (sze(f)==0) putln(RED_ERROR+"CacheResultJdbc ",f," no such file");
                else {
                    final URL u=new URL("jar:file://"+f+"!/");
                    putln(YELLOW_DEBUG+"CacheResultJdbc url="+u);
                    final Driver d=(Driver)Class.forName("org.postgresql.Driver", true, new URLClassLoader(new URL[]{u})).newInstance();
                    DriverManager.registerDriver(new JdbcDriverProxy(d));
                }
            } catch(Exception ex) { ex(ex,null);}
            String stm=null;
            boolean tableExists=false;
            try {
                _con=DriverManager.getConnection(_urlUserPwd[0], orS(_urlUserPwd[1], systProprty(SYSP_USER_NAME)), _urlUserPwd[2]);
                putln(YELLOW_DEBUG+" con=",_con);
            } catch(SQLException ex) { error(RED_CAUGHT_IN+" CacheResultJdbc "+ex); }
            if (_con==null) { _conFailed=true; return null; }
            try {
                tableExists=_con.getMetaData().getTables(null, null, TABLE, null).next();
            } catch(SQLException ex) { error(RED_CAUGHT_IN+" CacheResultJdbc "+ex); }
            if (myComputer()) putln(YELLOW_DEBUG+"tableExists="+tableExists);
            if (!tableExists) {
                try {
                    _con.prepareStatement(stm="CREATE TABLE IF NOT EXISTS "+TABLE+"(name text, section text, key text, value bytea, modified timestamp, hashcode int);\n").executeUpdate();
                } catch(SQLException ex) { ex(ex,stm); }

                try {
                    _con.prepareStatement("GRANT ALL PRIVILEGES ON table "+TABLE+" to strap;").executeUpdate();
                } catch(SQLException ex) { }
                try {
                    _con.prepareStatement(stm="CREATE INDEX index_key on cache (key);\n").executeUpdate();
                } catch(SQLException ex) { ex(ex,stm);}

            }

        }
        return _con;
    }
    public boolean init() {
        return !_conFailed && _urlUserPwd[0]!=null && con()!=null; }
    /* <<< Connection <<< */
    /* ---------------------------------------- */
    /* >>> Put Get >>> */

    public boolean put(String name, String section0, String key0, byte[] value, int hashcode) {
        final Connection con=con();
        if (con==null) return false;
        final String key=key0.replace('\'','_'), section=section0.replace('\'','_');
        String stm=null;

        try {
            final PreparedStatement pst=con.prepareStatement(stm="DELETE FROM "+TABLE+" where name='"+name+"' and section='"+section+"' and key='"+key+"'");
            if (pst!=null) pst.executeUpdate();
        } catch(SQLException ex) { ex(ex,stm); }

        try {
            final PreparedStatement pst=con.prepareStatement(stm="INSERT INTO "+TABLE+
                                                             "(name, section, key, value, hashcode, modified) VALUES('"+name+"', '"+section+"', '"+key+"', ?, "+hashcode+", 'now')");
            pst.setBytes(1, value);
            pst.executeUpdate();
            puts(" SQL+ ");
            //puts(value);
            return true;
        } catch(SQLException ex) { ex(ex,stm); }
        return false;
    }

    public BA get(String name, String section0, String key0) {
        byte[] value=null;
        final Connection con=con();
        final String key=key0.replace('\'','_'), section=section0.replace('\'','_');
        String stm=null;
        int hc=0;
        if (value==null && con!=null) {
            stm="SELECT value, hashcode from "+TABLE+" where key='"+key+"' and section='"+section+"' and name='"+name+"'";
            try {
                final ResultSet rs=con.prepareStatement(stm).executeQuery();
                if (rs.next()) {
                    hc=rs.getInt(2);
                    value=rs.getBytes(1);
                }
                puts(" SQL* ");
            } catch(SQLException ex) { ex(ex,stm);}
        }

        if (value==null) return null;
        final int L=value.length;
        final BA ba=
            value[L-4]=='\\' && value[L-3]=='0' && value[L-2]=='1' && value[L-1]=='2' ? new BA(L).filter(FILTER_DECODE_PSQL_OCTAL,value):
            new BA(value);
        if (myComputer() && strstr("\\012",ba)>=0) debugExit(RED_ERROR+" Octal: ",stm);
        if (hc!=0 && hc!=hashCd(ba)) {
            final String err="\n"+RED_ERROR+"CacheResultJdbc expected hashcode="+hashCd(ba)+"  "+stm+";\n";
            if (puts(onlyOnce(err))) appndToFile(err,file(CacheResult.dir()+"/errorHashCode.txt"));
            value=null;
        }
        return ba;
    }
    /* <<< Put Get <<< */
    /* ---------------------------------------- */
    /* >>> Helper methods >>> */
    private static void ex(Exception ex, String stm) {
        putln(RED_CAUGHT_IN+" CacheResultJdbc stm=",stm);
        putln(ex);
    }

}

