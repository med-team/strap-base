package charite.christo;
                                                                                                                                                    
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.io.*;
import java.net.*;

public class Sil implements  ChRunnable {
    public final static String ACTION="SilNewAct";
    private final ChRunnable _run;
    private Object _sisl;
    private ServerSocket _server;
    private boolean _success;
    private final int _port;
    private static int _portWarning;
    public Sil(ChRunnable r, int port) {
        //putln(ANSI_GREEN+"new Sil "+port+ANSI_RESET);
        _run=r;
        _port=port;
        if (r!=null) {
            if (port!=0) {
                try{
                    _server=new ServerSocket(port);
                    startThrd(thrdCR(this,"waitForSocket"));
                    _success=true;
                } catch (IOException ex) {
                    if (_portWarning!=port) putln("Warning: Could not listen on port "+port+" ",ex);
                    _portWarning=port;
                }
            } else {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            }
        }
    }
    public boolean isSuccess() { return _success;}

    public static void toSocketAndWaitForAnswerThenDie(int port, String argv[]) {
        try {
            final Socket socket=new Socket("localhost",  port);
            putln("Sil Going to write to socket ",socket);
            final OutputStream sos=socket.getOutputStream();
            for(String a : argv) {
                sos.write(a.replace('\t',' ').replace('\n',' ').getBytes());
                sos.write((byte)'\t');
            }
            sos.write((byte)'\n');
            //  sos.flush();
            startThrd(thrdCR(new Sil(null,0),"DIE",new Object[]{socket,"DIE"+port}));
            sleep(2222);
        } catch(Exception ex) { putln("toSocketAndWaitForAnswerThenDie: "+port+" ",ex); }
    }

    public Object run(String id,Object arg) {
        final Object[] argv=arg instanceof Object[] ? (Object[])arg :null;
        if (id=="DIE") {
            final Socket socket=(Socket)argv[0];
            final Object dieFor=argv[1];
            try {
                final String line=readLne(socket.getInputStream());
                if (line!=null && line.equals(dieFor)) {
                  putln("Sil going to die ",line);
                  System.exit(0);
                }
            } catch(IOException iox) {}

        }

        if (id=="waitForSocket") {
            while(true) {
                try{
                    final Socket client=_server.accept();
                    startThrd(thrdCR(this, "readLines",client));
                } catch (IOException ex) { putln("Caught in Sil: ",ex); }
            }
        }
        if (id=="readLines") {
            final Socket client=(Socket)arg;
            InputStream in=null;
            OutputStream os=null;
            try{
                in=client.getInputStream();
                os=client.getOutputStream();
            } catch (IOException ex) {putln("Caught in Sil: ", ex);  }

            while(true){
                try {
                    final String line=readLne(in);
                    if (line==null) break;
                    putln("Sil "+ANSI_GREEN, line, ANSI_RESET);
                    final String GET="GET /?";
                    final boolean startsGET=line.startsWith(GET);
                    final String send=startsGET ? line.substring(GET.length()).replaceAll("HTTP/[0-9].*","") : line;
                    if (_run.run(ACTION, send)!=null) {
                        os.write(("DIE"+_port+"\n").getBytes());
                        //os.flush();
                    }
                } catch (IOException ex) { putln("Caught in Sil: ",ex); break; }
            }
        }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        return null;
    }
}
