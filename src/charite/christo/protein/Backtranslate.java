package charite.christo.protein;
import charite.christo.*;
import java.util.Arrays;
import static charite.christo.ChUtils.*;
public class Backtranslate {

    public final static String ORGANISMS[]={"codonusage_Escherichia_coli_536","codonusage_Homo_sapiens"};
    private Backtranslate(){}
    public static Codonusage codonUsage(String name)  {
        final ChTokenizer T=new ChTokenizer();
        T.setText(readBytes(CLASSLOADER.getResourceAsStream("charite/christo/protein/"+name+".rsc")));
        final byte[] buf3=new byte[3];
        final Codonusage usage=new Codonusage();
        for(int count=0; count<4*4*4; count++) {
            if (!T.nextToken()) return null;
            //putln(T.asString());
            T.asBytes(buf3);
            final int codon=DNA_Util.actg2int(buf3,0);
            if (codon<0 || codon>=4*4*4) {  putln(" BT: error no such codon "+T.asString()); return null;}
            if (!T.nextToken()) return null;
            final byte aa=T.asChar();
            if (!T.nextToken()) return null;
            final float fraction=(float)T.asFloat();
            if (!T.nextToken()) return null;
            final float frequency=(float)T.asFloat();
            if (!T.nextToken()) return null;
            usage.fraction[codon]=fraction;
            usage.frequency[codon]=frequency;
            usage.codon2aa[codon]=aa;
            usage.name=name;
        }
        return usage;
    }
    public static byte[] backtranslate(byte[] aa, int end, Codonusage usage,boolean uracil) {
        if (aa==null) return null;
        if (end>aa.length) end=aa.length;
        final byte[] nt=new byte[end*3], buf3=new byte[3];
        final int aa2codon[]=usage.aa2codon();
        for(int i=end; --i>=0;) {
            final int i3=i*3;
            final byte a=aa[i];
            if (a<0 || a>'z') { nt[i3]= nt[i3+1]= nt[i3+2]=(byte)'X'; continue;}
            DNA_Util.int2actg(aa2codon[a],buf3,uracil);
            nt[i3]=buf3[0];
            nt[i3+1]=buf3[1];
            nt[i3+2]=buf3[2];
        }
        for(int j=3; --j>0;) for(int i=j; i<nt.length; i+=3) nt[i]|=32;
        return nt;
    }

    public static class Codonusage {
        private final float fraction[]=new float[4*4*4], frequency[]=new float[4*4*4];
        private final byte codon2aa[]=new byte[4*4*4];
        private int aa2codon[];
        private float aa2fraction[];
        public int[] aa2codon() {
            if (aa2codon==null) {
                Arrays.fill(aa2codon=new int[128],-1);
                aa2fraction=new float[128];
                for(int i=0; i<4*4*4; i++) {
                    final int aa=codon2aa[i];
                    if (aa<'A' || aa>'Z') continue;
                    final float f=fraction[i];
                    if (aa2fraction[aa]<f) {aa2fraction[aa]=f; aa2codon[aa]=aa2codon[aa|32]=i; }
                }
            }
            return aa2codon;
        }
        String name;
        @Override public String toString() { return "Codon usage "+name;}
    }

}
