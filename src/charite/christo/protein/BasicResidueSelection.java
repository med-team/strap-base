package charite.christo.protein;
import java.util.*;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.ProteinMC.*;
/**
  A skelleton of an Selection of Residues {@link ResidueSelection ResidueSelection}.
  @author Christoph Gille
*/
public class BasicResidueSelection extends AbstractVisibleIn123 implements ResidueSelection, HasName, HasRenderer {
    public final static int OFFSET_IS_1ST_IDX=1<<1;
    private ResidueSelection _sel0;
    private Protein _p;
    private String _name;
    private int _offset, _opt;
    private boolean _selected[];

    public BasicResidueSelection(ResidueSelection s) { _sel0=s;}
    public BasicResidueSelection(int opt) {_opt=opt;}

    /* ---------------------------------------- */
    /* >>> Protein >>> */

    public void setProtein(Protein newP) {
        final Protein oldP=_p;
        if (oldP!=newP) {
            _p=newP;
            if (newP!=null) Protein.incrementMC(MC_RESIDUE_SELECTIONS_V, newP);
            if (oldP!=null) Protein.incrementMC(MC_RESIDUE_SELECTIONS_V, newP);
        }
    }
     @Override public Protein getProtein() { return _p;}
    /* <<< Protein <<< */
    /* ---------------------------------------- */
    /* >>> Selected Residues >>> */
     @Override public boolean[] getSelectedAminoacids() {
        final ResidueSelection s=_sel0;
        final boolean bb[];
        if (s!=null) {
            s.setProtein(_p);
            bb=s.getSelectedAminoacids();
        } else bb=_selected;
        return bb!=null ? bb:NO_BOOLEAN;
    }
    @Override public int getSelectedAminoacidsOffset() {
        if (0!=(_opt&OFFSET_IS_1ST_IDX)) return Protein.firstResIdx(getProtein());
        final ResidueSelection s=_sel0;
        return s!=null ? s.getSelectedAminoacidsOffset() : _offset;
    }
    @Override public void setSelectedAminoacids(boolean bb[], int offset) { changeSelectedAA(bb, offset, this); }

    /* <<< Selected Residues  <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */

    @Override public String toString(){
        final String n=getName();
        final ResidueSelection s=_sel0;
        return s!=null && n==null ? s.toString() : n!=null ? n : super.toString();
    }
    public void setName(String s){
        _name=s;
    }
    public String getName(){
        return _name!=null ? _name : nam(_sel0);
    }

    private final static Object SB[]={null};
    public Object getRenderer(long options, long rendOptions[]) {
        final Protein p=getProtein();
        final BA sb=baClr(SB);
        //if (0==(getVisibleWhere()&VisibleIn123.SEQUENCE)) sb.a("Hidden ");
        if (0!=(options&HasRenderer.JLIST) && p!=null) sb.a(p).a('/');
        sb.a(getName()).a(' ',2).boolToText(getSelectedAminoacids(),getSelectedAminoacidsOffset()+1,",", "-");
        if (p==null && rendOptions!=null) rendOptions[0]=STRIKE_THROUGH;
        return sb;
    }

    public void getRGB(int rgb[]) {
        final boolean bb[]=getSelectedAminoacids();
        if (bb==null) return;
        final Protein p=getProtein();
        final int selOffset=p!=null ? getSelectedAminoacidsOffset() : 0;
        final int color=getColor()!=null ? getColor().getRGB(): 0xFF0000;
        final int to=mini(bb.length,rgb.length-selOffset);
        Arrays.fill(rgb,0);
        for(int i=maxi(0,-selOffset); i<to; i++) {
            if (bb[i]) rgb[i+selOffset]=color;
        }
    }

    /* <<< Renderer <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    public static boolean selectRange(int from, int to, ResidueSelection s) {
        if (s==null) return false;
        final Protein p=s.getProtein();
        final int f=maxi(from,0);
        final int t=p==null?to:mini(to, p.countResidues()+Protein.firstResIdx(p));
        if (t<=f) return changeSelectedAA(NO_BOOLEAN, 0, s);
        final boolean bb[]=setTrue(0, t-f, s.getSelectedAminoacids());
        Arrays.fill(bb,t-f, bb.length,false);
        return changeSelectedAA(bb,from,s);
    }
    public static boolean changeSelectedAA(boolean sel[], int offset, ResidueSelection s) {
        if (s==null) return false;
        final boolean changed=!boolOffstEquls(sel,offset, s.getSelectedAminoacids(), s.getSelectedAminoacidsOffset());
        if (changed) {
            if (s instanceof BasicResidueSelection) {
                ((BasicResidueSelection)s)._selected=sel;
                ((BasicResidueSelection)s)._offset=offset;
            } else s.setSelectedAminoacids(sel,offset);

            final Protein p=s.getProtein();
            if (p!=null) Protein.incrementMC(MC_RESIDUE_SELECTIONS, p);
        }
        return changed;

    }

    public static boolean setAminoSelected(boolean onOff, int iA, ResidueSelection s) {
        if (iA<0 || s==null) return false;
        final boolean bb0[]=s.getSelectedAminoacids();
        final int offs0=s.getSelectedAminoacidsOffset();
        if (bb0==null || iA<0 || !onOff && (iA<offs0 || offs0+bb0.length<=iA)) return false;
        final boolean noOffset=iA<offs0;
        boolean bb[]=bb0;
        int offs=offs0;
        if (noOffset && offs0!=0) {
            bb=chSze(withoutOffset(bb0, offs0),   1+maxi(iA, offs0+lstTrue(bb0)));
            offs=0;
        }
        final int i=iA-offs;
        if (i>=bb.length) bb=chSze(bb,i+1);
        if (bb[i]==onOff) return false;
        bb[i]=onOff;
        s.setSelectedAminoacids(bb,offs);
        Protein.incrementMC(MC_RESIDUE_SELECTIONS,s.getProtein());
        return true;
    }
    /* <<< Renderer <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    private Object _shared;
    public void setSharedInstance(Object shared) { _shared=shared;}
    public Object getSharedInstance() { return _shared; }
}
