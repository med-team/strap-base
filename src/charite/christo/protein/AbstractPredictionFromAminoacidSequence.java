package charite.christo.protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import charite.christo.*;
import java.awt.*;
import java.io.*;
import javax.swing.JComponent;
/**
   @author Christoph Gille
*/
public abstract class AbstractPredictionFromAminoacidSequence implements PredictionFromAminoacidSequence,Disposable, HasControlPanel {
    private final BA LOG=new BA(99);
    private String _urlResult, _sequences[];
    private char[][] _prediction;
    private final JComponent  PAN_WEB_BUTTONS=pnl(VB);
    private boolean _disposed;
    private Object _shared, _ctrl;
    public int countResults() { return  sze(_sequences);  }

    public Object getControlPanel(boolean real) {
        if (_ctrl==null) {
            if (!real) return CP_EXISTS;
            _ctrl=pnl(CNSEW,scrllpn(LOG),dHomePage(this),PAN_WEB_BUTTONS);
        }
        LOG.textView(true).tools().highlightOccurrence("mail",null,null, HIGHLIGHT_IGNORE_CASE|HIGHLIGHT_REPAINT, C(0xFF0000));
        return _ctrl;
    }

    public void setGappedSequences(String ss[]) { _sequences=ss;}

    public char[][] getPrediction(){ return _prediction;}

    public void dispose() {_disposed=true;}
    public void compute() {
        _prediction=new char[_sequences.length][];
        for(int i=0;i<_sequences.length && !_disposed;i++) {
            char cc[]=_prediction[i]=compute(filtrS(0L,LETTR,_sequences[i]));
            if (cc!=null) {
                for(int j=cc.length;--j>=0;) if (cc[j]==0) cc[j]=' ';
            }
        }
    }
    public abstract char[] compute(String sequ);
    public String findUrlInText(String txt) {
        if (txt==null) return null;
        final int posFrom=txt.toLowerCase().indexOf("a href="),posTo=txt.indexOf('>',posFrom+1);
        LOG.a(ANSI_GREEN+" found URL: ").a(posFrom).a('-').a(posTo).a("\n"+ANSI_RESET);
        if (posFrom<0 || posTo<0) {LOG.aln(RED_ERROR+": could not find string \"a href=\""); return null; }
        String sUrl=txt.substring(posFrom+7,posTo);
        sUrl=delPfx('"',delSfx('"',sUrl));
        if (serverRoot()!=null && ! looks(LIKE_EXTURL,sUrl)) sUrl=serverRoot()+"/"+sUrl;
        return sUrl;
    }
    public String serverRoot() { return null;}

    public BA getResultText(String sequence, String SERVER,Object[][] postData, boolean indirect) {
        if (sequence==null) return null;
        BA data=null;
        final String seq=filtrS(0L,LETTR,sequence), cacheSection, cacheKey;
        if (CacheResult.isEnabled()) {
            final BA sb=new BA(99);
            CacheResult.sectionForSequence(seq,sb);
            cacheSection=sb.toString();
            sb.clr();
            if (postData!=null) for(Object[] oo: postData) sb.join(oo,"|");
            else sb.a(sequence);
            cacheKey=sb.toString();
            data= CacheResult.getValue(0, getClass(), cacheSection, cacheKey,(BA)null);
            if (data!=null) {
                LOG.a(ANSI_GREEN+"found in cache\n"+ANSI_RESET);
                return data;
            }
        } else cacheKey=cacheSection=null;
        LOG.a(ANSI_YELLOW+"Going to contact "+ANSI_RESET).a(SERVER).a("...  ");
        if (!indirect) {
            data=Web.getServerResponse(0L,SERVER,postData);
            final File fTmpHtml=newTmpFile(".html");
            Web.insertBaseTag(url(SERVER),fTmpHtml, data);
            wrte(fTmpHtml,data);
            ChDelay.toContainer(fTmpHtml,PAN_WEB_BUTTONS,null,9);
            LOG.aln(data==null || sze(data.delBlanksR().delBlanksL())==0 ? RED_ERROR+" No response" : GREEN_SUCCESS);
        } else {
            String lines[]=null;
            final BA baURL=Web.getServerResponse(0L,SERVER,postData);
            if (baURL==null) return null;
            final String dUrl=baURL.toString();
            LOG.a(" searching for result URL:\n"+ANSI_YELLOW).a(dUrl).aln(ANSI_RESET);
            final String urlResult=_urlResult=findUrlInText(dUrl);
            LOG.a(ANSI_GREEN+" sUrl ").a(urlResult).aln(ANSI_RESET);
            ChDelay.toContainer(urlResult,PAN_WEB_BUTTONS,null,9);
            if (urlResult==null)  return null;
            if (_ctrl!=null) ((JComponent)_ctrl).add(pnl(urlResult),BorderLayout.SOUTH);
            LOG.a(ANSI_BLUE+"urlResult="+ANSI_RESET).aln(urlResult);
            boolean isReady=false;

            do {
                try{
                    data=readBytes(url(urlResult).openStream());
                } catch(IOException ioex){}
                if (data!=null) {
                    isReady=true;
                    lines=splitLnes(data);
                    for(String line : lines) {
                        if (line.indexOf("Wait here to watch the progress")>=0 ||
                            line.indexOf("function canceljob()")>=0)
                            isReady=false;
                    }
                }
                sleep(2222);
                LOG.a(" trying ").aln(urlResult);
            } while(!isReady);
        }
        LOG.a(ANSI_YELLOW).aln(SERVER).aln(data).aln(ANSI_RESET);
        if (data==null) LOG.aln(RED_ERROR+"could not load");
        else CacheResult.putValue(0, getClass(),cacheSection, cacheKey,data);
        return data;
    }
    public String getUrlOfResult() {return _urlResult;}

    public void setSharedInstance(Object shared) { _shared=shared;}
    public Object getSharedInstance() { return _shared; }

}
