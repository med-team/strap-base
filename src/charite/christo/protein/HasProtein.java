package charite.christo.protein;

/**
   Implemented by objects that contain references to Proteins.

   @author Christoph Gille
*/
public interface HasProtein {
      Protein getProtein();
}
