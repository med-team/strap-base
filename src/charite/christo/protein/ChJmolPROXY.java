package charite.christo.protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   <b>Home:</b> http://jmol.sourceforge.net/<br>
   <b>License:</b> WIKI:LGPL
   <br>
   The protein structure viewer WIKI:Jmol is written in Java and understands  Rasmol-instructions.
   @author Christoph Gille
*/
public class  ChJmolPROXY extends AbstractProteinViewerProxy implements ProteinViewer  {
    @Override public String getRequiredJars() { return "jmol_12_2_RC6_dev.jar"; }
    public static String getCustomizableInitCommands() {
        return
            "3D_cartoon\n"+
            "3D_lines off\n"+
            "select backbone and "+PROTEIN+"\n"+
            "3D_lines on\n"+
            "select hetero and not water and PROTEIN\n"+
            "spacefill true";
    }

    public static String getCustomizableExamples() { return Protein3dUtils.exampleCommands(Protein3dUtils.JMOL); }

}
