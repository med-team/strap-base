package charite.christo.protein;
import charite.christo.*;
import java.awt.Color;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
/**
   Residues are colorized according to their physical and chemical properties.
   @author Christoph Gille
*/
public class ShadingAA implements ActionListener {
    public final static String TYPES[]={"white","charge","hydropathy","chemical","nucleotide"};
    private ShadingAA(){}
    private static ShadingAA _inst;
    private final static Color[][] _CCB=new Color[9][], _CCW=new Color[9][];
    private static int[][] _rgb=new int[9][];
    public final static int CHARGE=1, HYDROPATHY=2, CHEMICAL=3, ACTG=4, SECSTRU=5, SOLVENT_ACC=6, ORGANISM=7, BALLOON=8;

    public static Color[] colors(String n, boolean whiteBG) {
        final int i=idxOfStrg(STRSTR_IC,n, TYPES);
        return colors(i<0?1:i,whiteBG);
    }
    public static Color[] colors(int type, boolean whiteBG) {
        final Color[][] ccb=_CCB, ccw=_CCW;
        if (ccb[type]==null) {
            if (type==SOLVENT_ACC) {
                ccw[SOLVENT_ACC]=ccb[SOLVENT_ACC]=new Color[150];
                for(int i=0;i<150;i++) ccb[SOLVENT_ACC][i]=C( ((255-i)<<16) | ((255-i)<<8) | (100+i));
            } else {
                java.util.Arrays.fill(ccb[0]=new Color[256], C(0xffFFff));
                java.util.Arrays.fill(ccw[0]=new Color[256], C(0x000000));
                java.util.Arrays.fill(_rgb[0]=new int[256], 0xffFFff);
                set(SECSTRU, 0, "H", 0xff6464, 0xff6464);
                set(SECSTRU, 0, "E", 0xffFF00, 0xB2B200);

                set(CHARGE, 0, "DE", 0xff6464, 0xff6464);
                set(CHARGE, 0, "RKH",0x00ffff, 0x00b2b2);

                set(HYDROPATHY, CHARGE, "YSTGNQC", 0xffFF00, 0xB2B200);
                set(HYDROPATHY, -1,     "AFPMWVIL",0x00ff00, 0x00ff00);

                set(CHEMICAL, CHARGE, "AGVIL", 0xFFffFF, 0x000000);
                set(CHEMICAL, -1,     "NQ",    0x00ff00, 0x00ff00);
                set(CHEMICAL, -1,     "FYW",   0xB34D1A, 0xB34D1A);
                set(CHEMICAL, -1,     "ST",    0xff00ff, 0xff00ff);
                set(CHEMICAL, -1,     "P",     0xffc800, 0xffc800);
                set(CHEMICAL, -1,     "CM",    0xffFF00, 0xB2B200);

                set(ACTG,  0, "G", 0xb6b6b6, 0x808080);
                set(ACTG, -1, "A", 0x00ff00, 0x00ff00);
                set(ACTG, -1, "T", 0xff0000, 0xff0000);
                set(ACTG, -1, "C", 0x00ffff, 0x00ffff);
            }
        }
        return (whiteBG ? ccw:ccb)[type];
    }

    public static int[] rgb() { return rgb(_i);}
    public static int[] rgb(int i) {
        colors(i,false);
        return _rgb[i];
    }
    private static void set(int type, int clone, String s, int c1, int c2) {
        if (_CCB[type]==null) _CCB[type]=_CCB[clone].clone();
        if (_CCW[type]==null) _CCW[type]=_CCW[clone].clone();
        if (_rgb[type]==null) _rgb[type]=_rgb[clone].clone();
        for(int j=2;--j>=0;){
            final Color cc[]=(j==0?_CCB:_CCW)[type], c=C(j==0?c1 : c2);
            final int[] rgb=_rgb[type];
            if (cc==null) continue;
            for(int i=s.length(); --i>=0;) {
                final int b=s.charAt(i);
                cc[b]=cc[b|32]=c;
                rgb[b]=rgb[b|32]=c1;
            }
        }
    }

    public static Color[] colors(boolean whiteBG) { return colors(_i, whiteBG); }
    private static int _i=3;
    private static ChCombo _shadingC;
    public static ChCombo choice() {
        if (_shadingC==null) {
            _shadingC=new ChCombo("White","Charge","Hydropathy","Chemical","Nucleotide","Second struct","Solvent access")
                .tt("Amino acids are drawn with different colors. Options: <br>"+
                    "<ul><li>3D: Red=helices Yellow=sheets. Only applicable to PDB files.</li>"+
                    "<li>Amino acid type (charge, hydrophobicity ...).</li>"+
                    "<li>Solvent accessibility: Currently only applicable for DSSP-files.</li></ul>")
                .s(_i)
                .li(_inst=new ShadingAA());
        }
        return _shadingC;
    }

    public static boolean set(String type) {
        final boolean black=strEquls(STRSTR_IC,"black",type);
        int i0=black?0:type==null?-1:idxOfStrg(STRSTR_IC,type, TYPES);
        final int i=i0<0?3:i0;
        if (_shadingC!=null) _shadingC.s(i);
        _i=i;
        if (black && withGui()) buttn(TOG_WHITE_BG).s(true);
        return i0>=0;
    }
    public static int i() { return choice().i();}
    public void actionPerformed(ActionEvent ev) {
        final int i=i();
        if (i>=0 && i<TYPES.length) _i=i;
    }
}
