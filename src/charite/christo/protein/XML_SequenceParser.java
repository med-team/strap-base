package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/*
This class loads sequence files in xml format. See File_example:xml1

   @author Christoph Gille
 -load="UPI:UPI00017E4407

scp  xml1.txt $STRAP_DIR_B/dataFiles/proteinFileExamples/

*/
public class XML_SequenceParser implements ProteinParser {
    public boolean parse(Protein p, long options, BA ba) {
        final byte[] T=ba.bytes();
        final int B=ba.begin();
        final int E=ba.end();
        if (E-B<9 || !(T[B]=='<' && T[B+1]=='?' && T[B+2]=='x' && T[B+3]=='m' && T[B+4]=='l')) return false;
        final int dasSequence=strstr(STRSTR_AFTER,"<DASSEQUENCE",T,B,E);
        final int gbSet=strstr(STRSTR_AFTER,"<GBSet",T,B,E);
        final int entry=dasSequence>0 ? dasSequence : gbSet>=0 ? strstr(STRSTR_AFTER,"<GBSeq",T,B,E) :  strstr(STRSTR_AFTER,"<entry",T,B,E);
        final int entryGT=strchr('>', T,entry,E);
        final int entryE=entry>0? strstr(dasSequence>0? "</DASSEQUENCE" : gbSet>0? "</GBSeq>" : "</entry", T, entry, E):-1;
        if (entryE<0) return false;
        final int sequence=strstr(STRSTR_AFTER,dasSequence>0?"<SEQUENCE": gbSet>0 ? "<GBSeq_sequence" : "<sequence",T,entry,entryE);
        if (sequence<0) return false;
        final int sequenceGT=strchr('>', T, sequence,E);
        final int sequenceE=strstr("</",T,sequence,entryE);
        if (sequenceE<0) return false;
        byte aa[]=null;
        while(true) {
            int count=0;
            for(int i=sequenceGT+1; i<sequenceE; i++) {
                if (is(LETTR,T[i])) {
                    if (aa!=null) aa[count]=(byte)T[i];
                    count++;
                }
            }
            if (aa==null) aa=new byte[count]; else break;
        }
        p.setResidueType(aa);
        for(int dbRef=entry; (dbRef=strstr(STRSTR_AFTER,"<dbReference",T,dbRef,entryE))>0;) {
            int dbRefE=strstr("</dbReference",T,dbRef,entryE);
            if (dbRefE<0) dbRefE=strstr("/>",T,dbRef,entryE);
            if (dbRefE>0) {
                 String db=xmlAttribute("db",T,dbRef,dbRefE);
                 if (db==null) db=xmlAttribute("type",T,dbRef,dbRefE);
                 db=ProteinUtils.mapRef(db);
                final String id=xmlAttribute("id",T,dbRef,dbRefE);
               putln(" db="+db+" id="+id);
                if (db!=null && id!=null) p.addDatabaseRef(db+":"+id);
            }
        }
        final String id=dasSequence>0 ? xmlAttribute("id", T, sequence, sequenceGT) : xmlAttribute("accession", T, entry, entryGT);
        if (id!=null) {
            final String db=
                id.indexOf(':')>0 ? null :
                id.startsWith("UPI") ? "UNIPARC:" :
                null;
            p.setAccessionID(db!=null ? db+id : id);
        }
        return aa.length>0;
    }

}
