
#soapWSDBFetch

POST /ws/services/WSDbfetchDoclit HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Strap
Host: www.ebi.ac.uk
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: ""
Content-Length: dddddddddddd

<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<soapenv:Body>
<fetchBatch xmlns="http://www.ebi.ac.uk/ws/services/WSDbfetchDoclit">
<db>DATABASE</db>
<ids>QUERIES</ids>
<format>FORMAT</format>
<style>raw</style>
</fetchBatch></soapenv:Body>
</soapenv:Envelope>



#soapGetStatusEBI

POST /Tools/services/soap/wublast HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Strap
Host: www.ebi.ac.uk
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: "urn:GetStatus"
Content-Length: dddddddddddd

<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><getStatus xmlns="http://soap.jdispatcher.ebi.ac.uk"><jobId xmlns="">JOBID</jobId></getStatus></soapenv:Body></soapenv:Envelope>

#soapGetResultEBI

POST /Tools/services/soap/wublast HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Strap
Host: www.ebi.ac.uk
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: "urn:GetResult"
Content-Length: dddddddddddd

<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><getResult xmlns="http://soap.jdispatcher.ebi.ac.uk"><jobId xmlns="">JOBID</jobId><type xmlns="">TYPE</type><parameters xsi:nil="true" xmlns=""/></getResult></soapenv:Body></soapenv:Envelope>

#soapWublastDB

POST /Tools/services/soap/wublast HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Strap
Host: www.ebi.ac.uk
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: "urn:Run"
Content-Length: dddddddddddd

<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><run xmlns="http://soap.jdispatcher.ebi.ac.uk">
<email xmlns="">christoph.gille@charite.de</email>
<title xsi:nil="true" xmlns=""/>
<parameters xmlns="">
<program>PROGRAM</program>
<exp>EXP</exp>
<alignments>ALIGNMENTS</alignments>
<scores>SCORES</scores>
<stype>protein</stype>
<sequence>&gt;test
SEQUENCE
</sequence>

<database><string>DATABASE</string></database>

</parameters>
</run></soapenv:Body></soapenv:Envelope>



#soapWublastParamDetailEBI

POST /Tools/services/soap/wublast HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Strap
Host: www.ebi.ac.uk
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: "urn:GetParameterDetails"
Content-Length: dddddddddddd

<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body>
<getParameterDetails xmlns="http://soap.jdispatcher.ebi.ac.uk">

<parameterId xmlns="">PARAMETERID</parameterId>

</getParameterDetails>
</soapenv:Body></soapenv:Envelope>

#soapWublastEbi

POST /Tools/services/soap/wublast HTTP/1.0
Content-Type: text/xml; charset=utf-8
Accept: application/soap+xml, application/dime, multipart/related, text/*
User-Agent: Strap
Host: www.ebi.ac.uk
Cache-Control: no-cache
Pragma: no-cache
SOAPAction: "urn:Run"
Content-Length: dddddddddddd

<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><run xmlns="http://soap.jdispatcher.ebi.ac.uk">
<email xmlns="">christoph.gille@charite.de</email>
<title xsi:nil="true" xmlns=""/>
<parameters xmlns="">

<program>PROGRAM</program>

<exp>EXP</exp><alignments>ALIGNMENTS</alignments>

<scores>SCORES</scores>

<stype>protein</stype>

<sequence>&gt;test
SEQUENCE
</sequence>

<database><string>DATABASE</string></database>

<sensitivity>SENSITIVITY</sensitivity>

</parameters>
</run></soapenv:Body></soapenv:Envelope>


