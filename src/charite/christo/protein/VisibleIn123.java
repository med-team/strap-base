package charite.christo.protein;

/**
   Visualization style for residue selections.
*/
public interface VisibleIn123 extends charite.christo.Colored {
    /** e.g. STRUCTURE | SEQUENCE means visible in 3d and 1d */
    int getVisibleWhere();
    void setVisibleWhere(int w);
    final static int
        STYLE_BACKGROUND=0,
        STYLE_IMAGE=1,
        STYLE_IMAGE_LUCID=2,
        STYLE_CIRCLE=3,
        STYLE_DOTTED=4,
        STYLE_LOWER_HALF_BACKGROUND=5,
        STYLE_UPPER_HALF_BACKGROUND=6,

        STYLE_HIDDEN=7,
        STYLE_UNDERLINE=8,
        STYLE_CURSOR=9,

        NO_FLASH=1<<3,
        SEQUENCE=1<<4,
        STRUCTURE=1<<5,
        SB=1<<6,
        HTML=1<<7,
        JALVIEW=1<<8,

        ANYWHERE=-1,
        BIT_SHIFT_LINE=16;

    int getStyle();
    void setStyle(int s);
    long ARROW_HEADS=1<<0;
}
