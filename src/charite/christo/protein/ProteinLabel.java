package charite.christo.protein;
import charite.christo.*;
import charite.christo.strap.ContextObjects;
import charite.christo.strap.StrapAlign;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.MouseEvent.*;
/**
A label for protein objects e.g. for JTree.
   @author Christoph Gille
*/
public class ProteinLabel extends ChButton {
    public final static long VERTICAL=1<<1, ALWAYS_CONTEXT_MENU=1<<2;
    private transient Object _p;
    private static Font _fnt;
    private final long _opt;
    private Protein p() { return (Protein)deref(_p);}
    public void setProtein(Protein p) { _p=p!=null ? p.WEAK_REF : null; }
    public ProteinLabel(long options, Protein p) {
        super('L',null);
        setProtein(p);
        _opt=options;
        updateOn(CHANGED_PROTEIN_AT_CURSOR,this);
        updateOn(CHANGED_PROTEIN_LABEL,this);
        if (_fnt==null) _fnt=getFont().deriveFont(Font.BOLD);
        setFont(_fnt);
        bg(0xFFffFF);

    }

    @Override public Dimension getPreferredSize() {
        final Protein p=p();
        if (p==null) return dim(0,0);
        final boolean isVertical=0!=(_opt&VERTICAL);
        final int iw=p.getIcon()!=null ? ICON_HEIGHT : 0;
        final CharSequence rt=getText();
        final int sw=strgWidth(this,rt), w=maxi(iw+sw+2,ICON_HEIGHT*2);
        return dim(isVertical ? ICON_HEIGHT : w, isVertical ? w: ICON_HEIGHT);
    }

    @Override public void paintComponent(Graphics gAwt){
        final Protein p=p();
        if (p==null) return;

        final Graphics2D g=(Graphics2D)gAwt;
        final boolean isVertical=0!=(_opt&VERTICAL);
        final int height=getHeight(), width=getWidth(), h=!isVertical ? height : mini(ICON_HEIGHT,width);
        final Rectangle cb=chrBnds(_fnt);
        Color bg=gcp(ChRenderer.KEY_NOT_EDITABLE_BG,this,Color.class);
        if (gcp(KOPT_IS_SELECTED,this)!=null) bg=bgSelected();
        g.setColor(bg!=null ? bg: C(0xFFffFF));
        if (isVertical) {
            g.fillRect(width-1,0,1,height);
            g.fillRect(0,height-1,width,1);
            g.rotate(Math.PI/2,h/2f,h/2f);
        } else if (bg!=null) g.fillRect(0,0,width,height);

        antiAliasing(g);
        int x=0;
        final Color color=gcp(KEY_COLOR, p, Color.class);
        if (color!=null) {
            g.setColor(color);
            g.fillRect(x,height/3,4,height/3);
            x+=4;
        }

        final Image image=p.getIconImage();
        if (image!=null) {
            g.drawImage(image,x,0,h , h,this);
            x+=maxi(h,ICON_HEIGHT);
        }
        p.reportIconWidth(x);
        final String txt=getText();
        final int y=h/2-cb.height/2;
        p.paintChain(Protein.PAINT_CHAIN_UNDERLINE, g, txt, x,y,cb.width,cb.height);
        g.setColor(getForeground());
        g.drawString(txt,x,y+y(cb));
        if (p==StrapAlign.cursorProtein() && !isVertical) {
            g.setColor(C(0xFF));
            g.drawLine(0, y+cb.height, prefW(this), y+cb.height);
        }
        ChRenderer.paint(this,g);
        if (!p.isInAlignment()) {
            g.setColor(C(0x646464,100));
            g.fillRect(1,1,9999,999);
        }

    }
    @Override public String getText() {
        final Protein p=p();
        return toStrgN(p==null?null:p.getRendTxt());
    }
    @Override public void processEvent(AWTEvent ev) {
        final int id=ev.getID();
        final Protein p=p();
        if (p==null) return;

        final boolean rClick=isPopupTrggr(false,ev), clickMenu=!rClick && id==MOUSE_CLICKED && 0!=(_opt&ALWAYS_CONTEXT_MENU);
        if (rClick || clickMenu) {
           if (isPopupTrggr(true, ev) || clickMenu) ContextObjects.openMenu('P', oo(p), NO_OBJECT);
        } else {
            if (id==MOUSE_PRESSED  && isCtrl(ev)) {
                final List v=StrapAlign.selectedObjectsV();
                if (!v.remove(p)) v.add(p);
                StrapEvent.dispatch(StrapEvent.OBJECTS_SELECTED);
            }
            if (id==MOUSE_DRAGGED) {
                if (!_doingDnd) {
                    exportDrg(this, ev, TransferHandler.COPY);
                    _doingDnd=true;
                }
            } else _doingDnd=false;
        }
        super.processEvent(ev);
    }
    { this.enableEvents(MOUSE_EVENT_MASK|MOUSE_MOTION_EVENT_MASK);}
    @Override public String getToolTipText(java.awt.event.MouseEvent ev) {
        return x(ev)>ICON_HEIGHT ? null : addHtmlTagsAsStrg(runCR(p(),ChRunnable.RUN_GET_TIP_TEXT,ev));
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
   /* >>> DnD  >>> */
  private boolean _doingDnd;
  public Object getDndDateien() {
        final List<File> v=new ArrayList();
        adAll((File[])runCR(p(),PROVIDE_DND_FILES,null),v);
        debugExit(v);
        return v;
    }

}
