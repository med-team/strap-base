package charite.christo.protein;
/**
   Classes that provide a prediction depending on amino acid setSequences.
   Some methods send many tasks at once to server. Therefore  not just one but an array of sequences.
   There are methods which make consensus for an alignment. Therefore aligned gapped sequences can be set.
   STRAP retrieves the result by calling {@link #getPrediction() getPrediction()}.
*/
public interface PredictionFromAminoacidSequence {
    /** set the sequences. Since some prediction methods take an alignment as input we pass several sequences that might be aligned.
        For example a secondary structure predictore might compute refine the prediction using multiple sequences
    */
    void setGappedSequences(String ss[]);
    /** Computation is started and may take long time. */
    void compute();
    /** Returns a character for each sequence position. For example 'H' for Helix.. */
    char[][]  getPrediction();
}
