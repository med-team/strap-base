package charite.christo.protein;
public class ProteinMC {
    public final static int
    /* General */
        MC_FILE_CONTENT=1,
        MC_CHARACTER_SEQUENCE=2,
        MC_SUBSET=3,
        MC_1ST_RES_IDX=4,
    /* Renderer */
        MC_CURSOR_PROTEIN=7,
        MC_PROTEIN_LABELS=8,
    /* Residue related */
        MC_RESIDUE_TYPE=10,
        MC_RESIDUE_TYPE_FULL=11,
    /*Alignment */
        MC_GAPPED_SEQUENCE=13,
    /* 3D */
        MC_RESIDUE_SECSTRU=18,
        MC_CALPHA_ORIG=19,
        MC_ATOM_COORD_ORIG=20,
        MC_MATRIX3D=21,
        MC_CALPHA=22,
        MC_ATOM_COORD=23,
        MC_SOLVENT_ACCESSIBILITY=25,
        MC_INFERRED_PDB=26,
    /* Nucleotide */
        MC_NUCLEOTIDES=30,
        MC_CODING=31,
        MC_NUCLEOTIDE_STRAND=32,
        MC_CDS=33,
        MC_NUC_IDX2TRANSLATED_IDX=34,
        MC_FWD_OR_RC=35,
    /* ResidueSelection */
        MC_RESIDUE_SELECTIONS_V=40,
        MC_RESIDUE_ANNOTATIONS_V=41,
        MC_RESIDUE_SELECTIONS=42,
        MC_RESIDUE_SELECTION_REFERENCE=43,
        MC_SELECTED_AA=44,  /* oder besser MC_DISPLAYED_SELECTIONS */
        MC_RESIDUE_ANNOTATION_GROUPS=46,
        MC_RESIDUE_SELECTION_LABELS=47,
    /* Other Objects */
        MC_HETERO_COMPOUNDS_V=50,
        MC_PROTEIN_USER_OBJECTS_V=51,
        MC_PROTEIN_VIEWERS_V=52,
        MC_MAX=53,

        MCA_PROTEINS_V=70,
        MCA_ALIGNMENT=71,
        MCA_SEQUENCE_FEATURES_V=72,
        MCA_RES_COUNT_NOSYNC=73,
        MCA_MAX=74;
}
