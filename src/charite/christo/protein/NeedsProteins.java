package charite.christo.protein;
/**
   Implemented by dialogs that act on a Protein.
   Example: {@link charite.christo.strap.DialogSuperimpose3D DialogSuperimpose3D}.
   @author Christoph Gille
*/
public interface NeedsProteins {
    void setProteins(Protein... pp);
}
