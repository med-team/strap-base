package charite.christo.protein;
/**
   A ProteinWriter is used to export a protein to a text file
   using a certain format such as pdb or fasta.
   @author Christoph Gille
*/
public interface ProteinWriter  {

    final static long PDB=1, FASTA=1<<1, ATOM_LINES=1<<2, SIDE_CHAIN_ATOMS=1<<3, SEQRES=1<<4, SEQRES_IF_COORDINATES=1<<5, HETEROS=1<<6, 
        NUCLEOTIDE_STRUCTURE=1<<7, NUCLEOTIDES=1<<8, HELIX_SHEET=1<<9,
        MET_INSTEAD_OF_MSE=1<<10, CHAIN_SPACE=1<<11, CHAIN_A=1<<12,CHAIN_B=1<<13, IDX_FOR_RESNUM=1<<14,
        COMPLETE_PDB= PDB | ATOM_LINES | SEQRES | HELIX_SHEET | HETEROS | NUCLEOTIDE_STRUCTURE;

/**
       Writes the protein data e.g. in fasta or PDB format
       @return true: was able to produce the text, false: unable to produce the text because information is not set in the protein model.
       A ProteinWriter for protein structures returns false if the coordinate fields in the Protein instance are empty.
*/

    boolean toText(Protein p, Matrix3D m3d[], long mode, charite.christo.BA sb);

    /**   Extension ".pdb" forPDB-files and ".fa" for fasta-files */
    String getFileSuffix();

    /**  index of first and last amino acid    */
    void setResidueRange(int indexOfFirstAmino, int indexOfLastPlus1);

    /** do not write all residues but only a certain subset.*/
    void selectResidues(boolean isSelected[]);

}
