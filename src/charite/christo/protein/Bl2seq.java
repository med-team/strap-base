package charite.christo.protein;
import charite.christo.*;
import java.io.File;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.DNA_Util.complement;
import static charite.christo.DNA_Util.triplet2aa;
/**HELP
   <i>SEE_DIALOG:charite.christo.strap.Dialog_bl2seq</i>
*/
public class Bl2seq implements HasNativeExec {
    public final static String BLASTN="blastn", BLASTX="blastx", blastp="blastp";
    public final static long  OUTPUT_SHORT=1<<3, OUTPUT_LONG=1<<4, ALIGNMENT=1<<7;
    private final byte[] _seqI, _seqJ;
    private final String _program;
    private final long _options;
    private final BA _log;
    private BasicExecutable _aexec;
    public Bl2seq(long opt, byte[] seqI, byte[] seqJ,String prg, BA log) {
        _seqJ=seqJ;
        _seqI=seqI;
        _program=prg.intern();
        _options=opt;
        _log=log;
    }

    public byte[] getSequence(int i) { return i==0?_seqI:_seqJ;}
    public BasicExecutable getNativeExec(boolean createNew) { return _aexec;}
    private static boolean installed[];
    public synchronized void compute() {
        final byte[] sequenceX=_seqI, sequenceY=_seqJ;

        if (installed==null) {
            final ChExec exeProbe=new ChExec(ChExec.IGNORE_ERROR).setCommandLineV("bl2seq", "--help");
            exeProbe.run();
            (installed=new boolean[1])[0]=!exeProbe.couldNotLaunch();
        }
        if (_resultTxt==null) {
            final boolean isTable=0==(_options&ALIGNMENT);
            final BasicExecutable aexec=_aexec=new BasicExecutable("bl2seq");
            if (!installed[0]) {
                aexec.setBinaryPackageURLs(BasicExecutable.DEFAULT_BINARY);
                aexec.installPackage();
            }
            final ChExec myExec=aexec.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.IGNORE_ERROR|ChExec.LOG);
            if (myExec==null) return;
            final Object args[]=rmNullA(new Object[]{
                    installed[0] ? "bl2seq" : aexec.fileExecutable(),
                    "-p",_program,"-i",writeFasta(sequenceX,"x"),"-j",writeFasta(sequenceY,"y"), "-g","F",
                    isTable?"-D":null,
                    isTable?"1":null
                });
            if (_log!=null) _log.join(args," ").a('\n');
            myExec.setCommandLineV(args);
            myExec.run();
            final BA txt=_resultTxt=myExec.getStdoutAsBA();
            if (_log!=null) _log.a("Exit value=").a(myExec.exitValue()).a('\n').aln(txt);
            if (myExec.exitValue()!=0 || txt==null) {
                final String msg="Error running the program Bl2seq. \n"+
                    "Perhaps Bl2seq is not properly installed in the directory .StrapAlign/bin/.\n"+
                    "You may download and install the ncbi toolkit from ftp://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/\n\n"+
                    txt+
                    "\n\n"+
                    toStrg(myExec.getStderr());
                error(msg);
            }
        }
    }
    private BA _resultTxt;
    public BA getResultText() { return _resultTxt; }
    /* <<< Compute <<< */
    /* ---------------------------------------- */
    /* >>> Parsing >>> */
    private void parse() {
        if (0!=(_options&ALIGNMENT)) assrt();
        final BA result=getResultText();
        if (result==null || _hits!=null) return;
        final int ends[]=result.eol();
        final byte T[]=result.bytes();
        final ChTokenizer TOK=new ChTokenizer();
        final Hit hh[]=new Hit[ends.length];
        for(int iL=0; iL<ends.length; iL++) {
            final int b=iL==0?0: ends[iL-1]+1;
            final int e=ends[iL];
            if (e-b<12||T[b]!='x') continue;
            int fromX,toX, fromY, toY;
            final double score;
            // x	y	30.84	107	74	0	97	417	9	115	5.1e-10	45.44
            TOK.setText(T,b,e);
            for(int i=7; --i>=0;) TOK.nextToken();

            fromX=TOK.asInt();
            TOK.nextToken(); toX=TOK.asInt();
            TOK.nextToken(); fromY=TOK.asInt();
            TOK.nextToken(); toY=TOK.asInt();
            TOK.nextToken();
            TOK.nextToken(); score=TOK.asFloat();
            if (fromX>toX) toX-=2;
            if (fromY>toY) toY-=2;
            hh[iL]=new Hit(this, fromX-1,toX, fromY-1, toY, score);
        }
        _hits=rmNullA(hh,Hit.class);
    }
    /* <<< Parsing <<< */
    /* ---------------------------------------- */
    /* >>> Class Hit >>> */
    private Hit[] _hits;
    public Hit[] getHits() {
        parse();
        return _hits;
    }
    public String getProgram() { return _program;}
    public static class Hit {
        private final int _fromI,_toI,_fromJ,_toJ;
        private final double _score;
        private final Bl2seq _bl2seq;
        public Hit(Bl2seq bl2seq, int fromI, int toI, int fromJ, int toJ, double score) {
            _fromI=fromI;
            _fromJ=fromJ;
            _toI=toI;
            _toJ=toJ;
            _bl2seq=bl2seq;
            _score=score;
        }
        public int getFrom(int i) { return i==0?_fromI:_fromJ;}
        public int getTo(int i) { return i==0?_toI:_toJ;}
        public Bl2seq getBl2seq() { return _bl2seq;}
        public double getScore() { return _score;}
    }

    /* <<< Class Hit <<< */
    /* ---------------------------------------- */
    /* >>> toText >>> */

    public static String toText(Hit h, long options) {
        final BA sb=new BA(100);
        if (0!=(options&OUTPUT_SHORT)) {
            sb.a("Hit (");
            for(int i=0;i<2;i++) sb.a(h.getFrom(i)).a('-').a(h.getTo(i)).a(' ').a(h.getScore(),4,2);
            sb.a(')');
        }
        final Bl2seq bl2seq=h.getBl2seq();
        final String prg=bl2seq.getProgram();
        if (0!=(options&OUTPUT_LONG)) {
            sb.a("Score=").a(h.getScore(),4,2).a('\n');
            for(int ij0=prg==BLASTX?-1:0; ij0<2; ij0++) {
                final int ij=ij0<0?0:ij0;
                final int f=h.getFrom(ij), t=h.getTo(ij);
                final byte[] seq=bl2seq.getSequence(ij);
                sb.a(ij==0?'i':'j').a(f,8).a(' ');
                if (ij0<0) {
                    if (f<t) for(int iN=f; iN<t; iN+=3) sb.a((char)triplet2aa(seq[iN],seq[iN+1],seq[iN+2])).a(' ',2);
                    if (f>t) for(int iN=f; iN>t; iN-=3) sb.a((char)triplet2aa(complement(seq[iN]),complement(seq[iN-1]),complement(seq[iN-2]))).a(' ',2);
                } else  {
                    for(int i=f; f<t ? i<t : i>t; i+= (f<t)?1:-1) {
                        sb.a((char)seq[i]);
                        if (prg==BLASTX && ij==1) sb.a(' ',2);
                    }
                }
                sb.a(f<t?t-1:t+1,8).a('\n');
            }
        }
        return sb.toString();
    }
    /* <<< toText <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */

    private File writeFasta(byte[] seq,String name) {
        final File fOut=file(dirTmp()+"/bl2seq/"+name+".fa");
        delFile(fOut);
        wrte(fOut,">"+name+"\n"+toStrg(seq)+"\n");
        return fOut;
    }
    /* <<<  Utils <<< */
    /* ---------------------------------------- */
    /* >>> Satic Utils >>> */
    public final static long TRANS_3=1<<10;
    public static boolean[] getTranslatedNucleotides(byte[] nt, byte[] aa, Hit h0, Hit[] hh, long options) {
        final boolean trans[]=new boolean[nt.length];
        final int from0=h0.getFrom(0), to0=h0.getTo(0);
        final boolean forward=from0<to0;
        final boolean hhTaken[]=new boolean[hh.length];
        nextHit:
        for(int iH=0;iH<hh.length;iH++) {
            final Hit h=hh[iH];
            final int from=h.getFrom(0), to=h.getTo(0);
            if (0!=(options&TRANS_3)) if (forward!=from<to || from%3!=from0%3) continue nextHit;
            for(int i=0;i<iH;i++) { /* Test for overlap with previous */
                if (!hhTaken[i]) continue;
                final int f=hh[i].getFrom(0), t=hh[i].getTo(0);
                if (from%3==f%3) continue;
                if (f<from && from<t)  continue nextHit;
                if (from<f && f<to) continue nextHit;
            }
            hhTaken[iH]=true;
            for(int i=from; i<to;i++) trans[i]=true;
            for(int i=from; i>to;i--) trans[i]=true;
        }
        return trans;
    }
}
