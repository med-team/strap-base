package charite.christo.protein;
import charite.christo.*;
public interface ValueOfResidue extends HasProtein, HasMC {
    /**
       Plugin that returns a numeric value for each amino acid of the protein or null.
       These are plotted in the alignment pane.
       It should notify changes by  dispatching  a <i>JAVADOC:StrapEvent</i> of type
       <i>JAVADOC:StrapEvent#VALUE_OF_RESIDUE_CHANGED</i>
    */
    double[] getValues();
    void setProtein(Protein p);

    /**
       Increment value when new values are computed.
    */
}
