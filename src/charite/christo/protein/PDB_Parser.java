package charite.christo.protein;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP
See File_example:pdb WIKI:Protein_Data_Bank_(file_format).

Initially, Strap loads only the C-alpha atoms,
but side chain atoms are read on demand.

There might be residues in the SEQRES record without corresponding
ATOM-lines.

These residues are written in lower case in the alignment panel indicating that they have no
3D-coordinates.

The interpretation of SEQRES-lines can be deactivated by unselecting
<i>CB:charite.christo.strap.StrapAlign#ACTION_ignoreSEQRES</i> or with the command line option "-noSeqres" .
@author Christoph Gille
Strange: pdb2i7j
pdb1ijs: stark geknickte RNA-Kette
pdb1a34: unit cell RNA problem
pdb1a6y: last residue in SEQRES is a MET and is not in ATOMS
pdb1acb: SEQRES   1 I   70  THR GLU PHE GLY  where is it

mmCIF to PDB
*/
public class PDB_Parser implements ProteinParser {
    private static ChTokenizer _tok;

    public final static int
        ATO=12,CHAIN_ID=21,
        ATOM=('A')+('T'<<8)+('O'<<16)+('M'<<24),
        HEAD=('H')+('E'<<8)+('A'<<16)+('D'<<24),
        COMP=('C')+('O'<<8)+('M'<<16)+('P'<<24),
        SOUR=('S')+('O'<<8)+('U'<<16)+('R'<<24),
        ENDM=('E')+('N'<<8)+('D'<<16)+('M'<<24),
        CONE=('C')+('O'<<8)+('N'<<16)+('E'<<24),
        REMA=('R')+('E'<<8)+('M'<<16)+('A'<<24),
        TER =('T')+('E'<<8)+('R'<<16)+(' '<<24),
        HETA=('H')+('E'<<8)+('T'<<16)+('A'<<24),
        TITL=('T')+('I'<<8)+('T'<<16)+('L'<<24),
        HELI=('H')+('E'<<8)+('L'<<16)+('I'<<24),
        MODR=('M')+('O'<<8)+('D'<<16)+('R'<<24),
        SHEE=('S')+('H'<<8)+('E'<<16)+('E'<<24);

    private final static byte cc[]=new byte[80];
    private static BA sbCOMPN, sbTITLE, sbSOURCE, sbSCIENT, sbHEAD, sbChains;

    public PDB_Parser() {
        if (sbCOMPN==null) {
            sbCOMPN=new BA(99);
            sbTITLE=new BA(99);
            sbSOURCE=new BA(99);
            sbSCIENT=new BA(99);
            sbHEAD=new BA(99);
            sbChains=new BA(11);
            _tok=new ChTokenizer();
        }
    }

    public boolean parse(Protein protein,long options,BA byteArray) {
        synchronized(cc) {
            final ChTokenizer TOK=_tok;
            final String selChains=null;
            if (protein==null || byteArray==null) return false;
            final boolean wantCAlpha=true,  wantSideChains=(options&SIDE_CHAIN_ATOMS)!=0;
            final int[] DD[]=atoiLookup(), P0=DD[0], P1=DD[1], P2=DD[2], P3=DD[3], P4=DD[4], P5=DD[5];
            final float[] FF[]=atofLookup(), P_1=FF[1], P_2=FF[2], P_3=FF[3];
            final int ends[]=byteArray.eol();
            final byte[] T=byteArray.bytes();
            boolean hasRemark=false, hasHeader=false, hasCalpha=false;
            int iRawAA_last=-1, nAtoms=0;
            // count Residues
            /* --- static buffers Thread safe --- */
            //int[] rawSeq32=ref_rawSeq32!=null ? ref_rawSeq32.get(): null;
            //if (rawSeq32==null) rawSeq32=NO_INT;
            int[] rawSeq32=new int[333];
            boolean wrongChain=false;
            sbCOMPN.clr();
            sbTITLE.clr();
            sbSOURCE.clr();
            sbSCIENT.clr();
            sbHEAD.clr();

            final int seqres[], seqresSorted[], seqresL;
            if ((options&IGNORE_SEQRES)==0)  {
                seqres=PDB_Parser_SeqRes.parse(T,ends,ends.length, selChains);
                seqresL=PDB_Parser_SeqRes.countResidues();
            } else { seqres=null; seqresL=0;}
            if (seqresL>0) {
                System.arraycopy(seqres,0,seqresSorted=new int[seqresL],0,seqresL);
                Arrays.sort(seqresSorted);
            } else seqresSorted=null;
            //for(int i=0; i<seqresL; i++) putln(" ",new String(bit32To4chars(seqres[i],null)));
            int secStruStart=0, secStruEnd=0,  iA_atCalpha=-1;
            int[] modres=null;
            for(int iL=0,iRawAA=0,lastChain=0, resNumLast=MIN_INT; iL<ends.length; iL++) {
                final int b= iL==0 ? byteArray.begin() : ends[iL-1]+1, e=prev(-SPC,T, ends[iL]-1, b-1)+1;
                final int l=e-b;
                if (l<5) continue;
                final byte c4=T[b+4], c5= l>5?T[b+5]:32, c6= l>6?T[b+6]:32;
                final int start4=charsTo32bit(T,b);
                BA sb=null;
                int sbStart=b+6;
                final boolean
                    isAtom=start4==ATOM && c4==' ',
                    isHet=start4==HETA && c4=='T' && c5=='M';
                if(start4==MODR) {
                    /*
                      MODRES 1GNV MIS A  221  SER  MONOISOPROPYLPHOSPHORYLSERINE
                    */
                    if (c4=='E' && c5=='S') {
                        final int res32=charsTo32bit(T,b+12);
                        byte res8=Protein.toOneLetterCode(res32);
                        if (res8==0) {
                            res8=Protein.toOneLetterCode(T[b+24],T[b+25],T[b+26]);
                            if ('A'<=res8 && res8<='Z') (modres==null ? modres=new int['Z'+1] : modres)[res8]=res32&0xFFffFF;
                        }
                    }
                } else if (start4==HELI && c4=='X' || start4==SHEE && c4=='T') {
                    if (secStruStart==0) secStruStart=b;
                    secStruEnd=b+l;
                } else if (isAtom  || isHet) {
                    if ( l>=54) {
                        if (T[b+16]!=' ' && T[b+16]!='A' || T[b+17]==' ') continue; /* Too short or DNA   col 16 is alternateLocation */
                        final int chain=T[b+CHAIN_ID];
                        final int resNum=T[b+22]+(T[b+23]<<6)+ (T[b+24]<<12) + (T[b+25]<<18) + (T[b+26]<<24);
                        if (chain!=lastChain) {
                            lastChain=chain;
                            wrongChain= selChains!=null && !(selChains.indexOf(chain)>=0 || (chain==' ' && selChains.indexOf('_')>=0));
                        }
                        if (wrongChain) continue;
                        final int resType=(T[b+17])+ (T[b+18]<<8) +  (T[b+19]<<16) + (chain<<24);
                        if (isAtom || isHet && seqresSorted!=null && Arrays.binarySearch(seqresSorted,resType)>=0) {
                            if (resNumLast!=resNum) { /* ATOM  24733  CA  ASP N 187E */
                                resNumLast=resNum;
                                if (rawSeq32.length<=iRawAA) rawSeq32=chSze(rawSeq32, iRawAA+222);
                                rawSeq32[iRawAA_last=iRawAA]=resType;
                                iRawAA++;
                            }
                            nAtoms++;
                        }
                        final boolean isCA=T[b+12]==' ' && T[b+13]=='C' && T[b+14]=='A';
                        if (isCA) hasCalpha=true;
                        if (isAtom && isCA) iA_atCalpha=iRawAA;
                    } else {lastChain=0; }
                } else if (start4==REMA && c4=='R' && c5=='K' && c6==' ') { /* REMARK   2 RESOLUTION. 2.4  ANGSTROMS */
                    hasRemark=true;
                    if (l>36 && T[b+11]=='R'&& strEquls("RESOLUTION.",T,b+11)) protein.setResolutionAngstroms((float)atof(T,b+22,e));
                } else if (start4==HEAD && c4=='E' && c5=='R' && c6==' ') {
                    hasHeader=true;
                    sb=sbHEAD;
                    if (is(LETTR_DIGT,T,62) && is(LETTR_DIGT,T,63) && is(LETTR_DIGT,T,64) && is(LETTR_DIGT,T,65)) {
                        final String pdbId=bytes2strg(T, b+62, b+62+4 ).toLowerCase();
                        protein.setPdbID(pdbId);
                        protein.setAccessionID("PDB:"+pdbId);
                    }
                } else if (start4==COMP && c4=='N' && c5=='D' && c6==' ') {
                    if (l>20 && T[b+11]=='E' && T[b+12]=='C' && T[b+13]==':') { /* COMPND   5 EC: 2.7.7.49,  2.7.7.7;   in PDB:3C6T */
                        TOK.setText(T,b+14,e);
                        TOK.setDelimiters(chrClas(" ,;\r:"));
                        final String ec[]=new String[TOK.countTokens()];
                        for(int i=ec.length; TOK.nextToken() && --i>=0;) ec[i]=TOK.asString();
                        protein.setEC(ec);
                    } else {
                        if (strEquls("MOLECULE: ",T,b+11)) sb=sbCOMPN;
                    }
                } else if (start4==TITL && c4=='E') {
                    sb=sbTITLE;
                } else if (start4==SOUR && c4=='C') {
                    for(int j=0;j<2;j++) {
                        final String key=j==0 ? "ORGANISM_SCIENTIFIC: " : "ORGANISM_COMMON: ";
                        final int i=strstr(STRSTR_AFTER,key,T,b+6,ends[iL]);
                        if (i>0) {
                            sbStart=i;
                            sb= j==0 ? sbSCIENT : sbSOURCE;
                        }
                    }
                }
                if (sb!=null && l>9) {
                    if (is(DIGT,T[b+9]) && T[b+10]==' ' && sbStart<b+11) sbStart=b+11;
                    sbStart=nxtE(-SPC,T,sbStart,e);
                    boolean skip=false;
                    if (sb==sbCOMPN) {
                        if (strEquls("MOL_ID: ",T,sbStart) || strEquls("CHAIN: ",T,sbStart) || strEquls("ENGINEERED: ",T,sbStart)) skip=true;
                        else if (strEquls("MOLECULE: ",T,sbStart)) sbStart+=10;
                    }
                    if (!skip) {
                        final int eNoSemi=T[e-1]==';' ? e-1 : e;
                        /* --- SOURCE   2 ORGANISM_SCIENTIFIC: POLIOVIRUS TYPE 1; --- */
                        //if (strstr(STRSTR_IC, T, sbStart, eNoSemi)>=0) continue;
                        if (strstr(STRSTR_IC, T, sbStart, eNoSemi,  sb.bytes(),0,sb.end())>=0) continue; // !!!!
                        if (sb==sbSCIENT || sb==sbSOURCE) {
                            for(int i=sbStart+1; i<e; i++) {
                                if (is(LETTR,T[i]) && sb==sbSCIENT || T[i-1]!=' ') T[i]|=32;
                            }
                        }
                        if (sb.end()>0) sb.a('\n');
                        sb.a(T,sbStart,eNoSemi);
                    }
                } else if (start4==ENDM && c4=='D' && c5=='L') {
                    break;
                }
                if (start4==TER) { lastChain=0; resNumLast=MIN_INT; }
            }// for
            final int nRawAA=iRawAA_last+1;
            if (secStruStart<secStruEnd) {
                final byte ss[]=new byte[secStruEnd-secStruStart];
                System.arraycopy(T,secStruStart,ss,0,ss.length);
                protein.setPdbTextSecStru(new BA(ss));
            }
            /*
            putln(" \n sbCOMPN="+sbCOMPN \n sbSCIENT="+sbSCIENT+" \n sbSOURCE="+sbSOURCE);
            */
            protein.setCompound(sbCOMPN.end()>0 ? sbCOMPN.toString() : null);
            sbTITLE.replaceChar('\n', ' ').replaceChar('\r',' ');
            protein.setTitle(sbTITLE.end()>0 ? simplifyTitle(sbTITLE.toString()): null);
            protein.setHeader(sbHEAD.end()>0 ? sbHEAD.toString(): null);
            protein.setOrganism(sbSOURCE.end()>0 ? sbSOURCE.toString(): null);
            protein.setOrganismScientific(sbSCIENT.end()>0 ? sbSCIENT.toString(): null);

            final int seqresAli[], seqresAliL, nAA;
            final byte[] residueChain;
            if (seqresL>0)  {
                int[] ali=PDB_Parser_SeqRes.alignment(rawSeq32,nRawAA);
                seqresAliL=PDB_Parser_SeqRes.alignmentLength();
                if (seqresAliL<nRawAA && seqresAliL<iA_atCalpha) { /* Not all residues from Atom lines used */
                    if (myComputer()) {
                        putln(ANSI_RED+"Warning:"+ANSI_RESET+" PDB_Parser_SeqRes ");
                        final BA sb=new BA(99);
                        for(int i=0;i<seqresAliL; i++) {
                            sb.a((char)Protein.toOneLetterCode(rawSeq32[i]));
                            if (i>0 && ali[i]-ali[i-1]>1) sb.a(ANSI_RED).a('!').a(ANSI_RESET);
                        }
                        putln(sb);
                    }
                    ali=null;
                }
                seqresAli=ali;
                if (seqresAli==null && myComputer()) putln("PDB_Parser_SeqRes.alignment==null for ",protein);
                System.arraycopy(PDB_Parser_SeqRes.getResidueChain(),0,residueChain=new byte[nAA=seqresL],0,nAA);
            } else { seqresAli=null; residueChain=new byte[nAA=nRawAA]; seqresAliL=0;}
            final HeteroCompound[] hets=HeteroCompound.parse(byteArray,false);
            final boolean hasWater=HeteroCompound.lastParseHasWater();
            if (hasWater) protein.setWaterInFile(true);
            for(HeteroCompound h : hets) {
                protein.addHeteroCompounds(0L,h);
                if (nAtoms>0) h.setPeptideFile(protein.getFile()); else h.setFile(protein.getFile());
                //if (null==protein.getFile()) assrt();
            }

            if (nAtoms==0 || !hasCalpha) {
                return (hasRemark&&hasHeader) || hets.length>0 || hasWater;
            }
            if (myComputer()) putln("("+protein+" "+nAtoms+" "+nAA+")  ");
            /*  --- Now the number of residues and atoms is known: nAtoms ,nAA  --- */
            final int[]
                residueAtomNumber=new int[nAA],
                residueNumber=new int[nAA],
                residueName=new int[nAA];
            int[] atomNo=null,
                residueFirstAtomIdx=null,
                residueLastAtomIdx=null,
                atomType32=null;
            final byte[] residueType=new byte[nAA];
            float[]
                cAlphaCoord=null,
                atomCoord=null,
                tempFactor=null,
                occupancy=null;
            byte[]
                insCode=null,
                atomType=null;
            Arrays.fill(residueNumber, MIN_INT);
            if (seqresAli!=null)  {
                for(int iAa=0;iAa<nAA; iAa++) {
                    final int resName=seqres[iAa];
                    byte rt=Protein.toOneLetterCode(resName);
                    if (rt==0) rt=(byte)idxOf(resName&0xFFffFF, modres,'A', 'Z'+1);
                    residueType[iAa]=(byte)((rt>0?rt:'X')|32);
                }
            } else {
                for(int i=mini(nAA,nRawAA); --i>=0;) {
                    final int resName=rawSeq32[i]&0x00ffFFff;
                    byte rt=Protein.toOneLetterCode(resName);
                    if (rt==0) rt=(byte)idxOf(resName, modres,'A', 'Z'+1);
                    residueType[i]=rt>0?rt:(byte)'X';
                }
            }
            if (wantSideChains) {
                Arrays.fill(atomCoord=new float[3*nAtoms],Float.NaN);
                atomType32=new int[nAtoms];
                atomType=new byte[nAtoms];
                tempFactor=new float[nAtoms];
                occupancy=new float[nAtoms];
                atomNo=new int[nAtoms];
                residueFirstAtomIdx=new int[nAA];Arrays.fill(residueFirstAtomIdx,-1);
                residueLastAtomIdx=new int[nAA];Arrays.fill(residueLastAtomIdx,-1);
            }
            // parse atoms
            if (wantCAlpha)  Arrays.fill(cAlphaCoord=new float[3*nAA],Float.NaN);

            final float xyz[]=new float[3];

            byte iCodeLast=-1;
            boolean fileHasSidechains=false;
            for(int iZ=0,iRawAA=0,iAt=0,resNumLast=-1, current_iAa=0,lastChain=-1,  iAt3=0;  iZ<ends.length; iZ++) {
                final int b= iZ==0 ? 0 : ends[iZ-1]+1;
                final int l=mini(ends[iZ]-b,cc.length);
                if (l<6) continue; /* DNA */
                final byte c4=T[b+4], c5=T[b+5];
                final int start4=charsTo32bit(T,b);

                if (start4==ENDM && c4=='D' && c5=='L')  break;
                if (l<54) continue;
                final boolean isAtom=start4==ATOM && T[b+4]==' ';
                final boolean isHeta=start4==HETA && T[b+4]=='T'&&T[b+5]=='M'&&T[b+6]==' ';
                if (!isAtom && !isHeta) continue;
                System.arraycopy(T,b,cc,0,l);
                if (cc[16]!=' ' && cc[16]!='A' || cc[17]==' ') continue;
                final byte chain=cc[CHAIN_ID];
                final int resTypeChain=cc[17] + (cc[18]<<8) + (cc[19]<<16) + (chain<<24);
                if (isHeta && (seqresSorted==null||Arrays.binarySearch(seqresSorted,resTypeChain)<0)) continue;

                int resNum=P0[cc[25]]+P1[cc[24]]+P2[cc[23]]+P3[cc[22]];
                if (cc[22]=='-'||cc[23]=='-'||cc[24]=='-') resNum=-resNum; /* 23-26 */
                if (isAtom && cc[16]!=' ' && cc[16]!='A') continue;
                final int aTyp=cc[ATO] + (cc[ATO+1]<<8) + (cc[ATO+2]<<16) + (cc[ATO+3]<<24);
                if (selChains!=null && !(selChains.indexOf(chain)>=0 || chain==' ' && selChains.indexOf('_')>=0 )) continue;
                final boolean isNewChain=chain!=lastChain;
                if (isNewChain) lastChain=chain;
                if (iAt>=nAtoms) {
                    if (myComputer()) putln(protein+"   WARNING!! iAt="+iAt+" nAtoms="+nAtoms);
                    break;
                }
                if (wantSideChains) {
                 atomType32[iAt]=aTyp;
                 atomType[iAt]=cc[77];
                 atomNo[iAt]=P0[cc[10]]+P1[cc[9]]+P2[cc[8]]+P3[cc[7]]+P4[cc[6]];
                 occupancy[iAt]=(P0[cc[59]]+P1[cc[58]]+P2[cc[56]]+P3[cc[55]]+P4[cc[54]])*.01f;
                 tempFactor[iAt]=(P0[cc[65]]+P1[cc[64]]+P2[cc[62]]+P3[cc[61]]+P4[cc[60]])*.01f;
                }
                final byte iCode=cc[26];
                final boolean isNewResidue=resNum!=resNumLast || iCode!=iCodeLast;
                resNumLast=resNum;
                iCodeLast=iCode;
                if (isNewResidue) {
                    final int iAa;
                    if (seqresAli!=null) {
                        if ( iRawAA>=nRawAA || iRawAA>=seqresAliL) break;
                        iAa=seqresAli[iRawAA];
                        if (iAa==-1 || iAa>nAA) break;
                        if (iAa<residueType.length) residueType[iAa]&=~32;
                        else if (myComputer()) putln(ANSI_RED+"Warning:"+ANSI_RESET+" residueType.length<=iAa ", protein);
                    } else iAa=iRawAA;
                    //ATOM   1941  CA ATHR A 256      36.156   8.453  44.850  0.51 17.30           C
                    if (nAA<=iAa) break;
                    residueAtomNumber[iAa]=P0[cc[10]]+P1[cc[9]]+P2[cc[8]]+P3[cc[7]]+P4[cc[6]]+P5[cc[5]];
                    residueNumber[iAa]=resNum;
                    residueName[iAa]=resTypeChain&0xffFFff;
                    if (iCode!=' ')  (insCode==null ? insCode=insCode=new byte[nAA] : insCode)[iAa]=iCode;
                    if (residueFirstAtomIdx!=null) residueFirstAtomIdx[iAa]=iAt;
                    residueChain[iAa]=chain;
                    current_iAa=iAa;
                    iRawAA++;
                } //aType==CA
                if ( aTyp!=ATOM_CA) fileHasSidechains=true;
                if (wantCAlpha && aTyp==ATOM_CA || wantSideChains) {
                    for(int ip=0,p=37;ip<3;p+=8,ip++) {  // 37 45 53
                        xyz[ip]= P_3[cc[p]] + P_2[cc[p-1]] + P_1[cc[p-2]] + P0[cc[p-4]] + P1[cc[p-5]] + P2[cc[p-6]];
                        if (cc[p-4]=='-'||cc[p-5]=='-'||cc[p-6]=='-'||cc[p-7]=='-') xyz[ip]=-xyz[ip];
                        if (atomCoord!=null) atomCoord[iAt3+ip]=xyz[ip];
                        if (cAlphaCoord!=null && aTyp==ATOM_CA) cAlphaCoord[current_iAa*3+ip]=xyz[ip];
                    }
                    if (residueLastAtomIdx!=null) residueLastAtomIdx[current_iAa]=iAt;
                }
                iAt++;
                iAt3+=3;
            }// for lines
            protein.setResidueFirstAndLastAtomIdx(residueFirstAtomIdx,residueLastAtomIdx);
            protein.setResidueType(residueType);
            protein.setResidueType32bit(residueName);
            protein.setResidueAtomNumber(residueAtomNumber);
            protein.setResidueChain(residueChain);
            protein.setResidueNumber(residueNumber);
            protein.setResidueInsertionCode(insCode);
            protein.setAtomCoordOriginal(atomCoord);
            protein.setAtomType32bit(atomType32);
            protein.setAtomType(atomType);
            protein.setAtomNumber(atomNo);
            protein.setAtomBFactor(tempFactor);
            protein.setAtomOccupancy(occupancy);
            protein.setResidueCalphaOriginal(cAlphaCoord);
            protein.setFileHasSidechainAtoms(fileHasSidechains);
            protein.setIsLoadedFromStructureFile(true);
            parseBU(T,ends,ends.length,protein);
            return true;
        }
    }
    public static String simplifyTitle(final String s0) {
        String s=delPfx("THE ",s0);
        s=delPfx("NMR ",s);
        s=delPfx("HIGH-RESOLUTION ",s);
        s=delPfx("X-RAY ",s);
        s=delPfx("CRYSTAL ",s);
        s=delPfx("SOLUYION ",s);
        s=delPfx("STRUCTURE ",s);
        s=delPfx("OF ",s);
        s=delPfx("THE ",s);
        s=delPfx("STRUCTURAL ANALYSIS OF ",s);
        s=rplcToStrg("MOLID: 1; MOLECULE: ","",s);
        return s;
    }
    /**
       Parsing biological unit like this:
       Test cases: PDB:1fnt PDB:3UNB VIPERDB:3iyu
    */
    private static Matrix3D _matrices[];
    private static void parseBU(final byte T[], final int ends[], final int nL, Protein protein) {
        int bmolecule=1, count=0, b1=-1, b2=-1;
        boolean getChains=false;
        for(int iL=0; iL<nL; iL++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1, b-1)+1;
            if (e-b<12 || T[b+7]!='3' || T[b+8]!='5' || T[b]!='R' || !strEquls("REMARK 350 ",T,b))  continue;
            if (strEquls("BIOMOLECULE:",T,b+11)) {
                bmolecule=maxi(1, atoi(T,b+23,e)); /* --- sometimes this line is incomplete --- */
            }
            if (strEquls("APPLY THE FOLLOWING TO CHAINS:",T,b+11)) {
                if (!getChains) sbChains.clr();
                getChains=true;
            }
            if (e-b>50 && strEquls("  BIOMT",T,b+11)) {
                getChains=false;
                switch(T[b+18]-'0') {
                case 1: b1=b; break;
                case 2: b2=b; break;
                default:
                    if (sze(_matrices)<count+9) _matrices=chSze(_matrices, count+99, Matrix3D.class);
                    final int num1=atoi(T,20+b1,e), num2=atoi(T,20+b2,e), num3=atoi(T,20+b,e);
                    if (num1!=num2 || num1!=num2) {
                        putln("Error parsing protein: "+protein+" The Matrix identifiers "+num1+" "+num2+" "+num3+" are not identical in line "+iL);
                    }
                    _matrices[count++]=new Matrix3D()
                        .setRotation(new double[][]{
                                {atof(T,b1+24,e),atof(T,b1+34,e),atof(T,b1+44,e)},
                                {atof(T,b2+24,e),atof(T,b2+34,e),atof(T,b2+44,e)},
                                {atof(T,b +24,e),atof(T,b +34,e),atof(T,b +44,e)}
                            })
                        .setTranslation(atof(T,b1+54,e), atof(T,b2+54,e), atof(T,b +54,e))
                        .setMoleculeAndChainsAndName(bmolecule, toStrg(sbChains), toStrg(num1));
                }
            } else if (getChains) {
                final ChTokenizer tok=_tok.setText(T,b,e).setDelimiters(chrClas(" ,"));
                while(tok.nextToken()) {
                    final int f=tok.from();
                    if (tok.to()-f==1 && is(LETTR_DIGT,T,f)) sbChains.a((char)T[f]);
                }
            }
        }
        protein.setBioMatrices(chSze(_matrices,count,Matrix3D.class)); /* !Always a copy! */
    }

}

/*
Ausnahmen:
-HETATM in SEQRES:
  106d_A*MCY  403d_B*IGU 155c_A*ACE

-  3e04_A Aminosaure ohne C-alpha

-  2amf_B:
   Es schliessen sich 2 PRO als HETAMN Zeilen an. Da PRO auch in SEQRES, Problem
   Geleich Problem  2amy_A

Mirrors
http://mmb.pcb.ub.es/pdb/getStruc.php?idCode=1sbc

Anschauen: pdb2a32_A.ent
ATOM   2510  N   GLY A 184A
ATOM   2517  N   PHE A 184      25.334  35.361  17.846  1.00 13.56           N
ATOM   1943  CA  SER A 246      50.229 -24.350 -14.885  1.00 72.69           C

*/
