package charite.christo.protein;
/**
       Compares two aligned amino acid sequences and returns a numeric value
       @author Christoph Gille
*/
public interface SequenceAlignmentScore2  {

    void setSequences(int mc, byte[] sequence1, byte[] sequence2, int fromCol, int toCol);
    void compute();
    double getValue();
    boolean isDistanceScore();
}
