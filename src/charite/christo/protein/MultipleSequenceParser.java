package charite.christo.protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import charite.christo.*;
import java.util.*;

/**
   Reads multiple sequences formats:  clustalW, MSF, WIKI:Fasta
   see
   <ul>
   <li>http://www.molecularevolution.org/mbl/resources/fileformats/</li>
   <li>http://bioinformatics.abc.hu/tothg/biocomp/other/Stockholm.html  WIKI:Stockholm_format </li>
   <li> http://www.ebi.ac.uk/help/formats_frame.html </li>
   <li> http://www.bioinformatics.nl/tools/crab_pir.html</li>
   <ul>
   @author Christoph Gille
   ?? BIOWIKI:NexusFormat
*/
public class MultipleSequenceParser {
    public final static int SIMPLE=1, MSF=2, STOCKHOLM=3, PRODOM=4, HSSP=5, FASTA=6, NEXUS=7, CLUSTALW=8,  PIR=9;
    //  http://www.molecularevolution.org/mbl/resources/fileformats
    private byte[][] _gapped, _header, _secStru;
    private int[] _seqStartAt, _seqEndAt;
    private String[] _sharedDbRefs, _pNames, _accessionIDs, _seqRefs[];
    private char _gap='-', _nameStartChar;
    public String[] getSequenceNames() { return _pNames; }
    public byte[][] getFastaHeader() { return _header; }
    public byte[][] gappedSequences() {/** @return The sequences */ return _gapped;}
    public byte[][] getSecondaryStructures() {/** @return The sequences */ return _secStru;}

    public String[] getAccessionIDs() { return _accessionIDs;}
    public String[][] getSequenceRefs() { return _seqRefs;}
    public String[] getSharedDatabaseRefs() { return _sharedDbRefs;}
    public int[] getFirstResidueIndex() {return _seqStartAt;}
    public int[] getLastResidueIndex() {return _seqEndAt;}
    public MultipleSequenceParser setGap(char g) { _gap=g; return this;}
    public MultipleSequenceParser setNamesStartWith(char start) { _nameStartChar=start; return this; }
    private static ChTokenizer _tok;
    private final ChTokenizer TOK=_tok==null ? _tok=new ChTokenizer() : _tok;

    public static int isFormat(int whatFormat, BA ba, Range returnLineRange, int[] returnProteinCount) {
        final byte[] T=ba.bytes();
        final int B=ba.begin(),E=ba.end(), ends[]=ba.eol();
        if (T==null || E-B<10 || ends.length<2) return 0;
        int proteinCount=9999;
        int fromLine=0, toLine=MAX_INT;
        final ChTokenizer tok=_tok==null ? _tok=new ChTokenizer() : _tok;
        synchronized(tok) {
            try {
                   if (whatFormat==MSF || whatFormat==CLUSTALW || whatFormat==0) {
                    // http://cubic.bioc.columbia.edu/predictprotein/Dexa/optin_msf.html
                    // http://www.pdg.cnb.uam.es/cagen/main/CAGENformats.html
                    // http://www.predictprotein.org/newwebsite/Dexa/optin_msfWWW.html
                    int countHeaderLines=0, msfHeader=0;
                    checkHeader:
                    for(int iL=0; iL<ends.length && countHeaderLines<4; iL++) {
                        final int b=iL==0?B : ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                        if (e-b>0) countHeaderLines++;
                        if (e-b>5) {
                            if ((whatFormat==MSF||whatFormat==0) && T[e-1]=='.' && T[e-2]=='.' && T[e-3]==' ') {
                                final int i=strstr(0L,"MSF:",T,b,e);
                                if (i==0 || i>0 && T[i-1]==' '  && is(DIGT, T, strchr(STRCHR_NOT,' ', T, i+4,e) ))  {
                                    msfHeader=iL;
                                    break checkHeader;
                                }
                            }
                        }
                        if ((whatFormat==CLUSTALW || whatFormat==0) && b+8<E && T[b]=='C' && T[b+1]=='L') {
                            if (strEquls("CLUSTAL FORMAT for T-COFFEE Version_",T,b)) {
                                fromLine=iL+1;
                                return CLUSTALW;
                            }
                            for(int iH=2; --iH>=0;) {
                                final String H=iH==0 ? "CLUSTAL W " : "CLUSTAL ";
                                if (strEquls(H,T,b)) {
                                    final int noSpc=nxt(-SPC,T,b+H.length(),e);
                                    if (noSpc<0 || is(DIGT, T, noSpc) || T[noSpc]=='(' && is(DIGT, T, noSpc+1) ) {
                                        fromLine=iL+1;
                                        return CLUSTALW;
                                    }
                                }
                            }
                        }
                    }
                    if (msfHeader>=0) {
                        boolean Name=false;
                        for(int iL=msfHeader; iL<ends.length; iL++) {
                            final int b=iL==0?B : ends[iL-1]+1, e=ends[iL];
                            if (!Name) {
                                if (e-b>6 && (T[b]==' ' || T[b]=='N') && strEquls("Name: ",T, strchr(STRCHR_NOT,' ',T,b,e))) Name=true;
                            } else  if (e-b>=2 && T[b]=='/' && T[b+1]=='/' && nxt(-SPC,T,b+2,e)<0) {
                                fromLine=iL+1;
                                return MSF;
                            }
                        }
                    }
                    if (whatFormat!=0) return 0;
                }

                if (whatFormat==PRODOM || whatFormat==0) {
                    if (E-B>100 && ends.length>3 && T[B]=='I' && T[B+1]=='D' && T[B+2]==' ') {
                        for(int iL=1; iL<ends.length; iL++) {
                            final int b=ends[iL-1]+1, e=ends[iL];
                            if (iL==1) {
                                if (e-b<13 || T[b]!='A' ||  T[b+1]!='C' || T[b+2]!=' ') break;
                                if (!tok.setText(T,b+2,e).nextToken() || !strEquls("PD",T,tok.from())) break;
                            } else {
                                if (e-b>46 && T[b]=='A' && T[b+1]=='L' && T[b+2]==' ') {
                                    fromLine=iL+1;
                                    return PRODOM;
                                }
                            }
                        }
                    }
                    if (whatFormat!=0) return 0;
                }
                if (whatFormat==HSSP || whatFormat==0) {
                    if  (E-B>1000 && T[B+0]=='H' && T[B+1]=='S' && strEquls("HSSP       HOMOLOGY DERIVED SECONDARY STRUCTURE OF PROTEINS",T,B)) return HSSP;
                    if (whatFormat!=0) return 0;
                }
                if (whatFormat==PIR || whatFormat==0) {
                    final int nProt=countFasta(ba, PIR);
                    if (nProt>0) {
                        proteinCount=nProt;
                        return PIR;
                    } else if (whatFormat!=0) return 0;
                }
                if (whatFormat==FASTA || whatFormat==0) {
                    final int nProt=countFasta(ba, FASTA);
                    if (nProt>0) {
                        proteinCount=nProt;
                        return FASTA;
                    } else if (whatFormat!=0) return 0;
                }

                if (whatFormat==NEXUS || whatFormat==0) {
                    // http://www.molecularevolution.org/mbl/resources/fileformats/nexus_dna.php
                    if  (E-B>20 && T[B+0]=='#' && strEquls(STRSTR_IC,"nexus",T,B+1) && is(SPC,T,B+6)) {
                        boolean beginData=false;
                        for(int iL=1; iL<ends.length; iL++) {
                            final int b=ends[iL-1]+1, e=ends[iL];
                            if (e-b<=1) continue;
                            final int lc0=T[b]|32;
                            if (fromLine>0) {
                                if (T[b]==';' || T[b+1]==';' && is(SPC,T,b) || lc0=='e' && strEquls(STRSTR_IC,"end;",T,nxt(-SPC,T,b,e))) {
                                    toLine=iL;
                                    return NEXUS;
                                }
                            } else {
                                if (beginData) {
                                    if ((lc0=='m' || (T[b+1]|32)=='m') && strEquls(STRSTR_IC,"matrix",T, nxt(-SPC,T,b,e))) fromLine=iL+1;
                                } else  {
                                    if (lc0=='b' && strEquls(STRSTR_IC,"begin data;",T,b)) beginData=true;
                                }
                            }
                        }
                        if (fromLine>0) return NEXUS;
                    }
                    if (whatFormat!=0) return 0;
                }
                if (whatFormat==STOCKHOLM || whatFormat==0) {
                    if (E-B>20 && T[B+0]=='#' && strEquls(" STOCKHOLM",T,B+1)) return STOCKHOLM;
                    if (whatFormat!=0) return 0;
                }
                if (whatFormat==SIMPLE || whatFormat==0) {
                    int blockBeginFirst=-1, nP=-1, blockBegin=-1, indent=-1, lineLength=-1;
                    boolean lineLengthDiffers=false, foundLetter=false;
                    for(int iL=0; iL<ends.length; iL++) {
                        final int b=iL==0 ? B : ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                        if (e<=b || is(SPC,T,b)) {
                            blockBegin=-1;
                            continue;
                        }
                        if (!is(LETTR_DIGT_US,T,b)) return 0;
                        if (blockBegin<0) {
                            if (lineLengthDiffers) return 0;
                            blockBegin=iL;
                            if (blockBeginFirst<0) blockBeginFirst=iL;
                        }
                        if (lineLength<0) lineLength=e-b;
                        else if (lineLength!=e-b) lineLengthDiffers=true;
                        final int iP=iL-blockBegin;
                        if (nP<=iP) nP=iP+1;
                        final int spc=strchr(STRSTR_E,' ',T,b,e);
                        if (indent>0 && spc-b>indent)  return 0;
                        final int refName=blockBeginFirst+iP>0 ? ends[blockBeginFirst+iP-1]+1 : B;
                        if (!strEquls(T, b,spc,   T,refName) || !is(SPC,T,refName+(spc-b))) return 0;
                        final int seqStart=strchr(STRCHR_NOT,' ',T,spc,e);
                        if (seqStart>0) {
                            if (indent<0) indent=seqStart-b;
                            else if (indent!=seqStart-b) return 0;
                            if (!cntainsOnly(LETTR_DASH_TILDE_DOT,T,seqStart,e)) return 0;
                            foundLetter= foundLetter || nxt(LETTR,T, seqStart,e)>0;
                        }
                    }
                    proteinCount=nP;
                    return lineLength>0 && indent>0 && nP>1  && foundLetter ? SIMPLE : 0;
                }
                return 0;
            } finally {
                if (returnLineRange!=null) { returnLineRange.set(fromLine,toLine);  }
                if (returnProteinCount!=null) { returnProteinCount[0]=proteinCount; }
            }
        }
    }

    private static boolean isPir(byte[] T, int b) {
        if (T.length<b+5 || T[b]!='>'||T[b+3]!=';') return false;
        final byte c1=T[b+1], c2=T[b+2];
        return
            (c1=='P'||c1=='F') && c2=='1' ||
            (c1=='D'||c1=='R') && (c2=='L'||c2=='C') ||
            (c1=='X'&&c2=='X');
    }
    public static int countFasta(BA ba, int format) {
        final byte[] T=ba.bytes();
        final int B=ba.begin(), ends[]=ba.eol();
        if (ends.length<2 || T[B]!='>') return 0;
        if (format==PIR && !isPir(T,B)) return 0;
        boolean isPirHeader=true;
        int count=1;
        for(int iL=1; iL<ends.length; iL++) {
            final int b=ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
            if (e<=b) continue;
            if (format==PIR ? isPir(T,b) : T[b]=='>') {
                isPirHeader=true;
                final int noSpc=nxt(-SPC,T,b+1,e);
                if (noSpc<0 || !is(LETTR_DIGT_US,T,noSpc)) return 0;
                count++;
                continue;
            }
            if (format==FASTA && !cntainsOnly(LETTR_DASH,T,b,e)) return 0;
            if (format==PIR) {
                if (!isPirHeader) {
                    for(int i=b; i<e; i++) {
                        final byte c=T[i];
                        if (c=='*' && i+1<e && !cntainsOnly(SPC,T,i+1,e)) return 0;
                        if (!(is(LETTR_DASH,c)||c==' '||c=='*')) return 0;
                    }
                }
                isPirHeader=false;
            }
        }
        return count;
    }
    private void stockholmHeader(int nP, byte[] txt, int[] ends) {
        final  BA BA=new BA(333);
        for(int iP=nP; --iP>=0;) {
            final String name=_pNames[iP];
            final int nameL=name.length();
            if (nameL==0) continue;
            final char name0=name.charAt(0);
            for(int iL=1; iL<ends.length; iL++) {
                final int b=ends[iL-1]+1;
                final int e=ends[iL];
                final int nameTo=b+5+nameL;
                if (e<=nameTo+4 || name0!=txt[b+5]) continue;
                if(  !equlsIgnoreNoWord(name,txt,b+5)||txt[nameTo]!=' ')  continue;
                if (txt[b]!='#' || txt[b+1]!='=' || txt[b+4]!=' ') continue;
                if (txt[b+2]=='G' && txt[b+3]=='S') {
                    {

                        final int fromWord=strstr(STRSTR_AFTER," AC ",txt,nameTo,e);
                        if (fromWord>0) {
                            if (_accessionIDs==null) _accessionIDs=new String[nP];
                            _accessionIDs[iP]=bytes2strg(txt,fromWord, nxtE(-LETTR_DIGT,txt,fromWord,e));
                        }
                    }
                    {
                        final int fromWord=strstr(STRSTR_AFTER," PDB; ",txt,nameTo,e);
                        if (fromWord>0) {
                            final int endWord=strchr(';', txt,fromWord,e);
                            final boolean
                                withChain=endWord-fromWord==6,   /* DR PDB; 1jm7 A; 24-64; */
                                withoutChain=endWord-fromWord==5;/* DR PDB; 1chc ; 8-46; */
                            if (withChain || withoutChain ) {
                                if (_seqRefs==null) _seqRefs=new String[nP][];
                                final BA ba=BA.clr().a("PDB:").a(txt,fromWord, fromWord+ 4);
                                if (withChain) ba.a(':').a((char)txt[fromWord+5]);
                                _seqRefs[iP]=adToStrgs(ba.toString(),_seqRefs[iP],4);
                            }
                        }
                    }
                }
                if (txt[b+2]=='G' && txt[b+3]=='R') {
                    final int iSS=strstr(STRSTR_AFTER," SS ",txt,nameTo,e);
                    if (iSS>0) {
                        final int fromWord=nxt(-SPC,txt,iSS,e);
                        final byte seq[]=_gapped[iP];
                        if (_secStru==null) _secStru=new byte[nP][];
                        final byte ss[]=_secStru[iP]=new byte[countLettrs(seq)];
                        for(int i=0, iA=0; i< seq.length; i++){
                            if (is(LETTR,seq[i])) ss[iA++]=txt[fromWord+i];
                        }
                    }
                }
            }
        }
    }
    public MultipleSequenceParser setText(BA ba) {
        final Range r=new Range();
        final int format=isFormat(0,ba,r,(int[])null);
        if (format!=0) setText(format,ba,r);
        return this;
    }

    public MultipleSequenceParser setText(int format, BA ba, Range fromToLine) {
        if (format==PIR || format==FASTA) parseFAorPIR(ba, format);
        else if (format==PRODOM) parseProdom(ba);
        else if (format==HSSP) parseHssp(ba);
        else if (format==MSF || format==STOCKHOLM || format==NEXUS || format==CLUSTALW || format==SIMPLE) parseMSF(format, ba, fromToLine);
        else assrt();
        return this;
    }
    private void parseMSF(int format, BA ba,Range lineRange) {
        final BA BA=new BA(333);
        final byte[] T=ba.bytes();
        final int ends[]=ba.eol();
        final boolean allowSlashInName=true, allowPipeInName=true, allowColonInName=format==CLUSTALW;
        boolean foundPipeInName=false, foundDashInName=false;

        final List<String> vNames=new ArrayList(); /* defines order */
        final Map<String,int[]> map=new HashMap();
        final int toLine=mini(lineRange.to(), ends.length);
        for(int iL=lineRange.from(); iL<toLine; iL++) {
            final int b0= iL==0 ? ba.begin() : ends[iL-1]+1;
            final int e=prev(-SPC,T,ends[iL]-1,b0-1)+1, b=nxt(-SPC,T,b0,e);
            if (e-b<2 || _nameStartChar!=0 && T[b0]!=_nameStartChar) continue;
            final int blank=firstBlank(T,b,e,allowSlashInName, allowPipeInName,allowColonInName);
            if (blank<0) continue;
            int countAA=0, countGG=0;
            for(int i=prev(-DIGT,T,e-1,blank-1)+1; --i>=blank; ) {
                final byte c=T[i];
                if (is(LETTR,c)) countAA++;
                else if (c=='-'||c=='.'|| c=='~' ) countGG++;
                else if (c!=' ' && c!=0 && c!='\t' && c!='\r') { countGG=countAA=-1;  break; }
            }
            if (countAA+countGG>0) {
                final String s=ba.newString(b,blank);
                final int ii[]=map.get(s);
                if (ii==null) vNames.add(s);
                map.put(s, adToArray(iL,ii,10));
            }
        }
        //debugTime("MultipleSequenceParser: protein names found duration=",time); // Throwable
        final int countNames=sze(vNames);
        byte[] buff=new byte[999];

        if (sze(map)!=countNames) assrt();
        final byte[][] gg=new byte[countNames][];
        final String nn[]=new String[countNames];
        int nProt=0;
        for(int iN=0; iN<countNames; iN++) {
            final String name=vNames.get(iN);
            final int lines[]=map.get(name);
            if (lines==null) { assrt(); continue; }
            int count=0,lastLetter=0;
            for(int iL : lines) {
                if (iL<0) break;
                final int b0= iL==0 ? ba.begin() : ends[iL-1]+1;
                final int e=ends[iL];
                final int blank=firstBlank(T,b0,e,allowSlashInName, allowPipeInName,allowPipeInName);
                if (blank<0) continue;
                final int size=count+e-blank;
                if (buff.length<size) buff=chSze(buff,size*3/2);
                final byte gapChar=(byte)_gap;
                for(int i=blank; i<e; i++) {
                    final byte c=T[i];
                    if ('a'<=c && c<='z' || 'A'<=c && c<='Z') buff[lastLetter=count++]=c;
                    else if (c=='-'||c=='.'|| c=='~')  buff[count++]=gapChar;
                }
            }
            if (lastLetter>0) {
                nn[nProt]=name;
                System.arraycopy(buff,0, gg[nProt]=new byte[lastLetter+1], 0, lastLetter+1);
                foundPipeInName|=name.indexOf('|')>0;
                foundDashInName|=name.indexOf('-')>0;
                nProt++;
            }
        }
        _gapped=chSze(gg,nProt);
        _pNames=chSze(nn,nProt);
        //debugTime("MultipleSequenceParser: gaps finished duration=",time);

        if (format==STOCKHOLM)  stockholmHeader(nProt, T, ends);
        if ( (format==MSF || format==STOCKHOLM) && (foundDashInName || foundPipeInName)) {
            int[] sAt=null, eAt=null;
            for(int iP=0; iP<nProt; iP++) {
                final String n=nn[iP];
                final int L=n.length();
                int from=0;
                while(true) {
                    final int to0=strchr(STRSTR_E,'|',n,from,L), to=strchr(STRSTR_E,'/',n,from,to0);
                    String xRef=null;
                    final int us=n.indexOf('_',from);
                    if (to-from==6 || (us>0 && us<to))  {
                        final int t=us-from==6 ? us : to;
                        if (cntainsOnly(UPPR_DIGT, n,from,  t)) xRef=BA.clr().a("UNIPROT:").a(n, from, t).toString();
                    }
                    if (to-from>10 && strEquls("ENS",n,from)) {
                        xRef=n.substring(from,to);
                    }
                    if (xRef!=null) {
                        if (_accessionIDs==null) _accessionIDs=new String[nProt];
                        _accessionIDs[iP]=xRef;
                    }
                    if (to==L) break;
                    from=to0+1;
                }

                //                 if (foundPipeInName && isMSF) {
                //                     final int lastPipe=n.lastIndexOf('|');
                //                     if (lastPipe>0) { /* --- ProDom --- */

                //                         if (_accessionIDs==null) _accessionIDs=new String[nProt];
                //                         _accessionIDs[iP]=BA.clr().a("SWISS:").a(n, n.lastIndexOf('|',lastPipe-1)+1,lastPipe).toString();
                //                     }
                //                 }
                if (foundDashInName) {
                    final int dash=n.lastIndexOf('-');
                    if (dash>0 && cntainsOnly(DIGT, n, dash+1,MAX_INT) && is(DIGT,n,dash-1)) {
                        final int firstDigt=prevE(-DIGT,n,dash-1,0)+1;
                        final char chDashOrUS=chrAt(firstDigt-1,n);
                        if (chDashOrUS=='_' || chDashOrUS=='-' || chDashOrUS=='/') {
                            if (sAt==null) {
                                _seqStartAt=sAt=new int[nProt];
                                _seqEndAt=eAt=new int[nProt];
                            }
                            sAt[iP]=atoi(n, firstDigt)-1;
                            eAt[iP]=atoi(n,dash+1)-1;
                        }
                    }
                }
            }
        }
        //debugTime("MultipleSequenceParser done duration=",time);
    }
    private final int firstBlank(byte txt[], int b, int e, boolean allowSlashInName, boolean allowPipeInName, boolean allowColonInName) {
        final int start=nxt(-SPC,txt,b,e);
        for(int i=start; i<e; i++) {
            if (i<0) continue;
            final byte c=txt[i];
            if (c==' ') return i;
            if (!( is(LETTR_DIGT_US,c) || i>start && (c=='-' || c=='.' || c=='+' || (allowSlashInName&&c=='/') || (allowPipeInName&&c=='|')  || (allowColonInName&&c==':')) ) ) return -1;
        }
        return -1;
    }
    private void parseFAorPIR(BA ba, int format) {
        final byte[] T=ba.bytes();
        final int ends[]=ba.eol(), E=ba.end();
        final BA BA=new BA(333);
        final byte bGap=(byte)_gap;
        int iSeq=0;
        for(int iL=0; iL<ends.length-1; iL++) {
            final int b= iL==0 ? ba.begin() : ends[iL-1]+1;
            final int e=ends[iL];
            if (e-b>0 &&  T[b]=='>') {
                if (e-b==1) return;
                if (_nameStartChar==0 || _nameStartChar==T[b+(format==PIR?4:1)]) iSeq++;
            }
        }
        final byte[][] gapped=new byte[iSeq][];
        final byte[][] hh=new byte[iSeq][];
        final String[] nn=new String[iSeq];
        String[][] refs=null;
        iSeq=0;
        int iL=0;
        while(iL<ends.length) {
            final int b= iL==0 ? ba.begin() : ends[iL-1]+1;
            final int e=prev(-SPC,T,ends[iL]-1,b-1)+1;
            final int seqStart=1+(format==PIR ? get(iL+1,ends) : e);
            final int nameStart=b+(format==PIR?4:1);
            if (e-b<1 || seqStart<0) continue;
            if (T[b]!='>') break;
            int next=e;
            while(iL<ends.length) {
                next=ends[iL]+1;
                if (next>=E || T[next]=='>') break;
                iL++;
            }
            iL++;
            if (_nameStartChar!=0 && _nameStartChar!=T[nameStart]) continue;
            if (next>=T.length) next=T.length;
            byte gg[]=null;
            while(true) {
                int iC=0;
                for(int iT=seqStart; iT<next && iT<T.length; iT++) {
                    final byte c=T[iT];
                    if (is(SPC,c)) continue;
                    if (gg!=null) gg[iC]= is(LETTR,c) ? c : bGap;
                    iC++;
                }
                if (gg==null) {
                    gg=new byte[iC];
                    int iT=nameStart;
                    for(; iT<e; iT++) {
                        final byte c=T[iT];
                        boolean cont=is(LETTR_DIGT_US,c) || c=='-' || c=='+';
                        if (!cont) {
                            for(int iRef=0; iRef<FASTA_REFS.length; iRef+=2) {
                                final String s=FASTA_REFS[iRef];
                                if (iT-b==s.length()+1 && strEquls(s,T, b+1)) {
                                    cont=true;
                                    break;
                                }
                            }
                        }
                        if (!cont) break;
                    }
                    if (gg!=null) {
                        if (iSeq>=nn.length) break;
                        nn[iSeq]=BA.clr().a(T,nameStart,iT).toString();
                        System.arraycopy(T,b+1,hh[iSeq]=new byte[e-b-1],0,e-b-1);
                    }
                    final String refs1[]=fastaSequenceRefs(T,b,0);
                    if (refs1.length>0)   (refs==null ? refs=new String[nn.length][] : refs)[iSeq]=refs1;
                } else break;
            }
            if (gapped.length<=iSeq) assrt();
            if (gapped.length>iSeq) gapped[iSeq++]=gg;
        }
        _seqRefs=refs;
        _gapped=gapped;
        _pNames=nn;
        _header=hh;
    }

    private final static String FASTA_REFS[]={"gi","GB:", "sp","UNIPROT:",  "pdb","PDB:"};
    private BA baRefs;
    private Collection<String>_vRefs;
    private String[] fastaSequenceRefs(byte[] T, int start, int end) {
        if (_vRefs==null) _vRefs=new ArrayList(); else _vRefs.clear();
        for(int i=start; i<end;i++) {
            if (T[i]=='|') {
                final int b=prev(-LETTR,T,i-1,start-1)+1;
                if (b>0 && i-b>1) {
                    for(int iRef=0; iRef<FASTA_REFS.length; iRef+=2) {
                        if (strEquls(FASTA_REFS[iRef],T,b)) {
                            final int idEnds=nxtE(-LETTR_DIGT_US,T,i+1,end);
                            if (baRefs==null) baRefs=new BA(20);
                            baRefs.clr().a(FASTA_REFS[iRef+1]).a(T,i+1,idEnds);
                            _vRefs.add(baRefs.toString());
                        }
                    }
                }
            }
        }
        return strgArry(_vRefs);
    }

    /*
      DR   PDB;         1I09 chain B  (60-270) KG3B_HUMAN (60-270)
      DR   PDB;         1IAN (26-244) MK14_CANFA (25-243)
    */
    private void parseProdom(BA ba){
        final byte[] T=ba.bytes();
        final int ends[]=ba.eol();
        final BA BA=new BA(333);
        byte[][] gapped=null;
        String[] names=null, refs[]=null, acc=null, sharedRefs=new String[5];
        int[] seqB=null, seqE=null, hashcde=null;
        TOK.setDelimiters(chrClas1(' '));
        while(true) {
            int count=0;
            for(int iL=1; iL<ends.length-1; iL++) {
                final int b=ends[iL-1]+1, e=ends[iL];
                if (e-b<13||T[b+2]!=' ') continue;
                final byte t0=T[b], t1=T[b+1];
                if (t0=='/') break;
                TOK.setText(T,b+3,e);
                if (t0=='A' && t1=='L') {
                    if (gapped!=null) {
                        TOK.nextToken();
                        final int f=TOK.from(), t=TOK.to();
                        TOK.nextToken();
                        seqB[count]=TOK.asInt()-1;
                        TOK.nextToken();
                        seqE[count]=TOK.asInt()-1;
                        if (TOK.nextToken() && TOK.nextToken()) {
                            gapped[count]=substrgB(T,TOK.from(),TOK.to());
                            final int pipe=strchr('|',T,f,t);
                            acc[count]=BA.clr().a("UNIPROT:").a(T, f,pipe>0?pipe:t).toString();
                            if (pipe>0) T[pipe]=(byte)'_';
                            final int nameStart= pipe>0 && strEquls(T,f,pipe,T,pipe+1)?  pipe+1 : f;
                            names[count]=BA.clr().a(T, nameStart,t).a('_').a(seqB[count]+1).a('_').a(seqE[count]+1).toString();
                            hashcde[count]=hashCd(T,pipe+1,t);
                            if(pipe>0) T[pipe]=(byte)'|';
                        }
                    }
                    count++;
                }
                if (hashcde!=null) {
                    if(t0=='D' && strEquls("DR   ",T,b)) {
                        if (TOK.nextToken() && strEquls("PDB;",T,TOK.from()) && TOK.nextToken()) {
                            final BA baTmp=BA.clr().a("PDB:").a(T,TOK.from(), TOK.to());
                            while(TOK.nextToken()) {
                                final int f=TOK.from();
                                final char c0=(char)T[f];
                                if (TOK.to()-f==1) baTmp.a(':').a((char)c0);
                                if (c0=='(') break;
                            }
                            if (!TOK.nextToken()) assrt();
                            final String name=TOK.asString();
                            final int hc=name.hashCode();
                            for(int iP=hashcde.length; --iP>=0;) {
                                if ( hashcde[iP]==hc && names[iP].indexOf(name)>=0) {
                                    refs[iP]=adToStrgs(baTmp.toString(), refs[iP],4);
                                }
                            }
                        } else {
                            TOK.setText(T,b+2,e);
                            if (TOK.nextToken() && T[TOK.to()-1]==';') {
                                final BA baTmp=BA.clr().a(T,TOK.from(),TOK.to()-1).a(':');
                                if (TOK.nextToken()) {
                                    baTmp.a(T,TOK.from(),TOK.to());
                                    sharedRefs=adToStrgs(baTmp.toString(),sharedRefs,4);
                                }
                            }
                        }
                    }
                    if (iL<3 && t0=='A' && t1=='C') {
                        if (TOK.nextToken() && strEquls("PD",T,TOK.from())) {
                            final String s=BA.clr().a("PRODOM:").a(T,TOK.from(),TOK.to()).toString();
                            sharedRefs=adToStrgs(s, sharedRefs,4);
                        }
                    }
                }

            }
            if (gapped==null) {
                _gapped=gapped=new byte[count][];
                _pNames=names=new String[count];
                _seqRefs=refs=new String[count][];
                _accessionIDs=acc=new String[count];
                _seqStartAt=seqB=new int[count];
                _seqEndAt=seqE=new int[count];
                hashcde=new int[count];
            } else break;

        }
        _sharedDbRefs=sharedRefs;
    }

    private void parseHssp(BA ba) {
        final byte[] T=ba.bytes();
        final int ends[]=ba.eol();
        final BA BA=new BA(20);
        int lineProts=0, lineAlignments=0, nProts=-1;
        String id=null;
        for(int iL=1; iL<ends.length-1; iL++) {
            final int b=ends[iL-1]+1, e=ends[iL];
            if (e-b<13) continue;
            if (T[b]=='#') {
                if (lineProts==0 && strEquls("## PROTEINS",T,b)) lineProts=iL+2;
                if (lineProts>0 && strEquls("## ALIGNMENTS",T,b)) {
                    if (lineAlignments==0) lineAlignments=iL;
                    nProts=iL-lineProts+1;
                    break;
                }
            }
            if (T[b]=='P' && strEquls("PDBID ",T,b)) id=bytes2strg(T,b+11,e).trim();
        }
        if (nProts>0 && lineAlignments>0) {
            final String names0[]=_pNames=new String[nProts];
            final String acc0[]=_accessionIDs=new String[nProts];
            final int from0[]=_seqStartAt=new int[nProts];
            final int to0[]=_seqEndAt=new int[nProts];
            names0[0]=id;
            acc0[0]="PDB:"+id;
            to0[0]=MAX_INT;
            for(int iL=lineProts, iP=1; iP<nProts; iL++, iP++) {
                final int b=ends[iL-1]+1, e=ends[iL];
                if (e-b<60) names0[iP]="error";
                else {
                    final int spc=nxt(SPC,T,b+8,e);
                    final int f=from0[iP]=atoi(T,b+48,e)-1;
                    final int t=to0[iP]=f+atoi(T,b+58,e);
                    final String nam=spc>b?bytes2strg(T,b+8,spc) : "error";
                    acc0[iP]="UNIPROT:"+nam;
                    names0[iP]=nam+"!"+f+"-"+t;
                }
            }
            final boolean[] chains=new boolean['z'+1];
            for(int iL=lineAlignments+2; iL<ends.length; iL++) {
                final byte c=get(ends[iL-1]+1,T), ch=get(ends[iL-1]+1+12,T);
                if (c=='#' || c==0 || c=='/') break;
                if (is(LETTR_DIGT,ch)) chains[ch]=true;
            }
            int iP=0;
            for(boolean b:chains) if (b) iP+=names0.length;
            final String names[]=new String[iP];
            final String acc[]=new String[iP];
            final int from[]=new int[iP];
            final int to[]=new int[iP];
            final byte[][] gapped=new byte[iP][];
            iP=0;
            for(char chain='0'; chain<='z'; chain++) {
                if (!chains[chain]) continue;
                final byte[][] gg=parseHssp(T,ends,ends.length,lineProts,chain, names0);
                for(int i=0; i<names0.length; i++) {
                    if(gg[i]==null) continue;
                    gapped[iP]=gg[i];
                    names[iP]=BA.clr().a(chain).a('_').a(names0[i]).toString();
                    acc[iP]=acc0[i];
                    from[iP]=from0[i];
                    to[iP]=to0[i];
                    iP++;
                }
            }
            if (id!=null) Arrays.fill(_seqRefs=new String[iP][],new String[]{"HSSP:"+id,"PDB:"+id});
            _pNames=chSze(names, iP);
            _accessionIDs=chSze(acc, iP);
            _seqStartAt=chSze(from, iP);
            _seqEndAt=chSze(to, iP);
            _gapped=chSze(gapped, iP);
        }
    }

    private byte[][] parseHssp(byte[] T, int[] ends, int nL, int lineProts, char chain, String[] names) {
        final byte gapped[][]=new byte[names.length][];
        //for(int i=names.length; --i>=0;) gapped[i]=NO_BYTE;
        for(int iLA=lineProts; iLA<nL; iLA++) {
            final int B0=ends[iLA-1]+1, e=ends[iLA];
            if (e>B0&&T[B0]=='#' && strEquls("## ALIGNMENTS",T,B0)) {
                final int iP0=atoi(T, B0+13, e);
                int toLine=-1, fromLine=-1;
                for(int iL=iLA+2; iL<nL; iL++) {
                    final int B=ends[iL-1]+1;
                    final boolean brk;
                    if (ends[iL]-B>14) {
                        final byte c=T[B], ch=T[B+12];
                        if (fromLine==-1 && ch==chain) fromLine=iL;
                        brk=fromLine>0 && (c=='#' || c==0 || c=='/' || ch!=chain && T[B+14]!='!');
                    } else brk=true;
                    if (brk) {  toLine=iL;  break; }
                }
                if (fromLine<0) continue;
                gapped[0]=new byte[toLine-fromLine];
                for(int iL=toLine; --iL>=fromLine;) {
                    final int iRes=iL-fromLine;
                    final int B=ends[iL-1]+1, E=ends[iL];
                    gapped[0][iRes]=E-B>14 ? T[B+14] : (byte)' ';
                    for(int i=B+51,iP=iP0;  i<E && iP<names.length; i++,iP++) {
                        if (T[i]<'A'|| 'Z'<T[i]) continue;
                        if (gapped[iP]==null) Arrays.fill(gapped[iP]=new byte[iRes+1], (byte)' ');
                        gapped[iP][iRes]=T[i];
                    }
                }
            }
        }
        return gapped;
    }
    /* <<< Parse <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */
    private static boolean equlsIgnoreNoWord(String needle, byte[] haystack, int fromH) {
        final int t=needle.length();
        for(int i=0; i<t; i++) {
            final char c=needle.charAt(i);
            if (is(LETTR_DIGT,c) && c!=haystack[fromH+i]) return false;
        }
        return true;
    }

}
