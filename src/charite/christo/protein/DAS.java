package charite.christo.protein;
import charite.christo.*;
import java.io.File;
import java.util.*;
import java.net.URL;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/*
  FYI, I have put up a static mirror of the /das/sources and /das/coordinatesystem documents:

  http://www.ebi.ac.uk/das-srv/registry/das/sources
  http://www.ebi.ac.uk/das-srv/registry/das/coordinatesystem

*/
public class DAS extends ChRenderer implements Comparator, ChRunnable  {

    public final static int DAYS_VALID=9;
    public final static String
        RUN_EXAMPLE_LOADED="DAS$$EL",
        URL_REGISTRY="http://www.dasregistry.org/das1/sources/",
        CAPABILITY_TYPE_feature="das1:features",
        UniProt="UniProt",
        AUTHORITIES[]={UniProt, "UniParc","PDBresnum","IPI","EMBL","Entrez", "NCBI", "Ensembl", "Other"},
        POSSIBLE_CAPABILITY_TYPES[]={"das1:alignment", "das1:entry_points", "das1:error_segment", CAPABILITY_TYPE_feature, "das1:interaction", "das1:maxbins", "das1:sequence", "das1:sources", "das1:structure", "das1:stylesheet", "das1:types", "das1:unknown_feature", "das1:unknown_segment"};
    /* ---------------------------------------- */
    /* >>> Classes >>> */
    public static class Capability {
        private String _t, _q;
        public String type() { return _t;}
        public String queryUri() { return _q;}
        public String toString() { return "DAS.Capability("+_t+"   "+_q+")";}
    }
    /* <<<  <<< */
    /* ======================================== */
    /* >>> class Coordinates >>> */
    public static class Coordinates {
        private String _a, _tr, _src;
        public String authority() { return _a;}
        public String testRange() { return _tr;}
        public String source() { return _src;}
        public String toString() { return "DAS.Coordinates( source="+_src+" authority="+_a+"  test_range="+_tr+")";}
    }
    /* <<< class Coordinates <<< */
    /* ======================================== */
    /* >>> class Entry >>> */
    public static class Entry implements Runnable {
        private ChRunnable _log;
        private boolean _testStarted;
        private BA _result;
        private String _title, _desc, _docHref, _time;
        private Coordinates[] _coord;
        private Capability[] _capab;
        private int _available;

        public int isAvailable(ChRunnable log) {
            if (_available==0) {
                final BA t=testResult(true, log);
                if (t!=null) {
                    final boolean ok=strstr("<DASGFF", t)>=0;
                    _available=ok ? CTRUE : CFALSE;
                    logUnavailable().a(ok?GREEN_AVAILABLE:"Not available: ").a("DAS service ").aln(title()).send();
                }
            }
            return _available;
        }

        public String urlTestResult(Coordinates c) {
            final String base=queryUri(DAS.CAPABILITY_TYPE_feature);
            if (base==null || c==null) return null;
            final String tr=c.testRange();
            if (tr==null) return null;
            return new BA(99).a(base).a("?segment=").filter(FILTER_URL_ENCODE, tr).toString();
        }
        public BA testResult(boolean startDownload, ChRunnable log) {
            if (startDownload && !_testStarted) {
                _testStarted=true;
                if (log!=null) _log=log;
                startThrd(this);
            }
            return _result;
        }

        public void run() {
             for(DAS.Coordinates coordinates : coordinates()) {
                 final String url=urlTestResult(coordinates);
                 if (url==null) continue;
                 final BA
                     res=readBytes(inStreamT(22, url, 0L)),
                     ba=new BA(sze(res)+222).a("URL: ").aln(url).a(res==null?RED_ERROR+": NO response":res);
                 if (_log!=null) _log.run(RUN_EXAMPLE_LOADED,null);
                 _result=ba.trimSize();
             }
        }

        public String title() { return _title;}
        public String description() { return _desc;}
        public String docHref() { return _docHref;}
        public String leaseTime() { return _time;}
        public Capability[] capabilities() { return _capab;}
        public Coordinates[] coordinates() { return _coord;}
        public String toString() {
            return toStrg(new BA(333).a("DAS.Entry(").a(title()).a(' ',3).a(description()).a(' ',2).aln(_docHref).join(capabilities()).a('\n').join(coordinates()));
        }
        public String queryUri(String type) {
            for(Capability c : capabilities()) {
                if (type==null || c.type()==type) return c.queryUri();
            }
            return null;
        }
    }
    /* <<< Classes <<< */
    /* ---------------------------------------- */
    /* >>> Download Entry >>> */
    public static URL getURL(String ref, char chain,  Entry entry, String type) {
        final BA baTmp=new BA(99);
        baTmp.clr()
            .a(entry.queryUri(type))
            .a(type==CAPABILITY_TYPE_feature?"?segment=":"?query=")
            .filter(FILTER_URL_ENCODE,0, ref,ref.indexOf(':')+1,MAX_INT);
        if (chain!=0) baTmp.a('.').a(chain);
        return url(baTmp.toString());
    }
    public static File downloadURL(String ref, char chain,  Entry entry, String type, ChRunnable log) {
        final URL url=DAS.getURL(ref,chain,  entry, type);
        if (log!=null) log.run(ChRunnable.RUN_APPEND,"DAS.URL= "+url+"\n");
        final String leaseTime=entry.leaseTime();
        final File fCAched=file(url), fLeaseTime=leaseTime!=null ? file(url+".leaseTime") : null;
        final boolean cache=ref!=P2U_TEST_ID && CacheResult.isEnabled();
        if (sze(fCAched)>0 && (ref!=P2U_TEST_ID && entry.isAvailable(null)==CFALSE || cache && sze(fCAched)>0 && strEquls(leaseTime, readBytes(fLeaseTime)))) {
            if (log!=null)  log.run(ChRunnable.RUN_APPEND, "DAS use "+ANSI_FG_MAGENTA+" cached "+ANSI_RESET+"value leaseTime="+leaseTime+"\n");
            return fCAched;
        }
        final int timeValid=!cache ? 0 : leaseTime==null ? DAYS_VALID : 0;

        final File f=ChUtils.urlGet(url, timeValid, (File[])null, log);
        if (sze(f)>0) wrte(fLeaseTime,leaseTime);
        if (log!=null) log.run(ChRunnable.RUN_APPEND,"DAS f="+f+" "+sze(f)+" Bytes\n");
        return f;
    }
    /* <<< Download <<< */
    /* ---------------------------------------- */
    /* >>> readRegistry >>> */
    public final static List<String> _vRegistry=new ArrayList();
    static {_vRegistry.add(URL_REGISTRY); }
    private static Entry _entries[];
    final static List _vEntries=new ArrayList(333);
    private static Map<String,Entry>  _mapEntries;
    public synchronized static Entry getEntryForTitle(String t, ChRunnable log) {
        if (t==null) return null;
        if (_mapEntries==null) {
            _mapEntries=new HashMap();
            for(Entry e : readRegistry(log)) _mapEntries.put(e.title(),e);
        }
        return _mapEntries.get(t);
    }

    public synchronized static Entry[] readRegistry(ChRunnable log) {
        if (_entries==null) {
            addAlternativeUrl("http://www.ebi.ac.uk/das-srv/registry/das/sources/",    URL_REGISTRY);
            _vEntries.clear();
            for(int i=_vRegistry.size(); --i>=0;) {
                readReg(url(_vRegistry.get(i)), _vEntries, log);
            }
            _entries=toArry(_vEntries,Entry.class);
            Arrays.sort(_entries, new DAS());
        }
        return DAS._entries;
    }
    private synchronized static void readReg(URL urlReg, List vEntries, ChRunnable log) {
        if (urlReg==null) return;
        final File fReg=urlGet(urlReg, CacheResult.isEnabled() ? DAYS_VALID : 0, (File[])null, log);
        final BA ba=readBytes(fReg);
        if (ba==null) return;
        final byte[][] AUTHORITIESbb=strgs2bytes(AUTHORITIES);
        final byte[][] TYPESbb=strgs2bytes(POSSIBLE_CAPABILITY_TYPES);
        final byte[] T=ba.bytes();
        final int E=sze(ba);
        final int ltSOURCESgt=strstr("<SOURCES>",T,ba.begin(),E);
        final List vTmp=new ArrayList();
        if (ltSOURCESgt>0) {
            for(int ltSOURCE=ltSOURCESgt; (ltSOURCE=strstr(STRSTR_AFTER,"<SOURCE ",T,ltSOURCE,E))>0; ) {
                final Entry entry=new Entry();
                final int end=strstr("</SOURCE>",T,ltSOURCE,E);
                final int sourceGT=strchr('>',T,ltSOURCE,end);
                if (null==(entry._title=xmlAttribute("title",T,ltSOURCE,sourceGT))) continue;
                entry._desc=xmlAttribute("description",T,ltSOURCE,sourceGT);
                entry._docHref=xmlAttribute("doc_href",T,ltSOURCE,sourceGT);
                vTmp.clear();
                for(int lt=ltSOURCE; (lt=strstr(STRSTR_AFTER,"<CAPABILITY ",T,lt,end))>0; ) {
                    final int gt=strchr('>',T,lt,end);
                    final Capability capability=new Capability();
                    if (null==(capability._q=xmlAttribute("query_uri", T, lt,gt))) continue;
                    if (null==(capability._t=xmlAttribute("type", T, lt,gt, POSSIBLE_CAPABILITY_TYPES, TYPESbb))) continue;
                    vTmp.add(capability);
                }
                if (vTmp.size()==0) continue;
                entry._capab=toArry(vTmp,Capability.class);
                vTmp.clear();
                for(int lt=ltSOURCE; (lt=strstr(STRSTR_AFTER,"<COORDINATES ",T,lt,end))>0;) {
                    final int gt=strchr('>',T,lt,end);
                    if (gt<0) continue;
                    final Coordinates coordinates=new Coordinates();
                    coordinates._a=xmlAttribute("authority",T,lt,gt, AUTHORITIES, AUTHORITIESbb);
                    coordinates._tr=xmlAttribute("test_range",T,lt,gt);
                    coordinates._src=xmlAttribute("source",T,lt,gt);
                    vTmp.add(coordinates);
                }
                if (vTmp.size()==0) continue;
                entry._coord=toArry(vTmp, Coordinates.class);

                for(int lt=ltSOURCE; (lt=strstr(STRSTR_AFTER,"<PROP ",T,lt,end))>0;) {
                    final int gt=strchr('>',T,lt,end);
                    if (entry._time==null) entry._time=strstr("name=\"leaseTime\"", T,lt,gt)>=0 ?  xmlAttribute("value", T,lt,gt) : null;
                }
                vEntries.add(entry);
            }
        }
    }
    /* <<< Get <<< */
    /* ---------------------------------------- */
    /* >>> Comparator  >>> */
    //     "Prosite Features (matches)"
    //    private final static String[] IMPORTANT_FEATURES={"CSA - extended", "netphos", "netoglyc", "cbs_total", "uniprot", "FireDB" };
    private static String[] _prefFeatures;
    public int compare(Object o1, Object o2) {
        if (_prefFeatures==null) _prefFeatures=custSettings(Customize.preferedDasSources);
        if (!(o1 instanceof Entry && o2 instanceof Entry)) return 0;
        final Entry e1=(Entry)o1, e2=(Entry)o2;
        final String s1=e1._title, s2=e2._title;
        final int i1=idxOfStrg(s1, _prefFeatures), i2=idxOfStrg(s2, _prefFeatures);
        if (i1!=i2) return i1>i2 ? -1 : 1;
        return s1.compareTo(s2);
    }
    /* <<< Comparator <<< */
    /* ---------------------------------------- */
    /* >>> pdb2uniprot  >>> */
    public final static String P2U_DB_SOURCE[]={UniProt};
    private static byte[] P2U_DB_SOURCEbb[];
    private final static String P2U_TEST_ID=new String("1ryp");
    private final static Map<String,String> _p2uMap=new HashMap();
    public static String pdb2uniprot(boolean noBlocking, String pdb, char chain, ChRunnable log)  {
        if (pdb==null) return null;
        final String pdb_chain=(pdb+"_"+chain).intern();
        String unip;
        synchronized(pdb_chain) {
            unip=_p2uMap.get(pdb_chain);
            if (unip==null && !noBlocking && !isSelctd(BUTTN[TOG_DEACT_DAS_PDB2U])) {
                if (P2U_DB_SOURCEbb==null) P2U_DB_SOURCEbb=strgs2bytes(P2U_DB_SOURCE);
                for(Entry e : readRegistry(log)) {
                    if (!"pdb_uniprot_mapping".equalsIgnoreCase(e.title())) continue;
                    final File f=downloadURL(pdb, chain,  e, "das1:alignment" , log);
                    final BA ba=readBytes(f);
                    if (ba==null) continue;
                    final byte[] T=ba.bytes();
                    final int E=ba.end();
                    for(int lt=ba.begin(); unip==null && (lt=strstr(STRSTR_AFTER,"<alignObject ",T,lt,E))>0; ) {
                        final int gt=strchr('>',T,lt,E);
                        final String dbSource=xmlAttribute("dbSource", T, lt,gt, P2U_DB_SOURCE, P2U_DB_SOURCEbb);
                        if (UniProt==dbSource) unip=xmlAttribute("dbAccessionId",T,lt,gt);
                    }
                    if (unip!=null) break;
                }
            }
        }
        if (pdb==P2U_TEST_ID) _p2uTestResult=unip;
        return unip;
    }

    private static String _p2uTestResult;
    public static void checkAvailabilityPdb2u() {
        startThrd(thrdM("pdb2uniprot",DAS.class, new Object[]{boolObjct(false),P2U_TEST_ID, charObjct('A'), null}));
        startThrd(thrdCR(new DAS(), "1ryp"));
    }
    public Object run(String id, Object arg) {
        if (id=="1ryp") {
            for(int i=999; --i>=0 && _entries==null; ) sleep(999);
            if (_entries==null) putln(RED_WARNING, "DAS._entries=null");
            sleep(8888);
            if (!looks(LIKE_UNIPROT_ID,_p2uTestResult)) {
                setSelctd(true,buttn(TOG_DEACT_DAS_PDB2U));
                logUnavailable().a("Not available: ");
            } else logUnavailable().a(GREEN_AVAILABLE);
            logUnavailable().a("DAS service pdb_uniprot_mapping. ").aln(_p2uTestResult).send();
        }
        return null;
    }

}

