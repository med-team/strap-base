package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
/**
   A matrix defines an affine transformation in 3D space.
   @author Christoph Gille
*/
public final class Matrix3D implements HasMC {
    public final static Matrix3D[] NONE={};
    private double _scale=1,  _xx=1, _xy, _xz, _xo,   _yx, _yy=1, _yz, _yo,   _zx, _zy, _zz=1, _zo;
    private int _biomolecule;

    /** Create a new unit matrix */
    public Matrix3D() {}
    public Matrix3D(BA ba){parse(ba);}
    public Matrix3D(
                    double xx,double xy, double xz, double xo,
                    double yx,double yy, double yz, double yo,
                    double zx,double zy, double zz, double zo) {
        _xx=xx;_xy=xy;_xz=xz; _xo=xo;
        _yx=yx;_yy=yy;_yz=yz; _yo=yo;
        _zx=zx;_zy=zy;_zz=zz; _zo=zo;
    }
    public Matrix3D(Matrix3D m) {
        this(m._xx, m._xy, m._xz, m._xo,
             m._yx, m._yy, m._yz, m._yo,
             m._zx, m._zy, m._zz, m._zo);
    }
    /* ---------------------------------------- */
    /* >>> Get  >>> */
    public double[][] getRotation() { return new double[][] { {_xx, _xy, _xz}, {_yx, _yy, _yz}, {_zx, _zy, _zz} };}
    public double[] getTranslation() { return new double[]{_xo,_yo,_zo}; }
    /* <<< Get  <<< */
    /* ---------------------------------------- */
    /* >>> Biomolecule >>> */
    private String _name, _chains;
    Matrix3D setMoleculeAndChainsAndName(int m, String chains, String name) {
        _biomolecule=m; _chains=chains; _name=name;
        _mc++;
        return this;
    }
    public String getName() { return _name;}
    public int getBiomolecule() { return _biomolecule;}
    String getChains() { return _chains;}
    /* <<< Biomolecule <<< */
    /* ---------------------------------------- */
    /* >>> Set >>> */
    /** Scale by f in all dimensions */
    public synchronized Matrix3D scale(double f) {
        _scale*=f;
        _xx*=f;_xy*=f;_xz*=f;_xo*=f;
        _yx*=f;_yy*=f;_yz*=f;_yo*=f;
        _zx*=f;_zy*=f;_zz*=f;_zo*=f;
        _mc++;
        return this;
    }
    /** rotate theta degrees about the x,y,z axis */
    public synchronized Matrix3D rotate(char axis, double theta) {
        final double ct=Math.cos(theta),  st=Math.sin(theta);
        double
            Nxx=_xx, Nxy=_xy ,Nxz=_xz, Nxo=_xo,
            Nyx=_yx, Nyy=_yy ,Nyz=_yz, Nyo=_yo,
            Nzx=_zx, Nzy=_zy ,Nzz=_zz, Nzo=_zo;
        if ('x'==axis) {
            Nyx=_yx*ct + _zx*st;
            Nyy=_yy*ct + _zy*st;
            Nyz=_yz*ct + _zz*st;
            Nyo=_yo*ct + _zo*st;
            Nzx=_zx*ct - _yx*st;
            Nzy=_zy*ct - _yy*st;
            Nzz=_zz*ct - _yz*st;
            Nzo=_zo*ct - _yo*st;
        } else if ('y'==axis) {
            Nxx=_xx*ct + _zx*st;
            Nxy=_xy*ct + _zy*st;
            Nxz=_xz*ct + _zz*st;
            Nxo=_xo*ct + _zo*st;
            Nzx=_zx*ct - _xx*st;
            Nzy=_zy*ct - _xy*st;
            Nzz=_zz*ct - _xz*st;
            Nzo=_zo*ct - _xo*st;
        } else if ('z'==axis) {
            Nyx=_yx*ct + _xx*st;
            Nyy=_yy*ct + _xy*st;
            Nyz=_yz*ct + _xz*st;
            Nyo=_yo*ct + _xo*st;
            Nxx=_xx*ct - _yx*st;
            Nxy=_xy*ct - _yy*st;
            Nxz=_xz*ct - _yz*st;
            Nxo=_xo*ct - _yo*st;
        }
        _xx=Nxx; _xy=Nxy; _xz=Nxz;  _xo=Nxo;
        _yx=Nyx; _yy=Nyy; _yz=Nyz;  _yo=Nyo;
        _zx=Nzx; _zy=Nzy; _zz=Nzz;  _zo=Nzo;
        _mc++;
        return this;
    }
    /** Multiply this matrix by a second: M=M*R */
    public Matrix3D mult(Matrix3D rhs) {
        if (rhs==null) return this;
        final double
            lxx=_xx*rhs._xx + _yx*rhs._xy + _zx*rhs._xz,
            lxy=_xy*rhs._xx + _yy*rhs._xy + _zy*rhs._xz,
            lxz=_xz*rhs._xx + _yz*rhs._xy + _zz*rhs._xz,
            lxo=_xo*rhs._xx + _yo*rhs._xy + _zo*rhs._xz + rhs._xo,
            lyx=_xx*rhs._yx + _yx*rhs._yy + _zx*rhs._yz,
            lyy=_xy*rhs._yx + _yy*rhs._yy + _zy*rhs._yz,
            lyz=_xz*rhs._yx + _yz*rhs._yy + _zz*rhs._yz,
            lyo=_xo*rhs._yx + _yo*rhs._yy + _zo*rhs._yz + rhs._yo,
            lzx=_xx*rhs._zx + _yx*rhs._zy + _zx*rhs._zz,
            lzy=_xy*rhs._zx + _yy*rhs._zy + _zy*rhs._zz,
            lzz=_xz*rhs._zx + _yz*rhs._zy + _zz*rhs._zz,
            lzo=_xo*rhs._zx + _yo*rhs._zy + _zo*rhs._zz + rhs._zo;
        _xx=lxx; _xy=lxy; _xz=lxz; _xo=lxo;
        _yx=lyx; _yy=lyy; _yz=lyz; _yo=lyo;
        _zx=lzx; _zy=lzy; _zz=lzz; _zo=lzo;
        return this;
    }
    public synchronized Matrix3D newInverse() {
        final Matrix3D m=new Matrix3D();
        m._xo=- _xx*_xo-_yx*_yo-_zx*_zo;
        m._xx=_xx; m._xy=_yx; m._xz=_zx;
        m._yo= -_xy*_xo-_yy*_yo-_zy*_zo;
        m._yx=_xy;m._yy=_yy;m._yz=_zy;
        m._zo= -_xz*_xo-_yz*_yo-_zz*_zo;
        m._zx=_xz;m._zy=_yz;m._zz=_zz;
        _mc++;
        return m;
    }
    /** Set to unit matrix */
    public synchronized Matrix3D unit() { _scale=_xx=_yy=_zz=1;_xo=_yo=_zo=_xy=_xz=_yx=_yz=_zx=_zy=0;  _mc++; return this;  }
    public synchronized Matrix3D setTranslation(double tx, double ty, double tz) {
        _xo=tx; _yo=ty; _zo=tz;
        _mc++;
        return this;
    }
    public synchronized Matrix3D setTranslation(double t[]) { return setTranslation(t[0],t[1],t[2]);}
    public synchronized Matrix3D setRotation(double r[][]) {
        if (r!=null) {
            _xx=r[0][0];_xy=r[0][1];_xz=r[0][2];
            _yx=r[1][0];_yy=r[1][1];_yz=r[1][2];
            _zx=r[2][0];_zy=r[2][1];_zz=r[2][2];
            _mc++;
        }
        return this;
    }
    /* <<< Set <<< */
    /* ---------------------------------------- */
    /* >>> Parse >>> */
    public boolean parsePlain(byte text[], int begin, int end) {
        if (sze(text)==0) return false;
        final ChTokenizer TOK=new ChTokenizer();
        TOK.setText(text, begin, end);
        final double d[]=new double[12];
        for(int i=0;i<12;i++) {
            if (!TOK.nextToken()) return false;
            if (Double.isNaN(d[i]=TOK.asFloat())) return false;
        }
        _xx=d[0];
        _xy=d[1];
        _xz=d[2];
        _yx=d[3];
        _yy=d[4];
        _yz=d[5];
        _zx=d[6];
        _zy=d[7];
        _zz=d[8];
        _xo=d[9];
        _yo=d[10];
        _zo=d[11];
        _mc++;
        return true;
    }
    public boolean parse(BA ba) {
        final byte T[]=ba.bytes();
        final int E=ba.end(), ends[]=ba.eol();
        int iRot=-1, iTrans=-1;
        for(int iL=0; iL<ends.length; iL++) {
            final int b0= iL==0 ? ba.begin() : ends[iL-1]+1,  e=prev(-SPC,T,ends[iL]-1,b0-1)+1, b=nxtE(-SPC,T,b0, e);
            if (e-b>0 && T[b]=='-') {
                if (e-b>8  && iRot<0   && strEquls("-ROTATION",T,b)) iRot=e;
                if (e-b>11 && iTrans<0 && strEquls("-TRANSLATION",T,b)) iTrans=e;
            }
            if (e-b>0 && T[b]=='<') {
                if (e-b>9  && strEquls("<rotation>",T,b)) iRot=b+10;
                if (e-b>12 && strEquls("<translation>",T,b)) iTrans=b+13;
            }
        }
        ChTokenizer TOK=null;
        if (iRot>0) {
            if (TOK==null) TOK=new ChTokenizer();
            TOK.setText(T, iRot, E);
            TOK.nextToken(); _xx=TOK.asFloat();
            TOK.nextToken(); _xy=TOK.asFloat();
            TOK.nextToken(); _xz=TOK.asFloat();
            TOK.nextToken(); _yx=TOK.asFloat();
            TOK.nextToken(); _yy=TOK.asFloat();
            TOK.nextToken(); _yz=TOK.asFloat();
            TOK.nextToken(); _zx=TOK.asFloat();
            TOK.nextToken(); _zy=TOK.asFloat();
            TOK.nextToken(); _zz=TOK.asFloat();
        }
        if (iTrans>0)  {
            if (TOK==null) TOK=new ChTokenizer();
            TOK.setText(T,iTrans,E);
            TOK.nextToken(); _xo=TOK.asFloat();
            TOK.nextToken(); _yo=TOK.asFloat();
            TOK.nextToken(); _zo=TOK.asFloat();
        }
        _mc++;
        return !isNaN() && iRot>=0 && iTrans>=0;
    }
    /* <<< Parse <<< */
    /* ---------------------------------------- */
    /* >>>  Analyze >>> */
    public boolean isUnit() {
        return
            .9999<_xx && _xx<1.0001  && .9999<_yy && _yy<1.0001 && .9999<_zz && _zz<1.0001  &&
            -.0001<_xy&&_xy<0.0001 &&
            -.0001<_xz&&_xz<0.0001 &&
            -.0001<_yz&&_yz<0.0001 &&
            -.0001<_xo&&_xo<0.0001 &&
            -.0001<_yo&&_yo<0.0001 &&
            -.0001<_zo&&_zo<0.0001;
    }
    public double getDeterminant(boolean normalize) {
        /*
          _xx _xy _xz    _xx _xy
          _yx _yy _yz    _yx _yy
          _zx _zy _zz    _zx _zy
        */
        final double d= _xx*_yy*_zz + _xy*_yz*_zx +  _xz*_yx*_zy  - _zx*_yy*_xz-  _zy*_yz*_xx -  _zz*_yx*_xy,  s=_scale;
        return normalize&&s!=0 ? d/(s*s*s) : d;
    }
     boolean isNaN() {
        return
            Double.isNaN(_xx) || Double.isNaN(_xy) || Double.isNaN(_xz) ||
            Double.isNaN(_yx) || Double.isNaN(_yy) || Double.isNaN(_yz) ||
            Double.isNaN(_zx) || Double.isNaN(_zy) || Double.isNaN(_zz) ||
            Double.isNaN(_xo) || Double.isNaN(_yo) || Double.isNaN(_zo);
    }
    /* <<< Analyze <<< */
    /* ---------------------------------------- */
    /* >>> Apply >>> */
    public synchronized float[] transformPoints(float v[], float tv0[],int n0) {
        final int n=mini(n0,v.length/3);
        final float
            tv[]=tv0!=null?tv0:new float[3*n],
            lxx=(float)_xx, lxy=(float)_xy, lxz=(float)_xz, lxo=(float)_xo,
            lyx=(float)_yx, lyy=(float)_yy, lyz=(float)_yz, lyo=(float)_yo,
            lzx=(float)_zx, lzy=(float)_zy, lzz=(float)_zz, lzo=(float)_zo;
        for(int i=n*3; (i -=3) >=0;) {
            final float x=v[i], y=v[i+1], z=v[i+2];
            tv[i  ]=x*lxx + y*lxy + z*lxz + lxo;
            tv[i+1]=x*lyx + y*lyy + z*lyz + lyo;
            tv[i+2]=x*lzx + y*lzy + z*lzz + lzo;
        }
        return tv;
    }
    /* <<< Apply <<< */
    /* ---------------------------------------- */
    /* >>> Distance >>> */
    private static double squareDist(float x1,float y1, float z1,   float x2,float y2, float z2 ) {
        final float dx=x2-x1, dy=y2-y1, dz=z2-z1;
        return dx*dx+dy*dy*dz*dz;
    }
    private static double squareDistOfPointAndLine(float xP,float yP, float zP, float x1,float y1, float z1,   float x2,float y2, float z2) {
        final double square12=squareDist(x1,y1,z1, x2,y2,z2), square1P=squareDist(x1,y1,z1, xP,yP,zP);
        if (square12<1E-10) return squareDist(xP,yP,zP, x1,x2,xP);
        final double skalarProdukt= ((x2-x1)*(xP-x1) + (y2-y1)*(yP-y1) + (z2-z1)*(zP-z1));
        final double square1F=skalarProdukt*skalarProdukt/square12;
        return square1P-square1F;
    }
    private static void mostDistantPoints2(float xyz[], int len, int[] result) {
        final int L=mini(xyz.length/3, len);
        double maxDist=Double.MAX_VALUE;
        for(int i=L; --i>=0;) {
            final int i3=3*i;
            for(int j=L; --j>i;) {
                final int j3=3*j;
                final double d=squareDist(xyz[i3],xyz[i3+1],xyz[i3+2], xyz[j3],xyz[j3+1],xyz[j3+2]);
                if (d>maxDist) {
                    maxDist=d;
                    result[0]=i;
                    result[0]=j;
                }
            }
        }
    }
    public static void mostDistantPoints3(float xyz[], int len, int[] result) {
        result[0]=result[1]=result[2]=-1;
        final int L=mini(xyz.length/3, len);
        mostDistantPoints2(xyz,L,result);
        double maxDist=Double.MAX_VALUE;
        final float x0,y0,z0, x1,y1,z1;
        {
            final int i0=result[0], i1=result[1];
            x0=xyz[i0]; y0=xyz[i0]; z0=xyz[i0];
            x1=xyz[i1]; y1=xyz[i1]; z1=xyz[i1];
        }
        for(int i=L; --i>=0;) {
            final int i3=3*i;
            final double d=squareDistOfPointAndLine(xyz[i3],xyz[i3+1],xyz[i3+2], x0,y0,z0, x1,y1,z1);
            if (d>maxDist) {
                maxDist=d;
                result[3]=i;
            }
        }
    }
    /* <<< Distance <<< */
    /* ---------------------------------------- */
    /* >>> equals >>> */
    private int _mc=0;
    public int mc() { return _mc;}
    @Override public int hashCode() { return (int)(999999*(_xx+_yy+_zz+ _xy+_xz+ _yz +_xo+_yo+_zo));  }
    @Override public boolean equals(Object o) {
        if (o instanceof Matrix3D) {
            final Matrix3D m=(Matrix3D)o;
            return m!=null&& _xx==m._xx&&_xy==m._xy&&_xz==m._xz&&_xo==m._xo&&_yx==m._yx&&_yy==m._yy&&_yz==m._yz&&_yo==m._yo&&_zx==m._zx&&_zy==m._zy&&_zz==m._zz&&_zo==m._zo;
        } else return false;
    }

    /* <<< equals <<< */
    /* ---------------------------------------- */
    /* >>> Export >>> */
    @Override public String toString() { return toText(FORMAT_XML, null, null).toString(); }
    public final static int FORMAT_PLAIN=1, FORMAT_JMOL=2, FORMAT_PYMOL=3, FORMAT_EXPLAIN=4, FORMAT_ROUNDED=5, FORMAT_XML=6;

    public BA toText(int format, String linePrefix, BA sb0) {
        final BA sb=sb0!=null?sb0:new BA(222);
        if (format==FORMAT_XML) {
            sb.a("<rotation>\n")
                .a(_xx).a('\t').a(_xy).a('\t').a(_xz).a('\n')
                .a(_yx).a('\t').a(_yy).a('\t').a(_yz).a('\n')
                .a(_zx).a('\t').a(_zy).a('\t').a(_zz).a("\n</rotation>\n<translation>\n")
                .a(_xo).a('\t').a(_yo).a('\t').a(_zo).a("\n</translation>\n");
        } else if (format==FORMAT_JMOL) {
            sb.a("r=[ ")
                .a(" [ ").a((float)_xx).a(", ").a((float)_xy).a(", ").a((float)_xz).a(" ], ")
                .a(" [ ").a((float)_yx).a(", ").a((float)_yy).a(", ").a((float)_yz).a(" ], ")
                .a(" [ ").a((float)_zx).a(", ").a((float)_zy).a(", ").a((float)_zz)
                .a(" ] ];\n t={ ").a((float)_xo).a(", ").a((float) _yo).a(", ").a((float) _zo).a(" };\n");

        } else if (format==FORMAT_PYMOL) {
            sb
                .a(_xx).a(", ").a(_xy).a(", ").a(_xz).a(", ").a(_xo).a(",  ")
                .a(_yx).a(", ").a(_yy).a(", ").a(_yz).a(", ").a(_yo).a(",  ")
                .a(_zx).a(", ").a(_zy).a(", ").a(_zz).a(", ").a(_zo)
                .a("  , 0, 0, 0, 1");
        } else if (format==FORMAT_EXPLAIN) {
            final String pfx=linePrefix!=null ? linePrefix : "";
            sb
                .a(pfx).a("X' = X *").a(_xx,4,3).a(" +  Y *").a(_xy,4,3).a(" +  Z *").a(_xz,4,3).a(" + ").a(_xo,4,3).a('\n')
                .a(pfx).a("Y' = Y *").a(_yx,4,3).a(" +  Y *").a(_yy,4,3).a(" +  Z *").a(_yz,4,3).a(" + ").a(_yo,4,3).a('\n')
                .a(pfx).a("Z' = Z *").a(_zx,4,3).a(" +  Y *").a(_zy,4,3).a(" +  Z *").a(_zz,4,3).a(" + ").a(_zo,4,3).a('\n');
        } else if (format==FORMAT_ROUNDED) {
            sb
                .a(_xx,4,3).a(' ').a(_xy,4,3).a(' ').a(_xz,4,3).a("  ")
                .a(_yx,4,3).a(' ').a(_yy,4,3).a(' ').a(_yz,4,3).a("  ")
                .a(_zx,4,3).a(' ').a(_zy,4,3).a(' ').a(_zz,4,3).a("  ")
                .a(_xo,4,3).a(' ').a(_yo,4,3).a(' ').a(_zo,4,3);
        } else {
            sb
                .a((float)_xx).a(' ').a((float)_xy).a(' ').a((float)_xz).a("  ")
                .a((float)_yx).a(' ').a((float)_yy).a(' ').a((float)_yz).a("  ")
                .a((float)_zx).a(' ').a((float)_zy).a(' ').a((float)_zz).a("  ")
                .a((float)_xo).a(' ').a( (float)_yo).a(' ').a( (float)_zo);
        }
        return sb;

    }
}
