package charite.christo.protein;
import static charite.christo.ChUtils.*;
public interface PredictSubcellularLocation {
    void setResidueType(byte[] aa);
    void setOrganism(int organism);
    Prediction[] getCompartments();
    public final static int
        HUMAN=0,
        UNKNOWN=101,CYTOSOL=102,MITO_MATRIX=103,ER=104, MICROSOME=105, NUCLEUS=106, PEROXISOME=107,
        MEMBRANE=108, LYSOSOME=109, GOLGI=110, SYNAPSE=111, CYTOSKELETON=112, CENTRIOLE=113,SECRETED=114, MAX_VARIABLE=115;
    public static class Prediction {
        public Prediction(int compartment, int score, int textPosition) { _compartment=compartment; _score=score; _textPosition=textPosition;}
        private final int _compartment, _textPosition;
        private int _score;
        private static String[] _organisms;
        public int getCompartment() { return _compartment;}
        public int getScore() { return _score;}
        public void setScore(int score) { _score=score;}
        public int getTextPosition() { return _textPosition;}

        public static String[] getOrganisms() {
            if (_organisms==null) _organisms=chSze(finalStaticInts(PredictSubcellularLocation.class,MAX_VARIABLE),HUMAN+1);
            return _organisms;
        }
        public static Prediction[] text2prediction(byte txt[], int from, int end) {
            if (txt==null) return null;
            Prediction[] pp=null;
            while(true) {
                int count=0;
                for(int c=UNKNOWN+1; c<=CENTRIOLE; c++) {
                    final String s=
                        c==MEMBRANE ? "membran" :
                        c==CYTOSOL ? "cytoplasm" :
                        c==MITO_MATRIX ? "mitochon" :
                        c==ER ? "endoplasmic" :
                        c==MICROSOME ? "microsome" :
                        c==NUCLEUS ? "nucleus" :
                        c==PEROXISOME ? "peroxisom" :
                        c==SYNAPSE ? "synaps" :
                        c==CYTOSKELETON ? "sytoskeleton" :
                        c==CENTRIOLE ? "centriol" :
                        c==GOLGI ? "golgi" :
                        c==LYSOSOME ? "lysosom" :
                        c==SECRETED ? "secreted" :
                        null;

                    int e=end;
                    if (strstr(STRSTR_IC,"/plocdir/",txt,0,end)>0) e=strstr(STRSTR_IC,"</h2><img ",txt,0,end);
                    final int i=strstr(STRSTR_IC,s,txt,from,e);
                    if (i>0) {
                        if (pp!=null) pp[count]=new Prediction(c,Integer.MIN_VALUE,i);
                        count++;
                    }
                }
                if (pp==null) pp=new Prediction[count]; else break;
            }
            /* Bubble sort */
            for(int i=pp.length; --i>=0; ) {
                for(int j=pp.length; --j>i; ) {
                    if (pp[i]._textPosition>pp[j]._textPosition) {
                        Prediction p=pp[i]; pp[i]=pp[j]; pp[j]=p;
                    }
                }
            }
            return pp;
        }
    }
    public final Prediction[] ERROR={};

}
