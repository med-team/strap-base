package charite.christo.protein;
import charite.christo.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.KeyEvent.*;
import static java.awt.event.MouseEvent.*;

/**HELP

   <i>PACKAGE:charite.christo.strap.</i>

   The simple 3D-viewer visualizes the C-alpha trace of one or several proteins as a polygon.
   Helices may be drawn in red and sheets in yellow or the entire chain is drawn in one color.

   There are a few advantage of the full featured sophisticated viewers also available in Strap:
   <ul>
   <li>Starts  without delay</li>
   <li>Uses minimal memory</li>
   <li>Applies transformation matrices to visualize biological molecules</li>
   </ul>

   <i>SEE_DIALOG:V3dUtils,H</i>
   @author Christoph Gille

*/
public class Protein3d extends ChPanel implements Disposable,ChRunnable, TooltipProvider, ProcessEv, PaintHook {
    private final static byte  LINE=1,DISC=12,PLUS=13,CROSS=14,LETTER=15, SEC_STRU=-1, CHAINS=-2, INVISIBLE=-3;
    public final static String
        KEY_INITIALLY_SHOW="P3$$IS", KEY_BIO_MOLECULES="P3$$M", KEY_INTERPRET_AFTER_LOADING="P3$$IAL",
        ID_DISPOSED="P3D$$3", ID_PROTEIN_VIEWER="P3D$$5", SPIN="P3D$$spin", KEY_MSG_NO_PROTEIN="P3D$$KMNP";

    final static String  RUN_LAYOUT="U";
    private static Object _lines;
    private static float[] _xyzT;
    private final EvAdapter LI=new EvAdapter(this);
    private final UniqueList<PView> _vDisplayed=new UniqueList(PView.class);
    private final UniqueList<JComponent> vNW=new UniqueList(JComponent.class), vNE=new UniqueList(JComponent.class);
    private JSlider _sliderDistDna;
    private int _durationPaint=33, _meanDuration=33, _size, _countLines, _ox, _oy,  _prevx, _prevy, _pMC, _thumbY[];
    private long _whenKeyEvent;
    private double _dxMean, _dyMean, _dzMean, _mzT;
    private float _scale=1, _xyzCursor[];
    private final Runnable _runLayout=thrdCR(this,RUN_LAYOUT);
    private final Matrix3D _tmat=new Matrix3D(),  _m3D=new Matrix3D().scale(5f);
    private final Line MOUSE=new Line(),MOUS_HET=new Line();
    private boolean _autoRotating, _dragged, _isWheel, _notYetScaled=true, _mouseIn, _hideDistantDNS;

    private final ChCombo _comboBio;
    private final ChButton
        _togBioColor=toggl(ChButton.ICON_SIZE,null).li(LI).i(IC_COLOR).tt("Each chain in a different color"),
        _cbGui=toggl("List proteins").r(_runLayout),
        _cbDetailBio=toggl("Chain-specific settings").li(LI)
        .tt("Toggle detailed/simple view.<br>The check-box is enabled if more than one molecules exists.<br>For most PDB entries, however, there is only one molecule with number 1");

    private Object[][] _dataBio[]=new Object[2][][];
    private ChJTable _tableBio;
    private final JComponent
        PAN_SE=pnl(),
        _transp[]=new JComponent[99];
    {
        final String tt="The four options are <ol>"+
            "<li>The  Asymmetric_unit* as recorded in the ATOM-lines of the PDB-file.</li>"+
            "<li>The WIKI:Biological_unit is  reconstructed from the BIOMT Matrices.</li>"+
            "<li>Same as above. Each chain gets a different color to contrast adjacent chains.</li>"+
            "<li>The user can specify what bio-molecules he wants to see.</li>"+
            "</ol>",
            oo[]={"Asymmetrical unit","Biological unit 1","Customized"};
        _comboBio=new ChCombo(ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT, oo).li(LI).tt(tt).s(1);
        LI.addTo("M", _comboBio);
        pcp(KOPT_HIDE_IF_DISABLED,"",_comboBio);

    }    /* ---------------------------------------- */
    /* >>> Instance >>> */
    public Protein3d() {
        super();
        pcp(KEY_PRINTABLE_COMPONENTS, wref(this),this);
        rtt(this);
        vNW.add(_comboBio);
        LI.addTo("Mfkmw", this);
        setMinSze(1,1, this);
        setBackground(C(0));
        setLayout(null);
    }
    public boolean isPreview() {
        for(PView dp : _vDisplayed.asArray()) if (dp._m3dPrev!=null) return true;
        return false;
    }
    private Matrix3D mx3d(PView pv) {
        if (pv==null) return null;
        if (isPreview()) return pv._m3dPrev;
        final Protein p=pv.getProtein();
        return pv._m3dPrev!=null ? pv._m3dPrev : p!=null ? p.getRotationAndTranslation() : null;
    }
    public void dispose() {
        _autoRotating=false;
        _vDisplayed.clear();
        run(ID_DISPOSED,null);
        for(PView pv : _vDisplayed.asArray()) {
            final Protein p=pv.getProtein();
            if (p!=null) p.removeProteinViewer(pv);
            pv.dispose();
        }
        _lastG=null;
    }
    /* <<<  Instance <<< */
    /* ---------------------------------------- */
    /* >>> getProteins >>> */
    private Object _pp;
    public Protein[] getProteins() {
        final PView dd[]=_vDisplayed.asArray();
        final int mc=_vDisplayed.mc();
        Protein pp[]=deref(_pp, Protein[].class);
        if (pp==null || _pMC!=mc)  {
            _pMC=mc;
            _pp=newSoftRef(pp=new Protein[dd.length]);
            for(int i=dd.length; --i>=0;) pp[i]=dd[i].getProtein();
        }
        return pp;
    }
    private PView initDP(PView dp, Protein p, boolean doInit) {
        dp._p=p;
        dp._p3d=this;
        final Object weakRef=wref(dp);
        dp._cbCS=toggl(ChButton.PAINT_IF_ENABLED,"AS").t("Active").tt("Show active site residues").s(true);
        for(ResidueSelection s : p.allResidueSelections()) {
            if (gcp(KEY_INITIALLY_SHOW,s)!=null) {
                dp._cbCS.s(true);
                break;
            }
        }
        int count=0;
        final Component bb[]=new Component[99];
        dp._cbHetero=toggl(ChButton.PAINT_IF_ENABLED, "Hetero").s(true)
            .tt("Show hetero groups such as flavins<br>With Shift-key: List all hetero compounds<br>Shift-key and Ctrl-key: Toggle atom color");

        bb[count++]=dp._cbHetero.cb();
        bb[count++]=dp._cbCS.cb();
        bb[count++]=addPaintHook(this,dp._colorChooser=new ChButton().ps(dim(( (COLORS.length-1)/3+2) * ICON_HEIGHT/3,ICON_HEIGHT)).tt(this));
        bb[count++]=dp._butNucl=new ChButton(ChButton.ICON_SIZE|ChButton.PAINT_IF_ENABLED,"N").i(IC_DNA).tt("Show nucleotide acid<br>With Shift-key: List of DNAs/RNAs.");
        bb[count++]=new ChButton(ChButton.ICON_SIZE, "CLOSE").t(null).rover(IC_CLOSE).tt("Remove the protein from this 3D-view");
        for(Component b : bb) {
            if (b==null) continue;
            pcp(PView.class, weakRef, b);
            if (b instanceof ChButton) ((ChButton)b).li(LI);
            LI.addTo("m",b);
        }
        dp._pLabel=runCR(p.getAlignment(), Protein.RUN_ALIGNMENT_GET_PROTEIN_LABEL,p);
        dp._toolPnl=pnl(CNSEW,pnl(dp._pLabel),null,null, pnl(bb),"#LBB");
        LI.addTo("m",dp._toolPnl);
        addMoli(MOLI_REFRESH_TIP,dp._colorChooser);
        dp._iColor= sze(p.getChainsAsString())<2  ? SEC_STRU : CHAINS;
        _vDisplayed.add(dp);
        if (doInit) afterAddProteins(p);
        return dp;
    }

    public void addProteins(Protein...pp0) {
        if (countNotNull(pp0)==0) return;
        if (!isEDT()) { inEdtCR(this,"ADD_PROTEIN",pp0); return; }
        final Protein[] ppLoaded=getProteins(), pp=pp0.clone();
        for(int iP=sze(pp); --iP>=0; ) {
            if (cntains(pp[iP],ppLoaded)) pp[iP]=null;
        }
        final Protein[] ppTrim=rmNullA(pp,Protein.class);
        if (ppTrim.length>0) {
            for(Protein p : ppTrim) {
                final PView pv=new PView();
                p.addProteinViewer(pv);
                initDP(pv,p,false);
            }
            afterAddProteins(ppTrim);
        }
    }

    private void afterAddProteins(Protein... pp) {
        if (pp==null) return;
        //  if (!isEDT()) { inEdtCR(this,"ADD_PROTEIN",pp);     return; }
        boolean hasMolecule1=false;
        final int propMol=MIN_INT; // ???
        for(int i=0; i<pp.length; i++) {
            final Protein p=pp[i];
            if (p.getResidueCalpha()!=null || p.getHeteroCompounds('H').length+p.getHeteroCompounds('N').length>0) {
                final String pdb=p.getPdbID();
                for(Protein prot : getProteins()) {
                    final String pdbId=prot.getPdbID();
                    if (pdb!=null && pdbId==pdb || pdbId==null) {
                        for(Matrix3D m3d : prot.getBioMatrices()) {
                            if (m3d.getBiomolecule()==1) hasMolecule1=true;
                        }
                    }
                }

                //final Object Molecules=gcp(KEY_BIO_MOLECULES,p); if (Molecules!=null && dp0!=null) dp0.molecules=propMol=atoi(Molecules);
            }
        }
        if (propMol!=MIN_INT) _comboBio.s(propMol==-1?0:2);
        else if (hasMolecule1)  {
            for(PView dp : _vDisplayed.asArray()) dp._molecules=1<<1;
            _comboBio.s(1);
        }
        drawProteins(null);
        if (_countLines==0) _comboBio.s(0);
        _notYetScaled=true;
        String lastPdb=null;
        for(Protein p : getProteins()) {
            final String pdb=p.getPdbID();
            if (lastPdb!=null && !lastPdb.equals(pdb)) { _comboBio.s(0); break; }
            lastPdb=pdb;
            final PView v=getView(p);
            for(Object cmd : oo(gcp(KEY_INTERPRET_AFTER_LOADING,p))) {
                if (v!=null) v.interpret(0L, toStrg(cmd));
            }
        }
    }
    public void removeProtein(Protein p) {
        if (p==null) return;
        final PView dp=getView(p);
        _vDisplayed.remove(dp);
        inEDTms(_runLayout,111);
        addProteins((Protein[])null);
    }
    /* <<< getProteins <<< */
    /* ---------------------------------------- */
    /* >>> Protein information  >>> */
    public boolean isProteinShown(Protein p) {
        for(PView dp : _vDisplayed.asArray())  if (dp.getProtein()==p) return dp._iColor!=INVISIBLE;
        return false;
    }
    public static Color chain2color(int i) {
        return COLORS[(i + (26-'A'%26)) % COLORS.length][0];
    }

    public boolean eachChainOneColor(boolean asChain, boolean testOnly) {
        boolean changed=false;
        final PView dd[]=_vDisplayed.asArray();
        for(int i=0; i<dd.length; i++) {
            final PView dp=dd[i];
            final Protein p=dp!=null ? dp.getProtein() : null;
            if (p==null) continue;
            final String chains=p.getChainsAsString();
            final int c=chrAt(0,chains);
            int iColor=COLORS.length-1-(i%COLORS.length);
            if (asChain && sze(chains)==1 && is(LETTR_DIGT,c)) iColor=is(DIGT,c) ? c+('P'-'A'-'0') : (c|32)-'a';
            final int newCol=iColor % COLORS.length;
            changed|=newCol!=dp._iColor;
            if (!testOnly) dp._iColor=newCol;
        }
        return changed;
    }
    public Matrix3D getRotationAndTranslationOfView() {return _m3D;}
    /* <<< Protein information <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id=="mouseMsg") {
            if (mouseMsgRect.width>0) repaint(mouseMsgRect);
            mouseMsgPrepare();
            if (mouseMsgRect.width>0) repaint(mouseMsgRect);
        }
        if (id==ChRunnable.RUN_GET_COLUMN_TITLE) return Protein3dUtils.titleFor3dViewers(getProteins());
        if (id==KEY_BIO_MOLECULES) {
            final PView dp=getView(get(0,arg,Protein.class));
            if (dp!=null) {
                final int i=atoi(get(1,arg));
                dp._molecules=i==-1 ? 0 : i;
                _comboBio.s(i==-1 ? 0 : 2);
                repaint();
            }
            _notYetScaled=true;
        }
        if (id=="ADD_PROTEIN") addProteins((Protein[])arg);
        if (id==SPIN) {
            _autoRotating=true;
            final Matrix3D mSpin=new Matrix3D();
            while(_autoRotating) {
                if (getWidth()<ICON_HEIGHT) sleep(100);
                else {
                    _m3D.mult(mSpin.unit().rotate('y',.02).rotate('x',.02*Math.sin(System.currentTimeMillis()/(1000F*10))));
                    repaint();
                    sleep(maxi(99,5*_meanDuration));
                    MOUSE.molecule=null;
                    MOUS_HET.molecule=null;
                }
            }
        }
        if (id==RUN_LAYOUT) {
            removeAll();
            final int w=getWidth(), h=getHeight();
            final boolean showTools=_cbGui.s() && w>16*EX && sze(_vDisplayed)>0;
            final float lastScale=_scale;
            MOUSE.molecule=null;
            MOUS_HET.molecule=null;
            if (w>16*EX) {
                final int ph=prefH(_cbGui);
                _cbGui.setBounds(0,h-ph, prefW(_cbGui) , ph);
                add(_cbGui);
            }
            if (showTools) {
                final PView dd[]=_vDisplayed.asArray();
                final ChJScrollPane sp=scrllpn(PAN_SE);
                add(sp);
                PAN_SE.removeAll();
                int maxWidth=1;
                for(PView dp: dd)  maxWidth=maxi(maxWidth, prefW(dp._toolPnl));
                final int gridH=mini(dd.length, maxi(1,(w-_cbGui.getWidth())/maxWidth)), gridV=(dd.length-1)/gridH+1;
                PAN_SE.setLayout(new GridLayout(gridV,gridH));
                for(int i=3;--i>=0;) {
                    final JComponent c=i==0?PAN_SE : i==1?sp: sp.getViewport();
                    noBrdr(c);
                    c.setOpaque(false);
                }

                for(int i=mini(_transp.length, gridH*gridV-dd.length); --i>=0;) {
                    if (_transp[i]==null) noBrdr(_transp[i]=pnl(OPAQUE_FALSE));
                    PAN_SE.add(_transp[i]);
                }
                for(PView dp: dd) PAN_SE.add(dp._toolPnl);
                repaintC(PAN_SE);
                {
                    int
                        vsb=JScrollPane.VERTICAL_SCROLLBAR_NEVER,
                        hsb=JScrollPane.HORIZONTAL_SCROLLBAR_NEVER,
                        ht=prefH(PAN_SE),
                        wt=prefW(PAN_SE);
                    if (wt>w-32) { wt=w-32; ht+=EX; hsb=JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS; }
                    if (ht>h/2) { ht=h/2; wt+=EX; vsb=JScrollPane.VERTICAL_SCROLLBAR_ALWAYS; }
                    sp.setVerticalScrollBarPolicy(vsb);
                    sp.setHorizontalScrollBarPolicy(hsb);
                    sp.setBounds(w-wt-3, h-ht-3, wt, ht);
                    if (w>0) _scale=(w-(float)ht)/w;
                }
            } else _scale=1;
            if (!Float.isNaN(_scale) && _scale!=lastScale) _m3D.scale(_scale/lastScale);
            enableDisable();
            final JComponent ccNE[]=vNE.asArray();
            int x=w;
            for(int i=ccNE.length; --i>=0;) {
                final JComponent c=ccNE[i];
                final int ph=prefH(c),pw=prefW(c);
                if ((x-=pw-1)>0 || i==ccNE.length-1) {
                    c.setBounds(x,0,pw,ph);
                    add(c);
                    if (i==ccNE.length-1 && c instanceof ChButton)  ((ChButton)c).setContentAreaFilled(w<4*EX);
                } else remove(ccNE[i]);
            }
            int y=x<16*EM ? ICON_HEIGHT:0;
            for(JComponent c : vNW.asArray()) {
                if (w>16*EM) {
                    final int ph=prefH(c);
                    c.setBounds(0,y,prefW(c),ph);
                    y+=ph+1;
                    add(c);
                } else remove(c);
            }
            repaint();
        }
        return null;
    }
    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>>  Single Shapes >>> */
    private static class Line implements Comparable  {
        Object molecule;
        float x0,y0,z0, x1,y1;
        Color color[];
        short idx;
        Matrix3D m3d;
        byte style;
        public int compareTo(Object o) {
            if (o==null) return -1;
            final float z=((Line)o).z0;
            return z<z0 ? -1 : z>z0 ? 1 : 0;
        }
    }
    private void boundingBox(final Line lines[],final int nLines) {
        double mxT,myT;
        float xminT,xmaxT, yminT,ymaxT, zminT,zmaxT;
        xminT=yminT=zminT=MAX_INT;
        xmaxT=ymaxT=zmaxT=MIN_INT;
        mxT=myT=_mzT=_dxMean=_dyMean=_dzMean=0;
        int styleLine=0;
        for(int i=0;i<nLines;i++) {
            final Line l=lines[i];
            if (null==l) assrt();
            else if (l.style==LINE) styleLine++;
        }
        int iLines=0;
        for(int i=0;i<nLines;i++) {
            final Line l=lines[i];
            if (l==null || (styleLine>2 && l.style!=LINE)) continue;
            if (l.x0<xminT) xminT=l.x0;
            if (l.x0>xmaxT) xmaxT=l.x0;
            if (l.y0<yminT) yminT=l.y0;
            if (l.y0>ymaxT) ymaxT=l.y0;
            if (l.z0<zminT) zminT=l.z0;
            if (l.z0>zmaxT) zmaxT=l.z0;
            mxT+=l.x0;
            myT+=l.y0;
            _mzT+=l.z0;
            iLines++;
        }
        if (iLines==0) return;
        mxT/=iLines; myT/=iLines; _mzT/=iLines;
        // --- Mean derivation ---
        for(int i=0;i<nLines;i++) {
            final Line l=lines[i];
            if (l==null || (styleLine>2 && l.style!=LINE)) continue;
            final float
                dx=xminT-l.x0,
                dy=yminT-l.y0,
                dz=zminT-l.z0;
            _dxMean+= (dx>0 ?  dx : -dx);
            _dyMean+= (dy>0 ?  dy : -dy);
            _dzMean+= (dz>0 ?  dz : -dz);
        }
        _dxMean/=iLines;
        _dyMean/=iLines;
        _dzMean/=iLines;
        if (_dzMean==0) _dzMean=.1f;
        _ox=(int)(getWidth()/2F-mxT);
        final int visH=getHeight()- (_cbGui.s() ? scrllpn(PAN_SE).getHeight() : 0);
        _oy=(int)(visH/2F-myT);
    }
    private void drawProteins(Graphics g) {
        final PView[] dpp=_vDisplayed.asArray();
        Line[] lines=deref(_lines,Line[].class);
        if (lines==null) _lines=newSoftRef(lines=new Line[100*1000]);
        int iLine;
        while(true) {
            iLine=0;
            for(PView dp:dpp) {
                if (dp._cbHetero.s()) {
                    for(HeteroCompound h:dp.getProtein().getHeteroCompounds('H'))  iLine=makeHeteros(lines,h,dp,iLine,true);
                }
                if (dp._showNucl>0) {
                    for(HeteroCompound dna:dp.getProtein().getHeteroCompounds('N')) {
                        iLine=makeHeteros(lines, dna, dp,iLine, dp._showNucl>1);
                    }
                }
                iLine=makeHeteros(lines, null, dp,iLine, false);
            }
            if (iLine+99>lines.length) _lines=newSoftRef(lines=new Line[iLine+9999]); else break;
        }
        _countLines=iLine;
        if (g!=null) {
            boundingBox(lines,iLine);
            Arrays.sort(lines,0,iLine);
            drawLines(lines, iLine,g);
        }
    }
    private int makeHeteros(Line[] lines, HeteroCompound h, PView dp,int iLine0, boolean allAtoms) {
        final int iColor=dp!=null ? dp._iColor : 0;
        if (iColor==INVISIBLE) return iLine0;
        final Protein p=dp!=null ? dp.getProtein() : null;
        int iL=iLine0;
        final int bioUnit=_comboBio.i();
        if (null==dp) assrt();
        final Matrix3D rotTrans=mx3d(dp), mm3d[]= bioUnit==0 ? null : p.getBioMatrices();
        final int nMat=sze(mm3d);
        final float[] xyz=h!=null ? h.getCoordinates() : p.getResidueCalpha(null);
        if (xyz==null) return iL;
        final float[] xyzT= _xyzT=redim(_xyzT,xyz.length,99);
        // final int distDna2=_hideDistantDNS && _sliderDistDna!=null ? _sliderDistDna.getValue()*_sliderDistDna.getValue() : -1;
        if (h!=null) {
            final boolean
                underMouse=h==MOUS_HET.molecule,
                nuclBackbone=!allAtoms && h.isNucleotideChain(),
                colorHeteros= dp==null|| (dp._colorHeteros&1)!=0;
            final byte[] base=h.getNucleotideBase(), aType=h.getAtomType();
            final char chain= h.getCompoundChain();
            final int name=h.getCompoundName32(), number=h.getCompoundNumber(), nA=h.countAtoms();
            for(int iM=maxi(1,nMat); --iM>=0;) {
                final Matrix3D m3d=nMat>iM && (nMat>1 || !mm3d[0].isUnit()) ? mm3d[iM]:null; // see pdb1r9o
                final String m3dChains=m3d!=null ? m3d.getChains() : null;
                if (m3dChains!=null) {
                    if (m3dChains.indexOf(chain)<0 && chain!=' ') continue;
                    final int m=m3d.getBiomolecule();
                    if (m>=0 && dp._molecules!=-1 &&  (dp._molecules&(1L<<m))==0) continue;
                }
                _tmat.unit().mult(m3d).mult(rotTrans).mult(_m3D).transformPoints(xyz, xyzT, xyz.length/3);
                int lastNum=0, lastNam=0;
                float lastX=Float.NaN ,lastY=0;
                //float[] dnaXYZ=null;
                for(int iA=0,i3=0; i3<xyz.length && iA<nA;  iA++,i3+=3) {
                    if (nuclBackbone  &&  aType[iA]!='P') continue;

                    final Line l=iL>=lines.length ? null : lines[iL]==null ? lines[iL]=new Line() : lines[iL];
                    iL++;
                    if (l==null) continue;
                    l.idx=(short)iA;
                    l.molecule=h;
                    final float x0=l.x0=xyzT[i3+0], y0=l.y0=xyzT[i3+1], z0=l.z0=xyzT[i3+2];
                    l.color=underMouse? YELLOWs : colorHeteros ?  COL_ATOMS[aType[iA]] : (iColor>=0 ? COLORS[iColor%COLORS.length]:WHITEs);
                    l.style=DISC;
                    {
                        final int n=name;
                        if (n==HETERO_NA||n==HETERO_MG||n==HETERO_MN||n==HETERO_ZN||n==HETERO_SE||n==HETERO_CO||n==HETERO_NI||n==HETERO_CA) l.style=PLUS;
                        else if (n==HETERO_FE) l.style=CROSS;
                    }
                    if (nuclBackbone) {
                        if (name!=lastNam || number!=lastNum) { lastNam=name; lastNum=number; }
                        if (!Float.isNaN(lastX)) {
                            final Line c=iL>=lines.length ? null : lines[iL]==null ? lines[iL]=new Line() : lines[iL];
                            iL++;
                            if (c!=null) {
                                c.molecule=null;
                                c.m3d=m3d;
                                c.x0=x0; c.x1=lastX;
                                c.y0=y0; c.y1=lastY;
                                c.z0=z0;
                                c.color=l.color;
                                c.style=LINE;
                            }
                        }
                        lastX=x0; lastY=y0;
                        if (underMouse) {
                            final float dx=MOUS_HET.x0-x0, dy=MOUS_HET.y0-y0;
                            if (dx*dx+dy*dy<64*64) {
                                final Line c=iL>=lines.length ? null : lines[iL]==null ? lines[iL]=new Line() : lines[iL];
                                iL++;
                                if (c!=null) {
                                    c.molecule=null;
                                    c.style=LETTER;
                                    c.x0=x0;
                                    c.y0=y0;
                                    c.z0=z0;
                                    c.x1= base!=null ? base[iA] : 0;
                                    c.color=l.color;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (h==null) {
            final byte[] ss=p.getResidueSecStrType(), cc=p.getResidueChain();
            final int rn[]=p.getResidueNumber(), nR=p.countResidues(), nR3=mini(xyz.length,nR*3);
            final boolean _cbCS=isSelctd(dp._cbCS);
            boolean hasActiveSite=false;
            for(char chain=' '; chain<='z'; chain++) {
                if (!is(LETTR_DIGT_US,chain) && chain!=' ') continue;
                final int first=p.getChainFirstResidueIdx(chain), last=mini(nR-1,p.getChainLastResidueIdx(chain));
                if (first<0 || first>=last) continue;
                for(int iM=maxi(1,nMat); --iM>=0;) {
                    final Matrix3D m3d=nMat>iM && (nMat>1 || !mm3d[0].isUnit()) ? mm3d[iM]:null;
                    final String m3dChains=m3d!=null ? m3d.getChains() : null;
                    if (m3dChains!=null) {
                        if (m3dChains.indexOf(chain)<0) continue;
                        final int m=m3d.getBiomolecule();
                        if (m>=0 && dp._molecules!=-1 &&  (dp._molecules&(1L<<m))==0) continue;
                    }

                    _tmat.unit().mult(m3d).mult(rotTrans).mult(_m3D).transformPoints(xyz,xyzT,nR3);
                    for(int iA=first,i3=first*3; i3<nR3-3 && iA<=last;  iA++,i3+=3) {
                        if (Float.isNaN(xyzT[i3]) || Float.isNaN(xyzT[i3+4])) continue;
                        if (sze(cc)>iA+1 && cc[iA]!=cc[iA+1]) continue;
                        if (sze(rn)>iA+1 && rn[iA+1]-rn[iA]!=1) {
                            final float dx=xyz[i3]-xyz[3+i3], dy=xyz[1+i3]-xyz[1+3+i3], dz=xyz[2+i3]-xyz[2+3+i3];
                            if (dx*dx + dy*dy + dz*dz>27)  continue;
                        }
                        final Line l=iL>=lines.length ? null : lines[iL]==null ? lines[iL]=new Line() : lines[iL];
                        iL++;
                        if (l!=null) {
                            l.idx=(short)iA;
                            l.m3d=m3d;
                            l.molecule=dp;
                            l.x0=xyzT[i3+0]; l.y0=xyzT[i3+1];  l.z0=xyzT[i3+2];
                            l.x1=xyzT[i3+3]; l.y1=xyzT[i3+4];
                            l.style=LINE;
                            if (iColor==INVISIBLE) l.color=null;
                            else {
                                final boolean bioCol=bioUnit==2 && isSelctd(_togBioColor);
                                if ( (!bioCol)  && iColor==SEC_STRU && iA<sze(ss)) l.color=COL_SECSTRU[ss[iA]];
                                else if (!bioCol && iColor==CHAINS && iA<sze(cc)) l.color=COLORS[ (cc[iA]+ (26-'A'%26)) % COLORS.length];
                                else if (bioCol) l.color=COLORS[((iColor>=0?iColor:0)+iM)%COLORS.length];
                                else l.color=iColor>=0  ? COLORS[iColor%COLORS.length] : WHITEs;
                            }
                        }
                    }
                    for(ResidueSelection s:p.allResidueSelections()){
                        if (s==null) continue;
                        if (gcp(FEATURE_ACTIVE_SITE,s)!=null) {
                            hasActiveSite=true;
                            if (!_cbCS) continue;
                        }
                        final Color colors[]=fainted(s instanceof Colored ? rgb(((Colored)s).getColor()) : 0xFFffFF);
                        if (s instanceof VisibleIn123 && (((VisibleIn123)s).getVisibleWhere()&VisibleIn123.STRUCTURE)==0) continue;
                        final boolean bb[]=s.getSelectedAminoacids();
                        if (bb==null) continue;
                        final int offset=s.getSelectedAminoacidsOffset()-Protein.firstResIdx(p), to=mini(last+1, bb.length+offset, xyzT.length/3);
                        for(int iA=maxi(0,offset,first); iA<to; iA++) {
                            if (!bb[iA-offset]) continue;
                            final Line l=iL>=lines.length ? null : lines[iL]==null ? lines[iL]=new Line() : lines[iL];
                            if (l!=null) {
                                l.molecule=null;
                                iL++;
                                l.style=LETTER;
                                final int iA3=3*iA;
                                l.x0=xyzT[iA3];
                                l.y0=xyzT[iA3+1];
                                l.z0=xyzT[iA3+2];
                                l.x1=p.getResidueType(iA);
                                l.color=colors;
                            }
                        }
                    }
                }
            }
            if (hasActiveSite!=dp._cbCS.isEnabled()) dp._cbCS.setEnabled(hasActiveSite);
        }
        return iL;
    }
    private void drawLines(final Line lines[],final int n,final Graphics g) {
        g.setFont(getFnt(12,true,Font.BOLD));
        Color lastColor=null;
        final int ox=_ox, oy=_oy;
        final String strgLen1[]=strgsOfLen1();
        final Rectangle clip=clipBnds(g);
        final int clipX1=x(clip),clipY1=y(clip), clipX2=x2(clip), clipY2=y2(clip);
        for(int i=0;i<n;i++) {
            final Line l=lines[i];
            final Color[] cc=l!=null?l.color : null;
            if (sze(cc)==0) continue;
            final int z=(int) ((l.z0-_mzT)/_dzMean*DEPTH + DEPTH/4);
            final Color color= cc[maxi(0,mini(cc.length-1,z))];
            if (color==null) continue;
            if (lastColor!=color) g.setColor(lastColor=color);
            if (( _dragged||_autoRotating) && _meanDuration>33 &&  z>(_meanDuration>55?DEPTH/3 : DEPTH/2)) continue;
            final int x=ox+(int)l.x0, y=oy+(int)l.y0, x1=ox+(int)l.x1, y1=oy+(int)l.y1,  style=l.style, ch=style==l.style?0:EM;
            if (
                (x<clipX1-ch && (style!=LINE || x1<clipX1)) ||
                (x>clipX2+ch && (style!=LINE || x1>clipX2)) ||
                (y<clipY1-ch && (style!=LINE || y1<clipY1)) ||
                (y>clipY2+ch && (style!=LINE || y1>clipY2))
                ) {
                continue;
            }

           switch(l.style) {
            case LINE:  g.drawLine(x,y, x1,y1);  break;
            case PLUS:  g.drawLine(x-3,y,x+3,y); g.drawLine(x,y-3,x,y+3); break;
            case CROSS: g.drawLine(x-3,y-3,x+3,y+3); g.drawLine(x+3,y-3,x-3,y+3); break;
            case LETTER:g.drawString(strgLen1[(byte)( ((int)l.x1) & 127)],x,y); break;
            default:
                g.fillOval(x-2,y-2,4,4);
            }
        }
    }
    /* <<< Shapes <<< */
    /* ---------------------------------------- */
    /* >>> HeteroCompound >>> */
    public boolean isNucleotideChainVisible(HeteroCompound ha) {
        for(PView dp: _vDisplayed.asArray())
            if (dp._showNucl>0 && cntains(ha,dp.getProtein().getHeteroCompounds('N'))) return true;
        return false;
    }
    public void showListOfHeterosAndNucleotides(Protein p) {}
    /* <<< HeteroCompound <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private final Rectangle mouseMsgRect=new Rectangle();
    private final String[] mouseMsg=new String[3];
    private final int[] mouseMsgX=new int[3], mouseMsgW=new int[3];
    private final Runnable mouseMsgRun=thrdCR(this,"mouseMsg");
    private void mouseMsgPrepare() {
        final int iA=MOUSE.idx;
        final int W=getWidth();
        final PView dp=(PView)MOUSE.molecule;
        final Graphics g=_lastG;
        if (g==null || dp==null) {
            mouseMsg[0]=mouseMsg[1]=mouseMsg[2]=null;
            mouseMsgRect.setBounds(0,0,0,0);
            return;
        }
        int wMax=0;
        final int x0=_ox+(int)MOUSE.x0, y0=_oy+(int)MOUSE.y0;
        int bx1=mini(x0-3, _ox+ (int)MOUSE.x1-3);
        int bx2=maxi(x0+3, _ox+ (int)MOUSE.x1+3);
        final int by1=mini(y0-3, _oy+ (int)MOUSE.y1-3, y0-3*charH(g));
        final int by2=maxi(y0+3, _oy+ (int)MOUSE.y1+3);
        final Protein p=dp.getProtein();
        Protein.mouseOver(p, iA+Protein.firstResIdx(p));

        for(int iLine=3; --iLine>=0;) {
            final BA sb=baTip();
            switch(iLine) {
            case 0:
                if (sze(_vDisplayed)<2) continue;
                sb.a(p);
                break;
            case 1:
                if (iA>=0) {
                    sb.a(' ').a(iA).a(' ').aSomeBytes(p.getResidueName32(iA),4);
                    sb.a(' ').a(p.getResidueNumber(iA)).a(':').a((char)p.getResidueChain(iA));
                }
                break;
            case 2:
                if (_comboBio.i()>0 && MOUSE.m3d!=null) sb.a("Bio unit ").a(MOUSE.m3d.getBiomolecule()).a(" BIOMT:").a(MOUSE.m3d.getName());
                break;
            }
            final int textWidth=strgWidth(g, mouseMsg[iLine]=sb.toString());
            wMax=maxi(wMax,textWidth);
            final int x=mini(x0,W-textWidth);
            bx1=mini(bx1,x);
            bx2=maxi(bx2,x+textWidth);
            mouseMsgX[iLine]=x;
            mouseMsgW[iLine]=textWidth;
        }
        mouseMsgRect.setBounds(bx1,by1, bx2-bx1, by2-by1);
    }

    private Graphics _lastG;
    @Override public void paintComponent(Graphics g){
        final int W=getWidth(), H=getHeight(), ox=_ox, oy=_oy;
        if (_mouseIn && !_dragged && _size!=W+H) { _mouseIn=false;  inEDTms(_runLayout,111); _size=W+H;}
        final long time=System.currentTimeMillis();
        final Rectangle clip=clipBnds(g, W, H);

        g.setColor(isWhiteBG() ? C(0xFFffFF) : getBackground());
        g.fillRect(x(clip), y(clip), clip.width, clip.height);
        g.setColor(getForeground());
        drawProteins(g);
        final Color white=C(0xFFffFF);
        if (gcp(KEY_IS_PRINTING,this)==null) {
            final PView dp=(PView)MOUSE.molecule;
            if (dp!=null) {
                final int charA=charA(g), charH=charH(g);
                g.setColor(C(0xFFAFAF));
                final Protein p=dp.getProtein();
                final int x0=ox+(int)MOUSE.x0, y0=oy+(int)MOUSE.y0;
                g.fillOval(x0-3,y0-3,6,6);
                g.drawOval(ox+ (int)MOUSE.x1-3, oy+(int)MOUSE.y1-3,6,6);
                if (W>ICON_HEIGHT && p!=null && !_isWheel && !_dragged && System.currentTimeMillis()-_whenKeyEvent>999) {
                    for(int iLine=3; --iLine>=0;) {
                        final String msg=mouseMsg[iLine];
                        if (msg==null) continue;
                        g.setColor(C(0x0,99));
                        final int y=y0+ (iLine-3)*charH;
                        g.fillRect(mouseMsgX[iLine],y,mouseMsgW[iLine],charH);
                        g.setColor(white);
                        g.drawString(msg,mouseMsgX[iLine],y+charA);
                    }
                }
            }
        }
        _durationPaint=(int)(System.currentTimeMillis()-time);
        _meanDuration= (_meanDuration*9+_durationPaint)/10;
        highlightAminoAcid(_xyzCursor);
        //if (hasFocus()) { g.setColor(RED); g.drawLine(0,0,9999,0);}
        if (_notYetScaled) {
            _notYetScaled=false;
            final float d=(float)(_dxMean+_dyMean+_dzMean);
            if (d!=0) _m3D.mult(_tmat.unit().scale(Math.min(1,(H+W)/d/3)));
            repaint();
        }

        if (W<=ICON_HEIGHT) {
            final Protein[] pp=getProteins();
            final int tYY[]=_thumbY=chSze(_thumbY, 2*pp.length);
            int y=H;
            nextProt:
            for(int i=0; i<pp.length; i++) {
                final Image im=pp[i].getIconImage();
                if (im==null) continue nextProt;
                for(int j=0; j<i; j++) if (im==pp[j].getIconImage()) continue nextProt;
                tYY[2*i]=y-=W+1;
                tYY[2*i+1]=y+W;
                g.drawImage(im,0,y,W,W,this);
            }
            pcp(ChTableLayout.KEY_HEIGHT_SOUTH_PANEL, intObjct(y),this);
        } else pcp(ChTableLayout.KEY_HEIGHT_SOUTH_PANEL,null,this);
                _lastG=g;

        paintHooks(this, g, true);
        final String msg=gcps(KEY_MSG_NO_PROTEIN,this);
        if (msg!=null) {
            g.setColor(C(0xAAaaFF));
            g.drawString(msg,EX,EM);
        }

    }
    public void paintChildren(Graphics g){
        try {
            if (_mouseIn && !_dragged && gcp(KEY_IS_PRINTING,this)==null) super.paintChildren(g);
        } catch(Exception e){}
    }
    public boolean paintHook(JComponent component,Graphics g, boolean after) {
        final PView dp=(PView)gcp(PView.class,component);
        final Color black=C(0), white=C(0xFFffFF);
        if (dp!=null && component==dp._colorChooser) {
            if (!after) return false;
            final boolean isDoc=gcp(HtmlDoc.KEY_DO_NOT_CLONE,component)!=null;
            final int ny=3, u=ICON_HEIGHT/ny, iColor=dp._iColor;
            g.setColor(iColor<0 ? C(0) :  COLORS[iColor%COLORS.length][0]);
            g.fillRect(0,0,999,999);
            final Protein p=dp.getProtein();
            for(int i=COLORS.length+3; --i>=0; ) {
                final int ix=i/ny, iy=i%ny, x=ix*u+4, y=iy*u+1;
                switch(i) {
                case 0:
                    if (p.getResidueSecStrType()!=null || isDoc){
                        g.setColor(C(0xFF0000));
                        g.fill3DRect(x,y,u-1,u-1,iColor!=SEC_STRU);
                        g.setColor(C(0xFFff00));
                        g.fillRect(x,y+u/2,u-2,u/2-1);
                        break;
                    }
                case 1:
                    if (sze(p.getChainsAsString())>1 || isDoc) {
                        g.setColor(black);
                        g.fill3DRect(x,y,u-1,u-1,iColor!=CHAINS);
                        for(int j=y+2;j<y+u-1;j++) { g.setColor(COLORS[j%COLORS.length][0]); g.drawLine(x+2,j,x+u-2,j);}
                    }
                    break;
                case 2:
                    g.setColor(C(0));
                    g.fill3DRect(x,y,u-2,u-2,iColor!=INVISIBLE);
                    g.setColor(white);
                    g.drawRect(x,y,u-2,u-2);
                    break;
                default:
                    g.setColor(COLORS[i-3][0]);
                    g.fill3DRect(x,y,u-1,u-1,i!=iColor+3);
                }
            }
        }
        return true;
    }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Tooltip >>> */
    private String tipLast;
    @Override public String getToolTipText(MouseEvent ev) {

        final PView pv=ev2thumb(ev);
        final ChRunnable runTT=deref(pv!=null ? pv.getProtein() : null, ChRunnable.class);
        if (runTT!=null) return addHtmlTagsAsStrg(runTT.run(ChRunnable.RUN_GET_TIP_TEXT,ev));
        final HeteroCompound ha=(HeteroCompound)MOUS_HET.molecule;
        if (ha!=null) {
            if (tipLast==null) {
                final String rt=toStrg(ha.getRenderer(0L,null));
                final Matrix3D m3d=MOUS_HET.m3d;
                final BA sb=baTip().a("<html><body><pre>").a(rt).a(" Matrix:" ).a(m3d!=null ? m3d.getName() : null);
                int isFirst=0;
                for(Protein p : getProteins()) {
                    for(HeteroCompound h:p.getHeteroCompounds('H')) {
                        if (ha!=h) continue;
                        if (isFirst++==0) sb.a('\n');
                        sb.a(p);
                        break;
                    }
                }

                tipLast=sb.a("</pre></html></body>").toString();
            }
            return tipLast;
        }
        final PView dp=(PView)MOUSE.molecule;
        if (dp!=null && dp.getProtein()!=null) {
            final boolean cbCS=isSelctd(dp._cbCS);
            for(ResidueSelection sel : dp.getProtein().residueSelectionsAt(0L, MOUSE.idx, MOUSE.idx+1,VisibleIn123.STRUCTURE)) {
                if (gcp(FEATURE_ACTIVE_SITE,sel)!=null && !cbCS) continue;
                if (sel==Protein.getMouseOverSelection()) continue;
                final String tip=dTip(sel);
                return  addHtmlTagsAsStrg(  (tip!=null ? tip : toStrg(sel)));
            }
        }
        return null;
    }
    public String provideTip(Object objOrEv) {
        final AWTEvent ev=deref(objOrEv, AWTEvent.class);
        final Object component=evtSrc(objOrEv);
        final PView dp=(PView)gcp(PView.class,component);
        if (dp!=null && component==dp._colorChooser) {
            final int
                ny=3,
                u=ICON_HEIGHT/ny,
                mx=x(ev),
                my=y(ev);
            for(int i=0;i<COLORS.length;i++) {
                final int ix=i/ny,iy=i%ny,x=ix*u,y=iy*u;
                if (x<=mx && mx<=x+u  &&  y<=my && my<=y+u) {
                    return
                        i==0 ? "<html><body>Helices <font color=\"#FF0000\"> red</font> and sheets <font color=\"#AAAA00\"> yellow</font></body></html>" :
                        i==1 ? "Each chain in a different color" :
                        i==2 ? "Hide protein" : "The entire protein in the respective color";
                }
            }
        }
        return "hallo";
    }
    /* <<<  Tooltip <<< */
    /* ---------------------------------------- */
    /* >>>  GUI >>> */
    public void enableDisable() {
        boolean isBioUnits=false;
        for(PView d:_vDisplayed.asArray()) {
            final Protein p=d.getProtein();
            d._cbHetero.setEnabled(p.getHeteroCompounds('H').length>0);
            d._butNucl.enabled(p.getHeteroCompounds('N').length>0);
            if (sze(p.getBioMatrices())>1) isBioUnits=true;
        }
        eachChainOneColor(true,true);
        boolean update=false;
        if (isBioUnits!=_comboBio.isEnabled()) {
            _comboBio.setEnabled(isBioUnits);
            update=true;
        }
        if (update) inEDTms(_runLayout,333);
    }
    public void addOrRemoveButton(int pos,JComponent b, int where ) {
        final UniqueList v= where==SwingConstants.WEST ? vNW : vNE;
        if (b!=null) {
            b.removeMouseListener(LI);
            if (pos>=0) {
                v.add(mini(sze(v),pos),b);
                LI.addTo("a",b);
            } else v.remove(b);
        }
        inEDTms(_runLayout,222);
    }

    public void setHideDistantDNS(boolean b) {
        _hideDistantDNS=b;
        if (_sliderDistDna==null) addActLi(LI,_sliderDistDna=new ChSlider("DIST_DNA",0,100,30).endLabels("Close", "Distant"));
        final ChFrame f=ChFrame.frame("Distant DNA",_sliderDistDna, ChFrame.CENTER).size(100,64);
        if (b) f.shw(ChFrame.ALWAYS_ON_TOP);
        else f.setVisible(false);
    }
    /* <<< GUI <<< */
    /* ---------------------------------------- */
    /* >>>  Event >>> */
    private PView ev2thumb(AWTEvent ev) {
        if (getWidth()<=ICON_HEIGHT) {
            final int tYY[]=_thumbY, y=y(ev);
            for(int i=sze(tYY)/2; --i>=0;) {
                if (tYY[2*i]<y && y<tYY[2*i+1]) {
                    return _vDisplayed.get(i);
                }
            }
        }
        return null;
    }
    public void processEv(AWTEvent ev) {
        final boolean mouseInLast=_mouseIn;
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final int
            id=ev.getID(),
            modi=modifrs(ev),
            iButton=buttn(ev),
            x=x(ev),
            y=y(ev),
            H=maxi(1,getHeight()),
            W=maxi(1,getWidth());
        final boolean
            shift=0!=(SHIFT_MASK&modi),
            ctrl=0!=(CTRL_MASK&modi),
            showTableBio=(cmd!=null || id==MOUSE_PRESSED) && ((q==_cbDetailBio || q==_comboBio || cmd=="RST_BIO") && _comboBio.i()==2),
            rstDataBio=showTableBio && _dataBio[0]==null || cmd=="RST_BIO";
        final PView dd[]=_vDisplayed.asArray();

        if (q==_togBioColor||q instanceof ChTableModel || cmd=="RST_BIO" || q==_cbDetailBio) _comboBio.s(2);

        final Protein[] pp=getProteins();
        if (q==this) {
            final PView pv=ev2thumb(ev);
            final Protein p=pv!=null ? pv.getProtein() : null;
            if (p!=null) {
                //if (processEvt(ev, pv._pLabel)) return;
                if (id==MOUSE_DRAGGED && !_dragged) {
                    adUniq(p.getFile(), dndV(true,this));
                    exportDrg(this , ev, TransferHandler.COPY);
                }

            }
            if (gcp(KEY_IS_RESIZING,this)!=null) return;
            final int scaleUpDown=scaleUpDown(ev);
            if (scaleUpDown!=0) {
                _tmat.unit();
                _m3D.mult(_tmat.unit().scale(1f+(scaleUpDown<0  ? -20f : 20f)/H));
                repaint();
            }
        }
        if (q==_sliderDistDna) repaint();
        if (rstDataBio) {
            final UniqueList<Object[]> v=new UniqueList(Object[].class);
            final Map<String,long[]> mapMol=new HashMap();
            boolean moreMolecules=false, hasPdbId=false;
            for(Protein p : pp)  {
                for(Matrix3D m3d:p.getBioMatrices()) moreMolecules|=m3d.getBiomolecule()>1;
                hasPdbId|=p.getPdbID()!=null;
            }
            _cbDetailBio.setEnabled(moreMolecules);
            if (!moreMolecules) _cbDetailBio.s(true);
            for(int detailed=2; --detailed>=0;) {
                mapMol.clear();
                v.clear();
                for(Protein p : pp) {
                    final String pdb=p.getPdbID();
                    final Object propMol=gcp(KEY_BIO_MOLECULES,p);
                    final long iPropMol=atol(propMol);
                    long mols=0;
                    for(Matrix3D m3d: p.getBioMatrices()) mols|= 1L<<m3d.getBiomolecule();
                    if (mols==0) continue;
                    if (hasPdbId && detailed==0) {
                        if (pdb!=null) {
                            long[] m=mapMol.get(pdb);
                            if (m==null) mapMol.put(pdb,m=new long[2]);
                            m[0]|=mols;
                            if (propMol!=null && iPropMol>0) m[1]|=iPropMol;
                        }
                    } else {
                        v.add(new Object[]{p, baTip().boolToText(mols,0,"-").toString(), baTip().boolToText(propMol!=null ? iPropMol : mols, 0, "-").toString(),null});
                    }
                }
                if (hasPdbId && detailed==0) {
                    for(Object pdbId : keyArry(mapMol)) {
                        final long mols=mapMol.get(pdbId)[0], iPropMol=mapMol.get(pdbId)[1];
                        final String s=baTip().boolToText(mols, 0, "-").toString();
                        v.add(new Object[]{pdbId, s, iPropMol>0 ? baTip().boolToText(iPropMol,0,"-").toString() :  (mols&2)!=0 ? "1" : s});
                    }
                }
                _dataBio[detailed]=v.asArray();
            }
        }
        final int detailedBio01=_cbDetailBio.s()?1:0;
        if (showTableBio) {
            if (_tableBio==null) {
                final ChTableModel tm=new ChTableModel(ChJTable.SET_VALUE_SELECTED_ROWS, "Protein or PDB-ID","Available biomolecules","Displayed bio unit").editable(false,false,true,true);
                addActLi(LI,tm);
                _tableBio=new ChJTable(tm,ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT)
                    .headerTip(null, "Molecules recorded in the PDB file", "List of molecule numbers entered by the user");
                addLi4CntxtMenu(_tableBio);
                final Object msg="Biological units are reconstructed from the Asymmetric_unit* with transformation matrices. "+
                    "Matrices are recorded in PDB files like<pre class=\"data\">REMARK 350 BIOMOLECULE ...</pre>"+
                    "<u>Third table column:</u> Specify the molecules to be shown.<br>"+
                    "Leave table cell empty to see the Asymmetric_unit*.<br>"+
                    "Enter a non-existing biological unit number to hide.<br>"+
                    "To selectively deactivate a matrix, add a leading space into the  respective PDB file lines.<br>",
                    pTxt=pnl(msg),
                    pMsg=pnl(VBHB, pnl(HBL, toggl().doCollapse(pTxt).doPack(), "Info Bio-Units"),pTxt);

                pcp(KEY_NORTH_PANEL,pMsg,_tableBio);
                pcp(KEY_SOUTH_PANEL,pnl(_togBioColor, " "," ",_cbDetailBio.cb(), new ChButton("RST_BIO").t("Reset table").li(LI)),_tableBio);
            } else {
                _tableBio.setRowHeight(32+detailedBio01);
                repaintC(_tableBio);
            }
            final ChFrame f=ChFrame.frame("Customized", _tableBio, ChFrame.AT_CLICK).shw(ChFrame.TO_FRONT);
            f.setLocation( _comboBio.getLocationOnScreen().x-2*ICON_HEIGHT, _comboBio.getLocationOnScreen().y+3*ICON_HEIGHT);

            for(int i=3;--i>=0;) _tableBio.setColWidth(true, i, i==0?200:i==1?64:200);
            repaint();
        }
        if ( (showTableBio||rstDataBio) && _tableBio!=null) {
            ((ChTableModel) _tableBio.getModel()).setData(_dataBio[detailedBio01]);
        }
        if (q instanceof ChTableModel || q==_comboBio || showTableBio || rstDataBio || q==_cbDetailBio ) {
            if (_comboBio.i()==2) {
                final Object[][] data=_dataBio[detailedBio01];
                for(int iRow=0; iRow<data.length; iRow++) {
                    final Object row[]=data[iRow],  o0=row[0];
                    for(Protein p:pp) {
                        if (p==o0 || o0==p.getPdbID()) {
                            final PView dp=getView(p);
                            final String s=toStrgTrim(row[2]);
                            if (dp!=null && s!=null)  dp._molecules=sze(s)==0 ? -1 : boolToLong(parseSet(s,999,0));
                        }
                    }
                }
                repaintC(_tableBio);
            } else {
                for(PView dp : dd) dp._molecules=2;
            }
            repaint();
        }
        if (q==_togBioColor) repaint();
        final PView dp=(PView)gcp(PView.class,q);
        final MouseEvent mev= ev instanceof MouseEvent ? (MouseEvent)ev : null;
        if (mev!=null && dp!=null) {
            if (id==MOUSE_PRESSED && q==dp._colorChooser) {
                final int ny=3, u=ICON_HEIGHT/3;
                final int f=((x-4)/u)*ny+y/u;
                dp._iColor= f==0 ? SEC_STRU : f==1 ? CHAINS : f==2 ? INVISIBLE : f-3;
                enableDisable();
            }
            if (ctrl) for(PView d : dd) d._iColor=dp._iColor;
            repaint();
        }
        if (id==ActionEvent.ACTION_PERFORMED) {
            final boolean isSelected=isSelctd(q);
            if (dp!=null) {
                if (cmd=="N" && ++dp._showNucl>2) dp._showNucl=0;
                for(PView d : dd) {
                    if (ctrl || dp==d) {
                        if (cmd=="CLOSE") removeProtein(d.getProtein());
                        if (cmd=="N" && !shift) d._showNucl=dp._showNucl;
                        if (cmd=="AS") d._cbCS.s(isSelected);
                        if (q==dp._cbHetero) {
                            if (shift&&ctrl) { dp._colorHeteros++; dp._cbHetero.s(!isSelected); }
                            if (!shift) { d._cbHetero.s(dp._cbHetero.s()); d._colorHeteros=dp._colorHeteros;}
                        }
                    }
                }
                final ChButton cbH=dp._cbHetero;
                if (shift && !ctrl && (cmd=="N" || q==cbH)) { if (q==cbH) cbH.s(!isSelected); showListOfHeterosAndNucleotides(dp.getProtein());}
                if (q==cbH || cmd=="AS" || cmd=="N") repaint();
            }
        }
        if (q==this) {
            final int kcode=keyCode(ev);
            if (kcode!=0 && id==KEY_PRESSED && 0==(modi&(SHIFT_MASK|CTRL_MASK))) {
                _tmat.unit();
                if (ctrl && kcode==VK_W) { dispose(); return; }
                if(kcode== VK_UP || kcode==VK_DOWN) { _m3D.mult(ctrl ? _tmat.scale(kcode==VK_UP?1.1f:.9f) : _tmat.rotate('x',kcode==VK_UP?.1:-.1)); repaint(); }
                if (kcode==VK_PAGE_UP||kcode==VK_PAGE_DOWN) { _m3D.mult(_tmat.rotate('x',kcode==VK_PAGE_UP ? 3.1415 : -3.1415)); repaint(); }
                _whenKeyEvent=System.currentTimeMillis();
            }
            if (id==FocusEvent.FOCUS_LOST || id==FocusEvent.FOCUS_GAINED) repaint();
            if (mev!=null) {
                final PView mdp=(PView)MOUSE.molecule;
                final Protein p=mdp==null?null: mdp.getProtein();
                setTipTiming(id==MOUSE_MOVED && !shift && !ctrl ? 500 : -1,0);
                final int iA=MOUSE.idx;
                if (id==MOUSE_PRESSED && p!=null) {
                    _autoRotating=false;
                    requestFocus();
                    Protein3dUtils.dispatchEvtPicked(mdp, p.getResidueNumber(iA), (char)p.getResidueChain(iA), (char)p.getResidueInsertionCode(iA), modi);
                }
                final int wheel=wheelRotation(ev);
                if (wheel!=0 && !ctrl) {
                    _isWheel=true;
                    _m3D.mult(_tmat.unit().rotate('z',wheel*.1f));
                    MOUSE.molecule=null;
                    MOUS_HET.molecule=null;
                    repaint();
                }
                if (id==MOUSE_RELEASED || (id==MOUSE_MOVED&&!shift)) {
                    _isWheel=false;
                    if (_dragged) { _dragged=false; repaint(); }
                }
                if (id==MOUSE_MOVED && !shift) {
                    if (findMouse(x-_ox,y-_oy)) {
                        mouseMsg[0]=mouseMsg[1]=mouseMsg[2]=null;
                        inEDTms(mouseMsgRun,11);
                    }
                }
                if (id==MOUSE_MOVED && p!=null) {
                    Cursor cursor=null;
                    final boolean cbCS=isSelctd(mdp._cbCS);
                    for(ResidueSelection sel : p.residueSelectionsAt(0L,iA ,iA+1,VisibleIn123.STRUCTURE)) {
                        if (gcp(FEATURE_ACTIVE_SITE,sel)!=null && !cbCS) continue;
                        if (sel==Protein.getMouseOverSelection()) continue;
                        if (sel instanceof PView || (_CCS+"ResidueAnnotation").equals(sel.getClass().getName())) {
                            cursor=cursr('H');
                            continue;
                        }
                    }
                    setCursor(cursor!=null ? cursor : cursr('D'));
                }
                if (iButton!=2 && (id==MOUSE_DRAGGED || id==MOUSE_MOVED&&shift)) {
                    if (!_dragged) {
                        _prevx=x;
                        _prevy=y;
                        _dragged=true;
                        _autoRotating=false;
                        return;
                    }
                    if (!_autoRotating) {
                        _tmat.unit();
                        final float
                            dx=(_prevx-x)*2f/W,dy=(_prevy-y)*2f/H,
                            xtheta=4f*dy, ytheta=4f*dx, ztheta=-4f*dx;
                        if (!ctrl && !shift && iButton!=3 && iButton!=2 || id==MOUSE_MOVED)  {
                            _tmat.rotate('x',xtheta);
                            _tmat.rotate('y',ytheta);
                        }
                        else {
                            if (ctrl && !shift) _tmat.rotate('z',ztheta);
                            if (shift && !ctrl) _tmat.scale(1f-dy*2).rotate('z',ztheta);
                        }
                        _m3D.mult(_tmat);
                    }
                    _prevx=x;_prevy=y;
                    MOUSE.molecule=null;
                    MOUS_HET.molecule=null;
                    repaint();
                }
                if (id==MOUSE_ENTERED || id==MOUSE_EXITED) _xyzT=null;
            }
        }
        if (!_dragged) {
            if (id==MOUSE_MOVED || id==MOUSE_ENTERED) _mouseIn=true;
            if (id==MOUSE_EXITED && iButton!=1 && (q==this || parentC(q)==this)) {
                //final int x1=q==this ? x : c.getX()+x;
                //final int y1=q==this ? y : c.getY()+y;
                if (( x<2||y<2 || x>W-3 || y>H-3)) _mouseIn=false;
            }
            if (mouseInLast!=_mouseIn) repaint();
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Mouse >>> */
    private boolean findMouse(int mx,int my) {
        int distCA=MAX_INT, distHA=MAX_INT;
        short  dp_idx=0;
        Object ha=null, dp=null;
        final Line[] lines=deref(_lines, Line[].class);
        if (lines==null) return false;
        for(int i=_countLines; --i>=0;) {
            final Line l=lines[i];
            if (l==null) {
                /*if (myComputer()) putln(RED_WARNING+"Protein3d l==null"); */
                continue;
            }
            if (l.style==LETTER) continue;
            final float x=l.x0, y=l.y0;
            final Object mol=l.molecule;
            if (mol!=null) {
                final float dx=x-mx, dy=y-my;
                final int dd=(int)(dx*dx+dy*dy);
                if (dd<8*8 && mol instanceof HeteroCompound) {
                    if (distHA>dd) {
                        distHA=dd;
                        if (MOUS_HET.molecule!=mol) tipLast=null;
                        MOUS_HET.x0=x;
                        MOUS_HET.y0=y;
                        MOUS_HET.m3d=l.m3d;
                        ha=mol;
                    }
                }
                else if (dd<8*8 && mol instanceof PView && distCA>dd) {
                    distCA=dd;
                    MOUSE.x0=x;
                    MOUSE.y0=y;
                    MOUSE.x1=l.x1;
                    MOUSE.y1=l.y1;
                    MOUSE.m3d=l.m3d;
                    dp_idx=l.idx;
                    dp=mol;
                }
            }
        }
        final boolean changed= dp_idx!=MOUSE.idx || dp!=MOUSE.molecule || ha!=MOUS_HET.molecule;
        MOUS_HET.molecule=ha;
        MOUSE.molecule=dp;
        MOUSE.idx=dp_idx;
        return changed;
    }
    public Protein getProteinAtMouse() { final PView dp=(PView)MOUSE.molecule;  return dp!=null ? dp.getProtein() : null; }
    public int getResidueIndexAtMouse() {return MOUSE.molecule!=null ? MOUSE.idx : -1; }
    /* <<< Mouse  <<< */
    /* ---------------------------------------- */
    /* >>> Highlight Residue >>> */
    public void highlightAminoAcidZ(Protein p,int idx) {
        for(PView dp : _vDisplayed.asArray()) {
            if (p!=null && idx>=0 && p==dp.getProtein()) {
                final float[] xyz=p.getResidueCalpha(null);
                if (sze(xyz)>3*idx+2) {
                    _xyzCursor=new float[]{xyz[idx*3],xyz[idx*3+1],xyz[idx*3+2]};
                    final Matrix3D m3d=mx3d(dp);
                    if (m3d!=null) m3d.transformPoints(_xyzCursor,_xyzCursor,1);
                    highlightAminoAcid(_xyzCursor);
                } else _xyzCursor=null;
                break;
            }
        }
    }
    public void highlightAminoAcid(float xyz[]) {
        final Graphics g=getGraphics();
        if (g==null || xyz==null || !isShowing()) return;
        final float xyzT[]=new float[3];
        _m3D.transformPoints(xyz,xyzT,3);
        if (!Float.isNaN(xyzT[0]+xyzT[1])) {
            ChIcon.drawCursor(g, (int)(_ox+xyzT[0]-16/2), (int)(_oy+xyzT[1]-16/2),16,16,this);
        }
    }

    /* <<<  Highlight Residue <<< */
    /* ---------------------------------------- */
    /* >>> Color >>> */
    private final static Map<Object,Color[]> _mapColors=new WeakHashMap();
    private static Color[] fainted(int rgb0) {
        final int rgb=rgb0==0?0xffFFff:rgb0;
        final int n1=DEPTH+1;
        Color[] ret=_mapColors.get(intObjct(rgb));
        if (ret==null) {
            _mapColors.put(intObjct(rgb),ret=new Color[n1]);
            double fR,fG,fB;
            fR=255&(rgb>>16);
            fG=255&(rgb>>8);
            fB=255&rgb;
            for(int i=0;i<DEPTH+1;i++) {
                double k=n1-i; k=k/n1;
                ret[i]=new Color((int)(k*fR),(int)(k*fG),(int)(k*fB));
            }
        }
        return ret;
    }

    private final static int DEPTH=64;
    private final static Color[]
        REDs=fainted(0xFF0000),
        YELLOWs=fainted(0xAAaa00),
        WHITEs=fainted(0xFFffFF),
        COL_SECSTRU[],
        COL_ATOMS[],
        COLORS[]=new Color['Z'-'A'+1][];
    static {
        final int[] chainRGBS={0xC0D0FF, 0xB0FFB0, 0xFFC0C8, 0xFFFF80, 0xFFC0FF, 0xB0F0F0, 0xFFD070, 0xF08080, 0xF5DEB3,
                               0x00BFFF, 0xCD5C5C, 0x66CDAA, 0x9ACD32, 0xEE82EE, 0x00CED1, 0x00FF7F, 0x3CB371,
                               0x33448B/*brighter*/,0xBDB76B, 0x336444/*brighter*/, 0x803344/*brighter*/, 0x808000, 0x800080, 0x008080, 0xB8860B, 0xB22222};
        for(int i=chainRGBS.length; --i>=0;) COLORS[i]=fainted(chainRGBS[i]);
    }
    static {
        final int TRANSPARENCY=128;
        Color[][] cc=COL_SECSTRU=new Color[256][];
        Arrays.fill(cc,WHITEs);
        cc['h']=cc['H']=REDs;
        cc['e']=cc['E']=YELLOWs;
        cc=COL_ATOMS=new Color[256][];
        Arrays.fill(cc,fainted(0xFFffFF));
        cc['n']=cc['N']=fainted(0x0000FF);
        cc['o']=cc['O']=fainted(0xff0000);
        cc['s']=cc['S']=fainted(0xffFF00);
        cc['p']=cc['P']=fainted(0x00ff00);
    }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Class Single Peptide chain  >>> */
    public static class PView implements ProteinViewer {
        private Matrix3D _m3dPrev;
        private int _iColor, _colorHeteros=1, _showNucl=1;
        private ChButton  _colorChooser, _butNucl, _cbHetero, _cbCS;
        private Component _toolPnl;
        private Object _pLabel;
        private long _molecules=-1;
        private Protein _p;
        private Protein3d _p3d;
        @Override public String toString() { return getProtein()+"@"+super.toString();}
        public Color getColor() { return _iColor>=0 ? COLORS[_iColor%COLORS.length][0] : null; }
        /* --- Interface ProteinViewer --- */
        public void setSelectedResidues(boolean[] bb, int offset) {
            final Protein p=getProtein();
            if (p!=null) {
                final String KEY_SEL="P3D$$S";
                BasicResidueSelection s=(BasicResidueSelection) gcp(KEY_SEL,p);
                if (s==null) {
                    pcp(KEY_SEL, s=new BasicResidueSelection(0), p);
                    s.setName(ResidueSelection.NAME_BACKBONE);
                    s.setVisibleWhere(VisibleIn123.STRUCTURE);
                    s.setProtein(p);
                    s.setColor(C(0xFFff00));
                }
                BasicResidueSelection.changeSelectedAA(bb,offset,s);
                p.addResidueSelection(s);
                new StrapEvent(this,StrapEvent.RESIDUE_SELECTION_ADDED).run();
            }
        }
        public Protein getProtein() { return _p;}
        public boolean setProtein(long options, Protein p, ProteinViewer inSameView) {
            if (p==null) return false;
            final PView pvSame= inSameView instanceof PView ? (PView) inSameView : null;
            Protein3d protein3D= pvSame!=null ? pvSame._p3d : null;
            if (protein3D==null) _p3d=protein3D=new Protein3d();
            protein3D.initDP(this, p,true);
            return true;
        }
        private final Object FLAGS=new Long(PROPERTY_MULTI_MOLECULE|PROPERTY_MULTI_VIEW);
        public Matrix3D getTransformationPreview() { return _m3dPrev;}
        public void transformPreview(Matrix3D m) { _m3dPrev=m;}
        public void setProperty(String id, Object value) {}
        private final static String supported[]={ COMMANDbackground, COMMANDbiomolecule, COMMANDreset};
        public Object getProperty(String getID) {
            if (getID==GET_SUPPORTED_COMMANDS) return supported;
            if (getID==GET_FLAGS) return FLAGS;
            if (getID==GET_CANVAS) return _p3d;
            return null;

        }
        public UniqueList<PView> getViewersSharingViewV(boolean proxies){ return _p3d._vDisplayed; }

        public void interpret(long options, String command0) {
            final String command=command0.trim(), word0=fstTkn(command);
            final int L0=sze(word0);
            if (Protein3dUtils.supports(word0,this)) {
                if (COMMANDbiomolecule.equals(word0)) {
                    final long m=atolDecHex(command,L0);
                    _molecules=m<0 ? 0 : m;
                    if (_p3d!=null) _p3d._comboBio.s(m<0 ? 0 : 2);
                } else if (COMMANDbackground.equals(word0)) {
                    _p3d.setBackground(C((int)(0xFFffFF&Protein3dUtils.rgbInScript(command))));
                } else if (COMMANDreset.equals(word0)) {
                    if (_p3d!=null) _p3d.getRotationAndTranslationOfView().unit().scale(5f);
                }
                ChDelay.repaint(_p3d,99);
            }
        }
        private boolean _disposed;
        public void dispose() {
            if (_disposed) return;
            _disposed=true;
            final Protein p=getProtein();
            _p3d.removeProtein( p);
            if (p!=null) p.removeProteinViewer(this);
            if (sze(_p3d._vDisplayed)==0) {
                dispos(_p3d);
                rmFromParent(_p3d);
            }
            new StrapEvent(this,StrapEvent.PROTEIN_VIEWER_CLOSED).run();
        }
    }
    public PView getView(Protein prot) {
        if (prot!=null)
            for(PView dp:_vDisplayed.asArray())
                if (dp.getProtein()==prot) return dp;
        return null;
    }
}
