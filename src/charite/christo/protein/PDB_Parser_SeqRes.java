package charite.christo.protein;

import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
The SEQRES lines in PDB files look like this:
<pre>
SEQRES  43 A  560  GLY VAL TRP PRO SER GLN TYR SER HIS VAL THR TRP LYS
SEQRES  44 A  560  LEU
SEQRES   1 B  560  MET SER LYS THR GLN GLU PHE ARG PRO LEU THR LEU PRO
SEQRES   2 B  560  PRO LYS LEU SER LEU SER ASP PHE ASN GLU PHE ILE GLN
SEQRES   3 B  560  ASP ILE ILE ARG ILE VAL GLY SER GLU ASN VAL GLU VAL
</pre>
   @author Christoph Gille
*/
public class PDB_Parser_SeqRes {
    private PDB_Parser_SeqRes(){}
    private static int seq32[]=new int[999],  countResidues, alignment[]=new int[999];
    private static byte[] resChain=new byte[999];
    static int countResidues() { return countResidues;}
    static byte[] getResidueChain() { return resChain;}
    static int[] parse(final byte T[], final int ends[], final int nL, String selectChains) {
        int nAA=0;
        for(int iL=0;iL<nL;iL++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1;
            final int L=ends[iL]-b;
            if (L<22||T[b]!='S'||T[b+1]!='E'||T[b+2]!='Q'||T[b+3]!='R')continue;
            final byte chain=T[b+11];
            if (selectChains!=null && selectChains.indexOf(chain)<0) continue;
            for(int iCol=19; iCol<68 && iCol+2<L; iCol+=4) {
                if (T[b+iCol]==' ') break;
                if (seq32.length<=nAA) {
                    final int newSze=seq32.length*3/2;
                    seq32=chSze(seq32,newSze);
                    resChain=chSze(resChain,newSze);
                }
                resChain[nAA]=chain;
                seq32[nAA]=charsTo32bit(T[b+iCol],T[b+iCol+1],T[b+iCol+2],chain);

                nAA++;
            }
        }
       countResidues=nAA;
       return seq32;
    }
    private static int atomResiduesUsed;
    static int alignmentLength() { return atomResiduesUsed;}
    static int[] alignment(int seq_at[],int nA) {
        final int seq_sr[]=seq32;
        final int nSeq_sr=countResidues;
        if (alignment.length<nA+2) alignment=new int[nA+99];
        int iA=0;
        for(int iS=0; iA<nA && iS<nSeq_sr;iA++,iS++) {
            final int aa_at=seq_at[iA];
            while( iS<seq_sr.length && aa_at!=seq_sr[iS]) iS++;
            alignment[iA]=iS;
        }
        atomResiduesUsed=iA;
        if (iA<alignment.length) alignment[iA]=-1; /* mark end */
        /*
           going from end to start to solve the following problem
           SvqqasRSKVSVAPLH should be    svqqaSRSKVSVAPLH
        */
        for(; --iA>0;)  {
            final int iAa_sr1=alignment[iA]-1;
            if (iAa_sr1>=0 && seq_sr[iAa_sr1]==seq_at[iA-1] && alignment[iA-1]!=iAa_sr1) {
                alignment[iA-1]=iAa_sr1;
            }
        }
        return alignment;
    }
}
/*
Bugs:
see 2pva  MSE: 1s29
ATOM    580  NZ  LYS A  74      47.866  87.970  -0.839  1.00 59.91           N

BUG in PDB:26DZ  Suche in Sequenz AFN suche in SEQRES "ALA PHE LEU ASN"
*/
