package charite.christo.protein;
import charite.christo.*;
import java.io.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.ChSoap.*;
/*
  http://www.ebi.ac.uk/ws/services/WSDbfetchDoclit?wsdl

  http://www.ebi.ac.uk/ws/services/WSDbfetchDoclit?wsdl
*/
public final class FetchSeqs {
    private final static boolean DEBUG=false;
    public final static long SKIP_EXISTING=1;
    public final static String
        CMD_WSDBFETCH_SEND_QUERY="CMD_WSDBFETCH_SEND_QUERY",
        CMD_WSDBFETCH_ERROR="CMD_WSDBFETCH_ERROR",
        CMD_WSDBFETCH_SUCCESS="CMD_WSDBFETCH_SUCCESS",
        DB_EMBL="EMBL",DB_UNIPARC="UNIPARC",
        DB_UNIPROT="UNIPROT",
        DB_NCBI_AA="NCBI_AA",
        NCBI_NT="NCBI_NT",
        DATABASES[]={DB_UNIPROT, DB_EMBL,DB_UNIPARC, DB_NCBI_AA, NCBI_NT};
    private FetchSeqs(){}
    private static String preferredFormat(String db) {
        return
            db==null ? null :
            strEquls(DB_UNIPROT,db) ? "uniprot" :
            DB_EMBL.equals(db) ? "embl" :
            db.startsWith("NCBI") ? "gb" :
            "fasta";
    }
    private static Object[] mapEntryFile=new Object[2];
    public static File dbColonID2file(String entry0) {
        if (entry0==null) return null;
        final String entry=rplcToStrg("NCBI:", "GB:", rplcToStrg("UNIPROTKB:", "UNIPROT:", entry0));
        final boolean isNcbi=entry0.startsWith("NCBI");
        Map<String,File> map=deref(mapEntryFile[isNcbi?1:0], Map.class);
        if (map==null) mapEntryFile[isNcbi?1:0]=newSoftRef(map=new HashMap());
        if (entry==null || sze(entry)>66) return null;
        File f=map.get(entry);
        if (f==null) {
            final int colon=entry.indexOf(':');
            if (colon<0) return null;
            final String database=( entry.startsWith("EXPASY:") || entry.startsWith("SWISS:") ) ? DB_UNIPROT : entry.substring(0,colon);
            map.put(entry,f=id2file(database,entry.substring(colon+1), null));
        }
        return f;
    }
    private static File id2file(String database,String id,String format0) {
        final String format=sze(format0)>0 ? format0 : preferredFormat(database);
        if (id==null||sze(id)>33 || database==null||sze(database)>33) return null;
        return file(dir()+"/"+(database+"_"+    filtrS('_'|FILTER_NO_MATCH_TO, FILENM,id)).toUpperCase()+"."+format);
    }

    private static File _dir;
    private static File dir() {
        if (_dir==null) {
            _dir=file(dirSettings() +"/wsdbfetch/");
            final String README=
                "You may remove the files in the directory to save memory\n"+
                "This directory contains protein files which are downloaded\n"+
                "via SOAP WSDbfetch from EBI\n";
            mkdrsErr(_dir,README);
        }
        return _dir;
    }
    public static Runnable thread_urlGet(long options, String tokens[]) {
        return thrdM("download", FetchSeqs.class, new Object[]{longObjct(options), tokens});
    }

    public static void download(long options, String tokens[]) {
        final Collection<String> vQuery=new HashSet();
        for(String db :DATABASES) {
            vQuery.clear();
            final String dbColon=db+":";
            if (tokens!=null) {
                for(String dbColonId:tokens) {
                    if (dbColonId!=null && dbColonId.startsWith(dbColon) ||
                        dbColonId!=null && DB_UNIPROT==db && (dbColonId.startsWith("EXPASY:")||dbColonId.startsWith("SWISS:"))) {
                        final int colon=dbColonId.indexOf(':'), pipe=dbColonId.indexOf('|');
                        final String s=pipe>0 ? dbColonId.substring(0,pipe) : dbColonId;
                        final String id=s.substring(colon+1);
                        if ( (options&SKIP_EXISTING)!=0 &&
                             (sze(id2file(db,id,preferredFormat(db)))>=PROTEIN_MIN_FILE_SZE || sze(file(Hyperrefs.toUrlString(s,Hyperrefs.PROTEIN_FILE)))>=PROTEIN_MIN_FILE_SZE)) {
                            continue;
                        }
                        vQuery.add(id);
                    }
                }
            }
            FetchSeqs.download(db, strgArry(vQuery), FetchSeqs.preferredFormat(db));
        }
    }
    private static BA _getResponse(final String database,final BA list_of_ids, final String format) {
        final boolean isNcbi=database.startsWith("NCBI");
        if (isNcbi) {
            final Object[] data[]={
                {"db","protein"},
                {"id",list_of_ids},
                {"retmode","text"},
                {"rettype","gb"}
            };

            return Web.getServerResponse(0l, "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi" , data);
        } else {
            final BA query=getRsc("soapWSDBFetch");
            if (query!=null) {
                query
                    .replace(STRPLC_FILL_RIGHT,"DATABASE", database)
                    .replace(STRPLC_FILL_RIGHT,"FORMAT", format)
                    .replace(STRPLC_FILL_RIGHT|STRPLC_FILL_RM,"QUERIES", list_of_ids);
                if (DEBUG) putln(ANSI_GREEN+"\nSocket Write"+ANSI_RESET+"\n");
                if (DEBUG) putln(query,"\n");
                return getResponse(0L, query, new BA(100*1000));
            }
        }
        return null;
    }

    private final static Map<String,Set> _setAlreadyLoaded=new HashMap();
    private final static int MAXNUM=100;
    public static void download(final String database,final String[] IDs,final String format0)  {
        if (database==null) return;
        synchronized( mkIdObjct("FetchSeqs",database)) {

            final boolean isNcbi=database.startsWith("NCBI");
            Set<String> setLoaded=_setAlreadyLoaded.get(database);
            if (setLoaded==null) _setAlreadyLoaded.put(database, setLoaded=new HashSet());

            int fromIdx=0;
            while(fromIdx<IDs.length) {
                final String format=sze(format0)>0 ? format0 : preferredFormat(database);
                final BA sb=new BA(IDs.length*10);
                for(int i=fromIdx, count=0; i<IDs.length && count<MAXNUM; i++) {
                    if (!setLoaded.contains(IDs[i])) {
                        final File f=id2file(database,IDs[i],format);
                        if (sze(f)==0) {
                            if (sze(sb)>0) sb.a(' ');
                            if (IDs[i]!=null) { sb.a(IDs[i]); count++; }
                        }
                    }
                    fromIdx=i+1;
                }
                final String queries=sb.toString();

                if (sze(queries)>0) {
                    final BA ba=_getResponse(database, sb,format);

                    //debugExit("\n\nFetchSeqs ba "+ba+"\n\n");
                    if (ba!=null && !BA.isError(ba)) {
                        final byte T[]=ba.bytes();
                        final int ends[]=ba.eol();
                        if (isNcbi) {
                            int lastStart=0;
                            String giID=null;
                            for(int iL=0; iL<ends.length; iL++) {
                                final int b= iL==0 ? 0 : ends[iL-1]+1, e=ends[iL];
                                if (strEquls("VERSION ",T,b)) {
                                    giID=wordAt(strstr(STRSTR_AFTER, "GI:", T,b, e), ba);
                                }
                                if (e-b>1 && T[b]=='/' && T[b+1]=='/') {
                                    if (giID!=null) {
                                        final File f=id2file(database,giID,"");
                                        wrte(f, T, lastStart, b);
                                        if (sze(f)>9) setLoaded.add(giID);
                                    }
                                    giID=null;
                                    lastStart=b+3;
                                }
                            }
                        } else {
                            final int end0=ba.end();
                            final int
                                beginO=strstr("<fetchBatchReturn" ,T,0,end0),
                                end=strstr("</fetchBatchReturn",T,beginO,end0),
                                begin=1+strchr('>',T,beginO,end0);

                            if (begin>0) {
                                ba.setBegin(begin);
                                ba.setEnd(end);
                                int from=0;
                                for(int iL=0; iL<ends.length; iL++) {
                                    final int b=iL==0 ? begin : ends[iL-1]+1, e=ends[iL];
                                    if (e-b<8) continue;
                                    final boolean entryStarts=T[b]=='I' && T[b+1]=='D' && T[b+2]==' ';
                                    if (entryStarts || T[b]=='A' && T[b+1]=='C' && T[b+2]==' ') {
                                        from=b;
                                        for(int iID=IDs.length; --iID>=0;) {
                                            final String id=IDs[iID];
                                            final int idxID=strstr(STRSTR_IC,id,T,b,e);
                                            if (idxID>0 && !is(LETTR_DIGT,T[idxID-1]) && !is(LETTR_DIGT,T,idxID+sze(id))) {
                                                final int slashSlash=strstr("\n//",T,b,end);
                                                if (slashSlash>0) {
                                                    final File f=id2file(database,id,format);
                                                    wrte(f, T, from,slashSlash+1);
                                                    if (sze(f)>9) setLoaded.add(id);
                                                } else stckTrc();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

