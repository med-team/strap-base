package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
   @author Christoph Gille
*/
public abstract class AbstractProteinViewerProxy extends AbstractProxy implements ProteinViewer, ChRunnable {

    public ProteinViewer pv(){ return (ProteinViewer)proxyObject(); }



    public Protein getProtein() { final ProteinViewer pv=pv(); return pv!=null ? pv.getProtein() : null;}
    public boolean setProtein(long options, Protein p, ProteinViewer inSameView) {
        try {
            final ProteinViewer pv=pv();
            this.toString=p+"@"+ChUtils.shrtClasNam(this);
            return pv!=null && p!=null && pv.setProtein(options, p,inSameView);
        } catch(Throwable ex) {ChUtils.stckTrc(ex); return false;}
    }

    public void handleEvent(StrapEvent ev) {
        final ProteinViewer pv=pv();
        if (pv instanceof StrapListener) ((StrapListener)pv).handleEvent(ev);
    }
    public void setProperty(String id, Object value) {
        final ProteinViewer pv=pv(); if (pv!=null) pv.setProperty(id, value);
    }
    public Object getProperty(String id) { final ProteinViewer pv=pv(); return pv!=null ? pv.getProperty(id) : null;}
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_GET_COLUMN_TITLE) return Protein3dUtils.titleFor3dViewers(Protein3dUtils.ppInV3D(this));
        return runCR(pv(), id,arg);
    }
    @Override public void dispose() {
        super.dispose();
        new StrapEvent(this, StrapEvent.PROTEIN_VIEWER_CLOSED).run();
    }
    public java.util.Collection<? extends ProteinViewer> getViewersSharingViewV(boolean proxyObject) {
        final ProteinViewer pv=pv();
        return pv==null ? null : pv.getViewersSharingViewV(true);
    }
    private String toString;
    @Override public String toString() { return toString!=null ? toString : super.toString();}

    {
        Protein3dUtils.initCustomizableSettings(this);
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> static utility >>> */

}
