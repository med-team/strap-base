package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**

   Strap events are broadcasted using <i>JAVADOC:StrapEvent#run()</i>.

   Listeners must implement <i>JAVADOC:StrapListener</i> and have a method <i>JAVADOC:StrapListener#handleEvent(StrapEvent)</i>.

   Plugins are usually automatically registered.

   Using the debug menu, listeners and events can be observed for debugging tools.

  @author Christoph Gille
*/
public class StrapEvent implements Runnable {
    public final static String RUN_DISPATCH="SEV$$D";
    public final static int
        FLAG_ALIGNMENT_CHANGED=1<<16,
        FLAG_RESIDUE_TYPES_CHANGED=1<<17,
        FLAG_ROW_HEADER_CHANGED=1<<18,
        FLAG_ATOM_COORDINATES_CHANGED=1<<19,
        FLAG_CHILDS_OF_PROTEINS_CHANGED=1<<20,

        CURSOR_MOVED_WITHIN_PROTEIN=1,
        CURSOR_MOVED_WITHIN_PROTEIN_DELAYED=2,
        CURSOR_CHANGED_PROTEIN=3,
        CURSOR_CLICKED=4,

        LISTENER_ADDED=9,

        PROTEIN_RENAMED=11|FLAG_ROW_HEADER_CHANGED,
        PROTEIN_ICON_CHANGED=12|FLAG_ROW_HEADER_CHANGED,

        OBJECTS_SELECTED=13,
        PROTEIN_SELECTED=14,
        RESIDUE_SELECTION_SELECTED=15,

        RUBBER_BAND_CREATED_OR_REMOVED=16,

        RESIDUE_TYPES_CHANGED=20 | FLAG_ALIGNMENT_CHANGED|FLAG_RESIDUE_TYPES_CHANGED,
        CHARACTER_SEQUENCE_CHANGED=21 | FLAG_ALIGNMENT_CHANGED|FLAG_RESIDUE_TYPES_CHANGED,
        NUCL_TRANSLATION_CHANGED=22 | FLAG_ALIGNMENT_CHANGED | FLAG_RESIDUE_TYPES_CHANGED,
        ATOM_COORDINATES_CHANGED=23 | FLAG_ATOM_COORDINATES_CHANGED| FLAG_ROW_HEADER_CHANGED,
        PROTEIN_3D_MOVED=24 | FLAG_ATOM_COORDINATES_CHANGED,
        VIEWER3D_MOLECULE_LOADED=25,
        ALIGNMENT_CHANGED=26|FLAG_ALIGNMENT_CHANGED,

        ORDER_OF_PROTEINS_CHANGED=30 |FLAG_ALIGNMENT_CHANGED|FLAG_ROW_HEADER_CHANGED,
        PROTEINS_HIDDEN=31 |FLAG_ALIGNMENT_CHANGED|FLAG_ROW_HEADER_CHANGED,
        PROTEINS_SHOWN=32 |FLAG_ALIGNMENT_CHANGED|FLAG_ROW_HEADER_CHANGED,
        PROTEINS_KILLED=33 |FLAG_ALIGNMENT_CHANGED|FLAG_ROW_HEADER_CHANGED,
        PROTEINS_ADDED=34 |FLAG_ALIGNMENT_CHANGED|FLAG_ROW_HEADER_CHANGED,

        VALUE_OF_RESIDUE_CHANGED=50,
        VALUE_OF_ALIGN_POSITION_CHANGED=51,
        VALUE_OF_RESIDUE_CHANGED_COLOR=52,
        VALUE_OF_ALIGN_POSITION_CHANGED_COLOR=53,
        VALUE_OF_PROTEIN_CHANGED=54,
        PROTEIN_PROTEIN_DISTANCE_CHANGED=55,

        RESIDUE_SELECTION_CHANGED_COLOR=60|FLAG_CHILDS_OF_PROTEINS_CHANGED,
        RESIDUE_SELECTION_CHANGED=61,
        RESIDUE_SELECTION_ADDED=62|FLAG_CHILDS_OF_PROTEINS_CHANGED,
        RESIDUE_SELECTION_DELETED=63|FLAG_CHILDS_OF_PROTEINS_CHANGED,
        RESIDUE_SELECTION_RENAMED=64|FLAG_CHILDS_OF_PROTEINS_CHANGED,

        HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED=101|FLAG_CHILDS_OF_PROTEINS_CHANGED,
        PLUGIN_CHANGED=102,

        BACKUP_WRITTEN=103,

        AA_SHADING_CHANGED=110,
        BACKGROUND_CHANGED=111,
        FONT_CHANGED=112,

        ALIGNMENT_SCROLLED=120,
        COMPUTATION_STARTED=121,
        PROTEIN_INFO_CHANGED=122,
        IMAGE_DOWNLOADED=123,

        PROGRESS=124,

        PROTEIN_VIEWER_LAUNCHED=132|FLAG_CHILDS_OF_PROTEINS_CHANGED,
        PROTEIN_VIEWER_CLOSED=133|FLAG_CHILDS_OF_PROTEINS_CHANGED,
        PROTEIN_VIEWER_PICKED=134,
        PROTEIN_VIEWER_SURFACES_CHANGED=135,
        PROTEIN_XREF_CHANGED=136,

        NEED_SCROLL_DOWN_ALIGNMENT=137,
        SCRIPT_LINE=150;
    private final static Object BUF[]={null};
    private static int _count;

    private static ChRunnable _align;
    private Object _objects[];
    private final Object _src;
    private final int _typ;

    public StrapEvent(Object source, int type) {
        _src=source;
        _typ=type;
    }
    public ChRunnable getProteinAlignment() {return _align;}
    public static void setProteinAlignmentForAll(ChRunnable pa) { _align=pa;}
    /** The ProteinAlignment dispatches this event to all Listeners. Threadsave. */
    public void run() { if (_align!=null) _align.run(RUN_DISPATCH, this); }
    /** The type of the event */
    public final int getType() {return _typ;}
    public final Object getSource() { return _src;}
    public Object[] parameters() { return _objects!=null ? _objects : NO_OBJECT; }
    public final StrapEvent setParameters(Object[] oo) { _objects=oo; return this;}

    @Override public String toString() {
        final BA sb=baClr(BUF);
        sb.a("\n"+ANSI_BOLD).a(_count++,5).a(ANSI_RESET+" Type="+ANSI_YELLOW)
            .a(finalStaticInts(StrapEvent.class,"").get(intObjct(getType()))).a(ANSI_RESET);
        final Object pp[]=parameters();
        final String cn=nam(clazz(_src));
        if (cn!=null) {
            final int dot=cn.lastIndexOf('.');
            sb.a(" Source=").a(cn,0,dot+1).a(ANSI_FG_BLUE+ANSI_UL).a(cn,dot+1,MAX_INT).a(ANSI_RESET);
        }
        sb.a('\n');
        if (sze(pp)>0) sb.a("      para={"+ANSI_FG_MAGENTA).join(pp,ANSI_RESET+","+ANSI_FG_MAGENTA).a(ANSI_RESET).a('}').aln(ANSI_RESET);
        return sb.toString();
    }

    public static void dispatchLater(int eventType, int timeInMillis) {
        if (eventType==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN) stckTrc();
        final int t=eventType&255;
        StrapEvent ev=DISPATCH_LATER[t];
        if (ev==null) DISPATCH_LATER[t]=ev=new StrapEvent(null,eventType);
        inEDTms(ev, timeInMillis);
    }
    public static void dispatch(int eventType) { StrapEvent.dispatchLater(eventType, 0); }
    private final static StrapEvent[] DISPATCH_LATER=new StrapEvent[256];

}
