package charite.christo.protein;
import charite.christo.BA;
/**
   @author Christoph Gille
   Classes inmplementinting this interface can export multiple alignment files.
*/
public interface AlignmentWriter {
    long
        HTML=1<<0,
        MSF=1<<1,
        CLUSTALW=1<<2,
        FASTA=1<<3,
        FILE_SUFFIX=1<<10,
        EXTRA_SPACE=1<<11,
        NUCLEOTIDE_TRIPLET=1<<12,
        NO_GAPS=1<<13,
        PDB_RESIDUE_NUMBER=1<<14,
        UNDERLINE_ANNOTATIONS=1<<21,
        SECONDARY_STRUCTURE=1<<22,
        CSS_BROWSER=1<<23,
        HTML_HEAD_BODY=1<<24,
        BALLOON_MESSAGE=1<<25,
        PROTEIN_ICONS=1<<27;
    void setProteins(Protein... proteins);
    /**
       The  alignment column index starts at zero.
       Note in STRAP only those columns are displayed where at least one protein has a residue.
       Common gaps are suppressed in the display.
       @see Protein#getResidueColumn() Protein.getResidueColumn()
    */
    void setColumnRange(int firstColumn, int LastColumn);
    /**
       Typically 50 to 70 residues are printed in one line.
       If MAX_INT is given, each protein is printed in one line.
    */
    void setResiduesPerLine(int resPerLine);
    /**
       The alignment text is written into the StringBuffer.
    */
    void getText( long mode, BA sb);
    /**
       The names for the row headers. Usually the protein names.
    */
    void setNames(String names[]);
    /**
     */
    void setCharForGapAndTerminalBlanks(char gap,char left,char right);
    /**
       A file name suffix like msf of fa or fasta or clustalW
    */
    String getFileExtension();
}
