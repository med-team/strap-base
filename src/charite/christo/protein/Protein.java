package charite.christo.protein;
import charite.christo.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.*;
import charite.christo.strap.ResidueAnnotation;
import charite.christo.strap.SequenceFeatures;
import charite.christo.strap.StrapAlign;
import charite.christo.strap.ResSelUtils;
import charite.christo.strap.ExpasyGff;
import charite.christo.strap.SPUtils;
import charite.christo.strap.StrapTree;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.ProteinMC.*;

/**
   <i>PACKAGE:charite.christo.strap.</i>
   The class Protein provides a protein model with amino acid
   sequences and coordinates for c-alpha atoms.
   <h3>Here are some frequently used get-methods:</h3>
   <ul>
   <li><i>JAVADOC:Protein#countResidues()</i> number of amino acids in the protein.</li>
   <li><i>JAVADOC:Protein#getResidueType()</i> returns the residue letters. This array of bytes contains one letter for each residue. It may have more elements than the protein has amino acids. The superfluous bytes are invalid.</li>
   Initially 0; increases monotonically
   as the residue type is successively modified.
   Can be used to check whether the amino acid sequence has stayed the same.
   </li>
   <li><i>JAVADOC:Protein#getResidueSecStrType()</i> returns a byte array representing the
   secondary structure type.</li>
   <li><i>JAVADOC:Protein#getResidueCalpha()</i> provides the c-alpha coordinates</li>
   </ul>
   @author Christoph Gille
*/
public class Protein implements HasMap, java.awt.event.ActionListener, HasName, HasWeakRef, Colored, ChRunnable, HasRendererMC, HasImage {
    public final static int
        BALLOON_MAXX=EM,
        CDS_GENE=0, CDS_PRODUCT=1, CDS_PROTEIN=2, CDS_NOTE=3, CDS_XREFS=4, CDS_TRANSCRIPT_ID=5,
        CDS_TRANSLATION=6,
        DRAG_MSA=1<<0, DRAG_ALL_CHAINS=1<<1, DRAG_ORIG=1<<2;
    public final static String
        TRANSLATIONS_AS_STRING[]={"Forward", "Reverse", "Forward complement", "Reverse complement"},
        KEY_SUGGEST_PDB_ID="P$$SP",
        RUN_ALIGNMENT_GET_PROTEINS="P$$AGPP",
        RUN_ALIGNMENT_ADD_AWT_LISTENERS="P$$AAL",
        RUN_ALIGNMENT_GET_PROTEIN_LABEL="P$$PL",
        RUN_ALIGNMENT_GET_GAP2COL="P$$GG2C",
        RUN_ALIGNMENT_GET_DRAG_OPTIONS="PA$$GDO";

    private final static int A_BF=0, A_COORD=1, A_COORD_PV=2, A_NAME_S=3, A_NUM=4, A_OCCUP=5, A_TYPE=6, A_COORD_O=7,
        C_COORD=8, C_COORD_ORIG=9, C_COORD_PREV=10, CHAIN_Fi=11, CHAIN_Li=12,
        RES_INS_CODE=14, RES_ACC=15, RES_ATOM_NUM=16, RES_CHAIN=17, RES_FIRST_Ai=18, RES_LAST_Ai=19,
        RES_NUM=20, RES_PHI=21, RES_PSI=22, RES_SS=23, RES_T=24, RES_T32=25, RES_T_STRG=26, RES_T_FL_STRG=27,
        RES_HAS_CA=28, FIRST_LAST_IDX=29, RES_SUBSET=30,
        MC_PROT_NOT_SPECIFIED[]=new int[MCA_MAX];
    private final static Object REF_PDB[]={null}, HAYSTACKS1[]=new Object[20],_BA[]=new Object[3];
    public final static Protein[] NONE={};
    private final static byte[] TRIPLET=new byte[3];
    private boolean _transient, _inAli=true, _loaded;

    public final static int G_OPT_VIEWER=1<<1;
    private static int _staticOpt;
    public static void addOptionsG(int opt) { _staticOpt|=opt; }
    public static int optionsG() { return _staticOpt; }
    private Collection<String> _vAllIds, _vAllDbRefs, _vCDS;
    /* ---------------------------------------- */
    // >>> Instance >>> */
    public Protein(ChRunnable alignment) {
        super();
        _align=alignment;
    }
    public Protein() { this(null); }
    /**
       creates Protein from a protein file
       The file type may be PDB, SWISSPROT, EMBL, FASTA
       The method is not thread safe because it uses a static byte buffer for file data.
    */

    public static Protein newInstance(File f) { return  newInstance(0L,f); }
    public static Protein newInstance(long options,File proteinFile) {
        if (sze(proteinFile)==0) return null;
        final Protein p=new Protein(StrapAlign.getInstance());
        p.setName(proteinFile.getName());
        p.setFile(proteinFile);
        SPUtils.parseProtein(proteinFile,(BA)null,options,p);
        return p;
    }

    public <P extends Protein> P cloneProtein(P pNew) {
        pNew.setResidueType(getResidueTypeFullLength());
        pNew.setName("cloned_"+getName());
        pNew.setResidueIndexOffset(getResidueIndexOffset());
        if (getResidueCalpha()!=null) {
            pNew.setAtomCoordOriginal(getAtomCoordinates(null));
            pNew.setAtomNumber (getAtomNumberFullLength());
            pNew.setAtomType32bit(getAtomName32FullLength());
            pNew.setResidueAtomNumber(getResidueAtomNumberFullLength());
            pNew.setResidueChain(getResidueChainFullLength());
            pNew.setResidueNumber(getResidueNumberFullLength());
            pNew.setResidueCalphaOriginal(getResidueCalphaOriginalFullLength());
            pNew.setResidueType32bit(getResidueName32FullLength());
            pNew.setResidueFirstAndLastAtomIdx(getResidueFirstAtomIdxFullLength(),getResidueLastAtomIdxFullLength());
            pNew.setResidueInsertionCode(getResidueInsertionCodeFullLength());
        }
        pcp(KEY_CLONED_FROM, WEAK_REF,pNew);
        return pNew;
    }
    /* <<<  Instance <<< */
    /* ---------------------------------------- */
    /* >>> Chains  >>> */
    /**
       Indices of the residues each chain starts with.
       To obtain the residue index of chain B call
       getChainFirstResidueIdx('B');
       See <i>JAVADOC:Protein#getChainLastResidueIdx(char)</i>
    */
    public synchronized int getChainFirstResidueIdx(char chain) {
        final Object[] c=cached();
        if (c[CHAIN_Fi]==null) calcFirstResidueIdx(c);
        final int cc[]=(int[])c[CHAIN_Fi];
        return cc!=null ? cc[chain&255] : -1;
    }
    /** See <i>JAVADOC:Protein#getChainFirstResidueIdx(char)</i>  */

    public synchronized int getChainLastResidueIdx(char chain) {
        final Object[] c=cached();
        if (c[CHAIN_Li]==null)  calcFirstResidueIdx(c);
        final int cc[]=(int[])c[CHAIN_Li];
        return cc!=null ? cc[chain&255] : -1;
    }
    private void calcFirstResidueIdx(Object[] c) {
        final byte chain[]=getResidueChain();
        if (chain==null) return;
        final int[] first=new int[256]; c[CHAIN_Fi]=first; Arrays.fill(first,-1);
        final int[] last=new int[256];  c[CHAIN_Li]=last;  Arrays.fill(last,-1);
        final int n=mini(chain.length,countResidues());
        for(int i=0; i<n; i++) {
            final byte ch=chain[i];
            if (first[ch]<0) first[ch]=i;
            last[ch]=i;
        }
    }
    /* <<< Chains <<< */
    /* ---------------------------------------- */
    /* >>> Atoms >>> */
    /**
       Indices of the atoms each residue starts with.
       To obtain the atom index of the 42th amino acid call
       int idx=getResidueFirstAtomIdx(42);
       then getAtomCoordinates()[idx*3] getAtomCoordinates()[idx*3+1] getAtomCoordinates()[idx*3+2]
       See <i>JAVADOC:Protein#getResidueLastAtomIdx(int)</i>
    */
    public synchronized int getResidueFirstAtomIdx(int resIdx) { return resIdx<countResidues() ? get(resIdx, getResidueFirstAtomIdx()) : INT_NAN;}
    /*
      Only used by ProteinParser
      See <i>JAVADOC:Protein#getResidueFirstAtomIdx(int)</i>
    */
    public synchronized int getResidueLastAtomIdx(int resIdx) { return resIdx<countResidues() ? get(resIdx, getResidueLastAtomIdx()) : INT_NAN; }
    /*
      Only used by ProteinParser
      See <i>JAVADOC:Protein#getResidueFirstAtomIdx(int)</i>
    */
    public synchronized void setResidueFirstAndLastAtomIdx(int first[],int last[]) {
        _resFstAtomIdx=first;
        _resLstAtomIdx=last;
        clearCache();
    }
    private int[] getResidueFirstAtomIdxFullLength() { return _resFstAtomIdx;}
    private int[] getResidueLastAtomIdxFullLength() { return _resLstAtomIdx;}
    public synchronized int[] getResidueFirstAtomIdx() { return !isResSubset() ? getResidueFirstAtomIdxFullLength() : (int[])computeSubset()[RES_FIRST_Ai];}
    public synchronized int[] getResidueLastAtomIdx() { return !isResSubset() ? getResidueLastAtomIdxFullLength() : (int[])computeSubset()[RES_LAST_Ai];}

    private int[] _resLstAtomIdx, _resFstAtomIdx;
    private float[] _resPhi, _resPsi, _atomCoordOriginal,_atomBF,_atomOccu, _cAlphaCoordOriginal, _resAccess;
    public synchronized void setResidueAnglePsi(float angle[]) { _resPsi=angle; clearCache();}
    private float[] getResidueAnglePsiFullLength() { return _resPsi; }
    public synchronized float[] getResidueAnglePsi() { return !isResSubset() ? getResidueAnglePsiFullLength() : (float[])computeSubset()[RES_PSI]; }
    public synchronized void setResidueAnglePhi(float angle[]) { _resPhi=angle; clearCache();}
    private float[] getResidueAnglePhiFullLength() { return _resPhi;}
    /* <<< Atoms <<< */
    /* ---------------------------------------- */
    /* >>> HeteroCompound >>> */
    private final static int HHh=0, HHn=1, HHnh=2;
    private final UniqueList<HeteroCompound> _vHH[]=new UniqueList[3];
    private UniqueList<HeteroCompound> vHH(char t) {
        final int i=t=='H'?HHh:t=='N'?HHn:HHnh;
        if (i==HHnh && t!='*' && myComputer()) assrt();
        if (_vHH[i]==null) {
            _vHH[i]=new UniqueList(HeteroCompound.class).i(i==HHh?IC_FLAVIN:i==HHn?IC_DNA:null).t(i==HHh?"Hetero compounds":i==HHn?"DNA/RNA":null);
            if (i==HHnh) _vHH[i].addHook(UniqueList.HOOK_CHANGE,this,"H_CHANGED");
        }
        return _vHH[i];
    }
    public synchronized HeteroCompound[] getHeteroCompounds(char t) {
        final UniqueList<HeteroCompound> v=_vHH[t=='H'?HHh:t=='N'?HHn:HHnh];
        if (v==null) return HeteroCompound.NONE;
        final HeteroCompound[] hh=v.asArray();
        for(HeteroCompound h : hh) h.setBelongsToProtein(WEAK_REF);
        return hh;
    }
    public final static long HETERO_ADD_UNIQUE=1<<0, HETERO_ADD_APPLY_INVERSE_MX=1<<1;
    public synchronized boolean containsHeteroCompound(HeteroCompound h) {
        final UniqueList v=_vHH[HHnh];
        if (v!=null) {
            for(Object het: v.asArray()) if (het.equals(h)) return true;
        }
        return false;
    }

    public boolean addHeteroCompounds(long options, HeteroCompound...hh) { /* no synchronized */
        boolean changed=false;
        for(int iH=0; iH<sze(hh); iH++) {
            final HeteroCompound h=hh[iH];
            if (h==null || 0!=(options&HETERO_ADD_UNIQUE) && containsHeteroCompound(h)) continue;
            if (0!=(options&HETERO_ADD_APPLY_INVERSE_MX)) {
                final Matrix3D m3d=getRotationAndTranslation();
                if (m3d!=null && !m3d.isUnit()) h.setCoordinates(m3d.newInverse().transformPoints(h.getCoordinates(), null, MAX_INT));
            }
            final boolean isDna=h.isNucleotideChain();

            changed|=vHH(isDna ? 'N':'H').add(h);
            if (isDna) adUniqR(h, 0, vHH('*'));
            else vHH('*').add(h);
        }
        if (changed) incrementMC(MC_HETERO_COMPOUNDS_V,this);
        return changed;
    }
    public boolean removeHeteroCompound(HeteroCompound h) { /* no synchronized */
        if (h==null) return false;
        boolean changed=false;
        for(UniqueList v :_vHH) changed|=remov(h,v);
        if (changed) {
            incrementMC(MC_HETERO_COMPOUNDS_V,this);
            if (h.getBelongsToProtein()==this) h.setBelongsToProtein(null);
            StrapEvent.dispatchLater(StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED,333);
        }
        return changed;
    }
    /* <<< HeteroCompound <<< */
    /* ---------------------------------------- */
    /* >>> Residues >>> */
    private int _resNum[], _resType32bit[], _resAtomNumber[], _hashCd, _hashCdMC, _countR;
    private byte _charSeq[], _resChain[], _secStr[], _insCode[];
    private short _numResInChain[];

    /**
       The original character sequence which can be aminos or nucleotides.
       Do not use this method.
    */
    public synchronized void setCharSequence(byte[] tt) {
        incrementMC(MC_CHARACTER_SEQUENCE,this);
        _charSeq=tt;
        clearCache();
    }
    /**
       The original character sequence which can be aminos or nucleotides.
       Do not use this method.
    */
    public synchronized byte[] getCharacters() { return nNull(_charSeq); }
    /**
       The sequence in one letter format.
       Use this method only if <i>JAVADOC:Protein#getResidueType(int)</i> cannot be used for performance reasons.
       The byte array might be longer than the number of residues in the proteins.
       In this case it is 0-terminated.
       Never ever obtain the length of the protein by getResidueType().length.
       Use <i>JAVADOC:Protein#countResidues() instead.</i>
    */
    public synchronized byte[] getResidueType() {
        final byte[] fullLength=getResidueTypeFullLength();
        final boolean[] bb=getResidueSubsetB();
        final int mc=_subsetMC+MC[MC_RESIDUE_TYPE_FULL];
        MC[MC_RESIDUE_TYPE]=mc;
        if (bb==null) return fullLength;
        return (byte[])computeSubset()[RES_T];
    }

    public synchronized int getResidueTypeHashCode() {
        final byte bb[]=getResidueType();
        final int N=countResidues(), mc=mc(MC_RESIDUE_TYPE);
        int hc=_hashCd;
        if (_hashCdMC!=mc || hc==0) {
            _hashCdMC=mc;
            hc=_hashCd=hashCd(bb,0,N,true);
        }
        return hc;
    }

    public synchronized byte[] getResidueTypeExactLength() { return getResidueType();}
    //  /** get the i-th amino acid. The index starts at 0 */
    public synchronized byte getResidueType(int i) {  return i<countResidues() ? get(i,getResidueType()) : 0; }
    /**  set the array of amino acids */
    public synchronized void setResidueType(Object seq) { setCharSequence(filtrBB(0L, LETTR, seq));}
    /** number of residues, returns  getResidueType().length */
    public synchronized int countResidues() {
        final byte bb[]=getResidueType();
        final int cr=bb==null ? 0:bb.length;
        if (_countR!=cr) { _countR=cr; MC_GLOBAL[MCA_RES_COUNT_NOSYNC]++; }
        return cr;
    }

    /** the chain denominators  of each residue  */
    public synchronized byte[] getResidueChain() { return !isResSubset() ? getResidueChainFullLength() : (byte[])computeSubset()[RES_CHAIN];}
    public synchronized byte[] getResidueChainFullLength() { return _resChain;}
    /** chain denominators of the i-th residue, usually capital letter */
    public synchronized byte getResidueChain(int i) {  return i<countResidues() ? get(i,getResidueChain()) : 0; }
    /** array  chain denominators, usually capital letter */
    private String _chains;
    public synchronized void setResidueChain(byte bb[]) {
        _resChain=bb;
        if (bb!=null) {
            byte[] sb=new byte[128];
            int count=0;
            byte c_last=0;
            for(int i=0; i<bb.length; i++) {
                final byte c=bb[i];
                if (c!=c_last && is(LETTR_DIGT_US,c) && strchr((char)c,sb,0,count)<0) sb[count++]=c;
                c_last=c;
            }
            _chains=bytes2strg(sb,0,count);
        }
        clearCache();
    }
    public String getChainsAsString() { return orS(_chains,"");}

    public synchronized short countResiduesInChain(byte chain) {
        final byte[] rc=getResidueChain();
        if (rc==null) return 0;
        if (_numResInChain==null) {
            _numResInChain=new short[256];
            for(int i=mini(countResidues(),rc.length);--i>=0;) _numResInChain[rc[i]]++;
        }
        return _numResInChain[chain];
    }
    /**
       Numbers of each residue, as recorded in the PDB files.
       Is usually not identical with running index.
       column 23-26
    */
    public synchronized int[] getResidueNumber() {
        return !isResSubset() ? getResidueNumberFullLength() : (int[])computeSubset()[RES_NUM];
    }

    public synchronized int[] getResidueNumberFullLength() { return _resNum;}
    /**  Number of i-th residue, as recorded in the PDB files or -1.*/
    public synchronized int getResidueNumber(int i) {
        final int[] num=getResidueNumber();
        return num==null || i>=num.length || i<0 ? INT_NAN : num[i];
    }
    /** set Numbers of each residue, as recorded in the PDB files.*/
    public synchronized void setResidueNumber(int[] rn) {_resNum=rn; clearCache();}
    /**
       ATOM    200  CA  LEU A  27B      8.378  37.835  28.244  1.00 37.40           C
       ATOM    208  CA  LEU A  27C      6.042  34.960  27.222  1.00 42.03           C
       ATOM    216  CA  ASN A  27D      2.874  36.266  25.650  1.00 42.04           C
       ^
       used in only a few PDB entries as e.g. in pdb43ca pdb1vdc
    */
    public synchronized byte[] getResidueInsertionCode() { return !isResSubset() ? getResidueInsertionCodeFullLength() : (byte[])computeSubset()[RES_INS_CODE];}
    public synchronized byte[] getResidueInsertionCodeFullLength() { return _insCode;}
    public synchronized byte getResidueInsertionCode(int i) { return i<countResidues() ? get(i,getResidueInsertionCode()) : 0;}
    public synchronized void setResidueInsertionCode(byte[] ic) {
        _insCode=ic;
        for(int i=sze(ic); --i>=0;) if (ic[i]==' ') ic[i]=0;
        clearCache();}
    /**
       Names (4 characters) of  residues, as recorded in the 4th column of  PDB files.
       LYS =&gt; 'L' + 'Y'*256 +'S'*256*256+' '*256*256*256
       See <i>JAVADOC:ChUtils#bit32To4chars(int,char[])</i>
       See <i>JAVADOC:ChUtils#charsTo32bit(int,int,int)</i>
    */
    public synchronized int[] getResidueName32() { return !isResSubset() ? getResidueName32FullLength() : (int[])computeSubset()[RES_T32];}
    private int[] getResidueName32FullLength() { return _resType32bit;}
    //   /**  Names of i-th residue, as recorded in the PDB files or -1.*/
    public synchronized int getResidueName32(int i) {
        int name32=i<countResidues() ? get(i,getResidueName32()) : MIN_INT;
        if (name32<=0) {
            final String s=toThreeLetterCode(getResidueType(i));
            name32=(s.charAt(0))+(s.charAt(0+1)<<8)+(s.charAt(0+2)<<16)+(' '<<24);
        }
        return name32;
    }
    /** set Names of each residue, as recorded in the PDB files. */
    public synchronized void setResidueType32bit(int[] rn) { _resType32bit=rn; clearCache();}

    /**
       Atom number of the first atom of the amino acid.
       <pre>
       ATOM  39607  CG  GLN V 195      18.039-150.197  79.532  1.00 76.24           C
       </pre>
       The atom number is 39607.
    */
    public synchronized int[] getResidueAtomNumber() { return !isResSubset() ? getResidueAtomNumberFullLength() : (int[])computeSubset()[RES_ATOM_NUM];}
    public synchronized int[] getResidueAtomNumberFullLength() { return _resAtomNumber;}
    /**  Atom number of i-th residue, as recorded in the PDB files or -1.*/
    public synchronized int getResidueAtomNumber(int i) {return i<countResidues() ? get(i,getResidueAtomNumber()) : MIN_INT;}
    /** set atom numbers  of each residue, as recorded in the PDB files.*/
    public synchronized void setResidueAtomNumber(int[] rai) {_resAtomNumber=rai; clearCache();}
    // ----------------------------------------
    /** Residue accessibility in square Angstroms, as taken from DSSP.  */
    public synchronized float[] getResidueSolventAccessibility() { return !isResSubset() ? getResidueSolventAccessibilityFullLength() : (float[])computeSubset()[RES_ACC]; }
    private float[] getResidueSolventAccessibilityFullLength() { return _resAccess; }
    /** Set accessibilities of residues */
    public synchronized void setResidueSolventAccessibility(float[] a) {
        incrementMC(MC_SOLVENT_ACCESSIBILITY,this);
        _resAccess=a;
        clearCache();
    }

    // ----------------------------------------
    /** secondary structure (H,E,C) of i-th residue  , H=helix, C=coil, E=Extended,Sheet */
    public synchronized byte getResidueSecStrType(int i) {return i<countResidues() ? get(i, getResidueSecStrType()) : 0;}
    /** Secondary structure (H,E,C) */
    public synchronized void setResidueSecStrType(byte[] a) {
        incrementMC(MC_RESIDUE_SECSTRU,this);
        _secStr=a;
        clearCache();
    }
    public synchronized byte[] getResidueSecStrType() { return !isResSubset() ? getResidueSecStrTypeFullLenght() : (byte[])computeSubset()[RES_SS]; }
    /** H=helix, C=coil, E=Extended,Sheet  */
    public synchronized byte[] getResidueSecStrTypeFullLenght() {
        if (_secStr==null) _secStr=helixSheet(0,MAX_INT,null,null);
        return _secStr;
    }
    protected synchronized byte[] helixSheet(int from, int to, boolean selected[], BA sb) {
        final BA secStru=getPdbTextSecStru();
        if (secStru==null) return null;
        final byte[] txt=secStru.bytes(), T=txt;
        final int ends[]=secStru.eol();
        byte[] ss=null;
        for(int iL=0; iL<ends.length; iL++) {
            final int b=iL==0?0:ends[iL-1]+1,  e=ends[iL];
            final boolean sheet=T[b]=='S'&&T[b+1]=='H'&&T[b+2]=='E'&&T[b+3]=='E'&&T[b+4]=='T';
            final boolean helix=T[b]=='H'&&T[b+1]=='E'&&T[b+2]=='L'&&T[b+3]=='I'&&T[b+4]=='X';
            if (!sheet && !helix) continue;
            final int CHAIN1=sheet ? 21 : 19, RN1=sheet ? 22 : 21;
            final int CHAIN2=sheet ? 32 : 31, RN2=33;
            if (b+CHAIN2>=T.length) {
                putln(RED_ERROR+"parse helixSheet line too short: ", toStrg(T,b,e));
                continue;
            }
            final int i1=resNumAndChain2resIdxZ(true, atoi(T,b+RN1,e),(char)T[b+CHAIN1], is(LETTR_DIGT,T,b+RN1+4)?(char)T[b+RN1+4]:(char)0);
            final int i2=resNumAndChain2resIdxZ(true, atoi(T,b+RN2,e),(char)T[b+CHAIN2], is(LETTR_DIGT,T,b+RN2+4)?(char)T[b+RN2+4]:(char)0);
            if (sb!=null) {
                for(int i=i1; i<i2;i++) {
                    if (from<=i && i<=to && (selected==null || i<selected.length && selected[i])) {
                        sb.a(T,b,e);
                        if (T[e-1]!='\n') sb.a('\n');
                        break;
                    }
                }
            } else {
                if (ss==null) ss=new byte[sze(getResidueTypeFullLength())];
                final int f=i1>=0?i1:0, t=mini(i2+1,ss.length);
                if (f<t) Arrays.fill(ss,f,t, (byte)(sheet?'E':'H'));
            }
        }
        return ss;
    }
    /* <<< Sec Structure <<< */
    /* ---------------------------------------- */
    /* >>> Atom >>> */
    private int _atomName32bit[],_atomNum[];
    private byte[] _atomType;
    /**
       Names (4 characters) of atoms, as recorded in the 3rd column of  PDB files.
       cAlpha= CA =&gt; 'C' + 'A'*256 +' ''*256*256+' '*256*256*256
       See <i>JAVADOC:ChUtils#bit32To4chars(int,char[])</i>
       See <i>JAVADOC:ChUtils#charsTo32bit(int,int,int)</i>
    */
    public synchronized int[] getAtomName32() { return !isResSubset() ? getAtomName32FullLength() : (int[])computeSubset()[A_NAME_S];     }
    private int[] getAtomName32FullLength() { return _atomName32bit;}
    public synchronized int getAtomName32(int iAtom) {
        final int at[]=getAtomName32();
        return at!=null && iAtom<at.length && 0<=iAtom && iAtom<countAtoms() ? at[iAtom] : -1;
    }
    public synchronized void setAtomType32bit(int at[]) {  _atomName32bit=at; clearCache();}

    public synchronized byte[] getAtomType() { return !isResSubset() ? getAtomTypeFullLength() : (byte[])computeSubset()[A_TYPE];     }
    private byte[] getAtomTypeFullLength() { return _atomType;}
    public synchronized void setAtomType(byte at[]) {  _atomType=at; clearCache();}

    /** The atom number is recorded in the 2nd column of pdb files */
    public synchronized void setAtomNumber(int atomNumber[]) {  _atomNum=atomNumber; clearCache();}
    /** The atom number is recorded in the 2nd column of pdb files */
    public synchronized int[] getAtomNumber() { return !isResSubset() ? getAtomNumberFullLength() : (int[])computeSubset()[A_NUM];}
    private int[] getAtomNumberFullLength() { return _atomNum;}

    public synchronized void setAtomCoordOriginal(float ii[]) { _atomCoordOriginal=ii; clearCache();}
    /**  Untransformed Coordinates of all atoms  order [x1,y1,z1,x2,y2,z2,x3,...] */
    private float[] getAtomCoordOriginalFullLength() { return _atomCoordOriginal;}

    /**  Transformed Coordinates of all atoms  order [x1,y1,z1,x2,y2,z2,x3,...].
         Never ever obtain the number of atoms by getAtomCoordinates().length.
         Instead use <i>JAVADOC:Protein#countAtoms()</i>
    */
    public synchronized float[] getAtomCoordinates() {
        final Object[] cached=cached();
        final float[] original=getAtomCoordinates(null);
        final Matrix3D t=getRotationAndTranslation();
        if (original==null) return null;
        final int mc=mcSum(MC_MATRIX3D,MC_ATOM_COORD_ORIG);
        float ff[]=(float[])cached[A_COORD];
        if (ff==null || setMC(MC_ATOM_COORD,mc)) {
            if (t==null) ff=original;  else t.transformPoints(original, ff=new float[original.length] ,original.length);
            cached[A_COORD]=ff;
        }
        return ff;
    }
    public synchronized int countAtoms(){ final float ff[]=getAtomCoordinates(null); return ff==null? 0 : ff.length/3;  }
    public synchronized void setAtomBFactor(float atomBFactor[]){_atomBF=atomBFactor; clearCache(); }
    public synchronized float[] getAtomBFactor() { return !isResSubset() ? getAtomBFactorFullLength() : (float[])computeSubset()[A_BF];}
    private float[] getAtomBFactorFullLength() { return _atomBF;}

    public synchronized void setAtomOccupancy(float atomOccupancy[]){_atomOccu=atomOccupancy; clearCache(); }
    public synchronized float[] getAtomOccupancy() { return !isResSubset() ? getAtomOccupancyFullLength() : (float[])computeSubset()[A_OCCUP];}
    private float[] getAtomOccupancyFullLength() { return _atomOccu;}
    /* <<< Atom <<< */
    /* ---------------------------------------- */
    /* >>> Header >>> */
    private String _header, _organism, _organismSc, _compound, _title;
    public void setHeader(String s) { _header=s;}
    public void setOrganism(String s) { _organism=s;}
    public void setOrganismScientific(String s) { _organismSc=s;}
    public void setCompound(String s) { _compound=s;}
    public void setTitle(String s) { _title=s;}
    public String getCompound() { return _compound;}
    public String getTitle() { return _title;}
    public String getHeader() { return _header;}
    public String getOrganism() { return _organism!=null ? _organism : getOrganismScientific();}
    public String getOrganismScientific() { return _organismSc;}
    /* DroMe for Drosophila_melanogaster */
    public String getOrganism5() {
        final String sc=getOrganismScientific();
        final int spc=strchr('_',sc);
        BA r=null;
        if (spc>1 && spc+2< sze(sc)) {
           r=new BA(33);
            if ("Escherichia_Coli".equalsIgnoreCase(sc)) r.a("Coli");
            else if ("Homo_Sapiens".equalsIgnoreCase(sc)) r.a("Human");
            else if ("Mus_Musculus".equalsIgnoreCase(sc)) r.a("Mouse");
            else if (sc.startsWith("Rattus")) r.a("Rat");
            else
                r.a(uCase(sc.charAt(0)))
                    .a(lCase(sc.charAt(1)))
                    .a(lCase(sc.charAt(2)))
                    .a(uCase(sc.charAt(spc+1)))
                    .a(lCase(sc.charAt(spc+2)));

        } else {
            final String sp=getSwissprotID();
            final int us=strchr('_',sp);
            if (us>=0 && sze(sp)-us>5) {
                r=new BA(33);
                for(int i=us+1; i<sp.length(); i++) {
                    final int c=sp.charAt(i);
                    r.a((char)( i==us+1 || i==us+4 ?  c&~32 : c|32));
                }
            }
        }
        return toStrg(r);
    }

    public  final static String[] INFO_TITLE="When file last modified;When loaded;Name;Title;Compound;Organism;Organism scientific; EC;Accession;Uniprot;PDB;Has coordinates Yes/No;Translated from nucleotides Yes/No;Balloon text;Hetero compounds".split(";");
    private final String[] _info=new String[INFO_TITLE.length];

    private final int _infoMC[]=new int[INFO_TITLE.length];
    private CharSequence info(int i) {

        if (i==0) {
            final File f=getFile();
            return f==null?"":fmtDate(f.lastModified());
        }
        if (i==9 || i==14) {
            final BA sb=baClr(0,_BA);
            int count=0;
            synchronized(sb) {
                if (i==9) {
                    final String acc=getAccessionID();
                    String s=delPfx("UNIPROT:",acc);
                    if (acc!=null && s!=acc) return s;
                    for(String id : FindUniprot.ids(this)) {
                        sb.a(count++==0?" By_similarity: ":" ").a(id, strEquls("UNIPROT:",id)?8:0,MAX_INT);
                    }
                }
                if (i==14) {
                    for(HeteroCompound h : getHeteroCompounds('*')) {
                        final String n=h.getCompoundName();
                        if (strstr(STRSTR_w,n,sb)<0) sb.a(' ',count++==0?0:1).a(n);
                    }
                }
                return sb;
            }
        }
        return
            i==1 ? fmtDate(_when).substring(5) :
            i==2 ? getName() :
            i==3 ? getTitle() :
            i==4 ? getCompound() :
            i==5 ? getOrganism() :
            i==6 ? getOrganismScientific() :
            i==7 ? new BA(99).join(getEC()," ") :
            i==8 ? getAccessionID() :
            i==10? getPdbID() :
            i==11? (getResidueCalpha()!=null ? "Yes" : "No") :
            i==12? (getNucleotides()!=null ? "Yes" : "No") :
            i==13? balloonText(false) :
            null;
    }

    public String getInfo(int i) {
        if (i==13) return balloonText(false);
        final Customize cust=Customize.customize(Customize.proteinInfoRplc);
        final int mc=cust.mc()+MC[MC_PROTEIN_LABELS];
        if (i<INFO_TITLE.length && (_info[i]==null || _infoMC[i]!=mc)) {
            _infoMC[i]=mc;
            final CharSequence cs=info(i);
            String s=null;
            if (cs!=null && i<5) {
                s=toBA(cs).replaceChar('\t',' ').replaceChar('\n',' ').toString().replaceAll("   +","  ");
                for(String line : cust.getSettings()) {
                    if (sze(line)==0 || line.charAt(0)=='#') continue;
                    final int tab=line.indexOf('\t');
                    final String replacement=toStrgTrim(rplcToStrg("%20", " ", tab<0?"":line.substring(tab+1)));
                    try {
                        s=s.replaceAll(tab<0?line:line.substring(0,tab), replacement);
                    } catch(Exception ex) { putln(RED_WARNING+"proteinInfoRplc ",ex);}
                }
            } else s=toStrg(cs);
            _info[i]=s;
        }
        return _info[i];
    }
    /* <<< Header <<< */
    /* ---------------------------------------- */
    /* >>> IDs >>> */
    private String _pdbId, _inferredPDB, _uniprotId, _swissID, _accID, _ec[];
    private static String rplcSbyU(String s) {
        return s!=null && s.startsWith("SWISS:") ? "UNIPROT:"+s.substring(6) : s;
    }
    public String[] getEC() { return nNull(_ec); }
    public void setEC(String...s) { _ec=s;}
    public synchronized String getUniprotID() {
        final String up=_uniprotId;
        if (sze(up)>0) return up;
        if (_pdbId==null) for(String s : getSequenceRefs()) if (s.startsWith("PDB:")) return s;
        return get(0,FindUniprot.ids(this));
    }
    public synchronized void setUniprotID(String s0) {
        if (s0!=null) {
            final String s=s0.startsWith("UNIPROT:") ? s0 : "UNIPROT:"+s0.substring(s0.indexOf(':')+1);
            addSequenceRef(_uniprotId=delLstCmpnt(s,'-').intern());
        }
    }
    public synchronized void setSwissprotID(String id) { _swissID=id; }
    public String getSwissprotID() { return _swissID;}

    public synchronized String getPdbID() {
        if (_pdbId==null) for(String s : getSequenceRefs()) if (s.startsWith("PDB:")) return s;
        return _pdbId;
    }

    public String getAnyPdbID() {
        String id=getPdbID();
        if (id==null) id=getInferredPdbID();
        if (id==null) id=gcps(KEY_SUGGEST_PDB_ID, this);
        return id;
    }

    public synchronized boolean hasPdbId(String s) {
        if (s==null) return false;
        final char c=pdbChain(s);
        return strEquls(STRSTR_IC,pdbID(s),getPdbID(),4) && (c==0 || getChainsAsString().indexOf(c)>=0);
    }
    public synchronized void setPdbID(String s) {
        addSequenceRef(_pdbId=s==null?null:addPfx("PDB:",s).intern());
    }
    public synchronized String getPdbRef() {
        if (getPdbID()!=null) return getPdbID();
        final String acc=getAccessionID();
        if (acc!=null && acc.startsWith("PDB:")) return acc;
        for(String s: getSequenceRefs()) if (s.startsWith("PDB:")) return s;
        return null;
    }
    public String getAccessionID() { return _accID; }
    public synchronized void setAccessionID(String s0) {
        if (sze(s0)==0) return;
        String s=rplcSbyU(s0);
        if (s0.indexOf(':')<0) {
            final String db=guessDbFromId(s, containsOnlyACTGNX());
            if (sze(db)>0) s=db+s;
        }
        addSequenceRef(_accID=s);
        addSequenceRef(s);
        if (s!=null && s.startsWith("UNIPROT:")) setUniprotID(s);
    }
    public synchronized String[] getSequenceRefs() { return strgArry(_vAllIds); }
    public synchronized String[] getDatabaseRefs() { return strgArry(_vAllDbRefs); }
    public synchronized void removeAllSequenceRefs() { clr(_vAllIds); }
    public synchronized void removeSequenceRef(String s) { remov(s,_vAllIds); }
    public synchronized void addSequenceRef(String s0) {
        if (s0==null) return;
        final String s=rplcSbyU(s0);
        if (_vAllIds==null) _vAllIds=new UniqueList(String.class);
        if (!_vAllIds.contains(s)) _vAllIds.add(s.intern());
    }
    public synchronized void addDatabaseRef(String s) {
        if (s==null) return;
        if (_vAllDbRefs==null) _vAllDbRefs=new UniqueList(String.class);
        else if (!_vAllDbRefs.contains(s)) _vAllDbRefs.add(s.intern());
    }
    /* <<< IDs <<< */
     /* ---------------------------------------- */
    /* >>>  Protein Name and URL >>> */
    private String _name, _name0, _origName;
    private int _nameHC;
    private java.net.URL _url;

    public Protein setName(String n) {
        _name=n.substring(maxi(n.lastIndexOf('/'),n.lastIndexOf('\\'))+1);
        _nameHC=0;
        if (_name0==null) _name0=_name;
        incrementMC(MC_PROTEIN_LABELS,this);
        return this;
    }
    public synchronized String getName0() {
        final String n=_name0;
        return n!=null ? n : getName();
    }
    public synchronized int getNameHC() {
        if (_nameHC==0) _nameHC=getName().hashCode();
        return _nameHC;
    }

    /** A protein should have a name, usually the file-name */
    public synchronized String getName() {
        final String n=_name;
        return n!=null ? n : UNNAMED;
    }
    @Override public synchronized String toString() { return getName();}

    public static String urlCd(boolean encode, CharSequence text, int from) {
        CharSequence cs=text;
        if (encode) {
            cs=strplc(0L,",", "%2C",cs, from, MAX_INT);
            cs=strplc(0L,":", "%3A",cs, from, MAX_INT);
        } else {
            cs=strplc(0L,"%2C", ",",cs, from, MAX_INT);
            cs=strplc(0L,"%3A", ":",cs, from, MAX_INT);
        }
        return toStrg(cs);
    }

    public void setURL(java.net.URL u) { _url=u; }
    public java.net.URL getURL() { return _url;}
    private final String[] name2base_ext() {
        final String name=getName(), sfx=getSfx(name,'.'), base=delLstCmpnt(name,'.');
        boolean success=false;
        if (sze(sfx)>0) {
            success=2*sze(sfx)<sze(base);
            for(String s : custSettings(Customize.proteinFileExtensions)) success=success || chrAt(strstr(STRSTR_AFTER,sfx,s),s)==' ';
        }
        return  new String[]{success ? base:name,success?sfx:""};
    }
    public String getOriginalName() { return _origName;}
    public void setOriginalName(String s) {  _origName=s;}

    /* <<< Protein Name and URL <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */
    private String _filePath,_fileName;
    private File _file, _fileWithAllChains, _fileWithSideChains, _fByHomol;
    private boolean _sideChainsFromPDB, _loadedFrom3DFile;
    private int _fileNameHC;
    public synchronized File getFile() { return _file;   }
    public String getFilePath() {
        return _filePath;
    }
    public String getFileName() { return _fileName;}
    public synchronized int getFileNameHC() { return _fileNameHC;}
    public synchronized Protein setFile(final File f) {
        MC[MC_FILE_CONTENT]=0;
        setViewFileWith(_file=f,'e');
        if (f!=null) {
            try{ _filePath=f.getCanonicalPath(); }catch(Exception ex) {stckTrc(ex);}
            _fileNameHC=(_fileName=f.getName()).hashCode();
        } else {
            _fileName=_filePath=null;
            _fileNameHC=0;
        }

        _filePath=null;
        if (_name==null && f!=null) { _name=f.getName(); _nameHC=0; }
        incrementMC(MC_PROTEIN_LABELS,this);
        return this;
    }

    public void setFileWithAllChains(File f) { setViewFileWith(_fileWithAllChains=f,'e'); }
    public synchronized void setFileWithSideChainsObtainableFromPDB(boolean b) { _sideChainsFromPDB=b;}
    public void setFileWithSideChains(File f) {  setViewFileWith(_fileWithSideChains=f,'e'); }
    public File getFileMayBeWithSideChains() { /* no synchronized */
        if (_fileWithSideChains!=null) return _fileWithSideChains;
        if (_fileHasSS || getResidueCalpha()==null || !_sideChainsFromPDB) return getFile();
        return downloadPdbChain(getPdbID(), chrAt(0,getChainsAsString()));
    }
    public synchronized File getFileWithAllChains() { // !!!!!!!!!!!!!!!!!
        if (_fileWithAllChains==null) {
            final File pf=getFile();
            final String n=pf!=null?pf.getName():null;
            final int L=sze(n);
            if (chrAt(L-6,n)=='_' && (n.endsWith(".pdb")||n.endsWith(".ent")))  {
                final File fAllChains=file(pf.getParent()+"/"+n.substring(0,L-6)+n.substring(L-4));
                if (sze(fAllChains)>0) return fAllChains;
            }
        }
        return _fileWithAllChains;
    }
    private boolean _fileHasSS;

    public void setFileHasSidechainAtoms(boolean b) { _fileHasSS=b;}

    public final static int m1stIdx=1, mANNO=2, mASSOCPDB=3, mDNA=4, mGAPS=5, mICON=6, mBALLON=7, mTRANS=8, FILE_TYPE_MAX=8;

    final static File _dirs[]=new File[2*FILE_TYPE_MAX];
    public static File strapFile(int t, Object protein, File dir0) {
        final boolean wm=0!=(optionsG()&G_OPT_VIEWER);
        final File dirW=dirWorking(), dirDnd=dirDndData(), dirA=dirStrapAnno(), dir=dir0!=null?dir0:wm?dirA:dirW;
        final Protein p=deref(protein,Protein.class);
        final String subdir=t==mGAPS || t==mTRANS || t==mDNA ? "gaps" : DIR_ANNOTATIONS;
        if (protein==null) {
            final File d;
            if (dir==dirW || dir==dirDnd) {
                final int idx=(t==mGAPS || t==mTRANS || t==mDNA ? 2 : 0) + (dir==dirDnd?1:0);
                if (_dirs[idx]==null) _dirs[idx]=file(dir,subdir);
                d=_dirs[idx];
            } else d=file(dir,subdir);
            mkdrsErr(d);
            return d;
        }

        final String
            acc=p!=null && dir==dirA ? rplcToStrg(":", "_", p.getAccessionID()) : null,
            name=acc!=null ? acc : t==m1stIdx ? delLstCmpnt(toStrg(protein),'!') : toStrg(protein),
            pn=urlCd(true,name,0),
            e=
            t==m1stIdx   ? ".1stIdx" :
            t==mANNO     ? ".sors" :
            t==mASSOCPDB ? ".get3dFrom" :
            t==mDNA      ? ".dna" :
            t==mGAPS     ? ".gaps" :
            t==mICON     ? ".icon" :
            t==mBALLON   ? ".balloon" :
            t==mTRANS    ? ".3Dtrans" :
            null;
        if (pn==null) {
            final File f=file(dir,subdir);
            mkdrsErr(f);
            return f;
        }
        return file(dir,subdir+"/"+pn+e);
    }

    private boolean _msf;
    public void setIsInMsfFile(boolean b) {  _msf=b;}
    public boolean isInMsfFile() { return _msf;}
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> One Letter code >>> */
    private final static String THREE_LETTER_CODES[]={
        "ALA","CYH","CYS","ASP","GLU","PHE","GLY","HIS","ILE","XXX","LYS","LEU","MET","ASN","HOH","PRO","GLN","ARG","SER","THR","XXX","VAL","TRP","XXX","TYR","err"};
    /** F ==> PHE */

    public static String toThreeLetterCode(int i) {
        final int idx=(i|32)-'a';
        return idx>=0 && idx<'z'-'a' ? THREE_LETTER_CODES[ idx] : "XXX";
    }
    private final static byte _lys2k[][]=new byte[128*128][];
    static {
        for(int i=0;i<THREE_LETTER_CODES.length;i++) addThreeLetterCode(THREE_LETTER_CODES[i],(char)('A'+i));
        addThreeLetterCode("MSE",'M');
        addThreeLetterCode("TRQ",'W'); // pdb1mda
        addThreeLetterCode("SEP",'S'); // pdb1f34
        addThreeLetterCode("B2A",'A'); // ALANINE BORONIC ACID
        addThreeLetterCode("B2F",'F'); // PHENYLALANINE BORONIC ACID
        addThreeLetterCode("B2I",'I'); // ISOLEUCINE BORONIC ACID
        addThreeLetterCode("B2V",'V'); // VALINE BORONIC ACID
        addThreeLetterCode("BAL",'A'); // BETA-ALANINE
        addThreeLetterCode("APE",'F'); // PHENYLALANYL AMIDE
        // addThreeLetterCode("CSO",'C'); // S-HYDROXYCYSTEINE
        // addThreeLetterCode("MIS",'S'); // MONOISOPROPYLPHOSPHORYLSERINE

    }

    // MODRES 1GNV MIS A  221  SER  MONOISOPROPYLPHOSPHORYLSERINE

    public static void addThreeLetterCode(String s, char ch) {
        if (sze(s)!=3) assrt();
        final int
            i=(s.charAt(0)&95),
            j=(s.charAt(1)&95),
            k=(s.charAt(2)&95);
        for(int L=4; --L>=0;) {
            final int L0=(L&1)!=0 ? 32 : 0,  L1=(L&2)!=0 ? 32 :0,  idx= (i|L0) + ((j|L1)<<7);
            byte bb[]=_lys2k[idx];
            if (bb==null) Arrays.fill(bb=_lys2k[idx]=new byte[256], (byte)'X');
            bb[k|32]=bb[k&~32]=(byte)ch;
        }
    }

    /**  Three-letter- to one-letter-code.  E.g. toOneLetterCode('T','R','P') returns 'y'  */
    public static byte toOneLetterCode(int c0,int c1, int c2) {
        final byte[] bb=_lys2k[(c0&127)+((c1&127)<<7)];
        return bb!=null ? bb[c2&127] : 0;
    }
    /** Equivalent to toOneLetterCode(n&255,(n>>>8)&255, (n>>>16)&255);  */
    public static byte toOneLetterCode(int name32) {
        final byte[] bb=_lys2k[(name32&127)+(((name32>>8)&127)<<7)];
        return bb!=null ? bb[(name32>>>16)&127] : 0;
    }
    /* <<< One Letter code <<< */
    /* ---------------------------------------- */
    /* >>> Structure >>> */

    public String pdbFileName(boolean res[], long writerOptions) {
        return new BA(99).a(this)
            .replaceChar('.','_')
            .replaceChar('-','_')
            .a('_').aHex(hashCodeCalpha(), 8)
            .boolToText(res,1, "_", "-")
            .a(".pdb").toString();
    }

    private File _pdbFile;
    public File writePdbFile(long writerOptions) { return writePdbFile((boolean[])null, writerOptions); }
    public File writePdbFile(boolean res[], long writerOptions) {
        synchronized(REF_PDB) {
            final BA ba=baSoftClr(0,REF_PDB);
            final String fn=pdbFileName(res,writerOptions);
            File f=_pdbFile;
            if (sze(f)==0 || !fn.equals(f.getName())) {
                f=_pdbFile=file(STRAPTMP+"/pdb/"+fn);
                delFileOnExit(f);
                final ProteinWriter1 pw=new ProteinWriter1();
                pw.selectResidues(res);
                if ( (writerOptions&ProteinWriter.SIDE_CHAIN_ATOMS)!=0) loadSideChainAtoms(null);
                long opt=writerOptions|ProteinWriter.PDB|ProteinWriter.ATOM_LINES|ProteinWriter.MET_INSTEAD_OF_MSE;
                if (0==(opt&(ProteinWriter.CHAIN_B|ProteinWriter.CHAIN_SPACE))) opt|=ProteinWriter.CHAIN_A;
                pw.toText(this,null, opt,ba);
                wrte(f,ba);
            }
            return _pdbFile;
        }
    }
    private long _hcXYZmc, _hcXYZ;
    public synchronized long hashCodeCalpha() {
        final float xyz[]=getResidueCalpha(null);
        final long mc=mcSum(MC_CALPHA_ORIG,MC_SUBSET,MC_ATOM_COORD_ORIG);
        if (_hcXYZ==0 || _hcXYZmc!=mc) {
            _hcXYZmc=mc;
            _hcXYZ=hashCdFloats(xyz, MAX_INT);
        }
        return _hcXYZ;
    }

    private String _onlyChains;
    private float _resolution=Float.NaN;
    public void setOnlyChains(String s){ _onlyChains=toStrgIntrn(s); }
    public String getOnlyChains() { return _onlyChains;}
    public float getResolutionAngstroms() { return _resolution;}
    public void  setResolutionAngstroms(float r) { _resolution=r;}

    /** set the array of untransformed calpha-coordinates {x0,y0,z0, x1,y1,z1, x2,y2,z2 ...} */
    public synchronized void setResidueCalphaOriginal(float[] xyz) {
        _cAlphaCoordOriginal=xyz;
        incrementMC(MC_CALPHA_ORIG,this);
        clearCache();
    }
    public synchronized float[] getResidueCalphaOriginalFullLength() { return _cAlphaCoordOriginal;}
    /** Are  calpha-coordinates known ? */

    public synchronized int countCalpha() {
        int count=0;
        final float xyz[]=getResidueCalpha();
        for(int i=mini(countResidues(), sze(xyz)/3); --i>=0;) {
            if (!Float.isNaN(xyz[3*i])) count++;
        }
        return count;
    }
    public synchronized byte[] getResidueTypeLowerCaseMeansNo3dCoordinates() {
        final Object[] cached=cached();
        final float ca[]=getResidueCalpha(null);
        final byte[] rt=getResidueType();
        final int nRes=mini(rt.length,countResidues());
        byte[] lc=(byte[])cached[RES_HAS_CA];
        if (lc==null || cacheEmpty(cached, RES_HAS_CA, mcSum(MC_CALPHA_ORIG,MC_RESIDUE_TYPE))) {
            cached[RES_HAS_CA]=lc=redim(lc,nRes,0);
            final int ca3=sze(ca)/3;
            for(int i=nRes;--i>=0;) {
                lc[i]=(byte) (i>=ca3 || Float.isNaN(ca[3*i])||Float.isNaN(ca[3*i+1])||Float.isNaN(ca[3*i+2]) ? (32|rt[i]) : (rt[i]&~32));
            }
        }
        return lc;
    }
    /**
       Get transformed  calpha-coordinates [Angstrom]
       @return  {x0,y0,z0, x1,y1,z1, x2,y2,z2 ...}
    */
    public synchronized float[] getResidueCalpha() {
        final Object[] cached=cached();
        final float caOri[]=getResidueCalpha(null);
        final Matrix3D t=getRotationAndTranslation();
        if (caOri==null || t==null) return caOri;
        final int nR=countResidues(), mc=mcSum(MC_CALPHA_ORIG,MC_MATRIX3D,MC_SUBSET);
        float[] ff=(float[]) cached[C_COORD];
        if (ff==null || setMC(MC_CALPHA, mc)) {
            cached[C_COORD]=ff=new float[mini(caOri.length,nR*3)];
            t.transformPoints(caOri, (float[]) ff, nR);
        }
        return ff;
    }
    /**
       @return The C-alpha coordinates
       @param m3d The coordinate system
       If m3d==null then the coriginal coordinates as recorded in the PDB file are returned.
       p.getResidueCalpha(p.getRotationAndTranslation()) is identical to p.getResidueCalpha()
    */
    public synchronized float[] getResidueCalpha(Matrix3D m3d) {
        final Object[] cached=cached();
        final float caOri[]=!isResSubset() ? getResidueCalphaOriginalFullLength() : (float[])computeSubset()[C_COORD_ORIG];
        if (caOri==null || m3d==null) return caOri;
        if (m3d==_m3d) return getResidueCalpha();
        final int nRes=countResidues();
        float[] ff=(float[]) cached[C_COORD_PREV];
        if (ff==null || _m3dPreview!=m3d || cacheEmpty(cached, C_COORD_PREV, mc(MC_CALPHA_ORIG)+m3d.mc())) {
            cached[C_COORD_PREV]=ff=redim(ff, mini(caOri.length,nRes*3),0);
            (_m3dPreview=m3d).transformPoints(caOri, ff ,nRes);
        }
        return ff;
    }
    public synchronized float[] getAtomCoordinates(Matrix3D m3d) {
        final Object[] cached=cached();
        final float ori[]=!isResSubset() ? getAtomCoordOriginalFullLength() : (float[])computeSubset()[A_COORD_O];
        if (ori==null || m3d==null) return ori;
        if (m3d==_m3d) return getAtomCoordinates();
        float[] ff=(float[])cached[A_COORD_PV];
        if (ff==null || _m3dPreview!=m3d || cacheEmpty(cached, A_COORD_PV, _subsetMC+mc(MC_ATOM_COORD_ORIG))) {
            cached[A_COORD_PV]=ff=redim(ff,ori.length,0);
            (_m3dPreview=m3d).transformPoints(ori, ff, MAX_INT);
        }
        return ff;
    }
    /**
       Get the rotation - translation matrix.
       The methods <i>JAVADOC:Protein#getAtomCoordinates()</i> and <i>JAVADOC:Protein#getResidueCalpha()</i> return transformed coordinates.
    */
    public synchronized Matrix3D getRotationAndTranslation() { return _m3d;}
    /** see <i>JAVADOC:Protein#getRotationAndTranslation()</i> */
    public synchronized void setRotationAndTranslation(Matrix3D m3d) {
        if (_m3d!=m3d) {
            incrementMC(MC_MATRIX3D,this);
            _m3d=m3d;
        }
    }
    private Matrix3D _m3d, _m3dPreview;
    public UniqueList<ProteinViewer> _v3d;
    public synchronized ProteinViewer[] getProteinViewers() {
        if (_v3d==null) return ProteinViewer.NONE;
        for(ProteinViewer v : _v3d.asArray()) {
            if (v==null || v.getProtein()==null) remov(v,_v3d);
        }
        return _v3d.asArray();
    }
    public synchronized void addProteinViewer(ProteinViewer v) {
        if (_v3d==null) (_v3d=new UniqueList(ProteinViewer.class).t("protein viewers")).addHook(UniqueList.HOOK_CHANGE,this,"V3D_CHANGED");
        if (_v3d.add(v)) incrementMC(MC_PROTEIN_VIEWERS_V,this);
        if (AbstractProxy.getProxy(v,ProteinViewer.class)!=null) assrt();
    }
    public synchronized void removeProteinViewer(ProteinViewer v) {
        if(remov(v,_v3d)) incrementMC(MC_PROTEIN_VIEWERS_V,this);
    }
    public synchronized CharSequence pdbNumberToIdx1(CharSequence expression) {
        return pdbNumberToIdx(expression, 1+getResidueIndexOffset());
    }

    private CharSequence pdbNumberToIdx(CharSequence expression, int offset) {
        if (strchr(':',expression)<0) return expression;
        final BA sb=toBA(expression);
        for(int colon=MAX_INT; (colon=strchr(STRSTR_PREV,':', sb, colon-1, 0))>=0; ) {
            final char chain=is(LETTR_DIGT, sb,colon+1) ? sb.charAt(colon+1) : 0;
            for(int digit=colon; --digit>=0;) {
                final char cDigit=sb.charAt(digit);
                if (cDigit==':' || is(SPC, cDigit)) break;
                if ('0'<=cDigit && cDigit<='9' && !is(DIGT, sb, digit-1) && chrAt(digit-1,sb)!=':') {
                    final int noDigt=nxtE(-DIGT, sb, digit, colon);
                    final int num=atoi(sb,digit);
                    final char ins=is(LETTR, sb, noDigt) ? sb.charAt(noDigt) : 0;
                    final int idx=resNumAndChain2resIdxZ(true, num,chain,ins);
                    final int exprEnd=noDigt+ (noDigt!=colon?0:chain==0?1:2);
                    if (idx<0) sb.replaceRange(digit,exprEnd, "NaN");
                    else sb.replaceRange(digit,exprEnd, "").insertI(digit,idx+offset);
                }
            }
        }
        return sb;
    }
    // ----------------------------------------
    /**
       Converts  String denominating a subset of residues
       into a boolean array
       E.g.  "1-3,5-6" ==>  XXX.XXX....
    */
    public synchronized boolean[] residueSubsetAsBool(CharSequence expr, int returnMinIdx[]) {
        return expr==null ? null : parseSet(pdbNumberToIdx1(expr), getCharacters().length,  returnMinIdx);
    }
    public synchronized boolean[] residueSubsetAsBooleanZ(CharSequence expr) {
        return expr==null ? null : parseSet(pdbNumberToIdx1(expr), getCharacters().length,  -1-getResidueIndexOffset());
    }
    // ----------------------------------------
    /**
       Given a pdb Number (2nd column in pdb files)
       What is the residue index ?
    */
    public synchronized int atomNumber2residueIdx(int pdbAtomNum) {
        final int strapAtomNum[]=getResidueAtomNumber();
        if (strapAtomNum==null) return -1;
        int minIdxDif=999999,aaIdx=-1;
        for(int iAa=strapAtomNum.length;--iAa>=0;) {
            final int san=strapAtomNum[iAa];
            if (san>pdbAtomNum) continue;
            if (minIdxDif>pdbAtomNum-san) {
                minIdxDif=pdbAtomNum-san;
                aaIdx=iAa;
            }
        }
        return aaIdx;
    }
    // ----------------------------------------
    /**
       Given a residue number and a chain denominator
       What is the residue index ?
    */
    public synchronized int resNumAndChain2resIdxZ(boolean fullLength, int resNum,char chain, char insCode) {
        final int rn[]=fullLength ? getResidueNumberFullLength() : getResidueNumber();
        final byte rc[]=fullLength ? getResidueChainFullLength() : getResidueChain();
        final byte ins[]=fullLength ? getResidueInsertionCodeFullLength() : getResidueInsertionCode();
        final int nR=fullLength ? getResidueTypeFullLength().length : countResidues();
        return indexOfNumChainInscode(nR, rn,rc,ins, resNum, chain,insCode);
    }
    private static int indexOfNumChainInscode(int countResidues, int rn[], byte rc[], byte ins[], int resNum,char chain0, char insCode) {
        if (rn==null) return -1;
        final char chain=chain0=='?' ?  0 : chain0=='_' ? ' ' : chain0;
        final int idxTo=mini(countResidues,sze(rn));
        for(int i=0; i<idxTo; i++) {
            if (rn[i]==resNum) {
                final byte chain_i=rc!=null && i<rc.length ? rc[i] : (byte)' ';
                final byte ins_i=ins!=null &&  i<ins.length ? ins[i] : 0;
                if ((ins_i==insCode||insCode==0) && (chain==0 || chain_i==chain))  return i;
            }
        }
        return -1;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> HasMap >>> */
    public final static Object KEY_BA_PARSE=new Object(), KEY_GAPS=new Object();

    public final Object WEAK_REF=newWeakRef(this);
    public Object wRef() { return WEAK_REF;}
    private Map _clientObjects;
    public Map map(boolean create) { return _clientObjects==null&&create?_clientObjects=new HashMap() : _clientObjects;}
    /**   See equivalent method in JComponent.    */
    public void putClientProperty(Object key, Object value) {
        pcp(key,value,this);
        incrementMC(MC_PROTEIN_USER_OBJECTS_V,this);
    }
    /**   See equivalent method in JComponent.    */
    public final Object getClientProperty(Object o) { return  gcp(o,this);}
    /* <<< HasMap <<< */
    /* ---------------------------------------- */
    /* >>> first and last index >>> */
    private int _idxOffset;
    public int getResidueIndexOffset() { return _idxOffset; }
    public synchronized void setResidueIndexOffset(int first) {
        if (_idxOffset!=first) {
            _idxOffset=first;
            clearCache();
            incrementMC(MC_1ST_RES_IDX,this);
        }
    }
    /* This is the displayed index */
    public synchronized int[] getFirstAndLastResidueIndex() {
        final Object[] cached=cached();
        final int N=getResidueTypeFullLength().length;
        final int i0=getResidueIndexOffset();
        final boolean[] bb=getResidueSubsetB();
        int[] fl=(int[])fromCache(cached, FIRST_LAST_IDX, 0);
        if (fl==null) {
            cached[FIRST_LAST_IDX]=fl=new int[2];
            if (bb!=null) {
                fl[0]=i0+fstTrue(bb);
                fl[1]=i0+lstTrue(bb);
            } else {
                fl[0]=i0;
                fl[1]=i0+N-1;
            }
        }
        return fl;
    }
    public static int firstResIdx(Protein p) { return p==null?0:p.getFirstAndLastResidueIndex()[0]; }

    /* <<< first and last index <<< */
    /* ---------------------------------------- */
    /* >>> Residue Subset >>> */
    private Object _resSubset;
    /**
       residueSubset("10-20,40-50") will return
       a new protein instance with residues from 10-20 and 40-50
       In PDB-files the pdb-residue-number can be referred to:
       10:-20:, 40:-50:
       One can add a chain identifier like 10:A-20:A, 40:A-50:A
       Note: counting of residues starts here at 1 and not 0 !!
    */
    public synchronized void setResidueSubset(String expression) { _setRS(expression); }
    public synchronized void setResidueSubset(boolean[] b) { _setRS(b); }
    private void _setRS(Object o) {
        _resSubset=o;
        clearCache();
        incrementMC(MC_SUBSET,this);
    }
    private boolean isResSubset() { return _resSubset!=null;}

    /**  Index of the first true in getResidueSubsetB(); */
    public synchronized int getResidueSubsetB1() { return getFirstAndLastResidueIndex()[0]-getResidueIndexOffset(); }

    public synchronized boolean[] getResidueSubsetB() {
        final Object expr=_resSubset;
        if (expr==null)  return null;
        final Object[] cached=cached();
        boolean bb[]=(boolean[])fromCache(cached, RES_SUBSET,0);
        if (bb==null) {
            bb= expr instanceof boolean[] ?(boolean[])expr :
                parseSet(pdbNumberToIdx1((String)expr), getCharacters().length,-getResidueIndexOffset()-1);
            if (_resSubset==null) _resSubset=new int[2];
            final int t0=fstTrue(bb);
            if (bb==null||t0<0) bb=ERR_BOOLEAN;
            cached[RES_SUBSET]=bb;

        }
        return bb==ERR_BOOLEAN?null:bb;
    }
    private Object[] computeSubset() {
        final Object[] c=cached();
        final byte[] fullLength=getResidueTypeFullLength();
        final int mc=mcSum(MC_SUBSET, MC_1ST_RES_IDX, MC_RESIDUE_TYPE_FULL);
        if (c[RES_T]==null || _subsetMC!=mc) {
            _subsetMC=mc;
            final int nR=fullLength.length;
            final boolean bb[]=getResidueSubsetB();
            final int countTrue=countTrue(bb,0,nR);
            c[RES_T]=subset(fullLength,bb,countTrue);
            c[RES_SS]=subset(getResidueSecStrTypeFullLenght(),bb,countTrue);
            c[RES_INS_CODE]=subset(getResidueInsertionCodeFullLength(),bb,countTrue);
            c[RES_CHAIN]=subset(getResidueChainFullLength(),bb,countTrue);
            c[RES_T32]=subset(getResidueName32FullLength(),bb,countTrue);
            c[RES_ACC]=subset(getResidueSolventAccessibilityFullLength(),bb,countTrue);
            c[RES_ATOM_NUM]=subset(getResidueAtomNumberFullLength(),bb,countTrue);
            c[RES_NUM]=subset(getResidueNumberFullLength(),bb,countTrue);
            final float coord[]=getAtomCoordOriginalFullLength();
            final float residueCalpha[]=getResidueCalphaOriginalFullLength();
            c[C_COORD_ORIG]=subset3(residueCalpha,bb,countTrue);
            final int
                atomName32[]=getAtomName32FullLength(),
                atomNumber[]=getAtomNumberFullLength(),
                firstIdx[]=getResidueFirstAtomIdxFullLength(),
                lastIdx[]=getResidueLastAtomIdxFullLength();
            final float[]
                atomOccupancy=getAtomOccupancyFullLength(),
                atomBFactor=getAtomBFactorFullLength(),
                residuePsi=getResidueAnglePsiFullLength(),
                residuePhi=getResidueAnglePhiFullLength();
            final byte[] atomType=getAtomTypeFullLength();
            if (coord!=null && firstIdx!=null && lastIdx!=null && c[A_COORD_O]==null&& bb!=null&&residueCalpha!=null) {
                int newAtomCount=0;
                final int max=mini( mini(nR,firstIdx.length,lastIdx.length),bb.length,residueCalpha.length/3);
                final int newFirstIdx[]=new int[countTrue], newLastIdx[]=new int[countTrue];
                for(int i=0,newIdx=0; i<max;i++)
                    if (bb[i]) {
                        if (firstIdx[i]<0) {
                            newFirstIdx[newIdx]=newLastIdx[newIdx]=-1;
                        }
                        else {
                            newFirstIdx[newIdx]=newAtomCount;
                            newAtomCount+=lastIdx[i]-firstIdx[i]+1;
                            newLastIdx[newIdx]=newAtomCount-1;
                        }
                        newIdx++;
                    }
                final float newCoord[]=new float[newAtomCount*3];
                c[A_NUM]=atomNumber!=null ? new int[newAtomCount] : null;
                c[A_NAME_S]=atomName32!=null ? new int[newAtomCount] :null;
                c[A_TYPE]=atomType!=null ? new byte[newAtomCount] : null;
                c[A_BF]=atomBFactor!=null ? new float[newAtomCount] : null;
                c[A_OCCUP]=atomOccupancy!=null ? new float[newAtomCount] : null;
                c[RES_PSI]=residuePsi!=null ? new float[newAtomCount] : null;
                c[RES_PHI]=residuePhi!=null ? new float[newAtomCount] : null;
                for(int i=0,newIdx=0; i<max;i++)
                    if (bb[i]) {
                        final int newF=newFirstIdx[newIdx],newL=newLastIdx[newIdx],first=firstIdx[i],n=newL-newF+1;
                        if (first>=0 &&  newF>=0) {
                            System.arraycopy(coord, first*3, newCoord,newF*3,3*n);
                            if (atomNumber!=null)    System.arraycopy(atomNumber,   first, c[A_NUM],newF,n);
                            if (atomName32!=null)    System.arraycopy(atomName32,   first, c[A_NAME_S],newF,n);
                            if (atomType!=null)      System.arraycopy(atomType,     first, c[A_TYPE],newF,n);
                            if (atomType!=null)      System.arraycopy(atomType,     first, c[A_TYPE],newF,n);
                            if (atomBFactor!=null)   System.arraycopy(atomBFactor,  first, c[A_BF],newF,n);
                            if (atomOccupancy!=null) System.arraycopy(atomOccupancy,first, c[A_OCCUP],newF,n);
                            if (residuePsi!=null)    System.arraycopy(residuePsi,   first, c[RES_PSI],newF,n);
                            if (residuePhi!=null)    System.arraycopy(residuePhi,   first, c[RES_PHI],newF,n);
                        }
                        newIdx++;
                    }
                c[RES_FIRST_Ai]=newFirstIdx;
                c[RES_LAST_Ai]=newLastIdx;
                c[A_COORD_O]=newCoord;
            }
        }
        return c;
    }
    private static float[] subset3(float[] in,boolean onlyTrue[],int countTrue) {
        if (in==null || onlyTrue==null) return in;
        final float out[]=new float[countTrue*3];
        final int n=mini(in.length/3,onlyTrue.length);
        for(int i=0,i3=0,count=0; i<n; i++,i3+=3)
            if (onlyTrue[i]) {
                if (count>=out.length) break;
                out[count]=in[i3];
                out[count+1]=in[i3+1];
                out[count+2]=in[i3+2];
                count+=3;
            }
        return out;
    }

    private static int[] subset(int[] in, boolean onlyTrue[],int countTrue) {
        if (in==null || onlyTrue==null) return in;
        final int out[]=new int[countTrue];
        final int n=mini(in.length,onlyTrue.length);
        for(int i=0,count=0; i<n; i++)
            if (onlyTrue[i]) {
                if (count>=countTrue) break;
                out[count++]=in[i];
            }
        return out;
    }

    private static byte[] subset(byte[] in,boolean onlyTrue[],int countTrue) {
        if (in==null || onlyTrue==null) return in;
        final byte out[]=new byte[countTrue];
        final int n=mini(in.length,onlyTrue.length);
        int count=0;
        for(int i=0; i<n; i++) {
            if (onlyTrue[i]) {
                if (count>=countTrue) break;
                out[count++]=in[i];
            }
        }
        return out;
    }

    private static float[] subset(float[] in,boolean onlyTrue[],int countTrue) {
        if (in==null || onlyTrue==null) return in;
        final float out[]=new float[countTrue];
        final int n=mini(in.length,onlyTrue.length);
        for(int i=0,count=0; i<n; i++)
            if (onlyTrue[i]) {
                if (count>=countTrue) break;
                out[count++]=in[i];
            }
        return out;
    }
    private static int[] fullInt(int[] in,boolean subset[], int defaulT) {
        if (in==null || subset==null) return in;
        final int out[]=new int[subset.length*3];
        int count=0;
        for(int i=0; i<subset.length && count<in.length; i++) {
            out[i]= subset[i] ? in[count++] : defaulT;
        }
        return out;
    }
    private static byte[] fullByte(byte[] in,boolean subset[]) {
        if (in==null || subset==null) return in;
        final byte out[]=new byte[subset.length*3];
        int count=0;
        for(int i=0; i<subset.length && count<in.length; i++) {
            if (subset[i]) out[i]=in[count++];
        }
        return out;
    }
    private static float[] fullFloat3(float[] in,boolean subset[]) {
        if (in==null || subset==null) return in;
        final float out[]=new float[subset.length*3];
        int count3=0;
        for(int i=0, i3=0; i<subset.length && count3+2<in.length; i++,i3+=3) {
            if (subset[i]) {
                out[i3]=in[count3];
                out[i3+1]=in[count3+1];
                out[i3+2]=in[count3+2];
                count3+=3;
            } else {
                out[i3]=out[i3+1]=out[i3+2]=Float.NaN;
            }
        }
        return out;
    }

     public synchronized boolean[] fromFullLengthB(boolean[] sel) {
        final boolean[] subset=getResidueSubsetB();
        if (subset==null || sel==null) return sel;
        final boolean bb[]=new boolean[mini(sel.length, subset.length)];
        int count=0;
        for(int i=0; i<bb.length;i++) {
            if (subset[i]) bb[count++]=sel[i];
        }
        return bb;
    }

    public synchronized boolean[] toFullLengthB(boolean[] sel) {
        final boolean[] subset=getResidueSubsetB();
        if (subset==null || sel==null) return sel;
        final int len=getCharacters().length;
        if (countTrue(subset,0, len)==len) return sel;
        final int L=mini(len,lstTrue(subset)+1);
        final boolean selFL[]=new boolean[L];
        for(int iL=0, k=0; iL<L; iL++) {
            if (subset[iL]) {
                if (k>=sel.length) break;
                selFL[iL]=sel[k];
                k++;
            }
        }
        return selFL;
    }
    /* <<< Subset <<< */
    /* ---------------------------------------- */
    /* >>> helper function >>> */
    private WeakHashMap _cacheTMalign;
    public WeakHashMap<Protein,Superimpose3D.Result> hashTMalign() {
        if (_cacheTMalign==null) _cacheTMalign=new WeakHashMap();
        return _cacheTMalign;
    }
    /* <<<   <<< */
    /* ---------------------------------------- */
    /* >>> Infer PDB >>> */
    private int[] _inferred3dMatch;
    public int[] getInferred3dCountMatches() { return _inferred3dMatch;}
    public float inferCoordinates(Protein pp3d[],Class classAligner, BA log) { /* no synchronized */
        if (pp3d==null) return Float.NaN;
        final byte seq[]=getResidueTypeExactLength();
            Protein bestP=null;
            float bestScore=0;
            byte bestGapped[][]=null;
            for(Protein p3d : pp3d) {
                if (p3d==null) continue;
                final byte[] seq3D=p3d.getResidueTypeExactLength();
                byte[][] aligned=AlignUtils.gappedForPerfectMatch(seq3D,seq);
                float score=Float.NaN;
                if (aligned!=null)  score=100000+seq.length;
                else {
                    SequenceAligner aligner=null;
                    try {
                        if (classAligner!=null) aligner= (SequenceAligner)classAligner.newInstance();
                    } catch(Exception ex) { putln("Protein: Cannot make instance for ",classAligner,ex);}
                    if (aligner!=null) {
                        if (p3d.getResidueCalpha()==null || p3d.getResidueNumber()==null ) continue;
                        aligner.setSequences(seq3D, seq);
                        aligner.compute();
                        aligned=aligner.getAlignedSequences();
                        if (isAssignblFrm(HasScore.class, aligner)) score=((HasScore)aligner).getScore();
                        else {
                            final byte aa[][]=aligner.getAlignedSequences();
                            if (sze(aa)>=2) score=Blosum.pairAignScore(-20f,-5f, aa[0],aa[1], 0, MAX_INT,  0);
                        }

                    }
                }
                if (sze(aligned)==2) {
                    if (score>bestScore) {
                        bestGapped=aligned.clone();
                        bestScore=score;
                        bestP=p3d;
                    }
                }
            }
            if (bestP!=null) {
                final int nR=seq.length;//countResidues();
                final int[] num3d=bestP.getResidueNumber(),     num=new int[nR];
                final int[] nam3d=bestP.getResidueName32(),     nam=new int[nR];
                final int[] anu3d=bestP.getResidueAtomNumber(), anu=new int[nR];
                final float[] alpha3d=bestP.getResidueCalpha(), alpha=new float[nR*3];
                final byte[] ss3d=bestP.getResidueSecStrType(), ss=ss3d!=null ? new byte[nR] : null;
                final byte[] cc3d=bestP.getResidueChain(), cc=new byte[nR];
                final byte[] gapped3D=bestGapped[0], gapped=bestGapped[1];
                Arrays.fill(alpha,Float.NaN);
                Arrays.fill(num,MIN_INT);
                Arrays.fill(nam,0);

                if (isEnabld(log)) {
                    log.a("ClustalW (").a(bestP).a(',').a(this).a(") score=").a(bestScore).a('\n');
                    AlignUtils.aliWithMidline(AlignUtils.PRINT_ALI_TRUNCATE, gapped, MIN_INT, "", gapped3D, MIN_INT, "", 99999, log);
                    log.send();
                }
                final int nR3d=mini(bestP.countResidues(),  alpha3d.length);
                int countMatch=0, countAll=0;
                int iA=idxOfLetters(gapped, seq);
                int iA3d=idxOfLetters(gapped3D, bestP.getResidueTypeExactLength());

                for(int col=0;  iA<nR && iA3d<nR3d && col<gapped.length && col<gapped3D.length;  col++) {
                    final byte g1=gapped[col], g3=gapped3D[col];
                    final boolean isL=is(LETTR,g1), isL3d=is(LETTR,g3);
                    if (isL3d) {
                        if ((g1|32)==(g3|32)) countMatch++;
                        countAll++;
                    }
                    if (isL && isL3d) {
                        num[iA]=num3d[iA3d];
                        nam[iA]=nam3d[iA3d];
                        anu[iA]=anu3d[iA3d];
                        for(int i=3;--i>=0;) alpha[iA*3+i]=alpha3d[iA3d*3+i];
                        if (ss3d!=null && iA3d<ss3d.length) ss[iA]=ss3d[iA3d];
                        if (iA3d<cc3d.length) cc[iA]=cc3d[iA3d];
                    }
                    if (isL) iA++;
                    if (isL3d) iA3d++;
                }
                _inferred3dMatch=new int[]{countMatch,countAll};
                final String pdbId=bestP.getPdbID();
                if (pdbId==null) assrt();
                if (pdbId!=null&&!pdbId.equals(_inferredPDB)) {
                    _inferredPDB=pdbId;
                    incrementMC(MC_INFERRED_PDB,this);
                }
                final boolean subset[]=getResidueSubsetB();
                setResidueChain(fullByte(cc,subset));
                setResidueNumber(fullInt(num,subset,INT_NAN));
                setResidueType32bit(fullInt(nam,subset,0));
                setResidueAtomNumber(fullInt(anu, subset, INT_NAN));
                setResidueCalphaOriginal(fullFloat3(alpha, subset));
                setFileWithSideChains(bestP.getFile());
                if (ss!=null) setResidueSecStrType(fullByte(ss,subset));
                for(HeteroCompound h : getHeteroCompounds('H')) removeHeteroCompound(h);

                addHeteroCompounds(HETERO_ADD_UNIQUE, bestP.getHeteroCompounds('H'));
                inferredPDB_markMatches(true,true);
                return bestScore;
            }
            return Float.NaN;
            //        }
    }
    public synchronized void inferredPDB_markMatches(boolean toLC, boolean selection) {
        final String pfx="Mismatch_with_3D_";
        final int nR=countResidues();
        final byte aa[]=getResidueType();
        for(ResidueSelection s : residueSelections()) {
            if (selection && strEquls(pfx, nam(s))) removeResidueSelection(s);
        }
        if (_inferredPDB!=null) {
            final float cAlpha[]=getResidueCalpha();
            boolean sel[]=null;
            for(int i=mini(aa.length,nR); --i>=0;) {
                final boolean isCalpha=!Float.isNaN(get(3*i, cAlpha));
                final int lc=aa[i]|32;
                if (toLC) aa[i]=(byte)(isCalpha?aa[i]&~32: lc);
                if (selection) {
                    final int aa32=getResidueName32(i), to1=32|toOneLetterCode(aa32>>0, aa32>>8, aa32>>16);
                    if (to1!=lc) (sel==null ? sel=new boolean[i+1]:sel)[i]=true;
                }
            }
            if (fstTrue(sel)>=0) {
                final BasicResidueSelection rs=new BasicResidueSelection(0);
                rs.setColor(C(0xFF0A0A));
                rs.setProtein(this);
                rs.setSelectedAminoacids(sel,getFirstAndLastResidueIndex()[0]);
                rs.setName(pfx+_inferredPDB.replace(':','_'));
                addResidueSelection(rs);
            }
        } else {
            for(int i=mini(aa.length,countResidues()); --i>=0;) aa[i]=aa[i]&=~32;
        }
        //setResidueType(aa);
    }
    public synchronized void detach3DStructure() {
        if (_inferredPDB!=null) {
            _inferredPDB=null;
            setResidueCalphaOriginal(null);
            setResidueSecStrType(null);
            setResidueNumber(null);
            setResidueChain(null);
            inferredPDB_markMatches(true,true);
        }
    }
    public String getInferredPdbID() { return _inferredPDB;}
    /* <<< Infer PDB Id <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private int _paintChainMC, _paintChainX;
    public final static long PAINT_CHAIN_UNDERLINE=1<<1;
    public void paintChain(long options,Graphics g, String T, int x0, int y, int w, int h) {
        final String chains=getChainsAsString();
        if (chains.length()==1) {
            final int L=T.length();
            final char c=chains.charAt(0);
            if (L>6 && T.charAt(L-4)=='.' && T.charAt(L-6)=='_' && T.charAt(L-5)==c) {
                final Font f=g.getFont();
                final int mc=f.hashCode()+T.hashCode();
                if (_paintChainX<=0 || _paintChainMC!=mc) {
                    _paintChainMC=mc;
                    _paintChainX=strgWidth(f,T.substring(0,L-6));
                }
                g.setColor(Protein3d.chain2color(c));
                if (0!=(PAINT_CHAIN_UNDERLINE&options)) g.drawLine(_paintChainX,y+h,_paintChainX+w,y+h);
                else g.fillRect(x0+_paintChainX,y,w,h);
            }
        }
    }

    private Matrix3D _bioMatrices[];
    public synchronized void setBioMatrices(Matrix3D mm[]) { _bioMatrices=mm;}
    public synchronized Matrix3D[] getBioMatrices() { return _bioMatrices!=null ? _bioMatrices : Matrix3D.NONE;}

    private BA _pdbTextSecStru;
    public void setPdbTextSecStru(BA txt) { _pdbTextSecStru=txt; _secStr=null; }
    public BA getPdbTextSecStru() { return _pdbTextSecStru;}

    public static String selectedPositionsToText(ResidueSelection s) { return selectedPositionsToText(s.getSelectedAminoacids(), s.getSelectedAminoacidsOffset(), s.getProtein()); }
    public static String selectedPositionsToText(boolean bb[], int offset,Protein protein) { return selectedPositionsToText(false, bb,offset,protein); }
    public static String selectedPositionsToText(boolean pdbNumbers, boolean bb[], int offset, Protein p) {
        if (bb==null) return "";
        BA sb=null;
        final int[] num=p!=null && pdbNumbers ? p.getResidueNumber() : null;
        final byte[] chain=num==null?null : p.getResidueChain();
        final byte[] insCodes=num==null?null : p.getResidueInsertionCode();
        for(int iB=0; iB<bb.length; iB++) {
            if (!bb[iB]) continue;
            if (sb==null) sb=new BA(99); else sb.a(", ");
            final int iA0=iB+offset;
            while( iB<bb.length && bb[iB]) iB++;
            final int iA1=iB+offset, d=iA1-iA0;
            if (num!=null && iA0>=0 && iA1<=num.length && num[iA0]!=MIN_INT) {
                sb.a(num[iA0]);
                final byte ic0=insCodes!=null && iA0<=insCodes.length ? insCodes[iA0] : 0;
                if (ic0!=0 && ic0!=32) sb.a((char)ic0);
                sb.a(':');
                if (chain!=null && chain.length>iA0) sb.a((char)chain[iA0]);
                if (d>1) {
                    sb.a(d>2 ? '-' : ',').a(num[iA1-1]);
                    final byte ic1=insCodes!=null && iA1-1<=insCodes.length ? insCodes[iA1-1] : 0;
                    if (ic1!=0 && ic1!=32) sb.a((char)ic1);
                    sb.a(':');
                    if (chain!=null && chain.length>=iA1) {
                        final char c=(char)chain[iA1-1];
                        if (c!=0 && c!=' ') sb.a(c);
                    }
                }
            } else {
                sb.a(iA0+1);
                if (d>1) sb.a(d>2 ? '-' : ',').a(iA1);
            }
        }
        return toStrgN(sb);
    }
    private boolean _isWaterInFile;
    protected void setWaterInFile(boolean b) { _isWaterInFile=b;}
    public boolean isWaterInFile() { return _isWaterInFile;}

    private static boolean _shownErrSideC;
    public void loadSideChainAtoms(BA ba) {
        if (getInferredPdbID()!=null) {
            final String error="Warning "+this+": Loading side chain atoms not supported if 3D-coordinates are inferred from another file.";
            if (!_shownErrSideC) error(error);
            _shownErrSideC=true;
            putln(error);
        }
        final int match[]=getInferred3dCountMatches();
        if (match==null || match[0]==match[1]) {
            if (getAtomCoordinates()==null) new PDB_Parser().parse(this,ProteinParser.SIDE_CHAIN_ATOMS,ba!=null ? ba : readBytes(getFileMayBeWithSideChains()));
        }
    }

    public static boolean equalsResidueType(Protein p1, Protein p2) {
        if (p1==null || p2==null) return false;
        final byte[] seq1=p1.getResidueType(), seq2=p2.getResidueType();
        final int n=p1.countResidues();
        if (n!=p2.countResidues() || p1.getResidueTypeHashCode()!=p2.getResidueTypeHashCode()) return false;
        for(int i=n; --i>=0;) if ((32|seq1[i]) != (32|seq2[i])) return false;
        return true;
    }
    public boolean isLoadedFromStructureFile() { return _loadedFrom3DFile; }
    public void setIsLoadedFromStructureFile(boolean b) { _loadedFrom3DFile=b;}
    public static String commonPdbId(Protein pp[]) {
        String pdbId=null;
        int count=0;
        for(int i=sze(pp); --i>=0;) {
            if (pp[i]==null) continue;
            final String s=pp[i].getPdbID();
            if (count++==0) pdbId=s;
            else if (s==null || !s.equals(pdbId)) return null;
        }
        return pdbId;
    }
    /* <<< PDB <<< */
    /* ---------------------------------------- */
    /* >>> Complexes >>> */
    /* E.g. Transducine pdb1scg_B.ent pdb1a0r_G.ent pdb1scg_A.ent pdb1a0r_B.ent pdb1a0r_P.ent pdb1scg_G.ent */
    public static Protein[] proteinsInAlignment(Protein p) {
        final Protein pp[]=p==null?null:(Protein[])runCR(p.getAlignment(),RUN_ALIGNMENT_GET_PROTEINS);
        return pp!=null?pp:NONE;
    }

    private ArrayList _vComplex;
    public Protein[] getProteinsSameComplex() { /* no synchronized */
        final int N=sze(_vComplex);
        if (N>0) {
            final Protein[] pp=new Protein[N];
            int count=0;
            for(int i=0; i<N; i++) {
                final Protein p=get(i,_vComplex,Protein.class);
                if (p!=null && p!=this) pp[count++]=p;
            }
            return chSze(pp,count,Protein.class);
        }
        final Protein[] ppAli=proteinsInAlignment(this);
        final String thisPdb=delLstCmpnt(getPdbID(), '_');
        if (ppAli.length>0 && sze(thisPdb)>0) {
            final Protein[] pp=new Protein[ppAli.length];
            int count=0;
            for(Protein p : ppAli) {
                final String pdb=p==null?null:p.getPdbID();
                if (strEquls(thisPdb,pdb,0) && p!=this) pp[count++]=p;
            }
            return chSze(pp,count,Protein.class);
        }
        return NONE;
    }
    public void setProteinsSameComplex(Protein[] pp) {
        if (_vComplex==null) _vComplex=new ArrayList(); else clr(_vComplex);
        for(Protein p : pp) {
            if (p!=this && p!=null) _vComplex.add(wref(p));
        }
    }
    /* <<< Complexes <<< */
    /* ---------------------------------------- */
    /* >>> Nucleotide >>> */
    private int _ACTGNmc=-1;
    private boolean _actgn=true;

    public final static boolean AMINO_ACIDS=true, NUCLEOTIDES=false;
    public final static int
        NO_TRANSLATE=-1,
        FORWARD=0, REVERSE=1, COMPLEMENT=2, REVERSE_COMPLEMENT=REVERSE|COMPLEMENT;
    private boolean _isTrans[], _cdsApplied;
    private int _readingFrame=NO_TRANSLATE;
    public synchronized void selectCodingStrand(int orientation) {
        _readingFrame=orientation;
        Protein.incrementMC(MC_FWD_OR_RC,this);
        clearCache();
        if (orientation==0) {
            _isTrans=null;
            delFile(strapFile(mDNA,this, null));
        }
    }
    public synchronized int getCodingStrand() { return _readingFrame;}
    public synchronized byte[] getNucleotides() {
        final byte cc[]=getCharacters();
        final int ori=getCodingStrand();
        setMC(MC_NUCLEOTIDES, mcSum(MC_CHARACTER_SEQUENCE, MC_FWD_OR_RC));
        return ori==NO_TRANSLATE ? null: cc;
    }
    /** The nucleotides after applying the necessary reverse and complement operations. The string has the exact length. */
    public synchronized byte[] getNucleotidesCodingStrand() {
        final int ori=getCodingStrand();
        final byte nucl[]=getNucleotides();
        if (nucl==null) return null;
        final Object[] cached=cached();
        final int n=countNucleotides();
        final int mc=mcSum(MC_NUCLEOTIDES, MC_FWD_OR_RC);
        if (cached[CACHE_NCS]==null || setMC(MC_NUCLEOTIDE_STRAND, mc)) {
            if (nucl==null) return null;
            final byte nu[]=0!=(ori&COMPLEMENT) ? DNA_Util.complement(nucl,n) : nucl;
            cached[CACHE_NCS]=0!=(ori&REVERSE)  ? DNA_Util.reverse(nu,n) : nu;
        }
        return (byte[])cached[CACHE_NCS];
    }

    public synchronized int countNucleotides() { return sze(getNucleotides());}
    /* The protein might be a nucleotide sequence */
    public synchronized boolean containsOnlyACTGNX() {
        final byte[] bb=getResidueType();
        final int nR=countResidues();
        final int mc=mc(MC_RESIDUE_TYPE);
        if (_ACTGNmc!=mc) {
            _ACTGNmc=mc;
            int count=0;
            for(int i=nR; --i>=0;) {
                final int c=bb[i]|32;
                if ( c=='a' || c=='c' || c=='t' || c=='g' || c=='u') count++;
            }
            _actgn=count>1 && count/(float)nR>0.95;
        }
        return _actgn;
    }

    /** Sets the sequence of nucleotides. e.g. "CAGTTGTACTG".getBytes()  */
    public synchronized void setNucleotides(Object rawNtSequence, int orientation) {
        setCharSequence(toByts(rawNtSequence));
        selectCodingStrand(orientation);
    }
    public synchronized byte[] getResidueTypeFullLength() {
        final int ori=getCodingStrand();
        final byte[] cc=getCharacters(), strand=getNucleotidesCodingStrand();
        if (ori==NO_TRANSLATE) return cc;
        final Object[] cached=cached();
        final boolean isTransl[]=isCoding();
        final int mc=mcSum(MC_NUCLEOTIDE_STRAND,MC_CODING,MC_CDS)+mc(MC_CHARACTER_SEQUENCE);
        if (cached[CACHE_RES]==null || setMC(MC_RESIDUE_TYPE_FULL,mc)) {
            if (strand==null) return cc;
            cached[CACHE_RES]=DNA_Util.translateTriplet2aaCompact(strand,isTransl);
        }
        return (byte[])cached[CACHE_RES];
    }
    /**
       The triplet at amino acid position
       @param ia the index of the amino acid
       @param buffer a buffer of 3 bytes
    */
    public synchronized byte[] getResidueTripletZ(int ia, byte buffer[]) {
        final int i0=get(3*(ia+firstResIdx(this)-getResidueIndexOffset()), coding2allPositions());
        final byte nn[]=getNucleotidesCodingStrand();
        final boolean isTransl[]=isCoding();
        if (i0<0 || isTransl==null) return null;
        int i1=i0+1; while(i1<nn.length && !get(i1,isTransl)) i1++;
        int i2=i1+1; while(i2<nn.length && !get(i2,isTransl)) i2++;
        if (i2>=nn.length) return null;
        buffer[0]=nn[i0];
        buffer[1]=nn[i1];
        buffer[2]=nn[i2];
        return buffer;
    }
    /**
       Determines which nucleotide needs to be translated (coding sequence).
       True for exon and false for intron or UTR.
    */
    public synchronized void setCoding(boolean bb[]) {
        clearCache();
        _isTrans=bb;
        Protein.incrementMC(MC_CODING,this);
        _cdsApplied=false;
    }
    /**
       True means a nucleotide is part of an triplet (exons). False means the nucleotide is not translated (intron,UTR)
    */
    public synchronized boolean[] isCoding() {
        final byte[] nn=getNucleotides();
        if (nn==null) return null;
        boolean[] coding=_isTrans;

        if (coding==null) coding=setTrue(0,countNucleotides(),null);
        return _isTrans=coding;
    }
    /**
       An array mapping nucleotide indices. E.g. the 100th translated
       nucleotide may be found at position 200 of the original
       nucleotide array if there is an untranslated region of 100
       nucleotides.
       <i>JAVADOC:Protein#coding2allPositions()</i>
    */
    public synchronized int[] toCodingPositions() { return (int[])_toCodingP()[CACHE_Ni_2_Ti]; }
    private final Object[] _toCodingP() {
        final Object[] cached=cached();
        final boolean isTransl[]=isCoding();
        final int mc=mc(MC_CODING);
        if (cached[CACHE_Ni_2_Ti]==null || setMC(MC_NUC_IDX2TRANSLATED_IDX,mc)) {
            final int n=countNucleotides();
            final int ii[]=redim( (int[])cached[CACHE_Ni_2_Ti],n,33); Arrays.fill(ii,0,n,-1);
            final int kk[]=redim( (int[])cached[CACHE_Ti_2_Ni],n,33); Arrays.fill(kk,0,n,-1);
            for(int i=0,idx=0;i<n;i++) {
                kk[idx]=i;
                if (isTransl==null || (isTransl.length>i && isTransl[i]))
                    ii[i]=idx++;
            }
            cached[CACHE_Ti_2_Ni]=kk;
            cached[CACHE_Ni_2_Ti]=ii;
        }
        return cached;
    }
    /**
       An array mapping nucleotide indices. E.g. the 100th translated
       nucleotide may be found at position 200 of the original
       nucleotide array if there is an untranslated region of 100
       nucleotides.
       See <i>JAVADOC:Protein#toCodingPositions()</i>
    */
    public synchronized int[] coding2allPositions() { return (int[])_toCodingP()[CACHE_Ti_2_Ni]; }
    public synchronized boolean[] ntPositions2aa(boolean selNts[], int offset, int returnMinIdx[]) {
        final int ii[]=toCodingPositions(), cN=countNucleotides();
        if (ii==null || selNts==null) return NO_BOOLEAN;
        final boolean isRev=0!=(getCodingStrand()&REVERSE);
        boolean selAA[]=null;
        int mi=MAX_INT, ma=MIN_INT;
        for(int pass=2; --pass>=0;) {
            for(int iN=mini(mini(selNts.length+offset,ii.length),cN); --iN>=offset;) {
                if (selNts[iN-offset]) {
                    final int iRev=isRev ? cN-iN-1:iN, ia=ii[iRev]/3+getResidueIndexOffset();
                    if (selAA!=null) {
                        if (ia>=mi && ia-mi<selAA.length) selAA[ia-mi]=true;
                    } else {
                        if (ia<mi) mi=ia;
                        if (ia>ma) ma=ia;
                    }
                }
            }
            if (selAA==null) {
                if (mi>ma) return NO_BOOLEAN;
                selAA=new boolean[ma-mi+1];
                returnMinIdx[0]=mi;
            }
        }
        return selAA;
    }
    /* <<< Nucleotide <<< */
   /* ---------------------------------------- */
    /* >>> CDS >>> */
    private Map<String,String[]> _mapCdsGene;
    public String[] getCDS() { return strgArry(_vCDS); }
    public synchronized boolean containsCDS(String sCDS) {  return cntains(sCDS,_vCDS); }
    public synchronized String[] geneProductProteinNoteForCDS(String cds) {
        return _mapCdsGene!=null ? _mapCdsGene.get(cds) : null;
    }

    public synchronized void addCDS(String cds,String[] geneProductProteinidNote) {
        if (_mapCdsGene==null)  {
            _mapCdsGene=new HashMap();
            _vCDS=new UniqueList(String.class);
        }
        _vCDS.add(cds);
        _mapCdsGene.put(cds,geneProductProteinidNote);
    }
    /* <<< CDS <<< */
    /* ---------------------------------------- */
    /* >>> Parsing  >>> */
    public final BA PARSING_INFO=new BA(999);
    private Class _proteinParserClass;
    private long _parsingTime;
    public void setProteinParserClass(Class p) { _proteinParserClass=p;}
    public void setParsingTime(long t) { _parsingTime=t;}
    public Class getProteinParserClass() { return _proteinParserClass;}
    public long getParsingTime() { return _parsingTime;}
    /**
       join(7940..7996,8095..8174,8287..8368,8506..8686,
       complement(join(1787209..1787388,1787427....
       join(complement(2261..2639),complement(881..1620))
    */
    public synchronized void parseCDS(String s) {
        if (s==null) return;
        final boolean compl= s.indexOf("compl")>=0;
        selectCodingStrand(compl ? REVERSE_COMPLEMENT : FORWARD);
        final byte txt[]=toByts(s);
        final ChTokenizer T=new ChTokenizer();
        T.setText(txt, nxt(DIGT,txt), MAX_INT);
        T.setDelimiters(chrClas(",()"));
        final boolean bb[]=new boolean[sze(getCharacters())];
        while(T.nextToken()) {
            final int idx1=T.from(), e=T.to();
            final int idx2=maxi(strstr(STRSTR_AFTER,"..",txt,idx1,e), strstr(STRSTR_AFTER,">",txt,idx1,e));
            if (idx2<0) continue;
            final int from=atoi(txt,idx1,e)-1,  to0=atoi(txt,idx2,e);
            if (from<0 || from>=to0) {
                //if (myComputer()) putln("Error in Protein#parseCDS "+this+" from="+from+" to="+to+" bb.length="+bb.length);
                continue;
            }
            final int to=mini(to0,bb.length);
            if (compl) Arrays.fill(bb, maxi(0, bb.length-to), mini(bb.length, bb.length-from),true);
            else if (from<to && from>=0 && to<=bb.length) Arrays.fill(bb, from,to,true);
        }
        setCoding(bb);
        _cdsApplied=true;
        final String refs=get(CDS_XREFS, geneProductProteinNoteForCDS(s));
        if (refs!=null) {
            removeAllSequenceRefs();
            for(String xRef: splitTokns(refs, chrClas1('\t'))) addSequenceRef(xRef);
        }
    }
    public synchronized boolean cdsExpressionApplied() { return _cdsApplied;}

    public synchronized void parseFrameShift(byte z[], int l) {
        if (z==null || l<3) return;
        final int rc= z[0]+z[1];
        selectCodingStrand(
                           rc=='C'+'R' ? REVERSE_COMPLEMENT :
                           rc=='C'+' ' ? (FORWARD|COMPLEMENT) :
                           rc=='R'+' ' ? REVERSE :
                           FORWARD);
        final int n=getCharacters().length;
        final boolean bb[]=new boolean[n];
        Arrays.fill(bb,true);
        for(int i=mini(n,l-2);--i>=0;) {
            final byte c=z[i+2];
            bb[i]= 'A'<=c && c<='Z';
        }
        setCoding(bb);
    }
    /* <<< Parsing <<< */
    /* ---------------------------------------- */
    /* >>> asString  >>> */
    /** AA sequence as string  */
    public synchronized String getResidueTypeAsString() { /* Needed for HighlightPattern */
        final Object cached[]=cached(), rt=getResidueType();
        String s=(String)fromCache(cached, RES_T_STRG, mc(MC_RESIDUE_TYPE));
        if (s==null) cached[RES_T_STRG]=s=toStrg(rt,0,countResidues()).toUpperCase();
        return s;
    }
  public synchronized String getResidueTypeFullLengthAsString() { /* Needed for Picr */
      final Object cached[]=cached(), rt=getResidueTypeFullLength();
        String s=(String)fromCache(cached, RES_T_FL_STRG, mc(MC_RESIDUE_TYPE));
        if (s==null) cached[RES_T_FL_STRG]=s=toStrg(rt).toUpperCase();
        return s;
    }
    public synchronized String getNucleotidesAsString(boolean revCompl) { /* Needed for HighlightPattern */
        final Object cached[]=cached();
        final byte bb[]=getNucleotides();
        final int n=countNucleotides(), id=revCompl ? CACHE_NR_STRG : CACHE_N_STRG;
        String s=(String)fromCache(cached, id, mc(MC_NUCLEOTIDES));
        if (s==null) {
            byte[] bb2=bb;
            if (revCompl) DNA_Util.reverseComplementDestructive(bb2=bb.clone(),n);
            cached[id]=s=bytes2strg(bb2,0,n).toUpperCase();
        }
        return s;
    }
    public String getGappedSequenceAsString() { return toStrg(getGappedSequenceExactLength());}
    /* <<< asString <<< */
    /* ---------------------------------------- */
    /* >>> Alignment Object >>> */
    private ChRunnable _align;
    public final ChRunnable getAlignment() { return _align;}
    public void setAlignment(ChRunnable a) { _align=a;}
    public void setIsInAlignment(boolean b) { _inAli=b;}
    public boolean isInAlignment() { return _inAli;}
    public void setLoaded(boolean b) { _loaded=b;}
    public boolean isLoaded() { return _loaded;}
    int[] unsyncResGap() { return _resGap;}
    int   unsyncResCount() {return _countR;}

    /* <<< Alignment Object <<< */
    /* ---------------------------------------- */
    /* >>> Cache >>> */
    private final static int
        CACHE_Ni_2_Ti=40+0, CACHE_Ti_2_Ni=40+1, CACHE_NCS=40+2, CACHE_RES=40+3,  CACHE_N_STRG=40+6, CACHE_NR_STRG=40+7,
        CACHE_groupAnno=60+0, CACHE_ResCol=60+2, CACHE_col2i=60+3, CACHE_gapped=60+4, CACHE_gappedSS=60+5, CACHE_selAA=60+6, CACHE_selNN=60+7,
        CACHE_gappedX=60+11,  CACHE_ResColW=60+12,   CACHE_col2nxt=60+13, CACHE_REND=60+14, CACHE_CHILDS=60+15, CACHE_childsBuf=60+16,
        MAX_CACHE=77;

    public final static int MC_GLOBAL[]=new int[MCA_MAX];
    private transient Object _cache;
    private final static boolean[] _mcIncremented=new boolean[MC_MAX];
    private final int[] MC=new int[MC_MAX], CACHE_MC=new int[MAX_CACHE];
    private int _subsetMC, _rGapMC, _mapRefSeqMC;
    public int mc(int type) {
        if (type>=MC.length) stckTrc();
        return MC[type]+MC_PROT_NOT_SPECIFIED[type];
    }
    public int mcSum(int t1, int t2) { return MC[t1]+MC[t2]+MC_PROT_NOT_SPECIFIED[t1]+MC_PROT_NOT_SPECIFIED[t2];}
    public int mcSum(int t1, int t2, int t3) { return MC[t1]+MC[t2]+MC[t3]+MC_PROT_NOT_SPECIFIED[t1]+MC_PROT_NOT_SPECIFIED[t2]+MC_PROT_NOT_SPECIFIED[t3];}

    public static void incrementMC(int type, Object proteins) {
        MC_GLOBAL[type]++;
        if (type<MC_MAX) {
            if (proteins==null) {
                MC_PROT_NOT_SPECIFIED[type]++;
            } else {
                _mcIncremented[type]=true;
                for(int k=szeVA(proteins); --k>=-1; ) {
                    final Object o=k<0?proteins:get(k,proteins);
                    for(int i=szeVA(o); --i>=-1;) {
                        final Object o2=i<0?o : get(i,o);
                        final Protein p=o2==null?null:o2 instanceof Protein ? (Protein)o2 : SPUtils.sp(o2);
                        if (p!=null) p.MC[type]++;
                    }
                }
            }
            if (type==MC_RESIDUE_SELECTIONS_V) incrementMC(MC_RESIDUE_SELECTIONS,proteins);
        }
    }

    public synchronized boolean setMC(int type, int value) {
        if (_mcIncremented[type]) assrt();
        if (MC[type]!=value) {
            MC[type]=value;
            return true;
        }
        return false;
    }

    private Object[] cached() {
        Object[] r=(Object[])deref(_cache);
        if (r==null) _cache=newSoftRef(r=new Object[MAX_CACHE]);
        return r;
    }

    public void clearCache() {
        _cache=null;
        final Map m=_serverMC;
        if (m!=null) m.clear();
    }
    private Object fromCache(Object cache[], int type, int mc) {
        if (CACHE_MC[type]!=mc) {
            CACHE_MC[type]=mc;
            cache[type]=null;
        }
        return cache[type];
    }
    private boolean cacheEmpty(Object cache[], int type, int mc) {
        if (CACHE_MC[type]!=mc) {
            CACHE_MC[type]=mc;
            cache[type]=null;
            return true;
        }
        return false;
    }

    /* <<< Cache <<< */
    /* ---------------------------------------- */
    /* >>> Gaps >>> */
    private int _resGap[];
    public synchronized void setResidueGap(int rg[]) {
        _resGap=rg;
        _rGapMC++;
        incrementMC(MCA_ALIGNMENT,null);
    }
    public synchronized void setResidueGap(int iA,int gap) {
        final int gaps[]=getResidueGap();
        if (iA>=0 && iA<sze(gaps)) {
            gaps[iA]=maxi(0,gap);
            setResidueGap(gaps);
        }
    }
    public synchronized int addResidueGap(int iA,int gap) {
        final int gg[]=getResidueGap();
        if (iA<0 || sze(gg)<=iA) return 0;
        final int add=gg[iA]+gap<0 ? -gg[iA] : gap;
        if (add!=0) {
            gg[iA]+=add;
            setResidueGap(gg);
        }
        return add;
    }
    public synchronized int[] getResidueGap() {
        final int n=countResidues();
        int[] gg=_resGap;
        if (gg==null) gg=_resGap=new int[n];
        if (gg.length<n || gg.length>n+99) gg=_resGap=chSze(gg,n);
        return gg;
    }
    public synchronized int getResidueGap(int i) { return i<countResidues() ? get(i,getResidueGap()) : INT_NAN;}
   /* <<< Gaps <<< */
    /* ---------------------------------------- */
    /* >>> Columns >>> */
    private static int _wideMC;
    private static boolean _wide;
    public static boolean isWide() { return _wide;}
    public static void setWide(boolean b){ if (_wide!=b) { _wide=b; _wideMC++;}  }

    /**
       Maps residue indices upon horizontal text positions of the alignment.
       @return an integer-array of all horizontal positions for each residue
    */
    public synchronized int[] getResidueColumn() {
        final Object[]cached=cached();
        final int nR=countResidues();
        getResidueType();
        final Gaps2Columns gap2col=(Gaps2Columns)runCR(getAlignment(), RUN_ALIGNMENT_GET_GAP2COL);
        if (!_inAli ||  gap2col==null || isWide()) return getResidueColumnWide();
        final int wide2narrow[]=gap2col!=null ? gap2col.wide2narrowColumn() : null;
        final int gaps[]=getResidueGap();
        final int mc=mc(MC_RESIDUE_TYPE)+_rGapMC+ (gap2col==null?0 : gap2col.mc());
        int cc[]=(int[])cached[CACHE_ResCol];
        if (cc==null || cc.length<nR || cacheEmpty(cached, CACHE_ResCol, mc)) {
            cached[CACHE_ResCol]=cc=Gaps2Columns.computeColumns(wide2narrow,gaps,nR,cc);
        }
        return cc!=null ? cc : NO_INT;
    }

    //  final boolean isDebug() { return myComputer() && strEquls("a2",getName());}
    private int[] getResidueColumnWide() {
        final Object[]cached=cached();
        final int nR=countResidues(), gaps[]=getResidueGap();
        int cc[]=(int[])cached[CACHE_ResColW];
        if (cc==null || cc.length<nR || cacheEmpty(cached, CACHE_ResColW,  mc(MC_RESIDUE_TYPE)+_rGapMC)) {
            cached[CACHE_ResColW]=cc=redim(cc,nR,33);
            for(int po=-1,ia=0;ia<nR;ia++) cc[ia]= po+=gaps[ia]+1;
        }
        return cc;
    }
    /** Maps residue indices to horizontal text positions of the alignment. */
    public synchronized int getResidueColumn(int i)  { return getResidueColumnZ(i-getFirstAndLastResidueIndex()[0]); }
    public synchronized int getResidueColumnZ(int i) { return i<countResidues() ? get(i,getResidueColumn()) : MIN_INT; }
    /** Get the horizontal position of the last residue.. */
    public synchronized int getMaxColumnZ() { return maxi(-1,getResidueColumnZ(countResidues()-1));}
    /* <<< Columns <<< */
    /* ---------------------------------------- */
    /*  >>> col to idx >>> */
    /**
       An array to map horizontal residue positions to residue indices. At gap positions the array contains -1.
    */
    public synchronized int[] columns2nextIndices() {
        final Object[] cached=cached();
        final int cc[]=getResidueColumn(), nR=countResidues();
        if (nR==0) return NO_INT;
        int[] ii=(int[])cached[CACHE_col2nxt];
        if (ii==null || cacheEmpty(cached, CACHE_col2nxt,  _wideMC+CACHE_MC[CACHE_ResColW]+CACHE_MC[CACHE_ResCol]+mc(MC_RESIDUE_TYPE))) {
            final int nI=getMaxColumnZ();
            ii= redim(ii,nI+1,99);
            Arrays.fill(ii,0,nI+1,-1);
            for(int ia=0;ia<nR;ia++) {
                final int c=cc[ia];
                if (c>=0 && c<ii.length) ii[c]=ia;
            }
            int last=-1;
            for(int i=nI;i>=0;i--) {
                if (ii[i]<0) ii[i]=last;
                else last=ii[i];
            }
            cached[CACHE_col2nxt]=ii;
        }
        return ii;
    }
    public synchronized byte[] getGappedSequence() { return (byte[])getGappedSequence(cached())[CACHE_gapped]; }
    //public byte[] getGGS() { return (byte[])cached()[CACHE_gapped]; }

    public synchronized int[] columns2indices() { return (int[])getGappedSequence(cached())[CACHE_col2i]; }
    public synchronized byte[] getGappedSecStru() { return (byte[])getGappedSequence(cached())[CACHE_gappedSS]; }
    /**
       Get  index of residue at the horizontal alignment position col.
       @param col the alignment position (column)
       @return The residue index or -1 for an alignment position (column).
    */
    public synchronized int column2indexZ(int col) {
        final int ii[]=columns2indices();
        return ii!=null && col<ii.length && 0<=col && col<=getMaxColumnZ() ? ii[col] : -1;
    }
    /**
       Get  index of residue at the horizontal alignment position col.
       if h points to a gap return the previous index.
       @param col  the alignment position (column)
       @return The residue index or -1 for an alignment position (column).
    */
    public synchronized int column2thisOrPreviousIndex(int col) {
        final int colMax=getMaxColumnZ();
        final int ii[]=columns2indices();
        if (col>colMax || col>=ii.length) return countResidues()-1;
        if (col<0) return 0;
        int c=col;  while(c>=0 && ii[c]<0) c--;
        return c<0 ? -1 : ii[c];
    }
    /**
       Get  index of residue at the horizontal alignment position h or the next index if h points to  a gap.
       @param h the alignment position (column)
       @return the index of the residue at alignment position
    */
    public synchronized int column2nextIndexZ(int h) {
        final int ii[]=columns2nextIndices();
        return h>=0 && h<=getMaxColumnZ() && ii.length>h ? ii[h]:-1;
    }
    /* <<< col to idx <<< */
    /* ---------------------------------------- */
    /* >>> gapped sequence >>> */

    /** The amino acid sequence with gaps (0x20). Array may be longer than getMaxColumnZ().In this case the end is marked with 0x00.  */
    private Object[] getGappedSequence(Object[]cached) {
        final byte aa[]=getResidueType(), ss[]=getResidueSecStrType();
        final int cc[]=getResidueColumn(), n=aa==null||cc==null||aa.length==0||cc.length==0?0:countResidues();

        final int mc=mcSum(MC_RESIDUE_TYPE,MC_RESIDUE_SECSTRU)+CACHE_MC[CACHE_ResColW]+CACHE_MC[CACHE_ResCol]+_rGapMC+_wideMC;
        byte gs[]=(byte[])cached[CACHE_gapped];
        if (gs==null || setMC(MC_GAPPED_SEQUENCE,mc)) {
            final boolean hasSecStru=n>0 && sze(ss)>0;
            final int RESERVE=11;
            final int maxCol=n<1?-1:cc[n-1], maxCol1=maxCol+1, c0=n==0?0:cc[0];
            if (gs==null||gs.length<maxCol1||n==0) cached[CACHE_gapped]=gs=new byte[maxCol1+RESERVE];
            if (n>0 && c0>maxCol) { if (myComputer()) putln(RED_ERROR+" in getGappedSequence ", getName()); stckTrc(); return cached;}
            if (c0>0) Arrays.fill(gs,0,c0,(byte)' ');
            if (c0<maxCol) Arrays.fill(gs,c0, maxCol,(byte)'-');
            Arrays.fill(gs,maxCol1,gs.length,(byte)0);
            final byte gss[];
            if (hasSecStru) {
                gss=redim((byte[])cached[CACHE_gappedSS],maxCol1,RESERVE);
                if (c0<=maxCol) {
                    Arrays.fill(gss,c0,maxCol,(byte)'-');
                    Arrays.fill(gss,maxCol1,gss.length,(byte)0);
                    Arrays.fill(gss,0,c0,(byte)' ');
                } else if (myComputer()) putln(RED_ERROR+" in getGappedSequence: c0="+c0+" maxCol="+maxCol);
            } else gss=null;
            cached[CACHE_gappedSS]=gss;
            final int[] c2i=redim((int[])cached[CACHE_col2i],maxCol1,RESERVE);
            Arrays.fill(c2i,0,maxCol1,-1);
            try {
                for(int i=n; --i>=0;) {
                    final int col=cc[i];
                    gs[col]=aa[c2i[col]=i];
                }
                if (hasSecStru) {
                    for(int i=n; --i>=0;) {
                        final int col=cc[i];
                        if (hasSecStru && ss.length>i) gss[col]=ss[i];
                    }
                }
            } catch(IndexOutOfBoundsException iobex) {
                putln(RED_ERROR+" maxCol1="+maxCol1+"\n gss="+sze(gss)+"\n gs="+sze(gs));
                stckTrc(iobex);
            }
            cached[CACHE_col2i]=c2i;
            cached[CACHE_gappedX]=null;
        }
        return cached;
    }
    /**  The amino acid sequence.  Gaps are character '-'. */
    public synchronized byte[] getGappedSequenceExactLength() {
        final Object[]cached=cached();
        final byte gg[]=getGappedSequence();
        if (gg==null) return NO_BYTE;
        final int maxCol=getMaxColumnZ();
        byte[] newSS=(byte[])cached[CACHE_gappedX];
        if (newSS==null) { System.arraycopy(gg,0,newSS=new byte[maxCol+1],0,maxCol+1); cached[CACHE_gappedX]=newSS; }
        return newSS;
    }
    /* <<< Get gapped Sequence <<< */
    /* ---------------------------------------- */
    /* >>> Set gapped Sequence >>> */
    /**
       The original residue types are kept but the gaps are inferred from the
       gapped sequence seq.
       A letter denotes an amino acid, any other character a gap.
    */
    public synchronized void inferGapsFromGappedSequence(byte[] seq) {
        if (seq==null) return;
        final int nR=countResidues(), gaps[]=new int[nR];
        for(int lastPos=-1,ia=0,pos=0; pos<seq.length && ia<nR; pos++) {
            final int c=seq[pos]|32;
            if ('a'<=c && c<='z') {
                gaps[ia++]=pos-lastPos-1;
                lastPos=pos;
            }
        }
        setResidueGap(gaps);
    }
    public synchronized void setGappedSequence(byte[] seq, int len) {
        setResidueType(filtr(0L,LETTR,seq,0,len));
        inferGapsFromGappedSequence(seq);
    }
    /** set the gapped sequence */
    public synchronized void setGappedSequence(CharSequence seq) { setGappedSequence(toByts(seq),MAX_INT);}
    /**
       Reads a line such as X2XXX3X1XXXX2XXXXX
       The numbers are gaps and the letters residue positions
    */
    public boolean parseGaps(final byte z[],final int l)  {
        if (z==null) return false;
        {
            final int count=countLettrs(z,0,l);
            final int gaps[]=new int[count];
            int po=0, ga=0;
            for(int i=0;i<l;i++) {
                final int c=z[i];
                if (is(LETTR,c)) {
                    gaps[po++]=ga;
                    ga=0;
                    continue;
                }
                if ('0'<=c && c<='9' || c=='-') {
                    int gap=z[i]-'0';
                    while(i+1<l && is(DIGT,z[i+1])) gap=10*gap+z[++i]-'0';
                    ga+=gap;
                }
            }
            setResidueGap(gaps);
        }
        /* may be toLowerCase */
        final int nR=countResidues();
        final byte aa[]=getResidueType();
        for(int i=0, ia=0; i<l && ia<nR; i++) {
            if (z[i]<='z' && z[i]>='a') aa[ia++] |=32;
        }
        return true;
    }
    /* <<< Set gapped Sequence <<< */
    /* ---------------------------------------- */
    /* >>> Sort >>> */
    public final static int O_WEB_TOK=0, O_WEB_TOKENS=1, O_WEB_TOK1=2, B_PROCESSED=4, B_INTO_SAME_LINE=5, B_REGISTERD=6;
    public final Object OBJECTS[]=new Object[7];
    private int _tokenIdx=MIN_INT,  _preferedOrder, _iRow=-1;
    public int getWebTokenIdx() {
        final Object t=OBJECTS[O_WEB_TOK], tt[]=(Object[])OBJECTS[O_WEB_TOKENS];
        if (_tokenIdx==MIN_INT && t!=null && tt!=null) _tokenIdx=idxOf(t,tt);
        return _tokenIdx;
    }

    public int getPreferedOrder() { return _preferedOrder;}
    public void setPreferedOrder(int t) { _preferedOrder=t;}
    public void setRow(int i) { _iRow=i;}
    public int getRow() { return !isInAlignment() ? -1 : _iRow;}

    /* <<< Sort <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */
    private Map<File, String> _mapDragfileModi;
    /* <<< DnD <<< */
    /* ---------------------------------------- */
    /* >>> Save >>> */
    public void save(int opt, File dir, BA error) {
        if (dir!=dirStrapAnno()) {
            final int gaps[]=getResidueGap();
            if (gaps!=null && (dir==dirDndData() || _rGapMC>0)) {
                final int n=countResidues();
                final byte buffer[]=new byte[12];
                final File f=strapFile(mGAPS, this, dir);
                OutputStream w=null;
                try {
                    w=fileOutStrm(f);
                    final int GAP=mini(n,gaps.length);
                    for(int i=0;i<GAP;i++) {
                        final int g=gaps[i];
                        if (g>0) w.write(buffer,0,itoa(g,buffer));
                        w.write((int)'X');
                    }
                } catch(IOException e) { cnwError(f,error); }
                closeStrm(w);
            }
            if (getNucleotides()!=null) {
                final int n=countNucleotides(), ori=getCodingStrand();
                final File f=strapFile(mDNA, this, dir);
                final boolean[] coding=isCoding();
                OutputStream w=null;
                try {
                    w=fileOutStrm(f);
                    w.write( 0!=(ori&REVERSE)   ? 'R' :' ');
                    w.write( 0!=(ori&COMPLEMENT)? 'C' :' ');
                    for(int i=0;i<n;i++) w.write(coding==null || (i<coding.length && coding[i]) ? 'X' : 'x');
                } catch(IOException e) { cnwError(f,error); }
                closeStrm(w);
            }
            {
                final Matrix3D m3d=getRotationAndTranslation();
                final File f=strapFile(mTRANS, this, dir);
                if (m3d==null) delFile(f);
                else if (!wrte(f,m3d.toString())) cnwError(f,error);
            }
            {
                final int idx=getResidueIndexOffset();
                final File f=strapFile(Protein.m1stIdx, this, dir);
                if (idx==0) delFile(f);
                else if (!wrte(f,toStrg(idx))) cnwError(f,error);
            }
            {
                final String pdbId=getInferredPdbID();
                final File f=strapFile(Protein.mASSOCPDB, this, dir);
                if (pdbId==null) delFile(f);
                else if (!wrte(f,pdbId)) cnwError(f,error);
            }
        }
        cnwError(ResidueAnnotation.save(this, dir), error);
    }
    static void cnwError(File f, BA sb) { if (sb!=null && f!=null) sb.a("Error: Could not write ").aln(f); }
    public boolean isTransient() { return _transient;}

    /** Non-transient proteins are not saved */
    public void setTransient(boolean b) { _transient=b;}
    /* <<< Save <<< */
    /* ---------------------------------------- */
    /* >>> Parser >>> */
    /**
       Interpretes the text and extracts the protein data and sets the fields such as residueType in the protein.
       @param parsers An array of parsers to do the job. Try all until one succeeds.
       @param txt the protein text in fasta or pdb or swissprot format.
       @return true on success
    */
    public boolean parse(BA txt, ProteinParser parsers[], long mode) {
        boolean ret=false;
        final long time=System.currentTimeMillis();
        for(ProteinParser parser:parsers) {
            if (parser!=null && parser.parse(this,mode,txt)) {
                setProteinParserClass(parser.getClass());
                setParsingTime(System.currentTimeMillis()-time);
                ret=true;
                break;
            }
        }
        if ( (mode&ProteinParser.SEQUENCE_FEATURES)!=0) ExpasyGff.readFeaturesInProteinFile(txt, 2, this);
        return ret;
    }

    /* <<< Parser <<< */
    /* ---------------------------------------- */
    private final static Map<Image,Image> mapImageSmall=new WeakHashMap();
    /* >>> Protein Icon >>> */

    private Image _iconImage;
    private File _iconFile;
    private javax.swing.ImageIcon _icon;
    private String _imageId, _iconURL;
    /** Get the icon of the protein or null */
    public Image getImage() { return getIconImage();}
    public javax.swing.ImageIcon getIcon() {
        final Image im=getIconImage();
        if (im==null) return null;
        if (_icon==null) {
            Image imSmall=mapImageSmall.get(im);
            if (imSmall==null)
                mapImageSmall.put(im, imSmall=ChIcon.getScaledInstance(im,ICON_HEIGHT, ICON_HEIGHT, dotSfx(_iconFile),null));
            if (imSmall!=null) _icon=new javax.swing.ImageIcon(imSmall);
        }
        return _icon;
    }
    public Image getIconImage() {
        if (_iconImage==null) return img(getResidueCalpha()!=null && !trueOrEmpty(gcp(SequenceAligner.KOPT_NOT_USE_STRUCTURE,this)) ?  IC_3D : getNucleotides()!=null ? IC_DNA : null);
        return _iconImage;
    }
    public void setIconImage(String url) {
        _iconURL=url;
        if (sze(url)==0) delFile(iconLink()); else wrte(iconLink(), url);
        _icon=null;
        _iconImage=null;
        for(String ext : JPG_PNG_GIF_BMP) delFile(file(DIR_ANNOTATIONS+"/"+getName()+ext));
        if (withGui()) findIconImage();
    }

    public String getIconUrl() { return _iconURL;}
    public String getImageId() { return _imageId;}
    public void findIconImage() {
        _icon=null;
        _iconImage=null;
        _imageId=_iconURL=null;
        final File f=iconLink();
        if (sze(f)==0) {
            final File dir=strapFile(mANNO, null, null);
            for(String ext : JPG_PNG_GIF_BMP) {
                final File fGif=file(dir,getName()+ext);
                if (sze(fGif)>0) wrte(f,toStrg(fGif));
            }
        }
        final String link=toStrgTrim(readBytes(f));

        if (sze(link)>0) {
            final String trim=_iconURL=link;
            _iconFile=file(trim);
            ChIcon.requestImage(_imageId=trim,(ChRunnable)this);
        }
    }
    public File iconLink() { return strapFile(mICON,this,null);}
    /* <<< Protein Icon <<< */
    /* ---------------------------------------- */
    /* >>> balloon text >>> */
    private String _balloon[]={null,null}, _balloonDirect;
    private long _balloonWhen;
    private File _balloonF;
    public void setBalloonText(String t) { _balloonDirect=t; _balloon[0]=_balloon[1]=null; }
    public String balloonText(boolean html) {
        if ( _balloon[0]==null ||  _balloonF==null || _balloonWhen!=_balloonF.lastModified()) {
            _balloonWhen=(_balloonF=strapFile(mBALLON, this, null)).lastModified();
            BA ba=readBytes(_balloonF);
            if (_balloonDirect!=null) (ba==null?ba=new BA(0) : ba).a('\n').a(_balloonDirect);
            _balloon[0]=toStrgN(ba);
            _balloon[1]=nxt(LT_GT_AMP, ba)>=0 ? toStrg(new BA(0).filter(FILTER_HTML_ENCODE,ba)) : _balloon[0];
        }
        return orS(_balloon[html?1:0],null);

    }
    /* <<< Balloon text <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    public final static int POS_CLICKED_3D=1, POS_PRINT_COLUMN=2;
    private static ProteinLabel _pLabel, _pLabelV;
    private int _iconWidth;
    private final Rectangle _labelSize=new Rectangle();
    private final Object _lSizeDepends[]=new Object[3];
    private final static Rectangle _rcls=new Rectangle();
    private final long HASH_CODE32=((long)hashCode())<<32, _when=System.currentTimeMillis();
    private Color _color;
    public void setColor(Color c) { _color=c;}
    public Color getColor() { return _color;}
    public void reportIconWidth(int w) {_iconWidth=w;}
    public long getRendererMC() { return HASH_CODE32+mcSum(MC_PROTEIN_LABELS,MC_CURSOR_PROTEIN);  }
    public Object getRenderer(long options, long rendOptions[]) {
        if (_pLabel==null) _pLabel=new ProteinLabel(0L,null);
        _pLabel.setProtein(this); return _pLabel;
    }
    public Component verticalRendererComponent() {
        if (_pLabelV==null) _pLabelV=new ProteinLabel(ProteinLabel.VERTICAL,null);
        _pLabelV.setProtein(this);
        return _pLabelV;
    }
    public String getRendTxt() {
        final Object[] cached=cached();
        getNucleotides();
        String s=(String)fromCache(cached, CACHE_REND, mcSum(MC_INFERRED_PDB,MC_NUCLEOTIDES,MC_PROTEIN_LABELS));
        if (s==null) {
            final BA sb=baClr(1,_BA).a(this);
            final int ori=getCodingStrand();
            if (getNucleotides()!=null) sb.a(' ').a(0!=(ori&REVERSE)?'R' : 'F').a(0!=(ori&COMPLEMENT)?'C' : ' ');
            sb.and("  (",getInferredPdbID(),")");
            cached[CACHE_REND]=s=sb.toString();
        }
        return s;
    }
    public Rectangle getLabelSize(Font f,Rectangle charBounds) {
        final Image image=getIconImage();
        final String name=getName();
        if (_lSizeDepends[0]!=name || _lSizeDepends[1]!=image || !f.equals(_lSizeDepends[2])) {
            _lSizeDepends[0]=name;
            _lSizeDepends[1]=image;
            _lSizeDepends[2]=f;
            computeLabelSize(f, charBounds, name, _labelSize);
        }
        return _labelSize;
    }
    public Rectangle computeLabelSize(Font f, Rectangle charBounds, String name, Rectangle rect) {
        final Rectangle r=rect!=null?rect : _rcls;
        final Image image=getIconImage();
        final int imHeight=hght(image);
        setRect(
                imHeight>0 ? wdth(image)*charBounds.height/imHeight : 0,
                0,
                x2(_labelSize)+ strgWidth(f,name),
                charBounds.height,
                r);
        return r;
    }
    public BA posAsTextZ(int type, final int iA) {
        final BA sb=baClr(2,_BA);
        if (iA<0 || iA>=countResidues()) return sb.a("error");
        sb.aSomeBytes(getResidueName32(iA),4);
        if (type!=POS_CLICKED_3D) sb.a(" Index ").a(getFirstAndLastResidueIndex()[0]+iA+1);
        final int rn=getResidueNumber(iA);
        if (rn!=INT_NAN) {
            final byte ins=getResidueInsertionCode(iA), chain=getResidueChain(iA);
            sb.a(type==POS_CLICKED_3D ? " " : " Residue ").a(rn);
            if (is(LETTR_DIGT, ins)) sb.a((char)ins);
            if (is(LETTR_DIGT,chain))sb.a(" Chain ").a((char)chain);
        }
        if (type!=POS_CLICKED_3D) {
            final float ca[]=getResidueCalpha();
            if (sze(ca)>3*iA && !Float.isNaN(ca[3*iA])) sb.a(" xyz ");
            final byte secS=get(iA,getResidueSecStrType());
            if (secS=='H' || secS=='E') sb.a(secS=='E' ? "  Helix (red)" : "  Beta sheet (yellow)");
            final int atomIdx=getResidueFirstAtomIdx(iA);
            if (atomIdx>=0) sb.a("  Atom ").a(atomIdx).a('-').a(getResidueLastAtomIdx(iA));
        }
        if (getNucleotides()!=null) {
            sb.a(" Nucleotide ").a(get(3*iA, coding2allPositions())+1);
            final byte trip[]=getResidueTripletZ(iA,TRIPLET);
            if (trip!=null) sb.a(' ').a((char)trip[0]).a((char)trip[1]).a((char)trip[2]);
        }
        if (type==POS_PRINT_COLUMN) sb.a(" Column ").a(getResidueColumnZ(iA)+1);
        return sb;
    }
    /* <<< Renderer <<< */
    /* -------------------------------------- */
    /* >>> Event >>> */
    public static BasicResidueSelection getMouseOverSelection() { return _mouseOverSel;}
    private static BasicResidueSelection _mouseOverSel;
    public static void mouseOver(Protein p,int iAa0) {
        final int iAa=iAa0<0?-1:iAa0;
        BasicResidueSelection s=_mouseOverSel;
        final Protein pPrev=s!=null ? s.getProtein() : null;
        final int iAaPrev= s!=null ? s.getSelectedAminoacidsOffset() : -1;
        if (p!=pPrev || iAa!=iAaPrev) {
            if (pPrev!=null) pPrev.removeResidueSelection(s);
            if (p!=null && iAa>=0 && iAa<p.countResidues()) {
                if (s==null) {
                    _mouseOverSel=s=new BasicResidueSelection(0);
                    s.setName(ResidueSelection.NAME_MOUSE_OVER);
                    s.setColor(Color.PINK);
                    s.setStyle(VisibleIn123.STYLE_CIRCLE);
                    s.setVisibleWhere(VisibleIn123.SEQUENCE|VisibleIn123.SB);
                }
                BasicResidueSelection.selectRange(iAa,iAa+1, s);
                p.addResidueSelection(s);
            }
            new StrapEvent(Protein.class, StrapEvent.RESIDUE_SELECTION_CHANGED).run();
        }
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final ChJList l=q instanceof ChJList ? (ChJList)q:null;
        final Object[] selected=l!=null ? l.getSelectedValues():null;
        if (cmd==ACTION_DELETE_OR_BACKSPACE && selected!=null) {
            for(boolean del:FALSE_TRUE) {
                int count=0;
                for(Object o : selected) {
                    o=deref(o);
                    final HeteroCompound h= o instanceof HeteroCompound ? (HeteroCompound)o:null;
                    if (del) removeHeteroCompound(h);
                    else count++;
                }
                if (del || !ChMsg.yesNo("Delete "+count+" compounds?")) break;
            }
        }
    }

    /* <<< Event <<< */
    /* -------------------------------------- */
    /* >>> Residue Selection >>> */
    public final static int SELECTIONS=0, SELECTIONS_ALL=1, SELECTIONS_ANNO=2, SELECTIONS_ANNO_NF=3, SELECTIONS_FEATURES=4, SELECTIONS_FEATURES_DEACT=5;
    private final UniqueList[] _vv=new UniqueList[6];
    public UniqueList<ResidueSelection> vSel(int i) {
        if (_vv[i]==null) {
            final String t=
                i==SELECTIONS ? "Residue selections" :
                i==SELECTIONS_ALL ? "All residue selections" :
                i==SELECTIONS_ANNO ? "Residue annotations and sequence features" :
                i==SELECTIONS_ANNO_NF ?  "Residue annotations" :
                i==SELECTIONS_FEATURES ? "Sequence features" :
                i==SELECTIONS_FEATURES_DEACT ? "Deactivated sequence features" :
                null;
            _vv[i]=new UniqueList(i<SELECTIONS_ANNO ? ResidueSelection.class : ResidueAnnotation.class).t(t);
        }
        return _vv[i];
    }

    private ResidueSelection[] resSel(int i) { return _vv[i]!=null ? (ResidueSelection[])_vv[i].asArray() : ResidueAnnotation.NONE; }
    public synchronized ResidueSelection[] residueSelections() { return resSel(SELECTIONS); }
    public synchronized ResidueSelection[] allResidueSelections() { return resSel(SELECTIONS_ALL); }
    public synchronized ResidueAnnotation[] residueAnnotations() { return (ResidueAnnotation[])resSel(SELECTIONS_ANNO); }
    /** Add a  selection of residues  */
    public boolean addResidueSelection(ResidueSelection s) {
        if (s==null) return false;
        final ResidueAnnotation a=deref(s, ResidueAnnotation.class);
        final boolean isCursor=s instanceof HasName && nam(s)==ResidueSelection.NAME_CURSOR;
        boolean changed=false;
        synchronized(this) {
            final boolean enab=a!=null && a.isEnabled();
            final String fn=a==null?null: a.featureName();
            if (fn!=null) {
                changed|=vSel(enab?SELECTIONS_FEATURES:SELECTIONS_FEATURES_DEACT).add(a);
                changed|=remov(a,_vv[enab?SELECTIONS_FEATURES_DEACT:SELECTIONS_FEATURES]);
                if (changed) incrementMC(MCA_SEQUENCE_FEATURES_V, this);
            }
            changed|=vSel(SELECTIONS_ALL).add(s);
            if (changed) {
                if (isCursor) { /*_vSelHidden.add(s); */}
                else if (a!=null) {
                    if (enab) vSel(SELECTIONS_ANNO).add(a);
                    else if (fn!=null) remov(a, _vv[SELECTIONS_ANNO]);
                } else vSel(SELECTIONS).add(s); //sortArry(vSel(SELECTIONS), ProteinUtils.comparator(ProteinUtils.COMP_SEL_VIS));
                if (s.getProtein()!=this) s.setProtein(this);
                incrementMC(MC_RESIDUE_SELECTIONS_V,this);
            }

            if (enab && ("Turn"==fn || "Helix"==fn || "Beta_strand"==fn)) {
                if (myComputer() && getResidueSecStrType()!=null) {
                    putln(RED_ERROR+" Feature sec stru ",a, this);
                    stckTrc();
                }
            }
        }
        if (changed && !isCursor) updateTree(a!=null ? a : _vv[SELECTIONS], null);
        return changed;
    }

    public synchronized ResidueSelection[] getResidueSelectionsWithName(String[] names) {
        final ResidueSelection ssAll[]=allResidueSelections(), ss[]=new ResidueSelection[ssAll.length];
        int count=0;
        for(ResidueSelection s : ssAll) {
            if (idxOfStrg(nam(s), names)>=0 ) ss[count++]=s;
        }
        return chSze(ss,count,ResidueSelection.class);
    }
    /** Remove residue selection */
    public synchronized boolean removeResidueSelection(ResidueSelection s) {
        if (s==null) return false;
        boolean changed=false;
        for(UniqueList v : _vv) changed|=remov(s,v);
        if (changed) {
            remov(s,StrapAlign.selectedObjectsV());
            if (ResSelUtils.type(s)=='F') incrementMC(MCA_SEQUENCE_FEATURES_V, this);
            incrementMC(MC_RESIDUE_SELECTIONS_V,this);
        }
        return changed;
    }
    /**
       Returns an array telling what residues are selected by at least one residue selection.
       0 element means not selected any other number means selected.
    */
    public byte[] selAminos() { /* no synchronized */
        final Object[] cached=cached();
        final int mc=mcv(_vv[SELECTIONS_ALL])+mc(MC_RESIDUE_SELECTIONS);
        final byte[] cSelA= (byte[]) cached[CACHE_selAA], cSelN=(byte[])cached[CACHE_selNN];
        if (cSelA==null  || cSelN==null || setMC(MC_SELECTED_AA, mc))  {
            byte[] bbbA=NO_BYTE, bbbN=NO_BYTE;
            for(ResidueSelection s: allResidueSelections()) {
                if (!ResSelUtils.isSelVisible(VisibleIn123.SEQUENCE|VisibleIn123.NO_FLASH,s)) continue;
                final SelectorOfNucleotides son=s instanceof SelectorOfNucleotides ? (SelectorOfNucleotides)s : null;
                if (son!=null) {
                    final boolean bbN[]=son.getSelectedNucleotides();
                    final int nN=countNucleotides();
                    final int selOffsetN=son.getSelectedNucleotidesOffset();
                    final int lastTrue=lstTrue(bbN);
                    if (lastTrue>=0) {
                        if (bbbN==NO_BYTE) {
                            if (cSelN!=null && nN<=cSelN.length) Arrays.fill(bbbN=cSelN,(byte)0);
                            else bbbN=new byte[nN+9];
                        }
                        final int iTo=mini(lastTrue+1, nN-selOffsetN);
                        for(int i=maxi(0,-selOffsetN); i<iTo; i++) {
                            if (bbN[i] && bbbN[i+selOffsetN]<Byte.MAX_VALUE) bbbN[i+selOffsetN]++;
                        }
                    }
                }
                final boolean bbA[]=ResSelUtils.selectedAminosDisplayed(s);
                final int nR=countResidues();
                final int selOffsetA=s.getSelectedAminoacidsOffset()-getFirstAndLastResidueIndex()[0];
                final int lastTrue=lstTrue(bbA);
                if (lastTrue>=0) {
                    if (bbbA==NO_BYTE) {
                        if (cSelA!=null && nR<=cSelA.length) Arrays.fill(bbbA=cSelA,(byte)0);
                        else bbbA=new byte[nR+9];
                    }
                    final int iTo=mini(lastTrue+1, nR-selOffsetA);
                    for(int i=maxi(0,-selOffsetA); i<iTo; i++) {
                        if (bbA[i] && bbbA[i+selOffsetA]<Byte.MAX_VALUE) bbbA[i+selOffsetA]++;
                    }
                }
            }
            cached[CACHE_selNN]=bbbN;
            cached[CACHE_selAA]=bbbA;
        }
        return (byte[])cached[CACHE_selAA];
    }
    public synchronized byte[] selNuc() {
        final Object[] c=cached();
        selAminos();
        final byte nn[]=(byte[])c[CACHE_selNN];
        return nn==null ? NO_BYTE:nn;
    }
    /**  Remove all selection of residues that are instances of c  */
    public synchronized void removeResidueSelection(Class c) {
        if (c==null) return;
        for(ResidueSelection s : allResidueSelections()) {
            if (isAssignblFrm(c,s)) removeResidueSelection(s);
        }
    }
    public synchronized ResidueSelection[] residueSelectionsAt(long options, int iA_from, int iA_to, int where) {
        final int nRes=countResidues();
        if (iA_from>=iA_to || iA_to<1 || iA_from >=nRes) return ResidueSelection.NONE;
        List<ResidueSelection> v=null;
        for(ResidueSelection s: allResidueSelections()) {
            if (!ResSelUtils.isSelVisible(where,s))  continue;
            if (nam(s)==ResidueSelection.NAME_CURSOR) continue;
            final boolean bb[]=
                0!=(options&VisibleIn123.ARROW_HEADS)  ? ResSelUtils.selectedAminosDisplayed(s) :
                s.getSelectedAminoacids();
            if (fstTrue(bb)<0) continue;
            final int selOffset=s.getSelectedAminoacidsOffset()-getFirstAndLastResidueIndex()[0];
            final int to=mini(nRes,bb.length+selOffset, iA_to);
            for (int iA=maxi(selOffset,iA_from);  iA<to; iA++) {
                if (bb[iA-selOffset]) {
                    (v==null ? v=new UniqueList(ResidueSelection.class) : v).add(s);
                    break;
                }
            }
        }
        return toArry(v,ResidueSelection.NONE);
    }
    public ResidueAnnotation getResidueAnnotationWithName(CharSequence name) {
        if (name==null) return null;
        final String trim=toStrgTrim(name);
        long created=0;
        ResidueAnnotation a=null;
        for(ResidueAnnotation ra : residueAnnotations()) {
            final String n=ra.value(ResidueAnnotation.NAME);
            if (n.indexOf(trim)>=0 && trim.equals(toStrgTrim(ra.value(ResidueAnnotation.NAME)))) {
                final long cr=ra.whenCreated();
                if (cr>created) { a=ra; created=cr;}
            }
        }
        return a;
    }
    private ChMap<String,UniqueList<ResidueAnnotation>> residueAnnotationsByGroup() {
        final Object[] cached=cached();
        final ResidueAnnotation aa[]=residueAnnotations();
        final int mc=mc(MC_RESIDUE_ANNOTATION_GROUPS)+mcv(_vv[SELECTIONS_ANNO])+ mcv(_vv[SELECTIONS_FEATURES])+ mcv(_vv[SELECTIONS_FEATURES_DEACT]);
        ChMap<String,UniqueList<ResidueAnnotation>> mapGroups=(ChMap)cached[CACHE_groupAnno];
        if (mapGroups==null || cacheEmpty(cached, CACHE_groupAnno, mc)) {
            if (mapGroups==null) {
                mapGroups=new ChMap(0L, String.class, UniqueList.class,16).i(IC_ANNO).t("Residue Annotations");
                cached[CACHE_groupAnno]=mapGroups;
            }
            for(UniqueList v : mapGroups.vArray()) clr(v);
            for(int i=3; --i>=0;) {
            }
            for(ResidueAnnotation a :aa) {
                if (a.featureName()==null) {
                    final String group=a.value(ResidueAnnotation.GROUP);
                    UniqueList<ResidueAnnotation> v=mapGroups.get(group);
                    if (v==null) mapGroups.put(group,v=new UniqueList(ResidueAnnotation.class).t(group).i(iicon(IC_ANNO)));
                    v.add(a);
                }
            }
            for(UniqueList v : mapGroups.vArray()) {
                if (v.size()==0) mapGroups.remove(v.getText());
            }
            for(int i=2; --i>=0;) {
                final List v=_vv[i==0?SELECTIONS_FEATURES_DEACT:SELECTIONS_FEATURES];
                if (sze(v)>2) Collections.sort(v,  comparator(COMPARE_RENDERER_TEXT));
            }
        }
        return mapGroups;
    }
    /* <<< ResidueAnnotation <<< */
    /* ---------------------------------------- */
    /* >>> SequenceFeatures >>> */
    public Map<String,ReferenceSequence> _mapRefSeq;

    private Set<CharSequence> _vInvalidF;
    public ReferenceSequence getReferenceSequence(String otherId, BA log, boolean background, Runnable whenFinished ) { /* no synchronized !!!! */
        getResidueType();
        final int mc=mc(ProteinMC.MC_RESIDUE_TYPE);
        if (_mapRefSeqMC!=mc || _mapRefSeq==null) {
            _mapRefSeqMC=mc;
            _mapRefSeq=new HashMap();
        }
        ReferenceSequence uniprot=_mapRefSeq.get(otherId);
        if (uniprot==null) _mapRefSeq.put(otherId,uniprot=new ReferenceSequence(otherId,this, log, background, whenFinished));
        return uniprot.isSuccess() || background ? uniprot : null;
    }
    public Set<CharSequence> vInvalidFeatureLine() { if (_vInvalidF==null) _vInvalidF=new HashSet();  return _vInvalidF;   }
    /* <<< SequenceFeatures <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */

    @Override public Object run(String id, Object arg) {
        if (id==PROVIDE_HIGHLIGHTS) return getName();
        if (id=="MC") return MC;
        if (id==PROVIDE_DND_FILES) {
            final int dragOptions=atoi(runCR(getAlignment(), RUN_ALIGNMENT_GET_DRAG_OPTIONS));
            final File f0=getFile(), dir=dirDndData();
            strapFile(mGAPS,null,dir);
            strapFile(mANNO,null,dir);

            final boolean
                isEntireMSF=(dragOptions&DRAG_MSA)!=0,
                isAllChains=(dragOptions&DRAG_ALL_CHAINS)!=0,
                isOrigFile3D=(dragOptions&DRAG_ORIG)!=0,
                writeSingleFA=isInMsfFile() && !isEntireMSF || f0==null,
                writeAllPdbChains=getResidueCalpha()!=null && isAllChains && getFileWithAllChains()!=null;
            final int bioMolecule=(dragOptions>>>8)&255;
            getAtomCoordinates();
            final byte[] sequence=getResidueTypeExactLength();
            if (_mapDragfileModi==null) _mapDragfileModi=new HashMap();
            final String mcOpts=mcSum(MC_RESIDUE_TYPE,MC_ATOM_COORD)+"_"+dragOptions;
            final String base=name2base_ext()[0], ext=name2base_ext()[1];
            final List<File> v=new ArrayList();
            int countB=0;
            final Matrix3D mmDrag[]=new Matrix3D[getBioMatrices().length];
            for(Matrix3D m:getBioMatrices()) if (m!=null && m.getBiomolecule()==bioMolecule) mmDrag[countB++]=m;

            cpy(iconLink(), strapFile(mICON, this, dir));

            if (countB==0 &&  !writeAllPdbChains) save(0, dir,null);

            if (countB>0) {
                loadSideChainAtoms(null);
                final BA ba=new BA(countResidues()*666);
                for(int iM=mmDrag.length; --iM>=0;) {
                    if (mmDrag[iM]==null) continue;
                    final File fB=file(dir+"/"+base+".biomol_"+bioMolecule+(iM<10?"_0"+iM :"_0" )+".pdb");
                    if (sze(fB)==0 || !mcOpts.equals(_mapDragfileModi.get(fB))) {
                        new ProteinWriter1().toText(this, new Matrix3D[]{mmDrag[iM]}, ProteinWriter.SIDE_CHAIN_ATOMS | ProteinWriter.COMPLETE_PDB, ba.clr());
                        wrte(fB,ba);
                    }
                    v.add(fB);
                }
                final File fB=file(dir+"/"+base+".biomol_"+bioMolecule+"_ALL.pdb");
                if (sze(fB)==0 || !mcOpts.equals(_mapDragfileModi.get(fB))) {
                    new ProteinWriter1().toText(this,null, ProteinWriter.PDB | ProteinWriter.SEQRES | ProteinWriter.HELIX_SHEET, ba.clr());
                    try {
                        final OutputStream fo=fileOutStrm(fB);
                        ba.write(fo);
                        for(Matrix3D m3d : mmDrag) {
                            if (m3d==null) continue;
                            new ProteinWriter1().toText(this,new Matrix3D[]{m3d}, ProteinWriter.PDB | ProteinWriter.ATOM_LINES | ProteinWriter.SIDE_CHAIN_ATOMS, ba.clr());
                            ba.write(fo);
                        }
                        closeStrm(fo);
                        v.add(fB);
                    } catch(IOException iox) { stckTrc(iox);}
                }
            } else {
                File fExpr=null;
                BA sbHeteros=null;
                final String outName=isEntireMSF && f0!=null ? f0.getName() : base+(writeAllPdbChains ? "_all":"")+(writeSingleFA ? ".fa":ext);
                final File fOut=file(dir+"/"+outName);
                if (!mcOpts.equals(_mapDragfileModi.get(fOut))) delFile(fOut);
                final Matrix3D m3d=getRotationAndTranslation();
                if (writeAllPdbChains) {
                    if (isOrigFile3D || (m3d==null || m3d.isUnit()) )  v.add(getFileWithAllChains());
                    else {
                        if (sze(fOut)==0) {
                            final long oldOptions=SPUtils.parserOptions();
                            SPUtils.setParserOptions(ProteinParser.SIDE_CHAIN_ATOMS);
                            final Protein p=Protein.newInstance(getFileWithAllChains());
                            p.setRotationAndTranslation(m3d);
                            wrte(fOut,ProteinWriter1.mergePdbWithOriginal(p));
                            SPUtils.setParserOptions(oldOptions);
                        }
                        v.add(fOut);
                    }
                } else if (writeSingleFA) {
                    if (sze(fOut)==0) {
                        final BA ba=new BA(countResidues()+999).a('>').a(this).a('|');
                        String r=" refs=";
                        for(String s: getSequenceRefs()) {ba.a(r).a(s).a(','); r="";}
                        final int[] firstLast=getFirstAndLastResidueIndex();
                        if (firstLast[0]>0) ba.a(" FIRSTINDEX=").a(firstLast[0]);
                        wrte(fOut,ba.a('\n').aln(sequence));
                    }
                    v.add(fExpr=fOut);
                } else {
                    if (!isOrigFile3D && getResidueCalpha()!=null && !(m3d==null || m3d.isUnit())) {
                        if (sze(fOut)==0) wrte(fOut,ProteinWriter1.mergePdbWithOriginal(this));
                        v.add(fExpr=fOut);
                    } else {
                        v.add(fExpr=f0);
                        for(HeteroCompound h : getHeteroCompounds('*'))  {
                            if (h.getFile()==null) continue;
                            if (sbHeteros==null) sbHeteros=new BA(333).aln(getName());
                            sbHeteros.a(h.getFile()).a(' ');
                        }
                    }
                }
                if (fExpr!=null) {
                    wrte(file(dir+"/"+fExpr.getName()+".proteinName"), sbHeteros!=null ? sbHeteros : getName()); // !!!!!
                }
            }
            while(v.remove(null)){}
            final File ff[]=toArry(v,File.class);
            for(File f:ff) _mapDragfileModi.put(f,mcOpts);
            return ff;
        }
        if (id=="SORT_RA") residueAnnotationsByGroup();
        if (id=="H_CHANGED") updateTree(vHH('N'), vHH('*'));
        if (id=="V3D_CHANGED") updateTree(_v3d,null);
        if (id==ChRunnable.RUN_SET_ICON_IMAGE) {
            final Image im=(Image)arg;
            if (_iconImage!=im) {
                _iconImage=im;
                _icon=null;
                incrementMC(MC_PROTEIN_LABELS,this);
                StrapEvent.dispatchLater(StrapEvent.PROTEIN_RENAMED,333);
            }
        }
        if (id==DialogStringMatch.PROVIDE_HAYSTACK || id==DialogStringMatch.PROVIDE_STRINGS || id==PROVIDE_WORD_COMPLETION_LIST) {
            final int iRadio=StrapTree.idxRadioSearch();
            if (id==DialogStringMatch.PROVIDE_HAYSTACK) {
                return iRadio==2 ? residueAnnotations() : null;
            } else {
                if (iRadio==3) return getResidueTypeAsString();
                final Object hh[]=HAYSTACKS1;
                int i=0;
                if (iRadio==1) {
                    if (hh.length<INFO_TITLE.length+4) {
                        stckTrc();
                        return null;
                    }
                    for(; i<INFO_TITLE.length; i++) hh[i]=info(i);
                    hh[i++]=getPdbRef();
                    hh[i++]=getHeader();
                    hh[i++]=getSequenceRefs();
                } else {

                    hh[i++]=getName();
                    hh[i++]=getFile();
                }
                for(;i<hh.length;i++) hh[i]=null;
                return hh;
            }
        }
        if (id==PROVIDE_JTREE_CHILDS) {
            final Object[] cached=cached();
            final ChMap grouped=residueAnnotationsByGroup();
            final int mc=mcv(_vv[SELECTIONS_ANNO])+  mcv(_vv[SELECTIONS])+mcv(_v3d)+mcv(_vHH[HHnh]);
            getHeteroCompounds('N'); /* see getD_R_H */
            Object[] cc=(Object[])fromCache(cached, CACHE_CHILDS, mc);
            if (cc==null) {
                Object[] buf=(Object[])cached[CACHE_childsBuf];
                if (buf==null) cached[CACHE_childsBuf]=buf=new Object[7];
                buf[0]=_vv[SELECTIONS];
                buf[1]=_v3d;
                buf[2]=grouped.vArray().length>0? grouped : null;
                buf[3]=_vv[SELECTIONS_FEATURES];
                buf[4]=_vv[SELECTIONS_FEATURES_DEACT];
                buf[5]=_vHH[HHh];
                buf[6]=_vHH[HHn];
                for(int i=buf.length; --i>=0;) if (sze(buf[i])==0) buf[i]=null;
                cached[CACHE_CHILDS]=cc=rmNullA(buf);
            }
            return cc;
        }
        if (id==ChRunnable.RUN_GET_TIP_TEXT) {
            final boolean shift;
            if (withGui()) {
                AWTEvent ev=deref(arg, AWTEvent.class);
                final AWTEvent lstEv=lstMouseEvt();
                if (ev==null && lstEv!=null && lstEv.getID()==MouseEvent.MOUSE_MOVED) ev=lstEv;
                final int x=x(ev);
                if (x>maxi(BALLOON_MAXX,_iconWidth)) return balloonText(false);
                shift=0!=(MouseEvent.SHIFT_MASK&modifrs(ev));
            } else shift=false;
            final BA sb=baTip().and(fPathUnix(getFile()),"<br>");
            final Object im=sze(_iconFile)>0 ? _iconFile.toURI() : getIconUrl();
            if (im!=null) {
                if (!shift) sb.a("<table><tr><td>");
                sb.a("<img src=\"").a(im).a("\">");
                if (shift) return sb;
                sb.aln("</td><td>");
            }
            {
                final String h=getHeader(), o=getOrganism(), sc=getOrganismScientific();
                String c=getTitle();
                if (c==null) c=getCompound();
                if (sze(h)>0) {
                    final int L=sb.end();
                    sb.filter(FILTER_HTML_ENCODE,h).replace(0L,"|", "<br>",L,MAX_INT).a("<br>");
                }
                if (sze(c)>0)  sb.filter(FILTER_HTML_ENCODE, c).a("<br>");
                if (sze(sc)>0) sb.filter(FILTER_HTML_ENCODE,sc).a("<br>");
                if (sze(o)>0 && !o.equals(sc)) sb.filter(FILTER_HTML_ENCODE,o).a("<br>");
            }
            {
                final String acc=getAccessionID(), uid=getUniprotID(), pid=getPdbID();
                if (getSequenceRefs().length+sze(acc)+sze(pid)+sze(uid)>0) {
                    sb.and(" Acc=", acc).and(" PDB=", eq(acc,pid)?null : pid).and(" UniProt=",eq(acc,uid)?null : uid).and(" Swissprot=",getSwissprotID());
                    int count=0;
                    for(String r : getSequenceRefs()) {
                        if (eq(r,pid) || eq(r,uid) || eq(r,acc)) continue;
                        if (count++==0) sb.a("IDs: ");
                        sb.a(count%6==0 ? '\n' : ' ').a(r);
                    }
                }
            }
            for(int i=2; --i>=0;) {
                final String txt=i==1 ? balloonText(true) : _balloonDirect;
                if (sze(txt)>0) sb.a("<br>").filter(FILTER_NO_HTML_BODY, txt).a("<br>");
            }

            if (getNucleotides()!=null) sb.a("<br>").a(get(getCodingStrand(), TRANSLATIONS_AS_STRING));
            final String unip[]=FindUniprot.ids(this);
            if (sze(unip)>0) {
                sb.a("<br>Uniprot by sequ identity:");
                for(String s : unip) sb.a(' ').a(delPfx("UNIPROT:",s));
                sb.a("<br>");
            }
            if (!Float.isNaN(getResolutionAngstroms())) sb.a("<br>Resolution ").a(getResolutionAngstroms()).a(" Angstrom<br>");
            if (im!=null) sb.aln("</td></tr></table>");
            final int[] firstLast=getFirstAndLastResidueIndex();
            if (firstLast[0]>0) sb.a("<br>").a(firstLast[0]+1).a('.',3).a(firstLast[1]+1).a("<br");
            if (getHeteroCompounds('N').length>0) {
                sb.colorBar(0x00FF00).a(" DNA/RNA compound structures # ").a(getHeteroCompounds('N').length).a(" &nbsp; ");
                for(HeteroCompound h : getHeteroCompounds('N')) sb.a(' ').a(h.getNameForPymol());
                sb.aln("<br>");
            }
            if (getHeteroCompounds('H').length>0) {
                sb.colorBar(0xFF0000);
                int sbL0=sze(sb);
                sb.a("Hetero compound structures # ").a(getHeteroCompounds('H').length).a(" &nbsp; ");
                for(HeteroCompound h : getHeteroCompounds('H')) {
                    sb.a(' ').a(h.getNameForPymol());
                    if (sze(sb)-sbL0>60) sbL0=sze(sb.a("<br>"));
                }
                sb.aln("<br>");
            }
            return sb;
        }
        return null;
    }
    /* <<< Threading  <<< */
    /* ---------------------------------------- */
    /* >>> JTree >>> */
    private ChJTree _jTree;
    public ChJTree getJTree() {
        ChJTree jt=_jTree;
        final ChRunnable align=deref(getAlignment(),ChRunnable.class);
        if (jt==null && align!=null) {
            _jTree=jt=new ChJTree(ChJTable.DEFAULT_RENDERER|ChJTable.DRAG_ENABLED, new ChTreeModel(0L,this));
            jt.setRootVisible(false);
            jt.setShowsRootHandles(true);
            jt.setRowHeight(ICON_HEIGHT);
            runCR(align,RUN_ALIGNMENT_ADD_AWT_LISTENERS,jt);
            pcp(KEY_DROP_TARGET_REDIRECT,WEAK_REF, jt);
            final Object msg=pnl(VBHB,
                                 "Note: Objects may have  a context menu and may support Drag-and-Drop  ",
                                 pnl(HBL,WATCH_MOVIE+MOVIE_Context_Menu," ",WATCH_MOVIE+MOVIE_Sequence_Features_in_3D));

            pcp(KEY_PANEL, pnl(CNSEW,scrllpn(SCRLLPN_TOOLS, jt, dim(222,222)),null, pnl(HBL,toggl().doCollapse(msg),msg)), jt);
        }
        return jt;
    }
    public void expandTreeNode(Class c, int type) {
        final Object node=
            _jTree==null ? null :
            c==ResidueAnnotation.class ? residueAnnotationsByGroup() :
            c==SequenceFeatures.class ? _vv[SELECTIONS_FEATURES] :
            c==ResidueSelection.class ? _vv[SELECTIONS] :
            c==HeteroCompound.class ? _vHH[type=='N'?HHn:HHh] : null;
        if (node!=null) ChJTree.expandAllNodes(node,_jTree);
    }
    private void updateTree(Object expand1, Object expand2) {
        final ChJTree jT=_jTree;
        final Window w=jT==null ? null : parentWndw(jT);
        if (w instanceof Frame && ((Frame)w).getState()==Frame.NORMAL) {
            Object ex1=expand1, ex2=expand2;
            if (ex1 instanceof ResidueAnnotation) {
                final ResidueAnnotation a=(ResidueAnnotation)ex1;
                ex1=residueAnnotationsByGroup();
                ex2=residueAnnotationsByGroup().get(a.value(ResidueAnnotation.GROUP));
            }
            if (ex1!=null || ex2!=null) pcp(ChJTree.KEY_EXPAND, new Object[]{ex1,ex2},jT);
            jT.updateKeepStateLater(0L, 99);
        }
    }
    /* <<< JTree <<< */
    /* ---------------------------------------- */
    /* >>> Script >>> */
    private Map<String,int[]> _serverMC;
    public void toXML(int opt, Collection vSel, BA sb, String cacheKey, boolean selected) {
        final int[] mc;
        if (cacheKey!=null) {
            if (_serverMC==null) _serverMC=new HashMap();
            mc=_serverMC.get(cacheKey);
        } else mc=null;
        final String infPDB=getInferredPdbID();
        final byte[] secStru=getResidueSecStrType(), chain=getResidueChain(), inser=getResidueInsertionCode(), nucl=getNucleotidesCodingStrand();
        final int nR=countResidues(), n1=Protein.firstResIdx(this), nn[]=getResidueNumber(), infPDBMatch[]=getInferred3dCountMatches();
        final File file=getFile(), file3D=getFileMayBeWithSideChains();

        sb.a("<protein name=\"").a(this).a("\" file=\"").a(file);
        if (file3D!=null && file3D!=file) sb.a("\" file3D=\"").a(file3D);
        if (vSel.contains(this)) sb.a("\" selected=\"true");
        sb.aln("\" >");
        if (n1!=0) sb.a("<residueIndex1>").a(n1+1).aln("</residueIndex1>");
        if (mc==null || mc[MC_GAPPED_SEQUENCE]!=MC[MC_GAPPED_SEQUENCE]) {
            sb.a("<sequence>").a(getGappedSequence(),0,getMaxColumnZ()).aln("</sequence>");
        }
        if (nucl!=null && (mc==null || mc[MC_CODING]!=MC[MC_CODING])) {
            sb.a("<codingSequence>").a(DNA_Util.onlyTranslated(nucl,isCoding())).aln("</codingSequence>");
        }
        if (infPDB!=null && infPDBMatch!=null) sb.a("<inferredStructure pdbID=\"").a(infPDB).a("\" match=\"").a(infPDBMatch[0]).a('/').a(infPDBMatch[1]).aln("\" />");
        for(int i=3; --i>=0;) {
            final String
                id=i==0?getPdbID() : i==1? getUniprotID(): getAccessionID(),
                t=i==0?"pdbId"     : i==1? "uniprotId"   : "accessionId";
            if (id!=null) sb.a('<').a(t).a('>').a(id).a("</").a(t).aln('>');
        }
        if (secStru!=null && (mc==null || mc[MC_RESIDUE_SECSTRU]!=MC[MC_RESIDUE_SECSTRU])) {
            for(int i=mini(secStru.length,nR); --i>=0;) {
                if (secStru[i]==0) secStru[i]=(byte)' ';
            }
            sb.a("<secondaryStructure>").a(secStru,0,nR).aln("</secondaryStructure>");
        }
        if (nn!=null && (mc==null || mc[MC_CALPHA]!=MC[MC_CALPHA])) { // !!!!!
            sb.a("<residueNumber>");
            for(int i=0;i<nn.length && i<nR; i++) {
                if (nn[i]==INT_NAN) sb.a("NaN ");
                else {
                    sb.a(nn[i]);
                    if (is(LETTR_DIGT,inser,i)) sb.a((char)inser[i]);
                    sb.a(':');
                    if (is(LETTR_DIGT,chain,i)) sb.a((char)chain[i]);
                    sb.a(' ');
                }
            }
            sb.aln("</residueNumber>");
        }
        final ResidueSelection ss[]=allResidueSelections();
        if (ss.length>0 && (mc==null || mc[MC_RESIDUE_SELECTIONS]!=MC[MC_RESIDUE_SELECTIONS])) {
            sb.aln("<residueSelections>");
            for(ResidueSelection s : ss) {
                sb.a("<residueSelection name=\"").a(nam(s)).a("\" aa=\"").boolToText(s.getSelectedAminoacids(), s.getSelectedAminoacidsOffset()+1," ", "-");
                if (vSel.contains(s)) sb.a("\" selected=\"true");
                sb.a("\" ");
                if (ResSelUtils.type(s)=='A') {
                    sb.aln(">\n<data>");
                    for(ResidueAnnotation.Entry e : ((ResidueAnnotation)s).entries()) {
                        if (!e.isEnabled()) continue;
                        sb.a(e.key()).a(' ').filter(FILTER_HTML_ENCODE, e.value()).a('\n');
                    }
                    sb.aln("</data>\n</residueSelection>");
                } else sb.aln(" />");
            }
            sb.aln("</residueSelections>");
            final Matrix3D m3d=getRotationAndTranslation();
            if (m3d!=null && !m3d.isUnit() && (mc==null || mc[MC_MATRIX3D]!=MC[MC_MATRIX3D])) {
                m3d.toText(Matrix3D.FORMAT_ROUNDED,null, sb.a("<transformation3D>")).aln("</transformation3D>");
            }
        }
        long pdbOpts=0;
        if ((0!=(opt&ExecByRegex.CALPHA_COORDINATES) || selected && 0!=(opt&ExecByRegex.CALPHA_COORDINATES_IF_SELECTED)) && (mc==null || mc[MC_CALPHA]!=MC[MC_CALPHA])) {
            pdbOpts=ProteinWriter.PDB;
        }
        if ((0!=(opt&ExecByRegex.ATOM_COORDINATES) || selected && 0!=(opt&ExecByRegex.ATOM_COORDINATES_IF_SELECTED)) && (mc==null || mc[MC_ATOM_COORD]!=MC[MC_ATOM_COORD])) {
            pdbOpts=ProteinWriter.PDB|ProteinWriter.SIDE_CHAIN_ATOMS;
        }
        if (pdbOpts!=0) new ProteinWriter1().toText(this, null,pdbOpts|ProteinWriter.ATOM_LINES|ProteinWriter.SEQRES | ProteinWriter.HELIX_SHEET|ProteinWriter.HETEROS, sb);
        if (
            !isInMsfFile() &&
            (0!=(opt&ExecByRegex.FILE_CONTENT) || selected && 0!=(opt&ExecByRegex.FILE_CONTENT_IF_SELECTED)) &&
            (mc==null || mc[MC_FILE_CONTENT]!=MC[MC_FILE_CONTENT])) {
            final BA txt=readBytes(file);
            if (sze(txt)>0)  sb.a("<fileContent>").filter(FILTER_HTML_ENCODE, txt).aln("</fileContent>");
        }
        if (cacheKey!=null) _serverMC.put(cacheKey,MC.clone());
        sb.aln("</protein>\n");
    }

    private static int mcv(UniqueList v) { return v==null?0:v.mc();}/* Faster than ChUtils.modic */

}
