package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   <b>Home: </b> http://openastexviewer.net  http://www.astex-therapeutics.com/<br>
   <b>License of OpenAstexViewer:</b>  WIKI:LGPL <br>
   <b>Authors: </b> Mike Hartshorn, Tom Oldfield<br><br>
   <br><br>

   @author Christoph Gille
   http://openastexviewer.net/web/OpenAstexViewer.jar
*/
public class ChAstexPROXY extends AbstractProteinViewerProxy implements HasControlPanel {

    @Override public String getRequiredJars() { return "OpenAstexViewer4.jar"; }

    public static String getCustomizableInitCommands() {
        return
            "secstruc ( molecule 'PROTEIN' );\n"+
            COMMANDselect+" $ALL\n"+
            COMMANDcartoon+"\n"+
            "display lines off  ( molecule 'PROTEIN'  );\n"+
            "display lines on (all and aminoacid and ( atom C or atom CA or atom CB or atom N or atom O));\n"+
            "display sticks  on  (all and not ( aminoacid or solvent) );\n"+
            "";
    }

    public static String getCustomizableExamples() {
        return
            "display  spheres on  (group RESIDUES);\n"+
            "display  spheres off (group RESIDUES);\n"+
            "display  cylinders on  (group RESIDUES);\n"+
            "display  cylinders off (group RESIDUES);\n"+
            "display  sticks on  (group RESIDUES);\n"+
            "display  sticks off  (group RESIDUES);\n"+
            "display  lines on (group RESIDUES);\n"+
            "display  lines off (group RESIDUES);\n"+
            "color blue   (group RESIDUES);\n"+
            "schematic -name 'schematic_RESIDUES' -alltube  true  -ribbonwidth 20 (group RESIDUES);\n"+
            "object remove 'schematic_PROTEIN';\n"+
            "surface -solid true  'surface_RESIDUES' pink (group RESIDUES);\n"+
            "object remove 'surface_PROTEIN';\n"+
            "schematic -name 'schematic_PROTEIN'  ( molecule 'PROTEIN' );\n"+
            "object remove 'schematic_PROTEIN';\n";
    }

    public static String getWordCompletionList() {
        return
            "select schematic  secstruc display center remove "+
            "object molecule aminoacid group surface current default "+
            "-alltube "+
            "-arrowheadwidth "+
            "-arrowsmoothing "+
            "-arrowthickness "+
            "-arrowwidth "+
            "-quality "+
            "-ribboncylinders "+
            "-ribbonellipse "+
            "-ribbonminwidth "+
            "-ribbonthickness "+
            "-ribbonwidth "+
            "-tuberadius "+
            "-tubesmoothing "+
            "-tubetaper "+
            "-tubetaperradius "+
            "true false ";
    }
}
