package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
   @author Christoph Gille
   // SocketOutputStream  BufferedInputStream
   java charite.christo.protein.PicrEBI
*/
public class PicrEBI implements ChRunnable {
    public final static int UNIPROT=1;
    private PicrEBI(){}
    private static Object[] CACHE={null};
    private final static String TEST_SEQ=new String("MATERYSFSLTTFSPSGKLVQLEYALAAVSGGAPSVGIIASNGVVIATENKHKSPLYEQHSVHRVEMIYNHIGMVYSGMGPDYRLLVKQARKIAQTYYLTYKEPIPVSQLVQRVATLMQEYTQSGGVRPFGVSLLICGWDNDRPYLYQSDPSGAYFAWKATAMGKNAVNGKTFLEKRYSEDLELDDAVHTAILTLKEGFEGKMTADNIEIGICDQNGFQRLDPASIKDYLASIP");
    public static synchronized String[] idsForSequence(boolean noBlocking, int type, String seq) {
        final java.util.Map<String,String[]> cache=fromSoftRef(0,CACHE, java.util.Map.class);
        final long time=System.currentTimeMillis();
        String result[]=cache.get(seq);
        if (result==null) {
            final String cacheS=new BA(99).a("Picr1").a(seq,0,3).a(seq.hashCode()%10).toString(), cacheK=seq;
            final long lastModi=CacheResult.getLastModified(PicrEBI.class, cacheS, cacheK);
            final boolean tooOld=System.currentTimeMillis()-lastModi>24L*60*60*1000;
            BA idS=!CacheResult.isEnabled() ? null : CacheResult.getValue(0, PicrEBI.class, cacheS, cacheK, (BA)null);
            if (strstr("UNIPROT:",idS)<0 && tooOld) idS=null;
            if (idS==null && !noBlocking && !isSelctd(BUTTN[TOG_DEACT_PICR])) {
                BA answer=null;
                for(int iDB=2; --iDB>=0 && strstr(0, "accession>", answer)<0; ) {
                    final Object query[][]={  {"sequence",seq},  {"database",  iDB>0?"SWISSPROT":"TREMBL"} };
                    answer=Web.getServerResponse(Web.NO_PRINT_EXCEPTION, new BA(999).a("http://www.ebi.ac.uk/Tools/picr/rest/getUPIForSequence&").a(seq==TEST_SEQ?"":DO_NOT_ASK).toString(), query);
                    if (answer==null && System.currentTimeMillis()-time>5000) setSelctd(true,buttn(TOG_DEACT_PICR));
                }
                if (answer!=null) {
                    final byte[] T=answer.bytes();
                    final int B=answer.begin(), E=answer.end();
                    idS=new BA(999);
                    for(int i=B;  (i=strstr(STRSTR_AFTER, "accession>", answer, i,E))>=0; i++) {
                        final int lt=strchr('<',T,i,E);
                        if (lt>i) idS.a("UNIPROT:").a(T,i,lt).a(' ');
                    }
                    CacheResult.putValue(CacheResult.OVERWRITE, PicrEBI.class, cacheS, cacheK, idS);
                } else if (tooOld && lastModi!=0) {
                    idS=CacheResult.getValue(0, PicrEBI.class,cacheS, cacheK, (BA)null);
                }
            }
            cache.put(seq,result=splitTokns(idS, chrClas1(' ')));
            //debugTime("PicrEBI",time);
        }
        return result;
    }

    private static String _testResult;
    public static void checkAvailability() {
        startThrd(thrdM("idsForSequence",PicrEBI.class, new Object[]{boolObjct(false), intObjct(0), TEST_SEQ}));
        startThrd(thrdCR(new PicrEBI(), "AVAIL"));
    }

    public Object run(String id, Object arg) {
        if (id=="AVAIL") {
            for(int i=9; --i>=0 && _testResult==null; ) sleep(999);
            if (!looks(LIKE_UNIPROT_ID, get(0,_testResult))) {
                setSelctd(true,buttn(TOG_DEACT_PICR));
                logUnavailable().a("Not available: ");
            } else logUnavailable().a(GREEN_AVAILABLE);
            logUnavailable().a("EBI service Picr\n").send();
        }
        return null;
    }

}
