package charite.christo.protein;
/**
   Returns a numeric value for a protein
*/
public interface ValueOfProtein {
    void setProtein(Protein p);
    void compute();
    double getValue();


}
