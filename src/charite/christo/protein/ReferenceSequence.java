package charite.christo.protein;
import charite.christo.*;
import java.io.*;
import java.net.URL;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class ReferenceSequence extends Thread {
    private final byte[] _resType;
    private final BA _log;
    private final String _otherId, _linkForAli;
    private final File[] _files;
    private final Runnable _runWhenIni;
    private final Object _p;
    private URL _url;
    private byte[] _alignment[];
    private short[] _u2i;
    private boolean _downloaded, _success;
    private float _score=Float.NaN;
    private int _aliStartsAtIdx;
    /* >>> Instance >>> */
    public ReferenceSequence(String otherId, Protein p, BA log, boolean background, Runnable runWhenInitialized) {
        _runWhenIni=runWhenInitialized;
        _otherId=otherId;
        _resType=p.getResidueTypeExactLength();
        _log=log;
        _linkForAli=Hyperrefs.PAIR_ALIGNMENT+otherId+"_"+p;
        _p=wref(p);
        _files=new File[]{  FetchSeqs.dbColonID2file(otherId), file(Hyperrefs.toUrlString(otherId,Hyperrefs.PROTEIN_FILE))};
        if (sze(_files[0])+sze(_files[1])==0) {
            final int MAX_AGE_DAYS=999;
            _url=url((Hyperrefs.toUrlString(otherId,Hyperrefs.PROTEIN_FILE)));
            if (background) {
                adUniq(this, _vDownload);
                if (!_downloading) startThrd(this);
            } else urlGet(_url,MAX_AGE_DAYS);
        }
        if (sze(_files[0])>0 || sze(_files[1])>0) init();
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Init >>> */
    private void init() {
        if (_downloaded) return;
        final Protein p=getReferenceProtein();
        final byte[] seq=p!=null ? p.getResidueTypeExactLength() : null;
        if (sze(seq)>0) {
            final SequenceAligner aligner=AlignUtils.newDefaultAligner2();
            aligner.setOptions(SequenceAligner.OPTION_CHECK_PERFECT_MATCH);
            aligner.setSequences(_resType,seq);
            aligner.compute();
            final byte[] aligned[]=aligner.getAlignedSequences(), a0=(byte[])get(0,aligned), a1=(byte[])get(1,aligned);
            final float score=aligner instanceof HasScore ? ((HasScore)aligner).getScore() : Blosum.blosumScore2(2,8,a0,a1);
            if ( !Float.isNaN(_score=score) && a0!=null && a1!=null) {
                _alignment=aligned;
                _aliStartsAtIdx=maxi(0,strstr(STRSTR_IC,a0, _resType));
                {
                    final File fAli=file(Hyperrefs.toUrlString(_linkForAli, Hyperrefs.PLAIN_TEXT));
                    final BA sb=new BA(999).a(a0).aln('\r');
                    final int to=mini(a0.length,a1.length);
                    for(int i=0; i< to;i++) sb.a((char)Blosum.BL2SEQ_MIDLINE[a0[i]&127][a1[i]&127]);
                    sb.aln('\r').a(a1).a("\r\n _alignment score=").a(score).aln('\r');
                    wrte(fAli,sb);
                }
                final short u2i[]=new short[seq.length];
                Arrays.fill(_u2i=u2i,(short)-1);
                final int to=mini(a0.length, a1.length);
                short idxO=(short)idxOfLetters(a0, _resType,0,MAX_INT);
                short idxU=(short)idxOfLetters(a1,seq,0,MAX_INT);
                for(int col=0; col<to; col++) {
                    final boolean lO=is(LETTR,a0[col]), lU=is(LETTR,a1[col]);
                    if (lO && lU && 0<=idxU && idxU<u2i.length) u2i[idxU]=idxO;
                    if (lO) idxO++;
                    if (lU) idxU++;
                }
                if (_log!=null) _log.a(_linkForAli).a(" clustalW-score=").a(_score).a('\n').send();
                _success=true;
            }
        } else if (_log!=null) _log.a(" Protein ").a(this).a(": failed to load ").aln(_otherId).a('\n');
        _downloaded=true;
        if (_success) runR(_runWhenIni);
    }
    /* <<< Init <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */

    public void run() {

        synchronized(_vDownload) {
            _downloading=true;
            for(int n; (n=sze(_vDownload))>0;) {
                for(int i=n; --i>=0;) {
                    final ReferenceSequence rs=(ReferenceSequence)get(i,_vDownload);
                    if (rs!=null) {
                        urlGet(rs._url,999);
                        rs.init();
                        final Protein p=(Protein)deref(_p);
                        if (rs.getReferenceProtein()!=null && p!=null) {
                            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS,p);
                            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTION_REFERENCE,p);
                            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,333);

                        }
                    }
                    _vDownload.remove(rs);
                }
                ChUtils.sleep(10);
            }
            _downloading=false;
        }
    }

    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> Getters >>> */
    public boolean isSuccess() { return _success; }
    public String getLinkForAlignment() { return _linkForAli;}
    public short[] getUniprotIdx2Idx() {  return _downloaded ? _u2i : null;}
    public Protein getReferenceProtein() {
        Protein p=mapP().get(_otherId);
        if (p==null) {
            for(File f : _files) {
                p=sze(f)==0?null:Protein.newInstance(f);
                if (p!=null) {
                    mapP().put(_otherId,p);
                    break;
                }
            }
        }
        return p;
    }
    public boolean[] uniprotIdx2Idx(boolean uniprot[], int offset, int returnMinIdx[]) {
        final short[] u2i=getUniprotIdx2Idx();
        final int N=mini(sze(u2i), uniprot.length+offset);
        int min=MAX_INT, max=MIN_INT;
        for(int i=N; --i>=offset; ) {
            if (!uniprot[i-offset]) continue;
            final int idx=u2i[i];
            if (idx<min) min=idx;
            if (idx>max) max=idx;
        }
        if (min>max) return NO_BOOLEAN;
        max=mini(min+99999,max);
        final boolean[] bb=new boolean[max-min+1];
        for(int i=N; --i>=offset; ) {
            if (uniprot[i-offset]) bb[u2i[i]-min]=true;
        }
        returnMinIdx[0]=min+Protein.firstResIdx((Protein)deref(_p));
        return bb;

    }

    public float getAlignmentScore() { return _score;}
    public BA getLocalAlignment(int aFrom, int aTo, BA sb) {
        if (!_downloaded) return null;
        final int[] fromToCol=new int[2];
        countMismatch(aFrom, aTo, fromToCol);
        final byte[] aa[]=_alignment, a0=aa[0],a1=aa[1];
        final int f=maxi(fromToCol[0], 0);
        final int t=mini(fromToCol[1], a0.length,a1.length);
        sb.a(' ', sze(_otherId)+1).a(a0, f,t);
        //for(int i=f; i<t; i++) sb.a((char)a0[i]);
        sb.a('\n').a(' ',sze(_otherId)+1);
        for(int i=f; i<t; i++) sb.a((char)Blosum.BL2SEQ_MIDLINE[a0[i]&127][a1[i]&127]);
        return sb.a('\n').and(_otherId," ").a(a1,f,t).a('\n');
        //for(int i=f; i<t; i++) sb.a((char)a1[i]);
    }
    public int countMismatch(int fromIdx, int toIdx, int colFromTo[]) {
        if (!_downloaded) return MAX_INT;
        final byte[] a0=_alignment[0], a1=_alignment[1];
        int iA=_aliStartsAtIdx, mismatch=0;
        if (colFromTo!=null) colFromTo[0]=-1;
        for(int i=0; i<a0.length && i<a1.length; i++) {
            if (is(LETTR,a0[i])) {
                if (iA>=fromIdx-2) {
                    if ((a0[i]|32)!=(a1[i]|32)) mismatch++;
                    if (colFromTo!=null) {
                        if (colFromTo[0]<0) colFromTo[0]=i;
                        colFromTo[1]=i+1;
                    }
                }
                if (iA++>toIdx) break;
            }
        }
        return mismatch;
    }
    /* <<< Getters <<< */
    /* ---------------------------------------- */
    /* >>> static >>> */
    private final static List<ReferenceSequence> _vDownload=new Vector();
    private static boolean _downloading;
    private static Object _map;
    private static Map<String,Protein> mapP() {
        Map<String,Protein> m=(Map)deref(_map);
        if (m==null) _map=newSoftRef(m=new HashMap());
        return m;
    }

}
