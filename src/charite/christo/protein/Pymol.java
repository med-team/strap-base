package charite.christo.protein;
import charite.christo.*;
import java.io.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.Protein3dUtils.*;

/**HELP

WIKI:PyMOL is a widely applied protein
structure viewer by Warren Delano.

<br><br><b>Residue selections:</b> Dragging the mouse over a part of
the amino acid sequence in the Strap alignment creates a
residue selection in Pymol.

The alignment cursor position refers to another Pymol selection named "cursor".

Users can refer to these selections in Pymol.  Using the <b>[S]</b>show or hide <b>[H]</b>hide  menus in the side panel
of Pymol the representations of the selected atoms can be specified like <I>sphere</I>, <I>cartoon</I> or
<I>stick</I>.

<br><br><b>Menu</b> The generic 3D-menu for the Pymol view is opened
by clicking the Pymol button which will appear in a separate small
panel. This pymol button and the Pymol menu-bar act as a drop target
for proteins and residue selections since proteins cannot directly
dropped into the Pymol application.

<br><br><b>Related links:</b> http://www.rubor.de/bioinf/ &nbsp;

http://www.rubor.de/anlagen/PyMOL_Tutorial.pdf

<br><br><b>Bugs:</b>
Pymol does not treat negative pdb residue numbers correctly.
Pymol selection is unspecific if the  chain identifier is a space character ' ';

<i>PACKAGE:charite.christo.strap.</i>
<i>SEE_DIALOG:V3dUtils,H</i>

@author Christoph Gille

 PyMOLWin.exe does not work replace it by PyMOL.exe

*/
public class Pymol implements ProteinViewer,ChRunnable,java.awt.event.ActionListener {
    public final static String
        DEFINE_THIS_COLOR="$DEFINE_THIS_COLOR",
        PYMOL_COLOR_NAME="$PYMOL_COLOR_NAME";

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Shared fields >>> */
    private Vector<String> shared_jobs;
    private String[] _shared_objects[];
    private ChExec _shared_exec;
    private UniqueList<Pymol> _shared_vView;

    /* <<< Shared fields <<< */
    /* ---------------------------------------- */
    /* >>> Constructor  >>> */
    public Pymol() {
        Protein3dUtils.initCustomizableSettings(this);
    }
    public String toString() { return getProtein()+"@"+super.toString();}
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Launch >>> */
    private boolean _isRunning;
    private void addPath(String fExe, ChExec ex) {
        if (ex==null) return;
        if (eqNz(fExe, downloadedBinary)) {
            final Map<String,String> m=new HashMap();
            final String parent=delLstCmpnt(fExe, isWin()?'\\':'/');
            if (isWin()) m.put("PATH", ChEnv.get("PATH")+";"+parent);
            m.put("DISPLAY", ChEnv.get("DISPLAY"));
            m.put("PYMOL_PATH",isMac() ?  dirBin()+"/MacPyMOL.app/pymol" : parent);
            ex.addToEnvironement(m);
        }
    }
    private boolean isWaitingForInput;
    private boolean launch(long options) {
        final boolean isWin=isWin(), isMac=isMac(), isLinux=isSystProprty(IS_LINUX386);
        final boolean usr_bin=fExists("/usr/bin/pymol");
        final List v=new ArrayList();
        boolean failedOpenDisplay=false;
        if (usr_bin) v.add("/usr/bin/pymol");
        else {
            if (_canInstall==null) _canInstall=new ChFrame("Pymol").ad(canInstall());
            _canInstall.shw(ChFrame.PACK|ChFrame.CENTER|ChFrame.ALWAYS_ON_TOP);
            if (isWin||isLinux||isMac) v.add(downloadedBinary);
            adAllUniq(Customize.getCustomizableForClass(getClass(), Customize.CLASS_ExecPath).getSettings(),v);
            v.add("pymol");
        }
        ChExec ex=null;
        for(String cmd0 : strgArry(v) ) {
            final String cmd[]=splitTokns(cmd0), fExe=cmd[0];//,cmd1=cmd.clone();
            putln(ANSI_GREEN+"fExe=",fExe,ANSI_RESET);
            final ChExec exProbe=new ChExec(0L).setCommandLineV(fExe,"-c");
            addPath(fExe, exProbe);
            exProbe.run();
            failedOpenDisplay|=exProbe.failedOpenDisplay();
            if (exProbe.exitValue()==0) {
                final long logPanel=isNextProcessHasLogPanel()?ChExec.STDOUT_IN_TEXT_BOX:0L;
                ex=new ChExec(ChExec.STDOUT|ChExec.STDERR|logPanel).setCommandLineV(cmd,"-p", "XXXXXXXXXX");
                ex.printlnStdout(this);
                addPath(fExe, ex);
                isWaitingForInput=false;
                if (0!=(options&SET_PROTEIN_VERBOSE)) ex.showStdoutAndStderr(null);
                startThrd(thrdCR(this,"EXEC_PYMOL",ex));
                for(int i=10; --i>=0 && !isWaitingForInput && !ex.finished(); ) sleep(555);
                if (!ex.finished() && ex.isRunning()) {
                    startThrd(thrdCR(this,"J"));
                    _shared_exec=ex;
                    break;
                }
                failedOpenDisplay|=ex.failedOpenDisplay();
            }
        }
        final String error=ex==null || ex.couldNotLaunch() ? "Pymol not started" :
            ex.finished() ? "Could start Pymol but the process already terminated" : null;
        if (error!=null) putln(RED_WARNING,"error=",error);
        if (error!=null && failedOpenDisplay) {
            error(
                  "Pymol: There might be problem running an external program.<br>"+
                  "It may help to deactivate display control by typing into a terminal shell"+
                  "<pre class=\"terminal\"> xhost +</pre>");
        }
        return error==null;
    }
    /* <<< Launch <<< */
    /* ---------------------------------------- */
    /* >>> ChRunnable >>> */
    public Object run(String id, Object arg) {
        if (id=="EXEC_PYMOL") {
            final ChExec ex=((ChExec)arg);
            _isRunning=true;
            ex.run();
            _isRunning=false;
            if (ex==_shared_exec) dispose();
        }
        if (id=="XXXXXXXXXX") {
            sleep(1111);
            if (!isWaitingForInput) {
                isWaitingForInput=true;
                interpretNow("from pymol import cmd");
                setVisblC(false, _canInstall, 0);
            }
        }
        if (id==ChRunnable.RUN_INTERPRET_LINE && arg!=null) {
            //putln("Pymol INTERPRET_LINE ",arg);
            final String s=toStrgTrim(arg);
            if (s.indexOf("delete ")>=0 || s.indexOf(" loaded as ")>=0) _shared_objects[0]=null;
            if (s.startsWith("ALL_OBJECTS")) {
                _shared_objects[0]=splitTokns(delPfx('[',delSfx(']',toStrgTrim(s.substring(11)))), chrClas(",'"));
            }
            if (s.indexOf("XXXXXXXXXX")>=0) {
                startThrd(thrdCR(this,"XXXXXXXXXX"));
            }
            if (s.startsWith("PyMOL: ") && s.indexOf("program termination.")>=0) {
                dispos(_shared_exec);
                if (_protein!=null) dispose();
            }
            if (strstr(STRSTR_IC, "You clicked", s)>=0) {
                for(Pymol pv : getViewersSharingViewV(false).asArray()) {
                    if (s.indexOf(" /"+pv._proteinName+"/")>=0) {
                        final int
                            i1=s.indexOf('/'),
                            i2=s.indexOf('/',i1+1),
                            i3=s.indexOf('/',i2+1),
                            i4=s.indexOf('/',i3+1),
                            L=sze(s);
                        if (i1>0 && L>i1+1 && i3+1<L && i4+1<L) {
                            final int
                                numStart=nxtE(DIGT,s, i4+1,MAX_INT),
                                insStart=nxt(-DIGT,s, numStart,MAX_INT);
                            final char chain=chrAt(i3+1, s);
                            Protein3dUtils.dispatchEvtPicked(pv, atoi(s,numStart), chain, chrAt(insStart,s), 0);
                        }
                        break;
                    }
                }
            }
        }
        if (id=="J") {
            while(_isRunning) {
                final Vector v=this.shared_jobs;
                final String s=(String)ChUtils.get(0,v);
                if (s!=null) {
                    remov(0,v);
                    interpretNow(s);
                }
                sleep(33);
                if ( (_shared_exec==null || !_shared_exec.isRunning()) && v!=null) v.clear();
            }
        }
        return null;
    }
    /* <<< ChRunnable <<< */
    /* ---------------------------------------- */
    /* >>> ActionEvent >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Protein protein=getProtein();
        if (cmd==ChExec.ACTION_TERMINATED  && _isRunning) {
            for(Protein p : Protein.proteinsInAlignment(protein)) {
                if (p==null) continue;
                for(ProteinViewer v : p.getProteinViewers()) {
                    if (v instanceof Pymol) new StrapEvent(v,StrapEvent.PROTEIN_VIEWER_CLOSED).run();
                }
            }
            _isRunning=false;
            shared_jobs.clear();
        }
        if (cmd=="DL") {
            final String vers=systProprty(SYSP_OS_VERSION);
            final String msg="This free Pymol binary for PPC may not work on a recent Mac OSX. You might need to get a current Pymol. Continue anyway?";
            if (isMac() && strEquls("10.",vers) && atoi(vers,3)>6 && !ChMsg.yesNo(msg)) return;

            final String fExe=downloadedBinary;
            synchronized(fExe) {
                if (0!=sze(file(fExe))) {
                    error("Please remove the old Pymol installation. Delete<pre> "+file(fExe).getParent()+"</pre>");
                } else {
                    final String zipFile=isWin() ? "PyMOL099" : isMac() ? "macpymol-0_99rc6" : "pymol-0_99rc6-bin-linux-x86-glibc23";
                    //putln("DL fExe="+fExe+"\nDL zipFile="+zipFile+"\nDL zipFile vor InteractiveDownload");
                    if (ChMsg.askPermissionInstall(urlOfBinariesForThisOS(),null)) InteractiveDownload.downloadFilesAndUnzip(Web.urls(urlOfBinariesForThisOS(),zipFile+".zip.gz"),dirBin());
                    final File fZip=file(dirBin()+"/"+zipFile+".zip");
                    if (ChZip.extractAll(ChZip.REPORT_ERROR|ChZip.SUGGEST_DEL, fZip,dirBin())) {
                        if (fExists(fExe)) {
                            mkExecutabl(file(fExe));
                            makeShellScript();
                        }
                        new ChFrame("Success").ad(pnl(iicon(IC_PYMOL),"Successfully installed pymol."))
                            .shw(ChFrame.PACK).shw(ChFrame.ALWAYS_ON_TOP|ChFrame.CENTER|CLOSE_CtrlW_ESC);
                        setVisblC(false, _canInstall, 0);
                    } else {
                        error("Error: could not install PyMOL "+fZip+" "+sze(fZip));
                        renamFile(fZip, fZip+".error");
                    }
                }
            }
        }
    }
    /* <<< ActionEvent  <<< */
    /* ---------------------------------------- */
    /* >>> Protein >>> */
    private String _proteinName;
    private Protein _protein;
    public Protein getProtein() {return _protein;}
    public boolean setProtein(long options, Protein p, ProteinViewer inSameView) {
        interpretNow("print");/* otherwise waitFor does not terminate */
        final File pdb=p!=null ? p.getFileMayBeWithSideChains() : null;
        if (pdb==null) return false;
        final Pymol pymol0=deref(inSameView,Pymol.class);
        final ChExec ex0=pymol0==null?null:pymol0._shared_exec;
        if (ex0!=null && ex0.isRunning()) {
            _shared_exec=ex0;
            _shared_vView=pymol0._shared_vView;
            _shared_objects=pymol0._shared_objects;
            this.shared_jobs=pymol0.shared_jobs;
        } else {
            _shared_objects=new String[1][];
            shared_jobs=new Vector();
            _shared_vView=new UniqueList(Pymol.class);
            launch(options);
            if (_shared_exec==null || !_shared_exec.isRunning()) {
                putln(RED_WARNING+"Could not launch PyMOL\n");
                return false;
            }
        }
        _shared_objects[0]=null;
        _proteinName=delSfx(".ent",delSfx(".pdb",filtrS('_'|FILTER_NO_MATCH_TO, FILENM, p.toString())));
        loadObject(pdb, _proteinName);
        for(HeteroCompound h: p.getHeteroCompounds('*')) {
            final String n=h.getNameForPymol();
            final File f=h.getFile();
            if (f==null) continue;
            loadObject(f, h.getNameForPymol());
            sendToPymol("hide everything, "+n+"; show "+ (h.isNucleotideChain()?"cartoon, ":"sticks, ")+n);
        }
        _shared_vView.add(this);
        startThrd(thread_setWndwStateT('I',0,0,"PyMOL Tcl/Tk GUI"));
        startThrd(thread_setWndwStateT('I',0,0,"The PyMOL Molecular Graphics System"));
        sleep(999);
        _protein=p;
        return true;
    }

    /* <<< Protein  <<< */
    /* ---------------------------------------- */
    /* >>> CommandInterpreter >>> */
    private final static String supported[]={
        COMMANDselect, COMMANDselection_name,
        COMMANDset_rotation_translation, COMMANDcenter,
        COMMANDspheres,COMMANDlines,COMMANDsticks,COMMANDribbons,
        COMMANDcartoon, COMMANDsurface,
        COMMANDcolor,
        COMMANDlabel,
        COMMANDcenter_amino
    };

    public void interpret(long options, String command) {
        final String id0=_selId, id=id0==null?"unnamed" :id0;
        final String word0=toStrgIntrn(fstTkn(command));
        final int L0=sze(word0);
        if (L0==0) return;
        final int para=nxtE(-SPC,command, L0,MAX_INT);
        final long color=rgbInScript(command);
        String send=idxOfStrg(word0, allCommands())>=0 ? null : rplcToStrg(PROTEIN,_proteinName,  rplcToStrg("COLOR","Color"+id,command));
        if (supports(word0,this)) {
            log3D(true,this,command);
            final boolean off=command.endsWith(" off");
            //  final String pfx=off ?"hide " : "hide everything, RESIDUES; show ";
            final String pfx=off ?"hide " : "show ";
            if (word0==COMMANDdots)         send=pfx+"dots,RESIDUES\n";
            else if (word0==COMMANDcenter_amino) send="reset;";
            else if (word0==COMMANDset_rotation_translation) { transform(matrixInScript(command)); send="";}
            else if (word0==COMMANDspheres) send=pfx+"spheres,RESIDUES\n";
            else if (word0==COMMANDmesh)    send=pfx+"mesh,RESIDUES\n";
            else if (word0==COMMANDsurface) send=pfx+"surface,RESIDUES\n";
            else if (word0==COMMANDlines)   send=pfx+"lines,RESIDUES\n";
            else if (word0==COMMANDsticks)  send=pfx+"sticks,RESIDUES\n";
            else if (word0==COMMANDribbons) send=pfx+"ribbon,RESIDUES\n";
            else if (word0==COMMANDcartoon) send=pfx+"cartoon,RESIDUES\n";
            else if (word0==COMMANDcolor)   send="color 0x"+rgbHex(color)+" ,RESIDUES\n";
            else if (word0==COMMANDlabel)   send="label RESIDUES, \""+(off?"": delSfx('"', delPfx('"', command.substring(para))))+"\"";
            else if (word0==COMMANDselect) {
                send=selectCmd(command.substring(para));
            } else if (word0==COMMANDselection_name) {
                _nextSelId=command.substring(para);
                send="";
            }
        }
        if (sze(send)>0) {
            sendToPymol(rplcToStrg(RESIDUES,id, send));
        }
    }

    private void sendToPymol(final String cmd0) {
        if (cmd0==null) return;
        final String cmd=rplcToStrg(PROTEIN,_proteinName,cmd0).trim();
        String startsWith=null;
        for(int i=0; i<3; i++) {
            final String s=i==0?"select cursor " : i==1 ? "select selected " : "cmd.enable(";
            if (cmd.startsWith(s)) startsWith=s;
        }
        if (startsWith!=null) {
            for(String s: strgArry(shared_jobs)) {
                if (s.startsWith(startsWith) || "reset".equals(cmd) && "reset".equals(s))  shared_jobs.remove(s);
            }
        }
        log3D(false, this,cmd);
        shared_jobs.add(cmd);
    }
    private String interpretNow(String s) {
        final ChExec ex=_shared_exec;
        if (ex!=null) {
            try {
                _shared_exec.write(s);
                _shared_exec.write("\r\n");
            } catch(IOException e){}
        }
        return "";
    }
    /* <<< CommandInterpreter <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    private boolean _disposed;
    public void dispose() {
        if (!_disposed) {
            _disposed=true;
            interpretNow("delete "+_proteinName);
            interpretNow("delete "+_proteinName+"_*");
            _shared_objects[0]=null;
            if (_shared_vView!=null) _shared_vView.remove(this);
            new StrapEvent(this,StrapEvent.PROTEIN_VIEWER_CLOSED).run();
        }
    }
    /* <<< Disposable <<< */
    /* ---------------------------------------- */
    /* >>> Object Management >>> */

    public void loadObject(File f, String n0) {
        final String path=toStrg(f);
        if (path!=null && n0!=null) {
            final String n=rplcToStrg(PROTEIN,_proteinName, n0);
            interpretNow("if not '"+n+"' in cmd.get_names('objects'): cmd.load( '"+path.replace('\\','/')+"' , '"+n+"')");
        }
    }
    /* <<< Object Management <<< */
    /* ---------------------------------------- */
    /* >>> Customize >>> */
    public static String getCustomizableExamples() {
        return
            "show dots,RESIDUES\n"+
            "show spheres,RESIDUES\n"+
            "show mesh,RESIDUES\n"+
            "show surface,RESIDUES\n"+
            "show lines,RESIDUES\n"+
            "show sticks,RESIDUES\n"+
            "show ribbon,RESIDUES\n"+
            "show cartoon,RESIDUES\n"+
            "color magenta, RESIDUES\n"+
            DEFINE_THIS_COLOR+", color "+PYMOL_COLOR_NAME+",+ RESIDUES\n";
    }
    public static String getCustomizableInitCommands() {
        return
            "hide everything,PROTEIN\n"+
            "show cartoon, PROTEIN\n"+
            "show spheres , PROTEIN and Het and not resn hoh and not resn hho and not  resn ohh and not resn h2o\n"+
            "show lines, (PROTEIN and name ca,c,n)\n"+
            "hide everything,PROTEIN and (resn hoh or resn hho or resn ohh or resn h2o) ";
    }
    public static String getCustomizableLateCommands() {
        return
            "select allWater , resn hoh or resn hho or resn ohh or resn h2o\n"+
            "select allHeteros , Het and not allWater\n"+
            "select allDNA , resn DA or resn DC or resn DG or resn DT  \n"+
            "select allRNA , resn A or resn C or resn G or resn T or resn U  \n"+
            "set selection_width=10\n"+
            "reset";
    }

    public static String getCustomizableExecPath() {
        return
            isWin() ? "%ProgramFiles%/Delano%20Scientific/PyMOL/PyMOLWin.exe" :
            isMac() ?
            "/Applications/PyMOL.app/Contents/MacOS/PyMOL\n"+
            "/Applications/MacPyMOL.app/Contents/MacOS/MacPyMOL\n"+
            "/Applications/PyMOLX11Hybrid.app/Contents/MacOS/PyMOL\n" :
            "pymol";
    }
    /* <<< Customize <<< */
    /* ---------------------------------------- */
    /* >>> TransformsCoordinates  >>> */
    private void transform(Matrix3D m3d) {
        final Protein p=getProtein();
        if (p==null) return;
        for(HeteroCompound h :p.getHeteroCompounds('*')) {
            if (h.getFile()!=null) transformObject(m3d,h.getNameForPymol());
        }
        transformObject(m3d, _proteinName);
    }
    //    private final Matrix3D MATRIX=new Matrix3D();
    private void transformObject(Matrix3D m3d, String n) {
        sendToPymol("matrix_reset "+n);
        if (m3d!=null) {
            sendToPymol("if '"+n+"' in cmd.get_names('objects'): cmd.transform_object('"+n+"',( "+m3d.toText(Matrix3D.FORMAT_PYMOL,null,null)+" ) )");
        }
    }

    /* <<< TransformsCoordinates <<< */
    /* ---------------------------------------- */
    /* >>> Installation >>> */
    private void makeShellScript() {
        if (sze(file(downloadedBinary))==0) return;
        final String
            downlDir= dirBin()+(isWin() ? "\\PyMOL099\\" : "/pymol/"),
            txt=
            isWin() ? "set PYMOL_PATH="+downlDir+"\r\nset PATH=%PATH%;%PYMOL_PATH%\r\n"+downloadedBinary +" %* \r\n":
            isMac() ? "#!/bin/bash\nexport PYMOL_PATH="+dirBin()+"/MacPyMOL.app/pymol\n"+downloadedBinary +" $* & \n":
            " export PYMOL_PATH="+downlDir+"\n"+downloadedBinary +" $* \n";

        wrte(file(downlDir+"/Pymol."+ (isWin() ? "bat" : isMac() ? "command" : "sh")),txt);
    }

   private final String downloadedBinary=
        dirBin()+(
                  isWin() ? "\\PyMOL099\\PyMOL.exe" :
                  isMac() ? "/MacPyMOL.app/Contents/MacOS/MacPyMOL" :
                  "/pymol/pymol.exe");
    private static ChFrame _canInstall;

    public static Object canInstall() { /* !public */
        final Pymol p=new Pymol();
        final String instCmd=installCmdForPckgs("pymol");
        return pnl(VBHB,
                   instCmd==null?null: "To install Pymol run the following command line in a root shell",
                   instCmd==null?" ":  ANSI_TERMINAL+instCmd,
                   "Strap can install Pymol automatically (13MB)", new ChButton("DL")
                   .cp(KEY_AVOID_GARBAGE_COLLECT,p)
                   .t("Download & install").li(p),
                   isMac() ? "This PPC version might not work on Mac 10.7 (Lion) or higher":null);
    }
    /* <<< Installation <<< */
    /* ---------------------------------------- */
    /* >>> ProteinViewer methods >>> */

    public UniqueList<Pymol> getViewersSharingViewV(boolean proxies) {
        return _shared_vView;
    }

    private final Object FLAGS=new Long(PROPERTY_MULTI_VIEW | PROPERTY_NAMED_SELECTIONS|PROPERTY_MULTI_MOLECULE|PROPERTY_EXTERNAL_PROCESS|PROPERTY_SEQUENCE_CURSOR_DELAYED);
    public void setProperty(String id, Object value) {
    }
    public Object getProperty(String getID) {
        if (getID==GET_FRAME_TITLE) return "PyMOL Viewer";
        if (getID==GET_ATOM_SELECTION_EXAMPLE) return " /////CA or /////CB ";
        if (getID==GET_SUPPORTED_COMMANDS) return supported;
        if (getID==GET_FLAGS) return FLAGS;
        return null;
    }
    /* <<< ProteinViewer methods <<< */
    /* ---------------------------------------- */
    /* >>> ResidueSelection >>> */
    private String _selId, _nextSelId;
    private String selectCmd(String expr) {
        final Selection3D atoms[]=textToSelection3D(expr, getProtein());
        final String sel=selection3dToText(PYMOL,atoms);
        final String id=makeSelId(atoms, _selId=_nextSelId);
        _nextSelId=null;
        return new BA(99).a("select ").a(id).a(" = ( /PROTEIN ").and("and",sel).a(" )\r\n").toString();
    }
    /* <<< ResidueSelection <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

}

// http://biodesign.asu.edu/labs/assets/lindsay/molvis/MOL.html
/*
  if 'pdb1a34_mtx42' in cmd.get_names('objects'): matrix_reset pdb1a34_mtx42
  See mgsl
*/
