package charite.christo.protein;
/**HELP

Proteins are stored in plain text files. A typical error is to save  protein files as <b>MS-Word</b>-Document.

Recognition of file formats in Strap works by try-and-error disregarding of the file suffix.

Only if all specific parsers cannot deal with the protein
text, the StupidParser is employed which assumes all letters in the
file to be amino acids.

<i>SEE_CLASS:PDB_Parser</i>
<i>SEE_CLASS:DSSP_Parser</i>
<i>SEE_CLASS:NumberedSequence_Parser</i>
<i>SEE_CLASS:ProteinParser</i>
<i>SEE_CLASS:SingleFastaParser</i>
<i>SEE_CLASS:StupidParser</i>
<i>SEE_CLASS:XML_SequenceParser</i>
<i>SEE_CLASS:SwissHeaderParser</i>

@author Christoph Gille
*/
public interface ProteinParser {
    long IGNORE_SEQRES=1, SIDE_CHAIN_ATOMS=1<<1, SEQUENCE_FEATURES=1<<2;
/**
       @return true: success, false: inappropriate file format
       @param text the entire file contents
       It is a byte array and not a String Object for performance reasons.
*/
    boolean parse(Protein p, long options, charite.christo.BA text);
}
