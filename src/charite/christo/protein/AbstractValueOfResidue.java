package charite.christo.protein;
import charite.christo.*;
import java.awt.Color;
public abstract class AbstractValueOfResidue implements ValueOfResidue, Colored { // , StrapListener
    private Protein _p;
    protected double _v[];
    protected int _mc;
    private Object _shared;

    public Protein getProtein() {return _p;}
    public void setProtein(Protein p) { if (_p!=p) _v=null; _p=p; }

    public int mc() { return _mc;}

    public void setSharedInstance(Object shared) { _shared=shared;}
    public Object getSharedInstance() { return _shared; }

    private Color _color=Color.PINK;
    public Color getColor() { return _color;}
    public void setColor(Color c) { _color=c;}

    // public void handleEvent(StrapEvent ev){
    //     if ( (ev.getType()&StrapEvent.FLAG_RESIDUE_TYPES_CHANGED)!=0) {
    //         final StrapAlign align=(StrapAlign)ev.getProteinAlignment();
    //         align.dispatchLater(StrapEvent.VALUE_OF_RESIDUE_CHANGED,111);
    //     }
    // }
}
