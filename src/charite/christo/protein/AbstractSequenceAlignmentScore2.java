package charite.christo.protein;

/**HELP
   This class computes the similarity of two aligned sequences using the Blosum62 similarity matrix and a gap creation and extension penelty
*/
public abstract class AbstractSequenceAlignmentScore2 implements SequenceAlignmentScore2, StrapListener {
    private boolean _valid;
    private double _v;
    private byte[] _s1,_s2;
    private int _upstreamMC, _from, _to;

    public void setSequences(int mc, byte[] s1, byte[] s2, int fromCol, int toCol) {
        if ( _from!=fromCol || _to!=toCol || _s1!=s1 || _s2!=s2 || mc!=_upstreamMC) _valid=false;
        _s1=s1;
        _s2=s2;
        _upstreamMC=mc;
        _from=fromCol;
        _to=toCol;

    }

    public void compute() {}
    public double getValue() {
        if (!_valid) {
            _valid=true;
            _v=computeScore(_s1, _s2, _from, _to);
        }
        return _v;

    }

    public abstract double computeScore(byte[] s1, byte[] s2, int fromCol, int toCol);

    public void handleEvent(StrapEvent e) {
        if (e.getType()==StrapEvent.ALIGNMENT_CHANGED) {
            StrapEvent.dispatchLater(StrapEvent.PROTEIN_PROTEIN_DISTANCE_CHANGED, 333);
        }
    }

}
