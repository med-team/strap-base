package charite.christo.protein;
import java.util. *;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
     Write a protein file
     @author Christoph Gille
*/
public class ProteinWriter1 implements ProteinWriter {
    private String _fileExt;
    private int _from, _to=MAX_INT;
    private boolean _selected[];
    /** do not write all residues but only a certain subset.*/
    public void selectResidues(boolean isSelected[]) { _selected=isSelected;}
    public String getFileSuffix() { return _fileExt;}
    public void setResidueRange(int from, int to) { _from=from; _to=to;}
    public boolean toText(Protein p, Matrix3D mm3d[], long options, BA sb) {
        final byte res[]=p.getResidueType();
        final int nR=p.countResidues(), to=mini(nR,_to);
        if ( (options&FASTA)!=0) {
            _fileExt=".fa";
            sb.a('>').aln(p);
            if ( (options&NUCLEOTIDES)!=0) {
                sb.a(DNA_Util.onlyTranslated(p.getNucleotidesCodingStrand(),p.isCoding()), 3*_from, 3*to);
            } else {
                sb.a(res, _from, to);
            }
            sb.a('\n');
        }
        if ( (options&PDB)!=0) {
            final char replaceChain=  0!=(options&CHAIN_A)?'A' : 0!=(options&CHAIN_B)?'B' : 0!=(options&CHAIN_SPACE) ? ' ' :  0;
            _fileExt=".pdb";
            sb.enlargeCapacityBy(999*1000,130);
            final boolean all= (options&SIDE_CHAIN_ATOMS)!=0 && p.getAtomCoordinates()!=null;
            final byte
                rChain[]=p.getResidueChain(),
                insCode[]=0!=(options&IDX_FOR_RESNUM) ? null : p.getResidueInsertionCode(),
                atomType[]=p.getAtomType();
            final int
                resNo[]=0!=(options&IDX_FOR_RESNUM) ? null : p.getResidueNumber(),
                aType32[]=p.getAtomName32(),
                residueFirstAtomIdx[]=p.getResidueFirstAtomIdx(),
                residueLastAtomIdx[]=p.getResidueLastAtomIdx(),
                atomNumber[]=p.getAtomNumber();
            final float[] tempFactor=p.getAtomBFactor(), occupancy=p.getAtomOccupancy();

            if ( (options&SEQRES)!=0) seqres(options,p,null,sb);
            if ( (options&SEQRES_IF_COORDINATES)!=0) seqres(options,p,p.getResidueCalpha(),sb);
            if ( (options&HELIX_SHEET)!=0) p.helixSheet(_from, to,_selected,sb);
            if ( (options& (ATOM_LINES|PDB))!=0) {
                final int n3d=mm3d!=null?mm3d.length:0;
                for(int iM=0; iM< (n3d>0?n3d:1); iM++) {
                    final Matrix3D m3d=iM<n3d?mm3d[iM]:null;
                    if (m3d==null && iM>0) continue;
                    final float xyz[]=all ? p.getAtomCoordinates(m3d) : p.getResidueCalpha(m3d);
                    if (xyz==null) continue;

                    for(int iAa=0; iAa<to; iAa++) {
                        if (_selected!=null && (iAa>=_selected.length || !_selected[iAa]) ||  iAa<_from) continue;
                        final int iStart=all ? residueFirstAtomIdx[iAa] : 0;
                        final int iEnd=all ? residueLastAtomIdx[iAa] : 0;
                        if (iStart<0) continue;
                        final char iCode=is(LETTR_DIGT,insCode,iAa)? (char)insCode[iAa]:' ';
                        for(int iA=iStart; iA<=iEnd;iA++) {
                            final int i3=3* (all?iA:iAa);
                            if (xyz.length<=i3+2) break;
                            final float x=xyz[i3];
                            if (Float.isNaN(x)) continue;
                            final int thisAtomNumber=all? (atomNumber!=null && iA<atomNumber.length ? atomNumber[iA] : -1):iAa+1;
                            sb.a("ATOM  ").a(thisAtomNumber,5).a(' ').aSomeBytes(all ? aType32[iA] : ATOM_CA,4).a(' ');
                            if ( (options&MET_INSTEAD_OF_MSE)!=0) sb.a(Protein.toThreeLetterCode(res[iAa])).a(' ');
                            else sb.aSomeBytes(p.getResidueName32(iAa),4);

                            sb.a(replaceChain!=0 ? replaceChain: (char)rChain[iAa])
                                .a(iAa<sze(resNo)? resNo[iAa] : iAa+1, 4)
                            .a(iCode).a(' ',3)
                            .a(x,4,3).a(xyz[i3+1],4,3).a(xyz[i3+2],4,3)
                            .a(all ? occupancy[iA]:1,3,2).a(all?tempFactor[iA]:0,3,2).a(' ',11)
                            .a(!all ? 'C' : atomType!=null && iA<atomType.length ? (char)atomType[iA]:' ').a('\n');
                        }
                    }
                    if ( (options&NUCLEOTIDE_STRUCTURE)!=0) for(HeteroCompound dna : p.getHeteroCompounds('N'))  dna.toText(0, m3d,sb);
                    if ( (options&HETEROS)!=0)  for(HeteroCompound ha:p.getHeteroCompounds('H')) ha.toText(0, m3d,sb);
                    if (n3d>0) sb.aln("TER\n");
                }
            }
        }
        return true;
    }
    /*
      SEQRES  19 A  275  TYR PRO ILE THR LYS HIS VAL ARG LEU TYR THR GLN VAL\n";
    */
    private void seqres(long options, Protein p, float xyz[], BA sb) {
        final char replaceChain=  0!=(options&CHAIN_A)?'A' : 0!=(options&CHAIN_B)?'B' : 0!=(options&CHAIN_SPACE) ? ' ' :  0;
        final byte[] rType=p.getResidueType(), cc=p.getResidueChain();
        final int nR=p.countResidues();
        byte c=-1;
        final int to=mini(nR,_to);
        int lastIdx=0;
        for(int iA0=_from; iA0<=to; iA0++) {
            final byte newChain= cc!=null && iA0<cc.length && cc[iA0]!=0 ? cc[iA0] : 32;
            if (newChain!=c || iA0==to) {
                if (c>=0) {
                    int total=-1;
                    while(true) {
                        int count=0,iLine=0;
                        for(int iA=lastIdx ; iA<iA0; iA++) {
                            if (_selected!=null && (iA>=_selected.length || !_selected[iA])) continue;
                            if (xyz!=null && ( iA*3>=xyz.length || Float.isNaN(xyz[iA*3]))) continue;
                            if (total>=0) {
                                if (count%13==0) {
                                    if (iLine>0) sb.a('\n');
                                    sb.a("SEQRES").a(++iLine,4).a(' ').a(replaceChain!=0?replaceChain:(char)c).a(total,5).a(' ',2);
                                }
                                sb.a(Protein.toThreeLetterCode(iA<nR ? rType[iA] : 0)).a(' ');
                            }
                            count++;
                        }
                        if (total<0) total=count; else break;
                    }
                    sb.a('\n');
                }
                c=newChain;
                lastIdx=iA0;
            }
        }
        sb.a('\n');
    }

    private static boolean createPairfitTarget_sel[];
    private final static int[] createPairfitTarget_atomIdx=new int[3];
    public final static String createPairfitTarget(Protein p, Matrix3D m, BA ba) {
        final int nR=p.countResidues();
        final int[] indices=createPairfitTarget_atomIdx;
        if (nR==1) {
            indices[0]=0;
            indices[1]=indices[2]=-1;
        } else  Matrix3D.mostDistantPoints3(p.getResidueCalpha(), nR, indices);
        final boolean[] sel=createPairfitTarget_sel=redim(createPairfitTarget_sel, nR,99);
        Arrays.fill(sel,false);
        for(int i : indices)   if (i>=0) sel[i]=true;

        final ProteinWriter1 pw=new ProteinWriter1();
        pw.selectResidues(sel);
        pw.toText(p,new Matrix3D[]{m},ProteinWriter.PDB|ProteinWriter.ATOM_LINES,ba);
        final BA baPymol=new BA(99).a(p).a('/',3);
        for(int idx : indices) {
            if (idx==0)  continue;
            baPymol.a(p.getResidueNumber(idx));
            final int insert=p.getResidueInsertionCode(idx);
            if (insert>0) baPymol.a((char)insert);
        }
        return baPymol.a("/ca").toString();
    }

    public final static BA mergePdbWithOriginal(Protein p) {
        final BA baOrig=readBytes(p.getFileMayBeWithSideChains(), null);
        final byte[] T=baOrig.bytes();
        final int E=baOrig.end();
        final long[] fGap=new long[3], tGap=new long[3];
        boolean hasSideChains=false;
        for(int i=E-15; --i>=0; ) {
            if (i>0 && T[i-1]!='\n') continue;
            for(int j=3; --j>=0;) {
                if (strEquls(j==2?"ATOM " :   j==1?"HELIX " : "SEQRES ",T,i) ||
                    strEquls(j==2?"HETATM " : j==1?"SHEET ":  null ,    T,i)) {
                    fGap[j]=i;
                    if (tGap[j]==0) tGap[j]=mini(E, 1+strchr(STRSTR_E,'\n', T, i, E));
                    hasSideChains=hasSideChains || T[i]=='A'  && strEquls("ATOM ", T,i) && T[13+i]=='C' && T[14+i]=='B' && T[15+i]==' ';
                }
            }
        }
        if (hasSideChains && p.getAtomCoordinates()==null)  p.loadSideChainAtoms(baOrig);

        if (fGap[1]==0) fGap[1]=tGap[1]=fGap[2];
        if (fGap[0]==0) fGap[0]=tGap[0]=fGap[2];

        final BA ba=new BA(maxi(sze(baOrig), p.countAtoms()*80));
        final Matrix3D mm3d[]={p.getRotationAndTranslation()};
        final ProteinWriter1 pw=new ProteinWriter1();
        if (!(  ((PDB|ATOM_LINES|SIDE_CHAIN_ATOMS|HETEROS|NUCLEOTIDE_STRUCTURE) & 0xffff0000)==0)) assrt();
        final long[] sorted={
            (fGap[0]<<40) | (tGap[0]<<16) | PDB|SEQRES,
            (fGap[1]<<40) | (tGap[1]<<16) | PDB|HELIX_SHEET,
            (fGap[2]<<40) | (tGap[2]<<16) | PDB|ATOM_LINES|SIDE_CHAIN_ATOMS|HETEROS|NUCLEOTIDE_STRUCTURE
        };
        Arrays.sort(sorted);
        for(int i=sorted.length; --i>=0;) {
            final long r=sorted[i];
            final int options=(int)r&0xFFFF;
            final int f= (int)((r>>>40)&0xffFFff);
            final int t= (int)((r>>>16)&0xffFFff);
            pw.toText(p, mm3d, options, ba.clr());
            baOrig.replaceRange(f,t,ba);
        }
        if (mm3d[0]!=null) mm3d[0].toText(Matrix3D.FORMAT_EXPLAIN, "REMARK     STRAP ",baOrig.a('\n'));
        return baOrig;
    }

}
