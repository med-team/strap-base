package charite.christo.protein;
/**
   Classes that provide a prediction of coiled coils.
   The letter X is return for sequence positions that are predicted to be coiled coils.
*/
public interface CoiledCoil_Predictor extends PredictionFromAminoacidSequence  {
}

