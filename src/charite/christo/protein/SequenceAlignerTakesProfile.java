package charite.christo.protein;
public interface SequenceAlignerTakesProfile extends SequenceAligner  {
   void addProfile(byte ss[][]);
   int getMaxNumberOfProfiles();
}
