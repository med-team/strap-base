package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;

/**HELP
 The fasta format is characterized by a <b>greater than</b> character followed by the header ( WIKI:FASTA_format File_example:fasta ).
*/
public class SingleFastaParser implements ProteinParser {
    public boolean parse(Protein prot, long options, BA ba) {
        final byte[] T=ba.bytes();
        final int E=ba.end(), B=ba.begin();
        if (E-B<4 || T[B]!='>') return false;
        int nl=B+1, nameLength=0;
        final int idEnd=nxt(-UPPR_DIGT_COLON, T,B+1, E);
        if (idEnd-B+1>=4) {
            final String
                id=bytes2strg(T,B+1, idEnd),
                db=guessDbFromId(id),
                ref=id.indexOf(':')>0||sze(db)==0?id:db+id;
            prot.addSequenceRef(ref);
        }

        while(nl<E) {
            final byte c=T[nl];
            if (c=='\n') {
                final int firstIdx=strstr(STRSTR_AFTER,"FIRSTINDEX=",T,B,nl);
                if (firstIdx>0) prot.setResidueIndexOffset(atoi(T,firstIdx,E));
                int idxPDB=B;
                while(  (idxPDB=strstr(STRSTR_AFTER,"PDB:",T,idxPDB,nl))>0) {
                    int endPdb=idxPDB;
                    while(endPdb<nl && (is(LETTR_DIGT,T[endPdb])||T[endPdb]==':')) endPdb++;
                    prot.addSequenceRef(bytes2strg(T,idxPDB-4,endPdb));
                }
                final int idxIDs=strstr(STRSTR_AFTER,"ALL_IDS=",T,B,nl);
                if (idxIDs>0) {
                    int to=idxIDs;
                    while(to<nl && T[to]!=' ') to++;
                    for (String s : bytes2strg(T,idxIDs,to).split(",")) {
                        prot.addSequenceRef(s);
                    }
                }
                break;
            }
            if (is(LETTR_DIGT,c)) nameLength++;
            nl++;
        }

        if (nl>=E || nameLength==0) return false;
        final byte[] aa=toByts(filtr(0L, LETTR, T, nl, E));

        final int keggOrganis=strstr(STRSTR_AFTER,"<title>KEGG ",T,0,E);
        if (keggOrganis>0) {
            prot.setOrganism(ba.newString(keggOrganis, strchr(':',T,keggOrganis,keggOrganis+99)));
            final int spc=nxtE(SPC,T,B+1,nl);
            //if (spc>0) prot.setAccessionID(( onlyACTGUN(aa,0,MAX_INT) ? "KEGG_NT:" : "KEGG_AA:") + ba.newString(B+1,spc));
            if (spc>0) prot.setAccessionID(( nxt(-ACTGUN,aa)<0 ? "KEGG_NT:" : "KEGG_AA:") + ba.newString(B+1,spc));

            final int semicolon1=strchr(';',T,spc,nl);
            prot.setCompound(ba.newString(nxt(-SPC,T,semicolon1+1,nl), strchr(STRSTR_E,';',T,semicolon1+1,nl)));
            final int ec=strstr(STRSTR_AFTER,"(EC:",T,B,nl);
            if (ec>0) prot.setEC(ba.newString(ec, strchr(')',T,ec,nl)));
        }

        if (aa.length>0) { prot.setResidueType(aa); return true;}

        return false;
    }
}
