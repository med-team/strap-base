package charite.christo.protein;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.ChSoap.*;
import charite.christo.blast.*;
/**
   @author Christoph Gille
*/
public class FindUniprot {
    public final static int BLASTER_MASK=15, BLAST_EBI=1, BLAST_NCBI=2, MAX_BLASTER=2, FETCH_AFTER=1<<4, MASK=255;
    static {assert 0==(BLASTER_MASK&FETCH_AFTER); }

    public final static String KEY="FUIP$$K", KEYmc="FUIP$$Kmc";

    private FindUniprot(){}
    public static Runnable thread_load(int opt, Protein pp[], ChRunnable log) {
        return thrdM("load", FindUniprot.class, new Object[]{intObjct(opt), pp,log});
    }
    public static void load(int opt, Protein pp[], ChRunnable log) {
        if (sze(pp)==0) return;
        boolean changed=false;
        for(int iP=0; iP<pp.length; iP++) {
            final Protein p=pp[iP];
            if (p==null || p.getUniprotID()!=null ) continue;
            if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(FindUniprot.class, (iP+1)*100/(pp.length+1),"FindUniprot: "+p+" ..."));
            load1(opt,p, log);
            changed=true;
        }
        if (changed && 0!=(opt&FETCH_AFTER)) fetchSoap(0L, pp);
        if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(FindUniprot.class,0,"FindUniprot: "+pp.length+" proteins done"));
    }
    public static String[] ids(Protein p) {
        final String sss[][]=(String[][])gcp(KEY, p);
        return sss==null ? NO_STRING : sze(sss[0])>0 ? sss[0] : sze(sss[1])>0 ?  sss[1]  :  NO_STRING;
    }
    public static String[] load1(int opt, Protein p, ChRunnable log) {
        final BA log1=new BA(99).setSendTo(withGui() ? BA.SEND_TO_NULL : BA.SEND_TO_STDOUT);
        final String pfx="Find uniprot ID "+p+":  ";
        final String seq=p==null || p.getUniprotID()!=null?null:p.getResidueTypeFullLengthAsString();
        if (sze(seq)<3) return NO_STRING;
        final int mc=p.mc(ProteinMC.MC_RESIDUE_TYPE_FULL);
        if (mc!=atoi(gcp(KEYmc,p))) {
            pcp(KEYmc, intObjct(mc), p);
            pcp(KEY,null,p);
        }
        String sss[][]=(String[][])gcp(KEY, p);
        if (sss==null) pcp(KEY,sss=new String[2][],p);
        final int FAST=0, SLOW=1;
        String ss[]=sss[FAST];
        final String pdbID=p.getPdbID();
        if (sze(ss)==0 && p.getUniprotID()==null && pdbID!=null) {
            synchronized(mkIdObjct("FUID$$p2u",p)) {
                log1.a(pfx).a(" pdb2uniprot ...").send();
                final String id=DAS.pdb2uniprot(false, pdbID,chrAt(0, p.getChainsAsString()), log);
                log1.a(" pdb2uniprot ").aln(sze(ss)>0 ? GREEN_SUCCESS:GREEN_DONE).send();
                if (id!=null) {
                    p.setUniprotID(id);
                    ss=new String[]{id};
                }
            }
        }
        if (sze(ss)==0) {
            synchronized(mkIdObjct("FUID$$picr",p)) {
                log1.a(pfx).a(" Picr ...").send();
                ss=PicrEBI.idsForSequence(false, PicrEBI.UNIPROT, seq);
                log1.a(" Picr ").aln(sze(ss)>0 ? GREEN_SUCCESS:GREEN_DONE).send();
            }
        }
        if (sze(ss=rmNullS(ss))>0) return sss[FAST]=addPrefix(ss);

        ss=sss[SLOW];

        final int blaster=opt&BLASTER_MASK;
        if (ss==null && blaster!=0 && sze(seq)>16) {
            log1.a(pfx).aln(" Blast ...").send();
            final SequenceBlaster b=blaster==BLAST_EBI ? new Blaster_SOAP_ebi() : new Blaster_web_ncbi();
            if (myComputer() && blaster==BLAST_EBI) return NO_STRING;
            b.setAAQuerySequence(seq);
            b.setDatabase(blaster==BLAST_EBI ? "uniprotkb" : "swissprot");
            b.setSensitivity(1);
            b.setWordSize(5);
            b.setNumberOfAlignments(10);
            synchronized(mkIdObjct("FUID$$blast"+blaster,p)) {
                final BlastResult br=BlastResult.newInstance(b, "Find Uniprot "+p, IC_UNIPROT);
                if (br!=null) {
                    int count=0;
                    final BlastHit[] hh=br.getHits();
                    ss=new String[hh.length];
                    for(BlastHit h : hh) {
                        for(BlastAlignment a : h.getAlignments()) {
                            if (strEquls(STRSTR_IC, seq, a.getMatchSeq())) {
                                ss[count++]=delLstCmpnt(h.getID(),'-');
                                break;
                            }
                        }
                    }
                }
                if (sze(ss=rmNullS(ss))>0) sss[SLOW]=ss;
            }
            log1.a(pfx).aln(" Blast ").a(sze(ss)>0 ? GREEN_SUCCESS:GREEN_DONE).send();
        }
        return ss==null?NO_STRING:addPrefix(ss);
    }
    private static String[] addPrefix(String[] ss) {
        if (ss==null) return null;
        for(int i=ss.length; --i>=0;) ss[i]=addPfx("UNIPROT:",ss[i]);
        return ss;
    }
    private final static Set<String> _vFetched=new HashSet();
    private static void fetchSoap(long opt, Protein pp[]) {
        if (sze(pp)==0) return;
        synchronized(_vFetched) {
            final Set<String> v=new HashSet();
            for(int iP=sze(pp); --iP>=0;) {
                final Protein p=pp[iP];
                if (p==null) continue;
                v.add(p.getAccessionID());
                adAll(p.getDatabaseRefs(), v);
                adAll(p.getSequenceRefs(), v);
            }
            v.removeAll(_vFetched);
            FetchSeqs.download(opt|FetchSeqs.SKIP_EXISTING, strgArry(v));
        }
    }

}
