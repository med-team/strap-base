package charite.christo.protein;
/**
   Classes that provide a prediction of membrane spanning helices.
   A container of GUI elements for the user is provided on pressing <i>ICON:IC_CONTROLPANEL</i> 
   when the class implements  {@link HasControlPanel HasControlPanel}. The GUI elements may be used to select  gap-panalties or BLOSUM matrices.
*/
public interface TransmembraneHelix_Predictor extends PredictionFromAminoacidSequence  {
}
