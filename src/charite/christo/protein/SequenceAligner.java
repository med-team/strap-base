package charite.christo.protein;
/**
   Classes that provide a calculation of sequence alignments.
   <i>PACKAGE:</i>
   <i>PACKAGE:charite.christo.</i>
   With <i>JAVADOC:SequenceAligner#setSequences(String[])</i> the amino acid sequences of the proteins are set.
   Alignment starts on invoking <i>JAVADOC:SequenceAligner#compute()</i>
   STRAP retrieves the result by calling <i>JAVADOC:SequenceAligner#getAlignedSequences()</i>.

   <i>JAVADOC:SequenceAligner#getOptions()</i>may rcontain the flag PROPERTY_ONLY_TWO_SEQUENCES
   if the procedure is capable of aligning only pairs of sequences.

   A container of GUI elements for the user is provided on pressing <i>ICON:IC_CONTROLPANEL</i>
   when the class implements  <i>JAVADOC:HasControlPanel</i>. The GUI elements may be used to select  gap-panalties or BLOSUM matrices.
*/
public interface SequenceAligner {
    public final static String KOPT_NOT_USE_STRUCTURE="NOT_USE_STRUCTURE_FOR_ALIGNMENT";

    long OPTION_USE_SECONDARY_STRUCTURE=1<<0, OPTION_CHECK_PERFECT_MATCH=1<<1,  OPTION_NOT_TO_SECURITY_LIST=1<<2;
    long PROPERTY_LOCAL_ALIGNMENT=1<<1, PROPERTY_ONLY_TWO_SEQUENCES=1<<2;
    float SCORE_FOR_PERFECT_MATCH=7777;
    void setSequences(byte[]... ss);

    /**
       Return the sequences that have been set with setSequences(String[])
    */

    byte[][] getSequences();
    /**
       The time consuming computation.
    */
    void compute();
    //    void setOptions(long flags);
    long getPropertyFlags();

    byte[][] getAlignedSequences();
    /**
       Multiple aligners are usually not limited in the number of sequences.
       Pair aligners should return "2".
       See <i>JAVADOC:charite.christo.strap.extensions.MultiFromPairAligner</i>
    */
    void setOptions(long optionFlags);
    long getOptions();

}
