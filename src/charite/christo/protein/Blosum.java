package charite.christo.protein;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class Blosum {
    private Blosum(){}
    public final static byte BLOSUM62[][]=new byte[128][128], BLASTMIDLINE[][]=new byte[128][128], BL2SEQ_MIDLINE[][]=new byte[128][128], BLOSUM62_MAX=9,BLOSUM62_MIN=-4;
    static {
         final byte[][] b62={
            /*     A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  *  */
            /*A*/{ 4,-2, 0,-2,-1,-2, 0,-2,-1, 0,-1,-1,-1,-2, 0,-1,-1,-1, 1, 0, 0, 0,-3, 0,-2,-1},
            /*B*/{-2, 4,-3, 4, 1,-3,-1, 0,-3, 0, 0,-4,-3, 3, 0,-2, 0,-1, 0,-1, 0,-3,-4,-1,-3, 1},
            /*C*/{ 0,-3, 9,-3,-4,-2,-3,-3,-1, 0,-3,-1,-1,-3, 0,-3,-3,-3,-1,-1, 0,-1,-2,-2,-2,-3},
            /*D*/{-2, 4,-3, 6, 2,-3,-1,-1,-3, 0,-1,-4,-3, 1, 0,-1, 0,-2, 0,-1, 0,-3,-4,-1,-3, 1},
            /*E*/{-1, 1,-4, 2, 5,-3,-2, 0,-3, 0, 1,-3,-2, 0, 0,-1, 2, 0, 0,-1, 0,-2,-3,-1,-2, 4},
            /*F*/{-2,-3,-2,-3,-3, 6,-3,-1, 0, 0,-3, 0, 0,-3, 0,-4,-3,-3,-2,-2, 0,-1, 1,-1, 3,-3},
            /*G*/{ 0,-1,-3,-1,-2,-3, 6,-2,-4, 0,-2,-4,-3, 0, 0,-2,-2,-2, 0,-2, 0,-3,-2,-1,-3,-2},
            /*H*/{-2, 0,-3,-1, 0,-1,-2, 8,-3, 0,-1,-3,-2, 1, 0,-2, 0, 0,-1,-2, 0,-3,-2,-1, 2, 0},
            /*I*/{-1,-3,-1,-3,-3, 0,-4,-3, 4, 0,-3, 2, 1,-3, 0,-3,-3,-3,-2,-1, 0, 3,-3,-1,-1,-3},
            /*J*/null,
            /*K*/{-1, 0,-3,-1, 1,-3,-2,-1,-3, 0, 5,-2,-1, 0, 0,-1, 1, 2, 0,-1, 0,-2,-3,-1,-2, 1},
            /*L*/{-1,-4,-1,-4,-3, 0,-4,-3, 2, 0,-2, 4, 2,-3, 0,-3,-2,-2,-2,-1, 0, 1,-2,-1,-1,-3},
            /*M*/{-1,-3,-1,-3,-2, 0,-3,-2, 1, 0,-1, 2, 5,-2, 0,-2, 0,-1,-1,-1, 0, 1,-1,-1,-1,-1},
            /*N*/{-2, 3,-3, 1, 0,-3, 0, 1,-3, 0, 0,-3,-2, 6, 0,-2, 0, 0, 1, 0, 0,-3,-4,-1,-2, 0},
            /*O*/null,
            /*P*/{-1,-2,-3,-1,-1,-4,-2,-2,-3, 0,-1,-3,-2,-2, 0, 7,-1,-2,-1,-1, 0,-2,-4,-2,-3,-1},
            /*Q*/{-1, 0,-3, 0, 2,-3,-2, 0,-3, 0, 1,-2, 0, 0, 0,-1, 5, 1, 0,-1, 0,-2,-2,-1,-1, 3},
            /*R*/{-1,-1,-3,-2, 0,-3,-2, 0,-3, 0, 2,-2,-1, 0, 0,-2, 1, 5,-1,-1, 0,-3,-3,-1,-2, 0},
            /*S*/{ 1, 0,-1, 0, 0,-2, 0,-1,-2, 0, 0,-2,-1, 1, 0,-1, 0,-1, 4, 1, 0,-2,-3, 0,-2, 0},
            /*T*/{ 0,-1,-1,-1,-1,-2,-2,-2,-1, 0,-1,-1,-1, 0, 0,-1,-1,-1, 1, 5, 0, 0,-2, 0,-2,-1},
            /*U*/null,
            /*V*/{ 0,-3,-1,-3,-2,-1,-3,-3, 3, 0,-2, 1, 1,-3, 0,-2,-2,-3,-2, 0, 0, 4,-3,-1,-1,-2},
            /*W*/{-3,-4,-2,-4,-3, 1,-2,-2,-3, 0,-3,-2,-1,-4, 0,-4,-2,-3,-3,-2, 0,3, 11,-2, 2,-3},
            /*X*/{ 0,-1,-2,-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1, 0,-2,-1,-1, 0, 0, 0,-1,-2,-1,-1,-1},
            /*Y*/{-2,-3,-2,-3,-2, 3,-3, 2,-1, 0,-2,-1,-1,-2, 0,-3,-1,-2,-2,-2, 0,-1, 2,-1, 7,-2},
            /*Z*/{-1, 1,-3, 1, 4,-3,-2, 0,-3, 0, 1,-3,-1, 0, 0,-1, 3, 0, 0,-1, 0,-2,-3,-1,-2, 4}
         };

        for(int i1=128; --i1>=0;)
            for(int i2=128; --i2>=0;) {
                final int uc1=i1&~32, uc2=i2&~32;
                final boolean isA='A'<=uc1&&uc1<'Z' && 'A'<=uc2&&uc2<'Z'  &&  uc1!='J'&& uc1!='O'&& uc1!='U' && uc2!='J'&& uc2!='O' && uc2!='U';
                final byte m=BLOSUM62[i1][i2]=isA ? b62[uc1-'A'][uc2-'A'] : Byte.MIN_VALUE;
                BLASTMIDLINE[i1][i2]=(byte) ( isA ? (uc1==uc2 ? uc1 : m>0 ? '+' : ' ') : ' ');
                BL2SEQ_MIDLINE[i1][i2]=(byte) (!isA ? ' ' : uc1==uc2 ? '|' : m>2 ? ':' : m>0 ? '.' : ' ');
            }
    }
    /*
      http://www.gsic.titech.ac.jp/supercon/supercon2003e/alignmentE.html
    */
    public static float pairAignScore(float gapOpen,float gapExt, byte[] s0,byte[] s1, int fromCol, int toCol, float addConstant) {
        final int L=mini(sze(s0),sze(s1), toCol);
        final byte blosum[][]=Blosum.BLOSUM62;
        boolean isGap=false, end0=false, end1=false;
        float score=0;
        for(int i=fromCol; i<L; i++) {
            final byte c0=s0[i], c1=s1[i];
            if (c0==0) end0=true;
            if (c1==0) end1=true;
            final boolean letter0=!end0 && 'A'<=c0 && (c0<='Z' ||  'a'<=c0 && c0<='z');
            final boolean letter1=!end1 && 'A'<=c1 && (c1<='Z' ||  'a'<=c1 && c1<='z');
            if (!letter0 && !letter1) continue;
            else if (letter0 && letter1) {
                score+=blosum[c0][c1]+addConstant;
                isGap=false;
            } else {
                score+=+(isGap?gapExt:gapOpen);
                isGap=true;
            }
        }
        return score;
    }

    public static float blosumScore3(float gapOpen,float gapExt, byte[] s0,byte[] s1,byte[] s2, int colFrom, int colTo) {
        final int to=mini(colTo,  mini(s0.length,s1.length,s2.length));
        float score=0;
        boolean isGap=false;
        final byte blosum[][]=Blosum.BLOSUM62;

        for(int i=colFrom;i<to;i++) {
            final int
                c0=s0[i]|32,
                c1=s1[i]|32,
                c2=s2[i]|32;
            final int
                isLetter0='a'<=c0 && c0<='z' ? 1:0,
                isLetter1='a'<=c1 && c1<='z' ? 1:0,
                isLetter2='a'<=c2 && c2<='z' ? 1:0,
                isL012=isLetter0+isLetter1+isLetter2;
            if (isL012==0) continue;
            if (isL012==3) {
                isGap=false; score+=(blosum[c0][c1]-BLOSUM62_MIN) * (blosum[c0][c2]-BLOSUM62_MIN);
                continue;
            }
            score-= isGap ? gapExt : gapOpen;
            isGap=true;
        }
        return score;
    }
    public static float blosumScore2(float gapOpen,float gapExt, byte[] seq0, byte[] seq1) {
        final int n=mini(sze(seq0),sze(seq1));
        float score=0;
        boolean isGap=false;
        final byte blosum[][]=Blosum.BLOSUM62;
        for(int i=0;i<n;i++) {
            final int c0=seq0[i]|32;
            final int c1=seq1[i]|32;
            final boolean isLetter0='a'<=c0 && c0<='z';
            final boolean isLetter1='a'<=c1 && c1<='z';
            if (!isLetter0 && !isLetter1) {continue;}
            if (isLetter0 && isLetter1) {
                isGap=false; score+=blosum[c0][c1];
                continue;
            }
            score-= isGap ? gapExt : gapOpen;
            isGap=true;
        }
        return score;
    }

}
