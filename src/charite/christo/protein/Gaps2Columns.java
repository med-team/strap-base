package charite.christo.protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.util.*;

/**HELP
Empty columns with a gap in each sequence are usually not displayed.
<br>
Consider the following case:

<pre class="data">
ASGATA  YTG
ATGATG  YTA
ASGGTAG FSG
       ^
</pre>
This empty column is not displayed. The view shows:

<pre class="data">
ASGATA YTG
ATGATG YTA
ASGGTAGFSG
</pre>

When the sequence "ASGGTAGAFSG"
was added to the multiple sequence alignment  the column would not be empty any more and would be visible.
In the multiple sequence alignment panel is the following:

<pre class="data">
ASGATA  YTG
ATGATG  YTA
ASGGTAG FSG
ASGGTAGAFSG
</pre>

This is the reason for the following phenomenon:
When gaps are added to one sequence it might seem that gaps are erased
in all other sequences instead.

   @author Christoph Gille
*/
public class Gaps2Columns {

    private List<Protein>_proteinsV;
    private Protein[] getProteins(){ return toArry(_proteinsV,Protein.class);}
    public void setProteinsV(List<Protein>v, int[] mc) { _proteinsV=v; _mcGlobal=mc;}

    private boolean _noGap[]=new boolean[5];

    private int _w2nMC, _mcGlobal[];
    private int[] _wide2narrow=new int[666], _narrow2wide=new int[333];
    private int _countColumns=0;

    public int countColumns() { wide2narrowColumn(getProteins()); return _countColumns;}
    public int mc() { return _w2nMC;}
    /**
     * Calculates the horizontal position (column) of all residues in the alignment.
     * from the number of gaps of each residue
     * Used for the default modus where  redundant gaps are suppressed.
     */
    public int[] computeWide2narrow(Protein pp[]) { return computeWide2narrow(pp,0, MAX_INT); }
    private int[] computeWide2narrow(Protein pp[], int fromCol, int toCol) {
        if (pp==null) return null;
        int maxCol=0;
        boolean[] noGap=_noGap;
        Arrays.fill(noGap,false);
        for(Protein p :pp) {
            if (p==null) continue;
            final int gaps[]=p.unsyncResGap(), nG=sze(gaps), nRes=p.unsyncResCount();
            int col=-1;
            for(int i=0;i<nRes;i++) {
                int g=i<nG?gaps[i]:0;
                if (g<0) { assrt(); g=0;}
                col+=g+1;
                if (noGap.length<=col) noGap=_noGap=chSze(noGap,maxi(999,33+col*3/2));
                noGap[col]=true;
            }
            if (maxCol<col) maxCol=col;
        }
        int w2s[]=_wide2narrow;
        if (w2s.length<=maxCol+1 || w2s.length>maxCol+666) _wide2narrow=w2s=new int[maxCol+333];
        int col=0;
        _deleted=0;
        // putln("\n");
        for(int i=0;i<=maxCol;i++) {
            w2s[i]=col;
            if (noGap[i] || !(fromCol<=i && i<toCol)) col++;
            else _deleted++;
            //if (fromCol<=i && i<toCol) puts(noGap[i]?"X":"*");

        }
        if (noGap.length>maxCol+9999) _noGap=new boolean[maxCol+333];
        w2s[maxCol+1]=col++;
        _countColumns=col;
        int[] s2w=_narrow2wide;
        if (s2w.length<=col || s2w.length>col+666) _narrow2wide=s2w=new int[col+333];
        for(int i=maxCol+1; --i>=0;) s2w[w2s[i]]=i;    // ??? ArrayIndexOutOfBoundsException: 520878
        //if (pp.length>10000 && debugTime("computeWide2narrow duration=",time);
        return w2s;
    }
    public int[] wide2narrowColumn() { return wide2narrowColumn(getProteins()); }
    public int[] wide2narrowColumn(Protein pp[]) {
        final int mc=_mcGlobal==null ? 0 : _mcGlobal[ProteinMC.MCA_ALIGNMENT]+_mcGlobal[ProteinMC.MCA_RES_COUNT_NOSYNC];
        if (_wide2narrow==null || _w2nMC!=mc) {
            _w2nMC=mc;
            computeWide2narrow(pp);
        }
        return _wide2narrow;
    }
    public int[] narrow2wideColumn() {
        wide2narrowColumn(getProteins());
        return _narrow2wide;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
    public static int[] computeColumns(int wide2narrow[],int gaps[], int nRes, int oldCols[]) {
        final int cols[]=redim(oldCols,nRes,99);
        final int nW=sze(wide2narrow), nG=sze(gaps);
        int  col=-1, lastCol=-1, i=0;
        for(; i<nRes; i++) {
            int g=i<nG ? gaps[i] : 0;
            if (g<0) { assrt(); g=0;}
            col+=g+1;
            if (col>=nW) break;
            int newCol=wide2narrow[col];
            if (newCol<=lastCol) newCol=lastCol+1;
            lastCol=cols[i]=newCol;
        }
        for(; i<nRes; i++) {
            int g=i<gaps.length ? gaps[i] : 0;
            if (g<0) { assrt(); g=0;}
            col+=g+1;
            if (col<=lastCol) col=lastCol+1;
            lastCol=cols[i]=col;
        }
        return cols;
    }
    /* <<<  <<< */
    /*--------------------------------------- */
    /* >>> external Utils >>> */
    private int _deleted;
    public static int eliminateCommonGaps(Protein pp[], int fromCol, int toCol) {
        final Gaps2Columns g2c=new Gaps2Columns();
        final int w2n[]=g2c.computeWide2narrow(pp,fromCol, toCol);
        int cols[]=new int[999];
        for(Protein p : pp) {
            final int nR=p.unsyncResCount(), gg[]=new int[nR];
            cols=computeColumns(w2n, p.unsyncResGap(), nR,   cols);
            for(int i=0; i<nR; i++) {
                gg[i]=cols[i]-(i>0?cols[i-1]+1:0);
                if (gg[i]<0) assrt();
            }
            p.setResidueGap(gg);
        }
        return g2c._deleted;
    }

}
