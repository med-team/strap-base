package charite.christo.protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import charite.christo.*;

/**
   @author Christoph Gille
*/
public interface SimilarPdbFinder {
    Result[] NOT_FOUND={};
    Result[] getSimilarStructures();
    Class CLASSES[]={};

    void setProtein(Protein p);
    public static class Result implements HasRenderer{
        private Object _p;
        public SimilarPdbFinder _simPdbFinder;
        private String _id;
        private int[] _fromTo, _ident;
        public Result(Protein p, SimilarPdbFinder simPdbFinder, String pdbID, int fromTo[], int[] ident) {
            _p=wref(p);
            _simPdbFinder=simPdbFinder;
            _id=pdbID;
            _fromTo=fromTo;
            _ident=ident;

        }
        public Protein getProtein() { return deref(_p,Protein.class);}
        public String getPdbID() { return _id;}
        private javax.swing.JComponent rComp;
        public Object getRenderer(long options, long rendOptions[]) {
            if (rComp==null) {
                final Protein p=getProtein();
                final int n=p==null?0:p.countResidues();
                final ChButton labRange=labl();
                if (n>0) {
                    labRange.setDrawFromTo(_fromTo[0]*100/n, _fromTo[1]*100/n);
                }
                final BA sScore=new BA(33).a(" ident=").a(_ident[0]).a('/').a(_ident[1]).a(' ');
                rComp=pnl(CNSEW,labRange,dim(99,ICON_HEIGHT),null,monospc(sScore), monospc(" "+delPfx("PDB:",getPdbID())+" "));
            }
            return rComp;
        }
    }

}

/*
support@ebi.ac.uk
   This sounds very similar to the pseudo-HSP mechanism used in FASTA 36. Which
   should be available as part of our SSS services
   (http://www.ebi.ac.uk/Tools/sss/) in the near future.

http://webclu.bio.wzw.tum.de/portal/web/simap/

*/
