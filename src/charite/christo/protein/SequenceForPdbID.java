package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   @author Christoph Gille
*/
public class SequenceForPdbID {

  // http://www.rcsb.org/pdb/files/fasta.txt?structureIdList=1ryp
    private SequenceForPdbID(){}
  public static byte[] getSequence(String pdbID, char chain) {
      final String id=delLstCmpnt(delPfx("PDB:",pdbID),'_');
      final String url=new BA(99).a("http://www.rcsb.org/pdb/files/fasta.txt?structureIdList=").a(id).toString();
      final java.io.File fDest=file(url);
      downloadAndDecompress(url,fDest);
      final BA ba=readBytes(fDest);
      if (sze(ba)==0) return null;
      final byte[] T=ba.bytes();
      final int E=ba.end();
      int header=-1;
      for(int i=0; i<E-1; i++) {
          if (T[i]==':' && (T[i+1]==chain || chain==' ' && T[i+1]=='_' )) {
              header=i;
              break;
          }
      }
      if (header<0 && chain!=' ') return null;
      final int from=strchr('\n',ba,header+1,MAX_INT)+1, to=strchr(STRSTR_E, '>',ba,from, E);
      if (from<=0 || from>=to) return null;
      final BA txt=new BA(to-from);
      txt.filter(0L, LETTR, ba,  from , to);
      return toByts(txt);
  }
}
