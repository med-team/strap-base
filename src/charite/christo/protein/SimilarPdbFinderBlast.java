package charite.christo.protein;
import charite.christo.blast.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class SimilarPdbFinderBlast implements SimilarPdbFinder {
    private Protein _p;
    private static Object _b;
    public void setProtein(Protein p) { _p=p; }
    public void setBlaster(Object b) { _b=b;}
    public synchronized Result[] getSimilarStructures() {
        final int MIN_LEN=16, OVERLAP=7;
        final Protein p=_p;
        final String res=p.getResidueTypeAsString();
        final byte found[]=new byte[sze(res)];
        final java.util.List<Result> v=new java.util.ArrayList();
        for(int iPart=9; --iPart>=0; ) {
            int from=-1, to=-1;
            for(int i=0; i<=found.length; i++) {
                if (i==found.length || found[i]!=0) {
                    if (from>=0 && i-from>MIN_LEN) {
                        to=i;
                        break;
                    }
                    from=to=-1;
                } else if (from<0) from=i;
            }
            if (from<0 || to-from<MIN_LEN) break;
            from=maxi(0,from-OVERLAP);
            to=mini(res.length(), to+OVERLAP);
            final String part=res.substring(from, to);
            boolean success=false;
            SequenceBlaster b=mkInstance(_b,SequenceBlaster.class);
            if (b==null) new Blaster_SOAP_ebi();
            pcp(KEY_CTRL_PNL,b,this);
            b.setDatabase("pdb");
            b.setSensitivity(1);
            b.setWordSize(5);
            b.setNumberOfAlignments(10);
            b.setAAQuerySequence(part);
            final BlastResult result=BlastResult.newInstance(b, "Find Structure "+p+"/"+(1+from)+"-"+(1+to), IC_3D );
            if (result==null) break;
            final BlastHit hits[]=result.getHits();
            for(int iH=0; iH<hits.length; iH++) {
                int start=MAX_INT, end=-1;
                final BlastAlignment aa[]=hits[iH].getAlignments();
                for(BlastAlignment a : aa) {
                    start=mini(start,a.getQueryStart());
                    end=maxi(end,a.getQueryEnd());
                }
                if (start>=0 && start<end) {
                    start+=from;
                    end+=from;
                    final byte[] seqM=aa[0].getMatchSeq();
                    final byte[] seqQ=aa[0].getQuerySeq();
                    final int total=mini(sze(seqM), sze(seqQ));
                    int ident=0;
                    for(int i=total; --i>=0;) {
                        if (is(LETTR, seqQ,i) && is(LETTR, seqM, i) && (seqM[i]|32)==(seqQ[i]|32)) ident++;
                    }
                    v.add(new Result(p, this,  "PDB:"+delPfx("PDB:",hits[iH].getID()), new int[]{start,end}, new int[]{ident,total} ));
                    for(int i=mini(found.length, end); --i>=start;) found[i]=(byte)'H';
                    success=true;
                }
            }
            if (!success) for(int i=to; --i>=from;) found[i]=(byte)'F';
        }
        return toArryClr(v, Result.class);
    }
}

