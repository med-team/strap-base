package charite.christo.protein;
/**
   @author Christoph Gille
*/
public interface  PhylogeneticTree {

    void setAlignment(String names[], byte aligned[][], Object[] proteins);

    /** Drawws tree. Returns null or HTML-Result. */
    void drawTree();

    /** Return null if not using a remote server **/
}
