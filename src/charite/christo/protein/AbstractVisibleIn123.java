package charite.christo.protein;
import charite.christo.*;
import java.awt.*;
import java.util.*;
import static charite.christo.ChUtils.*;
/**
An abstract implementation of {@link VisibleIn123 VisibleIn123}.
@author Christoph Gille
*/
public abstract class AbstractVisibleIn123 implements VisibleIn123, HasImage, HasMap, HasWeakRef {
    private int
        _where=STRUCTURE|SEQUENCE|SB,
        _style=VisibleIn123.STYLE_IMAGE;
    private Color _color=Color.YELLOW, _lastColor;
    private Image _image;
    private boolean _isBall;

    public int  getVisibleWhere() { return _where;}
    public void setVisibleWhere(int w) { _where=w; }
    public int  getStyle() { return _style; }
    public void setStyle(int style) { _style=style; }
    public Color getColor() { return _color;}
    public void  setColor(Color c) { _color=c;}
    public Image getImage() {
        final Color c=getColor();
        if (_isBall) {
            if (_lastColor!=c) { _image=null; _lastColor=c; }
            if (_image==null) _image=ChIcon.coloredBall(c, null);
        }
        return _image;
    }

    public void setImage(Image i) { _image=i;}
    public void imageBall(boolean b) { _isBall=b;}
    public static ChCombo newComboStyle() {
        return new ChCombo(finalStaticInts(VisibleIn123.class, STYLE_CURSOR));
    }

    /* ---------------------------------------- */
    /* >>> Map >>> */
    private Map _co;
    public Map map(boolean create) { return _co==null&&create?_co=new HashMap() : _co;}

    /* <<< Map <<< */

    public final Object WEAK_REF=newWeakRef(this);
    public Object wRef() { return WEAK_REF;}

}
