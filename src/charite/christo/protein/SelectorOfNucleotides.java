package charite.christo.protein;
/**
   Selects nucleotides.
   @see ResidueSelection
   @see charite.christo.strap.ResidueAnnotation
*/
public interface SelectorOfNucleotides {
void setProtein(Protein p);
    Protein getProtein();
    boolean[] getSelectedNucleotides();

    /** 
       The goal is to reduce the size of the returned boolean array. 
       if e.g. the 100th nt is selected getOffset() can return 99 and
       getSelectedNucleotides() can return new boolean[]{true}.

       BitSet would not have been a solution because the following causses OutOfMemoryError:
       new BitSet().set(1000*1000*1000,true);

    */

    int getSelectedNucleotidesOffset();
}
