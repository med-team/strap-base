package charite.christo.protein;
import charite.christo.*;
import java.util.*;
import java.io.*;
import java.awt.Component;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.ProteinViewer.*;
import static java.awt.event.MouseEvent.*;

public class Protein3dUtils implements ChRunnable {
    private final static Object KEY_SHARED_MAP=new Object();
    private final static String KEY_NATIV_MENUS="P3U$$NM";
    public final static int GENERIC=0,  SURFACE_ID=1, JMOL=2, PYMOL=3, ASTEX=4, RASMOL=5, JV=6,
        SURFACE_ID_COLON=1<<17, SURFACE_ID_DASH=1<<18, SURFACE_ID_KOMMA=1<<19, SURFACE_ID_DOT=1<<20;
    public final static String
        KEY_ADD_TO_NATIVE_MENU="P3U$$ATNM",
        SCRIPT_select_3D="select_3D ",
        VARIABLES="$FIRST $LAST $NTERM $CTERM $ALL $AMINOACID ";

    private static Protein3dUtils _inst;
    private static Protein3dUtils instance() {if (_inst==null) _inst=new Protein3dUtils(); return _inst; }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Cache >>> */
    private final static int CACHE_splitAtoms=1;
    private static Object rCached;
    private static synchronized Object[] cached() {
        Object[] r=(Object[])deref(rCached);
        if (r==null) rCached=newSoftRef(r=new Object[99]);
        return r;
    }

    private static String[] _atoms;
    public static String[] atomsOfAminos() {
        if (_atoms==null) {
            final String s="C C5 C5W CA CB CD CD1 CD2 CE CE1 CE2 CE3 CF CG CG1 CG2 CH0 CH1E CH1S CH2 CH2E CH2G CH2P CH3E CR1E CR1H CR1W CRHH CW CY CY2 CZ CZ2 CZ3 "+
                "N NC2 ND1 ND2 NE NE1 NE2 NH1 NH1S NH2 NH3 NZ "+
                "O OC OD1 OD2 OE1 OE2 OG OG1 OH OH1 OS OXT "+
                "SD SG SH1E SM ";
                _atoms=splitTokns(s);
        }
        return _atoms;
    }
    private static List<Protein> vPIV=new ArrayList();
    public static Protein[] ppInV3D(ProteinViewer...vv) {
        synchronized(vPIV) {
            vPIV.clear();
            if (vv!=null) for(ProteinViewer v : vv) ppInV3dV(v,vPIV);
            return sze(vPIV)==0?Protein.NONE:toArryClr(vPIV,Protein.class);
        }
    }

    public static List<Protein> ppInV3dV(ProteinViewer viewer, List<Protein> vProts) {
        if (viewer==null) return vProts;
        for(ProteinViewer v :vvSharingView(viewer)) adUniq(p(v), vProts);
        return vProts;
    }
    public static ProteinViewer[] vvSharingView(ProteinViewer pv) {
        if (pv==null) return ProteinViewer.NONE;
        final ProteinViewer vv[]=toArry(pv.getViewersSharingViewV(true), ProteinViewer.class);
        return sze(vv)>0 ? vv : new ProteinViewer[]{pv};
    }

    public static String titleFor3dViewers(Protein pp[]) {
        final BA sb=baTip().a(" #").a(pp.length).a(' ');
        for(Protein p : pp) {
            sb.a(delPfx("pdb",delSfx(".pdb",delSfx(".ent",delSfx(".Z",delSfx(".gz",p.getName())))))).a(' ');
        }
        return sb.toString();
    }
    public static Protein p(ProteinViewer pv) { return pv==null?null:pv.getProtein();}
    public static boolean isActive(ProteinViewer v) {
        final Protein p=p(v);
        return p==null ? false : cntains(v, p.getProteinViewers());
    }

    public static ProteinViewer[] views3D(Protein[] pp, Class clazz) {
        final List v=new ArrayList();
        for(Protein p : pp) {
            for(ProteinViewer pv : p.getProteinViewers()) {
                if (clazz==null || isAssignblFrm(clazz,pv)) adUniq(pv,v);
            }
        }
        return toArryClr(v,ProteinViewer.class);
    }

    public static Component viewerCanvas(ProteinViewer pv) { return pv==null?null: (Component)pv.getProperty(ProteinViewer.GET_CANVAS);}
    public static JMenuBar jMenuBar(ProteinViewer pv) { return pv==null?null: (JMenuBar)pv.getProperty(ProteinViewer.GET_JMENUBAR);}

    public static JComponent[] nativeMenus(ProteinViewer pv) {
        final Component panel=viewerCanvas(pv);
        if (panel==null) return null;
        JComponent[] mm=gcp(KEY_NATIV_MENUS, panel, JComponent[].class);
        if (mm==null) {
            final JMenuBar mb=jMenuBar(pv);
            if (mb!=null) {
                //final JMenu mOther=jMenu(pvp().m('b',null));
                removeMenuItems(new String[]{"file"}, new String[]{"close","exit","Close","Exit"}, mb);
                final List v=new ArrayList();
                for(Component c : mb.getComponents()) if (c instanceof JMenu) v.add(c);
                final JComponent mOther=gcp(KEY_ADD_TO_NATIVE_MENU, panel, JComponent.class);
                if (mOther!=null) {
                    v.add(Box.createHorizontalGlue());
                    v.add(mOther);
                }
                mm=toArry(v,JComponent.class);

            } else mm=new JComponent[0];
            pcp(KEY_NATIV_MENUS, mm,panel);
        }
        return mm;
    }

    public static ProteinViewer[] askViewersSharingView(ProteinViewer pv0, Protein pSuggestion, String msg) {
        final ProteinViewer[] vv=vvSharingView(pv0), vv2=vv.clone();
        if (vv.length<2) return vv;
        final Protein pp[]=new Protein[vv.length];
        int sel=-1;

        for(int i=vv.length; --i>=0;) {
            pp[i]=p(vv[i]);
            if (pSuggestion!=null && pSuggestion==pp[i]) sel=i;
        }

        final ChJList jl=new ChJList(pp,0);

        if (sel>=0) jl.setSelI(sel); else selectAllItems(jl);

        final Object pan=pnl(CNSEW,scrllpn(jl),"The 3D-View has more than one Proteins. Please select at least one.",msg);
        if (!ChMsg.yesNo(pan)) return ProteinViewer.NONE;
        final Object selected[]=jl.getSelectedValues();
        for(int i=sze(vv2); --i>=0;) if (!cntains(p(vv2[i]), selected)) vv2[i]=null;
        return rmNullA(vv2,ProteinViewer.class);
    }
    public static ProteinViewer findProteinViewerWithProteins(Class clazz, Protein pp[], boolean exactly) {
        for(int iP=sze(pp); --iP>=0; ) {
            if (pp[iP]==null) continue;
            for(ProteinViewer pv : pp[iP].getProteinViewers()) {
                if (clazz!=null && !isAssignblFrm(clazz,pv)) continue;
                final Protein ppV[]=ppInV3D(pv);
                if (!cntainsAll(pp, ppV)) continue;
                if (exactly && pp.length!=ppV.length) continue;
                return pv;
            }
        }
        return null;
    }
    public static ProteinViewer viewForProtein(Protein p, ProteinViewer pv) {
        if (p!=null && pv!=null)
            for(ProteinViewer v :vvSharingView(pv))
                if (p(v)==p) return v;
        return null;
    }
    public static ProteinViewer derefV3D(Object o) {
        return (ProteinViewer) (o instanceof ProteinViewer ? o : deref(ProteinViewer.mapViewer.get(o)));
    }
    //selectionString(
    //select ( molecule "a1_SaccharomycesCerevisiae.pdb" and (  (  residue 10 to 160  and chain '2'  and atom CA CB )  ) );
    //select Unnamed = (protein and ( ///2/183-187/CA,CB  or  ///A/183-187 ) )
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Scripting >>> */
    private final static String COMMANDS_WITH_COLOR[]={COMMANDcolor,COMMANDsurface_color, COMMANDbackground, COMMANDchange_object_color,COMMANDlabel_color};

    private static String[] _allCmds;
    public static String[] allCommands() {
        if (_allCmds==null) _allCmds=finalStaticStrings(true, "COMMAND", ProteinViewer.class);
        return _allCmds;
    }

    public static long rgbInScript(String command) {
        for(String word0 : COMMANDS_WITH_COLOR) {
            if (strstr(STRSTR_w,word0, command)<0) continue;
            final int begin=command.indexOf('#')+1;
            final int end=begin>0?nxtE(-HEX_DIGT,command,begin,MAX_INT) : -1;
            return end-begin<2?-1: Long.parseLong(command.substring(begin,end),16) | (end-begin>6 ? 0 : 255L<<24);
        }
        return -1;
    }
    public static String objectIdInScript(String c) {
        final int spc=nxt(SPC,c);
        int b=spc<0?-1 : nxt(-SPC, c, spc,MAX_INT), e=b<0?-1 : nxtE(SPC, c, b,MAX_INT);
        while(chrAt(e-1,c)==';') e--;
        if(chrAt(e-1,c)=='"') e--;
        if(chrAt(b,c)=='"') b++;
        return b<e && b>0 ? c.substring(b,e) : null;
    }
    public static Matrix3D matrixInScript(String c) {
        if (c.startsWith(COMMANDset_rotation_translation)) {
            if (c.indexOf("null")>0) return null;
            final Matrix3D m3d=new Matrix3D();
            if (m3d.parsePlain(c.getBytes(), COMMANDset_rotation_translation.length(), MAX_INT)) {
                return  m3d.isUnit() ? null : m3d;
            } else putln(RED_ERROR,"Protein3dUtils.matrixInScript: ",c);
        }
        return null;
    }
    public static String makeSelId(Selection3D[] atoms, String selId) {
        return sze(selId)>0 ? selId : sze(atoms)==0 ? "entire" : "unnamed";
    }
    /* <<< Scripting <<< */
    /* ---------------------------------------- */
    /* >>> Log >>> */
    public final static String LOG_PROTEIN_PFX=SCRIPT_select_3D+"*, ", LOG_FILE=STRAPOUT+"/commands.3d.txt";
    private final static Object KEY_LOG[]={new Object(), new Object()};

    public static BA log3D(boolean isGeneric, ProteinViewer pv0, String command) {
        final String KEY_p="P3U$$logP";
        ProteinViewer pv=AbstractProxy.getProxy(pv0,ProteinViewer.class);
        if (pv==null) pv=pv0;
        final Protein p=p(pv);
        if (p==null) return null;
        final Object key=KEY_LOG[isGeneric?0:1];
        final Map map=sharedHashMapV3D(pv);
        BA log=(BA)map.get(key);
        if (log==null) {
            final ChLogger logger=new ChLogger(0, 10*1000);
            if (isGeneric) logger.saveFile(file(LOG_FILE));
            pcp(KEY_TITLE, niceShrtClassNam(pv)+(isGeneric?" Generic":" Native"),logger);
            if (isGeneric) logger.setPreprocessor(instance());
            map.put(key,log=new BA(0).setSendTo(logger));
        }
        if (strEquls("3D_",command)==isGeneric && sze(command)>0) {
            final Object lastP=map.get(KEY_p);

            if (p!=lastP) {
                log.aln(LOG_PROTEIN_PFX+p);
                map.put(KEY_p,p);
            }
            log.aln(command);
            if (log3d==null) log3d=new boolean[]{prgOptT("-log3d")};
            if (log3d[0] && !isGeneric) putln(niceShrtClassNam(pv),ANSI_GREEN+"> ", ANSI_RESET, command);
        }
        return log.send();
    }
    private static boolean log3d[];
    public static void showLogPanel(boolean isGeneric, ProteinViewer pv) {
        if (pv!=null) shwTxtInW(log3D(isGeneric, pv, null));
    }

    private final static String LOG_SKIP[]={
        COMMANDzoom, COMMANDrotate,  COMMANDcenter_amino,
        COMMANDselection_name+" "+SELECTION_CURSOR,
        COMMANDselection_name+" "+ResidueSelection.NAME_STANDARD,
        COMMANDselection_name+" "+SELECTION_PICKED
    };
    private final int BB[]=new int[2];
    private void lineStarts(BA ba) {
        final int E=ba.end();
        final byte[] T=ba.bytes();
        for(int i=BB.length; --i>=0;) BB[i]=-1;
        for(int i=E, count=0; --i>=0;) {
            if (i==0 || T[i-1]=='\n' ) {
                BB[count++]=i;
                if (BB.length==count) break;
            }
        }
    }
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id==ChLogger.RUN_PREPROCESS) {
            final CharSequence line=(CharSequence)argv[0];
            final BA ba=(BA)argv[1];
            lineStarts(ba);
            final boolean isSel=strEquls(STRSTR_w_R, COMMANDselect, line,0);
            for(String cmd : LOG_SKIP) {
                if (strEquls(STRSTR_w_R, cmd, ba, BB[0])) {
                    ba.setEnd(BB[0]);
                    lineStarts(ba);
                    break;
                }
            }
            if (0<=BB[0] && strEquls(STRSTR_w_R, COMMANDselect, ba, BB[0])) {
                if (isSel || strEquls(STRSTR_w_R, COMMANDselect, ba, BB[1])) {
                    if (!strEquls(STRSTR_w_R, COMMANDselection_name, ba, BB[1])) {
                        ba.setEnd(BB[0]);
                        lineStarts(ba);
                    }
                }
            }
            for(String cmd : COMMANDS_WITH_COLOR) {
                if (strEquls(STRSTR_w_R, cmd,line,0) && strEquls(STRSTR_w_R, cmd, ba, BB[0])) {
                    final int colorStart=strchr('#',line);
                    if (colorStart>0 && strEquls(0L, line,0,colorStart, ba, BB[0])) {
                        ba.setEnd(BB[0]);
                        lineStarts(ba);
                    }
                }
            }
            return "";
        }
        return null;
    }

    /* <<< Log <<< */
    /* ---------------------------------------- */
    /* >>> Properties >>> */
    public static long flags(ProteinViewer v) {
        final Long f=v==null?null: (Long)v.getProperty(ProteinViewer.GET_FLAGS);
        return f==null ? 0 : f.longValue();
    }
    public static boolean supports(String command, ProteinViewer v) {

        if (strEquls(COMMAND_HIDE_EVERYTHING,command)) return !(v instanceof Protein3d.PView);
        if (sze(command)==0) return false;
        final String[] ss=v==null?null:(String[])v.getProperty(GET_SUPPORTED_COMMANDS);
            final int b=nxt(-SPC,command);
            if (ss!=null && b>=0) {
                int e=command.indexOf(' ', b);
                if (e<0) e=command.length();
                for(String s : ss) {
                    if (e==b+s.length() && strEquls(s, command, b)) return true;
                }
            }
            return false;
    }

    public static Map sharedHashMapV3D(ProteinViewer pv) {
        if (pv==null) return null;
        Map m=gcp(KEY_SHARED_MAP, pv, Map.class);
        if (m==null) {
            for(ProteinViewer v :vvSharingView(pv)) {
                if (m==null && v!=pv) m=gcp(KEY_SHARED_MAP, v, Map.class);
            }
            if (m==null) pcp(KEY_SHARED_MAP, m=new HashMap(),pv);
        }
        return m;
    }
    /* <<< Properties <<< */
    /* ---------------------------------------- */
    /* >>> Add structure  >>> */
    public static Runnable thread_addPdbId(String id_chain, Protein3d p3d, Class proteinClass) {
        return thrdM("addPdbId",Protein3dUtils.class, new Object[]{id_chain,p3d,proteinClass});
    }

    public static void addPdbId(String id_chain, Protein3d p3d, Class proteinClass) {
      if (p3d==null || sze(id_chain)<0) return;
      pcp(Protein3d.KEY_MSG_NO_PROTEIN, "Downloading "+id_chain+" ...",p3d);
      final File f=downloadPdbChain(id_chain,  pdbChain(id_chain));
      inEDT(thread_addStructure(f,p3d, proteinClass));
      pcp(Protein3d.KEY_MSG_NO_PROTEIN, null,p3d);
    }

    public static Runnable thread_addStructure(File f, Protein3d p3d, Class proteinClass) {
        return thrdM("addStructure",Protein3dUtils.class,new Object[]{f,p3d, proteinClass});
    }
    public static void addStructure(File f, Protein3d p3d, Class proteinClass) {
        Protein p=null;
        try { p=(Protein)proteinClass.newInstance(); } catch(Exception ex){}
        if (p==null) p=new Protein();
        p.setFile(f);
        final BA ba=readBytes(f);
        if (sze(ba)>0) {
            new PDB_Parser().parse(p,0L, ba);
            p3d.addProteins(p);
            setWndwState('F', p3d);
        }
    }

    /* <<< Add structure <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public static void dispatchEvtPicked(ProteinViewer pv, int resNum, char chain, char insertion, int awtModi) {
        if (p(pv)!=null) {
            final Selection3D sel=new Selection3D(0L, resNum, resNum, is(LETTR,insertion)?insertion:(char)0, is(LETTR_DIGT_US,chain)?chain:(char)0, null);
            new StrapEvent(pv,StrapEvent.PROTEIN_VIEWER_PICKED).setParameters(new Object[]{sel, intObjct(awtModi)}).run();
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> from Selection3D >>> */
    /* ( /b1_SaccharomycesCerevisiae and (////10-20    ////99 ) );  */
    public static String selection3dToText(int viewerId, Selection3D[] ss) {
        if (countNotNull(ss)==0) return "$NONE";
        final boolean
            isP=viewerId==PYMOL,  isJ=viewerId==JMOL, isA=viewerId==ASTEX, isR=viewerId==RASMOL, isG=viewerId==GENERIC,  isJR=isJ||isR,
            isLDU= (viewerId&0xFFFF)==SURFACE_ID;
        final BA sb=new BA(99);
        if (isA || isP || isJR) sb.a(" ( ");
        for(int i=0; i<ss.length; i++) {
            final Selection3D s=ss[i];
            final String name=s.getResidueName();
            if (0<i) {
                if (isA || isP || isJR) sb.a(" or ");
            }
            if (isA) sb.a(" ( "); /* Bugfix */
            if (name!=null && isG) sb.a(name).a(' ');
            else if ("$ALL".equals(name)) {
                sb.a(isA||isJR?" all "   : isG?name : isP?"/" : "");
            } else if ("$AMINOACID".equals(name)) {
                sb.a(isA?" aminoacid " : isJR? " amino " : isG?name : isP?" all and not het " : /* Todo no DNA no RNA */ "");
            } else if ("$NONE".equals(name)) {
                sb.a(isA||isJR||isP ? " none " :isG?name : "");
            } else if (name!=null) {
                if (0<i) sb.a(!isLDU ? " " : 0!=(viewerId&SURFACE_ID_KOMMA)?",":"__");
                sb.a(isJR?" [":isA?" name ":isP?" resn ":" ");
                if (isA || isP || isJR) sb.a(name);
                if (isJR) sb.a("] ");
            } else {
                final char ins=s.getInsertion(), chainSpc=s.getChain(), chain=chainSpc==' '?'_':chainSpc;
                final int fst=s.getFirst(), lst=s.getLast();
                if (0<i) sb.a(isJR?" or ":!isLDU?" " : 0!=(viewerId&SURFACE_ID_KOMMA)?",":"__");

                if (isA) sb.a(" residue ");
                if (isP) {
                    sb.a('/',3);
                    if (is(LETTR_DIGT,chain)) sb.a(chain); /* Bug: chain is space not treated properly */
                    sb.a('/');
                }
                sb.a(fst);
                if (ins!=0) {
                    if (isJR) sb.a('^').a(ins);
                    if (isG || isLDU) sb.a(ins);
                }
                if (fst<lst) {
                    sb.a(isA ? " to " : !isLDU || 0!=(viewerId&SURFACE_ID_DASH) ? "-" : "_").a(lst);
                    if (ins!=0) {
                        if (isJ) sb.a('^');
                        if (isJ || isG || isLDU) sb.a(ins);
                    }
                }
                if (ins!=0 || fst==lst) {
                    if (isA) sb.a(" and insertion '").a(ins==0?' ':ins).a('\'');
                    if (isP && ins!=0) sb.a(ins);
                }
                if (chain!=0) {
                    if (isJR||isG) { sb.a(':'); if (isG||chain!='_') sb.a(chain); }
                    if (isLDU)     sb.a( 0!=(viewerId&SURFACE_ID_COLON)?':':'c').a(chain);
                    if (isA)       sb.a(" and chain '").a(chain).a('\'');
                }
            }
            final String nativeA=s.getNativeAtoms(), atoms[]=s.getAtoms();
            if (sze(atoms)+sze(nativeA)>0) {
                sb.a(isP?"/" : isA?" and " : isJR?" and ( " : "");
                if (sze(nativeA)>0) {
                    if (isLDU) sb.a(nativeA.hashCode());
                    else if (!isG) sb.a(nativeA);
                } else {
                    if (isA) sb.a(" atom ");
                    for(int iAt=0; iAt<atoms.length; iAt++) {
                        final String a=atoms[iAt];
                        if (0<iAt) sb.a(isJR ? " or " : isP ? "," : isA ? " " : "");
                        if (isLDU) sb.a(0!=(viewerId&SURFACE_ID_DOT)?'.':'a');
                        if (isJR)  sb.a("*.");
                        if (isG)   sb.a('.');
                        if (isA || isP || isG || isJR || isLDU) sb.a(a);
                    }
                }
                if (isJR) sb.a(" ) ");
                if (isG && sze(nativeA)>0) sb.a(".NATIVE").a(Hex2bin.bin2hex(nativeA.getBytes(), 0, MAX_INT));
            }
            if (isA) sb.a(" ) "); /* Bugfix */
        }
        if (isA || isP || isJR) sb.a(" ) ");
        return sb.toString();
    }

    public static boolean[] selection3dToBoolZ(Selection3D atoms[], Protein p) {
        if (p==null || sze(atoms)==0) return NO_BOOLEAN;
        final boolean bb[]=new boolean[p.countResidues()];
        for(Selection3D a : atoms) {
            final char chain=a.getChain(), ins=a.getInsertion();
            final int first=p.resNumAndChain2resIdxZ(false, a.getFirst(), chain, ins);
            if (first<0) continue;
            final int last=p.resNumAndChain2resIdxZ(false, a.getLast(), chain, ins);
            if (last<first) continue;
            setTrue(first, mini(bb.length,last+1), bb);
        }
        return bb;
    }

    public static int selection3dToFirstLastIdxZ(boolean fullLength, boolean last, Selection3D at, Protein p) {
        return at==null||p==null ? -1 : p.resNumAndChain2resIdxZ(fullLength, last ? at.getLast() : at.getFirst(), at.getChain(), at.getInsertion());
    }

    /* <<< from Selection3D <<< */
    /* ---------------------------------------- */
    /* >>> to Selection3D >>> */

    public static Selection3D[] toSelectionS(ResidueSelection s) {
        if (s!=null)  {
            putln(RED_WARNING+"toSelectionS "+s.getSelectedAminoacidsOffset()+"   "+boolToTxt(s.getSelectedAminoacids()));
        }
        return
            s==null ? Selection3D.NONE :
            boolToSelection3D(true, s.getSelectedAminoacids(), s.getSelectedAminoacidsOffset(),s.getProtein(),null);
    }
    public static Selection3D[] boolToSelection3D(boolean fullLen, boolean bb[], int offset, Protein p, String atoms) {
        final int last=lstTrue(bb);
        if (p==null || last<0) return Selection3D.NONE;
        final List<Selection3D> v=new ArrayList();
        final int rn[]=   fullLen? p.getResidueNumberFullLength()        : p.getResidueNumber();
        final byte rc[]=  fullLen? p.getResidueChainFullLength()         : p.getResidueChain();
        final byte ri[]=  fullLen? p.getResidueInsertionCodeFullLength() : p.getResidueInsertionCode();
        final float xyz[]=fullLen? p.getResidueCalphaOriginalFullLength(): p.getResidueCalpha();

        final int nR=mini(sze(xyz)/3, sze(rn), mini(sze(rc), fullLen ? sze(p.getResidueTypeFullLength()) : p.countResidues(), last+1+offset));
        for(int i=maxi(0,offset); i<nR; ) {
            if (!bb[i-offset] || Float.isNaN(xyz[i*3])) { /* !!!! ArrayIndexOutOf */
                i++;
                continue;
            }
            final int rn1=rn[i];
            final byte rc1=rc[i], ri1=get(i,ri);
            int i2=i+1;
            while (i2<nR && bb[i2-offset]  && !Float.isNaN(xyz[i2*3]) &&  rc1==rc[i2] && ri1==get(i2,ri)) i2++;
            if (i2<=nR) v.add(new Selection3D(0L,  rn1, rn[i2-1], (char)ri1, (char)rc1,  splitAtoms(atoms)));
            i=i2;
        }
        return toArryClr(v, Selection3D.class);
    }

    public static byte[] rplcVars(byte txt[], Protein p) {
        final int[] resNum=p==null?null:p.getResidueNumber();
        final int nR=resNum==null?0:p.countResidues();
        if (nR==0 || strchr('$',txt)<0) return txt;
        final float xyz[]=p.getResidueCalpha();
        final BA ba=new BA(txt);
        for(int iN=4; --iN>=0;) {
            final String n=iN==0?"$FIRST":iN==1?"$LAST":iN==2?"$NTERM":"$CTERM";
            if (strstr(STRSTR_w, n,ba)<0) continue;
            final boolean first=iN==0||iN==2;
            int i=first?0: nR-1;
            while(Float.isNaN(get(3*i, xyz)) && i<nR && i>=0) i+=first?1:-1;
            final int rn=get(i, resNum);
            final int add=iN==2?-1:iN==3?1:0;
            final char chain=(char)get(0,p.getResidueChain());
            final String replacement=is(LETTR_DIGT,chain)?(rn+add)+":"+chain : toStrg(rn+add);
            ba.replace(STRSTR_w, n, replacement);
        }
        return toByts(ba);
    }

    private static ChTokenizer _tok;

    public static Selection3D[] textToSelection3D(Object text, Protein p) { return textToSelection3D(false,text,p); }

    public static Selection3D[] textToSelection3D(boolean convert_i2rn, Object text, Protein p) {
        final byte[] T=rplcVars(toByts(text),p);
        final int[] resNum=p==null?null:p.getResidueNumber();
        if (sze(T)==0) return Selection3D.NONE;
        ChTokenizer TOK=_tok;
        if (TOK==null) TOK=_tok=new ChTokenizer().setDelimiters(chrClas(", "));
        TOK.setText(T, 0, MAX_INT);
        final List<Selection3D> v=new ArrayList();
        while(TOK.nextToken()) {
            String error=null, name=null;
            final int B=TOK.from(), E=TOK.to(), endGrp=nxtE(SPC, T, B,MAX_INT), colon=strchr(':', T,B,endGrp);
            int resFrom=-1, resTo=-1;
            char insF=0, insT=0, chain=0;
            final int noWord1=nxtE(-LETTR_DIGT_US, T,B+1,MAX_INT);
            if (!looks(LIKE_NUM,T,B,E)) {
                name= chrAt(B,T)!='"' ? toStrg(T,B,noWord1) : toStrg(T,B+1, strchr(STRSTR_E,'"', T, B+1, noWord1));
            }
            else {
                resFrom=resTo=atoi(T,B,E);
                final int noDigt=nxtE(-DIGT, T,B+1,MAX_INT);

                if (is(LETTR, T, noDigt)) insF=insT=(char)T[noDigt];
                final int dashPos=
                    chrAt(noWord1, T)==':' && chrAt(noWord1+1, T)=='-' ? noWord1+1 :
                    chrAt(noWord1, T)==':' && chrAt(noWord1+2, T)=='-' ? noWord1+2 :
                    chrAt(noWord1, T)=='-' ? noWord1 :
                    -1;
                if (dashPos>0) {
                    if (!looks(LIKE_NUM, T, dashPos+1, E)) error="Number expected after \"-\" ";
                    else {
                        resTo=atoi(T,dashPos+1,E);
                        final int posIns=nxtE(-DIGT, T,dashPos+2, MAX_INT);
                        insT=is(LETTR, T, posIns) ? (char)T[posIns] : 0;
                    }
                }

                chain=colon>0 && is(LETTR_DIGT_US, T,colon+1) ? (char)T[colon+1] : 0;
            }
            final int atomsB=strchr('.', T,B,endGrp);
            final int atomsE=atomsB<0?-1:nxtE(SPC,T,atomsB,MAX_INT);
            final String nativeA=strEquls(".NATIVE",T,atomsB) ? toStrg(Hex2bin.hex2bin(T,atomsB+7,atomsE)) : null;
            final String atoms[]=nativeA!=null||atomsB<0?null : splitAtoms(T, atomsB, atomsE);

            if (convert_i2rn && colon<0 && resNum!=null) {
                resFrom=get(resFrom, resNum);
                resTo=get(resTo, resNum);
                chain=(char)get(resFrom,p.getResidueChain());
                if (resFrom==INT_NAN || resFrom>resTo) continue;
            }
            Selection3D s1=null, s2=null;
            if (name!=null) {
                s1=new Selection3D(0L, name, chain,atoms);
            } else if (insF==insT || resFrom==resTo) {
                s1=new Selection3D(0L, resFrom,resTo, insF,chain,atoms);
            } else {
                s1=new Selection3D(0L, resFrom,resFrom, insF,chain,atoms);
                s2=new Selection3D(0L, resFrom+1,resTo, insT,chain,atoms);
            }
            for(int i=2; --i>=0;) {
                final Selection3D s=i==0?s1:s2;
                if (s!=null) v.add(nativeA!=null ? s.newInstanceNativeAtoms(nativeA) :s);
            }
            if (error!=null) putln(RED_WARNING+" in toSelectionT:", toStrg(T,B,E)," ", error);
        }
        final Selection3D ss[]=toArry(v, Selection3D.class);
        Arrays.sort(ss);
        return ss;
    }
 /* <<< to Selection3D <<< */
    /* ---------------------------------------- */
    /* >>> Shift Intervall >>> */
    public static Selection3D[] rangeToSelection3D(boolean fullLen, int first, int last, Protein p) {
        if (p==null) return Selection3D.NONE;
        final float[] xyz=fullLen ? p.getResidueCalphaOriginalFullLength() : p.getResidueCalpha();
        final int rn[]=fullLen ? p.getResidueNumberFullLength() : p.getResidueNumber();
        final int nR=mini(sze(xyz)/3, sze(rn),  fullLen ? p.getResidueTypeFullLength().length : p.countResidues());
        final byte ins[]=fullLen ? p.getResidueInsertionCodeFullLength() : p.getResidueInsertionCode();
        final byte chain[]=fullLen ? p.getResidueChainFullLength() : p.getResidueChain();
        int i1=mini(nR-1,maxi(0,first));
        int i2=mini(nR-1,maxi(0,last));
        while(i1+1<nR && Float.isNaN(get(i1*3, xyz))) i1++;
        while(i2>0 && Float.isNaN(get(i2*3, xyz))) i2--;
        if (i1>i2 || i2>=nR || Float.isNaN(get(i1*3, xyz)) || Float.isNaN(get(i2*3, xyz))) return Selection3D.NONE;
        return new Selection3D[]{
            new Selection3D(0L,  rn[i1], rn[i2], i1==i2?chrAt(i1,ins):(char)0, chrAt(i1,chain), null)
        };
    }

    public static Selection3D[] selection3dRange(boolean range, Selection3D start, int len, Protein p) {
        if (start==null||p==null) return null;
        final int f=selection3dToFirstLastIdxZ(true, false, start,  p);
        final int t=selection3dToFirstLastIdxZ(true, true,  start,  p);
        int i1,i2;
        if (!range)  i1=i2=(len<0?f:t)+len;
        else {
            i1=len<0?f+len :f;
            i2=len<0?t : t+len;
        }
        return rangeToSelection3D(true, i1,i2,p);

    }

    /* <<< Shift Intervall <<< */
    /* ---------------------------------------- */
    /* >>> Atoms >>> */
    public static String[] splitAtoms(byte[] T, int B, int E) { return B>=E?NO_STRING : splitAtoms(toStrg(T,B,E)); }
    public static String[] splitAtoms(String s) {
        if (sze(s)==0) return NO_STRING;
        final Object cached[]=cached();
        Map<String,String[]> m=(Map)cached[CACHE_splitAtoms];
        if (m==null) cached[CACHE_splitAtoms]=m=new HashMap();
        String atoms[]=m.get(s);
        if (atoms==null) {
            atoms=splitTokns(s, chrClas1('.'));
            Collection v=null;
            for(int iA=atoms.length; --iA>=0;) {
                final String a=atoms[iA];
                final int L=sze(a);
                final boolean asteriskB=a.charAt(0)=='*', asteriskE=L>0 && a.charAt(L-1)=='*';
                if (asteriskB || asteriskE) {
                    for(String aType : atomsOfAminos()) {
                        final int longer=sze(aType)-L+1;
                        if (longer<0) continue;
                        if (
                            asteriskB && strEquls(a, 1, L,   aType, longer) ||
                            asteriskE && strEquls(a, 0, L-1, aType, 0)
                            ) {
                            v=adUniqNew(aType, v);
                            atoms[iA]=null;
                        }
                    }
                }
                if (v!=null) {
                    adAllUniq(atoms, v);
                    atoms=strgArry(v);
                }
            }
            m.put(s,atoms);
        }
        return atoms;
    }

    public static void changeAtoms(boolean isGeneric, Selection3D ss[], String expr) {
        final String aa[]=isGeneric ? splitAtoms(expr) : null;
        for(int i=sze(ss); --i>=0;) {
            if (ss[i]==null) continue;
            if (isGeneric && aa.length==0 && sze(ss[i].getAtoms())==0) continue;
            ss[i]=
                isGeneric ? ss[i].newInstanceChangeAtoms(aa) :
                ss[i].newInstanceNativeAtoms(expr);
        }
    }

    private static boolean sameChain(Selection3D s, char ins,char chain) {
        return s!=null&&s.getChain()==chain&&s.getInsertion()==ins && s.getResidueName()==null;
    }
    public static Selection3D[] addOrRemove(char operation, Selection3D[] ss0, int frst,int last, char ins, char chain) {
        Selection3D[] ss=ss0;
        final boolean add;
        if (operation=='!') {
            boolean a=true;
            for(int i=sze(ss); --i>=0;) {
                final Selection3D s=ss[i];
                if (!sameChain(s,ins,chain)) continue;
                final int F=s.getFirst(), L=s.getLast();
                if (F<=frst-1 && frst<=L || F<=last && last<=L) {
                    a=false;
                    break;
                }
            }
            add=a;
        } else add=operation=='+';
        boolean needsAdd=add;
        for(int i=sze(ss); --i>=0;) {
            final Selection3D s=ss[i];
            if (!sameChain(s,ins,chain)) continue;
            final int F=s.getFirst(), L=s.getLast();
            if (add) {
                if (F<=frst && last<=L) { needsAdd=false; break; }
                if (F<=frst && frst<=L+1  && L<last)  { ss[i]=new Selection3D(0L, F, last, ins, chain, null); needsAdd=false; }
                if (frst<F  && F<=last+1  && last<=L) { ss[i]=new Selection3D(0L, frst, L, ins, chain, null); needsAdd=false; }
            } else {
                if (frst<=F && L<=last) ss[i]=null;
                else {
                    final boolean wF=F<=frst-1 && frst<=L, wE=F<=last && last<=L;
                    final Selection3D
                        a1=wF && F<=frst-1 ? new Selection3D(0L, F, frst-1, ins, chain, null) : null,
                        a2=wE && last+1<=L ? new Selection3D(0L, last+1, L, ins, chain, null) : null;
                    if (wF||wE) {
                        ss[i]=a1;
                        if (a2!=null) ss=adToArray(a2,ss,0, Selection3D.class);
                    }
                }
            }
        }
        if (needsAdd) ss=adToArray(new Selection3D(0L, frst,last,ins,chain,null), ss,0, Selection3D.class);
        Arrays.sort(ss= rmNullA(ss, Selection3D.class));
        return ss;
    }
    /* <<< Atoms <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */
    public static File withHeterosInOneFile(Protein p) {
        final File file=p==null?null:p.getFileMayBeWithSideChains();
        if (file==null) return null;
        Collection<File>vH=null;
        for(HeteroCompound h : p.getHeteroCompounds('*')) {
            vH=adUniqNew(file, vH);
            vH=adUniqNew(h.getFile(), vH);
        }
        final File fileLoad;
        final int n=sze(vH);
        if (n>1) {
            int size=0;
            for(int i=0; i<n; i++) size+=sze(get(i,vH))+2;
            final BA ba=new BA(size);
            for(int i=0; i<n; i++) ba.aln(readBytes((File)get(i,vH)));
            wrte(fileLoad=newTmpFile(".pdb"), ba);
        } else fileLoad=file;
        return fileLoad;
    }
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> highlight >>> */
    public static void highlightAtoms(Component c, int x, int y, String type) {
        final java.awt.Graphics g=c==null?null: c.getGraphics();
        if (g==null) return;
        g.setColor(C(0xFF8080));
        final boolean isCA=nxt(UPPR,type)>=0;
        final int R=isCA ? EX/2 : EX/4;
        if(type=="S" || type=="s") g.fillOval(x-R,y-R,2*R,2*R);
        if(type=="C" || type=="c") ChIcon.drawCursor(g, x-R,y-R,2*R,2*R, c);
    }
    /* <<< highlight <<< */
    /* ---------------------------------------- */
    /* >>> Customize settings >>> */
    public static String exampleCommands(int viewerId) {
        if (viewerId==JMOL || viewerId==RASMOL || viewerId==JV) {
            return  "spacefill on\n"+
                "spacefill off\n"+
                "cpk on\n"+
                "cpk off\n"+
                "ribbons on\n"+
                "ribbons off\n"+
                "wireframe on\n"+
                "wireframe off\n"+
                "color red\n"+
                "color green\n"+
                "color blue\n"+
                "color yellow\n"+
                "label on\n"+
                "label off\n";
        }
        assrt();
        return null;
    }
    public static void initCustomizableSettings(ProteinViewer pv) {
        final Class c=pv.getClass();
        setSettings(pv,
                    Customize.getCustomizableForClass(c, Customize.CLASS_Example),
                    Customize.getCustomizableForClass(c, Customize.CLASS_ExecPath),
                    Customize.getCustomizableForClass(c, Customize.CLASS_InitCommands),
                    Customize.getCustomizableForClass(c, Customize.CLASS_LateCommands)

                    );
    }

}

