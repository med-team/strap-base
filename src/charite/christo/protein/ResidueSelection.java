package charite.christo.protein;

/**HELP
<i>PACKAGE:charite.christo.strap.</i>

Residue selections are objects attached to proteins which select one or more amino acid positions from the amino acid sequence.

For example dragging the mouse in the alignment pane selects a continuous chain of residues which  may be pasted e.g. into a BLAST web form.

<i>SEE_DIALOG:ResidueAnnotation</i>
<i>SEE_DIALOG:SequenceFeatures</i>

@author Christoph Gille
*/
public interface ResidueSelection extends HasProtein {
    String
        KOPT_DND_COLUMNS="RS$$DAC",
        NAME_MOUSE_OVER="mouse over", NAME_BACKBONE="Backbone", NAME_CURSOR="Cursor", NAME_STANDARD="selected";
    void setProtein(Protein p);
    /**
       @return  true for each selected residue.
    */
    boolean[] getSelectedAminoacids();

    /**
       @return  A possible offset.
       For example {true,true} with offset 6 means that that the 6th and 7th residue is selected.
    */
    int getSelectedAminoacidsOffset();
    /**
       The method body may be kept empty.
    */
    void setSelectedAminoacids(boolean bb[],int offset);
    final static ResidueSelection[] NONE={};

}
