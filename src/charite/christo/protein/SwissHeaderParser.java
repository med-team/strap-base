package charite.christo.protein;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**

See File_example:gb File_example:embl File_example:uniprot

<br><br>
<b>Format Specification:</b> http://www.expasy.org/sprot/userman.html http://www.uniprot.org/manual
   @author Christoph Gille
*/
public class SwissHeaderParser {
    private SwissHeaderParser(){}
    private static boolean[] _delim1, _delim2;
    private static BA _sb;
    public static boolean parse(Protein p, BA ba) {
        if (_sb==null) _sb=new BA(0);
        final BA sb=_sb;
        ba.delBlanksL();
        if (sze(ba)<20) return false;
        final byte[] T=ba.bytes();
        final int ends[]=ba.eol();
        final boolean isDNA=p.containsOnlyACTGNX() || p.getNucleotides()!=null;
        ChTokenizer tokenizer=null;
        byte ss[]=p.getResidueSecStrType();
        if (isDNA) parseCDS(p,ba);
        if (strEquls("LOCUS ",ba)) return parseNCBI(p,ba);
        collectEntries('D','E',sb.clr(),ba);
        p.setCompound(sb.toString());
        collectEntries('O','S',sb.clr(),ba);
        if (sb.end()>0) {
            final int parOpen=strstr(" (",sb);
            int parClose=-1;
            if (parOpen>1) {
                parClose=strstr( ") (",sb,parOpen, MAX_INT); /*OS   Danio rerio (Zebrafish) (Brachydanio rerio).*/
                if (parClose<0) parClose=strstr(").",sb,parOpen, MAX_INT);
            }
            final String os=parClose<0?sb.delSuffix(".").toString() : sb.newString(0,parOpen);
            p.setOrganismScientific(os);
            if (parClose>0) p.setOrganism(sb.newString(parOpen+2,parClose));
            sb.clr();
        }

        p.setEC(strgArry(ec(T,ends,vClr(FINDEC))));

        boolean isEnsemble=false;
        String accession=null;
        for(int pass=0, countCDS=sze(p.getCDS());  pass<2;  pass++) {
            for(int iL=0; iL<ends.length; iL++) {
                final int b= iL==0 ? ba.begin() : ends[iL-1]+1,  e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                if (e-b<6 || T[b+2]!=' ') continue;
                final byte c0=T[b], c1=T[b+1];
                isEnsemble=isEnsemble || c0=='D' && c1=='E' && strEquls("DE   annotated by Ensembl",T,b);
                /* --- FT                   /db_xref="PDB:1IRU" --- */
                /* --- FT                   /db_xref="UniProtKB/TrEMBL:Q9FDF2" */
                if (c0=='F' && c1=='T' && countCDS<2) { /* --- No refs for DNA-files more than one CDS --- */
                    final int dbref=strstr(STRSTR_AFTER,"/db_xref=",T,b,e);
                    if (dbref>0) {
                        int idx=-1;
                        final String DB=
                            (idx=strstr(STRSTR_AFTER,"Swiss-Prot:",T,dbref,e))>0 ? "UNIPROT:" :
                            (idx=strstr(STRSTR_AFTER,"TrEMBL:",T,dbref,e))>0 ? "TREMBL:" :
                            (idx=strstr(STRSTR_AFTER,"PDB:",T,dbref,e))>0 ? "PDB:" :
                            null;
                        if (DB!=null) {
                            final int quote=strchr('"',T,idx,e);
                            if (quote>0) p.addSequenceRef(DB+ba.newString(idx,quote));
                        }
                    }
                }
                if (pass==0) continue;

                if (c0=='F' && c1=='T') {
                    final char c=strEquls("FT   HELIX ",T,b) ? 'H' :strEquls("FT   STRAND ",T,b) ? 'E' : strEquls("FT   TURN ",T,b) ? 'T' : ' ';
                    if (c!=' ' && ss==null) p.setResidueSecStrType(ss=new byte[p.countResidues()]);
                    if (ss!=null) {
                        final int i1=mini(ss.length, atoi(T,b+14,e)), i2=mini(ss.length,atoi(T,b+21,e));
                        if (i1>0 && i2>0 && i2>i1) Arrays.fill(ss, i1-1, i2,(byte)c);
                    }
                }
                if (c0=='I' && c1=='D' || c0=='A' && c1=='C') {
                    if (_delim1==null) _delim1=chrClas(" ;,.");
                    final String db=isEnsemble ? "ENSEMBL:" : isDNA ? "EMBL:" : "UNIPROT:"; // !!!!!!!!!
                    {
                        final String id=tokn(T,b+3,e, _delim1), acc=db+id;
                        if (accession==null && c0=='A') accession=acc;
                        if (c0=='I' && id.indexOf('_')>0) p.setSwissprotID(id);
                        p.addSequenceRef(acc);
                    }
                    /* AC   Q86UR5; A7MBN6; O15048; Q5JY25; Q5SZK1; Q8TDY9; Q8TDZ5; Q9HBA1; */
                    if (T[e-1]==';') {
                        final int semikolon=strchr(';',T,b+3,e-3);
                        if (semikolon>0) {
                            if (tokenizer==null)  tokenizer=new ChTokenizer().setDelimiters(_delim1);
                            tokenizer.setText(T,b+3,e-1);
                            while(tokenizer.nextToken()) p.addSequenceRef(db+tokenizer.asString());
                        }
                    }
                }
                if (c0=='D' && c1=='R') {
                    {
                        final int idx=strstr(STRSTR_AFTER,"PDB; ",T,b+3,e);
                        if (idx>0) {
                            final int semi=strchr(';',T,idx,e);
                            if (semi>0) p.addSequenceRef("PDB:"+ba.newString(idx,semi));
                        }
                    }
                    for(int i=0;i<4;i++) {
                        final String DB,db;
                        switch(i) {
                        case 0: db="Pfam; "; DB="PFAM"; break;
                        case 1: db="PRINTS; "; DB="PRINTS"; break;
                        case 2: db="ProDom; "; DB="PRODOM"; break;
                        case 3: db="PROSITE; "; DB="PROSITE"; break;
                        default: DB=db="PROSITE; ";
                        }
                        final int idx=strstr(STRSTR_AFTER,db,T,b+3,e);
                        if (idx>0) {
                        final int semi=strchr(';',T,idx,e);
                        if (semi>0) p.addDatabaseRef(DB+":"+ba.newString(idx,semi));
                        }
                    }
                }
            }
        }
        if (accession!=null) p.setAccessionID(accession);
        return true;
    }
    private final static Object[] FINDEC={null};

    private static List ec(byte[] T, int ends[], List v) {
        for(int iL=1;iL<ends.length; iL++) {
            final int b=ends[iL-1]+1, e=ends[iL];
            if (e-b<10 || T[b]!='D' || T[b+1]!='E') continue;
            for(int i=b+2; i<e; i++) {
                final boolean gb=T[i]=='(';
                if ( (gb||T[i]==' ') && T[i+1]=='E' && T[i+2]=='C' && T[i+3]==(gb?' ':'=')) {
                    i=nxt(-SPC, T,i+4,e);
                    final int close=i<0?-1:nxt(-DIGT_DOT_DASH, T,i,e);
                    if (close>0) adUniq(bytes2strg(T,i,close), v);
                }
            }
        }
        return v;
    }
    //8848
    private static void collectEntries(char k0, char k1, BA sb, BA ba) {
        final int ends[]=ba.eol();
        final byte[] T=ba.bytes();
        loop_lines:
        for(int iL=0; iL<ends.length; iL++) {
            final int b= iL==0 ? ba.begin() : ends[iL-1]+1,  e=prev(-SPC,T,ends[iL]-1, b-1)+1;
            if (e-b<5 || k0!=T[b] || k1!=T[b+1] || ' '!=T[b+2]) continue;
            String skip=null;
            int nSkip=b+2;
            if (k0=='D' && k1=='E') {
                if (strEquls("   AltName: ",T,b+2)) continue;
                if (strEquls(skip="   RecName: Full=",T, b+2)) nSkip=skip.length()+b+2;
                final int noSpc=nxt(-SPC,T,b+2,e);
                if (noSpc<0) continue;
                final int endEC=e-noSpc>2 && T[noSpc]=='E' && T[noSpc+1]=='C' && T[noSpc+2]=='=' ? nxt(-DIGT_DOT_DASH, T,noSpc+3,e) : -1;
                if (endEC>0) nSkip=endEC+1;
            }
            final int nxtNoSpc=nxt(-SPC,T,nSkip,e);
            if (nxtNoSpc>0) {
                if (sb.end()>0) sb.a(' ');
                sb.a(T,nxtNoSpc, e);
            }
        }
    }
    private static boolean parseNCBI(Protein p, BA ba) {

        final byte[] T=ba.bytes();
        final int begin=ba.begin(), ends[]=ba.eol();
        final List v=vClr(FINDEC);
        ec(T,ends,v);
        for(int iL=0; iL<ends.length; iL++) {
            final int b0= iL==0 ? begin : ends[iL-1]+1,  e=prev(-SPC,T,ends[iL]-1,b0-1)+1;
            final int b=nxt(-SPC,T,b0,e);
            if (b<0 || e-b<9) continue;
            final byte c0=T[b];
            if (c0=='O' && strEquls("ORGANISM ",T,b))  p.setOrganismScientific(strgTrim(T,nxt(-SPC,T,b+8,e),e));
            if (c0=='A' && strEquls("ACCESSION ",T,b))  p.setAccessionID("ENTREZ:"+tokn(T,nxt(-SPC,T,b+9,e),e, null));
            if (c0=='D' && strEquls("DEFINITION ",T,b)) {
                String skip=null;
                int f=nxtE(-SPC, T, b+10, e);
                if (strEquls(skip="RecName: Full=", T,f)) f+=skip.length();
                int t=strchr(STRSTR_E, '[', T, f,e);
                t=strstr(STRSTR_E, "AltName:",T,f,t);
                p.setCompound(strgTrim(T,f, t));
            }
            if (c0=='V' && strEquls("VERSION ",T,b)) { /* --- VERSION     M57965.2  GI:24429600 --- */
                final int gi=strstr("GI:",T,b+7,e);
                if (gi>0) p.addSequenceRef(tokn(T,gi,e, null));
            }
            if (c0=='D' && strEquls("DBSOURCE ",T,b)) {  /* --- DBSOURCE    pdb: molecule 1CF1, chain 68,  --- */
                final int pdb=strstr(STRSTR_AFTER,"pdb: molecule",T,b+7,e);
                if (pdb>0) {
                    final int iChain=strstr(STRSTR_AFTER,", chain ",T,pdb,e);
                    final int chain=iChain>0 ? atoi(T,iChain,e) :0;
                    if (_delim2==null) _delim2=chrClas(", ");
                    p.setPdbID("PDB:"+tokn(T,pdb,e, _delim2).toLowerCase()+ (chain>='0' ? ":"+(char)chain:""));
                }
            }
        }
        p.setEC(strgArry(v));
        return true;
    }
    private  static BA baCDS;
    private static void parseCDS(Protein p, BA ba) {
        if (baCDS==null) baCDS=new BA(0);
        final byte T[]=ba.bytes();
        final int[] ends=ba.eol();
        boolean found=false;
        final BA sbXref=new BA(333);
        for(int source=0; source< (found?1:2); source++) {
            for(int iL=0; iL<ends.length; iL++) {
                final int b= iL==0 ? ba.begin() : ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                if (e-b<6 || !(T[b]=='F'&& T[b+1]=='T' || T[b]==' '&& T[b+1]==' ')) continue;
                final int nonSpc=nxt(-SPC,T, b+2,e);
                if (nonSpc<0) continue;
                if (source>0) { /* FT   source          1..754 */
                    final int idxSource=strEquls("source ",T,nonSpc) ? nxt(-SPC,T,nonSpc+7,e) : -1;
                    if (idxSource>0 && strstr("..",T,idxSource,e)>0) p.addCDS(ba.newString(idxSource,e),new String[]{"All","","",""});
                    break;
                }
                final int idxCDS=strEquls("CDS ",T,nonSpc) ? nxt(-SPC,T,nonSpc+4,e) : -1;
                if (idxCDS>0) {

                    sbXref.clr();
                    boolean alreadyProteinID=false;
                    String sCDS=null, ss[]=null;
                    baCDS.clr().a(T,idxCDS,e);
                    final int indent=idxCDS-b;
                    for(int jL=iL+1; jL<ends.length; jL++) {
                        final int b1=ends[jL-1]+1, e1=prev(-SPC,T,ends[jL]-1,b1-1)+1, nxt=b1+indent;
                        if (nxt(-SPC,T,b1+2,e1)!=nxt) break;
                        if (sCDS==null) {
                            if (T[nxt]=='/') sCDS=baCDS.toString();
                            else baCDS.a(T,nxt,e1);
                        }
                        if (sCDS!=null) {
                            for(int i=7;  --i>=0;) {
                                final String s=
                                    i==Protein.CDS_GENE ? "/gene=" :
                                    i==Protein.CDS_PRODUCT ? "/product=" :
                                    i==Protein.CDS_PROTEIN ? "/protein_id=" :
                                    i==Protein.CDS_TRANSLATION ? "/translation=" :
                                    i==Protein.CDS_TRANSCRIPT_ID ? "/note=\"transcript_id=" :
                                    "/note=" ;
                                final int quote=nxt+sze(s);
                                if (strEquls(s,T,nxt) && quote<e1) {
                                    if (ss==null) ss=new String[7];
                                    ss[i]=ba.newString(quote+ (T[quote]=='"'?1:0), e1- (T[e1-1]=='"'?1:0));
                                    break;
                                }
                            }
                            if (strEquls("/db_xref=\"",T,nxt)) {
                                final int n10=nxt+10;
                                final boolean isProtId=strEquls("protein_id",T,n10);
                                if (!strEquls("AFFY_",T,n10) && !strEquls("Illumina_",T,n10) && !strEquls("GO:GO:",T,n10) && !strEquls("Vega_transcript",T,n10) && !strEquls("Agilent",T,n10) && !(isProtId && alreadyProteinID)) {
                                    alreadyProteinID|=isProtId;
                                    String x=toStrg(T,n10, strchr(STRSTR_E,'"',T,n10,e1));
                                    final byte c0=T[n10];
                                    //if (c0=='p') x=rplc("protein_id","NCBI",x);
                                    if (c0=='U') x=rplcToStrg("Uniprot/SPTREMBL:","UNIPROT:",x);
                                    if (c0=='U') x=rplcToStrg("Uniprot/SWISSPROT:","UNIPROT:",x);
                                    if (c0=='E') x=rplcToStrg("EntrezGene:","NCBI:",x);
                                    if (c0=='R') x=rplcToStrg("RefSeq_dna:","NCBI:",x);
                                    if (c0=='R') x=rplcToStrg("RefSeq_peptide:","NCBI:",x);
                                    sbXref.a(x).a('\t');
                                }
                            }
                        }
                    }
                    if (sCDS!=null) {
                        while (p.containsCDS(sCDS)) sCDS+=" ";
                        if (sCDS.indexOf('(')>0 && sCDS.indexOf(')')<0) assrt();
                        if (ss==null) ss=new String[5];
                        ss[Protein.CDS_XREFS]=sbXref.toString();
                        p.addCDS(sCDS, ss);
                        found=true;
                    }
                }
            }
        }
    }

}
