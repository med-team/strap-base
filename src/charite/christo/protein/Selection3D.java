package charite.christo.protein;
import static charite.christo.ChUtils.*;
public class Selection3D implements Comparable {
    public final static Selection3D[] NONE={};
    private long _opt;
    private int _first, _last;
    private char _ins, _chain;
    private String _atoms[], _name, _nativeAtoms;
    @Override public String toString() { return "Selection3D name="+_name+" first="+_first+" last="+_last;}
    public Selection3D(Selection3D s) {
        this(s._opt,s._first, s._last, s._ins, s._chain, s._atoms);
        _nativeAtoms=s._nativeAtoms;
        _name=s._name;
    }

    public Selection3D(long options, int first, int last, char insertion, char chain, String atoms[]) {
      _opt=options;
      _first=first;
      _last=last;
      _ins=insertion;
      _chain=chain;
      _atoms=atoms;
  }

    public Selection3D(long options, String name, char chain, String atoms[]) {
        _opt=options;
        _name=name;
        _chain=name==null?chain:0;
        _atoms=atoms;
    }

    public long getOptions() { return _opt;}

    public int getFirst() {return _first;}
    public int getLast() {return _last;}

    public char getInsertion() { return _ins;}
    public char getChain() { return _chain;}

    public String[] getAtoms() { return _atoms;}
    public String getResidueName() { return _name; }
    public String getNativeAtoms() { return _nativeAtoms; }

    public Selection3D newInstanceChangeAtoms(String[] atoms) {
        final Selection3D n=new Selection3D(this);
        n._atoms=atoms;
        return n;
    }
    public Selection3D newInstanceNativeAtoms(String atoms) {
        final Selection3D n=new Selection3D(this);
        n._nativeAtoms=atoms;
        return n;
    }

    public int compareTo(Object o) {
        final Selection3D s=(Selection3D)o;
        final String nThis=getResidueName(), n=s.getResidueName();
        if (nThis!=null && n!=null) return n.compareTo(nThis);
        else if (nThis!=null) return 1;
        else if (n!=null) return -1;
        final long iThis=(  (long)  _chain<<63) | ((long)  _first<<32) | ((long)  _last<<1) |   _ins;
        final long i=    (  (long)s._chain<<63) | ((long)s._first<<32) | ((long)s._last<<1) | s._ins;
        return iThis==i ? 0 : iThis<i?-1 : 1;
    }

}
