package charite.christo.protein;
import charite.christo.*;
import java.io.File;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

The HeteroCompound class can holds DNA / RNA and hetero compound structures.

See  FMN.txt  and DNA.txt in http://www.bioinformatics.org/strap/dataFiles/proteinFileExamples/.

<br><br>

The compound inherits the 3D-transformation (rotation and translation)
from the peptide structure it is assigned to.
Two cases:
<ol>

<li>The compouned is recorded together with a peptide in one file.</li>

<li>The compouned is stored in a PDB file alone and is assigned to a peptide (Least Euclidean distance, Drag'n Drop ...)</li>

</ol>

   @author Christoph Gille
*/
public class HeteroCompound implements HasRendererMC,  HasMap, HasWeakRef, ChRunnable {
    public final static HeteroCompound NONE[]={};
    public final static int ONLY_PHOSPHATE=1;
    private int _countNuc, _cName32,  _cNo;
    private String _nuclSeq, _name;
    private boolean _isNucl, _isDesox;
    private char _cChain;
    private short[] _element;

    private byte[] _longName, _nuclBase, _atomType;
    private int[] _nuclNo, _atomNo, _atomName32;
    private float[] _coord, _occupancy, _tempFactor;

    public boolean isNucleotideChain() { return _isNucl;}
    public boolean isDNA() { return _isNucl&&_isDesox;}

    public HeteroCompound() {
        pcp(KEY_DND_NOT_ITSELF,"",this);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */
    private File _file, _peptideFile;
    public void setFile(File f) {_file=f; }
    /** Not null if it is defined in a PDB file on its own */
    public File getFile() { return _file;}
    /** Not null if it is defined in the same file as a peptide chain */
    public File getPeptideFile() { return _peptideFile;}
    public void setPeptideFile(File f) { _peptideFile=f;}
    public char getCompoundChain() { return _cChain;}
    public int countAtoms() { return sze(_atomType);}
    public int getCompoundName32() { return _cName32;}
    public int getCompoundNumber(){ return _cNo;}
    public byte[] getNucleotideBase() { return _nuclBase;}
    public byte[] getLongName() { return _longName!=null ? _longName:NO_BYTE;}
    public byte[] getAtomType() { return _atomType;}
    public int[] getNucleotideNumber() { return _nuclNo;}
    public int[] getAtomName32() { return _atomName32;}
    public int[] getAtomNumber(){ return _atomNo;}

    public String getCompoundName() {
        if (_name==null) _name=new BA(4).aSomeBytes(getCompoundName32(),4).delBlanksL().delBlanksR().toString();
        return _name;
    }

    private Object[] _arr4equ;
    private Object[] getArraysForEqual() {
        if (_arr4equ==null)
            _arr4equ=new Object[]{
                new int[]{_cName32, _cChain,_cNo},
                _nuclBase,_nuclNo,_longName,_atomName32,_atomType,_element};
        return _arr4equ;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Parsing >>> */
    public static HeteroCompound[] parse(BA ba, boolean nuclBackbone) {
        _lastHasWater=false;
        final int[] DD[]=atoiLookup(), P0=DD[0], P1=DD[1], P2=DD[2], P3=DD[3];
        Collection<HeteroCompound> v=null;
        int lastNo=0, lastName=0,  lastL=-1;
        byte lastChain=0;
        boolean lastNuc=false, lastDesox=false;
        final byte[] T=ba.bytes();
        final int[] ends=ba.eol();
        for(int iL=0; iL<=ends.length; iL++) {
            final int b= iL==0 ? ba.begin() : ends[iL-1]+1, e=iL<ends.length ? ends[iL] : 0;
            if (e-b>6 && T[b]=='A' && T[b+1]=='N' && T[b+2]=='I' && T[b+3]=='S' && T[b+4]=='O' && T[b+5]=='U' && T[b+6]==' ') continue;
            final int no, name;
            final byte chain;
            final boolean isDesoxy;
            final int ACTGU;
            if (e-b>53 && T[b]=='A' && (T[b+17]==' '||T[b+19]==' ')) {
                isDesoxy=T[b+17]=='D' || T[b+18]=='D';
                ACTGU=isDesoxy ? (T[b+17]==' '?19:18) : (  T[b+18]==' ' ? (T[b+17]==' '?19:17) : (T[b+17]|T[b+19])==' '?18:0 );
                } else { ACTGU=0; isDesoxy=false;}
            final byte actgu=ACTGU>0?T[b+ACTGU]:0;
            final boolean
                isNuc=actgu>0 && (actgu=='A' || actgu=='C' || actgu=='T' || actgu=='G' || actgu=='U') && T[b+1]=='T' && T[b+2]=='O' && T[b+3]=='M',
                isHet=e-b>53 && T[b+5]=='M' && T[b]=='H' && T[b+1]=='E' && T[b+2]=='T' && T[b+3]=='A' && T[b+4]=='T' ;
            if (isNuc || isHet) {
                chain=T[b+21];
                name=charsTo32bit(T[b+17],T[b+18],T[b+19],' ');
                {
                    final int cn=P0[T[b+25]]+P1[T[b+24]]+P2[T[b+23]]+P3[T[b+22]];
                    no=T[b+26]=='-'||T[b+25]=='-'||T[b+24]=='-' ? -cn : cn;
                }
            } else no=name=chain=0;
            if (lastName!=0) {
                if ( name==0 || lastChain!=chain || isHet && (lastName!=name||lastNo!=no)) {
                    final int n=lastName;
                    if (n!=HETERO_HOH&&n!=HETERO_HHO&&n!=HETERO_OHH&&n!=HETERO_H2O&&n!=HETERO_DOD&&n!=HETERO_DDO&&n!=HETERO_ODD&&n!=HETERO_D2O) {
                        final HeteroCompound ha=parseBlock(T,ends,lastL,iL, lastNuc);
                        if (ha!=null) {
                            v=adNotNullNew(ha,v);
                            ha._cChain=(char)lastChain;
                            if (!lastNuc) ha._cName32=n;
                            if (!lastNuc) ha._cNo=lastNo;
                            ha._isNucl=lastNuc;
                            ha._isDesox=lastNuc&&lastDesox;
                        }
                    } else _lastHasWater=true;
                    lastName=0;
                }
            } else {
                if (name!=0) {
                    lastChain=chain;
                    lastName=name;
                    lastNo=no;
                    lastL=iL;
                    lastNuc=isNuc;
                    lastDesox=isDesoxy;
                }
            }

            if (e-b>5 && T[b]=='E' && T[b+1]=='N' && T[b+2]=='D' && T[b+3]=='M' && T[b+4]=='D' && T[b+5]=='L') break;

        }
        final HeteroCompound[] hh= toArry(v, HeteroCompound.class);
        if (!nuclBackbone) {
            for(int iL=0; iL<ends.length; iL++) {
                final int b= iL==0 ? 0 : ends[iL-1]+1;
                final int e=ends[iL];
                if (e-b>16 && T[b]=='H' && T[b+1]=='E' && T[b+2]=='T' && T[b+3]=='N' && T[b+4]=='A' && T[b+5]=='M') {
                    final int nam=charsTo32bit(T,b+11);
                    for(HeteroCompound h:hh) {
                        if (h.getCompoundName32()==nam) {
                            int end=e;
                            while(end-1>b && isSpc(T[end-1])) end--;
                            if (end-b>15) System.arraycopy(T,b+15,h._longName=new byte[end-b-15],0,end-b-15);
                        }
                    }
                }
            }
        }
        return hh;
    }

    private static HeteroCompound parseBlock(byte[] T, int ends[], int fromL, int toL, boolean isNuc) {
        final int[] DD[]=atoiLookup(), P0=DD[0], P1=DD[1], P2=DD[2], P3=DD[3], P4=DD[4], P5=DD[5];
        final float[] FF[]=atofLookup(), P_1=FF[1], P_2=FF[2], P_3=FF[3];

        final HeteroCompound ha=new HeteroCompound();
        final int n=toL-fromL;
        final float[] coord=new float[3*n], occupancy=new float[n], tempFactor=new float[n];
        final byte[] atomType=new byte[n];
        final int[] atomName32=new int[n], atomNo=new int[n];
        final short element[]=new short[n];
        final byte base[]=isNuc ? new byte[n]:null;
        final int[] baseNo=isNuc ? new int[n] : null;
        for(int iL=fromL, iA=0; iL<toL; iL++,iA++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1;
            final int e=ends[iL];
            if (e-b<53 || T[b]=='A' && T[b+1]=='N') continue;
            atomNo[iA]=P0[T[b+10]]+P1[T[b+9]]+P2[T[b+8]]+P3[T[b+7]]+P4[T[b+6]]+P5[T[b+5]];
            atomType[iA]=T[b+13];
            atomName32[iA]=charsTo32bit(T[b+12],T[b+13],T[b+14],T[b+15]);
            occupancy[iA]=e-b>58 ?  (P0[T[b+59]]+P1[T[b+58]]+P2[T[b+56]]+P3[T[b+55]]+P4[T[b+54]])*.01f : Float.NaN;
            tempFactor[iA]=e-b>64? (P0[T[b+65]]+P1[T[b+64]]+P2[T[b+62]]+P3[T[b+61]]+P4[T[b+60]])*.01f : Float.NaN;
            for(int i=0,p=37;i<3;p+=8,i++) {
                final int idx=iA+iA+iA+i;
                coord[idx]= P_3[T[b+p]] + P_2[T[b+p-1]] + P_1[T[b+p-2]] + P0[T[b+p-4]] + P1[T[b+p-5]] + P2[T[b+p-6]];
                if (T[b+p-4]=='-'||T[b+p-5]=='-'||T[b+p-6]=='-'||T[b+p-7]=='-') coord[idx]=-coord[idx];
            }
            if (isNuc) {
                base[iA]=T[ b+ (T[b+19]!=' '?19:T[b+18]!=' '?18:17) ];
                final int cn=P0[T[b+25]]+P1[T[b+24]]+P2[T[b+23]]+P3[T[b+22]];
                baseNo[iA]=T[b+26]=='-'||T[b+25]=='-'||T[b+24]=='-' ? -cn : cn;
            }
            element[iA]=e-b>76?(short)(T[b+76]+(T[b+77]<<8)) : (short)(' '+(atomType[iA]<<8));
        }
        ha._occupancy=occupancy;
        ha._tempFactor=tempFactor;
        ha._element=element;
        ha._atomName32=atomName32;
        ha.setCoordinates(coord);
        ha._atomType=atomType;
        ha._atomNo=atomNo;
        ha._nuclBase=base;
        ha._nuclNo=baseNo;
        if (isNuc) {
            final BA ba=baTip();
            int c=0;
            for(int i=n; --i>=0;) {
                if (atomType[i]!='P') continue;
                ba.a((char)base[i]);
                c++;
            }
            ha._countNuc=c;
            ha._nuclSeq=ba.toString();
        }
        //int F=fromL==0 ? 0 : ends[fromL-1]+1, E=ends[toL];  TabItemTipIcon.set(null,null,  "<html><body><pre>"+ new String(T,F,E-F)+"</pre></body></html>",null,ha);
        return ha;
    }
    private static boolean _lastHasWater;
    static boolean lastParseHasWater() { return _lastHasWater;}

    /* <<< Parsing <<< */
    /* ---------------------------------------- */
    /* >>> Export >>> */
    public BA toText(int mode, Matrix3D m3d, BA sb0) {
        final BA sb=sb0!=null?sb0 : new BA(countAtoms()*80);
        final boolean onlyP= (mode&ONLY_PHOSPHATE)!=0, isNucl=isNucleotideChain();
        final int nA=countAtoms(), cNum=getCompoundNumber(), cName=getCompoundName32();
        final int[] nucNo=getNucleotideNumber();
        final int aNum[]=getAtomNumber(), aName[]=getAtomName32();

        final char chain=getCompoundChain();
        final float[] xyzOri=getCoordinates(), xyz=m3d==null||m3d.isUnit()?xyzOri:m3d.transformPoints(xyzOri,null,MAX_INT);
        final byte[] aType=getAtomType(), base=getNucleotideBase();

        if (!isNucleotideChain()) sb.a("HETNAM     ").aSomeBytes(getCompoundName32(),4).aln(getLongName());

        final char spaceOrD=isDNA() ? 'D' : ' ';
        for(int iA=0,i3=0; iA<nA; iA++,i3+=3) {
            if (onlyP && aType[iA]!='P') continue;
            sb.a(isNucl?"ATOM  ":"HETATM")
                .a(aNum[iA],5).a(' ')
                .aSomeBytes(aName[iA],4).a(' ');
            if (base!=null) sb.a(' ').a(spaceOrD).a((char)base[iA]).a(' ');
            else sb.aSomeBytes(cName,4);
            sb.a(chain)
                .a(nucNo!=null ? nucNo[iA] : cNum,4).a(' ',4)
                .a(xyz[i3],4,3).a(xyz[i3+1],4,3).a(xyz[i3+2],4,3)
                .a(_occupancy[iA],3,2).a(_tempFactor[iA],3,2)
                .a(' ',10).aSomeBytes(_element[iA],2).a('\n');
        }
        return sb;
    }
    private String _namePymol;
    public String getNameForPymol() {
        if (_namePymol==null) {
        char chain=getCompoundChain();
        if (chain==0||chain==' ') chain='_';
        _namePymol=
            !_isNucl ? baTip().aSomeBytes(getCompoundName32(),4).a(getCompoundNumber(),4).a('_').a(chain).replaceChar(' ','_').toString() :
            _file!=null ? delDotSfx(_file.getName()).replaceAll("\\W","_") :
            (_isDesox?'D':'R')+"NA_chain_"+chain;
        }
        return _namePymol;
    }
    /* <<< Export <<< */
    /* ---------------------------------------- */
    /* >>> Protein >>> */
    private Object _prot;
    void setBelongsToProtein(Object ref) {
        if (_prot!=ref) {
        _prot=ref;
        _rt=null;
        }
    }
    public Protein getBelongsToProtein() {return deref(_prot,Protein.class);}
    public Protein closestPeptide(Protein[] pp, long options) {
        float distance=Float.MAX_VALUE;
        Protein best=null;
        for(Protein p : pp) {
            final float xyz[]=p!=null ? p.getResidueCalpha(null) : null;
            if (xyz==null) continue;
            final float d=squareDistanceToNucleotidechain(xyz, 0, MAX_INT, null,options);
            if (d<distance) { best=p; distance=d; }
        }
        return best;
    }
    /* <<< Coordinates <<< */
    /* ---------------------------------------- */
    /* >>> Protein >>> */
    public final float squareDistanceToNucleotidechain(float[] xyz, int fromIdx, int toIdx, Matrix3D m3d, long options) {
        final boolean onlyP= (options&ONLY_PHOSPHATE)!=0;
        final float[] xyzOri=getCoordinates(), xyzH=m3d==null||m3d.isUnit()?xyzOri:m3d.transformPoints(xyzOri,null,MAX_INT);
        final byte[] aType=getAtomType();
        float distance=Float.MAX_VALUE;
        for(int iH=countAtoms(); --iH>=0;) {
            if (onlyP && aType[iH]!='P') continue;
            final int h3=3*iH;
            final float d=squareDistance(xyzH[h3], xyzH[h3+1], xyzH[h3+2], xyz, fromIdx, toIdx);
            if (distance>d) distance=d;
        }
        return distance;
    }
    public void setCoordinates(float xyz[]) { _coord=xyz; }
    public float[] getCoordinates() { return _coord; }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    private String _rt, _toStrg;
    public Object getRenderer(long options, long rendOptions[]) {
        final Protein p=getBelongsToProtein();
        if (_rt==null) {
            final BA sb=baTip().a(this).a(' ');
            if (_isNucl) {
                if (_countNuc<30) sb.a(_nuclSeq); else sb.a("has ").a(_countNuc).a(" nucleotides ");
            } else sb.a(getLongName());
            _rt=sb.and("  (",p,")").toString();
        }
        if (rendOptions!=null) rendOptions[0]=p==null?HasRenderer.STRIKE_THROUGH : 0L;
        return _rt;
    }
    @Override public String toString() {
        if (_toStrg==null)  {
            if (_isNucl) {
                final File f=getFile();
                _toStrg= (_isDesox?'D':'R')+"NA " +(f!=null ? f.getName() :"chain "+getCompoundChain());
            } else _toStrg=baTip().aSomeBytes(getCompoundName32(),4).a(getCompoundNumber(),5).a(':').a(getCompoundChain()).toString();
        }
        return _toStrg;
    }
    private final long _rendMC=(long)hashCode()<<32;
    public long getRendererMC() { return _rendMC; }

    /* <<< Renderer <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */
    private int _dndMC;
    private Object _dndMx;
    private File _fDnD;
    public Object run(String id, Object arg) {
        if (id==PROVIDE_DND_FILES) {
            File f=_fDnD;
            final Protein p=getBelongsToProtein();
            final Matrix3D m3d=(!notSelctd(BUTTN[TOG_DND_TRANSFORM_HETERO])) && p!=null ? p.getRotationAndTranslation() : null;
            final int mc=modic(m3d);
            if (sze(f)==0 || _dndMC!=mc || _dndMx!=m3d) {
                _dndMx=m3d;
                _dndMC=mc;
                final BA ba=toText(0, m3d, null);
                f=_fDnD=file(new BA(99).a(dirTmp()).a('/').a(getNameForPymol()).a('_').aHex(hashCd(ba,0,MAX_INT),8).a(".pdb"));
                wrte(f,ba);
                delFileOnExit(f);
            }
            return new File[]{f};
        }
        return null;
    }
    /* <<< DnD <<< */
    /* ---------------------------------------- */
    /* >>> HasMap >>> */
    private Map _clientObjects;
    public Map map(boolean create) {return _clientObjects==null&&create?_clientObjects=new HashMap() : _clientObjects;}
    public final Object _wr=newWeakRef(this);
    public Object wRef() { return _wr;}
    /* <<< HasMap <<< */
    /* ---------------------------------------- */
    /* >>> Equals >>> */
    @Override public boolean equals(Object o2) {
        if (this==o2) return true;
        if (!(o2 instanceof HeteroCompound)) return false;
        final Object[] a=getArraysForEqual(), a2=((HeteroCompound)o2).getArraysForEqual();
        for(int i=a.length; --i>=0;) {
            if (sze(a[i])!=sze(a2[i])) return false;
            if (a[i] instanceof   int[] && !Arrays.equals((  int[])a[i],(  int[])a2[i])) return false;
            if (a[i] instanceof  byte[] && !Arrays.equals(( byte[])a[i],( byte[])a2[i])) return false;
            if (a[i] instanceof short[] && !Arrays.equals((short[])a[i],(short[])a2[i])) return false;
        }
        return true;

    }

}

/*

Example DNA: pdb1gd2 zn-finger  pd
b1q82 (Ribosom(  pdb1l4p.ent

Superimpose pdb1cqt_A.ent pdb1cqt_B.ent

Flavine with Biological Unit:
pdb1fiq.ent pdb1jnw.ent pdb1jqi.ent pdb1p0n.ent pdb1qzu.ent pdb1reo.ent pdb1ryi.ent pdb1sbz.ent pdb1siq.ent pdb1sze.ent pdb1szf.ent pdb1szg.ent pdb1t57.ent pdb1v4b.ent

*/
