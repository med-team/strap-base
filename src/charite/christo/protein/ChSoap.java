package charite.christo.protein;
import charite.christo.*;
import java.net.*;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
   // SocketOutputStream  BufferedInputStream
*/
public class ChSoap {
    public final static int NCBI=1, EBI=2;
    private ChSoap() {}
    public final static String TYPE_OUT="out";
    public final static int STATUS_RUNNING=1, STATUS_FINISHED=2;

    public final static long ASK_FOR_PERMISSION=1<<1;

    public static BA getResponse(long options, BA query, BA buffer) {
        final int iXml=strstr(STRSTR_END, "<?xml",query);
        final int iHost=nxt(-SPC, query, strstr(STRSTR_AFTER, "\nHost: ",query, 0, iXml), iXml);
        final String host=wordAt(iHost, query);

        if (sze(query)<10 || 0!=(options&ASK_FOR_PERMISSION) && !Web.uploadPermission(ChFrame.CENTER, query, host)) {
            return BA.error(BA.ERROR_PERMISSION_DENIED_BY_USER);
        }
        OutputStream os=null;
        InputStream is=null;
        BA answer=null;
        Socket socket=null;
        try {
            socket=Web.newSocket(host);
            os=socket.getOutputStream();
            if (os!=null) {
                setContentLength(query);
                os.write(query.bytes(),0,query.end());
                os.flush();
                is=socket.getInputStream();
                answer=readBytes(is, buffer);
            }
        } catch(IOException ex){ stckTrc(ex);}
        closeStrm(os);
        closeStrm(is);
        closeStrm(socket);
        return answer;
    }

    private static BA _rsc;
    public static BA getRsc(String s) {
        final String fn="soap.rsc";
        synchronized(fn) {
            if (_rsc==null) _rsc=readBytes(rscAsStream(0L,_CCP,fn));
            if (_rsc==null) assrt();
        }
        int from=strstr(STRSTR_w|STRSTR_AFTER, new BA(33).a('#').a(s), _rsc);
        if (from<0) {
            putln(RED_ERROR,"ChSoap.getRsc ",s);
            assrt();
            return null;
        }
        from=nxtE(-SPC, _rsc,from,MAX_INT);
        final int to=strstr(STRSTR_E|STRSTR_AFTER, "</soapenv:Envelope>", _rsc,from, MAX_INT);
        return new BA(_rsc.newBytes(from,to));
    }

    public static int getStatus(String jobId0) {
        final String jobId=delPfx("NCBI:",jobId0);
        final BA q=getRsc(jobId0!=jobId ? "soapNcbiBlastStatus" : "soapGetStatusEBI");
        if (q==null) return 0;
        final BA answer=getResponse(0L, q.replace(0L,"JOBID",jobId),null);
        final int i=strstr(STRSTR_AFTER,"<status>",answer);
        return i<0 ? 0 :
            strEquls("ready", answer, i) || strEquls("FINISHED", answer, i) ? STATUS_FINISHED :
            strEquls("RUNNING",  answer, i) ? STATUS_RUNNING : 0;
    }

    public static boolean setContentLength(BA ba) {
        final byte[] T=ba.bytes();
        final int iXml=strstr("<?xml ",ba), iCont=strstr(STRSTR_AFTER, "Content-Length: ",ba);
        if (iXml<0 || iCont<0) return false;
        final int e= strstr(STRSTR_AFTER|STRSTR_E,"Envelope>",ba);
        int len=e-iXml, pos=iCont;
        while(chrAt(pos+1,ba)=='d') T[pos++]=' ';
        while(len>0) {
            if (pos<=iCont) {assrt(); return false;}
            T[pos--]=(byte)('0'+len%10);
            len/=10;
        }
        return true;
    }

    public static BA getResultDecode64(String jobId, String type, BA buffer) {
        final BA q=getRsc("soapGetResultEBI");
        if (q==null) return null;
        final BA answer=getResponse(0L, q.replace(0L,"JOBID",jobId).replace(0L,"TYPE",type), buffer);
        final int i=strstr(STRSTR_AFTER,"<output>",answer);
        final int gt=i<0?-1:strchr('<',answer,i,MAX_INT);
        return gt<0 ? null : charite.christo.libs.Base64.decode(answer.bytes(), i, gt);
    }

}

