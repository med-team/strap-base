package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

Sequence files where the sequence block is  at the end of the file.

See File_example:gb File_example:embl File_example:uniprot

This sequence block might have preceding or trailing
position numbers.

In case there is no numbering then the sequence
block must be indented by space of any length.  To identify the first
and last line of this sequence block the program starts looking from
the end of the file.  This is because files usually start with
annotations and references which might be mistaken for the sequence if
started from the first line.

<br>
GenBank sample: http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html ftp://ftp.ncbi.nih.gov/genbank/gbrel.txt

   @author Christoph Gille
*/
public class NumberedSequence_Parser implements ProteinParser {
    public boolean parse(Protein prot, long options, BA byteArray ) {
        final int  ends[]=byteArray.eol();
        final byte[] T=byteArray.bytes();
        int from=-1,to=-1;
        boolean numbersL=false,numbersR=false;
        nextLine:
        for(int iL=ends.length; --iL>=0; ) {
            final int begin= iL==0 ? byteArray.begin() : ends[iL-1]+1;
            final int end=ends[iL];
            if (to<0) {
                numbersL=isLineOK(T,begin,end,false, true,false);
                numbersR=isLineOK(T,begin,end,false, false,true);
            }
            final boolean isOK=isLineOK(T,begin,end, to>0 ,numbersL,numbersR);
            if (isOK) {
                from=begin;
                if (to<0) to=end;
            } else if (to>=0) break;

        }
        if (from<0) return false;
        final byte[] aa=toByts(filtr(0L,LETTR,T,from,to));
        if (aa!=null && aa.length>0) {
            boolean upCase=false;
            for(int i=aa.length; --i>=0;) {
                if ((aa[i]&32)==0) { upCase=true; break;}
            }
            if (!upCase) for(int i=aa.length; --i>=0;) aa[i]&= ~32;           
            prot.setResidueType(aa);
            SwissHeaderParser.parse(prot,byteArray);
            return true;
        }
        return false;
    }

    private static boolean isLineOK(byte T[], final int start, final int end,boolean acceptBlank, boolean needsNumL, boolean needsNumR) {
        boolean numbersL=false, numbersR=false, spaceL=false;
        int from=start, to=end;
        while(from<to && ( T[from]==' ' || T[from]=='\t')) { spaceL=true; from++;}
        while(from<to) {
            final byte c=T[to-1];
            if (!(c==' ' || c=='\r' || c=='\n' || c=='\t')) break;
            to--;
        }
        if (from==to) return acceptBlank;
        while (from<to && is(DIGT,T,from)) {
            from++;
            numbersL=true;
        }
        while (to>from && is(DIGT,T,to-1)) {
            to--;
            numbersR=true;
        }
        if (numbersR && !is(SPC,T,to-1)) return false;
        if (!(numbersL||numbersR||spaceL)) return false;
        if (needsNumR!=numbersR || needsNumL!=numbersL) return false;
        for(int i=from; i<to; i++) {
            final int b=T[i];
            if (!(b>='A' && (b<='Z' || b>='a' && b<='z') || b==' ' || b=='\t' || b=='\r')) return false;
        }
        return true;
    }

    public static int parseMultipleSwiss(BA ba, Protein pp[]) {
        if (MultipleSequenceParser.isFormat(MultipleSequenceParser.MSF, ba, (Range)null, (int[])null)>0) return 0;
        final int ends[]=ba.eol();
        final byte[] T=ba.bytes();
        int count=0;
        boolean hasSeq=false;
        String id=null;
        int from=ba.begin();
        boolean isGenbank=false;
        for(int iL=0; iL<ends.length; iL++) {
            final int b=iL==0 ? from: ends[iL-1]+1, e=ends[iL];
            if (e-b==0) continue;
            final int c0=T[b];
            final boolean slashSlash=e-b>=2 && c0=='/' && T[b+1]=='/' && nxt(-SPC,T,b+2,e)<0;
            if (!isGenbank  && c0=='L' && strEquls("LOCUS ",T,b)) isGenbank=true;
            if (!isGenbank && !slashSlash && c0!=' ' && ( !is(LETTR,c0) || !is(LETTR,T,b+1) || !is(SPC,T,b+2) ) ) return 0;
            if (!isGenbank && (c0=='I' && T[b+1]=='D'  || c0=='A' && T[b+1]=='C')) {
                if (!is(LETTR_DIGT,T,b+5)) return 0;
                if (c0=='I' || id==null) id=ba.newString(b+5, nxtE(-LETTR_DIGT_US,T,b+5,e));
            }
            if (isGenbank && c0=='A' && strEquls("ACCESSION ",T,b)) {
                final int pos=nxt(-SPC,T,b+10,e);
                if (pos>0) {
                    id=ba.newString(pos, nxtE(-LETTR_DIGT_US,T,pos,e));
                }
            }
            if (isGenbank  ? c0=='O' && strEquls("ORIGIN ",T,b) : c0=='S' && T[b+1]=='Q') hasSeq=true;
            if (slashSlash||iL==ends.length-1) {
                if (id!=null && hasSeq) {
                    final Protein p=pp!=null && pp.length>count ? pp[count] : null;
                    if (p!=null) {
                        new NumberedSequence_Parser().parse(p, 0, new BA(T,from,e) );
                        p.setName(id);
                    }
                    count++;
                }
                from=e+1;
                id=null;
            }
        }
        return count;
    }
}
