package charite.christo.protein;
import java.util.Arrays;

public class FrequenciesAA {

    public final static int  TOTAL='z'-'a'+1, VALID='z'-'a'+2, ENTROPY='z'-'a'+3, ENTROPY_DRAWN='z'-'a'+4;
    private FrequenciesAA(){}
    public static int atColumn(byte ggg[][], short[][] fff, int fromCol, int toCol,  boolean entropy) {
        int debugCount=0;

        for(int col=fromCol<0?0:fromCol; col<toCol && col<fff.length; col++) {
            short ff[]=fff[col];
            if (ff==null) fff[col]=ff=new short['z'-'a'+5];

            if (ff[VALID]==1)  continue;

            debugCount++;
            ff[VALID]=1;
            for(int iP=ggg.length; --iP>=0;) {
                final byte gg[]=ggg[iP];
                if (gg==null || col>=gg.length) continue;
                final int b=gg[col]|32;
                if (b>='a' && b<='z') ff[b-'a']++;
            }
            int n=0;
            for(int i='z'-'a'+1; --i>=0;) n+=ff[i];
            ff[TOTAL]=(short)n;
            if (entropy) {
                double e=0;
                for(int c='z'-'a'+1; --c>=0; ) {
                    final int f=ff[c];
                    if (f==0) continue;
                    final double p=f/(double)n;
                    e-=p*Math.log(p);
                }
                final double pExtrem=1f/(n>20?20:n), eExtrem=-n*pExtrem*Math.log(pExtrem), e10000=10000*e/eExtrem+1;
                // if (e10000<0 || e10000>10000) System.out.println("eeeeeeeeeeeeeeeeeeee "+(int)e10000);
                ff[ENTROPY]=n==0?0:(short)e10000;
            }
        }
        return debugCount;
    }

    public static void reset(short fff[][]) {
        for(int i=fff.length; --i>=0;) {
            final short ff[]=fff[i];
            if (ff!=null) Arrays.fill(ff,(short)0);
        }
    }

}
