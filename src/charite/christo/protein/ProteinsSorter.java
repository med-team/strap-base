package charite.christo.protein;

/**
It can reorder proteins so that similar sequences are in adjacent lines.
@author Christoph Gille
*/
public interface ProteinsSorter {
    Protein[] sortProteins(Protein[] proteins, boolean[] running);

}
