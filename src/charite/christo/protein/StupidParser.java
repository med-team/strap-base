package charite.christo.protein;
import charite.christo.*;


/**HELP
   This parser interprets all letters as amino acids.
   @author Christoph Gille
*/
public class StupidParser implements ProteinParser {
    public boolean parse(Protein p, long options, BA byteArray) {
        final byte[] txt=byteArray.bytes();
        final int l=byteArray.end();
        byte aa[]=null;
        while(true) {
            int count=0;
            for(int t=byteArray.begin(); t<l; t++) {
                final int c=txt[t];
                if ( 'a'<=c && c<='z' || 'A'<=c && c<='Z') {
                    if (aa!=null) aa[count]=(byte)c;
                    count++;
                }
            }
            if (aa==null) aa=new byte[count]; else break;
        }
        p.setResidueType(aa);
        return aa.length>0;
    }
}
