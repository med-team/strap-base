package charite.christo.protein;

/**
   Returns values for pairs of proteins.
*/
public interface CompareTwoProteins {

    void setProteins(Protein p1, Protein p2,int fromColumn, int toColumn);
    void compute();
    double getValue();
    boolean isDistanceScore();
}
