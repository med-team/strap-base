package charite.christo.protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.util.*;
public class ProteinUtils implements Comparator {

    private ProteinUtils(){};
    private final static ProteinUtils  _COMP[]=new ProteinUtils[4];
    /* ---------------------------------------- */
    /* >>> Comparator >>> */

    public final static int COMP_P_ALPHA=1, COMP_P_XYZ=2, COMP_SEL_VIS=3;

    public static Comparator comparator(int type) {
        if (_COMP[type]==null) _COMP[type]=new ProteinUtils();
        return _COMP[type];

    }
    public int compare(Object o0, Object o1) {
        final int t=idxOf(this, _COMP);
        if (t==COMP_P_XYZ || t==COMP_P_ALPHA) {
            final Protein p0=deref(o0,Protein.class), p1=deref(o1,Protein.class);
            if (p0==null && p1==null) return 0;
            if (p0==null) return -1;
            if (t==COMP_P_XYZ) {
                final long cach0=p0.hashCodeCalpha();
                final long cach1=p1.hashCodeCalpha();
                if (cach0!=cach1) return cach0<cach1?-1:1;
            }
            if (t==COMP_P_ALPHA || t==COMP_P_XYZ) {
                return compareAlphabetically(p0.getResidueTypeExactLength(),p1.getResidueTypeExactLength(),true);
            }
        }
        if (t==COMP_SEL_VIS) {
            if (o0 instanceof VisibleIn123 && o1 instanceof VisibleIn123) {
                final int
                    style0= ((VisibleIn123)o0).getStyle(),
                    style1= ((VisibleIn123)o1).getStyle();
                return style0<style1 ? -1 : style0>style1 ? 1 : 0;
            }
        }
        return 0;
    }
    /* <<<  <<< */

    private static Map<String,String> _mapRef;
    public static String mapRef(String s) {
        if (_mapRef==null) {
            _mapRef=new HashMap();
            _mapRef.put("UniProtKB/TrEMBL","UNIPROT");
        }
        return mapId(s,_mapRef);
    }

    public static String tabText2(Object p1, Object p2) {
        return
            delPfx("pdb", delLstCmpnt(toStrg(p1),'.' ))+"./."+
            delPfx("pdb", delLstCmpnt(toStrg(p2),'.' ))+"   ";
    }

    public final static String AMINO_ACID_NAMES[]={
        "Alanine",null,"Cysteine","Aspartate","Glutamate","Phenylalanine","Glycine","Histidine","Isoleucine",null,"Lysine","Leucine","Methionine","Asparagine",null,
        "Proline","Glutamine","Arginine","Serine","Threonine",null,"Valine","Tryptophan",null,"Tyrosine",null};

    public static char textHasAminoAcid(long opt, String n) {
        final int lst=n==null?0:(lstChar(n)|32);
        if (lst!='n' && lst!='e') return 0;
        for(int a=0; a<'Z'-'A'; a++) {
            if (0!=(opt&STRSTR_EOL) ? endWith(opt,AMINO_ACID_NAMES[a],n) : strstr(opt, AMINO_ACID_NAMES[a], n)>=0) {
                return (char)(a+'A');
            }
        }
        return 0;
    }

}

