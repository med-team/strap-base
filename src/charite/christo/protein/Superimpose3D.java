package charite.christo.protein;
import charite.christo.BA;
import java.util.Arrays;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Implemented by plugins that  perform a 3D-superposition of the C-alpha atoms of two protein chains.
   Before the method compute() is invoked, the reference and mobile proteins must be set with setProteins();
   @author Christoph Gille
*/
public interface Superimpose3D extends NeedsProteins {
    long PRINT_HUMAN_READABLE=1;
    long PARSE_ALIGNMENT=1, PARSE_MATRIX=1<<1, PARSE_SCORE=1<<2, PARSE_MSA_START_S=1<<3;
    int COLUMN_SEQUENCE=10;

    void setOptions(long optionFlags);
    long getOptions();
    /**
       @return the reference and the mobile protein
    */
    Protein[] getProteins();
    /**
       @return The plugin should return  the  mobile protein.
    */
    void compute();
    /**
       returns a 3X3 rotation matrix and a translation vector.
       returns a  {dx,dy,dz} translation vector
       This method is invoked by STRAP after the compute() method returned.
    */
    Result getResult();
    /**
        The Result class bundles the result returned after computation
    */
    byte[] NO_BYTE={};
    public static class Result {
        private final int _numSeq;
        public Result(int numberOfSequences) { _numSeq=numberOfSequences; }

    /* ---------------------------------------- */
    /* >>> Matrix3D >>> */
        private Matrix3D _m3d;
        public void setMatrix(Matrix3D m) { _m3d=m;}
        public Matrix3D getMatrix() { return _m3d; }
        /* <<< Matrix3D <<< */
        /* ---------------------------------------- */
        /* >>> Gapped Sequences >>> */
        private byte[][] _gapped;
        public void setGappedSequences(byte[] ss[]) {
            final int N=sze(ss);
            if (!(N==0 || N>=_numSeq)) assrt();
            _gapped=ss;
        }
        public byte[][] gappedSequences() { return _gapped; }
        public int[] getIndicesOfSequences() { return _indicesOfProteins;}
        public void setIndicesOfSequences(int ii[]) {
            if (ii!=null) {
                final int N=_numSeq;
                if (ii.length<N) assrt();
                for(int i=N; --i>=0;) {
                    if (idxOf(i,ii,0,N)<0) {
                        putln(RED_ERROR+"Superimpose3D setIndicesOfSequences numSeq="+N+"  ii="+intArrayToTxt(ii));
                        for(byte[] bb : gappedSequences()) putln(bb);
                        _indicesOfProteins=count01234(ii.length);
                        return;
                    }
                }
            }
            _indicesOfProteins=ii;
        }
        private int[] _indicesOfProteins;
        /* <<< Gapped Sequences <<< */
        /* ---------------------------------------- */
        /* >>> Score >>> */
        private float _score=Float.NaN, _rmsd=Float.NaN;
        public float getScore() { return _score;}
        public float getRmsd() { return _rmsd;}
        public void setScore(double s) { _score=(float)s;}
        public void setRmsd(double s) { _rmsd=(float)s;}
        /* <<< Score <<< */
        /* ---------------------------------------- */
        /* >>> Write Out >>> */
        public BA toText(BA sb0, long options) {
            final BA sb=sb0!=null ? sb0 : new BA(999);
            //sb.a(isInverse ? " Inverse:\n" : "");
            final boolean hr=(options&PRINT_HUMAN_READABLE)!=0;
            if (_m3d!=null) _m3d.toText(hr ? Matrix3D.FORMAT_EXPLAIN : Matrix3D.FORMAT_XML, hr?"mobile ":null,sb);
            final byte[] gg[]=gappedSequences();
            if (sze(gg)>0) {
                final int[] ii=getIndicesOfSequences();
                if (!hr) sb.aln("<alignment>");
                for(int i=0; i<gg.length; i++) {
                    final int idx=sze(ii)>=gg.length ? idxOf(i,ii) : i;



                    sb.a('s').a(idx)
                        .a(' ', COLUMN_SEQUENCE-1-stringSizeOfInt(idx))
                        .aln(gg[idx]);
                }
                if (!hr) sb.aln("</alignment>");
            }
            if (!Float.isNaN(getScore())) sb.a(" Score=").a(getScore());
            if (!Float.isNaN(getRmsd())) sb.a(" Rmsd=").a(getRmsd(),0,4);
            return sb;
        }

        public Result changeOrder(Object[] current, Object[] ordered) {
            final int n=_numSeq;
            if (n!=current.length && n!=ordered.length) {
                assrt();
            }
            if (arraysEqul(current,ordered)) return this;
            final Result r=new Result(n);
            r._score=getScore();
            r._rmsd=getRmsd();
            final byte[][] gg0=gappedSequences(), gg=gg0==null?null : new byte[n][];
            final int[] ii0=getIndicesOfSequences(), ii=ii0==null?null : new int[n];
            final boolean done[]=new boolean[n];
            if (ii!=null) Arrays.fill(ii,-1);

            for(int i=0; i<n; i++) {
                for(int j=0; j<n; j++) {
                    if (current[i]==ordered[j] && !done[j]) {
                        done[j]=true;
                        if (ii0!=null) ii[j]=ii0[i];
                        if (gg!=null) gg[j]=gg0[i];
                        break;
                    }
                }
            }
            r.setGappedSequences(gg);
            r.setIndicesOfSequences(ii);
            final Matrix3D m3d=getMatrix();
            if (m3d!=null) r.setMatrix(m3d.newInverse());
            return r;

        }

    }
}
/*
http://cssb.biology.gatech.edu/skolnick/files/FrTMalign/
*/
