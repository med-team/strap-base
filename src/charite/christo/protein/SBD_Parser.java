package charite.christo.protein;
import static charite.christo.ChUtils.*;
import charite.christo.*;

/**HELP
.sbd Files created with SeqBuilder
   @author Christoph Gille
*/
public class SBD_Parser implements ProteinParser {
    public boolean parse(Protein p, long options, BA ba) {
        final byte[] T=ba.bytes();
        final int E=ba.end();
        final int B=ba.begin();
        final int i=E-B<100 ? -1 : strstr(STRSTR_AFTER, "Created by SeqBuilder version ",T,B,mini(E,B+80));
        if (i<0) return false;
        final int seqStart=nxt(LETTR, T,i,E);
        if (seqStart<0) return false;
        final int seqEnd=nxtE(-LETTR,T,seqStart,E);
        //putln("seq="+ba.newString(seqStart,seqEnd)+"<<<");
        p.setResidueType(substrgB(T, seqStart ,seqEnd));

        return true;
    }
}
