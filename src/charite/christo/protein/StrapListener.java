package charite.christo.protein;

/**
All plugins  that need to react on changes of the alignment should implement this interface.

The option-menu  contains some utilities that allow to

<ul>
<li>view the list of registered <i>JAVADOC:StrapListener</i> </li>
<li>to see all dispatched <i>JAVADOC:StrapEvent</i>s </li>
</ul>

<i>JAVADOC:StrapListener</i>s are registered using
<i>JAVADOC:StrapAlign#addListener(StrapListener)</i> and unregistered
using <i>JAVADOC:StrapAlign#removeListener(Object)</i>.

<b>Thread-issues:</b> The method will always be invoked in the event dispatching thread even if the
event was created and dispatched from another thread.

*/
public interface StrapListener {
    void handleEvent(StrapEvent e);
}
