package charite.christo.protein;
import charite.christo.*;
import java.util.*;
/**
   A protein viewer can display proteins three-dimensionally.
   @author Christoph Gille
*/
public interface ProteinViewer extends Disposable, HasProtein, CommandInterpreter {
    String PROTEIN="PROTEIN", RESIDUES="RESIDUES", SELECTION_CURSOR=ResidueSelection.NAME_CURSOR, SELECTION_PICKED="picked";

    long PROPERTY_MULTI_MOLECULE=1L<<0, PROPERTY_MULTI_VIEW=1L<<2, PROPERTY_EXTERNAL_PROCESS=1L<<3, PROPERTY_TK=1L<<4;
    long PROPERTY_HEAVY_WEIGHT=1L<<5, PROPERTY_OPENGL=1L<<6;
    long PROPERTY_SEQUENCE_CURSOR_DELAYED=1L<<8,  PROPERTY_HAS_SCRIPT_PANEL=1L<<24;
    long PROPERTY_NAMED_SELECTIONS=1L<<30, PROPERTY_RIBBON_COLOR_NO_CHANGE=1L<<31;
    long PROPERTY_HAS_CONTEXTMENU=1L<<32, PROPERTY_NEEDS_UPDATE_SURFACES_WHEN_ROTATED=1L<<33;
    long INTERPRET_NO_MSG_DIALOGS=1L<<1;
    long SET_PROTEIN_VERBOSE=1L<<2;

    ProteinViewer[] NONE={};

    String
        PFX_SURFACE_OBJECT="surface_",
        COMMANDhighlight_selected_atoms="3D_highlight_selected_atoms",
        COMMANDhighlight_selected_amino_acids="3D_highlight_selected_amino_acids",
        COMMANDcenter_amino="3D_center_amino",
        COMMANDbiomolecule="3D_biomolecule",
        COMMANDzoom="3D_zoom", COMMANDrotate="3D_rotate", COMMANDcenter="3D_center", COMMANDset_rotation_translation="3D_set_rotation_translation",
        COMMANDbackground="3D_background",
        COMMANDribbons="3D_ribbons", COMMANDspheres="3D_spheres", COMMANDsticks="3D_sticks", COMMANDlines="3D_lines",
        COMMANDsurface="3D_surface", COMMANDsa_surface="3D_sa_surface",  COMMANDsurface_color="3D_surface_color",
        COMMANDmesh="3D_mesh", COMMANDdots="3D_dots", COMMANDcartoon="3D_cartoon",
        COMMANDcolor="3D_color",
        COMMANDchange_object_color="3D_change_object_color",
        COMMANDselect="3D_select",
        COMMANDselection_name="3D_selection_name",
        COMMANDlabel="3D_label",
        COMMANDlabel_color="3D_label_color",
        COMMANDshowScriptPanel="3D_script_panel",

        COMMANDobject_delete="3D_object_delete",

        COMMAND_HIDE_EVERYTHING="3d_hide_everything",
        COMMANDreset="3d_reset",
        STYLE_COMMANDS[]={COMMANDspheres,COMMANDsticks,COMMANDlines,COMMANDribbons,
                          COMMANDsurface,  COMMANDsa_surface,
                          COMMANDdots, COMMANDmesh, COMMANDcartoon},

        INTERNAL_USE[]={COMMANDchange_object_color, COMMANDset_rotation_translation};

    WeakHashMap<Object,Object> mapViewer=new WeakHashMap();
    HashSet<Class> SHOW_CURSOR_IF_OTHER_PROTEIN=new HashSet();

    /**
       The only use of the setProtein and getProtein methods are to
       to keep a reference to the protein object.
       Otherwise both methods are not important.
    */
    boolean setProtein(long options, Protein p, ProteinViewer inSameView);

    /**
        Each instance is connected with only one protein.
        But several instances can share the view such that several proteins are displayed in the same
        panel.
    */
    Collection<? extends ProteinViewer> getViewersSharingViewV(boolean proxyObject);

    final static String
    GET_FRAME_TITLE="PV$$gFT",
        GET_ATOM_SELECTION_EXAMPLE="PV$$gASE",
        GET_JMENUBAR="PV$$gJMB",
        GET_CANVAS="PV$$gC", GET_SURFACEOBJECTS="PV$$gSO", GET_LAST_CREATED_OBJECT="PV$$gLCO",
        GET_FLAGS="PV$$gF", GET_SUPPORTED_COMMANDS="PV$$gSC",
        GET_AWTMASK_CONT_SELECTION="PV$$gMCS",
        GET_AWTMASK_DISC_SELECTION="PV$$gMDS",
        SET_RUN_AFTER_INSTALLATION="PV$$sRAI";

    /**
        The method may return null.
        GET_JMENUBAR: javax.swing.JMenuBar
        GET_CANVAS: java.awt.Component
        GET_SURFACEOBJECTS: List<String>
        GET_FLAGS:  long-bit-mask with properties
        A generic command like COMMANDcolor: Boolean.TRUE if command is supported
    */

    Object getProperty(String id);
    void setProperty(String id, Object value);

}

