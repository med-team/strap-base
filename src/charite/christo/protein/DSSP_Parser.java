package charite.christo.protein;
import charite.christo.*;
import static charite.christo.ChUtils.*;

/**HELP
This class reads dssp files. See File_example:dssp

   @author Christoph Gille
*/
public class DSSP_Parser implements ProteinParser {
    private final static byte[]
        HEADER_LINE="     SECONDARY STRUCTURE DEFINITION".getBytes(),
        FORMAT_LINE="  #  RESIDUE".getBytes();
    public boolean parse(Protein prot,long options, BA byteArray) {
        final int ends[]=byteArray.eol();
        final byte[] text=byteArray.bytes();
        final int[] DD[]=atoiLookup(), P0=DD[0], P1=DD[1], P2=DD[2], P3=DD[3];
        final float[] FF[]=atofLookup(), P_1=FF[1];
        if (text.length<HEADER_LINE.length) return false;
        for(int i=HEADER_LINE.length;--i>=0;) {
            final int c=HEADER_LINE[i]|32;
            if (c>='a' && c!=(text[i]|32)) return false;
        }
        int iZ=0;
        for(int i=1;i<ends.length; i++) {
            final int b=ends[i-1]+1;
            if (b>=text.length-FORMAT_LINE.length) return false;
            iZ=i;
            for(int j=FORMAT_LINE.length;--j>=0;) if(text[b+j]!=FORMAT_LINE[j]) iZ=0;
            if (iZ>0) break;
        }
        if (iZ==0||iZ>ends.length-2) {
            putln("Error in DSSP_Parser while parsing a DSSP-file.\nCould not find line ",FORMAT_LINE);
            return false;
        }
        int // positions of data columns in each line
            pos_ss =-1,pos_x=-1,
            pos_as =-1,pos_y=-1,
            pos_acc=-1,pos_z=-1;
        int
            ias=0,ias3=0;
        // determination of tab positions
        final String s=new String(text,ends[iZ-1]+1,  ends[iZ]-ends[iZ-1]+1);
        pos_ss=s.indexOf("STRUC");
        pos_x =s.indexOf("X-CA");
        pos_y=s.indexOf("Y-CA");
        pos_z =s.indexOf("Z-CA");
        pos_as=s.indexOf("AA");
        pos_acc=s.indexOf("ACC");
        if (pos_ss==-1 ||  pos_x ==-1 || pos_y==-1 || pos_z==-1 || pos_as==-1 || pos_acc==-1) {
            putln("Error  reading dssp file :  There should be a line with a # and  strings STRUC  X-CA  Y-CA Z-CA  AA  and ACC. I am skipping this file");
            return false;
        }
        int countAA=0;
        for(int i=iZ+1; i<ends.length && ends[i]-ends[i-1] >125; i++) {
            final int resType=text[ends[i-1]+14];
            if (resType==' ') break;
            countAA++;
        }
        // initialize arrays
        final byte[]
            residueType=new byte[countAA],
            residueSecStrType=new byte[countAA],
            residueChain=new byte[countAA];
        final int[] residueNumber=new int[countAA];
        final float[]
            cAlphaCoord=new float[countAA*3],
            residueAccess=new float[countAA];
        // parse the lines
        final int CC=200;
        final byte cc[]=new byte[CC];
        for(++iZ,ias=ias3=0;ias<countAA;iZ++, ias++,ias3+=3) {
            int nCc=ends[iZ]-ends[iZ-1]-1;
            if (nCc>CC) nCc=CC;
            System.arraycopy(text,ends[iZ-1]+1,cc,0,nCc);
            int p,ip;
            // parse residue information
            residueNumber[ias]=P0[cc[9]]+P1[cc[8]]+P2[cc[7]]+P3[cc[6]];
            residueChain[ias]=(byte)cc[11];
            if ((residueType[ias]=(byte)cc[pos_as])=='!') continue;
            // parse coordinates
            for(ip=0,p=pos_x;ip<3;p+=pos_y-pos_x,ip++) {
                float xyz=P_1[cc[p+3]]+P0[cc[p+1]]+P1[cc[p]]+P2[cc[p-1]];
                if (cc[p-2]=='-'||cc[p-1]=='-'||cc[p]=='-'||cc[p+1]=='-') xyz=-xyz;
                cAlphaCoord[ias3+ip]=xyz;
            }
            // parse secondary structure
            byte ss=(byte)cc[pos_ss];
            if (ss!='H' && ss!='E') ss=(byte)'C';
            residueSecStrType[ias]=ss;
            p=pos_acc;
            // access
            residueAccess[ias]=P0[cc[p+2]]+P1[cc[p+1]]+P2[cc[p]];
        }// for iZ
        // create data integrity
        prot.setResidueSolventAccessibility(residueAccess);
        prot.setResidueType(residueType);
        prot.setResidueChain(residueChain);
        prot.setResidueNumber(residueNumber);
        prot.setResidueCalphaOriginal(cAlphaCoord);
        prot.setResidueSecStrType(residueSecStrType);
        prot.setIsLoadedFromStructureFile(true);
        return true;
    }
}
