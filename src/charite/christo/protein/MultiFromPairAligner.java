package charite.christo.protein;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
This class converts SequenceAligner  for two proteins into one for many proteins.
The  utility  is used to produce multiple sequence alignments with classes that
can align only two proteins.
@author Christoph Gille
MultiFromPairAligner
*/
public class MultiFromPairAligner implements CanBeStopped,NeedsProteins,SequenceAlignerSorting, HasControlPanel {
    private Object _aligner, _ctrl;
    private final Object _classes[]={null,null};
    /* >>> Constructor and Settings >>> */
    public MultiFromPairAligner(Object c) {
        if (!isClassOrPC(c)) assrt();
        _classes[0]=c;
    }
    public MultiFromPairAligner set3dMethod(Class c) { _classes[1]=c; return this;}
    public Object[] getClasses() { return _classes; }
    //public Class getClazzes() { return clas(clazz); }
    /* <<< Constructor and Settings <<< */
    /* ---------------------------------------- */
    /* >>> Interface Methods >>> */
    private int[] _sort;
    private Protein _pp[];
    private byte _seqs[][], _aligned[][];
    public byte[][] getSequences() { return _seqs;}
    public byte[][] getAlignedSequences() { return _aligned; }
    public void setSequences(byte[]...ss) { _seqs=ss;   }

    public void setProteins(Protein... pp) {
        _pp=pp;
        final BA log=log();
        if (log!=null)  {
            log.a("\n"+ANSI_YELLOW+"MultiFromPairAligner setProteins"+ANSI_RESET+"\n");
            for(int i=0; i<sze(pp);i++) log.a(i).a(' ').aln(pp[i]);
            if (!withGui()) log.setSendTo(BA.SEND_TO_NULL);
        }
    }
    public void setOptions(long flags) {}
    public long getPropertyFlags() { return 0L;}
    public long getOptions() { return 0L;}
    public int[] getIndicesOfSequences() { return _sort;}
    /* <<<  Interface Methods <<< */
    /* ---------------------------------------- */
    /* >>> ControlPanel >>> */
    private BA _log;
    public BA log() {
        if (_log==null && withGui()) {
            _log=new BA(999)
                .a("Log of MultiFromPairAligner(")
                .a(shrtClasNam(_classes[0])).a(")\n3d-Superposition: ")
                .aln(shrtClasNam(_classes[1]));
        }
        return _log;
    }
    @Override public Object getControlPanel(boolean real) {
        if (_ctrl==null) {
            if (!real) return CP_EXISTS;
            _ctrl=scrllpn(log());
        }
        return _ctrl;
    }
    /* <<< ControlPanel <<< */
    /* ---------------------------------------- */
    /* >>> Compute >>> */
    private final static int FLAG_setProteins=1, FLAG_3D=1<<1;
    private int[] _flags;
    private int flags(int i01) {
        if (_flags==null) {
            _flags=new int[2];
            for(int i=2;--i>=0;) {
                _flags[i]=
                    ( isAssignblFrm(NeedsProteins.class, _classes[i]) ? FLAG_setProteins : 0)|
                    ( isAssignblFrm(Superimpose3D.class, _classes[i]) ? FLAG_3D : 0);
            }
        }
        return _flags[i01];
    }

    private AProtein[] _aPP;
    private AProtein[] aProts() {
        if (_aPP==null) {
            final Protein pp[]=_pp;
            _aPP=new AProtein[pp.length];
            for(int i=pp.length;--i>=0;)
                _aPP[i]=new AProtein(_seqs[i],pp!=null && pp.length>i ? pp[i]: null,i);
        }
        return _aPP;
    }

    private AlignmentResult _results[][];
    private AlignmentResult[][] getAlignments() {
        final BA log=log();
        final String MSG_STOP="MultiFromPairAligner stopped";
        long timePG=0;
        final BA sbPG=new BA(99);
        if (_results==null) {
            _results=new AlignmentResult[_seqs.length][_seqs.length];
            final AProtein[] pp=aProts();
            final int N=pp.length*pp.length;

            final String constProgress= "/"+(pp.length*pp.length)+" "+niceShrtClassNam(_classes[0])+" "+(niceShrtClassNam(_classes[1]==null?"":_classes[1]))+" ";
            for(int iR=pp.length, count=0; --iR>=0;) {
                for(int iM=pp.length; --iM>=0; count++) {
                    final Protein pR=pp[iR].protein, pM=pp[iM].protein;
                    if (iR==iM) continue;
                    if (_results[iR][iM]==null) {
                        final AlignmentResult result=_results[iR][iM]=new AlignmentResult(pp[iR], pp[iM]);
                        SequenceAligner aligner=null;
                        byte[][] rrLocal=null;
                        double score=Double.NaN;
                        boolean is3D=false;
                        for(int try3d=2; --try3d>=0 && rrLocal==null;) {
                            is3D=try3d>0;
                            final Object claz=_classes[try3d];
                            if (claz==null || is3D && !(pM!=null && pR!=null && pM.getResidueCalpha()!=null && pR.getResidueCalpha()!=null)) continue;
                            aligner=mkInstance(claz, SequenceAligner.class,true);
                            if (aligner==null) continue;
                            if (pM!=null && (flags(try3d)&FLAG_setProteins)!=0) ((NeedsProteins)aligner).setProteins(pR,pM);
                            final boolean isSuper=isAssignblFrm(Superimpose3D.class, aligner) &&  pM!=null && (flags(try3d)&FLAG_3D)!=0;
                            if (isSuper) {
                                ((Superimpose3D)aligner).setProteins(pR, pM);
                            } else aligner.setSequences(pp[iR]._ungapped, pp[iM]._ungapped);
                            if (_stopped || get(0,_isInterrupted)) { putln(MSG_STOP); return null;}
                            _aligner=wref(aligner);

                            aligner.compute();
                            if (System.currentTimeMillis()-timePG>200) {
                                sbPG.clr().a(KEY_PROGRESS).a(count).a(constProgress).and(pR," ./. ",pM);
                                timePG=System.currentTimeMillis();
                                handleActEvt(this, sbPG.toString(),0);
                            }
                            if (_stopped) { putln(MSG_STOP); return null;}
                            rrLocal=aligner.getAlignedSequences();
                            final String iRiM=ANSI_RESET+" ("+iR+","+iM+") ";
                            if (get(0,rrLocal)==null || get(1,rrLocal)==null) {
                                if (log!=null) log.a(RED_ERROR).a(iRiM).a("getAlignedSequences() returns null ").aln(claz).send();
                                return null;
                            } else {
                                if (isSuper) {
                                    if (isInstncOf(HasScore.class, aligner)) score=((HasScore)aligner).getScore();
                                    else score=Blosum.pairAignScore(-20f,-4f,rrLocal[0],rrLocal[1], 0, MAX_INT,  -Blosum.BLOSUM62_MIN);
                                } else score=Blosum.pairAignScore(-20f,-5f,rrLocal[0],rrLocal[1], 0, MAX_INT, 0);

                                //putln(isSuper+" score="+score);
                                if (log!=null) log.a(GREEN_SUCCESS).a(iRiM).a("score=").a(score).a(' ').aln(try3d>0?" 3D":"").send();
                            }
                        }
                        if (get(0,rrLocal)==null || get(1,rrLocal)==null) {
                            return null;
                        }
                        final byte oldAli[][]={pp[iR]._ungapped, pp[iM]._ungapped};
                        if (myComputer() && sze(rrLocal)!=2) putln(RED_ERROR+"sze(rrLocal)!=2");
                        result.aligned=rrLocal;
                        result.score=score;
                        result.superimposed=is3D;
                        dispos(aligner);
                        if (log!=null) log.a("result(").a(iR).a(',').a(iM).a(")=").a(result.score).a('\n').send();

                        count++;
                        if (rrLocal!=null) _results[iM][iR]=result.invert();
                    }
                }
            }
            handleActEvt(this, KEY_PROGRESS+N+" Alignments done" ,0);
        }
        return _results;
    }
    public void compute(){
        final BA log=log();
        for(int i=2;--i>=0;) {
            if (_classes[i]!=null && mkInstance(_classes[i],SequenceAligner.class,true)==null) {
                error("Could not instanciate <pre>"+_classes[i]+"</pre>");
                debugExit("classes[i]="+_classes[i]);
                return;
            }
        }
        final AlignmentResult results[][]=getAlignments();
        if (results==null) {
            putln(RED_ERROR+" MultiFromPairAligner results=null");
            return;
        }

        final int nP=_seqs.length;
        if (nP<2) return;
        final ProteinStack stack=new ProteinStack();
        final AProtein aProtein[]=aProts();
        int topIdx=0;
        for(int try3d=2; --try3d>=0;) {
            double maxScoreSum=Float.MIN_VALUE;
            for(int i=nP; --i>=0;) {
                if (try3d>0 && aProtein[i].protein.getResidueCalpha()==null) continue;
                float scoreSum=0;
                for(int j=nP;--j>=0;) {
                    if (i==j) continue;
                    final AlignmentResult result=results[i][j];
                    scoreSum+=result.score*result.score;
                }
                if (maxScoreSum<scoreSum) {
                    maxScoreSum=scoreSum;
                    topIdx=i;
                }
            }
            if (topIdx>=0) break;
        }
        log.aln("\n A(i,j) means sequence alignment of protein i,j and S(i,j) means 3D superposition\n").send();
        stack.top=aProtein[topIdx];
        adUniq(stack.top,    stack.all);
        final List<AProtein> notAlignedV=new ArrayList();
        for(int i=nP;--i>=0;) {
            if (i!=stack.top._number && aProtein[i]!=null) notAlignedV.add(aProtein[i]);
        }
        while(notAlignedV.size()>0 && _running)  compute(stack,notAlignedV);
        _aligned=new byte[nP][];
        _sort=new int[nP];
        for(int i=0;i<nP && _running;i++) {
            final AProtein p=stack.all.get(i);
            _aligned[p._number]=p._gapped;
            _sort[i]=p._number;
        }
        _results=null;
        _pp=null;
    }
    private void compute(ProteinStack stack, List<AProtein> notAlignedV) {
        final AlignmentResult bestAli=bestAlignmentResult(stack.all, notAlignedV);
        final AProtein protStack=bestAli.proteinR, protNotAl=bestAli.proteinM;
        final byte aligned[][]=bestAli.aligned;
        if (protStack==null||aligned==null||protNotAl==null||aligned==null) {
            if (myComputer()) log().a(RED_ERROR+" in protStack=").a(protStack!=null).a(" aligned=").a(aligned).a(" protNotAl=")
                                  .a(protNotAl).a(" aligned=").a(aligned!=null ? aligned.length : -1).a('\n').send();
            _running=false;
            return;
        }
        log().a(bestAli.superimposed? " S(" : " A(").a(protStack._number).a(',').a(protNotAl._number).a(") ").send();
        final byte[][]  ist={protStack._gapped , protNotAl._gapped};
        final byte[][] soll={aligned[0],aligned[1] };
        final int[][] inserts=new MergeAlignments((int[])null,soll,ist).getInsertsAtCol();
        protNotAl._gapped=insertGapsAtColumns(inserts[1],protNotAl._gapped);
        for(int i=stack.all.size(); --i>=0; ) {
            final AProtein p=stack.all.get(i);
            p._gapped=insertGapsAtColumns(inserts[0],p._gapped);
        }
        notAlignedV.remove(bestAli.proteinM);
        adUniq(bestAli.proteinM, stack.all);
    }
    /* <<< Compute <<< */
    /* ---------------------------------------- */
    /* >>> Best Comparison  >>> */
    private AlignmentResult bestAlignmentResult(List<AProtein> pp1,List<AProtein> pp2) {
        AlignmentResult best=null;
        final AlignmentResult aa[][]=getAlignments();
        for(int i1=pp1.size(); --i1>=0;) {
            for(int i2=pp2.size(); --i2>=0;) {
                final AlignmentResult result=aa[pp1.get(i1)._number][pp2.get(i2)._number];
                if (best==null || result!=null && result.isBetterThan(best)) best=result;
            }
        }
        return best;
    }
    /* <<< Best Comparison <<< */
    /* ---------------------------------------- */
    /* >>>  Classes  >>> */
    private static class ProteinStack { AProtein top;  List<AProtein> all=new ArrayList();}
    private static class AlignmentResult {
        final AProtein proteinM, proteinR;
        byte[][] aligned;
        double score;
        boolean superimposed;
        public AlignmentResult(AProtein pR, AProtein pM) { proteinR=pR; proteinM=pM; }
        public AlignmentResult invert() {
            final AlignmentResult r=new AlignmentResult(proteinM, proteinR);
            r.score=score;
            r.superimposed=superimposed;
            r.aligned=new byte[][]{aligned[1], aligned[0]};
            return r;
        }
        private boolean isBetterThan(AlignmentResult r2) {
            if (r2==null) return true;
            if (superimposed && !r2.superimposed) return true;
            if (!superimposed && r2.superimposed) return false;
            return score>r2.score;
        }
    }
    private static class AProtein  {
        final byte[] _ungapped;
        final int _number;
        final Protein protein;// may be null
        byte[] _gapped;
        public AProtein(byte[] seq,Protein p,int number) {
            _gapped=seq;
            protein=p;
            _number=number;
            _ungapped=seq;
        }
        @Override public String toString() {return "AProtein#"+_number+" ";}
    }
    /* <<< Classes  <<< */
    /* ---------------------------------------- */
    /* >>> User Interface >>> */
    //    public final int getProgressMaximum() {int n=getSequences().length; return (n*n-n)/2;}
    //    public final int getProgress() {return count;}

    /* <<<  User Interface <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    private boolean _stopped, _running=true;
    public void stop() {
        _stopped=true;
        final CanBeStopped cbs=deref(_aligner, CanBeStopped.class);
        if (cbs!=null) cbs.stop();
    }
    {vALIGNMENTS.add(wref(this));}
    private boolean _isInterrupted[];
    public void setIsInterrupted(boolean[] boolArraySize1) {
        this._isInterrupted=boolArraySize1;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */
    public static byte[] insertGapsAtColumns(int insert[], byte orig[]) {
        int size=orig.length;
        for(int i=orig.length<insert.length ? orig.length:insert.length; --i>=0;) size+=insert[i];
        final byte txt[]=new byte[size];
        int iT=0;
        for(int i=0;i<orig.length; i++) {
            if (i<insert.length) {
                for(int j=insert[i]; --j>=0;) txt[iT++]=32;
            }
            txt[iT++]=orig[i];
        }
        return txt;
    }

}
