package charite.christo.protein;
import java.util.*;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class AlignUtils  {
    /**HELP
       
       <div id="Dokument" >
       <div class="NOPRINT" >
       Preparing document for printing:
       <form action="">
       <input type="button" onClick="showHideFigures();" value="Hide figures" >
       <input type="button" onClick="showHideFigures(true);" value="Show figures" >
       </form> When printing, backgrounds are automatically turned to white.<br>
       </div>
       <i>PACKAGE:charite.christo.strap.</i>
       <i>PACKAGE:charite.christo.protein.</i>
       
       <i>INCLUDE_DOC2:StrapAlign</i>
       <hr><hr>
       
       <i>INCLUDE_DOC2:StrapTree</i>
       <a name="MENU"></a><i>INCLUDE_FILE:~/@/doc/JMenuBar.html</i>
       <a name="MENU"></a><i>INCLUDE_FILE:~/@/doc/JPopupMenu.html</i>
       <hr><hr>
       <i>INCLUDE_DOC2:Strap</i>
       <i>INCLUDE_DOC2:StrapView</i>
       <i>INCLUDE_DOC2:StrapKeyStroke</i>
       <i>INCLUDE_DOC2:Gaps2Columns</i>
       
       <hr><hr>
       
       <a name="DRAG"></a><h2>Drag-and-drop</h2><i>INCLUDE_DOC2:ChTransferable</i>
       <i>INCLUDE_DOC2:DialogGenbank</i>
       <hr><hr>
       
       <i>INCLUDE_DOC3:V3dUtils</i>
       <i>INCLUDE_DOC3:Protein3d</i>
       <hr><hr>
       
       <i>INCLUDE_DOC2:ResidueSelection</i>
       <i>INCLUDE_DOC2:ResidueAnnotation</i>
       <i>INCLUDE_DOC2:ResidueAnnotationView</i>
       <hr><hr>
       
       <i>INCLUDE_DOC2:DialogExportProteins</i>
       <i>INCLUDE_DOC2:Texshade</i>
       <i>INCLUDE_DOC2:DialogPublishAlignment</i>
       <hr><hr>
       <i>INCLUDE_DOC2:ChStdout</i>
       <i>INCLUDE_DOC2:Microsoft</i>
       
       </div>
       <div id="Navigation"  style="font-size: small;">
       
       <div class="menu_headings">Introduction</div>
       <a 	href="#StrapAlign">Introduction</a><br>
       
       <div class="menu_headings">User interface</div>
       <a 	href="#StrapTree">Context menus</a><br>
       <a 	href="#MENU">Menu items</a><br>
       
       
       <div class="menu_headings">Alignment Panel</div>
       <a 	href="#Strap">Alignment projects</a><br>
       <a 	href="#StrapView">Alignment Panel</a><br>
       <a 	href="#StrapKeyStroke">Key bindings</a><br>
       <a 	href="#Gaps2Columns">Gaps</a><br>
       
       <div class="menu_headings">Import</div>
       <a 	href="#DRAG">Drag'n Drop</a><br>
       <a 	href="#DialogGenbank">Nucleotide files</a><br>
       
       <div class="menu_headings">Export</div>
       <a 	href="#DialogExportProteins">Export proteins</a><br>
       <a 	href="http://www.bioinformatics.org/strap/exampleOutput.html">Example alignment</a><br>
       <a 	href="#Texshade">PDF</a><br>
       
       <div class="menu_headings">3D-Visualization</div>
       <a 	href="#V3dUtils">3D-views</a><br>
       <a 	href="#Protein3d">Backbone</a><br>
       
       <div class="menu_headings">Residue Selections</div>
       <a 	href="#ResidueSelection">Residue selections</a><br>
       <a 	href="#ResidueAnnotation">Residue annotations</a><br>
       
       <div class="menu_headings">Running Strap</div>
       <a 	href="#Microsoft">Operation systems</a><br>
       <br>
       </div>
       
       @author Christoph Gille
       
    */
    
    /*
       The gapped sequences may be 0-terminated!
    */
    private AlignUtils(){}
    public static void appendName(String prefix, int i, BA sb) {
        sb.a(prefix).a('0',8-stringSizeOfInt(i)).a(i).a(' ');
    }

    public static BA toGappedFasta(byte gapped[][], String headerPrefix, int offset,  BA sb) {
        return _toGapped(true, gapped, headerPrefix, offset, sb);
    }
    public static BA toGappedFasta(byte gapped[][]) { return _toGapped(true, gapped, "s", 0, null); }

    public static BA toGappedMSF(byte gapped[][], String headerPrefix, int offset,  BA sb) {
        return _toGapped(false, gapped, headerPrefix, offset, sb);
    }

    private static BA _toGapped(boolean fastaFormat, byte gapped[][], String headerPrefix, int offset,  BA sb0) {
        final BA sb=sb0!=null ? sb0 : new BA(NO_BYTE);
        int size=sze(gapped)*(1+12);
        if (size==0) return sb;
        for(byte[] bb:gapped) size+=bb.length;
        sb.ensureCapacity(sze(sb)+size);
        for(int i=0; i<gapped.length; i++) {
            if (fastaFormat) sb.a('>').a(headerPrefix).a(i+offset).a('\n');
            else appendName(headerPrefix,i+offset,sb);
            for(byte c: gapped[i]) {
                if (c==0) break;
                sb.a('a'<=c && c<='z' || 'A'<=c && c<='Z' ? (char)c : '-');
            }
            sb.a('\n');
        }
        return sb;
    }

    public static void msfHeader(char type, byte gapped[][], BA sb) {
        sb.a("!!AA_MULTIPLE_ALIGNMENT 1.0\n\n myAlignment.msf  MSF: 224  Type: ")
            .a(type).a("  September  9, 2009 09:09  Check: 9999 ..\n\n");
        for(int i=0; i<sze(gapped); i++) {
            appendName("s",i,sb.a(" Name: "));
            sb.a(" Len:").a(countLettrs(gapped[i])).a(" Check: 1 Weight:1\n");
        }
        sb.a("\n\n//\n\n");
    }

    public static BA toUngappedMultipleFasta(byte sequences[][],String prefix, int offset) {
        int size=0;
        for(byte[] s: sequences) size+=s.length+20;
        final BA sb=new BA(size);
        for(int i=0; i<sequences.length; i++) {
            sb.a('>').a(prefix).a(offset+i).a("\r\n");
            for(byte c :sequences[i]) {
                if ('a'<=c && c<='z' || 'A'<=c && c<='Z') sb.a((char)c);
            }
            sb.a("\r\n");
        }
        return sb;
    }
    /* KEEP
       public static byte[][] blockFormatAlignment(byte[][] gapped, char gap) {
       int maxCol=-1;
       for(byte[] gg : gapped) maxCol=maxi(maxCol,prev(LETTR,gg, sze(gg)-1,-1));
       final int w=maxCol+1;
       byte[][] t=gapped;
       for(int i=gapped.length; --i>=0;) {
       if (gapped[i]==null) gapped[i]=t[i]=new byte[w];
       if (sze(gapped[i])!=w) {
       if (t==gapped) t=gapped.clone();
       t[i]=chSze(t[i],w);
       }
       for(int k=sze(t[i]); --k>=0;) {
       if (!is(LETTR,t[i],k)) {
       if (t==gapped) t=gapped.clone();
       if (t[i]==gapped[i]) t[i]=gapped[i].clone();
       t[i][k]=(byte)gap;
       }
       }
       }
       return t;
       }
    */
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> C-Alpha atoms and Superimpose3D >>> */
    public static byte[][] insertWhereNoCoordinates(int startIdxR,byte[] gapped3dR, int startIdxM, byte[] gapped3dM,   byte[] seqR, byte[] seqM) {
        if (gapped3dR==null || gapped3dM==null || seqR==null || seqM==null) return null;
        byte[] outR=null,outM=null;
        while(true) {
            int oR=0,oM=0,  colR=0, colM=0, iR=0, iM=0;
            for(int i=0; i<startIdxR && iR<seqR.length;) { final byte c=seqR[iR++]; if ('A'<=c && c<='Z') i++; }
            for(int i=0; i<startIdxM && iM<seqM.length;) { final byte c=seqM[iM++]; if ('A'<=c && c<='Z') i++; }
            boolean alreadyFoundLetterR=false, alreadyFoundLetterM=false;
            while(iR<seqR.length && iM<seqM.length && (colR<gapped3dR.length || colM<gapped3dM.length) ) {
                final boolean isLetterR=is(LETTR,gapped3dR,colR++);
                final boolean isLetterM=is(LETTR,gapped3dM,colM++);
                int countXR=0, countXM=0;
                if (isLetterR) while(iR<seqR.length && !('A'<=seqR[iR] && seqR[iR]<='Z')) {
                        if (alreadyFoundLetterR) {
                            if (outR!=null) outR[oR]=seqR[iR];
                            oR++;
                            countXR++;
                        }
                        iR++;
                    }
                if (isLetterM) while(iM<seqM.length && !('A'<=seqM[iM] && seqM[iM]<='Z')) {
                        if (alreadyFoundLetterM) {
                            if (outM!=null) outM[oM]=seqM[iM];
                            oM++;
                            countXM++;
                        }
                        iM++;
                    }
                for(int i=countXM-countXR; --i>=0;) {
                    if (outR!=null) outR[oR]=' ';
                    oR++;
                }
                for(int i=countXR-countXM; --i>=0;) {
                    if (outM!=null) outM[oM]=' ';
                    oM++;
                }

                if (iR<seqR.length) {
                    if (outR!=null) outR[oR]=isLetterR ? seqR[iR] : (byte)' ';
                    oR++;
                }
                if (iM<seqM.length) {
                    if (outM!=null) outM[oM]=isLetterM ? seqM[iM] : (byte)' ';
                    oM++;
                }
                if (isLetterR) { alreadyFoundLetterR=true; iR++; }
                if (isLetterM) { alreadyFoundLetterM=true; iM++; }
            }
            if (outR==null) {
                outR=new byte[oR];
                outM=new byte[oM];
            }
            else break;
        }

        return new byte[][]{outR,outM};
    }

    public static int[] gaps2Columns(int gg[], int toIndex) {
        final int cc[]=new int[gg.length], to=mini(gg.length,toIndex);
        int c=0;
        for(int i=0; i<to; i++) {
            c+=gg[i];
            cc[i]=c++;
        }
        return cc;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Convert idx to columns  >>> */
    public static int[] letterIdx2column(byte text[]) {
        final int indices[]=new int[countLettrs(text)];
        for(int i=0,iLetter=0;i<text.length;i++) {
            final int  b=text[i];
            if ('a'<=b && b<='z' || 'A'<=b && b<='Z' ) indices[iLetter++]=i;
        }
        return indices;
    }

    public static int[] column2letterIdx(byte text[]) {
        final int n=text.length;
        int indices[]=new int[n];
        Arrays.fill(indices,-1);
        final boolean charClass[]=chrClas(LETTR);
        for(int i=0,iLetter=0;i<n;i++) {
            if ( text[i]>0 && charClass[text[i]]) indices[i]=iLetter++;
        }
        return indices;
    }
    public static int column2letterIdx(byte seq[],int from) {
        int count=0;
        for(int i=mini(from,seq.length);--i>=0;) {
            final int c=seq[i]|32;
            if ('a'<=c && c<='z') count++;
        }
        return count;
    }

 /* <<< Convert idx to columns <<< */
    /* ---------------------------------------- */
    /* >>> Convert idx to columns  >>> */

    /*
      Prefix "t" means templeate
      Prefix "m" means the gapped sequence to be modified
    */
    public static int[] useGapsFromExternalAlignment(byte[] tGapped, byte[] mGapped, byte[] tResidues, byte[] mResidues,  int tGaps[], int mGaps[]) {
        final int[] tPos=gaps2Columns(tGaps,MAX_INT);
        final int[] mPos=gaps2Columns(mGaps,MAX_INT);

        int tIdx=maxi(0,idxOfLetters(tGapped,tResidues,0, MAX_INT));
        int mIdx=maxi(0,idxOfLetters(mGapped,mResidues,0, MAX_INT));

        for(int col=0; col<tGapped.length; col++) {
            final boolean tLetter=is(LETTR,tGapped, col);
            final boolean mLetter=is(LETTR,mGapped, col);
            if (tLetter && mLetter) {
                //putln(" tIdx="+tIdx+" mIdx="+mIdx+" Correctur="+(sumOf(mGaps,mIdx)+mIdx));
                mGaps[mIdx]+=  tPos[tIdx] - (sumOf(mGaps,mIdx+1)+mIdx);
                if (mGaps[mIdx]<0) mGaps[mIdx]=0;
            }
            if (tLetter) tIdx++;
            if (mLetter) mIdx++;
        }
        for(int iA=mIdx; iA<mResidues.length; iA++) {
            mGaps[iA]+=  mPos[iA] - (sumOf(mGaps,iA+1)+iA);
            if (mGaps[iA]<0) mGaps[iA]=0;
        }
        return mGaps;
    }

    public static byte[][] gappedForPerfectMatch(byte[]...ungapped) {
        if (ungapped.length!=2) return null;
        final int iNeedle, iHaystack;
        if (ungapped[0].length<ungapped[1].length) { iNeedle=0; iHaystack=1; } else {iNeedle=1; iHaystack=0;}
        final int idx=strstr(STRSTR_IC,ungapped[iNeedle],ungapped[iHaystack]);
        byte gapped[][]=null;
        if (idx>0) {
            final byte[] seq=new byte[ungapped[iHaystack].length];
            System.arraycopy(ungapped[iNeedle],0, seq, idx, ungapped[iNeedle].length);
            Arrays.fill(seq,0,idx,(byte)' ');
            gapped=new byte[2][];
            gapped[iHaystack]=ungapped[iHaystack];
            gapped[iNeedle]=seq;
            return gapped;
        }
        return gapped;

    }

    /* <<<   <<< */
    /* ---------------------------------------- */
    /* >>>Trim >>> */
    public static boolean atLeastNumLettersInColumn(int num, int col, byte[][] ali) {
        int count=0;
        for(byte[] row : ali) {
            if (is(LETTR,row,col) && ++count>=num) return true;
        }
        return false;
    }

    public static byte[][] trimAlignment(int numLetters, byte[][] ali) {
        if (ali==null) return null;
        final int L=maxSze(ali);
        int from=0, to=L;
        while(from<L && !atLeastNumLettersInColumn(numLetters, from,ali)) from++;
        while(to>=0 && !atLeastNumLettersInColumn(numLetters,  to-1,ali)) to--;
        final byte[][] neu=new byte[ali.length][];
        for(int i=neu.length; --i>=0;) {
            neu[i]=substrgB(ali[i],from,to);
        }
        return neu;

    }
    public final static long PRINT_ALI_IDENT_LETTER=1<<1, PRINT_ALI_TRUNCATE=1<<2;
    public static void aliWithMidline(long options, byte[] gg1, int start1, String header1, byte[] gg2, int start2, String header2, int width, BA ba) {

        final int toCol1=prev(LETTR,gg1), toCol2=prev(LETTR,gg2);
        final int toCol=0!=(options&PRINT_ALI_TRUNCATE)?mini(toCol1,toCol2) : maxi(toCol1,toCol2);
        final int fromCol=0!=(options&PRINT_ALI_TRUNCATE)?maxi(nxt(LETTR,gg1,0,MAX_INT), nxt(LETTR,gg2)) : 0;

        final int hL1=sze(header1), hL2=sze(header2), hL=maxi(hL1,hL2)+1;
        final int numL=start1==MIN_INT?0:maxi(stringSizeOfInt(start1+toCol),stringSizeOfInt(start2+toCol));
        int num1=countLettrs(gg1,0,fromCol)+start1, num2=countLettrs(gg2,0,fromCol)+start2;

        for(int fromC=fromCol; fromC<toCol; fromC+=width) {
            for(int j=0;j<3;j++) {

                if (hL1+hL2>0) {
                    if (j==0) ba.a(header1).a(' ', hL-hL1);
                    if (j==1) ba.a(' ', hL);
                    if (j==2) ba.a(header2).a(' ', hL-hL2);
                }
                if (numL>0) {
                    if (j==0) ba.a(num1, numL).a(' ');
                    if (j==1) ba.a(' ', numL+1);
                    if (j==2) ba.a(num2, numL).a(' ');
                }

                for(int i=fromC; i<fromC+width && i<toCol; i++)  {

                    final int c=
                        j==0?gg1[i] :
                        j==1 && 0!=(options&PRINT_ALI_IDENT_LETTER) && (gg1[i]|32)==(gg2[i]|32) ? (gg1[i]&~32) :
                        j==1 ? Blosum.BL2SEQ_MIDLINE[gg1[i]&127][gg2[i]&127] :
                        gg2[i];

                    ba.a((char)c);
                    if (j==0 && is(LETTR, gg1, i)) num1++;
                    if (j==2 && is(LETTR, gg2, i)) num2++;
                }
                puts("< ");
                ba.a('\n');
            }
            ba.a('\n');
        }

    }

    public static byte[][] applyAlignPositions(int[][] resNum, byte[][] sequences) {
        final int n=sequences.length;
        final BA aa[]=new BA[n];
        for(int i=n; --i>=0; ) aa[i]=new BA(3333);
        final int last[]=new int[n];
        Arrays.fill(last,-1);
        boolean done=false;
        nextCol:
        for(int col=0; !done; col++) {
            done=true;
            for(int auffuellen=2; --auffuellen>=0;) {
                for(int iP=n; --iP>=0;) {
                    final byte ss[]=sequences[iP];
                    if (resNum[iP].length<=col) continue nextCol;
                    final int rn=resNum[iP][col];
                    if (rn>=ss.length) continue nextCol;
                    done=false;
                    if (auffuellen>0) {
                        final int from=last[iP]+1;
                        if (from>0 && rn>=0) {
                            final int to=rn;
                            if (to-from>0) {
                                //putln("iP="+iP+" col="+col+" from-to="+from+"-"+to);
                                for(int iP2=n; --iP2>=0;) {
                                    if (iP2==iP) aa[iP2].a(ss, from, to);
                                    else aa[iP2].a(' ',to-from);
                                }
                            }
                        }
                        last[iP]=rn;
                    } else {
                        aa[iP].a(rn<0? '_' : (char)ss[rn]);
                    }
                }
            }
        }
        final byte[] bb[]=new byte[n][];
        for(int i=n; --i>=0; ) bb[i]=aa[i].trimSize().bytes();
        return bb;

    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> DefaultAligner2 for two sequences >>> */
    public static void setDefaultAligner2(String className) { _defaultA2=className; }
    private static String _defaultA2;
    public static String defaultAligner2Class() {
        if (_defaultA2==null) {
            final boolean useInst=isSystProprty(IS_USE_INSTALLED_SOFTWARE);
            _defaultA2=
                useInst && fExists("/usr/bin/kalign") ? CLASS_MultipleAlignerKalign :
                useInst && fExists("/usr/bin/clustalw") ? CLASS_MultipleAlignerClustalW :
                CLASS_PairAlignerNeoBioPROXY;
        }
        return _defaultA2;
    }
    public static SequenceAligner newDefaultAligner2() {
        SequenceAligner sa=(SequenceAligner)mkInstance(defaultAligner2Class());
        if ((sa instanceof AbstractProxy ? ((AbstractProxy)sa).proxyObject() : sa)==null) {
            sa=(SequenceAligner)mkInstance(CLASS_MultipleAlignerClustalW);
            putln(RED_WARNING+"StrapAlign.defaultPairAligner() cannot make instance of ",defaultAligner2Class());
        }
        return sa;

    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Count identical >>> */

    public static Ratio getIdentity(byte seq1[],byte seq2[],int from, int to) {
        return getIdentity(null,seq1,seq2,from,to);
    }

    public static Ratio getIdentity(Ratio result,byte seq1[],byte seq2[],int from, int to) {
        if (seq1==null || seq2==null) return null;
        final int l=ChUtils.mini(seq1.length,seq2.length,to);
        int ident=0,count=0;
        for(int i=from;i<l;i++) {
            final int b1=seq1[i]|32,b2=seq2[i]|32;
            if ('a'<=b1  && b1<='z' &&'a'<=b2  && b2<='z') {
                count++;
                if (b1==b2) ident++;
            }
        }
        if (result==null) result=new Ratio(1,1);
        result.setNumerator(ident);
        result.setDenominator(count);
        return result;
    }

}
