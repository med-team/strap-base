package charite.christo.protein;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class MergeAlignments {
    final int[][] _insertsAtCol, _inserts;
    public MergeAlignments(int[] startIdx, byte soll[][],  byte ist[][]) {
        final int nRow=ist.length;
        _inserts=new int[nRow][];
        _insertsAtCol=new int[nRow][];

        final int colIst[][]=new int[nRow][];
        final int col2aSoll[][]=new int[nRow][];
        for(int r=0;r<nRow;r++) {
            col2aSoll[r]=AlignUtils.column2letterIdx(soll[r]);
            final int start=startIdx==null ? countLettrs(ist[r],0,idxOfLetters(soll[r],ist[r],0,MAX_INT)) : get(r,startIdx);
            if (start>0) {
                for(int i=col2aSoll[r].length; --i>=0;) {
                    if (col2aSoll[r][i]>=0) col2aSoll[r][i]+=start;
                }
            }
            colIst[r]=AlignUtils.letterIdx2column(ist[r]);
            _inserts[r]=new int[countLettrs(ist[r])];
            _insertsAtCol[r]=new int[ist[r].length];
        }
        final int sumInserted[]=new int[nRow];
        boolean busy=true;
        final int toBeInserted[]=new int[nRow];
        for(int column=0;busy;column++) {
            busy=false;
            Arrays.fill(toBeInserted,0);
            int minInserted=0;
            for(int r=0;r<nRow;r++) {
                if (col2aSoll[r].length<=column) continue;
                busy=true;
                final int iAa=col2aSoll[r][column];
                if (iAa<0) continue;
                if (colIst[r].length>iAa) { // catch the case that soll-sequ longer than ist-sequ
                    toBeInserted[r]=column-colIst[r][iAa]-sumInserted[r];
                    if (minInserted>toBeInserted[r]) minInserted=toBeInserted[r];
                }
            }
            for(int r=0;r<nRow;r++) {
                if (col2aSoll[r].length<=column) continue;
                final int iAa=col2aSoll[r][column];
                if (iAa<0) continue;
                final int ins=toBeInserted[r]-minInserted;
                if (_inserts[r].length>iAa) {
                _inserts[r][iAa]=ins;
                if (colIst[r].length>iAa) {
                    _insertsAtCol[r][colIst[r][iAa]]=ins;
                    sumInserted[r]+=ins;
                }
                }
            }
        }

    }

    public int[][] getInsertsAtCol() { return _insertsAtCol; }

}
