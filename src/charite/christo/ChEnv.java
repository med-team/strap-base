package charite.christo;
import java.util.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
Java1.4 uses shell command "set"

*/
public class ChEnv {
    private ChEnv(){}
    private static Map<String,String> _map;
    private static synchronized Map<String,String> get() {
        Map m=_map;
        if (m==null) {
            m=_map=new HashMap();
            final boolean isWin=isWin();
            Map mSyst=null;
            try { mSyst=System.getenv(); } catch(Throwable ex) {putln("Caught in ChEnv ",ex);}
            if (mSyst!=null) {
                if (!isWin) m.putAll(mSyst);
                else {
                    for(Map.Entry e : entryArry(mSyst)) m.put(((String)e.getKey()).toUpperCase(),e.getValue());
                }
            } else {
                final ChExec ex=new ChExec(ChExec.WITHOUT_ASKING|ChExec.STDOUT).setCommandLine("SH set");
                ex.run();
                for(String s : splitStrg(ex.getStdout(),'\n')) {
                    final int iEqu=s.indexOf('=');
                    if (iEqu<1 || !is(LETTR_DIGT,s,0) || !is(LETTR_DIGT,s, iEqu-1)) continue;
                    final String k=s.substring(0,iEqu), v=s.substring(iEqu+1);
                    m.put(isWin?k.toUpperCase() : k,v);
                }
                ex.dispose();
            }
            final int L=m.size();
            final String[] vDollarKeys=new String[(isWin?2:1)*L], vKeys=new String[L], vFile=new String[vDollarKeys.length];
            int iDollarKeys=0, iKeys=0, iFile=0;
            for(Map.Entry e : entryArry(m)) {
                final String k=(String)e.getKey(), v=(String)e.getValue();
                if (v==null) continue;
                final String dk="$"+k;
                m.put(dk,v);
                vDollarKeys[iDollarKeys++]=dk;
                vKeys[iKeys++]=k;
                final boolean isFile= looks(LIKE_FILEPATH,v) && new File(v).exists();
                if (isFile) vFile[iFile++]=dk;
                if (isWin) {
                    final String pk="%"+k+"%";
                    m.put(pk,v);
                    vDollarKeys[iDollarKeys++]=pk;
                    if (isFile) vFile[iFile++]=pk;
                }
            }

            _keys=new String[][]{
                chSze(vKeys,iKeys),
                chSze(vDollarKeys,iDollarKeys),
                chSze(vFile,iFile)
            };
        }
        return m;
    }

    public final static int WITHOUT_DOLLAR=0, WITH_DOLLAR=1, WITH_FILEPATH=2;
    private static String[][] _keys;
    public static String[] getNames(int i) { get(); return _keys[i];  }

    public static String get(String s) { return s==null?null : get().get(isWin()?s.toUpperCase() : s); }

    /* <<< get <<< */
    /* ---------------------------------------- */
    /* >>> Debug >>> */
    public static BA propertiesAsText() {
        final BA sb=new BA(9999).a("shortCutMask=").aln(Integer.toBinaryString(shortCutMask())).aln("\n"+ANSI_UL+"System.getProperties()"+ANSI_RESET);
        for(Object key : keyArry(System.getProperties())) sb.a(key).a(" = ").aln(System.getProperties().get(key));
        for(int k=2; --k>=0;) {
            sb.a("\n"+ANSI_UL).a(k==0 ? "systProprty(SYSP":"systProprty(IS").a("_...)"+ANSI_RESET);
            final String nn[]=finalStaticInts(ChUtils.class,30);
            for(int i=0; i< nn.length; i++) {
                if (!strEquls(k==0?"SYSP_":"IS_", nn[i])) continue;
                sb.a(nn[i]).a(' ');
                if (k==0) sb.aln(systProprty(i)); else sb.a(isSystProprty(i)).a('\n');
            }
        }
        sb.aln("\n"+ANSI_UL+"Various methods"+ANSI_RESET);
        for(String method : splitStrg("javaVsn() dirWorking() dirHome() urlThisJarFile() thisJarFile() myComputer() prgParas()",' ')) {
            sb.a(' ').a(method).a(" = ").aln(invokeMthd(RFLCT_PRINT_EXCEPTIONS, delSfx("()",method), ChUtils.class));
        }

        sb.aln("\n"+ANSI_UL+"Constructing files from path"+ANSI_RESET);
        for(String path : splitStrg("C:\\temp \\temp /cygdrive/C/temp . .. ~/ ~ ~/@/",' ')) {
            if (!isWin() && path.indexOf("temp")>0) continue;
            sb.a(path).a("  f=").a(file(path));
            try {
                sb.a("  canonicalPath=").aln(file(path).getCanonicalPath());
            } catch(Exception e){stckTrc(e);}
        }

        sb.aln("\n"+ANSI_UL+"Roots"+ANSI_RESET);
        for(int i=0;i<2;i++) {
            sb.a("\n\nFile.list").a(i==0?"":"Existing").aln("Roots()=");
            for(File f: i==0? File.listRoots() : listExistingFileRoots())
                sb.a(f).aln(f.canRead() ? " (exists)" : " (not available)");
        }
        return sb.trimSize();
    }
    public static BA asText() {
        final BA sb=new BA(9999);
        final int longest=sze(longestName(getNames(WITHOUT_DOLLAR)));
        final String ulms="java.util.Arrays.useLegacyMergeSort";
        sb.a(ulms).a('=').a(sysGetProp(ulms)).a(' ').a(legacyMergeSort()).a('\n');
        for(String k : getNames(WITHOUT_DOLLAR)) {
            if (!is(LETTR_DIGT_US,k,0)) continue;
            sb.a(' ', longest-k.length()).a(ANSI_UL).a(k).a(ANSI_RESET+"  ").a(get(k)).a('\n',2);
        }
        return sb.trimSize();
    }
    /* <<< Debug <<< */
    /* ---------------------------------------- */
    /* >>> Exec >>> */
    private static String _path[];
    public static String[] execPath() {
        if (_path==null) _path=splitStrg(get("PATH"), systProprty(SYSP_PATH_SEP).charAt(0));
        return _path;
    }

    public static boolean[] executablesExist(String ee[]) {
        final boolean exists[]=new boolean[ee.length];
        for(int i=ee.length; --i>=0; ) {
            if (looks(LIKE_FILEPATH,ee[i])) {
                final File f=file(ee[i]);
                exists[i]=sze(f)>0 && !f.isDirectory();
            } else {
                for(String path : execPath()) {
                    final File f=file(path+(is(SLASH,lstChar(path))?"":"/")+ fstTkn(ee[i]));
                    if (exists[i]|=sze(f)>0 && !f.isDirectory()) break;
                }
            }
        }
        return exists;
    }
    public static String[] executablesReorder(String ee[]) {
        final String ss[]=new String[ee.length];
        final boolean[] exists=executablesExist(ee);
        int count=0;
        for(int yesNo=2; --yesNo>=0;) {
            for(int i=0;i<ee.length;i++) {
                if( (yesNo>0) == exists[i]) ss[count++]=ee[i];
            }
        }
        return ss;

    }

}
