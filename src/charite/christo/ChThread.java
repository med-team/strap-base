package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   Periodically calls Runnable objects.
   Runs in one One universal thread
*/
public final class ChThread implements Runnable {
    public final static int WEAK_REFERENCE=1<<1;  static { assert EDT<WEAK_REFERENCE;}

    /* --- Begin Job Instance ---- */
    private ChThread(Object run, String name) { _r=run; _name=name; }
    private final Object _r;
    private final String _name;
    private long _opt;
    private long _lastCall, _everyMs, _cumulTime;
    /* --- End Job Instance ---- */

    private final static List<ChThread> _vR=new Vector();
    private static boolean _running;
    private static ChThread _inst;
    private static void mayStart() {
        if (!_running) {
            _running=true;
            if (_inst==null) _inst=new ChThread(null,null);
            final Thread t=new Thread(_inst);
            t.setDaemon(true);
            startThrd(t);
        }
    }
    public static ChThread callEvery(int opt, int everyMs, Runnable runnable,String name) {
        ChThread thread=null;
        for(int i=_vR.size(); --i>=0;) {
            final ChThread ri=get(i,_vR,ChThread.class);
            if (ri==null) continue;
            final Runnable r=deref(ri._r,Runnable.class);
            if (everyMs<0) {
                if (r==runnable || r==null) _vR.remove(ri);
            } else {
                if (r==runnable) {
                    thread=ri;
                    break;
                }
            }
        }
        if (everyMs>0) {
            if (thread==null) _vR.add(thread=new ChThread(  (opt&WEAK_REFERENCE)!=0 ? wref(runnable): runnable, name));
            thread._opt=opt;
            thread._everyMs=everyMs;
            mayStart();
        }
        return thread;
    }

    public void setEveryMs(long ms) { if (ms<0) _vR.remove(this); _everyMs=ms;}
    public void run() {
        final int SLEEP=20;
        for(int n=0; ;n++) {
            if (n<0) n=0;
            if ((n&((1<<6)-1))==0) _vKeepRef[(n>>>6)&1].clear();
            for(int i=_vR.size(); --i>=0;) {
                final ChThread ri=(ChThread)_vR.get(i);
                final Runnable runnable=ri==null?null:deref(ri._r, Runnable.class);
                if (runnable==null) { _vR.remove(i); continue;}
                if (ri._lastCall<0) {
                    if ((ri._opt&EDT)!=0) inEDT(runnable);
                    else {
                        final long time=System.currentTimeMillis();
                        runnable.run();
                        ri._cumulTime+=System.currentTimeMillis()-time;
                    }
                    ri._lastCall+=ri._everyMs;
                }
                ri._lastCall-=SLEEP;
            }
            sleep(SLEEP);
        }
    }
    private static BA debugList(ThreadGroup g, int stufe, BA sb) {
        if (g!=null) {
            final Thread tt[]=new Thread[g.activeCount()];
            final ThreadGroup gg[]=new ThreadGroup[g.activeGroupCount()];
            final int nT=g.enumerate(tt,false);
            final int nG=g.enumerate(gg,false);
            sb.a(" *** ").a('+',stufe)
                .a(" Group=").a(g.getName())
                .a(" #groups=").a(nG)
                .a(" #threads=").a(nT)
                .aln(" *** ");
            for(int i=nT;--i>=0;) {
                sb.a(' ',stufe).a("Thread ").aln(tt[i] instanceof ChDelay ? tt[i] : tt[i].getName());
            }
            for(int i=0;i<nG;i++) debugList(gg[i],stufe+1,sb).a('\n');
        }
        return sb;
    }
    public static BA debugListThreads(BA sb) {
        sb.aln("\n *** Threads in ChThread:");
        for(int i=0; i<_vR.size(); i++) {
            final ChThread ri=get(i,_vR,ChThread.class);
            if (ri!=null) sb.a(ri._name).a("  cumul time=").a(ri._cumulTime).aln(" ms");
        }
        ThreadGroup g=Thread.currentThread().getThreadGroup(),g2;
        while( (g2=g.getParent())!=null) g=g2;
        return debugList(g,0, sb);
    }

    public static void simulateLongComputation() {
        final long time=System.currentTimeMillis();
        double a=1.00003;
        while(true) {
            for(int i=0; i<0xFFffFFf; i++) a=a*a;
            puts("/");
            if (System.currentTimeMillis()-time>1000*20) break;
        }
    }

    private final static List[] _vKeepRef={new Vector(),new Vector()};
    public static void preventGC(Object o) {
        _vKeepRef[0].add(o);
        _vKeepRef[1].add(o);
        mayStart();
    }
}
