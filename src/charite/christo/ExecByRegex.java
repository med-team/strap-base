package charite.christo;
import java.util.*;
import java.io.File;
import javax.swing.JComponent;
import java.awt.event.*;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

   This dialog allows to run programs on selected text or the current word under the mouse pointer.

   All registered programs with a matching filter for the current text are displayed in an expandable tree. They can
   be started by mouse click. For details, hold ctrl while clicking.

    <div class="figure" >
    <table style="caption-side: bottom">
    <CAPTION><b>Figure</b> Dialog to apply shell scripts or  Web addresses to selected text.
    Each  node in the tree represents a shell script which can be started by left click.
    The text in the text box "Argument for shell scripts" replaces the placeholder in the script (asterisk). Users can define own scripts.
    </CAPTION>
    <tr><td><i>JCOMPONENT:ExecByRegex#screenshot()!</i></td></tr>
    </table>
    </div>

   <br><br>
   The list of shell-scripts is editable using the tool button at the top of the dialog.
   Each line contains three tab-separated fields (Type Ctrl+I for  WIKI:Tab_character).

   Example:
   <pre class="data">
   demo\Example google&#09;.*&#09;http://www.google.com/search?q=*
   demo\Example: Matches starting with M&#09;^M.*$&#09; echo starts with letter M *
   </pre>
   Each line contains three fields separated by tab:
   <ol>
   <li>The display name which may consist of several path components separated by slash.</li>
   <li>Optional a regular expression which. If the test matches the regular expression, the entry is displayed.</li>
   <li>An executable file or shell script or the URL of a web service. The asterisk is replaced by the current text.</li>
   </ol>

   <br><br>
   <b>For administrators:</b>Alternatively, the program option "-customizeAddScriptByRegex" can be used to specify a file with shell-scripts.
   <br><br>
   <i>OS:W,<b>Windows-Cygwin:</b>If the program path contains the string "cygwin", then the process is started in a new cygwin shell rather than cmd.exe-shell.</i>

   @author Christoph Gille
*/
public final class ExecByRegex implements ProcessEv, ChRunnable, ActionListener {
    public final static int
        DEBUG=1<<1, ONLY_SELECTED_P=2, SERVER_CACHE=1<<3,
        ATOM_COORDINATES=1<<4, ATOM_COORDINATES_IF_SELECTED=1<<5,
        CALPHA_COORDINATES=1<<6, CALPHA_COORDINATES_IF_SELECTED=1<<7,
        FILE_CONTENT=1<<8, FILE_CONTENT_IF_SELECTED=1<<9;
    public final static String RUN_SCRIPT="EBR$$RS";
    private final int _customize;
    private JComponent _panel, _cbSelected, _jlJobs;
    private ChTextArea _ta;
    private ChJTree _jT;
    private final Collection _vName=new UniqueList(String.class);
    private ChRunnable _run;
    private final Map<String,String> _map=new HashMap();
    private ChPanel _labError;
    private static ExecByRegex _inst[];
    public ExecByRegex(int customize) { _customize=customize; }
    private final Vector _jobs=new Vector();

    public static JComponent panel(int customize,  Object para) {
        if (_inst==null) _inst=new ExecByRegex[Customize.MAX_ID+1];
        ExecByRegex i=_inst[customize];
        if (i==null) addActLi(i=_inst[customize]=new ExecByRegex(customize),Customize.customize(customize));
        if (para instanceof String) {
            i.panel();
            i._ta.t((String)para);
            if (i._jT!=null) i.run("U",null);
        } else i._run=(ChRunnable)para;
        return i.panel();
    }
    public JComponent panel() {
        if (_panel==null) {
            final boolean regex=_customize==Customize.scriptByRegex;
            final ChTreeModel tm=new ChTreeModel(0L,new Strings2Tree(Strings2Tree.LEAFS_CONTAIN_ENTIRE_PATH, _vName, chrClas(SLASH)));
            _jT=new ChJTree(0L,tm);
            _jT.setCellRenderer(new ChRenderer().options(ChRenderer.LAST_PATH_COMPONENT_SLASH));
            _jT.setRootVisible(false);
            _jT.setShowsRootHandles(true);
            _jT.setRowHeight(ICON_HEIGHT);
            rtt(_jT);
            evAdapt(this).addTo("m",_jT);
            pcp(ChJTree.KEY_NUM_EXPAND, "4",_jT);

            if (!regex) {
                _cbSelected=cbox(false,"Act only on selected proteins",
                                 "Include only proteins that are selected and the protein at the cursor position.<br>This will reduce the size of the XML output file.",null);
                _jlJobs=new ChJList(_jobs, 0);
            }
            final Object
                pNorth,
                pSouth,
                pEast=pnl(smallSourceBut(ExecByRegex.class), Customize.newButton(_customize, regex?Customize.scriptByRegex:Customize.strapApps).t(null).rover(IC_CUSTOM));
            if (regex) {
                addActLi(evAdapt(this),_ta=new ChTextArea(3,3));
                _ta.tools().saveInFile("ta_ExecByRegex");
                final Object pNorthS=pnl(CNSEW,_ta,"\nArgument for shell script  ","Click on item to run shell scripts/open URL."," ", " ");
                pSouth= setFG(0xFF0000, _labError=new ChPanel());
                pNorth=pnl(CNSEW,"<h2>Shell scripts and web apps</h2>",null, pNorthS, pEast, smallHelpBut(ExecByRegex.class));
            } else {
                final Object pNorthS="Click on item to run shell scripts or web service. Hold Ctrl-key for log message.";
                pSouth=pnl(CNSEW,_jlJobs, _cbSelected, _jlJobs);
                final Object bHelp=ChButton.doView(_CCS+"/apps.html").rover(IC_HELP)
                    .cp(KEY_SOUTH_PANEL,pnl(HBL, "For developers: ", new ChButton().doView(_CCS+"/appsDevel.html").t("Continue reading")));
                pNorth= pnl(CNSEW,"<h2>Shell scripts and web apps for Strap</h2>",null,pNorthS,pEast, bHelp);
            }
            _panel=pnl(CNSEW,scrllpn(0,_jT,dim(1,4*EX)),pNorth,pSouth);
            addActLi(evAdapt(this),Customize.customize(_customize));
            inEDT(thrdCR(this,"U"));
            _panel.setPreferredSize(dim(300,300));

        }
        return _panel;
    }

    public void addDir(File f, String path) {
        if (f==null) return;
        final String path2=path==null ? "" : (path==""?"":path+"/")+f.getName();
        if (isDir(f)) {
            for(File child: lstDirF(f)) addDir(child, path2);
        }
        else if (endWith(".list",f)) {
            adLines(readLines(f), false, null);
        } else ad(path2,toStrg(f));
    }
    private void ad(String name, String script) {
        if (sze(name)*sze(script)==0) return;
        _map.put(name, script);
        _vName.add(name);
        TabItemTipIcon.set("", delToLstChr(delToLstChr(name,'\\'),'/') , script, iicon(strEquls("http://",script)?IC_WWW:IC_BATCH), name);
    }

    private String adLines(String[] lines, boolean regex, String txt) {
        String error="";
        for(String line0 : lines) {
            final String line=toStrgTrim(line0);
            if (line==null || chrAt(0,line)=='#') continue;
            final String cols[]=line.split(line.indexOf('\t')>0?"\t":"%09"),  name=get(0, cols),  script=get(regex?2:1, cols);
            if (sze(name)*sze(script)==0 || !Insecure.EXEC_ALLOWED && !strEquls("http://",script) ) continue;
            if (regex && sze(cols[1])>0) {
                java.util.regex.Pattern p=null;
                try { p=ChRegex.pattern(cols[1]); } catch(Exception ex) {}
                if (p==null) { error="Error regular expr: "+cols[1]; continue; }
                if (!p.matcher(txt).matches()) continue;
            }
            ad(name, script);
        }
        return error;
    }
    public Object run(String id, Object arg) {
        final Object[] argv=arg instanceof Object[] ? (Object[])arg: null;
        if (id==RUN_SCRIPT) {
            final String job="Running "+argv[0];
            _jobs.add(job);
            _jlJobs.revalidate();
            _run.run(RUN_SCRIPT,arg);
            _jobs.remove(job);
            ChDelay.revalidate(_jlJobs, 555);
        }
        if (id=="U") {
            final String txt=toStrgN(_ta);
            _vName.clear();
            _map.clear();
            final boolean regex=_customize==Customize.scriptByRegex;
            final String error=adLines(custSettings(_customize), regex, txt);
            if (!regex) {
                addDir(file("/etc/strap/apps"), null);
                addDir(file(dirSettings()+"/apps"), null);
            }
            if (_labError!=null) _labError.t(error).revalidate();
            _jT.updateKeepStateLater(0L, 99);
        }
        if (id=="EXE") {
            final boolean debug=argv[1]!=null;
            if (debug) ChExec.log().send();
            new ChExec((debug?ChExec.SHOW_STREAMS:0L)|ChExec.STDOUT_IN_TEXT_BOX).setCommandLineV(argv).run();

        }
        return null;
    }
    /* <<< ChRunnable <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void processEv(java.awt.AWTEvent ev) {
        final Object q=ev.getSource(), item=objectAt(null,ev);
        final int modi=modifrs(ev),  id=ev.getID();
        final boolean ctrl=0!=(modi&CTRL_MASK);
        final String script=item==null ? null : _map.get(item);
        if (q==_ta || q instanceof Customize) inEdtLaterCR(this, "U",null);
        if ( (id==MOUSE_PRESSED || id==MOUSE_CLICKED) && q==_jT && script!=null) {
            final boolean http=strEquls("http://",script);
            final String sh=http?script: (isWin() && strstr(STRSTR_IC,"cygwin",script)>=0 ? ChExec.CYGWINSH:"SH")+" "+script;
            if (_customize==Customize.scriptByRegex) {
                final String txt=toStrgTrim(_ta);
                final BA log=ctrl?new BA(99):null;
                if (sze(txt)>0) {
                    final String command0=rplcToStrg("*",txt,script);
                    if (isPopupTrggr(false,ev)) {
                        if (isPopupTrggr(true,ev)) shwTxtInW(ChTextComponents.FRAME_OPTS|ChFrame.ALWAYS_ON_TOP,"Command",toBA(command0));
                    } else if (id==MOUSE_CLICKED) {
                        if (Web.localhost_sendData(command0, log)) {
                            if (log!=null) shwTxtInW(ChTextComponents.FRAME_OPTS|ChFrame.ALWAYS_ON_TOP, "localhost", log);
                        }
                        else if (http) visitURL(command0.trim(), modi);
                        else startThrd(thrdCR(this,"EXE",new String[]{sh,ctrl?"":null}));
                    }
                }
            } else {
                if (id==MOUSE_CLICKED) {
                    final int opt=
                        (ctrl?DEBUG:0)|
                        (isSelctd(_cbSelected)?ONLY_SELECTED_P:0)|
                        (strstr(STRSTR_w, "CALPHA_COORDINATES",sh)>=0 ? CALPHA_COORDINATES : 0)|
                        (strstr(STRSTR_w, "CALPHA_COORDINATES_IF_SELECTED",sh)>=0 ? CALPHA_COORDINATES_IF_SELECTED : 0)|
                        (strstr(STRSTR_w, "ATOM_COORDINATES",sh)>=0 ? ATOM_COORDINATES : 0)|
                        (strstr(STRSTR_w, "ATOM_COORDINATES_IF_SELECTED",sh)>=0 ? ATOM_COORDINATES_IF_SELECTED : 0)|
                        (strstr(STRSTR_w, "FILE_CONTENT",sh)>=0 ? FILE_CONTENT : 0)|
                        (strstr(STRSTR_w, "FILE_CONTENT_IF_SELECTED",sh)>=0 ? FILE_CONTENT_IF_SELECTED : 0)|

                        (strstr(STRSTR_w, "SERVER_CACHE",sh)>=0 ? SERVER_CACHE : 0);
                    startThrd(thrdCR(this, RUN_SCRIPT, new Object[]{sh, intObjct(opt)}));
                }
            }
        }
    }
    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        if (q instanceof Customize) {
            run("U",null);
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> html >>> */
    public static JComponent screenshot() {
        final ExecByRegex ex=new ExecByRegex(Customize.scriptByRegex);
        final JComponent jc=ex.panel();
        ex._ta.t(" Hello");
        ChJTree.expandAllNodes(null,ex._jT);
        jc.setPreferredSize(dim(50*EM,20*EX));
        return jc;
    }
    /* <<< html <<< */
    /* ---------------------------------------- */
 }
