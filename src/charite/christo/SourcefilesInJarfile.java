package charite.christo;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.zip.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;

/**
Lists all java source files in a give jar file
@author Christoph Gille
*/
// java charite.christo.SourcefilesInJarfile $HOME/java/strap.jar
public class SourcefilesInJarfile implements ProcessEv {
    private final List<ZipFile> vJarfiles=new ArrayList();
    private final List<String> _vEntries=new ArrayList();
    private ChJTree _t;
    private void add(ZipFile jf) {  adUniq(jf, vJarfiles);}

    private void collectFiles() {
        for(int i=0; i<sze(vJarfiles); i++) {
            final ZipFile jf=get(i,vJarfiles,ZipFile.class);
            if (jf==null) continue;
            final Enumeration entries=jf.entries();
            while(entries.hasMoreElements()) {
                final ZipEntry je=((ZipEntry)entries.nextElement());
                final String eName=je.getName(), name=delSfx(".java",eName);
                if (eName==name) continue;
                adUniq(name.replace('/','.'), _vEntries);
            }
        }
        sortArry(_vEntries);
        tree().updateKeepStateLater(0L, 99);
    }


    private ChJTree tree() {
        if (_t==null) {
            _t=new ChJTree(0L,new ChTreeModel(0L, new Strings2Tree(Strings2Tree.LEAFS_CONTAIN_ENTIRE_PATH|Strings2Tree.DIRS_FIRST, _vEntries, chrClas1('.'))));
            _t.setCellRenderer(new ChRenderer().options(ChRenderer.LAST_PATH_COMPONENT_DOT|ChRenderer.CLASS_NAMES));
            _t.setRootVisible(false);
            _t.setShowsRootHandles(true);
            _t.setBackground(C(DEFAULT_BACKGROUND));
            evAdapt(this).addTo("m",_t);
        }
        return _t;
    }

    public SourcefilesInJarfile(ZipFile...jarFiles){
        if (jarFiles!=null) for(ZipFile jf:jarFiles) add(jf);
    }
    private static SourcefilesInJarfile instance;
    public static ChButton newButton() { return newButton(null); }

    private static ChButton newButton(final SourcefilesInJarfile instance0) {
        if (instance0==null && instance==null) instance=new SourcefilesInJarfile();
        return new ChButton("L").t("List all Java Classes").li( evAdapt(instance0!=null ? instance0 : instance));
    }

    public void processEv(AWTEvent ev) {
        final String cmd=actionCommand(ev);
        final int id=ev.getID();
        if (id==MOUSE_PRESSED) {
            final String s=(String)objectAt(null,ev);
            if (strchr('.',s)>0) ChJavadoc.menu(s,(String[])null).show( tree(), x(ev)+EM, y(ev)+EX);
        }
        if (cmd=="L") {
            if (0==sze(vJarfiles)) {
                try {
                    java.io.File f=thisJarFile();
                    if (f==null && myComputer()) f=file("~/java/strap.jar");
                    if (f==null) return;
                    adUniq(new ZipFile(f),vJarfiles);
                    collectFiles();
                }catch(Exception ex){stckTrc(ex);}
            }
            ChFrame.frame("Classes in vJarfiles", scrllpn(SCRLLPN_TOOLS, tree()), ChFrame.STAGGER).shw(ChFrame.TO_FRONT);
        }
    }

}
