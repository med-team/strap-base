package charite.christo;
import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**HELP
   <h2>List of proteins, residue selections, hetero compounds and DNA/RNA</h2>.
   The list supports:<ul>
   <li>Drag'n drop  (Watch movie
   <i>STRING:ChConstants#MOVIE_Export_Proteins</i>
   <i>STRING:ChConstants#MOVIE_Drag_to_another_STRAP</i>
   <i>STRING:ChConstants#MOVIE_Drag_protein_to_3D</i>
   <i>STRING:ChConstants#MOVIE_Sequence_Features_in_3D</i>)
   <li>Reordering</li>
   <li>Context menu (Right-click)</li>
   </ul>
   <b>Keep window always on top of all other windows: </b>Ctrl+T  toggls "always on top" on/off<br><br>
   <b>Frequently used menu-items: </b>Menu items of the context-menu can be dragged out. They are found below the item list and act on the selected items.<br><br>
   <b>Sequence groups:</b> If the list is displaying proteins, two tool buttons provide "Sequence group" like functionality.
   @author Christoph Gille
*/
public class ChJList extends JList implements IsEnabled, ProcessEv, HasMC {
    public final static int OPTIONS_FILES=ChJTable.ICON_ROW_HEIGHT|ChJTable.DEFAULT_RENDERER|ChJTable.FILE_POPUP|ChJTable.RTT|ChJTable.DRAG_ENABLED;
    public final static String KEY_NO_HELP_ON_RIGHT_CLICK="CJL$$NH", KEY_UPDATE_ENABLED="CJL$$UE", KEY_ENABLE_IF_SELECTED="CJL$$ES";
    private int _selMC;
    private long _opt;
    private boolean _isInit;
    private ChButton _miHelp,  _miCtrl;
    private JPopupMenu _popup;
    private List _list;

    final Map MAPCP=new HashMap();
    /* >>> Instance >>> */

    public void setData(Object data) {
        if (data instanceof ListModel) setModel((ListModel)data);
        else if (data instanceof List) setModel(new JListModel(_list=(List)data));
        else if (data instanceof Object[]) {
            final DefaultListModel m=new DefaultListModel();
            for(Object o : (Object[])data) m.addElement(o);
            setModel(m);
        }
    }
    public long options() { return _opt;}
    public void addOptions(long options) { _opt|=options;}
    public ChJList(Object data, long options) {
        _opt=options;
        setData(data);
        int fixed=3*EX/2;
        if ( (options&ChJTable.EX_ROW_HEIGHT)!=0) fixed=EX+2;
        if ( (options&ChJTable.ICON_ROW_HEIGHT)!=0) fixed=ICON_HEIGHT;
        if ( (options&ChJTable.DEFAULT_RENDERER)!=0) setCellRenderer(new ChRenderer());
        if ( (options&ChJTable.CLASS_RENDERER)!=0) setCellRenderer(new ChRenderer().options(ChRenderer.CLASS_NAMES));
        if ( (options&ChJTable.RTT)!=0) rtt(this);
        if ( (options&ChJTable.FILE_POPUP)!=0) {
            if (gcp(DialogStringMatch.KEY_SAVE,this)==null) pcp(DialogStringMatch.KEY_SAVE, "searchFiles",this);
            FilePopup.instance().register(this);
        }
        if ( (options&ChJTable.SINGLE_SELECTION)!=0) setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setFixedCellHeight(fixed);
        if ( (options&ChJTable.DRAG_ENABLED)!=0) setDragEnabled(true);
        setFG(0,this);
        scrollByWheel(this);
        evAdapt(this).addTo("s",this);
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Model >>> */
    public List getList() { return _list; }
    public boolean isEnabled(Object o) {
        final Object
            enabled0=gcp(KEY_ENABLED,this),
            enabled=enabled0 instanceof ChCombo ? ((ChCombo)enabled0).instanceOfSelectedClass(IsEnabled.class) : enabled0;
        final IsEnabled en= isInstncOf(IsEnabled.class,enabled) ?(IsEnabled) enabled: null;
        return en!=null ? en.isEnabled(o) : true;
    }
    public ChJList setSelII(int[] ii) {
        pcp(KEY_IGNORE_SELECTION_EVT,"",this);
        clearSelection();
        setSelectedIndices(ii);
        pcp(KEY_IGNORE_SELECTION_EVT,null,this);
        _selMC++;
        return this;
    }
    public ChJList setSelI(int i) {
        pcp(KEY_IGNORE_SELECTION_EVT,"",this);
        setSelectedIndex(i);
        pcp(KEY_IGNORE_SELECTION_EVT,null,this);
        _selMC++;
        return this;
    }

    public final static long SEL_STRSTR=1<<1, SEL_STRSTR_IC=1<<2;
    public static Runnable thread_setSelO(long options, Object o, JComponent c) {
        return thrdM("setSelO",ChJList.class, new Object[]{longObjct(options),o, c});
    }
    public static void setSelO(long options, Object o, JComponent c) {
        if (c==null) return;
        final ChJList jl=deref(c,ChJList.class);
        final ChCombo jc=deref(c,ChCombo.class);
        if (jl==null && jc==null) assrt();
        if (!isEDT()) inEdtLater(thread_setSelO(options,o, c));
        else {
            final ListModel m=jc!=null?jc.getModel() : jl.getModel();
            if (m!=null) {
                final boolean ic=0!=(options&SEL_STRSTR_IC);
                for(int i=m.getSize(); --i>=0;) {
                    final Object oi=get(i,m);
                    boolean found=oi==o;
                    if (oi instanceof String && o instanceof String) {
                        found|=oi.equals(o);
                        if (ic || 0!=(options&SEL_STRSTR)) found|=strstr(ic?STRSTR_IC:0, o, oi)>=0;
                    }
                    if (found) {
                        if (jl!=null) jl.setSelI(i);
                        if (jc!=null) jc.s(i);

                        break;
                    }
                }
            }
        }
    }

    public ChJList setSelOO(Object[] oo) { setSelII(selIndices(oo,getModel())); return this; }
    public int mc() { return _selMC;}

    /* <<< Model <<< */
    /* ---------------------------------------- */
    /* >>> Scroll >>> */
    public void scrollToSelected() {
        if (!isVisbl(this)) return;
        int iFst=MAX_INT, iLst=-1;
        final int ii[]=getSelectedIndices();
        for(int i=sze(ii); --i>=0;) {
            if (ii[i]>=0) {
                if (ii[i]<iFst) iFst=ii[i];
                if (ii[i]>iLst) iLst=ii[i];
            }
        }
        final Rectangle r=iLst<0?null:getCellBounds(iFst, iLst);
        if (r != null) scrollRectToVisible(r);

    }
    /* <<< Scroll <<< */
    /* ---------------------------------------- */
    /* >>> preferredSize >>> */
    @Override public Dimension getPreferredSize() {
        final Dimension d=prefSze(this);
        try {
            return d!=null ? d : super.getPreferredSize();
        } catch(Exception e) { return dim(EM,EX);}
    }
    /* <<< preferredSize <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    {pcp(KEY_NOT_YET_PAINTED,"",this);}
    @Override public void paint(Graphics g) { try {super.paint(g);} catch(Throwable e) {} };
    @Override public void paintComponent(Graphics g) {
        if (!_isInit) {
            _isInit=true;
            pcp(KEY_NOT_YET_PAINTED,null,this);
            pcp(KEY_IGNORE_SELECTION_EVT,null,this);
            adSelListener(this);
        }

        if (paintHooks(this, g,false)) {
            if (!isEnabled()) g.clearRect(0,0,Short.MAX_VALUE,Short.MAX_VALUE);
            else {
                super.paintComponent(g);
                final String txt;
                if (sze(getModel())==0 && null!=(txt=toStrg(gcpa(KEY_IF_EMPTY,this)))) {
                    g.setColor(C(0x888888));
                    int y= charA(g)+EX/2;
                    for(String s:splitStrg(txt,'\n')) { g.drawString(s,EM,y); y+=charH(g)+1; }
                }
                if (!paintHooks(this, g,true)) return;
            }
        }
    }
    public String toString() { return toStrg(new BA(0).join(getSelectedValues())); }
    /* <<<  Paint <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    private DialogStringMatch _stringMatch;
    //ListSelectionListener
    public ChJList li(ActionListener l) {
        addActLi(l, this);
        return this;
    }
    { this.enableEvents(ENABLE_EVT_MASK);}
    private int _iMouse;
    @Override public void processEvent(AWTEvent ev) {
        if (!shouldProcessEvt(ev)) return;
        final int kcode=keyCode(ev), id=ev.getID(), modi=modifrs(ev);
        String cmd=null;
        if (isShrtCut(ev) && 0==(modi&SHIFT_MASK)) {
            if (kcode=='A') selectAllItems(this);
            if (kcode=='F') {
                if (_stringMatch==null) _stringMatch=new DialogStringMatch(0L,"fileChooser");
                _stringMatch.setListComponent(new Object[]{this}).showInFrame();
                return;
            }
        }
        if ( 0!=(_opt&ChJTable.FILE_TRANSFER_HANDLER) && jListDoDnD(ev)) return;
        final Point point=point(ev);
        final int idx=point==null?-1:locationToIndex(point);
        if (idx>=0) _iMouse=idx;
        if ((kcode==KeyEvent.VK_BACK_SPACE || kcode==KeyEvent.VK_DELETE) && id==KeyEvent.KEY_PRESSED) {
            if (0==(_opt&ChJTable.CHJTABLE_BACKSPACE_DEACTIVATED)) cmd=ACTION_DELETE_OR_BACKSPACE;
            if (0!=(_opt&ChJTable.BACKSPACE_DEL_ITEM) && null!=remov(_iMouse, getList())) revalAndRepaintCs(this);
        }

        if (idx>=0 && isPopupTrggr(true,ev) && gcp(KEY_NO_HELP_ON_RIGHT_CLICK,this)==null) {
            final Object o=get(idx,this);
            if (_popup==null) {
                _miCtrl=new ChButton("Shared control panel").li(evAdapt(this));
                _miHelp=new ChButton("Help").li(evAdapt(this));
                _popup=jPopupMenu(0, "list/context-menu",new Object[]{_miHelp,_miCtrl});
            }
            boolean showPopup=false;

            if (isClassOrPC(o)) {
                final Object cp=sharedCtrlPnl(o,true);
                if (cp!=null) {
                    pcp("O",cp, _miCtrl.enabled(showPopup=true));
                    pcp("C",o,_miCtrl);
                }
            } else _miCtrl.enabled(false);
            if (getHlp(o)!=null) pcp("C",o,_miHelp.enabled(showPopup=true));
            else _miHelp.enabled(false);
            if (showPopup) {
                shwPopupMenu(_popup);
                return;
            }
        }
        if (doubleClck(ev)) handleActEvt(this, ACTION_CLICKED_DOUBLE ,modi);
        if (id==MouseEvent.MOUSE_CLICKED) handleActEvt(this, ACTION_CLICKED ,modi);
        if (cmd!=null) handleActEvt(this,cmd,0);
        super.processEvent(ctrl2apple(ev));
        if (point!=null) {
            if (doubleClck(ev)) handleActEvt(this, ACTION_CLICKED_DOUBLE_AFTER_SUPER ,modi);
            if (id==MouseEvent.MOUSE_CLICKED) handleActEvt(this, ACTION_CLICKED_AFTER_SUPER ,modi);
        }
    }
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final int id=ev.getID();
        if (id==ActionEvent.ACTION_PERFORMED) {
            final Object para=gcp("O",q), clazz=gcp("C",q);
            if (q==_miHelp) showHelp(clazz);
            if (q==_miCtrl) ChFrame.frame(shrtClasNam(clazz),para,ChFrame.SCROLLPANE|ChFrame.PACK).shw();
            if (cmd==ACTION_SELECTION_CHANGED && gcp(KOPT_ALREADY_PAINTED,this)!=null) {
                _selMC++;
                final String cmd2=gcps(KEY_ACTION_COMMAND,this);
                handleActEvt(this,cmd2!=null?cmd2:ACTION_SELECTION_CHANGED,0);
                updateEnabled(gcp(KEY_UPDATE_ENABLED,this));
                enableIfSelected(this);
            }
        }
    }

    static void enableIfSelected(ChJList jl) {
        final boolean selectd=jl!=null && jl.getSelectedIndex()>=0;
        final Object buttons=gcp(KEY_ENABLE_IF_SELECTED,jl);
        for(int i=szeVA(buttons); --i>=-1;) {
            setEnabld(selectd, deref(i<0?buttons:get(i,buttons)));
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    @Override public String getToolTipText(MouseEvent ev) {
        return ballonMsg(ev,super.getToolTipText());
    }
    /* <<< Tip  <<< */
    /* ---------------------------------------- */
    /* >>> Frame >>> */
    public ChFrame showInFrame(long option, String title) {
        return ChFrame.frame(title, this, ChFrame.SCROLLPANE|option).shw(option|ChFrame.PACK_SMALLER_SCREEN);
    }
    /* <<< Frame <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */
    public Object getDndDateien() { return dndV(false,this);}

    /* <<< DnD <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    public static long getOptions(Object o) {
        return o instanceof ChJTable ? ((ChJTable)o).getOptions() :
            o instanceof ChJList ? ((ChJList)o)._opt :
            0;
    }

}
