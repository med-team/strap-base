package charite.christo;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.event.*;
import static java.awt.event.WindowEvent.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
   addComponentListener addContainerListener
*/
public class ChFrame extends JFrame implements ProcessEv {
    {
        assert CLOSE_MASK<(1<<11);
    }
    public final static long
        ALWAYS_ON_TOP=1<<11, TO_FRONT=1<<12,
        CENTER=1<<14, AT_CLICK=1<<15, STAGGER=1<<16, TO_ICON=1<<17,
        PACK=1<<19, SYSTEM_EXIT_WHEN_ALL_CLOSED=1<<21,
        ICONIFY_ON_CLOSE=1<<22, DISPOSE_ON_CLOSE=1<<23,  SHUT_DOWN_ON_CLOSE=1<<24,
        SHOW_IF_NOT_ICONIFIED=1L<<30, SHOW_IF_VISIBLE=1L<<31, DRAG=1L<<33, PACK_SMALLER_SCREEN=1L<<34,
        SCROLLPANE=1L<<35, SCROLL_TOP=(1L<<36)|SCROLLPANE, SCROLL_END=(1L<<37)|SCROLLPANE, CAN_FULLSCREEN=1L<<38;

    private final static List _vFrames=new Vector();
    private final static Map<Object,ChFrame> _mapContentFrame=new WeakHashMap();
    private static Object _lastFrame, _mainFrame, _child;
    private static int _startCount=MIN_INT;
    private static long _optsForAll, _whenShw;
    private static ChFrame _inst;
    private boolean _disposed, _shown, _imgExcept;
    private long _opts;
    static ChFrame instance() { if (_inst==null) _inst=new ChFrame(null); return _inst; }
    public ChFrame(String title) {
        t(title);
        if (!_imgExcept)  { final Image im=img(IC_STRAP); if (im!=null) setIconImage(im); }
        size(366,366);
        //try {revalAndRepaintC(getContentPane());} catch(Exception e){}
        _vFrames.add(wref(this));
        evAdapt(this).addTo("c",this);
        evLstnr(KLI_SHIFT_TIP).addTo("k", this);
        if (!withGui() && myComputer()) { stckTrc(); putln(RED_WARNING, "new ChFrame",title); }
    }
    public ChFrame t(String s) { setTitle(toStrg(new BA(99).a('(').a(startCount()).a(')').a(' ').aRplc(0,"<br>"," ",s))); return this;}
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Main Frame>>> */
    public static ChFrame mainFrame() { return deref(_mainFrame,ChFrame.class);}
    public static ChFrame anyFrame() {
        ChFrame f=mainFrame();
        if (f==null || !f.isShowing()) {
            for(int i=sze(_vFrames); --i>=0;) {
                f=get(i,_vFrames, ChFrame.class);
                if (f!=null && f.isShowing()) break;
            }
        }
        return f;
    }
    public static void setMainFrame(ChFrame f) { _mainFrame=wref(f);}
    public final ChFrame ad(Object obj) {
        final Component c=getPnl(obj);
        if (c instanceof Container) setContentPane((Container)c);
        else if (c!=null) getContentPane().add(c);
        requestFocus();
        return this;
    }
    static void repaintAll() { repaintC(_vFrames); }

    /* <<< ad <<< */
    /* ---------------------------------------- */
    /* >>> Icon  >>> */
    public final ChFrame i(Object ic) {
        final Image im=img(ic);
        if (im!=null && !_imgExcept)  setIconImage(im);
        return this;
    }
    @Override public void show() {
        try { super.show(); } catch(Exception e) {
            setIconImage(null);
            _imgExcept=true;
            super.show();
        }
    }
    /* <<< Icon <<< */
    /* ---------------------------------------- */
    /* >>> Show  >>> */
    @Override public void setVisible(boolean b) {
        try { super.setVisible(b); } catch(Exception e) {
            setIconImage(null);
            super.setVisible(b);
            _imgExcept=true;
        }
    }

    public Runnable thread_shw(long options) {
        return thrdM("shw",this, new Object[]{longObjct(options)});
    }
    public ChFrame shw() { return shw(AT_CLICK); }
    public ChFrame shw(long opt) {
        if (0!=(opt&CAN_FULLSCREEN)) {
            if (isSystProprty(IS_MAC7)) invokeMthd(0L, "setWindowCanFullScreen", "com.apple.eawt.FullScreenUtilities", new Object[]{this, boolObjct(true)});
        }

        if (!withGui() && myComputer()) { stckTrc(); putln(RED_ERROR, "ChFrame.shw",getTitle()); }
        if (!isEDT()) {
            inEdtLater(thread_shw(opt));
            return this;
        }
        _opts=opt;
        if ( (opt&DRAG)!=0) setDragMovesWindow(true, this);
        if ( (opt&SHOW_IF_NOT_ICONIFIED)!=0 && getExtendedState()==JFrame.ICONIFIED) return this;
        if ( (opt&SHOW_IF_VISIBLE)!=0 && !isVisible()) return this;
        final boolean alwaysOnTop=isAot(this);
        setAot(true,this);
        applyOptions(opt, this);
        setVisible(_shown=true);
        setExtendedState(NORMAL);
        _lastFrame=wref(this);
        final String title=getTitle();
        if (sze(title)>0 && (opt&TO_FRONT)!=0) setWndwStateLaterT('F', 999, title);
        {
            final Runnable t=thread_setWndwState('F',this);
            t.run();
            runAftrMS(11,t);
        }
        setAOT(alwaysOnTop,this);
        applyOptions(opt, this);
         _whenShw=System.currentTimeMillis();
         if ((opt&PACK)!=0) pack(); /* Better again */
        return this;
    }
    /* <<< Show <<< */
    /* ---------------------------------------- */
    /* >>> Geometry >>> */
    public static void setGeometry(Rectangle r, Window f) {
        if (f==null || r==null) return;
        final Rectangle screen=f.getGraphicsConfiguration().getBounds();
        if (screen==null) {
            f.setBounds(r);
            return;
        }
        final int x=x(r)>=0?x(r) : x2(screen) -r.width +x(r);
        final int y=y(r)>=0?y(r) : y2(screen) -r.height+y(r);
        f.setBounds(x,y, r.width,r.height);
    }
    public static boolean parseGeometry(String s, Rectangle r) {
        if (s==null) return false;
        int x=strchr('x',s);
        if (x<0) x=strchr('X',s);
        if (x<0 || !cntainsOnly(DIGT,s,0,x) || !is(DIGT,s,x+1)) return false;
        final int offsetX=intEndsAt(s,x+1), offsetY=intEndsAt(s,offsetX);
        setRect(atoi(s,offsetX), atoi(s,offsetY), atoi(s), atoi(s,x+1), r);
        return true;
    }
    /* <<< Geometry <<< */
    /* ---------------------------------------- */
    /* >>> Dispose >>> */
    public boolean isDisposed() { return _disposed;}
    public void superDispose() { super.dispose();}
    @Override public void dispose() {
        if (_disposed) return;
        _disposed=true;
        for(int j=2; --j>=0;) {
            final Object cp=j==0?deref(_child) : getContentPane();
            if (0!=closeOpts(cp)) clos(closeOpts(cp),cp);
        }
        super.dispose();

        if ( ((_optsForAll|_opts)&SYSTEM_EXIT_WHEN_ALL_CLOSED)!=0) {
            for(int i=sze(_vFrames); --i>=0;) {
                final ChFrame f=get(i,_vFrames,ChFrame.class);
                if (f==null || f._disposed) _vFrames.remove(i);
            }
            if (sze(_vFrames)==0) shutDwn(33333);
        }
        for(Map.Entry<Object,ChFrame> e :  entryArry(_mapContentFrame)) {
            if (e.getValue()==this) _mapContentFrame.remove(e.getKey());
        }
    }
    /* <<< Dispose <<< */
    /* ---------------------------------------- */
    /* >>> Options >>> */
    public static void setOptionsForAll(long opt) { _optsForAll=opt;}
    public static ChFrame frame(String title,  Object object,  long opt) {
        final Object o=gcp(KEY_PANEL, object)!=null ? gcp(KEY_PANEL,object) : object;
        ChFrame f=_mapContentFrame.get(o);
        _child=object;
        if (f==null || f.isDisposed()) {
            final ChTextComponents tools=ChTextComponents.tools(o);
            final Component jc=deref(o,Component.class);
            final ChJScrollPane osp=tools!=null ? getSP(jc) : null;
            final Object cont= osp!=null ? osp :
                jc!=null && (opt&SCROLLPANE)!=0 && child(jc,JScrollPane.class)==null ?
                scrllpn(SCRLLPN_INHERIT_SIZE |   ((opt&SCROLL_TOP)==SCROLL_TOP?SCRLLPN_TOP:0) |   ((opt&SCROLL_END)==SCROLL_END?SCRLLPN_LOCK_BOTTOM:0),  jc) : o,
                pNorth=gcp(KEY_NORTH_PANEL,o),
                pSouth=gcp(KEY_SOUTH_PANEL,o),
                pEast=gcp(KEY_EAST_PANEL,o),
                pWest=gcp(KEY_WEST_PANEL,o);

            f=new ChFrame(title).ad(pNorth!=null||pSouth!=null||pWest!=null||pEast!=null?pnl(CNSEW,cont, pNorth,pSouth, pEast, pWest) : cont);
            f.applyOptions(opt, jc);
            _mapContentFrame.put(o,f);
            f._opts=opt;
            pcp(KEY_FRAME,wref(f), o);
        }
        return f;
    }

    private void applyOptions(long opt, Component child) {
        if ((opt&CLOSE_MASK)!=0) closeOnKey((int)((opt&CLOSE_MASK)|CLOSE_CHILDS), child!=null?child:this, this);
        final Rectangle screen=getGraphicsConfiguration().getBounds();
        final int sw=screen.width, sh=screen.height;
        if ((opt&ALWAYS_ON_TOP)!=0) setAOT(true,this);

        if ((opt&PACK)!=0) pack();
        if (0!=(opt&PACK_SMALLER_SCREEN)) {
            pack();
            if (getWidth()>sw || getHeight()>sh) {
                setSize(mini(getWidth(),sw-4*EX), mini(getHeight(),sh-4*EX));
                setLocationFit(x(this), y(this), this);
            }
        }
        if ((opt&TO_ICON)!=0) setExtendedState(ICONIFIED);
        final Object lastF=deref(_lastFrame);
        final boolean atClick=
            (opt&AT_CLICK)!=0 ||
            (opt&STAGGER)!=0 && (lastF==null || screen==null || System.currentTimeMillis()-_whenShw>999);
        if (atClick) setLocationAtMouse(this);
        else if ((opt&STAGGER)!=0 && !_shown) {
            int x=x(lastF), y=y(lastF);
            if (x>x(screen)+sw *3/4) x=x(screen)+sw/10;
            if (y>y(screen)+sh *3/4) y=y(screen)+sh/10;
            setLocation(x+20, y+40);
        }
        else if ((opt&CENTER)!=0) setLocationRelativeTo(mainFrame());
    }
    /* <<< Options <<< */
    /* ---------------------------------------- */
    /* >>> Size and Location  >>> */
    public final ChFrame size(int w,int h) { setSize(maxi(1,w),maxi(1,h)); return this;}
    public final ChFrame relativeSize(double x,double y) {
        final Rectangle screen=getGraphicsConfiguration().getBounds();
        size(maxi(640,(int)(screen.width*x)),maxi(400,(int)(screen.height*y)));
        return this;
    }
    public static void setLocationAtMouse(Window win) {
        final Point p=win==null || dtkt()==null ? null : mouseLctn();
        if (p!=null) setLocationFit(x(p)-3,y(p)-3, win);
    }

    public static void setLocationFit(int x0, int y0, Window win) {
        if (win==null || dtkt()==null) return;
        //final Rectangle screen=win.getGraphicsConfiguration().getBounds();
        final Dimension dim=dtkt().getScreenSize();
        final int
            w=win.getWidth(),
            h=win.getHeight(),
            x=maxi(0, mini(x0, dim.width  /*x2(screen)*/-w)),
            y=maxi(0, mini(y0, dim.height /*y2(screen)*/-h));
        win.setLocation(x,y);
    }
    /* <<< Size and Location <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent  >>> */
    @Override public void processWindowEvent(WindowEvent e) {
        if (shouldProcessEvt(e)) {
            final int id=e.getID();
            String evt=null;

            if (id==WINDOW_DEACTIVATED) evt=ACTION_WINDOW_DEACTIVATED;
            if (id==WINDOW_ACTIVATED && javaVsn()>14) {
                evt=ACTION_WINDOW_ACTIVATED;
            }
            if (id==WINDOW_CLOSING) {
                setVisible(false);
                if ( (_opts&DISPOSE_ON_CLOSE)!=0) this.dispose();
                if ( (_opts&SHUT_DOWN_ON_CLOSE)!=0)  shutDwn(33333);
                evt=ACTION_WINDOW_CLOSING;
                if ( (_opts&ICONIFY_ON_CLOSE)!=0) { show(); setExtendedState(ICONIFIED); }
            }  else super.processWindowEvent(e);
            if (evt!=null) handleActEvt(this,evt,0);
        }
    }
    @Override public void processEv(AWTEvent ev) {
        if (shouldProcessEvt(ev)) {
            final Object q=ev.getSource();
            final int id=ev.getID();
            if (id==ComponentEvent.COMPONENT_SHOWN && q==this) requestFocus();
        }
        //http://stackoverflow.com/questions/4438390/setting-the-application-focus-to-a-java-program-in-ubuntu-ltsp

    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
    static Component imageObserver() {
        if (_vFrames==null) return null;
        for(int i=sze(_vFrames); --i>=0;) {
            final Component c=get(i,_vFrames,Component.class);
            if (c!=null && c.isVisible()) return c;
        }
        return null;
    }
    public static int startCount() {
        if (_startCount==MIN_INT) {
            final java.io.File f=file("~/@/startCount.txt");
            wrte(f,toStrg(1+(_startCount=atoi(readBytes(f)))));
        }
        return _startCount;

    }
}
