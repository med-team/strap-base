package charite.christo;
import java.io.*;
import java.util.*;
import java.lang.ref.Reference;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
Some computations  such as BLAST searches or 3D superpositions take long time.
Results are stored on HD in the directory ~/.StrapAlign/cached/

*/
public class CacheResult implements ChRunnable {
    private Object _archive;
    private final File _file, _fileHeader;
    private final Map<String, Object> mapStringBytesGet=new HashMap();
    private CacheResult(Class cacheClass,String section) {
        _file=getFile(cacheClass,section);
        _fileHeader=_file!=null ? new File(_file.getAbsolutePath()+".entries") : null;
    }
    private Archive archive() {
        Archive archive=(Archive)deref(_archive);
        if (archive==null) _archive=newSoftRef(archive=new Archive(_file,_fileHeader));
        return archive;
    }
   @Override protected void finalize() throws Throwable {
        logFinalize(ANSI_RED+ANSI_FG_WHITE+"CacheResult","");
        final Archive archive=(Archive)deref(_archive);
        if (archive!=null) archive.dispose();
        _archive=null;
        super.finalize();
    }

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Save >>> */
    private final static Object SYNC_SAVE=new Object();
    public static void save() {
        if (myComputer()) putln(ANSI_GREEN+"CacheResult.save"+ANSI_RESET+"\n");
        synchronized(SYNC_SAVE) {
            for (Map<String,Reference<CacheResult>> mmm : toArry(MAP.values(),Map.class)) {
                for(Object ref : valueArry(mmm)) {
                    final CacheResult c=deref(ref,CacheResult.class);
                    final Archive archive=c!=null ? (Archive)deref(c._archive) : null;
                    if (archive!=null) {
                        archive.dispose();
                        archive.close();
                    }
                }
            }
        }
    }
    public static void saveAndClear() { save(); MAP.clear(); }
    static { addShutdownHook1( thrdCR(new CacheResult(null,null),"EXIT")); }
    private static boolean alreadyExited;
    public Object run(String id, Object arg) {
        if (id=="EXIT") {
            if (!alreadyExited) {
                alreadyExited=true;
                save();
            }
        }
        return null;
    }
    private final static Object SYNC_SAVE_K=new Object();
    private void saveK(String key, BA ba) {
        synchronized(SYNC_SAVE_K) {
            final Archive archive=archive();
            archive.write(key,ba);
        }
    }
    /* <<< Save <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    private static Map<String,Reference<CacheResult>> map(Class c) {
        Map<String,Reference<CacheResult>>  m=MAP.get(c);
        if (m==null) MAP.put(c,m=new TreeMap());
        return m;
    }
    private final static Map<Class,Map<String,Reference<CacheResult>>> MAP=new HashMap();
    private static File dir;
    public static File dir() {
        if (dir==null) mkdrsErr(dir=file("~/@/cached/"),"In this directory computational results are cached.");
        return dir;
    }

    private final static Set<String> vMkDir=new HashSet();
    private final static Object GETFILE[]={null};
    static File getFile(Class clas, String section) {
        if (clas==null) return null;
        final String cn=clas.getName();
        final File f;
        synchronized(GETFILE) {
            f= file(baClr(GETFILE).a(dir()).a('/').a(cn,1+cn.lastIndexOf('.'),MAX_INT).a('/').a(section));
        }
        final String parent=f.getParent();
        if (vMkDir.add(parent)) mkdrsErr(file(parent));
        return f;
    }
    private static CacheResult getCacheResult(Class clas,String section) {
        if (clas==null||section==null) return null;
        final Map m=map(clas);
        if (m==null || section==null) return null;
        CacheResult prop=getV(section,m,CacheResult.class);
        if (prop==null) {
            final File f=CacheResult.getFile(clas,section);
            if (sze(f)>0) {
                m.put(section, newSoftRef(prop=new CacheResult(clas,section)));
            }
        }
        return prop;
    }

    public static void deleteCacheResult(Class clas,String section) {
        if (clas==null||section==null) return;
        final Map<String,Reference<CacheResult>> m=map(clas);
        if (m!=null) {
            final CacheResult c=getV(section,m,CacheResult.class);
            m.remove(section);
            final Archive a=c!=null ? (Archive)deref(c._archive) : null;
            if (a!=null) a.deleteBecauseError();
        }

    }

    /** Get the cached text */
    public static BA getValue(int opt, Class clas, String section, String key, BA buffer) {
        final CacheResultJdbc jdbc=CacheResultJdbc.instance();
        if (key==null || section==null) return null;
        final CacheResult prop=getCacheResult(clas, section);
        BA ba=null;
        if (prop!=null) {
            ba=deref(prop.mapStringBytesGet.get(key), BA.class);
            if (sze(ba)==0) ba=prop.archive().get(key,buffer);
        }
        if (ba==null && jdbc.init()) {
            final BA val=jdbc.get(shrtClasNam(clas),section,key);
            //if (myComputer() && val!=null) putln(YELLOW_DEBUG+" Gotten via JDBC ");
            if (val!=null) return val;
        }
        return ba;
    }

    public final static int OVERWRITE=1<<0, NO_SQL=1<<1, SOFTREF=1<<2;
    public static synchronized void putValue(int opt, Class clas, String section, String key, BA value) {
        if (!_storing || clas==null || section==null || key==null) return;
        final CacheResultJdbc jdbc=CacheResultJdbc.instance();
        if (0==(opt&NO_SQL) && jdbc.init()) {
            value.a1('\n');
            if (jdbc.put(shrtClasNam(clas),section,key, value.newBytes(), hashCd(value))) {
                //if (myComputer()) putln(YELLOW_DEBUG+" Added via JDBC ");
                return;
            }
        }

        CacheResult prop=getCacheResult(clas,section);
        final Map mmm=map(clas);
        if (prop==null) {
            prop=new CacheResult(clas,section);
            mmm.put(section,newSoftRef(prop));
        }
        prop.mapStringBytesGet.put(key, (value));
        if (0!=(opt&OVERWRITE) || !prop.archive().contains(key)) prop.saveK(key,value);
    }

       /**
       When was the Result that had been retrieved recently modified?
       This is not thread save !
    */
    public static long getLastModified(Class clas, String section, String key) {
        if (key==null || section==null) return 0;
        final CacheResult prop=getCacheResult(clas, section);
        return prop!=null ? prop.archive().getLastModified(key) : 0;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Static methods  >>> */
    private static boolean _enabled=true, _storing=true;
    public static boolean isEnabled() { return _enabled;}
    public static void setEnabled(boolean b) { _enabled=b; }

    public static void keyForSequences(byte ss[][], BA sb) {
        if (ss!=null) {
            for(byte[] seq:ss) {
                for(int i=0; i<8 && i<seq.length; i++) sb.a((char)seq[i]);
                sb.a(hashCd(seq,0,MAX_INT,true)).a('_');
            }
        }
    }
    public static void sectionForSequence(Object s, BA ba) {
        final int L=sze(s);
        if (L==0) return;
        ba.a(s,0,16).a('-').a(s,maxi(L-16,16),L).a('-').a(hashCd(s,0,99999,true));
    }
    /*    private static Object newSoftRef(Object o) { return new java.lang.ref.WeakReference(o); } */
}
