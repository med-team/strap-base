package charite.christo;
import java.io.File;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.KeyEvent.*;

public class ChTextView extends ChPanel implements ChRunnable {
    public final static String KOPT_SEARCH_SKIP_BLANKS="KB$$B";
    private final static int _lineSkip=1;
    final int _regionFT[]={0,0};

    private int
        _mc, _lineNum=MIN_INT,
        _maxWidth, _posClicked=-1,
        _regionFrom, _regionTo;

    private Object _refText;
    private BA _ba;
    private final TextMatches _region=new TextMatches(HIGHLIGHT_NOT_IN_SCROLLBAR|HIGHLIGHT_NEVER_REMOVE, _regionFT, 1, C(0x0000ff,0x30));
    public void setSelection(int f,int t) {
        _regionFT[0]=f;
        _regionFT[1]=t;
        _region.setFromTo(_regionFT,1);
    }

    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    public ChTextView(File f) { _file=f;}

    public ChTextView(CharSequence ba) {
        super();
        setText(ba);
        if (ba instanceof BA) ((BA)ba).setSendTo(this);
        setBackground(C(0xFFffFF));
    }
    public ChTextView(int size) { this(new BA(size).setFilterOptions(FILTER_BACKSPACE));}
    {
        setAutoscr(this);
        monospc(this);
        tools().addHighlight(_region);
    }

    private ChTextComponents _tools;
    public ChTextComponents tools() { if (_tools==null) _tools=new ChTextComponents(this); return _tools;}
    public ChTextView bg(Color bg) {
        if (bg==null) setOpaque(false);
        else setBackground(bg);
        return this;
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> setText >>> */
    private long _baMc;
    private void mayRevalidate() {
        if (gcp(KOPT_ALREADY_PAINTED,this)==null) return;
        final BA ba=byteArray();
        final long mc=ba.mc()|((long)_mc<<24)|((long)sze(ba)<<40);
        if (_baMc!=mc) {
            _baMc=mc;
            ChDelay.revalidate(this,222);
            _maxWidth=0;
        }
    }

    @Override public ChTextView t(CharSequence cs) {
        byteArray().clr().a(cs);
        _mc++;
        mayRevalidate();
        repaint();
        return this;
    }

    public ChTextView setText(CharSequence cs) {
        final BA ba=_ba=toBA(cs);
        if (ba==null) return this;
        _mc++;
        mayRevalidate();
        repaint();
        return this;
    }

    public synchronized BA byteArray() {
        if (_ba!=null) return _ba;
        BA ba=(BA)deref(_refText);
        if (ba==null) {
            _refText=newSoftRef(ba=readBytes(_file));
            if (ba!=null) tools().underlineRefs(ULREFS_BG);
            else ba=new BA(NO_BYTE).setFilterOptions(FILTER_BACKSPACE);
        }
        return ba;
    }
    private File _file;
    public File getFile() { return _file;}
    public void setFile(File f) { _file=f; _ba=null; _refText=null; ChDelay.repaint(this,333); }

    /* <<< setText  <<< */
    /* ---------------------------------------- */
    /* >>> _lineNum  >>> */
    private int offsetX() { return _lineNum==MIN_INT?0 :  7*charW(this);}
    public ChTextView setLineNumberStart(int b) { _lineNum=b; repaint(); return this;}
    /* <<< _lineNum <<< */
    /* ---------------------------------------- */
    /* >>> Size >>> */
    private int _gpsMC;
    @Override public Dimension getPreferredSize() {
        final Dimension ps=prefSze(this);
        if (ps!=null) return ps;
        final BA text=byteArray();
        if (text==null) return dim(1,1);
        final File f=_file;
        final int mc=text.mc() + (f!=null ? (int)(f.lastModified()/1024) : 0);
        final int ends[]=text.eol();
        if (_maxWidth==0 || _gpsMC!=mc) {
            _gpsMC=mc;
            int begin=0;
            for(int i=0; i<ends.length; i++) {
                final int e=ends[i];
                if (e-begin>_maxWidth) _maxWidth=e-begin;
                begin=e;
            }
        }
        return dim(maxi(1,charW(this)*_maxWidth+offsetX()), maxi(1,getFixedRowHeight()*ends.length));
    }
    public int getFixedRowHeight() { return charH(this)+_lineSkip;}
    /* <<< Size  <<< */
    /* ---------------------------------------- */
    /* >>> Point >>> */
    public void modelToView(int iPosition, Rectangle r) {
        final BA text=byteArray();
        final int[] ends=text.eol();
        for(int i=0;i<ends.length; i++) {
            if (ends[i]>iPosition) {
                final int
                    row=i,
                    begin=i==0 ? 0 : ends[i-1]+1,
                    col=iPosition-begin,
                    charW=charW(this), charH=charH(this);
                r.setBounds(col*charW+offsetX(), row*(_lineSkip+charH), charW,charH);
                return;
            }
        }
        r.setBounds(getWidth(), getHeight(), 0,0);
    }
    public int viewToModel(int x, int y) {
        final BA ba=byteArray();
        if (y<0 || ba==null) return 0;
        final int ends[]=ba.eol();
        final int row=y/maxi(1,getFixedRowHeight());
        if (row>=ends.length) return ba.end();
        final int lineStart=row<=0 ? 0 : ends[row-1]+1;
        return mini(ends[row],lineStart+(maxi(0,x-offsetX()))/charW(this));
    }
    /* <<< Point <<< */
    /* ---------------------------------------- */
    /* >>> paint >>> */
    private static AnsiEscape _att;
    @Override public void paintComponent(Graphics g) {
        mayRevalidate();
        final Color background,forground;
        {
            final Color bg=getBackground(), fg=getForeground();
            forground=fg!=null ? fg : C(0);
            background=!isOpaque()?null: bg!=null ? bg : C(0xFFffFF);
        }
        if (_att==null) _att=new AnsiEscape(); else _att.reset();

        final Rectangle clip=clipBnds(g, getWidth(), getHeight());
        final int clipX1=x(clip),clipY1=y(clip), clipX2=x2(clip), clipY2=y2(clip);

        if (isOpaque()) g.clearRect(clipX1,clipY1, clipX2-clipX1, clipY2-clipY1);
        tools().paintHook(this,g,false);
        final String strgLen1[]=strgsOfLen1();
        final BA text=byteArray();
        if (text==null) return;
        final long[] escape=text.getAnsiEscapes();
        final int escapeL=text.countAnsiEscapes();
        final byte T[]=text.bytes();
        final int ends[]=text.eol();
        final int fontSize=g.getFont().getSize();
        final Font font0=getFnt(fontSize,true, 0), fontBf=getFnt(fontSize,true, Font.BOLD);
        final int charH=charH(font0), charW=charW(font0), charA=charA(font0);
        final int xOff=offsetX();
        final int firstRow=maxi(0,clipY1/(charH+_lineSkip)), lastRow=(clipY2-1)/(charH+_lineSkip)+1;
        final int firstCol=maxi(0, (clipX1-xOff)/charW), lastCol=(clipX2-1)/charW+1;
        final int maxRow=mini(ends.length, lastRow+1);
        int escI=0, escPos=0;
        if (firstRow>0 && firstRow-1<ends.length) { /* Search ANSI_RESET */
            final int maxPos=ends[firstRow-1]+1;
            for(int i=0; i<escapeL; i++) {
                final int pos=(int)(escape[i]&AnsiEscape.MASK_POS);
                if (pos>maxPos) break;
                if ( 0==(escape[i]&(255L<<AnsiEscape.SHIFT_CODE))) {
                    escPos=pos;
                    escI=i;
                }
            }
        }
        Color fg=null, bg=null;
        Font font=font0;
        for(int row=firstRow; row<maxRow; row++) {
            final int begin=row==0 ? text.begin(): ends[row-1]+1;
            final int y=row*(charH+_lineSkip);
            final int to=mini(ends[row],begin+lastCol);
            final int from=mini(to,begin+firstCol);
            for(int i=from, x=firstCol*charW+xOff; i<to; i++,x+=charW ) {
                if (i>=escPos) {
                    boolean changed=false;
                    escPos=MAX_INT;
                    for(; escI<escapeL; escI++) {
                        final long esc=escape[escI];
                        final int pos=(int)(esc&AnsiEscape.MASK_POS);
                        if (pos>i) { escPos=pos; break; }
                        AnsiEscape.inferIntoAttribute(esc,_att);
                        changed=true;
                    }
                    if (changed) {
                        bg=_att.bg();
                        fg=_att.fg();
                    }
                }
                if ( (bg!=background || background==null) && bg!=null) {
                    g.setColor(bg);
                    fillBigRect(g,x,y,charW,charH);
                }
                g.setColor(fg!=null ? fg : forground);
                final Font fontCurrent=(_att.style()&AnsiEscape.BOLD)!=0 ? fontBf:font0;
                if (font!=fontCurrent) g.setFont(font=fontCurrent);
                g.drawString(T[i]=='\t' ? "\u00bb" : strgLen1[127&T[i]],x,y+charA);
                if ((_att.style()&AnsiEscape.UNDERLINE)!=0) g.drawLine(x,y+charH,x+charW, y+charH);
            }
            if (_lineNum!=MIN_INT) {
                g.setColor(C(0xFF));
                final String s=toStrg(row+_lineNum);
                g.drawString(s,  xOff-charW*(s.length()+1),y+charA);
            }
        }
        tools().paintHook(this,g,true);
    }
    /* <<< paint <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    @Override public void processEvent(AWTEvent ev) {
        final BA text=byteArray();
        if (text==null) return;
         final AWTEvent newEv=tools().pEv(ev);
        if (newEv==null) return;
         final int
            id=ev.getID(), modi=modifrs(ev), kcode=keyCode(ev),
            x=x(ev), y=y(ev),
            pos=viewToModel(x,y),
            B=text.begin(), E=text.end();

        final boolean ctrl= isShrtCut(ev), alt=0!=(ALT_MASK&modi), shift=0!=(SHIFT_MASK&modi);
        if (!alt && (!shift && id==MOUSE_DRAGGED || id==MOUSE_PRESSED && shift)) {
            if (_posClicked>=0) _regionFrom=_posClicked;
            _regionTo=pos;
            repaint();
        }
        if (id==MOUSE_PRESSED) {
            requestFocus();
            if (!shift) _posClicked=pos;

        }
        final int f=maxi(B, mini(_regionFrom, _regionTo));
        final int t=mini(E, maxi(_regionFrom, _regionTo));

        if ( (id==MOUSE_RELEASED || ctrl && kcode==VK_C) && t>f) toClipbd(toStrg(text.bytes(),f,t));
        setSelection(f,t);

        if (id==KEY_PRESSED && ctrl) {
            if(kcode==VK_L) setLineNumberStart(_lineNum==MIN_INT?0:MIN_INT);
            if (kcode==VK_A) {
                _regionFrom=0; _regionTo=E;
                repaint();
            }
        }
        super.processEvent(newEv);
        mayRevalidate();

    }
    { this.enableEvents(MOUSE_EVENT_MASK|MOUSE_MOTION_EVENT_MASK|KEY_EVENT_MASK|MOUSE_WHEEL_EVENT_MASK);}
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    @Override public String getToolTipText(java.awt.event.MouseEvent mev) {
        final String tt=(String)tools().run(ChTextComponents.TT, mev);
        return tt!=null ? tt : ballonMsg(mev, super.getToolTipText(mev));
    }
    public Object getDndDateien() { return dndV(false,this); }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Append >>> */
    public ChTextView a(CharSequence o) {
        final BA ba=byteArray();
        if (ba!=null) {
            ba.filter(FILTER_BACKSPACE,o);
            afterAppend();
        }
        return this;
    }
    void afterAppend() {
        if (isVisbl(this)) {
            ChDelay.repaint(this,333);
        }
    }
    /* <<< Append <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id==ChRunnable.RUN_APPEND && arg!=null) {
            if (arg!=byteArray()) a(toBA(arg));
            else afterAppend();
        }
        if (id==ChRunnable.RUN_SHOW_IN_FRAME) return ChFrame.frame("",this, CLOSE_CtrlW|CLOSE_DISPOSE|ChFrame.SCROLLPANE).shw(atol(arg));
        return null;
    }
}
