package charite.christo;
import static charite.christo.ChUtils.*;
import java.util.Set;
import javax.swing.*;

/**
   @author Christoph Gille
*/

public class ChListSelectionModel extends DefaultListSelectionModel {
    private final Object _refM;
    private final Set set=new ChSet(ChSet.WEAK_REFERENCE, Object.class);
    public ChListSelectionModel(Object m) { _refM=newWeakRef(m); }

    public Set getSet() { return set;}
    private ListModel m() {
        final Object m=deref(_refM);
        return
            m instanceof ListModel ? (ListModel)m :
            m instanceof JList ? ((JList)m).getModel() :
            m instanceof JComboBox ? ((JComboBox)m).getModel() :
            null;
    }

    public void fireValChanged() {
        final int L=sze(m());
        if (L>0) super.fireValueChanged(0, maxi(0,L-1));
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Overriden >>> */
    @Override public boolean isSelectedIndex(int index) {
        return getMinMax(index,index)>=0;
    }
    @Override public boolean isSelectionEmpty() {
        return getMinSelectionIndex()<0;
    }
    @Override public int getMaxSelectionIndex() {
        final int L=sze(m());
        return mini(L-1,getMinMax(L-1,0));
    }
    @Override public int getMinSelectionIndex() {
        final int L=sze(m());
        return mini(L-1,getMinMax(0,L-1));
    }
    @Override public void removeSelectionInterval(int from, int to) {
        if (addRemove(mini(from,to), maxi(from,to),false)) fireValChanged();
    }
    @Override public void addSelectionInterval(int from, int to) {
        updateLeadAnchorIndices(from, to);
        if (addRemove(mini(from,to), maxi(from,to),true)) fireValChanged();
    }
    @Override public void setSelectionInterval(int index0, int index1) {
        final int L=sze(m());
        final int mi=mini(index0,index1, L-1);
        final int ma=maxi(index0,index1, 0);
        final boolean changed=L>0 && (addRemove(0,mi-1, false) || addRemove(ma+1,L-1, false) || addRemove(mi,ma, true));
        updateLeadAnchorIndices(index0, index1);
        if (changed) fireValChanged();

    }
    @Override public int getAnchorSelectionIndex() { return _anchorIndex;}
    @Override public int getLeadSelectionIndex() { return _leadIndex; }
    /* <<< Overriden <<< */
    /* ---------------------------------------- */
    /* >>> Lead and anchor >>> */

    private int _anchorIndex, _leadIndex;
    private void updateLeadAnchorIndices(int anchorIndex, int leadIndex) {
        _anchorIndex=anchorIndex;
        _leadIndex=leadIndex;
    }
     /* <<< Lead and anchor <<< */
    /* ---------------------------------------- */
    /* >>> private utils  >>> */

    private boolean addRemove(int from, int to0, boolean addRemove) {
        final ListModel m=m();
        if (m==null) return false;
        final int to=mini(sze(m)-1, to0);
        boolean changed=false;
        for(int i=from; i<=to; i++) {
            final Object el=m.getElementAt(i);
            changed|= addRemove ? set.add(el) :  set.remove(el);
        }
        return changed;
    }

    private int getMinMax(final int from, final int to) {
        final ListModel m=m();
        if (m==null) return -1;
        final int L=sze(m);
        for(int i=from; from<to ? i<=to : i>=to; i+=(from<to?1:-1)) {
            if (i>=0 && i<L && set.contains(m.getElementAt(i))) return i;
        }
        return -1;
    }
    /* <<< private utils <<< */
    /* ---------------------------------------- */
    /* >>> In Superclass >>> */
    @Override public void clearSelection() {
        set.clear();

        super.clearSelection();
    }
    /*
      @Override public void setLeadSelectionIndex(int index) { this.leadIndex=index; }
      @Override public void setAnchorSelectionIndex(int index) { this.anchorIndex=index; }
      @Override public void insertIndexInterval(int index, int length, boolean before) {}
      @Override public void removeIndexInterval(int index0, int index1) {}

      @Override public boolean getValueIsAdjusting() { return false;}
      @Override public void setValueIsAdjusting(boolean valueIsAdjusting) {}
      @Override public void setSelectionMode(int selectionMode) { }
      @Override public int getSelectionMode() {return selectionMode;}
      @Override public void addListSelectionListener(ListSelectionListener x) {}
      @Override public void removeListSelectionListener(ListSelectionListener x){}
    */

}

