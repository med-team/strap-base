
package charite.christo;
public interface HasPrgParas extends NeedsSharedInstance {
    long FURTHER_PARAMETERS=1<<0;
    PrgParas getPrgParas();
    void defineParameters(long opt, String s);
}
