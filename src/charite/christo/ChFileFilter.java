package charite.christo;
import javax.swing.filechooser.FileFilter;
import static charite.christo.ChUtils.*;

public class ChFileFilter extends FileFilter implements java.io.FileFilter {
    private final String _vExt[], _vPfx[], _descr;
    private final boolean _ic, _allowFolders, _onlyFolders, _allowHidden;
    final FileFilter _other[];

    public ChFileFilter(FileFilter exclude[]) {
        _other=exclude;
        _vExt=_vPfx=null;
        _descr="other";
        _ic=_allowFolders=_onlyFolders=_allowHidden=false;
    }

    public ChFileFilter(String opt_suffices_descript) {
        final String
            rule=opt_suffices_descript.trim(),
            fst=fstTkn(rule),
            opts=chrAt(0,fst)=='-'?fst:"";
        final int hash=rule.indexOf('#');
        final String
            descr=hash<0?null:rule.substring(hash+1),
            patterns=delPfx(opts, delSfx(descr,rule));
        _ic=opts.indexOf('i')<0;
        _allowFolders=opts.indexOf('D')<0;
        _onlyFolders=opts.indexOf('d')>0;
        _allowHidden=opts.indexOf('H')<0;
        final String[] dotZip=opts.indexOf('z')>=0 ? COMPRESS_SUFFIX : NO_STRING;

        final String[] ss=splitTokns(patterns);
        _vExt=new String[(dotZip.length+1)*ss.length];
        _vPfx=new String[ss.length];

        for(int i=ss.length, iPfx=0, iSfx=0; --i>=0;) {
            final String s=ss[i];
            if (lstChar(s)=='*') _vPfx[iPfx++]=s.substring(0,sze(s)-1);
            else {
                _vExt[iSfx++]=s;
                for(String gz : dotZip) _vExt[iSfx++]=s+gz;
            }
        }
        _descr=rule;
        _other=null;
    }

    @Override public boolean accept(java.io.File f) {
        if (f==null) return false;
        final boolean isD=f.isDirectory();
        if (_other!=null) {
            for(FileFilter filter : _other) if (filter.accept(f)) return false;
            return true;
        }

        if (isD) return true;
        final String n=f.getName();
        if (_onlyFolders && !isD || !_allowFolders && isD || !_allowHidden && chrAt(0,n)=='.') return false;

        for(String s :_vExt) {
            if (s==null) break;
            if (_ic?endWith(STRSTR_IC, s,n):n.endsWith(s)) return true;
        }
        for(String s :_vPfx) {
            if (s==null) break;
            if (_ic?strEquls(STRSTR_IC, s,n):n.startsWith(s)) return true;
        }
        return false;
    }
    @Override public String getDescription() {
        return _descr;
    }
}
