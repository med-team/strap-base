package charite.christo;
/**
   Command interpreters can interpret commands given as a command line.
   @author Christoph Gille
*/
public interface CommandInterpreter {
    void interpret(long options, String command);
}
