package charite.christo;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;
import java.util.jar.JarFile;
import static charite.christo.ChUtils.*;

/**
 * A utility class for working around the java webstart jar signing/security bug
 *
 * see http://bugs.sun.com/view_bug.do?bug_id=6967414 and http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6805618
 * @author Scott Chan
  */
public class ChJarSignersHardLinker extends Thread {
    /**
     * the 1.6.0 update where this problem first occurred
     */
    private final static List HARDREFS=new Vector();
    private final static boolean LOG=false;
    private static void makeHardSignersRef(JarFile jar) {
        if (LOG) log("Making hard refs for: " + jar );
        if(jar!=null && jar.getClass().getName().equals("com.sun.deploy.cache.CachedJarFile")) {
        	//lets attempt to get at the each of the soft links.
        	//first neet to call the relevant no-arg method to ensure that the soft ref is populated
        	//then we access the private member, resolve the softlink and throw it in a static list.
            final long opts=RFLCT_PRINT_EXCEPTIONS|RFLCT_ASSERT_EXISTS|RFLCT_SET_ACCESSIBLE;
            invokeMthd(opts, "getSigners", jar);
            makeHardLink("signersRef", jar);
            invokeMthd(opts, "getSignerMap", jar);
            makeHardLink("signerMapRef", jar);
            //            invokeMthd(opts, "getCodeSources", jar);
            //            makeHardLink("codeSourcesRef", jar);
            invokeMthd(opts, "getCodeSourceCache", jar);
            makeHardLink("codeSourceCacheRef", jar);
        }
    }

    /**
     * if the specified field for the given instance is a Softreference
     * That soft reference is resolved and the returned ref is stored in a static list,
     * making it a hard link that should never be garbage collected
     * @param fieldName
     * @param instance
     */
    private static void makeHardLink(String fieldName, Object instance) {
        if (LOG) log("attempting hard ref to " + instance.getClass() + "." + fieldName);
        try {
            final Field signersRef=instance.getClass().getDeclaredField(fieldName);
            signersRef.setAccessible(true);
            final Object o=signersRef.get(instance);
            if(o instanceof SoftReference) {
                final Object o2=((SoftReference) o).get();
                if (LOG) { log("Hardreference: "); log(o2); }
                if (adUniq(o2,HARDREFS))  { /*putln("fieldName="+fieldName+"   "+clazz(instance)); */}
                

            } else {
                if (LOG) log("noooo!");
            }
        } catch (Throwable e) { stckTrc(e); }
    }
    /**
     * is the preloader enabled. ie: will the preloader run in the current environment
     * @return
     */
    public static boolean isEnabled() {
        if (!isSystProprty(IS_WEBSTARTED)) return false;
        final int PROBLEM_JRE_UPDATE=19;
        final String JRE_1_6_0="1.6.0_", javaVersion=sysGetProp("java.version");
        if(javaVersion.startsWith(JRE_1_6_0)) {
            return atoi(javaVersion.substring(JRE_1_6_0.length())) >= PROBLEM_JRE_UPDATE;
        }
        return false;
    }
	/**
	 * get all the JarFile objects for all of the jars in the classpath
	 * @return
	 */
	public static JarFile[] getAllJarsFilesInClassPath() {
        final URL uu[]=getAllJarUrls();
        final JarFile jj[]=new JarFile[uu.length];
	    for(int i=0; i<uu.length; i++) {
	        try {
	            if (uu[i]!=null) jj[i]=getJarFile(uu[i]);
	        } catch(IOException e) {
	        	if (LOG) { log("unable to retrieve jar at URL: "); log(uu[i]); }
	        }
	    }
	    return jj;
	}
    /**
     * Returns set of URLS for the jars in the classpath.
     * URLS will have the protocol of jar eg: jar:http://HOST/PATH/JARNAME.jar!/META-INF/MANIFEST.MF
     */
    static URL[] getAllJarUrls() {
        try {
            final List<URL> urls=new Vector();
            final Enumeration<URL> mfUrls=Thread.currentThread().getContextClassLoader().getResources("META-INF/MANIFEST.MF");
            while(mfUrls.hasMoreElements()) {
                final URL jarUrl=mfUrls.nextElement();
                if (LOG) log(jarUrl);
                if(!jarUrl.getProtocol().equals("jar")) continue;
                urls.add(jarUrl);
            }
            return toArryClr(urls,URL.class);
        } catch(IOException e) {
            stckTrc(e);
        }
        return new URL[0];
    }

    public static JarFile getJarFile(URL jarUrl) throws IOException {
        final URLConnection urlC=jarUrl.openConnection();
        if(urlC instanceof JarURLConnection) {
            // Using a JarURLConnection will load the JAR from the cache when using Webstart 1.6
            // In Webstart 1.5, the URL will point to the cached JAR on the local filesystem
            return ((JarURLConnection)urlC).getJarFile();
        }
        log("Expected JarURLConnection!!!");
        stckTrc();
        return null;
    }

    public void run() {
        log("run()");
        try {
            for (JarFile jar : getAllJarsFilesInClassPath()) {
                makeHardSignersRef(jar);
            }
        } catch (Throwable e) {
            if (LOG) log("Problem preloading resources");
            stckTrc(e);
        }
    }
    private static void log(Object s) {
        putln("JAR_SIGNERS_HARDLINKER ",s);
    }

}
