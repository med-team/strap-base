package charite.christo;
/**

Provides a GUI where the user can change the settings for a group of objects.
Return type: java.awt.Component.
See also <i>JAVADOC:HasControlPanel</i> 

@author Christoph Gille
*/
public interface HasSharedControlPanel extends NeedsSharedInstance {
    Object getSharedControlPanel();
}
