package charite.christo;
public interface ChRunnable {
    Object run(String id,Object arg);
    String
        RUN_APPEND="CR$$A",
        RUN_DOWNLOAD_FINISHED="CR$$DF",
        RUN_GET_COLUMN_TITLE="CR$$RCL",
        RUN_GET_ICON="CR$$IC",
        RUN_GET_ITEM_TEXT="CR$$IT",
        RUN_GET_PANEL="CR$$GP",
        RUN_GET_TAB_TEXT="CR$$TAB",
        RUN_GET_TIP_TEXT="CR$$TT",
        RUN_GET_DISSIMILARITY_VALUE="CR$$VD",
        RUN_INTERPRET_LINE="CR$$IL",
        RUN_IS_DISABLED="CR$$ID",
        RUN_IS_DRAG4XY="CR$$IDR",
        RUN_MODIFY_RENDERER_COMPONENT="CR$$MRC",
        RUN_REPAINT_CURSOR="CR$$RCURS",
        RUN_SET_PROGRESS="CR$$PRGRS",
        RUN_SET_ICON_IMAGE="CR$$SII",
        RUN_SET_TREE_VALUE="CR$$SRC",
        RUN_SHOW_IN_FRAME="CR$$SIF",
        RUN_SAY_DOWNLOADING="CR$$logDL";
}
