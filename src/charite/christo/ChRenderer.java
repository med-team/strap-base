package charite.christo;
import charite.christo.hotswap.ProxyClass;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**
   This list cell renderer is used for list components that contain java classes
   @author Christoph Gille
   BasicCheckBoxUI
*/
public class ChRenderer extends javax.swing.border.AbstractBorder
    implements ListCellRenderer, javax.swing.table.TableCellRenderer, javax.swing.tree.TreeCellRenderer,
               TableCellEditor, PaintHook, ProcessEv {
    public final static int
        LAST_PATH_COMPONENT_SLASH=1<<0,  LAST_PATH_COMPONENT_DOT=1<<1 ,CLASS_NAMES=1<<4;
    private final static Object KEY_HAS_CURSOR=new Object(),KEY_REJECTED=new Object(), KEY_COLOR_MARK=new Object();
    public final static String
        KEY_RENDERER_COMPONENT="CCR$$_3", KEY_TOGGLE_SELECTED="CCR$$_4",  KEY_NOT_EDITABLE_BG="CCR$$P",
        KEY_EDITOR_COMPONENT="CCR$$_7",
        KEY_EDITED_ROW="CCR$$_10",
        KEY_EDITED_COL="CCR$$_11",
        KOPT_NO_CHANGE_FG_AND_BG_IF_SELECTED="CCR$$_12",
        KEY_TO_CLOSE="CCR$$CLOSE",
        KOPT_FULL_FILE_PATH="CCR$$FP",
        CMD_FOND_NAME="FONT_NAME";
    private static Color _colors[], _togFillColor;

    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private final EvAdapter LI=new EvAdapter(this);
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);

        if (q==textField() && (cmd==ACTION_ENTER || cmd==ACTION_FOCUS_LOST)) {
            stopCellEditing();
        }
        if (q==_cbEd) {
            final JTable jt=deref(_treeOrList, JTable.class);
            if (jt!=null) {
                jt.setValueAt(boolObjct(_cbEd.isSelected()), _rowEd, _colEd);
            }
        }
        final Object toBeClosed=gcp(KEY_TO_CLOSE,q);
        if (toBeClosed!=null) {
            final boolean in=inButClose(q, x(ev), y(ev));
            if (isSimplClick(ev) && in) clos(0, toBeClosed==REP_PARENT_WINDOW ? parentWndw(q) : toBeClosed);
        }

    }
    public final static boolean inButClose(Object q, int x, int y) {
        final Object X=gcp(KEY_SMALL_X[SMALL_CLOSE],q);
        if (X==null) return false;
        final int xb0=atoi(X);
        final int xb=xb0<0? ((JComponent)q).getWidth()+xb0 : xb0;
        return y<=BUT_WIDTH+1 && xb<=x && x<=xb+BUT_WIDTH+1;
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Instance and options >>> */
    private int _opt;
    private static ChRenderer _inst;
    private Object _origRend;
    private Collection _vSel;
    public ChRenderer setOrigRenderer(Object renderer, Collection vSel) {
        _origRend=renderer;
        _vSel=vSel;
        return this;
    }

    private static ChRenderer instance() {if (_inst==null) _inst=new ChRenderer(); return _inst;}
    public ChRenderer options(long options) { _opt=(int)options; return this; }
    public long options() { return _opt;}
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> JLabels, Buttons ...  >>> */
    private static JLabel _blankLabel;
    public static JComponent blankLabel() {
        if (_blankLabel==null) _blankLabel=new JLabel();
        return _blankLabel;
    }

    private static Color color(int type) {
        if (_colors==null) _colors=new Color[]{C(BG_NOT_EDITABLE),C(0xb6b6b6),C(0xdeC8de),C(0xffFFff,200)};
        return _colors[type];
    }

    private AbstractButton _cbEd;
    private AbstractButton cbEditor() {
        if (_cbEd==null) LI.addTo("a", _cbEd=cbox(""));
        return _cbEd;
    }

    private static AbstractButton _cbMi;
    private static AbstractButton checkbox() {
        if (_cbMi==null) _cbMi=new JCheckBoxMenuItem("");
        return _cbMi;
    }

    private ChButton _label;
    public ChButton label() {
        if (_label==null)  {
            addPaintHook(this, _label=labl());
            monospc(_label.bg(0xFFffFF));

        }
        return _label;
    }

    private ChTextField _tf;
    public final ChTextField textField() {
        if (_tf==null) {
            (_tf=new ChTextField().li(LI)).tools().underlineRefs(ULREFS_GO|ULREFS_NOT_CLICKABLE);
            setBG(0xFFffFF,_tf);
        }
        return _tf;
    }
    /* <<< JLabels, Buttons ...  <<< */
    /* ---------------------------------------- */
    /* >>> Rows and Cols >>> */
    private int _rowEd=-1, _colEd=-1;
    public int editedRow() {return _rowEd;}
    public int editedCol() {return _colEd;}
    public Object _value;
    /* <<< Rows and Cols <<< */
    /* ---------------------------------------- */
    /* >>> CellEditor >>> */
    public boolean isCellEditable(EventObject anEvent){ return true;}
    public void removeCellEditorListener(CellEditorListener l){ _vCellEdLi.remove(wref(l));}
    public boolean shouldSelectCell(EventObject anEvent){return false;}
    public boolean stopCellEditing(){ stopOrCancelCellEditing(true); return true;}
    public void cancelCellEditing(){stopOrCancelCellEditing(false);}
    void stopOrCancelCellEditing( boolean isStopped){
        if (_vCellEdLi!=null) {
            for(int i=sze(_vCellEdLi);--i>=0;) {
                final CellEditorListener lis=get(i,_vCellEdLi,CellEditorListener.class);
                if (lis==null)  continue;
                final ChangeEvent ev=new ChangeEvent(this);
                if (isStopped) lis.editingStopped(ev); else lis.editingCanceled(ev);
            }
        }
    }
    /* <<< CellEditor <<< */
    /* ---------------------------------------- */
    /* >>> Specific methods for JTree, JList ...  >>> */
    Component getComponent() {
        if (_component!=null) return _component;
        final JComponent treeOrList=(JComponent)deref(_treeOrList);
        if (_value instanceof JComponent) {
            final Object c=_value;
            if (_faked==null) _faked=new ChPanel();

            return treeOrList instanceof JTree || c==_faked ? (JComponent)c : _faked.cp(ChPanel.KEY_REPRESENT_COMPONENT,c);
        }
        final ChButton LAB=gcp(KOPT_JUST_WRITING_PNG,treeOrList)!=null ? labl() : label();
        return LAB.t(toStrgN(_value));
    }
    public Component getTreeCellRendererComponent(JTree t, Object valueOrRef,boolean selected, boolean expanded,boolean leaf, int row,boolean focus) {
        process(t, valueOrRef,row,0,selected,focus,false);
        return getComponent();
    }

    private void applyModifiers(Object valueOrRef, Component c, boolean sel) {
        boolean s=false;
        if (c instanceof JLabel && _vSel!=null) {
            s=sel ||_vSel.contains(valueOrRef);
            c.setBackground(s ? bgSelected() : C(0xFFffFF));            
            c.setForeground(C(s? 0xFFffFF:0));
        }
        final List<ChRunnable>v=listServicesR(false,this, ChRunnable.RUN_MODIFY_RENDERER_COMPONENT);
        for(int j=sze(v); --j>=0;) {
            final ChRunnable r=(ChRunnable)get(j,v);
            if (r==null) continue;
            _valComp[0]=deref(valueOrRef);
            _valComp[1]=c;
            r.run(ChRunnable.RUN_MODIFY_RENDERER_COMPONENT, _valComp);
        }
        if (s) c.setForeground(C(0xFFffFF));
    }
    private static Component _emptyCell;
    private final static Object[]  _valComp={null,null};
    public Component getTableCellRendererComponent(JTable t,Object v,boolean selected,boolean focus,int row,int col) {
        if (_origRend instanceof TableCellRenderer) {

            final Component c=((TableCellRenderer)_origRend).getTableCellRendererComponent(t,v,selected, focus, row,col);
            applyModifiers(v, c, selected);
            return c;
        }
        if (t==null||!t.isEnabled() && gcp(KOPT_NOT_PAINTED_IF_DISABLED,t)!=null) {
            if (_emptyCell==null) _emptyCell=new JPanel();
            return _emptyCell;
        }
        process(t, v,row,col,selected,focus,false);
        return getComponent();
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Font >>> */
    private static Map<String,Object> mapFont=new HashMap();
    private static ChButton _labFont;
    public Component getListCellRendererComponent(JList jl,Object v,int row,boolean selected, boolean focus) {
        if (_origRend instanceof ListCellRenderer) {
            final Component c=((ListCellRenderer)_origRend).getListCellRendererComponent(jl,v,row,selected,focus);
            applyModifiers(v,c,selected);
            return c;
        }
        if (gcp(KEY_ACTION_COMMAND,jl)==CMD_FOND_NAME) {
            final ChButton lab=_labFont==null ? _labFont=labl() : _labFont;
            lab.t("ABCDEFG abcdefg  "+v).fg(selected ? 0xFFffFF:0).setBackground(C(selected? 0:0xFFffFF));
            final String fn=toStrg(v);
            if (fn!=null) {
                Font f=getV(fn,mapFont,Font.class);
                if (f==null) mapFont.put(fn,newSoftRef(f=new Font(fn,Font.PLAIN,14)));
                lab.setFont(f);
            }
            return lab;
        }
        process(jl, v,row,0,selected,focus,false);
        return getComponent();
    }
    public Component getTableCellEditorComponent(JTable t,Object valueOrRef,boolean selected,int row,int col) {
        final Object value=deref(valueOrRef);
        if (value==blankLabel()) return null;
        _rowEd=row;
        _colEd=col;
        final Object v_editor=gcp(KEY_EDITOR_COMPONENT,value);

        Component editor=(Component)(v_editor instanceof Component ? v_editor : value instanceof Component ? value : null);
        if (editor==null) {
            process(t, valueOrRef,row, col, selected,true,true);
            editor=getComponent();
        }
        pcp(KEY_EDITED_ROW,intObjct(row),editor);
        pcp(KEY_EDITED_COL,intObjct(col),editor);
        return editor;
    }
    private Collection _vCellEdLi;
    public void addCellEditorListener(CellEditorListener l) { _vCellEdLi=adUniqNew(wref(l), _vCellEdLi); }
    public Object getCellEditorValue(){
        return _componentEd==textField() ? getTxt(textField()) :
            _componentEd==_cbEd && _cbEd!=null ? boolObjct(_cbEd.isSelected()) : "";
    }
    /* <<< Specific methods for JTree, JList ... <<< */
    /* ---------------------------------------- */
    /* >>> Component >>> */
    private Component _componentEd,  _component;
    private transient Object _treeOrList;
    public void process(JComponent treeOrList, Object valueOrRef, int row, int column, boolean selected, boolean focus,boolean isEditing) {
        if (!withGui()) assrt();

        final ChButton LAB=gcp(KOPT_JUST_WRITING_PNG,treeOrList)!=null ? labl() : label();
        final int opt=_opt;
        final JTable jt=deref(treeOrList,JTable.class);

        boolean editable=false;
        try { editable=isVisbl(jt) && jt.isCellEditable(row,column); } catch(Exception ex){}
        pcp(KOPT_IS_IN_TABLE_HEADER, row<0 ? "" : null, LAB);
        if (treeOrList==null) { _component=new JLabel("treeOrList==null"); return;}
        Component c=null;
        final JComponent tf=textField();
        Object v=_value;
        try {
            try {
                if (deref(_treeOrList)!=treeOrList) _treeOrList=wref(treeOrList);
                v=deref(valueOrRef);
                v=orO(runCR(v,MAY_PROVIDE_TREE_VALUE,null), v);
                setTxt(null,LAB.i(null));
                if (v==null) { c=blankLabel(); return;}
                final Class cValue=v.getClass();
                LAB.setHorizontalAlignment(cValue==Integer.class ? JLabel.RIGHT : JLabel.LEFT);
                if (v==treeOrList) c=LAB.t("Root");
                if (_togFill==null) addPaintHook(instance(), _togFill=new ChPanel());
                if (v instanceof Color) {
                    c=_togFill;
                    _togFillColor=(Color)v;
                    _togFillSel=0;
                    return;
                }
                if (v instanceof Object[]) {
                    final Object oo[]=(Object[]) v;
                    if (oo.length==2 && oo[0]==KEY_TOGGLE_SELECTED && oo[1] instanceof Color) {
                        c=_togFill;
                        _togFillColor=(Color)oo[1];
                        _togFillSel=selected?1:-1;
                        return;
                    }
                }
                final Object hasRend=isInstncOf(HasRenderer.class, v) ? ((HasRenderer)v).getRenderer( treeOrList instanceof JList ? HasRenderer.JLIST : 0, _rendOpt): null;
                final boolean showClass=v instanceof Class ||
                    v instanceof ProxyClass ||
                    0!=(opt&CLASS_NAMES)/*  && v instanceof CharSequence && strchr('.',v)>0*/;
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 Color color;
                if (showClass) {
                    c=component4class(v,LAB);
                    color=v instanceof ProxyClass ? C(0xFFff00) : v instanceof Class && ((Class)v).isInterface() ? C(0x00FFFF) : null;
                    if (LAB.getIcon()==null) LAB.i(IC_BLANK);
                } else color=v instanceof Integer ? null : colr(v);

                final CharSequence titi_tip=dTip(v), titi_itm;
                Object titi_icn=dIIcon(v);
                if (titi_icn==null && v instanceof ChRunnable) titi_icn=((ChRunnable)v).run(ChRunnable.RUN_GET_ICON,null);
                if (showClass && !(v instanceof Collection || v instanceof Object[])) titi_itm=null;
                else {
                    CharSequence rt=_mapRenTxt.get(v);
                    if (rt==null) rt=_mapRenTxtWeak.get(v);
                    if (rt==null) rt=deref(hasRend,CharSequence.class);
                    if (rt==null) rt=dItem(v);
                    titi_itm=rt;
                }

                if (c==null && v instanceof String && 0!=(opt&(LAST_PATH_COMPONENT_SLASH|LAST_PATH_COMPONENT_DOT))) {
                    final String s=0!=(opt&LAST_PATH_COMPONENT_DOT) ? lstCmpnt(v,'.') : lstPathCmpnt(v);
                    c=LAB.t(sze(s)>0?s:toStrg(v));
                }
                if (c==null) c=derefJC(hasRend);
                if (c==null) c=gcp(KEY_RENDERER_COMPONENT,v, Component.class);
                if (c==null && v instanceof CharSequence) {
                    if (isEditing) setTxt(toStrg(v),c=tf);
                    else c=LAB.t((CharSequence)v);
                }
                if (c==null && v instanceof Boolean) {
                    final AbstractButton cb=isEditing ? cbEditor() : checkbox();
                    cb.getModel().setSelected(!(v instanceof String) &&  isTrue(v));
                    c=cb;
                }
                if (c==null && v instanceof File) {
                    final File f=(File)v;
                    final String path=(gcp(KOPT_FULL_FILE_PATH,treeOrList)==null ? nam(f) : toStrg(f));
                    c=LAB.i(f.isDirectory() ? IC_DIRECTORY : null);
                    final BA sb=baTip().aln(path);
                    if (!f.exists()) {
                        _rendOpt[0]=HasRenderer.STRIKE_THROUGH;
                        LAB.t(sb.a(" Not existing"));
                    } else {
                        _rendOpt[0]&=~HasRenderer.STRIKE_THROUGH;
                        LAB.t(sb.formatSize(f));
                    }
                }
                if (c==null && v instanceof URL) c=LAB.t(toStrg(v));
                if (c==null && v instanceof Icon) c=LAB.i((Icon)v).t("");
                if (c==null && HasName.class.isAssignableFrom(cValue)) c=LAB.t(nam(v));
                if (c==null && cValue==Integer.class) c=label().t(baTip().a(v).a(' '));
                if (c==null && v instanceof JComponent) c=(JComponent)v;
                if (c==null) c=LAB;
                final boolean keepColor=gcp(KOPT_NO_CHANGE_FG_AND_BG_IF_SELECTED,c)!=null;
                final Color fg= !isEnabled(v) ? color(selected?2:1) :  C(selected ? FG_SELECTED : 0);
                if (c!=null && !keepColor) {
                    c.setBackground(selected ? bgSelected() : C(0xFFffFF));
                    c.setForeground(fg);
                }
                if (c==LAB) {
                    LAB
                        .cp(KEY_COLOR, gcp(KEY_COLOR,v))
                        .cp(KEY_BACKGROUND, C(editable ? 0xFFffFF : DEFAULT_BACKGROUND))
                        .cp(KEY_COLOR_MARK,color)
                        .setBorder(color==null?null: instance());
                    LAB.setBorderPainted(color!=null);
                    if (titi_icn!=null || titi_tip!=null || titi_itm!=null) {
                        if (titi_itm!=null) LAB.t(titi_itm);
                        if (titi_tip!=null) LAB.tt(titi_tip);
                        if (titi_icn!=null) LAB.i(titi_icn);
                    }
                    LAB.setOptions(isEnabld(v) ? LAB.getOptions()&~ChButton.CROSSING_OUT : LAB.getOptions()|ChButton.CROSSING_OUT);
                }
            } catch(Throwable e) {stckTrc(e);}
        } finally {
            _value=v;
            _component=c;
            applyModifiers(v,c,selected);
            if (isEditing) _componentEd=c;
            pcp(KOPT_IS_SELECTED,selected?"":null, c);
            pcp(KEY_NOT_EDITABLE_BG, editable ? color(0) : null, c);
        }
    }
    /* <<< Component <<< */
    /* ---------------------------------------- */
       /* >>> IsEnabled  >>> */
    private IsEnabled _enable;
    public ChRenderer setEnabled(IsEnabled ie) { _enable=ie; return this;}
    private boolean isEnabled(Object o) { return _enable!=null  ? _enable.isEnabled(o) : true;}
    /* <<< IsEnabled <<< */
    /* ---------------------------------------- */
    /* >>> Class >>> */
    private Component component4class(Object value, ChButton label) {
        if (value instanceof JLabel) return (JLabel)value;
        final String cn;
        if (value instanceof Class) cn=nam(value);
        else if (value instanceof String) cn=(String)value;
        else if (value instanceof ProxyClass) cn=toStrg(value);
        else if (value!=null) cn=nam(value.getClass());
        else cn=null;
        if (cn==null) return label.t("error");
        final String cnShort=cn.substring(cn.lastIndexOf('.')+1);
        _rendOpt[0]=isProxyClassUnavailable(value) ? HasRenderer.STRIKE_THROUGH : 0L;
        final String cnShort2=delSfx("PROXY",cnShort);
        return label.t(cnShort!=cnShort2 ? cnShort2+" *" : cnShort).i(dIcon(cn));
    }
    /* <<< Class <<< */
    /* ---------------------------------------- */
    /* >>> PaintHook >>> */
    private final long _rendOpt[]={0};
    private static ChPanel _togFill, _faked;

    private static int _togFillSel;
    public boolean paintHook(JComponent c, Graphics g, boolean after) {
        if (after && c!=null && c==_togFill) {
            final int w=c.getWidth(), h=c.getHeight();
            final Color color=_togFillColor;
            if (color!=null) {
                g.setColor(color);
                if (_togFillSel==0) g.fillRect(0,0,w,h);
                else g.fill3DRect(2,2,w-4,h-4, _togFillSel==-1);
            }
        }
        if (after && c==_label && c!=null) {
            if (0!=(HasRenderer.STRIKE_THROUGH&_rendOpt[0])) {
                g.setColor(C(0));
                final int y0=c.getHeight()/2, w=c.getWidth();
                randomStrikeThrough(STRIKE_THROUGH_THICK, hashCd(getTxt(c)), g, 0, w,  y0);
            }
        }
        return true;
    }

    public static void paint(JComponent c,Graphics g) {
        if (c==null) return;
        if (gcp(KEY_HAS_CURSOR,c)!=null) {
            ChIcon.drawCursor(g,32,8,16,16,c);
        }
        if (gcp(KEY_REJECTED,c)!=null) {
            g.setColor(color(3));
            g.fillRect(0,0,9999,999);
        }
    }
    private static Random _rnd;
    private static Stroke _strokeSU;
    public final static long STRIKE_THROUGH_V=1<<1, STRIKE_THROUGH_THICK=1<<2;
    public static void randomStrikeThrough(long opt, int randomSeed, Graphics g,  int xFrom, int xTo, int y0) {
        if (_rnd==null) _rnd=new Random();
        _rnd.setSeed(randomSeed);
        final Graphics2D g2=(Graphics2D)g;
        final Stroke stroke0=g2.getStroke();
        if (0!=(STRIKE_THROUGH_THICK&opt)) g2.setStroke(_strokeSU==null ? _strokeSU=new BasicStroke(2f) : _strokeSU);
        double dir=0, yLast=0, y=0;
        for(int x=xFrom;x<xTo;x++) {
            g.setColor(C(0));
            if (0!=(opt&STRIKE_THROUGH_V)) {
                g.drawLine( y0+(int)yLast,x, y0+(int)y, x+1);
            } else g.drawLine(x, y0+(int)yLast, x+1, y0+(int)y);
            dir+=_rnd.nextFloat()-0.5-y/8-dir/8;
            y+=dir;
            yLast=y;
        }
        g2.setStroke(stroke0);
    }
    /* <<< PaintHook <<< */
    /* ---------------------------------------- */
    /* >>> Border >>> */
    @Override public void paintBorder(Component c, Graphics g, int x, int y, int width, int h) {
        final Color col=colr(gcp(KEY_COLOR_MARK,c)), old=g.getColor();
        if (col!=null) {
            g.setColor(col);
            g.fillRect(0,h/2-4,4,8);
            g.setColor(old);
        }
    }
    @Override public Insets getBorderInsets(Component c, Insets i) {
        i.left=4;
        i.top=i.right=i.bottom=0;
        return i;
    }
    /* <<< Border <<< */
    /* ---------------------------------------- */
    /* >>> Small Button >>> */
    private final static Object KEY_SMALL_X[]={new Object(),new Object(),new Object()};
    public final static int BUT_WIDTH=EM, SMALL_CLOSE=1, SMALL_MAXIM=2;
    public static void drawSmallButtons(JComponent c, Graphics g0) {
        if (c==null) return;

        final int W=wdth(c);
        if (gcp(KOPT_DRAW_UNDOCKABLE,c)!=null || c instanceof JMenuItem && ((JMenuItem)c).isArmed()) {
            drawDivider(c,g0,W-EX,0,EX,999);
        }
        for(int type=KEY_SMALL_X.length; --type>=0;) {

            final Object X=gcp(KEY_SMALL_X[type],c);
            if (X!=null) {

                int x=atoi(X);
                if (x==MIN_INT) return;
                if (x<0) x=W+x;
                final Graphics g=g0!=null ? g0 : c.getGraphics();
                if (g!=null) {
                    final int y=0;
                    g.setColor(C(0xFFffFF));
                    g.fillRect(x-2,y-2,BUT_WIDTH+4,BUT_WIDTH+4);
                    if (type==SMALL_CLOSE) {
                        g.setColor(C(0xff0000,99));
                        g.fill3DRect(x,y,BUT_WIDTH,BUT_WIDTH, true);
                        g.setColor(C(0x444444));
                        g.drawLine(x+1,y+1,x+BUT_WIDTH-1,y+BUT_WIDTH-1);
                        g.drawLine(x+BUT_WIDTH-1,y+1,x+1,y+BUT_WIDTH-1);
                    }
                    if (type==SMALL_MAXIM) {
                        g.setColor(C(0x8877ff));
                        g.fillRect(x,y,BUT_WIDTH-3,BUT_WIDTH-2);
                        g.setColor(C(0));
                        g.drawRect(x,y,BUT_WIDTH,BUT_WIDTH);
                    }
                }
            }
        }
    }
    public static boolean setSmallButtonXc(int x, Object o) {
        if (o==null) return false;
        final Object toBeClosed=gcp(KEY_TO_CLOSE,o);
        if (toBeClosed!=null && o instanceof Component) instance().LI.addTo("m",o);
        return setSmallButtonX(SMALL_CLOSE,x,o);
    }
    public static boolean setSmallButtonX(int type, int x, Object o) {
        if (o==null) return false;
        final Object key=KEY_SMALL_X[type],  old=gcp(key, o);
        final boolean same= x== (old==null?MIN_INT:atoi(old));
        if (!same) {
            pcp(key, intObjct(x), o);
            if (type==SMALL_CLOSE && o instanceof AbstractButton) instance().LI.addTo("M",o);
        }
        return !same;
    }
    private final static Map<Object,String> _mapRenTxt=new IdentityHashMap(), _mapRenTxtWeak=new WeakHashMap();
    public static void setRendererText(Object o, String txt) { _mapRenTxt.put(o,txt);}
    public static void setRendererTextWeak(Object o, String txt) { _mapRenTxt.put(o,txt);}
     static int hashC(Object o) {
        Object oHC=o;
        if (o instanceof UniqueList) {
            final String txt=((UniqueList)o).getText();
            if (txt!=null) oHC=txt;
        }
        return oHC==null ? 0 : oHC.hashCode();
    }
}
