package charite.christo;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class EvAdapter implements MouseListener, MouseWheelListener, MouseMotionListener, ActionListener, FocusListener, KeyListener, ComponentListener,
                                  ChangeListener, ListSelectionListener, ItemListener {
    private final Object _pe;
    final static String KEY="CEA$$K";
    public EvAdapter(ProcessEv processEvent) {
        _pe=wref(processEvent);
        pcp(KEY,this, processEvent);
    }
    private void pe(AWTEvent ev) {
        final ProcessEv pe=deref(_pe, ProcessEv.class);

        if (pe!=null && ev!=null) {
            // try {
            if (ev.getSource()==null) assrt();
            else pe.processEv(ev);
            //} catch(Throwable exception) { stckTrc(exception);}
        }
    }

    public void mouseExited(MouseEvent ev) {pe(ev);}
    public void mouseEntered(MouseEvent ev) {pe(ev);}
    public void mouseReleased(MouseEvent ev) {pe(ev);}
    public void mouseClicked(MouseEvent ev) {pe(ev);}
    public void mousePressed(MouseEvent ev) { pe(ev); }
    public void mouseDragged(MouseEvent ev) {pe(ev);}
    public void mouseMoved(MouseEvent ev) {pe(ev);}
    public void actionPerformed(ActionEvent ev) { pe(ev);}
    public void mouseWheelMoved(MouseWheelEvent ev) {pe(ev);}
    public void focusGained(FocusEvent ev) {pe(ev);}
    public void focusLost(FocusEvent ev) {pe(ev);}
    public void keyPressed(KeyEvent ev) { pe(ev);}
    public void keyReleased(KeyEvent ev) {pe(ev);}
    public void keyTyped(KeyEvent ev) {pe(ev);}

    public void componentResized(ComponentEvent ev) {pe(ev);}
    public void componentMoved(ComponentEvent ev) {pe(ev);}
    public void componentShown(ComponentEvent ev) {pe(ev);}
    public void componentHidden(ComponentEvent ev) {pe(ev);}

    public void stateChanged(ChangeEvent ev) {
        pe(new ActionEvent( ev.getSource(), ActionEvent.ACTION_PERFORMED, ACTION_CHANGE_EVENT, System.currentTimeMillis(), 0));
    }
    public void valueChanged(ListSelectionEvent ev) {
        pe( new ActionEvent( ev.getSource(), ActionEvent.ACTION_PERFORMED, ACTION_SELECTION_CHANGED, System.currentTimeMillis(), 0));
    }

    @Override public void itemStateChanged(ItemEvent ev) {
        pe( new ActionEvent( ev.getSource(), ActionEvent.ACTION_PERFORMED, ACTION_SELECTION_CHANGED, System.currentTimeMillis(), 0));
    }
    private static String[] _add, _remove;
    private final Object this1[]={this};
    private final static Object sync=new Object();
    public EvAdapter addTo(String types, Object p) {
        if (p==null) return this;
        synchronized(sync) {
            if (_add==null) {
                _add=new String['z'+1];
                _remove=new String['z'+1];
                for(int c='A'; c<='z';c++) {
                    final Class clazz=
                        c=='M' ? MouseMotionListener.class :
                        c=='C' ? ChangeListener.class :
                        c=='a' ? ActionListener.class :
                        c=='c' ? ComponentListener.class :
                        c=='f' ? FocusListener.class :
                        c=='i' ? ItemListener.class :
                        c=='k' ? KeyListener.class :
                        c=='m' ? MouseListener.class :
                        c=='s' ? ListSelectionListener.class :
                        c=='w' ? MouseWheelListener.class :
                        //c=='W' ? WindowListener.class :
                        //c=='j' ? AdjustmentListener.class :
                        null;
                    if (clazz==null) continue;
                    final String cn=ChUtils.shrtClasNam(clazz);
                    _add[c]="add"+cn;
                    _remove[c]="remove"+cn;
                }
            }
            for(int i=sze(types); --i>=0; ) {
                final int c=types.charAt(i);
                if (c==' ') continue;
                if (_add[c]==null) {
                    putln("types="+types+"<  c="+(char)c);
                    assrt();
                    continue;
                }
                final long opt=RFLCT_PRINT_EXCEPTIONS|RFLCT_ASSERT_EXISTS;
                final AbstractButton b= deref(p, AbstractButton.class);
                final Component comp= deref(p,Component.class);
                if (b!=null && c=='a') {
                    b.removeActionListener(this);
                    b.addActionListener(this);
                } else if (comp!=null && c=='m') {
                    comp.removeMouseListener(this);
                    comp.addMouseListener(this);
                } else if (comp!=null && c=='M') {
                    comp.removeMouseMotionListener(this);
                    comp.addMouseMotionListener(this);
                } else if (comp!=null && c=='k') {
                    comp.removeKeyListener(this);
                    comp.addKeyListener(this);
                } else {
                    invokeMthd(opt, _remove[c], p, this1);
                    invokeMthd(opt, _add[c], p, this1);
                }
            }
            return this;
        }
    }
}
// java.util.EventListenerProxy
