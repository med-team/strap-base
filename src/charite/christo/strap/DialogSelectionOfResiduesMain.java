package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

Residues selections are computed by a method chosen by the user.
The selected residues will be shown in the alignment pane,  the
horizontal scroll-bar and all 3d-backbones.

   @author Christoph Gille
*/
public class DialogSelectionOfResiduesMain extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener {
    private final ChCombo _comboClass=SPUtils.classChoice(ResidueSelection.class);
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd==ChButton.GO) {
            final ResidueSelection c=_comboClass.instanceOfSelectedClass(ResidueSelection.class);
            if (c!=null) adTab(CLOSE_DISPOSE, shrtClasNam(c), new DialogSelectionOfResidues(c,_comboClass.getSelectedItem()), this);
        }
        if (cmd=="SS") {
            Protein pp[]=StrapAlign.selectedProteins();
            if (pp.length==0) pp=StrapAlign.proteins();

            final List<ResidueSelection> v=new ArrayList();
            for(Protein p : pp)  adAll(ResSelUtils.fromSecStru(p),v);

            if (sze(v)==0) error(pp.length==0 ? "No protein" : "No secondary structure");
            else {
                StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_CHANGED);
                final String title="Residue selection from secondary structure elements";
                StrapAlign.showInJList(0L,null, v.toArray(), title, null).showInFrame(ChFrame.AT_CLICK|ChFrame.ALWAYS_ON_TOP, title);
            }
        }
    }
    public DialogSelectionOfResiduesMain() {
        pcp(KOPT_PANHS_NO_CTRL,"",_comboClass);
        final Object
            panC=_comboClass.panel(),
            pMore=pnl(VBHB,
                      pnl(HBL, new ChButton("SS").li(this).t("From secondary structure elements"), "As defined in PDB files "),

                      StrapAlign.coSelected().proteinMenu().b(ProteinPopup.CMD_HL_DIFF,0).i(IC_UNDERLINE),
                      " ",
                      "Intron / Exon boundaries from context menu",
                      " ",

                      pnl(HBL, "Dialogs, optionally selecting residues: ", StrapAlign.b(DialogBlast.class),  StrapAlign.b(DialogAlignOneToAll.class))
                      ),
            pCenter=pnl(VBHB,
                        dialogHead(this),
                        panC,
                        new ChButton(ChButton.GO).li(this),
                        pnlTogglOpts("More:",pMore),
                        pMore,
                        "##"
                        );
        setMaxSze(9999,2*ICON_HEIGHT, panC);
        adMainTab(pCenter, this, null);
    }
}
