package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.SequenceFeatures.*;
public class ExpasyGff  {
    private final static int F_NO_SEC_STRU=1, F_SEC_STRU=2;
    private ExpasyGff(){}
    public static void load(int opt, Protein pp[], ChRunnable log) {
        final Set<String> vID_already=new HashSet();
        BA baGFF=null;
        final int optFindID=opt&FindUniprot.MASK;
        final boolean fast=0==(optFindID&FindUniprot.BLASTER_MASK);
        final BA sb=SequenceFeatures.progressText(opt, new BA(99));
        final int sb0=sb.end();
        try {
            final List<String> vID=new ArrayList();
            for(int step : new int[]{0, F_NO_SEC_STRU, F_SEC_STRU}) {
                nextProtein:
                for(int iP=0; iP<pp.length; iP++) {
                    vID_already.clear();
                    final Protein p=pp[iP];
                    if (p==null) continue;
                    synchronized(mkIdObjct(fast?"EGFF$$ksF":"EGFF$$ks",p)) {
                        if (p.containsOnlyACTGNX()) continue;
                        if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(ExpasyGff.class, (iP+1)*100/(pp.length+1), sb.setEnd(sb0).a(p).a(" ...")));
                        final int countAnnotationsOld=p.residueAnnotations().length;
                        int countFeatures=0;
                        final String ids[]=(opt&OPT_ID_BY_HOMOLOGY)!=0 ? FindUniprot.load1(optFindID,p,SequenceFeatures.logR()).clone() : new String[]{p.getUniprotID(), p.getAccessionID()};
                        if (countNotNull(ids)==0) continue;
                        for(int j=ids.length; --j>=0;) {
                            if (!strEquls("UNIPROT:",ids[j],0) && !strEquls("SWISS:",ids[j],0) && !(strEquls("TREMBL:",ids[j],0) && !strEquls("PDB:",ids[j],0))) ids[j]=null;
                        }
                        for(String id0 :  ids) {
                            if (id0==null || !id0.startsWith("UNIPROT:") && !id0.startsWith("SWISS:") && !id0.startsWith("TREMBL:") && !id0.startsWith("PDB:")) continue;
                            final String id=id0.startsWith("PDB:") ? pdbID(id0) : id0.substring(id0.indexOf(':')+1);
                            if (!vID_already.contains(id)) vID_already.add(id); else continue;
                            final String gff_expasy_colon_Id="GFF_EXPASY:"+id;
                            final java.net.URL url=url(Hyperrefs.toUrlString(gff_expasy_colon_Id,Hyperrefs.PLAIN_TEXT));
                            final java.io.File f=urlGet(url,  CacheResult.isEnabled() ? MAX_DAYS:0);
                            if (f!=null && sze(f)==0 &&  sze(file(f+TMP_DOWNLOAD_SFX))==0) wrte(f,"0");
                            if (step==0 && log instanceof BA) ((BA)log).a(' ').a(gff_expasy_colon_Id).a(' ').a(f!=null?sze(f):-1).aln("bytes");

                            baGFF=readBytes(f,baGFF);
                            if (baGFF!=null) {

                                for(ResidueAnnotation aNew : parseFeatures(step, baGFF,  url, p, "", step==0 ? vID : null)) {
                                    aNew.addE(0,ResidueAnnotation.HYPERREFS,gff_expasy_colon_Id);
                                    if (aNew.isEnabled()) countFeatures++;
                                }
                                if (0!=(OPT_ONLY_FIRST_UNIPROT&opt)) break;
                            }
                        }

                        if (step==0) continue nextProtein;
                        final ResidueAnnotation[] aa=p.residueAnnotations();
                        if (step>0 && countAnnotationsOld!=aa.length)  SequenceFeatures.keepFeaturesWithBestScore(p);
                        if (countFeatures>0) {
                            Protein.incrementMC(ProteinMC.MCA_SEQUENCE_FEATURES_V,null);
                            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED, 999);
                        }
                    }
                }
                if (step==0) {
                    final String msg="GFF SOAP uniprot "+sze(vID)+" files ... ";
                    StrapAlign.drawMessage("@1"+msg);
                    StrapAlign.drawMessage("@1"+ANSI_GREEN+msg);
                }
            }
        } finally {
            if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(ExpasyGff.class,0, sb.setEnd(sb0).a(plrl(pp.length," %N protein%S done"))));
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Also for StrapDAS >>> */
    private final static Set _vLog=new HashSet();
    static ResidueAnnotation[] parseFeatures(int mode, BA ba, java.net.URL url, Protein p, String sequenceDatabase,  Collection vOnlyCollectIDs) {
        final BA log=SequenceFeatures.log();
        if (0==sze(ba) || p==null) return ResidueAnnotation.NONE;
        final boolean isResnum="PDBresnum".equals(sequenceDatabase);
        final String sequenceDB= isResnum ? "PDB" : sequenceDatabase;
        final byte[] T=ba.bytes();
        final int E=ba.end(), TABS[]=new int[9], ends[]=ba.eol();
        final boolean isXML=strstr("</FEATURE>",T,0,E)>0;
        final String xmlSegment=!isXML?null:xmlAttribute("id",T,strstr(STRSTR_w|STRSTR_AFTER,"<SEGMENT",T,0,E), E);

        Collection vRA=null;
        final BA baTmp=new BA(99);
        int b=0;
        nextFeature:
        for(int iL=0; ;iL++) {
            final int e;
            if (isXML) {
                b=strstr(STRSTR_w|STRSTR_AFTER,"<FEATURE",T,b,E);
                e=strstr("</FEATURE>",T,b,E);
            } else {
                if (iL>=ends.length) break;
                b=iL==0 ? 0 : ends[iL-1]+1;
                e=get(iL,ends);
                if (get(b,T)=='#') continue;
            }
            if (e<0 || b<0) break;
            final BA line=isXML ? new BA(T,b,e) : ba.subSequence(b,e);
            if (p.vInvalidFeatureLine().contains(line)) continue;
            for(ResidueSelection a:p.allResidueSelections()) {
                final CharSequence srcTxt= a instanceof ResidueAnnotation ? ((ResidueAnnotation)a).srcText() : null;
                if (sze(srcTxt)==sze(line) && strEquls(srcTxt, line,0)) continue nextFeature;
            }
            final String featureName, seqname, source, attribute, viewSrcText;
            final int origStart, origEnd;
            String disabledWhy=null;
            if (isXML) {
                final int featureGT=strchr('>',T,b,e);
                final int type=strstr(STRSTR_w|STRSTR_AFTER,"<TYPE",T,b,e), typeGT=strchr('>',T,type,e);
                final String sLabel=xmlAttribute("label",T,b,featureGT);
                final String sType=filtrS(FILTER_HTML_DECODE,0, T[typeGT-1]!='/' ? ba.getString(BA.STRING_POOL, typeGT+1,  strstr("</TYPE>",T,type,e)) : xmlAttribute("id",T, type,typeGT));
                final String category=xmlAttribute("category",T, type,typeGT);
                if (category!=null && category.indexOf(" similarity")>0) disabledWhy="Occures 'similarity' in category";
                final boolean[] found={false};
                final String
                    sTypeTrans=SequenceFeatures.mapSynonyms(sType,found),
                    sTypeLC=lCase(sType),
                    sTypeLC1=delSfx("_one",delSfx("_two",delSfx("_three",delSfx("_four",delSfx("_five",delSfx("_six",delSfx("_seven",delSfx("_eight",sTypeLC))))))));
                if (found[0] || setSequenceOntologyTermsLC().contains(sTypeLC) || setSequenceOntologyTermsLC().contains(sTypeLC1)) {
                    featureName=sTypeTrans;
                    attribute=filtrS(FILTER_HTML_DECODE,0,sLabel);
                } else {
                    featureName=filtrS(FILTER_HTML_DECODE,0, sType);
                    attribute=filtrS(FILTER_HTML_DECODE,0, ResidueAnnotation.mapFeatName(sType));
                }
                origStart=atoi(T,strstr(STRSTR_AFTER,"<START>",T,b,e),E)-1;
                origEnd=atoi(T,strstr(STRSTR_AFTER,"<END>",T,b,e),E);
                source=uCase(sequenceDB);
                seqname=xmlSegment;
                viewSrcText= url!=null ? file(url)+ChTextComponents.RANGE_SUFX_XML+(b-8)+"-"+(e+9) : null;
            } else { /* P25787	UniProtKB	Modified residue	24	24	.	.	.	Note=Phosphotyrosine	*/
                viewSrcText=null;
                if (tabulatrs('\t', T, b, e, TABS)<8) continue;
                seqname=ba.newString(b,TABS[0]);
                source=uCase(ba.newString(1+TABS[0],TABS[1]));
                if (vOnlyCollectIDs!=null) { if (source.startsWith("UNIPROT")) vOnlyCollectIDs.add(seqname); continue; }
                final String feature0=ResidueAnnotation.mapFeatName(ba.newString(1+TABS[1], TABS[2]));
                if (cntainsOnly(DIGT, T, 1+TABS[2],TABS[3])) origStart=atoi(T,1+TABS[2],E)-1; else continue;
                if (cntainsOnly(DIGT, T, 1+TABS[3],TABS[4])) origEnd=atoi(T,1+TABS[3],E); else continue;
                attribute=ba.newString(1+TABS[7],TABS[8]>0 ? TABS[8]:e);
                if ( (feature0=="Modified_residue" || feature0=="Topological_domain" || feature0=="Region") && attribute.startsWith("Note=")) {
                    final int semicolon=attribute.indexOf(';');
                    featureName=attribute.substring(5,semicolon>0 ? semicolon : sze(attribute));
                } else featureName=feature0;
            }
            if (featureName==null) continue;
            final String fName=ResidueAnnotation.mapFeatName(featureName);
            if ("Chain"==fName) continue;
            final boolean isSecStru="Turn"==fName || "Helix"==fName || "Beta_strand"==fName;
            if ( (mode==F_SEC_STRU || mode==F_NO_SEC_STRU) &&  isSecStru != (F_SEC_STRU==mode)) continue;
            final ResidueAnnotation a;
            if (isResnum) {
                final String sChain=is(LETTR_DIGT,p.getChainsAsString(),0) ? p.getChainsAsString().substring(0,1) : "";
                (a=new ResidueAnnotation(p)).addE(0,ResidueAnnotation.POS, (origStart+1)+":"+sChain+"-"+origEnd+":"+sChain);
                disabledWhy=null;
            } else {
                final ReferenceSequence uniprot=p.getReferenceSequence(source+":"+seqname, null,false,(Runnable)null);
                if (uniprot==null) continue;
                final float score=uniprot.getAlignmentScore();
                final int start=get(origStart, uniprot.getUniprotIdx2Idx())+Protein.firstResIdx(p);
                final int end=  get(origEnd-1, uniprot.getUniprotIdx2Idx())+1+Protein.firstResIdx(p);
                if (start<0 || end<0) {
                    if (log!=null) {
                        baTmp.clr().a("Failed to map positions: ").a(p).a('/').a(start).a('-').a(end).a(' ').aln(url).a(T,b,e).aln('\n');
                        if (_vLog.add(intObjct(baTmp.hashCode()))) log.aln(baTmp);
                    }
                    continue;
                }
                boolean poorAlignment=false;
                if (start<end) {
                    final int misma=uniprot.countMismatch(start,end,null);
                    if(/*score<50 ||*/  misma/(float)(end-start)>.3f) {
                        if (log!=null) {
                            baTmp.clr().a("Poor alignment ").a(p).a('/').a(start+1).a('-').a(end+1).a(" misma=").a(misma).a(' ').a(uniprot.getLinkForAlignment()).a(ALI_SCORE).a(score).a(' ').aln(url).a(T,b,e).aln('\n');
                            if (_vLog.add(intObjct(baTmp.hashCode()))) log.aln(baTmp);
                        }
                        poorAlignment=true;
                    }
                }
                final String rule=featureName2color(fName).rule, interval=rule=="-" ? "-" : ",";
                baTmp.clr().a(start+1,4);
                if (start+1<end) baTmp.a(interval).a(end);
                final String positions=baTmp.toString();
                a=new ResidueAnnotation(p);
                a.addE(0,ResidueAnnotation.POS, positions);
                a.setRendererText(fName+" "+positions+" "+ALI_SCORE+score);
                a.addE(0,"Note",uniprot.getLinkForAlignment());
                a.addE(0,"Remark",ALI_SCORE+score);
                a.setReferenceSequence(uniprot, start,end );
                final int selOffset=a.getSelectedAminoacidsOffset()-Protein.firstResIdx(p);
                if (selOffset>=p.countResidues()) disabledWhy="selOffset>=p.countResidues()";
                else if (poorAlignment) disabledWhy="Poor alignment ";
                else if (strstr("Status=Potential",T,b,e)>=0) disabledWhy="Occurs 'Status=Potential'";
                if (rule!=null && rule!="-") {
                    final char amino=(char) (p.getResidueType(selOffset)&~32);
                    if (rule.indexOf(amino)<0) disabledWhy="Only one amino and "+amino+" not in "+rule;
                }
            }
            if (viewSrcText!=null) a.addE(0,"SourceXML",viewSrcText);
            a.setSrcText(line);
            a.setStyle(VisibleIn123.STYLE_UNDERLINE | (1<<VisibleIn123.BIT_SHIFT_LINE));
            if (!isXML) a.addE(0,"SourceLine",line.replaceChar('\t',' '));
            a.addE(0,"Remark",attribute);
            setFN(fName, isXML ? SRC_DAS : SRC_GFF_EXPASY, a);
            a.setFeatureSrc(source, seqname);
            a.setValue(0,ResidueAnnotation.GROUP, (disabledWhy==null ? "" : "deactivated ")+(isXML ? "DAS-":"GFF-")+"features");
            addFeature(a,p, disabledWhy);
            vRA=adNotNullNew(a, vRA);
        }
        return toArry(vRA, ResidueAnnotation.class);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Ontology >>> */
    private static Set<String> _vSeqOnt;
    private static Set<String> setSequenceOntologyTermsLC() {
        if (_vSeqOnt==null) {
            _vSeqOnt=new HashSet();
            final BA ba=readBytes(rscAsStream(0L,_CCP,"sequence_ontology_terms.rsc"));
            if (ba!=null) {
                final byte T[]=ba.bytes();
                for(int i=ba.end(); --i>=0;)  if ('A'<=T[i] && T[i]<='Z') T[i]=(byte)(T[i]|32);
                final ChTokenizer TOK=new ChTokenizer().setText(ba);
                while(TOK.nextToken()) _vSeqOnt.add(TOK.asString());
            } else stckTrc();
        }
        return _vSeqOnt;
    }
    /* <<< Ontology <<< */
    /* ---------------------------------------- */
    /* >>> InProteinFile >>> */
    /*
      pdb1vyp.ent:
      REMARK 800 SITE
      REMARK 800 SITE_IDENTIFIER: AC1
      REMARK 800 SITE_DESCRIPTION: TNF BINDING SITE FOR CHAIN X
      SITE     1 AC1 17 HOH C 699  HOH C 700  HOH C 701  HOH C 702
      SITE     2 AC1 17 ALA X  23  PRO X  24  LEU X  25  THR X  26
      SITE     3 AC1 17 ALA X  58  GLN X 100  HIS X 181  HIS X 184
      SITE     4 AC1 17 ARG X 233  ALA X 302  GLY X 323  ARG X 324
      SITE     5 AC1 17 TYR X 351
      SITE     1 AC2 10 HOH C 703  HOH C 704  THR X  26  ALA X  58
      SITE     2 AC2 10 TYR X  68  PHE X 102  HIS X 181  HIS X 184
      SITE     3 AC2 10 TYR X 186  TYR X 351
    */
    public static ResidueAnnotation[] readFeaturesInProteinFile(BA text, int maxLen, Protein... pp) {
        final List<ResidueAnnotation> vAnnotations=new ArrayList();
        BA ba=null;
        boolean found=false;

        for(Protein p : pp) {
            boolean thisFound=false;
            ba=pp.length==1 && text!=null ? text : readBytes(p.getFile(),ba);
            if (ba==null) continue;
            final byte[] T=ba.bytes();
            final int B=ba.begin(), E=ba.end(),  ends[]=ba.eol();
            final ChTokenizer TOK=new ChTokenizer();
            Map<String,String> mapRemark=null;
            for(int iL=0; iL<ends.length; iL++) {
                final int b=iL==0 ? B :ends[iL-1]+1, e=ends[iL];
                if (e-b>28 && T[b+7]=='8' && T[b+8]=='0' && T[b+9]=='0' && T[b]=='R' && strEquls("REMARK 800 SITE_IDENTIFIER: ",T,b)) {
                    final String id=ba.getString(BA.STRING_TRIM,b+28,e);
                    for(int jL=iL+1; jL<ends.length; jL++) {
                        final int lineBegin=ends[jL-1]+1;
                        if (is(LETTR_DIGT, T, lineBegin)) {
                            if (strEquls("REMARK 800 SITE_DESCRIPTION: ",T,lineBegin)) {
                                (mapRemark==null ? mapRemark=new HashMap() : mapRemark).put(id,ba.getString(BA.STRING_TRIM, lineBegin+29, ends[jL]));
                            }
                            break;
                        }
                    }
                }
            }
            int b=0, e=0;
            ResidueAnnotation lastAnno=null;
            nextLine:
            for(int iL=0; iL<ends.length; iL++, b=e+1) {
                e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                final ResidueAnnotation a;
                final String featureName;
                if (e-b>10 && T[b]=='S' && strEquls("SITE: ",T,b)) {
                    final String siteID=tokn(T,b+11,e, null);
                    featureName=ResidueAnnotation.mapFeatName(siteID);
                    final BA sb=baTip();
                    for(int i=0; i<3; i++) {
                        final int resnum=b+26+i*11;
                        if (resnum>=e || T[resnum]==' ') break;
                        final char iCode=(char)T[resnum+1];
                        sb.a(atoi(T,resnum-3,E));
                        if (iCode!=' ') sb.a(iCode);
                        sb.a(':').a((char)T[resnum-4]).a(' ');
                    }
                    final String remark=mapRemark!=null ? mapRemark.get(siteID) : null;
                    a=new ResidueAnnotation(p);
                    a.addE(0,ResidueAnnotation.POS, sb);
                    if (remark!=null) a.addE(0,"Remark",remark);
                    thisFound=found=true;
                } else if (T[b]=='M' && strEquls("MODRES ",T,b) && e-b>25) {
                    /*
                      MODRES 1IVO ASN A   32  ASN  GLYCOSYLATION SITE
                    */
                    a=new ResidueAnnotation(p);
                    featureName=ResidueAnnotation.mapFeatName(mapSynonyms(ba.getString(BA.STRING_TRIM|BA.STRING_POOL, b+29, e), (boolean[])null));
                    a.addE(0,"Remark",featureName);
                    a.addE(0,"Note",ba.newString(b,e));
                    final char iCode=(char)T[b+22];
                    //final String pos= atoi(T,b+18)+(iCode!=' '?iCode+":":":")+(char)T[b+16];
                    a.addE(0,ResidueAnnotation.POS, atoi(T,b+18,E)+(iCode!=' '?iCode+":":":")+(char)T[b+16]);
                    thisFound=found=true;
                } else if (T[b]=='F' && T[b+1]=='T' &&  T[b+2]==' ') {
                    TOK.setText(T,b+3, e);
                    if (!TOK.nextToken()) continue nextLine;
                    if (lastAnno!=null && strEquls(" /FTId=",T,b+33)) {
                        lastAnno.addE(0,"Remark", ba.newString(b+35, T[e-1]=='.' ? e-1 : e));
                        continue nextLine;
                    }
                    {
                        final int f=TOK.from(), c0=T[f];
                        if ( (c0=='C'&&TOK.eq("CDS")||TOK.eq("CHAIN")) || c0=='e'&&TOK.eq("exon")  || !is(LETTR,c0) || !TOK.containsOnly(LETTR_DIGT_US)) continue nextLine;
                    }
                    final String feature0=TOK.asStringIP();
                    if (!TOK.nextToken() || !TOK.containsOnly(DIGT)) continue;
                    final int start=TOK.asInt()-1;
                    if (!TOK.nextToken() || !TOK.containsOnly(DIGT)) continue;
                    final int end=TOK.asInt();
                    if (end-start<1 || end-start>maxLen) continue;
                    featureName=ResidueAnnotation.mapFeatName(mapSynonyms(feature0, (boolean[])null));
                    final String interval=featureName2color(featureName).rule=="-" ? "-" : ",";
                    a=new ResidueAnnotation(p);
                    a.addE(0,"Remark",ba.newString(b,e));
                    a.setStyle(VisibleIn123.STYLE_UNDERLINE | (1<<VisibleIn123.BIT_SHIFT_LINE));
                    a.addE(0,ResidueAnnotation.POS, start+1==end ? toStrg(end) : (1+start)+interval+(end));
                    a.addE(0,ResidueAnnotation.GROUP, "FT_Lines");
                    thisFound=found=true;
                } else { a=null; featureName=null;}
                if (a!=null && featureName!=null) {
                    a.setFeatureSrc("ProteinFile","null");
                    setFN(featureName, SRC_PROTEIN_FILE, a);
                    addFeature(a,p, null);
                    lastAnno=a;
                    vAnnotations.add(a);
                }
            }
            if (thisFound) SequenceFeatures.keepFeaturesWithBestScore(p);
        }
        final ResidueAnnotation aa[]=toArry(vAnnotations,ResidueAnnotation.class);
        if (found) {
            Protein.incrementMC(ProteinMC.MCA_SEQUENCE_FEATURES_V,null);
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED, 999);
        }
        return aa;
    }
    /* <<<  In Protein File <<< */
    /* ---------------------------------------- */

}
