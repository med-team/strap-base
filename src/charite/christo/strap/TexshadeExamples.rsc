\feature{top}{$TEX_PROTEIN}{$TEX_RESIDUES}{box[Red]}{peptide}
\feature{ttop}{$TEX_PROTEIN}{$TEX_RESIDUES}{box[Red]}{peptide}
\feature{bbottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\uparrow$}{active site}
\feature{top}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\downarrow$}{active site}
\feature{bbottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\uparrow$}{$FIRST_A$_{$FIRST_INDEX}$ - $LAST_A$_{$LAST_INDEX}$}
\feature{bbottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\uparrow$}{$FIRST_A$_{$FIRST_NUMBER}$ - $LAST_A$_{$LAST_NUMBER}$}
\feature{bbottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\uparrow$}{$FIRST_A$_{$FIRST_NUMBER:$CHAIN}$ - $LAST_A$_{$LAST_NUMBER:$CHAIN}$}
\frameblock{$TEX_PROTEIN}{$TEX_RESIDUES}{Red[2pt]}
\emphregion{$TEX_PROTEIN}{$TEX_RESIDUES}
\emphblock{$TEX_PROTEIN}{$TEX_RESIDUES}
\shaderegion{$TEX_PROTEIN}{$TEX_RESIDUES}{Red}{Green}
\shadeblock{$TEX_PROTEIN}{$TEX_RESIDUES}{Red}{Green}
\feature{bbottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\uparrow$}{$FIRST_Aaa$_{$FIRST_INDEX}$}
\feature{bbottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:X}{cleavage}
\fingerprint{300}
\separationline{$TEX_PROTEIN}
    
