package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.ProteinList.*;
import static charite.christo.Insecure.EXEC_ALLOWED;

/**
   The protein menu.
   @author Christoph Gille
*/
public class ProteinPopup implements java.awt.event.ActionListener, IsEnabled, ChRunnable {
    public final static String
        ACTION_showInList="^Display list",
        ACTION_backbone="^Protein backbone",
        ACTION_close3d="^Close protein in all external 3D-viewers such as Pymol",
        CMD_HL_DIFF="^Identify differing positions in the aligned selected proteins";
    private final static String
        CMD_CLIPB="PP$$CB", CMD_EXPORT_DnD="$PP$$ED",
        TT_CLIPB="The sequence can then be pasted with Ctrl+V";
    private final static ChTextArea TA[]=new ChTextArea['Z'+1];
    final Object LAB_TITLE[]={ menuInfo(MI_HEADLINE,""), menuInfo(MI_LONG,"") };
    private final ContextObjects _objects;
    private static int _countJList;
    private static ChFileChooser _fChooser;
    private static ChButton _cbFirstOfTotal;
    private boolean _menuBar;
    private Object _msgDnD;
    private ChTextArea _taFirst;
    private DialogExecuteOnFile _execF, _execG;

    private FilePopup _fp;/*avoid gc */
    private JPopupMenu _popup;
    public ProteinPopup(ContextObjects o) { _objects=o; }
    public ProteinPopup cursor() {
        _menuBar=true;
        if (isScreenMenuBar()) setEnabld(false,(LAB_TITLE[1]=new JMenuItem()));
        return this;
    }

    /** These methods are for Tutorials */
    private static ProteinPopup m() { return StrapAlign.coSelected().proteinMenu();}

    public static Object docuEdSelected() { return m().b("ED",0); }
    public boolean isEnabled(Object buttton) {
        for(Protein p : _objects.proteins()) {
            if (ProteinList.isEnabled(p, atol(gcp("F",buttton)))) return true;
        }
        return false;
    }

    ChButton b(String s,long flags) {
        final ChButton b=new ChButton(s).li(ProteinPopup.this);
        rtt(b);
        String t=null, ic=null;
        if (flags!=0) pcp("F",longObjct(flags),b);
        if (s==CMD_CLIPB) { t="&Copy sequence to clipboard"; b.tt(TT_CLIPB); ic=IC_CLIPBOARD;}
        if (s==CMD_EXPORT_DnD) { t="Export protein files by Drag-and-Drop ..."; ic=IC_DND; }
        if (s==">A") { t="Export as multiple F^asta"; ic=IC_EXPORT; }
        if (s=="ED") { t="Prot&ein file"; ic=IC_EDIT;}
        if (!_menuBar) t=rplcToStrg("&","^",t);
        if (t!=null) b.t(t);
        if (ic!=null) b.i(ic);
        if (s!=CMD_EXPORT_DnD) b.cp(KEY_ENABLED,this);
        return b;
    }
    // ----------------------------------------
    private final Object[] PFF={null};
    public Object run(String id, Object arg) {
        if (id==StrapAlign.RUN_OPEN_DIALOG_HOOK) {
            SPUtils.dialogSetProteins(_objects.proteins(), deref(arg,JComponent.class));
        }
        if (id==PROVIDE_FILES) {
            final List v=vClr(PFF);
            for(Protein p : _objects.proteins()) adUniq(p.getFile(), v);
            return toArry(v,File.class);
        }
        return null;
    }
    public JPopupMenu menu() {
        final Protein pp[]=_objects.proteins();
        setTxt(plrl(pp.length,"Menu for %N protein%S"), LAB_TITLE[0]);
        setTxt(toStrg(new BA(99).join(pp,"   ")), LAB_TITLE[1]);
        if (_popup==null) {
            final Object
                bBT=b("BT",0).t("^Backtranslate amino acids to DNA ...").i(IC_DNA),
                bTO3D=b("TO3D",IN_3D).t("^Send 3D-commands in residue annotations to 3D-view[s]").i(IC_JMOL),
                bNeighb=b("NEIGHB", 0).tabItemTipIcon(DialogNeighbors.class),

                menuSearch[]={"i",IC_SEARCH, "Search",bNeighb, CLASS_DialogBlast},
                menuListResSel[]={
                    "Residue ^selections", "i", IC_UNDERLINE,
                    "Create new",
                    b("NEW_RA",0).t("^New Residue annotation").i(IC_ANNO),
                    b("FEATURES",0).t("^Download sequence features ...").i(IC_DOWNLOAD),
                    CLASS_DialogSelectionOfResiduesMain,
                    "-",
                    "List of selections in new window",
                    b("LIST_A",RES_ANNO).t("List residue ^annotations").i(IC_LIST),
                    b("LIST_F",SEQ_FEATURE).t("List sequence features").i(IC_LIST),
                    b("LIST_S",RES_SEL).t("List residue ^selections").i(IC_LIST),
                    "-",
                    "Apply",
                    bTO3D,
                    "-",
                    "Select",
                    b("SA",RES_ANNO).t("Select all Residue Annotations").i(IC_SELECT),
                    b("SS",RES_SEL).t("Select all Residue Selections").i(IC_SELECT),
                    b("SF",SEQ_FEATURE).t("Select all Sequence Features").i(IC_SELECT),
                    "To deselect hold Shift-key"
                },
                    menuSuper[]={
                        "^Structure superposition", "i", IC_SUPERIMPOSE,
                        CLASS_DialogSuperimpose3D,
                        b("RESET3D",CALPHA).t("^Restore original coordinates")
                        .i(IC_ORIGINALCOORDINATES).tt("The original coordinates are valid<br>The transformation is reset."),
                        bNeighb
                    };
            final String msgHet=
                "<h2>You need to use Drag-and-Drop to add hetero structures to protein</h2>"+
                "PDB-files containing only nucleotide chains or hetero compounds can be "+
                "dragged onto a protein label to associate the compound to the protein<br><br>"+
                "Also hetero compound items in list elements can be dragged to another protein.<br>"+
                "You may need to superimpose the source and target proteins to make coordinate systems compatible.";

            _fp=new FilePopup(FilePopup.NO_RENAME|FilePopup.PROTEIN_FILE);
            final Object oo[]={
                _menuBar?null:LAB_TITLE[0],
                LAB_TITLE[1],
                b(ACTION_showInList,0L).i(IC_LIST),
                " ",
                null,null,null,
                new Object[]{
                    "Se^quence", "i", IC_SEQUENCE,
                    b(CMD_CLIPB,0),
                    b(">A",0),
                    b("RANGE",0).i(IC_SCISSOR).t("Cut-ends, ^remove N- or C-terminus ..."),
                    b("1ST_IDX",0).t("Set index of ^1st residue ").i(IC_1),
                    "-",
                    new Object[]{
                        "i",IC_DNA,"^Nucleotide sequence",null,

                        new Object[]{
                            "^Translate nucleotides to amino acids",
                            b("RF",NT_or_ACTGN).tabItemTipIcon(EditDna.class),
                            b("GB",0).i(IC_DNA).tabItemTipIcon(CLASS_DialogGenbank),
                            CLASS_DialogInferCDS
                        },
                         bBT,
                        "-",
                        b(">T",NT).t("^Report Coding nucleotide sequence").tt("All translated nucleotides"),
                        b("REV_COMPL",NT_or_ACTGN).t("^Reverse complement"),
                        b(">D",NT).t("^Report sequence including introns and UTRs").tt("All nucleotides in current direction"),
                        b("EXON",NT).tabItemTipIcon(CLASS_DnaExonStart),
                        "-",
                        b("NO_DNA",NT).t("^Dispose nucleotide reading frame").i(IC_KILL)
                    },
                    new Object[]{
                        "i",IC_COMPARE,
                        "^Compare proteins",
                        CLASS_DialogCompareProteins,
                        CLASS_DialogPhylogeneticTree,
                        CLASS_DialogDotPlot
                    }
                },
                new Object[]{
                    "i", IC_3D,
                    "^3D structure", null,
                    new Object[] {
                        "i",IC_3D,
                        "^3D-Visualization",
                        b(ACTION_backbone,CALPHA).i(IC_3D).tt("Opens a new 3D-backbone view for protein[s] with c-alpha coordinates"),
                        CLASS_Dialog3DViewer,
                        bTO3D,
                        b("CLOSE_3D",IN_3D).t(ACTION_close3d).i(IC_CLOSE)
                    },
                    menuSuper,
                         new Object[]{
                        "^Associate 3D structure ...", "i", IC_3D,
                        CLASS_DialogSimilarStructure,
                        b("RM_3D", CALPHA).t("^Remove associated 3D-structure").i(IC_CLOSE),
                        "Note: This applies to sequence files which primarily come without 3D-information"
                    },

                    new Object[]{
                        "^Protein complex", "i",IC_PROTEASOME,
                        b("LIST_N", NUC3D).t("List ^nucleotide chain structures").i(IC_DNA),
                        b("LIST_H", HETERO3D).t("List ^hetero structures (i.e. ligands)").i(IC_FLAVIN),
                        b(">S",CALPHA).t("List positions of ^secondary structure elements").i(IC_HELIX),
                        b(IC_SCISSOR,CHAINS).tabItemTipIcon(PDB_separateChains.class),
                        b("1",CHAINS).t("^Only one certain chain ...").i(IC_1),
                        ChButton.doView(pnl(VBPNL,msgHet,pnl("See Tutorial ",Tutorials.b("copy_DNA")))).t("^Add hetero structure ...").i(IC_FLAVIN),
                        ChButton.doView(_CCP+"/defineComplexes.html").t("Define multi-protein complexes ...").i(IC_PROTEASOME),
                        b(">X",0).t("List protein objects joined to a complex").i(IC_LIST),
                    },
                    new Object[]{
                        "^Measure", "i",IC_MEASURE,
                        CLASS_DialogSelectionOfResiduesMain
                    },
                },
                new Object[] {
                    "^Properties", "i", IC_INFO,
                    "Get",
                    b("N2C",0).t("Copy protein Name").i(IC_CLIPBOARD),
                    b(">N",0).t("Protein ^Name").i(IC_INFO),
                    b(">I",0).t("^Accession ID").i(IC_INFO),
                    b(">H",0).t("^Header").i(IC_INFO),
                    b("INFO",0).t("Cross^links ...").i(IC_WWW),
                    "-",
                    "Modify",
                    EXEC_ALLOWED ? b("ED",0) : null,
                    b("RENAME",0).t("^Rename or Copy ...").i(IC_COPY),
                    helpBut(ChIcon.class).t("^Icon ...").i(IC_HAPPY),
                    !EXEC_ALLOWED ? null : b("DESCR",0).t("Balloon message ...").i(IC_TT).tt("The text appears when the mouse pointer is at the left of a protein label."),
                    "-",
                    "Visibility",
                    b( "H",IN_ALI).t("^Do not display protein in alignment").i(IC_CLOSE),
                    b("UH",NOT_IN_ALI).t("Display protein in alignment").i(IC_SHOW),
                    StrapAlign.button(StrapAlign.BUT_hiddenP),
                    "-",
                    "File",
                    !EXEC_ALLOWED ? null : new Object[]{  "E^xecute program", "i", IC_BATCH, bExec('f'),bExec('s'),bExec('g')},
                    "^f",_fp.setFiles(this).menuObjects(IC_DIRECTORY,"Protein file")
                },
                "^l",menuListResSel,
                "-",
                new Object[]{
                    "^Calculate",
                    "i", IC_COMPUTE,
                    new Object[]{
                        "Pre^dict","i",IC_HELIX,
                        CLASS_DialogPredictSecondaryStructures,
                        CLASS_DialogPredictTmHelices2,
                        CLASS_DialogPredictCoiledCoil,
                        CLASS_DialogSubcellularLocalization
                    },
                    b(">C",0).t("Amino acid ^composition").i(IC_BARCHART),
                    bBT,
                    "-",
                    new Object[]{"^Plot", "i",IC_PLOT, CLASS_DialogPlot, CLASS_DialogBarChart},
                  CLASS_DialogSelectionOfResiduesMain
                },
                menuSearch,
                pMenu('e',false),
                "-",

                "Deleted proteins are in "+file(dirWorking(),"trash"),
                b(IC_KILL,0).t("^Delete protein").i(IC_KILL).tt("To recover, open the './trash/'-folder and move the file back."),

                _menuBar?"&K":null,
                b(IC_CLOSE,0).t("^Close protein").i(IC_CLOSE).tt("The protein is removed without saving. The file is not deleted"),
                null
            };
            StrapAlign.replaceClassByButton(oo, this);
            _popup=jPopupMenu(_menuBar?MENU_HAS_ACCELERATOR:0, "Menu for protein", oo);
            if (_fp!=null) _popup.addPopupMenuListener(_fp); /* Update FilePopup title */
        }
        return _popup;
    }

    Object[] pMenu(char id, boolean longTitle) {
        if (id=='e') {
            return new Object[]{
                longTitle?"^Export proteins":"^Export",
                "i",IC_EXPORT,
                CLASS_DialogExportProteins,
                b(">A",0),
                b(CMD_EXPORT_DnD,0),
                "-",
                b(CMD_CLIPB,0),
                TT_CLIPB
            };
        }
        if (id=='E') {
            return new Object[]{
                "E^xport alignment",
                "i",IC_EXPORT,
                CLASS_DialogExportAlignment,
                CLASS_Texshade,
                CLASS_DialogPublishAlignment,
                CLASS_DialogExportWord,
                StrapAlign.button(StrapAlign.BUT_ZIP),
                "Hint: You may open a rubber band in the alignment panel with the mouse",
            };
        }
        if (id=='I') {
            final Object msg=pnl(VBHB,
                                 pnl(HBL,"You may import proteins by","WIKI:Drag_and_drop"),
                                 "Simply drag a protein file or protein web link into Strap.",
                                 WATCH_MOVIE+MOVIE_Load_Proteins
                                 );
            return new Object[]{
                longTitle?"A^dd proteins":"A^dd",
                "i", IC_ADD,
                null,
                new ChButton("OPEN").t("&Open file ...").li(this).i(iicon(IC_DIRECTORY)),
                "&N",CLASS_DialogNewProtein,
                CLASS_DialogImportMFA,
                ChButton.doView(msg).t("Load protein files by Drag-and-Drop ...").i(IC_DND),
                "-",
                CLASS_DialogFetchPdb,
                CLASS_DialogFetchSRS
            };
        }

        assrt();
        return null;
    }
    /* <<< Menu <<< */
    /* ---------------------------------------- */
    /* >>> Dialogs >>> */
    private void dialogFirstIdx(int go, Protein pp[]) {
        final boolean total=isSelctd(_cbFirstOfTotal);
        if (_taFirst==null) {
            (_taFirst=new ChTextArea("")).tools().enableUndo(true);
            StrapAlign.highlightProteins("P",_taFirst);
            pcp(KEY_NORTH_PANEL,"The table shows the index of the first residue for each protein. These indices can be changed.",_taFirst);
            _cbFirstOfTotal=toggl("Setting the first index of the total, i.e. non-cropped sequence").li(this);
            final Object
                pTotal=pnl(VBHB,
                          "The visible amino acid might be a subsequence of the total amino acid.<br>"+
                          "If the check-box is activated, the residue index of the total sequence is set.<br>"+
                           "If the visible amino acid sub-sequence does not start at the beginning it will also be changed.",
                          _cbFirstOfTotal.cb()
                          ),
                pSouth=pnl(VBHB,
                           pnl(new ChButton("1ST_IDX_GO").li(this).t("Apply")),
                           pnl(HBL,"Referring to the total amino acid sequence: ", _cbFirstOfTotal.t("").doCollapse(pTotal).doPack()),
                           pTotal);

            pcp(KEY_SOUTH_PANEL,pSouth,_taFirst);
        }
        if (go==0 || go==1) {
            final BA sb=new BA(999);
            final int maxL=sze(nam(longestName(pp)));
            for(Protein p : pp) {
                sb.a(' ',maxL-sze(p.toString())+1).a(p)
                    .a(p.getResidueIndexOffset()+1+(total?0:Protein.firstResIdx(p)-p.getResidueIndexOffset()), 7)
                    .a('\n');
            }
            _taFirst.t(sb.a('\n'));
        }
        if (go==1) ChFrame.frame("Change index of first residue.", _taFirst, CLOSE_CtrlW_ESC|ChFrame.PACK).shw(ChFrame.AT_CLICK);
        if (go==2) {
            for(String s : splitTokns(SPLIT_TRIM, toBA(_taFirst),0,MAX_INT, chrClas1('\n'))) {
                final int spc=nxt(SPC,s);
                final Protein p=spc<0?null:SPUtils.proteinWithName(s.substring(0,spc),pp);
                if (p!=null) {
                    final String name=toStrg(p), noExclam=delLstCmpnt(name,'!');
                    final int az[]=p.getFirstAndLastResidueIndex();
                    final int num=atoi(s,spc+1)-1;
                    if (name!=noExclam && !total) {
                        final String neu=noExclam+"!"+(num+1)+"-"+(az[1]-az[0]+num+1);
                        p.setResidueIndexOffset(num-az[0]+p.getResidueIndexOffset());
                        DialogRenameProteins.renameProtein(neu,  p, new BA(0));
                    } else p.setResidueIndexOffset(num);
                }
            }
            dialogFirstIdx(0, pp);
            StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_CHANGED);
            StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
            StrapEvent.dispatch(StrapEvent.PROTEIN_RENAMED);
        }
    }
    ChButton bExec(char t) {
        final ChButton b=
            t=='f' ?  b("EX_F",0).t("... on protein file ^names ...") :
            t=='s' ?  b("EX_S",0).t("... on amino acid ^sequences ...") :
            b("EX_G",0).t("... on ^gapped amino acid sequences ...");
        return b.i(IC_BATCH);
    }

    /* <<< Dialogs <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand(), label=txtForTitle(q);
        final List vSel=StrapAlign.selectedObjectsV();
        final int modiVsel=modic(vSel);
        final Protein[] pp=_objects.proteins(), ppVis=StrapAlign.visibleProteins();
        File ffResult[]=null;
        if (q==_fChooser && cmd==JFileChooser.APPROVE_SELECTION) {
            final File ff[]=_fChooser.getSelectedFiles();
            final BA sb=new BA(333);
            for(int i=sze(ff); --i>=0;) {
                final File f=ff[i], fDest=file(f.getName().replace(' ','_'));
                if (!toStrg(f).equals(toStrg(fDest))) cpy(f,fDest);
                sb.a(fDest).a(' ');
            }
            StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED, sb);
        }
        if (cmd=="OPEN") {
            if (_fChooser==null) {
                _fChooser=new ChFileChooser(ChFileChooser.BUT_PROJECT_FOLDER|ChFileChooser.MULTI, null).setFilters(Customize.customize(Customize.proteinFileExtensions));
                updateOn(CHANGED_PROTEIN_LABEL,_fChooser);
                _fChooser.addActionListener(this);
                setMinSze(99,99, _fChooser);
                addServiceR(StrapAlign.getInstance(), _fChooser, ChRunnable.RUN_MODIFY_RENDERER_COMPONENT);
                TabItemTipIcon.set("","Open file","Open file",IC_DIRECTORY, _fChooser);
            }
            StrapAlign.addDialog(_fChooser);
        }
        final int nP=pp.length;
        StrapEvent strEv=null;
        if (cmd==ACTION_showInList) {
            final String t="Proteins "+ ++_countJList;
            StrapAlign.showInJList(0L, null, pp, t, null).showInFrame(DISPOSE_CtrlW|ChFrame.DISPOSE_ON_CLOSE|ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN|ChFrame.ALWAYS_ON_TOP, t);
        }
        if (cmd=="RM_3D") {
            for(Protein p : pp) p.detach3DStructure();
            StrapEvent.dispatchLater(StrapEvent.ATOM_COORDINATES_CHANGED,1);
        }
        if (cmd=="SS" || cmd=="SA" || cmd=="SF") {
            for(Protein p:pp) {
                for(ResidueSelection s : p.allResidueSelections()) {
                    final char mID=ContextObjects.menuID(s);
                    if (mID=='A' && cmd=="SA" || mID=='S' && cmd=="SS" || mID=='F' && cmd=="SF") {
                        if (isShift(ev)) vSel.remove(s); else vSel.add(s);
                    }
                }
            }
        }
        if (nP==0) return;
        final String cancelMsg=
            cmd=="RF"     && nP>5 ? "Open nucleotide panel for "+nP+" sequences?" :
            cmd=="NEW_RA" && nP>1 ? "Create residue annotations for "+nP+" sequences?" :
            //cmd=="VIEW"   && nP>3 ? "View "+nP+" protein files?" :
            null;
        if (cancelMsg!=null && !ChMsg.yesNo(cancelMsg)) return;
        if (cmd=="FEATURES") SequenceFeatures.instance().showPanel();
        final Class cChild=
            cmd=="LIST_S" ? ResidueSelection.class :
            cmd=="LIST_A" ? ResidueAnnotation.class :
            cmd=="LIST_F" ? SequenceFeatures.class :
            cmd=="LIST_H" ? HeteroCompound.class :
            cmd=="LIST_N" ? HeteroCompound.class :
            null;
        if (cChild!=null) SPUtils.showChildObjects(cChild, cmd=="LIST_H"?'H':cmd=="LIST_N"?'N':0,  pp);
        if (cmd==CMD_HL_DIFF) {
            if (pp.length<2) { error(label+"\nError: Select at least 2 proteins"); return; }
            final boolean[][] bb=new boolean[pp.length][];
            final byte[][] gg=new byte[pp.length][];
            for(int col=0; ;col++) {
                byte letter=0;
                boolean end=true;
                for(int iP=0; iP<pp.length && letter!=-1; iP++) {
                    if (gg[iP]==null) gg[iP]=pp[iP].getGappedSequenceExactLength();
                    final byte c=get(col,gg[iP]);
                    if (c!=0) end=false;
                    if (is(LETTR,gg[iP], col)) {
                        if (letter==0) letter=c;
                        else if ( (letter|32)!=(c|32)) letter=-1;
                    }
                }
                if (end) break;
                if (letter!=-1) continue;
                for(int iP=pp.length; --iP>=0;) {
                    if (!is(LETTR, gg[iP], col)) continue;
                    final int iA=pp[iP].column2nextIndexZ(col);
                    if (iA>=0) {
                        if (iA>=sze(bb[iP])) bb[iP]=chSze(bb[iP],iA+1);
                        bb[iP][iA]=true;
                    }
                }
            }
            final String title="Different res";
            final ResidueAnnotation aa[]=new ResidueAnnotation[pp.length];
            nextP:
            for(int iP=pp.length; --iP>=0;) {
                if (bb[iP]==null) continue;
                final String pos=Protein.selectedPositionsToText(false, bb[iP], 0, pp[iP]);
                if (pos==null) continue;
                for(ResidueAnnotation a : pp[iP].residueAnnotations()) {
                    if (title.equals(a.getName()) && pos.equals(a.value(ResidueAnnotation.POS))) continue nextP;
                }
                pp[iP].addResidueSelection(aa[iP]=new ResidueAnnotation(pp[iP]));
                aa[iP].addE(0,ResidueAnnotation.POS, pos);
                aa[iP].addE(0,ResidueAnnotation.GROUP,title);
                aa[iP].addE(0,ResidueAnnotation.NAME,title);
                aa[iP].setColor(C(0xFF0000));
            }

            if (countNotNull(aa)>0) {
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,333);
                StrapAlign.showInJList(0L,null, aa,title,null).showInFrame(ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN|ChFrame.ALWAYS_ON_TOP, title);

            }
        }
        if (cmd=="NEIGHB") {
            final JComponent dia=StrapAlign.addDialogC(CLASS_DialogNeighbors);
            SPUtils.dialogSetProteins(ProteinList.NO_PDB_ID, pp, dia);
            runR(((Runnable)dia));
        }
        if (cmd=="TO3D") {
            boolean success=false;
            for(Protein p:pp) {
                success|=V3dUtils.residueSelectionsTo3D(V3dUtils.RSto3D_COMMANDS|V3dUtils.RSto3D_COLOR|V3dUtils.RSto3D_NO_WIRE|V3dUtils.RSto3D_SET_FOCUSED_PV,
                                                        p.residueAnnotations(), p.getProteinViewers());
            }
            if (!success) error("No 3D-commands to send to the 3D viewer");
        }

        if (cmd=="GB") runR(SPUtils.dialogSetProteins(pp, StrapAlign.addDialogC(CLASS_DialogGenbank)));
        if (cmd=="NO_DNA") {
            final int N=ProteinList.onlyProteins(ProteinList.NT,pp).length;
            if (ChMsg.yesNo("Discard all reading frame and intron/exon information of "+N+" sequences?")) {
                for(Protein p:pp) p.selectCodingStrand(Protein.NO_TRANSLATE);
                strEv=new StrapEvent(this,StrapEvent.NUCL_TRANSLATION_CHANGED);
            }
        }
        if (q instanceof PDB_separateChains) ffResult=((PDB_separateChains)q).getCreatedFiles();

        if (cmd==IC_SCISSOR) {
            final List<File> v=new ArrayList();
            for(Protein p:pp) adUniq(p.getFile(),v);
            final PDB_separateChains psc=new PDB_separateChains();
            addActLi(this,psc);
            psc.interactiveProcess(toArry(v, File.class));
        }
        if (cmd=="RESET3D") {
            for(Protein p:pp) p.setRotationAndTranslation(null);
            strEv=new StrapEvent(ProteinPopup.class,StrapEvent.PROTEIN_3D_MOVED);
        }
        final char outID=chrAt(0,cmd)=='>' ? chrAt(1,cmd) : 0;
        if (is(UPPR,outID)) {
            String title=label, text0=null;
            final CharSequence tip=q instanceof JComponent ? rmHtmlHeadBody(((JComponent)q).getToolTipText()) : null;
            boolean ntSeq=false, aaSeq=true;
            final BA sb=new BA(1000);
            final int maxL=sze(nam(longestName(pp)));
            if (cmd==">C") {
                title="AA composition";
                text0="Also see MOL_TOOLKIT:procomp \n\n";
                for(int isAbsolute=0; isAbsolute<2; isAbsolute++) {
                    sb.a("Amino acid composition (").a(isAbsolute>0 ? "absolute values":"percent").aln(")");
                    for(Protein p:pp) {
                        sb.a(' ', maxL-sze(toStrg(p))).a(p).a(' ',3);
                        final byte rt[]=p.getResidueType();
                        final int nR=p.countResidues();
                        if (nR==0) continue;
                        for(char c='A'; c<='Y'; c++) {
                            if (c=='B'||c=='J'||c=='O'||c=='U'||c=='X') continue;
                            int count=0;
                            for(int iA=nR; --iA>=0;) if ((rt[iA]&~32)==c) count++;
                            if (isAbsolute>0) sb.a(count,5); else sb.a(100f*count/nR, 3,1);
                            sb.a(c);
                        }
                        sb.a('\n');
                    }
                    sb.a('\n');
                }
            }
            if (cmd==">S") {
                text0="List of  WIKI:Protein_structure#Secondary_structure recorded in PDB and DSSP files.\n"+
                    "Note: The prediction from amino acid sequences  is not shown here.\n\n";
                for(Protein p:pp){
                    sb.a(p);
                    final byte ss[]=p.getResidueSecStrType();
                    if (ss==null) { sb.a(" No structure information"); continue;}
                    boolean bb[]=new boolean[ss.length];
                    for(char c : new char[]{'H','E','C'}) {
                        for(int i=ss.length; --i>=0; ) bb[i]= c==ss[i];
                        sb.a(c=='H'?"\n  HELICES ":c=='E'?"\n EXTENDED ":"\n   OTHER ").a(Protein.selectedPositionsToText(bb,0,p));
                    }
                    sb.a('\n',2);
                }
            }
            if (cmd==">N")  sb.a(nP).aln(" proteins").join(pp).a('\n',2);
            if (cmd==">I")  {
                for(Protein p : pp) {
                    sb.a(p).a(" acc=").a(p.getAccessionID())
                        .a("  refs={ ").join(p.getSequenceRefs()," ")
                        .a(" }  uniprot=").a(p.getUniprotID())
                        .a("   uniprot_by_identity={ ").join(FindUniprot.ids(p)," ")
                        .a(" }  pdb=").aln(p.getPdbID())
                        .a('\n');
                }
            }
            if (cmd==">H") {
                for(Protein p:pp) sb.a('>').aln(p).a(p.getCompound()).a('\n',3);
            }
            if (cmd==">A") {
                aaSeq=true;
                for(Protein p:pp) sb.a('>').aln(p).foldText(p.getResidueTypeAsString(),60,0).a('\n',2);
            }
            if (cmd==">X") {
                for(Protein p : pp) sb.a(' ',maxL-sze(p.toString())+1).a(p).a(' ',2).join(p.getProteinsSameComplex(), " ").a('\n',2);
            }
            if (cmd==">D") {
                text0=label+"\n"+tip;
                for(Protein p:pp) {
                    final byte[] nn=p.getNucleotidesCodingStrand();
                    if (sze(nn)>0) sb.aln(p).filter(FILTER_FOLD,50, nn).a('\n',2);
                }
            }
            if (cmd==">T") {
                ntSeq=true;
                for(Protein p : pp) {
                    final byte[] bb=DNA_Util.onlyTranslated(p.getNucleotidesCodingStrand(),p.isCoding());
                    if (bb==null) continue;
                    DNA_Util.firstCodonToUpperCase(bb);
                    sb.aln(p).filter(FILTER_FOLD, 50, bb).a('\n',2);
                }
            }
            ChTextArea ta=TA[outID];
            if (ta==null) {
                StrapAlign.highlightProteins("P",ta=TA[outID]=new ChTextArea(""));
                if (aaSeq) pcp(KOPT_CONTAINS_AA, "", ta);
                if (ntSeq) pcp(KOPT_CONTAINS_NT, "", ta);
            }
            sb.and(text0,"\n").a('\n');
            final File f=newTmpFile(cmd==">A"?".fasta":".txt");
            wrte(f,sb);
            ta.a(sb).tools().cp(KEY_SOUTH_PANEL,pnl(f)).showInFrame(title);

        }
        if (cmd==IC_KILL || cmd==IC_CLOSE) {
            final int row=StrapAlign.cursorRow(), col=StrapAlign.cursorColumn();
            if (cmd==IC_CLOSE || ChMsg.yesNo(pnl(VBPNL, nP<5 ? new BA(0).join(pp):"", pnl(plrl(nP,"Kill the %N proteins and move their files to "),"./trash/"," ?")))) {
                StrapAlign.rmProteins(cmd==IC_KILL, pp);
                final StrapView v=StrapAlign.alignmentPanel();
                if (v!=null) v.setCursor(StrapView.CURSOR_COLUMN,v.proteinInRow(row), col);
            }
        }
        if (cmd=="EX_F") {
            String files[]=new String[nP]; for(int i=0;i<nP;i++) files[i]=rplcToStrg(" ","%20",toStrg(pp[i].getFile()));
            if (_execF==null) _execF=new DialogExecuteOnFile(new Customize("execProtFile",custSettings(Customize.textEditors),"shell programs for protein files",Customize.LIST));
            _execF.show(files);
        }
        if (cmd=="EX_S") {
            final String ss[]=new String[nP];
            for(int i=0;i<nP;i++) {
                if (pp[i]!=null) ss[i]=pp[i]+"\t"+pp[i].getResidueTypeAsString();
            }
            new DialogExecuteOnFile(Customize.customize(Customize.execAminoSeq)).show(ss);
        }
        if (cmd=="EX_G") {
            final String ss[]=new String[nP];
            for(int i=0;i<nP;i++) if (pp[i]!=null)  ss[i]=pp[i]+"\t"+toStrg(pp[i].getGappedSequenceExactLength());
            if (_execG==null) _execG=new DialogExecuteOnFile(new Customize("execGappedSeq",custSettings(Customize.execAminoSeq),"shell programs for gapped sequences",Customize.LIST));
            _execG.show(ss);
        }
        if (cmd=="BT") {
            final ChCombo combo=new ChCombo(Backtranslate.ORGANISMS);
            final File dir=file(STRAPOUT+"/toDNA");
            mkdrsErr(dir);
            final Object msg=pnl(VBPNL,
                                 "The Backtranslated protein files will be written to",
                                 dir,
                                 "Codon usage of ",
                                 combo,
                                 " ",
                                 monospc("Also see MOL_TOOLKIT:rtranslate"),
                                 "Backtranslate the selected proteins?"
                                 );
            if (ChMsg.yesNo(msg)) {
                final Backtranslate.Codonusage usage=Backtranslate.codonUsage(combo.toString());
                if (usage==null) { return;}
                final File ff[]=ffResult=new File[pp.length];
                for(int iP=pp.length;--iP>=0;) {
                    final byte nt[]=Backtranslate.backtranslate(pp[iP].getResidueType(),pp[iP].countResidues(),usage,false);
                    for(int i=nt.length; --i>=0;) if (i%3==0) nt[i]&=~32; else nt[i]|=32;
                    wrte(ff[iP]=file(dir+"/"+pp[iP]+".toDNA"),">"+pp[iP]+"\r\n"+toStrg(nt)+"\r\n");
                }
            }
        }

        if (cmd=="1" || cmd=="RANGE" || cmd=="RENAME") {
            DialogRenameProteins.instance(ProteinPopup.class)
                .rename(cmd=="1"||cmd=="RANGE"?DialogRenameProteins.INFO_CHAIN_LETTER:DialogRenameProteins.GENERATE,pp,null);
        }
        if (cmd==CMD_EXPORT_DnD) {
            if (_msgDnD==null) {
                final ChButton b=StrapAlign.button(StrapAlign.BUT_DRAG_OPTS);
                _msgDnD=pnl(VBHB,
                            pnl(HBL,"You may export proteins by","WIKI:Drag_and_drop"),
                            WATCH_MOVIE+MOVIE_Export_Proteins,
                            pnl("You can change the export options: ",b.cln().like(b))
                            );
            }
            ChFrame.frame("D&D",_msgDnD,0).shw(ChFrame.AT_CLICK|ChFrame.PACK);
        }
        if (cmd==CMD_CLIPB) {
            final BA sb=new BA(999);
            for(Protein p:pp) {
                final byte aa[]=p.getResidueType();
                if (aa==null) continue;
                sb.a('>').aln(p).aln(aa);
            }
            toClipbd(sb);
        }
        if (cmd==ACTION_backbone) StrapAlign.new3dBackbone(0,pp);
        if (cmd=="REV_COMPL") {
            final File dir=file(STRAPOUT+"/rc");
            mkdrsErr(dir);
            final File ff[]=ffResult=new File[pp.length];
            for(int iP=pp.length;--iP>=0;) {
                final byte[] nn=pp[iP].getCharacters();
                final String fn=pp[iP]+".rc";
                final BA ba=new BA(nn.length+100).a('>').aln(fn).aln(DNA_Util.reverse(DNA_Util.complement(nn,MAX_INT), MAX_INT));
                wrte(ff[iP]=file(dir+"/"+fn),ba);
            }
        }

        if (cmd== "H") StrapAlign.setIsInAlignment(false, 99, pp);
        if (cmd=="UH") StrapAlign.setIsInAlignment( true, 99, pp);
        if (cmd=="INFO") SPUtils.showInfo(pp);
        if (cmd=="N2C") toClipbd(new BA(99).join(pp," "));
        if (q==_cbFirstOfTotal) dialogFirstIdx(0,pp);
        if (cmd=="1ST_IDX") dialogFirstIdx(1,pp);
        if (cmd=="1ST_IDX_GO") dialogFirstIdx(2,pp);
        for(Protein p:pp) {
            if (p==null) continue;
            if (cmd=="RF") ((EditDna)StrapAlign.addDialogC(EditDna.class)).addEditor(p,true);
            if (cmd=="ED") edFile(-1,p.getFile(), modifrs(ev));
            if (cmd=="NEW_RA") {
                final int pos=
                    Protein.firstResIdx(p)+
                    (StrapAlign.cursorProtein()==p ? StrapAlign.indexOfAminoAcidAtCursorZ(p)+1 : 10);
                final ResidueAnnotation a=new ResidueAnnotation(p);
                a.addE(0,ResidueAnnotation.POS,pos+"-"+(pos+10));
                a.addE(0,ResidueAnnotation.GROUP,"created in context-menu of proteins");
                p.addResidueSelection(a);
                StrapAlign.editAnnotation(a);
            }
            if (cmd=="CLOSE_3D") V3dUtils.disposeV(p.getProteinViewers());
            if (cmd=="DESCR") {
                final File f=Protein.strapFile(Protein.mBALLON, p, null);
                if (!f.exists()) wrte(f,"");
                edFile(-1, f, modifrs(ev));
            }

            if (cmd=="EXON") {
                if (ChMsg.yesNo(pnl("Create residue annotation at each ","WIKI:Exon"," start? ",smallHelpBut(DnaExonStart.class)))) DnaExonStart.mark(pp);
            }
            if (cmd=="LINK_EC") visitURL("http://www.expasy.org/cgi-bin/nicezyme.pl?"+get(0,p.getEC()),modifrs(ev));
        }
        runR(strEv);
        if (modiVsel!=modic(vSel)) new StrapEvent(this,StrapEvent.OBJECTS_SELECTED).run();
        if (countNotNull(ffResult)>0) {
            final ChJList jl=new ChJList(ffResult,ChJList.OPTIONS_FILES|ChJTable.DEFAULT_RENDERER);
            pcp(KEY_SOUTH_PANEL,"<sub>Hint: Use drag'n drop to load the sequence files into the alignment pane.<br>Right click for context menu.</sub>",jl);
            jl.showInFrame(ChFrame.AT_CLICK, label);
        }
    }
}
