package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class CsaWeb {
    private final static WeakHashMap<Protein,String> vAlreadyCSA=new WeakHashMap();
    private CsaWeb(){}
    public static void load(Protein pp[], ChRunnable log) {
        for(int iP=0;iP<pp.length;iP++) {
            final Protein p=get(pp[iP],Protein.class);
            if (p==null) continue;
            synchronized(mkIdObjct("CSAW$$syncp",p)) {
                if (vAlreadyCSA.get(p)!=null) continue;
                if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(CsaWeb.class,(iP+1)*100/(pp.length+1),"Catalytic_Site_Atlas: "+p+" ... "));
                vAlreadyCSA.put(p,"");
                final String pdb=p.getPdbID();
                if (p==null || pdb==null) continue;
                final java.net.URL url=url("http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/CSA/CSA_Site_Wrapper.pl?pdb="+ pdbID(pdb));
                final java.io.File f=urlGet(url, SequenceFeatures.MAX_DAYS);
                final String txt[]=readLines(f);
                if (txt==null) continue;
                String alignmentOn=null;
                final List<ResidueAnnotation> vAnno=new ArrayList();
                boolean foundBy=false,literatureReference=false;
                for(String line : txt) {
                    if (line==null) continue;
                    line=line.toUpperCase();
                    if (line.startsWith("FOUND BY:"))  foundBy=true;
                    if (!foundBy) continue;
                    literatureReference|=line.indexOf("LITERATURE REFERENCE")>0;
                    if (line.indexOf("ALIGNMENT ON")>0) {
                        final int gt=line.indexOf('>'),lt=line.indexOf('<',gt+1);
                        if (gt>0 && lt>0) alignmentOn=line.substring(gt+1,lt);
                    }
                    if (line.startsWith("<TD>") && !line.startsWith("<TD><B>") ) {
                        final String ss[]=line.split("<TD>");
                        if (ss.length<4) continue;
                        final String resType=ss[1].trim(), chain0=ss[2].trim(), resNum=ss[3].trim(), chain=sze(chain0)==0 ? "?" : chain0;
                        if (sze(resType)!=3 || Protein.toOneLetterCode(chrAt(0,resType),chrAt(1,resType),chrAt(2,resType))=='X') continue;
                        final ResidueAnnotation a=new ResidueAnnotation(p);
                        a.setValue(0,ResidueAnnotation.NAME, (literatureReference ? "CATALYTIC_SITE" : "catalytic_site ")+resType);
                        a.setValue(0,ResidueAnnotation.POS,resNum+":"+chain);
                        // if (myComputer()) putln("CsaWeb: "+a.getValue(ResidueAnnotation.NAME)+"   "+a.getValue(ResidueAnnotation.POS)+
                        //                         "            # ",a.getSelectedAminoacids()," +"+a.getSelectedAminoacidsOffset());
                        vAnno.add(a);
                        SequenceFeatures.setFN(FEATURE_ACTIVE_SITE, SequenceFeatures.SRC_CSA, a);
                        continue;
                    }
                    if (line.indexOf("</TABLE>")>=0 && sze(vAnno)>0) {
                        for(ResidueAnnotation a : toArry(vAnno,ResidueAnnotation.class)) {
                            a.addE(0,ResidueAnnotation.GROUP,"CSA");
                            a.addE(0,ResidueAnnotation.NAME, "Catalytic_Site_Atlas");
                            a.setFeatureSrc("CATALYTIC_SITE_ATLAS",pdb);
                            if (countTrue(a.getSelectedAminoacids())>0) {                                
                                a.addE(0,ResidueAnnotation.VIEW3D, ProteinViewer.COMMANDsticks);
                                a.addE(0,ResidueAnnotation.HYPERREFS," PDB_CATALYTIC_SITE:"+pdbID(pdb));
                                a.addE(0,"Remark"," Cat Site Atlas");
                                if (literatureReference) {
                                    a.addE(0,"Note","from literature");
                                    pcp(Protein3d.KEY_INITIALLY_SHOW,"",a);
                                }
                                if (alignmentOn!=null) a.addE(0,"Note","indirectly, by alignment to "+alignmentOn);
                                pcp(FEATURE_ACTIVE_SITE,"",a);
                            }
                            SequenceFeatures.addFeature(a,p, null);
                        }
                        vAnno.clear();
                        foundBy=literatureReference=false;
                        alignmentOn=null;
                    }
                }
            }
        }
        if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(CsaWeb.class,0,"CatalyticSiteAtlas: "+pp.length+" proteins done"));
    }
}
