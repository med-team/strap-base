package charite.christo.strap;
import java.util.*;
import java.io.File;
import javax.swing.*;
import charite.christo.*;
import charite.christo.strap.extensions.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class SPUtils implements Comparator, ChRunnable {

    public final static int
        INFER3D_SHOW_BACKBONE=1<<3, INFER3D_IF_NOT_ALREADY=1<<4, INFER3D_SET_FILE=1<<5,
        ALIGN_CHANGE_ORDER=1<<1,
        SUPERIMP_EVENT=1<<1, SUPERIMP_COMPLEX_BEST=1<<2;
    public final static String INFER3D_AUTO[]=new String[0];
    private static SPUtils _inst;
    private static SPUtils instance() { return _inst==null ? _inst=new SPUtils() : _inst;}
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="INF_3D" && _inf3DRunning==0 && sze(_inf3DQueue)>0) {
            final Object[] pAndId=(Object[])get(0,_inf3DQueue);
            if (pAndId!=null) {
                _inf3DQueue.remove(0);
                for(Runnable t : threadInferCoordinates(0, new Protein[]{(Protein)pAndId[0]},new String[]{(String)pAndId[1]})) startThrd(t);
            }
        }
        if (id=="classChoice") argv[1]=classChoice((Class)argv[0]);
        return null;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> 3D for one >>> */
    private static List _inf3DQueue=new Vector();
    private static int _inf3DRunning;
    private static boolean _startedInf3D;
    public static void inferCoordinates1(Protein p,String pdbID) {
        if (!_startedInf3D) { _startedInf3D=true; ChThread.callEvery(0, 222, thrdCR(instance(),"INF_3D"),"INF_3D"); }
        _inf3DQueue.add(new Object[]{p,pdbID});
    }
    /* <<< 3D for one <<< */
    /* ---------------------------------------- */
    /* >>> 3D >>> */
    private final static Object SYNC_map=new Object();
    private static Object _mapF3d;

    public static Runnable[] threadInferCoordinates(int options, Protein pp[], String[] pdbID) {
        return new Runnable[] {
            thrdM("inferCoordinates",SPUtils.class, new Object[]{intObjct(options), pp, pdbID, CLASS_Blaster_SOAP_ebi}),
            thrdM("inferCoordinates",SPUtils.class, new Object[]{intObjct(options), pp, pdbID, CLASS_Blaster_web_ncbi})
        };
    }
    public static void inferCoordinates(int options, Protein pp[], String[] pdbID, String clazz0) {
        final String INFO="INFERING 3D-COORIDINATES\n\n"+
            "Downloading pdb files from the pdb-server.\n"+
            "With the help of WIKI:ClustalW, the 3D-coordinates will be assigned to the corresponding residues.\n",
            TITLE="Infer 3D coordinates";
        final BA log=logInfer3D();
        _inf3DRunning++;
        try {
            Map<String,Protein[]> mapF3d;
            synchronized(SYNC_map) {
                mapF3d=deref(_mapF3d,Map.class);
                if (mapF3d==null) _mapF3d=newSoftRef(mapF3d=new HashMap());
            }
            int count=0;
            final Protein[][] pp3d_array=new Protein[pp.length][];
            final String clazz=orS(clazz0, CLASS_Blaster_SOAP_ebi);
            for(int idx=pp.length; --idx>=0;) {
                final int iP=clazz==CLASS_Blaster_SOAP_ebi ? idx :pp.length-1-idx;
                final Protein p=pp[iP];
                if (p==null || 0!=(options&INFER3D_IF_NOT_ALREADY) && p.getResidueCalpha()!=null) continue;
                count++;
                final String pdb_chain=pdbID!=INFER3D_AUTO ? get(iP,pdbID) : sze(p.getPdbID())>0 ? p.getPdbID() : getBestPdbID(p, clazz);
                if (sze(pdb_chain)==0) continue;
                final char chain=pdbChain(pdb_chain);
                Protein[] pp3d;
                synchronized(SYNC_map) { pp3d=mapF3d.get(pdb_chain);}
                if (pp3d==null) {
                    pp3d=StrapAlign.downloadPdbAndCreateProteins(pdbID(pdb_chain), chain, StrapAlign.proteins());
                    log.a("downloaded:  ").a(pdb_chain).join(pp3d,",").a('\n').send();
                    if (sze(pp3d)==0) continue;
                    synchronized(SYNC_map) { mapF3d.put(pdb_chain,pp3d); }
                }
                pp3d_array[iP]=pp3d;
            }
            if (count>0) {
                inEDT(thrdM("infc_Blocking", SPUtils.class,  new Object[]{intObjct(options), pp, pp3d_array, log}));
            }
        } finally{ _inf3DRunning--;}
    }

    private static String getBestPdbID(Protein p, Object clazz) {
        final SimilarPdbFinderBlast sim=new SimilarPdbFinderBlast();
        sim.setProtein(p);
        sim.setBlaster(clazz);
        final SimilarPdbFinder.Result rr[]=sim.getSimilarStructures();
        return sze(rr)==0 ? null : rr[0].getPdbID();
    }

    private static String _inf3DInfo=
        "\nNow you can view these proteins three-dimensionally.\n"+
        "Place the alignment cursor on the respective protein and press the \"3d-structure\" toolbar.\n"+
        "Use Drag'n Drop to move more proteins into the same view.\n\n\n";
    public static boolean infc_Blocking(int options, Protein pp[],Protein[][] pp3d_array, BA log) {

        final boolean doLog=isEnabld(log);
        int success=0;
        for(int iP=pp.length; --iP>=0;) {
            final Protein p=pp[iP];
            if (p==null) continue;
            final Protein[] pp3d=pp3d_array[iP];
            if (pp3d==null) continue;
            final float score=p.inferCoordinates(pp3d, name2class(AlignUtils.defaultAligner2Class()), log);
            if (p.getFile()==null && 0!=(options&INFER3D_SET_FILE)) p.setFile(p.getFileMayBeWithSideChains());
            if (isEnabld(log)) log.a(p).a("  Align-score=").a(score).aln(Float.isNaN(score)?" failure":"").send();
            if (!Float.isNaN(score)) {
                //p.setOnlyChains(String.valueOf(chain));
                success++;
            }
        }
        if (doLog) log.a('\n').send();
        if (success>0) {
            ChMsg.soundBG(ChMsg.SOUND_SUCCESS);
            if (doLog && _inf3DInfo!=null) { log.a(_inf3DInfo).send(); _inf3DInfo=null; }
            if ( (options&INFER3D_SHOW_BACKBONE)!=0) StrapAlign.new3dBackbone(0,pp);
        }

        new StrapEvent(SPUtils.class, StrapEvent.ATOM_COORDINATES_CHANGED).run();
        return success>0;
    }
    /* <<< Infer coordinates <<< */
    /* ---------------------------------------- */
    /* >>> Alignment  >>> */
    private final static void rm(boolean[] shouldStop, Object aligner) {
        if (shouldStop==CAN_BE_STOPPED) {
            CanBeStopped.vALIGNMENTS.remove(shouldStop);
            CanBeStopped.vALIGNMENTS.remove(aligner);
        }
    }
    private static boolean[] isInterrupted(boolean shouldStop[], Object aligner) {
        if (shouldStop==CAN_BE_STOPPED) {
            final boolean[] bb={false};
            CanBeStopped.vALIGNMENTS.add(bb);
            CanBeStopped.vALIGNMENTS.add(aligner);
            return bb;
        } else return shouldStop;
    }

    public static SequenceAligner alignerInstance() {
        final SequenceAligner sa=StrapPlugins.instanceSA(StrapAlign.defaultClass(SequenceAligner.class), 10, true);
        if (sa instanceof MultiFromPairAligner) {
            ((MultiFromPairAligner)sa).set3dMethod(name2class(StrapAlign.defaultClass(SequenceAligner3D.class)));
        }
        if (sa==null) {
            assrt();
            return
                Insecure.EXEC_ALLOWED ? new Aligner3D() :
                new MultiFromPairAligner(PairAlignerNeoBioPROXY.class).set3dMethod(Superimpose_CEPROXY.class);
        }
        return sa;
    }
    public static Runnable threadAlignProteins(int options, SequenceAligner aligner,List<Protein> vP, boolean shouldStop[]) {
        return thrdM("alignProteins",SPUtils.class, new Object[]{intObjct(options), aligner, vP, shouldStop});
    }
    public static SequenceAligner alignProteins(int options, SequenceAligner aligner0, List<Protein> vP, boolean shouldStop[]) {
        final Protein pp[]=spp(vP);
        if (sze(pp)==0) return null;
        final SequenceAligner aligner=aligner0!=null ? aligner0 : alignerInstance();
        final boolean isInterrupted[]=isInterrupted(shouldStop,aligner);
        setSeqs(aligner, pp);
        addActLi(StrapAlign.li(),aligner);
        aligner.setSequences(residueTypeArray(pp));
        aligner.compute();
        CanBeStopped.vALIGNMENTS.remove(aligner);
        final byte[][] aligned=aligner.getAlignedSequences();
        if (sze(aligned)==0) putln("Error: SPUtils.alignProteins(): aligned=null");
        else if (aligned.length<pp.length) putln("Error: SPUtils.alignProteins(): aligned.length="+aligned.length+" pp.length="+pp.length);
        else {
            if (aligner instanceof MultiFromPairAligner) ((MultiFromPairAligner)aligner).setIsInterrupted(isInterrupted);
            new AcceptAlignment2(pp,0,aligned);
            final int ii[]=0!=(options&ALIGN_CHANGE_ORDER) && aligner instanceof SequenceAlignerSorting ? ((SequenceAlignerSorting)aligner).getIndicesOfSequences(): null;
            if (sze(ii)>=pp.length) {
                for(int i=ii.length; --i>=0;) pp[i].setPreferedOrder(ii[i]);
                StrapAlign.inferOrderOfProteins(StrapAlign.SORT_PREFORDER, pp);
            }
            StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_CHANGED,1);
        }
        rm(shouldStop,aligner);
        return aligner;
    }
    /* <<< Alignment <<< */
    /* ---------------------------------------- */
    /* >>> Superimpose >>> */
    private static String _method3D;
    private synchronized static Superimpose3D.Result superimposeTwoProteins(int options, Protein pM,Protein pR) {
        if (pM==null || pR==null) return null;

        if (0!=(options&SUPERIMP_COMPLEX_BEST) && sze(pM.getProteinsSameComplex())+sze(pR.getProteinsSameComplex())>0) {
            return superimposeTwoComplexes(pM, pR);
        }
        final String method=StrapAlign.defaultClass(Superimpose3D.class);
        Superimpose3D.Result r=method==_method3D ? pM.hashTMalign().get(pR) : null;
        _method3D=method;
        if (r==null) {
            r=superimposeTwoProteinsM( method,pM,pR);
            pM.hashTMalign().put(pR,r);
        }
        return r;
    }

    private static Superimpose3D.Result superimposeTwoComplexes(Protein pMobile, Protein pRef) {
        final Protein ppM[]=pMobile.getProteinsSameComplex();
        final Protein ppR[]=pRef.getProteinsSameComplex();
        final int N=(sze(ppM)+1)*(sze(ppR)+1);
        Superimpose3D.Result rBest=null;
        float scoreBest=Float.NaN;
        int sumLen=0;
        for(int pass=2; --pass>=0;) {
            for(int i=sze(ppM); --i>=-1;) {
                for(int j=sze(ppR); --j>=-1;) {
                    final Protein pM=i>=0?ppM[i]:pMobile;
                    final Protein pR=j>=0?ppR[j]:pRef;
                    final int len=mini(pM.countCalpha(),pR.countCalpha());
                    if (pass!=0) sumLen+=len;
                    else {
                        StrapAlign.drawMessage(pM+" ./. "+pR);
                        final Superimpose3D.Result r=superimposeTwoProteins(0, pM,pR);
                        if (r==null) continue;
                        final float score=r.getScore()*len/(sumLen/(float)N);
                        if (rBest==null || score>scoreBest && !Float.isNaN(score)) {
                            rBest=r;
                            scoreBest=score;
                        }
                    }
                }
            }
        }

        return rBest;
    }

    public synchronized static Superimpose3D.Result superimposeTwoProteinsM(Object clazz,Protein pM,Protein pR) {
        if (pM==null || pR==null || pM.getResidueCalpha()==null || pR.getResidueCalpha()==null) return null;
        final Superimpose3D s= (Superimpose3D)(clazz instanceof Superimpose3D ? clazz : mkInstance(clazz,Superimpose3D.class,true));
        if (s==null) return null;
        s.setProteins(pR,pM);
        CanBeStopped.vALIGNMENTS.add(s);
        s.compute();
        CanBeStopped.vALIGNMENTS.remove(s);
        final Superimpose3D.Result result=s.getResult();
        if (result==null) putln("Error in SPUtils#superimposeTwoProteinsM result==null for pM=",pM," pR ",pR);
        return result;
    }

    public final static boolean SUPERIMPOSING[]={false};
    private static int _countSP;
    public static Runnable threadSuperimposeProteins(int options, List<Protein> vP,boolean isInterrupted[], Protein[] pBest) {
        return thrdMR("superimposeProteins",SPUtils.class, new Object[]{intObjct(options),vP,isInterrupted}, pBest);
    }
    public static Protein superimposeProteins(int options, List<Protein> vP, boolean shouldStop[]) {
        final boolean[] isInterrupted=isInterrupted(shouldStop, null);
        final Protein pp[]=spp(vP);
        SUPERIMPOSING[0]=true;
        Protein pBest=null;
        float bestScore=Float.MIN_VALUE;
        long timePG=0;
        final int N=pp.length*pp.length;
        int count=0;
        for(Protein pi : pp) {
            float scoreSum=0;
            for(Protein pj : pp) {
                if (get(0,isInterrupted)) break;
                ++count;
                if (pi!=null && pj!=null && pi!=pj && pi.getResidueCalpha()!=null && pj.getResidueCalpha()!=null) {
                    final Superimpose3D.Result r=superimposeTwoProteins(options, pi,pj);
                    if (r!=null) scoreSum+=r.getScore();
                    if (System.currentTimeMillis()-timePG>200) {
                        timePG=System.currentTimeMillis();
                        StrapAlign.drawMessage("#"+ ++_countSP+"  "+count+"/"+N+" "+pi+" ./. "+pj);
                    }
                }
            }
            if (scoreSum>bestScore) { bestScore=scoreSum; pBest=pi;}
        }

        if (pBest!=null) {
            StrapAlign.drawMessage(count+" Superpositions done. Reference protein="+pBest);
            pBest.setRotationAndTranslation(null);
            for(Protein p : pp) {
                if (!get(0,isInterrupted) && p!=null && p!=pBest) {
                    final Superimpose3D.Result r=superimposeTwoProteins(options,p,pBest);
                    if (r!=null) setMatrix(options, r.getMatrix(), p);
                }
            }
            setMatrix(options, null,pBest);
        }
        SUPERIMPOSING[0]=false;
        rm(shouldStop,null);
        if (0!=(options&SUPERIMP_EVENT) && sze(vP)>0) StrapEvent.dispatch(StrapEvent.PROTEIN_3D_MOVED);
        return pBest;
    }
    private static void setMatrix(int options, Matrix3D m, Protein p) {
        if (p==null) return;
        p.setRotationAndTranslation(m);
        if (0!=(options&SUPERIMP_COMPLEX_BEST)) {
            for(Protein pSame : p.getProteinsSameComplex())  pSame.setRotationAndTranslation(m);
        }
    }

    /* <<<  superimposeProteins <<< */
    /* ---------------------------------------- */
    /* >>> Continue superposition till interrupted >>> */
    public static Runnable threadLoopSuperimpose(int options, List<Protein> vP, boolean[] stop) {
        return thrdM("loopSuperimpose", SPUtils.class, new Object[]{intObjct(options), vP, stop});
    }
    public static void loopSuperimpose(int options, List<Protein> vP, boolean[] stop) {
        final boolean interrupt[]=isInterrupted(stop,null);
        do {
            final int modi=modic(vP)+sze(vP);
            superimposeProteins(options, vP,interrupt);
            while(modi==modic(vP)+sze(vP) && !get(0,interrupt)) sleep(444);
        } while(!get(0,interrupt));
        if (sze(stop)>1) stop[1]=false;
        rm(stop,null);
    }
    /* <<<  Continue superposition  <<< */
    /* ---------------------------------------- */
    /* >>> View Comparison >>> */
    public static void viewComparison(Protein p0, Protein p1, Object clazzOrInst) {

        if (p0==null || p1==null || clazzOrInst==null) return;
        final boolean newInst=isClassOrPC(clazzOrInst);
        final Object instance= newInst ? mkInstance(clazzOrInst) : clazzOrInst;
        final SequenceAligner aligner=deref(instance,SequenceAligner.class);
        final Superimpose3D s3d=deref(instance, Superimpose3D.class);
        if (newInst) {
            if (aligner!=null) {
                setSeqs(aligner, new Protein[]{p0,p1});
                aligner.compute();
            }
            if (s3d!=null) {
                superimposeTwoProteinsM(instance,p0,p1);
            }
        }
        final List<JComponent> vPanels=new ArrayList();
        if (s3d!=null && p0.getResidueCalpha()!=null  && p1.getResidueCalpha()!=null ) {
            final Superimpose3D.Result result=s3d.getResult();
            final Matrix3D m3d=result==null?null:result.getMatrix();
            if (m3d==null) error("result.getMatrix()==null");
            else {
                final Protein3d p3D=StrapAlign.new3dBackbone(V3dUtils.OPEN_NOT_ADD_PANEL, new Protein[]{p0,p1});
                p3D.eachChainOneColor(false,false);
                final Protein3d.PView pv=p3D.getView(p0);
                if (pv!=null) pv.transformPreview(m3d);
                final JComponent pan=pnl(CNSEW,p3D, null, ChButton.doCtrl(s3d));
                pcp("T","3D",pan);
                vPanels.add(pan);
            }
        }
        if (aligner!=null) {
            final byte[] gapped[]=aligner.getAlignedSequences();
            if (gapped==null) {
                final Object ctrlP=ctrlPnl(aligner);
                final String MSG="The computation did not produce a result. "+(CacheResult.isEnabled() ? "<br>Suggestion: Disable cache and try again." : "");
                if (ctrlP!=null) {
                    pcp(KEY_NORTH_PANEL,MSG,ctrlP);
                    ChFrame.frame(shrtClasNam(aligner),ctrlP, CLOSE_CtrlW_ESC).shw(ChFrame.AT_CLICK);
                }
                error(MSG);
            }
            final Protein a0=new Protein(), a1=new Protein();
            a0.setGappedSequence(gapped[0], MAX_INT);
            a1.setGappedSequence(gapped[1], MAX_INT);
            a0.setName(p0.getName());
            a1.setName(p1.getName());
            final ExportAlignment w=new ExportAlignment();
            w.setProteins(a0,a1);
            final BA sb=new BA(999);
            w.getText(AlignmentWriter.HTML,sb);
            final JComponent pan=pnl(CNSEW,scrllpn(new ChJTextPane(sb)),null,pnl(ChButton.doCtrl(aligner)));
            pcp("T","Alignment",pan);
            vPanels.add(pan);
        }
        final int N=sze(vPanels);
        if (N>0) {
            final JTabbedPane tabbed=N>1 ? new JTabbedPane() : null;
            JComponent pan=tabbed;
            for(int i=N; --i>=0;) {
                final JComponent p=vPanels.get(i);
                if (tabbed!=null) adTab(0, gcps("T", p),p, tabbed);
                else pan=p;
            }
            new ChFrame("3d "+p0+" "+p1).ad(pan).shw(CLOSE_CtrlW_ESC|ChFrame.AT_CLICK);
        }

    }
    /* <<< View Comparison <<< */
    /* ---------------------------------------- */
    /* >>> Species Icons >>> */
    public static String speciesName2Icon(final Protein p) {
        if (p==null) return null;
        final String s=p.getOrganismScientific();
        return speciesName2Icon(s!=null ? s : p.getOrganism());
    }
    public static String speciesName2Icon(final String name) {
        final int c0=chrAt(0,name)|32, c1=chrAt(1,name)|32,nL=sze(name);
        String baseURL=null;
        final String lines[]=custSettings(Customize.speciesIcons);
        if (sze(lines)*nL==0) return null;
        nextL:
        for(String line : lines) {
            line=line.trim();
            if (line.startsWith("http://")) {
                baseURL=line;
                continue;
            }
            final int L=line.length(), spc=line.indexOf(' ');
            if (spc<0 || L-spc<5 || c0!=(line.charAt(0)|32) || c1!=(line.charAt(1)|32)) continue;
            for(int i=0; i<L && i<nL; i++) {
                final char c=line.charAt(i);
                if (i==nL) {
                    if (c==' ' || c=='*') break;
                    else continue nextL;
                }
                if (c==' ') {
                    if (is(LETTR_DIGT_US,name,i)) continue nextL;
                    break;
                }
                if ((c|32) != (name.charAt(i)|32)) continue nextL;
            }
            final int iconB=nxtE(-SPC, line, spc,L);
            if (strEquls("NONE",line,iconB)) return "0";
            if (strEquls("http://",line, iconB)) return line.substring(iconB);
            else return baseURL==null ? null : delSfx("/",baseURL)+"/"+line.substring(iconB);
        }
        return null;
    }
    public static void setProteinIcon(Protein pp[], boolean overwrite,char type) {
        for(Protein p : pp) {
            if (p==null || !overwrite && sze(p.iconLink())>0) continue;
            final String
                pdbID=p.getPdbID(),
                s=  (type=='3'||type=='A')  && pdbID!=null ?
                URL_RCSB_IMAGES+delLstCmpnt(delPfx("pdb:",pdbID.toLowerCase()),':')+"_bio_r_250.jpg" :
                type=='S' || type=='A' ? speciesName2Icon(p) :
                null;
            if (s!=null && s!="0") p.setIconImage(s);
        }
    }
    /* <<< Species Icons <<< */
    /* ---------------------------------------- */
    /* >>> MSA  >>> */
    public static Runnable threadDownloadOriginalProteins(int opt, Protein pp[], String database, File files[]) {
        return thrdM("downloadOriginalProteins", SPUtils.class, new Object[]{intObjct(opt), boolObjct(false), pp,database,files});
    }
    public static void downloadOriginalProteins(int webOptions, boolean newThread, Protein pp[], String database, File files[]) {
        if (files==null) {
            if (sze(database)==0) {
                final String keys[]=Hyperrefs.getDatabases(Hyperrefs.PROTEIN_FILE)[0];
                for(int iK=0; iK<=keys.length; iK++) {
                    final String db=iK<0 || iK==keys.length ? "PDB:" : keys[iK];
                    for(Protein p : pp) {
                        if (strEquls(db, idForDl(p, null), 0) && (p.isInMsfFile() || p.getFile()==null)) {
                            final Runnable t=threadDownloadOriginalProteins(webOptions, pp, db, (File[])null);
                            if (newThread) startThrd(t); else t.run();
                        }
                    }
                }
            } else {
                File ff[]=null;
                final String[] aa=new String[pp.length];
                for(int iP=pp.length; --iP>=0;) aa[iP]=idForDl(pp[iP], database);
                FetchSeqs.download(0L,aa);
                final File[] ffEquivalent=null;
                final Protein ppInf3d[]=new Protein[pp.length];
                for(int iP=pp.length; --iP>=0;) {
                    final String a=aa[iP];
                    final Protein p=pp[iP];
                    if (!strEquls(database,a,0)) continue;
                    if (a.startsWith("PDB:")) ppInf3d[iP]=p;
                    else {
                        File f=FetchSeqs.dbColonID2file(a);
                        if (sze(f)==0) f=urlGet(url(Hyperrefs.toUrlString(a,Hyperrefs.PROTEIN_FILE)), MAX_INT, ffEquivalent, (ChRunnable)StrapAlign.getInstance());
                        if (f!=null) {
                            (ff==null?ff=new File[iP+1] : ff)[iP]=f;
                            SwissHeaderParser.parse(p,readBytes(f));
                            p.setFile(f);
                        }
                    }
                }
                if (ff!=null) inEDT(threadDownloadOriginalProteins(webOptions, pp,"",ff));
                inferCoordinates(INFER3D_SET_FILE, ppInf3d, aa, CLASS_Blaster_SOAP_ebi);
            }
        } else {
            assrtEDT();
            BA sbError=null;
            for(int iP=mini(pp.length,files.length); --iP>=0;) {
                final Protein p=pp[iP];
                if (p==null || p.getFile()!=null) continue;
                final File f=files[iP];
                final BA ba=readBytes(f);
                final String a=idForDl(p, database);
                if (a==null || sze(ba)==0) continue;
                if (p.getFile()==null) p.setFile(f);
                final byte[] seq=p.getResidueTypeExactLength();
                int firstAmino=-1;
                String matchingCDS=null;
                // if (a.startsWith("PDB:")) inferCoordinates(new Protein[]{p}, strgArry(a), (BA)null, 0L);else
                {
                    final Protein pTmp=new Protein();
                    SPUtils.parseProtein(f, ba, 0,pTmp);
                    final String cds[]=pTmp.getCDS();
                    byte[] newSeq=pTmp.getResidueTypeExactLength();
                    if (sze(cds)>0) {
                        for(int iCDS=cds.length; --iCDS>=0;) {
                            pTmp.parseCDS(cds[iCDS]);
                            firstAmino=strstr(STRSTR_IC,seq, newSeq=pTmp.getResidueTypeExactLength());
                            if ( firstAmino>=0) {
                                matchingCDS=cds[iCDS];
                                break;
                            }
                        }
                    } else firstAmino=strstr(STRSTR_IC,seq, newSeq);
                    if (firstAmino<0) {
                        if (sbError==null) sbError=new BA(3333);
                        sbError.a("Warning for ").a(a).aln("  The sequence in downloaded file does not match").aln(newSeq).aln(seq).a('\n',2);
                        if (strstr("Runtime Error",ba)>0) delFileOnExit(f);
                    }
                    p.setIsInMsfFile(false);
                    if (firstAmino>=0) {
                        SPUtils.parseProtein(f, ba, 0, p);
                        p.setResidueSubset((firstAmino+1)+"-"+(firstAmino+seq.length));
                        p.parseCDS(matchingCDS);
                        p.setFile(f);
                        p.setAccessionID(a);
                        if ((webOptions&StrapAlign.OPTION_RENAME_SWISS)!=0) {
                            final String newName=DialogRenameProteins.newName(p,DialogRenameProteins.PRC3_DROME);
                            if (sze(newName)>0) p.setName(newName);
                        }
                    }
                }
            }
            shwTxtInW("Error inferring sequence files",sbError);
        }
    }
    final static String DEFAULT_DBS[]={"UNIPROT:", "SWISS:", "GB:", "NCBI_AA:", "NCBI_NT:", "PDB:"};
    private static String idForDl(Protein p, Object dbs0) {
        if (p==null) return null;
        final String  acc=p.getAccessionID(), refs[]=p.getSequenceRefs();
        if (!p.isInMsfFile() && p.getFile()!=null) return null;
        final Object dbs=orO(dbs0, DEFAULT_DBS);
        if (strEquls(dbs,acc,0)) return acc;
        for(String id : refs) {
            if (strEquls(dbs, id, 0)) return id;
        }
        return get(0, PicrEBI.idsForSequence(true, PicrEBI.UNIPROT, p.getResidueTypeFullLengthAsString()));

    }

    public static Runnable thread_fetchIdsAndDownloadOriginalProteins(boolean ids, Protein pp[]) {
        return thrdM("fetchIdsAndDownloadOriginalProteins", SPUtils.class, new Object[]{boolObjct(ids),pp});
    }
    public static void fetchIdsAndDownloadOriginalProteins(boolean startPicr, Protein pp[]) {
        if (pp==null) return;
        final String msg=ANSI_W_ON_B+"Fetching "+pp.length+" protein files";
        StrapAlign.drawMessage(msg+" ...");
        final String dbs[]={FetchSeqs.DB_NCBI_AA, FetchSeqs.DB_UNIPROT};
        downloadOriginalProteins(0, false, pp, null, (File[])null);
        if (startPicr) {
            for(Protein p : pp) {
                final String id=idForDl(p, dbs);
                if (id!=null || p==null) continue;
                final String
                    result[]=PicrEBI.idsForSequence(false, PicrEBI.UNIPROT, p.getResidueTypeFullLengthAsString()),
                    report="EBI Picr "+p+":  "+(sze(result)==0?" No ID found" : "Success");
                StrapAlign.drawMessage(report);
            }
            downloadOriginalProteins(0, false, pp, null, (File[])null);
        }
        StrapAlign.drawMessage(msg+" - done");
    }

    /* <<< downloadOriginalProteins <<< */
    /* ---------------------------------------- */
    /* >>> Comparator >>> */

    public static void sortVisibleOrder(Protein pp[]) {
        final Protein ppVis[]=StrapAlign.visibleProteins();
        for(int iP=ppVis.length; --iP>=0;) ppVis[iP].setRow(iP);
        sortArry(pp, comparator('R'));
    }

    private final static SPUtils comparators[]=new SPUtils['z'];
    private char _instType;
    public static Comparator comparator(char type) {
        if (comparators[type]==null) (comparators[type]=new SPUtils())._instType=type;
        return comparators[type];
    }
    public int compare(Object o1, Object o2) {
        final Protein
            p1=o1 instanceof Protein ? (Protein)o1 : get(0,o1,Protein.class),
            p2=o2 instanceof Protein ? (Protein)o2 : get(0,o2,Protein.class);
        if (p1==p2)  return 0;
        if (p1==null) return -1;
        if (p2==null) return 1;
        if (_instType=='P' || _instType=='R') {
            int iP1=-1,iP2=-1;
            switch(_instType) {
            case 'P':
                iP1=p1.getPreferedOrder();
                iP2=p2.getPreferedOrder();
                if (iP1<0 && iP2<0) return p1.getRow()-p2.getRow();
                break;
            case 'R':
                iP1=p1.getRow();
                iP2=p2.getRow();
            }
            return iP1==iP2 ? p1.getName().compareTo(p2.getName()) : iP1<0?1 : iP2<0?-1 : iP1-iP2;
        }
        return 0;
    }
    /* <<< Comparator  <<< */
    /* ---------------------------------------- */
    /* >>> SequenceAligner utils  >>> */
    public static boolean hasResidueSelection(Protein p, String names[]) {
        if (p!=null) {
            for(ResidueSelection s : p.allResidueSelections()) {
                final String name=nam(s);
                if (name!=null && idxOfStrg(name, names)>=0 ) return true;
            }
        }
        return false;
    }

    public static void setSeqs(SequenceAligner aligner, Protein...pp) {
        final byte ss[][]=residueTypeArray(pp);
        aligner.setSequences(ss);
        if (isAssignblFrm(NeedsProteins.class, aligner)) ((NeedsProteins)aligner).setProteins(pp);
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Childs >>> */
    static String titleForChild(Class c) {
        return
            c==null ? null :
            c==HeteroCompound.class    ? HeteroPopup.TITLE :
            c==ResidueSelection.class  ? "Residue selections" :
            c==ResidueAnnotation.class ? "Annotated residue selections" :
            c==SequenceFeatures.class  ? "Sequence features" :
            null;
    }

    //    static Object keyForJlist(Object o) {    }
    static void showChildObjects(Class c, int type, Protein[] pp) {
        if (pp.length==1) {
            final ChJTree jT=pp[0].getJTree();
            if (jT!=null) {
                ChFrame.frame("Child objects "+pp[0],jT, ChFrame.PACK).shw(ChFrame.AT_CLICK|ChFrame.TO_FRONT);
                pp[0].expandTreeNode(c,type);
                jT.updateKeepStateLater(0L, 99);
            }
        } else {
            Collection v=null;
            Object[] childs=null;
            for(Protein p : pp) {
                if (p==null) continue;
                childs=null;
                if (c==HeteroCompound.class) childs=p.getHeteroCompounds((char)type);
                if (c==ResidueSelection.class) childs=p.allResidueSelections();
                if (c==ResidueAnnotation.class || c==SequenceFeatures.class) {
                    final ResidueAnnotation aa[]=p.residueAnnotations().clone();
                    for(int i=aa.length; --i>=0;) {
                        if ((aa[i].featureName()!=null) != (c==SequenceFeatures.class) || !aa[i].isEnabled()) aa[i]=null;
                    }
                    childs=rmNullA(aa);
                }
                adAll(childs,v==null ? v=new ArrayList() : v);
            }
            final String title=titleForChild(c);
            if (null==title) assrt();
            if (sze(v)>0) StrapAlign.showInJList(0L, null, oo(v), title,null).showInFrame(ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN,title);
        }
    }

    private static ChFrame _rhInfoW;
    private static ChTextView _rhInfoTA;
    private static void _rhInfoAdd(String id, BA sb, BA sbStru) {
        if (strchr(':',id)<0 || strstr(id,sb)>=0 || strstr(id,sbStru)>=0) return;
        if (id.startsWith("PDB:")) {
            final String pdb=delLstCmpnt(id,'_');
            if (strstr(pdb,sbStru)<0) sbStru.a(pdb).a(' ');
        } else sb.a(id).a(' ');
    }
    static void showInfo(Protein pp[]) {
        final int countP=countNotNull(pp);
        if (countP==0) return;
        Protein oneP=null;
        final BA sb=new BA(333);
        final ChButton butHet=StrapView.button(StrapView.BUT_LIST_HET);
        for(Protein p : pp) {
            if (p==null) continue;
            if (_rhInfoW==null) {
                _rhInfoTA=new ChTextView("");
                final Object
                    pSouth=pnl(butHet, C(0xFFffFF), WATCH_MOVIE+MOVIE_Export_Proteins+" "+MOVIE_Load_Proteins+" "+MOVIE_Context_Menu),
                    pnl=pnl(CNSEW,_rhInfoTA,null,pSouth, "#RBB");
                (_rhInfoW=new ChFrame("Protein x-refs").ad(pnl)).setUndecorated(true);
                pcp(ChRenderer.KEY_TO_CLOSE, REP_PARENT_WINDOW, _rhInfoTA);
                ChRenderer.setSmallButtonXc(0, _rhInfoTA);
            }
            final BA sbSeq=new BA(99), sbRef=new BA(99), sbStru=new BA(99);
            _rhInfoAdd(p.getPdbID(),sbSeq,sbStru);
            _rhInfoAdd(p.getAccessionID(),sbSeq,sbStru);
            for(String s : p.getSequenceRefs()) _rhInfoAdd(s,sbSeq,sbStru);
            for(String s : p.getDatabaseRefs()) _rhInfoAdd(s,sbSeq,sbStru);
            for(String ec : p.getEC()) if (ec!=null && strchr('-',ec)<0) sbRef.a(' ').a(ec).a(' ');
            final String uniprot=p.getUniprotID();
            if (uniprot!=null) {
                final int colon=uniprot.indexOf(':');
                sbRef.a("DASTY:").a(uniprot,colon+1,MAX_INT).a("  ARCHSCHEMAu:").a(uniprot,colon+1,MAX_INT).a(' ');
            }
            for(String s:p.getSequenceRefs()) {
                final String pf=delPfx("PFAM:",s);
                if (pf!=s) sbRef.a(" ARCHSCHEMApf:").a(pf).a(' ');
            }
            int isFirst=0;
            sb.a('\n',oneP==null?0:1).a(' ',2).aln(fPathUnix(p.getFile()));
            for(BA s:new BA[]{sbStru,sbSeq,sbRef}) if (sze(s)>0) sb.a(isFirst++==0?' ':'\n').a(s);
            oneP=p;
        }
        butHet.enabled(countP!=1 ? false :  sze(oneP.run(PROVIDE_JTREE_CHILDS,null))>0).cp("P",wref(oneP));
        _rhInfoTA.t(sb).tools().cp(KOPT_DRAGS_PARWINDOW,"").underlineRefs(0);
        _rhInfoW.shw(ChFrame.AT_CLICK|ChFrame.PACK|ChFrame.ALWAYS_ON_TOP|ChFrame.DRAG|CLOSE_CtrlW_ESC);

        ChFrame.setLocationAtMouse(_rhInfoW);
        revalAndRepaintCs(butHet);
    }

    public static int[] leadingAndTrailingLowerCaseLettersInMSA(Protein pp[]) {
        if (sze(pp)==0) return null;
        int firstU=MAX_INT, lastU=0;
        boolean onlyUC=true;
        for(Protein p : pp) {
            if (p==null) continue;
            final byte aa[]=p.getResidueType();
            final int L=p.countResidues();
            final int fU=nxt(UPPR, aa,0,L);
            if (fU>0) onlyUC=false;
            firstU=mini(firstU,p.getResidueColumnZ(fU));
            final int lU=prev(UPPR,aa,L-1,-1);
            if (lU!=L-1) onlyUC=false;
            lastU=maxi(lastU, p.getResidueColumnZ(lU));
        }
        if (onlyUC) return null;
        for(Protein p : pp) {
            final int fU=nxt(UPPR, p.getResidueType(),0,p.countResidues());
            if (fU>0 && p.getResidueColumnZ(fU-1)>=firstU) return null;
        }
        return firstU>0 && firstU<lastU ? new int[]{firstU,lastU} : null;
    }
    public static void setResidueSubsetFromColumnRange(Protein p, int from, int to ) {
        final int skipped=Protein.firstResIdx(p)-p.getResidueIndexOffset();
        final int f=p.column2nextIndexZ(from)+skipped;
        final int t=p.column2nextIndexZ(to)+skipped;
        if (f>=0 && t>f) {
            final int R=p.countResidues();
            final int gg[]=p.getResidueGap().clone();
            final int gapNew[]=new int[R];
            System.arraycopy(gg,f,gapNew,0,R-f);
            for(int i=0; i<f; i++) gapNew[0]+=(gg.length>i?gg[i]:0)+1;
            p.setResidueGap(gapNew);
            final boolean bb[]=new boolean[t+1];
            Arrays.fill(bb,f,t,true);
            p.setResidueSubset(bb);
            p.setName(delLstCmpnt(p.getName(),'!')+"!"+f+"-"+t);
        }
    }
    private static Set _allFiles;
    private static int _allFilesMC;
    public static Set allFiles() {
        final int mc=modic(StrapAlign.vProteins());
        Set v=_allFiles;
        if (v==null || _allFilesMC!=mc) {
            _allFilesMC=mc;
            if (v==null) _allFiles=v=new HashSet();
            clr(v);
            for(Protein p : StrapAlign.proteins()) {
                final File f=p.getFile();
                if (f!=null) v.add(toStrg(f));
            }
        }
        return v;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Protein Subset >>> */
    static Protein newProteinNarrowToResidueSelection(Protein p, ResidueSelection[] ss) {
        if (p==null || ss==null) return null;
        int from=MAX_INT, to=-1;
        for(ResidueSelection s : ss) {
            if (s!=null && s.getProtein()==p) {
                from=mini(from, ResSelUtils.firstAmino(s));
                to=maxi(to, ResSelUtils.lastAmino(s)+1);
            }
        }
        return from>=to ? null : newProteinSubset(p, (1+from)+"-"+to);
    }

    public static Protein newProteinColumnFromTo(Protein p, int west, int east) {
        if (p==null) return null;
        final int i0=Protein.firstResIdx(p)+1;
        return newProteinSubset(p,  (i0+p.column2nextIndexZ(west))+"-"+(i0+p.column2thisOrPreviousIndex(east-1)));
    }

    public static Protein newProteinSubset(Protein p, String expr) {
        if (nxt(-SPC,expr)<0 || p==null) return null;
        final Protein pNew=p.cloneProtein(new Protein());
        pNew.setResidueSubset(expr);
        return pNew;
    }
    // =========================================
    public static Protein sp(Object o) {
        if (o==null) return null;
        Protein p=o instanceof Protein ?  (Protein)o : deref(o,Protein.class);
        if (p==null) {
            final HasProtein hp=deref(o,HasProtein.class);
            if (hp!=null) p=hp.getProtein();
        }
        return p;
    }
    public static Protein[] spp(Object pp) {
        Protein p1, ret[]=null;
        Object array=null;
        if (pp==null) return Protein.NONE;
        if (pp instanceof Object[]) {
            if (((Object[])pp).length==0) return Protein.NONE;
            else if (pp instanceof Protein[]) ret=(Protein[])pp;
            else array=pp;
        }
        else if (pp instanceof Collection) {
            if (sze(pp)==0) return Protein.NONE;
            else if (pp instanceof UniqueList && ((UniqueList)pp).getClazz()==Protein.class) return (Protein[])((UniqueList)pp).asArray();
            else array=pp;
        } else if ((p1=sp(pp))!=null) return new Protein[]{p1};
        if (array!=null) {
            if (array instanceof Set) array=oo(array);
            Collection<Protein>vP=null;
            final int N=sze(array);
            for(int i=0; i<N; i++) vP=adUniqNew(sp(get(i,array)), vP);
            return vP==null?Protein.NONE:toArry(vP,Protein.class);
        }
        return ret==null ? Protein.NONE : rmNullA(ret,Protein.class);
    }

    /**  returns the 1st protein whose name equals to name.*/
    public static Protein proteinWithName(String name,Protein pp0[]) {
        final Protein pp[]=pp0!=null ? pp0 : StrapAlign.proteins();
        if (name!=null && pp!=null) {
            final int hcName=name.hashCode();
            for(Protein p:pp) {
                if  (p==null) continue;
                final Object wt=p.OBJECTS[Protein.O_WEB_TOK1];
                if (hcName==p.getNameHC() && name.equals(p.getName()) || wt!=null && wt.equals(name)) return p;
            }
        }
        return null;
    }

    public static Protein proteinWithNameAndFile(String n, File file, Object pp[]) {
        final String fn=nam(file);
        if (pp!=null && fn!=null) {
            final int fnHC=fn.hashCode(), nHC=n!=null?n.hashCode():0;
            for(Object o:pp) {
                final Protein p= o instanceof Protein ? (Protein)o:null;
                if (p==null || n!=null && !(nHC==p.getNameHC() && n.equals(p.getName()))) continue;
                final File f=p.getFile();
                if (f!=null && fnHC==p.getFileNameHC() && f.getName()==p.getFileName()) return p;
            }
        }
        return null;
    }
    public static void updateCountResidues() { /* See Protein.unsyncResCount() */
        for(Protein p : StrapAlign.proteins()) if (p!=null) p.countResidues();
    }

    public static int maxColumn(Protein pp[]) {
        int max=0;
        updateCountResidues();
        for(Protein p : pp) if (p!=null) max=maxi(max,p.getMaxColumnZ());
        return max;
    }
    private static long _parseOpt;
    public static long parserOptions() { return _parseOpt;}
    public static void setParserOptions(long opt) { _parseOpt=opt; }

    public static byte[][] residueTypeArray(Protein pp[]) {
        if (pp==null) return null;
        byte seq[][]=new byte[pp.length][];
        for(int i=pp.length;--i>=0;) if (pp[i]!=null) seq[i]=pp[i].getResidueTypeExactLength();

        return seq;
    }

    static byte[][] gappedSequences(Protein pp[], int colFrom0,  int colTo0,  char gap) {
        final byte[][] gapped_ali=new byte[pp.length][];
        Arrays.fill(gapped_ali,NO_BYTE);
        final int[] wide2narrow=new Gaps2Columns().computeWide2narrow(pp);
        final Range range=colsToLessProteins(pp, colFrom0, colTo0-1, wide2narrow);
        if (range==null) return gapped_ali;
        final int from=range.from(), to=range.to();
        for(int iP=pp.length; --iP>=0;) {
            final Protein p=pp[iP];
            if (p==null) continue;
            final int nR=p.countResidues();
            final int[] cols=Gaps2Columns.computeColumns(wide2narrow,p.getResidueGap(),nR, null);
            final byte[] aa=p.getResidueType();
            byte[] seq=null;
            for(int iA=nR; --iA>=0;) {
                final int c=cols[iA];
                if (c<from) break;
                if (c<to) {
                    if (seq==null) Arrays.fill(gapped_ali[iP]=seq=new byte[c-from+1],(byte)gap);
                    seq[c-from]=aa[iA];
                }
            }
        }
        return gapped_ali;
    }

    static Range colsToLessProteins(Protein pp[], int colFrom, int colTo) {
        return colsToLessProteins( pp, colFrom, colTo, new Gaps2Columns().computeWide2narrow(pp));
    }
    private static Range colsToLessProteins(Protein pp[], int colFrom, int colTo, int[] wide2narrow) {
        int cMax=-1, cMin=MAX_INT, iFrom=-1, iTo=-1;
        Protein pFrom=null, pTo=null;
        for(Protein p : pp) {
            if (p==null) continue;
            for(int j=0;j<2;j++) {
                final int i= j==0 ? p.column2nextIndexZ(colFrom) : p.column2thisOrPreviousIndex(colTo-1);
                final int c=i<0?-1:p.getResidueColumnZ(i);
                if (c<colFrom || c>=colTo) continue;
                if (c<cMin) { pFrom=p; cMin=c; iFrom=i;}
                if (cMax<c) {   pTo=p; cMax=c;   iTo=i;}
            }
        }
        if (pFrom==null || pTo==null || iFrom<0 || iTo<0) return null;
        final int[]
            colsF=Gaps2Columns.computeColumns(wide2narrow, pFrom.getResidueGap(), pFrom.countResidues(), (int[])null),
            colsT=Gaps2Columns.computeColumns(wide2narrow,   pTo.getResidueGap(),   pTo.countResidues(), (int[])null);
        return  colsF.length<=iFrom || colsT.length<=iTo ? null : new Range(colsF[iFrom], 1+colsT[iTo]);
    }

    public static Protein[] getProteinsSameRow(Protein p) {
        final Object a=p.getAlignment();
        final StrapView v=a instanceof StrapAlign ? ((StrapAlign)a).alignmentPanel() : null;
        if (v!=null) {
            final Protein pp[]=v.proteinsInRow(v.findRow(p));
            if (sze(pp)>0 && cntains(p,pp)) return pp;
        }
        return new Protein[]{p};
    }

    public static boolean parseProtein(File proteinFile, BA text, long mode, Protein p) {
        if (proteinFile==null && text==null) return false;
        final BA ba;
        if (text!=null) ba=text;
        else {
            final int fLen=sze(proteinFile);
            ba= fLen>0 ? readBytes(proteinFile,ba4CurrThrd(Protein.KEY_BA_PARSE)) : null;
            if (ba!=null) ba.insidePreTags(null);
        }
        return sze(ba)>0 ? p.parse(ba,StrapAlign.proteinParsers(),mode) : false;
    }
    /* <<< Subset <<< */
    /* ---------------------------------------- */
    /* >>> Names >>> */
    public static String[] names(Protein pp[], boolean ext, boolean toUnderscore) {
        final int L=sze(pp);
        final String nn[]=new String[L];
        for(int i=L;--i>=0;) {
            String n=pp[i]==null?null:pp[i].getName();
            if (n==null) continue;
            if (ext) n=delLstCmpnt(n,'.');
            if (toUnderscore) n=filtrS('_'|FILTER_NO_MATCH_TO, FILENM, n);
            nn[i]=n;
        }
        return nn;
    }
    public static Protein[] noDuplicateNames(Protein pp0[], String names[]) {
        Protein pp[]=pp0.clone();
        nextP:
        for(int i=0; i<pp.length; i++) {
            if (pp[i]==null) continue;
            final String n=names[i];
            for(int j=0; j<i; j++) {
                if (pp[j]!=null && n.equals(names)) {
                    pp[i]=null;
                    continue nextP;
                }
            }
        }
        return rmNullA(pp,Protein.class);
    }
    /* <<< Names <<< */
    /* ---------------------------------------- */
    /* >>> Heteros >>> */

    public static boolean assignHeterosToClosestPeptides(Protein pHet, Protein ppPeptide[]) {
        boolean success=false;
        for(HeteroCompound h : pHet.getHeteroCompounds('*')) {
            final Protein pAddTo=h.closestPeptide(ppPeptide, h.isNucleotideChain() ? HeteroCompound.ONLY_PHOSPHATE : 0);
            if (pAddTo!=null) { /* Add hetero to peptide */
                pAddTo.addHeteroCompounds(Protein.HETERO_ADD_UNIQUE,h);
                success=true;
            }
        }
        return success;
    }

    public static Protein[] assignHeterosToClosestPeptides(Protein pp[]) {
        final Protein ppP[]=pp.clone();
        for(int i=pp.length; --i>=0; ) {
            if (ppP[i].countResidues()==0) {
                ppP[i]=null;
                assignHeterosToClosestPeptides(pp[i],  ppP);
            }
        }
        return rmNullA(ppP,Protein.class);
    }

    public static Protein[] ppWithAminos(Protein pp0[]) {
        final Protein pp[]=pp0.clone();
        for(int i=pp.length; --i>=0;) if (pp[i]!=null && pp[i].countResidues()==0) pp[i]=null;
        return rmNullA(pp,Protein.class);
    }

    public static Protein[] assignHeterosToClosestPeptides(List<Protein> vAll, String tokens[]) {
        final Collection<String> tGroup=new HashSet();
        final Collection<Protein> pGroup=new ArrayList(), vPept=new ArrayList();
        Collection<Protein> v3d=null;
        for(int iH=sze(vAll); --iH>=0;) {
            final Protein pHet=vAll.get(iH);
            if (pHet==null) continue;
            if (pHet.countResidues()>0) { vPept.add(pHet);  continue; }
            if (pHet.getHeteroCompounds('*').length==0) continue;
            final int iT=idxOfStrg((String)pHet.OBJECTS[Protein.O_WEB_TOK],tokens);
            if (iT<0) continue;
            /* Expression in Parenthesis ( peptide.pdb  hetero.pdb ) */
            tGroup.clear();
            pGroup.clear();
            for(int i=idxOf(")",tokens,iT,MAX_INT)-1; i>=0 && tokens[i]!="("; --i) tGroup.add(tokens[i]);
            tGroup.add(tokens[iT]);
            for(int i=sze(vAll); --i>=0;) {
                final Protein p=vAll.get(i);
                if (p.getResidueCalpha()!=null && tGroup.contains(p.OBJECTS[Protein.O_WEB_TOK])) pGroup.add(p);
            }
            if (!SPUtils.assignHeterosToClosestPeptides(pHet,spp(pGroup)))  v3d=adNotNullNew(pHet,v3d);
        }
        if (v3d!=null) StrapAlign.new3dBackbone(0, spp(v3d)); /*  Hetero in its own 3D */
        return spp(vPept);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> ValueOfProtein >>> */
    public final static int COMPV_NO_BLOCKING=1<<0;
    public static double computeValue(int opt, Object score, Protein p1, Protein p2, int fromColumn, int toColumn) {
        if (score==null || p1==null) return Double.NaN;
        if (p2!=null) {
            final CompareTwoProteins c2p=deref(score, CompareTwoProteins.class);
            if (c2p!=null) {
                c2p.setProteins(p1,p2,fromColumn,toColumn);
                if (0==(opt&COMPV_NO_BLOCKING))  c2p.compute();
                return c2p.getValue();
            }
            final SequenceAlignmentScore2 ali=deref(score, SequenceAlignmentScore2.class);
            if (ali!=null) {
                final byte[] s1=p1.getGappedSequence();
                final byte[] s2=p2.getGappedSequence();
                ali.setSequences(p1.mc(ProteinMC.MC_GAPPED_SEQUENCE)+p2.mc(ProteinMC.MC_GAPPED_SEQUENCE) ,s1,s2, fromColumn, toColumn);
                if (0==(opt&COMPV_NO_BLOCKING)) ali.compute();
                return ali.getValue();
            }
        }
        final ValueOfProtein vop=deref(score, ValueOfProtein.class);
        if (vop!=null) {
            vop.setProtein(p1);
            if (0==(opt&COMPV_NO_BLOCKING)) vop.compute();
            return vop.getValue();
        }
        return Double.NaN;
    }

    public static boolean isDistanceScore(Object o) {
        return
            isAssignblFrm(CompareTwoProteins.class,o) ? ((CompareTwoProteins)o).isDistanceScore() :
            isAssignblFrm(SequenceAlignmentScore2.class,o) ? ((SequenceAlignmentScore2)o).isDistanceScore() :
            false;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Sort >>> */
    private static Object _diaSort;
    public static void showSortDialog() {
        if (_diaSort==null) {
            final Object dd[]={ new DialogSortProteins(), new DialogSort(DialogSort.PROTEINS, null)};
            final AbstractButton radio[]=radioGrp(new String[]{"By sequence similarity", "By Protein name, organism name etc."},0, null);
            for(int i=2; --i>=0;) gcp(KEY_CLONED_FROM, radio[i],ChButton.class).doCollapse(false, dd[i]).doPack(dd[0]);
            _diaSort=pnl(VBHB,pnl(FLOWRIGHT, "<sub>Opened by mouse-click on alignment header bar</sub>"),"<b><u>Sorting proteins</u></b>",  radio, dd[0], dd[1]);

        }

        ChFrame.frame("Change order of proteins", _diaSort, ChFrame.PACK|CLOSE_CtrlW_ESC|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.AT_CLICK);
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Sort >>> */

    /* <<< Sort <<< */
    /* ---------------------------------------- */
    /* >>> Dialog >>> */

    public static JComponent dialogSetProteins(Protein pp[], JComponent d) { return dialogSetProteins(MIN_INT, pp,d);}
    public static JComponent dialogSetProteins(long filter, Protein pp0[], JComponent d) {

        if (d!=null && countNotNull(pp0)>0) {
            final Protein pp[]=rmNullA( pp0.clone(), Protein.class);
            if (d instanceof NeedsProteins) ((NeedsProteins)d).setProteins(pp);
            else {
                for(ProteinCombo c : childsR(d,ProteinCombo.class)) {
                    for(int iP=0; iP<pp.length; iP++) {
                        if (c.isEnabled(pp[iP])) {
                            c.s(pp[iP]);
                            pp[iP]=null;
                            break;
                        }
                    }
                }
                for(ProteinList pl : childsR(d,ProteinList.class)) {
                    if (filter!=MIN_INT) pl.setFilter(filter);
                    pl.clearSelection();
                    pl.setSelOO(pp);
                    pl.scrollToSelected();
                }
            }
        }
        return d;
    }
    public static ChCombo classChoice(Class anInterface) {
        if (!isEDT()) {
            final Object argv[]={anInterface,null};
            inEdtCR(instance(),"classChoice",argv);
            return (ChCombo)argv[1];
        }
        final List v=StrapPlugins.allClassesV(anInterface);
        final ChCombo c=new ChCombo(ChJTable.CLASS_RENDERER, v);
        c.classRenderer(anInterface);
        updateOn(CHANGED_PLUGINS_ADDED_OR_REMOVED,c);
        return c;
    }

   public static ChCombo classChoice2(Class ...interfaces) {
        final List v=StrapPlugins.for2Interfaces(interfaces);
        final ChCombo c=new ChCombo(ChJTable.CLASS_RENDERER, v);
        updateOn(CHANGED_PLUGINS_ADDED_OR_REMOVED,c);

        return c;
    }

   /* <<< Dialog <<< */
   /* ---------------------------------------- */
   /* >>> Export >>> */
   public static BA alignmentToXML(int opt, String script) {
        final BA sb=new BA(1000*1000).a("<xml>\n\n<!-- The information in this file can be used within scripts.\nSee menu File / Plugins.\n"+
                                        "The output of the script can contain html code and/or a strap script lines.\n"+
                                        "To allow fast parsing, xml-tags start directly at the beginning of the line. -->\n\n"+
                                        "<strapSessionId>").a(TIME_AT_START).aln("</strapSessionId>");
        final Collection vSel=StrapAlign.selectedObjectsV();
        final Protein pCursor=StrapAlign.cursorProtein();

        sb.aln("<proteins>");
        for(Protein p : StrapAlign.visibleProteins()) {
            final boolean sel=vSel.contains(p) || p==pCursor;
            if (0!=(opt&ExecByRegex.ONLY_SELECTED_P) && !sel) continue;
            p.toXML(opt, vSel,sb,  script, sel);
        }
        sb.aln("</proteins>\n");
        final StrapView view=StrapAlign.alignmentPanel();
        final java.awt.Rectangle r=view==null?null:view.rectRubberBand();
        if (r!=null) sb.a("<rectangle from=\"").a(x(r)+1).a("\" to=\"").a(x2(r)).a("\" proteins=\"").join(StrapView.ppInRectangle(r, view)," ").aln("\" />");

        return sb.aln("</xml>");
    }

}

