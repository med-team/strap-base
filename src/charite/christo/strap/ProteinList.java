package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   A JList to select proteins.
   @author Christoph Gille JList
*/
public class ProteinList extends ChJList {
    public final static long NT=1, CALPHA=1<<1, PDB_ID=1<<2, EC=1<<3, IN_ALI=1<<4, NOT_IN_ALI=1<<5,
        ACTGN=1<<6, NT_or_ACTGN=1<<7, NOT_ACTGN=1<<8, NO_CALPHA=1<<9,
        IN_3D=1<<10, CHAINS=1<<11, SECSTRU=1<<12,
        SOLVENT=1<<21, RES_ANNO=1<<22, RES_SEL=1<<23, SEQ_FEATURE=1<<24,
        NONE=1<<25, HETERO3D=1<<26, NUC3D=1<<27, INFERRED_3D=1<<28, NO_3D_FILE=1<<29, NO_PDB_ID=1<<30;
    private long _filter;
    private static ProteinComboModel _model;
    private final ChListSelectionModel _selModel=new ChListSelectionModel(this);
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    public ProteinList(long filter) {
        super(_model=new ProteinComboModel(filter), ChJTable.ICON_ROW_HEIGHT|ChJTable.FILE_TRANSFER_HANDLER|ChJTable.DRAG_ENABLED);
        _filter=filter;
        setMinSze(4*EM,4*EX, this);
        StrapAlign.addAwtListeners(this);
        StrapAlign.newDropTarget(this,false);
        setSelectionModel(_selModel);
    }
    public void setFilter(long filter) { _filter=filter; }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private boolean _isInitialized;
    @Override public void paintComponent(Graphics g) {
        antiAliasing(g);
        if (!_isInitialized) {
            _isInitialized=true;
            setBG(0xFFffFF,this);
            setCellRenderer(new ChRenderer().setEnabled(this));
            updateOn(CHANGED_PROTEIN_LABEL,this);
            updateOn(CHANGED_PROTEIN_AT_CURSOR,this);
            updateOn(CHANGED_PROTEIN_ORDER,this);
            rtt(this);
        }
        super.paintComponent(g);
        final int cp=StrapAlign.proteins().length;
        if (cp==0) {
            final String msg[]={ANSI_FG_BLUE+" This is the protein selector","which allows to select several proteins.","Currently no protein is present in Strap."};
            drawMsg(DRAW_MSG_SET_COLOR, msg,this,g,0);
        } else {
            final int y=cp*ICON_HEIGHT+EX;
            final int h=getHeight();
            if (y+3*EX<h) {
                final String msg[]={ANSI_FG_BLUE+"Note: more than one proteins can be selected","by left-click with shift or ctrl or by typing Ctrl+A."};
                drawMsg(DRAW_MSG_SET_COLOR, msg,this,g,h-2*EX);
            }
        }
    }
    @Override public void paint(Graphics g) { try {super.paint(g);} catch(Throwable e) {} }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> selected proteins >>> */
    final public Protein[] selectedProteins() {
        final Set set=_selModel.getSet();
        set.retainAll(StrapAlign.vProteins());
        final Protein pp[]=(Protein[])set.toArray(Protein.NONE);
        SPUtils.sortVisibleOrder(pp);
        return pp;
    }
    final public Protein[] selectedOrAllProteins() {
        final Protein pp[]=selectedProteins(),ppVis[]=StrapAlign.visibleProteins();
        return pp==null||pp.length<1 && ChMsg.noProtsSelectedButWantTakeAll(ppVis.length) ? ppVis : pp;
    }
    final public Protein[] selectedProteinsAtLeast(int atLeast) {
        final Protein pp[]= selectedOrAllProteins();
        if (pp.length<atLeast) {
            StrapAlign.errorMsg("Select at least "+atLeast+" proteins");
            return null;
        }
        return pp;
    }

    public ProteinList selectAll(long filter) {
        clearSelection();
        if (filter!=NONE) {
            final Set set=_selModel.getSet();
            set.clear();
            final ListModel m=_model;
            for(int i=sze(m);--i>=0;) {
                final Protein p=(Protein)_model.getElementAt(i);
                if (p!=null && isEnabled(p,filter)) set.add(p);
            }
            if (_isInitialized) _selModel.fireValChanged();
        }
        return this;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Scrollpane, Layout >>> */

    @Override public Dimension getPreferredSize() {
        try {
            return dim(super.getPreferredSize().width, StrapAlign.proteins().length*ICON_HEIGHT);
        } catch (Exception e){return dim(4*EM,4*EX);}
    }

    public JComponent scrollPane() {
        pcp(DialogStringMatch.KEY_SAVE, "Proteins", this);
        final JComponent sp=scrllpn(SCRLLPN_INHERIT_SIZE, this);
        sp.setBorder(BorderFactory.createEtchedBorder());
        return sp;
    } 
    @Override public boolean getScrollableTracksViewportWidth() { return true;}
    /* <<< Scrollpane <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
    @Override public boolean isEnabled(Object o) {
        if (!(o instanceof Protein)) return false;
        return super.isEnabled(o) && (_filter==0 || isEnabled((Protein)o,_filter));
    }

    public static boolean isEnabled(Protein pp[],  long flags) {
        if (pp!=null) for(Protein p:pp) if (isEnabled(p,flags)) return true;
        return false;
    }

    public static boolean isEnabled(Protein p, final long flags) {
        if (p==null) return false;
        boolean e=true;
        if ((flags & NT)!=0 && (p==null || p.getNucleotides()==null)) e=false;
        if ((flags & ACTGN)!=0 && !p.containsOnlyACTGNX()) e=false;
        if ((flags & NT_or_ACTGN)!=0 && (p==null || p.getNucleotides()==null && !p.containsOnlyACTGNX())) e=false;
        if ((flags & NOT_ACTGN)!=0 && p.containsOnlyACTGNX()) e=false;
        if ((flags & NO_CALPHA)!=0 && p.getResidueCalpha()!=null) e=false;
        if ((flags & CALPHA)!=0 && p.getResidueCalpha()==null) e=false;
        if ((flags & EC)!=0 && p.getEC()==null) e=false;
        if ((flags & PDB_ID)!=0 && p.getPdbID()==null) e=false;
        if ((flags & NO_PDB_ID)!=0 && p.getPdbID()!=null) e=false;
        if ((flags & IN_ALI)!=0 && (p==null || !p.isInAlignment())) e=false;
        if ((flags & NOT_IN_ALI)!=0 && (p==null || p.isInAlignment())) e=false;
        if ((flags & CHAINS)!=0 && p.getChainsAsString().length()<2) e=false;
        if ((flags & IN_3D)!=0 && p.getProteinViewers().length==0) e=false;
        if ((flags & SECSTRU)!=0 && p.getResidueSecStrType()==null) e=false;
        if ((flags & SOLVENT)!=0 && p.getResidueSolventAccessibility()==null) e=false;
        if ((flags & INFERRED_3D)!=0 && p.getInferred3dCountMatches()==null) e=false;
        if ((flags & NO_3D_FILE)!=0 && p.isLoadedFromStructureFile()) e=false;
        if ((flags & (RES_SEL|RES_ANNO|SEQ_FEATURE))!=0) {
            final ResidueSelection ss[]=p.allResidueSelections();
            if ((flags & RES_SEL)!=0     && ResSelUtils.countType('S',ss)==0) e=false;
            if ((flags & RES_ANNO)!=0    && ResSelUtils.countType('A',ss)==0) e=false;
            if ((flags & SEQ_FEATURE)!=0 && ResSelUtils.countType('F',ss)==0) e=false;
        }
        if ((flags & (HETERO3D|NUC3D))!=0) {
            int countHet=0, countNuc=0;
            for(HeteroCompound h : p.getHeteroCompounds('*')) if (h.isNucleotideChain()) countNuc++; else countHet++;
            if ((flags & HETERO3D)!=0 && countHet==0) e=false;
            if ((flags & NUC3D)!=0 && countNuc==0) e=false;
        }
        return e;
    }
  public static Protein[] onlyProteins(long flags, Protein pp[]) {
    if (countNotNull(pp)==0) return Protein.NONE;
    final ArrayList v=new ArrayList(pp.length);
    for(Protein p : pp)  if (isEnabled(p,flags)) adUniq(p,v);
    return toArry(v,Protein.NONE);
  }

}
