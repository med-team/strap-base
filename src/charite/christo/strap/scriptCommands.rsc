3D_native viewer type [jmol|astex|pymol] , 3D-command
 E 3D_native jmol, 3D_label "This label appears only under Jmol"
 E 3D_native astex, 3D_label "This label appears only under Astex"
 H The command is send only under the condition, that the current 3D view is of the given type.
 H This allows for native commands that are spicific to certain viewer types but not understood in other 3D-viewers.

echo bla bla bla
 H The text is printed to standard output (console) or to a file specified with -scriptOutput=filePath
 E echo Hello world

print_proteins
 H Prints a list of all proteins to standard output (console) or to a file specified with -scriptOutput=filePath

print_residue_selections proteins
 H Prints a list of all residue selections of the proteins

print_3D_transformation proteins
 H Prints the 3x3 rotation matrix and translation vector of the given proteins.
 H

superimpose proteins
 H Superimposes the proteins. The reference protein is the one that is most similar to all others
 E superimpose *

align proteins
 H Aligns the proteins
 E align *

wait_for command
 H waits for the command to be finished which has been previously started with bg
 E bg align *
 E ... some script lines ...
 E wait_for align

tree proteins
 H Displays a phylogenetic tree
 E tree *

GFF_expasy_features proteins
 H Loads the GFF files and underlines the sequence features
 E GFF_expasy_features *
 H Also see DAS_features

STRAP_to_front
 H The application frame as top level window

iconify
 H The application frame is iconfied

load list of URLs or database IDs or file paths
 H The proteins are loaded

aa_sequence amino acid sequence , protein name
 H Defines a protein by amino acid sequence. The sequence may have gaps "-".
 E aa_sequence -VLSAAERAQVKAAWGKI--QAGAHGAEALERMFLGFPTTKTYPF, Xenopus

ants residues
 H Draws a rubber band rectangle around all given residue selections
 E protein1/20-30  protein2/22-40

box residue
 H Same as ants

balloon_text Text , proteins and residue selections
 H Defines the mouse-over text for proteins and residue selections
 E balloon_text bla bla , protein1
 E balloon_text NULL , protein1

close_3D view ID , proteins
 H Removes the proteins from the 3D view. If no proteins are given, the 3D view is closed.

download_protein_files proteins
 H The accession ID is used to download the original protein file.
 H See command accession_id

hide proteins
 H The proteins are removed from the current alignment but remain in memory.

unhide proteins
 H Proteins previoulsly hidden with hide are put back into the alignment

jalview proteins
 H The proteins are shown in Jalview

put_property key=value , proteins
 E put_property NOT_USE_STRUCTURE_FOR_ALIGNMENT=true ,  *
 E put_property NOT_USE_STRUCTURE_FOR_ALIGNMENT=NULL ,  *

secondary_structure character-sequence , protein
 E secondary_structure ---HHHHHH---HHHHH---EEEEEEEEEE----EEEEEE , Xenopus

set_residue_index_offset number , protein
 H Change the numbering of residues

set_ruler_secondary_structure theProtein
 H The protein should have secondary structure information.
 H A ruler is drawn above or below the alignment.
 H Helices are red and beta sheets are yellow.

set_conservation_threshold number
 H Number is in the range of 0 to 100 and defines the threshold up from where conserved residues are emphasized.
 E set_conservation_threshold 70

set_export_columns from-to
 H Show only the part of the alignment from column number 100 to 200
 E set_export_columns 100-200

set_characters_per_line number
 H Number of letters per line for non-interactive mode with command line export options -toHTML, -toClustal and -toMSF.
 E set_characters_per_line 60

set_color_mode color-type
 H Residue colors.
 H white|black|charge|hydropathy|chemical|nucleotide
 E set_color_mode hydropathy

find_uniprot_id_fast  protein1 protein2 ...
 H Like find_uniprot_id but does not use time consuming Blast.

find_uniprot_id protein1 protein2 ...
 H The Uniprot identifier is important to query certain web resources.
 H The  uniprot entry which matches or contains  the given sequence is determined.

define_protein_complex protein1 protein2 ...
 H A multiprotein complex consisting of several chains is formed.
 H By default, chains of the same PDB entry form a complex.
 H See superimpose_multiprotein_complexes.

superimpose_protein_complexes protein1 protein2 ...
 H Multiprotein complexes as whole are superimposed upon each other.
 H See define_protein_complex

open_3D  ViewID, protein1 protein2 ...
 H The given proteins are opened in a 3D-viewer.
 H ViewID is an identifier which allows this view to be refered to by the commands select_3D and close_3D.

wire protein1 protein2 ...
 H Show 3D protein trace

close_wire proteins
 H Closes the 3D backbone representations

use_3D_viewer jmol
 H Set jmol as the default 3D-viewer. Possible arguments: astex, pymol, jmol.

select_3D 3D-ViewID, proteins or residue selections
 E select_3D  ViewID
 E select_3D  ViewID , subtilisin
 E select_3D  ViewID , subtilisin/100-200
 H Subsequent commands starting with "3D_" such as "3D_select" act on the selected 3D-view, protein (and residue selection).
 H The first argument is the identifier of a 3D-view (See open_3D).
 H The second argument is optional and is one or several proteins in this view or residue selections.
 H Instead of referring to a particular  view ID, the constant strings $ANY_VIEWER or $ANY_WIRE can be used.
 H Note that the command "select_3D" and "3D_select" are different.

accession_id id, protein
 E accession_id UNIPROT:HSLV_ECOLI , hslv_ecoli
 H The accession ID for a protein is set.
 H Providing the accession ID allows direct access to BioDAS services.

add_annotation selection_name, key, value
 H An entry consisting of a key such as "Remark" or "Balloon" and a value such as a free text is added to the named selection.
 E add_annotation myProtein.pdb/myAnnotation1, Balloon, Another line

set_annotation selection_name, key, value
 H like add_annotation but already existing entries with this key are deleted.

remove_xref reference, protein

add_xref reference, protein
 E add_xref PDB:1NED , hslv_ecoli

below_row row-number or protein , list of proteins
 H Change the order of proteins in the alignment.

to_row row-number or protein , list of proteins
 H Change the order of proteins in the alignment.

cds coding sequence expression , myProtein
 E cds join(10 20...30) , protein
 E cds complement(join(10 20...30)) , myProtein
 H The protein myProtein is given as a genomic sequence.
 H This command specifies, how the DNA is translated into protein.

close protein1 protein2 ...
 H The proteins are removed from the view.
 H The remain on the hard disk.

cursor protein/residue
 H The alignment cursor is set to a residue of a protein.
 H The name of a residue selection can be given instead.

DAS_features list of DAS services, list of proteins
 E DAS_features CSA%20-%20extended uniprot cbs_total netphos netoglyc , UNIPROT:P29590
 H Position specific annotations are loaded from the DAS services and underlined in the alignment.
 H Also see GFF_expasy_features

delete residue_selection1 residue_selection2 ... " :
 H The specified residue selection is removed.

gaps x--xxxxxx-xxx-xxxxxx-xx-x , protein
 H Gaps are inserted into the sequence.
 H This allows for alignments computed on the server rather than computed in the client.

icon URL , protein
 E icon http://www.bioinformatics.org/strap/images/dog.png , protein
 E icon NULL , protein
 H The protein icon is displayed in the alignment row at the left.

project_coordinates PDB ID , protein
 E project_coordinates PDB:1BOR_A , UNIPROT:P29590
 E project_coordinates AUTO , UNIPROT:P29590
 E project_coordinates NULL , UNIPROT:P29590
 H The 3D coordinates are infered into the protein.
 H With AUTO, the  most similar PDB entry is determined automatically.

rotate_translate  xx  xy  xz    yx  yy  yz    zx  zy  zz   translation_x  translation_y  translation_z , protein
 H The protein is moved in 3D space.
 H If a protein is given instead of a 3x3 matrix and translation vector then the transformation of that protein is used.
 H  Also see command "superimpose".
 E -0.51  -0.69   0.50   0.22   0.45   0.86  -0.82   0.55  -0.80     91.15 25.13  55.68 , protein
 E  NULL, protein

rotate_translate_protein_complex  xx  xy  xz  yx  yy  yz  zx  zy  zz  translation_x  translation_y  translation_z , protein
 H Works similar as  rotate_translate.
 H In addition, if protein is part of a multiprotein complex,
 H then also all peptide chains of the complex are  rotated and translated.

scroll_to protein/residue [protein/residue ... ]
 H The alignment is scrolled such that residues of interest come into sight.

select protein1 protein2 ... residue_selection1 ...
 H selected proteins are highlighted in the row header.
 H selected residue selections are indicated by marching ants.

unselect protein1 protein2 ... residue_selection1 ...
 H deselects proteins or residue selections.

3D_spheres [on/off]
 H Display the selected amino acids or atoms as spheres.

3D_biomolecule
 E 3D_biomolecule 0x2  $ANY_WIRE, PDB:1a34
 H The biological units are given as a decimal or hexadecimal number which is a bit masks.
 H For example 0x8 means the biological unit 3 since 2^3=8.
 H -1 means the asymmetrical unit.

3D_cartoon  [on/off]
 H Display the selected amino acids as spheres.

3D_center
 H Center the view to the selected amino acids or atoms.

3D_center_amino
 H Center the view to all peptide chains.

3D_color html-color
 E 3D_color #ff0000
 H The color for the selected atoms is given as a 6 digit hexadecimal number.
 H First 2 digits Red, middle 2 digits Green and last 2 digits Blue.

3D_dots [on/off]
 H Draws dots around the selected atoms.

3D_label "text"
 H Sets a text label for the selected atoms.

3D_label_color color
 E 3D_label_color #ff0000
 H Color for text labels created with 3D_label.

3D_lines [on/off]
 H Displays the selected atoms as lines.

3D_mesh [on/off]
 H Draws a mesh around the selected atoms.

3D_object_delete Object-name.
 H Delets an object such as a surface.

3D_ribbons [on/off]
 H Displays the selected amino acids as ribbons.

3D_rotate X angle
 H Rotates angle (degree not radiant) around axis X,  Y or Z.

3D_sa_surface [on/off]
 H Creates a surface object.
 H The color is specified in a previous 3D_surface_color-command.

3D_surface [on/off]
 H Creates a surface object.
 H The color is specified in a previous 3D_surface_color-command.

3D_surface_color RGB-color
 E #77FF00FF
 H The color is given as hexadecimal.
 H If 8 digits, then the first two digits are transparency.

3D_script_panel
 H Opens the native script panel of that viewer.

3D_select expression
 E 3D_select 69.CA
 E 3D_select NADH
 H Selects atoms or amino acids in the active 3D view.

3D_selection_name identifier
 H Sets the name of the selection for the following 3D_select command

3D_sticks [on/off]
 H Displays the selected atoms as stick.

3D_zoom  percentage
 E 3D_zoom 200%

3D_highlight_selected_atoms [milliseconds]
 E 3D_highlight_selected_atoms 2000
 H The selected atoms are highlighted.

3D_highlight_selected_amino_acids [milliseconds]
 E 3D_highlight_selected_amino_acids 2000
 H The cAlpha atoms of the selected residues are highlighted.
