package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.io.File;
import javax.swing.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.strap.SPUtils.*;
import static charite.christo.protein.ProteinMC.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.KeyEvent.*;
import static javax.swing.BorderFactory.*;
/**HELP

   The central view of the Strap application is the alignment panel.  It
   shows the names and sequences of loaded proteins.
   The protein labels can be <b>dragged</b> up or down in the row
   header to change their order.

   In the amino acid sequences white space can be added or removed,
   so-called alignment gaps.

   <br><br><b>Multiple alignment windows: </b> If two or more
   alignment views are displayed at the same time
   (<i>ITEM:StrapAlign#ACTION_newPan</i>), they display the same
   alignment.

   But if two independent Strap sessions are run in parallel, both
   display different alignments.  Proteins and residue selections can
   be exchange copied from one to the other by Drag-n-Drop.

   <br><br><b>Shading</b>:
   Three  residue shadings are available in the tool-bar: "charge", "hydropathy" and
   "chemical".

   Alternatively, secondary structure can be highlighted:  Helices are
   painted red and sheets yellow.

   Further GUI controls are in the context-menu.

   <br><br><b>Editing</b>: Manipulation of the multiple sequence alignment is
   performed with the keyboard.
   Strap has many  sophisticated keyboard commands.
   Please see <i>ITEM:StrapKeyStroke</i>.

   <br><br><b>Cursor</b>: The cursor position is shown as a flashing white rectangle in the alignment window.
   Usually only the alignment row containing the cursor is changed when
   for example a gap is introduced by pressing the space bar.
   If more than one protein is selected, then gaps are introduced into all selected proteins.

   <br><b>Residue selections</b> are highlighted in the multiple sequence
   alignment.  When the mouse is over a highlighted residue a balloon message appears.
   Right-click opens the context menu and Ctrl+left-click selects the selection. It can also be selected by dragging a rectangular region.

   <br><br><b>Scrolling:</b>
   The horizontal
   scroll-bar outlines the entire alignment and shows selections and
   plots. It can be enlarged with the mouse.
   If there are many proteins the vertical scroll-bar is visible.
   But also the horizontal
   scroll-bar can scroll vertically.
   Try  mouse wheel turning  with and without Shift and  Ctrl.
   @author Christoph Gille
*/
public final class StrapView implements ChRunnable,StrapListener,Disposable, PreferredSize, ActionListener, PaintHook, TooltipProvider, ProcessEv {
    public final static String KEY_AP="SV$$AP";
    public final static int
        SECSTRU_HEIGHT=6,
        BUT_LIST_HET=13, BUT_DL_FILE=14,
        BUT_NUCL=15, BUT_SEQVISTA=16, BUT_3D=17, BUT_EXPORT=18,
        BUT_ALIGN=19, BUT_ALIGN_CTRL=20, BUT_ALIGN_UNDO=21, BUT_FEATURE=23, BUT_WARN_MARCHING_ANTS=24, BUT_SORT=25,
        TOG_MAXIMIZE=31, TOG_OUTLINE_HSB=32, TOG_BRIGHTER=33, TOG_SHADE_ONLY_SELECTED=34, TOG_PLOT_ENTIRE_PANEL=35,TOG_PROTECT_COL=36, TOG_INFO=37;
    private final static int
        COLi=0, COLh=1, COLs=2,
        PR_ONLY_CHANGES=1<<0, PR_ALL_SELECTED=1<<2,
        T_ENABLED=11, T_CLEAR_SB=12,
        BUT_RESTORE_CURSOR=6, BUT_SAVE_CURSOR=8,
        MCSV_RESSEL_RECT=1, MCSV_CHARACTERS=2, FREQ_AA=3/*4*/, GAPPED=5/*6*/, RULER_NUM=7;
    private final long[] CACHE_MC=new long[10];
    private final static String
         NSEW_W="w"+(char)('s'+'w')+(char)('n'+'w'),
         NSEW_N="n"+(char)('n'+'e')+(char)('n'+'w'),
         NSEW_S="s"+(char)('s'+'e')+(char)('s'+'w'),
         NSEW_E="e"+(char)('s'+'e')+(char)('n'+'e'),
         ANSEW="A"+NSEW_W+NSEW_N+NSEW_S+NSEW_E;
    private final Color _colorCur= C(0xFFffFF,88), COLOR_HL=C(0xFFff10);
    private static int _flashResSel, _wInfo;
    private final StrapKeyStroke _ks=new StrapKeyStroke(this);
    private Object _noArrowHeads;
    private ChCombo _vopClass;
    private static ChCombo _choiceI;
    private final JComponent
        _pRH[]={null,null}, _pSH[]={null,null},
        _pC, _pS, _pR, _rh=pnl(), _ri=pnl(), _panel, _panSB,
        _tfCharSpc[]={
            new ChTextField(isMac()?"5":"3").saveInFile("Strap_hCharSpace").li(this),
            new ChTextField(isMac()?"5":"3").saveInFile("Strap_vCharSpace").li(this)
        };
    private boolean _disposed,  _painted, _shadingSB=true, _cursorBright, _repaintOnMouse, _hideSB;
    private int
        _notYetED,
        _dnd, /* S=Selection, P=Protein */
        _xDragFrom, _yDragFrom,
        _whatDrag,_whatMouse, /* 0=nothing dragged, A=new rubber band or new selection, P=protect ali, H=resize, n,s,e,w = change size existing rubber band. */
        _rowPressed, _colPressed,
        _countCol, _protectFromCol, _selIntervallRow=-1, _selIntervallCol=-1,
        _rhRowShift=-1,
        _rhRow=-1,
        _reduceHeight,_reduceHeightSave,
        _lineSkip=1,
        _fontSize=maxi(5,getPrpty(StrapView.class,"fontSize",16)),
        _whereTip,
        _scrollV,
        _charWidths[],
        _cursorAA, _cursorCol,
        _mc, _ppMC, _ppVisMC, _revalMC, _revalMC2, _inAliMC=-1, _rubMC, _rubProtMC, _countColMC=-1;
    private static long _whenKeyPress;
    private Point _zoomCenter;
    private static SequenceAligner _aligner;
    private final static byte TRIPLET[]=new byte[3];
    private static boolean _anyPainted, _mouseEnterDragTarget;
    private Protein _ppR[], _pp[], _ppVis[], _ppShading[]=Protein.NONE, _ppGapped[], _cursorP, _rhProtsDrag[], _vopOtherProt;
    private final Map<Object,Object[]> mapSavedCursor=new HashMap();
    private final HScrollBar _hSB=new HScrollBar(this);
    final ChScrollBar _vSB=new ChScrollBar(JScrollBar.VERTICAL), _hSB0=new ChScrollBar(JScrollBar.HORIZONTAL);
    private final ChTableLayout _tLayout;
    private static Runnable _runAnts;
    private static Protein[] _ppClipboard;
    private static StrapView _inst;
    private static StrapView instance() { if (_inst==null) _inst=new StrapView(false); return _inst;}
    private Object _rCached;
    Object[] cached() {
        Object[] r=deref(_rCached, Object[].class);
        if (r==null) _rCached=newSoftRef(r=new Object[10]);
        return r;
    }
    public StrapView(boolean real) {
        super();
        final EvAdapter li=evAdapt(this);
        if (!real) { _tLayout=null; _panSB=_panel=_pR=_pS=_pC=null; return;}
        if (!withGui()) stckTrc();
        _pC=pnl();
        _pS=pnl();
        _pR=pnl();
        for(int i=2; --i>=0;) {
            _pRH[i&1]=pnl(dim(10, _rulerCB.height+2));
            _pSH[i&1]=pnl();
        }
        final ChJScrollPane
            sp=scrllpn(SCRLLPN_ORIG_SB, _pC),
            spRuler=scrllpn(SCRLLPN_NO_VSB, _pR),
            spSecStru=scrllpn(SCRLLPN_NO_VSB, _pS);
        pcp(KEY_TIP_LOCATION, new Point(100,20),_rh);
        addProteins(cursorRow()+1,StrapAlign.proteins());
        //setMinSze(33,33, _rh);
        setAutoscr(_rh);
        setAutoscr(_ri);
        final ChTableLayout tl=_tLayout=new ChTableLayout(ChJTable.NO_REORDER);
        pcp(KOPT_NOT_INDICATE_TOO_SMALL,"",tl);
        spRuler.setHorizontalScrollBar(_hSB0);
        spSecStru.setHorizontalScrollBar(_hSB0);
        sp.setHorizontalScrollBar(_hSB0);
        _panSB=pnl(CNSEW,null,pnl(CNSEW,_hSB0, null,null,null, _butSB));
        _panel=pnl(CNSEW,tl, null, _panSB);
        setTheFont("Dialog",0,true);
        //setMinSze(1,1, sp);
        for(int i=0; i<2; i++) {
            final ChJScrollPane s=scrllpn(i==0?_ri:_rh);
            rmFromParent(s.getHorizontalScrollBar());
            rmFromParent(s.getVerticalScrollBar());
            //setMinSze(1,1, s);
            s.setVerticalScrollBar(_vSB);
            tl.addCol(0, pnl(CNSEW, s,_pRH[i], _pSH[i], KOPT_NOT_INDICATE_TOO_SMALL));
            TabItemTipIcon.set(null,i==0? "Protein information" : "Protein names",null,null,s);
            s.setBorder(createMatteBorder(0,0,0,i==0?EM:2, C(DEFAULT_BACKGROUND)));
        }
        sp.setVerticalScrollBar(_vSB);
        addCP(KEY_CLOSE_HOOKS, thrdCR(this,"X"), sp);
        tl
            .addCol(CLOSE_ALLOWED|CLOSE_NO_REMOVE, pnl(CNSEW,sp, spRuler, spSecStru))
            .setTitle(COLh,"Row headers")
            .setTitle(COLi,"Protein information")
            .setTitle(COLs,"Sequences")
            .t().setW(0,0,0,COLi).setW(16*EM, EM,2222, COLh).setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        enableDisable(this);
        final Object
            dia=StrapAlign.dialogPanel(),
            header=setTip(
                          "the amino acid at the cursor position"+
                          "<ul><li><b>i</b>: residue index starting with 1</li>"+
                          "<li><b>c</b>: column (horizontal position) including gaps </li"+
                          "<li><b>p</b> residue number in the structure (pdb)  file</li>"+
                          "<li><b>n</b> nucleotide index and coding triplet</li></ul>",
                          tl.th());
        if (0==sze(listServices(false, button(BUT_ALIGN), TooltipProvider.class))) setTip(this,button(BUT_ALIGN));

        for(Object c : new Object[]{gcp(ChTabPane.KEY_PANE1,dia), gcp(ChTabPane.KEY_PANE2,dia), _hSB, _hSB0, _butSB, _vSB,_pC,_pR,_pS,_pSH[0],_pSH[1],_pRH[0],_pRH[1], header}) {
            li.addTo("m", c);
            pcp("MB", "", c);
        }
        li.addTo("Mkmw", _rh).addTo("mw", _ri).addTo("Mm",header).addTo("Mfkmwc",_pC).addTo("Mmw", _hSB).addTo("c",sp);
        pcp(KEY_LOOKS_LIKE_DIVIDER,"",header);
        final Object rThis=wref(this);
        for(Component c : new Component[]{_pC,_pR,_pSH[0], _pSH[1],_pS,_rh,_ri,tl,_panel,_vSB, _hSB}) {
            if (c==_pC||c==_pS||c==_pR) setTip(this,c);
            if (c==_pC||c==_pS||c==_pR||c==_rh||c==_ri||c==_panel||c==tl||c==_pSH[0]||c==_pSH[1]) setPrefSze(this,c);
            if (c==_pC||c==_pS||c==_pR||c==_rh||c==_ri||c==_panel||c==_vSB||c==_hSB) {
                addPaintHook(this,c);
                pcp(StrapView.class,rThis,c);
            }
            final char ds= c==_rh||c==_ri||c==_pC?'b' : c==_pS||c==_pR ? 'H' : ' ';
            if (ds!=' ') setDragScrolls(ds, c, null);
            if (c==_pR||c==_pS||c==_pC||c==_rh||c==_ri) pcp(ChPanel.KOPT_UPDATE_SIZE,"", c);
        }
        noBrdr(spSecStru);
        noBrdr(sp);
        noBrdr(spRuler);
        noBrdr(_hSB);
        addMoli(MOLI_REFRESH_TIP,_pC);
        addActLi(this,Customize.customize(Customize.proteinInfoRplc));
    }
    public void dispose() {
        if (_disposed) return;
        _disposed=true;
        StrapAlign.closeAlignmentPanel(this);
        rmAllListnrs(true, panel());
        StrapAlign.rmListener(this);
        rmAllChilds(panel());
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Model >>> */
    private final UniqueList<Protein[]> vROWS=new UniqueList(Protein[].class).rmOptions(UniqueList.UNIQUE);//.addOptions(UniqueList.ASSERT_EDT);
    { vROWS.ensureCapacity(5555); }
    public int countRows() { return vROWS.size();}
    List<Protein[]> list() { return vROWS;}
    public Protein proteinInRow(int row) { return get(0,get(row,vROWS),Protein.class);}
    public int mc() { return _mc+vROWS.mc(); }
    public int findRow(Protein p) {
        if (p!=null) {
            final int nR=countRows();
            for(int row0=-1; row0<nR; row0++) {
                final int row=row0<0?p.getRow() : row0;
                if (row<0) continue;
                final Protein[] ppRow=proteinsInRow(row);
                if (ppRow.length==1 && ppRow[0]==p || cntains(p,ppRow)) {
                    p.setRow(row);
                    return row;
                }
            }
        }
        return -1;
    }
    private boolean addProteinToRow(boolean top, Protein p,int row) {
        if (p==null) return false;
        final List<Protein[]> v=list();
        final Protein[] ppRow=v.get(row);
        if (ppRow==null) return false;
        final Protein[] ppNew=
            top ? set1stElement(p,ppRow,0, Protein.class) :
            adToArray(p,ppRow, 0, Protein.class);
        if(ppNew!=ppRow) v.set(row,ppNew);
        final boolean changed=ppRow!=ppNew || ppRow[0]!=ppNew[0];
        if (changed) _mc++;
        return changed;
    }
    public void addProtein(int afterRow, Protein p) {
        if (p!=null) list().add(maxi(0,afterRow), spp(p));
    }
    public Protein[] proteinsInRow(int row) {
        final Protein pp[]=list().get(row);
        return pp!=null ? pp : Protein.NONE;
    }
    public void removeProteins(Protein... pp) {
        for(int row=countRows(); --row>=0; ) {
            final Protein ppRow[]=proteinsInRow(row);
            boolean changed=false;
            for(Protein p : pp) changed|=setToNull(p,ppRow);
            if (changed) {
                list().set(row, rmNullA(ppRow, Protein.class));
                _mc++;
            }
        }
    }
    int removeEmptyRows() {
        final List<Protein[]> v=list();
        int count=0;
        for(int i=countRows();--i>=0;) {
            final Protein[] ppRow=v.get(i);
            if (ppRow==null || ppRow.length==0) v.remove(i);
            else count+=ppRow.length;
        }
        return count;
    }
    // ----------------------------------------
    public Protein[] pp() {
        if (!isEDT()) {
            final Protein[] ret[]={null};
            inEDT(thrdCR(this,"pp",ret));
            return ret[0];
        }
        final int  mc=mc();
        final List<Protein[]> v=list();
        Protein[] pp=_pp;
        if (pp==null||_ppMC!=mc) {
            _ppMC=mc;
            final int nR=v.size(), nP=removeEmptyRows();
            if (pp==null || pp.length!=nP) pp=new Protein[nP];
            String error=null;
            int iP=0;
            nextRow:
            for(int iR=0; iR<nR; iR++) {
                final Protein[] ppRow=v.get(iR);
                if (ppRow==null || ppRow.length==0) error="sze(ppRow)==0";
                else {
                    for(Protein p : ppRow) {
                        if (iP>=nP) {
                            error="iP>=nP "+iP;
                            break nextRow;
                        } else if (p==null) {
                            error="p==null "+iP;
                        } else pp[iP++]=p;
                    }
                }
            }
            if (iP!=nP) error="iP!=nP";
            if (error!=null) putln(RED_ERROR+"StrapView ",error);
            _pp= error!=null ? rmNullA(pp, Protein.class) : pp;
        }
        return pp;
    }
    public Protein[] visibleProteins() {
        final Protein[] ppAll=pp();
        final List<Protein[]> v=list();
        final int nR=v.size();
        if (nR==ppAll.length) return ppAll;
        Protein[] pp=_ppVis;
        final int mc=mc();
        if (_ppVisMC!=mc || pp==null) {
            _ppVisMC=mc;
            if (pp==null || pp.length!=nR) pp=new Protein[nR];
            boolean error=false;
            for(int iR=0; iR<nR; iR++) {
                final Protein[] ppRow=v.get(iR);
                if (ppRow==null || ppRow.length==0) error=true;
                else pp[iR]=ppRow[0];
            }
            _ppVis= error ? rmNullA(pp, Protein.class) : pp;
            if(error) assrt();
        }
        return pp;
    }
    /* <<< Model, Proteins <<< */
    /* ---------------------------------------- */
    /* >>> GUI Elements >>> */
    private static ChSlider _sliderSim;
    public static ChSlider sliderSimilarity() {
        if (_sliderSim==null) {
            final ChSlider s=_sliderSim=new ChSlider("SLIDER_SIMIL",-100,100,50)
                .tt("Highlighting sequence positions<ul><li>Slider Left: Differing positions</li><li>Slider middle: All</li><li>Slider right: Conserved positions</li></ul>")
                .endLabels("Diff", "All","Conserv");
            pcp(KOPT_HIDE_IF_DISABLED,"",s);
            setPrefSze(false, 20*EM,2*EX, s);
            pcp(KEY_ADAPTED_SIZES, new Dimension[]{dim(20*EM,2*EX),dim(10*EM,2*EX)}, s);
            addActLi(instance(),s);
        }
        return _sliderSim;
    }

    private static ChButton[] _button;
    public static ChButton button(int t) {
        if (t==BUT_WARN_MARCHING_ANTS) {
            final ChButton b=
                ChButton.doView("The rubber band selection of the alignment panel is created and resized by dragging the mouse.<br>"+
                                "It is indicated by red/white WIKI:Marching_ants.<br>"+
                                "The rubber band has a context menu (Right click) where it can be removed.")
                .cp(KOPT_NOT_PAINTED_IF_DISABLED,"")
                .setOptions(ChButton.NO_FILL|ChButton.NO_BORDER)
                .t("Warning: Rectangular alignment region is defined. ")
                .enabled(false);
            _vAnts.add(wref(b));
            return b;
        }
        if (_button==null) {
            _button=new ChButton[99];
            for(int i=99; --i>=0; ) {
                final ChButton b=_button[i]=
                    i==TOG_MAXIMIZE? toggl(ChButton.DISABLED,"").rover(IC_FIT_TO_WIDTH).tt("Maximize alignment pane") :
                    i==TOG_BRIGHTER? toggl("Brighter").tt("Unconservered amino acids appear brighter.") :
                    i==TOG_OUTLINE_HSB? toggl("Highlight conserved position in the scroll-bar").tt("Draw colored aminoacids in horizontal scroll bar") :
                    i==TOG_SHADE_ONLY_SELECTED? toggl("Highlight conserved position only for the selected sequences") :
                    i==TOG_PLOT_ENTIRE_PANEL? toggl("Plot over all rows").s(true) :
                    i==TOG_PROTECT_COL? toggl("Protect the alignmen^t up from a certain position")
                    .tt("When the alignment is edited, it is protected right from the black/white line.<br>Gaps can not be altered beyond this point.") :
                    i==TOG_INFO? toggl().rover(IC_INFO).tt("Protein description in row header.") :
                    i==BUT_3D? new ChButton(" 3D ").tt("Backbone representation of the 3D-structure.") :
                    i==BUT_NUCL? new ChButton(ChButton.HIDE_IF_DISABLED,"DNA").tt("If the character sequence contains only the letters a c t g and n<br>a nucleotide view is opened.") :
                    i==BUT_SEQVISTA? new ChButton(ChButton.HIDE_IF_DISABLED,"SeqVISTA").tt("SeqVISTA presents a holistic, graphical view of features annotated on nucleotide sequences.<br>http://zlab.bu.edu/SeqVISTA/") :
                    i==BUT_EXPORT? new ChButton().rover(IC_WINWORD).tt("Exporting the alignment to web browser, MS-Word, Jalview, PDF ... ") :
                    i==BUT_FEATURE? new ChButton("Sequence Features ...").cp(KEY_SMALL_TEXT, "S.F.").tt("Download Sequence Features. Highlight glycosylated, phosphorylated, metal binding ... residues.") :
                    i==BUT_RESTORE_CURSOR? new ChButton(ChButton.DISABLED|ChButton.HIDE_IF_DISABLED,"Revert Cursor").cp(KEY_SMALL_TEXT,"R.C.") :
                    i==BUT_SORT?  new ChButton("SORT").t("Sort proteins ...").i(IC_SORT) :
                    i==BUT_SAVE_CURSOR? new ChButton(ChButton.DISABLED|ChButton.HIDE_IF_DISABLED,"Save cursor").cp(KEY_SMALL_TEXT,"S.C.") :
                    i==BUT_ALIGN_UNDO? new ChButton(ChButton.DISABLED,"Undo align").cp(KEY_SMALL_TEXT,"Undo").tt("Restore alignment gaps to what it was before the align button was pressed.") :
                    i==BUT_ALIGN? new ChButton(ChButton.DISABLED,null) :
                    i==BUT_ALIGN_CTRL ? new ChButton(ChButton.DISABLED,"Control panel of last computation") :
                    i==BUT_LIST_HET ? new ChButton().doClose(0,REP_PARENT_WINDOW).i(IC_FLAVIN).tt("Child objects such (DNA/RNA, ligands...)") :
                    i==BUT_DL_FILE ? new ChButton().rover(IC_DOWNLOAD).cp(KOPT_HIDE_IF_DISABLED,"")
                    .tt("This button acts on sequences that are not yet associated to a protein file.<br>"+
                        "It loads information like protein name, organism, EC-number etc.<br>"+
                        "For each protein a protein file is requested from Uniprot.<br>"+
                        "This requires a protein ID.<br>"+
                        "If no accession ID is associated to the protein a Uniprot ID is found for the aminoacid sequence.<br>") :
                    null;
                if (b!=null) {
                    if (i==BUT_RESTORE_CURSOR || i==BUT_SAVE_CURSOR) b.tt("[S]ave and [R]estore the cursor and scroll position.<br>The modifier keys meta shift and control can be used in any combination to store more than one positions.");
                    if (i==BUT_SEQVISTA||i==TOG_MAXIMIZE||i==BUT_ALIGN_UNDO||i==BUT_LIST_HET) pcp(KOPT_HIDE_IF_DISABLED,"",b);
                    b.li(instance());
                }
            }
        }
        return _button[t];
    }
    /* <<< GUI Elements <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    private final ChDelay _threadPR[]=new ChDelay[20];
    private ChDelay threadPR(int option) {
        if (_threadPR[option]==null) _threadPR[option]=(ChDelay)thrdCR(this,"paintRegion", intObjct(option), new long[ARRAY_DURATION]);
        return _threadPR[option];
    }
    private ChDelay thread(int option) {
        if (_threadPR[option]==null) _threadPR[option]=(ChDelay)thrdCR(this,option+"thread");
        return _threadPR[option];
    }
    private boolean flashResSel(boolean dark) {
        if (dark) _flashResSel=-2;
        final int n=sze(_resSelMouse), i=_flashResSel<0 ? 1: _flashResSel%(n+1);
        ResSelUtils.noHide(_noArrowHeads=get(i,_resSelMouse));
        if (n>0) _pC.repaint(_selBounds);
        return n>0;
    }
    @Override public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="DL_P_FILE") {
            final Component b=button(BUT_DL_FILE), par=parentC(b);
            rmFromParent(b);
            SPUtils.fetchIdsAndDownloadOriginalProteins(true, visibleProteins());
            toContainr(0,b,par);
            repaintC(_ri);
            ChMsg.speak("Information loaded");
        }
        if (id=="CLRHL" && _colorHlp==COLOR_HL)  highlightRows(null);
        if (id=="X") StrapAlign.closeAlignmentPanel(this);
        if (id=="pp") ((Protein[][])arg)[0]=pp();
        if (id=="scrRectToVis") { sleep(atoi(argv[3])); scrRectToVis((Rectangle)argv[0], argv[1]!=null, atoi(argv[2])); }
        if (id.endsWith("thread")) {
            final int atoi=atoi(id);
            if (atoi==T_CLEAR_SB) repaintC(_hSB.clearImage());
            else if (atoi==T_ENABLED) enableDisable(this);
        }
        if (id=="FLASH") {
            boolean ants=StrapAlign.coSelected().residueSelections('s').length>0;
            for(StrapView v : StrapAlign.alignmentPanels()) {
                if (!v.flashResSel(false) && System.currentTimeMillis()-_whenKeyPress>1000 ) {
                    if ( 0==(_flashResSel&1)) {
                        final boolean bright=0==(_flashResSel&2);
                        inEdtLaterCR(v,"FLASH_EDT",bright?"":null);
                    }
                }
                ants=ants||v.rectRubberBand()!=null;
            }
            _flashResSel++;
            if (ants) inEdtLater(_runAnts==null ? _runAnts=thrdCR(instance(),"ANTS") : _runAnts);
        }
        if (id=="FLASH_EDT") {
            final boolean bright=arg!=null;
            repaintCursor(bright);
            StrapAlign.doRepaintCursor(bright);
        }
        if (id=="ANTS") {
            for(StrapView v : StrapAlign.alignmentPanels()) {
                try {
                    v.ants(v._pC.getGraphics(), prefH(v._hSB)<=0?null:v._hSB.getGraphics());
                } catch(Throwable ex){ stckTrc(ex);}
            }
        }
        if (id=="AFTER_ALIGN") {
            final SequenceAligner aligner=(SequenceAligner)argv[0];
            final long time=System.currentTimeMillis()-atol(argv[1]);
            if (!isEDT()) {
                final Runnable t=thrdCR(this, id, arg);
                inEdtLater(t);
                inEDTms(t,999);
            } else {
                setEnabld(true, button(BUT_ALIGN_UNDO));
                setButAlign(0);
                setEnabld(ctrlPnl(_aligner)!=null, button(BUT_ALIGN_CTRL));
                final boolean success=aligner!=null && aligner.getAlignedSequences()!=null;
                if (argv[2]!=null && time>4444) {
                    argv[2]=null;
                    ChMsg.speakBG(success ? "Alignment finished" : "Alignment failed", success? ChMsg.SOUND_SUCCESS:ChMsg.SOUND_FAILURE);
                }
                StrapAlign.drawMessage(success ? "@1"+ANSI_GREEN+"Alignment finished" : "@1"+RED_WARNING+"Alignment failed");
            }
        }
        if (id=="paintRegion") {
            final Graphics g=_pC.getGraphics();
            if (g!=null) {
                final Rectangle v=visibleRowsAndCols();
                final int mode=atoi(arg), x1=x(v),x2=x2(v);
                paintRegion(g, x1, x2,  y(v), y2(v), mode);
                if (mode==PR_ONLY_CHANGES) {
                    paintSecStru(null, x1, x2,true);
                    paintRuler(null, x1, x2,true);
                    paintEntropy(null, x1, x2,true);
                }
                plot(g);
            }
        }
        return null;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> Rows and Columns >>> */
    private final Rectangle _rectRowsCols=new Rectangle(), _rectXYWH=new Rectangle(), _selBounds=new Rectangle();
    public int countColumns() {
        if (_panel==null) return 0;
        final Protein pp[]=pp();
        final int mc=mc()+Protein.MC_GLOBAL[MCA_ALIGNMENT];
        if (_countColMC!=mc) {
            _countColMC=mc;
            _countCol=SPUtils.maxColumn(pp)+1;
        }
        return _countCol;
    }
    public Rectangle visibleRowsAndCols() {
        final Rectangle v=visibleXYWH();
        setRect( x2col(x(v)), y2row(y(v)), x2col(v.width-1)+1, y2row(v.height-1)+1, _rectRowsCols);
        return _rectRowsCols;
    }
    public int row2y(int row) { return (charB().height+_lineSkip)*row; }
    public int col2x(int col) { return charB().width*col; }
    public int x2col(int x) {
        final int w=charB().width;
        return x<0||w<=0 ? -1 : x/w;
    }
    public int y2row(int y) {
        final int h=charB().height+_lineSkip;
        return y<0||h<=0 ? -1 : y/h;
    }
    private  void setProtectFromColumn(int col, boolean toggle) {
        final ChButton b=button(TOG_PROTECT_COL);
        if (col<=0) b.s(false);
        boolean sel=b.s();
        if (toggle) b.s(sel=!sel);
        _protectFromCol=sel && col>0 ? col : 0;
        repaintC(_pC);
        repaintC(_hSB);
    }
    /* <<< Rows and Columns <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    private int plotAreaH(char what) {
        if (what=='S') return secStruP()==null?0: SECSTRU_HEIGHT;
        if (what=='E') return countRows()<2?0: SECSTRU_HEIGHT;
        return 0;
    }
    @Override public Dimension preferredSize(Object c) {
        if (c==_pR) return dim(wdth(paneSize()), prefH(_pRH[0]));
        if (c==_pS || c==_pSH[0] || c==_pSH[1]) return dim( c==_pS?wdth(paneSize()):1, plotAreaH('E')+plotAreaH('S'));
        if (c==_pC) return paneSize();
        if (c==_rh || c==_ri) return dim(_tLayout.columnWidth(c==_rh?COLh:COLi), hght(paneSize()));
        if (c==_toolbar) return 0==(Protein.optionsG()&Protein.G_OPT_VIEWER) && StrapAlign.alignmentPanels().length==0 ? dim(0,0) : null;
        if (c==panel()) {
            if (StrapAlign.button(StrapAlign.TOG_MAXIMIZE_DIALOGS).s()) return dim(0,0);
            final int tbH=prefH(toolbars()), ah=StrapAlign.panelHeight(), h=maxi( prefH(parentC(_hSB)), mini(ah, prefHeight()));
            return dim(1, maxi(2*EX+tbH, mini(h, ah-tbH)));
        }
        return null;
    }
    private void mayRevalidate() {
        final int mc=Protein.MC_GLOBAL[MC_RESIDUE_SELECTIONS]+Protein.MC_GLOBAL[MCA_PROTEINS_V]+
            (SequenceFeatures.onlySelected() ? modic(StrapAlign.selectedObjectsV()) : 0);
        if (_revalMC!=mc) { /* Fehler mc erhoeht sich beim blinken unter mouse pointer */
            _revalMC=mc;
            SequenceFeatures.updateNames();
            final int newSpc=ResSelUtils.layoutFeatures(pp())*3;
            if (_lineSkip!=newSpc) {
                setSpaceBetweenRows(newSpc);
                correctHeight();
                revalAndRepaintC(panel());
                setMarchingAnt(ANTS_SEARCH, _pC, null, null, null); /* Clear NextAndPrevSelection */
            }
        }
        final int mc2=(countColumns()<<16)|countRows();
        if (_revalMC2!=mc2) {
            _revalMC2=mc2;
            panel().getParent().invalidate();
            revalAndRepaintC(panel());
        }
    }

    public void setSpaceBetweenRows(int space) {
        final int old=_lineSkip;
        _lineSkip=maxi(1,space);
        if (old!=_lineSkip && _painted) {
            repaintC(_pC);
            _vSB.setValue(_vSB.getValue()*(charB().height+_lineSkip)/(charB().height+old));
        }
    }
    private final Dimension _dimPan=new Dimension();
    public JComponent panel() { return _panel; }
    Dimension paneSize() {
        _dimPan.setSize(col2x(2+countColumns()), row2y(countRows()));
        return _dimPan;
    }
    public Rectangle visibleXYWH() {
        if (isEDT() && _hSB!=null) {
            final JViewport vp=scrllpn(_pC).getViewport();
            setRect(-x(_pC),-y(_pC),wdth(vp), hght(vp), _rectXYWH);
        }
        return _rectXYWH;
    }
    private Rectangle _charBounds;
    Rectangle charB() {
        if (_charBounds==null) {
            _charBounds=new Rectangle(0,0,1,1);
            setTheFont("",0,true);
        }
        return _charBounds;
    }
    private final int _zoomReduceHeight[]=new int[100];
    private void setTheFont(String fontName, int size, boolean isAbsolute) {
        final Rectangle rVis=_rectRowsCols;
        if (rVis!=null && _painted && _zoomCenter==null) _zoomCenter=new Point( x(rVis)+rVis.width/2, y(rVis)+rVis.height/2);
        final String fn=fontName!=null && fontName!="" ? fontName : getPrpty(StrapView.class,"fontName","Monospaced");
        if (size!=0) {
            final int fs=_fontSize=maxi(3, isAbsolute ? size : _fontSize+size);
            if (fs<_zoomReduceHeight.length && _zoomReduceHeight[fs]>=0) reduceHeight(mini(_reduceHeight, _zoomReduceHeight[fs]));
        }
        final Font font=new Font(fn,Font.PLAIN,_fontSize);
        if (_fontSize<40) setPrpty(StrapView.class,"fontSize",_fontSize);
        setPrpty(StrapView.class,"fontName",fn);
        panel().setFont(font);
        final Font rhFont=getFnt(font.getSize(), true, Font.PLAIN);
        _rh.setFont(rhFont);
        _ri.setFont(rhFont);
        final Rectangle cbOld=_charBounds, cb=_charBounds=new Rectangle(chrBnds(font));
        cb.width+=atoi(toStrg(_tfCharSpc[0]));
        cb.height+=atoi(toStrg(_tfCharSpc[1]));
        final Point zc=_zoomCenter;
        if(zc!=null) scrRectToVis(new Rectangle( col2x(x(zc)), row2y(y(zc)),  1,1), true);
        if (cbOld!=null) reduceHeight(_reduceHeight-(cbOld.height-cb.height)*countRows());
        _hSB.setUnitIncrement(cb.width);
        _hSB0.setUnitIncrement(cb.width);
    }

    public void showMinRows(int n) {
        final int h=charB().height+_lineSkip*n, pH=_pC.getHeight();
        if (pH-_reduceHeight<h) reduceHeight(pH-h);
    }
    public static void correctHeight1() {
        if (!isEDT()) inEdtLater( thrdM("correctHeight1",StrapView.class));
        else {
            StrapView v=StrapAlign.alignmentPanel();
            if (v==null) v=get(0,StrapAlign.alignmentPanels(),StrapView.class);
            if (v!=null) {
                final int old=v._reduceHeight;
                final int neu=v.row2y(v.countRows())-hght(StrapAlign.frame())/3;
                if (neu>old) v.reduceHeight(neu);
            }
        }
    }
    static boolean correctHeight() {
        long sumH=0;
        final StrapView[] vv=StrapAlign.alignmentPanels();
        for(StrapView v: vv) sumH+=v.prefHeight();
        final long tooHigh=  sumH-StrapAlign.panelHeight()+ prefH(_toolbar);
        if (tooHigh>0 && sumH>0) {
            for(StrapView v: vv) {
                v._reduceHeight=(int)(v._reduceHeight+tooHigh*v.prefHeight()/sumH);
                ChDelay.revalidate(v.panel(),222);
            }
        }
        boolean maximize=StrapAlign.dividerLocation(true)>10 || StrapAlign.dividerLocation(false)>10;
        for(StrapView v: vv) maximize=maximize|| v._reduceHeight>1 || v._reduceHeightSave>0;
        setEnabld(maximize, button(TOG_MAXIMIZE));
        return tooHigh>0;
    }
    public void reduceHeight(int h) {
        final int old=_reduceHeight, neu=mini(hght(paneSize())+EM, maxi(h,0));
        _reduceHeight=neu;
        if (_fontSize<_zoomReduceHeight.length) _zoomReduceHeight[_fontSize]=_reduceHeight;
        if (correctHeight() || old!=neu) {
            panel().invalidate();
            scrllpn(_pC).invalidate();
            revalAndRepaintC(scrllpn(_pC));
            revalAndRepaintC(panel());
        }
    }
    private int prefHeight() {
        return prefH(_pSH[0])+prefH(_pRH[0])+prefH(_panSB)+row2y(countRows()) + prefH(_tLayout.th())+EX/2-_reduceHeight;
    }

    /* <<< Layout <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    private static String _tip;
    private final static String RC4CM="<br><font color=#FF4455><b>Right-click to open context menu</b></font>";
    public String provideTip(Object objOrEv) {
        final AWTEvent ev=deref(objOrEv, AWTEvent.class);
        final Object c=evtSrc(objOrEv);
        final boolean shift=isShift(ev);
        final Rectangle cb=charB();
        final int modi=modifrs(ev), x=x(ev), y=y(ev), lineH=cb.height+_lineSkip, row=y/lineH, col=x/cb.width;
        BA sb=null;
        if (c==button(BUT_ALIGN)) {
            return
                "The selected or all proteins are aligned using 3D-structures and AA sequences.<br><br>"+
                "Hold <u>shift</u> to infer order.<br><br>If  more control is required then use the Align-dialog instead.<br><br>"+
                "<u>Right-click button</u> for context menu (Choose method, show details of last computation).<br>"+
                "<br>Current method: "+StrapPlugins.mapL2S(StrapAlign.defaultClass(SequenceAligner3D.class));
        }
        if (c==_pS) {
            final int y2S=plotAreaH('S'), y2E=y2S+plotAreaH('E');
            if (0<y && y<y2S) sb=baTip().a(secStruP()).a("<br>").colorBar(0xff0000).a("Helices ").colorBar(0xb2b200).a("Sheets");
            else if (y2S<y && y<y2E) {
                sb=baTip().a("Sequence variability:");
                for(int i=-2; i<=2;i++) sb.a("<br>").colorBar(blueYellowRed((2+i)/4f).getRGB()).a(i==-2||i==2?" Highly ":"").a(i<0?" Conserved" : i>0?" Variable":" Medium");
            }
        }
        if (c==_pR) sb=baTip().a("Sequence positions of ").a(rulerP());
        if (c==_pC) {
            final ResidueSelection ss[]=_resSelMouse;
            int where=col+row*1024+modi;
            for(int i=0; i<sze(ss); i++) where+=ss[i].hashCode();
            if (_tip==null || _whereTip!=where) {
                _whereTip=where;
                sb=baTip();
                if (sze(ss)>0) {
                    for(int i=0; i<ss.length; i++) {
                        if (i>0) sb.a("<hr>");
                        sb.or(dTip(ss[i]), dItem(ss[i]), ss[i]);
                    }
                    sb.a1("<br>");
                    if (!shift) sb.a("<sub><font color=\"#FF0000\">Shift-key for details</font></sub>");
                    else sb.a("Handling:<ul>"+
                              "<li>Drag'n Drop: Residue selections can be dragged onto other proteins and 3D-structure views</li>"+
                              "<li>Right-click: The context menu provides many options.</li>"+
                              "<li>Double click: Edit annotations and associated 3D-rendering commands.</li>"+
                              "</ul>");
                    _tip=addHtmlTagsAsStrg(sb);
                } else if(ANSEW.indexOf(_whatMouse)>0 && getPrpty(StrapView.class,"popupShownR",null)==null) {
                    _tip=addHtmlTagsAsStrg(RC4CM.substring(4)+" of rectangular region");
                } else if (Math.abs(x-cb.width* getProtectFromColumn((char)0) )<EM) {
                    _tip=button(TOG_PROTECT_COL).getToolTipText();
                } else _tip=null;
            }
            return _tip;
        }
        return toStrg(sb);
    }
    /* <<< Tip <<< */
    /* ---------------------------------------- */
    public boolean paintHook(JComponent c, Graphics g, boolean after) {
        final Protein[] ppVis=visibleProteins(), ppSel=StrapAlign.selectedProteins();
        if (!after && c==_vSB && 0==sze(_hlRows)) _vSB.setHasSelection(ppVis.length>0 && ppSel.length>0);
        if (c==_pC &&  !after) return false;
        if (g==null || !after) return true;
        if (_rhProtsDrag==null) antiAliasing(g);
        final boolean debugPaint=false;
        _painted=true;
        final int W=wdth(c);
        if (c==_hSB) {
            if (after) ants(null, g);
            else _hSB.getTrack();
        } else  {
            final Rectangle clip=clipBnds(g, W, c.getHeight()), cb=charB();
            final int clipX1=x(clip),clipY1=y(clip), clipX2=x2(clip), clipY2=y2(clip);
            final int charW=cb.width, charA=y(cb), charH=hght(cb), lineH=charH+_lineSkip;
            if (_secStruShown!= (secStruP()!=null)) { _secStruShown=(secStruP()!=null); revalAndRepaintC(_pS); }
            if (c==_pS) {
                paintSecStru(g, clipX1/charW, clipX2/charW,false);
                paintEntropy(g, clipX1/charW, clipX2/charW,false);
            }

            if (c==_pR) paintRuler(g, clipX1/charW, clipX2/charW,false);
            if (c==_ri || c==_rh || c==_pC) {
                final int rowFrom=clipY1/lineH, rowTo=mini(ppVis.length, (clipY2-1)/lineH+1);
                if (c==_rh || c==_ri) {
                    final Font font=c.getFont();
                    g.setFont(font);
                    final int parentW=wdth(c.getParent());
                    if (c==_ri) {
                        g.setColor(C(0));
                        final int iInfo=_choiceI==null?0:_choiceI.i();
                        for(int iP=_rhDragProt2row==null ? rowFrom : 0; iP< (_rhDragProt2row==null ? rowTo : ppVis.length); iP++) {
                            final int row= sze(_rhDragProt2row)>iP ? _rhDragProt2row[iP]:iP;
                            if (row<rowFrom || row>=rowTo) continue;
                            final Protein p=ppVis[iP];
                            final String print=toStrg(greekToUnicode(p==null?null : p.getInfo(iInfo)));
                            if (sze(print)==0) continue;
                            final int x=(int) ((parentW-strgWidth(font, print))*_adjust[COLi]);
                            g.drawString(print,x,row*lineH+charA);
                        }
                    }
                    if (c==_rh) {
                        final boolean hlIdentical=StrapAlign.button(StrapAlign.TOG_HIGHLIGHT_IDENTICAL_SEQUENCES).s();
                        if (lineH==0) return true;
                        final int cursor=_cursorBright ? cursorRow() : -1;
                        barChart(g,ppVis,rowFrom,rowTo);
                        for(int iP=_rhDragProt2row==null ? rowFrom : 0; iP< (_rhDragProt2row==null ? rowTo : ppVis.length); iP++) {
                            final int row= sze(_rhDragProt2row)>iP ? _rhDragProt2row[iP]:iP;
                            if (row<rowFrom || row>=rowTo) continue;
                            final Protein p=ppVis[iP];
                            final String name=toStrg(p);
                            final Image im=p.getIconImage();
                            final int wIm=im==null?0:(im.getWidth(_rh)*charH/maxi(1,im.getHeight(_rh)));
                            final int countH=p.getHeteroCompounds('H').length, countN=p.getHeteroCompounds('N').length, wHetero=countN+countH==0?0:3;
                            final int nP=proteinsInRow(iP).length,  wN=nP==1 ? 0: stringSizeOfInt(nP)*charW;
                            final int labelSize=wN+wHetero+wIm+strgWidth(font,name);
                            final int y=row*lineH;
                            final Color highlight=get(iP, _hlRows, Color.class);
                            final boolean selected=_hlRows==null && cntains(p,ppSel); /* !! Performance */
                            final Color bg=
                                highlight!=null ? deref(orO(highlight, bgSelected()),Color.class) :
                                selected ? bgSelected() : iP==cursor ? C(0xFFffFF) : null;
                            if (bg!=null) { g.setColor(bg); fillBigRect(g, 0,y,3000, lineH);}
                            if (iP==cursor) { g.setColor(C(0xFFffFF)); g.drawRect(0,y,3000, lineH-1); }
                            int x=(int) ((parentW-labelSize)*_adjust[COLh])+wHetero;
                            if (nP>1) {
                                g.setColor(C(0x88));
                                g.drawString(toStrg(nP),x,y+charA+2);
                                x+=wN;
                            }
                            if (im!=null) {
                                g.drawImage(im,x, y, wIm, charH,_rh);
                                x+=wIm;
                            }
                            if (hlIdentical && iP>0 && Protein.equalsResidueType(ppVis[iP-1],p)) {
                                g.setColor(C(0xFF0600,66));
                                fillBigRect(g, 0,y-1,300,2);
                            }
                            p.paintChain(0L,g, name, x,y,charW,charH);
                            g.setColor(C(selected ? 0xFFffFF : 0));
                            g.drawString(p.getRendTxt(),x, y+charA);
                            if (wHetero>0) {
                                g.setColor(C(0)); fillBigRect(g, 0,y+1,wHetero,lineH-2);
                                if (countN>0) { g.setColor(C(0xFF00));   fillBigRect(g, 0,y+2,  wHetero-1, lineH/3); }
                                if (countH>0) { g.setColor(C(0xFF0000)); fillBigRect(g, 0,y+lineH/2+2,  wHetero-1, lineH/3);}
                            }
                            if (row==_rhMark) rhDrawMark(row,g);
                        }
                    } /* _rh */
                }
                if (c==_pC) {
                    if (!_anyPainted) {
                        debugTime("\n"+ANSI_GREEN+"StrapView: "+ANSI_RESET, TIME_AT_START);
                        if (BENCHMARKS['S']) debugExit("");
                        _anyPainted=true;
                        ChThread.callEvery(0, 155, thrdCR(instance(),"FLASH"),"FLASH");
                    }
                    mayRevalidate();

                    final int colLeft=clipX1/charW, colRight=(clipX2-1)/charW+2;
                    paintRegion(g, colLeft,colRight, rowFrom,  rowTo, 0);
                    plot(g);
                    if (_whatMouse=='H' || _whatDrag=='H') {
                        for(int row=rowFrom; row<rowTo; row++) {
                            if (lMarginResizable(row)) drawDivider(null,g,-x(_pC), row2y(row), EM*3/2, lineH);
                        }
                    }
                    ants(g,null);
                } /* c==_pC */
            }
            if (c==_vSB && ppVis.length>0) {
                if (lineH!=_vSB.getUnitIncrement(1)) _vSB.setUnitIncrement(lineH);
                final Rectangle track=_vSB.getTrack();
                final int v=-y(_pC);
                if (cursorProtein()!=null && _cursorBright) {
                    final Rectangle r=rectCursor(_vSB);
                    g.setColor(C(0xFFffFF));
                    g.fill3DRect(r.x,r.y, r.width,r.height, true);
                }
                for(int iP=ppVis.length; --iP>=0;) ppVis[iP].setRow(iP);
                if (v!=_scrollV) { _scrollV=v;  _hSB.repaint(_hSB.getThumb());  }
                g.setColor(bgSelected());
                final int thickness=mini(3,maxi(1,ppSel.length<5 ? 3 : track.height/ppVis.length-1));
                for(int iP=ppSel.length; _hlRows==null && --iP>=0;) {
                    final Protein p=ppSel[iP];
                    final int iRow=p==null?-1:p.getRow();
                    if (iRow<0 || iRow>=ppVis.length || ppVis[iRow]!=p) continue;
                    fillBigRect(g, 3, y(track) + track.height*iRow/ppVis.length, 99, thickness);
                }
                for(int iRow=sze(_hlRows); --iRow>=0;) {
                    final Color color=get(iRow,_hlRows, Color.class);
                    if (color==null) continue;
                    g.setColor(color);
                    fillBigRect(g, 3, y(track) + track.height*iRow/ppVis.length, 99, thickness);
                }
            }
        }
        return true;
    }
    private boolean lMarginResizable(int row) {
        final Rectangle r=rectRubberBand();
        final int col=x(visibleRowsAndCols());
        return !( (x(r)==col || x(r)==col+1) && row>=y(r) && row<y2(r))  &&
            residueSelectionsAt(row,col  , VisibleIn123.SEQUENCE).length==0 &&
            residueSelectionsAt(row,col+1, VisibleIn123.SEQUENCE).length==0;
    }
    /* <<< Tip <<< */
    /* ---------------------------------------- */
    /* >>> Rectangle >>> */
    private void getBoundsAndRepaint(ResidueSelection ss[], int row, Rectangle r) {
        final int N=sze(ss);
        final Protein p=proteinInRow(row);
        int x=0, y=0, w=0, h=0;
        if (p!=null && N>0) {
            int left=MAX_INT, right=MIN_INT;
            for(int i=N; --i>=0; ) {
                if (ss[i]==null || ss[i].getProtein()!=p) continue;
                final boolean bb[]=ss[i].getSelectedAminoacids();
                final int offset=ss[i].getSelectedAminoacidsOffset()-Protein.firstResIdx(p);
                left=mini(left,offset+fstTrue(bb));
                right=maxi(right,offset+lstTrue(bb));
            }
            x=col2x(p.getResidueColumnZ(maxi(0,left)));
            w=maxi(0,col2x(p.getResidueColumnZ(mini(p.countResidues()-1,right))+1)-x);
            y=row2y(row);
            h=row2y(1);
        }
        if (!rectEquals(x, y, w, h, r)) {
            ChDelay.runAfterMS(0, thread_repaint(_pC, r), 999);
            setRectAndRepaint(x, y, w, h, r, _pC);
        }
    }
    private void ants(Graphics gAwtAli, Graphics gAwtSB) {
        final Graphics2D gAli=(Graphics2D)gAwtAli, gSB=(Graphics2D)gAwtSB;
        if (gAli==null && gSB==null) return;
        final Stroke stroke0=gAli!=null ? gAli.getStroke() : gSB.getStroke();
        if (gAli!=null) gAli.translate(BUGFIX_OPENJDK_G2, BUGFIX_OPENJDK_G2);
        if (gSB!=null)  gSB.translate(BUGFIX_OPENJDK_G2, BUGFIX_OPENJDK_G2);
        final Rectangle rRowsCols=visibleRowsAndCols(), cb=charB();
        final int charW=wdth(cb), charH=hght(cb), lineH=charH+_lineSkip;
        final int cMin=x(rRowsCols), cMax=cMin+rRowsCols.width;
        final int rMin=y(rRowsCols), rMax=rMin+rRowsCols.height;
        final int nRows=countRows(), nCols=countColumns();
        if (nRows<=0||nCols<=0) return;
        final Stroke dashedStroke0=dashedStroke(0), dashedStroke1=dashedStroke(1);
        final Protein[] ppVis=visibleProteins();
        final ResidueSelection ss[]=StrapAlign.coSelected().residueSelections('s');
        Rectangle track=null;
        for(ResidueSelection s : ss) {
            final Protein p=sp(s);
            if (p==null || !cntains(s,_resSelMouse) && (!ResSelUtils.isSelVisible(VisibleIn123.SEQUENCE,s) || sze(_resSelMouse)>0)) continue;
            final int offset=ResSelUtils.selAminoOffsetZ(s);
            final boolean bb[]=s.getSelectedAminoacids();
            final int row=idxOf(p,ppVis);
            if (row<0 || bb==null) continue;
            final int gaps[]=p.getResidueGap();
            final int cols[]=p.getResidueColumn();
            final int nR=p.countResidues();
            final int style= isInstncOf(VisibleIn123.class,s) ? ((VisibleIn123)s).getStyle() : -1;
            final int yT=ResSelUtils.yT(style,charH);
            final int selHeight=ResSelUtils.height(style,charH);
            final int iB_to=mini(bb.length, nR-offset);
            final Color color=s instanceof VisibleIn123 ? ((VisibleIn123)s).getColor() : C(0xFFffFF), complColor=C(color==null?0:  0xFFffFF&(~color.getRGB()));
            for(int iB=maxi(0,-offset); iB<iB_to; iB++) {
                if (!bb[iB]) continue;
                int j=iB+1;
                while(j<iB_to && bb[j] && gaps[j+offset]==0) j++;
                final int colFrom0=cols[iB+offset], colTo0=cols[j-1 +offset]+1;
                iB=j-1;
                for(boolean isSB : FALSE_TRUE) {
                    if (!isSB) {
                        if (cMax<colFrom0 || cMin>colTo0) continue;
                        if (rMax<row+1 || rMin>row) continue;
                    }
                    final int colFrom=isSB ? colFrom0 : maxi(cMin-1,colFrom0), colTo=isSB ? colTo0 : mini(cMax+1,colTo0);
                    final Graphics g= isSB ? gSB : gAli;
                    if (g==null) continue;
                    final int x1, x2, y1, y2;
                    if (isSB) {
                        if (track==null) track=_hSB.getTrack();
                        x1=x(track)+colFrom*track.width/nCols;
                        x2=x(track)+colTo  *track.width/nCols-1;
                        y1=y(track)+row    *track.height/nRows;
                        y2=y(track)+(row+1)*track.height/nRows-1;
                    } else { x1=colFrom*charW; x2=colTo*charW-2; y1=row*lineH+yT; y2=y1+selHeight; }
                    for(boolean isDashed:FALSE_TRUE) {
                        g.setColor(isDashed ? complColor : color);
                        ((Graphics2D)g).setStroke(!isDashed ? stroke0 : isSB || y2-y1<3 ? dashedStroke1 : dashedStroke0);
                        if (y2-y1<3) {
                            if (isDashed) for(int y=y1; y<=y2; y++) g.drawLine(x1,y,x2,y);
                            else fillBigRect(g, x1,y1,x2-x1,y2-y1);
                        } else g.drawRect(x1,y1,x2-x1, y2-y1);
                    }
                }
            }
        }
        final Rectangle r=rectRubberBand();
        final int rw=x(r), rn=y(r), re=x2(r), rs=y2(r);
        if (rw<re && rn<rs) {
            if (track==null) track=_hSB.getTrack();
            for(boolean isSB : FALSE_TRUE) {
                final Graphics2D g= isSB ? gSB : gAli;
                if (g==null) continue;
                final int x1, x2, y1, y2;
                if (isSB) {
                    if (track==null) continue;
                    x1=x(track)+rw*track.width/nCols;
                    x2=x(track)+re*track.width/nCols-1;
                    y1=y(track)+rn*track.height/nRows+1;
                    y2=y(track)+rs*track.height/nRows;
                } else  { x1=rw*charW; x2=re*charW-2; y1=rn*lineH; y2=rs*lineH-2; }
                g.setColor(C(0xFF0000));
                g.setStroke(stroke0);
                g.drawRect(x1-1,y1-1,x2-x1+2,y2-y1+2);
                for(boolean isDashed:FALSE_TRUE) {
                    g.setColor( C(isDashed ? 0xFF0000 : 0xFFffFF));
                    g.setStroke(!isDashed ?  stroke0 : isSB  ? dashedStroke1 : dashedStroke0);
                    g.drawRect(x1,y1,x2-x1, y2-y1);
                }
            }
            for(int i=sze(_vAnts); --i>=0;) {
                final JComponent jc=get(i,_vAnts,JComponent.class);
                final Graphics2D g=isVisbl(jc) ? (Graphics2D)jc.getGraphics() : null;
                if (g==null) continue;
                if (!isEnabld(jc)) setEnabld(true,jc);
                for(boolean isDashed:FALSE_TRUE) {
                    g.setColor( C(isDashed ? 0xFF0000 : 0xFFffFF));
                    g.setStroke(!isDashed ?  stroke0 :  dashedStroke0);
                    g.drawRect(0,0, jc.getWidth()-1,jc.getHeight()-1);
                }
            }
        }
        drawProtectAlignment( stroke0, dashedStroke1, gAli,  gSB, 0,MAX_INT);
        if (gAli!=null) { gAli.setStroke(stroke0); gAli.translate(-BUGFIX_OPENJDK_G2,-BUGFIX_OPENJDK_G2); }
        if (gSB!=null)  { gSB.setStroke(stroke0); gSB.translate(-BUGFIX_OPENJDK_G2,-BUGFIX_OPENJDK_G2); }
    }
    int getProtectFromColumn(char WorN) {
        if (!button(TOG_PROTECT_COL).s()) return -1;
        final Gaps2Columns g2c=StrapAlign.g2c();
        if (g2c==null) return -1;
        final char w_or_n=WorN!=0 ? WorN :  Protein.isWide() ? 'W' : 'N';
        return
            w_or_n=='W' ? _protectFromCol :
            get(_protectFromCol, g2c.wide2narrowColumn());
    }
    private void drawProtectAlignment(Stroke stroke0, Stroke dashedStroke, Graphics2D gAli, Graphics2D gSB, int colFrom, int colTo) {
        final int colProtect=getProtectFromColumn((char)0),  nCols=countColumns();
        if (colProtect<=0 || colProtect<colFrom || colProtect>colTo || nCols==0) return;
        for(boolean isSB : FALSE_TRUE) {
            final Graphics2D g= isSB ? gSB : gAli;
            if (g==null) continue;
            final int x, y1,y2;
            if (isSB) {
                final Rectangle track=_hSB.getTrack();
                if (track==null) continue;
                x=x(track)+colProtect*track.width/nCols;
                y1=0;
                y2=_hSB.getHeight();
            } else {
                final Rectangle rect= visibleXYWH();
                x=col2x(colProtect);
                if ((x<x(rect)-2 || x>2+x2(rect))) continue;
                y1=y(rect);
                y2=y1+rect.width;
            }
            if (stroke0!=null) g.setStroke(stroke0);
            g.setColor(C(0xFFffFF));
            fillBigRect(g, x-1, 0, 3, Short.MAX_VALUE);
            g.setColor(C(0));
            if (dashedStroke!=null) g.setStroke(dashedStroke);
            g.drawLine(x,y1, x,y2);
        }
    }
    static Protein[] ppInRectangle(StrapView view) {
        final StrapView v=view!=null?view:StrapAlign.alignmentPanel();
        return v==null?Protein.NONE : ppInRectangle(v.rectRubberBand(), v);
    }
    static Protein[] ppInRectangle(Rectangle rect, StrapView view0) {
        final StrapView v=view0!=null ? view0 : StrapAlign.alignmentPanel();
        final Rectangle r=v==null?null:rect;
        final int x=x(r), y=y(r), h=y2(r)-y;
        final Protein pp[]=x2(r)<=x(r) || h<=0 ? Protein.NONE : new Protein[h];
        for(int i=pp.length;  --i>=0;) {
            final Protein p=v.proteinInRow(y+i);
            if (p.getMaxColumnZ()>=x) pp[i]=p;
        }
        return rmNullA(pp,Protein.class);
    }
    static Protein[] ppInRectangleForAlignment(int minNum) {
        final StrapView view=StrapAlign.alignmentPanel();
        if (view==null) return Protein.NONE;
        Protein pp[]=StrapAlign.selectedProteinsInVisibleOrder();
        final Protein ppRegion[]=ppInRectangle(view);
        if (ppRegion.length>=0) {
            final List<Protein> vP=new ArrayList();
            for(Protein p:pp) if (idxOf(p,ppRegion)>=0) adUniq(p, vP);
            pp=sze(vP)>=minNum ? spp(vP) : ppRegion.length>0 ? ppRegion : pp;
        }
        if (pp.length<minNum) pp=view.visibleProteins();
        return pp;
    }
    static String proteinsInRectangleToText(StrapView view) {
        final BA sb=new BA(999);
        final Rectangle rect=view==null?null:view.rectRubberBand();
        final int rectW=x(rect), rectE=x2(rect);
        for(Protein p : ppInRectangle(view)) {
            if (p==null) continue;
            final int idx0=Protein.firstResIdx(p);
            sb.a(delLstCmpnt(p.getName(),'!')).a('/').a(idx0+p.column2nextIndexZ(rectW)+1).a('-').a(idx0+p.column2thisOrPreviousIndex(rectE-1)+1).a('\n');
        }
        return toStrg(sb);
    }
    /* <<< Region <<< */
    /* ---------------------------------------- */
    /* >>> paint >>> */
    public static JComponent alignmentPane(StrapView v){ return v==null?null : v._pC; }
    public JComponent alignmentPane() { return _pC;}
    public HScrollBar hsb() { return _hSB;}
    static boolean anyPainted() { return _anyPainted;}
    boolean isBusy() {
        return
            ChDelay.contains(this) || ChDelay.contains(_hSB) ||
            ChDelay.contains(thread(T_CLEAR_SB)) || ChDelay.contains(threadPR(PR_ONLY_CHANGES));
    }
    private static void markDirtyRect(boolean fill, short ccc[][], int col,int row, int width, int height) {
        if (ccc==null) return;
        final int fromR=maxi(0,row);
        final int toR=mini(sze(ccc), row+height);
        for(int r=fromR; r<toR; r++) {
            final short cc[]=ccc[r];
            final int fromC=maxi(0,col);
            final int toC=mini(sze(cc), col+width);
            for(int c=fromC; c<toC; c++) {
                if (!fill && !(c==fromC || r==toC-1 || r==fromR || r==toR-1)) continue;
                cc[c]=-1;
            }
        }
    }
    private Font _font;

    public void paintRegion(Graphics gAwt, int colLeft,  int colRight, int rowTop,  int rowBot, int options) {
        final Object cached[]=cached();
        final String strgLen1[]=strgsOfLen1();
        mayRevalidate();
        final boolean brighter=button(TOG_BRIGHTER).s();
        final Graphics2D g=(Graphics2D)gAwt;
        int countPainted=0, countAll=0;
        final Rectangle cb=charB();
        final Color secStr2color[]=ShadingAA.colors(ShadingAA.SECSTRU,false),
            actg2color[]=ShadingAA.colors(ShadingAA.ACTG,false),
            secStr2color_LessConserved[]=brighter ? darkerColors(secStr2color) : darkerColors(darkerColors(secStr2color)),
            actg2color_LessConserved[]=brighter ? darkerColors(actg2color) : darkerColors(darkerColors(actg2color)),
            fg=C(0xFFffFF),
            fgLessConserved=C( brighter ? 0xc0c0c0 : 0x808080),
            black=C(0), defaultBg=C(DEFAULT_BACKGROUND);
        final boolean debugPaint=false;
        final JComponent panel=panel();
        {
            final Font f=panel.getFont();
            g.setFont(f);
            if (_charWidths==null || _font!=f) _charWidths=panel.getFontMetrics(_font=f).getWidths();
        }
        if (_rhProtsDrag==null) antiAliasing(g);
        final Protein[] ppVis=visibleProteinsReordered();
        SPUtils.updateCountResidues();
        final short oldCharacters[][];
        {
            short[][] old=(short[][])cached[MCSV_CHARACTERS];
            if (sze(old)<ppVis.length) cached[MCSV_CHARACTERS]=old=new short[ppVis.length][];
            oldCharacters=old;
        }
        final int charW4=cb.width/4, charW8=maxi(charW4/8,1), lineH=cb.height+_lineSkip;
        final short[][] frequencies= frequenciesAtColumn(colLeft,colRight,'C');
        final int threshold=sliderSimilarity().getValue();
        final Color colors[]=ShadingAA.colors(false);
        final int iShading=ShadingAA.i();
        for(int row=maxi(rowTop,0); row<=rowBot && row<ppVis.length; row++) {
            final int y=row*lineH;
            final Protein p=ppVis[row];
            if (p==null) continue;
            final boolean isShading=frequencies!=null && _ppShading==ppVis || cntains(p,_ppShading);
            final boolean isACTG=iShading==ShadingAA.ACTG && p.getNucleotides()!=null;
            final boolean noArrowHeads=sp(_noArrowHeads)==p;
            final ResidueSelection[] residueSelections=p.allResidueSelections();
            final byte selectedAA[]=p.selAminos();
            final byte seq[]=p.getGappedSequence(),secStr[]=p.getGappedSecStru();
            if (sze(seq)==0) continue;
            final int maxCol=p.getMaxColumnZ();
            final int col2idx[]=p.columns2indices();
            /* ---  gaps black --- */
            short[] ocr=oldCharacters[row];
            if (sze(ocr)<seq.length) ocr=oldCharacters[row]=chSze(ocr,seq.length+99);
            final short[] oldChRow=ocr;
            final boolean shadeSecStru=iShading==ShadingAA.SECSTRU;
            final float access[]=iShading!=ShadingAA.SOLVENT_ACC?null:p.getResidueSolventAccessibility();
            final Color colorSolvAcc[]=access==null?null:ShadingAA.colors(ShadingAA.SOLVENT_ACC,false);
            for(int col=colLeft, x=colLeft*cb.width; col<colRight && col<=maxCol; col++,x+=cb.width) {
                if (col<0 || col2idx==null||col>=col2idx.length) continue;
                final int ia=col2idx[col];
                final int hasSelection= noArrowHeads ? 1 : ia>=0 && ia<selectedAA.length ? selectedAA[ia] : 0;
                final int triplet8;
                final byte triplet[]=isACTG ? p.getResidueTripletZ(ia,TRIPLET) : null;
                final short chr=seq[col];
                final byte chr7F=(byte)(chr&0x7F);
                final boolean conserved;
                final short newCh;
                if (ia>=0) {
                    int t8=0;
                    if (triplet!=null) {
                        final int
                            t0=triplet[0]|32,
                            t1=triplet[1]|32,
                            t2=triplet[2]|32;
                        t8=
                            t0=='a'  ? 0+0 : t0=='c' ? 1+0 : t0=='g' ? 2+0 : 3+0 +
                            t1=='a'  ? 0+4 : t1=='c' ? 1+4 : t1=='g' ? 2+4 : 3+4 +
                            t2=='a'  ? 0+8 : t2=='c' ? 1+8 : t2=='g' ? 2+8 : 3+8;
                    }
                    short nc=chr;
                    boolean isCons=false;
                    if (isShading && frequencies!=null && frequencies.length>col) {

                        final int c=(chr7F|32)-'a';
                        final short ff[]=frequencies[col];

                        if (ff!=null && c>=0 && c<='z'-'a') {
                            final int total=ff[FrequenciesAA.TOTAL], t100=threshold*total/100;

                            isCons= threshold>=0 ? ff[c]>=t100 : ff[c]<=total+t100;
                        }
                    }
                    if (isCons) { nc|=0x80; t8|=0x80; }
                    nc|= (hasSelection<<8);
                    t8|= (hasSelection<<8);
                    newCh=nc;
                    triplet8=t8;
                    conserved=isCons;
                } else { triplet8=0;newCh=0;conserved=false;}
                final boolean isSame= oldChRow[col] == (triplet==null ? newCh : triplet8);
                if (!isSame) oldChRow[col]= triplet==null ? newCh : (short)triplet8;
                countAll++;
                final boolean needsPaint=
                    !isSame ||
                    (0!=(options&PR_ALL_SELECTED) ? hasSelection!=0 :  0!=(options&PR_ONLY_CHANGES) ? hasSelection==Byte.MAX_VALUE :
                     true);
                if (needsPaint) {
                    g.setColor(chr==' ' || chr==0 ? defaultBg : black);
                    fillBigRect(g, x,y,cb.width,cb.height+_lineSkip);
                    final Color newColor;
                    {
                        final Color nc;
                        if (shadeSecStru && col<sze(secStr)) nc=( conserved ? secStr2color : secStr2color_LessConserved)[secStr[col]];
                        else
                            if (access!=null && ia>=0 && ia<access.length) nc=colorSolvAcc[mini((int)access[ia],colorSolvAcc.length-1)];
                            else nc=conserved ? colors[chr] : fgLessConserved;
                        newColor=nc!=black ? nc : fg;
                    }
                    if (chr!=0 && chr!='-')  {
                        cb.x=(cb.width-_charWidths[chr7F])/2;
                        if (hasSelection!=0) {
                            setRect(x,y,cb.width, lineH, RECT_EDT);
                            ResSelUtils.drawResidueSelection(Protein.AMINO_ACIDS,triplet==null ? chr7F : 0,p,residueSelections,ia,g, RECT_EDT,cb);
                        }
                        if (triplet==null) {
                            g.setColor(newColor);
                            g.drawString(strgLen1[chr7F&127], x(cb)+x,y+y(cb));
                            countPainted++;
                        } else {
                            for(int i=0,xNt=x+charW8;i<3;i++) {
                                final int nt=triplet[i]|32, h=nt=='c' || nt=='g' ? cb.height : cb.height/2;
                                final Color color=(conserved ? actg2color : actg2color_LessConserved)[nt];
                                g.setColor(color!=black ? color : fg);
                                fillBigRect(g, xNt,y+1,charW4-1,h-2);
                                xNt+=charW4;
                            }
                        }
                    }
                }
            }
            /* --- Clear after the sequence --- */
            if (colRight>maxCol) {
                g.setColor(defaultBg);
                fillBigRect(g, (maxCol+1)*cb.width,y,(colRight-maxCol)*cb.width, cb.height+_lineSkip);
                final int to=mini(colRight, ocr.length);
                if (maxCol+1<to) Arrays.fill(ocr,maxCol+1,to,(short)0);
            }
        }
        //putln("paintRegion "+options+"   countPainted="+countPainted+" countAll="+countAll+"   "+(options==PR_ALL_SELECTED));
        //final int duration=(int)(System.currentTimeMillis()-time);
    }
    /* <<< paint <<< */
    /* ---------------------------------------- */
    /* >>> HL >>> */
    private Color _hlRows[], _colorHlp;
    private void highlightRows(Color cc0[]) {
        final Color cc[]=countNotNull(cc0)==0?null:cc0;
        if ((cc==null)!=(_hlRows==null) || !arraysEqul(_hlRows, cc)) {
            _hlRows=cc;
            repaintC(_vSB);
            repaintC(_rh);
        }
    }
    void highlightProteins(Object ss[], Color color) {
        Color cc[]=null;
        final int n=countRows();
        for(int i=sze(ss); --i>=0;) {
            final int iP=findRow(sp(ss[i]));
            if (iP>=0 && iP<n) {
                if (cc==null) cc=new Color[n];
                cc[iP]=(Color)orO(colr(ss[i]), color);
            }
        }

        highlightRows(cc);
        if (COLOR_HL==(_colorHlp=color)) inEDTms(_runClearHL,3333);
    }
    private final Runnable _runClearHL=thrdCR(this,"CLRHL");
    /* <<< HL <<< */
    /* ---------------------------------------- */
    /* >>> Scroll >>> */
    private Object _butSB=cbox(false,"Overview within the scroll-bar  ",null, this);
    void strapSB() {
        if (_butSB!=null) {
            setPrefSze(false, 1, 3*EX, _hSB);
            for(int i=3; --i>=0;) {
                getSP(i==0?_pR:i==1?_pS : _pC).setHorizontalScrollBar(_hSB);
            }
            _panSB.removeAll();
            pnl(_panSB,CNSEW, _hSB);
            _butSB=null;
            revalAndRepaintC(_hSB);
            revalAndRepaintC(_pS);
            revalAndRepaintC(_pR);
        }
    }

    boolean isShadingScrollbar() { return _shadingSB;}
    void scrollRel(char v_or_h, int i,boolean charsOrPixels) {
        final Rectangle r=visibleXYWH(), cb=charB();
        if (v_or_h=='h') r.x+=  i* (charsOrPixels?cb.width:1);
        else {
            r.y+=i* (charsOrPixels?(cb.height+_lineSkip):1);
            ChScrollBar.lock(0, _vSB);
        }
        scrRectToVis(r,false);
    }
    void scrollCursorToVisible() {
        if (_pC==null) return;
        final int w=charB().width, h=charB().height+_lineSkip;
        final Rectangle RECT_EDT=new Rectangle();
        setRect(cursorColumn()*w, cursorRow()*h,w,h, RECT_EDT);
        _zoomCenter=null;
        _pC.scrollRectToVisible(RECT_EDT);
    }
    void scrollToVisible(int x0, int y0) {
        if (_pC==null) return;
        final int x=x0>=0 ? x0 : 1-x(_pC);
        final int y=y0>=0 ? y0 : 1-y(_pC);
        setRect(mini(x, _pC.getWidth()-EX),mini(y, _pC.getHeight()-EX), EX,EX, RECT_EDT);
        scrRectToVis(RECT_EDT,false);
    }
    void scrRectToVis(Rectangle rect, boolean center) { scrRectToVis(rect,center,1); }
    private void scrRectToVis(Rectangle rect, boolean center, int tryAgain) {
        pcp(KOPT_DO_NOT_INVALIDATE, "", StrapAlign.dialogPanel());
        if (!isEDT()) inEdtLaterCR(this,"scrRectToVis",new Object[]{rect, center ? "":null, intObjct(tryAgain), null});
        else {
            final int midx=x(rect)+rect.width/2;
            final int midy=y(rect)+rect.height/2;
            if (center) {
                final int h=_tLayout.getHeight();
                final int w=_tLayout.columnWidth(COLs);
                _pC.scrollRectToVisible(new Rectangle( midx-w/2, midy-h/2,  w,h));
            } else _pC.scrollRectToVisible(rect);
            if (tryAgain>0 && !scrllpn(_pC).getViewport().getViewRect().contains(midx,midy)) {
                startThrd(thrdCR(this,"scrRectToVis",new Object[]{rect,"", intObjct(tryAgain-1), intObjct(555)}));
            }
        }
        pcp(KOPT_DO_NOT_INVALIDATE, null, StrapAlign.dialogPanel());
    }
    public void scrRectToRowCol(boolean fill, Rectangle r) {
        if (r==null) return;
        final int
            col1=x(r), col2=x2(r),
            x1=col1<col2?col2x(col1) : x(visibleXYWH()),
            y1=row2y(y(r)),
            x2=col1<col2?col2x(col2) : x2(visibleXYWH()),
            y2=row2y(y2(r));
        final Component pane=alignmentPane();
        pane.invalidate();
        scrRectToVis(new Rectangle(x1,y1,x2-x1-EX, y2-y1+2*EX),true);
        if (fill) try {
                final Graphics g=pane.getGraphics();
                g.setColor(C(0xffffff,99));
                g.fill3DRect(x1,y1, x2-x1, y2-y1, true);
                //g.drawRect(x1,y1, x2-x1, y2-y1);
                _repaintOnMouse=true;
            } catch(Exception e){}
    }
    /* <<< Scroll <<< */
    /* ---------------------------------------- */
    /* >>> Order Of proteins >>> */
    void moveLines(int von, int nach, int n) {
        int newCursorLine=cursorRow();
        final int nRows=countRows(), row=newCursorLine;
        if (von+n>nRows || nach+n>nRows) return;
        final int[] newIndices=moveIndices(nRows,von,nach,n);
        if (newIndices==null) return;
        final Protein[] newRows[]=new Protein[nRows][];
        for(int i=0;i<nRows;i++) {
            final int j=newIndices[i];
            if (j<0) return;
            newRows[i]=proteinsInRow(j);
            if (j==row) newCursorLine=i;
        }
        list().clear();
        adAll(newRows,list());
        setCursor(0,cursorProtein(), cursorAminoAcid());
        ChDelay.runAfterMS(EDT|PAUSE,threadPR(PR_ONLY_CHANGES), threadPR(PR_ONLY_CHANGES).LAST[DURATION]*4);
        final long durationRH[]=((ChPanel)_rh).getWhenPainted();
        ChDelay.repaint(_rh,4*durationRH[DURATION],durationRH);
        ChDelay.repaint(_ri,4*durationRH[DURATION],durationRH);
        repaintC(_hSB.clearImage());
        StrapEvent.dispatchLater(StrapEvent.ORDER_OF_PROTEINS_CHANGED,111);
    }
    private static int[] moveIndices(int max, int von, int nach, int n) {
        int[] r=_rhIndices;
        if (r==null || r.length!=max) r=_rhIndices=new int[max];
        int i,j=0;
        if (max<2 || (von+n)>max ||  nach>=max || von<0 || nach<0) return null;
        for(i=0;i<max;i++) r[i]=i;
        if (von<nach) {
            if (nach+n>max) return r;
            for(i=0;i<von;i++) r[j++]=i;
            for(i=von+n;i<n+nach;i++) r[j++]=i;
            for(i=von;i<von+n;i++) r[j++]=i;
            for(i=nach+n;i<max;i++) r[j++]=i;
        }
        if (nach<von) {
            if (von+n>max) return r;
            for(i=0;i<nach;i++) r[j++]=i;
            for(i=von;i<von+n;i++) r[j++]=i;
            for(i=nach;i<von;i++) r[j++]=i;
            for(i=von+n;i<nach;i++) r[j++]=i;
        }
        return r;
    }
    /* <<< Order Of proteins <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    @Override public void handleEvent(StrapEvent ev) {
        if (_disposed) return;

        final int t=ev.getType();
        boolean doEnable=false, clrHSB=false;
        if (t==StrapEvent.PROTEIN_INFO_CHANGED) { repaintC(_ri); repaintC(_rh); }
        if (t==StrapEvent.PROTEINS_HIDDEN || t==StrapEvent.PROTEINS_KILLED) {
            final int nRowsOld=countRows();
            if (idxOf(cursorProtein(),visibleProteins())<0) setCursor(0,null,0);
            updateIsInAli();
            clrHSB=doEnable=true;
            final int nRows=countRows();
            if (nRowsOld>nRows) reduceHeight(maxi(0, _reduceHeight- row2y(nRowsOld-nRows)));
            if (nRows<5) reduceHeight(0);
        } else if (t==StrapEvent.PROTEINS_ADDED || t==StrapEvent.PROTEINS_SHOWN) {

            final Object oo[]=ev.parameters();
            final int row=get(1,oo)!=null ? atoi(get(1,oo)) : -1;
            final Protein pp[]=get(0,oo, Protein[].class);
            if (sze(pp)>0) addProteins(row, pp);
            updateIsInAli();
            setCursor(0, cursorProtein(),cursorAminoAcid());
            clrHSB=doEnable=true;
        } else if (t==StrapEvent.NEED_SCROLL_DOWN_ALIGNMENT) scrRectToVis(new Rectangle(0,row2y(countRows())-2, 1, 1),false,4);
        int paintRegion=0;
        if (panel()!=null && _toolbar!=null && (_painted || (System.currentTimeMillis()-TIME_AT_START)>9999)) {
            if ( (t&StrapEvent.FLAG_ALIGNMENT_CHANGED)!=0) {
                clrHSB=true;
                inEDTms(threadPR(PR_ONLY_CHANGES), threadPR(PR_ONLY_CHANGES).LAST[DURATION]/2);
            }
            if (t==StrapEvent.AA_SHADING_CHANGED) {
                repaintC(_pC);
                if (!button(TOG_OUTLINE_HSB).s()) clrHSB=true;
            }
            if (t==StrapEvent.ORDER_OF_PROTEINS_CHANGED || t==StrapEvent.PROTEINS_KILLED || t==StrapEvent.PROTEINS_HIDDEN || t==StrapEvent.PROTEINS_SHOWN || t==StrapEvent.PROTEINS_ADDED) {
                _ppR=null;
                repaintC(_vSB);
            }
            if ((t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN || t==StrapEvent.CURSOR_CHANGED_PROTEIN) && StrapAlign.alignmentPanel()==this) {
                threadPR(PR_ONLY_CHANGES).run();
                doEnable=true;
            }
            if (t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR || t==StrapEvent.RESIDUE_SELECTION_CHANGED) threadPR(PR_ALL_SELECTED).run();
            if (t==StrapEvent.RESIDUE_SELECTION_CHANGED || t==StrapEvent.RESIDUE_SELECTION_DELETED || t==StrapEvent.RESIDUE_SELECTION_ADDED) {
                if (ev.getSource()!=Protein.class) button(TOG_OUTLINE_HSB).s(_shadingSB=false);
                clrHSB=true;
                paintRegion=PR_ONLY_CHANGES;
            }
            if (t==StrapEvent.ATOM_COORDINATES_CHANGED || t==StrapEvent.RESIDUE_SELECTION_DELETED) {
                repaintC(_pC);
                inEDTms(thread(T_ENABLED),555);
            }
            final boolean chColor=t==StrapEvent.VALUE_OF_ALIGN_POSITION_CHANGED_COLOR || t==StrapEvent.VALUE_OF_RESIDUE_CHANGED_COLOR;
            if (chColor || t==StrapEvent.VALUE_OF_ALIGN_POSITION_CHANGED || t==StrapEvent.VALUE_OF_RESIDUE_CHANGED) {
                final ChButton tog=button(TOG_OUTLINE_HSB);
                if (tog.s()) clrHSB=true;
                tog.s(_shadingSB=false);
                if (chColor) plot(_pC.getGraphics()); else repaintC(_pC);
                repaintC(_hSB);
            }
            if (t==StrapEvent.OBJECTS_SELECTED) {
                doEnable=true;
                final Graphics g=_pC.getGraphics();
                final long hc=hcSelectedResSel(false), hcUL=hcSelectedResSel(true);
                if (_hcSelResSelU!=hcUL) repaintC(_pC);
                else if (g!=null && _hcSelResSel!=hc) {
                    final Rectangle v=visibleRowsAndCols();
                    paintRegion(g, x(v), x2(v),  y(v), y2(v),  PR_ALL_SELECTED);
                    ants(g, null);
                } else threadPR(PR_ONLY_CHANGES).run();
                _hcSelResSelU=hcUL;
                _hcSelResSel=hc;
                repaintC(_vSB);  repaintC(_hSB);
            }
            if ( (t& (StrapEvent.FLAG_ROW_HEADER_CHANGED|StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED))!=0) {
                repaintC(_rh);
                repaintC(_ri);
            }
            if (t==StrapEvent.VALUE_OF_PROTEIN_CHANGED || t==StrapEvent.PROTEIN_PROTEIN_DISTANCE_CHANGED) {
                if (sze(getPlottersV())>0 || _vopClass!=null) {
                    final long durationRH[]=((ChPanel)_rh).getWhenPainted();
                    ChDelay.repaint(_rh,4*durationRH[DURATION], durationRH);
                }
            }
        }
        if (doEnable) inEDTms(thread(T_ENABLED),333);
        if (clrHSB) {
            final long duration=_hSB.LAST[DURATION]*5+100;
            ChDelay.runAfterMS(duration<100 ? EDT|PAUSE : EDT, thread(T_CLEAR_SB),duration);
        }
        if (paintRegion>0) ChDelay.runAfterMS(EDT|PAUSE,threadPR(paintRegion),threadPR(paintRegion).LAST[DURATION]*4);
    }
    private long _hcSelResSel,_hcSelResSelU;
    private long hcSelectedResSel(boolean underlinings) {
        final ResidueSelection ss[]=StrapAlign.coSelected().residueSelections('s');
        final int maxUnderline=ResSelUtils.maxUnderlining();
        long mc=0;
        for(ResidueSelection s : ss) {
            final Protein p=s.getProtein();
            if (p==null) continue;
            if (underlinings) {
                if (!ResSelUtils.styleUL(s)) continue;
                final boolean sel[]=s.getSelectedAminoacids();
                if (lstTrue(sel)-fstTrue(sel)<=maxUnderline) continue;
            }
            mc+=s.hashCode()+((long)p.hashCode()<<32);
        }
        return mc;
    }
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final JComponent qjc=deref(q,JComponent.class);
        final List vSel=StrapAlign.selectedObjectsV();
        final int
            vSelMC=modic(vSel),
            id=ev.getID(), kcode=keyCode(ev), modi=modifrs(ev), x=x(ev), y=y(ev),
            nRows=countRows(), iButton=buttn(ev);
        final boolean
            isPop=isPopupTrggr(false,ev), isPopT=isPopupTrggr(true,ev),
            ctrl=isCtrl(ev), shift=isShift(ev), alt=(modi&ALT_MASK)!=0, shortCut=isShrtCut(ev),
            toggleSel=isTogglSelect(ev),
            dragHori, dragVert,
            mouseButton= 0!=(modi&(BUTTON1_MASK|BUTTON2_MASK|BUTTON3_MASK));
        if ( (id==ComponentEvent.COMPONENT_RESIZED || id==COMPONENT_SHOWN) && (q==_pC || q==getSP(_pC)) && _tLayout!=null) {
            final boolean hide=prefW(_pC)< _tLayout.columnWidth(2)-EX;
            if (_hideSB!=hide) {
                _panSB.setPreferredSize((_hideSB=hide)?dim(0,0):null);
                revalAndRepaintC(_panSB);
            }
        }
        if (id==MOUSE_DRAGGED && _xDragFrom!=MIN_INT) {
            final int dx=Math.abs(x-_xDragFrom), dy=Math.abs(y-_yDragFrom);
            dragHori=dx>dy+1;
            dragVert=dx<dy-10;
        } else dragVert=dragHori=false;
        if (isPop && q==_button[BUT_ALIGN]) {
            JPopupMenu pop=gcp("P", q, JPopupMenu.class);
            if (pop==null) {
                final Object[] items={ button(BUT_ALIGN_CTRL), button(BUT_ALIGN_UNDO),
                                       StrapAlign.menu(SequenceAligner3D.class),
                                       StrapAlign.menu(SequenceAlignerTakesProfile.class),
                                       buttn(TOG_CACHE) };
                pcp("P",pop=jPopupMenu(0, "But align", items),q);
            }
            if (isPopT) shwPopupMenu(pop);
        }
        if (_tLayout==null) return; /* Above it may be instance(). But up from here it is a real instance */
        if (q==_pC && id==KEY_PRESSED) _whenKeyPress=System.currentTimeMillis();
        final BasicResidueSelection moSel=_resSel;
        final Rectangle cb=charB();
        final int
            lineH=cb.height+_lineSkip, charW=cb.width,
            mCol=q!=_pC?-1: x/charW,
            mRow=q!=_pC && q!=_rh?-1: y/lineH;
        final Protein pRow=proteinInRow(mini(nRows-1,mRow));
        if (isPopT) {
            if (q==_hSB) shwPopupMenu(getPopup(false));
            if (q==_pC) {
                final ResidueSelection[] ss=ResSelUtils.plusSelected(false, _resSelMouse);
                if (ss.length>0) ResidueSelectionPopup.showContextMenu(ss);
                else {
                    setPrpty(StrapView.class,"popupShownR","");
                    shwPopupMenu(getPopup(ANSEW.indexOf(_whatMouse)>0));
                }
            }
            if (q==_rh || q==_ri) {
                setPrpty(StrapView.class,"popupShownP","");
                StrapAlign.showContextMenu('P',oo(pRow));
            }
        }
        if (isPop) return;
        final ChTableLayout tl=_tLayout;
        final JComponent header=tl.th();
        if (q==_rh && id==KEY_PRESSED) {
            if (!ctrl&&!alt&& _ks.group(keyChar(ev))) return;
            final Protein[] ppSel=StrapAlign.selectedProteins();
            boolean done=false;
            if ((kcode==VK_BACK_SPACE || kcode==VK_DELETE) && ChMsg.yesNo(plrl(ppSel.length, "Remove %N protein%S?"))) StrapAlign.rmProteins(false,ppSel);
            if ( (ctrl||shortCut)  && !shift && !alt) {
                if (done=kcode=='A') adAll(pp(), vSel);
                else if (done=kcode=='C') {
                    _ppClipboard=ppSel.clone();
                    toClipbd(new BA(999).join(ppSel," "));
                } else if (done=kcode=='X') {
                    _ppClipboard=ppSel.clone();
                    rmAll(ppSel,vSel);
                    StrapAlign.rmProteins(false, ppSel);
                    StrapEvent.dispatch(StrapEvent.PROTEINS_KILLED);
                }
                else if (done=kcode=='V') StrapAlign.addProteins(_rhRow, _ppClipboard);
                else if (done=kcode=='F') StrapAlign.tree().selectDialog();
            }
            if (!done && (kcode==VK_DOWN || kcode==VK_UP || kcode==VK_PAGE_UP || kcode==VK_PAGE_DOWN) && _rhRow>=0) {
                final int
                    ctrl1=ctrl?99999:1,
                    page=maxi(1,hght(visibleRowsAndCols())-1),
                    steps=kcode==VK_DOWN ? ctrl1 : kcode==VK_UP ? -ctrl1 : kcode==VK_PAGE_DOWN ? (ctrl?99999:page) : kcode==VK_PAGE_UP ? -(ctrl?99999:page) : 0,
                    newRow=mini(countRows()-1, maxi(0, _rhRow+ steps));
                rmAll(ppSel,vSel);
                if (shift) {
                    if (_rhRowShift<0) _rhRowShift=_rhRow;
                    for(int row=mini(_rhRowShift,newRow); row<=maxi(_rhRowShift,newRow); row++)  adAll(proteinsInRow(row), vSel);
                } else {
                    _rhRowShift=-1;
                    adAll(proteinsInRow(newRow), vSel);
                }
                _rhRow=newRow;
                _pC.scrollRectToVisible(new Rectangle(x(visibleXYWH()), row2y(maxi(0,newRow-1)), 30, row2y(3)));
                done=true;
            }
            if (done) ((KeyEvent)ev).consume();
        }
        final int wheel=wheelRotation(ev);
        if (scaleUpDown(ev)!=0) setTheFont("",scaleUpDown(ev),false);
        else if (wheel!=0) {
            final int wheel2=alt ? wheel :4*wheel;
            if (q==_pC) {
                StrapAlign.setAlignmentPanelWithFocus(this);
                if (!shift && _vSB.isVisible()) scrollRel('v',4*wheel2,false); else scrollRel('h',wheel2,true);
            } else if (q==_rh || q==_ri) {
                final float f=_adjust[q==_rh?COLh:COLi]+wheel*.1f;
                _adjust[q==_rh?COLh:COLi]=f<0?0:f>1?1:f;
                repaintC(q);
            } else if (q==_hSB) scrollRel('h',wheel2,true);
        }
        final MouseEvent mev=deref(ev,MouseEvent.class);
        if (mev!=null) {
            final Rectangle rect=rectRubberBand();
            final int rectW=x(rect), rectN=y(rect), rectE=x2(rect), rectS=y2(rect);
            boolean rectChanged=false;
            if (id==MOUSE_PRESSED && ( q==_pC && !V3dUtils.setFocusedP(true, proteinInRow(mRow)) || gcp("MB",q)!=null)) {
                if (!isSelctd(StrapAlign.button(StrapAlign.TOG_MULTI_MBARS))) StrapAlign.setMenuBar(null,0,0);
                StrapAlign.setToolpane(null);
            }
            if (q==_pC) {
                if (0==_notYetED++) enableDisable(this);
                if (id==FocusEvent.FOCUS_LOST || id==FocusEvent.FOCUS_GAINED) setFG(id==FocusEvent.FOCUS_GAINED ? 0: 0x808080, header);
                if (id==MOUSE_EXITED && !mouseButton) {
                    if (x<2*EX) _pC.repaint(0,0,EX*3/2,32000);
                    _whatMouse=0;
                }
                if (id==MOUSE_CLICKED||id==MOUSE_PRESSED||id==MOUSE_RELEASED) {
                    _zoomCenter=null;
                    Arrays.fill(_zoomReduceHeight,-1);
                }
                if (mev!=null) {
                    final Object sOld=get(0,_resSelMouse);
                    _resSelMouse=(id==MOUSE_EXITED ? null : residueSelectionsAtXYstrict(x,y));
                    if (sOld!=get(0,_resSelMouse)) flashResSel(true);
                    ResSelUtils.setHide(_resSelMouse);
                    getBoundsAndRepaint(_resSelMouse, mRow, _selBounds);
                }
                final ResidueSelection ss[]=_resSelMouse;
                if (id==MOUSE_CLICKED) {
                    if (doubleClck(ev)) {
                        int countNotBB=0;
                        ResidueSelection ms=null;
                        for(int i=0; i<sze(ss); i++) {
                            final ResidueSelection s=(ResidueSelection)get(i,ss);
                            if (nam(s)==ResidueSelection.NAME_STANDARD) ms=s;
                            else if (nam(s)!=ResidueSelection.NAME_BACKBONE) countNotBB++;
                        }
                        if (ms!=null && countNotBB==0) ResSelUtils.toResidueAnnotation(true,ms);
                        else if ( (countNotNull(ss)>1 || ResSelUtils.countType('S',ss)>0)) ResSelUtils.chooseResSels(ResSelUtils.CHOOSE_EDIT, ss,ev, null, null);
                        else StrapAlign.editAnnotation(get(0,ss,ResidueAnnotation.class));
                    } else if ( toggleSel && !shift && !alt && sze(ss)>0) {
                        for(ResidueSelection s : ss)  {
                            if (s!=_resSelCursor && !vSel.remove(s)) vSel.add(s);
                        }
                    } else if (iButton!=2 && shift && !alt && !ctrl && _selIntervallCol>=0 && _selIntervallRow>=0) {
                        final int
                            r1=mini(_selIntervallRow, mRow),
                            r2=maxi(_selIntervallRow, mRow),
                            c1=mini(_selIntervallCol, mCol),
                            c2=maxi(_selIntervallCol, mCol);
                        adAllUniq(resSelInRectangle(new Rectangle(c1,r1, c2-c1+1, r2-r1+1)),vSel);
                    }
                    if (sze(ss)>0 && iButton==1) { _selIntervallRow=mRow; _selIntervallCol=mCol; }
                    StrapAlign.setAlignmentPanelWithFocus(this);
                    if (isSimplClick(ev) && !setCursor(StrapView.CURSOR_COLUMN,proteinInRow(mRow),mCol)) StrapEvent.dispatchLater(StrapEvent.CURSOR_CLICKED,99);
                } /* _pC MOUSE_CLICKED */
                final ResidueSelection selResSel[]=StrapAlign.coSelected().residueSelections('s');
                if (id==MOUSE_DRAGGED && iButton!=2 && _dnd==0 && !alt && _whatDrag!='H') {
                    if (_xDragFrom==MIN_INT) {
                        _resSelDragOri=_resSelMousePressed;
                        _whatDrag=_whatMouse!=0?_whatMouse :'A';
                        _beforeDrag[0]=sp(moSel);
                        _beforeDrag[1]=moSel.getSelectedAminoacids().clone();
                        _beforeDrag[2]=intObjct(moSel.getSelectedAminoacidsOffset());
                        _beforeDrag[3]=new Rectangle(rectRBA);
                    }
                    if (_xDragFrom!=MIN_INT) {
                        final int what=_whatDrag;
                        final boolean changeRect=ANSEW.indexOf(what)>0;
                        final ResidueSelection ssO[]=_resSelDragOri;
                        scrollToVisible(x,y);
                        if ( (what=='A' || what=='S') && (dragVert || cntainsAtLeastOne(ssO, selResSel)) && sze(ssO)>0 && !shift) {
                            revert('S');
                            revert('R');
                            final List v=dndV(true,qjc);
                            for(ResidueSelection s : ResSelUtils.plusSelected(false,ssO)) adUniq(ResSelUtils.dndFile(s), v);
                            if (sze(v)>0) {
                                _dnd='S';
                                exportDrg(qjc, ev, TransferHandler.COPY);
                            }
                        }
                        if (_dnd==0 && pRow!=null) {
                            if (what=='P') setProtectFromColumn(mCol>=countColumns()-1 ? 0 : get(mCol, StrapAlign.g2c().narrow2wideColumn()), false);
                            else if ( y2row(_yDragFrom)==mRow && !changeRect){
                                revert('R');
                                selectResidues(true, sze(ssO)==0, pRow, x2col(_xDragFrom), mCol, null, this);
                            } else {
                                final int
                                    rn=!changeRect ? mini(y2row(_yDragFrom), mRow) : NSEW_N.indexOf(what)>=0 ? mRow : rectN,
                                    rs=!changeRect ? maxi(y2row(_yDragFrom), mRow) : NSEW_S.indexOf(what)>=0 ? mRow : rectS,
                                    re=!changeRect ? maxi(x2col(_xDragFrom), mCol) : NSEW_E.indexOf(what)>=0 ? mCol : rectE,
                                    rw=!changeRect ? mini(x2col(_xDragFrom), mCol) : NSEW_W.indexOf(what)>=0 ? mCol : rectW;
                                rectChanged|=setBndsRBA(rw, rn, re-rw, mini(rs,nRows)-rn);
                                revert('S');
                            }
                        }
                    }
                } /* _pC MOUSE_DRAGGED */
                if (id==MOUSE_MOVED || id==MOUSE_RELEASED) {
                    if (_repaintOnMouse) {
                        _repaintOnMouse=false;
                        ChDelay.repaint(_pC,333);
                    }
                    int whatMouse=0;
                    final int overRect, colProtect=!Protein.isWide() ? getProtectFromColumn('N') : -1;
                    if (rectW<rectE && rectN<rectS) {
                        final int d=charW/2, rN=rectN*lineH, rS=rectS*lineH, rE=rectE*charW, rW=rectW*charW;
                        overRect=
                            !(rN-d<y && rS+d>y && rW-d<x && rE+d>x) ? 0:
                            (Math.abs(rN-y)<d ? 'n' : 0) +
                            (Math.abs(rS-y)<d ? 's' : 0) +
                            (Math.abs(rE-x)<d ? 'e' : 0) +
                            (Math.abs(rW-x)<d ? 'w' : 0);
                    } else overRect=0;
                    final boolean looksResized=x+x(q)<2*EM && lMarginResizable(mRow);
                    if (colProtect>0 && Math.abs(x-col2x(colProtect))<charW/2) whatMouse='P';
                    else if (cntainsAtLeastOne(ss, selResSel)) whatMouse='S';
                    else if (overRect!=0) whatMouse=overRect;
                    else if (looksResized) whatMouse='H';
                    else if (sze(ss)>0) whatMouse='S';
                    else whatMouse=0;
                    if ((_whatMouse=='H')!=(whatMouse=='H')) qjc.repaint(-x(q),0,2*EM,9999);
                    _whatMouse=whatMouse;

                    qjc.setCursor(cursr( whatMouse=='P' ? 'w' : whatMouse=='H' ? 'e' : whatMouse=='S' ? 'H' : ANSEW.indexOf(whatMouse)>0 ? whatMouse : 'D'));
                    qjc.setAutoscrolls(!looksResized);
                    setTipTiming(500,0);
                } /* _pC MOUSE_MOVED || MOUSE_RELEASED */
                if (id==MOUSE_RELEASED && ANSEW.indexOf(_whatDrag)>=0 && rect!=null) setRectRubberBand(x(rect),y(rect), wdth(rect), hght(rect));

                if (id==MOUSE_PRESSED) {
                    setMarchingAnt(ANTS_SEARCH, qjc, null, null, null); /* Clear NextAndPrevSelection */
                    setEnabld(true, button(BUT_SAVE_CURSOR));
                    StrapAlign.setAlignmentPanelWithFocus(this);
                    _rowPressed=mRow;
                    _colPressed=mCol;
                    _resSelMousePressed=_resSelMouse;
                    qjc.requestFocus();
                }  /* _pC MOUSE_PRESSED */

                if ( rectW<rectE && ( id==MOUSE_RELEASED && _whatDrag!=0 || id==MOUSE_PRESSED && rectChanged)) {
                    if (!shift && !ctrl) rmAll(StrapAlign.coSelected().residueSelections('s'), vSel);
                    String msg=null;
                    for(ResidueSelection s:resSelInRectangle(null) ) {
                        if (s==_resSelCursor) continue;
                        if (ctrl) { vSel.remove(s); msg="Ctrl-key: Unselected";}
                        else {
                            if (shift) msg="Shift-key: Added";
                            vSel.add(s);
                        }
                    }
                    if (msg!=null) StrapAlign.drawMessage("@1"+ANSI_GREEN+msg+" all residues selections in rectangular region");
                }
            } /* q==_pC */
            if (q==header) {
                if (id==MOUSE_EXITED) {
                    pcp(KEY_LOOKS_LIKE_DIVIDER, cursr(_reduceHeight==0 ? 's':'n'), header);
                    repaintC(q);
                }
                if (id==MOUSE_DRAGGED) {
                    if (_xDragFrom==MIN_INT && Math.abs(tl.columnWidth(COLi)+tl.columnWidth(COLh)-x)<55)_whatDrag='H';

                    _reduceHeightSave=0;
                    reduceHeight(_reduceHeight+y-qjc.getHeight()/2);
                    button(TOG_MAXIMIZE).s(false);
                    final ChButton tog=StrapAlign.button(StrapAlign.TOG_MAXIMIZE_DIALOGS);
                    if (tog.s()) { tog.s(false);  StrapAlign.rightCollapse(false); }
                }
                if (id==MOUSE_CLICKED && !ctrl && !shift && x<wdth(q)-EX) SPUtils.showSortDialog();
            } /* q==header */
            if (_whatDrag=='H' &&  (id==MOUSE_DRAGGED || id==MOUSE_RELEASED) && _xDragFrom!=MIN_INT && (q==_pC || q==header)) {
                tl.t().setW(x(SwingUtilities.convertPoint(qjc,x,0,tl))-tl.columnWidth(COLi), EM, 22222, COLh);
            }
            if (q==_rh) {
                final Protein ppVis[]=visibleProteins(), ppSel[]=StrapAlign.selectedProteins();
                final Container parent2=qjc.getParent().getParent();
                final int px=x(SwingUtilities.convertPoint(qjc, x, y,parent2));
                final boolean setSelection=_rhDragProt2row==null;
                if (id==MOUSE_DRAGGED && !alt) {
                    if (_rhProtsDrag==null) _rhProtsDrag=idxOf(pRow,ppSel)>=0 ? StrapAlign.selectedProteinsInVisibleOrder() : spp(pRow);

                    if (_dnd==0 && pRow!=null && (dragHori || px<4 || px>parent2.getWidth()-4) || _mouseEnterDragTarget) {
                        _dnd='P';
                        System.gc();
                        final List dndFile=dndV(true,qjc);
                        for(Protein p: _rhProtsDrag) {
                            if (p!=null) adAllUniq((File[])p.run(PROVIDE_DND_FILES,null), dndFile);
                            adUniq(p,dndFile);
                        }
                        final MouseEvent dndEv=new MouseEvent(qjc,MOUSE_DRAGGED, System.currentTimeMillis(), modi, 0, mini(qjc.getHeight()-4,maxi(4,y)), 0,false);
                        exportDrg(qjc, isMac() ? mev: dndEv, shift  ? TransferHandler.COPY_OR_MOVE :  TransferHandler.COPY);
                    } /* _rh dnd */
                    if (_dnd==0) {
                        _rhDragDestRow=mRow;
                        if (_rhDragSrcRow<0) {
                            _rhDragSrcRow=mRow;
                            _rhCountDragRows=1;
                            if (cntains(pRow,ppSel)) {
                                for(int r=mRow;--r>=0 && cntains(proteinInRow(r),ppSel);) _rhDragSrcRow=r;
                                for(int r=mRow; r<nRows && cntains(proteinInRow(r),ppSel); r++) _rhCountDragRows=r-_rhDragSrcRow+1;
                            }
                            _rhLastDragDestRow=-1;
                        }
                        qjc.setCursor(cursr('H'));
                        if (_rhLastDragDestRow!=_rhDragDestRow ) {
                            _rhDragProt2row= moveIndices(ppVis.length,_rhDragDestRow, _rhDragSrcRow, _rhCountDragRows);
                            if (setSelection && _rhDragProt2row!=null) {
                                BasicResidueSelection.selectRange(0,countColumns(), _resSelMarkRow);
                                for(int i=_rhCountDragRows; --i>=0;) {
                                    final Protein p0=(Protein)get(0,proteinsInRow(i+_rhDragSrcRow));
                                    if (p0!=null) { p0.addResidueSelection(_resSelMarkRow); _resSelMarkRow.setProtein(p0); }
                                }
                            }
                            repaintC(q);
                            repaintC(_ri);
                            threadPR(PR_ONLY_CHANGES).run();
                            _hSB.clearImage();
                            ChDelay.repaint(_hSB,111);
                            _rhLastDragDestRow=_rhDragDestRow;
                        }
                    }
                } /* _rh MOUSE_DRAGGED */
                if (id==MOUSE_MOVED || id==MOUSE_RELEASED || _dnd!=0) {
                    _rhProtsDrag=null;
                    if (_rhDragProt2row!=null) {
                        _rhDragProt2row=null;
                        if (_dnd==0) moveLines(_rhDragSrcRow,mRow,_rhCountDragRows);
                        threadPR(PR_ONLY_CHANGES).run();
                        repaintC(q);
                    }
                }
                setTipTiming(id==MOUSE_EXITED || x>lineH ? -1 : 30,0);
                if (id==MOUSE_MOVED) {
                    rhDrawMark(mRow, null);
                    _rhDragSrcRow=-1;
                    final int tt=x/Protein.BALLOON_MAXX, row=y2row(y), where=(row<<10)|modi|(tt<<28);
                    final Protein pp[]=proteinsInRow(row), p0=get(0,pp,Protein.class);
                    if (_whereTip!=where) {
                        _whereTip=where;
                        final String tip;
                        if (p0==null || tt>3) tip=null;
                        else if (tt==0) {
                            final BA sb=
                                pp.length==1 ? toBA(p0.run(ChRunnable.RUN_GET_TIP_TEXT,ev)) :
                                baTip().a("This row contains ").a(pp.length).a(" proteins:<br><sup>").join(pp,"<br>").a("</sup>");
                                if (getPrpty(StrapView.class,"popupShownP",null)==null) sb.a(RC4CM);
                                if (getPrpty(StrapView.class,"xrefShownP",null)==null) sb.a("<br>Click for cross-references");
                            tip=addHtmlTagsAsStrg(sb);
                        } else tip=p0.balloonText(false);
                        setTip(orS(tip,"<html><body></body></html>"),_rh);
                    }
                }
                if (id==MOUSE_EXITED) rhDrawMark(-1, null);
                if (id==MOUSE_CLICKED && pRow!=null) {
                    if (x<=TRIANGL && isSimplClick(ev)) SPUtils.showInfo(spp(pRow));
                    else {
                        qjc.requestFocus();
                        if ( iButton==1 && !alt) {
                            if (toggleSel && !shift) {
                                if (!vSel.remove(pRow)) vSel.add(pRow);
                            } else if (shift) {
                                for(int i=mini(mRow,_rhClickedRow); i<=maxi(mRow,_rhClickedRow); i++) vSel.add(get(i,ppVis));
                            } else { vSel.clear(); vSel.add(pRow);}
                            repaintC(q);
                        }
                        if (StrapAlign.tree().getParent().getWidth()>10) scrollToVis(pRow,StrapAlign.tree());
                        _rhClickedRow=mRow;
                        highlightRows(null);
                    }
                    _rhRow=mRow;
                    _rhRowShift=-1;
                } /* _rh MOUSE_CLICKED */
                if (!setSelection && _rhDragProt2row==null) for(Protein p : ppVis)  p.removeResidueSelection(_resSelMarkRow);
            } /* q==_rh */
            if (q==_pS || q==_pR) {
                final Protein p=q==_pS?secStruP() : rulerP();
                if ((id==MOUSE_ENTERED || id==MOUSE_EXITED)  && !mouseButton) highlightProteins(id==MOUSE_EXITED?null:new Protein[]{p},C(0xFF0000));
                if (id==MOUSE_CLICKED) {
                    if (q==_pS && plotAreaH('S')>0) {
                        if (_secStruPC==null) (_secStruPC=new ProteinCombo(ProteinList.SECSTRU)).s(p).li(this);
                        ChFrame.frame("Secondary structure",_secStruPC, ChFrame.PACK|CLOSE_CtrlW_ESC|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.AT_CLICK);
                    }
                    if (q==_pR) {
                        if (_rulerPC==null) {
                            (_rulerPC=new ProteinCombo(0L)).s(p).li(this);
                            pcp(KEY_NORTH_PANEL, (_rulerResNum=toggl("Display PDB residue number and chain").doRepaint(_pR)).cb(), _rulerPC);
                        }
                        ChFrame.frame("Ruler",_rulerPC, ChFrame.PACK|CLOSE_CtrlW_ESC|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.AT_CLICK);
                    }
                }
            }
            if (id==MOUSE_DRAGGED) {
                if (_xDragFrom==MIN_INT) {
                    _xDragFrom=x;
                    _yDragFrom=y;
                }
            }
            if (id==MOUSE_MOVED || id==MOUSE_RELEASED || id==MOUSE_PRESSED) {
                _xDragFrom=MIN_INT;
                _whatDrag=_dnd=0;
                _mouseEnterDragTarget=false;
            }
            if (id==MOUSE_RELEASED && (q==_hSB || q==_vSB || q==_hSB0)) {
                if (_reduceHeight>hght(paneSize())) { reduceHeight(MAX_INT);}
                repaintC(_pC);
                repaintC(_pR);
                repaintC(_pS);
            }
            if ( (q==_pC || q==_rh) && id==MOUSE_EXITED) setTipTiming(-1,0);
        }
        if (q==_pC) _ks.pe(ev);
        if (vSelMC!=modic(vSel)) StrapEvent.dispatch(StrapEvent.OBJECTS_SELECTED);
    }
    private final Object _beforeDrag[]=new Object[4];
    private void revert(int what) {
        if (what=='S') {
            final boolean bb[]=(boolean[])_beforeDrag[1];
            selectResidues(false, false, (Protein)_beforeDrag[0], atoi(_beforeDrag[2]),0, bb==null?NO_BOOLEAN:bb.clone(), this);
        }
        if (what=='R') {
            final Object r=_beforeDrag[3];
            if (r!=null) setBndsRBA(x(r), y(r), wdth(r), hght(r));
        }
    }
    public void actionPerformed(ActionEvent ev) {
        actPerformed(_panel!=null?this:StrapAlign.alignmentPanel(),ev);
    }
    private static void actPerformed(StrapView view,  ActionEvent ev) {
        final Object q=ev.getSource();
        if (view==null || q==null) return;
        final String cmd=ev.getActionCommand();
        final HScrollBar hSB=view._hSB;
        final Rectangle rect=view.rectRubberBand();
        final int
            bi=idxOf(q,_button),
            modi=ev.getModifiers(),
            rectW=x(rect),
            rectE=x2(rect);
        final boolean  select=isSelctd(q), ctrl=0!=(modi&CTRL_MASK), shift=0!=(modi&SHIFT_MASK);
        final Protein
            ppSel[]=StrapAlign.selectedProteins(),
            ppAll[]=view.visibleProteins(),
            pCursor=view.cursorProtein(),
            ppSelOrCursor[]= ppSel.length>0 ? ppSel :  pCursor!=null ? spp(pCursor) : ppAll.length==1 ? ppAll :Protein.NONE;
        if (cntains(q,view._tfCharSpc)) view.setTheFont("",0,false);
        if (q==_choiceI) repaintC(view._ri);
        if (q==Customize.customize(Customize.proteinInfoRplc)) repaintC(view._ri);
        if (q==view._secStruPC) {
            view._secStruP=sp(q);
            revalAndRepaintC(view._pS);
            repaintC(view._hSB);
        }
        if (q==view._rulerPC) {
            view._rulerP=sp(q);
            revalAndRepaintC(view._pR);
        }
        if (bi==BUT_ALIGN_UNDO) {
            setEnabld(false, button(BUT_ALIGN_UNDO));
            for(Protein p : ppAll) {
                final int[] gaps=gcp(button(BUT_ALIGN_UNDO),p,int[].class);
                if (gaps!=null) p.setResidueGap(gaps);
            }
            StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_CHANGED,1);
        }
        if (bi==BUT_DL_FILE) startThrd(thrdCR(view,"DL_P_FILE"));
        if (bi==BUT_LIST_HET) SPUtils.showChildObjects(HeteroCompound.class, 'H', spp(gcp("P",q)));
        if (bi==BUT_3D) {
            final Protein ppSel3D[]=ProteinList.onlyProteins(ProteinList.CALPHA, shift?ppAll:ppSelOrCursor);
            if (ppSel3D.length>0) {
                if (ctrl) {
                    final ProteinViewer pv=V3dUtils.open3dViewer(V3dUtils.OPEN_INIT_SCRIPT,ppSel3D,StrapAlign.defaultClass(ProteinViewer.class));
                    if (shift) {
                        Protein3dUtils.showLogPanel(true,pv);
                        Protein3dUtils.showLogPanel(false,pv);
                    }
                } else StrapAlign.new3dBackbone(0,ppSel3D);
            } else {
                final BA sb=new BA(999);
                final String ids[]=new String[ppSelOrCursor.length];
                final Protein[] ppI=new Protein[ppSelOrCursor.length];
                int count=0;
                for(Protein p : ppSelOrCursor) {
                    final String id=gcps(Protein.KEY_SUGGEST_PDB_ID, p);
                    if (id==null) continue;
                    sb.a(ids[count]=id).a(" for ").aln(ppI[count++]=p);
                }
                final String msg="Do you want to use the following 3D-structures?<br>Possible mismatches with your sequence will be highlighted.";
                if (count>0 && ChMsg.yesNo(pnl(CNSEW,scrllpn(0, sb, dim(60*EM,9*EX)),msg))) {
                    final Runnable r=SPUtils.threadInferCoordinates(0,ppI,ids)[0];
                    startThrd(thrdRRR(r,V3dUtils.thread_open3dViewer(0,ppI,Protein3d.PView.class,null)));
                } else {
                    final JComponent dia=StrapAlign.addDialogC(CLASS_DialogSimilarStructure);
                    pcp(KEY_OPTIONS, longObjct(DialogSimilarStructureResult.VIEW3D_ON_PRESSING_APPLY), dia);
                    runR(SPUtils.dialogSetProteins(ppSelOrCursor, dia));
                }
            }
        }
        if (q==button(TOG_OUTLINE_HSB)) {
            view._shadingSB=select;
            repaintC(hSB.clearImage());
            view.strapSB();
        }
        if (q==view._butSB) view.strapSB();

        if (cmd=="CLIPBOARD" && rectW<rectE) toClipbd(baTip().join(SPUtils.gappedSequences(ppInRectangle(view),rectW,rectE,'-')));
        if (cmd=="RM_RECT") { view.setRectRubberBand(0,0,0,0); repaintC(view._pC); }
        if (cmd=="NEW_ANNO" || cmd=="ANNO_R") {
            final Protein pp[];
            final int colFrom, colTo;
            if (cmd=="NEW_ANNO") {
                pp=spp(view.proteinInRow(view._rowPressed));
                if (pp.length==0) return;
                colFrom=view._colPressed;
                colTo=colFrom+1;
            } else {
                pp=ppInRectangle(view);
                colFrom=rectW;
                colTo=rectE;
            }
            if (pp.length>0) {
                final List vSel=StrapAlign.selectedObjectsV();
                vSel.clear();
                for(Protein p : pp) {
                    final ResidueAnnotation a=new ResidueAnnotation(p);
                    final int idx0=Protein.firstResIdx(p);
                    a.setValue(0,ResidueAnnotation.GROUP,"Rectangle");
                    a.setValue(0,ResidueAnnotation.POS,(idx0+p.column2nextIndexZ(colFrom)+1)+"-"+(idx0+p.column2thisOrPreviousIndex(colTo-1)+1));
                    a.setColor(C(0xFF00FF));
                    a.setStyle(VisibleIn123.STYLE_BACKGROUND);
                    a.setValue(0,"Note","filled in rectangular region");
                    a.setValue(0,ResidueAnnotation.NAME,"rectangle");
                    p.addResidueSelection(a);
                    vSel.add(a);
                    if (cmd=="NEW_ANNO") StrapAlign.editAnnotation(a);
                }
                new StrapEvent(view,StrapEvent.RESIDUE_SELECTION_CHANGED).run();
                new StrapEvent(view,StrapEvent.OBJECTS_SELECTED).run();
                StrapAlign.coSelected().selectionMenu().COLOR_BUTTON.run(ChRunnable.RUN_SHOW_IN_FRAME,null);
            }
        }
        if (cmd=="CROP") {
            final Protein pp[]=ppInRectangle(view);
            for(Protein p : pp) {
                final int idx0=Protein.firstResIdx(p);
                pcp(cmd, new int[]{idx0+p.column2nextIndexZ(rectW), idx0+p.column2thisOrPreviousIndex(rectE-1)+1}, p);
            }
            DialogRenameProteins.instance(StrapView.class).rename(DialogRenameProteins.INFO_CUT_TERMINI,pp,cmd);
        }
        if (cmd=="U" || cmd=="L") {
            for(Protein p: ppInRectangle(view)) {
                final byte aa[]=p.getResidueTypeFullLength();
                for(int col=rectW; col<rectE; col++) {
                    final int iA=p.column2indexZ(col)+p.getResidueSubsetB1();
                    if (0<=iA && iA<aa.length) aa[iA]=(byte) (cmd=="U" ? aa[iA]&~32 : aa[iA]|32);
                }
                p.setResidueType(aa);
            }
            new StrapEvent(view,StrapEvent.RESIDUE_TYPES_CHANGED).run();
        }
        if (cmd=="A") runCR(StrapAlign.addDialogC(DialogAlign.class), DialogAlign.RADIO_REGION, null);
        if ((cmd=="DG" || cmd=="IG" || cmd=="DGA") && rectW<rectE ) {
            final Protein pp[]=ppInRectangle(view);
            if (cmd=="DGA") AcceptAlignment2.delGaps(pp, rectW, rectE, null);
            else {
                final int deleted=cmd!="DG"?0:AcceptAlignment2.eliminateCommonGaps(pp, rectW, rectE, null, StrapAlign.g2c().wide2narrowColumn());
                final Object KEY=new Object();
                for(Protein p:pp) pcp(KEY, new int[]{p.column2nextIndexZ(rectW),p.column2nextIndexZ(rectE)},p);
                for(Protein p:pp) {
                    final int iA1=gcp(KEY, p, int[].class)[0];
                    final int iA2=gcp(KEY, p, int[].class)[1];
                    if (cmd=="DG" && iA2!=0) p.addResidueGap(iA2,deleted);
                    if (cmd=="IG" && iA2>=0) p.setResidueGap(iA1, rectE-rectW+p.getResidueGap(iA2));
                }
                for(Protein p:pp) pcp(KEY, null, p);
            }
            new StrapEvent(view,StrapEvent.ALIGNMENT_CHANGED).run();
        }
        if (bi==BUT_EXPORT || cmd=="W") StrapAlign.addDialogC(CLASS_DialogExportWord);
        if (cmd=="WS") Strap.instance().run("WS",select?"false":"true");
        if (bi==TOG_PLOT_ENTIRE_PANEL) StrapEvent.dispatchLater(StrapEvent.VALUE_OF_RESIDUE_CHANGED,111);
        if (bi==BUT_SORT) SPUtils.showSortDialog();
        if (bi==BUT_ALIGN) {
            if (gcp("C",button(BUT_ALIGN))==null) {
                for(Protein p: StrapAlign.proteins()) pcp(button(BUT_ALIGN_UNDO),null,p);
                final Protein[] pp=ppSel.length>1?ppSel:ppAll;
                if (sze(pp)>1) {
                    final long when=System.currentTimeMillis();
                    stopAllAlignments();
                    setButAlign(-1);
                    final SequenceAligner aligner=SPUtils.alignerInstance();
                    aligner.setOptions(SequenceAligner.OPTION_NOT_TO_SECURITY_LIST);
                    _aligner=aligner;
                    for(Protein p:pp) pcp(button(BUT_ALIGN_UNDO),p.getResidueGap().clone(),p);
                    startThrd(thrdRRR(
                                      SPUtils.threadAlignProteins((modi&SHIFT_MASK)==0 ? 0 : SPUtils.ALIGN_CHANGE_ORDER, aligner, Arrays.asList(pp), CAN_BE_STOPPED),
                                      thrdCR(view,"AFTER_ALIGN", new Object[]{aligner,longObjct(when), ""})
                                      ));
                }
            } else stopAllAlignments();
        }
        if (bi==BUT_ALIGN_CTRL) shwCtrlPnl(ChFrame.PACK|CLOSE_CtrlW_ESC|ChFrame.AT_CLICK, "Aligner", _aligner);
        if (cmd=="FONT") {
            if (_fFont==null) {
                final ChJList l=new ChJList(allFnts(),ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT).li(view);
                pcp(KEY_ACTION_COMMAND,ChRenderer.CMD_FOND_NAME,l);
                ChJList.setSelO(ChJList.SEL_STRSTR_IC, "Monospaced",l);
                final Object pSouth=pnl(VBHB,
                                        pnl(HB,"Horizontal space between characters", view._tfCharSpc[0]),
                                        pnl(HB,"Vertical space between characters", view._tfCharSpc[1]),
                                        " ",
                                        "Change font size by typing  Ctrl++ or by Ctrl+mouse-wheel.",
                                        "You may need to use number block of keyboard."
                                        );
                _fFont=new ChFrame(cmd).ad(pnl(CNSEW,scrllpn(0,l,dim(333,222)),null,pSouth));
            }
            _fFont.shw(ChFrame.AT_CLICK|ChFrame.PACK);
        }
        if (cmd==ChRenderer.CMD_FOND_NAME) view.setTheFont(toStrg(q),0,true);
        if (cmd=="CLOSE") StrapAlign.closeAlignmentPanel(view);
        if (bi==BUT_FEATURE) {
            if (ctrl) StrapDAS.show(false);
            else SequenceFeatures.instance().showPanel();
        }
        if (q==ShadingAA.choice()) {
            inEDTms(view.thread(T_ENABLED),111);
            final Protein pp[]=view.visibleProteins();
            repaintC(view._pC);
            repaintC(hSB.clearImage());
            view.thread(T_CLEAR_SB).run();
            ChDelay.repaint(hSB,hSB.LAST[DURATION]*4);
            if (button(TOG_OUTLINE_HSB).s()) repaintC(hSB.clearImage());
            ChDelay.revalidate(view.panel(),333);
            StrapEvent.dispatchLater(StrapEvent.AA_SHADING_CHANGED,111);
        }
        if (bi==TOG_MAXIMIZE) {
            StrapAlign.rightCollapse(select);
            revalAndRepaintC(view.panel());
            if (select) {
                view._reduceHeightSave=view._reduceHeight;
                view.reduceHeight(0);
                StrapAlign.button(StrapAlign.TOG_MAXIMIZE_DIALOGS).s(false);
            } else view.reduceHeight(view._reduceHeightSave);
            revalAndRepaintC(view.panel());
        }
        if (bi==TOG_PROTECT_COL ) {
            final Rectangle r=view.visibleRowsAndCols();
            final int cc=x(r)+r.width/2;
            view.setProtectFromColumn(!select ? 0 : get(cc, StrapAlign.g2c().narrow2wideColumn()), false);
        }
        if (bi==TOG_BRIGHTER || bi==TOG_SHADE_ONLY_SELECTED) { repaintC(view._pC); repaintC(hSB); }
        if (bi==TOG_INFO) {
            final ChJTable t=view._tLayout.t();
            if (select) {
                final int wi=_wInfo>5*EM?_wInfo:222, wh=t.column(COLh).getWidth();
                for(int i=2;--i>=0;){
                    t.setW(wi, 0, 2222, COLi);
                    t.setW(wh, EM, 2222, COLh);
                }
            } else {
                _wInfo=t.column(COLi).getWidth();
                t.setW(0, 0, 0, COLi);
            }
        }
        if (bi==BUT_RESTORE_CURSOR || strEquls("RESTORE",cmd,0)) view.saveRestore(false, intObjct(modi+trailInt(cmd)));
        if (bi==BUT_SAVE_CURSOR || strEquls("SAVE",cmd,0)) view.saveRestore(true, intObjct(modi+trailInt(cmd)));
        if (bi==BUT_SEQVISTA) {
            final List<File> v=new ArrayList();
            for(Protein p : ppSelOrCursor)  if (p!=null && (p.countNucleotides()>0 || p.containsOnlyACTGNX())) adUniq(p.getFile(),v);
            Hyperrefs.toSeqvista(toArry(v,File.class));
        }
        if (bi==BUT_NUCL && sze(ppSelOrCursor)>0) {
            final List<Protein> vGB=new ArrayList() , vNoGB=new ArrayList();
            EditDna pane=null;
            for(Protein p : ppSelOrCursor) {
                adUniq(p, sze(p.getCDS())>0 && p.countNucleotides()==0 ? vGB : vNoGB);
            }
            if (sze(vGB)>0) runR(SPUtils.dialogSetProteins( spp(vGB), StrapAlign.addDialogC(CLASS_DialogGenbank)));
            if (sze(vNoGB)>2) ChUtils.error("Please select only one nucleotide sequence");
            else {
                for(int i=0; i<sze(vNoGB);i++) {
                    final Protein p=vNoGB.get(i);
                    if (pane==null) pane=(EditDna)StrapAlign.addDialogC(EditDna.class);
                    pane.addEditor(p,true);
                }
            }
        }
        if (q==sliderSimilarity()) {
            StrapEvent.dispatchLater(StrapEvent.AA_SHADING_CHANGED,333);
            view.threadPR(PR_ONLY_CHANGES).run();
            button(TOG_OUTLINE_HSB).s(view._shadingSB=true);
            repaintC(hSB.clearImage());
        }
    }
    /* <<< ActionEvent <<< */
    /* ---------------------------------------- */
    /* >>> Frequency >>> */
    Protein[] ppShading() { final Protein pp[]=_ppShading; return pp==null?Protein.NONE:pp;}
    byte[][] gappedSeq(boolean selectedProteins) {
        Protein[] pp=selectedProteins ? StrapAlign.selectedProteins() : visibleProteins();
        if (pp.length==0) pp=visibleProteins();
        _ppGapped=pp;
        final Object[] cached=cached();
        final int X01=selectedProteins?1:0;
        byte[][] ggg=(byte[][])cached[GAPPED+X01];
        if (ggg!=null && ggg.length!=pp.length) ggg=null;
        final long mc=mc()+Protein.MC_GLOBAL[MCA_ALIGNMENT]+(!selectedProteins?0:modic(StrapAlign.selectedObjectsV()));
        if (ggg==null || mc!=CACHE_MC[GAPPED+X01]) {
            CACHE_MC[GAPPED+X01]=mc;
            if (ggg==null) cached[GAPPED+X01]=ggg=new byte[pp.length][];
            for(int i=pp.length; --i>=0;) {
                final Protein p=pp[i];
                ggg[i]=p==null ? NO_BYTE : p.getGappedSequence();
            }
        }
        return ggg;
    }
    short[][] frequenciesAtColumn(int fromCol, int toCol, char allOrSel/*[c,a,s, C,A,S]*/) {
        final boolean selectedProteins=(32|allOrSel)=='s' || (32|allOrSel)=='c' && button(TOG_SHADE_ONLY_SELECTED).s();
        final int cols=countColumns(), X01=selectedProteins?1:0;
        final Object[] cached=cached();
        final byte[][] ggg=gappedSeq(selectedProteins);
        final long mc=CACHE_MC[GAPPED+X01]+Protein.MC_GLOBAL[MCA_ALIGNMENT];
        short[][] fff=(short[][])cached[FREQ_AA+X01];
        if (cols>=sze(fff)) cached[FREQ_AA+X01]=fff=new short[cols+99][];
        else if (CACHE_MC[FREQ_AA+X01]!=mc) FrequenciesAA.reset(fff);
        CACHE_MC[FREQ_AA+X01]=mc;
        FrequenciesAA.atColumn(ggg,fff , fromCol, toCol,  !selectedProteins);
        if (is(UPPR,allOrSel)) _ppShading=_ppGapped;
        return fff;
    }
    /* <<< Frequency <<< */
    /* ---------------------------------------- */
    /* >>> Row header >>> */
    private final static int TRIANGL=EM+6, _3angl[][]={ { EM, EM/2, 0, 0}, { 0,  EM,   0, 0} };
    private float _adjust[]={1,0};

    private static int[] _rhIndices;
    private int _rhDragProt2row[],_rhMark=-1, _rhDragSrcRow=-1, _rhDragDestRow, _rhLastDragDestRow, _rhClickedRow=0, _rhCountDragRows=1;
    public JComponent rowHeader() { return _rh;}
    private int[] draggedProtein2row() { return _rhDragProt2row;}
    private void rhDrawMark(int row, Graphics g0) {
        if (_rhMark!=row || g0!=null) {
            final int  w=TRIANGL, h=TRIANGL;
            if (_rhMark>=0 && g0==null) _rh.repaint(0, row2y(_rhMark), 99, h );
            if (row>=0) {
                final Graphics g=g0!=null?g0:_rh.getGraphics();
                final int ry=row2y(row);
                if (g!=null) {
                    g.setColor(C(0xFFffFF));
                    g.fillRect(0, ry, w, h);
                    g.setColor(C(0));
                    g.translate(3, ry+4);
                    g.fillPolygon(_3angl[0],_3angl[1], 4);
                    g.translate(-3, -ry-4);
                    g.drawRect(1,ry+1, w-2, h-2);
                }
            }
            if (g0==null) _rhMark=row;
        }
    }
    public static void setRowHeaderType(String t) {
        for(StrapView v : StrapAlign.alignmentPanels()) repaintC(v.rowHeader());
    }
    public void mouseEnterDrogTarget(Object target) {
        if (target instanceof ChJList || gcp(V3dUtils.KEY_PROTEIN_VIEWER,target)!=null) _mouseEnterDragTarget=true;
    }
    /* <<< Row header <<< */
    /* ---------------------------------------- */
    /* >>> Plotting >>> */
    private static UniqueList _vPl;
    static UniqueList<Object> getPlottersV() { return _vPl==null ? _vPl=new UniqueList(Object.class) : _vPl;}
    private final WeakHashMap<Object,double[][]> mapMiMa=new WeakHashMap();
    private void plot(Graphics g) { final Rectangle vis=visibleXYWH(); plot(g,_pC,charB().width, y(vis), y2(vis)); }
    void plot(Graphics g, Component component, double charWf, int yMin,int yMax) {
        if (sze(getPlottersV())==0) return;
        final Object[] cached=cached();
        final int charW=(int)charWf, small=maxi(1,charW>>4), charH=charB().height, xMin, xMax;
        {
            final Shape c=g.getClip();
            final Rectangle  r= c!=null ? c.getBounds() : null;
            if (r!=null) { xMin=x(r); xMax=xMin+r.width;}
            else { xMin=0; xMax=MAX_INT;}
        }
        final short[][] ccc=component==_pC ? (short[][])cached[MCSV_CHARACTERS] : null;
        for(Object hasValues : getPlottersV().asArray()) {
            final ValueOfResidue vor;
            final ValueOfAlignPosition vap;
            {
                final ValueOfAlignPosition vap0= hasValues instanceof ValueOfAlignPosition  ? (ValueOfAlignPosition) hasValues : null;
                final ValueOfResidue       vor0= hasValues instanceof ValueOfResidue        ? (ValueOfResidue) hasValues  : null;
                vap= vap0!=null && vap0.getValues()!=null  && vap0.getProteins()!=null ? vap0 : null;
                vor= vap==null && sp(vor0)!=null && vor0.getValues()!=null ? vor0 : null;
            }
            final double vv[]=vor!=null ? vor.getValues() : vap!=null ? vap.getValues() : null;
            if (vv==null) continue;
            int ymin=yMin,ymax=yMax;
            final Protein p=sp(vor);
            if (component==_pC) { /* plot in separate row */
                final int row=!button(TOG_PLOT_ENTIRE_PANEL).s() ? findRow(sp(hasValues)) : -1;
                if (row>=0) { ymin=row*(_lineSkip+charH);  ymax=ymin+_lineSkip+charH; }
            }
            final Color color=colr(hasValues);
            g.setColor(color!=null ? color : C(0xFFAFAF));
            final int vvLength=mini(vor!=null ? p.countResidues(): countColumns(),vv.length);
            double[][]vv_mima=mapMiMa.get(hasValues);
            final double[] mima;
            final int modi=modic(vap!=null ? vap : vor);
            if (vv_mima==null || vv_mima[0]!=vv || modi!=(int)vv_mima[1][2]) {
                mapMiMa.put(hasValues,  vv_mima=new double[][]{vv,mima=new double[]{Double.MAX_VALUE,Double.MIN_VALUE,modi}});
                for(int i=vvLength; --i>=0;) {
                    if (vv[i]<mima[0]) mima[0]=vv[i];
                    if (vv[i]>mima[1]) mima[1]=vv[i];
                }
            } else mima=vv_mima[1];
            final double mi=mima[0], ma=mima[1],  h_ma_mi= ma==mi ? 1 : (ymax-ymin-4)/(ma-mi);
            final int toCol=(int)mini((int)((xMax)/charWf+1),countColumns()+1);
            for(int col=maxi(0,(int)(xMin/charWf-1)), lastY=-1; col<toCol; col++) {
                final int x=(int)(col*charWf);
                final int iA=vap!=null ? col : p.column2indexZ(col);
                final double v=iA>=0 && iA<vvLength ? vv[iA] : Float.NaN;
                if (Double.isNaN(v)) { lastY=-1; continue;}
                final int y= ymax-(int) ((v-mi)*h_ma_mi)-2;
                if (y>0) {
                    if (lastY>0) g.drawLine(x+small, y, x-small,lastY);
                    fillBigRect(g, x+small,y-1,charW-small*2,3);
                }
                lastY=y;
                if (component==_pC) markDirtyRect(true, ccc,(y-charH/2)/charH, col,1,2);
            }
        }
    }
    /* <<< Plotting <<< */
    /* ---------------------------------------- */
    /* >>>  Bar Chart >>> */
    private double[] _vopValues;
    void setBarchart(ChCombo c,int fromCol, int toCol, Protein p) {
        _vopClass=c;
        _vopOtherProt=p;

        repaintC(_rh);
    }
    private void barChart(Graphics gr, Protein pp[], int rowFrom, int rowTo) {
        final Object KEYc="SV$$VOPc", KEYi="SV$$VOPi";
        final ChCombo choice=_vopClass;
        if (choice==null) return;
        final Object sharedInst=choice.instanceOfSelectedClass(null);
        final Rectangle cb=charB();
        final int w=_rh.getWidth();
        double vv[]=_vopValues;
        if (sze(vv)<pp.length) vv=_vopValues=new double[pp.length];
        double max=Double.MIN_VALUE,min=Double.MAX_VALUE;
        for(int i=pp.length; --i>=0; ) {
            final Protein p=pp[i];
            if (p==null) continue;
            Object inst=gcp(KEYc,p)==sharedInst ? gcp(KEYi,p) : null;
            if (inst==null) {
                inst=mkInstance(choice);
                pcp(KEYi,inst,p);
                pcp(KEYc,sharedInst,p);
            }
            final double v=vv[i]= SPUtils.computeValue(0, inst, p, _vopOtherProt, 0, MAX_INT);
            if (min>v)min=v;
            if (max<v)max=v;
        }
        if (max==Double.MIN_VALUE) return;
        final Color  COLOR_BAR=C(0xFFff00), COLOR_BAR2=C(0xaaAA00);
        for(int i=rowFrom; i<rowTo; i++) {
            if (Double.isNaN(vv[i])) continue;
            final int width=(int)((vv[i]-min)/(max-min)*(w-9));
            final int y=row2y(i);
            gr.setColor(COLOR_BAR);
            fillBigRect(gr, 2,y, width+3, cb.height );
            gr.setColor(COLOR_BAR2);
            gr.drawRect(2,y, width+3, cb.height );

        }
    }
    /* <<< Bar Chart <<< */
    /* ---------------------------------------- */
    /* >>> Toolbar >>> */
    private static ChFrame _fFont;
    private static JComponent _toolbar;
    static JComponent toolbars() {
        if (_toolbar==null) {
            final Component shade=ShadingAA.choice().li(instance());
            if (myComputer() && strstr("flav",dirWorking())>=0) ShadingAA.choice().s(5); /*FLAV*/
            setMaxSze(-1,-1, shade);
            _choiceI=new ChCombo(Protein.INFO_TITLE).s(5).li(instance());
            setPrefSze(false, 20*EM, MIN_INT, _choiceI);
            final JComponent
                cbBright=button(TOG_BRIGHTER).cb(), cbSelected=button(TOG_SHADE_ONLY_SELECTED).cb(), cbColorSB=button(TOG_OUTLINE_HSB).cb(),
                pInfo=pnl(C(BG_TOGGLE_BUT),
                          _choiceI,
                          Customize.newButton(Customize.proteinInfoRplc).rover(IC_CUSTOM).t(null).tt("Shorten text by text replacements"),
                          button(BUT_DL_FILE)
                          ),
                pButtons=pnl(
                             FLOWLEFT,
                             "#ETB",
                             KOPT_IS_VALIDATE_ROOT,
                             button(TOG_INFO).doCollapse(false,pInfo),
                             pInfo,
                             " ",
                             sliderSimilarity(),
                             shade,
                             " ",
                             toggl().doCollapse(false,new Object[]{cbBright,cbSelected,cbColorSB}),
                             cbBright,
                             cbSelected,
                             cbColorSB,
                             " ",
                             button(BUT_NUCL),
                             !myComputer() ? null :  button(BUT_SEQVISTA).t("SeqVISTA ????"),
                             button(BUT_3D),
                             button(BUT_FEATURE),
                             button(BUT_EXPORT),
                             button(BUT_ALIGN),
                             button(BUT_ALIGN_UNDO),
                             button(TOG_MAXIMIZE),
                             buttn(BUT_LOG),
                             button(BUT_SAVE_CURSOR),
                             button(BUT_RESTORE_CURSOR),
                             KOPT_ADAPT_SMALL_SIZE
                             );
            addMoli(ADD_MOLI_CHILDS|MOLI_REPAINT_ON_ENTER_AND_EXIT,pButtons);
            evAdapt(instance()).addTo("m",button(BUT_ALIGN)).addTo("M",pButtons);
            setButAlign(0);
            addMoli(ADD_MOLI_CHILDS|MOLI_REPAINT_ON_ENTER_AND_EXIT,pButtons);
            pcp(KEY_PREF_SIZE, dim(EX, 2+prefH(pButtons)), pButtons);
            _toolbar=pButtons;

            enableDisable(null);
        }
        return _toolbar;
    }
    static void enableDisable(StrapView v) {
        pcp(KOPT_DO_NOT_INVALIDATE, "", StrapAlign.dialogPanel());
        final Protein
            ppSel[]=StrapAlign.selectedProteins(),
            pCursor=v==null?null:v.cursorProtein(),
            pp[]=v==null?Protein.NONE:v.visibleProteins(),
            p1=pCursor!=null ? pCursor : ppSel.length>0 ? ppSel[0] : pp.length==1 ? pp[0] : null;
        final boolean isNucEnabled=ProteinList.isEnabled(ppSel, ProteinList.NT_or_ACTGN) ||  ppSel.length==0 && ProteinList.isEnabled(p1, ProteinList.NT_or_ACTGN);
        setEnabld(isNucEnabled, button(BUT_NUCL));
        setEnabld(isNucEnabled, button(BUT_SEQVISTA));
        setEnabld(sze(ppSel)>0 || pCursor!=null, button(BUT_3D));
        for(int i=3; --i>=0;) setEnabld(pp.length>0, button(i==0 ? BUT_EXPORT : i==1 ? BUT_FEATURE : TOG_BRIGHTER));
        setEnabld(pp.length>0, ShadingAA.choice());
        for(int i=2; --i>=0;) setEnabld(pp.length>1, i==0 ? button(BUT_ALIGN) : sliderSimilarity());
        final int color=pp.length>0 ? 0xFF : 0x808080;
        final ChButton b=button(BUT_EXPORT).fg(color);
        if (b.getIcon()==null) b.setBorder(gcp(C(color),b, javax.swing.border.Border.class));
        setButAlign(ppSel.length>1?ppSel.length : MAX_INT);
        pcp(KOPT_DO_NOT_INVALIDATE, null, StrapAlign.dialogPanel());
        boolean noF=false;
        for(Protein p : pp) noF=noF||p.getFile()==null || p.isInMsfFile();
        setEnabld(noF, button(BUT_DL_FILE));
    }
    public static void setButAlign(int nProt) {
        final boolean isAligning=nProt<0;
        if (!isEDT()) inEdtLater(thrdM("setButAlign",StrapView.class, new Object[]{intObjct(nProt)}));
        else {
            pcp(KOPT_DO_NOT_INVALIDATE, "", StrapAlign.dialogPanel());
            final String t=nProt<1?"Align " : nProt==MAX_INT ? "Align all" : "Align "+nProt;
            button(BUT_ALIGN).cp("C", isAligning ? "C":null)
                .t(t)
                .i(isAligning?IC_STOP:null)
                .setHeight(isAligning ? prefH(sliderSimilarity())-2 : -1);
            repaintC(button(BUT_ALIGN));
            pcp(KOPT_DO_NOT_INVALIDATE, null, StrapAlign.dialogPanel());
        }
    }
    /* <<< Toolbar <<< */
    /* ---------------------------------------- */
    /* >>> ResidueSelection >>> */
    private static ResidueSelection _resSelMouse[], _resSelMousePressed[], _resSelDragOri[];
    private final BasicResidueSelection _resSelMarkRow=new BasicResidueSelection(0), _resSelCursor=new BasicResidueSelection(0), _resSel=new BasicResidueSelection(0);
    {
        _resSelCursor.setName(ResidueSelection.NAME_CURSOR);
        _resSelCursor.setColor(C(0xFFffFF));
        _resSelCursor.setImage(img(IC_TEXT_CURSOR));
        _resSelCursor.setStyle(VisibleIn123.STYLE_CURSOR);
        _resSelCursor.setVisibleWhere(VisibleIn123.SEQUENCE);
        _resSelMarkRow.setStyle(VisibleIn123.STYLE_BACKGROUND);
        _resSelMarkRow.setColor(C(0xFFffFF,99));
        _resSelMarkRow.setVisibleWhere(VisibleIn123.SEQUENCE);
        _resSel.setName(ResidueSelection.NAME_STANDARD);
        _resSel.setColor(C(0x5555ff));
        _resSel.setVisibleWhere(0xffffffff);
        _resSel.setStyle(VisibleIn123.STYLE_DOTTED);
    }
    BasicResidueSelection resSelCursor() { return _resSelCursor;}
    private ResidueSelection[] resSelInRectangle(Rectangle rect) {
        final Rectangle r=rect!=null ? rect : rectRubberBand();
        if (r==null) return ResidueSelection.NONE;
        final List<ResidueSelection> v=new ArrayList();
        for(Protein p : ppInRectangle(r, this)) {
            final int iA0=p.column2nextIndexZ(maxi(0,x(r))), iA1=p.column2thisOrPreviousIndex(x2(r));
            if (iA0<0 || iA1<0) continue;
            for (ResidueSelection rs : p.residueSelectionsAt(VisibleIn123.ARROW_HEADS, iA0, iA1, VisibleIn123.ANYWHERE)) {
                if (rs!=null && nam(rs)!=ResidueSelection.NAME_CURSOR) v.add(rs);
            }
        }
        return toArry(v,ResidueSelection.NONE);
    }
    ResidueSelection[] residueSelectionsAtXY(int x, int y,int where) {
        return residueSelectionsAt( y2row(y), x2col(x), where);
    }
    ResidueSelection[] residueSelectionsAtXYstrict(int x, int y) {
        final ResidueSelection[] ss=residueSelectionsAtXY(x,y, VisibleIn123.SEQUENCE);
        ResidueSelection[] ssNeu=null;
        final int charH=charB().height;
        for(int i=sze(ss); --i>=0;) {
            final VisibleIn123 a=get(i,ss, VisibleIn123.class);
            final int style=a==null ? 0 : a.getStyle();
            if (style==0) continue;
            final int uToleranz=(style&255)!=VisibleIn123.STYLE_UNDERLINE ? 0 : ss.length>1 ? 2 : charH/2;
            final int lToleranz=(style&255)!=VisibleIn123.STYLE_UNDERLINE ? 0 : 1;
            final int dy=y-row2y(y2row(y)), yT=ResSelUtils.yT(style, charH), h=ResSelUtils.height(style, charH);
            if (uToleranz+dy<yT || dy>yT+h+lToleranz)  (ssNeu==null ? ssNeu=ss.clone() : ssNeu)[i]=null;
        }
        return ssNeu==null?ss : rmNullA(ssNeu, ResidueSelection.class);
    }
    ResidueSelection[] residueSelectionsAt(int row, int col,int where) {
        final Object[] cached=cached();
        final Protein p=proteinInRow(row);
        if (p==null) return ResidueSelection.NONE;
        final long mc=Protein.MC_GLOBAL[MCA_ALIGNMENT]+ p.mcSum(MC_RESIDUE_SELECTIONS, MC_SELECTED_AA)+((long)row<<48)+((long)col<<32);
        ResidueSelection ss[]=(ResidueSelection[])cached[MCSV_RESSEL_RECT];
        if (ss==null || CACHE_MC[MCSV_RESSEL_RECT]!=mc) {
            CACHE_MC[MCSV_RESSEL_RECT]=mc;
            final int iA=p.column2indexZ(col);
            cached[MCSV_RESSEL_RECT]=ss=p.residueSelectionsAt(VisibleIn123.ARROW_HEADS, iA,iA+1, where|VisibleIn123.NO_FLASH);
        }
        return  ss;
    }
    static void selectResidues(boolean toClipbrd,  boolean to3D, Protein protein,  int col1,  int col2, boolean[] selRes, StrapView view0) {
        final StrapView view=view0!=null?view0:StrapAlign.alignmentPanel();
        if (view==null) return;
        final BasicResidueSelection s=view._resSel;
        final Protein oldP=sp(s), p=protein!=null?protein : oldP;
        if (p==null) return;
        s.setVisibleWhere(s.getVisibleWhere()|VisibleIn123.SEQUENCE);
        p.addResidueSelection(s);
        s.setProtein(p);
        final int idx0=Protein.firstResIdx(p);
        boolean changed=protein!=oldP;
        if (selRes==null) {
            final int colMax=mini(p.getMaxColumnZ(), maxi(col1,col2)-1);
            changed|=BasicResidueSelection.selectRange(idx0+p.column2nextIndexZ(mini(col1,col2)), idx0+p.column2nextIndexZ(colMax)+1, s);
        } else changed|=BasicResidueSelection.changeSelectedAA(selRes, col1, s);
        if (oldP!=p && oldP!=null) oldP.removeResidueSelection(s);
        if (changed) {
            final boolean sel[]=s.getSelectedAminoacids();
            final int offset=ResSelUtils.selAminoOffsetZ(s);
            if (countTrue(sel)==0) p.removeResidueSelection(s);
            if (toClipbrd) {
                final int from=fstTrue(sel), to=1+lstTrue(sel);
                if (from<to) toClipbd(toStrg(p.getResidueTypeExactLength(),offset+from,offset+to));
            }
            for(ProteinViewer pv : p.getProteinViewers()) {
                if (to3D) V3dUtils.residueSelectionsTo3D(0L, new ResidueSelection[]{s}, new ProteinViewer[]{pv});
            }
            StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_CHANGED);
        }
    }
    /* <<< ResidueSelection <<< */
    /* ---------------------------------------- */
    /* >>> Menu >>> */
    private JPopupMenu _jpm, _jpmR;
    JPopupMenu getPopup(boolean region){
        if (region) {
            if (_jpmR==null) {
                final Object
                    bNarrow=new ChButton("CROP").t("Crop amino acids sequences ...").li(this).i(IC_SCISSOR),
                    oo[]={
                    menuInfo(MI_HEADLINE,"Menu for rectangular region"),
                    null,null,
                    new Object[]{
                        "Export","i",IC_EXPORT,
                        new ChButton("CLIPBOARD").t("Copy to clipboard").li(this).i(IC_CLIPBOARD),
                        new ChButton("W").t("Export as alignment ...").li(this).i(IC_EXPORT)
                    },
                    new Object[]{
                        "Edit",
                        new ChButton("U").t("To upper case").li(this),
                        new ChButton("L").t("To lower case").li(this),
                        "-",
                        new ChButton("IG").li(this).t("Insert gaps"),
                        new ChButton("DG").li(this).t("Delete common gaps"),
                        new ChButton("DGA").li(this).t("Delete all gaps"),
                        "-",
                        bNarrow
                    },
                    new Object[] {
                        "Align", "i", IC_ALIGN,
                        new ChButton("A").t("Align rectangular region ...").i(IC_ALIGN).li(this),
                        "Note: Aligned residues outside the rubber band remain untouched."
                    },
                    " ",
                    CLASS_DialogPhylogeneticTree,
                    new ChButton("ANNO_R").t("Select residues").li(this).i(IC_SELECT),
                    "-",
                    new ChButton("RM_RECT").t("Remove rubber band rectangle").li(this).i(IC_CLOSE)
                };
                StrapAlign.replaceClassByButton(oo,null);
                _jpmR=jPopupMenu(0, "rect-region-contextmenu", oo);
            }
            return _jpmR;
        } else {
            if (_jpm==null) {
                final ChButton butSR[]=new ChButton[6];
                for(int i=butSR.length; --i>=0;) {
                    butSR[i]=new ChButton((i<3?"SAVE":"RESTORE")+(i%3)).t((i<3?"Save":"Restore")+" cursor position "+(i%3+1)).li(this);
                }
                final Object oo[]={
                    menuInfo(MI_HEADLINE,"Menu for the alignment window"),
                    null,null,
                    new Object[]{
                        "^New ", "i", IC_ADD,
                        StrapAlign.button(StrapAlign.BUT_NEW_PANEL),
                        StrapAlign.button(StrapAlign.BUT_NEW_STRAP),
                        "-",
                        new ChButton("NEW_ANNO").t("Create residue selectio^n").i(IC_UNDERLINE).li(this),
                    },
                    new Object[]{
                        "^View", "i", IC_SHOW,
                        "&F",CLASS_DialogHighlightPattern,
                        new ChButton("FONT").t("Character size and ^font ...").li(this),
                        StrapAlign.button(StrapAlign.TOG_HIGHLIGHT_IDENTICAL_SEQUENCES),
                        StrapAlign.button(StrapAlign.TOG_HIDE_GAPS),
                        "-",
                        button(TOG_BRIGHTER),
                        buttn(TOG_ANTIALIASING),
                        "-",
                        helpBut(ChIcon.class).t("Protein icons ...").i("")
                    },
                    new Object[]{
                        "^Arrange proteins", "i", IC_SORT,
                        "h", CLASS_DialogManyInOneRow,
                        button(BUT_SORT)
                    },
                    new Object[]{
                        "^Alignment", "i", IC_ALIGN,
                        CLASS_DialogAlign,
                        button(TOG_PROTECT_COL)
                    },
                    new Object[]{
                        "^Cursor position",  "i", IC_TEXT_CURSOR,
                        butSR[0],butSR[1],butSR[2],"-",butSR[3],butSR[4],butSR[5]
                    },
                    "-",
                    new ChButton("CLOSE").t("^Close alignment panel").i(IC_CLOSE).li(this)
                };
                StrapAlign.replaceClassByButton(oo,null);
                _jpm=jPopupMenu(0, "Alignment-Contextmenu", oo);
            }
            return _jpm;
        }
    }
    /* <<< Menu <<< */
    /* ---------------------------------------- */
    /* >>> Cursor  >>> */
    public int cursorAminoAcid() { return _cursorAA; }
    public int cursorColumn() {
        if (!isEDT()) return _cursorCol;
        final Protein p=cursorProtein();
        return p==null ? 0  : p.getResidueColumnZ(_cursorAA);
    }
    public int cursorRow() { return findRow(_cursorP); }
    public Protein cursorProtein() { return _cursorP;}
    private final Rectangle _rectCursor[]={new Rectangle(), new Rectangle(), new Rectangle(), new Rectangle()};
    Rectangle rectCursor(Component jc) { return rowCol2rect(cursorRow(), cursorColumn(),jc, null); }
    Rectangle rowCol2rect(int row, int col, Component jc, Rectangle rect) {
        final int nR=countRows(),  nC=countColumns();
        int x=0, y=0, w=0, h=0;
        if (row>=0 && nR>0 && nC>0 && jc!=null) {
            if (jc==_hSB) {
                final Rectangle track=_hSB.getTrack();
                final int th=hght(_hSB)-_hSB.secStruH();
                x=track.width *col / nC + x(track);
                y=th*row / nR + y(track);
                w=maxi(2, track.width /nC);
                h=maxi(1, th/nR);
            }
            if (jc==_vSB) {
                final Rectangle track=_vSB.getTrack();
                x=2;
                y=y(track)+track.height*row/nR;
                w=_vSB.getWidth()-4;
                h=maxi(2,track.height/nR);
            }
            if (jc==_pC) {
                x=col2x(col);
                y=row2y(row);
                w=col2x(1);
                h=row2y(1);
            }
            if (jc==_rh) {
                x=0;
                y=row2y(row);
                w=999;
                h=row2y(1);
            }
        }
        final Rectangle r;
        if (rect!=null) setRect(x,y,w,h, r=rect);
        else {
            r=_rectCursor[jc==_hSB?0:jc==_vSB?1:jc==_pC?2:3];
            setRectAndRepaint(x,y,w,h,  r, jc);
        }
        return r;
    }
    private void repaintCursor(boolean bright) {
        if (_cursorP==null) return;
        _cursorBright=bright;
        _resSelCursor.setColor(bright ? C(0xFFffFF) : _colorCur);
        _pC.repaint(rectCursor(_pC));
        _vSB.repaint(rectCursor(_vSB));
        _hSB.repaint(rectCursor(_hSB));
        _rh.repaint(rectCursor(_rh));
    }
    public final static int CURSOR_COLUMN=1<<1;
    public boolean setCursor(int opt, Protein p, int idxOrCol) { /* FIRST_AMINO_IS_IDX_0 */
        if (!isEDT()) {
            inEDT(thrdM("setCursor",this,new Object[]{intObjct(opt), p,intObjct(idxOrCol)}));
            return true;
        }
        if (0!=(CURSOR_COLUMN&opt)) {
            final int iA=p==null?-1: maxi(0, p.column2nextIndexZ(mini(p.getMaxColumnZ(), maxi(0, idxOrCol))));
            return setCursor(opt&~CURSOR_COLUMN, p, iA);
        }
        final int row=findRow(p), iA=p==null?-1:idxOrCol, col=p==null?-1:p.getResidueColumnZ(mini(p.countResidues()-1,maxi(0,iA)));
        int evType=iA==_cursorAA?0 : StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN;
        if (_cursorP!=p) {
            evType=StrapEvent.CURSOR_CHANGED_PROTEIN;
            if (p!=null) {
                Protein.incrementMC(MC_CURSOR_PROTEIN,p);
                p.addResidueSelection(_resSelCursor);
            }
            if (_cursorP!=null) {
                _cursorP.removeResidueSelection(_resSelCursor);
                Protein.incrementMC(MC_CURSOR_PROTEIN, _cursorP);
            }
        }
        final boolean changed=_cursorAA!=iA || _cursorP!=p;
        if (changed) {
            final int idx0=Protein.firstResIdx(p);
            BasicResidueSelection.selectRange(idx0+iA, idx0+iA+1, _resSelCursor);
        }
        if (changed ||_cursorCol!=col) {
            String t0="Protein ", t1="";
            if (p!=null) {
                t0+=(row+1);
                t1=toStrg(p.posAsTextZ(Protein.POS_PRINT_COLUMN, iA).aIfSze(2,"  concatenated chains: ", p.getChainsAsString()));
            }
            _tLayout.setTitle(COLh,t0);
            _tLayout.setTitle(COLs,t1);
        }
        _cursorAA=iA;
        _cursorCol=col;
        StrapAlign.PROTEIN_AT_CURSOR[0]=_cursorP=p;
        if (changed) repaintCursor(true);
        if (evType!=0) new StrapEvent(this,evType).setParameters(new Object[]{p, intObjct(iA),intObjct(row)}).run();
        return evType!=0;
    }
    public void saveRestore(boolean save, Object key) {
        if (save) {
            final int ia=cursorAminoAcid();
            final Protein p=cursorProtein();
            if (ia>=0 && p!=null) {
                mapSavedCursor.put(key,new Object[]{wref(p),intObjct(ia),new Rectangle(visibleXYWH())});
                StrapAlign.drawMessage("@1"+ANSI_GREEN+"Saved current position to "+key);
            }
            setEnabld(true, button(BUT_RESTORE_CURSOR));
        } else {
            final Object[] sc=mapSavedCursor.get(key);
            if (sc!=null) {
                setCursor(0, (Protein)deref(sc[0]), atoi(sc[1]));
                scrRectToVis((Rectangle)sc[2],false);
            }
            alignmentPane().requestFocus();
        }
    }
    /* <<< Cursor <<< */
    /* ---------------------------------------- */
    /* >>> Proteins >>> */
    boolean addProteins(int row0, Protein pp[]) {
        if (sze(pp)==0) return false;
        final int modi=mc(), nRows=countRows();
        final List<Protein[]> vRows=list();
        final Protein pp0[]=pp();
        for(int iP=0; iP<pp.length; iP++) {
            final Protein p=pp[iP];
            if (p==null  || !p.isInAlignment()  || idxOf(p,pp,0,iP)>=0 ) continue;
            final int N=countRows();
            final int row=mini(row0+(N-nRows),N);
            if (cntains(p,pp0)) {
                if (row0>=0) {
                    final int inRow=findRow(p);
                    final Protein[] v=proteinsInRow(inRow);
                    if (v!=null) {
                        if (inRow!=row) {
                            vRows.remove(v);
                            vRows.add(row,v);
                        }
                        addProteinToRow(true, p,mini(sze(vRows)-1,row));
                    }
                }
            } else if (p.OBJECTS[Protein.B_INTO_SAME_LINE]!=null && N>0 && addProteinToRow(false, p,N-1)) {
                p.OBJECTS[Protein.B_INTO_SAME_LINE]=null;
            } else {
                if (row0<0) vRows.add(spp(p)); else vRows.add(row,spp(p));
            }
        }
        if (modi!=mc()) {
            if (nRows>0 && pp.length<11)  highlightProteins(pp, COLOR_HL);
            inEDTms(thread(T_ENABLED),555);
            return true;
        }
        return false;
    }
    void oneLineManyProteins(Protein pVisible,Protein pp[]) {
        final Protein proteins[]=pp.clone();
        final int iLine=findRow(pVisible);
        if (iLine>=0) {
            for( int i; (i=idxOf(pVisible,proteins))>=0;) proteins[i]=null;
            removeProteins(proteins);
            removeProteins(pVisible);
            final Protein rowWithPVisible[]=proteinsInRow(iLine);
            list().remove(iLine);
            final Protein[] ppRow=set1stElement(pVisible, pp,0,Protein.class);
            list().set(iLine,ppRow);
            for(int i=rowWithPVisible.length; --i>=0;) addProtein(iLine,rowWithPVisible[i]);
            removeEmptyRows();
            setCursor(0,cursorProtein(), cursorAminoAcid());
            StrapEvent.dispatchLater(StrapEvent.PROTEINS_HIDDEN,111);
        }
    }
    private void updateIsInAli() {
        Collection<Protein> vAd=null, vRM=null;
        final Protein pp[]=pp();
        for(Protein p : StrapAlign.getProteins()) {

            if (p.isInAlignment()) {
                if (!cntains(p,pp)) (vAd==null ? vAd=new HashSet() : vAd).add(p);
            } else {
                if (cntains(p,pp))  (vRM==null ? vRM=new HashSet() : vRM).add(p);
            }
        }
        for(Protein p : pp) {
            if (!p.isInAlignment()) (vRM==null ? vRM=new HashSet() : vRM).add(p);
        }
        if (vRM!=null) { removeProteins(spp(vRM)); removeEmptyRows();}
        if (vAd!=null) addProteins(-1,spp(vAd));
        final int mc=mc();
        if (_inAliMC!=mc) {
            _inAliMC=mc;
            final int nRows=countRows();
            if (nRows>10) reduceHeight(_reduceHeight-charB().height);
            inEDTms(thread(T_ENABLED),111);
            for(int i=3; --i>=0;) ChDelay.revalidate(button(i==0 ? BUT_3D : i==1 ? BUT_NUCL : BUT_SEQVISTA),99);
            for(int i=5; --i>=0;) ((ChPanel)(i==0?_pC:i==1?_pR:i==2?_pS:i==3?_rh : _ri)).updateSize();
            setCursor(0, cursorProtein(), cursorAminoAcid());
        }
        showMinRows(2);
    }
    Protein proteinAtY(int y) { return get(y/maxi(1,_lineSkip+charB().height), visibleProteins(),Protein.class);}
    Protein[] proteinsAtY(int y) {
        final Protein p=proteinAtY(y), pp[]=StrapAlign.selectedProteins();
        if (p==null) return Protein.NONE;
        final int i=idxOf(p,pp);
        if (i<0) return spp(p);
        if (i==0) return pp;
        final Protein[] ppNew=pp.clone();
        ppNew[0]=p;
        ppNew[i]=pp[0];
        return ppNew;
    }
    /* ---------------------------------------- */
    Protein[] visibleProteinsReordered() {
        final int[] drag=draggedProtein2row();
        final Protein[] pp=visibleProteins();
        if (drag==null) return pp;
        if (_ppR==null || _ppR.length!=pp.length) _ppR=new Protein[pp.length];
        for(int i=drag.length; --i>=0;) {
            final int row=drag[i];
            if (row>=0 && row<pp.length && i<pp.length) _ppR[row]=pp[i];
        }
        return _ppR;
    }
    /* <<< Proteins <<< */
    /* ---------------------------------------- */
    /* >>> Rubber band >>> */
    private final Rectangle _rub=new Rectangle();
    private final static Rectangle rectRBA=new Rectangle(), RECT_EDT=new Rectangle();
    private Object[][] _rubP;
    private final static List _vAnts=new ArrayList();
    public void setRectRubberBand(int col, int row, int numCol, int numRow) {

        if (numRow<=0||numCol<=0) {
            setRect(0,0,0,0, _rub);
            _rubP=null;
        } else {
            final Object rp[][]=new Object[numRow][];
            for(int iP=numRow; --iP>=0;) {
                final Protein p=proteinInRow(iP+row);
                if (p==null) continue;
                final int i0=Protein.firstResIdx(p);
                final int col0=i0+p.column2nextIndexZ(col);
                final int col1=i0+p.column2thisOrPreviousIndex(col+numCol-1);
                if (0<=col0 && col0<=col1) {
                    rp[iP]=new Object[]{wref(p),new int[]{col0, col1}};

                }
            }
            _rubP=rp;
        }
        setEnabld(_rubP!=null, _vAnts);
        _rubProtMC++;
    }
    private boolean _zeroRB=true;
    private boolean setBndsRBA(int x, int y, int w, int h) {
        final boolean zero=w<1||h<1;
        if (_zeroRB!=zero) {
            _zeroRB=zero;
            StrapEvent.dispatchLater(StrapEvent.RUBBER_BAND_CREATED_OR_REMOVED,222);
        }
        if ( zero ? setRect(0,0,0,0, rectRBA) : setRect(x,y,w,h, rectRBA)) {
            repaintC(_pC);
            repaintC(_hSB);
            return true;
        }
        return false;
    }
    public Rectangle rectRubberBand() {
        if (ANSEW.indexOf(_whatDrag)>=0) return rectRBA;
        final Object[] cached=cached();
        final int mc=_rubProtMC+vROWS.mc()+Protein.MC_GLOBAL[MCA_ALIGNMENT];
        final Object[][] rp=_rubP;
        if (rp==null) {
            setRect(0,0,0,0, _rub);
            return null;
        }
        if (_rubMC!=mc) {
            _rubMC=mc;
            int rowMin=MAX_INT, rowMax=MIN_INT, colMax=MIN_INT, colMin=MAX_INT;
            for(Object[] row : rp) {
                final Protein p=get(0,row, Protein.class);
                if (p==null) continue;
                final int i0=Protein.firstResIdx(p);
                final int iRow=findRow(p);
                if (iRow<0) continue;
                final int ii[]=get(1,row,int[].class);
                final int col0=p.getResidueColumnZ(ii[0]-i0);
                final int col1=p.getResidueColumnZ(ii[1]-i0);
                if (col0>=0) {
                    if (col0<colMin) colMin=col0;
                    if (col0>colMax) colMax=col0;
                }
                if (col1>=0) {
                    if (col1<colMin) colMin=col1;
                    if (col1>colMax) colMax=col1;
                }
                rowMax=maxi(rowMax,iRow);
                rowMin=mini(rowMin,iRow);
            }
            final int w=maxi(0,colMax-colMin)+1;
            final int h=maxi(0,rowMax-rowMin)+1;
            final short[][] ccc=(short[][])cached[MCSV_CHARACTERS];
            for(int i=2; --i>=0;) {
                final Rectangle r=i==0?rectRBA : _rub;
                markDirtyRect(false,ccc, x(r)-1, y(r)-1, r.width, r.height);
                markDirtyRect(false,ccc, x(r)  , y(r)  , r.width, r.height);
                if (h<1 || w<1) setRect(0,0,0,0,r); else setRect(colMin, rowMin, w, h, r);
            }
            inEDTms(threadPR(PR_ONLY_CHANGES),99);
        }
        return _rub;
    }
    static Rectangle rectangle(StrapView view0) {
        final StrapView v=view0!=null ? view0 : StrapAlign.alignmentPanel();
        return v==null?null : v.rectRubberBand();
    }
    /* <<< Rubber Band <<< */
    /* ---------------------------------------- */
    /* >>> SecStru >>> */
    private ProteinCombo _secStruPC;
    private byte _secStruC[];
    private Protein _secStruP, _secStruP1;
    private int _secStruMC;
    void setSecStruP(Protein p) {
        _secStruP=p;
        if (_secStruPC!=null) _secStruPC.s(p);
        repaintC(_pS);
    }
    public Protein secStruP() {
        final Protein p=_secStruP;
        if (p!=null && p.isInAlignment() && p.isLoaded() && p.getResidueSecStrType()!=null) return p;
        final int mc=mc()+Protein.MC_GLOBAL[ProteinMC.MC_RESIDUE_SECSTRU]+Protein.MC_GLOBAL[ProteinMC.MCA_PROTEINS_V];
        Protein p1=_secStruMC!=mc ? null : _secStruP1;
        _secStruMC=mc;
        for(int vis=2; p1==null&&--vis>=0;) {
            for(Protein pr : vis!=0?visibleProteins():StrapAlign.proteins()) {
                if (pr.getResidueSecStrType()!=null && (p1==null||p1.countResidues()<pr.countResidues())) p1=pr;
            }
        }
        return _secStruP1=p1;
    }
    private boolean _secStruShown;
    private void paintSecStru(Graphics gAwt, int fromCol, int toCol, boolean onlyChanges) {
        final Protein p=secStruP();
        final byte[] ss=p==null?null:p.getGappedSecStru();
        final Graphics g=gAwt!=null?gAwt:_pS!=null?_pS.getGraphics() : null;
        if (g==null || ss==null) return;
        final int nCol=countColumns(), charW=wdth(charB());
        byte current[]=_secStruC;
        if (sze(current)<nCol) _secStruC=current=chSze(ss,nCol+99);
        final int from=maxi(p.getResidueColumnZ(0), fromCol), to=mini(nCol, toCol+1);
        Color cLast=null;
        final Color c0=C(DEFAULT_BACKGROUND), ch=C(0xFF0000), ce=C(0xb2b200);
        int debugCount=0;
        for(int col=from; col<to; col++) {
            final byte c=col<ss.length?ss[col]:0;
            if (!onlyChanges || current[col]!=c) {
                current[col]=c;
                final Color color=c=='H'?ch : c=='E'? ce : c0;
                if (cLast!=color) g.setColor(cLast=color);
                g.fillRect(col*charW,1,charW,SECSTRU_HEIGHT-2);
                debugCount++;
            }
        }
        //if (myComputer() && debugCount>0) puts(" SS"+debugCount);
    }
    /* <<< SecStru <<< */
    /* ---------------------------------------- */
    /* >>> Ruler >>> */
    private Protein _rulerP;
    private ProteinCombo _rulerPC;
    private ChButton _rulerResNum;
    private final Font _rulerF=new Font("",Font.PLAIN, 9);
    private final Rectangle _rulerCB=chrBnds(_rulerF);
    public Protein rulerP() {
        Protein p=_rulerP;
        if (p==null || !p.isInAlignment()) _rulerP=p=proteinInRow(0);
        return p;
    }
    private void paintRuler(Graphics gAwt, int fromCol, int toCol, boolean onlyChanges) {
        final Graphics g=gAwt!=null?gAwt:_pR.getGraphics();
        final Protein p=rulerP();
        if (g==null || p==null) return;
        final Object[] cached=cached();
        final int cols=countColumns(), ii[]=p.columns2indices(), from=maxi(0, p.getResidueColumnZ(0)-2, fromCol-2);
        final int nColP=mini(ii.length,p.getMaxColumnZ()+1), to=mini(toCol+2,cols);
        boolean changed=false;
        int current[]=(int[]) cached[RULER_NUM];
        if (sze(current)<=to) cached[RULER_NUM]=current=chSze(current,cols+99);
        for(int col=from; col<to; col++) {
            int ia=col<nColP?ii[col]:-1;
            if (ia<0 || ia%10!=0) ia=-1;
            if (current[col]!=ia) {
                changed=true;
                current[col]=ia;
            }
        }
        int debugCount=0;
        if (changed || !onlyChanges) {
            debugCount++;
            final int[] rn=isSelctd(_rulerResNum) ? p.getResidueNumber() : null;
            final byte[] chain=rn==null?null:p.getResidueChain();
            g.setColor(C(DEFAULT_BACKGROUND));
            fillBigRect(g, 0,0, MAX_INT,99);
            g.setColor(C(0));
            g.setFont(_rulerF);
            final BA sb=new BA(12);
            final int y=1+_rulerCB.y;
            for(int col=from; col<to; col++) {
                final int i=current[col];
                if (i>=0) {
                    if (rn!=null) {
                        sb.clr().a(get(i,rn)).a(':');
                        if (is(LETTR_DIGT,chain,i)) sb.a((char)chain[i]);
                    } else sb.clr().a(i+1+Protein.firstResIdx(p));
                    final String s=toStrg(sb);
                    g.drawString(s, col2x(col)-strgWidth(_rulerF, s)/2+charB().width/2, y);
                }
            }
        }
        //if (myComputer() && debugCount>0) puts(" SS"+debugCount);
    }
    /* <<< Ruler <<< */
    /* ---------------------------------------- */
    /* >>> Entropy >>> */
    private void paintEntropy(Graphics gAwt, int fromCol, int toCol, boolean onlyChanges) {
        final Graphics g=gAwt!=null?gAwt:_pS.getGraphics();
        final int cols=countColumns(), to=mini(toCol+1, cols), rows=countRows();
        final short[][] fff=frequenciesAtColumn(fromCol, to, 'a');
        if (g==null || rows==0) return;
        final int y=plotAreaH('S')+1, w=maxi(3,charB().width)-2;
        int debugCount=0;
        for(int col=maxi(0,fromCol); col<to; col++) {
            final short[] ff=fff[col];
            Color c=null;
            if (ff!=null) {
                final short e=ff[FrequenciesAA.ENTROPY];
                if (onlyChanges && e!=0 && e==ff[FrequenciesAA.ENTROPY_DRAWN]) continue;
                ff[FrequenciesAA.ENTROPY_DRAWN]=e;
                c=0==e?null : blueYellowRed((e-1)*0.0001f);
            }
            debugCount++;
            g.setColor(c!=null?c:C(DEFAULT_BACKGROUND));
            g.fillRect(col2x(col)+1, y, w, SECSTRU_HEIGHT-2);
        }
        //if (myComputer() && debugCount>0) puts(" EE"+debugCount);
    }
    /* <<< Entropy <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */
    public void dropObjects(Object[] oo, Map mapTargets) {
        final Component target=(Component)mapTargets.get(Component.class);
        for(Object o : oo(oo)) {
            final Protein p=deref(o,Protein.class);
            if (target==_pR && p!=null) {
                _rulerP=p;
                repaintC(_pR);
                if (_rulerPC!=null) _rulerPC.s(p);
                break;
            }
            if (target==_pS && p!=null && p.getResidueSecStrType()!=null) {
                _secStruP=p;
                if (_secStruPC!=null) _secStruPC.s(p);
                repaintC(_pS);
                repaintC(_hSB);
                break;
            }
        }
    }
}
