package charite.christo.strap;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.ResidueAnnotation.*;
import static charite.christo.ChUtils.*;
/**
Choice menu for keys of residue selections.
   @author Christoph Gille
*/
public class ComboKeys extends javax.swing.DefaultComboBoxModel {

    public ComboKeys(boolean all) { mainKeys=all ? MAIN_KEYS : new Object[0]; }
    @Override public int getSize() {
        return
            OTHER_KEYS.length+
            AddAnnotation.customizeKeys().getSettings().length+
            mainKeys.length;
    }
    private final Object[] mainKeys;
    @Override public Object getElementAt(int i) {
        final Object[] oo2=AddAnnotation.customizeKeys().getSettings();
        final int n1=OTHER_KEYS.length, n2=oo2.length, n3=mainKeys.length;
        return
            (i<n1)     ? OTHER_KEYS[i] :
            (i-=n1)<n2 ? oo2[i] :
            (i-=n2)<n3 ? mainKeys[i] :
            "error";
    }

}
