package charite.christo.strap;
/**HELP

WIKI:Secondary_structure  are predicted from the amino acid sequences.

The predicted WIKI:Alpha_helices and WIKI:Beta_sheets are highlighted in the alignment.
<br>

Also see: http://genamics.com/expression/strucpred.htm  http://www.life.uiuc.edu/z-huang/seqanalysis.html
   @author Christoph Gille
*/
public class DialogPredictSecondaryStructures extends AbstractDialogPredict {
    {
        init(this,  charite.christo.protein.SecondaryStructure_Predictor.class, "secondary structure elements");
    }
}

// http://npsa-pbil.ibcp.fr/cgi-bin/npsa_automat.pl?page=npsa_sopma.html
