package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;

import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class EditDnaTable extends javax.swing.table.DefaultTableModel implements java.awt.event.ActionListener,StrapListener  {
    private final Protein _p;
    private final ChJTable _jt=new ChJTable(this,0);
    private final ChTextField _tf=new ChTextField("").li(this)
        .tt("An expression describing the intron-exon structure.<br>For example the following expression defines two exons:<pre>11..102,304..420</pre>.");
    private final ChButton _cbCompl=toggl(ChButton.ICON_SIZE,"R").li(this)
         .t("Reverse").tt("Interpret CDS string as reverse");
    private final Object _labProt;
    private int _ft[], _ftMc=-1, _mcEv;
    public EditDnaTable(Protein p) {
        _labProt=new ProteinLabel(0L,_p=p);
        setColumnCount(5);
        _jt.setColWidth(false, 0,ChJTable.COLUMN_WIDTH_4_DIGITS);
        setTextField();
    }
    public Object getPanel() {
        final Object
            title=pnl("Exons of ",_labProt),
            b=new ChButton("Apply").li(this).tt("Apply the exon-intron expression in the textfield");
        return pnl(CNSEW, scrllpn(_jt),title,pnl(HB,_tf,null,null,_cbCompl.cb(),b));
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> TableModel >>> */
    public boolean isCellEditable(int row,int col) { return col>0;}
    public String getColumnName(int i) {
        final int N=_p.countNucleotides();
        final BA sb=new BA(22);
        if (i==0) sb.a('#');
        else if (i==1) sb.a("Start");
        else if (i==2) sb.a("End");
        else sb.a(N).a('-').a(i==3?"Start":"End").a('+').a(1);
        return sb.toString();
    }
    public Class getColumnClass(int col){ return  Integer.class;}
    public int getRowCount(){ return _p==null?0:exonsFT().length/2;}
    public int v(int row,int col) {
        final int ft[]=exonsFT(), N=_p.countNucleotides();
        return
            col==0 ? row+1 :
            col==1 ? ft[2*row]+1 :
            col==2 ? ft[2*row+1] :
            col==3 ? N-ft[2*row] :
            col==4 ? N-ft[2*row+1]+1 :
            -1;
    }
    public Object getValueAt(int row,int col) { return intObjct(v(row,col)); }
    public void setValueAt(Object o, int row, int col) {
        boolean bb[]=_p.isCoding();
        final int ft[]=exonsFT();
        if (2*row+1>=ft.length) return;
        final int oldF=ft[2*row], oldT=ft[2*row+1], v=atoi(o), N=_p.countNucleotides();
        for(int i=maxi(0,oldF); i<oldT && i<bb.length; i++) bb[i]=false;
        int f=oldF,t=oldT;
        if (col==1) f=v-1;
        if (col==2) t=v;
        if (col==3) f=N-v;
        if (col==4) t=N-v+1;
        if (sze(bb)<N) bb=chSze(bb,N);
        for(int i=maxi(0,f); i<t && i<bb.length; i++) bb[i]=true;
        _p.setCoding(bb);
        StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
        StrapEvent.dispatch(StrapEvent.ALIGNMENT_CHANGED);
    }
    /* <<< TableModel <<< */
    /* ---------------------------------------- */
    /* >>> Exons >>> */
    private int[] exonsFT() {
        final boolean[] T=_p.isCoding();
        final int N=_p.countNucleotides();
        final int mc=_p.mc(ProteinMC.MC_CODING);
        int ft[]= _ft;
        if (_ftMc!=mc || ft==null) {
            _ftMc=mc;
            if (T==null) {
                ft=chSze(ft,2);
                ft[0]=0;
                ft[1]=N;
            } else {
                int count=0;
                boolean prev=false;
                for(int i=0; i<=N && i<=T.length; i++) {
                    final boolean t=i<T.length?T[i]:false;
                    if (prev!=t) {
                        if (sze(ft)<=count) ft=chSze(ft,count+10);
                        ft[count++]=i;
                    }
                    prev=t;
                }
                ft=chSze(ft, count);
            }
            _ft=ft;
        }
        return ft;
    }

    /* <<< Exons <<< */
    /* ---------------------------------------- */
    /* >>> Change >>> */
    private void setTextField() {
        final int ft[]=exonsFT(), N=_p.countNucleotides();
        final BA sb=new BA(99);
        if (_cbCompl.s())
            for(int i=ft.length/2; --i>=0;) sb.a(N-ft[2*i+1]+1).a("..").a(N-ft[2*i]).a(',',i==0?0:1);
        else {
            for(int i=0; i<ft.length/2; i++) sb.a(',',i==0?0:1).a(ft[2*i]+1).a("..").a(ft[2*i+1]);
        }
        revalAndRepaintC(_tf.t(sb));
    }

    private void setT(boolean bb[]) {
        _p.setCoding(bb);

        StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
        StrapEvent.dispatch(StrapEvent.ALIGNMENT_CHANGED);
        ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,_jt,222,null);
        setTextField();
    }
    /* <<< Change <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==_cbCompl) setTextField();
        if (cmd=="Apply" || cmd==ACTION_ENTER && q==_tf){
            final int N=_p.countNucleotides();
            boolean bb[]=parseSet(rplcToStrg("..","-", toStrg(_tf)), N,-1);
            if (_cbCompl.s()) reverseArray(bb=chSze(bb,N), 0, MAX_INT);
            setT(bb);
            setTextField();
        }
    }
     public void handleEvent(StrapEvent ev){
        final int t=ev.getType();
        if (t==StrapEvent.NUCL_TRANSLATION_CHANGED) {
            _p.isCoding();
            final int mc=_p.mc(ProteinMC.MC_CODING);
            if (_mcEv!=mc) {
                _mcEv=mc;
                ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,_jt,222,null);
            }
        }
        if ( (t&StrapEvent.FLAG_ROW_HEADER_CHANGED)!=0) revalAndRepaintCs(_labProt);
    }
    /* <<< Event <<< */

}
