package charite.christo.strap;
import charite.christo.protein.*;
import java.awt.*;
import static java.awt.Color.*;
import static charite.christo.ChUtils.*;
// see http://www-ab.informatik.uni-tuebingepann.de/software/welcome_en.html MACAW
/**
   A gapped sequence with colorshading is drawn.
   Not used for the main alignment view in Strap.
   It is used for the Dialoges {@link DialogAlign DialogAlign} and {@link DialogAlignOneToAll DialogAlignOneToAll}.
   @author Christoph Gille
*/
public class DrawGappedSequence  {
    public final static int SHADE_SEC_STRU=1, AA2NT=2, NUCLEOTIDES=4;
    private final static Color
        _ss2colorBright[]=ShadingAA.colors(ShadingAA.SECSTRU,false),
        _ss2colorDark[]=darkerColors(_ss2colorBright),
        COLOR_NOT_CONSERVED=new Color(190,190,190);
    private Rectangle _cb;
    private Font _font;
    private short _frequen[][];
    private static int _minFreq;
    private static DrawGappedSequence _inst;
    public static DrawGappedSequence instance() {
        if (_inst==null) _inst=new DrawGappedSequence();
        return _inst;
    }

/**
       Draws a gapped sequence into the graphics context.
       @param component  any component and is used for the management of images  of
       selections (ImageObserver).  May be null.
       @param origin point where to start to draw,
       @param shadeAminos An array of Color of length 256. E.g. Alanin will be
       drawn in the color shadeAminos['A'],  May be null.
*/

    public void draw(Component component,Graphics g,Point origin, Protein p, Color[] shadeAminos, long mode) {
        draw(component,g,origin, 0, p.getGappedSequence(),0,p.getMaxColumnZ(),p,shadeAminos,mode);
    }

    public void draw(Component component, Graphics gAwt,Point origin, int idxOfFirstAA, byte[] seq, int fromCol, int toCol, Protein p, Color[] shadeAminos, long mode) {
        final Graphics2D g=(Graphics2D)gAwt;
        if (seq==null && p==null) return;
        final byte[] secStru=(mode&SHADE_SEC_STRU)!=0 && p!=null ? p.getResidueSecStrType() : null;
        final Font font=g.getFont();
        if (_cb==null || _font!=font) { _font=font;_cb=chrBnds(font);}
        final int charA=y(_cb), charW=_cb.width,  charH=_cb.height;
        if (charW==0) return;
        final int n=seq.length, threshold=_minFreq;
        final Rectangle clip=clipBnds(g);
        if (y(clip)>y(origin)+charH || y2(clip)<y(origin)) return;
        int colStart=(x(clip)-x(origin))/charW;
        int colEnd=mini((x2(clip)-x(origin))/charW+1,n);
        colEnd=mini(toCol, colEnd,seq.length);
        if (colStart<fromCol) colStart=fromCol;
        final Color colorFG=isWhiteBG() ? BLACK : WHITE;
        final ResidueSelection residueSelections[]=p!=null ? p.allResidueSelections() : null;
        final Rectangle RECT=new Rectangle(0,0,charW,charH);
        final byte[] selectedAA=p!=null ? p.selAminos() : NO_BYTE;
        final String strgLen1[]=strgsOfLen1();
        for(int col=0, iA=idxOfFirstAA;col<colEnd;col++) {
            final int iAmino=(mode&AA2NT)!=0 ? iA/3: (mode&NUCLEOTIDES)!=0 ? get(iA,p.toCodingPositions())/3 : iA;
            final byte c=seq[col];
            final int a=c|32;
            if (a<'a' || a>'z') continue;
            //if (idxOfFirstAA>0 && col<20) putln("col="+col+" iAmino="+iAmino+"  "+(char)secStru[iAmino]);
            //if (idxOfFirstAA>0) putln("col="+col+" iAmino="+iAmino);
            if (col>=colStart) {
                final short ff[]=_frequen!=null && _frequen.length>col ? _frequen[col]:null;
                boolean cons=false;
                if (ff!=null) {
                    final int total=ff[FrequenciesAA.TOTAL], t100=threshold*total/100;
                    cons=threshold>=0 ? ff[a-'a']>=t100 : ff[a-'a']<=total+t100;
                }
                final Color color=secStru!=null && secStru.length>iAmino ?
                    (cons ? _ss2colorBright : _ss2colorDark)[secStru[iAmino]] :
                    (cons ? shadeAminos==null ? WHITE : shadeAminos[a] : COLOR_NOT_CONSERVED);
                setRect(x(origin)+charW*col, y(origin),charW,charH, RECT);
                if (0<iAmino && iAmino<selectedAA.length && selectedAA[iAmino]!=0) {
                    ResSelUtils.drawResidueSelection(Protein.AMINO_ACIDS,c,p,residueSelections,iAmino,g,RECT,_cb);
                }
                g.setColor(WHITE==color || color==null ? colorFG : color);
                g.drawString(strgLen1[c&127],x(RECT),y(RECT)+charA);
            }
            iA++;
        }
    }

    public void setConservation(short frequenciesOfAminos[][], int minFrequency) {
        _frequen=frequenciesOfAminos;
        _minFreq=minFrequency;
    }
   public void drawBlastMidline(byte b1[],byte b2[], int colFrom, int colTo, byte[][] matrix, Graphics g,int x0, int y0) {
        final byte[][] M=matrix!=null ? matrix : charite.christo.protein.Blosum.BL2SEQ_MIDLINE;
        final Rectangle cb=chrBnds(g.getFont());
        final int charW=cb.width;
        final String strgLen1[]=strgsOfLen1();
        final int y=y0+y(cb), to=mini(colTo, b1.length, b2.length);
        for(int col=colFrom,x=x0+colFrom*charW; col<to; col++,x+=charW) {
            final byte mid=M[b1[col]&127][b2[col]&127];
            if (mid!=' ') g.drawString(strgLen1[mid],x,y);
        }
    }
}
