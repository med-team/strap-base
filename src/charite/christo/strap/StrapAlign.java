package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
                                         
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.net.URL;
import java.io.File;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.dnd.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.ProteinMC.*;
import static charite.christo.protein.Protein.MC_GLOBAL;
import static charite.christo.strap.SPUtils.*;
/**
   The root class of the alignment from which all information is accessible. For each Strap session exactly one instance of this class exists.

   <ul>
   <li><i>JAVADOC:StrapAlign#proteins()</i>: The proteins loaded in Strap can be obtained with this method. <i>JAVADOC:Protein</i> is the object for proteins.</li>
   <li><i>JAVADOC:StrapAlign#selectedProteins()</i>: This method returns  only the selected proteins.</li>
   </ul>

   @author Christoph Gille
   IOException: Bad file descriptor
*/
public class StrapAlign implements ProcessEv, ChRunnable, DropTargetListener, PaintHook {

    public final static String
        OBJECT_JLIST="StAl$$1",
        VIEW_SB="SB",
        ACTION_SORT="StAl$$4",
        ACTION_newPan="New alignment panel",
        ACTION_ignoreSEQRES="Ignore \"SEQRES\" lines in PDB-files",
        ACTION_openPlugin="Standard plugins ...",
        CMD_DEL_ICONS="Delete icons of selected proteins",
        CMD_PDB_ICONS="Load image icons from pdb-site",
        CMD_SPECIES_ICONS="Load species pictogramms",
        CMD_TABLE_ICONS="List protein icons",
        CMD_ALL_MENUS="Menubar: List of all menu items",
        CMD_ALL_POPUP="Context-menus: List of all menu items",
        KOPT_DELETE_DISABLED="StAl$$5",
        KEY_DAS="StAl$$3",
        LABEL_ALL_OR_SELECTED_P="A_or_SEL_pp",
        RUN_OPEN_DIALOG_HOOK="SA$$ROD",
        RUN_CHECK_PLUGIN_UPTODATE="SA$$CPUD";
    private final static Object SYNC_CPY=new Object(), KEY_TOKENS=new Object(), KEY_ADD_HET=new Object(), KEY_DC=new Object(),
        KOPT_DROP_TARGET_IS_CURRENT_PV=new Object();

    private final static Collection
        vPluginNotUpToDate=new HashSet(),
        vDragFailed=new HashSet();
    private static StrapView _alignFokus;
    private final static List<ChRunnable> _vRepaintCursor=new Vector();
    private final static List _vHlProteins=new Vector(), _vDT=new Vector(), _vLis=new Vector(),  _vNumPP=new Vector();

    private final static int
        _msfCountP[]={0},
        OPTION_NOT_LOOKING_FOR_REFS_IN_WEBPAGES=1<<0,
        DROP_HETERO_INVERSE_MX=1<<1;
    public final static int
        ANIM_SET_CURS_SCROLL_TO_VISIBLE=1<<2,
        SORT_WEB=1<<1, SORT_ALL=1<<3, SORT_PREFORDER=1<<4,
        OPTION_SKIP_PDB_IF_ALREADY_LOADED=1<<12, OPTION_RENAME_SWISS=1<<13,
        OPTION_USE_UNIPROT_SOAP=1<<14,   OPTION_DOWNLOAD_ORIGINAL_PROTEINS=1<<15, OPTION_SCROLL=1<<16,
        OPTION_notSplitChains=1<<17,
        OPTION_PROCESS_LOADED_PROTS=1<<20, OPTION_EVENT_PROTEINS_ADDED=1<<21, OPTION_SHOW_ORPHAN_HETEROS_IN_3D=1<<22,
        TOG_MAXIMIZE_DIALOGS=1, TOG_HIGHLIGHT_IDENTICAL_SEQUENCES=2, TOG_SKIP_IDENTICAL_SEQUENCE=3,
        TOG_HIDE_GAPS=4, TOG_IN_NEW_FRAME=5, TOG_MULTI_MBARS=8, TOG_TUTORIAL_SIMUL_MENUS=9,

        BUT_STOP_ALL=11, BUT_SAVE=12, BUT_SELECT_BY_NAME=13, BUT_UPDATEUI=14, BUT_NEW_PANEL=15, BUT_NEW_STRAP=16,
        BUT_hiddenP=17, BUT_ZIP=18,
        BUT_WATCH_EV=19,
        BUT_LIST_EV=20, BUT_DRAG_OPTS=21;
    private final static long _selMC[]=new long[128];
    private final static ChButton buttons[]=new ChButton[99];
    private static Container _panRight;
    private static JSplitPane _splitLR;
    private static Object _taNotes, _freeMem, _vSelProtO[]={null}, _dragOpts;
    private static int _reloadProts, _shoppingBusy, _parsersMc, _splitLRv, _selvMC;
    private static boolean _initialized, _shoppingBusyLast, _unsaved, _isOtherMB;
    private static long _selPmc, _selSmc, _whenCursor;
    private static StrapAlign _inst; { if (_inst!=null) assrt(); _inst=this;}
    private static EvAdapter _li;
    private static Component _tool, _toolParent, _zipPrj, _mb[],  _pmItems[];
    private static JMenu _pMenu;
    private static JMenuBar _mbb[], _mb2;

    /* ---------------------------------------- */
    /* >>> Instance >>> */
    public static StrapAlign getInstance() { return _inst;}

    public StrapAlign() {
        StrapEvent.setProteinAlignmentForAll(_inst=this);
        addActLi(li(),ChStdout.class);
        addActLi(li(),FilePopup.instance());
    }
    public static EvAdapter li() {
        final StrapAlign i=getInstance();
        if (i==null) { stckTrc(); return null; }
        if (i._li==null) {
            ChTextComponents.addLiDb(i._li=new EvAdapter(i));
        }
        return i._li;
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Collections >>> */
    private static UniqueList _vSel, _vPanels, _vP, _vPAli, _vPnotAli;
    public static UniqueList<Protein> vProteins() {
        if (_vP==null) _vP=new UniqueList(Protein.class).t("Loaded Proteins").addOptions(UniqueList.ASSERT_EDT|UniqueList.USE_HASH_SET);
        return _vP;
    }
    public static UniqueList<Protein> ppNotAliV() {
        if (_vPnotAli==null) _vPnotAli=new UniqueList(Protein.class).t("Hidden proteins").addOptions(UniqueList.ASSERT_EDT|UniqueList.USE_HASH_SET);
        return _vPnotAli;
    }
    public static UniqueList<Protein> ppAliV() {
        if (_vPAli==null) _vPAli=new UniqueList(Protein.class).t("Displayed Proteins").addOptions(UniqueList.ASSERT_EDT|UniqueList.USE_HASH_SET);
        return _vPAli;
    }

    public static UniqueList<StrapView> alignmentPanelsV() {
        if (_vPanels==null) _vPanels=new UniqueList(StrapView.class);
        return _vPanels;
    }

    public final static List selectedObjectsV() {
        return _vSel==null ? _vSel=new UniqueList(Object.class).t("vSELECTED").addOptions(UniqueList.USE_HASH_SET) : _vSel;
    }
    /* <<< Collections <<< */
    /* ---------------------------------------- */
    /* >>> >>> */
    private static void vProteinsAddOrRm(Protein p, boolean add) {
        if (p==null) return;
        if (add) adNotNull(p, vProteins());
        else remov(p, vProteins());
        p.setLoaded(add);
    }
    public final static long selectedObjectsMC(char type) {
        final Object oo[]=oo(selectedObjectsV());
        final int mc=modic(selectedObjectsV());
        if (_selvMC!=mc) {
            _selvMC=mc;
            Arrays.fill(_selMC,0);
            for(Object o : oo) {
                final char t=o instanceof Protein ? 'P' : o instanceof ResidueSelection ? 's' : 0;
                if (t!=0) {
                    final long inc=o.hashCode()+(1L<<40L);
                    _selMC[t]+=inc;
                    if (t=='s')_selMC[ResSelUtils.type(o)]+=inc;
                }
            }
        }
        return _selMC[type];
    }
    public static ChButton button(String s) { return new ChButton(s).li(li()); }
    public static ChButton button(int i) {
        if (buttons[i]==null) {
            String t=null, b=null, tt=null, save=null, ic=null;
            boolean li=true, sel=false;
            if (i==TOG_TUTORIAL_SIMUL_MENUS) { t="Simulate menubar for menu-items in tutorials"; sel=true; }
            if (i==TOG_MULTI_MBARS) { t="Display the menubar of the 3D viewer and of Strap at the same time"; li=sel=true; }

            if (i==TOG_MAXIMIZE_DIALOGS) { ic=IC_FIT_TO_WIDTH; t=""; tt="Maximize the dialog pane"; }
            if (i==TOG_HIGHLIGHT_IDENTICAL_SEQUENCES) {
                t="Highlight adjacent rows with identical sequence";
                save="identical_seq:=";
            }
              if (i==TOG_SKIP_IDENTICAL_SEQUENCE) {
                t="Do not load duplicate sequence";
                save="noIdentSeq";
                li=false;
            }
            if (i==TOG_HIDE_GAPS) {
                t="Show empty alignment columns";
                tt="Hide or show superfluous gaps occurring in all proteins at the same column ";
            }
            if (i==TOG_IN_NEW_FRAME) {
                t="Open next proteins in a separate Strap session";
                li=false;
            }
            if (i==BUT_STOP_ALL) b="Stop all running alignment computations";
            if (i==BUT_SAVE) {
                b="Save";
                tt="Saves the gaps and annotations of the alignment for each protein in a separte file.<br>"+
                    "Protein files are not saved since they not modified by Strap.";
                ic=IC_SAVE_AS;

            }
            if (i==BUT_hiddenP) b="Access proteins that are not in the alignment";
            if (i==BUT_ZIP) { b="Save as zip ..."; ic=IC_ZIP; }
            if (i==BUT_SELECT_BY_NAME) { b="^Select by typing the names ..."; ic=IC_SELECT;}
            if (i==BUT_UPDATEUI)  { b="Rescue graphical user interface"; tt="Resolve rare cases of broken menus, buttons or other elements of the user interface."; ic=IC_RESCUE; }
            if (i==BUT_NEW_STRAP) { b="New Strap instance"; tt="New independent alignment panel ..."; }
            if (i==BUT_NEW_PANEL) { b=ACTION_newPan; tt="All alignment windows display the same alignment. <br>Close with Ctrl+W"; }
            if (i==BUT_LIST_EV) { b="List of event listeners (StrapListener)"; }
            if (i==BUT_DRAG_OPTS) { b="Drag options \u25bc"; }
            if (i==BUT_WATCH_EV) { b="Watch Strap events (StrapEvent)"; }
            if (t!=null || b!=null) {
                final ChButton but=buttons[i]= (b!=null?new ChButton(b).i(ic) : toggl(t).s(sel)).tt(tt);
                if (li) but.li(li());
                if (save!=null) but.save(StrapAlign.class,save);
            }

        }
        return buttons[i];
    }
    public static ChButton butSetDefault(Class interf, String clazz) {
        if (interf==null || clazz==null || StrapPlugins.isDeactivated(clazz)) return null;
        final String
            shrt=lstCmpnt(clazz,'.'),
            info=
            clazz==CLASS_Superimpose_TM_align  ? "(Fortran, very fast, little memory)" :
            clazz==CLASS_Superimpose_native_CE ? "(C++, system  dependent)" :
            clazz==CLASS_Superimpose_CEPROXY   ? "(Java  slow, high memory requirement, system independent)" :
            "",
            t=delSfx("PROXY",delPfx("Superimpose_", shrt)) +"  "+info,
            para0=
            interf==SequenceAlignerTakesProfile.class ? "-alignerP=" :
            interf==SequenceAligner3D.class ? "-a3d=" :
            interf==Superimpose3D.class     ? "-s3d=" :
            interf==ProteinViewer.class     ? "-v3d=" :
            interf==SequenceAligner.class   ? "-aligner=" :
            "-default"+lstCmpnt(interf.getName(),'.')+"=",
            para=para0+shrt;
        return new ChButton(t).li(li()).cp(KEY_DC, para).tt("Or use program parameter <pre>"+para+"</pre>");
    }
    private static Map<Class,Object[]> _menus=new HashMap();
    public static Object[] menu(Class interf) {
        Object[] m=_menus.get(interf);
        if (m==null) {
            final String title=
                interf==Superimpose3D.class ? "Set method for 3D-Superposition" :
                interf==SequenceAligner3D.class ? "Set method for 3D-based sequence alignment" :
                interf==SequenceAlignerTakesProfile.class ? "Set sequence alignment method for mixed sequence and 3D alignment" :
                null;
            if (title!=null) {
                final List v=StrapPlugins.allClassesV(interf);
                m=new Object[sze(v)+4];
                m[0]=title;
                m[1]="i";
                m[2]=IC_CUSTOM;
                m[3]=Insecure.EXEC_ALLOWED?null:"Some methods are not available in Strap lite";
                for(int i=sze(v); --i>=0;) {
                    final Object c=get(i,v);
                    if (c==CLASS_Superimpose_GoedePROXY || c==CLASS_Superimpose_LajollaProteinPROXY) continue;
                    m[i+4]=butSetDefault(interf, nam(c));
                }
            }
            _menus.put(interf,m);
        }
        return m;
    }
    /* <<< Factories <<< */
    /* ---------------------------------------- */
    /* >>> ContextObjects >>> */
    final static Protein[] PROTEIN_AT_CURSOR={null};
    private static ContextObjects _coCur, _coSel;
    public static ContextObjects coSelected() {
        if (_coSel==null) _coSel=new ContextObjects(false,selectedObjectsV(), "Selected");
        return _coSel;
    }
    public static ContextObjects coCursor() {
        if (_coCur==null) _coCur=new ContextObjects(false,PROTEIN_AT_CURSOR, "Cursor");
        return _coCur;
    }

    /* <<< ContextObjects <<< */
    /* ---------------------------------------- */
    /* >>> gapped >>> */
    private static Gaps2Columns _g2c;
    public static Gaps2Columns g2c() {
        if (_g2c==null) (_g2c=new Gaps2Columns()).setProteinsV(ppAliV(), Protein.MC_GLOBAL);
        return _g2c;
    }
    /* <<< gapped <<< */
    /* ---------------------------------------- */
    /* >>> Get/Add/Remove Proteins >>> */
    /**  Load proteins  */
    public static void addProteins(int row, Protein[] pp) {
        if (countNotNull(pp)>0) {
            for(Protein p : pp) {
                if (p==null) continue;
                setIsInAlignment(true, -1, pp);
                vProteinsAddOrRm(p,true);
                adNotNull(p, (p.isInAlignment() ? ppAliV() : ppNotAliV()));
            }
            new StrapEvent(StrapAlign.class, StrapEvent.PROTEINS_ADDED).setParameters(new Object[]{pp, intObjct(row)}).run();
        }
        MC_GLOBAL[MCA_PROTEINS_V]=modic(vProteins())+ppAliV().mc();
    }
    /**   Closes proteins.    @param delete: Also delete on HD   */
    public static void rmProteins(boolean toTrash, Protein... pp) {
        if (countNotNull(pp)==0) return;
        if (!isEDT()) {
            inEDT(thrdM("rmProteins",StrapAlign.class, new Object[]{boolObjct(toTrash),pp}));
            return;
        }
        final File dirTrash=file(TRASH);
        mkdrsErr(dirTrash,"This directory contains deleted proteins");
        final String dirWorking=toStrg(dirWorking());
        for(Protein p: pp) {
            if (p==null) continue;
            if (toTrash) {
                final File f=p.getFile();
                if (sze(f)>0 && Insecure.canModify(f) && dirWorking.equals(f.getParent())) renamFile(f, dirTrash+"/"+f.getName());
            }
            vProteinsAddOrRm(p,false);
            ppAliV().remove(p);
            ppNotAliV().remove(p);
            p.setIsInAlignment(false);
            selectedObjectsV().remove(p);
            for(ResidueSelection s: p.residueSelections()) selectedObjectsV().remove(s);
            for(ProteinViewer pv : p.getProteinViewers()) adUniq(wref(pv), vViewersOfDisposed);
        }
        MC_GLOBAL[MCA_PROTEINS_V]=vProteins().mc()+ppAliV().mc();
        StrapEvent.dispatch(StrapEvent.PROTEINS_KILLED);
        PROTEIN_AT_CURSOR[0]=cursorProtein();
        setProgress(false);
    }

    public static boolean setIsInAlignment(boolean b, int eventDelay, Protein...  pp) {
        if (getInstance()==null || sze(pp)==0) return false;
        if (!isEDT()) {
            final Object[] ret={null};
            inEDT(thrdMR("setIsInAlignment", StrapAlign.class, new Object[]{boolObjct(b), intObjct(eventDelay), pp}, ret));
            return isTrue(ret[0]);
        }
        final UniqueList v=ppAliV(), vNot=ppNotAliV();
        final int modi=v.mc()+ vNot.mc();
        v.ensureCapacity(pp.length);
        vProteins().ensureCapacity(pp.length);
        for(Protein p : pp) {
            if (p==null) continue;
            p.setIsInAlignment(b);
            vProteinsAddOrRm(p,true);
            adNotNull(p, b ? v : vNot);
            remov(p, b ? vNot : v);
        }
        MC_GLOBAL[MCA_PROTEINS_V]=modic(vProteins())+ppAliV().mc();
        if (modi!=v.mc()+ vNot.mc()) {
            final List vS=selectedObjectsV();
            if (!b && sze(vS)>0) {
                final int selModi=modic(vS);
                for(Protein p : pp) if (p!=null) {  rmAll(p.residueSelections(), vS);  vS.remove(p); }
                if (selModi!=modic(vS)) StrapEvent.dispatchLater(StrapEvent.OBJECTS_SELECTED,333);
            }
            if (eventDelay>=0) StrapEvent.dispatchLater(b?StrapEvent.PROTEINS_SHOWN : StrapEvent.PROTEINS_HIDDEN, eventDelay);
            return true;
        }
        return false;
    }

    /**  get all loaded Proteins  */
    public static Protein[] proteins() { return vProteins().asArray(); }
    /** Deprecated. Use proteins() ! */
     public static Protein[] getProteins() { return proteins(); }
    /**
       Get the proteins that are displayed and not hidden in the focused view.
       Note that proteins can be hidden in a view.
    */
    public static Protein[] visibleProteins()  {
        final StrapView v=alignmentPanel();
        return v!=null ? v.visibleProteins() : Protein.NONE;
    }
    /* <<< Get/Add/Remove Proteins <<< */
    /* ---------------------------------------- */
    /* >>> Sorting >>>> */
    private static Runnable thread_inferOrderOfProteins(int options, Object ppSorted) {
        return thrdM("inferOrderOfProteins", StrapAlign.class, new Object[]{intObjct(options), ppSorted});
    }
    public static void inferOrderOfProteins(int options, Object ppSorted) {
        final boolean isWeb=0!=(options&SORT_WEB), isVisibleProts=0==(options&SORT_ALL);
        if (!isEDT()) {
            inEDT(thread_inferOrderOfProteins(options,ppSorted));
            return;
        }
        final StrapView view=isVisibleProts ? alignmentPanel() : null;
        final List vAll=view!=null ? view.list() :  !isVisibleProts ? vProteins() : null;
        if (sze(vAll)>0) {
            final boolean prefOrderAlreadySet= 0!=(options&SORT_PREFORDER);
            for(int row=sze(vAll); --row>=0; ) {
                final Object o=vAll.get(row);
                final Protein p=o instanceof Protein ? (Protein)o : get(0, o ,Protein.class);
                if (p==null) continue;
                if (!prefOrderAlreadySet) p.setPreferedOrder(-1);
                p.setRow(row);
            }
            if (!prefOrderAlreadySet) {
                final int N=sze(ppSorted);
                for(int i=N; --i>=0;) {
                    final Protein p=get(i,ppSorted,Protein.class);
                    if (p!=null) p.setPreferedOrder(isWeb ? (N*p.getWebTokenIdx()+i) : i);
                }
            }
            sortArry(vAll, SPUtils.comparator('P'));
            StrapEvent.dispatchLater(StrapEvent.ORDER_OF_PROTEINS_CHANGED,111);
        }
    }
    /* <<< Sorting <<< */
    /* ---------------------------------------- */
    /* >>> StrapView and Cursor >>> */
    public static void repaintCursor(boolean add, ChRunnable r) {
        if (r!=null) {
            if (add) adUniq(wref(r), _vRepaintCursor);
            else _vRepaintCursor.remove(wref(r));
        }
    }
    static void doRepaintCursor(boolean bright) {
        final List vc=_vRepaintCursor;
        for(int i=sze(vc);--i>=0;) {
            final ChRunnable r=getRmNull(i,vc,ChRunnable.class);
            if (r!=null) r.run(ChRunnable.RUN_REPAINT_CURSOR, bright?"":null);
        }
    }
    public static void animatePositionZ(int options, Protein p, int iA) {
        startThrd(thrdCR(getInstance(), "ANIM", new Object[]{p,new int[]{0,iA}}));
    }
    public static void animatePositionS(ResidueSelection ss[]) {
        final Protein p;
        if (sze(ss)==1 && null!=(p=sp(ss[0]))) animatePositionZ(0, p, ResSelUtils.firstAmino(ss[0])-Protein.firstResIdx(p));
    }

    /**
       Get  index of the row of a Protein
       returns -1 in case the protein object is not found
    */

   public static boolean isBusy() {
        for(StrapView sp: alignmentPanels())  if (sp.isBusy()) return true;
        return false;
    }
    public static long whenMovedCursor() { return _whenCursor; }
    public static int indexOfAminoAcidAtCursorZ(Protein p) { /* FIRST_AMINO_IS_IDX_0 */
        final int col=cursorColumn();
        return col<0 || p==null ? -1 : p.column2nextIndexZ(col);
    }
    /**
       get the protein where the cursor is located in.
       @return the Protein where the cursor is currently on or null
       See <i>JAVADOC:StrapAlign#cursorAminoAcid()</i>
    */
    public static Protein cursorProtein()  {
        final StrapView v=alignmentPanel();
        return v!=null ? v.cursorProtein() : null;
    }

    public static int cursorColumn()  {
        final StrapView v=alignmentPanel();
        return v!=null ? v.cursorColumn() : 0;
    }
    public static int cursorRow()  {
        final StrapView v=alignmentPanel();
        return v!=null ? v.cursorRow() : 0;
    }
    /**
       moves the cursor in the alignment panel that has the focus
    */
    public static void setCursor(Protein p,int iAa) {
        final StrapView v=alignmentPanel();
        if (v!=null && v.setCursor(0,p,iAa)) v.scrollCursorToVisible();
    }
    public static StrapView[] alignmentPanels() { return alignmentPanelsV().asArray();}
    public static StrapView alignmentPanel() {
        if (_alignFokus==null && withGui() && getInstance()!=null) _alignFokus=get(0, alignmentPanels(), StrapView.class);
        return _alignFokus;
    }
    static void setAlignmentPanelWithFocus(StrapView sp) {
        if (_alignFokus!=sp) {
            StrapEvent.dispatchLater(StrapEvent.ORDER_OF_PROTEINS_CHANGED,111);
            _alignFokus=sp;
            if (sp!=null) sp.alignmentPane().requestFocus();
        }
    }
    static void closeAlignmentPanel(StrapView a) {
        final List v=alignmentPanelsV();
        if (v.contains(a) && (sze(v)>1 || ChMsg.yesNo("Close the only alignment panel?"))) {
            v.remove(a);
            if (sze(v)==0) ChMsg.savedMsg("Open new alignment panel:<br> menu-bar =&gt; view =&gt; "+ACTION_newPan,"newPanel",1);
            if (_alignFokus==a) _alignFokus=null;
            a.dispose();
            layoutAlignmentPanels();
        }
    }
    public static void newAlignmentPanel(boolean evenIfOneIsOpened) {
        final int N=sze(alignmentPanelsV());
        if (!evenIfOneIsOpened && N>0) return;
        if (!isEDT()) inEdtLaterCR(getInstance(), "newAlignmentPanel", null);
        else {
            if ( (!_initialized || !evenIfOneIsOpened) && N>0) return;
            final StrapView v=new StrapView(true);
            adNotNull(v, alignmentPanelsV());
            newDropTarget(v.panel(),true);
            layoutAlignmentPanels();
            addListener(v);
            _alignFokus=v;
        }
        final Protein pp[]=proteins();
        if (pp.length==1) setCursor(pp[0],0);

    }
    private static void layoutAlignmentPanels() {
        if (_splitLR==null || _panRight==null) return;
        for(Component c : _panRight.getComponents()) if (c!=dialogPanel()) _panRight.remove(c);
        final JComponent vB=pnl(VB);
        for(StrapView v : alignmentPanels()) {
            vB.add(v.panel());
            setPrefSze(false, 100,100, v.panel());
        }
        _panRight.add(vB,BorderLayout.SOUTH);
        revalAndRepaintC(_panRight);
    }
    public static void strapSB() {
        final StrapView v=alignmentPanel();
        if (v!=null) v.strapSB();
    }
    /* <<< StrapView and Cursor <<< */
    /* ---------------------------------------- */
    /* >>> Menu >>> */

    private static JPopupMenu _prefMenu;
    private final static Map<String,String> _mapMenuItem2Topic=new HashMap();
    public static void showContextMenu(char menuType, Object ooMouse[]) {
        ContextObjects.openMenu(menuType, ooMouse, oo(selectedObjectsV()));
    }
    private static void mapMenuItem2Topic(Object oo[], String menuText) {
        for(Object mi : oo) {
            if (mi instanceof ChButton) {
                final Object c=gcp(ChButton.KEY_CLASS,mi);
                if (c!=null) {
                    final String cn=c instanceof Class ? nam(c) : c.toString();
                    _mapMenuItem2Topic.put(cn,menuText);
                }
            } else if (mi instanceof Object[]) mapMenuItem2Topic((Object[])mi,menuText);
        }
    }

    public static void replaceClassByButton(Object oo[],ChRunnable hook) {
        IsEnabled isEnabled=null;
        for(int i=0;i<oo.length;i++) {
            final Object o=oo[i];
            if (o=="h") i++;
            else if (o instanceof Class || o instanceof String && strEquls(_CC,o)) {
                pcp(KEY_ENABLED, isEnabled, oo[i]=b(o));
                pcp(RUN_OPEN_DIALOG_HOOK, hook, oo[i]);
            }
            else if (o instanceof IsEnabled) isEnabled=(IsEnabled)o;
            else if (o instanceof Object[]) replaceClassByButton((Object[]) o,hook);
        }
    }
    private static ProteinPopup _pPopup;
    private static Object pMenu() {
        final Object p=PROTEIN_AT_CURSOR[0];
        if (childs(_pMenu).length>4 != (p!=null) || _pMenu==null) {
            if (_pMenu==null) (_pMenu=new JMenu("Current Protein")).setMnemonic('P');
            final JPopupMenu pm=((JMenu)_pMenu).getPopupMenu();
            pm.removeAll();
            Component deact1=null, deact2=null;
            if (p!=null) {
                if (_pmItems==null) {
                    _pmItems=childs((_pPopup=new ProteinPopup(coCursor())).cursor().menu());
                }
                final String t1="This menu acts on the ", t2="protein at the cursor position.";
                if (isScreenMenuBar()) {
                    pm.add(deact1=new JMenuItem(t1));
                    pm.add(deact2=new JMenuItem(t2));
                } else pm.add(menuInfo(0,t1+t2));
                for(Component c : _pmItems) pm.add(c);
            } else pm.add(deact1=new JMenuItem("To activated this menu, set the cursor by clicking a protein's amino acid in the alignment pane."));
            setEnabld(false,deact1);
            setEnabld(false,deact2);
            revalidateC(_pMenu);
        }
        if (_pPopup!=null) setTxt(toStrg(p), _pPopup.LAB_TITLE[1]);
        return _pMenu;
    }

    private static Object[] menuEntries() {
        final boolean isV=0!=(Protein.optionsG()&Protein.G_OPT_VIEWER), isMac=isMac();
        final ProteinPopup proteinPopup=coSelected().proteinMenu();
        final ActionListener li=li();
        final Object
            bSelPat=new ChButton(StrapTree.ACTION_SELECT).li(tree().LI).t("Select by string pattern ...").i(IC_SEARCH),
            butFS= isSystProprty(IS_MAC7) ? new ChButton("FS").t("Toggle full screen").li(li) : null,
            bDiff=proteinPopup.b(ProteinPopup.CMD_HL_DIFF,0).i(IC_UNDERLINE),
            menuTutorials[]= {
            "Tutorials", "i", IC_TUTORIAL,
            "General",
            Tutorials.b("INTRO"),
            Tutorials.b("DnD"),
            Tutorials.b("UNDOCKI"),
            "-",
            "3D",
            Tutorials.b("BLAST_PDB"),
            Tutorials.b("3DVis"),
            Tutorials.b("SUPERPOSITION"),
            Tutorials.b("3D"),
            Tutorials.b("copy_DNA"),
            "-",
            "Mutations",
            Tutorials.b("MUTATIONS"),
            "-",
            "Advanced",
            Tutorials.b("WOBBLEBASE"),
            Tutorials.b("TEXSHADE"),
            Tutorials.b("ALIG_ALIGNMENTS"),
            "-",
            button(TOG_TUTORIAL_SIMUL_MENUS)
        };
        final Object[] ooo={
            new Object[]{
                "^File",null,
                proteinPopup.pMenu('I',true),
                proteinPopup.pMenu('e',true),
                proteinPopup.pMenu('E',true),
                "-",
                "&",button(BUT_SAVE),
                isV?null:new Object[]{
                    "^Backup","i",IC_SAVE_AS,
                    new ChButton("BUT_BACKUP").t("Make backup").li(li),
                    CLASS_DialogRestoreFromBackup
                },
                "-",
                new Object[]{
                    "^Utilities", "i", IC_TOOLS,
                    butFS==null?null: "&f", butFS,
                    "&s", new ChButton("SCRIPTP").t("Script panel").li(li).i(IC_CONSOLE),
                    !Insecure.EXEC_ALLOWED ? null :
                    new Object[] {"Java based plugins (deprecated)", "i",IC_PLUGIN,
                                  //new ChButton(ACTION_openPlugin).i(IC_PLUGIN).li(li),
                                  CLASS_StrapHotswap
                    },
                    "-",
                    new ChButton("NP").t("^Notepad ").i(IC_NOTEPAD).li(li),
                    new ChButton(IC_PUBMED).t("Manage Pubmed Abstracts").i(IC_PUBMED).li(li),
                    " ",
                    new ChButton("DIR").li(li).t("List Strap ^folders ...").i(systProprty(SYSP_ICON_FILE_BROWSER)),
                    new ChButton("^Rescue deleted files - open ./trash/").doViewFile(file(dirWorking()+"/trash")).i(IC_RESCUE),
                    "-"
                },
                new ChButton(ExecByRegex.RUN_SCRIPT).t("External ^Apps").i(IC_BATCH).li(li),
                "-",
                "&Q", new ChButton("BUT_EXIT").t("Exit").li(li).tt("Quit the program"),
            },
            pMenu(),
            new Object[]{
                "Se^lect",
                "Select proteins",
                new ChButton("SP").li(li).t(isScreenMenuBar() ? "^All proteins" : "^All"),
                new ChButton("SP").cp("SP",longObjct(-1)).li(li).t("^Invert selection"),
                new Object[]{
                    "^3D", "i", IC_3D,
                    "Shift-key to unselect",
                    new ChButton("SP").li(li).cp("SP",longObjct(ProteinList.CALPHA)).t("Select all Proteins with ^3D-coordinates").i(IC_3D),
                    new ChButton("SP").li(li).cp("SP",longObjct(ProteinList.HETERO3D)).t("Select all Proteins with ^hetero compounds").i(IC_FLAVIN),
                    new ChButton("SP").li(li).cp("SP",longObjct(ProteinList.NUC3D)).t("Select all Proteins with ^DNA/RNA-structure").i(IC_DNA)
                },
                ChButton.doView(_CCS+"/SequenceGroups.html").t("Groups ..."),
                "-",
                "Select proteins and annotations",
                bSelPat,
                button(BUT_SELECT_BY_NAME),
                "-",
                button(BUT_hiddenP)
            },
            new Object[]{
                "^Search",null,
                "Sequence",
                CLASS_DialogBlast,
                "&F",CLASS_DialogHighlightPattern,
                "-",
                "3D",
                CLASS_DialogSimilarStructure,
                DialogNeighbors.class,
                "Proteins and residue selections",
                bSelPat
            },
            new Object[]{
                "^Align",null,
                "Basic",
                CLASS_DialogAlign,
                CLASS_DialogSuperimpose3D,
                CLASS_DialogBlast,
                new Object[] {
                    "Alignment ^panel", "i", IC_ALIGN,
                    new Object[] {"New", "i", IC_ADD, button(BUT_NEW_PANEL), button(BUT_NEW_STRAP)},
                    new Object[] {
                        "Arrange proteins",
                        "h",CLASS_DialogManyInOneRow,
                        StrapView.button(StrapView.BUT_SORT)
                    },
                    new Object[]{
                        "^Highlight", "i", IC_UNDERLINE,
                        button(TOG_HIGHLIGHT_IDENTICAL_SEQUENCES),
                        "&F",CLASS_DialogHighlightPattern,
                        bDiff
                    },
                    StrapView.button(StrapView.TOG_PROTECT_COL)
                },
                proteinPopup.pMenu('E',true),
                "-",
                "Advanced",
                CLASS_DialogAlignOneToAll,
                isV?null:CLASS_IntermediateSeq,
                CLASS_Dialog_bl2seq,
                "-",
                button(BUT_STOP_ALL).i(IC_STOP)
            },
            new Object[]{
                "Pre^dict",null,
                "Prediction from amino acid sequence",
                CLASS_DialogPredictTmHelices2,
                CLASS_DialogPredictSecondaryStructures,
                CLASS_DialogPredictCoiledCoil,
                DialogSubcellularLocalization.class
            },
            new Object[] {
                "A^nnotate",null,
                new Object[]{
                    "^New residue selection", "i", IC_ADD,
                    ChButton.doView(_CCS+"/newResAn.html").t("^New ...").i(IC_UNDERLINE),
                    CLASS_DialogResidueAnnotationList,
                    setIcn(iicon(IC_UNDERLINE), StrapView.button(StrapView.BUT_FEATURE).mi("^Import sequence features ...")),
                    CLASS_DialogSelectionOfResiduesMain,
                },
                new Object[]{
                    "Copy",
                    "i",IC_COPY,
                    CLASS_DialogCopyAnnotations,
                    ResidueSelectionPopup.staticBut(ResidueSelectionPopup.WIKI_D),
                },
                "-",
                "Advanced",
                CLASS_DialogResidueAnnotationChanges,

            },
            new Object[]{
                "Anal^yze",null,
                new Object[]{
                    "^Compare", "i",IC_COMPARE,
                    CLASS_DialogPhylogeneticTree,
                    CLASS_DialogDotPlot,
                    CLASS_DialogCompareProteins,
                    bDiff,
                    "-",
                    CLASS_DialogNonRedundantSequenceSet,
                    "-",
                    "Special:",
                    CLASS_DialogDifferentResidues
                },
                new Object[]{
                    "^Plot","i",IC_PLOT,
                    CLASS_DialogPlot,
                    CLASS_DialogBarChart,
                    CLASS_DialogDotPlot
                },
                new Object[]{
                    "^Select sequence positions","i",IC_UNDERLINE,
                    CLASS_DialogSelectionOfResiduesMain,
                    bDiff
                },
                new Object[]{
                    "^Miscellaneous",
                    isV?null : new Object[]{"Projects",null, CLASS_FlavinePROXY, CLASS_Proteasome,CLASS_Prione},
                    CLASS_DialogDifferentResidues
                }
            },
            new Object[]{
                "Preferences",null,
                new ChButton("DIRX").li(li).t("Clear cache and temporary files...").i(IC_KILL),
                new Object[]{
                    "Applications", "i", IC_BATCH,
                    Insecure.EXEC_ALLOWED?Customize.newButton(Customize.buttonFileViewers):null,
                    new Object[]{
                        "Bioinformatics software", "i", IC_CUSTOM,
                        new Object[]{
                            "Default algorithms",
                            menu(Superimpose3D.class),
                            menu(SequenceAligner3D.class),
                            menu(SequenceAlignerTakesProfile.class)
                        },
                        new Object[]{
                            "Automated software installation",
                            Customize.newButton(Customize.fortran_compiler, Customize.C_compiler, Customize.CplusPlus_compiler).t("From source code"),
                            isWin() ? Microsoft.newCygwinButton("") : null
                        },
                        new Object[]{"Web services", buttn(TOG_DEACT_PICR), buttn(TOG_DEACT_DAS_PDB2U)}
                    }
                },
                menuItms(MENUITMS_WEB).toArray(),
                menuItms(MENUITMS_SECURITY).toArray(),
                new Object[]{
                    "Messages", "i", IC_EDIT,
                    buttn(TOG_SOUND),
                    buttn(BUT_PREV_MSG),
                },
                new Object[]{
                    "Appearance", "i", IC_SHOW,
                    buttn(TOG_MENUICONS),
                    buttn(TOG_LOCK_JMENU),
                    buttn(TOG_WHITE_BG),
                    buttn(TOG_ANTIALIASING),
                    buttn(TOG_CTRL_WHEEL),
                    buttn(BUT_LAF),
                    "-",
                    button(BUT_UPDATEUI),
                    butFS==null?null: "&f", butFS
                },
                new Object[]{
                    "Proteins","i",IC_3D,
                    new Object[]{
                        "Reading protein files",
                        button(TOG_SKIP_IDENTICAL_SEQUENCE),
                        toggl(ACTION_ignoreSEQRES).li(li).tt("Only amino acids with C-alpha ATOM-lines are regarded."),
                        toggl("Read only first model of NMR-PDB files").s(true).save(ChUtils.class,PROP_NMR_ONLY_FIRST_MODEL)
                    },
                    Customize.newButton(Customize.proteinFileExtensions).t("File extensions"),
                    new Object[]{ "Appearance", button(TOG_HIGHLIGHT_IDENTICAL_SEQUENCES) },
                    button(BUT_DRAG_OPTS).mi("Drag options", iicon(IC_DND))
                },
                new Object[]{
                    "For program developers", "i", IC_CUSTOM,
                                                                                                      
                    new Object[]{
                        "Properties",
                        new ChButton("ENV").li(li).t("Environment variables"),
                        new ChButton("PROP").li(li).t("System properties"),
                        new ChButton("getDefaults").li(li).t("UIManager.getDefaults() ..."),
                        new ChButton("ASK_HOME").t("Enter the home directory $HOME in cases where Java cannot detect it").li(li),
                    },
                    new Object[]{
                        "Stream",
                        ChStdout.newButton('R'),
                        ChStdout.newButton('O'),
                    },

                    new Object[]{
                        "Event",
                        button(BUT_LIST_EV),
                        button(BUT_WATCH_EV)
                    },
                    new Object[]{
                        "Protein",
                        button(TOG_HIDE_GAPS),
                        new ChButton("PARSE_INFO").t("Parsing report of selected proteins").li(li),
                        new ChButton("DEBUG_noLetters").t("Are there residues that are not letters?").li(li),
                        new ChButton("CLIENT_PROPERTY").t("List client properties of selected proteins").li(li)
                    },
                    new Object[]{
                        "Java",
                        buttn(TOG_JAVA_SRC),
                        ChButton.doOpenURL(URL_STRAP_JARS+"src").t("Source codes of embedded Java libraries"),
                        new ChButton("MEM").t("Free memory").li(li)
                    },
                    new Object[]{
                        "Misc",
                        new ChButton("DEBUG_ON").t("Debugging on").li(li),
                        new ChButton("DEBUG_GC").t("Garbage collection").li(li),
                        new ChButton("DEBUG_T").t("List all threads").li(li)

                    }
                }
            },
            new Object[]{
                "^Help",
                new ChButton("About").li(li).i(IC_INFO),
                new ChButton("MANUAL").t("Manual ...").i(IC_BOOK).li(li),
                new ChButton("MAN").t("Command-line arguments").i(IC_CONSOLE).li(li),
                new Object[] {
                    "Online documentation", "i",IC_WWW,
                    ChButton.doOpenURL(URL_STRAP).t("Home page").i(IC_WWW),
                    ChButton.doOpenURL("Adobe_flash_movies*").i(IC_MOVIE)
                },
                menuTutorials,
                new Object[] {
                    "Looking for menu items",
                    "i", IC_SEARCH,
                    new ChButton(CMD_ALL_POPUP).li(li),
                    new ChButton(CMD_ALL_MENUS).li(li),
                    "Note: The lists of menu items are searchable (Ctrl+F)"
                },
                 new Object[] {
                    "Selected topics",
                    "h",StrapAlign.class,
                    new Object[]{
                        "Loading files", "i",IC_DIRECTORY,
                        "h",Strap.class,
                        "h",ChButton.doOpenURL("Drop_Web_Link*"),
                        "h",InteractiveDownload.class
                    },
                    new Object[]{
                        "Alignment panel","i",IC_ALIGN,
                        "h",StrapView.class,
                        "h",StrapKeyStroke.class,
                        "h",Gaps2Columns.class
                    },

                    "h",ResidueAnnotation.class,
                    new Object[]{
                        "3D", "i",IC_3D,
                        "h",V3dUtils.class,
                        "h",Protein3d.class
                    },
                    new Object[]{
                        "Computer system", "i", isMac?IC_MAC_OS : IC_WINDOWS,
                        "h", Microsoft.class,
                        ChButton.doView(_CC+"/Macintosh.html").t("Strap on Macintosh").i(IC_MAC_OS)
                    },
                    "h", StrapTree.class,
                    "h", ChTextView.class
                 }
            }
        };
        return ooo;
    }
    public static BA allMenus(boolean show, boolean menuBar, BA sb) {
        final BA SB=sb!=null ? sb : new BA(9999);

        if (menuBar) {
            SB.a("<hr><h2>Menu-bar</h2><b>Undock:</b> Frequently used menu items can be dragged to the desktop.<br><br>");
            for(Component menu : strapMenus()) if (!"Current Protein".equals(getTxt(menu))) jMenu2html(0,menu, SB);
        } else {
            final ContextObjects co=new ContextObjects(false,NO_OBJECT, "allMenus");
            final StrapView sv=new StrapView(false);
            final Object[][] contextMenus={
                {"Proteins", co.proteinMenu().menu()},
                {"residue annotations", co.selectionMenu().menu('A')},
                {"residue selections", co.selectionMenu().menu('S')},
                {"files",FilePopup.instance().menu(null)},
                {"rubber band selection", sv.getPopup(true)},
                {"alignment panel", sv.getPopup(false)},
            };
            SB.aln("<hr><h2>Context-Menus in Strap</h2>\nWIKI:Context_menu are opened by right click (see "+MOVIE_Context_Menu+"<br>");
            if (isMac()) SB.aln("The right mouse button can be simulated by alt+Ctrl+click.");
            for(Object[] menu : contextMenus) {
                SB.a("<hr><h2>Context menu of ").a(menu[0]).aln("</h2><div class=\"popupmenu\">");
                jMenu2html(0,(Component)menu[1],SB);
                SB.a("</div>");
            }
        }
        wrte(file("~/@/doc/"+(menuBar?"JMenuBar":"JPopupMenu")+".html"),SB);
        if (show) {
            final String tab=menuBar ? "Menu bar":"Context menus";
            JComponent tv=PANELS_JH['H'].componentWithTabtext(tab);
            if (tv==null) TabItemTipIcon.set(tab,null,null,null,tv=scrllpn(new ChJTextPane(SB)));
            PANELS_JH['H'].addTab(CLOSE_ALLOWED, tv);
        }
        return SB;
    }
    static JComponent[] makeMenus(Object[] ooo0, ActionListener li) {
        final List v=new Vector();
        int iMenu=0;
        final Object ooo[]=ooo0.clone();
        replaceClassByButton(ooo,null);
        final boolean macLaf=isMacLaf();
        for(Object o:ooo) {
            if (o instanceof AbstractButton || o instanceof JLabel) {
                v.add(o);
                continue;
            }
            final Object oo[]=oo(o);
            if (sze(oo)==0) continue;
            final JMenu m=jMenu(MENU_HAS_ACCELERATOR, oo,"menubar");
            final String txt=getTxt(m), sortKey=++iMenu+ " "+txt;
            if (sortKey.indexOf("plug")>=0) pcp(ChTabPane.KEY_SORT,sortKey, PANELS_JH['J']);
            mapMenuItem2Topic(oo,sortKey);
            if (m==null) continue;
            final boolean isPref="Preferences"==txt;
            if (isPref) _prefMenu=m.getPopupMenu();
            if (!isPref || !macLaf) {
                v.add(m);
                addActLi(li,m);
            }
        }
        if (_prefMenu==null) assrt();
        return toArry(v,JComponent.class);
    }
    static Component[] strapMenus() {
        if (_mb==null) {
            final Component cc[]=makeMenus(menuEntries(),li());
            int n=0;
            for(Object o : cc) {
                if (o==_pMenu) continue;
                if ((n=undockSaved(o))==0) break;
            }
            if (n>0) n=undockSaved(_prefMenu);
            if (n>0) n=undockSaved(coSelected().proteinMenu().menu());
            if (n>0) n=undockSaved(coSelected().selectionMenu().getMenuObjects('s'));
            Component[] ooFlav=null;
                                                                                                                 
            _mb=joinArrays(cc,ooFlav,Component.class);
        }
        return _mb;
    }
    public static JMenuBar menubars(int i01) {
        if (_mbb==null) {
            _mbb=new JMenuBar[]{new ChJMenuBar(),new ChJMenuBar()};
            pcp(KOPT_DROP_TARGET_IS_CURRENT_PV,"",_mbb[0]);
            newDropTarget(_mbb[0],false);
        }
        return _mbb[i01];
    }

    public static boolean isOtherMenuBar() { return _isOtherMB; }
    /*
      Alle childs des mb setOpaque(false) geht unter Nimbus und Mac, aber nicht mit Windows.
      Auch setBackground(null) kein Erfolg.
    */
    public static void setMenuBar(Component mm0[], int fg, int bg) {
        final Component mm[]=mm0!=null ? mm0 : strapMenus();
        _isOtherMB= mm!=strapMenus();
        final boolean isMain=!(isMac() || button(TOG_MULTI_MBARS).s()) || strapMenus()==mm;
        boolean reval=true;
        final JMenuBar mb=menubars(isMain?0:1);
        final Container cp=frame().getContentPane();
        if (!arraysEqul(JMenu.class, mm, mb.getComponents())) {
            mb.removeAll();
            toMenuBar(0,mm,mb);
            if (isMain) V3dUtils.setPV(V3dUtils.PV_MENUBAR, null);
            reval=true;
        }
        if (!isMain && _mb2==null) cp.add(_mb2=mb,BorderLayout.NORTH);
        if (isMain) noMenuIconsOnMac(mb);
        if (isMain && _mb2!=null) {
            reval=true;
            cp.remove(menubars(1));
            _mb2=null;
        }
        if (mm==mm0 && !isMain) {
            for(Object c : childs(mb)) {
                if (gcp(KEY_DROP_TARGET_REDIRECT,c)==null) {
                    pcp(KOPT_DROP_TARGET_IS_CURRENT_PV, "",c);
                    StrapAlign.newDropTarget(c, false);
                }
            }
        }

        if (reval) {
            ChDelay.revalidate(mb,333);
            ChDelay.repaint(mb,444);
        }
        setFgBgMb(mb,fg, fg==bg ? DEFAULT_BACKGROUND : bg);

    }

    /* <<< Menu <<< */
    /* ---------------------------------------- */
    /* >>> Help/Manual >>> */
    public static BA manPage(char mode) {
        return
            decodeEscChrs(readBytes(rscAsStream(0L,_CCS,"manPage.rsc")),mode)
            .replace(0L, "STARTS", fExists(GUI_BINARY) ?  "java -jar strap.jar" : lstPathCmpnt(GUI_BINARY));
    }

    static void generateManual() {
        final AbstractButton cbClearAll=cbox("Clear "+dirDocumentation());
        final boolean doAll;
        final String cn=nam(AlignUtils.class);
        final File fMan=file(dirDocumentation(),"manual.html");

        String sb_ftp="\n\nDocumentation will be written to\n "+fMan+"\nand will appear in the web browser.\n";
        if (myComputer()) {
            doAll=ChMsg.yesNo(pnl(VB,cbClearAll,"All?"));
            sb_ftp+="\n scp ~/.StrapAlign/doc/"+cn+".html  $STRAP_DIR_B/doc/manual.html\n\n";
        } else doAll=false;
        if (cbClearAll.isSelected()) { deleteTree(dirDocumentation()); mkdrsErr(dirDocumentation());}
        allMenus(false,true,null);
        allMenus(false,false,null);
        final String cc[];
        if (doAll) {
            final String shellCmd="cd ~/java; "+
                "fgrep -l  '/**"+"HELP' $(find charite -name '[A-Z]*.java') | sed 's|\\./||1;s|.java$||1' | tr '/' '.'; "+
                "find charite -name '[A-Z]*Help.html' | sed 's|\\./||1;s|Help.html$||1' | tr '/' '.'";
            final ChExec ex=new ChExec(ChExec.STDOUT).setCommandLineV("SH",shellCmd);
            ex.run();
            cc=splitTokns("-log -noExit "+toStrg(ex.getStdout()));
            sb_ftp+=UPLOAD_DOC;
        } else cc=new String[]{"-log","-noExit",cn};
        HtmlDoc.main(cc);
        shwTxtInW(HtmlDoc.log().a(sb_ftp));
        cpy(file(dirDocumentation(),cn+".html"),fMan);
        inEDTms(thrdRRR(threadWritePng(), thread_visitURL(fMan, 0)), 3333);
    }
    /* <<< Help/Manual <<< */
    /* ---------------------------------------- */
    /* >>> Frame / GUI>>> */
    private static ChFrame _frame;
    public static ChFrame frame() {
        if (_frame==null && li()!=null && withGui()) {
            final String title="Strap "+(Insecure.CLASSLOADING_ALLOWED?"":"Lite")+" Project: "+dirWorking().getName();
            addActLi(li(),_frame=new ChFrame(title).i(IC_STRAP));
            li().addTo("c",_frame);
            ChFrame.setMainFrame(_frame);
            _frame.setBounds(new Rectangle(55,40,888,540));
            _frame.setJMenuBar(menubars(0));
            try {
                final Rectangle screen=_frame.getGraphicsConfiguration().getBounds();
                _frame.size(x(screen)+maxi(800,wdth(screen)-300), y(screen)+maxi(hght(screen)-200,500));
            } catch(Exception ex){}
        }
        return _frame;
    }
    void initUI() {
        ChIcon.setImageObserver(frame());
        final StrapTree st=tree();
        if (0==(Protein.optionsG()&Protein.G_OPT_VIEWER)) {
            final BA lastSession=readBytes(file("proteins.list"));
            if(sze(lastSession)>0) {
                final ChTextArea ta=new ChTextArea(lastSession.delBlanksR().a('\n'));
                final String security=Insecure.EXEC_ALLOWED && !isSelctd(buttn(TOG_NASK_EXEC)) || isSelctd(buttn(TOG_NASK_UPLOAD)) ?
                    "<sub>Customize security settings in Preferences menu.</sub>" : null;
                final JComponent
                    b=new ChButton("Load the following files").li(li()).cp("LD_LAST",ta),
                    pan=pnl(CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE,ta),pnl(VBHB,security, pnl(HBL,b,"<sub>Note: Use Tab-key for file name completion</sub>")), pnl(REMAINING_VSPC1) );
                ta.tools().highlightOccurrence("#HIDDEN",null,null, HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(0xFF0000));
                highlightProteins("PD",ta);
                TabItemTipIcon.set("Previous",null,null,null,pan);
                closeOnKey(CLOSE_CtrlW, ta, pan);
                pcp(ChTabPane.KEY_SORT,"0T",pan);
                TabItemTipIcon.set("Load",null,null,null, pan);
                dialogPanel().addTab(0, pan);
            } else Tutorials.openTutorial("WELCOME");
        }
        addActLi(li(),HtmlDoc.class);
        addShutdownHook1( thrdCR(this,"EXIT"));
        Customize.setParent(dialogPanel(),li());

        final Component
            panMain=frame().getContentPane(),
            spTree= scrllpn(0,st,dim(99,33333));
        setMinSze(0,0, spTree);
        _panRight=pnl(CNSEW,dialogPanel());
        (_splitLR=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, spTree, _panRight)).setDividerLocation(0);
        pnl(panMain,CNSEW, _splitLR, null, Web.labelTestConnection(null, "Please check the Web Proxy settings in \"Preferences\""));
        setToolpane(null);

        frame().shw(ChFrame.CAN_FULLSCREEN);
        if (prgOptT("-iconify")) frame().setExtendedState(JFrame.ICONIFIED);
        newDropTarget(panMain,true);
        newDropTarget(menubars(0),true);
        newDropTarget(menubars(1),true);
        newDropTarget(dialogPanel(),true);
        buttn(TOG_WHITE_BG).li(li());
        setMenuBar(null,0,0);
        setDefaultDownloadLogger(this);
        error((CharSequence)null);
        addActLi(li(), HtmlDoc.instance());
        ChTextComponents.setContextMenuProvider(this);
        pcp(KEY_CONTEXT_OBJECTS, selectedObjectsV(), jlUndock(true));  /* See ChUtils MOUSE_ENTERED setCntxtObj(..) */
        newDropTarget(jlUndock(true), false);
        setLi4CntxtMenu(st.LI);
        dialogPanel().addSouth(_shopping);

        setMinSze(0,0, dialogPanel());
        final ChTabPane
            tpj=new ChTabPane(ChTabPane.RIGHT|ChTabPane.COLLAPSE),
            tph=new ChTabPane(ChTabPane.RIGHT|ChTabPane.COLLAPSE);
        addActLi(li(),tpj);
        addActLi(li(),tph);
        PANELS_JH['J']=tpj;
        setMinSze(0,0, tpj);
        PANELS_JH['H']=tph;
        setMinSze(0,0, tph);
        TabItemTipIcon.set("","Java source codes","Java source codes",IC_JAVA,tpj);
        TabItemTipIcon.set("Help","Help texts","help texts",null,tph);

        DAS.checkAvailabilityPdb2u();
        PicrEBI.checkAvailability();
        _initialized=true;
    }

    public static void setToolpane(Component c0) {
        final Component stp=StrapView.toolbars(),  c=c0!=null ? c0 : stp;
        Component tParent=_toolParent;
        if (tParent==null) {
            tParent=_toolParent=pnl(HB, stp.getPreferredSize());
            dialogPanel().add(tParent,BorderLayout.SOUTH);
            setMaxSze(9999, prefH(stp), tParent);
        }
        if (c!=_tool) {
            pcp(KOPT_DO_NOT_INVALIDATE,"",dialogPanel());
            if (_tool!=null) ((JComponent)_toolParent).remove(_tool);
            ((JComponent)_toolParent).add(_tool=c);
            c.setBounds(0,0,_toolParent.getWidth(), hght(_toolParent));
            revalAndRepaintC(c);
            pcp(KOPT_DO_NOT_INVALIDATE,null,dialogPanel());
        }
    }
    public static void showPreferencesDialog() { shwPopupMenu(_prefMenu);}

    /* <<< Frame/ GUI <<< */
    /* ---------------------------------------- */
    /* >>> */
    private static StrapTree _tree;
    public static StrapTree tree() { return _tree==null ? _tree=new StrapTree() : _tree; }
    static void viewTree(Object o) {
        if (_splitLR.getDividerLocation()<200) _splitLR.setDividerLocation(200);
        if (o!=null) {
            final javax.swing.tree.TreePath tp=getTreePath(o, tree());
            if (tp!=null) tree().scrollPathToVisible(tp);
        }
    }
    /* <<< Tree <<< */
    /* ---------------------------------------- */
    /* >>> Web Alignment >>> */
    static Protein3d webAlignment3D(Protein[] ppNew3Da, Protein3d p3d0, boolean exact) {
        final ChTableLayout pan3D=tPanel(CLASS_Protein3d);
        Protein3d p3d=p3d0;
        if (p3d==null) {
            Protein3d p3dAlready=findBackbone3D(true,ppNew3Da);
            if (p3dAlready==null && !exact) p3dAlready=findBackbone3D(false,ppNew3Da);
            if (p3dAlready==null) p3d=new3dBackbone(0,ppNew3Da);
            pan3D.narrowAllOthers(p3dAlready!=null ? p3dAlready : p3d, maxi(ICON_HEIGHT-pan3D.getAllComponents().length, EX));
        }
        if (p3d!=null) {
            for(Protein p : ppNew3Da) p3d.addProteins(p);
            if (p3d.getProteins().length>1) p3d.eachChainOneColor(true,false);
        }
        addDialog(pan3D);
        return p3d;
    }
    public static Runnable threadWebAlignment(int option, CharSequence webAlignment, Object targets[]) {
        return thrdM("webAlignment",StrapAlign.class, new Object[]{intObjct(option), webAlignment, targets});
    }
    public static void webAlignment(int option, CharSequence webAlignment,  Object targets0[]) {
        if (webAlignment==null) return;

        newAlignmentPanel(false);
        final Protein alreadyLoaded[]=proteins();
        final String[] tokens, tokens0=intrn(splitTokns(webAlignment));
        int chainsWereSplit=0;
        {
            final List v=new Vector();
            for(String t0 : tokens0) {
                boolean added=false;
                if (0==1) if (t0.startsWith("PDB:")) {
                    final String t=delFromChar(t0,'|'), fields0= delPfx(t,t0);
                    final File ff[]=t.indexOf('_')<0 && t.indexOf(':',4)<0 ? downloadAllPdbChains(pdbID(t), pdbChain(t)) : null;
                    //if (t.indexOf(':',4)>0 && debug("Colon before chain letter is depricated")) stckTrc();
                    for(int i=sze(ff); --i>=0;) {
                        final String fn=ff[i].getName();
                        final int us=fn.indexOf('_');
                        final String chain=substrg(fn,us+1,us+2);
                        final int pipe=fields0.indexOf('|',1);
                        final String fields= /* Append underscore chain to the name field */
                            chrAt(0,fields0)!='|' || pipe==1 || ff.length==1 ? fields0 :
                            pipe<1  ? fields0+"_"+chain :
                            fields0.substring(0,pipe)+"_"+chain+fields0.substring(pipe);
                        if (us>0) { v.add(t+"_"+chain+fields); chainsWereSplit++; added=true;  }
                    }
                }
                if (!added) v.add(t0);
            }
            tokens=strgArry(v);
        }
        final String[] ttSorted=tokens.clone();
        final Collection<String> notLoaded=new HashSet();
        final UniqueList<Protein> vAll=new UniqueList(Protein.class).addOptions(UniqueList.USE_HASH_SET).ensureCapacity(1000);
        final int nT;
        { /* Unhide those that are already loaded */
            final Collection<Protein> vHidden=new Vector();
            int nT0=0;
            for(int i=tokens.length; --i>=0;) {
                final String t=tokens[i];
                if (sze(t)>1) {
                    nT0++;
                    adNotNull(t, notLoaded);
                    final String noPipe=delFromChar(t,'|');
                    for(Protein  p : alreadyLoaded) {
                        if (strEquls(noPipe,p.OBJECTS[Protein.O_WEB_TOK1])) {
                            if (t!=noPipe) applyControlFields(t.split("\\|"),p);
                            vAll.add(p);
                            if (!p.isInAlignment()) vHidden.add(p);
                            remov(t, notLoaded);
                            ttSorted[i]="";
                        }
                    }
                }
            }
            if (setIsInAlignment(true, -1, spp(vHidden))) StrapEvent.dispatchLater(StrapEvent.PROTEINS_SHOWN, 99);
            nT=nT0;
        }
        if (/*webAlignment.startsWith("PDB:") && */vAll.size()>0 && sze(targets0)>0) {
            for(Object t :targets0) {
                if (t instanceof Integer) {
                    new StrapEvent(StrapAlign.class, StrapEvent.PROTEINS_ADDED).setParameters(new Object[]{spp(vAll),t}).run();
                    break;
                }
            }
        }
        setProgress(true);
        Protein3d p3d=null;
        boolean showDialogPane=false;
        final Object[] targets=ad(new Object[]{KEY_TOKENS,tokens}, targets0, 2, Object.class);
        String[] dasFeatures=null;
        ProteinViewer viewer3D=null;
        for(int i=targets.length; --i>=0;) {
            final Object t=targets[i];
            if (t instanceof Protein3d) p3d=(Protein3d)t;
            if (t instanceof Object[] && get(0,t)==KEY_DAS) dasFeatures= get(1,t,String.class).split("\\|");
            if (viewer3D==null) viewer3D=gcp(V3dUtils.KEY_PROTEIN_VIEWER, t, ProteinViewer.class);
        }
        if ( (option&OPTION_USE_UNIPROT_SOAP)!=0 && nT>1) startThrd(FetchSeqs.thread_urlGet(FetchSeqs.SKIP_EXISTING, tokens));
        sortArry(ttSorted);
        for(int from=0; ; ) { /* Download groups of tokens in separate threads */
            while(from<ttSorted.length && sze(ttSorted[from])<2) from++;
            if (from>=ttSorted.length) break;
            final String dbColon=ttSorted[from].substring(0,ttSorted[from].indexOf(':')+1);
            int to=from;
            while(to<ttSorted.length && sze(ttSorted[to])>1 && ttSorted[to].startsWith(dbColon)) to++;
            if (to>from) startThrd(thread_downloadProteins(option, subArry(from,to, ttSorted, 0,String.class) ,targets,vAll, notLoaded));
            from=to;
        }
        final List<Protein> vSP=new Vector(), vPdb=new Vector();
        final boolean shouldSuperimpose=tokens0.length>1 && chainsWereSplit<2, superimposing[]={false,shouldSuperimpose};
        if (shouldSuperimpose) startThrd(SPUtils.threadLoopSuperimpose(SPUtils.SUPERIMP_EVENT, vSP, superimposing));
        final Runnable
            event=cntains(ACTION_SORT, targets) ?
            thrdRRR(new StrapEvent(StrapAlign.class,StrapEvent.PROTEINS_ADDED), thread_inferOrderOfProteins(SORT_WEB, vAll) ) : null;
        final List<Protein> vNew3D=new Vector();
        { /* Wait for the proteins to be download */
            final List<Protein> vIn=new Vector();
            final Object REGIST=new Object();
            for(int loop=10*10*60; --loop>=0;) {
                sleep(700);
                if (loop%10==0) {puts(" wait-for#"+sze(notLoaded)+" "); } else puts(".");
                boolean foundNewProtein=false;
                vIn.clear();
                for(int i=sze(vAll); --i>=0;) {
                    final Protein p=get(i,vAll,Protein.class);
                    if (p==null) continue;
                    final Object oo[]=p.OBJECTS;
                    final String t=(String)oo[Protein.O_WEB_TOK];
                    if (oo[Protein.B_PROCESSED]==null) continue;
                    if (oo[Protein.B_REGISTERD]==REGIST) continue;
                    oo[Protein.B_REGISTERD]=REGIST;
                    if (!p.isInMsfFile()) {
                        if (p.getResidueCalpha()!=null) {
                            if (p.getCharacters().length==0) assrt();
                            adUniq(p,vPdb);
                            if (sze(Protein.strapFile(Protein.mTRANS, p, null))==0) {
                                if (!isTrue(Strap.DO[Strap.NO_SUPERPOSE], idxOfStrg(t,tokens))) adUniq(p,vSP);
                                else if (p.getRotationAndTranslation()!=null) {
                                    p.setRotationAndTranslation(null);
                                    StrapEvent.dispatchLater(StrapEvent.PROTEIN_3D_MOVED,333);
                                }
                            }
                            if (viewer3D==null && !isTrue(Strap.DO[Strap.NO_3D], idxOfStrg(t,tokens))) {
                                adUniq(p,vNew3D);
                                p3d=webAlignment3D(spp(vNew3D),p3d,false);
                                showDialogPane=true;
                            }
                        }
                    }
                    if (!p.isInAlignment()) {
                        vIn.add(p);
                        foundNewProtein=true;
                    }
                }
                if (sze(vIn)>0) setIsInAlignment(true, 222, spp(vIn));
                if (foundNewProtein) {
                    if (event!=null) inEdtLater(event);
                    if ((option&OPTION_SCROLL)>0) StrapEvent.dispatchLater(StrapEvent.NEED_SCROLL_DOWN_ALIGNMENT,1111);
                }
                if (sze(notLoaded)==0) break;
            }
            if (sze(vNew3D)>0 && null==findBackbone3D(true, spp(vNew3D))) { /*p3d might be null */
                webAlignment3D(spp(vNew3D),p3d, true);
            }
        } /* All proteins are downloaded now */
        // debugExit("webAlignment ",vAll);

        superimposing[0]=true; /* Stop for now */
        while(superimposing[1]) sleep(99);

        setWndwStateT('I',0,0,"D");
        final Protein[] ppPeptide=SPUtils.assignHeterosToClosestPeptides(vAll,tokens);
        setProgress(false);
        //boolean newProteinExists=false; for(Protein p: ppPeptide) newProteinExists=newProteinExists||!cntains(p,alreadyLoaded);
        if (ppPeptide.length>1 && tokens0.length>1) {  /* Alignment */
            final List<Protein> vA=new Vector(ppPeptide.length);
            for(int i=0; i<ppPeptide.length; i++) {
                final Protein p=ppPeptide[i];
                if (sze(Protein.strapFile(Protein.mGAPS, p, null))==0 && !isTrue(Strap.DO[Strap.NO_ALIGNMENT],p.getWebTokenIdx())) vA.add(p);
            }
            final boolean reorder=Strap.DO[Strap.DO_CHANGE_ORDER]!=null;
            SPUtils.alignProteins(reorder ? SPUtils.ALIGN_CHANGE_ORDER:0, (SequenceAligner)null, vA, CAN_BE_STOPPED);
            if (!reorder) inferOrderOfProteins(SORT_WEB,vA);
        }
        if ((option&OPTION_SCROLL)>0) StrapEvent.dispatch(StrapEvent.NEED_SCROLL_DOWN_ALIGNMENT);
        if (!cntains(Strap.NOT_TO_FRONT,targets)) setWndwState('F',frame());
        if (showDialogPane) StrapView.correctHeight1();
        if (dasFeatures!=null) startThrd(StrapDAS.thread_load(0, dasFeatures, ppPeptide, (ChRunnable)getInstance()));
        if (shouldSuperimpose) SPUtils.superimposeProteins(SPUtils.SUPERIMP_EVENT, vSP, CAN_BE_STOPPED);
        if (sze(vNew3D)>0 && null==findBackbone3D(true, spp(vNew3D))) { /*p3d might be null */
            webAlignment3D(spp(vNew3D),p3d, true);
            putln(RED_WARNING+"StrapAlign ","WEBALIGNMENT_3D should have been shown previously vNew3D=",vNew3D);
        }
        /* Icon and CSA */
        startThrd(thrdCR(getInstance(), "CSA", spp(vPdb)));
        SPUtils.setProteinIcon(ppPeptide,false,'A');
    }
    /* <<< webAlignment <<< */
    /* ---------------------------------------- */
    private final static Collection<Protein> vPWT=new Vector();
    private static Protein[] ppWithToken(String s, List<Protein>v) {
        synchronized(vPWT) {
            vPWT.clear();
            boolean found=false;
            for(int i=sze(v); --i>=0 && s!=null;) {
                final Protein p=v.get(i);
                if (p!=null && ( s.equals(p.OBJECTS[Protein.O_WEB_TOK])||s.equals(p.OBJECTS[Protein.O_WEB_TOK1]  ))) {
                    adUniq(p,vPWT);
                    found=true;
                }
            }
            return found ? toArryClr(vPWT,Protein.class) : Protein.NONE;
        }
    }
    public static Runnable thread_downloadProteins(int options, String entry[], Object[] targets, List<Protein> vAll, Collection<String>notLoaded) {
        return thrdM("downloadProteins",StrapAlign.class, new Object[]{intObjct(options), entry, targets, vAll, notLoaded});
    }
    public static void downloadProteins(int options, String tokens[], Object[] targets, List<Protein> vAll, Collection<String>notLoaded) {
        long whenProc=System.currentTimeMillis();
        final List<Protein> vA=new Vector(), vProcess=new Vector();
        for (int iT=0; iT<tokens.length; iT++) {
            vA.clear();
            final Protein pp[]=downloadProteinID(options & ~OPTION_PROCESS_LOADED_PROTS, tokens[iT], targets,vA);
            for(int i=0; i<sze(vA); i++) {
                adUniq(vA.get(i),vAll);
                adUniq(vA.get(i),vProcess);
            }
            if (System.currentTimeMillis()-whenProc>3333 || iT==tokens.length-1) {
                processLoadedProteins(options, spp(vProcess),targets);
                vProcess.clear();
                whenProc=System.currentTimeMillis();
            }
            remov(tokens[iT], notLoaded);
        }
    }
    public static Protein[] downloadProteinID(int options, String entry, Object[] targets, List<Protein> vAll) {
        if (entry==null) return Protein.NONE;
        {
            final Protein[]  pp1=ppWithToken(entry, vAll), pp2=ppWithToken(entry, StrapAlign.vProteins());
            if (pp1.length+pp2.length>0) {
                if ( (options&OPTION_PROCESS_LOADED_PROTS)!=0) {
                processLoadedProteins(options, pp1,targets);
                processLoadedProteins(options, pp2,targets);
                }
                adAllUniq(pp2,vAll);
                return Protein.NONE;
            }
        }
        final String field[]=entry.split("\\|"),  urlOrId=delAfter(KEEP_DAYS_IN_CACHE,get(0,field),true), nameSpecified=get(1,field);
        final int daysInCache=keepDaysInCache(get(0,field));
        final int L=sze(urlOrId);
        final boolean failed= L==0;
        String  allTokens[]=null;
        for(int i=sze(targets); --i>=0;) {
            final Object t=targets[i];
            if (t instanceof Object[] && get(0,t)==KEY_TOKENS) allTokens=(String[])get(1,t);
        }
        final int underscore=maxi(urlOrId.indexOf('_',5), urlOrId.indexOf(':',5));
        final String pdbId=urlOrId.startsWith("PDB:") ? pdbID(urlOrId) : null;
        String name="",  url=null;
        Protein proteins[]=null;

        if (looks(LIKE_FILEPATH,urlOrId)) {
            final Protein p=Protein.newInstance(file(delLstCmpnt(urlOrId,'!')));
            proteins=new Protein[]{p};
            p.setAlignment(getInstance());
        }

        if (proteins==null && !failed && pdbId!=null) {
            final char chain=underscore>0 ? chrAt(underscore+1, urlOrId) : 0;
            downloadAllPdbChains(pdbId, chain);
            proteins=downloadPdbAndCreateProteins(pdbId,chain, proteins());
            final File fAllChains=localPdbFile(pdbId,(char)0);
            for(int i=sze(proteins); --i>=0;) proteins[i].setFileWithAllChains(fAllChains);
        } else if (!failed) {
            url=Hyperrefs.toUrlString(urlOrId,Hyperrefs.PROTEIN_FILE);
            final int slash=urlOrId.lastIndexOf('/'), eq=urlOrId.lastIndexOf('='), colon=urlOrId.indexOf(':');
            final String name0=urlOrId.substring( (slash>0 ? maxi(slash,eq) : colon)+1);
            name=looks(LIKE_EXTURL,entry) ? delSfx(".Z",delSfx(".gz",delSfx(".GZ",delSfx(".bz2",name0)))) : name0;
        }
        if (!failed && proteins==null) {
            final String proteinName= sze(nameSpecified)>0 ? nameSpecified : name.replace(':','_');
            /* --- Problem with colon in KEGG:btk:BT9727_1829 --- */
            Protein p=null;
            for(Protein prot: proteins()) {
                if (proteinName.equals(prot.getName()) || proteinName.equals(prot.getOriginalName())) {
                    p=prot;
                    break;
                }
            }
            final File proteinFile=file(name2file(proteinName));
            if (p==null && (url!=null || pdbId!=null)) {
                //File fAll=null;
                if (sze(proteinFile)==0) {
                    final char onlyChain=chainAfterUS(urlOrId);
                    final File fDownloaded;
                    if (pdbId!=null) fDownloaded=downloadPdbChain(pdbId,onlyChain); //!!! redundant??
                    else {
                        final File fSoap=FetchSeqs.dbColonID2file(urlOrId);
                        if (sze(fSoap)>=PROTEIN_MIN_FILE_SZE) fDownloaded=fSoap;
                        else {
                            fDownloaded=urlGet(url(url), daysInCache, new File[]{fSoap},(ChRunnable)getInstance());
                            if (daysInCache==0) delFileOnExit(fDownloaded);
                        }
                    }
                    synchronized(SYNC_CPY) {
                        if (sze(fDownloaded)!=0 && sze(proteinFile)==0) cpy(fDownloaded, proteinFile);
                    }
                }
                if (sze(proteinFile)>0) {
                    final Protein ppNewOld[][]=loadTheProteinsInList(options&~OPTION_PROCESS_LOADED_PROTS, proteinName,targets);
                    proteins=get(1,ppNewOld,Protein[].class);
                    adAll(proteins,vAll);
                    if (sze(proteins)==1) proteins[0].setOriginalName(proteinName);
                }
            }
            if (p!=null) proteins=spp(p);
        }
        if (proteins==null) proteins=Protein.NONE;
        for(Protein p : proteins) {
            if (p==null) continue;
            if (p.getCharacters().length>0) {
                p.setURL(url(url));
                final String newName0=sze(nameSpecified)>0 ? nameSpecified : (options&OPTION_RENAME_SWISS)!=0 ? DialogRenameProteins.newName(p,DialogRenameProteins.PRC3_DROME) : null;
                if (sze(newName0)>0) {
                    final int iDot=newName0.lastIndexOf('.');
                    final String chains=p.getChainsAsString();
                    p.setName(proteins.length>1 && sze(chains)==1 && iDot<0 ? newName0+"_"+chains+".pdb" : newName0);
                }
            }
            if (Hyperrefs.ensemblOrganism(name)!=null) { /* Ensembl */
                for(String cds : p.getCDS()) {
                    final String[] gppn=p.geneProductProteinNoteForCDS(cds);
                    if (idxOfStrg(name,gppn)>=0) {
                        /*Colon:  AL353593.33:31623..31898,AL353593.33:32740..33015,AL353593.33:33855..34130 */
                        if (cds.indexOf(':')<0) p.parseCDS(cds);
                        else if (gppn[Protein.CDS_TRANSLATION]!=null) {
                            p.setResidueType(gppn[Protein.CDS_TRANSLATION]);
                        }
                        final String gene=gppn[Protein.CDS_GENE];
                        if (sze(gene)>0 && strstr(gene,name)<0) p.setName(name=gene+"_"+name);
                    }
                }
            }
            if (entry.indexOf("UNI")>=0) p.addSequenceRef("UNIPROT:"+entry.substring(entry.indexOf(':')+1));
            applyControlFields(field, p);
            p.OBJECTS[Protein.O_WEB_TOK]=entry;
            final String chains=p.getChainsAsString();
            p.OBJECTS[Protein.O_WEB_TOK1]=pdbId!=null && underscore<0 && sze(chains)>0 ? urlOrId+"_"+chains : urlOrId;
            p.OBJECTS[Protein.O_WEB_TOKENS]=allTokens;
            adUniq(p,vAll);
        }
        if ((options&OPTION_PROCESS_LOADED_PROTS)!=0) processLoadedProteins(options, proteins,targets);
        return proteins;
    }
    private static void applyControlFields(String field[], Protein p) {
        if (!isEDT()) inEdtLaterCR(getInstance(),"applyControlFields",new Object[]{field,p});
        else {
            int evt=-1;
            String f, v;
            if (sze(f=get(1,field))>0) {

                f=rplcToStrg("$ORGANISM_SCIENTIFIC", orS(p.getOrganismScientific(), p.getOrganism()), f);
                f=rplcToStrg("$NAME", p.getName(),f);
                if (f.indexOf(v="$SP1")>=0) f=rplcToStrg(v, delLstCmpnt(p.getSwissprotID(), '_'), f);
                if (f.indexOf(v="$ORGANISM5")>=0) f=rplcToStrg(v, p.getOrganism5(),f);
                if (f.indexOf(v="$PDB")>=0) f=rplcToStrg(v, delPfx("PDB:",p.getPdbID()),f);
                f=rplcToStrg("$ORGANISM", p.getOrganism(), f);
                f=rplcToStrg("$SP", p.getSwissprotID(), f);

                p.setName(f);
                evt=StrapEvent.PROTEIN_RENAMED;
            }
            if (sze(f=get(2,field))>0) { p.setIconImage(f); evt=StrapEvent.PROTEIN_RENAMED; }
            if (sze(f=get(3,field))>0) {
                for(String sel : splitTokns(f, chrClas1('#'))) {
                    int selStart=6;
                    while(chrAt(selStart,sel)==',') selStart++;
                    final String ranges=delSfx(',',sel.substring(selStart));
                    ResidueAnnotation a=p.getResidueAnnotationWithName(ranges);
                    if (a==null) (a=new ResidueAnnotation(p)).addE(0,ResidueAnnotation.NAME,ranges);
                    a.addE(0,ResidueAnnotation.POS, ranges);
                    a.setColor(C((int)hexToInt(sel)));
                    for(String cmd3d : ProteinViewer.STYLE_COMMANDS) {
                        if (0<=strstr(0L, cmd3d,3,MAX_INT, sel,0,MAX_INT))  a.addE(0,ResidueAnnotation.VIEW3D, cmd3d);
                    }
                    p.addResidueSelection(a);
                }
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
            }
            if (sze(f=get(4,field))>0) {
                final String s=f.charAt(0)=='#' ? get(atoi(f,1)-1,p.getCDS()) : f;
                p.parseCDS(s);
                evt=StrapEvent.NUCL_TRANSLATION_CHANGED;
            } else if (sze(p.getCDS())==1) {
                p.parseCDS(get(0,p.getCDS()));
            }
            if (sze(f=get(5,field))>0) {
                final String cmd=ProteinViewer.COMMANDbiomolecule+" 0x"+delPfx("0x",f.toLowerCase());
                for(ProteinViewer pv : p.getProteinViewers()) V3dUtils.sendCommand(0L, cmd,pv);
                pcp(Protein3d.KEY_INTERPRET_AFTER_LOADING,cmd,p);
            }
            if (evt!=0) StrapEvent.dispatchLater(evt,555);
        }
    }
    /* <<< Web Alignment <<< */
    /* ---------------------------------------- */
    /* >>> load protein >>> */
    /**
       Loads proteins from the project directory into Strap.
       @param listOfProteins A string containing the names of proteins to be loaded separated by space, \t or \n. Proteins can be groupped useing curly brackets.
       @return An array of all loaded proteins
    */
    public static Protein[][] loadTheProteinsInList(int options, CharSequence listOfProteins) {
        return getInstance()==null || sze(listOfProteins)==0?null: loadTheProteinsInList(options, listOfProteins, oo(StrapView.KEY_AP));
    }
    /**
       @return {proteins that are loaded  ,  proteins that already had been loaded}
    */
    private static Protein[][] loadTheProteinsInList(int options, CharSequence listOfProteinNames, Object targets[])  {
        if (sze(listOfProteinNames)==0) return null;
        if (!isEDT()) {
            final Protein[][] result={null,null};
            inEdtCR(getInstance(), "LPP", new Object[]{intObjct(options), listOfProteinNames, targets,result});
            return result;
        }
        final int opt=options|Strap.loadOptions();
        final ChJList jList=get(targets,ChJList.class);
        final UniqueList<Protein> vP=new UniqueList(Protein.class), vNew=new UniqueList(Protein.class);
        final Protein[] ppLoaded=proteins();
        int brace=0, braceCount=0;
        boolean isHidden=false;
        Protein addTo=null;
        BA ba=null;
        final Collection<String> vAlready=new HashSet();
        nextToken:

        for(String s00 : splitTokns(toStrg(listOfProteinNames).replace('\\','/'))) {
            if (nxt(LETTR_DIGT,s00)>=0 && !vAlready.add(s00)) continue;
            final String[] ss0;
            if (chrAt(0,s00)=='@') {
                ss0=splitTokns(readBytes(s00.substring(1)));
                if (sze(ss0)==0) { putln(RED_WARNING+"StrapAlign ","No such file ",s00); continue; }
            } else ss0=new String[]{s00};

                                                                                                               
            for(String s0 : ss0) {
                final char c0=chrAt(0,s0);
                final int shift=c0=='{'||c0=='}' ? 1:0;
                if (c0=='}') { brace--; braceCount=0; }
                if (c0=='{') brace++;
                final String s1=s0.substring(shift), s=delPfx("RNA:",delPfx("HETATM:",delPfx("DNA:", s1)));
                final boolean addHetero=s!=s1;
                if (sze(s)==0) continue;
                // for(int j=0;j<iS; j++)  if (s.equals(ss0[j])) continue nextTokein;
                if ("#HIDDEN".equals(s)) { isHidden=true; continue;}
                final int lastSlash=maxi(s.indexOf('/'),s.indexOf('\\'));
                final File f0=file(Protein.urlCd(true,s,lastSlash+1)), f=sze(f0)==0 && s.indexOf('!')>0 ? file(delLstCmpnt(s,'!')) : f0;
                {
                    final String fn=lstPathCmpnt(s);
                    Protein pL=proteinWithName(fn,vP.asArray());
                    if (pL==null) pL=proteinWithName(fn,ppLoaded);
                    if (pL==null && jList!=null) pL=SPUtils.proteinWithNameAndFile(fn,f, oo(jList.getList()));
                    if (pL!=null) { vP.add(pL); continue; }
                }
                if (sze(f)>0) {
                    ba=readBytes(f,ba);
                    //boolean error=strstr(STRSTR_IC, "No entries found", ba)>=0;
                    if (/*!error && */ba!=null) {
                        ba.insidePreTags(null);
                        final Protein[] ppMany=proteinsFromMsfText(opt, ba,f, 2, getInstance());
                        if (sze(ppMany)>0) {
                            adAll(ppMany,vNew);
                            adAll(ppMany,vP);
                            continue nextToken;
                        }
                        {
                            final Protein p=readProtein(s,ba);
                            final HeteroCompound[] hh=p==null?null:p.getHeteroCompounds('*');
                            final int nR=p==null?0:p.countResidues();
                            if (p!=null && nR+p.countNucleotides()+hh.length>0) {
                                if (p.getProteinParserClass()==StupidParser.class && (opt&OPTION_NOT_LOOKING_FOR_REFS_IN_WEBPAGES)==0) {
                                    final CharSequence refs=Hyperrefs.getProteinReferencesInHtmlFile(ba);
                                    if (sze(refs)>0) {
                                        startThrd(threadWebAlignment(OPTION_NOT_LOOKING_FOR_REFS_IN_WEBPAGES, refs,targets));
                                        continue nextToken;
                                    }
                                }
                                if (nR==0 && addTo!=null && (addHetero||brace>0)) {

                                    pcp(KEY_ADD_HET,wref(addTo),p);
                                }
                                if (!addHetero && nR>0 && braceCount==0) addTo=p;
                                vP.add(p);
                                vNew.add(p);
                                if (isHidden) pcp("isHidden", boolObjct(true), p);
                                if (brace>0 && braceCount++>0) p.OBJECTS[Protein.B_INTO_SAME_LINE]="";
                            }
                        }
                    }
                } else ba=null;
            }

        }
        setProgress(false);
        if ( (opt&OPTION_PROCESS_LOADED_PROTS)!=0) processLoadedProteins(opt, vP.asArray(),targets);
        if ( (opt&OPTION_DOWNLOAD_ORIGINAL_PROTEINS)!=0) SPUtils.downloadOriginalProteins(opt, true, vNew.asArray(), null,null);
        return new Protein[][]{vNew.asArray(), vP.asArray()};
    }

    public static void processLoadedProteins(int options, Protein[] pp, Object[] targets) {
        if (sze(pp)==0) return;
        if (!isEDT()) {
            inEDT(thrdM("processLoadedProteins", StrapAlign.class, new Object[]{intObjct(options), pp,targets}));
            return;
        }

        final Protein[] targetPP=get(targets,Protein[].class), ppLoaded=proteins();
        ProteinViewer viewer3D=null;
        StrapView view=null;
        ChJList jList=null;
        int targetRow=MIN_INT;
        for(int i=sze(targets); --i>=0;) {
            final Object t=targets[i];
            if (gcp(OBJECT_JLIST,t)==t) jList=gcp(OBJECT_JLIST,t,ChJList.class);
            if (view==null) view=gcp(StrapView.class,t, StrapView.class);
            if (t instanceof StrapView) view=(StrapView)t;
            if (t instanceof Integer) targetRow=atoi(t);
            if (t instanceof ProteinViewer) viewer3D=(ProteinViewer)t;
            if (viewer3D==null) viewer3D=gcp(V3dUtils.KEY_PROTEIN_VIEWER, t, ProteinViewer.class);
            if (viewer3D==null) viewer3D=deref(t, ProteinViewer.class);
        }
        final List<Protein> vP=new Vector(pp.length), v3D=new Vector(), v3D_hetero=new Vector(), vHidden=new Vector();
        final boolean skipIdentical=withGui() && button(TOG_SKIP_IDENTICAL_SEQUENCE).s();
        nextProtein:
        for(int iP=0; iP<pp.length; iP++) {
            final Protein p=pp[iP];
            if (p==null) continue;
            if (p.getCharacters().length>0) {
                if (p.getResidueCalpha()!=null) {
                    if (viewer3D!=null) V3dUtils.open3dViewer(V3dUtils.OPEN_INIT_SCRIPT, spp(p) , viewer3D);
                }
                if (skipIdentical) {
                    for(int jP=sze(vP); --jP>=0;) if (Protein.equalsResidueType(p,vP.get(jP))) continue nextProtein;
                    for(Protein q: ppLoaded) if (Protein.equalsResidueType(p,q))  continue nextProtein;
                }
                vP.add(p);
                if (gcp("isHidden",p)!=null) vHidden.add(p);
            } else {
                int countH=0;
                final File f=p.getFile();
                for(HeteroCompound h :  p.getHeteroCompounds('*')) {
                    h.setFile(f);
                    final Protein addTo;
                    if (targetPP!=null) {
                        addTo=sze(targetPP)==1 ? targetPP[0]:null;
                        if (addTo==null) drawMessage(ANSI_W_ON_B+"Nucleotide and hetero compounds can be dragged only on single proteins.");
                    } else addTo=gcp(KEY_ADD_HET, p, Protein.class);
                    if (jList!=null) {
                        adUniqR(h, targetRow, jList.getList());
                        ChDelay.repaint(jList,33);
                    }
                    if (addTo!=null) {
                        /*
                          Problem: 1vqp_4.pdb
                          ATOM  61658  C6   DC 4  75
                          HETATM61659  P   PPU 4  76
                        */
                        if (addTo.containsHeteroCompound(h)) drawMessage(ANSI_BLUE+ANSI_FG_WHITE+"DNA/RNA/Hetero "+h+" already associated to "+addTo);
                        else {
                            final long hOpt= 0!=(options&DROP_HETERO_INVERSE_MX) ? Protein.HETERO_ADD_APPLY_INVERSE_MX : 0L;
                            addTo.addHeteroCompounds(hOpt,h);
                            StrapEvent.dispatchLater(StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED,333);
                            drawMessage("@1 Associating "+f.getName()+" to "+addTo);
                        }
                    } else if ((options&OPTION_SHOW_ORPHAN_HETEROS_IN_3D)!=0) v3D_hetero.add(p);
                    countH++;
                }
                if (countH==0 && !p.isWaterInFile()) errorMsg("Error parsing "+f);
            }
            pcp("isHidden",null,p);
            //            pcp("brace",null,p);
            p.OBJECTS[Protein.B_PROCESSED]="";
        }
        final Protein[] proteins=spp(vP);
        if (proteins.length>0) {
            if (jList!=null) {
                for(Protein p : proteins) adUniqR(p, targetRow,  jList.getList());
                jList.repaint();
            } else {
                setIsInAlignment(true, -1, proteins);
                if (view==null && cntains(StrapView.KEY_AP,targets)) view=alignmentPanel();
                if (view!=null) view.addProteins(targetRow, proteins);
                Protein.incrementMC(MCA_ALIGNMENT,null);
                setIsInAlignment(false, -1, spp(vHidden));
            }
            for(int i=sze(targets); --i>=0;) runR(get(i,targets, Runnable.class));
            if ( (options&OPTION_EVENT_PROTEINS_ADDED)!=0) StrapEvent.dispatchLater(StrapEvent.PROTEINS_ADDED,111);
        }
        if (sze(v3D)>0) new3dBackbone(0, spp(v3D));
        if (sze(v3D_hetero)>0) new3dBackbone(0, spp(v3D_hetero));
        if (proteins().length>0 && withGui()) newAlignmentPanel(false);
    }
    /* <<< loadTheProteinsInList <<< */
    /* ---------------------------------------- */
    /* >>> MSA >>> */
    static Protein[] proteinsFromMsfText(int options, BA ba, File f, int minFA, StrapAlign align) {
        if (ba==null) return Protein.NONE;
        final Range msf_FromTo=new Range();
        final int format=MultipleSequenceParser.isFormat(0,ba,msf_FromTo, _msfCountP);
        Protein[] pp=null;
        final File ffChains[]= 0!=(options&OPTION_notSplitChains) ||  countTrue(PDB_separateChains.chainsInPDBfile('P',ba))<2 ? null : new PDB_separateChains().processFile(ba,f, false);
         if (sze(ffChains)>0) {
            pp=new Protein[ffChains.length];
            for(int i=ffChains.length; --i>=0;) pp[i]=Protein.newInstance(ffChains[i]);
            pp=SPUtils.assignHeterosToClosestPeptides(pp);
        } else if (format!=0 && _msfCountP[0]>=minFA) {
            final MultipleSequenceParser parser=new MultipleSequenceParser().setText(format, ba, msf_FromTo);
            final String[] nn=parser.getSequenceNames();
            final byte[][] gapped=parser.gappedSequences();
            if (nn==null) return Protein.NONE;
            if (nn.length>5000) {
                final BA sb=new BA(sze(ba))
                    .a(f).a("\nThe file contains more than 5000 sequences.\nTherefore identical sequences are omitted.\n #Sequences=")
                    .a(nn.length).aln("\n\n Skipped sequences:\n");
                final int nameLen=sze(longestName(nn)), hCodes[]=new int[nn.length];
                for(int i=nn.length; --i>=0;) hCodes[i]=hashCd(gapped[i],0,MAX_INT);
                final int[] hCodesSorted=hCodes.clone();
                Arrays.sort(hCodesSorted);
                final boolean[] alreadyTaken=new boolean[nn.length];
                for(int i=0; i<nn.length;i++) {
                    final int pos=Arrays.binarySearch(hCodesSorted, hCodes[i]);
                    if (pos<0) { assrt();continue; }
                    if (alreadyTaken[pos]) {
                        sb.a(nn[i]).a(' ',maxi(1,nameLen+2-sze(nn[i]))).aln(gapped[i]);
                        nn[i]=null;
                        gapped[i]=null;
                    } else alreadyTaken[pos]=true;
                }
                shwTxtInW("Skipped sequences",sb.trimSize());
            }
            pp=new Protein[nn.length];
            final int[] first=parser.getFirstResidueIndex(), last=parser.getLastResidueIndex();
            final byte secStru[][]=parser.getSecondaryStructures();
            final String accID[]=parser.getAccessionIDs();
            final String seqRefs[][]=parser.getSequenceRefs();
            final String sharedDbRefs[]=parser.getSharedDatabaseRefs();
            final String fn0=f!=null ? delDotSfx(f.getName()) : null, pfam;
            final boolean isStockholm=format==MultipleSequenceParser.STOCKHOLM;
            if (sze(fn0)>2) {
                int digi0=-1;
                for(int i=sze(fn0); --i>=0;) if (is(DIGT,fn0, i)) digi0=i; else break;
                final char c0=fn0.charAt(0), c1=fn0.charAt(1);
                pfam=isStockholm && digi0==2 && c0=='P' && c1=='F' ? "PFAM:"+fn0 : null;
            } else pfam=null;
            final Protein[] ppLoaded=proteins();
            final ImageIcon icn=iicon(pfam!=null ? IC_PFAM : null);
            final String[] dirAnnoListing=lstDir(Protein.strapFile(Protein.mANNO,null,null));
            final String[] keys=Hyperrefs.getDatabases(Hyperrefs.PROTEIN_FILE)[0];
            final byte[] char0_ofKeys=new byte[keys.length];
            int char0_ofKeysCount=0;
            for(String key : keys) {
                final char c0=key.charAt(0);
                if (c0>='A' && strchr(c0,char0_ofKeys,0,char0_ofKeysCount)<0) char0_ofKeys[char0_ofKeysCount++]=(byte)c0;
            }
            for(int iP=pp.length; --iP>=0;) {
                if (nn[iP]==null) continue;
                final String pn0=nn[iP], pn=pn0.replace('/','_').replace('|','_');
                Protein p=proteinWithName(pn,ppLoaded);
                if (p==null) (p=new Protein()).setProteinParserClass(MultipleSequenceParser.class);
                (pp[iP]=p).setName(pn);
                { /* Accession ID */
                    final char c0=chrAt(0,pn0);
                    final int colon=pn0.indexOf(':');
                    String foundKey=colon>0 && c0=='P' && colon==3 && strEquls("PDB", pn0,0) ? "PDB:" : colon>0 && c0=='E' && strEquls("ENSEMBL", pn0,0) ? "ENSEMBL:" : null;
                    if (foundKey==null && colon>0) {
                        if (foundKey==null) {
                            if (strchr(c0, char0_ofKeys,0,char0_ofKeysCount)>=0) {
                                for(String key : keys) {
                                    final int keyL=sze(key);
                                    if (colon+1==keyL && key.charAt(0)==c0 && strEquls( key, pn0,0) ) {
                                        foundKey=key;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (foundKey!=null) {
                        final boolean isPDB=foundKey=="PDB:";
                        final String id=wordAt(colon+1, pn0,  isPDB ? LETTR_DIGT_US_COLON : LETTR_DIGT);
                        p.setAccessionID(foundKey+id);
                        p.setName(id.replace(':', '_'));
                    } else if (sze(pn)>6 && cntainsOnly(UPPR_DIGT_US,pn)) p.addSequenceRef(pn);
                }
                p.setGappedSequence(gapped[iP],MAX_INT);
                if (secStru!=null) p.setResidueSecStrType(secStru[iP]);
                if (seqRefs!=null && seqRefs[iP]!=null) for(String s : seqRefs[iP]) p.addSequenceRef(s);
                if (sharedDbRefs!=null) for(String s : sharedDbRefs) p.addDatabaseRef(s);
                p.setFile(f);
                p.setIsInMsfFile(true);
                for(int i=sze(dirAnnoListing); --i>=0;) {
                    if (dirAnnoListing[i].startsWith(pn)) {
                        readAttributes("",p,(File)null);
                        p.findIconImage();
                        break;
                    }
                }

                if (sze(accID)==pp.length) p.setAccessionID(accID[iP]);
                if (sze(first)==pp.length && sze(last)==pp.length) p.setResidueIndexOffset(first[iP]);
                if (pfam!=null) p.addDatabaseRef(pfam);
                if (icn!=null) p.run(ChRunnable.RUN_SET_ICON_IMAGE,icn.getImage());
            }
            SPUtils.setProteinIcon(pp,false,'A');
            final int fromTo[]=SPUtils.leadingAndTrailingLowerCaseLettersInMSA(pp);
            for (Protein p : pp) {
                if (p!=null) {
                    if (fromTo!=null) SPUtils.setResidueSubsetFromColumnRange(p, fromTo[0], fromTo[1]);
                    p.setAlignment(align); /* muss nach leadingAndTrailingLowerCaseLettersInMSA */
                }
            }
        }
        final int countMultiSwiss=sze(pp)==0 ? NumberedSequence_Parser.parseMultipleSwiss(ba, null) : 0;
        if (countMultiSwiss>=minFA) {
            pp=new Protein[countMultiSwiss];
            for(int i=countMultiSwiss; --i>=0;) {
                pp[i]=new Protein(align);
                pp[i].setIsInMsfFile(true);
                pp[i].setFile(f);
            }
            NumberedSequence_Parser.parseMultipleSwiss(ba,pp);
        }
        return pp;
    }
    /* <<< MSA <<< */
    /* ---------------------------------------- */
    /* >>> PDB >>> */
    static Protein[] downloadPdbAndCreateProteins(String pdbId, char chain, Protein[] ppLoaded) {
        if (myComputer() && sze(pdbId)!=4) { putln("pdbId=", pdbId); stckTrc(); }
        if (chain!=0) {
            Protein pAlready=proteinWithName(pdbId+"_"+chain+".pdb" ,ppLoaded);
            if (pAlready==null) pAlready=proteinWithName("PDB:"+pdbId+"_"+chain ,ppLoaded);
            return pAlready!=null ? spp(pAlready) : proteinInstances(new File[]{downloadPdbChain(pdbId,chain)},ppLoaded);
        }

        return proteinInstances(downloadAllPdbChains(pdbId, chain),ppLoaded);
    }
    private static Protein[] proteinInstances(File[] ff, Protein[] ppAlready) {
        final Protein[] pp=new Protein[ff.length];
        BA buffer=null;
        for(int i=0; i<ff.length; i++) {
            final String name=ff[i].getName();
            final Protein
                pAlready=proteinWithName(name,ppAlready),
                p=pAlready!=null ? pAlready : readProtein(name, buffer=readBytes(ff[i],buffer));
            if (p!=null && sze(p.getResidueType()) + sze(p.getHeteroCompounds('N')) + sze(p.getHeteroCompounds('H'))>0)   (pp[i]=p).setFile(ff[i]);
        }
        return delet(null,pp,Protein.class);
    }
    /* <<< PDB <<< */
    /* ---------------------------------------- */
    /* <<< Save Proteins  <<< */

    public static void setNotSaved() { _unsaved=true; }
    static boolean needsSaving() { return _unsaved;}
    static boolean save() {
        final File dir=0!=(Protein.optionsG()&Protein.G_OPT_VIEWER)?dirStrapAnno():dirWorking();
        final boolean success=saveAlignment(dir);
        if (success) drawMessage("@1"+ANSI_GREEN+" Saved to "+fPathUnix(dir));
        mkProtList(false);
        return success;
    }
    private static boolean saveAlignment(File dir) {
        _unsaved=false;
        mkdrsErr( Protein.strapFile(Protein.mANNO,null,dir));
        mkdrsErr( Protein.strapFile(Protein.mGAPS,null,dir));
        final BA error=new BA(9);
        for(Protein p : proteins())  if (!p.isTransient()) p.save(0, dir,error);
        if (sze(error)>0)  { error(error); return false; }
        return true;
    }
    private static ChTextField _zipTf;
    private static ChButton _zipB;
    private void zipAlignmentProject(String file,int awtModi) {
        final File dir=file(STRAPOUT+"/zip");
        if (sze(file)==0) {
            if (_zipPrj==null) {
                final Object
                    pRecover=pnl(VBHB,
                                 "1. Extract the zip file into a new project directory.", " ",
                                 "2. Start Strap, select the directory with the extracted files as the project directory.", " "
                                 );
                _zipPrj=pnl(VBHB,
                            "ZIP archive of the alignment project will be created",
                            " ",
                            pnl(HB,"File name: ", _zipTf=new ChTextField("alignment.zip").li(li())),
                            " ",
                            _zipB=new ChButton("Make zip-file").li(li()).tt(""),
                            " ",
                            pnl(HBL,pnlTogglOpts("*Recover the project from the zip-file: ",pRecover)),
                            pRecover,
                            " "
                            );
            }
            _zipTf.tools().saveInFile("zipFile").enableWordCompletion(dir).underlineRefs(ULREFS_NOT_CLICKABLE);
            ChFrame.frame("Zip archive project", _zipPrj, ChFrame.PACK).shw(ChFrame.AT_CLICK);
        } else {
            final List<File> v=new Vector();
            final File fZip=file(file(STRAPOUT), delSfx(".zip", file)+".zip"), dirZP=file(dirTmp(),"zip"), dirZ=file(dirZP,dirWorking().getName());
            deleteTree(dirZP);
            mkdrsErr(dirZ);
            Protein.strapFile(Protein.mGAPS, null, dirZ);
            Protein.strapFile(Protein.mANNO, null, dirZ);
            for(Protein p: ppAliV().asArray()) {
                p.save(0, dirZ,null);
                adUniq(p.getFileMayBeWithSideChains(),v);
                adUniq(p.getFile(),v);
                for(HeteroCompound dna: p.getHeteroCompounds('*')) adUniq(dna.getFile(), v);
            }
            v.add(mkProtList(true));
            final Map<File,File[]> data=new HashMap();
            final File ff[]=toArry(v,File.class);
            final String dirW=dirWorking().toString();
            for(int i=ff.length; --i>=0;) { /* Proteins from other dir */
                if (!dirW.equals(toStrg(ff[i].getParentFile()))) {
                    cpy(ff[i],file(dirZ, ff[i].getName()));
                    ff[i]=null;
                }
            }
            data.put(dirWorking().getParentFile(), ff);
            data.put(dirZP, lstDirF(dirZ));
            ChZip.makeZipFile(fZip, data ,null);
            _zipB.addDnD(fZip);
            viewFile(dir,awtModi);
        }
    }
    private static void mkProtList1(boolean noPath, Protein p, BA sb, Collection<String> vMsf) {
        final String name=Protein.urlCd(true,toStrg(p),0);
        final File f=p.getFile();
        if (p.isInMsfFile() && f!=null) {
            final String path=noPath ? f.getName() : toStrg(f);
            if (vMsf.add(path)) sb.a(path);
        } else {
            final String wDir=toStrg(dirWorking());
            if (f==null || noPath && wDir.equals(toStrg(f.getParentFile()))) sb.a(name); else sb.a(f.getParentFile()).a('/').a(name);
            for(HeteroCompound h:  p.getHeteroCompounds('*')) {
                final File fH=h.getFile();
                if (fH!=null) sb.a(!h.isNucleotideChain()?" HETATM:": h.isDNA() ?" DNA:":" RNA:")
                                  .a(noPath && wDir.equals(fH.getParentFile()) ? fH.getName() : toStrg(fH))
                                  .a(' ');
            }
        }
        sb.a('\n');
    }
    private static File mkProtList(boolean noPath) {
        final Collection<String> vMsf=new HashSet();
        final Collection<Protein> vDone=new HashSet();
        final Protein pp[]=proteins();
        final BA sb=new BA(33*pp.length);
        final StrapView view=alignmentPanel();
        final int nRow=view==null?0 : view.countRows();
        for(int row=0; row<nRow; row++) {
            final Protein ppRow[]=view.proteinsInRow(row);
            if (ppRow.length>1) sb.aln('{');
            for(Protein p: ppRow) { mkProtList1(noPath, p,sb,vMsf); vDone.add(p); }
            if (ppRow.length>1) sb.aln('}');
        }
        int count=0;
        for(Protein p : pp) {
            if (!vDone.add(p)) continue;
            if (count++ ==0) sb.aln("\n\n#HIDDEN\n");
            mkProtList1(noPath, p,sb,vMsf);
        }
        final File f=file("proteins.list");
        if (sze(sb)>0) wrte(f,sb);
        return f;
    }
    /* <<< Save Proteins <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    public Object run(String id,Object arg) {
        if (id==StrapEvent.RUN_DISPATCH) { dispatch((StrapEvent)arg);}
        if (id==Protein.RUN_ALIGNMENT_GET_PROTEINS) { return proteins();}
        if (id==Protein.RUN_ALIGNMENT_GET_GAP2COL ) return g2c();
        if (id==Protein.RUN_ALIGNMENT_GET_PROTEIN_LABEL) return arg==null?null: new ProteinLabel(0L,(Protein)arg);
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        final boolean isViewer=0!=(Protein.optionsG()&Protein.G_OPT_VIEWER);
        if (id=="CSA") CsaWeb.load( (Protein[])arg, null);
        if (id==ExecByRegex.RUN_SCRIPT) {
            final String script=(String)argv[0];
            final int opt=atoi(argv[1]);
            final boolean debug=0!=(opt&ExecByRegex.DEBUG);
            final long time=System.currentTimeMillis();
            final BA data=SPUtils.alignmentToXML(opt, script);
            BA response=null;
            if (strEquls("http://",script)) {
                response=Web.getServerResponse(0L, script, new Object[][]{{"strap", data}});
                if (debug && response==null) error("No response from "+script);
                else if (strEquls("RESET_CACHE",response.trim())) {
                    //putln(YELLOW_DEBUG+"RESET_CACHE",script);
                    for(Protein p : proteins()) p.clearCache();
                    response=Web.getServerResponse(0L, script, new Object[][]{{"strap", data}});
                } else if (debug && !looks(LIKE_HTML,response)) shwTxtInW("Server response "+script, response);
            } else if (sze(script)!=0) {
                File f=file(dirTmp()+"/strap.xml");
                delFile(false,f);
                if (sze(f)>0) delFileOnExit(f=file(dirTmp()+"/strap"+time+".xml"));
                wrte(f,data);
                debugTime(" Created xml file "+f,time);
                if (debug) ChExec.log().send();
                final ChExec ex=new ChExec(ChExec.STDOUT|(debug?ChExec.SHOW_STREAMS:0)).setCommandLine(script+" "+f);
                ex.run();
                response=new BA(ex.getStdout());
            }
            if (sze(response)>0) new StrapScriptInterpreter().runScripts(new CharSequence[]{response});
        }
        if (id=="SHOPPING") {
            final boolean busy=_shoppingBusy-->0;
            if (busy || !busy && _shoppingBusyLast) _shopping.repaint();
            _shoppingBusyLast=busy;
            return null;
        }
        if (id==Protein.RUN_ALIGNMENT_GET_DRAG_OPTIONS) return intObjct(dragOptions());
        if (id==Protein.RUN_ALIGNMENT_ADD_AWT_LISTENERS) {
            addAwtListeners(arg);
            newDropTarget(arg,false);
        }
        if (id==ChRunnable.RUN_SET_PROGRESS) drawMessage( get(PRGRSS_TXT, argv,String.class));
        if (id==ChRunnable.RUN_APPEND || id==ChRunnable.RUN_SAY_DOWNLOADING) drawMessage(toStrg(arg));
        if (id==ChScriptPanel.RUN_SCRIPT && argv[0]=="STRAP") new StrapScriptInterpreter().interpretScript((BA)argv[1]);
        if (id==RUN_CHECK_PLUGIN_UPTODATE) {
            List<File> v=null;
            for(URL u : (URL[])arg) {
                final File f=StrapPlugin.jarFile(u);
                if (sze(f)>0 && !vPluginNotUpToDate.contains(f) && 0==isUpToDate(f,u)) (v==null?v=new Vector():v).add(f);
            }
            if (v!=null) inEdtLaterCR(this,"ASK_UPDATE_PLUGIN",toArry(v,File.class));
        }
        if (id=="ASK_UPDATE_PLUGIN") {
            final File ff[]=(File[])arg;
            final Object
                pNorth="The following plugin files are not up-to-date:",
                pCenter=scrllpnT(new ChJList(ff,ChJList.OPTIONS_FILES)),
                pSouth="Delete these files to force downloading the most recent version the next time?";
            if (ChMsg.yesNo(pnl(CNSEW,pCenter,pNorth,pSouth))) {
                //ChClassLoader.newInstance();
                for(File f : ff) delFile(f);
            } else for(File f : ff) vPluginNotUpToDate.add(f);
        }
        if (id=="applyControlFields") applyControlFields((String[])argv[0], (Protein)argv[1]);
        if (id=="newAlignmentPanel") newAlignmentPanel(false);
        if (id=="dragFailed" && arg!=null) {
            if (vDragFailed.add(arg)) {
                final String addr=new BA("http://www.bioinformatics.org/strap/error.php?"+DO_NOT_ASK+"&error="+ANSI_YELLOW+"DRAG"+ANSI_RESET+"_")
                    .filter(FILTER_URL_ENCODE, arg).toString();
                readBytes(addr);
            }
        }
        if (id=="LPP") {
            final Protein pp[][]=loadTheProteinsInList(atoi(argv[0]), (CharSequence)argv[1],(Object[])argv[2]);
            if (pp!=null) System.arraycopy(pp,0,((Protein[][])argv[3]),0,pp.length);
        }
        if (id=="EXIT") {
            if (!isViewer) mkProtList(true);
            if (_taNotes!=null) wrte(file("notepad.txt"), toBA(_taNotes));
        }
        if (id=="LOAD_P") ((Protein[])argv[2])[0]=readProtein((String)argv[0],(BA)argv[1]);
        if (id=="WATCH_PROTEIN_FILES") {
            final long time=System.currentTimeMillis();
            boolean reloaded=false;
            Protein dragExit[]=null;
            UniqueList<Protein> vDelete=null;
            nextProtein:
            for(Protein p:proteins()) {
                if (p.getProteinParserClass()==MultipleSequenceParser.class) continue;
                final File f=p.getFile();
                if (f==null) continue;
                final long lm=f.lastModified();
                final boolean lmSet=p.mc(MC_FILE_CONTENT)!=0;
                if (p.setMC(MC_FILE_CONTENT,(int)(lm-TIME_AT_START))) {
                    if (lm==0) {
                        if (dragExit==null) {
                            final File ff[]=getDragFiles();
                            dragExit=new Protein[ff.length];
                            for(int iF=ff.length; --iF>=0;) {
                                dragExit[iF]=SPUtils.proteinWithNameAndFile(null,ff[iF],proteins());
                            }
                        }
                        if (cntains(p, dragExit))  (vDelete==null ? vDelete=new UniqueList(Protein.class) : vDelete).add(p);
                    } else if (lmSet && _reloadProts!=3) {
                        inEdtLaterCR(this,"RELOAD_PROTEIN",new Object[]{p,f});
                        reloaded=true;
                    }
                }
            }
            if (vDelete!=null) rmProteins(true,vDelete.asArray());
            _threadWatchFiles.setEveryMs(maxi(999, reloaded ? 333 : (int)(System.currentTimeMillis()-time)*99));
        }
        if (id=="RELOAD_PROTEIN") {
            final Protein p=(Protein)argv[0];
            final File f=(File)argv[1];
            if (_reloadProts!=1) _reloadProts=ChMsg.option("The file "+f+"<br>for protein "+p+ " changed on disk.<br>reload ?", new String[]{"Yes","Always","No","Never"});
            if (_reloadProts==0 || _reloadProts==1) {

                SPUtils.parseProtein(p.getFile(),null, 0,p);
                StrapEvent.dispatchLater(StrapEvent.CHARACTER_SEQUENCE_CHANGED,1);
                if (p.getResidueCalpha()!=null) StrapEvent.dispatchLater(StrapEvent.ATOM_COORDINATES_CHANGED,1);
            }
        }
        if (id==ChRunnable.RUN_MODIFY_RENDERER_COMPONENT) {
            SPUtils.allFiles();
            final File f=get(0,argv,File.class);
            final Component jc=get(1,argv,Component.class);
            if (f!=null && jc!=null) {
                final boolean loaded=SPUtils.allFiles().contains(toStrg(f));
                if (loaded) setFG(0xb200,jc);
                final BA tip=FilePopup.tip(f);
                if (loaded) tip.insert(0,"<b>Already Loaded</b><br>");
                setTip(tip,jc);
                if (jc instanceof JLabel && loaded) setIcn(iicon(IC_ALIGN+"16x16"),jc);
            }

        }
        if (id==PROVIDE_JPopupMenu) {
            final AWTEvent ev=(AWTEvent)arg;
            final ChTextComponents tools=ev==null?null: ChTextComponents.tools(ev.getSource());
            if (tools!=null) {
                final Object oo[]=proteinsAndAnnotationsWithRegex( delPfx('>',tools.getWordUnderMouse(null)), proteins(), 'A');
                final char type=idxOfInst(0L, ResidueAnnotation.class, oo)>0 ? 'A' : 'P';
                return TabItemTipIcon.set(null,
                                          type=='A' ? "Menu for residue annotation ..." : type=='P' ? "Menu for protein ..." : null, null,
                                          type=='P' ? IC_3D : type=='A' ? IC_ANNO : null,
                                          ContextObjects.popupMenu(type, oo, oo));

            }
        }
        if (id=="ANIM" || id=="ANIMedt") {
            final Protein p=deref(argv[0], Protein.class);
            final int radius_iA[]=(int[])argv[1], iA=radius_iA[1];
            if (id=="ANIM") {
                for(int radius=40; (radius-=5)>=0;) {
                    radius_iA[0]=radius;
                    inEdtLaterCR(this,"ANIMedt",argv);
                    sleep(100);
                }
                for(StrapView v : alignmentPanels()) {
                    repaintC(v.alignmentPane());
                    repaintC(v.hsb());
                }
            } else {
                for(StrapView v : alignmentPanels()) {
                    final Component ali=v.alignmentPane(), sb=v.hsb();
                    final int row=v.findRow(p), col=p.getResidueColumnZ(iA);
                    if (row>=0 && col>=0) {
                        final int radius=2*radius_iA[0], r2=radius*2;
                        for(int isSB=2; --isSB>=0;) {
                            final Component c=isSB!=0?sb:ali;
                            final Graphics g=!isVisbl(c)?null:c.getGraphics();
                            if (g==null) continue;
                            final Rectangle r=v.rowCol2rect(row,col,c, RECT);
                            for(int i=0;i<3; i++) {
                                g.setColor(C(i==0 || i==2 ? 0xFFffFF : 0xFF));
                                final int j=i==0?0: i==1?2: 1;
                                g.drawOval(x(r)+wdth(r)/2 - radius+j, y(r)+hght(r)/2 - radius+j,  r2+2*j,r2+2*j);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    private final static Rectangle RECT=new Rectangle();
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> Protein3d >>> */
    public static Runnable thread_new3dBackbone(long options,Protein pp[], Protein3d[] ret) {
        return thrdMR("new3dBackbone",StrapAlign.class, new Object[]{longObjct(options), pp}, ret);
    }
    public static Protein3d new3dBackbone(long options,Protein pp[]) {
        if (!isEDT()) {
            final Protein3d[] ret={null};
            inEDT(thread_new3dBackbone(options,pp,ret));
            return ret[0];
        }
        TabItemTipIcon.set(null,null,null,IC_3D, Protein3d.PView.class);
        TabItemTipIcon.set(null,null,null,IC_3D, Protein3d.class);
        return (Protein3d) Protein3dUtils.viewerCanvas(V3dUtils.open3dViewer(options, pp, Protein3d.PView.class));
    }
    public static Protein3d findBackbone3D(boolean exactly,Protein ... pp) {
        return (Protein3d) Protein3dUtils.viewerCanvas(Protein3dUtils.findProteinViewerWithProteins(Protein3d.PView.class, pp, exactly));
    }
    private final static Map<String,ChTableLayout> _mapTP=new HashMap();
    public boolean existsTablePanel(String className) { return _mapTP.get(className)!=null;}
    public static ChTableLayout tPanel(String className) {
        ChTableLayout tl;
        synchronized(_mapTP) { tl=_mapTP.get(className);}
        if (tl==null && !isEDT()) {
            inEDT(thrdM("tPanel", StrapAlign.class, new Object[]{className}));
            synchronized(_mapTP) { tl=_mapTP.get(className);}
        }
        if (tl==null) {
            tl=new ChTableLayout(ChJTable.CHJTABLE_FOCUSED_BLACK|ChJTable.CHJTABLE_SHOW_BUT_MAXIM);
            synchronized(_mapTP) { _mapTP.put(className, tl);}
            addActLi(li(),tl);
            TabItemTipIcon.set(dTab(className),"",dTip(className), dIIcon(className), tl);
            if (className==CLASS_Protein3d || isAssignblFrm(ProteinViewer.class, name2class(className))) pcp(ChTabPane.KEY_SORT,"3D",tl);
        }
        return tl;
    }
    /* <<< Protein3d <<< */
    /* ---------------------------------------- */
    /* >>> StrapListener >>> */
    private boolean handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        if (!withGui()) return false;
        if (_watchEv!=null && isVisbl(_watchEv)) {
            final BA ba=_watchEv.byteArray();
            if (sze(ba)>9999) ba.set(ba.newBytes(888,MAX_INT),0,MAX_INT);
            ba.aln(ev);
            ChDelay.repaint(_watchEv,333);
        }
        if ( (t&StrapEvent.FLAG_ROW_HEADER_CHANGED)!=0) {
            ChDelay.repaint(_panAnno,333);
            updateAllNow(CHANGED_PROTEIN_LABEL|UPDATE_DO_REPAINT);
            DialogRenameProteins.update();
        }
        if (t==StrapEvent.ORDER_OF_PROTEINS_CHANGED) updateAllNow(CHANGED_PROTEIN_ORDER);
        if ( (t&StrapEvent.FLAG_CHILDS_OF_PROTEINS_CHANGED)!=0) updateAllNow(CHANGED_CHILDS_OF_PROTEIN|UPDATE_DO_UPDATE_TREES);
        if (t==StrapEvent.OBJECTS_SELECTED) {
            if (ev.getSource()!=tree()) selectObjectsIE(selectedObjectsV(),tree());
            setCntxtObj(selectedObjectsV());
            undockEnableDisable(null);
            SequenceFeatures.fChanged(true);
        }
        if (t==StrapEvent.OBJECTS_SELECTED || (t&StrapEvent.FLAG_ROW_HEADER_CHANGED)!=0) {
            for(int i=sze(_vNumPP); --i>=0;) setNumOfProteins(deref(_vNumPP.get(i), AbstractButton.class));
        }
        if (t==StrapEvent.RESIDUE_SELECTION_CHANGED || t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR) SequenceFeatures.updateTable(99);
        if (t==StrapEvent.CURSOR_CHANGED_PROTEIN) {
            PROTEIN_AT_CURSOR[0]=cursorProtein();
            updateAllNow(CHANGED_PROTEIN_AT_CURSOR|UPDATE_DO_REPAINT);
            pMenu();
        }
        if (t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN || t==StrapEvent.CURSOR_CHANGED_PROTEIN) doRepaintCursor(true);
        if (t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN) StrapEvent.dispatchLater(StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN_DELAYED, 222);
        if ( (t& (StrapEvent.FLAG_RESIDUE_TYPES_CHANGED|StrapEvent.FLAG_ALIGNMENT_CHANGED))!=0) Protein.incrementMC(MCA_ALIGNMENT,null);
        if (t==StrapEvent.VIEWER3D_MOLECULE_LOADED) {
            final ProteinViewer pv=(ProteinViewer)ev.getSource();
            final Protein p=SPUtils.proteinWithNameAndFile(null, (File)ev.parameters()[0], proteins());
            if (p!=null && !cntains(pv, p.getProteinViewers())) V3dUtils.open3dViewer(0L, new Protein[]{p}, pv);
            return false;
        }
        tree().maybeUpdate();
        return true;
    }
    private static ChJList _jlListeners;
    private static ChTextView _watchEv;
    public static void addListener(StrapListener sl) {
        if (adUniq(wref(sl),_vLis)) {
            StrapEvent.dispatch(StrapEvent.LISTENER_ADDED);
            revalAndRepaintC(_jlListeners);
        }
    }
    /**
       Removes the object from the list of listeners. If the object is not in the list nothing happens.
       @param sl Object which might implement StrapListener
    */

    public static void rmListener(Object sl)  {
        _vLis.remove(wref(sl,false));
        revalAndRepaintC(_jlListeners);
    }
    /**  Broadcasts an event. Use <i>JAVADOC:StrapEvent#run()</i> instead.   */
    public static void highlightProteins(String options, Object jc) {
        final ChTextComponents t=ChTextComponents.tools(jc);
        if (t==null) return;
        _vHlProteins.add(wref(jc));
        t.enableUndo(true);
        final boolean
            isP=strchr('P',options)>=0,
            isA=strchr('A',options)>=0;
        final Object compl[]={
            !isP?null:vProteins(),
            !isA?null: ResidueAnnotation.vAll,
            strchr('D',options)<0?null: dirWorking(),
            strchr('H',options)<0?null: ChTextComponents.COMPL_HYPERREFS
        };
        if (countNotNull(compl)>0) t.enableWordCompletion(compl);
        t.highlightOccurrence(ResSelUtils.instance((isA?ResSelUtils.HIGHLIGHT_A:0)|(isP?ResSelUtils.HIGHLIGHT_P:0)),
                               chrClas(-FILENM), isA ? chrClas("/ \r\t\n"):chrClas(SPC),
                               HIGHLIGHT_UPDATE_IF_TEXT_CHANGES|HIGHLIGHT_STYLE_RRECT|HIGHLIGHT_NOT_IN_SCROLLBAR, C(DEFAULT_BACKGROUND));

    }

    public void dispatch(StrapEvent ev)  {
        if (ev==null) return;
        final int t=ev.getType();
        final Object q=ev.getSource();
        { /* Check event parameters */
            final Object[] pp=ev.parameters();
            final boolean ok=
                t==StrapEvent.PROTEIN_VIEWER_PICKED ?  get(0,pp) instanceof Selection3D && get(1,pp) instanceof Integer :
                t==StrapEvent.SCRIPT_LINE ? pp instanceof String[] :
                t==StrapEvent.VIEWER3D_MOLECULE_LOADED ? get(0,pp) instanceof File :
                (t==StrapEvent.PROTEINS_SHOWN || t==StrapEvent.PROTEINS_ADDED) ? pp.length==0 || get(0,pp) instanceof Protein[] && get(1,pp) instanceof Integer :
                true;
            if (!ok) {
                putln(RED_ERROR,"Wrong parameters for StrapEvent ",ev,"\n");
                if (myComputer()) stckTrc();
            }
        }
        if (!isEDT()) inEdtLater(ev);
        else {
            if (t==StrapEvent.PROTEINS_ADDED || t==StrapEvent.PROTEINS_KILLED) {
                for(int i=sze(_vHlProteins); --i>=0;) {
                    final Component jc=getRmNull(i,_vHlProteins, Component.class);
                    final ChTextComponents tools=isVisbl(jc)?ChTextComponents.tools(jc):null;
                    if (tools!=null) inEDTms(tools.run('H'),333);
                }
                Protein.incrementMC(MCA_ALIGNMENT,null);
            }
            V3dUtils.handleEvent(this,ev);
            if (q!=this && q!=StrapAlign.class && !handleEvent(ev)) return;
            final List v=_vLis;
            for(int i=sze(v); --i>=0;) {
                final StrapListener li=getRmNull(i,v,StrapListener.class);
                if (li!=null) li.handleEvent(ev);
            }
            if (t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN || t==StrapEvent.CURSOR_CHANGED_PROTEIN) _whenCursor=System.currentTimeMillis();
            if (t==StrapEvent.OBJECTS_SELECTED) {
                updateAllNow(CHANGED_SELECTED_OBJECTS);
                long mc=selectedObjectsMC('P');
                if (_selPmc!=mc) {
                    _selPmc=mc;
                    StrapEvent.dispatch(StrapEvent.PROTEIN_SELECTED);
                }
                mc=selectedObjectsMC('s');
                if (_selSmc!=mc) {
                    _selPmc=mc;
                    StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_SELECTED);
                }
            }
        }
    }
    /** Events are dispatch with some latence. Concurrent events of same type are dispatched as one. */
    /* <<< StrapListener <<< */
    /* ---------------------------------------- */
    /* >>> Dialogs >>> */
    final static Map<Class,Object> MAP_DIALOGS=new HashMap();

    private static JComponent addDialogPrepare(JComponent o) {
        for(Component c : childsR(o, JComponent.class)) {
            final Container parent=parentC(c), grandP=parentC(parent);
            if (parent instanceof JViewport && grandP instanceof JScrollPane) {
                if (c instanceof ChTextArea || c instanceof JList) setMinSze(1,4*EX, grandP);
                if (c instanceof ChPanel) setMinSze(prefW(c),prefH(c)+EX, grandP);
            }
        }
        try { ((JTabbedPane) o).setSelectedIndex(0); } catch (Exception e) {}
        final StrapView v=alignmentPanel();
        if (v!=null) {
            final int avail=hght(_panRight)-EX-maxi(10*EX, hght(o.getMinimumSize()))-hght(v.hsb())- hght(StrapView.toolbars());
            v.reduceHeight(prefH(v.alignmentPane()) - avail);
        }
        return o;
    }

    /**
       Adds a dialog to the dialog panel or brings it to front.
       <ul>
       <li>1st case: o is an instance of JComponent</li>
       <li>2nd case: o instanceof HasPanel</li>
       </ul>
       See <i>JAVADOC:TabItemTipIcon</i>
    */
    public static void addDialog(JComponent comp) {
        if (comp==null || getInstance()==null) return;
        if (!isEDT()) inEdtLater(thrdM("addDialog",StrapAlign.class, new Object[]{comp}));
        else {
            if (gcp(ChTabPane.KEY_SORT,comp)==null)
                pcp(ChTabPane.KEY_SORT, _mapMenuItem2Topic.get(comp.getClass().getName()),comp);
            addDialogPrepare(comp);
            dialogPanel().addTab(CLOSE_ALLOWED, comp);
            setMenuBar(null,0,0);
            setToolpane(null);
            MAP_DIALOGS.put(comp.getClass(),wref(comp));
            addDialogPrepare(comp);
        }
    }
     /**
       Make an instance of a dialog and add it to the dialog pane.
       The constructor either has no parameter or StrapAlign.this as parameter.
    */
    public static JComponent addDialogC(Object classOrNameOfClass) { return addDialogC(false, classOrNameOfClass); }
    public static JComponent addDialogC(boolean multiInstances, Object classOrNameOfClass) {
        final Class clas;
        try {
            clas=
                getInstance()==null ? null :
                classOrNameOfClass instanceof String ? Class.forName((String)classOrNameOfClass):
                classOrNameOfClass instanceof Class ? (Class)classOrNameOfClass :
                null;
        } catch(Exception e){ stckTrc(e); return null; }

        if (clas==null) return null;
        final String key="SA$$SDC", cn=clas.getName();
        final ChTabPane panDialogs=dialogPanel();
        if (!multiInstances) {
            for(JComponent dialog : panDialogs.tabComponents()) {
                if (gcp(key,dialog)!=cn) continue;
                addDialog(dialog);
                return dialog;
            }
        }
        JComponent jc=null;
        try {
            jc=deref(getPnl(clas.getConstructor(new Class[0]).newInstance()), JComponent.class);
        } catch(Exception e) { stckTrc(e.getCause()); }
        if (jc!=null) {
            pcp(ChTabPane.KEY_SORT, _mapMenuItem2Topic.get(cn),jc);
            pcp(key,cn,jc);
            if (dIcon(clas)==null) TabItemTipIcon.set(null,null,null,IC_BLANK,clas);
            addDialog(jc);
            addListener(deref(jc,StrapListener.class));
        }
        return jc;
    }

    public static JComponent addDialogSetClass(Object dialog, Object selectedClass) {
        final JComponent comp=addDialogC(dialog);
        for(ChCombo c : childsR(comp,ChCombo.class)) {
            if (c.containsClasses()) c.s(selectedClass);
        }
        return comp;
    }

    private static ChTabPane _panAnno, _panDia;
    public static ChTabPane dialogPanel() {
        if (_panDia==null) addActLi( li(), _panDia=new ChTabPane(ChTabPane.LEFT));
        return _panDia;
    }
    static ChButton b(Object c) { return b(c, null);}
    private static ChButton b(Object clas, ContextObjects cPP) {
        return new ChButton("b()").tabItemTipIcon(clas).li(li())
            .cp("PROTEINS", cPP!=null?cPP:ContextObjects.defaultInstance())
            .cp(ChButton.KEY_CLASS,clas);
    }

    static int panelHeight() { return hght(_splitLR);}
    static int dividerLocation(boolean old) { return old ? _splitLRv : _splitLR!=null ? _splitLR.getDividerLocation() : 0;}

    static void rightCollapse(boolean collapse) {
        if (_splitLR==null) return;
        final int d=_splitLR.getDividerLocation();
        if (collapse) {
            if (d>0) _splitLRv=d;
            _splitLR.setDividerLocation(0);
        } else if (_splitLRv>0) _splitLR.setDividerLocation(_splitLRv);
    }
    /* <<< Dialogs <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public static void addAwtListeners(Object o) { if (getInstance()!=null) tree().LI.addTo("kmM",o); }
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final int id=ev.getID(), iButton=idxOf(q,buttons), modi=modifrs(ev), kcode=keyCode(ev);
        final boolean shift=0!=(modi&MouseEvent.SHIFT_MASK);
        if (id==ComponentEvent.COMPONENT_RESIZED && q==_frame) {
            for(StrapView v : alignmentPanels()) {
                v.correctHeight();
                revalidateC(v.panel());
            }
        }

        final StrapView view=alignmentPanel();
        if (id==MouseEvent.MOUSE_ENTERED && view!=null) view.mouseEnterDrogTarget(q);
        if (id==ActionEvent.ACTION_PERFORMED) {
            final ChTabPane panDialogs=dialogPanel();
            if (cmd=="ALL_CMDS") {
                if (_scriptList==null) _scriptList=HelpCommands.getInstance(Strap.SCRIPT_COMMANDS).alphabetically(null);
                shwTxtInW(ChFrame.SCROLLPANE|CLOSE_CtrlW_ESC, "Alphabetical list of all commands",_scriptList);
            }
            if (cmd==ExecByRegex.RUN_SCRIPT) {
                ChFrame.frame("Execute shell script for Strap", ExecByRegex.panel(Customize.strapApps,this), CLOSE_CtrlW_ESC|ChFrame.PACK|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.TO_FRONT|ChFrame.AT_CLICK).i(IC_BATCH);
            }

            if (cmd=="FS") {
                final Object ap=invokeMthd(0, "getApplication", "com.apple.eawt.Application");
                invokeMthd(0, "requestToggleFullScreen", ap, new Object[]{frame()});
            }
            if (cmd=="About") Strap.aboutDialog();
            if (cmd=="MEM") {
                if (_freeMem==null) _freeMem=pnl(HBL,"Free memory ", new FreeMemoryBar());
                ChFrame.frame("Free memory", _freeMem, ChFrame.PACK|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.AT_CLICK);
            }
            if (q==panDialogs || q instanceof ChTableLayout) {
                ChTableLayout tl=null;
                ProteinViewer pv=null;
                if (q==panDialogs)  tl=deref(panDialogs.selectedComponent(), ChTableLayout.class);
                else {
                    tl=(ChTableLayout)q;
                    if (cmd==ChTableLayout.COLUMN_ADDED)  panDialogs.addTab(CLOSE_ALLOWED, tl);
                    if (cmd==ChTableLayout.COLUMN_REMOVED && _mapTP.values().contains(tl) && tl.t().getColumnCount()==0) panDialogs.removeTab(tl);
                }
                if (tl!=null) {
                    final Object oo[]=tl.getAllComponents();
                    pv=Protein3dUtils.derefV3D(orO(get(tl.getSelectedIndex(),oo), get(sze(oo)-1,oo)));
                }
                if (cmd==ChTabPane.ACTION_SELECTION_CHANGED || cmd==ChTabPane.ACTION_ADDED || cmd==ChTabPane.ACTION_REMOVED ||
                    cmd==ChTableLayout.COLUMN_ADDED || cmd==ChTableLayout.COLUMN_REMOVED) {
                    if (pv!=null) V3dUtils.setPV(V3dUtils.PV_FOCUSED, pv);
                    else setMenuBar(null,0,0);
                    setToolpane(null);
                }
                frame().toFront();
                StrapView.correctHeight1();
            } else {
                final List vSel=selectedObjectsV();
                final Protein[] ppSel=selectedProteins(), sel_or_all=ppSel.length>0 ? ppSel : proteins();
                boolean errorNoProtein=false;
                final boolean selected=isSelctd(q);

                if (strEquls(KEY_PROGRESS,cmd)) drawMessage(delPfx(KEY_PROGRESS,cmd));

                if (q==_panAnno || q==PANELS_JH['H'] || q==PANELS_JH['J']) {
                    final ChTabPane tp=(ChTabPane)q;
                    if (cmd==ChTabPane.ACTION_ADDED || cmd==ChTabPane.ACTION_SELECTION_CHANGED) panDialogs.addTab(CLOSE_ALLOWED, tp);
                    if (cmd==ChTabPane.ACTION_REMOVED && tp.tabComponents().length==0) panDialogs.removeTab(tp);
                    pcp(ChTabPane.KEY_SORT,"0H", q);
                    for(ChJTextPane c:childsR(PANELS_JH['H'],ChJTextPane.class)) pcp(ACTION_DATABASE_CLICKED,"Load",c);
                }

                if (cmd==ChStdout.ACTION_DATA_TRANSFER) {
                    drawMessage("Protein and alignment files can be dragged into Strap.\nIcon images can be dragged onto Proteins and residue annotations.");
                }
                final ChJList jList=gcp(OBJECT_JLIST,q,ChJList.class);
                if (cmd=="DRAG_RST") { radioGrpSet(0,_dragRadioOrigF); radioGrpSet(0,_dragRadioChains); radioGrpSet(0,_dragRadioMSA); _dragBiomTog.s(false);  }

                if (radioGrpIdx(q, _dragRadioOrigF)==0)   _dragBiomTog.s(false);
                if (radioGrpIdx(q, _dragRadioChains)==1)  _dragBiomTog.s(false);
                if (q==_dragBiomTog && selected) { radioGrpSet(1,_dragRadioOrigF); radioGrpSet(0, _dragRadioChains);  }
                if (cmd=="SCRIPTP") openScriptPanel();
                if (cmd=="SP") {
                    final long m=atol(gcp("SP",q));
                    if (m==0 || m==-1) adAll(visibleProteins(),vSel);
                    if (m==-1) rmAll(ppSel,vSel);
                    if (m>0) {
                        for(Protein p : visibleProteins()) {
                            if (ProteinList.isEnabled(p,m)) {
                                if (shift) vSel.remove(p);
                                else vSel.add(p);
                            }
                        }
                    }
                    StrapEvent.dispatchLater(StrapEvent.OBJECTS_SELECTED,22);
                }

                if (jList!=null && view!=null) {
                    final List<Protein[]> vR=  cmd=="SHOW_HIDE_U" ? gcp("SHOW_HIDE_U",q,List.class) : cmd=="SHOW_HIDE" ? new Vector() : null;
                    if (cmd=="SEL" || cmd=="UNSEL" || cmd=="SCROLL") {
                        final Object vv[]=jList.getSelectedValues(), vv1[]=sze(vv)>0 ? vv : oo(jList.getList());
                        if (sze(vv1)>0) {
                            if (cmd=="SCROLL") {
                                if (view!=null) view.scrRectToRowCol(true,ResSelUtils.enclosingRectangle(vv1));
                            } else {
                                if (cmd=="SEL") adAll(vv1, vSel);
                                else rmAll(vv1,vSel);
                                StrapEvent.dispatch(StrapEvent.OBJECTS_SELECTED);
                            }
                        }
                    }
                    if (cmd=="SHOW_HIDE_U") ((ChButton)q).enabled(false);
                    if (cmd=="SHOW_HIDE") {
                        final ChButton bUndo=gcp("SHOW_HIDE_U",q,ChButton.class);
                        if (bUndo!=null) {
                            final int nRows=view.countRows();
                            final List<Protein[]> vOld=new Vector(nRows);
                            for(int row=0; row<nRows; row++) vOld.add(view.proteinsInRow(row));
                            bUndo.enabled(true).cp("SHOW_HIDE_U",vOld);
                        }
                        for(int i=0; i<sze(jList); i++) {
                            final Protein p=get(i,jList,Protein.class);
                            if (p!=null) vR.add(spp(p));
                        }
                    }
                    if (vR!=null) {
                        view.list().clear();
                        view.list().addAll(vR);
                        final Protein[] ppShow=view.pp(), ppH=proteins().clone();
                        for(int i=ppH.length; --i>=0;) if (cntains(ppH[i],ppShow)) ppH[i]=null;
                        setIsInAlignment(false, 99, ppH);
                        setIsInAlignment(true,  99, ppShow);
                    }
                }
                {
                    final String db=delPfx(ACTION_DATABASE_CLICKED,cmd);
                    if (cmd!=db) {
                        final String entry=db.startsWith("PDB:") ? db.replaceAll(":_","") : db;
                        startThrd(threadWebAlignment(0, entry, new Object[]{StrapView.KEY_AP}));
                    }
                    final String soap=delPfx(FetchSeqs.CMD_WSDBFETCH_SEND_QUERY,cmd);
                    if (soap!=cmd) drawMessage("@1"+ANSI_W_ON_B+"SOAP "+soap);
                }
                if (cmd==IC_PUBMED) DesktopUtils.openPmidManager("",ChFrame.AT_CLICK);
                if (cmd=="NP") {
                    if (_taNotes==null) {
                        final ChTextArea ta=new ChTextArea(readBytes(file("notepad.txt")));
                        _taNotes=ta;
                        newDropTarget(ta,false);
                        pcp(KEY_IF_EMPTY,"Type notes.\nFiles, URLs and database Ids act as hyper-link.\nType F1 for Help",ta);
                        pcp(ACTION_DATABASE_CLICKED,"Load",ta);
                        pcp(ChTextComponents.KOPT_F1_HELP,"",ta);
                        addActLi(li(),ta);
                        highlightProteins("APDH",ta);
                    }
                    ChFrame.frame("Notes",_taNotes,ChFrame.SCROLLPANE|CLOSE_CtrlW).shw();
                }
                if (cmd=="DIR" || cmd=="DIRX") {
                    final BA sb=new BA(999).a(cmd=="DIR" ? " those with asterisk \"*\"" : "The following directories")
                        .aln(" can be deleted without losing essential information.\n");
                    File f;
                    if (cmd=="DIR") {
                        sb.a("   Project directory:  ").aln(dirWorking()).a("   Settings:  ").aln(file("~/@/"));
                        if (isDirNotEmpty(f=dirJars())) sb.a(" * JAR files:  ").aln(f);
                        if (isDirNotEmpty(f=dirBin()))  sb.a(" * Downloaded program files:  ").aln(f);
                    }
                    if (isDirNotEmpty(f=file("~/@/log"))) sb.a(" * Log files: ").aln(f);
                    if (isDirNotEmpty(f=CacheResult.dir())) sb.a(" * Cache:  ").aln(f);
                    if (isDirNotEmpty(f=file(dirWorking()+"/trash"))) sb.a(" * Trash: ").aln(f);
                    if (isDirNotEmpty(f=dirTmp())) sb.a(" * Temporary files: ").aln(f);
                    if (isDirNotEmpty(f=file("~/@/strapTmp"))) sb.a(" * Temporary files: ").aln(f);
                    if (isDirNotEmpty(f=dirWeb())) sb.a(" * Cached web files: ").aln(f);
                    if (isDirNotEmpty(f=file("~/@/previousVersions"))) sb.a(" * Earlier strap versions: ").aln(f);
                    if (isDirNotEmpty(f=file("~/@/pdbChains"))) sb.a(" * Splitted PDB files: ").aln(f);
                    if (cmd=="DIRX") sb.aln("\n Advanced: SQL database cache*");
                    shwTxtInW("Directories",sb);
                }
                if (cmd=="ENV")  shwTxtInW("Environment", ChEnv.asText());
                if (cmd=="PROP") shwTxtInW("Java Properties", ChEnv.propertiesAsText());
                if (cmd=="getDefaults") LAFChooser.showUiDefaults();

                if (cmd=="ASK_HOME") ChMsg.askUserHome(cmd);
                if (q==buttn(TOG_WHITE_BG)) {
                    StrapEvent.dispatchLater(StrapEvent.BACKGROUND_CHANGED,222);
                    ChDelay.repaint(_splitLR,333);
                }
                if (cmd=="PM") shwPopupMenu(_prefMenu);
                if (q==_zipB || q==_zipTf && cmd==ACTION_ENTER) {
                    final String s0=toStrg(_zipTf),
                        s=delSfx(".zip",s0.substring(nxt(SLASH,s0)+1).replaceAll(REGEX_NO_FILE_NAME,"_"))+".zip";
                    if (s!=s0) _zipTf.t(s);
                    zipAlignmentProject(s,modi);
                }
                if (q==_zipTf && cmd==ACTION_TEXT_CHANGED) _zipB.addDnD(null);
                if (cmd=="STRAP_JNLP") {
                    final Object msg=pnl(VBHB,
                                          "Currently, Strap is running in Web-mode.<br>"+
                                          "Launching Strap in application mode provides full functionality",
                                          pnl(HB,"You can exchange proteins between both alignment views with, ","WIKI:Drag_and_drop"),
                                         MOVIE_Drag_to_another_STRAP
                                          );
                    if (ChMsg.yesNo(msg)) visitURL(URL_STRAP_JNLP,0);
                }
                if (iButton==BUT_DRAG_OPTS) ChFrame.frame("Options for exporting proteins", pnlDragOptions(), ChFrame.PACK).shw(ChFrame.AT_CLICK);
                if (iButton==TOG_HIGHLIGHT_IDENTICAL_SEQUENCES && view!=null) view.rowHeader().repaint();
                if (iButton==BUT_ZIP) zipAlignmentProject(null,0);
                if (iButton==TOG_MULTI_MBARS) setMenuBar(null,0,0);
                if (cmd=="MANUAL") generateManual();
                if (cmd=="MAN") shwTxtInW(ChFrame.SCROLLPANE, getTxt(q), manPage('A'));
                if (cmd==ACTION_CLICKED_DOUBLE) {
                    final BA s=new BA(0).join(((FilePopup)q).getFiles());
                    loadTheProteinsInList(OPTION_PROCESS_LOADED_PROTS|OPTION_EVENT_PROTEINS_ADDED|OPTION_SHOW_ORPHAN_HETEROS_IN_3D, s);
                }
                if (gcp("LD_LAST",q)!=null) loadTheProteinsInList(OPTION_PROCESS_LOADED_PROTS|OPTION_EVENT_PROTEINS_ADDED, toStrg(gcp("LD_LAST",q)));
                if (cmd=="b()") {
                    final Object cName=gcp(ChButton.KEY_CLASS,q);
                    if (cName==CLASS_DialogHighlightPattern && null!=gcp(KOPT_SUPPORTS_CTRL_F,focusedC(444))) return;
                    final Class c=clas(cName);
                    final JComponent jc=addDialogC(c);
                    if (jc==null) {
                        if (cName==CLASS_Texshade) Insecure.tellNoPermission(' ',dItem(cName));
                        return;
                    }
                    final ChRunnable hook=gcp(RUN_OPEN_DIALOG_HOOK,q,ChRunnable.class);
                    if (hook!=null) hook.run(RUN_OPEN_DIALOG_HOOK,jc);
                    else {
                        final ContextObjects obj=gcp("PROTEINS",q, ContextObjects.class);
                        Protein prots[]=obj==null?null:obj.proteins();
                        if (prots==null) prots=selectedProteins();
                        SPUtils.dialogSetProteins(prots, jc);
                    }
                    frame().show();
                }
                long loadMask=0;
                if (cmd=="AA_SC") loadMask=ProteinParser.SIDE_CHAIN_ATOMS;
                if (cmd==ACTION_ignoreSEQRES) loadMask=ProteinParser.IGNORE_SEQRES;

                if (loadMask!=0) {
                    final long opt=SPUtils.parserOptions();
                    SPUtils.setParserOptions(selected?opt|loadMask: opt&~loadMask);
                }
                if (cmd=="CLIENT_PROPERTY") {
                    final BA sb=new BA(999);
                    for(Protein p: sel_or_all) 	sb.aln(p).aln(p.map(false));
                    shwTxtInW("CLIENT_PROPERTY",sb);
                }
                if (cmd=="DEBUG_noLetters") {
                    final BA sb=new BA("In the following all residues are listed that are not letters:\n");
                    for(Protein p:sel_or_all) {
                        final byte bb[]=p.getResidueType();
                        for(int j=p.countResidues();--j>=0;)
                            if (!is(LETTR,bb[j])) sb.a(p).a("iAa=").a(j).a(bb[j]).a(' ').a((char)bb[j]);
                    }
                    shwTxtInW("Wrong residues characters", sb);
                }
                if (cmd=="DEBUG_ON") puts("MY_COMPUTER");

                if (cmd=="DEBUG_T") shwTxtInW("Threads", ChThread.debugListThreads(new BA(3333)));

                if (iButton==TOG_HIDE_GAPS) {
                    Protein.setWide(selected);
                    Protein.incrementMC(MCA_ALIGNMENT,null);
                    StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_CHANGED,111);
                }
                if (iButton==BUT_SAVE) save();
                if (iButton==BUT_UPDATEUI) SwingUtilities.updateComponentTreeUI(frame());
                if (cmd==CMD_ALL_MENUS)	allMenus(true,true,null);
                if (cmd==CMD_ALL_POPUP)	allMenus(true,false,null);
                if (cmd==CMD_DEL_ICONS) {
                    errorNoProtein=ppSel.length==0;
                    for(Protein p: ppSel) p.setIconImage("");
                    StrapEvent.dispatchLater(StrapEvent.PROTEIN_RENAMED,111);
                }
                if (cmd==CMD_PDB_ICONS || cmd==CMD_SPECIES_ICONS) {
                    errorNoProtein=ppSel.length==0;
                    boolean ex=false;
                    for(Protein p:ppSel) {
                        if (p!=null && sze(p.iconLink())>0) { ex=true; break;}
                    }
                    final boolean overwrite=ex && ChMsg.yesNo("overwrite existing icons?");
                    SPUtils.setProteinIcon(ppSel,overwrite, cmd==CMD_PDB_ICONS ? '3':'S');
                }
                if (cmd==CMD_TABLE_ICONS) {
                    final Collection v=new HashSet();
                    for(Protein p : sel_or_all) {
                        String s=p.getOrganismScientific();
                        if (s==null) s=p.getOrganism();
                        if (s==null) continue;
                        final BA link=readBytes(p.iconLink());
                        if (link!=null) v.add(delFromChar(s,' ')+" "+link);
                    }
                    shwTxtInW("Species and icons",new BA(999).join(v));
                }
                if (iButton==BUT_NEW_PANEL) newAlignmentPanel(true);
                if (iButton==BUT_NEW_STRAP) {
                    final Object msg=
                        pnl(VBHB,
                             "<u>Two Strap-sessions</u>",
                             pnl(HBL,"For exchanging proteins between the two alignment panels use ","WIKI:Drag_and_drop"),
                             " ",
                             WATCH_MOVIE+MOVIE_Drag_to_another_STRAP,
                             " ",
                             "Launching another Strap-session"
                             );
                    if (ChMsg.yesNo(msg)) visitURL(URL_STRAP_JNLP, 0);
                }
                if (cmd=="BUT_BACKUP") {
                    save();
                    final String dir="backup/"+fmtDate(0)+"/";
                    if (saveAlignment(file(dir))) {
                        final String msg="<br><br>Only the alignment gaps and the annotations are saved,<br>but the sequence files are not saved.";
                        ChMsg.msgDialog(0L, pnl(VBPNL,"Writing backup to directory ",file(dir),msg));
                    }
                    new StrapEvent(this,StrapEvent.BACKUP_WRITTEN).run();
                }
                if (iButton==BUT_STOP_ALL) stopAllAlignments();
                if (cmd==ACTION_openPlugin) StrapPlugins.dialogStaticPlugins();
                if (iButton==BUT_WATCH_EV) {
                    if (_watchEv==null) {
                        (_watchEv=new ChTextView(""))
                            .tools().cp(ChTextComponents.KOPT_CLASS_AT_CURSOR,"")
                            .underlineRefs(ULREFS_DISABLE)
                            .cp(KEY_OPTS_SCROLLPANE, intObjct(SCRLLPN_CLR_BUT));
                    }
                    ChFrame.frame("StrapEvent", _watchEv, ChFrame.ALWAYS_ON_TOP|ChFrame.SCROLLPANE|CLOSE_CtrlW|ChFrame.ICONIFY_ON_CLOSE|ChFrame.SCROLL_END).shw(ChFrame.AT_CLICK);
                }
                if (iButton==BUT_LIST_EV) {
                    if (_jlListeners==null) _jlListeners=new ChJList(_vLis, ChJTable.CLASS_RENDERER);
                    ChFrame.frame("List of StrapEvent listeners",_jlListeners, ChFrame.PACK|ChFrame.ALWAYS_ON_TOP|ChFrame.SCROLLPANE).shw(ChFrame.AT_CLICK);
                }
                if (cmd=="PARSE_INFO") {
                    final BA sb=new BA("Info of parsing the ").a(ppSel.length).aln(" selected proteins");
                    for(Protein p:sel_or_all)
                        sb.a('\n').aln(p).a("parsed by ").a(p.getProteinParserClass())
                            .a(" within ").a(p.getParsingTime()).aln(" ms").aln(p.PARSING_INFO);
                    new ChFrame("parsingInfo").ad(scrllpn(new ChTextArea(sb))).shw(0);
                }

                if (cmd==VIEW_SB) strapSB();
                if (iButton==TOG_MAXIMIZE_DIALOGS) {
                    rightCollapse(selected);
                    revalAndRepaintC(panDialogs);
                    for(StrapView v : alignmentPanels()) revalAndRepaintC(v.panel());
                }
                {
                    String para;
                    if (cmd!=(para=delPfx(HtmlDoc.ACTION_OPEN_DIALOG,  cmd))) addDialogC(findClas(para,STRAP_PACKAGES));
                    if (cmd!=(para=delPfx(HtmlDoc.ACTION_OPEN_TUTORIAL,cmd))) Tutorials.openTutorial(para);
                }
                if (errorNoProtein) errorMsg("No protein selected");
                if (q==_frame && (cmd==ACTION_WINDOW_ACTIVATED || cmd==ACTION_WINDOW_DEACTIVATED) && parentWndw(_shopping)!=q) {
                    _shopping.setBorder(cmd==ACTION_WINDOW_DEACTIVATED ? BorderFactory.createRaisedBevelBorder()  : BorderFactory.createLoweredBevelBorder());
                }
                if (cmd=="BUT_EXIT" || ( q==_frame && cmd==ACTION_WINDOW_CLOSING)) Strap.quitStrap();
                if (iButton==BUT_hiddenP) {
                    viewTree(ppAliV());
                    ChMsg.msgDialog(0,onlyOnce("Hidden proteins are found in a tree node \"Hidden proteins\" in the expandable tree."));
                }
                if (iButton==BUT_SELECT_BY_NAME) {
                    final ChTextArea ta=new ChTextArea("");
                    final Object
                        bb[]={new ChButton("SL_ADD").li(li()).t("Select"),  new ChButton("SL_RM").li(li()).t("Deselect")},
                        explain=pnl("Type a list of proteins or residue selections. Regular expressions are supported. <br>"+
                            "Residue selections are written in the form <i>ProteinName/selectionName</i>. <br>"+
                                    "Take advantage of tab-key word-completion"),
                            top=pnl(VBPNL, pnlTogglOpts("Explain",explain), explain),
                            pnl=pnl(CNSEW,scrllpn(ta), top, pnl(bb));
                    new ChFrame("Select by name").ad(pnl).shw(ChFrame.ALWAYS_ON_TOP|ChFrame.STAGGER);
                    for(Object b:bb) pcp("TA",wref(ta),b);
                    pcp(KEY_IF_EMPTY,"Type list of protein names\nTab key for auto expansion.",ta);
                    highlightProteins("PA",ta);
                    ta.tools()
                        .saveInFile("StrapAlign_selectByName")
                        .highlightOccurrence(getInstance(), chrClas(SPC), chrClas(-LETTR_DIGT_US), HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(0xFFff14,90));
                }
                if (cmd=="SL_ADD" || cmd=="SL_RM") {
                    final Object pp_aa[]=proteinsAndAnnotationsWithRegex(toStrg(gcp("TA",q)), proteins(), 'A');
                    for(Object o : pp_aa) {
                        if (cmd=="SL_RM") vSel.remove(o); else  vSel.add(o);
                    }
                    StrapEvent.dispatchLater(StrapEvent.OBJECTS_SELECTED,222);
                }
            }
            setDefaultClassByPara(gcps(KEY_DC,q));
        }
        if (id==MouseEvent.MOUSE_PRESSED && q instanceof ChJList || q instanceof ChJTree) {
            animatePositionS(derefArray(contextObjectsAt(ev),ResidueSelection.class));
        }
        if (q==_shopping) {
            if (id==MouseEvent.MOUSE_PRESSED) undockProgress();
            if (id==MouseEvent.MOUSE_CLICKED) {
                setWndwState('F',frame());
                if (view!=null) view.alignmentPane().requestFocus();
                undockProgress();
            }
        }
    }
    /* <<< ActionEvent <<< */
    /* ---------------------------------------- */
    /* >>> selected Objects >>> */
    /**  An array of all objects of the spcified type that are selected in the object tree */
    public static Protein[] selectedProteinsInVisibleOrder() {
        final List v=vClr(_vSelProtO);
        final Protein pp[]=coSelected().proteins();
        for(Protein p : visibleProteins()) {
            if (cntains(p,pp)) v.add(p);
        }
        return toArryClr(v,Protein.class);
    }

    public static Protein[] selectedOrVisibleProteins(int minNum) {
        final Protein pp[]=selectedProteinsInVisibleOrder();
        return pp.length>=minNum?pp : visibleProteins();
    }
    public static Protein[] selectedProteins() { return coSelected().proteins(); }
    public static void setNumOfProteins(AbstractButton b) {
        String txt=gcps(LABEL_ALL_OR_SELECTED_P,b);
        if (txt==null) {
            pcp(LABEL_ALL_OR_SELECTED_P, txt=getTxt(b), b);
            adUniq(wref(b),_vNumPP);
        }
        if (getInstance()!=null && strstr(LABEL_ALL_OR_SELECTED_P,txt)>=0) {
            setTxt(rplcToStrg(LABEL_ALL_OR_SELECTED_P, plrl(selectedOrVisibleProteins(1).length," %N protein%S"), txt), b);
        }
    }
    /* <<< selected Objects <<< */
    /* ---------------------------------------- */
    /* >>> Drag-and-Drop >>> */
    public static void newDropTarget(Object c,boolean recursive) {
        if (c instanceof Component && javaVsn()>14) {
            if (adUniq(wref(c), _vDT)) new DropTarget((Component)c,DnDConstants.ACTION_COPY,getInstance());
            if (recursive) for (Component comp: childs((Component)c)) newDropTarget(comp,true);
        }
        li().addTo("m",c);
    }
    private Protein _dropProteinsFromStrgSelection[];
    private final Map<Class,Object> DND_TARGET=new HashMap();
    public <T extends Object> T dndTarget(Class<T> c) { return deref(DND_TARGET.get(c), c); }
    private Transferable _dropTransferable;
    private BA _dropProtsFromStrgBA;
    private String _dropProtLnk, _dropImg, _dropMsg, _dropScript, _dropStrg;
    private File _dropFiles[], _dropFilesResSel[], _dropFileAnno;
    private Object _dropObjects[], _dropLogMsg, _dndTarget;
    /* --- Macosx has a bug. see CDataTransferer_dragQueryFile --- */
    private static BA _logDnd;
    @Override public void dropActionChanged(DropTargetDragEvent ev) {}
    @Override public void dragEnter(DropTargetDragEvent ev) { ev.acceptDrag(DnDConstants.ACTION_COPY); }
    @Override public void dragOver(DropTargetDragEvent ev) {  verifyDrop(ev,ev.getTransferable(),ev.getLocation()); }
    private void verifyDrop(DropTargetEvent event, Transferable t, Point point) {
        final BA log=new BA(333);
        DND_TARGET.clear();
        final Object target;
        Protein[] targetPP;
        final ResidueAnnotation[] targetAA;
        final ProteinViewer targetV3D;
        try {
            {
                Object tc=((DropTarget)event.getSource()).getComponent(), t3d=null;
                DND_TARGET.put(Component.class, tc);
                tc=orO(gcpa(KEY_DROP_TARGET_REDIRECT,tc), tc);
                targetPP=derefArray(tc,Protein.class);
                ResidueAnnotation[] taa=derefArray(tc,ResidueAnnotation.class);
                ProteinViewer v3d=null;
                if (point!=null) {
                    final Object oPoint=objectAt(point,tc),  o=orO(gcpa(KEY_DROP_TARGET_REDIRECT,oPoint), oPoint);
                    if (o!=null) {
                        if (v3d==null) v3d=deref(get(0,gcpa(V3dUtils.KEY_PROTEIN_VIEWER,o)), ProteinViewer.class);
                        if (v3d==null) v3d=gcp(V3dUtils.KEY_PROTEIN_VIEWER,o,ProteinViewer.class);
                        if (o instanceof Protein) targetPP=derefArray(contextObjects(o, coSelected().proteins()),Protein.class);
                        else if (ResSelUtils.isSelVisible(0,o) && ResSelUtils.type(o)=='A') taa=derefArray(contextObjects(o, coSelected().residueSelections('A')), ResidueAnnotation.class);
                        else if (o==ppNotAliV()) tc=ppNotAliV();
                        if (isInstncOf(ProteinViewer.class,o)) t3d=o;
                    }
                    final StrapView view=gcp(StrapView.class,tc,StrapView.class);
                    DND_TARGET.put(StrapView.class, view);
                    if (view!=null) {
                        final boolean ali=tc==view.alignmentPane();
                        if (ali || tc==view.rowHeader()) targetPP=view.proteinsAtY(y(point));
                        if (ali) {
                            final ResidueSelection[] ss= view.residueSelectionsAtXY(x(point), y(point),VisibleIn123.SEQUENCE).clone();
                            for(int i=ss.length; --i>=0;) if (!ResSelUtils.isSelVisible(0,ss[i]) || ResSelUtils.type(ss[i])!='A') ss[i]=null;
                            taa=derefArray(ResSelUtils.plusSelected(true, ss),ResidueAnnotation.class);

                        }
                    }
                }
                target=tc;
                targetAA=taa;
                if (v3d==null) v3d=deref(orO(t3d, gcp(V3dUtils.KEY_PROTEIN_VIEWER, target)), ProteinViewer.class);
                if (v3d==null && gcpa(KOPT_DROP_TARGET_IS_CURRENT_PV,target)!=null) v3d=V3dUtils.getPV(V3dUtils.PV_MENUBAR);
                targetV3D=v3d;

            }
            DND_TARGET.put(ResidueAnnotation[].class, targetAA);
            DND_TARGET.put(Protein[].class, targetPP);
            DND_TARGET.put(ProteinViewer.class, targetV3D);
            DND_TARGET.put(Collection.class, deref(target,Collection.class));
            DND_TARGET.put(JTextComponent.class, deref(target,JTextComponent.class));
            final ResidueAnnotation aaTarget[]=dndTarget(ResidueAnnotation[].class);
            if (_dropTransferable!=t || _dndTarget!=target || event instanceof DropTargetDropEvent) {
                _dndTarget=target;
                _dropTransferable=t;
                _dropMsg=_dropImg=_dropScript=null;
                _dropFiles=_dropFilesResSel;
                _dropFileAnno=null;
                _dropProtLnk=null;
                if (prgOptT("-logDnD")) log.a(ChTransferable.reportDataFlavors(t));
                _dropObjects=(Object[])ChTransferable.getData("OO",t);
                final Object fileList[]=oo(ChTransferable.getData("FF",t));
                final String dataUriLst=(String)ChTransferable.getData("URI",t);
                final String dataString=(String)ChTransferable.getData("S",t);
                log.a("verifyDrop:\n dataString=").aln(dataString).a(" dataUriLst=").aln(dataUriLst).a(" fileLst={").join(fileList, ",").a('}');
                final List vWhatIf=new Vector(), vF=new Vector(), vResSelF=new Vector();
                boolean useFiles=true;
                for(Object o : oo(_dropObjects)) {
                    if (o instanceof ResidueSelection || o instanceof Protein || o instanceof HeteroCompound) useFiles=false;
                }
                if (useFiles) {
                    final boolean[] delim=isMac()||isWin() ? chrClas1('\n') : chrClas(SPC);
                    final List vLines=new Vector();
                    adAllUniq(splitTokns(dataString,delim), vLines);
                    adAllUniq(splitTokns(dataUriLst,delim), vLines);
                    for(Object o : fileList) adUniq(toStrg(o), vLines);

                    for(Object line :  oo(vLines)) {
                        String sData=toStrgTrim(line);
                        if (fileList.length>0) sData=rplcToStrg(" ","%20",sData);
                        if (looks(LIKE_EXTURL,sData)) sData=delLstCmpnt(sData,'\n').trim();  /* firefox on DOS */
                        final boolean isYahoo=sData.indexOf(".yahoo.")>0;
                        if (sData.indexOf(".msn.")>0 || isYahoo || sData.indexOf(".google.")>0) {
                            for(String ext : JPG_PNG_GIF_BMP) {
                                if (strstr(STRSTR_w_R, ext,sData)<0) continue;
                                String sImg=urlDecode(urlDecode(sData));
                                int iImg=strstr(STRSTR_w_L|STRSTR_AFTER,"imgurl=",sImg);
                                if (iImg<0) iImg=strstr(STRSTR_w_L|STRSTR_AFTER,"imgrefurl=",sImg);
                                if (iImg<=0) continue;
                                sImg=sImg.substring(iImg);
                                sImg=delFromChar(sImg,'&');
                                sImg=delFromChar(sImg,'?');
                                final int i=sImg.indexOf("%3F");
                                if (i>0) sImg=sImg.substring(0,i);
                                sData=isYahoo && !sImg.startsWith("http://") ? "http://"+sImg : sImg;
                                break;
                            }
                        }
                        if (sData==null) continue;
                        final String
                            s=sData,
                            dotSfx=toStrgIntrn(dotSfx(delSfx(".z",delSfx(".gz",s.toLowerCase())))),
                            msg=dotSfx==".abw" || dotSfx==".odt" ? "Wordprocessor-documents must first be saved as plain text with line ends!" :
                            0<strstr(STRSTR_w, dotSfx, " .pdf .zip .tif .tiff .xls ") ? "These file types are not supported!" :
                            null;
                        if (endWith(ResSelUtils.DND_SUFFIX, s)) { adUniq(file(s), vResSelF); continue; }
                        if (endWith(ResidueAnnotationView.FILE_EXT_DND,s)) {  _dropFileAnno=file(s);  continue; }
                        if (dotSfx==".strap") { _dropScript=s; continue; }
                        if (msg!=null) { _dropMsg=msg; continue;}
                        final String dbColonID=Hyperrefs.toDbColonID(s);
                        {
                            final File f=nxt(LETTR_DIGT,s)>=0 ? file(FILE_NO_ERROR, s) : null;
                            if (sze(f)>0 && !strEquls("PDB:",dbColonID)) {
                                if (idxOfStrg(dotSfx, JPG_PNG_GIF_BMP)>=0) _dropImg=s;
                                else if (!endWith(STRSTR_IC, ".url",f)) adUniq(f,vF);
                            }
                        }
                        if (idxOfStrg(dotSfx,JPG_PNG_GIF_BMP)>=0 && (looks(LIKE_EXTURL_S,s)) ) _dropImg=s;
                        else {
                            String pf=null;
                            if (0<strstr(STRSTR_w, dotSfx, ".vdb .pdb .ent .dssp .swiss .swissprot .embl .genbank .seq .fa .fasta .mfa .msf .clustalw .aln")) pf=s;
                            if (pf==null) pf=dbColonID;
                            if (pf==null) pf=Hyperrefs.toDbColonID(urlDecode(s));
                            if (pf==null && s.startsWith("http:")) {
                                startThrd(thrdCR(this,"dragFailed",s));
                                _dropMsg="Do not know what to do with  "+s;
                                pf=s;
                            }
                            if (pf!=null) {
                                Protein pAlready=null;
                                for(Protein p : proteins())  if (pf.equals(p.OBJECTS[Protein.O_WEB_TOK1])) pAlready=p;
                                if (pAlready!=null) _dropObjects=oo(pAlready);
                                else {
                                    adUniq("Load protein",vWhatIf);
                                    if (pf.indexOf(':')<0 && sze(file(pf))>0 || pf.startsWith("file:/")) adUniq(file(pf), vF);
                                    else _dropProtLnk=pf;
                                }
                            }
                            log.a(" s=").a(s).a(" toDbColonID=").a(Hyperrefs.toDbColonID(urlDecode(s))).a(" dropProteinLink ").aln(_dropProtLnk);
                        }
                    }
                }

                if (_dropScript!=null) adUniq("Run strap script ", vWhatIf);
                if (_dropImg!=null) adUniq(sze(targetAA)>0 ? "Set icon for selection" : sze(targetPP)>0 ? "Set protein icon" : "Icons can be dropped on proteins and selections", vWhatIf);
                _dropFiles=toArry(vF, File.class);
                if (sze(_dropFilesResSel=toArry(vResSelF, File.class))>0) adUniq("Copy residue selection", vWhatIf);

                for(Object o :  oo(sze(_dropObjects)>0 ? _dropObjects : _dropFiles)) {
                    final Protein p=
                        o instanceof Protein ? (Protein)o :
                        o instanceof File || o instanceof CharSequence && strchr('\n',o)<0 && strchr('\t',o)<0 ? proteinWithName(nam(file(o)), proteins()) : null;
                    final File f=o instanceof File ? (File)o : null;
                    final int nPT=sze(dndTarget(Protein[].class));
                    adUniq(
                           targetV3D!=null && o instanceof ResidueSelection ? "Highlight selection in 3D" :
                           p!=null && targetV3D!=null ? "Display 3D-structure" :
                           p!=null && target==ppNotAliV() ? "Hide protein" :
                           ppNotAliV().contains(p) ? "Un-hide" :
                           cntains(p,proteins()) ? "Proteins are already loaded - just change row " :
                           p==null && sze(f)>0  ? "Load protein" :
                           nPT>0 && o instanceof ResidueSelection ? "Copy selection to protein" :
                           nPT>0 && o instanceof HeteroCompound   ? "Add hetero group to protein" :
                           null, vWhatIf);
                }
                if (dataString==null) _dropProteinsFromStrgSelection=null;
                else if (dataString!=null && !dataString.equals(_dropStrg)) {
                    if (sze(_dropFiles)==0 && _dropImg==null && _dropProtLnk==null && _dropObjects==null) {
                        final BA ba=new BA(dataString).removeLeadingSpace().trim().a('\n');
                        if (sze(ba)>4 && !strEquls("file:/",ba)) {
                            Protein pp[]=proteinsFromMsfText(0, ba, (File)null, 1,this);
                            if (sze(pp)==0) {
                                pp=spp(new Protein());
                                SPUtils.parseProtein((File)null,ba,0, pp[0]);
                                SwissHeaderParser.parse(pp[0],ba);
                            }
                            _dropProteinsFromStrgSelection=pp;
                            _dropProtsFromStrgBA=ba;
                            for(Protein p : pp) {
                                String n=p.getName();
                                if (n==null || n==UNNAMED) n=p.getAccessionID();
                                if (n==null || n==UNNAMED) n=p.getPdbID();
                                if (n!=null) p.setName(n.substring(n.indexOf(':')+1));
                            }
                        }
                    }
                }
                if (sze(_dropProteinsFromStrgSelection)>0) adUniq(toStrg(new BA(99).a("Load ").join(_dropProteinsFromStrgSelection)), vWhatIf);
                _dropStrg=dataString;
                if(_dropMsg==null) _dropMsg="  When dropped: "+vWhatIf;
                setProgress(false);
            }
            if (_dropMsg!=null) drawMessage("@1"+(lstChar(_dropMsg)=='!' ? ANSI_W_ON_B : "")+_dropMsg);
        } finally {
            if (_logDnd==null) _logDnd=new BA(99).sendToLogger(0, "Drag'n Drop",IC_DND, 33*1000);
            if (sze(log)!=sze(_dropLogMsg) || !strEquls(log,_dropLogMsg)) {
                _logDnd.a(log).send();
                _dropLogMsg=log;
            }
        }
    }
    @Override public void dragExit(DropTargetEvent ev) {
        final ChButton b=button(BUT_DRAG_OPTS);
        StrapView.toolbars().add(b);
        revalAndRepaintC(b);
        ChDelay.highlightButton(b,2222);
        inEDTms(thread_rmFromParent(b), 4444);
    }
    @Override public void drop(DropTargetDropEvent event) {
        resetDragFiles();
        event.acceptDrop(DnDConstants.ACTION_COPY);
        /* putln("getTransferDataFlavors = ",event.getTransferable().getTransferDataFlavors()); */
        final Point point=event.getLocation();
        verifyDrop(event, event.getTransferable(), point);
        final Component targetC=dndTarget(Component.class);
        final ResidueAnnotation aaTarget[]=dndTarget(ResidueAnnotation[].class);
        final ChJList jList=gcp(V3dUtils.KEY_PROTEIN_VIEWER, targetC)!=null ? null : deref(targetC,ChJList.class);
        final JTable jTable=deref(targetC,JTable.class);
        final StrapView view=dndTarget(StrapView.class);
        final int y=point!=null ? y(point) : -1, row;
        final Protein[] ppTarget=dndTarget(Protein[].class);
        if (jList!=null) {
            final int row0=point!=null ? jList.locationToIndex(point) : -1;
            final Rectangle cell=jList.getCellBounds(row0,row0);
            row= cell==null ? -1:row0+ (y>y(cell)+hght(cell)/2 ? 1 : 0);
        } else row= view==null||(targetC!=view.alignmentPane()&&targetC!=view.rowHeader()) ?  -1 : view.y2row(y+hght(view.charB())/2);
        final Object oo[]=_dropObjects;
        if (jList!=null && jList!=jlUndock(false)) {
            if (sze(oo)>0) {
                final List lm=jList.getList();
                final Object[] selected;
                if (lm!=null && gcp(OBJECT_JLIST,jList)==jList) {
                    selected=jList.getSelectedValues();
                    for(Object o:oo) if (o instanceof Protein || isAssignblFrm(ResidueSelection.class,o) || o instanceof HeteroCompound) adUniqR(o,row,lm);
                } else selected=oo;
                jList.setSelOO(selected);
                jList.repaint();
                return;
            }
        }
        final JTextComponent targetTF=dndTarget(JTextComponent.class);
        final int nPPT=countNotNull(ppTarget);
        final ProteinViewer v3dTarget=dndTarget(ProteinViewer.class);
        final HeteroCompound[] hhDrop=derefArray(_dropObjects, HeteroCompound.class);
        final ResidueSelection ssDrop[];
        final boolean sel4best;
        {
            boolean best=false;
            ResidueSelection ss[]=derefArray(_dropObjects,ResidueSelection.class);
            if (sze(ss)==0 && sze(_dropFilesResSel)>0) ss=ResSelUtils.dropResSel(best=nPPT==0 && v3dTarget==null && targetTF==null, _dropFilesResSel, proteins());
            sel4best=best;
            ssDrop=ss;
        }
        int strapEvent=-1;
        if (sze(_dropScript)>0) new StrapScriptInterpreter().runScripts(new BA[]{new BA(_dropScript)});
        else if (_dropImg!=null) {
            if (sze(aaTarget)>0) {
                final String cc[]={"Cancel", toStrg(ResSelUtils.asText(true, aaTarget[0], new BA("For selection "))), aaTarget.length==1?null:"For "+aaTarget.length+" selections"};
                final int iChoice=ChMsg.option("Change background image",rmNullS(cc));
                for(int i=sze(aaTarget); --i>=0 && iChoice>0;) {
                    aaTarget[iChoice==1? 0:i].setValue(0,ResidueAnnotation.BG_IMAGE,_dropImg);
                    if (iChoice==1) break;
                }
                strapEvent=StrapEvent.RESIDUE_SELECTION_CHANGED;
            } else if (nPPT>0) {
                final String
                    one="For "+ppTarget[0],
                    all=nPPT==1?null: plrl(nPPT, "For %N protein%S"),
                    cc[]=rmNullS(new String[]{one, all, "Cancel"});
                final int iChoice=ChMsg.option("Change icon image of protein", cc);
                for(int i=nPPT; --i>=0;) {
                    if (i==0&&get(iChoice,cc)==one || get(iChoice,cc)==all) ppTarget[i].setIconImage(relativFilePath(dirWorking(), _dropImg));
                }
            }
        } else if (_dropProtLnk!=null && sze(_dropFiles)==0) {
            startThrd(threadWebAlignment(0, _dropProtLnk,new Object[]{v3dTarget, StrapView.KEY_AP, intObjct(row), Strap.NOT_TO_FRONT}));
        } else if (sze(hhDrop)>0 && nPPT>0) {
            for(Protein p : ppTarget) {
                if (p.addHeteroCompounds(Protein.HETERO_ADD_UNIQUE|Protein.HETERO_ADD_APPLY_INVERSE_MX, hhDrop)) strapEvent=StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED;
            }
        } else if (_dropFileAnno!=null) {
            if (get(0,aaTarget)!=null) {
                ResidueSelection ss[]={aaTarget[0]};
                if (jTable==null) ss=ResSelUtils.plusSelected(true, ss);
                final int tRow=jTable==null?-1:jTable.rowAtPoint(point);
                for(ResidueSelection s : ss)  ((ResidueAnnotation)s).dropRows(readBytes(_dropFileAnno), tRow>=0 ? tRow : MAX_INT);
            }
        } else if (sze(ssDrop)>0) {
            if (v3dTarget!=null) {
                final String msg="Send amino acid selections to protein viewers";
                final Protein pSuggest=sp(ssDrop[0]);
                V3dUtils.residueSelectionsTo3D_I(V3dUtils.RSto3D_SET_FOCUSED_PV, ssDrop, Protein3dUtils.askViewersSharingView(v3dTarget,pSuggest, msg));
            } else if (targetTF!=null) {
                final BA sb=new BA(999);
                if (gcp(ResidueSelection.KOPT_DND_COLUMNS,targetTF)!=null) sb.boolToText(ResSelUtils.toAlignmentColumns(ssDrop),1,",","-");
                else for(ResidueSelection s : ssDrop) ResSelUtils.asText(false,s,sb);
                ChTextComponents.insertAtCaret(0L,sb, targetTF);
            } else {
                if (!sel4best) ResSelUtils.dropResSelAsk(ssDrop, ppTarget);
                strapEvent=StrapEvent.RESIDUE_SELECTION_ADDED;
            }
        } else if (sze(_dropFiles)>0) {
            final BA sb=new BA(999);
            final Map<String,String[]> mapHeteros=new HashMap();
            for(File f: _dropFiles) {
                if (sze(f)==0) continue;
                final File fPn=file(dirDndData()+"/"+f.getName()+".proteinName");
                final String
                    pn2[]=sze(fPn)>0 && System.currentTimeMillis()-fPn.lastModified()<10*1000 ?  readLines(fPn) : null,
                    pn=orS(get(0,pn2), f.getName());
                if (sze(get(1,pn2))>0) mapHeteros.put(pn,splitTokns(pn2[1]));
                final File fNew=file(pn);
                if (sze(fNew)==0 || !fileEquls(fNew,f)) try { hardLinkOrCopy(f, fNew); } catch(Exception ex){}
                sb.aln(sze(fNew)>0?pn:f);
                for(int t=1; t<=Protein.FILE_TYPE_MAX; t++) {
                    final File
                        fSrc=Protein.strapFile(t, pn, dirDndData()),
                        fDst=Protein.strapFile(t, pn, dirWorking());
                    if (sze(fSrc)>0 && (System.currentTimeMillis()-fSrc.lastModified())<100*1000) cpyIfDiffers(fSrc,fDst);

                }
            }
            final Protein ppp[][]=loadTheProteinsInList(OPTION_PROCESS_LOADED_PROTS|OPTION_EVENT_PROTEINS_ADDED|OPTION_SHOW_ORPHAN_HETEROS_IN_3D|DROP_HETERO_INVERSE_MX, sb, new Object[]{v3dTarget, ppTarget, jList!=null?null:StrapView.KEY_AP, intObjct(row), v3dTarget});
            if (ppp!=null) {
                final List<HeteroCompound> v=new Vector();
                for(int i=sze(ppp[0]); --i>=0;) {
                    final Protein p=ppp[0][i];
                    if (p==null) continue;
                    if (p.getCharacters().length==0) adAll(p.getHeteroCompounds('N'), v);
                    else {
                        for(Object fn : oo(mapHeteros.get(p.getName()))) {
                            final File fSrc=file(toStrg(fn));
                            if (sze(fSrc)==0) continue;
                            final HeteroCompound hh[]=HeteroCompound.parse(readBytes(fSrc),false);
                            for(HeteroCompound h : hh) h.setFile(fSrc);
                            if (p.addHeteroCompounds(Protein.HETERO_ADD_UNIQUE,hh)) strapEvent=StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED;
                        }
                    }
                }
            }
        } else {
            Protein[] ppDropped=derefArray(_dropObjects,Protein.class);
            if (sze(ppDropped)==0 && sze(ppDropped=_dropProteinsFromStrgSelection)>0) {
                String fn=ppDropped.length==1 ? ppDropped[0].getName() : null;
                if (fn==null || fn==UNNAMED ) {
                    final CharSequence suggest=new BA(0).join(ppDropped,"_");
                    final Object tf=new ChTextField(sze(suggest)<40?suggest:"",COMPLETION_FILES_IN_WORKING_DIR).cols(60,true,true);
                    if (!ChMsg.yesNo(pnl(VBHB,"Enter the protein file name"+(ppDropped.length==1 ? "" :" for "+ppDropped.length+" sequence"),tf)) || 0==sze(fn=toStrg(tf))) ppDropped=null;
                }
                if (ppDropped!=null) {
                    final File f=file(filtrS('+'|FILTER_NO_MATCH_TO,FILENM, fn));
                    if (f!=null && (sze(f)==0  || fileEquls(f,_dropProtsFromStrgBA) || ChMsg.yesNo(pnl(VBPNL,"The file",f,"already exists. Overwrite?")))) {
                        wrte(f,_dropProtsFromStrgBA);
                        Protein[] rm=new Protein[ppDropped.length];
                        for(int iP=ppDropped.length; --iP>=0;) {
                            final Protein p=ppDropped[iP], pAlready=proteinWithName(p.getName(), proteins());
                            p.setFile(f);
                            if (pAlready!=null) {
                                if (p.getResidueTypeAsString().equalsIgnoreCase(pAlready.getResidueTypeAsString())) ppDropped[iP]=pAlready;
                                else rm[iP]=pAlready;
                            }
                        }
                        rmProteins(false,rm);
                    } else ppDropped=null;
                }
            }
            if (ppDropped!=null) {
                final boolean hide=dndTarget(Collection.class)==ppNotAliV();
                if (hide) setIsInAlignment(false, 11, ppDropped);
                else processLoadedProteins(0,ppDropped, new Object[]{view, intObjct(row), v3dTarget});
                dispatch(new StrapEvent(this, hide ? StrapEvent.PROTEINS_HIDDEN : StrapEvent.PROTEINS_ADDED).setParameters(new Object[]{ppDropped,intObjct(row)}));
            }
            StrapEvent.dispatchLater(StrapEvent.ORDER_OF_PROTEINS_CHANGED,5);
        }
        if (view!=null) view.dropObjects(oo, DND_TARGET);
        _dropImg=null;
        _dropProteinsFromStrgSelection=null;
        _dropProtLnk=_dropStrg=null;
        drawMessage("");
        ChDelay.repaint(targetC,5);
        revalAndRepaintC(jList);
        if (strapEvent>=0)  StrapEvent.dispatchLater(strapEvent,111);
        DND_TARGET.clear();
    }/*drop*/
    private static ChCombo _dragBiomChoice;
    private static ChButton _dragBiomTog;
    private static AbstractButton[] _dragRadioOrigF, _dragRadioChains, _dragRadioMSA;
    private static int dragOptions() {
        pnlDragOptions();
        return
            (radioGrpIdx(_dragRadioMSA)==1    ? Protein.DRAG_MSA : 0) |
            (radioGrpIdx(_dragRadioChains)==1 ? Protein.DRAG_ALL_CHAINS : 0) |
            (radioGrpIdx(_dragRadioOrigF)==0  ? Protein.DRAG_ORIG : 0)  |
            (_dragBiomTog.s()  ? ((_dragBiomChoice.i()+1)<<8) : 0);
    }

    private static Object pnlDragOptions() {
        if (_dragOpts==null) {
            final String ss[]=new String[100];
            for(int i=ss.length; --i>=0; ) ss[i]="Molecule "+(1+i);
            _dragRadioOrigF= radioGrp(new String[]{"Simply copy the protein file","Write the protein with transformed coordinates"},0,li());
            _dragRadioChains=radioGrp(new String[]{"Drag only current protein chain", "Drag all chains of the original structure file"},0,li());
            _dragRadioMSA=radioGrp(new String[]{"Transfer only current protein in fasta format", "Transfer complete multiple sequence file"},0,li());
            _dragBiomTog=toggl("Write complete biological molecule").li(li());
            final Object
                pnlBio=pnl(VBHB,
                           pnl(HBL,_dragBiomChoice=new ChCombo(ss),"  ","Warning: There might be a delay<br>of a few seconds for large files."),
                           "Biological molecules are defined by BIOMT lines in the PDB file"
                           ),
                pnlPdb=pnl(VBHB,"#ETBem",
                           pnl(HB,"<b>Options for PDB structure files</b>")," ",
                           _dragRadioOrigF,
                           " ",
                           _dragRadioChains,
                           " ",
                           _dragBiomTog.cb(),
                           pnl(pnlBio),
                           " "
                           ),
                pnlMSF=pnl(VBHB,"#ETBem",
                           pnl(HB,"<b>Options for multiple sequence files like clustalW or MSF or multiple Fasta </b>","#","WIKI:Stockholm_format"),
                           " ",
                           _dragRadioMSA
                           ),
                pnlHet=pnl(VBHB,"#ETBem","<b>Options for RNA/DNA structures, ligands and hetero compounds </b>"," ", buttn(TOG_DND_TRANSFORM_HETERO).cb()),
                pnl=pnl(VBHB, pnl(HB,"<b><u>Options for protein export via Drag-and-Drop</b></u>","#",smallHelpBut(DialogExportProteins.class)),
                        pnlPdb,pnlMSF,pnlHet,
                        new ChButton("DRAG_RST").li(li()).t("Restore defaults")
                        );
            _dragBiomTog.doCollapse(pnlBio);
            _dragOpts=pnl;
        }
        return _dragOpts;
    }
    /* <<< Drop <<< */
    /* ---------------------------------------- */
    /* >>> Read proteins >>> */
    public static char chainAfterUS(String p) {
        final int i=usChainOrFileExt(p);
        return chrAt(i,p)=='_' ? chrAt(i+1,p) : 0;
    }
    static String colonChain(String p) {
        if (p==null) return "";
        final int dot=p.lastIndexOf('.'), colon=p.indexOf(':', dot+1), exclam=p.indexOf('!', dot+1);
        if (colon<0 || exclam>0 && exclam<colon) return "";
        final int to=exclam>0 ? exclam : sze(p);
        if (to-colon<2) return "";
        for(int i=colon+1; i<to; i++) if (!is(LETTR_DIGT,p, i)) return "";
        return p.substring(colon+1,to);
    }
    static int usChainOrFileExt(String s) {
        final int d=s==null? -1 : s.lastIndexOf('.');
        return
            d< maxi(s.lastIndexOf('/'), s.lastIndexOf('\\')) ?  -1 :
            d>3 && chrAt(d-2,s)=='_' && is(UPPR_DIGT,s,d-1) && (strEquls(STRSTR_IC,".ent",s,d)||strEquls(STRSTR_IC,".pdb",s,d)) ? d-2 :
            d;
    }
    private static int lenWithoutColonChain(String p) {
        if (p==null) return 0;
        final int max=maxi(p.lastIndexOf('/'),p.lastIndexOf('\\'),p.lastIndexOf('.'));
        return strchr(STRSTR_E, ':', p, max, MAX_INT);
    }
    public static String name2file(String name) {
        if (name==null) return null;
        final String n=Protein.urlCd(true,name, isWin() && looks(LIKE_DRIVE_LETTER, name,0) ? 2:0);
        return n.substring(0,lenWithoutColonChain(n));
    }
    private static ChThread _threadWatchFiles;
    final static List<java.lang.ref.Reference<ProteinViewer>> vViewersOfDisposed=new Vector();
    private static Protein mayTakeFrom3DViewer(File f, String name) {
        final List v=vViewersOfDisposed;
        for(int i=sze(v); --i>=0;) {
            final ProteinViewer pv=get(i,v, ProteinViewer.class);
            final Protein p=sp(pv);
            if (p==null) { v.remove(i); continue; }
            if (f.equals(p.getFile()) && name.equals(p.getName())) {
                v.remove(i);
                return p;
            }
        }
        return null;
    }
    private static Protein readProtein(String s, BA ba) {
        final StrapAlign align=getInstance();
        if (s==null) return null;
        if (align!=null && !isEDT()) {
            final Protein[] result={null};
            inEdtCR(align,"LOAD_P",new Object[]{s,ba,result});
            return result[0];
        }
        final int us_dot=usChainOrFileExt(s);
        final int to=lenWithoutColonChain(s);
        final boolean us_chain=chrAt(us_dot,s)=='_';
        final File fAllChains=file(us_chain ? s.substring(0,us_dot)+s.substring(us_dot+2,to) : s.substring(0,to));
        File fParse=us_chain ?  file(name2file(s)) : fAllChains;
        if (sze(fParse)==0 && s.indexOf('!')>0) {
            final File f=file(name2file(delLstCmpnt(s,'!')));
            if (sze(f)>0) fParse=f;
        }
        if (us_chain && sze(fAllChains)>0 && fParse!=null) {
            final BA buffer=ba4CurrThrd(Protein.KEY_BA_PARSE);
            if (sze(fParse)==0 && !PDB_separateChains.writePdbOnlyChain(fParse,s.charAt(us_dot+1),readBytes(fAllChains,buffer))) return null;
        }
        final int lstSlash=maxi(s.lastIndexOf('/'),s.lastIndexOf('\\'));
        final String name=Protein.urlCd(false, s.substring(lstSlash+1),0);
        final Protein pLoaded=align!=null ? proteinWithName(name, proteins()) : null;
        if (pLoaded!=null) return pLoaded;
        final Protein pRecycle=deref(mayTakeFrom3DViewer(fParse, name), Protein.class);
        if (pRecycle!=null) return pRecycle;
        final Protein p=new Protein(align);
        if (us_dot>0 && !us_chain) {
            final String colonChain=colonChain(s);
            if (sze(colonChain)>0) p.setOnlyChains(colonChain);
        }
        p.setFile(fParse);
        p.setName(name);
        SPUtils.parseProtein(fParse,ba, SPUtils.parserOptions(),p);
        if (ba!=null && ba.end()<555 && strstr("Runtime Error in ",ba)>0) delFileOnExit(fParse);
        final int exclam=name.indexOf('!',lstSlash+1);
        if (exclam>0) {
            final String expr=name.substring(exclam+1);
            p.setResidueSubset(chrAt(exclam+1,name)=='-' ? "1"+expr : expr);
        }
        readAttributes(align!=null ? "" :"31AN",p, (File)null);
        if (_threadWatchFiles==null && align!=null) _threadWatchFiles=ChThread.callEvery(0, 999, thrdCR(align,"WATCH_PROTEIN_FILES"),  "WATCH_PROTEIN_FILES");
        if (p.getCharacters().length>0) p.findIconImage();
        return p;
    }
    public static void readAttributes(String what, Protein p, File dir0) {
        final boolean all="".equals(what), wm=0!=(Protein.optionsG()&Protein.G_OPT_VIEWER);
        final File dir=dir0==null && wm ? dirStrapAnno() : null;

        if (all || what.indexOf('G')>=0) {
            final File f=Protein.strapFile(Protein.mGAPS, p,dir);
            if (sze(f)>0) {
                final BA ba=readBytes(f,ba4CurrThrd(Protein.KEY_BA_PARSE));
                if (ba!=null) p.parseGaps(ba.bytes(),ba.end());
            }
        }
        if (all || what.indexOf('1')>=0) {
            final BA txt=readBytes(Protein.strapFile(Protein.m1stIdx, p, dir));
            if (txt!=null) p.setResidueIndexOffset(atoi(txt));
        }
        if (all || what.indexOf('3')>=0) {
            final BA txt=readBytes(Protein.strapFile(Protein.mTRANS, p, dir));
            p.setRotationAndTranslation(txt!=null ? new Matrix3D(txt) : null);
        }
        if (all || what.indexOf('A')>=0) {
            ResidueAnnotation.loadResidueAnnotation(p,Protein.strapFile(Protein.mANNO,p,dir));
        }
        if (all || what.indexOf('N')>=0) {
            final File f=Protein.strapFile(Protein.mDNA, p, dir);
            final BA ba=sze(f)>0 ? readBytes(f,ba4CurrThrd(Protein.KEY_BA_PARSE)) : null;
            if (ba!=null) p.parseFrameShift(ba.bytes(), ba.end());
        }
        if (all && !p.isLoadedFromStructureFile()) {
            final File f=Protein.strapFile(Protein.mASSOCPDB, p,dir);
            final String pdbId=toStrgTrim(readBytes(f));
            if (sze(pdbId)>3) SPUtils.inferCoordinates1(p,pdbId);
        }
    }
    /** Get all available protein parsers. */
    public static ProteinParser[] proteinParsers() {
        final int mc=StrapPlugins.mc();
        if (_parsers==null || _parsersMc!=mc) {
            final List v=new Vector();
            _parsersMc=mc;
            for(Object c: StrapPlugins.allClassesV(ProteinParser.class).asArray()) v.add(mkInstance(c,ProteinParser.class,true));
            _parsers=toArry(v, ProteinParser.class);
        }
        return _parsers;
    }

    private static ProteinParser _parsers[];
    /* <<< Utilities for reading Proteins <<< */
    /* ---------------------------------------- */
    /* >>> ResidueSelection >>> */
    /** Displays a named selection of residues so that the user can edit it */
    public static void editAnnotation(ResidueAnnotation s) {
        if (s==null) return;
        ResidueAnnotationView v=gcp(KEY_GET_VIEW, s,ResidueAnnotationView.class);
        if (v==null) v=new ResidueAnnotationView(s);
        ChTabPane pa=_panAnno;
        if (pa==null) {
            pa=_panAnno=new ChTabPane(ChTabPane.RIGHT|ChTabPane.COLLAPSE);
            TabItemTipIcon.set("","Named residue selections","residue selections<br>associated to the proteins",IC_ANNO, pa);
            setMinSze(1,1, pa);
            addActLi(li(),pa);
        }
        pa.addTab(CLOSE_ALLOWED, (Component)v.getPanel(HasPanel.NEW_PANEL));
        addDialog(pa);
    }
    /* <<< ResidueSelection <<< */
    /* ---------------------------------------- */
    /* >>> List proteins and annotations >>> */
    private static ChJList jList(Object oo[], long option) {
        final List v=new Vector();
        adAll(oo,v);
        final ChJList l=new ChJList(v,option|ChJTable.DRAG_ENABLED|ChJTable.ICON_ROW_HEIGHT);
        addAwtListeners(l);
        newDropTarget(l,false);
        pcp(OBJECT_JLIST,"",l);
        updateOn(CHANGED_CHILDS_OF_PROTEIN,l);
        updateOn(CHANGED_PROTEIN_LABEL,l);
        pcpAddOpt(KEY_CLOSE_OPT, CLOSE_DISPOSE, l);
        return l;
    }
    private static BA _jlistHelp;
    private static Map<Object,ChJList> mapChJList=new HashMap();
    public static ChJList showInJList(long options, Object id, Object oo[], Object north, Object south) {
        Object panNorth=north;
        ChJList jList=id==null?null:mapChJList.get(id);
        if (jList==null) mapChJList.put(id,jList=jList(oo,options|ChJTable.FILE_TRANSFER_HANDLER|ChJTable.DEFAULT_RENDERER));
        else {
            if (0!=(options&ChJTable.CHJTABLE_CLEAR)) jList.getList().clear();
            adAllUniq(oo, jList.getList());
            revalAndRepaintC(jList);
        }
        JComponent pan=gcp(KEY_PANEL, jList, JComponent.class);
        if (pan==null) {
            final boolean hasP=get(oo,Protein.class)!=null, hasS=get(oo,ResidueSelection.class)!=null, hasH=get(oo,HeteroCompound.class)!=null;
            final Object ref=wref(jList), bSH, bSH_undo, bS, bUS, bScr;
            if (hasP||hasS) {
                bS=new ChButton("SEL").t("Select").tt("Select in alignment pane");
                bUS=new ChButton("UNSEL").t("Deselect").tt("Deselect in alignment pane");
                bScr=new ChButton("SCROLL").t("Scroll to");
                for(int i=3; --i>=0;) {
                    final ChButton b=(ChButton)(i==0?bS:i==1?bUS:bScr);
                    addCP(ChJList.KEY_ENABLE_IF_SELECTED, b.li(li()).enabled(false), jList);
                }
                final Object
                    diap=!hasP?null:new DialogSort(DialogSort.PROTEINS, jList),
                    dias=!hasS?null:new DialogSort(DialogSort.RES_SEL,  jList);
                panNorth=pnl(VBHB,
                             !hasP?null:pnlTogglOpts("*Sort proteins", diap),
                             !hasP?null:diap,
                             !hasS?null:pnlTogglOpts("*Sort residue selections", dias),
                             !hasS?null:dias
                             );

            } else {
                bScr=bUS=null;
                bS=hasH ? buttn(TOG_DND_TRANSFORM_HETERO).cb() : null;
            }
            if (_jlistHelp==null) _jlistHelp=new BA(0).join(new Object[]{getHlp(ChJList.class),getHlp(ChTransferable.class)}).trimSize();
            pcp(ChJList.KEY_NO_HELP_ON_RIGHT_CLICK,"",jList);
            pcp(OBJECT_JLIST,ref,jList);
            pcp(OBJECT_JLIST,ref, bS);
            pcp(OBJECT_JLIST,ref, bUS);
            pcp(OBJECT_JLIST,ref, bScr);
            if (hasP) {
                bSH_undo=new ChButton(ChButton.DISABLED,"SHOW_HIDE_U").t("Undo").li(li())
                    .tt("Show the proteins as befor pressing the button")
                    .cp(OBJECT_JLIST,ref);
                bSH=new ChButton("SHOW_HIDE").t("These proteins in alignment pane").li(li())
                    .tt("Exactly the proteins in this list are shown in the alignment.<br>The others are set invisible.<br>"+
                        "To get back hidden proteins go menu-bar &gt; Select &gt; " +getTxt(button(BUT_hiddenP)))
                    .cp("SHOW_HIDE_U",bSH_undo)
                    .cp(OBJECT_JLIST,ref);
            } else bSH_undo=bSH=null;
            pcp(DialogStringMatch.KEY_SAVE, "StrapObjects",jList);
            final Object
                pBut=pnl(CNSEW,pnl(bSH,bSH_undo, bScr, bS, bUS), null,null,smallHelpBut(_jlistHelp)),
                undockTarget=undockNewTarget(jList);

            pan=pnl(CNSEW,scrllpn(0,jList/*, dim(EX,3*EX)*/), panNorth, pnl(CNSEW,undockTarget,pBut,south));
            pcp(KEY_PANEL,pan,jList);
        }
        return jList;
    }
    /* <<< List proteins and annotations <<< */
    /* ------------------------------------- */
    /* >>> Scripting >>> */
    private static ChScriptPanel _scriptPnl;
    private static BA _scriptList;
    private static void openScriptPanel() {
        if (_scriptPnl==null) {
             final Object[] wc={
                StrapScriptInterpreter.ANY_VIEWER,  StrapScriptInterpreter.ANY_WIRE,
                Protein3dUtils.allCommands(),
                StrapScriptInterpreter.allScriptCommands(),
                dirWorking(),
                "$PROTEINS_WITH_PDB_ID",
                "$PROTEINS_WITH_UNIPROT_ID",
                ResidueAnnotation.vAll,
                ResSelUtils.variables(0),
                SequenceAligner.KOPT_NOT_USE_STRUCTURE,
             };
             final Object buts[]={
                 new ChButton("ALL_CMDS").li(li()).rover(IC_LIST).tt("Alphabetical list of all commands"),
                 ChButton.doOpenURL(URL_STRAP+"web/strap_script.html").rover(IC_HELP)
             };
             _scriptPnl=new ChScriptPanel("STRAP", wc, buts, getInstance());
             final ChTextArea ta= _scriptPnl.textPane();
             highlightProteins("PAD", ta);
            final JComponent pSouth=gcp(KEY_SOUTH_PANEL, ta, JComponent.class);
            if (pSouth!=null) pSouth.add(ChButton.doView(StrapScriptInterpreter.log()).t("Errors"), 0);
            final ChTextComponents t=ta.tools().underlineRefs(ULREFS_WEB_COLORS);
            final long options=HIGHLIGHT_UPDATE_IF_TEXT_CHANGES|HIGHLIGHT_NOT_IN_SCROLLBAR|HIGHLIGHT_STYLE_UL;
            t.highlightOccurrence(StrapScriptInterpreter.allScriptCommands(),chrClas(SPC),null, options, C(COLOR_REF_GRAY));
            t.highlightOccurrence(Protein3dUtils.allCommands(),chrClas(SPC),chrClas(SPC), options, C(COLOR_REF_GRAY));
            final Object hc=HelpCommands.getInstance(Strap.SCRIPT_COMMANDS);
            pcp(HelpCommands.class,hc, rtt(StrapScriptInterpreter.log().textView(true)));
            pcp(HelpCommands.class,hc, rtt(ta));
        }
        _scriptPnl.showFrame("Scripts for Strap");
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Default aligner >>> */

    private static Map<Class,String> mapDefaultClass=new HashMap();
    public static String defaultClass(Class c) {
        String cn=mapDefaultClass.get(c);
        if (cn==null) {
            final boolean useInst=isSystProprty(IS_USE_INSTALLED_SOFTWARE);
            if (c==SequenceAlignerTakesProfile.class)    cn=CLASS_MultipleAlignerClustalW;
            if (c==SequenceAligner.class)  cn=Insecure.EXEC_ALLOWED ? CLASS_Aligner3D : CLASS_PairAlignerNeoBioPROXY;
            if (c==PhylogeneticTree.class) cn=Insecure.CLASSLOADING_ALLOWED ? CLASS_PhylogeneticTree_Archaeopteryx : CLASS_PhylogeneticTree_TreeTop;
            if (c==Superimpose3D.class || c==SequenceAligner3D.class) cn=useInst && fExists("/usr/bin/tm-align") ? CLASS_Superimpose_TM_align : CLASS_Superimpose_CEPROXY;
            if (c==ProteinViewer.class) cn=useInst && fExists("/usr/bin/pymol") ? CLASS_Pymol : CLASS_ChAstexPROXY;
        }
        return cn;
    }
    public static boolean setDefaultC(Class c, String cn0) {
        if (c==null || cn0==null) return false;
        final String cn=cn0.indexOf('.')>0 ? cn0 : StrapPlugins.mapS2L(cn0);
        if (cn!=null && (cn!=cn0 || cn.indexOf('.')>0)) {
            mapDefaultClass.put(c, cn);
            return true;
        } else error("@Error in setDefaultC c="+c+" cn0="+cn0+" cn="+cn);
        return false;
    }
    public static boolean setDefaultClassByPara(String a) {
        final int eq;
        if (sze(a)<6 || a.charAt(0)!='-' || (eq=strchr('=',a))<0 || eq>sze(a)-2) return false;
        Class defaultC=null;
        if (a.startsWith("-default")) {
            for(Class c : StrapPlugins.interfaces()) {
                final String cn=nam(c);
                if (strEquls(0L, a,8,eq,  cn, cn.lastIndexOf('.')+1)) defaultC=c;
            }
        }
        if (defaultC!=null) {}
        else if (a.startsWith("-v3d="))      defaultC=ProteinViewer.class;
        else if (a.startsWith("-s3d="))      defaultC=Superimpose3D.class;
        else if (a.startsWith("-a3d="))      defaultC=SequenceAligner3D.class;
        else if (a.startsWith("-aligner="))  defaultC=SequenceAligner.class;
        else if (a.startsWith("-alignerP="))  defaultC=SequenceAlignerTakesProfile.class;
        else if (a.startsWith("-aligner2="))  {
            final String cn0=a.substring(eq+1), cn=cn0.indexOf('.')>0 ? cn0 : StrapPlugins.mapS2L(cn0);
            AlignUtils.setDefaultAligner2(cn);
            return true;
        }
        return defaultC!=null ? setDefaultC(defaultC, a.substring(eq+1)) : false;
    }
    /* <<< Default aligner <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    public final synchronized static Protein[] proteinsWithRegex(CharSequence list, Protein pp[]) {
        return (Protein[])proteinsAndAnnotationsWithRegex(list,pp,'p');
    }
    /** 'A' means create ResidueAnnotation if slash in expression */
    public final synchronized static Object[] proteinsAndAnnotationsWithRegex(CharSequence list, Protein pp[], char alsoSelections) {
        if (list==null) return NO_OBJECT;
        final BA ba=list instanceof BA ? (BA)list:new BA(list);
        final byte T[]=ba.bytes();
        final ChTokenizer TOK=new ChTokenizer().setText(ba);
        List v=null;
        while(TOK.nextToken()) {
            final int f=TOK.from(), to=TOK.to(), slash=strchr('/',T,f,to), t=slash>=0?slash:to;
            final String expr=ba.newString(f,t);
            final char prop=
                "$PROTEINS_WITH_PDB_ID".equals(expr) ? 'P' :
                "$PROTEINS_WITH_UNIPROT_ID".equals(expr) ? 'U' :
                0;
            if (prop!=0) {
                for(Protein p : visibleProteins()) {
                    if (prop=='P' && p.getPdbID()==null) continue;
                    if (prop=='U' && p.getUniprotID()==null && FindUniprot.ids(p).length==0) continue;
                    (v==null?v=new Vector():v).add(p);
                }
                continue;
            }
            java.util.regex.Pattern pattern=null;
            final boolean allProts=t-f==1&&T[f]=='*' || t-f==2 && T[f]=='.' && T[f+1]=='*';
            try {
                if (!allProts && (strchr('*',T,f,t)>=0 || strchr('\\',T,f,t)>=0 || strchr('[',T,f,t)>=0)) pattern=java.util.regex.Pattern.compile(expr);
            } catch(Exception e){ putln(e);}
            for(Protein p:pp) {
                if (p==null || t<=f) continue;
                final String pn=p.getName(), wt=(String)p.OBJECTS[Protein.O_WEB_TOK1], accID=p.getAccessionID(), uniprotID=p.getUniprotID();
                final int len_pn=sze(pn);
                if (allProts ||
                    pattern!=null && (pattern.matcher(pn).matches() || wt!=null && pattern.matcher(wt).matches() ) ||
                    (len_pn==t-f || len_pn>t-f && chrAt(t-f,pn)=='!') && strEquls(0L, T, f, t,  pn, 0) ||
                    sze(wt)==t-f && strEquls(wt,T,f) ||
                    sze(accID)==t-f && strEquls(accID,T,f) ||
                    accID!=uniprotID && sze(uniprotID)==t-f && strEquls(uniprotID,T,f) ||
                    strEquls("PDB:",expr,0) && ( p.hasPdbId(expr) || strEquls(expr,wt,0))
                    ) {
                    if (slash<0) (v==null?v=new Vector():v).add(p);
                    else if ( (alsoSelections|32)=='a') {
                        boolean found=false;
                        for(ResidueSelection a : p.allResidueSelections()) {
                            final String an=nam(a);
                            if (an==null) continue;
                            java.util.regex.Pattern patt=null;
                            try {
                                if (to-slash>2 && (strchr('*',T,slash+1,to)>=0 || strchr('[',T,slash+1,to)>=0)) patt=java.util.regex.Pattern.compile(ba.newString(slash+1,to));
                            } catch(Exception e){ putln(e); }
                            if (to-slash==2&&T[slash+1]=='*' || patt!=null && patt.matcher(an).matches() || sze(an)==to-slash-1 && strEquls(an,T,slash+1)) {
                                (v==null?v=new Vector():v).add(a);
                                found=true;
                            }
                        }
                        if (!found && alsoSelections=='A' && looksLikeSet(T,slash+1,to, p.getResidueChain()!=null, true)) {
                            final ResidueAnnotation ra=new ResidueAnnotation(p);
                            ra.setValue(0,ResidueAnnotation.NAME,"selection");
                            p.removeResidueSelection(ra);
                            ra.addE(0,ResidueAnnotation.POS,ba.newString(slash+1,to));
                            (v==null?v=new Vector():v).add(ra);
                        }
                    }
                }
            }
        }
        return (alsoSelections|32)=='a' ?  oo(v) : toArry(v,Protein.NONE);
    }
    /* <<< Scripting <<< */
    /* ---------------------------------------- */
    /* >>> Msg >>> */
    public static void drawMessage(String s) {  drawMsg(s, _tool); }
    public static void errorMsg(String s) { drawMessage(sze(s)==0 ? null : chrAt(0,s)=='@' ? s.substring(0,2)+ANSI_RED+s.substring(2) : ANSI_RED+s); }
    public boolean paintHook(JComponent c,Graphics g, boolean after) {
        if (c==_shopping && after) {
            final ImageIcon ic=iicon(IC_SHOPPING);
            final int H=hght(c), x=_shoppingBusy<=0?0: (int) ((1+Math.sin(System.currentTimeMillis()/300))*10);
            if (H>0) g.drawImage(ic.getImage(), x, (H-ICON_HEIGHT)/2, frame());
            if (_shoppingBusy<=0) {
                g.setColor(C(0));
                g.drawString("Drop", ICON_HEIGHT*2, charA(g)+(H-charH(g))/2);
            }
            if (c==compAtMouse()) drawDivider(c, g, 0, 0, 222, H);
        }
        return true;
    }
    /* <<< msg <<< */
    /* ---------------------------------------- */
    /* >>> Drop area >>> */
    private static JWindow _shoppingW;
    private static JComponent _shopping;

    {
        if (withGui()) {
            final Object but=ChButton.doOpenURL("Drop_Web_Link*").t(null).rover(IC_BLANK).tt("Drop target<br>Click for more info which will appear in web browser.");
            _shopping=pnl(CNSEW,null,null,null,null, but, dim(9*EM+ICON_HEIGHT*3/2, ICON_HEIGHT+2));
            li().addTo("m", _shopping);
            setTip("Drop protein files and protein links into the shopping cart.<br>The files will be loaded in Strap.<br>It is detachable. Move it close to where the files or protein links are.<br><br>"+(!isWin()?null:"<sub>Limitations: Recent Versions of the IE-Web Browsers do not support Drag-and-drop</sub> "), _shopping);
            addPaintHook(this, _shopping);
            ChThread.callEvery(0, 99, thrdCR(this,"SHOPPING"),  "SHOPPING");
            newDropTarget(_shopping,true);
            addMoli(MOLI_REPAINT_ON_ENTER_AND_EXIT,_shopping);
        }
    }
    private static void setProgress(boolean b) { _shoppingBusy=10;} // ??????
    private void undockProgress() {
        JWindow f=_shoppingW;
        if (f==null) {
            _shoppingW=f=new JWindow();
            revalAndRepaintC(parentC(_shopping));
            final Point p=_shopping.getLocationOnScreen();
            f.setFocusable(false);
            f.add(_shopping);
            f.pack();
            f.setLocation(p);
            f.show();
            setWndwState('T',f);
            setDragMovesWindow(true,f);

        }
    }

    /* <<< Drop area <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

}
