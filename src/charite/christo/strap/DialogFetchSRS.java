package charite.christo.strap;
import charite.christo.*;
import javax.swing.*;
import java.awt.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

With this dialog a number of sequence files can be loaded from public servers.

A text containing  sequence IDs is pasted into the text area.

The program automatically identifies and highlights certain types of IDs within the text
on pressing the respective highlight button.

Subsequently, they can be  fetched from the respective server.

<br><br><b>Related Web-sites:</b> http://www.ensembl.org/Multi/martview

   @author Christoph Gille
deprecated: Q8ICC3
*/
public class DialogFetchSRS extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener {
    private final static String sTYPE[]={"HSLV_ECOLI\" (Swissprot)","P68871\" (letter + letters/digits)",
                                         "M57965\" (letter + digits)","gi|20259105 \" (NCBI)","NM_002726\" (NCBI)",
                                         "UPI0000672BF8 (Uniparc)"};
    private final static int HSLV_ECOLI=0,P68871=1,M57965=2,GI_20259105=3,NM_002726=4, UPI0000672BF8=5;
    private final ChTextArea _ta=new ChTextArea(4,4);//.saveInFile("DialogFetchSRS");
    private final JComponent  pLoad=pnl();
    private static Customize _cust;
    private String _words[];
    private TextMatches _highlight;
    public DialogFetchSRS() {
        if (_cust==null) {
            final String ss[]={"EXPASY:","UNIPROT:","UNIPARC:","EMBL:","NCBI:","KEGG_AA:","KEGG_NT:", "ENSEMBL:"};
            _cust=new Customize("fetchProteinFile",ss,"URLs to get protein files from. Asterisk $* is replaced by the ID.",Customize.LIST);
        }
        addActLi(this,_cust);
        setSettings(this, _cust);
        final Container pHighlight=pnl(new GridLayout(sTYPE.length,1));
        for(String t : sTYPE) {
            final ChButton b=new ChButton("like \""+t).li(this);
            b.setHorizontalAlignment(LEFT);
            pHighlight.add(b);
        }
        layoutLoadPanel();
        final Object
            pNorth=pnl(HBL,
                       "First step: Enter text containing protein IDs  ",
                       new ChButton("EX").t("For newbies: show example").li(this)
                       ),
            pAlsoSee=pnl(VBHB,
                          StrapAlign.b(CLASS_DialogFetchPdb),
                          WATCH_MOVIE+MOVIE_Load_Proteins+" "+MOVIE_Load_Proteins_web_pdb
                          ),

            pSouth=pnl(VBHB,"Last step: Click the respective file server:",
                       pnl(pLoad),
                       pnlTogglOpts("Also see ",pAlsoSee),
                       pAlsoSee),
            pHL=pnl(CNSEW,pnl(pHighlight),"2nd step: Highlight all IDs in the text"),
            pCenter=pnl(CNSEW,scrllpn(_ta),null,null,pHL),
            pan=pnl(CNSEW,pCenter,pnl(VBPNL,dialogHead(this),pNorth),pSouth);
        pcp(KEY_IF_EMPTY,"First step: Enter here a text with  protein IDs \n",_ta);
        addActLi(this,_ta.tools().enableWordCompletion(dirWorking()).enableUndo(true));
        adMainTab(remainSpcS(pan), this,null);
    }

    private void layoutLoadPanel() {
        pLoad.removeAll();
        final String dbs[]=_cust.getSettings();
        pLoad.setLayout(new GridLayout(1,dbs.length));
        for(String db: dbs) {
            pLoad.add(new ChButton("L").li(this).t(delSfx(':',db)).cp("L",db));
        }
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        boolean clrErr=false;
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final ChTextArea ta=_ta;
        final ChTextComponents tools=ta.tools();

        if (q instanceof Customize) { layoutLoadPanel(); revalidate();}
        if (cmd=="EX")
            ta.setText(
                       "HSLV_ECOLI or HSLV_BACSU or P68871 can be loaded from Uniprot and Expasy\n\n"+
                       "M57965 can be loaded from embl.\n\n"+
                       "UPI0000672BF8 ore UPI0000685C48 are loaded from UNIPARC\n"+
                       "NM_002726 or gi|20259105 can be loaded from NCBI\n"+
                       ta.getText());

        for(int iT=sTYPE.length;--iT>=0;) {
            if (cmd.endsWith(sTYPE[iT])) {
                tools.setChanged();
                final BA ba=tools.byteArray();
                tools.removeHighlight(_highlight);
                final byte T[]=ba.bytes();
                final int iE=ta.getSelectionEnd(), iS=ta.getSelectionStart();
                final boolean invalid=iS<0||iS>=iE-1;
                final int[] fromTo=findIds(iT, T, invalid?0:iS,  invalid?ba.end():iE);
                //  hslv_ecoli hslv_bacsu
                _highlight=new TextMatches(0L, fromTo, MAX_INT, C(0xFF4400,0x80));
                tools.addHighlight(_highlight);
                final String ww[]=_words=new String[sze(fromTo)/2];
                for(int i=0; i<_words.length; i++) ww[i]=ba.newStringTrim(fromTo[2*i],fromTo[2*i+1]);
                repaint();
                clrErr=true;
            }
        }
        if (q==ta) { tools.removeHighlight(_highlight); _words=null; _highlight=null; clrErr=true; }
        if (cmd=="L") {
            if (tools.byteArray().onlyWhiteSpace()) { StrapAlign.errorMsg("Type/paste a text containing sequence IDs ");return;}
            final String DB=gcps("L",q), ww[]=_words;
            if (ww==null) StrapAlign.errorMsg("Highlight the IDs by pressing one of the 'highlight..'-buttons");
            else if (ww.length>0) {
                final String urls[]=new String[ww.length];
                for(int i=0;i<ww.length;i++) urls[i]=DB+ww[i];
                adTab(DISPOSE_CtrlW, delSfx(':',DB), new FilesFetchedFromServer(urls,ww), this);
            }
        }
        if (clrErr) StrapAlign.errorMsg("");
    }

    private static boolean t(char type, byte b) {
        switch(type) {
        case 'n': return '0'<=b && b<='9';
        case 'L': return 'A'<=b && b<='Z';
        case 'W': return '0'<=b && b<='9' || 'A'<=b && b<='Z';
        case 'w': return '0'<=b && b<='9' || 'a'<=b && b<='z' || 'A'<=b && b<='Z';
        }
        return false;
    }
    private static int[] findIds(int type, byte[] T, int iStart, int iEnd) {
        int[] fromTo=null;
        for(;;) {
            int count=0;
            for(int i=iStart; i<iEnd-2; i++) {
                int start=i, end=0;
                switch(type) {
                case NM_002726:
                case HSLV_ECOLI:
                    if (T[i]=='_') {
                        final int nLeft=count("LEFT",T,i-1), nRight=count("DIGIT_OR_LETTER",T,i+1);
                        if (nRight>2 && nLeft>1) {
                            start=i-nLeft;
                            end=i+nRight+1;
                        }
                    }
                    break;
                case P68871:
                    if ((i==0 || !t('w',T[i-1])) && t('L',T[i]) && t('W',T[i+1]) ) {
                        final int nDigitsOrCapitals=count("DIGIT_OR_CAPITAL",T,i+1);
                        int nDigits=0;
                        for(int j=0;j<nDigitsOrCapitals;j++) if (t('n',T[i+1+j])) nDigits++;
                        if (nDigitsOrCapitals>4 && nDigits>0) end=i+nDigitsOrCapitals+1;
                    }
                    break;
                case GI_20259105:
                    if (i>=3 && (i==3 || !t('w',T[i-4]) && (T[i-3]|32)=='g' && (T[i-2]|32)=='i'  && (T[i-1]=='|' || T[i-1]==':')) ) {
                        final int nDigits=count("DIGIT",T,i+1);
                        if (nDigits>0) end=i+nDigits+1;
                    }
                    break;
                case M57965:
                    if ((i==0 || !t('w',T[i-1])) && t('L',T[i]) && t('n',T[i+1]) ) {
                        final int nDigits=count("DIGIT",T,i+1);
                        if (nDigits>4 && (T.length<=i+nDigits+1 || !t('w',T[i+nDigits+1]))) end=i+nDigits+1;
                    }
                    break;
                case
                    UPI0000672BF8:
                    if (T[i]=='U' && T[i+1]=='P' && T[i+2]=='I' && cntainsOnly(LETTR_DIGT, T,i+3,i+13)) end=i+13;
                    break;
                }
                if (end>0) {
                    if (fromTo!=null) {
                        fromTo[2*count]=start;
                        fromTo[2*count+1]=end;
                    }
                    count++;
                }
            }
            if (fromTo==null)  fromTo=new int[2*count]; else break;
        }
        return fromTo;
    }
    // ----------------------------------------
    private static int count(String what,byte[] T, int pos) {
        if (T==null) return 0;
        int count=0;
        final char t=what=="DIGIT" ? 'n' : what=="DIGIT_OR_CAPITAL" ? 'W' : what=="DIGIT_OR_LETTER" ? 'w' : 0;
        int i=pos;
        if (t!=0)  while (i<T.length && t(t,T[i])) { count++; i++;}
        else if (what=="LEFT") while (i>=0 && t('w',T[i])) { count++; i--;}
        return count;
    }
}
