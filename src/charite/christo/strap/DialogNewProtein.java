package charite.christo.strap;
import charite.christo.*;
import java.awt.event.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**HELP

The protein name and the sequence data is entered.
The sequence of one letter codes can be entered directly or a one of several sequence file formats can be used.

<i>SEE_CLASS:charite.christo.protein.ProteinParser</i>.

   @author Christoph Gille

*/
public class DialogNewProtein extends AbstractDialogJPanel implements ActionListener {
    private final javax.swing.JComponent
        _tf=new ChTextField("",COMPLETION_FILES_IN_WORKING_DIR).cols(30,false,true),
        _ta=new ChTextArea(""),
        _b=new ChButton("Create protein file");
    public DialogNewProtein() {
        addActLi(this,_tf);
        addActLi(this,_ta);
        addActLi(this,_b);
        pcp(KEY_IF_EMPTY,"Enter the name of the protein file.",_tf);
        pcp(KEY_IF_EMPTY,"Enter the the one letter sequence text or any protein file format",_ta);
        _b.setEnabled(false);
        ChTextComponents.tools(_ta).enableUndo(true);
        pnl(this, CNSEW, scrllpn(SCRLLPN_INHERIT_SIZE, _ta),pnl(VB,dialogHead(this),pnl(HB, _tf," ",_b,"#")),pnl(REMAINING_VSPC1));
        StrapAlign.highlightProteins("P",_tf);
    }
    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        if (q==_b) {
            final String fn=toStrg(_tf).replaceAll(REGEX_NO_FILE_NAME,"_"), t=toStrg(_ta);
            final java.io.File f=file(fn);
            if (f==null) return;
            if (isDir(f)) { error(f+"<br> is a directory"); return;}
            if (sze(f)>0 && !ChMsg.yesNo(f+"<br>already exists, overwrite ?")) return;
            StrapAlign.rmProteins(false,SPUtils.proteinWithName(fn,StrapAlign.proteins()));
            wrte(f,t);
            StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED, fn);
        }
        if (q==_tf || q==_ta) {
            _b.setEnabled(!ChTextComponents.tools(_tf).isEmpty() && !ChTextComponents.tools(_ta).isEmpty());
        }

    }

}
