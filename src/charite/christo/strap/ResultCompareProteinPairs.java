package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
In each line of the input file the program expects two (or more) protein files.
The proteins are loaded and compared with each other.

In the result list the protein names and the alignment score are written.

For time consuming computations like 3D-superpositions, this can be
used to fill the cache with results over-night.
*/
class ResultCompareProteinPairs extends AbstractDialogJPanel implements ChRunnable, ActionListener  {
    private final Vector<Vector<String>> vResult=new Vector();
    private final Vector<String> vColumnNames=new Vector();
    private final BA LOG=new BA("");
    { vColumnNames.add("protein1"); vColumnNames.add("protein2"); vColumnNames.add("score");}
    private final JTable _jTable=new JTable(vResult,vColumnNames);
    private final Object _clazz, _otherClass,  _sharedInst;
    private final int _fromCol, _toCol;
    private final ChTextField _tfFile;
    private ChFrame _frameFs;
    private final ChButton butStop=new ChButton("stop").li(this), butGo=new ChButton("GO").t(ChButton.GO).li(this);
    public ResultCompareProteinPairs(Object clazz, Object sharedInstance, int fromColumn, int toColumn) {
        _fromCol=fromColumn;
        _toCol=toColumn;
        _clazz=clazz;
        _sharedInst=sharedInstance;
        _otherClass=isInstncOf(AbstractAlignAndCompare.class, sharedInstance) ? ((AbstractAlignAndCompare)sharedInstance).getOtherClass() : null;

        _tfFile=new ChTextField("",COMPLETION_DIRECTORY_PATH).li(this).saveInFile("ResultCompareProteinPairs_inputFile");
        _tfFile.tools().enableWordCompletion(dirWorking());
        pcp(KEY_IF_EMPTY,"Enter input file name",_tfFile);
        final Object
            pButtons=pnl(butGo, buttn(TOG_CACHE).cb()),
            pSouth=pnl( new GridLayout(2,1),pnl(CNSEW,_tfFile,null,null,new ChButton("HD").t("Browse").li(this),"Input file:"),pButtons);
        pnl(this,CNSEW, scrllpn(_jTable),dialogHead(this), pSouth);
        butGo.enabled(sze(file(_tfFile))>0);
        _tfFile.tools().underlineRefs(ULREFS_NOT_CLICKABLE);
    }

    public Object run(String id, Object arg) {
        if (id=="GO") {
            final BA ba=(BA)arg;
            final int ends[]=ba.eol();
            final byte[] text=ba.bytes();
            vResult.clear();
            for(int iL=0; iL<ends.length; iL++) {
                if (isDisposed()) break;
                final int b=iL==0 ? 0 : ends[iL-1]+1 , e=ends[iL];
                final String tt[]=splitTokns(0, text,b,e, chrClas(" \t\n,"));
                Protein[] pp=null;
                for(int i=tt.length; --i>=0;) {
                    if (isDisposed()) break;
                    final Protein p=protein(tt[i]);
                    if (p!=null) {
                        if (pp==null) pp=new Protein[i+1];
                        pp[i]=p;
                    }
                }
                pp=rmNullA(pp,Protein.class);
                if (pp==null) continue;
                long time0=System.currentTimeMillis();
                for(Protein p0 : pp) {
                    if (p0==null) continue;
                    final String n0=p0.getName();
                    Set setNames=mapProteinNames.get(n0);
                    if (setNames==null) mapProteinNames.put(n0,setNames=new HashSet());
                    for(Protein p1 : pp) {
                        if (p1==null || isDisposed()) continue;
                        final String n1=p1.getName();
                        if (setNames.contains(n1) || pp.length==2 && n0.equals(n1)) continue;
                        setNames.add(n1);
                        final Object dis=mkInstance(_clazz,Object.class,true);
                        if (dis==null) continue;
                        final double v=SPUtils.computeValue(0, dis,p0,p1,_fromCol,_toCol);
                        final Vector vR=new Vector();
                        vR.add(n0);
                        vR.add(n1);
                        vR.add(String.valueOf((float)v));
                        vResult.add(vR);
                        final long t=System.currentTimeMillis();
                        //putln("vResult="+vResult.size());
                        if (t-time0>333) {
                            //putln("TableModelEvent "+vResult.size());
                            revalAndRepaintC(_jTable);
                            time0=t;
                        }
                        dispos(dis);
                        sleep(10);
                    }
                    if (pp.length==2) break;
                }
                revalAndRepaintC(_jTable);
            }
            inEdtLaterCR(this,"FINISH",null);
        }
        if (id=="FINISH") {
            final Container parent=butStop.getParent();
            try {
                parent.remove(butStop);
                parent.add(labl("All done").fg(0xFF0000));
                revalAndRepaintC(parent);
            }catch(Exception e){}
        }
        return null;
    }
    private final Map<String,Set<String>> mapProteinNames=new HashMap();
    private Object _mapPP;
    private Protein protein(String s) {
        Protein p=SPUtils.proteinWithName(s,null);
        if (p==null) {
            Map<String,Protein> map=(Map)deref(_mapPP);
            if (map==null) _mapPP=newSoftRef(map=new HashMap());
            p=map.get(s);
            if (p==null) map.put(s,p=Protein.newInstance(file(s)));
        }
        if (p==null && LOG!=null) LOG.a("Could not load protein ").aln(s);
        return p;
    }
    public void actionPerformed(ActionEvent ev) {
        StrapAlign.errorMsg("");
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        if (q==_tfFile)  butGo.enabled(sze(file(_tfFile))>0);
        if (cmd=="HD") {
            if (_frameFs==null) _frameFs=new ChFrame("").ad(addActLi(this,new ChFileChooser(ChFileChooser.CLOSE,"ResultCompareProteinPairs")));
            _frameFs.shw(ChFrame.CENTER);
        }
        if (cmd==JFileChooser.APPROVE_SELECTION) _tfFile.setText(toStrg(((ChFileChooser)q).getSelectedFile()));
        if (cmd=="GO" || cmd==ACTION_ENTER) {
            final BA txt=readBytes(file(_tfFile));
            if (txt==null) { StrapAlign.errorMsg("Could not open input file "+_tfFile); return;}
            final JComponent c=(JComponent)q, p=parentC(false,c,JComponent.class);
            p.remove(c);
            if (LOG!=null) p.add(ChButton.doView(LOG).t("Log"));
            if (isAssignblFrm(HasControlPanel.class,_clazz)) p.add(new ChButton("CTRL").t("Details").li(this));
            for(Class hasView : new Class[]{Superimpose3D. class,SequenceAligner.class}) {
                if (isAssignblFrm(hasView, _clazz) || isAssignblFrm(hasView, _otherClass)  ) {
                    p.add(new ChButton("VIEW").t("View result").li(this));
                    break;
                }
            }
            p.add(butStop);
            revalAndRepaintC(p);
            startThrd(thrdCR(this,"GO",txt));
            _tfFile.setEditable(false);
        }
        if (cmd=="stop") dispose();
        if (cmd=="VIEW" || cmd=="CTRL") {
            final int i=_jTable.getSelectedRow();
            if (i<0) { StrapAlign.errorMsg("Select a table row first"); return;  }
            final Vector<String> v=vResult.get(i);
            final Protein p0=protein(v.get(0)),p1=protein(v.get(1));
            if (cmd=="VIEW") SPUtils.viewComparison(p0,p1, _otherClass);
            if (cmd=="CTRL") {
                final Object dis=mkInstance(_clazz,Object.class,true);
                setSharedParas(_sharedInst, dis);
                SPUtils.computeValue(0, dis, p0,p1, _fromCol,_toCol);
                shwCtrlPnl(CLOSE_CtrlW_ESC|ChFrame.AT_CLICK|ChFrame.SCROLLPANE, toStrg(v), dis);
            }
        }
    }

}
