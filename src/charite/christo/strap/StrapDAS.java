package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import java.util.*;
import javax.swing.*;
import static java.awt.event.MouseEvent.MOUSE_PRESSED;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.DAS.Entry;
/**HELP
   BioDAS* services provide residue specific annotations for proteins.
   BioDAS* services are listed in <i>STRING:DAS#URL_REGISTRY</i> (http://www.dasregistry.org/).
   Strap reads this BioDAS* registry file and sorts the services according to the authority.
   The most important authority is "UniProt" which is displayed in the first tab.
   The user can select one or several DAS services in the table and one or several proteins.
   When <i>LABEL:ChButton#BUTTON_GO</i> is pressed, the DAS features are read from the selected services.
   For this purpose, the database references stored in the protein files are used.
   The database references of proteins can be seen in the balloon text over the protein labels of the alignment.
   If a services from the "UniProt" tab is used, then only those
   proteins can be processed, that have a "UniProt" reference.
   <br><br><b>Cache:</b> The downloaded files are cached in
   <i>STRING:ChUtils#dirWeb()</i> and are valid for <i>STRING:DAS#DAYS_VALID</i> days.

   @author Christoph Gille
   ARMA AgamP BDGP BGI BROADD BROADE BROADO BROADS BUSHBABY Btau_ CAT CEL CHIMP COMMON_SHREW CSAV EMBL EMDB Ensembl Entrez EquCab FUGU GENCODE GUINEAPIG HEDGEHOG HIV HUGO_ID Havana IPI JGI KEGG MEDAKA MGI MICROBAT MMUL_ NCBI NCBI m NCBIM OANA PDBresnum PPYG Pfam RABBIT RGSC SGD SQUIRREL TAIR TENREC TETRAODON TIGR TREESHREW UniParc UniProt WASHUC WB WS ZFISH Zv cavPor dipOrd gorGor micMur pika proCap pteVam tarSyr turTru vicPac
*/
public class StrapDAS extends ChRenderer implements ChRunnable, ProcessEv  {

    private static EvAdapter _li;
    private static StrapDAS _inst;
    private static StrapDAS instance() { if (_inst==null) _li=new EvAdapter(_inst=new StrapDAS()); return _inst;}
    private StrapDAS(){}
    private DialogStringMatch _diaSearch;
    private static ChButton _butGO, _butRenderer;
    private static JComponent _pButtons,   _labMsg;
    private static JTabbedPane _tabbed;
    private static ChTextArea _taSouth;
    private static boolean _downloadExamples;
    private static BA _sbExamples;
    private ChPanel _progress;

    private JComponent labMsg() { return _labMsg==null? _labMsg=pnl(" ") : _labMsg;}
    public static ChButton docButExamble(){
        getFeatureTableData();
        return ChButton.doView(_sbExamples).t("Example outputs");
    }

/* "No","Authority","Title","Coordinate source","Example entry",  "Log", "List Created" */
    private final static int
        COLUMN_NO=0,
        COLUMN_AUTHORITY=1,
        COLUMN_TITLE=2,
        COLUMN_SOURCE=3,
        COLUMN_AVAILABLE=4,
        COLUMN_LOG=5,
        COLUMN_LIST=6,
        FIELD_ENTRY=10,
        FIELD_coordinates=11,
        FIELD_V=12,
        FIELD_JLIST_AA=13,
        FIELD_LOG=14,
        FIELD_EXAMPLE_URL=15;
    /* ---------------------------------------- */
    /* >>> readRegistry >>> */
    public static ChRunnable logR() { return instance();}
    private static Object[][][] featureTableData;
    private static Object[][][] getFeatureTableData() {
        final Entry ee[]=DAS.readRegistry(logR());
        synchronized("SDAS$$syncFTD") {
            Object rr[][][]=StrapDAS.featureTableData;
            if (rr==null) {
                _sbExamples=new BA(9999);
                final String AA[]=DAS.AUTHORITIES;
                rr=StrapDAS.featureTableData=new Object[AA.length][][];
                final List<Object[]> v=new ArrayList();
                final BA baTmp=new BA(99);
                for(int iA=AA.length;--iA>=0; ) {
                    v.clear();
                    for(int iE=0, no=0; iE<ee.length; iE++) {
                        final Entry e=ee[iE];
                        final String uri=e.queryUri(DAS.CAPABILITY_TYPE_feature);
                        if (uri==null) continue;
                        for(DAS.Coordinates coordinates : e.coordinates()) {
                            final String a=coordinates.authority();
                            if (iA==AA.length-1 ? idxOfStrg(a,AA)>=0 : AA[iA]!=a) continue;
                            /* "No","Authority","Title","Coordinate source","Example entry",  "Log", "List Created" */
                            final Object[] row=new Object[16];
                            row[COLUMN_NO]=intObjct(++no);
                            row[COLUMN_AUTHORITY]=a;
                            row[COLUMN_TITLE]=e.title();
                            row[COLUMN_SOURCE]=coordinates.source();
                            row[FIELD_coordinates]=coordinates;
                            final String urlEx=e.urlTestResult(coordinates);
                            if (urlEx!=null) {
                                row[FIELD_EXAMPLE_URL]=urlEx;
                                baTmp.clr().filter(FILTER_NO_MATCH_TO|'_', FILENM, e.title()+"_"+coordinates.testRange()+".xml");
                                _sbExamples.a("wget -N -T 10 -O ").a(baTmp).a(' ').aln(urlEx);
                            }
                            row[FIELD_ENTRY]=e;
                            v.add(row);
                        }
                    }
                    rr[iA]=toArry(v,Object[].class);
                }
            }
            return rmNullA(rr, Object[][].class);
        }
    }

    /* <<< Get <<< */
    /* ---------------------------------------- */
    /* >>> For a given Protein  >>> */
    public static Runnable thread_load(int opt, String title[], Protein pp[], ChRunnable log) {
        return thrdM("load",StrapDAS.class, new Object[]{intObjct(opt), title, pp, log});
    }
    public static void load(int opt, String titles[], Protein pp[], ChRunnable log) {
        final Object registry[][][]=getFeatureTableData();
        if (registry==null) { SequenceFeatures.log().a(RED_ERROR).aln("StrapDAS: registry==null"); return;}

        final List<Object[]> v=new ArrayList();
        for(String t : titles) {
            if (sze(t)==0) continue;
            for(Object[][] table : registry) {
                for(Object[] row : table) if (eqNz(rplcToStrg("%20"," ",t),row[COLUMN_TITLE])) v.add(row);
            }
            if (sze(v)==0) SequenceFeatures.log().a(RED_ERROR).a(" No such DAS feature ").aln(t);
        }
        loadFeaturesInRows(opt, toArry(v,Object[].class), pp, log);
    }
    private final static Object KEY_DAS_F=new Object();
    /* <<< For Protein <<< */
    /* ---------------------------------------- */
    /* >>> For Proteins >>> */
    public static void loadFeaturesInRows(int opt, Object[] rows[], Protein pp[], ChRunnable log) {
        if (sze(rows)==0) return;
        if (0!=(opt&SequenceFeatures.OPT_BG)) {
            startThrd(thrdM("loadFeaturesInRows",StrapDAS.class, new Object[]{intObjct(opt&~SequenceFeatures.OPT_BG), rows,pp, log}));
            return;
        }
        for(Object[] row: rows) ((Entry)row[FIELD_ENTRY]).isAvailable(log);
        final int optFindID=opt&FindUniprot.MASK;

        final BA sb=new BA(99);
        for(int iRow=0; iRow<rows.length; iRow++) {
            final Object row[]=rows[iRow];
            final Entry e=(Entry)row[FIELD_ENTRY];
            if (e==null) continue;
            final int sb0=SequenceFeatures.progressText(opt,sb).a(row[COLUMN_AUTHORITY]).a(' ').a(row[COLUMN_TITLE]).end();
            for(int iP=0; iP<pp.length; iP++) {
                final Protein p=get(iP,pp, Protein.class);
                if (p==null) continue;
                if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(StrapDAS.class, (iRow*pp.length+iP+1)*100/(rows.length*pp.length+1), sb.setEnd(sb0).a(' ').a(p).a(" ...")));
                _loadFeatures(opt, row, p, log);
                SequenceFeatures.updateTable(333);
            }
            if (log!=null) log.run(ChRunnable.RUN_SET_PROGRESS, prgrss(StrapDAS.class,0, sb.setEnd(sb0).a(' ').a(plrl(pp.length," done for %N protein%S and ")).a(plrl(rows.length," %N type%S"))));

        }
        setEnabld(true, _butGO);
    }

    public static void _loadFeatures(int opt, Object[] row, Protein p, ChRunnable globalLog) {
        if (p==null) return;

        final int optFindID=opt&FindUniprot.MASK;
        final boolean fast=0==(optFindID&FindUniprot.BLASTER_MASK);
        final JTable jTable=focusedTable();
        final Entry entry=(Entry)row[FIELD_ENTRY];
        for(int j=100; --j>=0;) {
            if (entry.isAvailable(globalLog)!=0) break;
            sleep(99);
        }
        BA log=(BA)row[FIELD_LOG];
        String pfx=null;
        if (!withGui()) {
            log=new BA(99).setSendTo(BA.SEND_TO_STDOUT);
            pfx="Sequence features: ";
        } else if (log==null) row[FIELD_LOG]=log=new BA(99);
        nextProtein:
        synchronized(mkIdObjct(fast?"SDAS$$syncF":"SDAS$$sync",p)) {
            log.a(pfx).aln(p).send();
            final ResidueAnnotation aa[]=p.residueAnnotations();
            for(ResidueAnnotation a : aa) if (gcp(KEY_DAS_F, a)!=null) return;
            final String authority=(String)row[COLUMN_AUTHORITY];
            int count=0;
            final Collection<String> vId=new ArrayList();
            if (0!=(opt&SequenceFeatures.OPT_ID_BY_HOMOLOGY)) adAllUniq(FindUniprot.load1(optFindID, p, logR()), vId);
            adUniq(p.getAccessionID(),vId);
            adAllUniq(p.getSequenceRefs(),vId);
            adAllUniq(p.getDatabaseRefs(),vId);
            for(int download=0;download<2;download++) {
                for(String ref : strgArry(vId)) {
                    if (_frame!=null && (opt&SequenceFeatures.OPT_STOP_ON_CLOSED_FRAME)>0 && !_frame.isVisible()) break nextProtein;
                    if (ref==null) continue;
                    final boolean isResnum=eqNz("PDBresnum",authority);
                    final String aut=isResnum ? "PDB" : authority;
                    final boolean equls=strEquls(STRSTR_IC, aut, ref) && chrAt(aut.length(),ref)==':';
                    if (equls) {
                        final String urlSeq=Hyperrefs.toUrlString(ref, Hyperrefs.PROTEIN_FILE);
                        log.a(pfx).a(p).a(" ref=").a(ref).a(" ref-url=").aln(urlSeq).send();
                        final File
                            ffEquivalent[]={FetchSeqs.dbColonID2file(ref)},
                            fSeq=isResnum ? null : urlGet(download!=0 ? url(urlSeq) : null, 999, ffEquivalent, globalLog);
                            if (download==0 && sze(fSeq)==0) continue;
                            log.a(pfx).a(p).a(" ref-seq=").a(fSeq).a(' ').a(sze(fSeq)).aln(" Bytes").send();
                            if (!isResnum && sze(fSeq)<20) continue;
                            final char chain=ref.startsWith("UNIPROT:") ? 0 : chrAt(0,p.getChainsAsString());
                            final BA ba=ChUtils.readBytes(DAS.downloadURL(ref,chain,  entry, DAS.CAPABILITY_TYPE_feature, globalLog));
                            final java.net.URL url=DAS.getURL(ref,chain,  entry, DAS.CAPABILITY_TYPE_feature);
                            log.a(pfx).a(p).a(" url=").aln(url).send();
                            if (ba==null) continue;
                            final ResidueAnnotation[] aaNew=ExpasyGff.parseFeatures(0, ba,  url, p, authority, (Collection)null);
                            for(ResidueAnnotation a : aaNew) {
                                row[FIELD_V]=adNotNullNew( wref(a), (Collection)row[FIELD_V]);
                                if (a.isEnabled()) count++;
                                a.addE(0,"RefSequence", ref+" "+fPathUnix(fSeq));
                                a.addE(0,ResidueAnnotation.HYPERREFS,toStrg(url));
                            }
                            revalAndRepaintC(jTable);
                            log.a(pfx).a(p).aln(GREEN_DONE+" ").send();
                    }
                    if (0!=(SequenceFeatures.OPT_ONLY_FIRST_UNIPROT&opt) && count>0) break;
                }
            }
            if (count>0) {
                Protein.incrementMC(ProteinMC.MCA_SEQUENCE_FEATURES_V,null);
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED, 999);
            }
            log.aln("/////\n");
        }
    }

    /* <<<  For a given Protein <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    private static ChFrame _frame;
    private static boolean _initialized;
    private JComponent _panel;
    public static void show(boolean downloadExamples) {
        if (!_initialized) {
            _initialized=true;
            startThrd(thrdCR(instance(),"REGISTRY",  thrdCR(instance(),"AFTER_REGISTRY")));
        }
        _downloadExamples=downloadExamples;
        if (_frame==null) _frame=new ChFrame("DAS sources").size(666,444).ad(instance().getPanel());
        _frame.shw();
    }
    private JComponent getPanel() {
        if (_panel==null) {
            if (!withGui()) assrt();
            if (_progress==null) _progress=prgrssBar();
            _tabbed=new JTabbedPane();
            _tabbed.addChangeListener(_li);
            final JComponent pNorth=pnl(CNSEW,"<h2>BioDAS Entries</h2>",null,null, panHS(StrapDAS.class));
            _taSouth=new ChTextArea(DAS.URL_REGISTRY);
            pcp(ACTION_DATABASE_CLICKED,"Load",_taSouth);
            _taSouth.setLineWrap(true);
            _butGO=new ChButton().li(_li).t(ChButton.GO);
            _pButtons=pnl(CNSEW,pnl("Load DAS features for selected proteins",_butGO, "  ", SequenceFeatures.butFindUniprot()), null, labMsg());
            addMoli(MOLI_REPAINT_ON_ENTER_AND_EXIT,_pButtons);
            final Object pSouth=pnl(CNSEW,_taSouth, _progress, _pButtons);
            _panel=pnl(CNSEW,_tabbed,pNorth, pSouth);
            addMoli(MOLI_REPAINT_ON_ENTER_AND_EXIT,labMsg());
        }
        return _panel;
    }
    /* <<< Layout <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    @Override public java.awt.Component getTableCellRendererComponent(JTable t, final Object value0,boolean selected,boolean focus,int row0,int col){
        if (_butRenderer==null) _butRenderer=new ChButton(ChButton.PAINT_IN_TABLE,"");
        Object value=value0;
        final int row=ChJTable.rowIndexToModel(row0,t);
        if (col==COLUMN_AVAILABLE) {
            final Object[] rowData=get(row, gcp("D",t,Object[][].class) , Object[].class);
            final Object val=rowData[COLUMN_AVAILABLE];
            final Entry e=(Entry)rowData[FIELD_ENTRY];
            if (val==null) {
                final BA ba=e.testResult(_downloadExamples, this);
                if (ba!=null) {
                    final byte[] T=ba.bytes();
                    final int E=ba.end();
                    if (strstr("</FEATURE>", T, 0, E)>0) {
                        value="No sequence range in feature";
                        for(int iStart=0; (iStart=strstr(STRSTR_AFTER,"<START>",  T, iStart,E))>0;) {
                            if (atoi(T,iStart, E)>0) value="OK";
                        }
                    } else value="Error";
                }
                rowData[COLUMN_AVAILABLE]=value;
            }
        }
        if (col==COLUMN_AVAILABLE && value!=null) return _butRenderer.t((String)value);
        if (col==COLUMN_LIST || col==COLUMN_LOG) {
            final int iTab=_tabbed.getSelectedIndex();
            if (iTab>=0) {
                final Object rowData[]=get(row, featureTableData[iTab], Object[].class);
                final List v=(List) get(FIELD_V,rowData);
                if (col==COLUMN_LOG && v!=null ||  col==COLUMN_LIST && sze(v)>0) {
                    final String label= col==COLUMN_LOG ? "Log" : col==COLUMN_LIST ? "# "+sze(v) : "";
                    return _butRenderer.t(label);
                }
            }
        }
        return super.getTableCellRendererComponent(t,value,selected,focus,row, col);
    }
    /* <<< Renderer  <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id==DAS.RUN_EXAMPLE_LOADED) ChDelay.repaint(_tabbed, 333);
        if (id==ChRunnable.RUN_APPEND) drawMsg(toStrg(arg), labMsg());
        if (id==ChRunnable.RUN_SET_PROGRESS) {
            SequenceFeatures.instance().run(id,arg);
            prgrss2bar(_progress, argv);
        }
        if (id==ChRunnable.RUN_SAY_DOWNLOADING) drawMsg(toStrg(arg), _pButtons);
        if (id=="REGISTRY") {
            getFeatureTableData();
            inEDT((Runnable)arg);
        }
        if (_tabbed!=null) {
            _diaSearch=new DialogStringMatch(0L, "StrapDAS");
            if (id==ChRunnable.RUN_DOWNLOAD_FINISHED) _tabbed.repaint();
            final Object[][][] reg=StrapDAS.featureTableData;
            final String[] prefFeatures=custSettings(Customize.preferedDasSources);
            if (id=="AFTER_REGISTRY" && reg!=null) {
                final ChButton butSearch=ChButton.doView(_diaSearch).i(IC_SEARCH).t("Search");
                _tabbed.removeAll();
                for(int iCoord=0; iCoord<DAS.AUTHORITIES.length; iCoord++) {
                    final ChTableModel m=new ChTableModel(0L, "No","Authority","Title","Coordinate source","Example entry",  "Log", "List Created");
                    m.setData(reg[iCoord]);
                    final ChJTable t=new ChJTable(m,0).renderer(this);
                    ChJTable.setRowSorter(new Comparator[]{t},t);
                    t.setColWidth(false, 0,ChJTable.COLUMN_WIDTH_4_DIGITS);
                    t.setRowHeight(EX+8);
                    pcp("D",reg[iCoord],t);
                    _li.addTo("mf",t);

                    adTab(0, DAS.AUTHORITIES[iCoord],  scrllpnT(t).addMenuItem(butSearch), _tabbed);
                    if (iCoord==0 && sze(reg[iCoord])>1) t.setRowSelectionInterval(0, prefFeatures.length-1);
                }
                _tabbed.setSelectedIndex(0);
                _li.addTo("C",_tabbed);
            }
        }
        return null;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private static ChJTable focusedTable() {
        final JTabbedPane tp=_inst!=null ? _inst._tabbed : null;
        return tp!=null ? child(_tabbed.getSelectedComponent(), ChJTable.class) : null;
    }
    public void processEv(java.awt.AWTEvent ev) {
        final int id=ev.getID();
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final ChJTable t=focusedTable();
        if (t==null) return;
        final int rr[]=ChJTable.selectedRowsConverted(t);
        final Object[][] rows=gcp("D",t,Object[][].class);
        if (cmd==ACTION_CHANGE_EVENT && _diaSearch!=null)   _diaSearch.setListComponent(new Object[]{focusedTable()});
        if (q==t && (id==MOUSE_PRESSED || cmd==ACTION_CHANGE_EVENT)) _butGO.enabled(sze(rr)>0);
        if (id==MOUSE_PRESSED) {
            final int row=ChJTable.rowIndexToModel(t.rowAtPoint(point(ev)),t);
            final int col=t.columnAtPoint(point(ev));
            final Object[] rowData=get(row, rows, Object[].class);
            if (rowData!=null) {
                final Entry entry=(Entry)rowData[FIELD_ENTRY];
                final DAS.Coordinates coordinates=(DAS.Coordinates)rowData[FIELD_coordinates];
                final List<ResidueAnnotation> vAA= get(FIELD_V, rowData,List.class);
                final String title= (String)rowData[COLUMN_TITLE];
                final BA logBa=(BA)rowData[FIELD_LOG];
                final ChTextView log=logBa==null?null:logBa.textView(true);
                if (col==COLUMN_LOG && log!=null) {
                    pcp(ACTION_DATABASE_CLICKED,"Load",log);
                    ChFrame.frame(title, log, ChFrame.SCROLLPANE|CLOSE_CtrlW_ESC).shw();
                    return;
                } else if (col==COLUMN_LIST && sze(vAA)>0) {
                    final Object pSouth=pnl("Note: the list supports ","WIKI:Drag_and_drop");
                    if (rowData[FIELD_JLIST_AA]==null) rowData[FIELD_JLIST_AA]=new Object();
                    StrapAlign.showInJList(0L, rowData[FIELD_JLIST_AA], vAA.toArray(),title, pSouth).showInFrame(ChFrame.AT_CLICK|CLOSE_CtrlW_ESC, title);
                    return;
                } else if (col==COLUMN_AVAILABLE) {
                    final BA ba=entry==null?null : entry.testResult(false, this);
                    if (ba!=null) new ChTextView(ChCodeViewer.fontifyXmlDAS(ba)).tools().showInFrame((String)rowData[COLUMN_TITLE]);
                }
                if (sze(rr)==1) {
                    final BA sb=new BA(333);
                    final Object testRange=coordinates.testRange();
                    if (rowData[COLUMN_AVAILABLE]==null) {
                        final Object url=rowData[FIELD_EXAMPLE_URL];
                        sb.a("Testing: ")
                            .aRplc(FILTER_TO_UPPER, "PDBresnum","PDB",(String) rowData[COLUMN_AUTHORITY])
                            .a(':').a(testRange).a(' ',2).a(url).a(' ',2).a(fPathUnix(url(url)));
                    } else sb.a(testRange);
                    _taSouth.t(sb.a('\n').a(entry.docHref()).a('\n',2).a(entry.description()));
                } else _taSouth.t(DAS.URL_REGISTRY);
                _taSouth.tools().underlineRefs(0);
            }
        }
        if (q==_butGO && sze(rr)>0) {
            Protein pp[]=StrapAlign.selectedProteins();
            if (pp.length==0) pp=StrapAlign.visibleProteins();
            SequenceFeatures.instance().showPanel();
            final List vRows=new ArrayList();
            for(int i=sze(rr); --i>=0;) adUniq(get(rr[i],rows), vRows);
            setEnabld(false, _butGO);
            loadFeaturesInRows(SequenceFeatures.OPT_BG|SequenceFeatures.OPT_STOP_ON_CLOSED_FRAME, toArry(vRows,Object[].class), pp, logR() );
        }
    }

    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */

}
