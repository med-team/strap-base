package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.AbstractButton;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.VisibleIn123.*;
import static charite.christo.protein.ProteinMC.*;
import static charite.christo.strap.SPUtils.sp;
/**
   @author Christoph Gille
 */
public class ResSelUtils implements Comparator, ChRunnable {
    public final static String DND_SELECTED_UC="SELECTED_UPPER_CASE",
        DND_SUFFIX=".residueSelection",
        INFO_TITLE[]=(
                      "Name;Feature name;Protein name;"+
                      "First selected amino acid;Last selected amino acid;Count selected amino acids;"+
                      "First selected nucleotide;Last selected nucleotide;Count selected nucleotides;"+
                      "Annotation").split(";");

    private final static int ARROW_LEN=4;
    private static Polygon _arrowL, _arrowR;
    private static ResSelUtils _inst[]=new ResSelUtils['P'+1];
    private ResSelUtils() {}
    private int _type;
    final static int  HIGHLIGHT_P=1<<1, HIGHLIGHT_A=1<<2;
    static ResSelUtils instance(int type) {
        if (_inst[type]==null) (_inst[type]=new ResSelUtils())._type=type;
        return _inst[type];
    }

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Compare >>> */
    /* Cursor to top */
    public static void sortResidueSelectionsByVisibility(ResidueSelection[] ss) {
        sortArry(ss, instance(0));
    }

    /* -1 if o1 should be drawn on top of o2 */
    public int compare(Object o1, Object o2)  {
        final ResidueSelection s1=o1 instanceof ResidueSelection ? (ResidueSelection)o1 : null;
        final ResidueSelection s2=o2 instanceof ResidueSelection ? (ResidueSelection)o2 : null;
        if (s1!=null && s2!=null) {
            final String n1=s1 instanceof HasName ? ((HasName)s1).getName() : null;
            final String n2=s2 instanceof HasName ? ((HasName)s2).getName() : null;
            if (n1==ResidueSelection.NAME_CURSOR) return -1;
            if (n2==ResidueSelection.NAME_CURSOR) return  1;
            if (n1==ResidueSelection.NAME_STANDARD) return -1;
            if (n2==ResidueSelection.NAME_STANDARD) return  1;

            final int style1=style(s1);
            final int style2=style(s2);
            if (style1<style2) return 1;
            if (style1>style2) return -1;
            final int count1=countTrue(s1.getSelectedAminoacids());
            final int count2=countTrue(s2.getSelectedAminoacids());
            if (count1<count2) return -1;
            if (count1>count2) return 1;
        }
        return s1==null?1:s2==null?-1:0;
    }

    public static boolean equalAminoPositions(ResidueSelection s1, ResidueSelection s2) {
        return
            s1==null || s2==null || s1.getProtein()!=s2.getProtein() ? false :
            boolOffstEquls(s1.getSelectedAminoacids(), s1.getSelectedAminoacidsOffset(), s2.getSelectedAminoacids(), s2.getSelectedAminoacidsOffset());
    }
    /* <<< Compare <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */

    static void killResidueAnnotation(ResidueAnnotation...ss) {
        if (ss.length==0) return;
        final BA msg=new BA(333).aln(plrl(ss.length, "Really kill %N residue annotation%S:"));
        if (ss.length<10)
            for(int i=0;i<ss.length;i++) msg.a(ss[i].getName()).a(' ',2).aln(sp(ss[i]));
        if (ChMsg.yesNo(monospc(msg))) {
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_DELETED, 111);
            for(int i=0;i<ss.length;i++) ss[i].dispose();
        }
    }

    public static ResidueAnnotation[] toResidueAnnotation(boolean edit, ResidueSelection... ss) {
        if (countNotNull(ss)==0) return ResidueAnnotation.NONE;
        final Collection<ResidueSelection> vOrig=new ArrayList(),vA=new ArrayList();
        for(ResidueSelection s:ss) {
            final Protein p=sp(s);
            if (p==null || s instanceof ResidueAnnotation) continue;
            final String n=nam(s);
            final ResidueAnnotation a=new ResidueAnnotation(p);
            a.setValue(0,ResidueAnnotation.GROUP, n==ResidueSelection.NAME_CURSOR ? ResidueSelection.NAME_CURSOR : "copied");
            a.setValue(0,ResidueAnnotation.POS, Protein.selectedPositionsToText(s));
            a.setValue(0,ResidueAnnotation.NAME, "copied_from_"+ s);
            p.addResidueSelection(a);
            if (colr(s)!=null) a.setColor(colr(s));
            if (edit) StrapAlign.editAnnotation(a);
            if (n==ResidueSelection.NAME_STANDARD || n==ResidueSelection.NAME_BACKBONE) p.removeResidueSelection(s);
            else vOrig.add(s);
            vA.add(a);
        }
        final int n=sze(vOrig);
        if (n>0 && ChMsg.yesNo(plrl(n,"%N persistant annotated residue selection object%S %H been created.<br>Remove the original amino acid selection%S?"))) {
            for(ResidueSelection s  : toArry(vOrig,ResidueSelection.NONE)) sp(s).removeResidueSelection(s);
        }
        if (sze(vA)>0) {
        StrapAlign.setNotSaved();
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED,111);
        }
        return toArry(vA,ResidueAnnotation.NONE);
    }

    public static SelectorOfNucleotides[] selOfNuclAt(Protein p,int iN) {
        if (iN<0) return new SelectorOfNucleotides[0];
        Collection v=null;
        for(Object o:p.allResidueSelections()) {
            if (!SelectorOfNucleotides.class.isAssignableFrom(o.getClass())) continue;
            final SelectorOfNucleotides selNt=(SelectorOfNucleotides)o;
            final boolean bb[]=selNt.getSelectedNucleotides();
            final int offset=selNt.getSelectedNucleotidesOffset();
            if (bb!=null && offset<=iN && bb.length>iN-offset && bb[iN-offset]) v=adNotNullNew(o,v);
        }
        return  toArry(v, SelectorOfNucleotides.class);
    }

    static String optimizedPositions(ResidueAnnotation s, boolean pdbNumbering) {
        final Protein p=sp(s);
        if (p==null) return null;
        final boolean
            isAmino=s.isAmino(),
            bb[]= isAmino ? s.getSelectedAminoacids() :  s.getSelectedNucleotides();
        final int offset=isAmino ? s.getSelectedAminoacidsOffset() : s.getSelectedNucleotidesOffset();
        return Protein.selectedPositionsToText(pdbNumbering, bb, offset, p);
  }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  asString >>> */
    public static BA selectedAsString(ResidueSelection sel, BA sb) {
        final Protein p=sp(sel);
        final byte[] aa=p!=null ? p.getResidueType() : null;
        if (aa==null) return sb;
        boolean isFirst=true;
        int lastIndex=0;
        final boolean bb[]=sel.getSelectedAminoacids();
        final int selOffset=selAminoOffsetZ(sel), to=mini(sze(bb), p.countResidues()-selOffset);
        for(int i=maxi(0,-selOffset); i<to; i++) {
            if (bb[i]) {
                if (!isFirst && i-lastIndex>1) sb.a(',');
                isFirst=false;
                lastIndex=i;
                sb.a((char)aa[i+selOffset]);
            }
        }
        return sb;
    }
    /* <<< asString <<< */
    /* ---------------------------------------- */
    /* >>> Draw >>> */
    private static int _maxUL=7;
    static void setMaxUnderlining(int l) { _maxUL=l;}
    static int maxUnderlining() { return _maxUL;}

    static int yT(int style, int charH) {
        if ((style & 255)==STYLE_UNDERLINE) {
            final int line=(style & 0xFF0000)>>BIT_SHIFT_LINE;
            return charH+3*line;
        }
        return style==STYLE_LOWER_HALF_BACKGROUND ?  charH/2-1 : 0;
    }

    static int height(int style, int charH) {
        return
            (style & 255)==STYLE_UNDERLINE ? 2 :
            style==STYLE_LOWER_HALF_BACKGROUND || style==STYLE_UPPER_HALF_BACKGROUND ? charH/2-2 :
            charH-4;
    }

    public static boolean[] selectedAminosDisplayed(ResidueSelection s) {
        if (s instanceof ResidueAnnotation && s!=_noHide) {
            final ResidueAnnotation a=(ResidueAnnotation)s;
            final boolean sa[]=a.getSelectedAminoacids(), saViewed[]=(boolean[])a.cached()[ResidueAnnotation.IS_SEL_V];
            return saViewed!=null ? saViewed : sa;
        }
        return s!=null ? s.getSelectedAminoacids() : NO_BOOLEAN;
    }

    private final static Stroke STROKE_DOTTED=new BasicStroke(1f,BasicStroke.CAP_BUTT,BasicStroke.CAP_BUTT,1f,new float[]{1f,1f},0);
    static void drawResidueSelection(boolean amino, byte chr,Protein p, ResidueSelection[] ss, int iAa0, Graphics2D g, Rectangle r, Rectangle cb) {
        g.translate(x(r),y(r));
        final String strgLen1[]=strgsOfLen1();
        for(int i=sze(ss); --i>=0;) { // backward 'cos cursor
            final ResidueSelection s=ss[i];
            if (!isSelVisible(VisibleIn123.SEQUENCE, s)) continue;
            final int iAa, booleanOffset;
            final boolean bb[], notShowingAll;
            if (amino==Protein.AMINO_ACIDS) {
                final boolean[] selected=s.getSelectedAminoacids();
                bb=selectedAminosDisplayed(s);
                notShowingAll=selected!=bb;
                iAa=iAa0;
                booleanOffset=selAminoOffsetZ(s);
            } else {
                notShowingAll=false;
                iAa=iAa0;
                if (s instanceof SelectorOfNucleotides) {
                    final SelectorOfNucleotides selNt=(SelectorOfNucleotides)s;
                    bb=selNt.getSelectedNucleotides();
                    booleanOffset=selNt.getSelectedNucleotidesOffset();
                } else { booleanOffset=0; bb=null;}
            }
            final int iBB=iAa-booleanOffset;
            if (bb==null || iBB<0 || bb.length<=iBB || !bb[iBB]) continue;
            final int
                style0=s instanceof VisibleIn123 ? ((VisibleIn123)s).getStyle() : VisibleIn123.STYLE_BACKGROUND,
                style=style0==STYLE_CURSOR ? STYLE_BACKGROUND : style0,
                charH=cb.height,
                yT= yT(style,charH),
                height=height(style,charH);

            final boolean terminus= bb.length==iBB+1 ||  !bb[iBB+1];
            final int rW= terminus ? maxi(3,r.width-3) : r.width;
            final Color color=style==STYLE_IMAGE_LUCID  ? null : colr(s);
            final Image image=(style==STYLE_IMAGE || style==STYLE_IMAGE_LUCID || (255&style)==STYLE_UNDERLINE) && isInstncOf(HasImage.class,s) ? ((HasImage)s).getImage() : null;
            if (color!=null) {
                switch(style&255) {
                case STYLE_HIDDEN:
                    break;
                case STYLE_CIRCLE:
                    g.setColor(color);
                    for(int d=0,w=rW,h=charH;d<4;d++) {
                        g.drawOval( (rW-w)/2, (charH-h)/2,w,h);
                        w-=3;h-=4;
                    }
                    break;
                case STYLE_DOTTED:
                    final Stroke stroke0=g.getStroke();
                    g.setStroke(STROKE_DOTTED);
                    g.setColor(color);
                    for(int y=height; --y>=0;) g.drawLine(y%2,y,rW-1,y);
                    g.setStroke(stroke0);
                    break;
                case STYLE_UNDERLINE:
                    g.setColor(color);
                    if (notShowingAll && (iBB==1 || iBB>1 && iBB+2==bb.length)) {
                        if (_arrowL==null) {
                            final int D=4;
                            final Polygon ar=_arrowR=new Polygon(), al=_arrowL=new Polygon();
                            ar.addPoint(ARROW_LEN,0);
                            ar.addPoint(0,D);
                            ar.addPoint(0,-D);
                            ar.addPoint(ARROW_LEN,0);
                            al.addPoint(0,0);
                            al.addPoint(ARROW_LEN,D);
                            al.addPoint(ARROW_LEN,-D);
                            al.addPoint(0,0);
                        }
                        g.setColor(C(color.getRGB(),100));
                        final int transX=iBB==1 ? 0 : rW-ARROW_LEN;
                        g.translate(transX,yT);
                        g.fillPolygon(iBB==1?_arrowR:_arrowL);
                        g.translate(-transX,-yT);
                    } else fillBigRect(g, 0,  yT, rW,2);
                    break;
                case STYLE_IMAGE:
                case STYLE_LOWER_HALF_BACKGROUND:
                case STYLE_UPPER_HALF_BACKGROUND:
                case STYLE_BACKGROUND:
                    g.setColor(color);
                    fillBigRect(g, 0,yT, rW,height);
                    break;
                default:
                }
            }
            {
                final Component obs=ChIcon.imageObserver();
                final int h=image==null&&obs!=null?0:image.getHeight(obs);
                if (h>0) {
                    final int w=image.getWidth(obs);
                    final int chars=w*3/h/2;
                    if (chars>0) {
                        final int w2=w/chars;
                        if (chars<2) g.drawImage(image,0,  yT,rW, height, obs);
                        else {
                            final int x1=w2*(iAa % chars), x2=x1+w2;
                            g.drawImage(image , 0,  yT,rW, height,   x1 ,0,x2,h, obs);
                        }
                    }
                }
            }
        }
        if (chr!=0 && chr!=32){
            g.setColor(C(0));
            final String draw=strgLen1[chr&127];
            final int a=y(cb),x=x(cb);
            g.drawString(draw, x+0,a  );
            g.drawString(draw, x+1,a+1);
            g.drawString(draw, x-1,a-1);
            g.drawString(draw, x-1,a+1);
            g.drawString(draw, x+1,a-1);
        }
        g.translate(-x(r),-y(r));
    }

    private static Object _hide[], _noHide;
    public static void setHide(Object hide[]) { _hide=hide; }
    public static void noHide(Object noHide) { _noHide=noHide; }
    public static boolean isSelVisible(int where0, Object o) {
        final int where=where0&~VisibleIn123.NO_FLASH;
        return
            o instanceof ResidueSelection &&
            (where!=where0 || o==_noHide || !cntains(o,_hide)) &&
            !(o instanceof ResidueAnnotation &&  !((ResidueAnnotation)o).isEnabled()) &&
            !(where!=0 && o instanceof VisibleIn123 && 0==(((VisibleIn123)o).getVisibleWhere()&where));
    }

    public static int style(Object o) { return o instanceof VisibleIn123 ? ((VisibleIn123)o).getStyle() : -1; }
    public static boolean styleUL(Object o) { return o instanceof ResidueSelection && (style(o) &0xf)==VisibleIn123.STYLE_UNDERLINE; }
    /* <<< Draw <<< */
    /* ---------------------------------------- */
    /* >>> Layout Underlining >>> */
    //    private static int _maxPos;
    public static int layoutFeatures(Protein pp[]) {
        assrtEDT();
        // if (!isEDT()) {
        //     inEdtLater(thrdM("layoutFeatures",ResSelUtils.class, new Object[]{pp}));
        //     debugExit("EDT");
        //     return _maxPos;
        // }
        int maxLine=-1, max_nR=0;
        for(Protein p : pp) max_nR=maxi(max_nR,p.countResidues());
        final int features[]=new int[max_nR];
        ResidueSelection[] ff=new ResidueSelection[99];
        for(Protein p : pp) {
            Arrays.fill(features,0);
            final ResidueSelection[] ss=p.allResidueSelections();
            int count=0;
            for(ResidueSelection s : ss) {

                if (styleUL(s) || type(s)=='F') {
                    if (count>=ff.length) ff=chSze(ff,count+99, ResidueSelection.class);
                    ff[count++]=s;
                }
            }
            if (count>0) Arrays.sort(ff,0,count, SequenceFeatures.instance());

            for(int iS=0; iS<count; iS++) {
                final ResidueSelection s=ff[iS];
                if (!isSelVisible(VisibleIn123.SEQUENCE|VisibleIn123.NO_FLASH,s)) continue;
                final boolean sel[]=selectedAminosDisplayed(s);
                final int from=s.getSelectedAminoacidsOffset()-Protein.firstResIdx(p), to=mini(features.length,from+sel.length );

                int foundLine=-1;
                nextLine:
                for(int iLine=0; iLine<32; iLine++) {
                    final int bitMask=1<<iLine;
                    for(int iA=from; iA<to; iA++) {
                        if (iA<0 || iA<from || iA<features.length && sel[iA-from] && (features[iA]&bitMask)!=0) continue nextLine;
                    }
                    foundLine=iLine;
                    for(int iA=from; iA<to; iA++) features[iA]|=bitMask;
                    break;
                }
                if (foundLine>=0) {
                    ((VisibleIn123)s).setStyle(VisibleIn123.STYLE_UNDERLINE+ (foundLine<<VisibleIn123.BIT_SHIFT_LINE));
                    if (maxLine<foundLine) maxLine=foundLine;
                }
            }
        }
        // return _maxPos=maxLine+1;
return maxLine+1;
    }
    /* <<< Layout Underlining <<< */
    /* ---------------------------------------- */
    /* >>> Map >>> */
    private static AbstractButton _cbProceedSame;
    private final static String REPLACE="Replace", SKIP="Skip it", CANCEL="Cancel";

   public static boolean[] mapPositionsZ(ResidueSelection ss[],  Protein pDest) {
       if (pDest==null||sze(ss)==0) return null;
       boolean selected[]=null;
       for(ResidueSelection s:ss) {
           final Protein pSrc=sp(s);
           if (pSrc==null) continue;
           selected=mapPositionsZ(s.getSelectedAminoacids(), selAminoOffsetZ(s), pSrc, pDest, selected);
       }
       return selected;
   }

    public static boolean[] mapPositionsZ(boolean bbSrc[], int offsetSrc, Protein src, Protein dest, boolean[] buffer) {
        final boolean selDest[]=buffer!=null ? buffer : new boolean[dest.countResidues()];
        for(int i=sze(bbSrc)+offsetSrc; --i>=offsetSrc;) {
            if (bbSrc[i-offsetSrc]) {
                final int col=src.getResidueColumnZ(i);
                final int idxDest=dest.column2nextIndexZ(col);
                if (idxDest>=0 && idxDest<selDest.length && dest.getResidueColumnZ(idxDest)==col) selDest[idxDest]=true;
            }
        }
        return selDest;
    }
    public final static int COPY_ADD_TO_PROTEIN=1<<1, COPY_SINGLE=1<<2, COPY_ASK_NAME=1<<3, COPY_REPORT=1<<4;
    private static ResidueAnnotation copyResidueSelectionsSingle(int options, ResidueSelection ss[], Protein pDest) {
        final boolean selected[]=mapPositionsZ(ss,pDest);
        if (fstTrue(selected)<0) return null;
        final String name= toStrgTrim(0!=(options&COPY_ASK_NAME) ? ChMsg.input("Please enter the name of the new residue selection", 20, "Unnamed") : "Unnamed");
        if (sze(name)==0) return null;
        final ResidueAnnotation a=new ResidueAnnotation(pDest);
        a.setValue(0,ResidueAnnotation.NAME,name);
        a.setValue(0,ResidueAnnotation.GROUP,"Copied");
        a.setValue(0,ResidueAnnotation.POS,Protein.selectedPositionsToText(selected, Protein.firstResIdx(pDest),pDest));
        final ResidueAnnotation s0=deref(ss[0],ResidueAnnotation.class);
        if (s0!=null) {
            nextEntry:
            for(ResidueAnnotation.Entry e0 : s0.entries()) {
                for(ResidueSelection s : ss) {
                    if (s==null || s==ss[0]) continue;
                    boolean found=false;
                    if (s instanceof ResidueAnnotation) {
                        for(ResidueAnnotation.Entry e : ((ResidueAnnotation)s).entries()) {
                            if (e.key()==ResidueAnnotation.POS) continue;
                            if (e.key()==e0.key() && e.value().equals(e0.value())) {
                                found=true;
                                break;
                            }
                        }
                    }
                    if (!found) continue nextEntry;
                }
                a.addE(e0.isEnabled()?0:ResidueAnnotation.E_DISABLED, e0.key(), e0.value());
            }
        }
        a.setColor(colr(ss[0]));
        pDest.addResidueSelection(a);
        return a;
    }
    static ResidueSelection[] copyResidueSelections(int options, ResidueSelection ss[], Protein[] ppDest) {
        if (sze(ss)==0 || sze(ppDest)==0) return ResidueSelection.NONE;
        final BA sb=new BA(99);
        final ResidueAnnotation aaCopied[];
        if (0!=(options&COPY_SINGLE)) {
            final Collection<ResidueAnnotation> v=new ArrayList();
            for(Protein p : ppDest) adNotNull(copyResidueSelectionsSingle(options, ss,p),v);
            aaCopied=toArry(v,ResidueAnnotation.NONE);
        } else aaCopied=copyResidueSelections(options,ss, ppDest, "copied", sb);
        if (0!=(options&COPY_REPORT) && countNotNull(aaCopied)>0) {
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED,111);
            final String t="Copied residue selections";
            StrapAlign.showInJList(0L, null, aaCopied,t,null).showInFrame(ChFrame.AT_CLICK|ChFrame.PACK_SMALLER_SCREEN, t);
        }
        return aaCopied;
    }
    static ResidueAnnotation[] copyResidueSelections(int options, ResidueSelection[] ssSrc, Protein ppDest[], String group, BA sbError) {
        if (sze(ssSrc)==0) return ResidueAnnotation.NONE;
        final Collection<ResidueAnnotation> v=new ArrayList();
        for(ResidueSelection s:ssSrc) {
            if (s==null) continue;
            final ResidueAnnotation aSrc=deref(s, ResidueAnnotation.class);
            for(Protein p0:ppDest) {
                final Protein pDest=sp(p0), pSrc=sp(s);
                if( pDest==null) continue;
                final String nameSrc=s instanceof HasName ? nam(s) : toStrg(s);
                if (pSrc==null) {
                    if (sbError!=null) sbError.a("Skipping Selection ").a(nameSrc).aln(" because getProtein() returns null.");
                } else if (cntains(s,pDest.allResidueSelections())) {
                    if (sbError!=null) sbError.a("Skipping Selection ").a(nameSrc).a(" of protein ").a(pDest).aln(": source and destination protein are identical");
                } else if (sp(s)==pDest) {
                    pDest.addResidueSelection(s);
                    adUniq(s,v);
                    continue;
                } else {
                    final boolean[] newPos=mapPositionsZ(new ResidueSelection[]{s}, pDest);
                    final String name=nameSrc!=ResidueSelection.NAME_STANDARD ? nameSrc : nameSrc+"_"+Protein.selectedPositionsToText(s);
                    if (fstTrue(newPos)>=0) {
                        final ResidueAnnotation a=new ResidueAnnotation(pDest);
                        final String feature;
                        if (aSrc!=null) {
                            for(ResidueAnnotation.Entry e : aSrc.entries()) a.addE(e.isEnabled()?0:ResidueAnnotation.E_DISABLED, e.key(),e.value());
                            feature=a.featureName();
                        } else feature=null;
                        a.setColor(s instanceof Colored ? ((Colored)s).getColor() : C(0xAAaa44));
                        a.setValue(0,ResidueAnnotation.GROUP,group.trim().replaceAll(ProteinViewer.PROTEIN, pSrc.toString()));
                        a.setValue(0,"Copied_from",name +" @ " +pSrc+( aSrc!=null ? "!"+aSrc.value(ResidueAnnotation.POS):""));
                        a.setValue(0,ResidueAnnotation.POS, Protein.selectedPositionsToText(newPos, Protein.firstResIdx(pDest),pDest));
                        a.setValue(0,ResidueAnnotation.NAME, feature!=null && !feature.equals(name) ? feature+"_"+name : name!=null ? name : shrtClasNam(s) );
                        v.add(a);
                    }
                }
            }
        }
        if (0!=(options&COPY_ADD_TO_PROTEIN)) mayBeAddResidueAnnotations(toArry(v, ResidueAnnotation.NONE),DialogCopyAnnotations.class);
        return toArry(v,ResidueAnnotation.NONE);
    }
    static ResidueAnnotation[] mayBeAddResidueAnnotations(ResidueAnnotation[] annotations, Object evtSrc) {
        if (_cbProceedSame!=null) _cbProceedSame.setSelected(false);
        Object allChoice=null;
        final Collection<ResidueAnnotation> vAdded=new ArrayList();
        boolean deleted=false;
        for(ResidueAnnotation sNew:annotations) {
            if (sNew==null) continue;
            final String name=sNew.getName();
            final Protein pDest=sp(sNew);
            if (pDest==null || name==null) continue;
            final ResidueAnnotation[] existingAnnotations=pDest.residueAnnotations();
            int nExisting=0;
            for(ResidueAnnotation s:existingAnnotations) {
                if (sNew!=s && name.equals(s.getName())) nExisting++;
            }
            final Object choice=nExisting==0 ? "" : allChoice!=null ? allChoice : howProceedWhenExist(name,nExisting);
            if (choice==null) continue;
            if (CANCEL.equals(choice)) break;
            if (isSelctd(_cbProceedSame)) allChoice=choice;
            if (SKIP.equals(choice)) continue;
            if (REPLACE.equals(choice)) {
                for(ResidueAnnotation s:existingAnnotations)
                    if (name.equals(s.getName())) {
                        pDest.removeResidueSelection(s);
                        s.dispose();
                        deleted=true;
                    }
            }
            pDest.addResidueSelection(sNew);
            adUniq(sNew,vAdded);
        }
        if (deleted) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_DELETED,111);
        if (sze(vAdded)>0) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED,111);
        return toArry(vAdded, ResidueAnnotation.NONE);
    }
    private final static Object howProceedWhenExist(String name,int nExisting) {
        if (nExisting<1) return null;
        if (_cbProceedSame==null) _cbProceedSame=cbox("Treat following duplicate names the same way");
        final String s=(nExisting>1 ?  nExisting+" annotations" : "An annotation")
            +" with the name <br><b><center>"+name
            +"</center></b><br>already exists for this protein.<br>It is possible to have several annotations with the same name.<br>How do you want to proceed ?";
        return ChMsg.optionVal(pnl(VBHB,s, _cbProceedSame),new String[]{
            REPLACE,
            "Keep it and create another",
            SKIP,
            CANCEL
        });
    }
    /* <<< Map/Copy <<< */
    /* ---------------------------------------- */
    /* >>> Residue Position to Column >>> */

    public static boolean[] toAlignmentColumns(ResidueSelection[] ss) {
        boolean[] bb=NO_BOOLEAN;
        if (ss!=null) {
            for(ResidueSelection s : ss) {
                final Protein p=sp(s);
                if (p!=null) bb=orBB(bb, toAlignmentColumnsZ(s.getSelectedAminoacids(), selAminoOffsetZ(s), p), bb,99);
            }
        }
        return bb;
    }

    private static boolean[] toAlignmentColumnsZ(boolean sel[], int offset, Protein p) {
        boolean bb[]=null;
        for(int i=lstTrue(sel); --i>=0; ) {
            if (!sel[i]) continue;
            final int col=p.getResidueColumnZ(i+offset);
            (bb==null ?  bb=new boolean[col+1] : bb)[col]=true;
        }
        return bb!=null ? bb : NO_BOOLEAN;
    }
    public static int[] aliColumnRange(String tokens, Protein[] pp) {
        int[] r=parseRange(tokens,0,MAX_INT, null);
        if (r==null) {
            int count=0;
            r=new int[]{MAX_INT, 0};
            for(Object o : StrapAlign.proteinsAndAnnotationsWithRegex(tokens, pp, 'A')) {
                final ResidueSelection s=deref(o,ResidueSelection.class);
                final Protein p=sp(s);
                if (p!=null) {
                    count++;
                    r[0]=mini(r[0], p.getResidueColumn(ResSelUtils.firstAmino(s)));
                    r[1]=maxi(r[1], p.getResidueColumn(ResSelUtils.lastAmino(s))+1);
                }
            }
            if (count==0) return NO_INT;
        }
        return r;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Texshade >>> */
    final static String RUN_TEX_ONE_PROTEIN="SP$$RTOP";
    public static void texshade(CharSequence txt, Protein p) {
        final Object dia=SPUtils.dialogSetProteins(new Protein[]{p}, StrapAlign.addDialogC(CLASS_Texshade));
        if (dia!=null) runCR(dia, RUN_TEX_ONE_PROTEIN, txt);
        else Insecure.tellNoPermission('X', "Cannot run TeXshade<br>");
    }
    /* <<< Texshade <<< */
    /* ---------------------------------------- */
    /* >>> Filter >>> */
    public static ResidueSelection[] filter(int type, ResidueSelection ss0[], Protein pp[]) {
        if (ss0==null) return ResidueSelection.NONE;
        ResidueSelection[] ss=ss0;
        for(int i=ss.length; --i>=0;) {
            final ResidueSelection s=ss[i];
            final Protein p=s==null?null:s.getProtein();
            final boolean del=
                p==null || !isSelVisible(type,s) ||
                pp!=null && !cntains(p,pp);
            if (del) {
                if (ss==ss0) ss=ss0.clone();
                ss[i]=null;
            }
        }
        return rmNullA(ss,ResidueSelection.class);
    }

    /* <<< Filter <<< */
    /* ---------------------------------------- */
    /* >>> Type >>> */
    public static char type(Object o) {
        if (!(o instanceof ResidueSelection)) return 0;
        if (o instanceof ResidueAnnotation) return  ((ResidueAnnotation)o).featureName()==null ? 'A' : 'F';
        return 'S';
    }

    public static int countType(char type,  ResidueSelection ss[]) {
        if (ss==null) return 0;
        int countA=0, countF=0, countS=0;
        for(ResidueSelection s : ss) {
            final char t=type(s);
            if (t=='A') countA++;
            if (t=='F') countF++;
            if (t=='S') countS++;
        }
        return type=='A' ? countA : type=='F' ? countF : countS;
    }
    /* <<< Type <<< */
    /* ---------------------------------------- */
    /* >>> Align >>> */

    public static void openDialogRealign(ResidueSelection ss[]) {
        runCR(StrapAlign.addDialogC(CLASS_DialogAlign), DialogAlign.RADIO_SELECTION, listFromTo(ss, 4,new BA(999)));
    }
    public static BA listFromTo(ResidueSelection ss[], int minLen, BA sb) {
        final Protein[] pp=SPUtils.spp(ss);
        SPUtils.sortVisibleOrder(pp);
        for(Protein p : pp) {
            int frst=MAX_INT,last=MIN_INT;
            for(ResidueSelection s : ss) {
                if (p!=s.getProtein()) continue;
                frst=mini(frst,firstAmino(s));
                last=maxi(last, lastAmino(s));
            }
            if (frst>=0 && last-frst>=minLen) {
                sb.a(delLstCmpnt(p,'!')).a('/').a(frst+1);
                if (frst!=last) sb.a('-').a(last+1);
                sb.a('\n');
            }
        }
        return sb;
    }

    public static int firstAmino(ResidueSelection s) { return s==null ? -1 : fstTrue(s.getSelectedAminoacids())+s.getSelectedAminoacidsOffset(); }
    public static int  lastAmino(ResidueSelection s) { return s==null ? -1 : lstTrue(s.getSelectedAminoacids())+s.getSelectedAminoacidsOffset(); }
    /* <<< Align <<< */
    /* ---------------------------------------- */
    /* >>> Choose for Context or Annotation >>> */

    public final static long CHOOSE_SINGLE=1<<1, CHOOSE_CONTEXT=1<<2, CHOOSE_EDIT=1<<3;
    public static void chooseResSels(long options, ResidueSelection ss[], AWTEvent ev, String msg, Runnable run) {
        if (countNotNull(ss)==0) return;
        final boolean singl=0!=(options&CHOOSE_SINGLE);
        final Component jl=new ChJList(weakRefArry(ss), ChJTable.ICON_ROW_HEIGHT|ChJTable.DEFAULT_RENDERER | (singl?ChJTable.SINGLE_SELECTION:0L)).setSelI(0);
        final String
            title=plrl(singl?1:2, "Choose %A residue selection%S"), m,
            id=0!=(options&CHOOSE_CONTEXT) ? "CTXT" : 0!=(options&CHOOSE_EDIT) ? "EDIT" : null;
        if (0!=(options&CHOOSE_EDIT))  m="Edit residue annotation";
        else m=0!=(options&CHOOSE_CONTEXT) ? "Context menu for residue selections" : msg;
        final Object
            b=new ChButton("CRS").t(ChButton.GO).doClose(0,REP_PARENT_WINDOW).r(id==null?run:thrdCR(instance(0), id, new Object[]{jl})),
            pSouth=pnl(VBHB,title,pnl(b)),
            pMain=pnl(CNSEW,scrllpn(SCRLLPN_EBORDER|SCRLLPN_INHERIT_SIZE|SCRLLPN_TOOLS, jl/*, dim(1,4*EX)*/), m, pSouth);
        new ChFrame(title).ad(pMain).shw(ChFrame.AT_CLICK|ChFrame.PACK|ChFrame.ALWAYS_ON_TOP);
    }
    /* <<< Choose for Context or Annotation <<< */
    /* ---------------------------------------- */
    /* >>> Is position selected >>> */
    public static boolean isSelectedAAZ(int iA, ResidueSelection s) {
        return
            s==null ? false :
            get(iA-s.getSelectedAminoacidsOffset()+Protein.firstResIdx(s.getProtein()), s.getSelectedAminoacids());
    }

    public static int selAminoOffsetZ(ResidueSelection s) {
        return s==null?0:s.getSelectedAminoacidsOffset()-Protein.firstResIdx(sp(s));
    }
    /* <<< Is position selected  <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=deref(arg,Object[].class);
        final ChJList jList=get(0,argv,ChJList.class);
        final ResidueSelection ss[]=derefArray(jList==null?null : jList.getSelectedValues(), ResidueSelection.class);
        if (countNotNull(ss)>0) {
            if (id=="EDIT") {
                toResidueAnnotation(true, ss);
                for(ResidueSelection s : ss) StrapAlign.editAnnotation(deref(s , ResidueAnnotation.class));
            }
            if (id=="CTXT") ResidueSelectionPopup.showContextMenu(ss);
        }

        if (id==PROVIDE_HIGHLIGHTS) {
            final Collection
                vP=0==(_type&HIGHLIGHT_P) ? null : StrapAlign.vProteins(),
                vA=0==(_type&HIGHLIGHT_A) ? null : ResidueAnnotation.vAll;
            final int mc=
                (0==(_type&HIGHLIGHT_P) ? 0 : Protein.MC_GLOBAL[MC_PROTEIN_LABELS]+modic(vP))+
                (0==(_type&HIGHLIGHT_A) ? 0 : Protein.MC_GLOBAL[MC_RESIDUE_SELECTION_LABELS]);
            if (_hlMC!=mc || _hl==null) {
                _hlMC=mc;
                final int L1=sze(vA), L2=sze(vP);
                final String tt[]=new String[L1+L2];
                for(int i=L1; --i>=0;) {
                    final ResidueAnnotation a=get(i,vA,ResidueAnnotation.class);
                    if (a!=null) tt[i]=(String)a.getRenderer(HasRenderer.JLIST,null);
                }
                for(int i=L2; --i>=0;) tt[L1+i]=nam(get(i,vP));
                _hl=OccurInText.addToLookupTable(tt,null);
            }
            return _hl;
        }
        return null;
    }

    private int _hlMC;
    private byte[][][] _hl;

    /* <<< Threading  <<< */
    /* ---------------------------------------- */
    /* >>> Variables >>> */

    private static String[]  _varNames[];
    public static String[] variables(int n) {
        if (_varNames==null) {
            final String[]
                fl=splitTokns("AAA aaa Aaa Aaa INDEX NUMBER A "),
                vv=splitTokns("$CHAIN $SELECTION_NAME $TEX_RESIDUES $TEX_PROTEIN"),
            aa=new String[fl.length*2];
            for(int i=0; i<fl.length; i++) {
                aa[2*i  ]="$FIRST_"+fl[i];
                aa[2*i+1]= "$LAST_"+fl[i];
            }
            _varNames=new String[][]{joinArrays(aa, vv, String.class), aa};
        }
        return _varNames[n];
    }
    public static CharSequence replaceVariable(CharSequence s0, Object o0) {
      if (strchr('$',s0)<0) return s0;
      if (s0==null) return null;
        ResidueSelection a=deref(o0,ResidueSelection.class);
        Protein p=deref(o0,Protein.class);
        final Object oo[]=deref(o0, Object[].class);
        for(int i=sze(oo); --i>=0; ) {
            if (a==null) a=deref(oo[i],ResidueSelection.class);
            if (p==null) p=deref(oo[i],Protein.class);
        }
        if (p==null && a!=null) p=a.getProtein();
        CharSequence s=a==null?s0: replaceVariableBB(s0, a.getSelectedAminoacids(), a.getSelectedAminoacidsOffset(), nam(a), p, null);
        final java.awt.Color color=colr(a);
        if (color!=null) { /* Pymol */
            final int iDef=strstr(STRSTR_w, Pymol.DEFINE_THIS_COLOR,s);
            final int iCol=strstr(STRSTR_w, Pymol.PYMOL_COLOR_NAME,s);
            if (iDef>=0 || iCol>=0) {
                final int rgb=color.getRGB();
                final BA hex=new BA(7).a('c').aHex(rgb,6);
                if (iDef>=0 ) {
                    s=strplc(STRSTR_w, Pymol.DEFINE_THIS_COLOR, "set_color "+hex+" , [ "+  ( (rgb>>16)&255)/256f+","+(((rgb>>8)&255)/256f)+","+((rgb&255)/256f)+" ] ", s);
                }
                if (iCol>=0) s=strplc(STRSTR_w, Pymol.PYMOL_COLOR_NAME, hex, s);
            }
        }
        return s;
    }

    public static CharSequence replaceVariableBB(CharSequence s0,  boolean bb[], int offset, String name, Protein p, Protein allPP[]) {
        if (strchr('$',s0)<0) return s0;
        CharSequence s=s0;
        s=strplc(STRSTR_w_R, "$SELECTION_NAME", name, s);
        if (strstr("$FIRST_",s)>=0 || strstr("$LAST_",s)>=0) {
            for(String n : variables(1)) {
                if (strstr(n,s)<0) continue;
                final int L=sze(n), idx=offset+ (chrAt(1,n)=='F' ? fstTrue(bb) : lstTrue(bb));
                final char cz=chrAt(L-1, n), cy=chrAt(L-2, n), cx=chrAt(L-3, n);
                String rplc=null;
                if (p==null || idx<0) rplc="?";
                else if ( (cx|32)=='a' && (cy|32)=='a' && (cz|32)=='a') {
                    final int a=p.getResidueName32(idx);
                    final byte[] aaa={
                        (byte)((a    )&0xff|(cx&32)),
                        (byte)((a>>8 )&0xff|(cy&32)),
                        (byte)((a>>16)&0xff|(cz&32))
                    };
                    rplc=toStrg(aaa);
                }
                else if (cy=='E'&&cz=='X') rplc=toStrg(idx+1);
                else if (cy=='_'&&cz=='A') rplc=toStrg((char) p.getResidueType(idx));
                else if (cy=='E'&&cz=='R') rplc=idx<0||p==null?"-1":toStrg(idxAmino(idx,p));
                if (rplc!=null) s=strplc(STRSTR_w_R, n, rplc, s);
            }
        }
        String n;
        if (strstr(n="$CHAIN",s)>=0) s=strplc(STRSTR_w_R,n,toStrg(p==null?'?':(char) p.getResidueChain(fstTrue(bb)+offset)),s);
        if (strstr(n="$TEXSHADE_RESIDUES",s)>=0 || strstr(n="$TEX_RESIDUES",s)>=0) s=strplc(STRSTR_w_R,n,texshadeRange(bb,offset,p),s);
        if (strstr(n="$PROTEIN_NO",s)>=0 || strstr(n="$TEX_PROTEIN",s)>=0) {
            s=strplc(STRSTR_w_R, n, p==null||allPP==null ? "1" : toStrg(idxOf(p,allPP)+1),s);
        }
        return s;
    }

    public static String texshadeRange(boolean bb[], int offset, Protein p) {
        final BA sb=new BA(40);
        for(int i=0;i<bb.length;i++) {
            if (bb[i]) {
                if (sze(sb)>0) sb.a(',');
                sb.a(idxAmino(i+offset,p)).a("..");
                while (i<bb.length && bb[i]) i++;
                sb.a(idxAmino(--i+offset,p));
            }
        }
        return toStrg(sb);
    }

    private static boolean _resNumPDB;
 static int idxAmino(int i, Protein p) {
        final int nn[]=p==null?null:p.getResidueNumber(), ia=i-Protein.firstResIdx(p);
        return _resNumPDB&&i>=0 && ia<sze(nn)  ? nn[ia] : i+1;
    }
    public static void useResNum(boolean b) { _resNumPDB=b;}

    /* <<< Variables <<< */
    /* ---------------------------------------- */
    /* >>> Misc >>> */
    public final static int NARROW_IS_ALIGNMENT_MATCH=1<<0;
    public static void cropSequences(int opt, ResidueSelection aa[]) {
        final String
            what=0!=(opt&NARROW_IS_ALIGNMENT_MATCH) ? " sequence matches " : " residue selections ",
            msg=
            "The leading and trailing part of the amino acid sequence before and after the "+what +
            (" will be removed.<br>"+
             "This is implemented with the help of the rename dialog.<br>"+
             "By appending the respective sequence range to the protein name, the amino acid sequence is pruned respectively.<br><br>"+
             "Continue?");
        if (aa.length>=0 && ChMsg.yesNo(msg)) {
            final Protein pp[]=new Protein[aa.length];
            final Object key=ResSelUtils.class;
            for(int i=0; i<aa.length; i++) {
                final Protein p=sp(aa[i]);
                if (p==null) continue;
                final boolean bb[]=aa[i].getSelectedAminoacids();
                final int offset=aa[i].getSelectedAminoacidsOffset(), fst=fstTrue(bb);
                if (fst>=0) pcp(key, new int[]{fst+offset, lstTrue(bb)+offset+1},  pp[i]=sp(aa[i]));
            }
            if (countNotNull(pp)>0) {
                SPUtils.sortVisibleOrder(pp);
                DialogRenameProteins.instance(key).rename(0L, pp, key);
            }
        }

    }

    public static String ttSelect() {
        return
            "Selected residue selections are indicated by marching ants in the alignment pane.<br><br>"+
            "<b>Selecting residue selections in the alignment pane: </b>"+
            "<ul><li>Ctrl+left-click highlightings.</li><li>Open a red-white rubber band around the underlinings.</li></ul>";
    }

    private static String _styles[];
    public static String[] styles() {
        if (_styles==null) _styles=finalStaticInts(VisibleIn123.class, "STYLE", VisibleIn123.STYLE_UNDERLINE+1);
        return _styles;
    }
    public static String value(String key, ResidueSelection s) { return s instanceof ResidueAnnotation ? ((ResidueAnnotation)s).value(key) : null;}

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Bounding box  >>> */

    public static Rectangle enclosingRectangle(Object[] pp_oo) {
        int left=MAX_INT, right=MIN_INT, top=MAX_INT, bottom=MIN_INT;
        final Protein ppVis[]=StrapAlign.visibleProteins();
        for(Object o : oo(pp_oo)) {
            final ResidueSelection s=deref(o,ResidueSelection.class);
            final Protein p=s!=null?sp(s) : deref(o,Protein.class);
            final int row=p!=null ? idxOf(p,ppVis) : -1;

            if (row>=0) {
                final int nR=p.countResidues(), a0=firstAmino(s), a1=lastAmino(s);
                top=mini(top,row);
                bottom=maxi(bottom,row+1);
                if (a0>=0) {
                    left=mini(left,p.getResidueColumn(mini(nR-1,a0)));
                    right=1+maxi(right,p.getResidueColumn(mini(nR-1,a1)));
                }
            }
        }
        return top>bottom?null:new Rectangle(left,top, right-left, bottom-top);
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> DnD  >>> */
    static String selectedToUpperCase(ResidueSelection s, String on, String off) {
        final Protein p=sp(s);
        final byte[] aa=p==null?null:p.getResidueType();
        final int N=sze(aa);
        if (N==0) return null;
        final BA ba=new BA(aa.length+99);
        final boolean bb[]=s.getSelectedAminoacids();
        final int offset=s.getSelectedAminoacidsOffset()-Protein.firstResIdx(p);
        boolean prev=false;
        for(int i=0; i<N; i++) {
            final boolean b=get(i-offset,bb);
            if (b && !prev) ba.a(on);
            ba.a((char)(b ? (aa[i]&~32) : (aa[i]|32) ));
            if (!b && prev) ba.a(off);
            prev=b;
        }
        return toStrg(ba);
    }

    public static File dndFile(ResidueSelection s) {
        final Protein p=sp(s);
        if (p==null) return null;
        final BA sb=new BA(3333);
        ResidueAnnotation.saveS(true, s, sb);
                final File f=file(STRAPTMP+"/"+p+"_"+firstAmino(s)+"-"+lastAmino(s)+"_"+nam(s)+DND_SUFFIX);
        //final File f=file(new BA(99).a(STRAPTMP).a('/').filter(' '|FILTER_NO_MATCH_TO|FILTER_TRIM, FILENM,p).a('_').a(firstAmino(s)).a('-').a(lastAmino(s)).a('_').a(nam(s)).a(DND_SUFFIX));
        wrte(f,sb);
        delFileOnExit(f);
        return f;
    }

    public static int idxOfUpperCase(byte[] uc, byte[] rt, boolean returnScore) {
        int idx=-1, maxScore=-1;
        final int up1=nxt(UPPR,uc), up9=prev(UPPR,uc);
        if (up1<0) return -1;
        final int c1=uc[up1]|32;
        for(int i=rt.length-up9+up1; --i>=0;) {
            if ((rt[i]|32)!=c1 || !strEquls(STRSTR_IC,uc,up1,up9+1,  rt, i)) continue;
            int score=0, k=i, kTo=maxi(i-up1,0);
            while (k>=kTo && (rt[k]|32)== (32|uc[up1+k-i])) k--;
            score+=(i-k);
            final int kFrom=k=i+up9-up1;
            kTo=mini(rt.length, uc.length+i-up1);
            while (k<kTo && (rt[k]|32)== (32|uc[up1+k-i])) k++;
            score+=k-kFrom;
            if (score>maxScore) {
                maxScore=score;
                idx=i;
            }
        }
        return returnScore ? maxScore : idx;
    }

    public static ResidueAnnotation[] dropResSel(boolean addToProtein, File ff[], Protein ppAll[]) {
        final ResidueAnnotation ss[]=new ResidueAnnotation[sze(ff)];
        for(int iF=0; iF<ss.length; iF++) {
            final BA txt=readBytes(ff[iF]);
            if (sze(txt)==0) continue;
            final ResidueAnnotation a=new ResidueAnnotation(null);
            a.parse(txt,0,MAX_INT);
            final byte[] uc=toByts(a.value(DND_SELECTED_UC));
            if (sze(uc)==0) continue;
            int bestScore=-1, bestI=-1;
            Protein bestP=null;
            for(Protein p : ppAll) {
                final int score=idxOfUpperCase(uc,p.getResidueTypeExactLength(), true);
                if (bestScore<score) {
                    bestScore=score;
                    bestI=idxOfUpperCase(uc,p.getResidueTypeExactLength(), false);
                    bestP=p;
                }
            }
            final int up1=nxt(UPPR,uc);
            if (bestP==null || up1<0) continue;
            final boolean bb[]=new boolean[prev(UPPR,uc)+1-up1];
            for(int i=bb.length; --i>=0;) bb[i]=is(UPPR,uc,i+up1);
            a.setProtein(bestP);
            a.setValue(0,ResidueAnnotation.POS, new BA(99).boolToText(bb,1+bestI+Protein.firstResIdx(bestP)," ","-"));
            a.setValue(0,DND_SELECTED_UC,null);
            if (addToProtein) bestP.addResidueSelection(a);
            ss[iF]=a;
        }
        return rmNullA(ss,ResidueAnnotation.class);
    }

    public static void dropResSelAsk(ResidueSelection ss[], Protein[] ppTarget) {
        final int np=(sze(ppTarget));
        if (np==0) return;
        final Object cbSingle=countNotNull(ss)>1 ? cbox(plrl(ss.length, "Concatenate %N selection%S to one single selection")) : null;
        final String cc[]={"Do not copy", "Copy to "+ppTarget[0], np==1?null:plrl(np, "To  %N  proteins") };
        final int iChoice=ChMsg.option(pnl(VB,cbSingle,plrl(ss.length, "Copy %N residue selection%S?")),rmNullS(cc));
        if (iChoice>0) {
            final int opt=COPY_ASK_NAME|COPY_ADD_TO_PROTEIN|COPY_REPORT |(isSelctd(cbSingle)?COPY_SINGLE:0);
            StrapAlign.animatePositionS(copyResidueSelections(opt, ss, iChoice==2 ? ppTarget : SPUtils.spp(ppTarget[0])));
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Secondary structure  >>> */

    public static ResidueAnnotation[] fromSecStru(Protein p) {
        final String GROUP="Secondary_structure";
        final byte[] ss=p==null?null:p.getResidueSecStrType();
        if (ss==null) return ResidueAnnotation.NONE;
        final Collection v=new ArrayList();
        final int nR=mini(ss.length, p.countResidues()), first=Protein.firstResIdx(p);
        for(int iType=2; --iType>=0;) {
            final int type=iType==0?'H':'E';
            final String name0=type=='H'?"Helix_":"Sheet_";

            for(ResidueAnnotation s : p.residueAnnotations()) {
                if (!GROUP.equals(s.value(ResidueAnnotation.GROUP))) continue;
                if (s.getName().startsWith(name0) && cntainsOnly(DIGT, s.getName(), name0.length(), MAX_INT)) s.dispose();
            }

            for(int i=0, count=0; i<nR; i++) {
                if (ss[i]==type) {
                    final int from=i;
                    while(++i<nR && ss[i]==type);
                    final ResidueAnnotation a=new ResidueAnnotation(p);
                    a.setValue(0,ResidueAnnotation.GROUP,GROUP);
                    a.setValue(0,ResidueAnnotation.POS,(first+1+from)+"-"+(first+i));
                    a.setValue(ResidueAnnotation.E_NO_SAVE, ResidueAnnotation.NAME,name0+ ++count);
                    a.setColor(C(type=='H'?0xFF0000:0xFFff00));
                    a.setValue(0,ResidueAnnotation.BG_IMAGE, "prediction_"+(char)type);

                    v.add(a);
                    p.addResidueSelection(a);
                }
            }
        }
        return toArry(v,ResidueAnnotation.class);
    }

    public static BA asText(boolean name, ResidueSelection s,BA sb) {
        if (s!=null) {
            sb.a(sp(s)).a('/');
            if (sze(nam(s))>0 && name) sb.a(nam(s));
            else sb.boolToText(s.getSelectedAminoacids(), 1+Protein.firstResIdx(sp(s))+s.getSelectedAminoacidsOffset(),",","-");
            sb.a('\n');
        }
        return sb;
    }

   public static ResidueSelection[] plusSelected(boolean strictType, ResidueSelection ss[]) {
        Collection<ResidueSelection> v=null;
        for(char type : strictType?  new char[]{'A','F','S'} : new char[]{'s'}) {
            final Object aa[]=StrapAlign.coSelected().residueSelections(type);
            if (cntainsAtLeastOne(ss,aa)) adAllUniq(aa,v==null?v=new ArrayList() : v);
        }
        if (v==null) return ss!=null?ss:ResidueSelection.NONE;
        return toArry(v,ResidueSelection.class);
   }

    public final static int INFO_ANNOTATION=9;
    public static String info(int i, ResidueSelection s, Object comboKey) {
        if (s==null) return null;
        final ResidueAnnotation a=s instanceof ResidueAnnotation ? (ResidueAnnotation)s : null;
        if (i==0) return nam(s);
        if (i==1) return a==null?null:a.featureName();
        if (i==2) return toStrg(s.getProtein());
        //
        if (i==3) return toStrg(firstAmino(s));
        if (i==4) return toStrg(lastAmino(s));
        if (i==5) return toStrg(countTrue(s.getSelectedAminoacids()));
        //
        if (i>=6 && i<=8) {
            final SelectorOfNucleotides sn=s instanceof SelectorOfNucleotides ? (SelectorOfNucleotides)s : null;
            final boolean nn[]=sn==null?null : sn.getSelectedNucleotides();
            final int off=sn==null?0: sn.getSelectedNucleotidesOffset();
            if (i==6) return nn==null?"":toStrg(fstTrue(nn)+off+1);
            if (i==6) return nn==null?"":toStrg(lstTrue(nn)+off+1);
            if (i==8) return toStrg(countTrue(nn));
        }
        if (i==INFO_ANNOTATION && a!=null && comboKey!=null) return a.value(toStrg(comboKey));
        return null;
    }

}
