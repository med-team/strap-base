package charite.christo.strap;
import charite.christo.protein.Protein;
import charite.christo.*;
/**
   Plugins that return a numeric value for each position (column) in the alignment or null.
   These are plotted in the alignment pane.
   It should notify changes by  dispatching events of type 
   <i>JAVADOC:StrapEvent#VALUE_OF_ALIGN_POSITION_CHANGED</i>
   and dispatching it with  <i>JAVADOC:StrapEvent#run()</i>.
*/
public interface ValueOfAlignPosition extends HasMC{
    void setProteins(Protein... proteins);
    Protein[] getProteins();
    double[] getValues();
    /**
       Increment value when new values are computed.
    */


}
