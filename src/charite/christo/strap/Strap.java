package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import java.util.*;
import java.awt.Component;
import javax.swing.*;
import static charite.christo.strap.StrapScriptInterpreter.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

   Each alignment project is stored in one file directory.  This file
   directory is specified by the user at the beginning of the session and
   is valid during the entire session.

   Two instances of Strap for different projects can be run at the same time.
   Proteins and residue selections can be copied from one to the other via Drag'n drop.

   <br><br><b>Loading protein files:</b> Protein files are loaded into Strap
   by dragging one or more files from the desktop or any other location
   into Strap.
   <br><br>
   Web-pages may contain links to protein databases.
   These links can be dragged from the web browser into Strap
   (Drop_Web_Link* for details).
   <br><br>
   Several Strap-sessions can be opened at the same time and proteins can be dragged from one to the other.

   @author Christoph Gille
*/

public class Strap implements java.awt.event.ActionListener,ChRunnable {
    private final static String EXPORT[]={"-toFasta","-toMultipleFasta","-toMSF","-toClustal","-toHTML","-toWord"};
    final static int NO_ALIGNMENT=0, NO_SUPERPOSE=1, NO_3D=2, DO_CHANGE_ORDER=3;
    final static String DO[]=new String[4], NOT_TO_FRONT="-notToFront",  SCRIPT_COMMANDS="charite/christo/strap/scriptCommands.rsc";
    private final static List<BA> _vScripts=new ArrayList();
    private final static Object SYNC_EX=new Object();
    private final static Set<String> _vDialogs=new HashSet();
    private static ChFrame _frQuit, _frProjects, _frBrowse, _fAbout;
    private static ChTextField _newJarLab, _tf;
    private static File _fProjects;
    private static Component _newJarParent;
    private static Strap _inst;
    private static String _projects[], _listFile;
    private static boolean _initialized, _newJarDownloading, _newJarDownloaded;
    private static int _countAct, _opt=StrapAlign.OPTION_USE_UNIPROT_SOAP;
    private static java.awt.Rectangle _geometry;
    private static Object _bSave, _bScript;
    public static void setListFile(String s) { _listFile=s;}
    public static String listFile() { return _listFile;}
    public static int loadOptions() { return _opt;}

    static Strap instance() { if (_inst==null) _inst=new Strap(); return _inst;}
    static {
        sysSetPrptrty("com.apple.mrj.application.apple.menu.about.name", "Strap");
        sysSetPrptrty("java.util.Arrays.useLegacyMergeSort","true"); // TimSort http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7075600
    }
    /* ---------------------------------------- */
    /* >>> Event >>> */

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q instanceof JMenuItem) setWndwState('t',_frProjects);
        {
            if (cmd=="SST") showScriptText(_vScripts,true);

            final boolean cancel=cmd==ACTION_WINDOW_CLOSING && q==_frQuit || cmd=="C";
            if (cmd=="V") {
                DesktopUtils.main("-viewTextFiles");
                _frProjects.dispose();
            }
            if (cmd==ACTION_WINDOW_CLOSING && q==_frProjects) synchronized(SYNC_EX) {
                    Web.checkForNewVersion("<*");
                    sleep(333);
                    shutDwn(9999);
                }
            if (cancel) {
                final ChFrame f=StrapAlign.frame();
                setVisblC(true, f, 0);
                f.setExtendedState(0);
                revalAndRepaintC(f);
                pcp(KOPT_DISABLE_EV,_frProjects,"");
                setVisblC(false,_frQuit, 0);
                pcp(KOPT_DISABLE_EV,_frProjects,null);
            }
            if (cmd=="S" || cmd=="Q" || cancel) {
                final ChTextArea ta=gcp(ChTextArea.class,_frQuit,ChTextArea.class);
                final String txt=toStrgTrim(ta);
                if (nxt(-SPC, txt)>=0) {
                    ta.t("");
                    final boolean isUploading[]={true};
                    final Runnable t=ChStdout.thread_uploadBug("\n"+ANSI_MAGENTA+"Comment"+ANSI_RESET+" "+txt+"\n\n", isUploading);
                    if (cancel) startThrd(t); else t.run();
                    for(int i=0; i<20&&isUploading[0]; i++) sleep(100);
                }
                if (cmd=="S") StrapAlign.save();
                if (cmd=="Q" || cmd=="S") synchronized(SYNC_EX) {
                        final Object ffn[]=oo(SequenceFeatures._vNoGeneralization);
                        if (sze(ffn)>0) {
                            final File f=file("~/@/log/featuresAtSamePosition.txt");
                            final Collection v=new HashSet(), vReport=new ArrayList();
                            adAll(splitLnes(readBytes(f)),v);
                            int len=0;
                            for(Object o : ffn) {
                                final String line=o.toString();
                                if (v.add(line)) {
                                    len+=line.length()+1;
                                    vReport.add(line);
                                }
                            }
                            if (len>0) {
                                final BA ba=new BA(len+sze(f));
                                String lines[]=strgArry(v);
                                Arrays.sort(lines);
                                wrte(f,ba.join(lines,"\n"));

                                lines=strgArry(vReport);
                                Arrays.sort(lines);
                                putln(f,ba.clr().join(lines,"\n"));
                            }
                        }
                        shutDwn(9999);
                    }
            }
        }
        if (cmd=="About") Strap.aboutDialog();
        if (cmd=="H") {
            final File f=file(getTxt(q));
            openProject(f,"",  null,null,null,false);
            saveProjectDir(f);
        }
        if (cmd=="SEARCH_HD") {
            final ChFileChooser fs=new ChFileChooser(ChFileChooser.CLOSE|ChFileChooser.FOLDERS, "Strap");
            addActLi(this,fs);
            final String TITLE="Choose project directory";
            if (_frBrowse==null) _frBrowse=new ChFrame(TITLE).ad(fs).size(800,600);
            _frBrowse.shw(ChFrame.ALWAYS_ON_TOP|ChFrame.CENTER);
            setWndwStateLaterT('F', 99, TITLE);
        }
        if (cmd==ACTION_ENTER) {
            final String s=toStrg(_tf);
            if (sze(s)==0) error("The text field is empty.<br>Enter the complete path");
            final File f=file(FILE_NO_ERROR, s);
            if (f!=null) {
                if (s.indexOf(' ')>0 || s.indexOf("%20")>0){
                    final BA err=new BA(999).a("The directory <pre class=\"data\">").a(s).a("</pre>Is invalid because it contains white space!<br>");
                    if (myComputer() || isWin()) err.aRplc(0L, "DIR", f, readBytes(rscAsStream(0L,_CC,"substDriveLetter.html") ));
                    error(err);
                    return;
                }
                else if (f.exists() && !isDir(f)) error("The path<pre>"+s+"</pre>is not a directory. It is a file.");
                else {
                    Insecure.securitySetAllowedPath(f);
                    if (mkdrsErr(f)) {
                        openProject(f,"",(Object[])null,null,null,false);
                        saveProjectDir(f);
                    }
                }
            }
        }
        if (cmd==JFileChooser.APPROVE_SELECTION) _tf.t(toStrg(((ChFileChooser)q).getSelectedFile()));
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Quit About >>> */
    public static void aboutDialog() {
        if (_fAbout==null) {
            final String about="<h1>Strap</h1>Author Christoph Gille<br>Home-page: "+URL_STRAP+
                "<br>If used in publication please cite one of those: <ul>"+
                "<li>Structural interpretation of mutations and SNPs using Strap PUBMED:16322575</li>"+
                "</ul>"+
                "<br><br>Date of compilation: "+dateOfCompilation()+
                "<br><br>Recommended Java-Version: 1.5 or higher<br>Current Java-Version "+systProprty(SYSP_JAVA_VERSION)+
                "<br><br><sub><u>Icon collection:</u> Icons 8 see  http://www.visualpharm.com/<br>"+
                "This product includes software developed by the Apache Software Foundation (http://www.apache.org/).</sub><br><br>";

            _fAbout=new ChFrame("About").ad(pnl(CNSEW,new ChJTextPane(about),null, pnl("License: ",ChButton.doView(_CC+"licenses/GPL.txt").t("Gnu License"))));
        }
        _fAbout.shw(ChFrame.AT_CLICK|ChFrame.PACK|CLOSE_CtrlW_ESC);
    }
    static void quitStrap() {
        Web.checkForNewVersion("<q");
        CacheResult.save();
        if (StrapAlign.getInstance()==null) synchronized(SYNC_EX) { shutDwn(33333); }
        final ChFrame f=StrapAlign.frame();
        ChStdout.mayUploadSecurityViolations();

        if (prgOptT("-notAskQuit") && !prgOptT("-noExit")) {
            if (StrapAlign.needsSaving()) StrapAlign.save();
            synchronized(SYNC_EX) { shutDwn(33333); }
        } else inEDTms(thrdM("msgQuit",Strap.class), 99);
        if (prgOptT("-noExit")) {
            StrapAlign.dialogPanel().removeAll();
            StrapAlign.rmProteins(false, StrapAlign.proteins());
            setVisblC(false,f,0);
            f.getContentPane().removeAll();
            f.dispose();
            stopAllAlignments();
        }
    }

   public static void msgQuit() {
        final ChFrame f=StrapAlign.frame();
        if (_frQuit==null) {
            final ChTextArea taComments=new ChTextArea("");
            pcp(KEY_IF_EMPTY,"Type here", taComments);
            final Strap li=instance();
            final ChButton t1=toggl(), t2=toggl();
            _bSave=new ChButton("S").li(li).t("Save and Quit");
            final Object
                sp=scrllpn(0,taComments,dim(1,4*EX)),
                contactN="Please type any problems, feed-back, questions or suggestions. The message will be sent to the author. ",
                contactS=pnl(HB, "If you want a reply,  please insert your  E-mail address, Or send an E-mail directly to ", monospc(" christophgil@googlemail.com ")),
                contact=pnl(CNSEW,sp,contactN, contactS,"#ETBem"),
                pnlOpts=buttn(TOG_KEEP_TMP_FILES).cb(),
                like=pnl(VBHB,"#ETBem", "Please recommend Strap to friends and colleagues.",
                         "For web links to Strap use the following address:",
                         pnl("Home page of Strap: ",URL_STRAP )),
                msg=pnl(VB,
                        pnl(HBL,
                            t1.doPack().doCollapse(true,t2).doCollapse(false,contact).rover(IC_MAIL).tt("Contact"), " ",
                            t2.doPack().doCollapse(true,t1).doCollapse(false,like).rover(IC_LIKE).tt("Like it"), " "
                            ),
                        like,
                        contact,
                        pnl(" ",
                            _bSave,"#"," ",
                            pnl(C(0xFF0000),new ChButton("Q").li(li).t("Quit without saving")),"#"," ",
                            new ChButton("C").li(li).t("Do not quit"),"#"," "
                            ),
                        pnl(HBL, pnlTogglOpts("Options:", pnlOpts ), " ", pnlOpts),
                        pnl(" "),
                        _newJarDownloading ? _newJarLab : null,
                        pnl(" "),
                        prgrssShutDwn()
                        );
            if (prgOptT("-update") && !_newJarDownloaded) {
                _newJarParent=(JComponent)msg;
                startThrd(thrdCR(instance(), "newJarInit"));
            }
            addActLi(li, _frQuit=new ChFrame("Quit Strap").ad(new ChJScrollPane(SCRLLPN_INHERIT_SIZE,(JComponent)msg)));
            pcp(ChTextArea.class,taComments, _frQuit);
            //            inEDTms(thrdM("requestFocus", bSave),999);
        }
        _frQuit.size(500,200).shw(ChFrame.ALWAYS_ON_TOP|ChFrame.CENTER);

        f.setExtendedState(JFrame.ICONIFIED); /* Sometimes empty frame */
        inEDTms(thrdM("requestFocus", _bSave),99);
        setVisblC(false,f,9);
    }
    /* <<< Quit About <<< */
    /* ---------------------------------------- */
    /* >>> main >>> */
    public static void main(String[] argv0) {
        String argv[]=rscAsURL(0,"charite/christo/to_html")==null?argv0 : adToStrgs("-toHTML", argv0, 0);
        setPrgParameters(0,"Strap",argv);
        putln(" --- Strap by Christoph Gille --- ", myComputer()?"*":"");
        if (ChJarSignersHardLinker.isEnabled()) startThrd(new ChJarSignersHardLinker());
        boolean security=true;
        for(int i=0; i<argv.length; i++) {
            String a=argv[i];
            if (a==null) continue;
            if (chrAt(0,a)=='"' && lstChar(a)=='"') a=argv[i]=a.substring(1,a.length()-1);
            final char c0=chrAt(0,a);
            if (c0=='-') {
                for(String s : EXPORT) {
                    if (a.equals(s) || a.startsWith(s) && chrAt(s.length(),a)=='=') {
                        setNoGui();
                        security=false;
                    }
                }
                if (a.startsWith("-newJar")) a="-update";
            } else {
                if (c0=='"' && lstChar(a)=='"') a=a.substring(1,a.length()-1);
                if (looks(LIKE_FILEPATH,a) && new File(a).exists() && a.indexOf(' ')>0) a=rplcToStrg(" ","%20",a);
            }
            argv[i]=a;
        }
        if (!prgOptT("-allowFileModification")) Insecure.setFileModificationControl(security);
        {
            final String h=sysGetProp(USER_DOT_HOME);
            //final String varHome=isWin() ? "HOMEPATH":"HOME", v=ChEnv.get(varHome);
            //if (sze(v)<3) putln(RED_WARNING+"Environment var ", varHome,": ", v);
            if (sze(h)>0 && h.indexOf('%')<0 && withGui()) inEDT(thrdCR(instance(),"DISCL"));
        }
                                                                                                                   
        inEdtCR(instance(),"MAIN",argv);
    }
    /* <<< Main <<< */
    /* ---------------------------------------- */
    /* >>> Web-Mode  >>> */
    public static void removeJNLP_files(boolean sayClearFolder) {
        boolean msg=false;
        final String[] start="strap web_strap web-strap strapLite strap-lite".split(" ");
        for(String sDir : "~/ ~/Desktop ~/Desktop/downloads ~/Dokumente/Downloads ~/Downloads ~/Desktop/Download ~/Documents/Download %TEMP% %TMP% /tmp".split(" ")) {
            final File dir0=!isWin() && chrAt(0,sDir)=='%' ? null : file(sDir);
            File dir=dir0;
            for(int dir2=2; --dir2>=0;) {
                if (dir2==0) {
                    if (dirHome()==dirHome2() || chrAt(0,sDir)!='~') continue;
                    dir=file(dirHome2()+sDir.substring(1));

                }
                final String ff[]=lstDir(dir);
                for(String f : ff) {
                    final int c0=chrAt(0,f)|32;
                    if ((c0=='s'||c0=='w') &&  f.endsWith(".jnlp") && strEquls(STRSTR_IC, start,f)) delFile(file(dir,f));
                }
                msg=msg || sayClearFolder && sze(ff)>33;
            }
        }
    }

    private static boolean isWeb() {
        for(String a : prgOpts())  {
            if (isSystProprty(IS_WEBSTARTED) && a.startsWith("-script")
                || a.startsWith("-align=") || a.startsWith("-alignAndRearange=") || a.startsWith("-load=") || a.startsWith("-pdb=")) return true;
        }
        return false;
    }
    private final static BA[] scriptsSorted(Collection v) {
        /* Fix: -script1=...  before -script11=... */
        final String ss0[]=strgArry(v);
        for(int i=ss0.length; --i>=0;)  ss0[i]=ss0[i].replace((char)0,' ').replace('=',(char)0);
        Arrays.sort(ss0);
        final BA ss[]=new BA[ss0.length];
        for(int i=0; i<ss.length; i++) {
            String s=ss0[i];
            if (sze(s)==0) continue;
            if (s.startsWith("-script")) s=s.substring(1+s.indexOf((char)0));
            s=s.replace((char)0,'=');
            final File f=looks(LIKE_EXTURL,s)?null:file(FILE_NO_ERROR,s);
            ss[i]=new BA(sze(f)>0 ? toStrg(url(f)): jnlpDecode(s));

        }
        return ss;
    }

    /* <<< Web-Mode <<< */
    /* ---------------------------------------- */
    /* >>> run >>> */
    public Object run(String id,Object arg) {
        final boolean newAct= id==Sil.ACTION;
        final int jv=javaVsn();
        final String[] argv= newAct && arg instanceof String ? splitTokns(arg, chrClas1('\t')) : deref(arg, String[].class);
        for(int i=sze(argv); --i>=0;) {
            final String a=delPfx("-publication=",argv[i]);
            if (argv[i]!=a) argv[i]="-script="+URL_STRAP+"PDF/pub/"+a+".html";
        }
        final String DISCLAIMER=
            "Email christophgil@googlemail.com\n\n\n"+
            "No warranty  that  embedded programs for alignment computation, structure prediction, 3D-Visualization ... and Strap itself\n"+
            "will be free from harmful code, so-called Malware.\n\n"+
            "If you worry about safety, then run Strap in a Sandbox .\n\n"+
            "See http://www.bioinformatics.org/strap/security.html for more info.\n\n"+
            "Run Strap?";
        if (id=="DISCL") ChMsg.disclaimerMsg("Strap",DISCLAIMER, sysGetProp(USER_DOT_HOME)+(isWin()?"/":"/.")+"StrapAlign/strapDisclaimer.txt");
        if (strEquls("newJar", id,0) && jv!=14)  {
            final File newJar=file(isWin() ? "~/StrapAlign/new_strap.jar" : "~/@/new_strap.jar"), newJar200=file(newJar+".pack.gz");
            final String sUrl=URL_STRAP+"strap.jar.pack.gz";
            final Component parent=_newJarParent;
            final boolean isJar=sze(thisJarFile())>1000*1000;
            boolean doPack=false;
            if (id=="newJarInit") {
                final int contentLength=getContentLen(url(sUrl)), isUpToDate=isUpToDate(thisJarFile(),url(sUrl));
                if (!isJar || 0==isUpToDate && 1000*1000<contentLength) inEdtLaterCR(this,"newJarButDL",null);
            }
            if (id=="newJarButDL") {
                if (_newJarLab==null) (_newJarLab=new ChTextField()).setEditable(false);
                _newJarLab.setOpaque(false);
                final ChButton but=new ChButton("Update Strap");
                final Object msg=pnl(HBL,"For the next session use updated Strap version: ", but);
                but.r(thrdCR(this, "newJarCB", msg));
                toContainr(TOCONTAINR_REVAL, msg, parent);
                doPack=true;
            }
            if (id=="newJarCB") {
                toContainr(TOCONTAINR_REVAL, pnl(HBL,_newJarLab), parent);
                rmFromParent(arg);
                delFile(newJar);
                delFile(newJar200);
                _newJarDownloading=true;
                startThrd(thrdCR(this,"newJarObserve"));
                startThrd(thrdCR(this,"newJarDL"));
            }
            if (id=="newJarDL") {
                _newJarDownloaded=true;
                downloadAndDecompress(sUrl,newJar200);
                synchronized(SYNC_EX) {
                    ChZip.unpack200(newJar200,0L);
                    sleep(99);
                    if (sze(newJar)==0) delFile(newJar);
                    else if (isJar) {
                        if (dirHome2()!=dirHome()) {
                            final File newJar2=file(dirHome2()+"/StrapAlign/new_strap.jar");
                            putln("\nStandard new_strap.jar: ", newJar);
                            putln("Other new_strap.jar: ", newJar2);
                            delFile(newJar2);
                            cpy(newJar, newJar2);
                        }
                        cpy(thisJarFile(), file("~/@/previousVersions/strap."+fmtDate(thisJarFile().lastModified())+".jar"));
                    }
                }
                _newJarDownloading=false;
            }
            if (id=="newJarObserve") {
                final File f=file(newJar200+"TMP");
                final BA ba=new BA(99);
                while(_newJarDownloading) {
                    ba.clr().a("new_strap.jar ").format10(sze(f),11).a(" bytes");
                    setTxt(toStrg(ba), _newJarLab);
                    sleep(99);
                }
                setTxt(newJar+" "+(sze(newJar)/1024)+" kByte  download finished", _newJarLab);
                inEdtLaterCR(this,"newJarFinalMsg",null);
            }

            if (id=="newJarFinalMsg") {
                toContainr(TOCONTAINR_REVAL,  pnl(HBL,C(0xaaFFaa),"At next start, the updated version will be used."), parent);
                setBG(0xaaFFaa, parentC(_newJarLab));
                doPack=true;
            }
            if (doPack) inEdtLater(thrdM("pack", parentWndw(parent)));

        }
        if (id=="MAIN" || newAct) {
            setPrgParameters(0, "strap",argv);
            final String userHome=systProprty(SYSP_USER_HOME); /* First method, because may ChMsg.askUserHome */
            final String help=newAct ? null : prgOptT("-help") || prgOptT("--help") || prgOptT("-h") ? "" : orS(MAP_ARGV.get("-help"), MAP_ARGV.get("-h"));
            if (help!=null) {
                if ("script".equals(help)) putln(HelpCommands.getInstance(SCRIPT_COMMANDS).alphabetically(null));
                else putln(StrapAlign.manPage(printAnsiColors()?'A':' '));
                System.exit(0);
            } else if (withGui()) setPrgParameters(PARA_RDIRCT, "strap",argv);

            if (DesktopUtils.main(argv)) {
                Web.checkForNewVersion("strap_u");
                return null;
            }
            final Collection<Object>vTargets=new ArrayList();
            _vDialogs.clear();
            BA webAlignment=null;
            if (isSystProprty(IS_WEBSTARTED)) setWndwStateT('I',0,0,"D");
            removeJNLP_files(_countAct++ >10);
            boolean isAt=false;
            final BA sb=new BA(333);
            String[] vars=null, varValues=null;
            int nVars=0;
            for(int i=DO.length; --i>=0;) DO[i]=null;
            Collection<String> scripts=null;
            long loadMask=ProteinParser.SEQUENCE_FEATURES;
            final String OO[]=new String[99];
            for(int early=2; --early>=0;) {
                int countStrapFiles=0;
                for(int iA=0; iA<argv.length; iA++) {
                    String argi=argv[iA];
                    for(int i=0; i<nVars; i++) argi=rplcToStrg(vars[i],varValues[i],argi);
                    final String a=toStrgTrim(argi);
                    if (interpretPrgPara(early!=0,a)) continue;
                    final int eq=a.indexOf('='), inQuotes=chrAt(eq+1,a)=='"' && lstChar(a)=='"'?1:0;
                    if (early!=0) {
                        if (a.endsWith(".strap") && !a.startsWith("-script"))  argv[iA]="-scriptzzzz"+countStrapFiles+++"="+a;
                        if (endWith(STRSTR_IC,".LIST", a) ) _listFile=delPfx('@',a);
                    } else if ("@".equals(a)) { isAt=true;
                    } else if (a.charAt(0)!='-') {
                        sb.a('@', isAt?1:0).a(a).a(' ');
                        isAt=false;
                    } else {
                        final String afterEqu=eq<0?null: toStrg(jnlpDecode(a.substring(eq+1+inQuotes, a.length()-inQuotes)));
                        final String a0=toStrgIntrn(eq<0?a:a.substring(0,eq));
                        final String aq=toStrgIntrn(eq<0?null:a.substring(0,eq+1));
                        if (aq=="-SUBMIT=") continue;
                        int iO=0;
                        for(String s : EXPORT) OO[iO++]=s;
                        final boolean isT=optT(a);
                        final boolean changeOrder=aq==(OO[iO++]="-alignAndRearange=");
                        final boolean isAlign=aq==(OO[iO++]="-align=") || changeOrder, isLoad=aq==(OO[iO++]="-load=");
                        if (changeOrder) DO[DO_CHANGE_ORDER]="all";
                        if (isLoad) DO[NO_ALIGNMENT]="all";
                        final String ali=aq==(OO[iO++]="-pdb=") ? "PDB:"+delPfx("PDB",afterEqu.toUpperCase()) : isAlign||isLoad? afterEqu : null;
                        if (ali!=null) {  (webAlignment==null ? webAlignment=new BA(99) : webAlignment).a(ali).a(' '); }
                        else if (idxOf(a0,EXPORT)>=0) setNoGui();
                        else if (StrapAlign.setDefaultClassByPara(a)) {}
                        else if (CacheResultJdbc.processPara(a)) {}
                        else if ("-rename=sp".equals(a)) _opt|=StrapAlign.OPTION_RENAME_SWISS;
                        else if (a0==(OO[iO++]="-hideAll")) { if (isT && newAct) StrapAlign.setIsInAlignment(false, 0, StrapAlign.proteins()); }
                        else if (a0=="-downloadOriginalProteins") { if (isT) _opt|=StrapAlign.OPTION_DOWNLOAD_ORIGINAL_PROTEINS; }
                        else if (a0==(OO[iO++]="-separatePdbChains")) {if (!isT) _opt|=StrapAlign.OPTION_notSplitChains; }
                        else if (a0==NOT_TO_FRONT) { if (isT) vTargets.add(a0); }
                        else if (a0==(OO[iO++]="-noSeqres")) { if (isT) loadMask=ProteinParser.IGNORE_SEQRES; }
                        else if (a0==(OO[iO++]="-noSequenceFeatures")) { if (isT) loadMask&=(~ProteinParser.SEQUENCE_FEATURES); }
                        else if (a0.startsWith("-script") && eq>0 && (eq==7 || cntainsOnly(LETTR_DIGT_US,a,7,eq))) { if (aq!="-scriptOutput=") scripts=adNotNullNew(a, scripts); }
                        else if (a0==(OO[iO++]="-noIdentical")) StrapAlign.button(StrapAlign.TOG_SKIP_IDENTICAL_SEQUENCE).s(isT);
                        else if (aq==(OO[iO++]="-geometry=")) ChFrame.parseGeometry(afterEqu, _geometry=new java.awt.Rectangle());
                        else if (aq==(OO[iO++]="-noA=")) DO[NO_ALIGNMENT]=afterEqu;
                        else if (aq=="-listFile=" && myComputer()) _listFile=afterEqu; /*FLAV*/
                        else if (aq=="-noSP=") DO[NO_SUPERPOSE]=afterEqu;
                        else if (aq=="-no3D=") DO[NO_3D]=afterEqu;
                        else if (aq==(OO[iO++]="-dialog=")) _vDialogs.add(afterEqu);
                        else if (aq==(OO[iO++]="-dasFeatures=")) vTargets.add(new Object[]{StrapAlign.KEY_DAS,afterEqu});
                        else if (aq==(OO[iO++]="-dasRegistry=")) adAllUniq(splitTokns(afterEqu), DAS._vRegistry);
                        else if (aq=="-variable=") {
                            for(String s : splitTokns(afterEqu, chrClas1(','))) {
                                final int colon=s.indexOf('=');
                                if (colon<1) continue;
                                if (sze(vars)<nVars+1) { vars=chSze(vars,nVars+10); varValues=chSze(varValues,nVars+10); }
                                vars[nVars]=s.substring(0,colon);
                                varValues[nVars++]=s.substring(colon+1);
                            }
                        }
                        else if (aq=="-port=") {
                            final int port=atoi(afterEqu);
                            if (port<1024) putln("Error option ",a," Please use port numbers > 1024 ");
                            else if (new Sil(this,port).isSuccess()) {
                                putln("\n"+ANSI_GREEN+"Accepting"+ANSI_RESET+" commands at port "+port+" Try:\n echo -load=PDB:1sbc | telnet localhost "+port+"\n");
                            }
                        } else if (
                                   !isPrgOpt(a) &&
                                   a0!="-preScript" && a0!="-iconify"&&aq!="-o=" && a0!="-Xmx" && a0!="-noExit" && a0!="-notAskQuit" &&
                                   a0!=(OO[iO++]="-manual") && a0!=(OO[iO++]="-logDnD") && a0!=(OO[iO++]="-update") && a0!=(OO[iO++]="-separateInstance")
                                   ) {
                            final String pp[]=flattenA(String.class, true, new Object[]{OO, PRG_OPTS,DesktopUtils.OPTS});
                            Arrays.sort(pp);
                            final BA ba=new BA(999);
                            for(int i=0; i<pp.length;i++) {
                                ba.a(pp[i]).a(' ', sze(longestName(pp))+1-sze(pp[i])).a('\n', i%3==2?1:0);
                            }
                            putln(RED_WARNING+"Unknown command line option ", a, "\nUse -h for help.\nList of options:\n",ba);
                        }
                    }
                }
                SPUtils.setParserOptions(loadMask);
            }
            String fnHtml=null;
            {
                      StrapAlign align=null;
                for(String xopt : EXPORT) {
                    final String v=MAP_ARGV.get(xopt);
                    if (v==null) continue;
                    final long optHtml=ExportAlignment.HTML|ExportAlignment.PROTEIN_ICONS|ExportAlignment.HTML_HEAD_BODY|ExportAlignment.BALLOON_MESSAGE|
                        ExportAlignment.UNDERLINE_ANNOTATIONS;
                    final long msaFormat=
                        "-toFasta"==xopt?ExportAlignment.FASTA:
                        "-toMSF"==  xopt?ExportAlignment.MSF:
                        "-toClustal"==xopt?ExportAlignment.CLUSTALW:
                        "-toHTML"==xopt?optHtml|ExportAlignment.CSS_BROWSER :
                        optHtml;
                    final String msaPara=orS(v, xopt=="-toWord" && fnHtml!=null ? null :  MAP_ARGV.get("-o"));
                    if (align==null) {
                        align=new StrapAlign();
                        align.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED|StrapAlign.OPTION_SHOW_ORPHAN_HETEROS_IN_3D,sb);
                        if (scripts!=null) new StrapScriptInterpreter().runScripts(scriptsSorted(scripts));
                    }
                    String range=(String)VALUES.get(SCRIPT_set_export_columns);
                    int resPerLine=atoi(VALUES.get(SCRIPT_set_characters_per_line));

                    final Object pSS=VALUES.get(SCRIPT_set_ruler_secondary_structure);

                    final Protein pp[]=align.getProteins();
                    if (msaFormat==0) {
                        for(Protein p : pp) {
                            final File f=file(STRAPOUT+"/"+p+".fasta");
                            wrte(f,sb.clr().a('>').aln(p).aln(p.getResidueTypeExactLength()));
                            putln("Written ",f+"  "+sze(f)," bytes");
                        }
                    } else {
                        final String para[]=splitTokns(msaPara, chrClas1(',')), para0=get(0,para);
                        final String fn=para0==null && xopt=="-toWord" && fnHtml!=null ? delSfx(".html",fnHtml)+".doc" : delSfx('*',para0);
                        if (xopt=="-toHTML") fnHtml=fn;
                        for(int i=1; i<sze(para); i++) {
                            final String a=para[i].trim();
                            final char c=chrAt(1,a)!='=' ? 0 : chrAt(0,a);
                            if (c=='w') resPerLine=atoi(a,2);
                            else if (c=='r') range=a.substring(2);
                            else putln(RED_ERROR, "Unknown parameter ", a);
                        }
                        File f=fn==null?null:file(FILE_NO_ERROR, fn);
                        Protein pSecStru=deref(pSS,Protein.class);
                        for(Protein p : pp) if (pSS==null && pSecStru==null && p.getResidueSecStrType()!=null) pSecStru=p;
                        final ExportAlignment w=new ExportAlignment();
                        w.setProteins(pp);
                        w.setRulerSecStru(pSecStru);
                        w.setResiduesPerLine(resPerLine<=0?60:resPerLine);
                        if (0!=(msaFormat&AlignmentWriter.HTML)) {
                            final BA script=StrapScriptInterpreter.interpretedText();
                            w.setProperty(ExportAlignment.PROPERTY_SCRIPT, script);
                        }
                        w.setProperty(ExportAlignment.PROPERTY_IDENT_THRESHOLD, VALUES.get(SCRIPT_set_conservation_threshold));
                        w.setProperty(ExportAlignment.PROPERTY_COLORING, VALUES.get(SCRIPT_set_color_mode));
                        if (range!=null) {
                            final int[] r=ResSelUtils.aliColumnRange(range, pp);
                            if (r!=null) {
                                if (r.length==0) putln(RED_ERROR+"Unrecognized residue selection ", range);
                                else w.setColumnRange(r[0], r[1]);
                            }
                        }
                        final BA output=new BA(9999);
                        w.getText(para0!=fn?msaFormat&~ExportAlignment.HTML_HEAD_BODY:msaFormat,output);
                        if (f==null) f=file(STRAPOUT+"/alignment."+  (xopt=="-toWord" ? "doc" : w.getFileExtension()));
                        wrte(f, output);
                        putln(xopt," Written  "+f+"  "+sze(f)+" bytes");
                        if (0!=(msaFormat&AlignmentWriter.HTML)) {
                            putln(ANSI_RED+ANSI_FG_WHITE+"If published in the web, please keep the link to the Strap home page."+ANSI_RESET);
                        }
                        CacheResult.save();
                    }
                }
                if (align!=null) System.exit(0);
            }
           if (!newAct) {
                LAFChooser.setLAF(LAFChooser.IF_NOT_ALREADY,false);
                TabItemTipIcon.ad(_CC,"tabItemTipIcon1.rsc");
                TabItemTipIcon.ad(_CC,"tabItemTipIcon2.rsc");
                if (LINUX_PACKAGE  && withGui() && !fExists(GUI_BINARY) && !prgOptT("-probeWebProxy")) {
                    putln(TOERR, "\n"+RED_WARNING+"Strap Started without option -probeWebProxy.\nStrap is usually started with "+GUI_BINARY+" of the package Strap.");
                    putln(TOERR, fExists(GUI_BINARY)?"":"The package Strap is not yet installed.\n");
                }
            }
            vTargets.add(StrapView.KEY_AP);
            vTargets.add(StrapAlign.ACTION_SORT);
            Runnable script=null;

            _vScripts.clear();
            if (sze(scripts)>0) {
                final BA[] ss=scriptsSorted(scripts);
                adAll(ss,_vScripts);
                if (_bScript==null) toLogMenu(_bScript=new ChButton("SST").t("Show script text").i(IC_CONSOLE).li(this),null,null);

                script=new StrapScriptInterpreter().thread_runScripts(ss);
            }
            if (prgOptT("-manual")) {
                new StrapAlign();
                ChFrame.setOptionsForAll(ChFrame.SYSTEM_EXIT_WHEN_ALL_CLOSED);
                StrapAlign.generateManual();
                return null;
            }
                if (myComputer()) putln(" newAct="+newAct+" isWebArguments="+isWeb()+" argv=", argv);
            if (newAct) openProject(null,null, oo(vTargets), webAlignment, script, true);
            else {
                final boolean isWebArguments=isWeb();
                Web.checkForNewVersion(!Insecure.EXEC_ALLOWED?"strapLite" : isWebArguments ? "webStrap" : "strap");
                startThrd(thrdCR(this, "CACHE_FIX"));
                if (isWebArguments) {
                    if (isSystProprty(IS_WEBSTARTED)) {
                        final boolean sepInstance=(StrapAlign.button(StrapAlign.TOG_IN_NEW_FRAME).s() || prgOptT("-separateInstance"));
                        if (!sepInstance && !new Sil(instance(),0).isSuccess()) {
                            Sil.toSocketAndWaitForAnswerThenDie(16968,argv);
                            if (!new Sil(this,16968).isSuccess()) {
                                error("Unable to start communication via localhost:16968"+(jv==14 ? "Reason: Java version too old " : ""));
                            }
                        }
                        setWndwStateT('I',0,0,"D");
                    }
                    final File f=dirWeb();
                    if (isDir(f)) {
                        buttn(TOG_NASK_UPLOAD).s(true);
                        File dir=null;
                        for(int i=1;i<444 && (dir==null || dir.exists()) ;i++) dir=file(f+"/temp"+i);
                        delFileOnExit(dir);
                        openProject(dir,"", oo(vTargets),webAlignment,script,false);
                    } else ChMsg.msgDialog(0L, "Error: Could not create directory<br>Unable to start the program  properly.");
                }
                if (!isWebArguments && (sze(sb)>0 || !isSystProprty(IS_WEBSTARTED) && script!=null)) {
                    File d=dirWorking();
                    if (d.equals(dirHome())|| d.equals(file(sysGetProp(USER_DOT_HOME)+"/Desktop"))) {
                        setWorkingDir(d=file("~/@/defaultProject"));
                        final String readme=
                            "Strap avoids the home or desktop directory to be the project directory.\n"+
                            "The directory "+d+" is used instead.\n";
                        mkdrsErr(d, readme);
                        putln("\n",readme,"\n");
                    }
                    openProject(d,sb, oo(vTargets),null,script,false);
                } else if(!isWebArguments) {
                    _fProjects=file("~/@/StrapProjects");
                    _projects=readLines(_fProjects);
                    _tf=new ChTextField("",COMPLETION_DIRECTORY_PATH).cols(80,true,false).li(this);

                    String lastProject=null;
                    if (_projects!=null) {
                        for(int i=_projects.length; --i>=0;) {
                            final File d=file(FILE_NO_ERROR, _projects[i]);
                            if (!isDir(d)) _projects[i]=null;
                            else lastProject=toStrg(d);
                        }
                    }
                    JComponent panPrevProjects=null;
                    if (lastProject!=null) {
                        _tf.t(lastProject);
                        final JComponent pan=pnl(VBHB,"<h2>Open Project</h2>");
                        for(String p : _projects) {
                            if (p!=null) pan.add(new ChButton(ChButton.MAC_TYPE_ICON,"H").t(fPathUnix(p)).li(instance()));
                        }

                        panPrevProjects=scrllpn(pan);
                    } else {
                        final String
                            base=!Insecure.ARBITRARY_WORKING_DIRECTORY_ALLOWED ? toStrg(dirSettings()) :
                            strchr(' ',userHome)>0 ? "c:\\temp" : userHome;
                        _tf.t(base+File.separatorChar+"testProject");
                    }
                    final String updateJava=
                        isMac() ? null :
                        jv==14 || jv==15 && isSystProprty(IS_LINUX) ? "You use Java version 1."+(jv%10)+" <font color=\"#FF0000\"> Please update! </font>":
                        jv==15 ? "Please consider to updated  Java. Current version: 1."+(jv%10) :
                        null;

                    final JComponent
                        butSearch=new ChButton("SEARCH_HD").li(instance()).t("Browse"),
                        pTF=pnl(CNSEW,_tf,null,null,butSearch," "),
                        pTools=pnl(new ChButton("V").li(instance()).t("Plain text viewer")),
                        testWeb= Web.labelTestConnection(null, "Please check the Web Proxy settings"),
                        pSouth=pnl(VBHB,"#ETB",
                                   " ",
                                   "<h2>Create project folder</h2>",
                                   pTF,
                                   pnl(new ChButton(ACTION_ENTER).li(instance()).t(ChButton.GO)),
                                   " "
                                   ),
                        pSouth2=pnl(VB,
                                    pSouth," ",
                                    updateJava==null?null: pnl(updateJava,  !isMac() ? "http://java.com/download/" : "http://www.apple.com/search/downloads/"),
                                    Web.tfAutoprobeProxy(),
                                    " ",
                                    " ",
                                    pnlTogglOpts("Other tools: ",pTools),
                                    pTools
                                    ),
                        pMain=pnl(CNSEW,panPrevProjects, testWeb, pSouth2,null,"#EBem");
                    ChDelay.parentWindowToFront(pMain,5000);
                    if (prgOptT("-update")) { _newJarParent=pSouth2; startThrd(thrdCR(this, "newJarInit")); }
                    final ChFrame f=_frProjects=new ChFrame("Strap").ad(pMain);
                    final JMenuBar mb=new JMenuBar();
                    mb.add(Box.createHorizontalGlue());
                    mb.add(jMenu(new Object[]{"Web",  buttn(BUT_TEST_PROXY),ChButton.doOpenURL(URL_STRAP).t("Home page")}));
                    mb.add(jMenu(new Object[]{" Help", new ChButton("About").i(IC_INFO).li(instance()), "h", StrapAlign.class, "h", Strap.class,
                                              ChButton.doOpenURL(URL_STRAP+"security.html").t("Security (In Browser)").i(IC_SECURITY),
                                              ChButton.doOpenURL("Adobe_flash_movies*").i(IC_MOVIE)
                            }));
                    f.setJMenuBar(mb);
                    addActLi(instance(), f.shw(ChFrame.PACK|ChFrame.ALWAYS_ON_TOP));
                    for(AbstractButton mi : childsR(mb,AbstractButton.class)) mi.addActionListener(this);
                    _tf.requestFocus();
                }
            }
            for(int i=BUTTN.length; --i>=0;) buttn(i);
            return "";
        }
        if (id=="CACHE_FIX") { /* Fehler in AbstractAligner: Ein buchstabe zuviel bei Section */
            final File dir0=CacheResult.dir(), fDone=new File(dir0,"fixedError1.txt");
            if (sze(fDone)>0) return null;
            for(int a=2; --a>=0;) {
                for(Object cl : oo(StrapPlugins.allClassesV(a==1?SequenceAligner.class:Superimpose3D.class))) {
                    final String cn=delSfx("PROXY",lstCmpnt(toStrg(cl),'.'));
                    if(cn==null) continue;
                    final File dir=new File(dir0,cn);
                    for(String e : lstDir(dir)) {
                        final int dash=e.indexOf('-');
                        if (dash<=16 || !e.endsWith(".entries")) continue;
                        final String f=delSfx(".entries",e);
                        final File
                            eOld=new File(dir,e),
                            fOld=new File(dir,f),
                            eNew=new File(dir,e.substring(dash-16)),
                            fNew=new File(dir,f.substring(dash-16));

                        if (fNew.exists() || eNew.exists()) {
                            delFileOnExit(fOld);
                            delFileOnExit(eOld);
                        } else {
                            //putln("");
                            //putln("mv "+fOld+"   "+fNew);
                            //putln("mv "+eOld+"   "+eNew);
                            renamFile(fOld,fNew);
                            renamFile(eOld,eNew);
                        }
                    }
                }
            }
            wrte(fDone,"OK");
        }
        return null;
    }

    /* <<< run <<< */
    /* ---------------------------------------- */
    /* >>> Open _projects >>> */
    private static void saveProjectDir(File o) {
        if (o==null) return;
        if (_projects!=null) {
            for(int iP=_projects.length; --iP>=0;) {
                if (!isDir(file(toStrgTrim(_projects[iP])))) _projects[iP]=null;
            }
        }
        final List<String> l=new ArrayList();
        l.add(toStrg(o));
        adAllUniq(_projects,l);
        if (!wrte(_fProjects, baTip().join(l))) putln("Caught in Strap.java: unable to  write file strapProjects");
    }

    private static void openProject(File projectDir, CharSequence files, Object[] targets, CharSequence webAlignment, Runnable runFinished,boolean newAct) {
        StrapAlign align=null;
        if (!newAct) {
            if (!_initialized) {
                _initialized=true;
                final String s=sysGetProp("java.vm.name");
                if (strstr(STRSTR_IC,"GNU",s,0,999)>=0 || strstr(STRSTR_IC,"CACAO",s,0,999)>=0 ) {
                    error("java.vm.name="+s+"\n\nStrap may not work properly with this Java version.\nRecommended Java system:\nhttp://java.com/download\n\n");
                }
            }
            if (_frProjects!=null) _frProjects.dispose();
            if (_frBrowse!=null) _frBrowse.dispose();
            Insecure.securitySetAllowedPath(projectDir);
            setWorkingDir(projectDir);
            mkdrsErr(projectDir,"This is a project directory of a Strap project.");
            if (isWeb()) Protein.addOptionsG(Protein.G_OPT_VIEWER);
            align=new StrapAlign();
            ChFrame.setMainFrame(StrapAlign.frame());
            ChFrame.setGeometry(_geometry, StrapAlign.frame());
            Protein.strapFile(Protein.mANNO,null,null);
            Protein.strapFile(Protein.mGAPS,null,null);
            align.initUI();
                                                                                                                        
        } else align=StrapAlign.getInstance();
        if (align!=null) {
            final int opt=_opt|StrapAlign.OPTION_SKIP_PDB_IF_ALREADY_LOADED|(runFinished==null?StrapAlign.OPTION_SCROLL:0);
            StrapAlign.loadTheProteinsInList(opt|StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED|StrapAlign.OPTION_SHOW_ORPHAN_HETEROS_IN_3D, files);
            if (webAlignment!=null) startThrd(thrdRRR(StrapAlign.threadWebAlignment(opt, webAlignment,targets), runFinished));
            else Web.runAfterProbeProxy(0L, runFinished, 8000);
            for(Object o : oo(_vDialogs)) {
                final Class clazz=
                    "L".equals(o)?DialogSubcellularLocalization.class :
                    "B".equals(o)?DialogBlast.class :
                    name2class(fullClassNam(false, toStrg(o),STRAP_PACKAGES));
                if (clazz!=null) {
                    if (StrapPlugins.dialogsForPlugin(clazz).length>0) StrapPlugins.applyPlugin(clazz);
                    else StrapAlign.addDialogC(clazz);
                }
            }
        }
        setFolderIcon(dirSettings(),"superimpose.jpg", "This folder contains program files of Strap.");
    }
    /* <<< Open project <<< */

}

