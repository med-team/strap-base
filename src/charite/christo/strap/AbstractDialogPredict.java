package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
   The user can predict secondary structure or TM helices in selected proteins.
   @author Christoph Gille
*/
public abstract class AbstractDialogPredict extends AbstractDialogJTabbedPane implements ActionListener  {
    private ChCombo _comboClass;

    private final AbstractButton _cbClone=cbox("in new row");
    private Class _dClass, _iClass;
    private String _predictWhat;
    private JTabbedPane _tp;
    private ProteinList _ppl;
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    protected final PredictionFromAminoacidSequence newPredictor(Protein[] pp,Object clazz) {
        final PredictionFromAminoacidSequence predictor=mkInstance(clazz,PredictionFromAminoacidSequence.class,true);
        if (predictor==null)  { error("Could not instantiate <br>"+clazz); return null; }
        if (pp==null) return null;
        final String seq[]=new String[pp.length];
        for(int i=0;i<pp.length; i++) {
            seq[i]=pp[i]==null ? null : toStrg(pp[i].getGappedSequenceExactLength());
        }
        predictor.setGappedSequences(seq);
        return predictor;
    }
    public void init(AbstractDialogPredict dialog, Class interfaceClass, String predictWhat) {
        _dClass=dialog.getClass();
        _iClass=interfaceClass;
        _predictWhat=predictWhat;
        getPanel();
    }
    private ChCombo _comboClass(){
        if (_comboClass==null) _comboClass=SPUtils.classChoice(_iClass).save(_dClass,"PredictionFromAminoacidSequence");
        return _comboClass;
    }
    public JTabbedPane getPanel() {
        if (_tp==null) {
            _ppl=new ProteinList(0L);
            _tp=this;
            final boolean isTM=_iClass==TransmembraneHelix_Predictor.class;
            final Container
                pCenter=pnl(VBHB,
                            pnl(CNSEW,null,pnl(VBHB,"Prediction Method:",_comboClass().panel())),
                            _ppl.scrollPane(),
                            "",
                            pnl(buttn(TOG_CACHE).cb(), " ", new ChButton("GO").t(ChButton.GO).li(this)),
                            " ",
                            isTM?"#JS": null,
                            isTM?pnl(HBL,"In preparation:",StrapAlign.b(CLASS_DialogPredictTmHelicesMulti)) : null
                            ),
                pMain=pnl(CNSEW,pCenter,dialogHead(this), pnl(REMAINING_VSPC1));
            adMainTab(pMain, _tp, getClass());
        }
        return _tp;
    }
    // ----------------------------------------
    private int _resultNum;
    private PredictionFromAminoacidSequence _predictor;
    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="GO") {
            final Protein pp[]=_ppl.selectedOrAllProteins();
            if (pp.length==0) return;
            _predictor=newPredictor(pp,_comboClass());
            final Result r=new Result(_predictor,pp, getClass());
            adTab(DISPOSE_CtrlW, "result "+_resultNum++,r, _tp);
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Class Result >>> */
    protected final class Result extends JPanel implements Disposable, Runnable, ActionListener, ChRunnable {
        private final Thread _thread;
        private final Protein _pp[];
        private boolean _finished, _failed;
        private final PredictionFromAminoacidSequence _predictor;
        private final ChButton
            _lab=labl("Calculating please wait ..."),
            _tog3D=toggl("3D").t("Show in 3D").tt("Show result in 3D protein backbone view").li(this),
            _togSB=toggl(StrapAlign.VIEW_SB).t("Show in alignment").tt("Show result in alignment pane").li(this).li(StrapAlign.li());
        private final UniqueList<BasicResidueSelection> _vSel=new UniqueList(BasicResidueSelection.class);

        public BasicResidueSelection[] selections() { return _vSel.asArray();}
        public Protein[] getProteins() { return _pp;}
        public Class clazz() { return clas(_predictor);}
        public boolean isFailed() { return _failed; }
        public boolean isFinished() { return _finished; }
        public PredictionFromAminoacidSequence predictor() { return _predictor;}
        public ChButton togVisibleSB() { return _togSB;}
        /* <<<  <<< */
        /* ---------------------------------------- */
        /* >>> Constructor  >>> */
        public Result(PredictionFromAminoacidSequence predictor,Protein pp[], Class clazz) {
            _predictor=predictor;
            if (_cbClone.isSelected()) {
                if (pp.length>1) error("The option <pre>"+_cbClone.getText()+"</pre> is only available for one protein");
                else {
                    final Protein p0=pp[0],p=new Protein(StrapAlign.getInstance());
                    p.setTransient(true);
                    p.setResidueType(p0.getResidueType());
                    p.setResidueGap(p0.getResidueGap());
                    p.setFile(null);
                    p.setName(shrtClasNam(_predictor));
                    StrapAlign.addProteins(-1,new Protein[]{p});
                    pp=new Protein[]{p};
                }
            }
            _pp=pp;
            final Object
                help_to_model=
                "The computed predictions are only attached in form of residue selections to proteins.<br>"+
                "Thus, the result of the prediction is not directly part of the protein models ( method  Protein#getResidueSecStrType() )<br>"+
                "<br>"+
                "By clicking this button, the prediction is stored in the protein object as it would be if the protein was loaded from a PDB file. <br>",
                panDetails= clazz!=DialogPredictCoiledCoil.class ? pnl(HBL,new ChButton("TO_MODEL").li(this).t("Set result to protein model"),smallHelpBut(help_to_model)) : null,
                pMain=pnl(VBPNL,
                          pnl(HB,shrtClasNam(predictor),"#",  ChButton.doClose15(DISPOSE_CtrlW,this)),
                          _lab,
                          pnl(ChButton.doCtrl(predictor), _togSB.s(true).cb1(), _tog3D.cb1()),
                          panDetails!=null ? pnlTogglOpts(panDetails) : null,
                          panDetails
                          );
            _tog3D.s(false);
            _togSB.s(false);
            pnl(this,CNSEW,null,pMain);
            startThrd(_thread=new Thread(this,"AbstractDialogPredict"));
        }
        public void dispose() {
            if (_thread!=null && _thread.isAlive()) _thread.stop();
            dispos(_predictor);
            for (BasicResidueSelection s : selections()) {
                final Protein p=s.getProtein();
                if (p!=null) p.removeResidueSelection(s);
            }
            _togSB.s(false);
            _tog3D.s(false);
            run("UPDATE_SELECTIONS",null);
        }
    /* <<< Constructor/dispose <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
        public void actionPerformed(ActionEvent ev) {
            final String cmd=ev.getActionCommand();
            if (cmd=="3D" || cmd=="SB") run("UPDATE_SELECTIONS",null);
            if (cmd=="TO_MODEL") {
                final char prediction[][]=_predictor.getPrediction();
                for (int iP=_pp.length; --iP>=0;) {
                    final Protein p=_pp[iP];
                    byte[] ss=p.getResidueSecStrType();
                    final String KEEP="ADP$$KEFP";
                    if (ss==null || gcp(KEEP,p)!=null) {
                        pcp(KEEP,"",p);
                        ss=new byte[p.countResidues()];
                        final char pred[]=prediction[iP];
                        if (pred==null) continue;
                        for(int i=mini(ss.length, pred.length); --i>=0;) {
                            final int c=pred[i]|32;
                            if (c=='h' || c=='t' || c=='e' || c=='c') ss[i]=(byte)pred[i];
                        }
                        p.setResidueSecStrType(ss);
                    }
                }
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,10);
            }
        }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
        public Object run(String id,Object arg) {
            if (id=="AFTER_COMPUT") {
                final char prediction[][]=(char[][])arg;
                if (sze(_vSel)>0) {
                    _tog3D.s(true);
                    _togSB.s(true);
                    add(new DialogPredictTexshade( prediction, _pp,shrtClasNam(_predictor)).panel(),BorderLayout.CENTER);
                } else {
                    rmFromParent(_tog3D.cb1());
                    rmFromParent(_togSB.cb1());
                }
            }
            if (id=="UPDATE_SELECTIONS" || id=="AFTER_COMPUT") {
                    if (sze(_vSel)>0) {
                        final boolean is3D=_tog3D.s(), isAlign=_togSB.s();
                        final int visible=(is3D ? VisibleIn123.STRUCTURE : 0) | (isAlign ? (VisibleIn123.SEQUENCE | VisibleIn123.SB) : 0);
                        for(BasicResidueSelection s:selections()){
                            final Protein p=s.getProtein();
                            p.removeResidueSelection(s);
                            if(isAlign || is3D) p.addResidueSelection(s);
                            s.setVisibleWhere(visible);
                        }
                        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
                    }
            }
            return null;
        }
        public void run(){
            final long time=System.currentTimeMillis();
            _predictor.compute();
            _finished=true;
            runR(gcp(KEY_RUN_AFTER, this, Runnable.class));
            final char prediction[][]=_predictor.getPrediction();
            _failed=prediction==null;
            boolean success=false;
            for(int iP=_pp.length;--iP>=0;){
                if (iP<sze(prediction) && prediction[iP]!=null) {
                    makeSelection(_pp[iP],prediction[iP]);
                    success=true;
                }
            }
            if (System.currentTimeMillis()-time>2222) {
                ChMsg.speakBG(success ? "Prediction finished" : "Prediction failed", success ? ChMsg.SOUND_SUCCESS : ChMsg.SOUND_FAILURE);
            }
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
            String msg=null;
            if (!success) msg="The method did not produce a result. <br>Chose another one.<br> Press <I>details</I> for more information.";
            else {
                inEdtLaterCR(this,"AFTER_COMPUT",prediction);
                msg="Computation finished: "+
                    (sze(_vSel)>0?"" : "<font color=\"#FF3333\">no</font> "+_predictWhat+" <font color=\"#FF3333\">found</font><br>"+
                     "<sup>If you think that this is due to a temporary error (of a server) <br>"+
                     "and you want to compute it again switch of the cache.</sup>");

                if (sze(_vSel)>0) {
                    add(new DialogPredictTexshade(prediction, _pp,shrtClasNam(_predictor)).panel(),BorderLayout.CENTER);
                    msg+=_predictWhat+" are highlighted in the alignment";
                } else {
                    rmFromParent(_tog3D.cb1());
                    rmFromParent(_togSB.cb1());
                    msg+="<font color=\"#FF3333\">no</font> "+_predictWhat+" <font color=\"#FF3333\">found</font><br>"+
                        "<sup>If you think that this is due to a temporary error (of a server) <br>"+
                        "and you want to compute it again switch of the cache.</sup>";
                }
            }
            _lab.t(msg);
            inEdtLaterCR(this,"UPDATE_SELECTIONS",null);
        }
        /* <<< Threading <<< */
        /* ---------------------------------------- */
        /* >>> ResidueSelection >>> */
        private void makeSelection(Protein p,char[] predi) {
            if (predi==null) return;
            for(char c='0';c<='z';c++) {
                if (!is(LETTR_DIGT,c)) continue;
                final Color col=C(
                                  c=='X' ? 0xFFafAF :
                                  c=='H' || c=='h' ? 0xFF0000 :
                                  c=='E' || c=='e' ? 0xFFff00 :
                                  0xFF00);
                if (col==null) continue;
                int lastIdx=-1;
                for(int i=predi.length;--i>=0;) if (predi[i]==c) { lastIdx=i; break;}
                if (lastIdx<0) continue;
                final boolean bb[]=new boolean[lastIdx+1];
                for(int i=0;i<=lastIdx;i++) bb[i]=(predi[i]==c);
                final Image im=char2image(c);
                if (im==null) continue;
                final BasicResidueSelection s=new BasicResidueSelection(0);
                final String name=(shrtClasNam(_predictor))+'_'+c;
                s.setStyle(VisibleIn123.STYLE_IMAGE_LUCID);
                s.setProtein(p);
                s.setSelectedAminoacids(bb, Protein.firstResIdx(p));
                s.setImage(im);
                s.setColor(col);
                s.setName(name);
                ImageIcon icon=dIIcon(_predictor);
                if (icon==null)
                    icon=iicon(
                               isAssignblFrm(TransmembraneHelix_Predictor.class,_predictor) ? IC_TM_HELIX :
                               isAssignblFrm(CoiledCoil_Predictor.class,_predictor) ? IC_COILEDCOIL :
                               isAssignblFrm(SecondaryStructure_Predictor.class,_predictor) ? IC_HELIX :
                               null);
                TabItemTipIcon.set(null,name,shrtClasNam(_predictor),icon,s);
                _vSel.add(s);
            }
        }

    }
    static Image char2image(char c){
            ImageIcon i=iicon(c=='s' ?  IC_SCISSOR : c=='h' ? "prediction_H|" : "prediction_"+c);
            if (i==null) i=iicon("prediction_"+(char)(c&~32));
            return img(i);
        }
}
