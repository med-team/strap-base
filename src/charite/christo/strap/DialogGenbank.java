package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

The Genbank file format and Embl file format are widely used file
formats for annotated nucleotide sequences (format specifications:
http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html and
ftp://ftp.ncbi.nih.gov/genbank/gbrel.txt and
http://www.ebi.ac.uk/embl/Documentation/User_manual/usrman.html).

After loading a Genbank or Embl file, the nucleotide sequence is initially shown.

Only after translation into amino acids, the amino acid sequence is shown.

The translation is directed by a the CDS-sequence-features within the protein file.

Example:

<pre class="data" style="float: right;">
   CDS     5. .799
   /gene="Psmb5"
   /product="proteasome subunit X"
</pre>

If the Genbank file contains several genes the user can choose one of them.

<br>

<i>SEE_DIALOG:DialogFetchSRS</i>
<i>SEE_DIALOG:EditDna</i>
<span class="clearall"></span>
@author Christoph Gille
*/
public class DialogGenbank extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener,NeedsProteins,Runnable {
    private int _count;
    private Object[] _vProt;
    public DialogGenbank() {
        final Object
            pSouth=pnl(new ChButton("GO").t(ChButton.GO).li(this)),
            box=pnl(CNSEW,"Select proteins in Genbank or EMBL format", dialogHead(this),pSouth);
        adMainTab(box, this,null);
    }

    public void setProteins(Protein... pp) {
        final int n=sze(pp);
        _vProt=new Object[n];
        for(int i=n; --i>=0;) _vProt[i]=wref(pp[i]);
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource(),  data[][]=gcp("D",q,Object[][].class), row[]=gcp("R",q,Object[].class);
        final Protein p= gcp("P",q,Protein.class);
        final ChJTable jt= gcp("T",q,ChJTable.class);
        if (q instanceof ChCombo && row!=null) {
            final int idx=((ChCombo)q).i();
            for(Object o : row) {
                if (o instanceof ChCombo)  ((ChCombo)o).s(idx);
            }
            repaint();
        }
        if (cmd=="GO") {
            final Protein pp[]=StrapAlign.selectedProteins();
            if (pp.length==0) error("No protein selected");
            else {
                setProteins(pp);
                run();
            }
        }
        if (cmd=="K" && p!=null) {
            p.selectCodingStrand(Protein.NO_TRANSLATE);
            StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
        }
        if (cmd=="A" && jt!=null && data!=null) {
            final int ii[]=ChJTable.selectedRowsConverted(jt);
            Protein pp[]=null;
            for(int iRow=data.length; --iRow>=0;) {
                if (ii.length>0 && idxOf(iRow,ii,0,ii.length)<0) continue;
                final Protein prot=(Protein)deref(data[iRow][1]);
                final Object cds=data[iRow][2];
                if (prot!=null) {
                    prot.parseCDS(toStrg(cds));
                    if (pp==null) pp=new Protein[data.length];
                    pp[iRow]=prot;
                }
            }
            StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
        }
    }

    public void run() {
        final Protein pp[]=derefArray(_vProt,Protein.class);
        final Object[][] data=new Object[pp.length][];
        ChButton butReset=null;
        for(int iP=0; iP<pp.length; iP++) {
            final Protein  p=pp[iP];
            final String CDS[]=p.getCDS();
            if (sze(CDS)==0) {
                data[iP]=new Object[]{toStrg(iP+1), wref(p), "Error: No CDS", "", "","", "",""};
                continue;
            }
            butReset=new ChButton(ChButton.ICON_SIZE,"K").li(this).t(null).i(IC_KILL)
                .tt("Discard all reading frame and intron/exon information")
                .cp("P",wref(p))
                .cp(ChRenderer.KEY_EDITOR_COMPONENT,"");

            final Object cc[]=new Object[5];
            for(int col=5; --col>=0;) {
                String ss[]=null, oneVal="";
                for(int i=CDS.length; --i>=0;) {
                    final String cds=CDS[i], s0;
                    if (col==0) s0=cds;
                    else {
                        final String gpps[]=p.geneProductProteinNoteForCDS(cds);
                        s0=gpps!=null && gpps.length>col-1?gpps[col-1]:"";
                    }
                    if (sze(s0)>0) {
                        if (ss==null) ss=new String[CDS.length];
                        ss[i]=s0;
                        if (oneVal=="") oneVal=s0;
                        else oneVal=null;
                    }
                }
                if (sze(oneVal)>0 || ss==null) cc[col]=oneVal;
                else {
                    final Object c=new ChCombo(ss).li(this);
                    cc[col]=c;
                    pcp("R",cc,c);
                    pcp(ChRenderer.KEY_EDITOR_COMPONENT,c,c);
                    pcp(ChRenderer.KEY_RENDERER_COMPONENT,c,c);
                }
            }
            pcp(ChRenderer.KEY_RENDERER_COMPONENT,butReset, iicon(IC_KILL));
            data[iP]=new Object[]{toStrg(iP+1), wref(p), cc[0], cc[1], cc[2], cc[3], cc[4], butReset};
        }
        final ChJTable jt=new ChJTable(new ChTableModel(0L, "#", "Protein", "CDS", "Gene", "Product", "Protein", "Note", "Reset")
                                       .setData(data),ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT);
        jt.setColWidth(false, 0,ChJTable.COLUMN_WIDTH_4_DIGITS);
        jt.setColumnWidthC(false,7, butReset);
        final Object
            butApply=new ChButton("A").t("Apply all or selected rows").li(this).cp("D",data).cp("T",jt),
            pSouth=pnl(butApply),
            tab=pnl(),
            pNorth=pnl(CNSEW,null,null,null,ChButton.doClose15(0,tab)),
            pnl=pnl(CNSEW,scrllpn(jt),pNorth,pSouth);
        remainSpcS(tab,pnl);
        adTab(CLOSE_ALLOWED, toStrg(++_count), tab, this);
    }

}
/*
Problem:
AL935121
FT   CDS             complement(join(104984..105040,105385..105531,
FT                   AL772299.10:2661..2732,AL772299.10:3575..3646,
FT                   AL772299.10:4101..4220,AL772299.10:5400..5462,
FT                   AL772299.10:5872..6063,AL772299.10:9023..9139,
FT                   AL772299.10:10369..10503,AL772299.10:12643..12788,

*/
