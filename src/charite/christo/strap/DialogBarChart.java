package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

   The bar chart visualizes values assigned to single proteins.

   In the bar chart dialog the Java class must be selected that performs the calculation.

   As a simple test  "number of residues in proteins" can be chosen.

   The bar chart is shown within the row header of the alignment window.

   <i>SEE_DIALOG:DialogCompareProteins</i>
   @author Christoph Gille
*/
public class DialogBarChart extends AbstractDialogJPanel implements ActionListener, StrapListener {
    private final ChCombo _classChoice=SPUtils.classChoice2(ValueOfProtein.class, CompareTwoProteins.class, SequenceAlignmentScore2.class).li(this);
    private final ProteinCombo _comboP2=new ProteinCombo(0);
    private Object  _sharedInst;
    private StrapListener _sharedInstLi;
    private final ChButton _togShow=toggl("Show chart").li(this);

    private final Object
        _tfFrom=new ChTextField("  1").cols(6,true,true).li(this).ct(Integer.class),
        _tfTo=new ChTextField("99999").cols(6,true,true).li(this).ct(Integer.class),
        _pnlRef;

    /* ---------------------------------------- */
    /* >>> Instance  >>> */
    public DialogBarChart() {
        final Object panDetails=pnl(HBL,"Alignment position from ",_tfFrom," to ",_tfTo);
        _pnlRef=pnl(VB,   KOPT_HIDE_IF_DISABLED,
                    pnl(HBL,"Reference protein " ,_comboP2.li(this)),
                    pnl(HBL, pnlTogglOpts("Options",panDetails), panDetails)
                    );

        final Object
            pVoP=pnl(HBL,"Computation method ",_classChoice.panel()),
            butTable=new ChButton("T").t("As table").li(this),
            pCenter=pnl(VB,
                        " ",
                        pVoP,
                        _pnlRef,
                        pnl(HBL, _togShow.cb()," ",butTable)
                        );
        pnl(this,CNSEW,pCenter,dialogHead(this), pnl(REMAINING_VSPC1));
        setEnabld(false,_pnlRef);
        myUpdate();
    }

    @Override public void dispose() {
        final StrapView view=StrapAlign.alignmentPanel();
        if (view!=null) view.setBarchart(null,0,0,null);
    }

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Update >>> */
    private void myUpdate() {
        final StrapView view=StrapAlign.alignmentPanel();
        if (view==null) return;
        view.setBarchart(_togShow.s() ? _classChoice : null, atoi(_tfFrom), atoi(_tfTo), _comboP2.getProtein());
        view.rowHeader().repaint();
        StrapEvent.dispatchLater(StrapEvent.VALUE_OF_PROTEIN_CHANGED,1);
    }
    /* <<< Update <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void actionPerformed(ActionEvent ev) {
        StrapAlign.errorMsg("");
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==_classChoice) {
            _sharedInst=_classChoice.instanceOfSelectedClass(null);
            _sharedInstLi=deref(_sharedInst, StrapListener.class);
            setEnabld(!isInstncOf(ValueOfProtein.class, _sharedInst), _pnlRef);
        }
        if (cmd=="T") {
            final BA sb=new BA(999);
            final int fromCol=atoi(_tfFrom), toCol=atoi(_tfTo);
            final Protein p2=_comboP2.getProtein();
            for(Protein p: StrapAlign.visibleProteins()) {
                final Object inst=mkInstance(_classChoice);
                sb.a(p)
                    .a('\t')
                    .a((float)SPUtils.computeValue(0, inst, p, p2, fromCol, toCol))
                    .a('\n');
            }
            shwTxtInW(shrtClasNam(_sharedInst),sb);
        }
        myUpdate();
    }

    public void handleEvent(StrapEvent ev) {
        final StrapListener li=_sharedInstLi;
        if (li!=null && _togShow.s()) li.handleEvent(ev);
    }

}
