package charite.christo.strap.science;
import charite.christo.*;
import charite.christo.strap.*;
import java.net.*;
public class Prione extends AbstractAlignmentProject  {
    public Prione() {
        final String URL_BASE=ChConstants.URL_STRAP+"science/prp/";
        setURLs(
                ChUtils.url(URL_BASE+"prioneAlignments.jar"),
                ChUtils.url(URL_BASE+"prioneFiles.jar")
                );
        run();
    }
}
