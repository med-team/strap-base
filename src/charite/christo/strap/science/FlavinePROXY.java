package charite.christo.strap.science;
import javax.swing.*;
import charite.christo.strap.*;
import charite.christo.*;
/**HELP
<h2> Proteins with Flavine Hetero groups </h2>
(In preparation)
<br>
<br>
All flavine containing  proteins contained in the PDB are
sorted into classes (...<b>.LIST</b>) according to the fold and the location of the flavin molecule.
You can visualize residues adjacent to the flavine molecule: Strap-Menu: <I>analyze/select residues ...</I>
<br><b>XY-Charts (Scatter plots)</b>
<ul>
<li>1d means local sequence similarity</li>
<li>1D means global sequence similarity</li>
<li>3d means local structure similarity</li>
<li>3D means global structure similarity</li>
</ul>
The Scatter plots allow picking. Each dot is a pair of proteins. A menu for the picked pair is available.
<br><br>
Further buttons
<ul>
<li>to load all proteins into the multiple sequence alignment</li>
<li>to load all proteins into one and the same backbone structure viewer</li>
<li>to view the table of data</li>
<li>to superimpose the first two proteins</li>
</ul>
<br>
<b>Two possible ways for detailed 3D-view with side chains:</b>
<ul>
<li>From within a 3D-backbone viewer press the <I>rasmol</I> or <I>jmol</I> button</li>
<li>Load the proteins into the multiple sequence alignment. Use menu  <I>view</I> and item  <I>protein to external view ...</I>   </li>
</ul>
*/
public class FlavinePROXY extends charite.christo.AbstractProxy implements HasControlPanel,HasPanel {
    private static AbstractAlignmentProject instance;
    public static AbstractAlignmentProject getInstance() { return instance;}
    public static void setInstance(AbstractAlignmentProject a) { instance=a;}
    @Override public String getRequiredJars() { return "jfreechart-0.9.18.jar "+jarFile(JCOMMON)+jarFile(LOG4J)+jarFile(COMMONS_COLLECTIONS);}
    //    public FlavinePROXY() { proxyObject(); }
    @Override public Object getPanel(int mode) { return proxyObject();  }

}
