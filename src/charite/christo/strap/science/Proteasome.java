package charite.christo.strap.science;
import charite.christo.strap.*;
import charite.christo.*;
import java.net.*;
/**HELP
<h1>Proteasome</h1>
This project presents the multiple sequence alignment of the proteasome core.
The core structure consists of many homologous subunits.
*/
public class Proteasome extends AbstractAlignmentProject  {
    public Proteasome() {
        final String URL_BASE=ChConstants.URL_STRAP+"science/proteasome/";
        setURLs(
                ChUtils.url(URL_BASE+"proteasomeAlignment.jar"),
                ChUtils.url(URL_BASE+"proteasomeFiles.jar")
               );
        run();
    }
}
