package charite.christo.strap.science;
import charite.christo.*;
import java.util.*;

/*Bortz Seite 415 */
public class SpearmansRangCorrelation {
    private SpearmansRangCorrelation(){}
    public static double calcRoh(double xx[],double yy[]) {
        final int n=xx.length;
        final boolean isNaN[]=new boolean[n];
        for(int i=ChUtils.mini(n,yy.length); --i>=0;)
            isNaN[i] = Double.isNaN(xx[i]) || Double.isNaN(yy[i]);

        final int rxx[]=getRangs(xx,isNaN),ryy[]=getRangs(yy,isNaN);
        long d2=0;
        for(int i=0;i<rxx.length;i++) {
            final int x=rxx[i], y=ryy[i], d=x-y;
            d2+=d*d;
        }

       final double sigma=1.0-6.0*d2/n/(n*n-1);
       return sigma;
    }
    private static int[] getRangs(double dd[],boolean isNaN[]){
        final int n=dd.length, countTrue=ChUtils.countTrue(isNaN), rr[]=new int[n-countTrue];
        double sorted[]=new double[n-countTrue];
        int count=0;
        for(int i=0;i<dd.length && i< isNaN.length;i++)
            if (!isNaN[i]) sorted[count++]=dd[i];
        Arrays.sort(sorted);
        for(int i=0,j=0;i<n;i++) {
            if (!isNaN[i])
                rr[j++]=idxOf(dd[i],sorted);
        }
        return rr;
    }
    private static int idxOf(double o,double oo[]) {
        for(int  i=0;i<oo.length;i++)
            if (o==(oo[i])) return i;
        return -1;
    }
}
