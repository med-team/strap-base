package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

With this dialog a reference protein can be aligned to many other
proteins producing pair-alignments for each protein.

On pressing <i>LABEL:ChButton#BUTTON_GO</i>  the  sequence alignments appear in the preview.

<br><br><b>Inferred into the working alignment</b>
Sequence alignments can be applied to the working sequence alignment.
Only the gaps of the reference protein are changed in the working alignment.
The other proteins are not changed.

<br><br><b>Highlighting of the match:</b>
Table rows can be selected using the <i>ICON:IC_SHOW</i> toggles.
The matches of selected rows are highlighted in the alignment using residue selection objects.

<br><br><b>Limitations:</b>
The induction of gaps from the calculated sequence alignment into the working
sequence  alignment may fail because only the residue positions of the
reference protein but not of the other sequences are changed by
this procedure.

Therefore, it might not find sufficient white space in the multiple sequence
alignment.

<i>SEE_DIALOG:DialogAlign</i>
<i>SEE_DIALOG:DialogSuperimpose3D</i>
@author Christoph Gille
*/
public class DialogAlignOneToAll extends AbstractDialogJTabbedPane implements StrapListener, java.awt.event.ActionListener {

    private final ProteinCombo _comboP=new ProteinCombo(0);
    private final ProteinList _listP=new ProteinList(0L).selectAll(0);
    private final ChCombo _comboC=SPUtils.classChoice(SequenceAligner.class).save(DialogAlign.class,"SequenceAligner");

    public DialogAlignOneToAll() {
        final Object
            pDetails=pnl(VBHB, buttn(TOG_ALI_NOT_COMMUT).cb(), buttn(TOG_CACHE).cb()),
            pWestNorth=pnl(VBHB,
                           pnl(HBL,"Reference protein:", _comboP),
                           pnl(HBL, "Method:", _comboC.panel()),
                           pnl(HBL,pnlTogglOpts(pDetails), "#", new ChButton("GO").t(ChButton.GO).li(this), "#"),
                           pDetails
                           ),
            pWest=pnl(CNSEW,null,pWestNorth,null,null,KOPT_TRACKS_VIEWPORT_WIDTH),
            pCenter=pnl(new GridLayout(1,2),pWest, scrllpn(SCRLLPN_INHERIT_SIZE,_listP)),
            pMain=pnl(CNSEW,pCenter,dialogHead(this),pnl(REMAINING_VSPC1));
        adMainTab(pMain, this, getClass());
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        StrapAlign.errorMsg("");
        final String cmd=ev.getActionCommand();
        if (cmd=="GO") {
            final Protein pp[]=_listP.selectedOrAllProteins();
            if (pp.length==0) { StrapAlign.errorMsg("Select at least one protein");return;}
            if (pp.length==0) return;
            final Object c=new DialogAlignOneToAllResult(pp,_comboP.getProtein(),_comboC);
            adTab(DISPOSE_CtrlW, _comboP+" "+niceShrtClassNam(_comboC), getPnl(c), this);
        }
    }

    public void handleEvent(StrapEvent e) {
        final int t=e.getType();
        if (t==StrapEvent.RESIDUE_SELECTION_CHANGED || t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR || t==StrapEvent.AA_SHADING_CHANGED) {
            ChDelay.repaint(this,333);
        }
    }
}
