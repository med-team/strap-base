# Colors of features for black and white background.
# 4 columns:  feature-name   |  rgb-screen   |  rgb-paper  |  allowedResidues

Initiator_methionine  #198752 #198752 M
Sequence_Features     #3cbc42 #3cbc42 -
Non-terminal_residue  #198752 #198752 X
Propeptide            #9b7057 #9b7057 -
Modified_residue      #50a9a5 #50a9a5 X
Transmembrane         #83a09d #83a09d -
Binding_site          #cbe43d #cbe43d -
Region                #337b80 #337b80 -
Metal_binding         #e0825e #e0825e -
Glycosylation         #b3d4a9 #b3d4a9 NTS
Turn                  #704d4f #704d4f -
Topological_domain    #e2546a #e2546a -
Signal_peptide        #7e53dd #7e53dd -
Peptide               #4b8e6f #4b8e6f -
Compositional_bias    #6fc3da #6fc3da -
DNA_binding           #b77289 #b77289 -
Helix                 #ff7777 #ff7777 -
Active_site           #FF6666 #FF6666 -
Beta_strand           #ffff00 #AAaa00 -
Disulfide_bond        #DDdd00 #DDdd00 C
Cytoplasmic           #FFff00 #FFff00 -
Lumenal               #aaAAaa #aaAAaa -
4-hydroxyproline      #FF00FF #FF00FF P
N-acetylalanine       #FF00FF #FF00FF A
Nitrated_tyrosine     #FF00FF #FF00FF Y
Phosphoserine         #00ff00 #00ff00 TSY
Phosphothreonine      #00ff00 #00ff00 TSY
Phosphotyrosine       #00ff00 #00ff00 TSY
DISEASE               #ff00ff #ff00ff - 
POLYMORPHISM          #00ff00 #00ff00 -
CATALYTIC_SITE        #FF0000 #FF0000 -
catalytic_site        #FF0000 #FF0000 -





