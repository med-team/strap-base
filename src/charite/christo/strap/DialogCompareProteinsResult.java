package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.io.File;
import java.awt.event.*;
import javax.swing.table.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;

/**
   Result panel of DialogCompareProteins
*/
class DialogCompareProteinsResult extends AbstractDialogJPanel implements  Disposable, StrapListener,ProcessEv,TableModel,ChRunnable,TableCellRenderer,IsEnabled {

    private ChJTable _table;

    private double _max=Double.MIN_VALUE, _min=Double.MAX_VALUE;
    private GraphSun _gs;
    private final Object _clas, _pNorth;
    private final Object _sharedInst;
    private final Protein _ppRow[], _ppCol[];
    private final int _fromCol, _toCol;
    private GraphEdge _edges[][];
    private final EvAdapter LI=new EvAdapter(this);
    private final ChCombo _choiceRend=new ChCombo("Colored oval","Symbol","Number").li(LI);

    private final ChSlider
        _sliderH=new ChSlider("sliderHeight",1, 50,16).endLabels("Height",null),
        _sliderS=new ChSlider("sliderScale", 1,100,20).endLabels("Scale",null);
    /* ---------------------------------------- */
    /* >>> Instance >>> */
    DialogCompareProteinsResult(Object clas, Object sharedInstance, Protein ppRow[],Protein ppCol[], int fromColumn, int toColumn) {
        _fromCol=fromColumn;
        _toCol=toColumn;
        _clas=clas;
        _sharedInst=sharedInstance;
        _ppRow=ppRow;
        _ppCol=ppCol;
        TabItemTipIcon.set(null,shrtClasNam(clas),null,null,this);
        final AbstractAlignAndCompare aac=deref(sharedInstance, AbstractAlignAndCompare.class);
        _pNorth=shrtClasNam(clas)+"  "+(aac==null?"":shrtClasNam(clas(aac.comboClass())));
        makeEdges(ppRow,ppCol!=null ? ppCol : ppRow);
        startThrd(thrdCR(this,"COMPUTE"));
        if (ppCol==null) graph();
        else {
            table();
            StrapAlign.addListener(this);
            StrapAlign.addListener(deref(sharedInstance,StrapListener.class));
        }
        pcp(KEY_IMAGE_FILE_NAME,"compareProteins_graph",_gs);
        pcp(KEY_IMAGE_FILE_NAME,"compareProteins_table",_table);
        pcp(KEY_PRINTABLE_COMPONENTS,  new Object[]{_gs,_table}, this);
        addActLi(LI,_sliderH);
        addActLi(LI,_sliderS);
    }
    @Override public void dispose() {

        if (_gs!=null)  _gs.dispose();
        StrapAlign.rmListener(this);
        StrapAlign.rmListener(deref(_sharedInst,StrapListener.class));
        rmFromParent(getPanel());
        _edges=null;
   }
    public Container getPanel() { return this;}
    public boolean isEnabled(Object o) { return !StrapAlign.isBusy();}
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Renderer >>> */
    private final RendererNumberAsOval _renNum=new RendererNumberAsOval(this);
    private final TableCellRenderer _ren=new ChRenderer();//.onlyIcons();
    public Component getTableCellRendererComponent(JTable t,Object o,boolean isSel,boolean foc,int row,int col) {
        if (row==-1) {
            final int c=_table.convertColumnIndexToModel(col);
            return  c==0 ? ChRenderer.blankLabel() : _ppCol[c-1].verticalRendererComponent();
        }
        return (col>0 ? _renNum : _ren).getTableCellRendererComponent(t,o,isSel,foc,row,col);
    }
    /* <<< Renderer <<< */
    /* ---------------------------------------- */
    /* >>> TableModel >>> */
    public String getColumnName(int c) {return c==0 ? "" : _ppCol[c-1].getName();}
    public boolean isCellEditable(int row,int column) {return false;}
    public void setValueAt(Object o,int row,int col) {}
    public Class getColumnClass(int c) {return c==0 ? Protein.class : GraphEdge.class;}
    public int getColumnCount() {return _ppCol.length+1;}
    public int getRowCount() {return _ppRow.length;}
    public Object getValueAt(int row0,int col) {
        try {
            final int row=  _ppRow==_ppCol && row0+1<getColumnCount() ?  _table.convertColumnIndexToModel(row0+1)-1 : row0;
            return  (col==0) ? _ppRow[row] :  _edges[row][col-1];
        } catch(Exception e) { return "error";}
    }
    public void addTableModelListener(javax.swing.event.TableModelListener l){}
    public void removeTableModelListener(javax.swing.event.TableModelListener l){}
        /* <<< TableModel <<< */
    /* ---------------------------------------- */
    /* >>> Table >>> */
    private void table() {
        rtt(_table=new ChJTable(this,ChJTable.ICON_ROW_HEIGHT).renderer(this));
        _table.setColumnSelectionAllowed(true);
        final JTableHeader th=_table.getTableHeader();
        th.setBackground(C(BG_NOT_EDITABLE));
        th.setOpaque(true); // !!!
        th.setDefaultRenderer(this);
        th.setCursor(cursr('M'));
        updateOn(CHANGED_SELECTED_OBJECTS,th);
        updateOn(CHANGED_PROTEIN_AT_CURSOR,th);
        final Object
            pSouth=pnl(FLOWLEFT,
                      ChButton.doSharedCtrl(_sharedInst),
                      _sliderH, _sliderS,
                       " ", _choiceRend," ",new ChButton("T").t("Text").li(LI)),
            pan=pnl(CNSEW, scrllpn(_table),_pNorth,pSouth);
        remainSpcS(this,pan);
        LI.addTo("m",_table);
        LI.addTo("m",th);
    }
     /* <<< TableM <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    private final Runnable _runRepaint=thrdCR(this,"REPAINT");
    public void handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        if (t==StrapEvent.PROTEIN_PROTEIN_DISTANCE_CHANGED
            // || t==StrapEvent.ALIGNMENT_CHANGED && isAssignblFrm(SequenceAlignmentScore2.class, _clas)
            ) {
            ChDelay.runAfterMS(EDT, _runRepaint, 111);
        }
    }
    /* <<< Event  <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    private final double[] _d2={0,0};
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_GET_DISSIMILARITY_VALUE || id==RendererNumberAsOval.TIP) {
            final GraphEdge e=(GraphEdge)arg;
            final GraphNode from=e.from(), to=e.to();
            final Protein p1=(Protein)from.component(), p2=(Protein)to.component();
            final Object data[]=(Object[])e.data(), dist=data[0];

            final double v=
                data[1]==null ?  Double.MIN_VALUE : /* Not yet computed = IC_HOURGLASS */
                SPUtils.computeValue(SPUtils.COMPV_NO_BLOCKING, dist, p1, p2, 0, Integer.MAX_VALUE);
            if (id==ChRunnable.RUN_GET_DISSIMILARITY_VALUE) {
                _d2[0]=v;
                _d2[1]=SPUtils.isDistanceScore(dist) ? 1 : -1;
                return dist==null ? null : _d2;
            }  else return addHtmlTagsAsStrg(new BA(99).a(p1).a("<br>").a(p2).a("<br>").a((float)v));
        }
        if (id=="REPAINT") {
            _renNum.set(_choiceRend.i(),  _sliderS.getValue()*.03/(1E-9+_max-_min), -_min);
            repaintC(_table);
        }
        if (id=="COMPUTE") {
            final GraphEdge eee[][]=_edges;
            long time=System.currentTimeMillis();
            for(int i=0; i<eee.length; i++) {
                for(int j=0; j<eee[i].length; j++) {
                    final GraphEdge e=eee[i][j];
                    final Object data[]=(Object[])e.data(), dis=data[0];
                    if (dis==null) continue;
                    while(StrapAlign.isBusy()) sleep(99);
                    final double v=SPUtils.computeValue(0, dis, (Protein)e.from().component(), (Protein)e.to().component(), _fromCol, _toCol);
                    data[1]="";
                    if (_max<v) _max=v;
                    if (_min>v) _min=v;
                    final long t=System.currentTimeMillis();
                    if (t-time>444) { time=t; if (_table!=null) _table.repaint();}
                }
                sleep(99);
                ChDelay.runAfterMS(EDT, _runRepaint, 111);
            }
        }
        return null;
    }
    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> GraphEdge >>> */

    private void makeEdges(Protein ppRows[], Protein ppCols[]) {
        final int nR=ppRows.length,nC=ppCols.length;
        final GraphNode[] nodeRow=new GraphNode[nR], nodeCol;
        for(int i=0;i<nR;i++) nodeRow[i]=new GraphNode(ppRows[i]);
        if (ppRows==ppCols) nodeCol=nodeRow;
        else {
            nodeCol=new GraphNode[nC];
            for(int i=0;i<nC;i++) nodeCol[i]=new GraphNode(ppCols[i]);
        }
        final GraphEdge ee[][]=new GraphEdge[nR][nC];

        for(int iR=0;iR<nR;iR++) {
            for(int iC=0; iC<nC; iC++) {
                final Object dis=mkInstance(_clas);
                ee[iR][iC]=new GraphEdge(nodeRow[iR],nodeCol[iC], this, new Object[]{dis,null});
            }
        }
        _edges=ee;
    }
    private void graph() {
        final GraphSun gs=_gs=new GraphSun();
        gs.setEnabled(this);
        final GraphEdge eee[][]=_edges;
        for(GraphEdge[] ee:eee) {
            for(GraphEdge e:ee) gs.addEdge(e);
        }
        startThrd(gs);
        makeTranslucent(gs);
        gs.addButton(new ChButton("T").t("Text").li(LI),0);
        gs.addButton(smallHelpBut(_clas),0);
        gs.addButton(ChButton.doSharedCtrl(_sharedInst),0);
        pnl(this,CNSEW, gs,_pNorth);
    }
    /* <<< GraphEdge <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final int id=ev.getID();
        final boolean isPop=isPopupTrggr(false,ev), isPopT=isPopupTrggr(true,ev);
        final ChJTable t=_table;
        final Object th=t==null?null : t.getTableHeader();
        if (q!=null && t!=null && (q==t || q==th) && (id==MOUSE_PRESSED || id==MOUSE_CLICKED)) {
            final int nRow=t.getRowCount(), nCol=t.getColumnCount();
            Protein ppSel[]=null;
            if (isPopT) {
                int count=0;
                for(int rc=2; --rc>=0;) {
                    final int ii[]=rc==1 ? t.getSelectedRows() : t.getSelectedColumns();
                    if (ii==null) continue;
                    for(int i : ii) {
                        final Protein p=deref(get(i-(rc==0?1:0), rc==0?_ppCol:_ppRow), Protein.class);
                        if (p==null) continue;
                        if (ppSel==null) ppSel=new Protein[nRow+nCol];
                        ppSel[count++]=p;
                    }
                }
            }
            final int row=t.rowAtPoint(point(ev)), col=t.columnAtPoint(point(ev));
            final Object v=q==th?get(col-1, _ppCol):getValueAt(row, col);
            final Protein p=deref(v, Protein.class);
            if (isPop && p!=null) {
                if (isPopT) ContextObjects.openMenu('P',oo(p), ppSel);
            } else if (id==MOUSE_CLICKED || id==MOUSE_RELEASED || id==MOUSE_PRESSED ) {
                final GraphEdge edge=deref(v, GraphEdge.class);
                final Object dist=edge==null?null : get(0,edge.data());
                if (dist!=null) {
                    final Protein p0=(Protein)edge.from().component();
                    final Protein p1=(Protein)edge.to().component();
                    if (isPop) {
                        if (isPopT) shwCtrlPnl(ChFrame.AT_CLICK|ChFrame.DISPOSE_ON_CLOSE, p0+"  "+p1, dist);
                    } else if (id==MOUSE_CLICKED) {
                        final Object inst=dist instanceof AbstractAlignAndCompare ? ((AbstractAlignAndCompare)dist).getCalculator() : _sharedInst;
                        SPUtils.viewComparison(p0, p1, inst);
                    }
                }
            }
        }
        if (id==ActionEvent.ACTION_PERFORMED) {
            final String cmd=actionCommand(ev);
            if (q==_choiceRend) _sliderS.setEnabled(_choiceRend.getSelectedIndex()<2);
            if (q==_sliderH) { t.setRowHeight(_sliderH.getValue()); revalAndRepaintC(t);}
            if (q==_sliderS || q==_choiceRend) {
                if (q==_sliderS) revalAndRepaintC(t);
                ChDelay.runAfterMS(EDT, _runRepaint, 111);
            }
            if (cmd=="T") {
                final Protein[] ppR=_ppRow, ppC=_ppCol!=null ? _ppCol:ppR;
                final BA sb=new BA(ppR.length*ppC.length*100).join(ppC,"\t").a('\n');
                int iRow=0;
                for(Protein pR:ppR) {
                    sb.a(pR).a('\t');
                    for(int iCol=0;iCol<ppC.length;iCol++) sb.a((float)_edges[iRow][iCol].len()).a('\t');
                    sb.a('\n');
                    iRow++;
                }
                sb.a('\n',2);
                iRow=0; for(Protein pR:ppR) {
                    int iCol=0;
                    for(Protein pC:ppC) {
                        sb.a((float)_edges[iRow][iCol++].len()).a('\t').a(pR).a('\t').aln(pC);
                    }
                    iRow++;
                }
                final File f=file(STRAPOUT+"/Result_CompareSomeSequences.txt");
                wrte(f,sb);
                new ChTextView(f).tools().showInFrame("Compare proteins");
            }
        }
    }
}
