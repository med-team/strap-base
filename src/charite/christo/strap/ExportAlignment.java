package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.Color;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.HtmlUtil.*;
/**
@author Christoph Gille
bugzilla: http://www.openoffice.org/issues/show_bug.cgi?id=99259
*/
public class ExportAlignment implements AlignmentWriter, Comparator {
    private String _names[], _sfx, _icons[];
    private Protein _pp[];
    private int _fstCol=0, _toCol=MAX_INT, _resPerLine=50;
    private long _alignmentID;
    private Object _pSS;
    private boolean _has3d;
    public Protein[] getProteins() { return _pp;}
    public String[] getNames(){ return _names;}
    public void setProteins(Protein... pp) { _pp=pp;}
    public void setNames(String[] names) { _names=names;}
    public void setColumnRange(int first, int last){ _fstCol=first; _toCol=last; }
    public void setResiduesPerLine(int n) { _resPerLine=n;}
    public void setCharForGapAndTerminalBlanks(char gap, char blankN, char blankC) { }
    public String getFileExtension() { return _sfx;}

    private Map _map;
    public final static String PROPERTY_SCRIPT="EA$$KS", PROPERTY_IDENT_THRESHOLD="EA$$IT", PROPERTY_COLORING="EA$$C";
    public void setProperty(String id, Object value) { (_map==null?_map=new HashMap():_map).put(id,value); }

    public void getText(long opts,BA sb) {
        final String
            HTML_BASE=URL_STRAP+"toHTML/",
            JS_SCRIPT="<script language=\"JavaScript\" type=\"text/javascript\" >",
            JS_SCRIPT_SRC=delSfx('>',JS_SCRIPT)+" src=\"",
            LINK_TO_STRAP="<a name=\"l2s\" href=\""+URL_STRAP+"\" onmouseover=\"tip(this,'This alignment view has been created with Strap');\"><img alt=\"*\" style=\"height: 0.7em;\" height=\"7\" border=\"0\" src=\""+URL_STRAP+"images/sun_java.png\"/></a>";
        if ( (opts&FASTA)!=0) {
            exportFasta(opts,sb);
            return;
        }
        final long alignmentID=_alignmentID=System.currentTimeMillis();
        _sfx= (opts&CLUSTALW)!=0 ? "clustalw" : (opts&MSF)!=0 ? "msf" : (opts&FASTA)!=0 ? "fasta" : "html";
        final Protein pp[]=_pp;
        final int width=_resPerLine;
        final String names[]=getNames();
        final boolean
            html=    (opts&HTML)!=0,
            dna=     (opts&NUCLEOTIDE_TRIPLET)!=0,
            browser= (opts&CSS_BROWSER)!=0;
        final int wide2narrow[]=new Gaps2Columns().computeWide2narrow(pp), cols[][]=new int[pp.length][], cols2idx[][]=new int[pp.length][];
        final Range columnRange=SPUtils.colsToLessProteins(pp, _fstCol, _toCol);
        if (columnRange==null) return;
        final String[] pNames=names!=null ? names: new String[pp.length];
        int maxCol=0, maxIdx=0;
        for(int iP=pp.length; --iP>=0;) {
            final Protein p=pp[iP];
            final int nRes=p.countResidues();
            maxIdx=maxi(maxIdx,nRes+Protein.firstResIdx(p));
            cols[iP]=Gaps2Columns.computeColumns(wide2narrow,p.getResidueGap(),nRes, null);
            cols2idx[iP]=col2nextIdx(cols[iP],nRes);
            maxCol=maxi(maxCol, get(nRes-1, cols[iP]));
            if (pNames[iP]==null) {
                final String na=nam(p);
                pNames[iP]=(opts&FILE_SUFFIX)==0 ? delLstCmpnt(na,'.') : na;
                final String im=html && (0!=(opts&PROTEIN_ICONS)) && 0!=(opts&PROTEIN_ICONS) ? p.getIconUrl() : null;
                if (im!=null) (_icons==null?_icons=new String[pp.length]:_icons)[iP]=im;
            }

        }
        int threshold=atoi(gcp(PROPERTY_IDENT_THRESHOLD,_map));
        if (threshold<=0) threshold=50;
        final String coloring=orS((String)gcp(PROPERTY_COLORING, _map),"charge");

        final int nameL=sze(longestName(pNames)), numL=stringSizeOfInt(maxIdx), leadingSpc=nameL+numL+3;
        maxCol=mini(maxCol,columnRange.to()-1);
        final int maxFeaturesPos=ResSelUtils.layoutFeatures(pp);
        final HtmlUtil state=html?new HtmlUtil() : null;
        long linesWithFeature=0;
        final BA sbTip=html && 0!=(opts&BALLOON_MESSAGE) ?new BA(99):null;
        if (html) {
            if ((opts&HTML_HEAD_BODY)!=0) {
                sb
                    .aln("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html>\n<head>")
                    .aln("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">")
                    .aln("<style type=\"text/css\">").join(custSettings(Customize.cssAlignBrowser)).aln("\n</style>");
                if (true || !myComputer()) sb.a(JS_SCRIPT_SRC).a(HTML_BASE).a("toHTML001.js\"></script>");
                else sb.a(JS_SCRIPT_SRC).aln("toHTML001.js\"></script>");
                sb.aln("<title>alignment</title>\n</head>\n<body>");
                if (browser) {
                    final BA script=(BA)gcp(PROPERTY_SCRIPT,_map);
                    sb.aln("<div class=\"strap_alignment\" id=\"strap_alignment_"+alignmentID+"\">");
                    sb.aln(JS_SCRIPT);
                    setProtData(sb);

                    linesWithFeature=makeTips(sb);
                    sb.a("pcp('NUM_PROTEINS',").a(pp.length).a(",findAlignment(").a(_alignmentID).aln("));");
                    sb.aln("</script>");
                    if (browser && sze(script)>0) {
                        sb.a("<pre style=\"display: none;\" class=\"script_").a(alignmentID).a("\" id=\"script_").a(alignmentID).aln("\">")
                            .a(StrapScriptInterpreter.SCRIPT_set_characters_per_line).aln(_resPerLine)
                            .a(StrapScriptInterpreter.SCRIPT_set_color_mode).aln(coloring)
                            .a(StrapScriptInterpreter.SCRIPT_set_conservation_threshold).aln(threshold)
                            .filter(FILTER_HTML_ENCODE,script).aln("</pre>");
                    }

                    final BA controls=readBytes(rscAsStream(0L, _CCP, "toHTML.html"));

                    sb.aln(controls.replace(0,"toHTML3D.html",_has3d ? readBytes(rscAsStream(0L, _CCP,"toHTML3D.html")):"").replace(0,"ALID",toStrg(alignmentID)).replace(0,"<script>",JS_SCRIPT));
                }
            }
            sb.a("\n<div id=\"alignment_").a(alignmentID).aln("\">");
        }
        else if ((opts&CLUSTALW)!=0) sb.aln("CLUSTAL W 2.1 multiple sequence alignment\n");
        else if ((opts&MSF)!=0) {
            sb.aln("!!AA_MULTIPLE_ALIGNMENT 1.0\n\n test.msf  MSF: 224  Type: P  September  9, 2009 09:09  Check: 9999 ..\n");
            for(int i=0;i<pp.length;i++) sb.a(" Name: ").a(pNames[i]).a(" Len:").a(pp[i].countResidues()).aln(" Check: 1 Weight:1");
            sb.aln("\n//\n");
        }
        final byte[] TRIPLET=new byte[3];
        final char c13[]=new char[dna ? 3:1];
        final Color shadingColors[]=!html?null: ShadingAA.colors(coloring, true);
        final short[][] frequencies=!html?null: freqAtCol(pp,cols, columnRange.to()+1);

        for(int h=columnRange.from(); h<=maxCol; h+=width) {
            if (html) {
                final Protein pSS=deref(_pSS,Protein.class);
                final int i=pSS==null?-1:idxOf(pSS,pp);
                if (i>=0) showSS(pSS, h, mini(h+width,maxCol+1), leadingSpc, cols[i], cols2idx[i], sb);
                sb.aln("<div class=\"alignment_block\">");
            }

            for(int iP=0;iP<pp.length;iP++) {
                boolean addedLink=false;
                if (html) {
                    sb.a("<div class=\"alignment_row").a(iP).aln("\">").a("<pre class=\"seq\">");
                    if (_icons!=null) img(opts,get(iP, _icons),sb);
                }
                final Protein p=pp[iP];
                final byte
                    selected[]=html && (opts&UNDERLINE_ANNOTATIONS)!=0 ? p.selAminos() : null,
                    resType[]=p.getResidueType(),
                    resSecStr[]=(opts&SECONDARY_STRUCTURE)!=0 ? p.getResidueSecStrType() : null;

                final boolean hasNt=p.getNucleotides()!=null;
                final int
                    nRes=p.countResidues(),
                    column2nextIdx[]=cols2idx[iP],
                    resCol[]=cols[iP],
                    firstCol=resCol.length>0 ? resCol[0] : 0,
                    toColumn=nRes>0 ? resCol[nRes-1] : 0;

                final String acc=browser ? p.getAccessionID() : null;
                if (html) {
                    final Object tip=_mapTT.get(p);
                    if (tip!=null) sb.a("<span onmouseover=\"tip(this,").a(tip).a(");\">").a(pNames[iP]).a("</span>");
                    else sb.a(pNames[iP]);
                    sb.and("<a class=\"proteinLink\" href=\"javascript:shw('", acc,"');\">*</a>").a(' ', 1+nameL-sze(pNames[iP])+(acc!=null?0:1));
                    int num=MIN_INT;
                    if (h+width>=firstCol && h<=toColumn && h>=0 && h<column2nextIdx.length) {
                        final int idx=column2nextIdx[h], nn[]=p.getResidueNumber();
                        num= (opts&PDB_RESIDUE_NUMBER)!=0 && nn!=null ? get(idx,nn) : idx+1;
                    }
                    if (num==MIN_INT) sb.a(' ',numL+1); else sb.a(num+Protein.firstResIdx(p), numL).a(' ');
                    sb.a("<span class=\"sequence\">");
                } else sb.a(pNames[iP]).a(' ',nameL+1-sze(pNames[iP]));
                for(int hh=h; hh<h+width && hh<=maxCol; hh++) {
                    final int iAA= get(hh,column2nextIdx);
                    if (iAA>=0 && iAA<nRes && hh==resCol[iAA]) { // is no gap
                        if (dna && hasNt) {
                            final byte trip[]=p.getResidueTripletZ(iAA,TRIPLET);
                            if (trip!=null) {
                                c13[0]=(char)(trip[0]&~32);
                                c13[1]=(char)(trip[1]&~32);
                                c13[2]=(char)(trip[2]|32);
                            } else c13[0]=c13[1]=c13[2]='?';
                        } else {
                            c13[0]=(char)resType[iAA];
                            if (!is(LETTR,c13[0])) c13[0]='X';
                            if (dna) c13[1]=c13[2]='~';
                        }
                        if (html && iAA>=0 && nRes>iAA) {
                            boolean isCons=true;
                            final int c=(resType[iAA]|32)-'a';
                            final short ff[]=get(hh, frequencies, short[].class);
                            if (ff!=null && c>=0 && c<='z'-'a') {
                                final int total=ff[FrequenciesAA.TOTAL];
                                isCons=threshold>=0 ? ff[c]>=threshold*total/100 : ff[c]<=total+threshold*total/100;
                            }
                            final byte secStr=get(iAA,resSecStr);
                            int bg=secStr=='H' ? 0xFFCCBB : secStr=='E' ? 0xFFFF99 : -1;
                            final int rgb=isCons ? rgb(shadingColors[resType[iAA]]) : 0;
                            String url=null;
                            if (sbTip!=null) sbTip.clr();

                            if ((opts&UNDERLINE_ANNOTATIONS)!=0 && get(iAA,selected)!=0) {
                                int countTip=0;
                                final ResidueSelection ss[]=p.residueSelectionsAt(0L,iAA,iAA+1,VisibleIn123.HTML);

                                if (ss.length>0) {
                                    _compareSS=ss.clone();
                                    Arrays.sort(ss,this);
                                    _compareSS=null;
                                }
                                for(ResidueSelection s : ss) {
                                    if (!ResSelUtils.isSelVisible(0,s)) continue;
                                    if ((ResSelUtils.style(s)&255)==VisibleIn123.STYLE_UNDERLINE) continue;
                                    if (sbTip!=null) {
                                        Object tip=_mapTT.get(s);
                                        if (tip!=null) { sbTip.a(' ').a(tip); countTip++; }

                                    }
                                    final Color color=colr(s);
                                    if (color!=null) {
                                        bg=rgb(color);
                                        url=orS(ResSelUtils.value(ResidueAnnotation.HYPERREFS,s),url);
                                    }
                                }
                            if (countTip>0) sbTip.a('\'').bytes()[0]='\'';
                            }
                            state.setStyles(isCons?B:0, rgb, bg, url, "", toStrg(sbTip), sb);
                        }
                        sb.a(c13);
                        if (html && iP==pp.length-1 && iAA==p.countResidues()-1) {
                            addedLink=true;
                            sb.a(LINK_TO_STRAP);
                        }
                        if((opts&EXTRA_SPACE)!=0 && !html) sb.a(' ');
                    } else {
                        for(int j=dna ? 3 : 1; --j>=0;) {
                            sb.a(html&&(hh<firstCol||hh>toColumn) ? ' ' :  (opts&CLUSTALW)!=0  ? '-' : '.');
                            if((opts&EXTRA_SPACE)!=0 && !html) sb.a(' ');
                        }
                    }
                }// hh
                if (html) {
                    state.reset(sb);
                    sb.a("</span> <!-- sequence -->");
                    final int colFrom=mini(h+width,maxCol+1);
                    if ((opts&UNDERLINE_ANNOTATIONS)!=0) {
                        showFeatures(p,h, colFrom, leadingSpc, state, maxFeaturesPos, resCol, column2nextIdx, null);
                        tellFeatures(state, sb.a(' ', addedLink?0:1));
                    }
                    state.reset(sb).aln("&nbsp;</pre>");
                    if ((opts&UNDERLINE_ANNOTATIONS)!=0) showFeatures(p,h, colFrom, leadingSpc, state, maxFeaturesPos, resCol, column2nextIdx,  sb);
                    sb.aln("</div> <!-- alignment_row -->");
                }
                sb.a('\n');
            }//iP
            if (html) {
                    {
                        sb.a("<pre class=\"seq\">");
                        if (_icons!=null) img(0L,null,sb);
                        sb.a(' ',leadingSpc).a("<span class=\"matches\"></span></pre>");
                    }

                sb.aln("\n</div> <!-- alignment_block -->");
            }
            sb.a('\n');
        }//h
        if (html) {
            sb.aln("</div><!-- seq_ali_begin_ -->\n</div><!-- strap_alignment -->\n");
            state.reset(sb);
            final int nF=sze(_vFeaturesAll);
            if (nF>0) {
                final List<Strap> vFN=new ArrayList();
                if (browser) sb.a(JS_SCRIPT).a("writeToggleDiv('LEGEND_").a(alignmentID).a("');</script>Legend");
                sb.a("<div id=\"LEGEND_").a(alignmentID).a("\" style=\"visibility:hidden;\">\n<table summary=\"sequence feature\">");
                for(int i=0; i<nF; i++) {
                    final ResidueAnnotation a=(ResidueAnnotation)get(i,_vFeaturesAll);
                    final String fn=a.featureName();
                    if (fn!=null && adUniq(fn,vFN)) {
                        sb.a("<tr><td>").colorBar(rgb(a.fgForWhiteBG())).a("</td><th>").a(toShortFeatureName(fn)).a("</th><td>").a(fn).aln("</td></tr>");
                    }
                }
                sb.aln("</table></div>");
            }
            state.reset(sb);
            sb.aln(JS_SCRIPT);
            writePcpProtData(sb);
            sb.a("afterLoadingAlignment(").a(alignmentID).aln(");");
            sb.aln("</script>");
            if (0!=(opts&HTML_HEAD_BODY)) sb.aln("</body>\n</html>");
        }
    }
    private Collection<String> _setTF;
    private Collection<ResidueAnnotation> _vFeaturesAll, _vFeaturesLine;
    private void tellFeatures(HtmlUtil state,  BA sb) {
        if (_setTF==null) _setTF=new HashSet(); else clr(_setTF);
        state.reset(sb);
        int count=0;
        for(int i=0; i<sze(_vFeaturesLine); i++) {
            final ResidueAnnotation a=(ResidueAnnotation)get(i,_vFeaturesLine);
            final String fn=a==null ? null : a.featureName();
            if (fn!=null && _setTF.add(fn)) {
                sb.a(count++==0?'(':',');
                state.setStyles(0, rgb(a.fgForWhiteBG()), -1,"","",null,sb);
                sb.a("<span onmouseover=\"tip(this,'").a(fn).a("')\">").a(toShortFeatureName(fn)).a("</span>");
            }
        }
        state.reset(sb);
        if (count>0) sb.a(')');
    }

    public void setRulerSecStru(Protein p) { _pSS=wref(p); }
    private void showSS(Protein p, int hFrom, int hTo, int leadingSpc,  int resCol[], int c2i[], BA sb) {
        final byte ss[]=p.getResidueSecStrType();
        final int nRes=mini(sze(ss), p.countResidues());
        final int maxCol=mini( get(nRes-1,resCol), c2i.length-1, hTo-1);
        if (nRes==0) return;
        int prevC=' ';
        sb.a("<pre class=\"secStru\">");
        if (_icons!=null) img(0L,null,sb);
        sb.a(' ',leadingSpc);
        for(int h=hFrom; h<=maxCol; h++) {
            final int iA=c2i[h];
            final int c0=iA<0 || iA>nRes || h!=resCol[iA] ? ' ' : ss[iA];
            int c=32|c0;
            if (c!='h' && c!='e') c=' ';
            if (c==prevC) sb.a((char)c);
            else {
                if (prevC!=' ') sb.a("</span>");
                if (c!=' ') sb.a("<span class=\"").a((char)c).a("CONSENSUS").a("\" onmouseover=\"tip(this,'").a(c=='h'?"helix (":"extended, beta sheet (").a(p).a(")');\" >").a((char)c0);
                else sb.a(' ');
            }
            prevC=c;
        }
        sb.a(prevC==' '?null:"</span>").aln("</pre>");
   }

    private BA _sbTmp, _sbTip;
    private void showFeatures(Protein p, int hFrom, int hTo, int leadingSpc, HtmlUtil state, int maxFeaturesPos, int resCol[], int c2i[], BA sb) {
        final int nRes=p.countResidues();
        final int maxCol=mini( get(nRes-1,resCol), c2i.length-1, hTo-1);
        final ResidueSelection aa[]=p.allResidueSelections();
        final byte bb[]=p.selAminos();
        if (_sbTip==null) _sbTip=new BA(99); else _sbTip.clr();
        if (_sbTmp==null) _sbTmp=new BA(99); else _sbTmp.clr();
        clr(_vFeaturesLine);
        int count=0;
        for(int iF=0; iF<10; iF++) {
            boolean hasFeature=false;
            for(ResidueSelection a:aa) {
                final int style=ResSelUtils.style(a);
                if ((style&255)!=VisibleIn123.STYLE_UNDERLINE || !ResSelUtils.isSelVisible(VisibleIn123.HTML,a)) continue;
                if (iF==((style>>VisibleIn123.BIT_SHIFT_LINE) & 0xFF)) { hasFeature=true; break;}
            }
            if (!hasFeature) continue;
            if (sb!=null) {
                sb.a("<pre class=\"ftr\">");
                if (_icons!=null) img(0L,null,sb);
            }
            count++;
            int space=leadingSpc;
            nextCol:
            for(int h=hFrom; h<=maxCol; h++) {
                final int iA=c2i[h];
                if (iA>=bb.length) break;
                if (iA<0 || iA>nRes || h!=resCol[iA] || bb[iA]==0) { space++; continue;}
                for(ResidueSelection s:aa) {
                    final int style=ResSelUtils.style(s);
                    if ((style&255)!=VisibleIn123.STYLE_UNDERLINE || !ResSelUtils.isSelVisible(VisibleIn123.HTML,s)) continue;
                    if (!get(iA-s.getSelectedAminoacidsOffset()+Protein.firstResIdx(p), s.getSelectedAminoacids())) continue;
                    if (iF!=((style>>VisibleIn123.BIT_SHIFT_LINE) & 0xFF)) continue;
                    final ResidueAnnotation a=deref(s,ResidueAnnotation.class);
                    if (sb==null) {
                        if (ResSelUtils.type(a)=='F') {
                            _vFeaturesLine=adUniqNew(a,_vFeaturesLine);
                            _vFeaturesAll=adUniqNew(a,_vFeaturesAll);
                        }
                        continue;
                    }
                    if (space>0) {
                        state.setStyles(0,0xFFffFF,0xFFffFF,"","",null,sb);
                        sb.a(' ', space);
                        space=0;
                    }
                    final String url=a==null?null: orS(a.lastValue("DAS"), a.lastValue(ResidueAnnotation.HYPERREFS));
                    final int color0=0xFFffFF & rgb(a!=null ? a.fgForWhiteBG():colr(s));
                    final int color=color0==0xffFFff ? 0xddDDdd : color0;
                    _sbTmp.clr().a(" class=\"ftr_").aHex(color, 6).a("\"").a(" style=\" background-color: #").aHex(color, 6).a(";\" ");
                    if (url!=null) {
                        final boolean js=!url.startsWith("javascript");
                        _sbTmp.a(" onclick=\"").a(!js?null:"javascript:shw('").a(url).a(!js?null:"');").a("\" ");
                    }
                    state.setStyles(0, color, 0xffffff, null, _sbTmp.toString(), toStrg(_mapTT.get(s)) , sb);
                    sb.a('-');
                    continue nextCol;
                }
                space++;
            }
            if (sb!=null) state.reset(sb).a("</pre>");
        }
        if (sb!=null) {
            for(;count<maxFeaturesPos; count++) sb.a("<pre class=\"ftr\"> &nbsp; </pre>");
        }
    }
    private static int[] col2nextIdx(int cc[], int nRes) {
        if (nRes==0) return new int[0];
        final int nI=cc[nRes-1];
        final int ii[]= new int[nI+1];
        Arrays.fill(ii,0,nI+1,-1);
        for(int iA=0;iA<nRes;iA++) {
            final int c=cc[iA];
            if (c>=0 && c<ii.length) ii[c]=iA;
        }
        int last=-1;
        for(int i=nI;i>=0;i--)
            if (ii[i]<0) ii[i]=last;
            else last=ii[i];
        return ii;
    }

    private static String toShortFeatureName(String fn) {
        if (sze(fn)<4) return fn;
        final int space=fn.indexOf(' ');
        return space<0 || space==sze(fn)-1 ? fn.substring(0,1) :  fn.substring(0,1)+fn.substring(space+1,space+2);
    }

    public static void img(long opt, String url, BA sb) {
        sb.a("<img width=\"10\" ").a(url==null||0!=(opt&CSS_BROWSER)?null:"height=\"12\" ")
            .a("class=\"proteinIcon").a(url==null?"Null":null).a("\" alt=\" \" src=\"").or(url, URL_STRAP+"images/blank32x1.gif").a("\"/>");
    }
    // <spacer style=\"width: 10em\">
    private static short[][] freqAtCol(Protein[] pp, int[][] columns, int toCol) {
        final short[][] fff=new short[toCol]['Z'-'A'+2];
        for(int iP=pp.length; --iP>=0;) {
            final Protein p=pp[iP];
            final int cols[]=columns[iP];
            final byte rt[]=p.getResidueType();
            for(int iA=mini(cols.length, p.countResidues()); --iA>=0; ) {
                final int c=cols[iA];
                if (c>=toCol) continue;
                final int a=rt[iA]|32;
                if ('a'<=a && a<='z') {
                    fff[c][a-'a']++;
                    fff[c][FrequenciesAA.TOTAL]++;
                }
            }
        }
        return fff;
    }

    private void exportFasta(long opts, BA sb){
        _sfx="fasta";
        final Protein pp[]=_pp;
        for(int iP=0;iP<pp.length;iP++) {
            final Protein  p=pp[iP];
            final String s=_names!=null && _names[iP]!=null ? _names[iP] : nam(p);
            if (sb!=null) sb.a('>').aln(s);
            final byte[] res=p.getGappedSequenceExactLength();
            if (sze(res)==0) continue;
            final int from=_fstCol, to=mini( res.length-1, _toCol)+1;
            int count=0;
            for(int i=from; i<to;i++) {
                final byte c=res[i];
                final boolean let=is(LETTR,c);
                if ( (opts&NO_GAPS)!=0 && let) continue;
                sb.a(let?(char)c :'-');
                if (++count % _resPerLine ==0 && i!=to-1) sb.a('\n');
            }
            sb.a('\n');
        }
    }

    private ResidueSelection _compareSS[];
    private int _compareA;
    public int compare(Object o1, Object o2) { /* Die breiten nach oben */
        if (o1==o2) return 0;
        if (o1==null) return 1;
        if (o2==null) return -1;
        final int off1=((ResidueSelection)o1).getSelectedAminoacidsOffset();
        final int off2=((ResidueSelection)o2).getSelectedAminoacidsOffset();
        final boolean bb1[]=((ResidueSelection)o1).getSelectedAminoacids();
        final boolean bb2[]=((ResidueSelection)o2).getSelectedAminoacids();
        final int iA=_compareA;
        final int score1=(get(iA-1-off1, bb1)?1:0)+(get(iA+1-off1, bb1)?1:0);
        final int score2=(get(iA-1-off2, bb2)?1:0)+(get(iA+1-off2, bb2)?1:0);
        return (score1==score2 ?  idxOf(o1,_compareSS)-idxOf(o2,_compareSS) : score2-score1);

    }

    final Map _mapTT=new HashMap();
    public long makeTips(BA sb) {
        long linesWithFeature=0;
        int iBalloon=(int)(System.currentTimeMillis()%100000);
        final BA sbTmp=new BA(999);
        for(Protein p : _pp) {
            final BA tip=toBA(p.run(ChRunnable.RUN_GET_TIP_TEXT,null));
            if (tip!=null) {
                _mapTT.put(p,intObjct(iBalloon));
                sb.a("_TT[").a(iBalloon++).a("]='").filter(FILTER_QUOTE_TO_HTML|FILTER_LT_GT_4TT,0,tip.replaceChar('\n',' ')).aln("';");
            }
            for(ResidueSelection s:p.allResidueSelections()) {
                if (!ResSelUtils.isSelVisible(VisibleIn123.HTML,s)) continue;

                final ResidueAnnotation a=deref(s,ResidueAnnotation.class);
                sbTmp.clr().colorBar(rgb(a!=null?a.fgForWhiteBG() : colr(s)));
                sbTmp.filter(FILTER_HTML_ENCODE, orS(a==null?null:a.featureName(), nam(s))).a("<br>");
                if (a!=null) {
                    sbTmp.and("<font size=\"1\">On click: ", a.value(ResidueAnnotation.HYPERREFS),"</font>");
                    for(ResidueAnnotation.Entry e : a.entries()) {
                        if (e.key()==ResidueAnnotation.BALLOON) sbTmp.a(sze(sbTmp)==0?null:"<br>").filter(FILTER_HTML_ENCODE, e.value());
                    }
                }
                if (sbTmp.replaceChar('\n',' ').end()>0) {
                    _mapTT.put(s,intObjct(iBalloon));
                    sb.a("_TT[").a(iBalloon++).a("]='").filter(FILTER_QUOTE_TO_HTML|FILTER_LT_GT_4TT,0,sbTmp).aln("';");
                }
                final int style=ResSelUtils.style(s);
                if ((style&255)!=VisibleIn123.STYLE_UNDERLINE) continue;
                final int line=(style>>VisibleIn123.BIT_SHIFT_LINE)&255;
                linesWithFeature |= (1<<line);
            }
        }
        return linesWithFeature;
    }
    public void setProtData(BA sb) {
        for(int iP=0;iP<_pp.length;iP++) {
            Protein p=_pp[iP];
            sb.a("_pName[").a(iP).a("]='").a(p).a("';");
            {
                sb.a("_rowHeaders[").a(iP).a("]='");
                if (_icons!=null) img(0,get(iP, _icons),sb);
                sb.a(p).aln("';");
            }
            final String pdb=p.getPdbID();
            if (pdb!=null && p.getResidueCalpha()!=null) {
                _has3d=true;
                sb.a("_pdbID[").a(iP).a("]='").a(pdb).aln("';");
            }
        }
        if (_has3d) sb.aln("_has3d=true");
    }

    public void writePcpProtData(BA sb) {
        sb.a("var ali=findAlignment(").a(_alignmentID).aln(");");
        final BA sbI0=new BA(999);
        boolean setIdx0=false;

        for(int iP=0;iP<_pp.length;iP++) {
            Protein p=_pp[iP];
            {
                final int nn[]=p.getResidueNumber(), nR=mini(sze(nn),p.countResidues());
                final String id=orS(p.getPdbID(), p.getInferredPdbID());
                if (nR>0 && id!=null) {
                    sb.a("pcp('I2RESNUM").a(iP).a("',[");
                    final byte cc[]=p.getResidueChain();
                    final byte ii[]=p.getResidueInsertionCode();
                    for(int i=0; i<nR; i++) {
                        if (i>0) sb.a(',');
                        sb.a('\'');
                        sb.a(nn[i]);
                        if (is(LETTR,ii,i)) sb.a((char)ii[i]);
                        sb.a(':');
                        if (is(LETTR_DIGT,cc,i)) sb.a((char)cc[i]);
                        sb.a('\'');
                    }
                    sb.aln("],ali);");
                }
            }
            {
                final int idx0=Protein.firstResIdx(p);
                if (idx0!=0) setIdx0=true;
                sbI0.a(idx0).a(',');
            }
        }
        if (setIdx0) sb.a("pcp('IDX_OFFSET',[").a(sbI0).aln("0],ali);");
    }
}