package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.*;
import java.util.*;
import javax.swing.JComponent;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   Common part of  the dialogs {@link DialogFetchSRS DialogFetchSRS} and {@link DialogFetchPdb DialogFetchPdb}.
   @author Christoph Gille
*/
public final class FilesFetchedFromServer extends AbstractDialogJPanel implements ChRunnable, ProcessEv {
    private final ChJScrollPane _scroll;
    public final static String BUT_LAB_Load="Load selected proteins";
    private ChButton _butDelError;
    private final ChTextView _labStatus=new ChTextView(" busy fetching ...");

    private final List<File>
        _vDownloaded=new UniqueList(File.class).addOptions(UniqueList.USE_HASH_SET),
        _vAlready=new UniqueList(File.class).addOptions(UniqueList.USE_HASH_SET);
    private final List<String> _vFailure=new UniqueList(File.class).addOptions(UniqueList.USE_HASH_SET);
    private final ChJList _jl, _jlAlready, _jlFailure;

    public FilesFetchedFromServer(String urls[], String fileNames[]) {
        super();
        pcp(DialogStringMatch.KEY_SAVE, "Fetched", _jlAlready=new ChJList(_vAlready,ChJList.OPTIONS_FILES));
        pcp(DialogStringMatch.KEY_SAVE, "Fetched", _jlFailure=new ChJList(_vFailure,ChJList.OPTIONS_FILES));
        pcp(DialogStringMatch.KEY_SAVE, "Fetched", _jl=new ChJList(_vDownloaded,ChJList.OPTIONS_FILES));
        _scroll=scrllpnT(_jl);
        if (urls!=null) {
            final ChRenderer r=new ChRenderer();
            addServiceR(StrapAlign.getInstance(), r, ChRunnable.RUN_MODIFY_RENDERER_COMPONENT);
            _jl.setCellRenderer(r);
            _jlAlready.setCellRenderer(r);
            startThrd(thrdCR(this,"F",new Object[]{urls,fileNames}));
            startThrd(FetchSeqs.thread_urlGet(FetchSeqs.SKIP_EXISTING, urls));
        }
        final Object
            pButtons=pnl(
                         new ChButton(BUT_LAB_Load).li(evAdapt(this)).tt("The list shows the downloaded protein files.<br>Load the selected proteins into Strap."),
                         ChButton.doView(log()).t("log")),
            pSouth=pnl(VBPNL,pnl(_labStatus,"<sup>Note: Right-click for popup-menu on files</sup>"),pButtons),
            lExist=scrllpnT(_jlAlready),
            lError=scrllpnT(_jlFailure);

        final Object tl=new ChTableLayout(ChJTable.NO_REORDER)
            .addCol(0,_scroll).addCol(0,lExist).addCol(0,lError)
            .setTitle(0,"Success")
            .setTitle(1,"Already existing")
            .setTitle(2,"Not fetched");
        remainSpcS(this, pnl(CNSEW,tl,dialogHead(this),pSouth));
    }

    public Object run(String id, Object arg) {
        final Object argv[]= arg instanceof Object[] ? (Object[])arg : null;
        if (id=="F") {
            final String[] urls=(String[])argv[0], fileNames=(String[])argv[1];
            final HashSet<String> vAvoidAskingTheSame=new HashSet();
            nextUrl:
            for(int i=0; i<urls.length && !this.isDisposed(); i++) {
                final String idColonDb=urls[i],  fn=fileNames[i], url=Hyperrefs.toUrlString(idColonDb,Hyperrefs.PROTEIN_FILE);
                final int lFn=sze(fn);
                if (idColonDb==null || lFn==0 || fn.indexOf(' ')>=0) continue;
                final File f=file(fn), fAllChains;
                final char chain;
                if (fn.endsWith(".ent") && chrAt(lFn-6, fn)=='_') {
                    fAllChains=file(fn.substring(0,lFn-6)+".ent");
                    chain=chrAt(lFn-5,fn);
                } else {fAllChains=f; chain=0;}
                {
                    final String s=url+chain;
                    if (vAvoidAskingTheSame.contains(s)) continue;
                    vAvoidAskingTheSame.add(s);
                }
                log().a("\nurl: ").a(url).a(" fileNames: ").a(fn).a(" allChains: ").aln(fAllChains).a("Cache: ").aln(file(url)).send();
                if (sze(f)>0) {
                    log().a(f.getName()).aln(" already exists. skipped.").send();
                    _vAlready.add(f);
                    fileViewsUpdate();
                    continue nextUrl;
                }
                log().a("FilesFetchedFromServer: going to open ").aln(url).send();
                final File
                    fSoap=FetchSeqs.dbColonID2file(idColonDb),
                    fDownload= sze(fSoap)>0 ? fSoap : urlGet(url(url),999);
                if (sze(fDownload)==0) {
                    log().a("unable to download ").aln(url).send();
                    delFile(fDownload);
                    _vFailure.add(url);
                    continue nextUrl;
                }
                final String fnDL=fDownload.getAbsolutePath();
                if (fnDL.endsWith(".Z") || fnDL.endsWith(".gz")||fnDL.endsWith(".bzip2")||fnDL.endsWith(".zip")) {
                    final InputStream is=inStream(fnDL,STREAM_UNZIP);
                    if (is!=null) cpy(is,f);
                    else {
                        delFile(fDownload);
                        continue nextUrl;
                    }
                } else {
                    if (sze(fDownload)==0) continue nextUrl;
                    if (sze(fDownload)<33333) {
                        final BA txt=readBytes(fDownload);
                        if (txt==null) continue;
                        txt.delBlanksL().delBlanksR();
                        if (sze(txt)<7 || strEquls(STRSTR_IC,"no entries",txt,0) || looks(LIKE_HTML,txt)) {
                            final File fError=file(f+"_error");
                            renamFileOrCpy(fDownload,fError);
                            _vDownloaded.add(fError);
                            fileViewsUpdate();
                            if (_butDelError==null) _scroll.addMenuItem(_butDelError=new ChButton("DEL_ERROR").t("remove error files").li(evAdapt(this)));
                            continue nextUrl;
                        }
                    }
                    if (chain!=0) {
                        if (!PDB_separateChains.writePdbOnlyChain(f, chain, readBytes(fDownload) ))
                            hardLinkOrCopy(fDownload,fAllChains);
                    } else hardLinkOrCopy(fDownload,f);
                }
                _vDownloaded.add(sze(f)>0 ? f : fAllChains);
                log().a("success: ").aln(fAllChains).send();
                fileViewsUpdate();
            }

            _labStatus.t(ANSI_FG_GREEN+" ALL DONE ");
            fileViewsUpdate();
        }
        return null;
    }
    private void fileViewsUpdate() {
        for(int i=3;--i>=0;) {
            final ChJList l=i==0?_jl:i==1?_jlAlready:_jlFailure;
            revalAndRepaintC(l);
            ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,l,111,null);
        }
    }

    public void processEv(java.awt.AWTEvent ev) {
        final String cmd=actionCommand(ev);
        if (cmd!=null) {
            String s=null;
            final String
                soap_f=delPfx(FetchSeqs.CMD_WSDBFETCH_SEND_QUERY,cmd),
                soap_s=delPfx(FetchSeqs.CMD_WSDBFETCH_SUCCESS,cmd),
                soap_e=delPfx(FetchSeqs.CMD_WSDBFETCH_ERROR,cmd);
            if (cmd!=soap_f) {
                log().a(s=ANSI_BLUE+"SOAP WSDbfetch: ").aln(soap_f).send();
            }
            if (cmd!=soap_s){
                log().aln(s=ANSI_GREEN+"SOAP WSDbfetch success").send();
            }

            if (cmd!=soap_e) {
                log().aln(s=ANSI_RED+"SOAP WSDbfetch failed").send();
                final File fErr=file(soap_e);
                if (sze(fErr)==0) error(soap_e);
            }
            if (s!=null) {
                StrapAlign.drawMessage("@1"+s);
            }
        }
        if (cmd=="DEL_ERROR") {
            for(int i=sze(_vDownloaded); --i>=0;) {
                final File f=_vDownloaded.get(i);
                if (endWith("_error",f)) {
                    delFile(f);
                    _vDownloaded.remove(f);
                }
            }
            fileViewsUpdate();
        }
        if (cmd==BUT_LAB_Load){
            final Object[] oo={_jl.getSelectedValues(),_jlAlready.getSelectedValues()};
            if (sze(oo[0])+sze(oo[1])==0 && (sze(_vDownloaded)+sze(_vAlready)<6 || ChMsg.yesNo("No protein file selected.<br>Load all?"))) {
                oo[0]=_vDownloaded;
                oo[1]=_vAlready;
            }
            final BA sb=new BA(99);
            for(Object o : oo) {
                for(int i=sze(o); --i>=0;) {
                    final File f=get(i,o,File.class);
                    if (f!=null) sb.a(f.getName()).a(' ');
                }
            }
            StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED,sb);
            fileViewsUpdate();
        }
    }


    private static BA _log;
    private static BA log() {
        if (_log==null) _log=new BA(99);
        return _log;
    }

}
