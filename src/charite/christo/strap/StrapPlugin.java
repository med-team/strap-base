package charite.christo.strap;
import charite.christo.*;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.zip.ZipFile;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class StrapPlugin implements HasRenderer {
    private final ZipFile _zipFile;
    private final String _clazzName, _menu;
    private final URL _downloads[];
    private final BA _help;
    private Class _clazz;
    public StrapPlugin(String clazzName, String menu, ZipFile zipFile) {
        _clazzName=clazzName;
        _zipFile=zipFile;
        _menu=toStrgN(menu);
        final String lines[]=readLines(ChZip.inputStream("download.txt", zipFile));
        Collection v=null;
        for (int i=sze(lines); --i>=0;) {
            for(String s : splitTokns(lines[i])) {
                v=adUniqNew(url(delLstCmpnt(s,'#')),v);
            }
        }
        _downloads=toArry(v, URL.class);
        _help=readBytes(ChZip.inputStream(clazzName.replace('.','/')+"Help.html", zipFile));
    }
    public String getMenu() { return _menu;}
    public final Class getClazz(boolean compute) {
        if (_clazz==null && compute) {
            Collection vU=new ArrayList(), vF=new ArrayList();
            for(URL u : _downloads) {
                final String s=delSfx(".pack.gz",toStrg(u));
                if (s.endsWith(".jar")) {
                    if (vU==null) { vU=new ArrayList(); vF=new ArrayList();}
                    vU.add(u);
                    vF.add(jarFile(u));
                }
            }
            //putln(" vF="+vF+"   "+vU);
            InteractiveDownload.downloadFiles(toArry(vU,URL.class), toArry(vF,File.class) );
            vF.add(file(_zipFile.getName()));

                _clazz=Insecure.findClassByName(_clazzName, toArry(vF,File.class) );
                if (_clazz==null) putln(RED_WARNING, "StrapPlugin clazz=null ",_clazzName);
        }
        return _clazz;
    }
    public String toString() {
        return new BA(0).a("StrapPlugin@").a(_clazzName).a(",[").join(_downloads,"|").a("],").a(_menu).toString();
    }
    public Object getRenderer(long options, long rendOptions[]) { return _clazzName;}
    public BA getDocumentation() { return _help; }
    public String getClassName() { return _clazzName;}
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
    public static File jarFile(Object url) {
        return file(dirPlugins(),filtrS('_'|FILTER_NO_MATCH_TO, FILENM, toStrgTrim(url)));
    }

    private static int _pluginsMC;
    private static StrapPlugin[] _plugins;
    public static StrapPlugin[] getPlugins() {
        final ChZip chZip=ChZip.getInstance(dirPlugins());
        chZip.jarFiles();
        final int mc=ChZip.getInstance(dirPlugins()).jarFiles_mc();
        if (_plugins==null || _pluginsMC!=mc) {
            _pluginsMC=mc;
            for(String dd : chZip.joinFiles("delete.txt", (Map)null)) {
                for(String d: splitTokns(dd)) delFile(file(dirPlugins()+"/"+dd));
            }
            final List v=new ArrayList();
            final Map<String,java.util.zip.ZipFile> map=new HashMap();
            for(String line : chZip.joinFiles("classes.txt", map)) {
                final String tt[]=splitTokns(line);
                if (tt.length>0) v.add(new StrapPlugin(tt[0], get(1,tt, String.class), map.get(line)));
            }
            _plugins=toArry(v,StrapPlugin.class);
        }
        return _plugins;
    }
}
