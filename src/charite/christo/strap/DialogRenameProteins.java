package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
@author Christoph Gille
*/
public class DialogRenameProteins implements ActionListener, Runnable {
    private final static String KEY_PREV="DRP$$P";
    public final static int
        P40301=1, P40301_DroMe=2, PRC3_DROME=3, PRC3_DROME_P40301=4,  Drosophila_melanogaster_P40301=5,
        D_melanogaster_P40301=6, P40301_D_melanogaster=7, Fruit_fly_P40301=8, MAX_VARIABLE=9,
        GENERATE=1<<0, COPY=1<<1, INFO_CUT_TERMINI=1<<2, INFO_CHAIN_LETTER=1<<3, COMPENSATE_GAPS=1<<4;
    private final static int RENAME_UNDO=1<<2;
    private final ChButton _labInfo=labl();
    private final ChFrame _frame;
    private final ChTextArea _ta=new ChTextArea(""), _taHeader=new ChTextArea("");
    private final ChCombo
        _combo=new ChCombo(finalStaticInts(DialogRenameProteins.class, MAX_VARIABLE)),
        _comboMvCp=new ChCombo("Rename proteins","Create new proteins");
    private final Object _pnl, _cutTermini, _whichChain, _pnlGenerate;
    private final Map<Protein,String> _mapP=new WeakHashMap();
    /* ---------------------------------------- */
    /* >>> Constructor  >>> */
    private static DialogRenameProteins _lastInst;
    private DialogRenameProteins() {
        _lastInst=this;
        addActLi(this, _ta.tools().enableWordCompletion(dirWorking()).enableUndo(true));
        _taHeader.setEditable(false);
        final Object
            explainCut=
            pnl(HBL,"#ETB","An exclamation mark and a range expression needs to be appended to the protein name.<br><br>"+
                "Consider a protein named \"myProtein\" where you want only the sub-sequence from residue 100 to 200.<br>"+
                "You would type the following rename line:"+
                "<pre class=\"terminal\">\n  myProtein.seq ==> myProtein.seq!100-200\n</pre>"+
                "<sub>For PDB files you can refer to the residue number and chain using the Rasmol syntax (residueNumber-colon-chainLetter).</sub><br>"),
            explainChain=
            pnl(HBL,"#ETB","an underscore followed by the chain letter needs to be added.<br><br>"+
                "Consider the pdb file 1ryp.pdb. To select chain C it needs to be rename to 1ryp_C.pdb.<br>"),
            pnlGenerate=pnl("Alternatively, protein names can be generated according to a rule.",
                            pnl(HBL,_combo.s(4), " ",new ChButton("C").t("generate").li(this)));

        _cutTermini=pnl(VB, KOPT_HIDE_IF_DISABLED, "#ETB", pnlTogglOpts("*Removing N- and C-termini", explainCut),  explainCut);
        _whichChain=pnl(VB, KOPT_HIDE_IF_DISABLED, "#ETB", pnlTogglOpts("*Selecting one single chain of the multi chain structure file", explainChain), explainChain);
        _pnlGenerate=pnl(VB,KOPT_HIDE_IF_DISABLED, "#ETB", pnlTogglOpts("*By organism, compound name and ID", pnlGenerate), pnlGenerate);

        final Object
            pUndo=pnl(HBL, "#ETB",  new ChButton("R").t("Reset to previous").li(this)," ",new ChButton("R0").t("Reset to original").li(this)),
            pS=pnl(VBHB,
                   pnl(HBL,_comboMvCp," ",new ChButton("GO").li(this).t(ChButton.GO).tt("The proteins are renamed according to the list of old and new names  in the text area.")),
                   pnlTogglOpts("*Undo", pUndo),
                   pUndo,
                   _labInfo),
            pN=pnl(VBHB,
                   _cutTermini,
                   _whichChain,
                   _pnlGenerate,
                   " ",
                   "Enter a line for each protein to be renamed or copied.",
                   "Each line should contain the current name and the new name.",
                   "Hit Tab key for name auto completion."
                   ),
            pC=pnl(CNSEW,scrllpn(0,_ta,dim(50*EM,10*EX)),_taHeader,null,null,"#ETB");

        _pnl=pnl(CNSEW,pC,pN,pS);
        _frame=new ChFrame("Change protein name").ad(_pnl).size(555,444).shw(CLOSE_CtrlW_ESC|ChFrame.AT_CLICK);
        StrapAlign.highlightProteins("P",_ta);
        applyOptions(0);
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> API >>> */
    private static ChMap<Object,DialogRenameProteins> _mapInst;
    public static DialogRenameProteins instance(Object key) {
        if (_mapInst==null) _mapInst=new ChMap(0L,String.class, DialogRenameProteins.class,4);
        DialogRenameProteins d=_mapInst.get(key);
        if (d==null) _mapInst.put(key,d=new DialogRenameProteins());
        return d;
    }
    static void update() {
        final DialogRenameProteins d=_lastInst;
        if (d!=null && _mapInst!=null) inEDTms(d,999);
    }
    public void run() {
        for(DialogRenameProteins d : _mapInst.vArray()) d._ta.setText(d._ta.getText());
    }
    public void rename(long options, Protein pp[], Object key) {
        _mapP.clear();
        for(Protein p : pp) if (p!=null) _mapP.put(p,p.getName());
        applyOptions(options);
        setText(0,key);
        _frame.shw(ChFrame.AT_CLICK|ChFrame.PACK);
    }
    /* <<< API  <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void actionPerformed(ActionEvent ev) {

        final String cmd=ev.getActionCommand();
        if (cmd=="R")  setText(-1,null);
        if (cmd=="R0") setText(-RENAME_UNDO,null);
        if (cmd=="C")  setText(_combo.i(),null);
        if (cmd=="GO") { go(); setText(0,null);}
    }
    /* <<< AWTEvent  <<< */
    /* ---------------------------------------- */
    /* >>> go >>> */
    private void go() {
        final Protein pp[]=StrapAlign.proteins();
        final BA ba=toBA(_ta);
        final int ends[]=ba.eol();
        final byte[] T=ba.bytes();
        final ChTokenizer t=new ChTokenizer();
        final BA sbError=new BA(9999), sbCopy=new BA(99);
        final boolean copy=_comboMvCp.i()>0;
        int success=0;
        for(int iL=0;iL<ends.length; iL++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1;
            final int e=ends[iL];
            if (e>b && T[b]=='#') continue;
            t.setText(T,b,e);
            t.setDelimiters(chrClas(SPC));
            if (!t.nextToken()) continue;
            final String old=t.asString();
            if (!t.nextToken()) {
                sbError.a("Line ").a(iL).aln(": missing new name");
                continue;
            }
            final String neu=t.asString();
            if (t.nextToken()) {
                sbError.a("Line ").a(iL).aln(": more than two tokens found in line");
                continue;
            }
            if (old.equals(neu)) continue;
            final Protein p=SPUtils.proteinWithName(old,pp);
            if (p==null)     {
                sbError.a("Line ").a(iL).a(": no such protein  \"").a(old).aln("\"");
                continue;
            }
            if (renameOrCopy(COMPENSATE_GAPS|(copy?COPY:0),neu,p, sbError, sbCopy)) success++;
            if (sze(sbCopy)>0) StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED, sbCopy);
            if (sze(sbError)>0) error(sbError);
            _labInfo.t(new BA(99).a(" successfully  ").a(copy ?"copied " : "renamed ").a(success).a(" proteins.")).fg(0x00FF00);
            StrapEvent.dispatchLater(StrapEvent.PROTEIN_RENAMED,111);
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
            _ta.setText(_ta.getText());
        }
    }
    public static boolean renameProtein(String neu, Protein p, BA sbError) {return renameOrCopy(0, neu,p,sbError, null); }
    private static boolean renameOrCopy(int opts, String neu, Protein p, BA sbError, BA sbCopy) {
        final String old=p.getName();
        final boolean copy=0!=(opts&COPY);
        final File fOld=p.getFile();
        if (fOld==null) {
            sbError.a("Warning: ").a(old).aln(" has no file");
            if (copy) return false;
        }
        if (neu.indexOf(':')>0 && p.getResidueCalpha()==null) {
            sbError.a("Error: ").a(old).a(" colon found in \"").a(neu).aln("\" but file is not a 3D file");
            return false;
        }
        final int R=p.countResidues();
        final int idx0_old=maxi(0,fstTrue(p.getResidueSubsetB()));
        final int gaps[]=p.getResidueGap().clone();
        final int exclamOld=old.lastIndexOf('!');
        final int exclamNeu=neu.lastIndexOf('!');
        if (exclamNeu>=0 && exclamNeu<StrapAlign.usChainOrFileExt(neu)) {
            sbError.a("Error: ").a(old).a("  exclamation mark which preceed residue range expressions must be after the file extension. \"").a(neu).aln("\" ");
            return false;
        }
        final String dir=copy ? "" :  fOld.getParent()+"/";
        final File fNeu=file(dir+StrapAlign.name2file(neu));
        final File fNeuNoChain=file(dir+StrapAlign.name2file(deleteUnderscoreChain(neu)));
        final File fOldNoChain=file(dir+StrapAlign.name2file(deleteUnderscoreChain(old)));
        boolean success=false;
        if (!p.isInMsfFile()) {
            if (copy) p.save(0, dirWorking(),null);
            for(int m=1; m<=Protein.FILE_TYPE_MAX; m++) {
                final File fN= Protein.strapFile(m,neu,null);
                final File fO= Protein.strapFile(m,old,null);
                if (!fO.equals(fN)) {
                    delFile(fN);
                    if (copy) cpy(fO,fN);
                    else renamFileOrCpy(fO,fN);
                }
            }
        }
        if (copy) {
            if (!fNeu.equals(fOld)) {
                if (!canModifyNiceMsg(fNeu)) return false;
                delFile(fNeu);
                if (p.isInMsfFile()) {
                    wrte(fNeu,new BA(999).a('>').aln(p).aln(p.getResidueTypeFullLength()));
                    final int idx=p.getResidueIndexOffset();
                    final File f=Protein.strapFile(Protein.m1stIdx, fNeu.getName(),null);
                    if (idx==0) delFile(f); else wrte(f,toStrg(idx));
                } else if (sze(fOldNoChain)>0) {
                    if (!fNeuNoChain.equals(fOldNoChain)) {
                        delFile(fNeuNoChain);
                        cpy(fOldNoChain,fNeuNoChain);
                    }
                } else cpy(fOld,fNeu);
                success=true;
            }

            sbCopy.aln(neu);
        } else {
            final String colonChainNeu=StrapAlign.colonChain(neu);
            final char usChainNeu=StrapAlign.chainAfterUS(neu);
            final boolean colonChainChanged=!colonChainNeu.equals(StrapAlign.colonChain(old));
            final boolean usChainChanged= usChainNeu!=StrapAlign.chainAfterUS(old);
            final boolean subsetChanged=exclamNeu+exclamOld>-2 && !neu.substring(exclamNeu+1).equals(old.substring(exclamOld+1));
            if (sze(fNeu)==0) {
                if (usChainChanged) {
                    final File fOrig=sze(fOldNoChain)>0 ? fOldNoChain : fOld;
                    if (!canModifyNiceMsg(fNeu)) return false;
                    PDB_separateChains.writePdbOnlyChain(fNeu, usChainNeu,readBytes(fOrig));
                } else {
                    if (!p.isInMsfFile() && !delLstCmpnt(toStrg(fNeu), '!').equals(delLstCmpnt(toStrg(fOld), '!'))) {
                        if (!canModifyNiceMsg(fNeu)) return false;
                        renamFile(fOld,fNeu);
                    }
                    success=true;
                }
            }
            if (colonChainChanged || usChainChanged || subsetChanged) {
                p.setOnlyChains(colonChainNeu!="" ? colonChainNeu : null);
                if (sze(fNeu)>0) SPUtils.parseProtein(fNeu,null,SPUtils.parserOptions(), p);
                final String subset=exclamNeu>0 ? neu.substring(exclamNeu+1): "";
                p.setResidueSubset(subset);
                final int idx0=maxi(0,fstTrue(p.getResidueSubsetB()));
                final int D=idx0-idx0_old;
                if (0!=(COMPENSATE_GAPS&opts)) {
                    if (D>0 ) {
                        if (idx0>=0 && R>D) {
                            final int[] gapNew=new int[R];
                            System.arraycopy(gaps,D,gapNew,0,R-D);
                            for(int i=0; i<D; i++) gapNew[0]+=(gaps.length>i ? gaps[i]:0)+1;
                            p.setResidueGap(gapNew);
                            pcp(KEY_PREV+p.getName(),gaps,p);
                        }
                    } if (D<0) {
                        int[] gapNew=gcp(KEY_PREV+neu,p,int[].class);
                        if (gapNew==null) {
                            gapNew=new int[R-D];
                            System.arraycopy(gaps,0,gapNew,-D, R);
                            gapNew[0]=maxi(0,gapNew[-D]+D);
                            gapNew[-D]=0;
                        }
                        p.setResidueGap(gapNew);
                    }
                }
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_TYPES_CHANGED,1);
                if (p.getResidueCalpha()!=null) StrapEvent.dispatchLater(StrapEvent.ATOM_COORDINATES_CHANGED,1);
            }
            p.setName(neu);
            success=true;
            if (sze(fNeu)>0) p.setFile(fNeu);
        }
        return success;
    }
    /* <<< GO <<< */
    /* ---------------------------------------- */
    /* >>> setText >>> */
    private void setText(int type, Object key) {
        final String col0="Current protein name";
        final BA sb=new BA(999);
        final Protein[] pp= keyArry(_mapP, Protein.class).clone();
        SPUtils.sortVisibleOrder(pp);
        final int max=maxi(sze(nam(longestName(pp))),sze(col0));
        for(Protein p: pp) {
            if (p==null) continue;
            final int fromTo[]=gcp(key,p,int[].class);
            final Object
                name=p.getName(),
                neu=
                fromTo!=null ? new BA(99).a(delLstCmpnt(toStrg(p),'!')).a('!').a(fromTo[0]+1).a('-').a(fromTo[1]) :
                (-type==RENAME_UNDO) ? p.getName0() :  type<0 ? _mapP.get(p) : newName(p,type);
            sb.a(' ',max+1- sze(name)).a(name).a(' ',2).aln(neu!=null?neu:name);
        }
        _ta.setText(sb.toString());
        _taHeader.setText(sb.clr().a(' ',max+1-sze(col0)).a(col0).a("  New protein name").toString());
        setBG(DEFAULT_BACKGROUND,_taHeader);
    }
    private void applyOptions(long options) {
        _comboMvCp.s((options&COPY)!=0 ? 1 :0);
        setEnabld(0!=(options&GENERATE), _pnlGenerate);
        setEnabld(0!=(options&INFO_CUT_TERMINI),  _cutTermini);
        setEnabld(0!=(options&INFO_CHAIN_LETTER), _whichChain);
    }
    /* <<< setText <<< */
    /* ---------------------------------------- */
    /* >>> Utils  >>> */
    private static String deleteUnderscoreChain(String p) {
        final int us=StrapAlign.usChainOrFileExt(p);
        return us<0 || p.charAt(us)!='_' ? p : p.substring(0,us)+p.substring(us+2);
    }
    public static String newName(Protein p, int type) {
        final String
            allID[]=p.getSequenceRefs(),
            org0=p.getOrganism(),
            sc0=p.getOrganismScientific(),
            org=org0!=null ? org0.replaceAll("[^-A-Za-z0-9_+-]","_") : null,
            sc=sc0!=null ? sc0.replaceAll("[^-A-Za-z0-9_+-]","_") : null,
            swiss=p.getSwissprotID();

        final int spc=strchr('_',sc);
        String  embl=null;
        for(String s0:allID) {
            final String s=s0.substring(s0.indexOf(':')+1);
             if (embl==null && s.indexOf('_')<2)  embl=s;
        }
        BA r=null;
        switch(type) {
        case P40301:
            if (embl!=null) r=new BA(embl);
            break;
        case PRC3_DROME:
            if (swiss!=null) r=new BA(swiss);
            break;
        case PRC3_DROME_P40301:
            if (embl!=null && swiss!=null) r=new BA(swiss).a('_').a(embl);
            break;
        case P40301_DroMe:
            r=new BA(99).a(embl).a(p.getOrganism5());
            break;
        case Drosophila_melanogaster_P40301:
            if (sc!=null && embl!=null) r=new BA(sc).a('_').a(embl);
            break;
        case Fruit_fly_P40301:
            if (org!=null && embl!=null) r=new BA(org).a('_').a(embl);
            break;
        case P40301_D_melanogaster:
        case D_melanogaster_P40301:
            if (sc!=null && embl!=null && spc>1) {
                r=new BA(99);
                final boolean e=(type==P40301_D_melanogaster);
                if (e) r.a(embl).a('_');
                r.a(sc.charAt(0)).a('_').a(sc,spc+1,MAX_INT);
                if (!e) r.a('_').a(embl);
            }
            break;
        }
        if (r!=null) {
            final String pn=p.getName();
            final int i=StrapAlign.usChainOrFileExt(pn);
            if (i>=0) r.a(pn.substring(i));
            return r.toString();
        }
        return null;
    }
}
