package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   With this Dialog one or more nucleotide sequences can be translated to
   amino acids according to the WIKI:Genetic_code such that they are most similar to a given reference amino acid sequence.

   The resulting amino acid sequence depends on
   <ul>
   <li>the nucleotide sequence</li>
   <li>the WIKI:Reading_frame (forward or WIKI:Reverse_complement)</li>
   <li>The WIKI:Coding_sequence / WIKI:Non_coding_DNA</li>
   </ul>

   @author Christoph Gille
*/
public class DialogInferCDS extends AbstractDialogJPanel implements ActionListener {
    private final ProteinCombo comboProtein;
    private final ProteinList proteinList=new ProteinList(ProteinList.NT_or_ACTGN).selectAll(0);
    private final ChButton butUndo=new ChButton("Undo").enabled(false).li(this), butLog=new ChButton("Log").enabled(false).li(this);
    private final BA _log=new BA(99).sendToLogger(0,"Infer CDS", IC_DNA, 33*1000);
    public DialogInferCDS() {
        final Object
            pWestNorth=pnl(VBPNL,
                             "Reference amino acid sequence",
                             comboProtein=new ProteinCombo(ProteinList.NOT_ACTGN),
                             " ",
                             pnl(new ChButton("GO").t(ChButton.GO).li(this), butUndo, butLog)
                             ),
            pWest=pnl(CNSEW,null,pWestNorth,null,null,KOPT_TRACKS_VIEWPORT_WIDTH),
            pEast=pnl(CNSEW,proteinList.scrollPane(),"List of nucleotide sequence to be translated"),
            pCenter=pnl(new java.awt.GridLayout(1,2),scrllpn(pWest),pEast);
        pnl(this,CNSEW,pCenter,dialogHead(this));
    }

  public void actionPerformed(ActionEvent ev) {
        StrapAlign.errorMsg("");
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();

        if (cmd=="GO" || q==butUndo) {
            final Protein pp[]=proteinList.selectedOrAllProteins();
            int count=0;
            if (cmd=="GO") {
                final BA sbError=new BA(0), sbWarning=new BA(0);
                final Protein pRef=comboProtein.getProtein();
                if (pp.length==0) sbError.a("Select at least one protein with nt-sequence.\n");
                if (pRef==null) sbError.a("No reference amino acid sequence selected.\n");
                for(Protein p : pp) {
                    if (p==pRef) continue;
                    if (!p.containsOnlyACTGNX() && p.getNucleotides()==null) {
                        sbWarning.a(p).a(" does not contain nucleotides (Letters A, C, T and G)\n");
                    }
                }

                if (sze(sbWarning)>0) error(sbWarning);
                if (sze(sbError)>0) { error(sbError); return; }
                final byte aa[]=pRef.getResidueTypeExactLength();
                for(Protein p : pp) {
                    if (p==pRef) continue;
                    _log.a('=',99).a('\n').a("Sequence ").aln(p).send();
                    byte[] ntSeq=p.getNucleotides();
                    if (ntSeq==null && p.containsOnlyACTGNX()) ntSeq=p.getResidueType();
                    if (ntSeq==null) continue;
                    final Bl2seq bl2seq=new Bl2seq(0L, ntSeq, aa, Bl2seq.BLASTX, _log);
                    bl2seq.compute();
                    final Bl2seq.Hit hh[]=bl2seq.getHits();
                    if (sze(hh)>0) {
                        final boolean[] trans=Bl2seq.getTranslatedNucleotides(ntSeq, aa, hh[0],  hh,0L);
                        if (countTrue(trans)>20) {
                            final boolean reverse=hh[0].getFrom(0)>hh[0].getTo(0);
                            if (reverse) reverseArray(trans,0,MAX_INT);
                            pcp(butUndo,new Object[]{intObjct(p.getCodingStrand()), p.isCoding()},p);
                            p.selectCodingStrand(reverse?Protein.REVERSE_COMPLEMENT : Protein.FORWARD);
                            p.setCoding(trans);
                            count++;
                        }
                    }
                }
                if (count>0) {
                    butUndo.enabled(true);
                    butLog.enabled(true);
                }
            }
            if (q==butLog) shwTxtInW(_log);

            if (q==butUndo) {
                for(Protein p : pp) {
                    final Object oo[]=gcp(butUndo,p,Object[].class);
                    if (oo!=null) {
                        p.selectCodingStrand(atoi(oo[0]));
                        p.setCoding((boolean[])oo[1]);
                        count++;
                    }
                }
            }
            if (count>0) StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
        }
    }

}
