package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public abstract class AbstractAligner implements Disposable, HasControlPanel, HasMap, HasNativeExec, IsEnabled, CanBeStopped {
    public final static String
        MFA_FILE_BASE="input", MFA_FILE="input.fa",
        PFX_DPKG=BasicExecutable.PFX_DPKG,
        RPLC_WIN_DOT_EXE=BasicExecutable.RPLC_WIN_DOT_EXE;
    private final static String ERR=RED_ERROR+"AbstractAligner ";

    private Object _aex, _shared, _ctrl;
    private long _opt,_flags, _parameterOpt;
    private String _implementID="_2", _parameters;
    private Protein _proteins[];
    private byte[][] _ss;
    /* >>> Setters Getters >>> */
    public void setSequences(byte[]... ss) { _ss=ss; }
    public byte[][] getSequences() {
        final byte[][] ss=_ss;
        return ss!=null?ss : SPUtils.residueTypeArray(getProteins());
    }

    public final void setOptions(long optionFlags){ _opt=optionFlags; }
    public final long getOptions(){ return _opt; }
    public final long getPropertyFlags() { return _flags;}
    public void setPropertyFlags(long flags) { _flags=flags; }
    protected void setImplementationID(int s) { _implementID="_"+s; }

    public final void setProteins(Protein...pp) { _proteins=pp; }
    public final Protein[] getProteins() { return _proteins; }
    /* <<< Setters Getters <<< */
    /* ---------------------------------------- */
    /* >>> Exec >>> */

    public BasicExecutable getNativeExec(boolean createNew) {
        if (_aex==null && createNew) _aex=new BasicExecutable();
        return deref(_aex, BasicExecutable.class);
    }

    public BasicExecutable initX(String name, String srcUrls, String instScript) {
        return (BasicExecutable) (_aex=BasicExecutable.init(getNativeExec(true), name, srcUrls, instScript));
    }
    public void stop() {
        final BasicExecutable e=getNativeExec(false);
        if (e!=null) e.stop();
    }
    /* <<< Exec <<< */
    /* ---------------------------------------- */
    /* >>> Command line options >>> */

    private PrgParas _prgPara;
    public final PrgParas getPrgParas() {
        final AbstractAligner si=(AbstractAligner)getSharedInstance();
        if (si!=null) return si.getPrgParas();
        if (_prgPara==null) {
            _prgPara=new PrgParas();
            if (_parameters!=null || _parameterOpt!=0) _prgPara.defineGUI(_parameterOpt, _parameters);
        }
        return _prgPara;
    }
    public final void setSharedInstance(Object shared) { _shared=shared;}
    public final Object getSharedInstance() { return _shared; }
    public void defineParameters(long opt, String s) { _parameterOpt=opt; _parameters=s;}
    /* <<<  Command line options <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    @Override public void dispose() {
        final BasicExecutable e=getNativeExec(false);
        if (e!=null) {
            e.dispose();
            if (e==_aex) _aex=newSoftRef(e);
        }
    }
    /* <<< Disposable <<< */
    /* ---------------------------------------- */
    /* >>> Ctrl >>> */
    public Object getControlPanel(boolean real) {
        if (_aex!=null) {
            final BasicExecutable ae=getNativeExec(false);
            final ChExec e=ae==null?null:ae.exec(false);
            return e==null?null:real?ctrlPnl(e):CP_EXISTS;
        }
        if (_ctrl==null) {
            final Object cached=gcp(KEY_CACHE_TEXT,this);
            if (cached!=null) {
                if (!real) return CP_EXISTS;
                _ctrl=ChButton.doView(cached).t("Found in cache");
            }
        }
        return _ctrl;
    }
    /* <<< Ctrl <<< */
    /* ---------------------------------------- */
    /* >>> gcp >>> */
    private Map _obj;
    public Map map(boolean create) {return _obj==null&&create?_obj=new HashMap() : _obj;}
    /* <<< gcp <<< */
       /* ---------------------------------------- */
    /** >>> compute >>> */
    private final static Object BA[]={null,null};

    public abstract Superimpose3D.Result computeResult(byte[][] sequences, Protein[] proteins);
    public void compute() {
        final long options=getOptions();
        final boolean
            isSuper=isAssignblFrm(Superimpose3D.class, this),
            isAlign=isAssignblFrm(SequenceAligner.class, this);
        final int cacheOpt=CacheResult.OVERWRITE/* | (isSuper ? CacheResult.SOFTREF : 0)*/;
        final byte[][] seq, seq0=getSequences();
        final Protein[] pp, pp0=getProteins();
        if (sze(seq0)<2 && sze(pp0)<2) {
            putln(ERR,"#seq="+sze(seq0));
            return;
        }
        if (this instanceof NeedsProteins && (get(0,pp0)==null || get(1,pp0)==null) ) {
            putln(ERR,"proteins are null \n getProteins()=",getProteins(), "\n this=",this);
            stckTrc();
            return;
        }
        if (!isSelctd(BUTTN[TOG_ALI_NOT_COMMUT])) {
            if (pp0!=null) {
                sortArry(pp=pp0.clone(), ProteinUtils.comparator(ProteinUtils.COMP_P_XYZ));
                seq=SPUtils.residueTypeArray(pp);
            } else {
                pp=null;
                sortArry(seq=seq0.clone(), comparator(COMPARE_ALPHABET));
            }
        } else {
            pp=pp0;
            seq=seq0;
        }

        Superimpose3D.Result result=null;
        if (isAlign && (getOptions()&SequenceAligner.OPTION_CHECK_PERFECT_MATCH)!=0) {
            final byte[][] aligned=AlignUtils.gappedForPerfectMatch(seq);
            if (aligned!=null) {
                result=new Superimpose3D.Result(seq.length);
                result.setGappedSequences(aligned);
                result.setScore(SequenceAligner.SCORE_FOR_PERFECT_MATCH);
            }
        }
        String cacheSection=null, cacheKey=null;
        if (result==null) {
            synchronized(BA) {
                final BA sbKey=baClr(0,BA), sbSection=baClr(1,BA);
                CacheResult.sectionForSequence(seq[0],sbSection);
                sbSection.a(_implementID);
                CacheResult.keyForSequences(seq,sbKey);
                if (this instanceof SequenceAligner3D || isSuper) {
                    for(int i=0; i<sze(pp); i++) {
                        if (pp[i]!=null) sbKey.a('_').aHex(pp[i].hashCodeCalpha(), 8);
                    }
                }
                final byte[][][] profiles=getProfiles();
                for(int i=0; i<sze(profiles); i++) {
                    if (profiles[i]==null) continue;
                    for(byte[] gapped : profiles[i]) {
                        if (gapped==null) sbKey.a("null");
                        else sbKey.a('P').a(hashCd(gapped,0,MAX_INT,true)).a('l').a(hashCd(gapped,0,MAX_INT,true));
                    }
                }
                cacheKey=sbKey.a('_').join(getPrgParas().asStringArray(),"_").toString();
                cacheSection=sbSection.toString();
                if (CacheResult.isEnabled() && (options&SequenceAligner.OPTION_USE_SECONDARY_STRUCTURE)==0) {
                     final BA output=CacheResult.getValue(cacheOpt, getClass(), cacheSection, cacheKey, (BA)null);
                    pcp(KEY_CACHE_TEXT, newSoftRef(new BA(0).a(output)), this);
                    if (output!=null && checkIntegrity(output)) {
                        final long parseOptions=
                            (isSuper ? Superimpose3D.PARSE_MATRIX : 0) |
                            (isAlign ? Superimpose3D.PARSE_ALIGNMENT : 0) |
                            (isAssignblFrm(HasScore.class, this) ? Superimpose3D.PARSE_SCORE : 0);
                        result=parse(seq.length, output, null, parseOptions);
                        if (errorInPairalignment(result,pp," from cache")) result=null;
                    }
                }
            }
        }
        if (result==null) {

            result=computeResult(seq, pp);
            final BasicExecutable e=getNativeExec(false);
            if (e!=null && e==_aex)  _aex=newSoftRef(e);
            if (result!=null && (CacheResult.isEnabled() || 0==CacheResult.getLastModified(getClass(), cacheSection, cacheKey))) {
                CacheResult.putValue(cacheOpt, getClass(), cacheSection, cacheKey, result.toText(null,0L));
                if (myComputer() && !CacheResultJdbc.instance().init()) checkCache_ClassSectionKey(getClass(),cacheSection, cacheKey,seq);
            }
        }
        if (result!=null) _result=result.changeOrder(pp!=null?pp:seq, pp0!=null?pp0:seq0);
        else putln(ERR," result=null ",shrtClasNam(this));
        if (myComputer()) errorInPairalignment(_result,pp0, "Am Ende ");
    }

    /* <<< Compute <<< */
    /* ---------------------------------------- */
    /* >>> Superimpose3D >>> */
    private Superimpose3D.Result _result;
    public Superimpose3D.Result getResult() { return _result; }

    /* >>> Score >>> */
    public final float getScore() {
        final Superimpose3D.Result r=getResult();
        return r!=null ? r.getScore(): Float.NaN;
    }
    public final float getRmsd() { final Superimpose3D.Result r=getResult(); return r!=null ? r.getRmsd() : Float.NaN;}

    /* >>>Sequences  >>> */
    public byte[][] getAlignedSequences(){
        final Superimpose3D.Result r=getResult();
        return r!=null ? r.gappedSequences() : null;
    }

    /* >>> Sorting >>> */
    public int[] getIndicesOfSequences() {
        final Superimpose3D.Result r=getResult();
        return r!=null ? r.getIndicesOfSequences() : null;
    }

    /* >>> Profiles >>> */
    private java.util.ArrayList<byte[][]> _vProfiles;
    public void addProfile(byte ss[][]) {
        if (ss!=null) {
        if (_vProfiles==null) _vProfiles=new java.util.ArrayList();
        if (sze(_vProfiles)<getMaxNumberOfProfiles()) _vProfiles.add(ss);
        }
    }
    public byte[][][] getProfiles() {  return _vProfiles!=null ? toArry(_vProfiles, byte[][].class) : null; }
    public int getMaxNumberOfProfiles() { return 3;}
    /* >>> IsEnabled >>> */
    public boolean isEnabled(Object p) {
        if (this instanceof Superimpose3D || this instanceof SequenceAligner3D) {
            return p instanceof Protein && !trueOrEmpty(gcp(SequenceAligner.KOPT_NOT_USE_STRUCTURE,p)) && ((Protein)p).getResidueCalpha()!=null;
        }
        return true;
    }
    /* <<< IsEnabled <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */
    private static int _instances;
    private File _dir;
    public final File dirTemp() {
        if (_dir==null)  {
            final BasicExecutable x=getNativeExec(false);
            _dir=x!=null ? x.dirTemp() : file(dirTmp()+"/"+shrtClasNam(this)+"/"+ ++_instances);
        }
        return _dir;
    }
    public final File mfaInFile(byte[][] ss) {
        if (sze(ss)==0) return null;
        final File f=file(dirTemp()+"/"+MFA_FILE);
        wrte(f,AlignUtils.toUngappedMultipleFasta(ss,"s",0));
        return f;
    }
    public final File pdbFile(Protein p, int i, boolean res[], long protWriterOptions) {
        final BA sb=new BA(p.countResidues()*120);
        final ProteinWriter1 pw=new ProteinWriter1();
        pw.selectResidues(res);
        if ( (protWriterOptions&ProteinWriter.SIDE_CHAIN_ATOMS)!=0) p.loadSideChainAtoms(null);
        long opt=protWriterOptions|ProteinWriter.PDB|ProteinWriter.ATOM_LINES|ProteinWriter.MET_INSTEAD_OF_MSE;
        if (0==(opt&(ProteinWriter.CHAIN_B|ProteinWriter.CHAIN_SPACE))) opt|=ProteinWriter.CHAIN_A;
        pw.toText(p,null, opt,sb);
        final File f=file(dirTemp()+"/s"+i+".pdb");
        wrte(f,sb);
        return f;
    }
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> parse >>> */
    public static byte[][] insertWhereNoCoordinates(int startR, byte ggR[], int startM, byte ggM[], Protein pR, Protein pM) {
        return AlignUtils.insertWhereNoCoordinates(startR,ggR, startM, ggM, pR.getResidueTypeLowerCaseMeansNo3dCoordinates(), pM.getResidueTypeLowerCaseMeansNo3dCoordinates());
    }
    public static Superimpose3D.Result parse(int numSeq, BA txt, Superimpose3D.Result result0, long options) {
        if (txt==null) {
            putln(ERR,  "#parse: txt is null");
            return null;
        }
        final byte[] T=txt.bytes();
        final int E=txt.end(), ends[]=txt.eol();
        final Superimpose3D.Result result= result0!=null ? result0 : new Superimpose3D.Result(numSeq);
        boolean error=false;
        if ( (options&Superimpose3D.PARSE_MATRIX)!=0) {
            final Matrix3D m3d=new Matrix3D();
            result.setMatrix(m3d);
            if (!m3d.parse(txt)) error=true;
        }
        final boolean parseScore=(options& (Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MATRIX))!=0;
        if (parseScore) {
            float score=Float.NaN, rmsd=Float.NaN;
            String t;
            int pos;
            for(int i=E-6; --i>=1;) {
                final int c0=T[i]|32;
                if (c0=='s' && (T[i+1]|32)=='c' && !is(LETTR_DIGT_US,T,i-1)) {
                    if ( (strEquls(STRSTR_IC,t="score=",T,i)||strEquls(STRSTR_IC,t="score =",T,i)) && is(DIGT_DASH,T,pos=nxt(-SPC,T,i+t.length(),E))) score=(float)atof(T,pos,E);
                }
                if (c0=='r' && (T[i+1]|32)=='m' && !is(LETTR_DIGT_US,T,i-1)) {
                    if ( (strEquls(STRSTR_IC,t="rmsd=",T,i)||strEquls(STRSTR_IC,t="rmsd =",T,i)) && is(DIGT,T,pos=nxt(-SPC,T,i+t.length(),E))) rmsd=(float)atof(T,pos,E);
                }
                if (c0=='a' && T[i+1]=='l' && T[i+2]=='i' && chrAt(i-1,T)=='\n') { /* ClustalW */
                    if ( strEquls(STRSTR_IC,t="Alignment Score ",T,i) && is(DIGT_DASH,T,pos=nxt(-SPC,T,i+t.length(),E))) score=(float)atof(T,pos,E);
                }
            }
            if (!Float.isNaN(score)) result.setScore(score);
            if (!Float.isNaN(rmsd)) result.setRmsd(rmsd);
        }
        if ( (options&Superimpose3D.PARSE_MSA_START_S)!=0) {
            final MultipleSequenceParser parser=new MultipleSequenceParser().setNamesStartWith('s').setText(txt);

            final byte gg0[][]=parser.gappedSequences();
            if (sze(gg0)>=numSeq) {
                final String names[]=parser.getSequenceNames();
                final byte gg[][]=chSze(gg0,numSeq, byte[].class);
                final byte sorted[][]=new byte[gg.length][];
                final int[] ii=new int [gg.length];
                java.util.Arrays.fill(ii, -1);
                boolean errorI=false;
                for(int i=0;i<gg.length;i++) {
                    final int j=atoi(names[i],nxt(DIGT,names[i]));
                    if (j<sorted.length && j>=0) {
                        sorted[j]=gg[i];
                        ii[j]=i;   // ?????
                    } else errorI=true;
                }
                errorI|=idxOf(-1,ii,0,MAX_INT)>=0;
                if (errorI) {
                    putln(ERR, "names=",names);
                    return null;
                }
                result.setIndicesOfSequences(ii);
                result.setGappedSequences(sorted);
            }
            else error=true;
        }

        if ( (options&Superimpose3D.PARSE_ALIGNMENT)!=0) {
            int aliLineStart=-1, aliLineEnd=-1;
            for(int iL=0; iL<ends.length; iL++) {
                final int b0= iL==0 ? 0 : ends[iL-1]+1,  e=prev(-SPC,T,ends[iL]-1,b0-1)+1, b=nxtE(-SPC,T,b0, e);
                if (e-b<10 || T[b]!='<') continue;
                else if (strEquls("<alignment>",T,b)) aliLineStart=iL+1;
                else if (strEquls("</alignment>",T,b)) aliLineEnd=iL;
            }
            if (aliLineStart>0 && aliLineStart<aliLineEnd) {
                byte gg[][]=null;
                int ii[]=null;
                while(true) {
                    int count=0;
                    for(int iL=aliLineStart; iL<aliLineEnd; iL++) {
                        final int b=iL==0 ? 0 : ends[iL-1]+1,  e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                        if (e-b>1 && T[b]=='s' && is(DIGT, T, b+1)) {
                            if (gg!=null) {
                                final int idx=atoi(T,b+1,e);
                                ii[idx]=count;
                                gg[idx]=txt.subSequenceAsBytes(b+Superimpose3D.COLUMN_SEQUENCE,e);
                            }
                            count++;
                        }
                    }
                    if (gg!=null) break;
                    else {
                        gg=new byte[count][];
                        ii=new int[count];
                    }
                }
                result.setGappedSequences(gg);
                result.setIndicesOfSequences(ii);
            } else error=true;
        }
        return !error ? result : null;
    } /*  Superimpose3D.Result parse( ) */

    private final static byte[] TAGS[]={"rotation".getBytes(),"translation".getBytes(), "alignment".getBytes()};
    /* <<< parse <<< */
    /* ---------------------------------------- */
    /* >>> checkIntegrity >>> */
public final static boolean checkIntegrity(BA txt) {
        boolean ok=true;
        int end=0;
        final byte[] T=txt.bytes();
        final int ends[]=txt.eol();
        for(byte[] tag : TAGS) {
            final int L=tag.length;
            int open=0, close=0;
            for(int i=0; i<ends.length; i++) {
                final int b=i==0?txt.begin():ends[i-1]+1;
                final int e=ends[i];
                if (end<e) end=e;
                if (e-b<L+2 || T[b]!='<') continue;
                if (T[b+1]=='/' && e-b>L+2 && T[b+L+2]=='>' && strEquls(tag,T,b+2)) close++;
                if (T[b+L+1]=='>' && strEquls(tag,T,b+1)) open++;
            }
            if (open!=close) {
                putln(new BA(99).a('-',80));
                putln(ERR, " checkIntegrity ",tag," "+open+" "+close);
                putln(txt);
                putln(new BA(99).a('-',80));
                ok=false;
            }
        }
        return ok;
    }
    private static void checkCache_ClassSectionKey(Class c, String section, String key, byte[] ss[]) {
        if (sze(ss)!=2) return;
        //CacheResult.saveAndClear();
        boolean error=false;
        final BA output=CacheResult.getValue(0, c, section, key, (BA)null);
        if (output==null) { error=true; putln(ANSI_MAGENTA+"AbstractAlignerDebug output is null"); }
        else {
            final Superimpose3D.Result result=AbstractAligner.parse(ss.length, output, null, Superimpose3D.PARSE_ALIGNMENT);
            errorInPairalignment(result,ss, "checkResultClassSectionKey");
        }
        if (error) {
            putln(ANSI_YELLOW+"checkResultClassSectionKey ",section, "  ",key);
            putln(ss[0],"\n",ss[1],"\n"+ANSI_RESET);
        }
    }

    private static boolean errorInPairalignment(Superimpose3D.Result r, Protein pp[], String msg) {
        if (sze(pp)!=2 || r==null)return false;

        if ( errorInPairalignment(r, SPUtils.residueTypeArray(pp),msg)) {
            putln("Proteins=",pp);
            return true;
        }
        return false;
    }
    private static boolean errorInPairalignment(Superimpose3D.Result r, byte[] ss[], String msg) {
        if (sze(ss)!=2 || r==null)return false;
        final byte[][] ali=r.gappedSequences();
        boolean error=false;
        if (sze(ali)<2) error=true;
        else {
            final byte[]
                a0=filtrBB(0L, LETTR, ali[0]),
                a1=filtrBB(0L, LETTR, ali[1]);

            if (strstr(STRSTR_IC,a0,ss[0])<0 || strstr(STRSTR_IC,a1,ss[1])<0) {
                putln(RED_ERROR+" AbstractAlignerDebug   ",msg);
                putln(" errorInResult s0=",ss[0],"\n\n AbstractAligner s1=",ss[1],"\n");
                putln(r.toText(null,0L));
                error=true;
            }
        }
        return error;
    }
    /* <<< checkIntegrity <<< */

}
