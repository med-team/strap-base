package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**HELP

A dot-plot is a correlogram of two sequence WIKI:Dot_plot_(bioinformatics).

Each dot in the pane represents a pair of residues from either
protein.  The brightness of the dot is mainly determined by the
similarity of both amino acids according to the blosum62 matrix.  A
number of positions up and downstream are taken into account
with declining weight.

<br><br><b>GUI-controls:</b>
<ul>
<li><b>Slider:</b> The width of the low pass filter with Co-sinus weights.</li>
<li><b>Slider:</b> The brightness of the entire plot.</li>
<li><b>Combo box:</b> The proteins of the X axis and Y axis.</li>
<li><b>Toggle <i>CB:DialogDotPlot#CB_LAB_Trace</i>:</b> View the alignment trace according to the alignment panel.</li>
</ul>

<br><br><b>Highlighting:</b><br>

Positions can be highlighted in the pane  by clicking.  Correspond sequence positions are highlighted.

<br><br><b>Periodicity</b> Sequence periodicity becomes evident by more than one white oblique trace. See PDB:1g61 for an example.

<br><br> Save ink while printing: <i>JCOMPONENT:ChUtils#buttn(TOG_WHITE_BG)!</i>

<br><br><b>Related links:</b> PUBMED:15724278  MOL_TOOLKIT:dnadot
@author Christoph Gille
*/
public class DialogDotPlot extends AbstractDialogJPanel implements Disposable, HasControlPanel, StrapListener, PaintHook, ProcessEv  {
    private final static int WEIGHTS_AVERAGE_VALUE=1024;
    private Backtranslate.Codonusage _usage;
    private Image _imageBall;
    private final DialogDotPlot_Sequence[] dotplotSeq={new DialogDotPlot_Sequence(this,'X'), new DialogDotPlot_Sequence(this,'Y')};
    private final JComponent panArrow=pnl();
    private final long _opt;
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Weights >>> */
    private final static int[][] COSINUS=new int[50][];
    private final static byte[][] DISTANCE_NT=new byte[128][128], DISTANCE_RC=new byte[128][128];
    static {
        for(int w=COSINUS.length; --w>0;) {
            final int ww[]=COSINUS[w]=new int[w+1];
            for(int i=0;i<=w;i++) {

                ww[i]=(int)(100000 *(1+Math.cos(i*Math.PI/w)));
            }
            normalizeWeights(ww);
        }
        for(int i=128; --i>=0;) {
            for(int j=128; --j>=0;) {
                int ci=i|32, cj=j|32;
                if (ci=='u') ci='t';
                if (cj=='u') cj='t';
                DISTANCE_NT[i][j]= (byte)(ci==cj && ( ci=='a' || ci=='c' || ci=='t' || ci=='g') ? 3 :-3);
                final boolean compl= ci=='a'&&cj=='t' || ci=='a'&&cj=='u' || ci=='c'&&cj=='g' ||  ci=='g'&&cj=='c' ||  ci=='t'&&cj=='a' ||  ci=='u'&&cj=='a';
                DISTANCE_RC[i][j]= (byte) (compl ? 3 :-3);
            }
        }
    }
    private static void normalizeWeights(int ww[]) {
        double mean=0, min=MAX_INT;
        for(int w : ww) {
            if (w<min) min=w;
            mean+=w;
        }
        mean/=ww.length;
        for(int i=ww.length; mean>0 && --i>=0;)  {
            ww[i]=(int) (WEIGHTS_AVERAGE_VALUE*(ww[i]-min)/mean);
        }
    }
    /* <<< Weights <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    public DialogDotPlot() { this(0L,null,null);  }
    public final static long BL2SEQ=1<<1, CB_HITS=1<<2, NUCLEOTIDES=1<<3;
    public DialogDotPlot(long options, Protein pX, Protein pY) {
        _opt=options;
        if (pX!=null && pY!=null) {
            comboProtX.s(pX);
            comboProtY.s(pY);
            comboProtX.setEnabled(false);
            comboProtY.setEnabled(false);
        }
        final boolean b2s=0!=(BL2SEQ&options);
        if (b2s) {
            sliderWidth.setPaintLabels(true);
            sliderWidth.setPaintTicks(true);
            sliderWidth.setLabelTable(sliderWidth.createStandardLabels(10));
            sliderWidth.setMajorTickSpacing(8);
            sliderWidth.setMinorTickSpacing(2);
            _bl2seq=new int[0][];
        }
        final boolean removeTopPanel=0!=(_opt&(CB_HITS|BL2SEQ)), noNT=0!=(options&NUCLEOTIDES);
        final Object pWest=pnl(VBHB,
                               b2s?null: sliderWidth,
                               b2s?null: pnl(HBL,"Width of low pass filter"),
                               b2s?null: comboFilter,
                               " ",
                               b2s?null: sliderBrightness,
                               b2s?null:"Brightness",
                               b2s?null:" ",
                               b2s?null: sliderThreshold,
                               b2s?null: "Threshhold",
                               pnl(HBL,"Marks: ",butMark,butClear,butColor),
                               togTrace.cb(),
                               labPosition,
                               pnl(HBL, ChButton.doCtrl(this).cp(KOPT_HIDE_IF_DISABLED,"")),
                               "##"
                               );
        for(JComponent c :dotplotSeq) {
            LI.addTo("mw",c);
            inEDTms((Runnable)c,2500);
        }
        final String ttNT="Nucleotide sequence.<br>If no nt-sequence is defined in the file than backtranslate using the selected coding table.";
        addPaintHook(this,panArrow);
        final Object
            pProteins=removeTopPanel?null:
            pnl(FLOWLEFT,
                "Horizontal: ",pX==null ? comboProtX : new ProteinLabel(0L,pX), noNT?null:_togNTX.tt(ttNT).cb(),
                " Vertical: ", pY==null ? comboProtY : new ProteinLabel(0L,pY), noNT?null:_togNTY.tt(ttNT).cb()),
            pNorth=pnl(VB,
                       removeTopPanel?null:dialogHead(this),
                       pProteins,
                       pnl(new GridLayout(3,1), panArrow, dotplotSeq[0].getPanel(),  dotplotSeq[1].getPanel() )
                       );
        pnl(this,CNSEW,_dotplot,pNorth,null,null, pnl(CNSEW,null,pWest));
        LI.addTo("Mmw",_dotplot);
        StrapAlign.addListener(this);
        enableDisable();
        ChDelay.repaint(_dotplot,999);
    }
    @Override public void dispose() {
        remove(selectorX);
        remove(selectorY);
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Alignment >>> */
    int alignmentPos(char X_or_Y) { return X_or_Y=='X'? alignmentPosX : alignmentPosY;}
    public byte[] getSeq(char X_or_Y) {
        final Protein p=protein(X_or_Y);
        if (p==null) return null;
        final byte aa[]=p.getResidueTypeExactLength();
        final byte nt[]=p.getNucleotidesCodingStrand();
        if (_usage==null) _usage=Backtranslate.codonUsage(comboUsage.toString());
        final boolean isNuc=(X_or_Y=='X' ? _togNTX:_togNTY).s();
        final boolean isBT=isNuc && nt==null;
        final byte seq[]= isBT ?  Backtranslate.backtranslate(aa,aa.length,_usage,false) : isNuc ? nt : aa;
        sequenceType[X_or_Y=='X'?0:1]=isBT?'B':isNuc?'N':'A';
        return seq!=null ? seq:NO_BYTE;
    }
    private char[] sequenceType={'A','A'};
    char sequenceType(char X_or_Y) { getSeq(X_or_Y); return sequenceType[X_or_Y=='X' ? 0:1];}
    public boolean isNucleotide(char X_or_Y) { return sequenceType(X_or_Y)!='A'; }
    void setAlignmentPos(char X_or_Y,int pos) {
        if (nx*ny==0) return;
        final int plotWidth=_plotWidth;
        final int plotHeight=_plotHeight;
        final int xOld=plotWidth*alignmentPosX/nx;
        final int yOld=plotHeight*alignmentPosY/ny;
        if (X_or_Y=='X') {
            final int xNew=plotWidth*(alignmentPosX=pos)/nx;
            _dotplot.paintImmediately(xOld,0,1, plotHeight);
            _dotplot.paintImmediately(xNew,0,1, plotHeight);
        } else {
            final int yNew=plotHeight*(alignmentPosY=pos)/ny;
            _dotplot.paintImmediately(0,yNew, plotWidth,1);
            _dotplot.paintImmediately(0,yOld, plotWidth,1);
        }
        final int x=this.alignmentPosX, y=this.alignmentPosY;
        labPosition.setText(
                             " x="+x+ " "+chrAt(x,getSeq('X'))+
                             " y="+y+ " "+chrAt(y,getSeq('Y')));
    }
    Protein protein(char X_or_Y) {return ((ProteinCombo)(X_or_Y=='X' ? comboProtX : comboProtY)).getProtein();}
    private void resetImage(){
        refValues=null;
        _dotplot.repaint();
        dotplotSeq[0].repaint();
        dotplotSeq[1].repaint();
        if (this instanceof Runnable) startThrd((Runnable)this);
        pcp(KEY_IMAGE_FILE_NAME, "dotplot-"+protein('X')+"-"+protein('Y'),this);
    }
    /* <<< Alignment <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent  >>> */
    private final EvAdapter LI=new EvAdapter(this);
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final int id=ev.getID();
        final Protein px=protein('X'), py=protein('Y');
        {
            final int wheel=wheelRotation(ev), x=x(ev), y=y(ev), h=_plotHeight, w=_plotWidth;
            if (q==_dotplot &&  (id==MOUSE_PRESSED || id==MOUSE_DRAGGED) && !(w*h==0||px==null||py==null)) {
                setAlignmentPos('X',x*nx/w);
                setAlignmentPos('Y',y*ny/h);
                dotplotSeq[0].run();
                dotplotSeq[1].run();
                butMark.enabled(true);
            }
            if (wheel!=0) {
                if (cntains(q,dotplotSeq)) {
                    if (isCtrl(ev)) {
                        dotplotSeq[0].setFontSize(wheel,false);
                        dotplotSeq[1].setFontSize(wheel,false);
                    } else if (q instanceof DialogDotPlot_Sequence) {
                        final JScrollBar sb=getSB('H',q);
                        if (sb!=null) sb.setValue(sb.getValue()+wheel*EM);
                    }
                }
                if (q==_dotplot) {
                    dotplotSeq[isShift(ev)?0:1].move(5*wheel);
                    ChDelay.repaint(_dotplot,333);
                }
            }
            if (id==MOUSE_RELEASED) {
                for(JComponent c : dotplotSeq) c.repaint();
            }
        }
        final String cmd=actionCommand(ev);
        if (cmd!=null) {
            if (q==butColor) {
                final Color c=this.color=colr(q);
                selectorX.setColor(c);
                selectorY.setColor(c);
                _imageBall=null;
                highlight(_dotplot.getGraphics());
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR,111);
                _dotplot.repaint();
            }
            if (q==comboUsage) {
                resetImage();
                if (q==comboUsage) _usage=null;
            }
            if (q==_togNTX || q==_togNTY) {
                for(JComponent c :dotplotSeq) c.revalidate();
                resetImage();
            }
            if (q instanceof ProteinCombo) {
                clearSelection();
                resetImage();
            }
            if (cmd==CB_LAB_Trace) _dotplot.repaint();
            if (q==butClear) {
                clearSelection();
                butClear.enabled(false);
                butColor.enabled(false);
            }
            if (q==butMark)  {
                if (nSelected+2<selected.length) {
                    selected[nSelected++]=alignmentPosX;
                    selected[nSelected++]=alignmentPosY;
                }
                BasicResidueSelection.setAminoSelected(true,alignmentPosX,selectorX);
                px.addResidueSelection(selectorX);
                selectorX.setName("dotplot");
                selectorX.setStyle(VisibleIn123.STYLE_CIRCLE);
                selectorX.setColor(color);
                BasicResidueSelection.setAminoSelected(true,alignmentPosY,selectorY);
                py.addResidueSelection(selectorY);
                selectorY.setName("dotplot");
                selectorY.setColor(color);
                selectorY.setStyle(VisibleIn123.STYLE_CIRCLE);
                for(Runnable r : dotplotSeq) r.run();
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
                butColor.enabled(true);
                butClear.enabled(true);
                _dotplot.repaint();
            }
            if (q==sliderWidth || q==comboFilter) resetImage();
            if (q instanceof ChSlider) ChDelay.repaint(_dotplot,99);
            enableDisable();
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> GUI Elements >>> */
    public final static String CB_LAB_Trace="Trace of the alignment";
    private final ChButton
        togTrace=toggl(CB_LAB_Trace).li(LI).s(true),
        butColor=new ButColor(0,C(0xFFAFAF),LI).enabled(false).t("").rover(IC_COLOR),
        butClear=new ChButton(ChButton.DISABLED,"Clear").li(LI).tt("clear selection"),
        butMark=new ChButton(ChButton.DISABLED, "Add").li(LI),
        _togNTX=toggl(ChButton.MAC_TYPE_ICON,"nt").li(LI),
        _togNTY=toggl(ChButton.MAC_TYPE_ICON,"nt").li(LI);
    private final JComponent _dotplot=pnl(); {addPaintHook(this,_dotplot); }
    private int nx,ny, _plotWidth, _plotHeight, valuesW,valuesH;
    private int alignmentPosX=10,alignmentPosY=10;
    private final JLabel labPosition=new JLabel();
    private final ChCombo
        comboProtX=new ProteinCombo(0).li(LI),
        comboProtY=new ProteinCombo(0).li(LI),
        comboUsage=new ChCombo(Backtranslate.ORGANISMS).li(LI),
        comboFilter=new ChCombo("Cosinus weights","Triangular weights","Constant weights").li(LI);
    private final ChSlider
        sliderBrightness=new ChSlider("",0,1024,256)
        .tt("set the intensity of the plot<br>Values are first multiplied <br>and then turned into gray color"),
        sliderThreshold=new ChSlider("",0,MAX_WERT_SQRT, MAX_WERT_SQRT/2),
        sliderWidth=new ChSlider("",1,COSINUS.length-1,COSINUS.length/2)
        .tt("we apply a <br>moving average low pass filter<br>set here the width of the cosinus curve <br> #<sub>weights</sub>=2*n+1");
    {
        addActLi(LI,sliderBrightness);
        addActLi(LI,sliderThreshold);
        addActLi(LI,sliderWidth);
        pcp(KEY_PRINTABLE_COMPONENTS, new Object[]{dotplotSeq[0],dotplotSeq[1],_dotplot},this);
    }
    private void enableDisable() {
        final Protein px=protein('X'), py=protein('Y');
        final boolean onlyNX=px!=null && px.containsOnlyACTGNX();
        final boolean onlyNY=py!=null && py.containsOnlyACTGNX();
        _togNTX.setEnabled(!onlyNX);
        _togNTY.setEnabled(!onlyNY);
        if (onlyNX) _togNTX.s(false);
        if (onlyNY) _togNTY.s(false);
    }
    /* <<< GUI Elements <<< */
    /* ---------------------------------------- */
    /* >>> Highlight  >>> */
    private void highlight(int ix,int iy,Graphics g) {
        if (nx*ny==0) return;
        if (_imageBall==null)  _imageBall=ChIcon.coloredBall(color,this);
        if (_imageBall==null || g==null) return;
        final int radius=_imageBall.getWidth(this), x=_plotWidth*ix/nx-radius/2, y=_plotHeight*iy/ny-radius/2;
        ChIcon.drawImage(g,_imageBall,x,y,radius,radius, ChIcon.getAlphaComposite(70),this);
    }
    private Rectangle rectCursor;
    private Rectangle rectCursor() {
        final Protein px=protein('X'), py=protein('Y');
        final char tX=sequenceType('X'), tY=sequenceType('Y');
        final int cursorCol=StrapAlign.cursorColumn();
        if (nx*ny==0 || px==null || py==null || cursorCol<0) return null;
        int iAx=px.column2indexZ(cursorCol), iAy=py.column2indexZ(cursorCol);
        if (iAx<0) iAx=-99;
        if (iAy<0) iAy=-99;
        final int ix=tX=='N' ? get(iAx*3,px.coding2allPositions()) : tX=='B' ?  iAx*3 : iAx;
        final int iy=tY=='N' ? get(iAy*3,py.coding2allPositions()) : tY=='B' ?  iAy*3 : iAy;
        final int RADIUS=16;
        return new Rectangle(_plotWidth*ix/nx-RADIUS/2, _plotHeight*iy/ny-RADIUS/2 ,RADIUS,RADIUS);
    }
    private void highlight(Graphics g) { for(int i=0; i<nSelected;i+=2) highlight(selected[i],selected[i+1],g);}
    private Color color=C(0xFFAFAF);
    /* <<< Highlight <<< */
    /* ---------------------------------------- */
    /* >>> ResidueSelection >>> */
    private final int selected[]=new int[66];
    private int nSelected=0;
    private final BasicResidueSelection selectorX=new BasicResidueSelection(0), selectorY=new BasicResidueSelection(0);
    { selectorX.imageBall(true);selectorY.imageBall(true);}
    private void clearSelection(){
        final Protein px=protein('X'), py=protein('Y');
        selectorX.setSelectedAminoacids(null, Protein.firstResIdx(px));
        selectorY.setSelectedAminoacids(null, Protein.firstResIdx(py));
        remove(selectorX);
        remove(selectorY);
        nSelected=0;
        _dotplot.repaint();
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
    }
    private static void remove(ResidueSelection s) {
        final Protein p=SPUtils.sp(s);
        if (p!=null) p.removeResidueSelection(s);
    }
    private static void addPoint(int x, int y, double score, int[][] vHits) {
        if (vHits.length<=y) return;
        if (vHits[y]==null) vHits[y]=new int[3];
        final int newEntry=x| ( (int)(10*score)<<16 ) | (1<<31);
        int[] hy=vHits[y];
        /* --- Point already set? Replace if score is higher. --- */
        for(int i=hy.length; --i>=0;) {
            if (hy[i]!=0 && (hy[i]&0xffFF)==x) {
                hy[i]=maxi(newEntry, hy[i]);
            }
        }
        int idx=0;
        while(idx<hy.length && hy[idx]!=0) idx++;
        if (hy.length<=idx) vHits[y]=hy=chSze(hy,idx+3);
        hy[idx]=newEntry;
    }
    public void addPoints(
                          byte[] tX, int startX, boolean revX, int fromX,
                          byte[] tY, int startY, boolean revY, int fromY,
                          double score, int[][] vHits) {
        int seqX=startX, seqY=startY;
        final char typX=sequenceType('X');
        final char typY=sequenceType('Y');
        final boolean ntAutoX= typX=='A' && nxt(-ACTGUN,getSeq('X'))<0;
        final boolean ntAutoY= typY=='A' && nxt(-ACTGUN,getSeq('Y'))<0;
        final int stepX=(revX?-1:1) * (typX=='B'|| (ntAutoX&&!ntAutoY) ? 3:1);
        final int stepY=(revY?-1:1) * (typY=='B' ? 3:1);
        for(int x=fromX, y= (ntAutoY&&!ntAutoX)?fromY/3:fromY; seqX<tX.length && seqY<tY.length; seqX++,seqY++) {
            final boolean lx=is(LETTR,tX,seqX);
            final boolean ly=is(LETTR,tY,seqY);
            if (lx&&ly)  addPoint(x,y, score, vHits);
            else if (is(EOL,tX,seqX)||is(EOL,tY,seqY)) break;
            if (lx) x+=stepX;
            if (ly) y+=stepY;
        }
    }
    /* <<< ResidueSelection <<< */
    /* ---------------------------------------- */
    /* >>> getValues >>> */
    private int[][] _bl2seq;
    public void setBl2seq(int[][] data, Object hasCtrlPnl) {
        if (data==null && hasCtrlPnl instanceof String) _drawMsg=(String)hasCtrlPnl;
        else _hasCtrlPnl=hasCtrlPnl;
        _ctrl=null;
        refValues=null;
        _bl2seq=data;
        _dotplot.repaint();
    }
    private Object refValues;
    public short[][] getValues(byte[] seqX,char tX, byte[] seqY, char tY,  int w, int h, int weights[]) {
        short min=Short.MAX_VALUE, max=Short.MIN_VALUE;
        short[][] values=(short[][]) deref(refValues);
        if (sze(values)!=w || w<1 ||values[0].length!=h) values=new short[w][h];
        final int b2s[][]=_bl2seq;
        if (b2s!=null) {
            if (b2s.length==0) return null;
            for(int iy=b2s.length; --iy>0;) {
                final int[] line=b2s[iy];
                if (line==null) continue;
                for(int d:line) {
                    if (d==0) continue;
                    final int ix=d&0xffff;
                    final int score=(d>>16) &0xfff;
                    final int x=ix*w/nx;
                    final int y=iy*h/ny;
                    final int v=score;
                    if (x>=0 && y>=0 && x<w && y<h && values[x][y]<v)  values[x][y]= (short)(v>Short.MAX_VALUE ? Short.MAX_VALUE:v);
                }
            }
            for(int x=w;--x>=0;) {
                for(int y=h; --y>=0;) {
                    int v=values[x][y];
                    if (v<min) v=min;
                    if (v>max) v=max;
                }
            }
        } else {
            final byte[][] DISTANCE_BLOSUM=Blosum.BLOSUM62;
            final int w2=weights.length/2;
            final boolean isNT= tX!='A' || nxt(-ACTGUN,seqX)<0;
            final int filter=comboFilter.i();
            for(int x=w;--x>=0;) {
                final int ix=nx*x/w;
                final short[] vv=values[x];
                java.util.Arrays.fill(vv, Short.MIN_VALUE);
                for(int y=h; --y>=0;) {
                    final int iy=ny*y/h;
                    int sum=0;
                    for(int isRC= (isNT?2:1); --isRC>=0;) {
                        final byte distance[][]= isRC!=0 ? DISTANCE_RC : isNT ? DISTANCE_NT : DISTANCE_BLOSUM;
                        for(int i=weights.length, rx0=ix+i,ry=iy+i,  lx0=ix-i, ly=iy-i; --i>=0;  rx0--, ry--, lx0++, ly++) {
                            final int rx,lx;
                            if (isRC!=0) { rx=lx0; lx=rx0;} else { rx=rx0; lx=lx0;}
                            final int dr= (rx>=0 && ry>=0 && rx<seqX.length && ry<seqY.length) ?  distance[seqX[rx]][seqY[ry]] : 0;
                            final int dl=(i>0 && lx>=0 && ly>=0 && lx<seqX.length && ly<seqY.length) ?  distance[seqX[lx]][seqY[ly]] : 0;
                            switch(filter) {
                            case 0: sum+=weights[i]* (dr+dl); break;
                            case 1: sum+=(weights.length-i)*(dr+dl); break;
                            case 2: sum+=dr+dl; break;
                            }
                            sum+=(dr+dl);
                        }
                    }
                    final short v=sum>Short.MAX_VALUE ? Short.MAX_VALUE : sum<Short.MIN_VALUE ? Short.MIN_VALUE  : (short)sum;
                    vv[y]=v;
                    if (x>w2 && y>w2 && x<w-w2 && y<h-w2) {
                        if (v<min) min=v;
                        if (v>max) max=v;
                    }
                }
            }
        }
        if (max>min) {
            for(int x=w;--x>=0;) {
                final short[] vvX=values[x];
                for(int y=h; --y>=0;) {
                    int v=vvX[y];
                    v=MAX_WERT*(v-min)/(max-min);
                    vvX[y]=(short)v;
                    if ( (v<0 || v>MAX_WERT) && myComputer()) putln("Warning DialogDotPlot v="+v+" MAX_WERT="+MAX_WERT);
                }
            }
        }
        return values;
    }
    private final static int MAX_WERT_SQRT=100, MAX_WERT=MAX_WERT_SQRT*MAX_WERT_SQRT;
    /* <<< getValues <<< */
    /* ---------------------------------------- */
    /* >>> StrapEvent >>> */
    private int _mcResSel;
    public final void handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        boolean repaintSeq=false;
        if ((t==StrapEvent.BACKGROUND_CHANGED || t==StrapEvent.ALIGNMENT_CHANGED) && togTrace.s()) _dotplot.repaint();
        if (t==StrapEvent.BACKGROUND_CHANGED || t== StrapEvent.ALIGNMENT_CHANGED || t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN || t==StrapEvent.CURSOR_CHANGED_PROTEIN) {
            if (rectCursor!=null) _dotplot.repaint(rectCursor);
            rectCursor=rectCursor();
            if (rectCursor!=null) _dotplot.repaint(rectCursor);
            repaintSeq=true;
        }
        if (t==StrapEvent.RESIDUE_SELECTION_CHANGED || t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR) {
            final Protein px=protein('X'), py=protein('Y');
            if (px!=null && py!=null) {
                final int mc=px.mc(ProteinMC.MC_RESIDUE_SELECTIONS)+py.mc(ProteinMC.MC_RESIDUE_SELECTIONS)+comboProtX.mc()+comboProtY.mc();
                if (_mcResSel!=mc) {
                    _mcResSel=mc;
                    repaintSeq=true;
                }
            }
        }
        if ( (t&StrapEvent.FLAG_RESIDUE_TYPES_CHANGED)!=0) resetImage();
        if (t==StrapEvent.AA_SHADING_CHANGED || repaintSeq) {
            for(JComponent c : dotplotSeq) c.repaint();
        }
    }
    /* <<< StrapEvent <<< */
    /* ---------------------------------------- */
    /* >>> PaintHook >>> */
    private String _drawMsg;
    public boolean paintHook(JComponent component, Graphics g, boolean after) {
        final boolean isWhiteBG=isWhiteBG();
        final int w=wdth(component);
        final int h=hght(component);
        if (component==_dotplot && after){
            _plotHeight=h;
            final Rectangle clip;
            if (_plotWidth!=w || _plotHeight!=h) {
                _plotWidth=w;
                _plotHeight=h;
                clip=null;
            } else clip=clipBnds(g, w, h);
            final int minX=maxi(0,x(clip));
            final int minY=maxi(0,y(clip));
            final int maxX=mini(w, x2(clip)+1);
            final int maxY=mini(h, y2(clip)+1);
            if (w<3 || h<3) return true;
            g.setFont(getFnt(16,true,Font.BOLD));
            final Protein px=protein('X'), py=protein('Y');
            if (px==null || py==null) return true;
            final char tX=sequenceType('X'), tY=sequenceType('Y');
            this.nx= tX=='N' ? px.countNucleotides() : (tX=='B'?3:1)*px.countResidues();
            this.ny= tY=='N' ? py.countNucleotides() : (tY=='B'?3:1)*py.countResidues();
            String msg=_drawMsg;
            if (px==null || py==null)  msg="Two proteins required !";
            else if (nx*ny==0) msg="Erro: Sequence is empty !";
            else {
                final int brightness=sliderBrightness.getValue();
                final int threshhold=sliderThreshold.getValue()*sliderThreshold.getValue();
                final int weights[]=COSINUS[mini(COSINUS.length-1,maxi(1, sliderWidth.getParent()==null ? 1 : sliderWidth.getValue()))];
                short values[][]=(short[][]) deref(refValues);
                {
                    final int vW=mini(nx,w), vH=mini(ny,h);
                    if (values==null || valuesH!=vH || valuesW!=vW) {
                        refValues=newSoftRef(values=getValues(getSeq('X'),tX, getSeq('Y'),tY,valuesW=vW,valuesH=vH,weights));
                    }
                }
                final int vWidth=sze(values),  vHeight=vWidth>0 ? values[0].length : 0;
                final boolean autoScale=_bl2seq!=null;
                int autoScaleMa=MIN_INT, autoScaleMi=MAX_INT;
                if (autoScale) {
                    for(int x=vWidth; --x>=0;) {
                        for(int y=sze(values[x]); --y>=0;) {
                            final int v=values[x][y];
                            if (v<autoScaleMi) autoScaleMi=v;
                            if (v>autoScaleMa) autoScaleMa=v;
                        }
                    }
                }
                if (vHeight>0 && autoScaleMa!=autoScaleMi ) {
                    final int vMinX=maxi(0,minX*vWidth/w-1);
                    final int vMaxX=mini(vWidth,maxX*vWidth/w+1);
                    final int vMinY=maxi(0,minY*vHeight/h-1);
                    final int vMaxY=mini(vHeight,maxY*vHeight/h+1);
                    final java.awt.image.BufferedImage image=sharedImage('v',vHeight);
                    final int[] rgb=sharedRGB(vHeight);
                    for(int x=vMinX; x<vMaxX; x++) {
                        final short[] valuesX=values[x];
                        java.util.Arrays.fill(rgb,vMinY,vMaxY,0);
                        for(int y=vMinY; y<vMaxY; y++) {
                            final int v0=(int)valuesX[y];
                            if (v0>threshhold || autoScale) {
                                int v=autoScale ? (int)(256L*(v0-autoScaleMi)/(autoScaleMa-autoScaleMi)) :
                                    (v0-threshhold)*brightness/(MAX_WERT-threshhold);
                                if (v>255) v=255; else if (v<0) v=0;
                                if (isWhiteBG) v=255-v;
                                rgb[y]=(v << 16)  | (v<<8) |  v;
                            } else if (isWhiteBG) rgb[y]=0xFFffFF;
                        }
                        image.setRGB(0, 0, 1,vMaxY-vMinY, rgb,  vMinY,1);
                        final int dx1=x*w/vWidth;
                        final int dy1=vMinY*h/vHeight;
                        final int dx2=(1+x)*w/vWidth;
                        final int dy2=vMaxY*h/vHeight;
                        g.drawImage(image,dx1,dy1,dx2,dy2,  0,0,1,vMaxY-vMinY,this);
                    }
                } else if (_drawMsg==null) {
                    msg=0!=(_opt&CB_HITS) ? " Activate check-boxes of hits " : " calculating...";
                }
            }
            if (msg!=null) {
                g.fillRect(0,0,w,h);
                g.setColor(C(0xFFAFAF));
                g.drawString(msg,20,20);
            }
            if (togTrace.s()) {
                g.setColor(C(0xFF64ff,99));
                int lastX=-1,lastY=-1;
                final int lineWidth=w/50+1;
                for(int x=minX;x<maxX;x++) {
                    final int ix=(int) (x* (long)nx/w);
                    final int iAx= tX=='N' ? get(ix,px.toCodingPositions())/3 : tX=='B' ? ix/3 : ix;
                    final int col=px.getResidueColumnZ(iAx);
                    final int iAy=py.column2nextIndexZ(col);
                    if (py.getResidueColumnZ(iAy)!=col) continue;
                    final int iy=tY=='N' ? get(iAy*3,py.coding2allPositions()) : tY=='B' ?  iAy*3 : iAy;
                    final int y=h*iy/ny;
                    g.fillRect(lastX,lastY-lineWidth/2, x-lastX,lineWidth);
                    lastX=x;lastY=y;
                }
            }
            highlight(g);
            final Rectangle r=rectCursor();
            if (r!=null) ChIcon.drawCursor(g,x(r),y(r), wdth(r), hght(r),this);
            rectCursor=r;
            if (alignmentPosX>=0 && alignmentPosY>0 && nx>0 && ny>0) {
                g.setColor(C(0xFF0000));
                final int x=w*alignmentPosX/nx, y=h*alignmentPosY/ny;
                g.drawLine(0,y,w,y);
                g.drawLine(x,0,x,h);
            }
            final int secStr2color[]=ShadingAA.rgb(ShadingAA.SECSTRU);
            for(int isVertical=2; --isVertical>=0;) {
                final int THICKNES=4;
                if ((isVertical!=0 ? minX:minY)>THICKNES) continue;
                final Protein p=isVertical!=0?py:px;
                final byte ss[]=p.getResidueSecStrType();
                if (ss==null) continue;
                final int nR=p.countResidues();
                final java.awt.image.BufferedImage im=sharedImage(isVertical!=0 ? 'V':'H',nR);
                final int rgb[]=sharedRGB(nR);
                for(int i=nR;--i>=0;) rgb[i]= i<ss.length ? secStr2color[ss[i]] : 0;
                if (isVertical!=0) {
                    im.setRGB(0, 0, 1,nR, rgb,  0,1);
                    g.drawImage(im,0,0,THICKNES-1,h,  0,0,1,nR,this);
                } else {
                    im.setRGB(0,0,nR,1, rgb, 0, nR);
                    g.drawImage(im,  0,0, w,THICKNES-1,    0,0,nR,1, this);
                }
            }
        }
        if (component==panArrow && after) {
            g.setColor(C(0xFF0000));
            g.drawLine(w/2,0, w/2,h);
            for(int i=-1; i<=1; i+=2) g.drawLine(w/2+ i*EM/2, h-EM,   w/2,h);

        }
        return true;
    }
    /* <<< PaintHook <<< */
    /* ---------------------------------------- */
    /* >>> Logging >>> */
    private BA _log;
    public BA log() {
        if (_log==null) _log=new BA(0);
        return _log;
    }
    /* <<< Logging <<< */
    /* ---------------------------------------- */
    /* >>> ControlPanel >>> */
    private Object _hasCtrlPnl, _ctrl;
    @Override public Object getControlPanel(boolean real) {
        if (!real) return hasCtrlPnl(_hasCtrlPnl) ? CP_EXISTS : null;
        if (_ctrl==null) {
            _ctrl=pnl(
                      CNSEW,scrllpn(log()),
                      0!=(_opt&NUCLEOTIDES) ? null : pnl(VB, pnl("In case of translation of aminos to triplets use the codon table: ",comboUsage)),
                      ctrlPnl(_hasCtrlPnl));
        }
        return _ctrl;
    }
}
/*
tblastn aa , n
blastx n , aa
*/
