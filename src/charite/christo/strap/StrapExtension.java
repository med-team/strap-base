package charite.christo.strap;
import charite.christo.protein.*;
/**
StrapExtension defines a general multi-purpose plugin.
*/
    public interface StrapExtension extends StrapListener  {
        void handleEvent(StrapEvent e);
        void run();
    }
