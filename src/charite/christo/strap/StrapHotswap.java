package charite.christo.strap;
import charite.christo.hotswap.ProxyClass;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP
<i>PACKAGE:charite.christo.protein.</i>
<i>PACKAGE:charite.christo.strap.extensions.</i>

The hotswap-dialog provides a comfortable environment for writing Java based plugins.
The concept of class-hotswap has been published in PUBMED:16469097.
<br><br>
<b>Warning:</b> The Java classes in Strap may change in the future which might require that existing plugins need to be adapted.
Therefore External Apps (File menu of Strap) should be used instead if possible!

<br><br>

The tree view shows the main Java interfaces of Strap as expandable
nodes. The child nodes are classes that implement these Java
interfaces.

<br><br><b>Quick start:</b>

 Follow this short guide:
Activate the node <b>"StrapExtension"</b> in the tree.
Subsequently, press the button
<i>BUTTON:StrapHotswap#BUT_LAB_New</i>
to create a new plugin of type "StrapExtension".
You will be asked for a name of your new plugin. You can accept the
suggested name.  The WIKI:Source_code of the
new plugin  is opened with a text editor.
The source is a minimal example for "StrapExtension" which can be extended.
To start  press <i>BUTTON:StrapHotswap#BUT_LAB_Apply</i>.
This demo plugin opens a message box with information on loaded proteins.
Change the code in the text editor. For example chose a different message text.
Save the source code.
When the altered source code is saved to hard disk  the  WIKI:Compiler is automatically started.

Run the plugin again and observe, that the message text has changed.

If the compilation fails, the WIKI:Compilation_error is shown in the log panel. Please insert a syntax error and try again.

<br><br><b>JAR-files:</b>
All WIKI:Jar_files in the directories ~/.StrapAlign/plugins  and  ~/.StrapAlign/jars are included to the WIKI:Classpath_(Java).

<br><br><b> Types of plugins:</b>
Each plugin implements  a certain WIKI:Java_interface which determins the purpose of the plugin. The above example uses StrapExtension.
StrapExtension has a run() method which is run directly, whereas other plugins are used within a certain dialog.

<br><br><b>StrapEvent:</b> Classes implementing
<i>JAVADOC:StrapListener</i> are notified when the cursor moves, when
proteins are added or deleted or when the multiple sequence alignment
is altered.  These  Events can be monitored (Debug-menu).

<br><br><b>Customization:</b>
<i>ICON:IC_CUSTOM</i> The preferred  WIKI:Text_editor or  WIKI:Integrated_development_environment used for Java source texts can be specified with the customize button at the top bar.
<br><br><b>Accessing data of the multiple sequence alignment by plugins:</b>
For the communication with the Strap three classes are usually sufficient:
<ul>
<li><i>JAVADOC:StrapAlign</i></li>
<li><i>JAVADOC:StrapEvent</i></li>
<li><i>JAVADOC:Protein</i></li>
</ul>
<br><br><b>Changing elements in an array:</b>
Some methods return arrays.  Do not change array elements without subsequently calling the respective set-method.
Also be aware, that some arrays are longer than the current information.
*/
public class StrapHotswap implements HasPanel, ChRunnable, ProcessEv, PaintHook {
    /* --- Buttons --- */
    public final  static String  BUT_LAB_Apply="Apply or start", BUT_LAB_New="New plugin";
    private final JTabbedPane _tabPane=new JTabbedPane();
    private final ChJTree _jt;
    public StrapHotswap() {
        setSettings(this, Customize.javaSourceEditor);
        mkdrsErr(dirHotswap(),"Here hotswap-plugins are stored");
        final Object[] root={ vNO_INTERFACE, vNOT_COMPILED, TabItemTipIcon.set("","Interfaces","","", StrapPlugins.getInterfacesAndClasses())};
        _jt=new ChJTree(0, new ChTreeModel(0, root));
        _jt.setCellRenderer(new ChRenderer().options(ChRenderer.CLASS_NAMES));
        rtt(_jt);
        LI.addTo("m",_jt);
        _jt.setRootVisible(false);
        _jt.setShowsRootHandles(true);
        scrollByWheel(_jt);
        addPaintHook(this,_jt);
        final BA logJavac=ChCompilerJavacPROXY.log();
        final ChTextView ta=logJavac.textView(true);
        pcp(KOPT_TRACKS_VIEWPORT_WIDTH,"",ta);
        final JComponent
            spTree=scrllpnT(_jt),
            pTools=pnl(VBHB,
                       ChButton.doView(logJavac).t("Log messages"),
                       SourcefilesInJarfile.newButton().t("List all source files"),
                       new ChButton("Open directory in file browser").doViewFile(dirHotswap()).tt(toStrg(dirHotswap())),
                       StrapAlign.button(StrapAlign.BUT_WATCH_EV),
                       StrapAlign.button(StrapAlign.BUT_LIST_EV),
                       buttn(TOG_JAVA_SRC).cb()
                       ),
            panButtons=pnl(
                           VBHB,
                           butNew,
                           " ",
                           butEdit,
                           " ",
                           butApply,
                           " ",
                           pnlTogglOpts("Tools: ",pTools),
                           pTools
                           ),
            lComponent=scrllpn(ta).addMenuItem(ta.tools().newClearButton().li(LI)).addMenuItem(togCompiler),
            rComponent=pnl(CNSEW,spTree, dialogHead(this), null,null, pnl(CNSEW,null,panButtons));
        setMinSze(9,9, _tabPane);
        adTab(0, "Interfaces and plugins",rComponent, _tabPane);
        adTab(0, "Compiler messages",lComponent, _tabPane);
        TabItemTipIcon.set("","","Hotswap plugins",IC_PLUGIN, _tabPane);
        enableDisable();
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
     /* >>> AWTEvent >>> */
    private final EvAdapter LI=new EvAdapter(this);
    public void processEv(AWTEvent ev) {
        final int id=ev.getID();
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final boolean ctrl=isCtrl(ev), shift=isShift(ev);
        final File file=_theFile;
        final BA logJavac=ChCompilerJavacPROXY.log();
        if (cmd!=null) {
            StrapAlign.errorMsg("");
            if (cmd==CMD_CLEAR) _tabPane.setSelectedIndex(0);
            if (q==togCompiler) {
                logJavac.aln(togCompiler.s() ?
                           "the compiler will automatically compile\nwhen java files are modified." :
                           "Internal compiler is switched off.\n\n"+
                           "STRAP expects that the compiler is invoked by the program\n"+
                           "that you use to edit the Java file.\n\n");
            }
            if (q==butNew) {
                if (shift) revalAndRepaintC(_jt);
                else if (ctrl) _jt.updateUI();
                else create(_theInterface);
            }
            if (q==butApply) {
                if(_proxyClass!=null) {
                    if (ctrl) assignNewProxy(_proxyClass.getSourcefile());
                    StrapPlugins.applyPlugin(_proxyClass);
                }
                else StrapPlugins.applyPlugin(_theClass);
            }
            if (q==butEdit) {
                if (file!=null) {
                    final Customize c=Customize.customize(Customize.javaSourceEditor);
                    final ChExec ex=new ChExec(0).setCommandsAndArgument(c.getSettings(),file.getAbsolutePath());
                    if (ctrl) ex.setCustomize(c);
                    startThrd(ex);
                }
                if (!shift) {
                    final Class c=_theClass!=null ? _theClass : _theInterface;
                    if (c!=null) showJavaSource(c.getName(),(String[])null);
                    if (file!=null && _proxyClass!=null) {
                        BA src=readBytes(file);
                        if (sze(src)>0) {
                            if (endWith(".java",file)) src=ChCodeViewer.highlightSyntax(ChCodeViewer.ANSI, src, null);
                            new ChTextView(src).tools().showInFrame(0,_proxyClass.getName()).cp(ChTextComponents.KOPT_CLASS_AT_CURSOR, "");
                        }
                    }
                }
            }
        }
        if (q==_jt &&id==java.awt.event.MouseEvent.MOUSE_PRESSED) {
            Object o=objectAt(null,ev);
            if (o instanceof UniqueList) o=((UniqueList)o).run(MAY_PROVIDE_TREE_VALUE,null);
            if (o!=null && _selection!=o) selectionChanged(_selection=o);
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> File >>> */
    private  File className2SourceFile(String s){return file(dirHotswap(),s+".java");}
    private final Set<String> vAllJavaFiles=new HashSet();
    private final static Map<File,File> mapJavafile2Classfile=new HashMap();
    private final static Map<File,Object/*Long*/> mapWhenCompiled=new HashMap();
    private long _classefilesLM, _lastModified;
    private String shortClassName(String s0) {
        final String s=delSfx(".java",s0);
        return s!=null ? s.substring(1+s.lastIndexOf(File.separatorChar)) : null;
    }
    private ProxyClass assignNewProxy(File f) {
        if (f==null) return null;
        final String className=delDotSfx(f.getName());
        ProxyClass pc=ProxyClass.getProxyClass(className);
        if (pc!=null) { ProxyClass.remove(pc); pc=null;}
        pc=ProxyClass.getProxyClass(f,className);
        _toBeSelected=pc;
        sortClasses();
        _jt.updateKeepStateLater(0L, 33);
        _toBeSelected=pc;
        inEDTms(_threadSelect,999);
        return pc;
    }
    private ProxyClass _toBeSelected;
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> StrapEvent >>> */
    private void dispatchStrapEvent(Class c) {
        if (c==null) return;
        final int type=
            CompareTwoProteins.class.isAssignableFrom(c) ? StrapEvent.PROTEIN_PROTEIN_DISTANCE_CHANGED:
            ValueOfAlignPosition.class.isAssignableFrom(c) ? StrapEvent.VALUE_OF_ALIGN_POSITION_CHANGED:
            ValueOfProtein.class.isAssignableFrom(c) ? StrapEvent.VALUE_OF_PROTEIN_CHANGED:
            ValueOfResidue.class.isAssignableFrom(c) ? StrapEvent.VALUE_OF_RESIDUE_CHANGED:
            ResidueSelection.class.isAssignableFrom(c) ? StrapEvent.RESIDUE_SELECTION_CHANGED: 0;
        if (type>0) {
            inEDTms(new StrapEvent(this,type),999);
            inEDTms(new StrapEvent(this,type),2999);
        }
        inEDTms(new StrapEvent(this,StrapEvent.PLUGIN_CHANGED),99);
    }
    /* <<< StrapEvent  <<< */
    /* ---------------------------------------- */
    /* >>>Threading  >>> */
    private final Runnable _threadSelect=thrdCR(this,"SELECT");
    private boolean _painted, _running=true;
    public boolean paintHook(JComponent component, Graphics g, boolean after) {
        if (!_painted) {
            _painted=true;
            run("UPDATE",null);
            startThrd(thrdCR(this,"CK"));
            _jt.expandPath(_jt.getPathForRow(2));
        }
        return true;
    }
    public Object run(String id,Object arg) {
        final boolean isWin=isWin();
        final BA logJavac=ChCompilerJavacPROXY.log();
        if (id=="UPDATE") {
            final long time=System.currentTimeMillis();
            //ProxyClass.setClassLoader(ChClassLoader1.getInstance(new File[]{dirHotswap()}));
            vAllJavaFiles.clear();
            final File ff[]=lstDirF(dirHotswap());
            final boolean isCompiler=togCompiler.s();
            if (ff.length==0) return null;
            for(File f : ff) {
                if (!isCompiler || !endWith(".java",f) || !is(LETTR,f.getName(), 0)) continue;
                File fClass=mapJavafile2Classfile.get(f);
                if (fClass==null) mapJavafile2Classfile.put(f,fClass=file(delSfx("java",f)+"class"));
                Object whenCompiled=mapWhenCompiled.get(f);
                if (whenCompiled==null) whenCompiled=sze(fClass)>0 ? longObjct(fClass.lastModified()) : null;
                if (atol(whenCompiled)< f.lastModified()) {
                    new ChCompilerJavacPROXY().compile(classpth(true, dirHotswap()), f);
                    _tabPane.setSelectedIndex(1);
                    mapWhenCompiled.put(f,longObjct(System.currentTimeMillis()));
                }

            }
            for(File f : ff) {
                final File fClass=endWith(".java",f) ? mapJavafile2Classfile.get(f) : null;
                if (fClass==null) continue;
                final String shortName=shortClassName(f.getName());
                if (null==ProxyClass.getProxyClass(shortName))  assignNewProxy(f);
                vAllJavaFiles.add(shortName);
                if (fClass.lastModified()>_classefilesLM) {
                    final ProxyClass pc=ProxyClass.getProxyClass(shortName);
                    if (pc!=null) {
                        pc.hotswap();
                        if (pc!=null) {
                            final BA sb=new BA(99);
                            for(Class i:pc.getProxyInterfaces()) { final String sn=i.getName(); sb.a(' ').a(sn, sn.lastIndexOf('.')+1,MAX_INT);}
                            final String s=sb.toString();
                            if (!s.equals(_mapFile2Interface.get(f)))  {
                                logJavac.a(shortName).a(" "+ANSI_YELLOW+"implements "+ANSI_RESET).aln(s);
                                _mapFile2Interface.put(f,s);
                                ProxyClass.getProxyClass(f,shortName);
                            }
                        }
                        dispatchStrapEvent(pc.getClassInstance());
                    } else ProxyClass.getProxyClass(f,shortName);
                }
            }
            for(ProxyClass pc : ProxyClass.getProxyClasses()) {
                if (!vAllJavaFiles.contains(pc.getName())) ProxyClass.remove(pc);
            }
            sortClasses();
            _jt.updateKeepStateLater(0L, 333);
            _classefilesLM=time;
        }
        if (id=="SELECT") {
            final javax.swing.tree.TreePath tp=_toBeSelected!=null ? getTreePath(_toBeSelected,_jt) : null;
            if (tp!=null) {
                _jt.clearSelection();
                _jt.addSelectionPath(tp);
                selectionChanged(_toBeSelected);
            }
        }
        if (id=="EXPAND") _jt.expandPath(getTreePath(arg,_jt));        
        if (id=="CK") {
            long dirPlugins_lastModified=0;
            for(int count=0; _running; count++) {
                sleep(isWin ? 444 : 44);
                final long lm=dirHotswap().lastModified();
                if ((count&15)!=0 && lm==dirPlugins_lastModified) continue;
                dirPlugins_lastModified=lm;
                final boolean isCompiler=togCompiler.s();
                long sum=0;
                for(File f : lstDirF(dirHotswap())) {
                    if (endWith(isCompiler ? ".java" : ".class", f)) sum+=f.lastModified();
                }
                if (_lastModified!=sum) {
                    _lastModified=sum;
                    inEDTms(thrdCR(this,"UPDATE"),3);
                }
            }
        }
        return null;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> create >>> */
    private void create(final Class interFace) {
        if (interFace==null) return;
        final String ifName=shrtClasNam(interFace);
        String name="";
        for(int i=0;i<99;i++) {
            name="Test_"+ifName+"_"+i;
            if (!className2SourceFile(name).exists()) break;
        }
        inEDTms( thrdCR(this,"EXPAND",StrapPlugins.allClassesV(interFace)),444);
        final BA source=readBytes(getClass().getResourceAsStream("hotswapExamples/"+ifName+"Example.java"));
        if (sze(source)==0) { StrapAlign.errorMsg("No example for "+shrtClasNam(interFace)+" available."); return; }
        do {
            final String MSG="Creating class of type "+ifName+".<br><br>Enter a name for the new class starting with a capital letter and containing letters, digits or underscore";
            name=ChMsg.input(MSG,30,name);
            if (name==null) return;
        } while(!(sze(name)>1 &&  is(UPPR,name,0) && cntainsOnly(LETTR_DIGT_US,name)));

        final File f=className2SourceFile(name);
        if (f.exists()) {
            StrapAlign.errorMsg("The file "+f+" already exists.");
            return;
        }
        wrte(f,source.replaceChar('\t',' ').replace(0L, ifName+"Example",name));
        if (!f.exists()){
            StrapAlign.errorMsg("Could not write  file "+f);
            return;
        }
        startThrd(new ChExec(0).setCommandsAndArgument(custSettings(Customize.javaSourceEditor),f.getAbsolutePath()));
    }
    /* <<< create <<< */
    /* ---------------------------------------- */
    /* >>> GUI  >>> */
    private final ChButton
        butApply=new ChButton(BUT_LAB_Apply).li(LI)
        .tt("The activated class is loaded and instantiated.<br>You need to select a class in the expandable tree.<br>Depending on the interfaces the class implements<br>an appropriate action is performed.<br>If the Ctrl-key is pressed simultaneously the class is discarded and reloaded."),
        butNew=new ChButton(BUT_LAB_New).li(LI).tt("Creates a new plugin.<br>You need to select an interface such as \"StrapExtension\" in the tree view."),
        butEdit=new ChButton("Edit / view source code").li(LI)
        .tt("loads the java source code into the editor<br>A second non editable view is opened (unless the Shift-key is pressed).");
    private final ChButton togCompiler=toggl("Automatic compilation on/off").tt("Turn off if you use another compiler.").s(true).li(LI);
    private void enableDisable(){
        butEdit.setEnabled(_theFile!=null||_theClass!=null || _proxyClass!=null || _theInterface!=null);
        butNew.setEnabled(_theInterface!=null);
        butApply.setEnabled(_theClass!=null || _proxyClass!=null);
    }
    /* <<< GUI <<< */
    /* ---------------------------------------- */
    /* >>> Selection  >>> */
    private File _theFile;
    private Class _theClass, _theInterface;
    private ProxyClass _proxyClass;
    private Object _selection;
    private void selectionChanged(Object o) {
        _theFile=null;
        _theClass=_theInterface=null;
        _proxyClass=deref(o, ProxyClass.class);
        if (_proxyClass!=null) _theFile=_proxyClass.getSourcefile();

        if (o instanceof Class) {
            final Class c=(Class)o;
            if (c.isInterface()) _theInterface=c;
            else _theFile=className2SourceFile(shrtClasNam(_theClass=c));
        }
        if (o instanceof String) _theFile=className2SourceFile((String)o);
        if (sze(_theFile)==0) _theFile=null;
        enableDisable();
        _toBeSelected=null;
    }
    /* <<< Selection <<< */
    /* ---------------------------------------- */
    /* >>> Sort Classes >>> */
    private final List
        vNO_INTERFACE=new UniqueList(Object.class).t("Classes not implementing a proper interface").i(IC_UNHAPPY),
        vNOT_COMPILED=new UniqueList(Object.class).t("Java files failed to compile").i(IC_UNHAPPY);
    private final Map<File,String> _mapFile2Interface=new HashMap();
    private void sortClasses() {
        final Class II[]=StrapPlugins.interfaces();
        for(int i=II.length; --i>=0;) {
            final List l=StrapPlugins.allClassesV(i);
            for(int j=sze(l);--j>=0;) {
                final Object o=l.get(j);
                if (o instanceof ProxyClass) l.remove(o);
            }
        }
        vNOT_COMPILED.clear();
        vNO_INTERFACE.clear();
        for(String f: lstDir(dirHotswap())) {
            if (endWith(".java",f)  && is(LETTR,f,0)) vNOT_COMPILED.add(delDotSfx(f));
        }
        for(ProxyClass pc : ProxyClass.getProxyClasses()) {
            final Class c=pc.getClassInstance();
            if (c!=null) {
                vNOT_COMPILED.remove(pc.getName());
                boolean hasInterface=false;
                for(int i=0;i<II.length; i++) {
                    final List l=StrapPlugins.allClassesV(i);
                    if (II[i].isAssignableFrom(c)) {
                        hasInterface=true;
                        if (!l.contains(pc)) { l.add(0,pc); }
                    } else l.remove(pc);
                }
                if (!hasInterface && !vNO_INTERFACE.contains(pc)) vNO_INTERFACE.add(pc);
                else vNO_INTERFACE.remove(pc);
            } else vNOT_COMPILED.add(pc.getName());
        }
        StrapPlugins.setChanged();
        updateAllNow(CHANGED_PLUGINS_ADDED_OR_REMOVED);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Implemented Interfaces >>> */
    public Object getPanel(int options) {return _tabPane;}
    public void dispose() { _running=false;}
    /* <<< Implemented Interfaces <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
}
