package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

A backup of the multiple sequence alignment data can be written to the
directory "./backup/.

The files in the backup-directory contain the gaps, the annotations to
amino acid and nucleotide positions, the nucleotide frame-shift and
the 3D-rotation and 3D-translation.

Using the dialog <i>ITEM:DialogRestoreFromBackup</i>, the saved
information is loaded by pressing
<i>LABEL:ChButton#BUTTON_GO</i>.

@author Christoph Gille
*/
public class DialogRestoreFromBackup extends AbstractDialogJPanel implements StrapListener,java.awt.event.ActionListener {

     private static ChButton
         _togFrame=toggl("restore nt-frame shift").s(true),
         _togTrans3D=toggl("restore 3D-transformation").s(true),
         _togAnno=toggl("restore resi-due annotations").s(true),
         _togGaps=toggl("restore alignment gaps").s(true);
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        StrapAlign.errorMsg("");
        final String cmd=ev.getActionCommand();
        if (cmd=="GO" || cmd=="D") {
            final int i=jList.getSelectedIndex();
            final File dir=i<0 ? null : file("backup/"+_listing.get(i));
            if (dir==null) {
                StrapAlign.errorMsg("A backup folder must be selected");
                return;
            }
            if (cmd=="GO") {
                Protein pp[]=StrapAlign.selectedProteins();
                if (pp==null||pp.length<1)
                    if (ChMsg.yesNo("No protein is selected. Do you want to restore all proteins?")) pp=StrapAlign.proteins();
                    else return;
                for(Protein p:pp){
                    String what="";
                    if (isSelctd(_togFrame))   what+="N";
                    if (isSelctd(_togGaps))    what+="G";
                    if (isSelctd(_togTrans3D)) what+="3";
                    if (isSelctd(_togAnno))    what+="A";
                    StrapAlign.readAttributes(what,p,dir);
                }
                StrapAlign.setNotSaved();
                StrapEvent.dispatchLater(StrapEvent.NUCL_TRANSLATION_CHANGED,111);
                StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_CHANGED,111);
            }
            if (cmd=="D") {
                if (ChMsg.yesNo("Really delete "+dir)) {
                    deleteTree(dir);
                    if (isDir(dir)) delFileOnExit(dir);
                    myUpdate();
                }
            }
        }

    }
    public DialogRestoreFromBackup() {
        setSettings(this, Customize.fileBrowsers);
        final Object
            pNorth=pnl(VBHB,dialogHead(this),"Select a backup. The date format is Year_Month_Day_Hour_Minute"),
            pTools=pnl(VBHB,
                       pnl(HBL, "File directory: ",fDirectory()),
                       new ChButton("D").t("Delete selected backup").li(this)
                       ),
            pSouth=pnl(VBPNL,
                       pnl(HBL, "Select the proteins to be restored. Then push the button ", new ChButton("GO").t(ChButton.GO).li(this)),
                       pnlTogglOpts("Tools:",pTools),
                       pTools),
            pEast=pnl(VB,_togGaps.cb(),_togAnno.cb(),_togTrans3D.cb(),_togFrame.cb()),
            pan=pnl(CNSEW,scrllpn(jList),pNorth, pSouth, pEast);
        remainSpcS(this,pan);
        myUpdate();
    }
    public void handleEvent(StrapEvent ev) {
        if (ev.getType()==StrapEvent.BACKUP_WRITTEN) myUpdate();
    }
    private final java.util.Vector<String> _listing=new java.util.Vector();
    private final JList jList=new JList(_listing);
    private File _fDir;
    private File fDirectory(){
        if (_fDir==null) _fDir=file("backup");
        mkdrs(_fDir);
        return _fDir;
    }
    private void myUpdate() {
        _listing.clear();
        adAll(lstDir(fDirectory()), _listing);
        sortArry(_listing);
        ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,jList,111,null);
    }
}
