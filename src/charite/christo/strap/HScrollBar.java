
package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.util.Arrays;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.KeyEvent.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   The View of an alignment window has a horizontal scrollbar which is used as a viewport.
   @author Christoph Gille
   Problems with AquaScrollBarUI: Only draggable at bottom
   Problems with WindowsScrollBarUI: Buttons too big
*/
public class HScrollBar extends ChScrollBar {
    private final StrapView _view;
    private boolean _divider, _clear, _knob=true;
    private Object _img;
    private int _val, _dividerWidth=EM, _w, _h;
    final long LAST[]=new long[ARRAY_DURATION];
    static {
        pcp(KOPT_METAL,"", HScrollBar.class);
    }
    public HScrollBar(StrapView v) {
        super(HORIZONTAL);
        _view=v;
    }

    /* ---------------------------------------- */
    /* >>> Image >>> */

    public HScrollBar clearImage() { _clear=true; return this;}
    private void paintImage(Graphics g,  boolean isOutline,int w,int height) {
        final int RGB_AFTER_SEQUENCE= getBackground().getRGB();
        final Protein pp[]=_view.visibleProteinsReordered(), secStruP=_view.secStruP();
        final int secStruH=secStruP==null?0:secStruH(), hSeq=height-secStruH;
        final int nP=pp.length, countCols=_view.countColumns();
        if (countCols<1 || nP==0 || hSeq<2 || w<2) { return;}
        final java.awt.image.BufferedImage imageLine=sharedImage('H',countCols);
        final int unit=countCols/w+1, rgb[]=sharedRGB(countCols+unit+1);
        /* --- gaps black --- */
        final int[] shadingColors=ShadingAA.rgb();
        if (shadingColors==null) return;
        final boolean showSecStru=ProteinList.isEnabled(pp,ProteinList.SECSTRU) && ShadingAA.i()==ShadingAA.SECSTRU;
        boolean isNewline=true;
        final Protein[] ppShading=_view.ppShading();
        final short[][] fff=isOutline ? _view.frequenciesAtColumn(0,MAX_INT,'c') : null;
        final int threshold=StrapView.sliderSimilarity().getValue();
        int firstCol=MAX_INT, lastCol=MIN_INT;
        for(int iP=0;iP<nP;iP++) {
            if (isNewline) {
                Arrays.fill(rgb,0,countCols, 0x999999);
                firstCol=MAX_INT;
                lastCol=MIN_INT;
                isNewline=false;
            }
            final int y1=hSeq*iP/nP, y2=mini(hSeq*(1+iP)/nP,hSeq);
            final Protein p=pp[iP];
            if (p==null) continue;
            final boolean isShading=isOutline && (ppShading==pp || cntains(p,ppShading));
            final int nRes=p.countResidues(), cols[]=p.getResidueColumn(), nCols=nRes==0 ? 0:p.getMaxColumnZ();
            if (nRes==0||nCols==0) continue;
            if (firstCol>cols[0]) firstCol=cols[0];
            if (lastCol<cols[nRes-1]) lastCol=cols[nRes-1];
            if (!isShading) drawSelection(p,rgb,unit);
            if (y1!=y2 || iP==nP-1) {
                isNewline=true;
                final byte ss[]=showSecStru && isShading ? p.getGappedSecStru() : null;
                final byte[] gapped=p.getGappedSequence();
                for(int col=mini(countCols,nCols,gapped.length);--col>=0;) {
                    final int rt=gapped[col]|32;
                    if (rt<'a' || rt>'z') rgb[col]=0;
                    else if (isShading) {
                        final int c=(rt|32)-'a';
                        boolean cons=false;
                        if (fff!=null && fff.length>col && fff[col]!=null && c>=0 && c<='z'-'a') {
                            final int total=fff[col]['z'-'a'+1], t100=threshold*total/100, f=fff[col][c];
                            cons= threshold>=0 ? f>=t100 : f<=total+t100;
                        }
                        if (ss==null) {
                            if (cons) rgb[col]=shadingColors[rt];
                        } else if (col<ss.length) {
                            final byte s=ss[col];
                            rgb[col]= s=='H' ? (cons?0xff0000:0xaa0000) : s=='E' ? (cons?0xffff00:0xaaaa00) :0;
                        }
                    }
                }

                Arrays.fill(rgb,0,mini(countCols,firstCol+1), _knob?RGB_AFTER_SEQUENCE:0);
                if (lastCol<countCols) Arrays.fill(rgb, maxi(0,lastCol+1), countCols, _knob?RGB_AFTER_SEQUENCE:0);

                try {
                    imageLine.setRGB(0,0,countCols,1,rgb,0,countCols);
                    g.drawImage(imageLine,0,y1,w,y2, 0,0,countCols,1,this);
                } catch(Throwable ex) { if (myComputer()) stckTrc(ex); }
            }
        }
        if (secStruH>0) {
            final byte ss[]=secStruP.getGappedSecStru();
            final int N=mini(rgb.length, sze(ss), secStruP.getMaxColumnZ()+1);
            if (N<countCols) Arrays.fill(rgb,N, countCols,RGB_AFTER_SEQUENCE);
            for(int col=N; --col>=0;) rgb[col]=ss[col]=='E'?  0xb2b200 : ss[col]=='H' ? 0xFF0000 : DEFAULT_BACKGROUND;
            try {
                g.setColor(C(DEFAULT_BACKGROUND));
                g.drawLine(0,hSeq,w,hSeq);
                imageLine.setRGB(0,0,countCols,1,rgb,0,countCols);
                g.drawImage(imageLine,0,hSeq+1,w,hSeq+secStruH+1, 0,0,countCols,1,this);
            } catch(Throwable ex) { stckTrc1(ex); }
        }
    }
    /* <<< Image <<< */
    /* ---------------------------------------- */
    /* >>> paintComponent >>> */
    public int secStruH() {
        final Protein p=_view.secStruP();
        return null==(p==null?null:p.getGappedSecStru())?0:5;
    }
    @Override public void paintComponent(Graphics g) {
        final long time=System.currentTimeMillis();
        final int totalH=hght(this), hSeq=maxi(1,totalH-secStruH()), nCols=_view.countColumns(), value=getValue();
        final Rectangle visible=_view.visibleXYWH();
        if (value!=_val) {
            StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_SCROLLED,99);
            _val=value;
        }
        if (nCols<1) return;
        final int tw,tx,ty,  width, trx;
        {
            final Rectangle track=getTrack(), thumb=getThumb();
            tw=maxi(2, wdth(thumb)-2);
            tx=x(thumb)+1;
            ty=y(thumb);
            if (track==null) { super.paintComponent(g); return; }
            width=maxi(1, wdth(track));
            trx=x(track);
        }

        {
            g.setColor(getBackground());
            if (totalH==1) { g.fillRect(0,0, wdth(this),1); return;}
            g.fillRect(0,0,trx,hSeq);
            g.fillRect(trx+width,0,hSeq,999);
            final Image image;
            {
                Image i=(Image)deref(_img);
                final int iH=hght(i), iW=wdth(i);
                if (iH<hSeq || iW<width) i=null;
                if (_clear || i==null || _w!=width || _h!=hSeq) {
                    _w=width;
                    _h=hSeq;
                    _clear=false;
                    if (i==null)  _img=newSoftRef(i=createImage(width,totalH+10));
                    paintImage(i.getGraphics(),_view.isShadingScrollbar(),width,totalH);
                    i=(Image)deref(_img);
                }
                image=i;
            }
            if (image==null) return;
            g.drawImage(image,trx,0, trx+width, totalH,  0,0,width, totalH,this);
            if (_view.countRows()==0 || nCols<5) return;
            if (_knob && visible.width<_view.paneSize().width) {
                final int
                    hPan=_view.paneSize().height,
                    y1= y(visible)*hSeq/hPan,
                    y2=y2(visible)*hSeq/hPan;
                if (visible.height<hPan) {
                    g.setColor(C(0x8080ff,0x80));
                    g.fillRect(tx,0,tw,y1);
                    g.fillRect(tx, y2, tw,hSeq-y2);
                    g.setColor(C(0xFF));
                    g.drawLine(tx, y1,tx+tw,y1);
                    g.drawLine(tx, y2,tx+tw,y2);
                } else {
                    g.setColor(C(0x8080ff,0x60));
                    g.fillRect(tx,ty,tw,hSeq);
                }
                g.setColor(C(0xFF));
                g.drawLine(tx,0,tx,hSeq-1);
                g.drawLine(tx+tw,0, tx+tw, hSeq-1);
            }
            final Rectangle r=_view.rectCursor(this);
            if (r.width>0) {
                g.setColor(_view.resSelCursor().getColor());
                ((Graphics2D)g).fill(r);
            }
            {
                /* --- ValueOfAlignPosition --- */
                g.translate(trx,0);
                _view.plot(g,this,width/(double)nCols,0,hSeq);
                g.translate(-trx,0);
            }
            if (_divider) drawDivider(this,g,0,0,9999,_dividerWidth);
            LAST[DURATION]=(LAST[WHEN]=System.currentTimeMillis())-time;
            paintHooks(this, g, true);
        }
    }
    private static void drawSelection(Protein p,int rgb[], int unit) {
        final int cols[]=p.getResidueColumn(), nR=p.countResidues();
        if (cols==null || cols.length<nR) return;
        for(ResidueSelection s : p.allResidueSelections()){
            if (!(s instanceof VisibleIn123)) continue;
            final VisibleIn123 shown= (VisibleIn123)s;
            if ((shown.getVisibleWhere()&VisibleIn123.SB)==0) continue;
            final Color c=shown.getColor();
            final int color=c!=null ? c.getRGB() : 0;
            final boolean selAA[]=ResSelUtils.selectedAminosDisplayed(s);
            if (selAA==null || selAA.length==0) continue;
            final int selOffset=s.getSelectedAminoacidsOffset()-Protein.firstResIdx(p);
            final int iMax=mini(selAA.length+selOffset, nR);
            for(int iAa=maxi(0,selOffset); iAa<iMax; iAa++) {
                if (selAA[iAa-selOffset]) {
                    final int col=cols[iAa];
                    for(int i=unit+col; --i>=col;) {
                        if (i<rgb.length) rgb[i]=color;
                    }
                }
            }
        }
    }
    /* <<< paintComponent <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private char _dragged;
    private int _dragY,  _maxDragX, _minDragX, _dragFromY;
    @Override public void processEvent(AWTEvent ev) {
        final int
            id=ev.getID(),
            modi=modifrs(ev),
            y=y(ev),
            x=x(ev);
        boolean overDivider=false;
        final boolean knob=
            id==MOUSE_CLICKED ? 0==(modi&SHIFT_MASK)*(modi&CTRL_MASK) :
            id==MOUSE_DRAGGED ? true :
            _knob;
        if (knob!=_knob) { _img=null; _knob=knob; repaint(); }
        {
            final boolean oldLooksLikeSlider=_divider;
            overDivider= y<=_dividerWidth;
            _divider=overDivider && id==MOUSE_MOVED || _dragged=='Z';
            if (_divider!=oldLooksLikeSlider) repaint();
            if (id==MOUSE_MOVED) {
                setCursor(cursr(y<_dividerWidth ? 'n' : 'M'));
                _dragged=0;
            }
            if (id==MOUSE_ENTERED  && 0==(modi&(BUTTON1_MASK|BUTTON2_MASK|BUTTON3_MASK)))  _dragged=0;
            if (id==MOUSE_DRAGGED) {
                if (_dragged==0) {
                    _dragged= overDivider ? 'Z' :  's';
                    _dragFromY=y;
                    _maxDragX= _minDragX=x;
                }
                else if (_dragged=='Z') {
                    setPreferredSize(dim(1, mini(StrapAlign.panelHeight()*3/4, maxi(EX, hght(this)-y))));
                    StrapView.correctHeight();
                    _dividerWidth=EM*2/3;
                    ChDelay.revalidate(this,99);
                }
            }
            if (_dragged=='s') {
                final int dy=y-_dragFromY;
                if ( (_maxDragX=maxi(_maxDragX,x))-(_minDragX=mini(_minDragX,x))<(dy>0?dy:-dy)-1  || (modi&ALT_MASK)!=0)  _dragged='S';
            }
            if (_dragged=='S') {
                _view.scrollRel('v', (y-_dragY)*_view.paneSize().height/(1+ hght(this)),false);

            }
            _dragY=y;
            if (id==MOUSE_RELEASED ||id==MOUSE_EXITED && _dragged==0) {
                _divider=false;
                repaint();
            }
        }
        if (_dragged!='Z' && !(overDivider&&id==MOUSE_PRESSED)) {
            try { super.processEvent(ev);} catch(Throwable e){}
        }
    }
    /* <<< AWTEvent <<< */
}
