package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;

import javax.swing.JComponent;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

For a given protein the following steps are performed:

<ol>

<li>
If no PDB ID is associated with the protein, the most similar PDB structure is identified using Blast.
This step is time consuming.
</li>

<li>
Using the PDB id, lists of structurally similar proteins are retrieved from
two different Web-services.
<ul>
<li>GangstaPlus* PUBMED:17118190</li>
<li>Dali* PUBMED:8578593</li>
</ul>
Similarity is generally based on structural similarity
irrespectively of the amino acid sequence.
</li>

</ol>

@author Christoph Gille
*/
public class DialogNeighbors extends AbstractDialogJTabbedPane implements  StrapListener, Runnable {
    private final ProteinList _listPP=new ProteinList(0);
    public DialogNeighbors() {
        final Object pMain=pnl(CNSEW,
                               scrllpn(_listPP),
                               dialogHead(this),
                               pnl(HBL, "Select a protein and press ",new ChButton("GO").t(ChButton.GO).r(this))
                               );
        adMainTab(remainSpcS(pMain),this,null);
    }

    public void run() {
        assrtEDT();
        final Protein[] pp=_listPP.selectedProteins();
        if (pp.length==0) error("No protein selected");
        for(Protein p : pp) {
            final DialogNeighborsResult2 result=new DialogNeighborsResult2(p);
            final Runnable run=thrdEdtLater(thrdCR(result,"RUN",this));
            if (p.getAnyPdbID()==null) {
                final JComponent dia=StrapAlign.addDialogC(CLASS_DialogSimilarStructure);
                pcp(KEY_RUN_AFTER, run, dia);
                runR(SPUtils.dialogSetProteins(ProteinList.NO_PDB_ID, new Protein[]{p}, dia));
            } else runR(run);
        }

    }

    public void handleEvent(StrapEvent ev) {
        final int type=ev.getType();
        if (type==StrapEvent.PROTEINS_ADDED||type==StrapEvent.PROTEINS_KILLED) {
            for(java.awt.Component c : getComponents()) {
                final DialogNeighborsResult2 result=deref(c,DialogNeighborsResult2.class);
                if (result!=null) result.highlight();
            }
        }
    }

}
