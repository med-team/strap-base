package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
class DialogAlignOneToAllAlignment extends BasicResidueSelection implements PreferredSize, Disposable, ActionListener, Runnable, Comparable, ChRunnable, PaintHook, ProcessEv {
    private final static String NAME="MATCHING_";
    private final Point ORG=new Point();
    private final SequenceAligner _a;
    private String _labScoreTxt, _tip;
    private JComponent _pMain, _ali, _butUndo;
    private final Protein _pp[];
    private final DialogAlignOneToAllResult _result;
    private boolean _painted;
    private float _score;
    private int _sliderV=MIN_INT;
    private final int _idx;
    private ChButton _labScore;

    public DialogAlignOneToAllAlignment(Protein[] pp, SequenceAligner al, int idx, DialogAlignOneToAllResult result) {
        super(0);
        _idx=idx;
        _result=result;
        _pp=pp;
        setProtein(pp[1]);
        setName(NAME+shrtClasNam(al));
        _a=al;
        SPUtils.setSeqs(_a, pp);

    }

    public JComponent panel() {
        if (_pMain==null) {
            _pMain=pnl(HB,"Calculating, please wait",KOPT_NOT_INDICATE_TOO_SMALL);
            _butUndo=new ChButton(ChButton.DISABLED,"U").t("Undo").li(this);
            setColor(C(0xFF00FF));
            setPrefSze(this, _ali=pnl(KOPT_TRACKS_VIEWPORT_HEIGHT));

            _labScore=ChButton.doCtrl(_a);
            if (_labScore!=null) _labScore.t(_labScoreTxt).tt(_tip); else _labScore=labl(_labScoreTxt);
            final ProteinLabel proteinLabel= new ProteinLabel(0L, _pp[1]);
            setBG(DEFAULT_BACKGROUND,proteinLabel);
            rtt(proteinLabel);
            final Object
                tf=new ChTextField().cols(20,true,true).li(this),
                butApply=new ChButton("A").t("Infer alignment").li(this)
                .tt("<b>Accept & apply to the alignment</b><br>Only the protein "+_pp[0]+" is changed.<br>The protein "+_pp[1]+" is not changed "+_tip),
                pWest=pnl(tf, _labScore," ", butApply,_butUndo, KOPT_NOT_INDICATE_TOO_SMALL),
                rowHeader=pnl(CNSEW,pWest,pnl(" "), KOPT_NOT_INDICATE_TOO_SMALL, new ChButton().doClose(CLOSE_DISPOSE, this),proteinLabel);
            pcp(KEY_IF_EMPTY,"For notes",tf);
            final ChJScrollPane sp=scrllpn(SCRLLPN_INHERIT_SIZE, _ali);
            final JScrollBar sb=getSB('H',_ali);
            noBrdr(sp);
            sp.getViewport().setBackground(C(DEFAULT_BACKGROUND));
            sb.setUnitIncrement(EX);
            addPaintHook(this,sb);

            pnl(_pMain, CNSEW, sp, rowHeader);
            setDragScrolls('V',_ali, scrllpn(_result._jt));
            setDragScrolls('H',_ali, sp);
            addPaintHook(this,_ali);

        }
        return _pMain;

    }

    static final String RUN_ENABLE="RE", RUN_SLIDER="SLIDER";
    public Object run(String id, Object arg) {
        final float[] minMaxScore=_result.minMaxScore;
        if (id==RUN_ENABLE && _labScore!=null && minMaxScore[1]!=minMaxScore[0]) _labScore.setForeground(float2blueRed((_score-minMaxScore[0])/(minMaxScore[1]-minMaxScore[0])));
        if (id==RUN_SLIDER) {
            _sliderV=atoi(arg);
            getSelectedAminoacids();
        }
        return null;
    }
    public int compareTo(Object o) {
        if (!(o instanceof DialogAlignOneToAllAlignment)) return 0;
        final DialogAlignOneToAllAlignment al=(DialogAlignOneToAllAlignment)o;
        final float v1,v2;
        if (isSelctd(_result._cbSortScore)) { v1=al._score; v2=_score; } else {v1=al._idx; v2=_idx;}
        return v1<v2 ? -1 : v1>v2 ? 1 :0;
    }
    /* <<< Comparable  <<< */
    /* ---------------------------------------- */
    /* >>> Dispose >>> */
    public void dispose() {
        _result.vALignments.remove(this);
        _result._jt.tableChanged(new TableModelEvent(_result));
    }
    /* <<< Dispose  <<< */
    /* ---------------------------------------- */
    /* >>> paint >>> */
    public boolean paintHook(JComponent component, Graphics g, boolean after) {
        final Protein pp[]=_pp;
        final Font font=getPnl(_result).getFont();
        getPnl(_result).setFont(font);
        g.setFont(font);
        final byte[][] seqs=_a.getAlignedSequences();
        if (sze(seqs)<2 ||  0==sze(seqs[0])*sze(seqs[1])) return true;
        final byte[] s0=seqs[0], s1=seqs[1];
        final Color bg=C(isWhiteBG() ? 0xFFffFF : 0);
        final int h=component.getHeight(), w=component.getWidth();
        if (component instanceof ChScrollBar && after) {
            final byte[][] BLOSUM=Blosum.BLOSUM62;
            final ChScrollBar sb=(ChScrollBar)component;
            final Rectangle track=sb.getTrack(), thumb=sb.getThumb();
            final int L=maxi(s0.length, s1.length), rgb[]=sharedRGB(L);
            final java.awt.image.BufferedImage image=sharedImage('H',L);
            g.setColor(C(0x8080ff,0x80));
            for(int i=L; --i>=0;) {
                final int c1=s1.length>i? s1[i]|32:0;
                final int c0=s0.length>i? s0[i]|32:0;
                rgb[i]=c0==c1?0xffFFffFF : BLOSUM[c0&255][c1&255]>0 ? 0x777777 : 0;
            }
            image.setRGB(0,0,L,1,rgb,0,L);
            g.drawImage(image,x(track),0,  x2(track),h, 0,0,L,1,_ali);
            g.fillRect(x(thumb),0, thumb.width,thumb.height);
            g.setColor(C(0xff));
            g.drawRect(x(thumb),0, thumb.width,thumb.height);
            g.setColor(C(DEFAULT_BACKGROUND));
            g.fillRect(0,0,x(track),99);
            g.fillRect(x2(track),0,9999,99);
        }
        if (component==_ali && after) {
            final Rectangle cb=chrBnds(g.getFont());
            final int charW=cb.width, charH=cb.height;
            g.setColor(bg);
            g.fillRect(0,0,w,99999);
            final int mode=ShadingAA.i()==ShadingAA.SECSTRU ? DrawGappedSequence.SHADE_SEC_STRU:0;
            int firstL=0,lastL=MAX_INT;
            for(int iS=2; --iS>=0;) {
                final byte[] s=seqs[iS];
                final int startsAt=idxOfLetters(s,pp[iS].getResidueTypeExactLength());
                if (startsAt>=0) {
                    ORG.y=(charH+1)*iS*2;
                    _result.dgs.draw(_ali,g,ORG, maxi(0, startsAt),s,0,MAX_INT, pp[iS], ShadingAA.colors(false),mode);
                    g.setColor(C(DEFAULT_BACKGROUND));
                    final int fL=nxt(LETTR,s),lL=prev(LETTR,s);
                    if (firstL<fL) firstL=fL;
                    if (lastL>lL) lastL=lL;
                    g.fillRect(0,iS*charH, fL*charW, charH*2+2);
                    g.fillRect((lL+1)*charW, iS*charH, w, charH*2+2);
                }
            }
            if (!_painted) { _ali.scrollRectToVisible(new Rectangle(firstL*charW,0,(lastL-firstL)*charW,1)); _painted=true;}
            ORG.y=charH+1;
            g.setColor(C(0xFFffFF));
            _result.dgs.drawBlastMidline(s0,s1, 0, MAX_INT, Blosum.BLASTMIDLINE, g, 0, ORG.y);

            final int prefHeight=prefH(panel()); // 3*cb.height+3+ICON_HEIGHT+EX+4;
            final ChJTable jT=_result._jt;
            if (jT.getRowHeight()!=prefHeight) {
                jT.setRowHeight(prefHeight);
                revalAndRepaintC(jT);
            }
        }
        return true;
    }
    @Override public Object getRenderer(long options, long rendOptions[]) { return panel();}
    /* <<< paint <<< */
    /* ---------------------------------------- */
    /* >>> ChRunnable >>> */
    public void run() {
        final Protein pp[]=_pp;
        setSharedParas(_result._sharedInst,_a);
        _a.compute();
        final byte[][] seqs=_a.getAlignedSequences();
        if (seqs==null || seqs.length<2) return;

        _score=scor(_a);
        if (!Float.isNaN(_score)) {
            _labScoreTxt="score="+_score;
        } else {
            final Ratio r=AlignUtils.getIdentity(seqs[0],seqs[1],0,9999999);
            _score= r.getDenominator()==0 ? 0:r.getNumerator()/(float)r.getDenominator();
            _labScoreTxt="ident="+r;
            _tip="<br>identical amino acid "+r.getNumerator()+"<br>in "+r.getDenominator()+" positions";
        }
        if (_labScore!=null) _labScore.t(_labScoreTxt).tt(_tip);
        final float[] minMaxScore=_result.minMaxScore;
        if (minMaxScore[0]>_score) minMaxScore[0]=_score;
        if (minMaxScore[1]<_score) minMaxScore[1]=_score;
    }
    /* <<< ChRunnable <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */

    public void processEv(AWTEvent ev) {
        if (ev.getSource()==_ali) _result._jt.processEvent(ev);
    }

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="A" || cmd=="U") {
            final Protein pp[]=_pp;
            final int[] gaps, gapsSaved=gcp(_butUndo, pp[0], int[].class);
            if (cmd=="A") {
                final byte[] seqs[]=_a.getAlignedSequences();
                if (sze(seqs)==2) {
                    final byte[] mGapped=seqs[0], mResidues=pp[0].getResidueTypeExactLength();
                    final byte[] tGapped=seqs[1], tResidues=pp[1].getResidueTypeExactLength();
                    final int[] tGaps=pp[1].getResidueGap(), mGaps=pp[0].getResidueGap();
                    if (gapsSaved==null) pcp(_butUndo, pp[0].getResidueGap().clone(),pp[0]);
                    gaps=AlignUtils.useGapsFromExternalAlignment(tGapped, mGapped,tResidues,  mResidues,  tGaps, mGaps);
                } else gaps=null;
            } else gaps=gapsSaved;
            if (gaps!=null) pp[0].setResidueGap(gaps.clone());
            StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_CHANGED,111);
            StrapAlign.setNotSaved();
            setEnabld(cmd!="U", _butUndo);
        }
    }

    private boolean _selAA[];
    private int _selSlider, _selMC, _selBlosums[];
    private final static int WINDOW=4;

    @Override public int getSelectedAminoacidsOffset() { return _pp[1].getResidueIndexOffset(); }

    @Override public boolean[] getSelectedAminoacids() {
        final int slider=_sliderV;
        boolean bb[]=_selAA;
        final Protein p=_pp[1];
        final byte[] rt=p.getResidueTypeFullLength();
        final int mc=p.mc(ProteinMC.MC_RESIDUE_TYPE);
        if (bb==null || _selSlider!=slider || _selMC!=mc) {
            _selSlider=slider;
            _selMC=mc;
            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS, p);
            final byte[][] BLOSUM=Blosum.BLOSUM62, seqs=_a.getAlignedSequences();
            if (sze(seqs)<2 || 0==sze(seqs[0])*sze(seqs[1])) return null;
            final byte[] s0=seqs[0], s1=seqs[1];
            final int L=mini(s0.length, s1.length);
            bb=_selAA=redim(bb, rt.length, 0);
            if (_selBlosums==null) {

                _selBlosums=new int[bb.length];
                final int startsAt=maxi(0,idxOfLetters(s1,rt));
                if (startsAt<0) return null;
                for(int col=0, a=startsAt; col<L; col++) {
                    int sum=0;
                    window:
                    for(int k=-WINDOW+col; k<=WINDOW+col; k++) {
                        if (k<0 || k>=L) continue;
                        if (!is(LETTR,s0,k) || !is(LETTR,s1,k)) {
                            sum=MIN_INT;
                            break window;
                        }
                        sum+=BLOSUM[s0[k]&127][s1[k]&127];
                    }
                    if (a<_selBlosums.length) _selBlosums[a]=sum;
                    if (is(LETTR,s1,col)) a++;
                }
            }
            for(int i=0; i<bb.length; i++)  bb[i]=i<_selBlosums.length && _selBlosums[i]>slider;
        }
        return bb;
    }

    public Dimension preferredSize(Object c) {
        if (c==_ali && c!=null && _a!=null) {
            final Rectangle cb=chrBnds(getPnl(_result).getFont());
            final byte[][] seqs=_a.getAlignedSequences();
            if (seqs!=null && cb!=null) return dim(cb.width*maxi(seqs[0].length,seqs[1].length), 3*cb.height+3);
        }
        return dim(EX,EX);
    }
}

