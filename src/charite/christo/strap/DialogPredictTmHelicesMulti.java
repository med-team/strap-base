package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.extensions.Hydrophobicity;
import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.event.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class DialogPredictTmHelicesMulti extends DialogPredictTmHelices2 implements StrapListener {
    private ProteinList proteinList;
    private ChJList classList;
    private int iResult;
    @Override public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="GO") {
            final Protein pp[]=proteinList.selectedProteinsAtLeast(1);
            if (sze(pp)==0) return;
            final Object classes[]=classList.getSelectedValues();
            if (sze(classes)==0) StrapAlign.errorMsg("Select at least one method");
            else adTab(DISPOSE_CtrlW,  toStrg(iResult++),new ResultPane(pp,classes), this);
        } else super.actionPerformed(ev);
    }
    private ChCombo _settings;
    private ChCombo comboSettings() {
        if (_settings==null) _settings=SPUtils.classChoice(TransmembraneHelix_Predictor.class).classRenderer(HasSharedControlPanel.class);
        return _settings;
    }
    private final ChButton butSettings() {
        return ChButton.doView(
                               pnl(VBPNL,
                                   "method",
                                   comboSettings(),
                                   " ",
                                   ChButton.doSharedCtrl(comboSettings()).cln().t("Settings"),
                                   helpBut(comboSettings()).t("Documentation"),
                                   smallSourceBut(comboSettings())
                                   )
                               ).t("More ...");
    }
    @Override public JTabbedPane getPanel() {
        proteinList=new ProteinList(0L);
        classList=new ChJList(StrapPlugins.allClassesV(TransmembraneHelix_Predictor.class),ChJTable.CLASS_RENDERER);
        final Object
            pCenter=pnl(new GridLayout(1,2),proteinList.scrollPane(),scrllpn(classList)),
            pSouth=pnl(new GridLayout(1,3),
                       new ChButton("GO").t(ChButton.GO).li(this),
                       butSettings(),buttn(TOG_CACHE).cb()
                       );

        adMainTab(pnl(CNSEW,pCenter,dialogHead(this),pSouth), this,null);
        StrapAlign.addListener(this);
        return this;
    }

    private class ResultPane extends JPanel implements javax.swing.table.TableModel,CellEditorListener,Disposable,javax.swing.table.TableCellRenderer,Runnable,PaintHook,
                                                       java.awt.event.ActionListener,
                                                       java.awt.event.MouseMotionListener {
        private final JComponent panOverview=pnl();
        {addPaintHook(this,panOverview).addMouseMotionListener(this);}

        public void mouseDragged(java.awt.event.MouseEvent ev) {
            final Protein p=proteins[0];
            final StrapView v=StrapAlign.alignmentPanel();
            if (p!=null && v!=null) {
                int col=p.getResidueColumnZ(maxi(0,ev.getX())*p.countResidues()/panOverview.getWidth());
                if (col<0) col=p.getMaxColumnZ();
                v.hsb().setValue(col*v.charB().width);
                panOverview.repaint();
            }
        }
        public void mouseMoved(java.awt.event.MouseEvent ev) {}

        public boolean  paintHook(JComponent c,Graphics g, boolean after) {
            if (after && c==panOverview && sze(proteins)==1) {
                final Protein p=proteins[0];
                final int width=c.getWidth(),height=c.getHeight(),nRes=p.countResidues();
                if (nRes==0) return false;
                final int rgb[]=new int[nRes];
                final  java.awt.image.BufferedImage imageLineT=new  java.awt.image.BufferedImage(nRes,1, java.awt.image.BufferedImage.TYPE_INT_ARGB);
                final int nR=results.length;
                for(int iR=nR; --iR>=0;) {
                    final BasicResidueSelection ss[]=results[iR].selections();
                    for(int iS=0; iS<ss.length; iS++) {
                        if (ss[iS].getProtein()!=p) continue;
                        ss[iS].getRGB(rgb);
                        imageLineT.setRGB(0,0,nRes,1, rgb,0,nRes);
                        final int y=height*iR/nR;
                        final int h=height*(iR+1)/nR-y-1;
                        try {
                            g.drawImage(imageLineT,0,y,width,y+(h>0 ? h : 1), 0,0,nRes,1,this);
                        } catch(Exception e){}
                    }
                }
                final StrapView sv=StrapAlign.alignmentPanel();
                if (sv!=null) {
                    final Rectangle r=sv.visibleRowsAndCols();
                    final int from=p.column2nextIndexZ(x(r))*width/nRes;
                    final int to=p.column2thisOrPreviousIndex(x2(r))*width/nRes;
                    g.setColor(C(0x8888FF,128));
                    if (from<to) g.fillRect(from,0,to-from,height);
                }
            }
            return true;
        }
        public void actionPerformed(java.awt.event.ActionEvent ev) {
            final String cmd=ev.getActionCommand();
            final Object q=ev.getSource();
            revalidate();
            showHide();
            if (cmd=="TXT" && proteins!=null && results!=null) {
                final Protein ppAli[]=new Protein[(1+results.length)*proteins.length];
                for(int iR=0;iR<results.length;iR++) {
                    final Protein pp[]=results[iR].getProteins();
                    for(int iP=0; iP<pp.length; iP++) {
                        ppAli[iP*(results.length+1)]=pp[iP];
                        final Protein pPredict=new Protein();
                        final char[][] predictions=results[iR].predictor().getPrediction();
                        if (predictions!=null && predictions.length>0) {
                            final char[] pr=predictions[0];
                            if (pr!=null) {
                                pPredict.setResidueType(new String(pr).replace((char)0,'x').replace(' ','x'));
                                final Protein p=pp[iP];
                                pPredict.inferGapsFromGappedSequence(p.getGappedSequence());
                                pPredict.setGappedSequence(toStrg(pPredict.getGappedSequenceExactLength()).replace('x',' '));
                            }
                        }
                        final String name=shrtClasNam(results[iR].predictor());
                        pPredict.setName(name);
                        ppAli[iP*(results.length+1)+iR+1]=pPredict;
                    }
                }
                final int residuesPerLine=atoi(ChMsg.input("The result will be shown as a multiple alignment.<br>Long sequence lince will be broken.<br>How many residues per line ?",7,"60"));
                final BA sb=new BA(9999);
                final ExportAlignment w=new ExportAlignment();
                w.setProteins(ppAli);
                w.setResiduesPerLine(residuesPerLine);
                w.getText(ExportAlignment.CLUSTALW|ExportAlignment.FILE_SUFFIX,sb);
                shwTxtInW("",sb);
            }
            if (q==togSingle || q==togConsensus) {
                final StrapView sv=StrapAlign.alignmentPanel();
                if (sv!=null) revalAndRepaintC(sv);

            }
        }
        public void dispose() {
            for(Result r:results) r.dispose();
            for(BasicResidueSelection c:consensus) {
                final Protein p=c==null?null:c.getProtein();
                if (p!=null) { p.removeResidueSelection(c); }
            }
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
            for(StrapView sv: StrapAlign.alignmentPanels()) {
                if (sv==null) continue;
                for(ValueOfResidue[] vv : hydrophobicity)
                    for(ValueOfResidue v : vv)
                        sv.getPlottersV().remove(v);
            }
            StrapEvent.dispatchLater(StrapEvent.VALUE_OF_RESIDUE_CHANGED,111);
        }
        private final Protein proteins[];
        private final BasicResidueSelection consensus[];
        private final Object classes[];
        private final ChJTable table;
        private final Result results[];
        private final ChButton
            butCtrl[],
            togProteins[],
            togProteinsH[],
            togMethods[],
            togHydro[],
            togConsensus=toggl("Show consensus").li(ResultPane.this),
            togSingle=toggl("Show single").li(ResultPane.this);
        private final ChCombo comboConsensusMethod;

        ResultPane(Protein[] pp,Object[] cc) {
            {
                final List v=new ArrayList();
                for(int i=0;i<cc.length;i++) v.add("> "+i);
                comboConsensusMethod=new ChCombo(0L, v.toArray()).li(this);
            }
            classes=cc;
            proteins=pp;
            hydrophobicity=new ValueOfResidue[proteins.length][Hydrophobicity.SCALE_NAMES.length];
            consensus=new BasicResidueSelection[proteins.length];
            table=new ChJTable(this,ChJTable.ICON_ROW_HEIGHT|ChJTable.NO_REORDER).renderer(this);
            table.setCellSelectionEnabled(false);
            table.setDefaultEditor(Object.class,_chRen);
            table.getTableHeader().setDefaultRenderer(this);
            table.getTableHeader().setCursor(cursr('M'));
            togMethods=new ChButton[classes.length];
            togHydro=new ChButton[Hydrophobicity.SCALE_NAMES.length];
            for(int i=togMethods.length; --i>=0;){
                togMethods[i]=toggl(ChButton.PAINT_IN_TABLE|ChButton.ICON_SIZE|ChButton.RENDERER_SELF,null).i(IC_SHOW).s(true).li(this);
            }
            for(int i=togHydro.length; --i>=0;){
                togHydro[i]=toggl(ChButton.PAINT_IN_TABLE|ChButton.ICON_SIZE|ChButton.RENDERER_SELF, null).i(IC_SHOW).li(this);
            }

            togProteins=new ChButton[proteins.length];
            for(int i=togProteins.length; --i>=0;) {
                togProteins[i]=toggl(ChButton.PAINT_IN_TABLE|ChButton.ICON_SIZE|ChButton.RENDERER_SELF,null).i(IC_SHOW).s(true).li(this);
            }

            togProteinsH=new ChButton[proteins.length];
            for(int i=togProteinsH.length; --i>=0;) {
                togProteinsH[i]=toggl(ChButton.PAINT_IN_TABLE|ChButton.ICON_SIZE|ChButton.RENDERER_SELF,null).i(IC_SHOW).s(true).li(this);
            }

            final Object
                pButtons=pnl(
                             pnl("#TB consensus from all predictions",comboConsensusMethod,togConsensus.cb()),
                             togSingle.cb(),
                             new ChButton("TXT").li(this).t("export as text")
                             ),
                pNorth=pnl(CNSEW,null,null,null,new ChButton().doClose(CLOSE_RM_CHILDS, this)),
                sp=scrllpn(0,table,dim(32,32));
            Object pCenter=sp;
            if (pp.length==1) {
                pCenter=new JSplitPane(JSplitPane.VERTICAL_SPLIT,panOverview, (Component)sp);
                panOverview.setPreferredSize(dim(32,32));
                setBG(0,panOverview);
            }
            pnl(this,CNSEW,pCenter,pNorth,pButtons);
            butCtrl=new ChButton[classes.length];
            results=new Result[classes.length];
            int butCtrlW=10*EM;
            for(int i=0;i<classes.length;i++) {
                final PredictionFromAminoacidSequence predictor=newPredictor(proteins,classes[i]);
                pcp(KEY_RUN_AFTER, this, results[i]=new Result(predictor,proteins,getClass()));
                results[i].togVisibleSB().s(false);
                final ChButton b=ChButton.doCtrl(predictor);
                if (b!=null) {
                    butCtrl[i]=b.t("Details").i("").setOptions(ChButton.PAINT_IN_TABLE);
                    butCtrlW=prefW(b)+ChJTable.COLUMN_EXTRA_WIDTH;
                }
            }
            for(int i=getColumnCount(); --i>=0;) {
              if (i!=1) table.setColWidth(false, i, i==0 ?  butCtrlW : ICON_HEIGHT+1);
            }
            _chRen.addCellEditorListener(this);
            showHide();
        }
        public void run() { revalAndRepaintC(table); showHide();}
        final ValueOfResidue hydrophobicity[][];
        private void showHide() {
            final StrapView sv=StrapAlign.alignmentPanel();
            final boolean isSingle=togSingle.s();
            if (sv==null) return;
            for(int iR=0; iR<results.length; iR++) {
                if (results[iR]==null) continue;
                final BasicResidueSelection ss[]=results[iR].selections();
                for(int iS=0;iS<ss.length; iS++) {
                    final BasicResidueSelection s=ss[iS];
                    //s.setImage("");
                    s.setStyle(isSingle ? ((results.length-iR-1)<<VisibleIn123.BIT_SHIFT_LINE) + VisibleIn123.STYLE_UNDERLINE : VisibleIn123.STYLE_IMAGE_LUCID);
                    s.setVisibleWhere(isSingle ? VisibleIn123.SEQUENCE : VisibleIn123.STYLE_IMAGE);
                    final int iMethod=idxOf(results[iR].clazz(),classes);
                    if (iMethod<0) continue;
                    final Protein p=s.getProtein();
                    final int iProtein=idxOf(p,proteins);
                    if (iProtein<0) continue;
                    p.removeResidueSelection(s);
                    if (isSingle && togMethods[iMethod].s() && togProteins[iProtein].s()) p.addResidueSelection(s);
                    s.setColor(method2color(iMethod));
                }
            }
            for(int iP=proteins.length; --iP>=0;) {
                final Protein p=proteins[iP];
                BasicResidueSelection c=consensus[iP];
                p.removeResidueSelection(c);
                if (!togConsensus.s()) continue;
                if (c==null) {
                    c=consensus[iP]=new BasicResidueSelection(0);
                    c.setStyle(VisibleIn123.STYLE_BACKGROUND);
                    c.setName("consensus TM-prediction");
                    c.setColor(C(0xFF0000));
                }
                final List<boolean[]> vPred=new ArrayList();
                for(Result r: results)
                    for(BasicResidueSelection s : r.selections()) {
                        if (s==null) continue;
                        final int iMethod=idxOf(r.clazz(),classes);
                        if (s.getProtein()==p && togMethods[iMethod].s()) {
                            vPred.add(s.getSelectedAminoacids());
                        }
                    }
                setPredictions(toArry(vPred, boolean[].class),c);
                p.addResidueSelection(c);
            }
            int countHydro=0;
            for(int iP=proteins.length; --iP>=0;) {
                for(int iH=Hydrophobicity.SCALE_NAMES.length; --iH>=0;) {
                    final boolean shown=togHydro[iH].s() && togProteinsH[iP].s();
                    if (shown) {
                        if (hydrophobicity[iP][iH]==null) {
                            final Hydrophobicity h=new Hydrophobicity();
                            h.setProtein(proteins[iP]);
                            h.setScale(Hydrophobicity.SCALE_NAMES[iH]);
                            h.setColor(method2color(iH));
                            hydrophobicity[iP][iH]=(h);
                        }
                        countHydro++;
                        sv.getPlottersV().add(hydrophobicity[iP][iH]);
                    } else sv.getPlottersV().remove(hydrophobicity[iP][iH]);
                }
            }
            if (countHydro>=0) StrapView.button(StrapView.TOG_PLOT_ENTIRE_PANEL).s(false);

            sv.setSpaceBetweenRows(maxi(isSingle ? results.length*3 : 0,countHydro==0 ? 0 : sv.panel().getFont().getSize()));
            panOverview.repaint();
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
            StrapEvent.dispatchLater(StrapEvent.VALUE_OF_RESIDUE_CHANGED,111);
        }
        public String getColumnName(int c) {return "";}
        public boolean isCellEditable(int row,int col) {
            final int rowHydrophobFrom=classes.length+2,rowHydrophobTo=rowHydrophobFrom+Hydrophobicity.SCALE_NAMES.length;
            if (row<rowHydrophobFrom) {
            return
                ((col==0 || col==proteins.length+2) && row<classes.length) ||
                (row==classes.length && (col>1 && col<proteins.length+2));
            }
            return  rowHydrophobFrom<=row && row<rowHydrophobTo && col==2+proteins.length ||
                row==rowHydrophobTo && col>1 && col<2+proteins.length;
        }
        public void setValueAt(Object o,int row,int col) {}
        public Class getColumnClass(int c) {return c==0 ? Class.class : JComponent.class;}
        public int getColumnCount() {return proteins.length+3;}
        public int getRowCount() { return classes.length+1  + 1+ Hydrophobicity.SCALE_NAMES.length+1;}
        public Object getValueAt(int row,int col) {
            final int rowHydrophobFrom=classes.length+2,rowHydrophobTo=rowHydrophobFrom+Hydrophobicity.SCALE_NAMES.length;
            final int iProt=col-2;
            if (row<rowHydrophobFrom-1) {
                final int iMethod=row;
                if (iMethod==classes.length) return (iProt>=0 && iProt <proteins.length) ? togProteins[iProt] : "";
                if (col==0) return  butCtrl[iMethod];
                if (col==1) return  classes[iMethod];
                if (iProt==proteins.length) return togMethods[iMethod];
                if (iProt>=0 && iProt<proteins.length && iMethod>=0 && iMethod<classes.length) {
                    if (results[iMethod].isFailed()) return iicon(IC_UNHAPPY);
                    if (!results[iMethod].isFinished()) return iicon(IC_HOURGLASS);
                    return togMethods[iMethod].s() && togProteins[iProt].s() ? method2color(iMethod) : "";
                }
            }
            if (row==rowHydrophobFrom-1) return "";
            if (row>=rowHydrophobFrom && row<rowHydrophobTo) {
                final int iHydro=row-rowHydrophobFrom;
                return
                    col==0 ? "":
                    col==1 ? Hydrophobicity.SCALE_NAMES[iHydro]:
                    col==2+proteins.length ? togHydro[iHydro] :
                    (togHydro[iHydro].s() && togProteinsH[iProt].s() ? method2color(iHydro) : "");
            }
            if (row==rowHydrophobTo) return iProt>=0 && iProt<proteins.length ?  togProteinsH[iProt]: "";
            return "error";
        }
        private Color method2color(int iMethod) {
            final int i=iMethod%7;
            return C(i==0 ? 0xff0000 : i==1 ? 0xffafaf : i==2 ? 0xFF00FF : i==3 ? 0x00FF00 : i==4 ? 0Xff : i==5 ? 0xffFF : 0xffFf00);
        }
        public void addTableModelListener(TableModelListener l){}
        public void removeTableModelListener(TableModelListener l){}
        private final ChRenderer _chRen=new ChRenderer();
        public Component getTableCellRendererComponent(JTable t,Object o,boolean isSel,boolean foc,int row,int col) {
            if (row==-1) return  col<2 || col>=proteins.length+2 ? ChRenderer.blankLabel() : proteins[col-2].verticalRendererComponent();
            if (o instanceof JComponent) return (JComponent)o;
            return _chRen.getTableCellRendererComponent(t,o,isSel,foc,row,col);
        }
        public void editingStopped(ChangeEvent ev) {}
        public void editingCanceled(ChangeEvent ev) {}

        private void setPredictions(boolean[][] predictions, BasicResidueSelection cons) {
            int len=0;
            final String sMethod=comboConsensusMethod.toString();
            if (chrAt(0,sMethod)=='>') {
                final int num=atoi(sMethod,1);
                for(boolean[] p: predictions) {
                    if (p!=null && p.length>len) len=p.length;
                }
                final boolean bb[]=new boolean[len];
                for(int iA=len;--iA>=0;) {
                    int count=0;
                    for(boolean[] p:predictions) {
                        if (p!=null && p.length>iA && p[iA]) count++;
                    }
                    bb[iA]=count>num;
                }
                BasicResidueSelection.changeSelectedAA(bb, Protein.firstResIdx(SPUtils.sp(cons)), cons);

            }
        }

    }
    public void handleEvent(StrapEvent ev) {
        final int type=ev.getType();
        if (type==StrapEvent.ALIGNMENT_SCROLLED) {
            final Component c=getSelectedComponent();
            if (c instanceof ResultPane) ChDelay.repaint(((ResultPane)c).panOverview,222);
        }
    }
}
/*

1Q3I_A.pdb
1MHS_A.pdb
1NRW_A.pdb
1Y8A_A.pdb
1SU4_A.pdb
1WR8_B.pdb

*/
