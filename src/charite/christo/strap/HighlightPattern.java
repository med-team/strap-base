package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
class HighlightPattern extends AbstractVisibleIn123 implements ActionListener {
    private byte[][] _patterns;
    private boolean _isNt;
    private String _patternText="", sPatterns[];
    private Object _butColor;
    public String getText() { return _patternText;}
    final NextAndPrevSelection nextAndPrevSelection=new NextAndPrevSelection();
    public Object butColor() {
        if (_butColor==null) _butColor=new ButColor(0,null,this);
        return _butColor;
    }

    public void actionPerformed(ActionEvent ev) {
        if (ev.getActionCommand()==ACTION_COLOR_CHANGED)  {
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR,111);
        }
    }

    public boolean setText(String text0) {
        final String s=text0!=null ? text0.trim().toUpperCase() : "";
        try {
            return !s.equals(_patternText);
        } finally {
            sPatterns=splitTokns(_patternText=s);
            _patterns=new byte[sPatterns.length][];
            for(int i=_patterns.length;--i>=0; ) _patterns[i]=sPatterns[i].getBytes();
        }
    }
    final boolean isNT() {return _isNt;}
    final boolean setNT(boolean b) {  try { return _isNt==b; } finally { _isNt=b; } }
    void save(BA sb) {
        final String pat=_patternText;
        if (sze(pat)>0) sb.a(pat).a('\t').a('#').a(getColor(),0,8).a('\t').a(getVisibleWhere()).a('\t').a(_isNt).a('\n');        
    }
    private void parse(String line) {
        final String ss[]=splitTokns(line, chrClas1('\t'));
        if (sze(ss)>3) {
            setText(ss[0]);
            setColor(str2color(ss[1]));
            setVisibleWhere(atoi(ss[2]));
            setNT(isTrue(ss[3]));
        }
    }
    // ----------------------------------------
    private final DialogHighlightPattern _dialog;
    public HighlightPattern(String line,DialogHighlightPattern d) {
        _dialog=d;
        parse(line);
        setStyle(STYLE_BACKGROUND);
    }
    public Selector getResidueSelection(Protein p) {
        for(ResidueSelection s : p.residueSelections()) {
            if (s instanceof Selector) {
                if (((Selector)s)._hp==this) {
                    return (Selector)s;
                }
            }
        }
        return null;
    }
    public ResidueSelection[] getResidueSelections(Protein pp[]) {
        final List v=new ArrayList();
        for(Protein p : pp) {
            final ResidueSelection s=p==null ? null : getResidueSelection(p);
            if (s!=null) v.add(s);
        }
        return toArry(v,ResidueSelection.NONE);
    }
    void addToProteins(Protein[] pp) {
        final boolean remove=sze(getText())==0 || getVisibleWhere()==0;
        for(Protein p : pp) {
            final Selector s0=getResidueSelection(p);
            if (remove) {
                if (s0!=null) {
                    p.removeResidueSelection(s0);
                    s0.setProtein(null);
                }
            } else {
                final Selector s=s0!=null ? s0: new Selector(this);
                if (s!=s0) p.addResidueSelection(s);
                s.setProtein(p);
            }
        }
    }
    // ----------------------------------------
    final class Selector extends BasicResidueSelection implements SelectorOfNucleotides, HasRenderer {
        private final HighlightPattern _hp;
        private boolean[] _selRes, _selNT, _selAAfromNT;
        private byte[] _selResL, _selNtL, _resultLength;
        private String _ntText, _aaText;
        private int _selNtMC,  _rtMC, _selAAfromNTOffset;

        public Selector(HighlightPattern hp) { super(OFFSET_IS_1ST_IDX); _hp=hp;}
        @Override public void setColor(Color c) { _hp.setColor(c);}
        @Override public Color getColor() { return _hp.getColor();}
        @Override public int getVisibleWhere() {return _hp.getVisibleWhere();}
        @Override public void setVisibleWhere(int n) { _hp.setVisibleWhere(n);}
        @Override public int getStyle() {return _hp.getStyle();}
        @Override public Object getRenderer(long options, long rendOptions[]) { return _hp._patternText;}
        @Override public String getName() { return "Highlight "+_hp._patternText;}
        public int getSelectedNucleotidesOffset() { return 0;}
        public byte[] getMatchLenghts() { return _selResL;}
        private boolean[] getMatches(String haystack, String needles[], boolean buffer[], byte[] bufferMatchLength, boolean clearBuffer) {
            final boolean bb[]=redim(buffer, sze(haystack),9);
            if (clearBuffer && bb==buffer) Arrays.fill(bb,false);
            final byte[] resL=_resultLength=redim(bufferMatchLength, sze(haystack), 9);
            for(String needle : needles) {
                try {
                    final Pattern pattern=Pattern.compile(needle);
                    final java.util.regex.Matcher matcher=pattern.matcher(haystack);
                    while(matcher.find()) {
                        final int start=matcher.start();
                        bb[start]=true;
                        resL[start]=(byte)mini(127,matcher.end()-start);
                    }
                }catch(Exception e){ _dialog.setError("Incomplete regular expression:   \""+needle+"\"");}
            }
            return bb;
        }
        private boolean[] getMatches(final byte haystack[], final int length, final byte needles[][], final boolean[] buffer, final byte[] bufferMatchLength, boolean clearBuffer) {
            if (haystack==null || needles==null) return NO_BOOLEAN;
            final boolean bb[]=redim(buffer, haystack.length ,9);
            final byte[] resL=_resultLength=redim(bufferMatchLength, haystack.length, 9);
            if (clearBuffer && bb==buffer) Arrays.fill(bb,false);

            for(byte[] needle : needles) {
                final int firstLetter=nxt(LETTR,needle);
                if (firstLetter<0) continue;
                final int needle0=needle[firstLetter];
                nextPos:
                for(int i=mini(haystack.length,length)-needle.length+1; --i>=0; ) {
                    if (needle0!= (haystack[i+firstLetter]&(~32))) continue nextPos;
                    for(int j=needle.length; --j>=0;) if (needle[j]!='.' && needle[j] != (haystack[i+j]&(~32)))  continue nextPos;
                    bb[i]=true;
                    resL[i]=needle.length<127 ? (byte) needle.length : 127;
                }
            }
            return bb;
        }

        public boolean[] getSelectedNucleotides() {
            if (!_isNt) return null;
            final Protein p=getProtein();
            final int orientations=_dialog.orientations();
            final boolean
                isForward=(orientations&1)!=0,
                isCompl=(orientations&2)!=0;
            if (sze(_patternText)==0 || !isNT() || !isCompl&&!isForward) return NO_BOOLEAN;
            final byte[] nts=p.getNucleotides();
            p.getResidueType();
            final int countNT=p.countNucleotides();
            if (nts==null) return null;
            final int mc=
                p.mcSum(ProteinMC.MC_RESIDUE_TYPE,ProteinMC.MC_NUCLEOTIDES,ProteinMC.MC_1ST_RES_IDX)+(orientations<<8);

            if (_selNT==null || _ntText!=_patternText || _selNtMC!=mc) {
                _selNtMC=mc;
                _ntText=_patternText;
                final boolean isRegex=isRegex(_patternText);
                if (isCompl) {
                    if (isRegex) {
                        _selNT=getMatches(p.getNucleotidesAsString(true),sPatterns,_selNT, _selNtL, true);
                    } else {
                        DNA_Util.reverseComplementDestructive(nts,countNT);
                        _selNT=getMatches(nts,countNT,_patterns, _selNT, _selNtL, true);
                        DNA_Util.reverseComplementDestructive(nts,countNT);
                    }
                    _selNtL=_resultLength;
                    reverseArray(_selNT,0,countNT);
                }
                if (isForward) {
                    _selNT=  isRegex ?
                        getMatches(p.getNucleotidesAsString(false),sPatterns,_selNT, _selNtL, !isCompl) :
                        getMatches(nts,countNT,_patterns, _selNT, _selNtL, !isCompl);
                }
                _selNtL=_resultLength;
                final int returnMinIdx[]={0};
                _selAAfromNT=p.ntPositions2aa(_selNT,0, returnMinIdx);
                _selAAfromNTOffset=returnMinIdx[0];
                Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS,p);
            }
            return _selNT;
        }
        @Override public int getSelectedAminoacidsOffset() {
            getSelectedAminoacids();
            return _isNt ? _selAAfromNTOffset : Protein.firstResIdx(getProtein());
        }

        @Override public boolean[] getSelectedAminoacids() {
            if (sze(_patternText)==0) return NO_BOOLEAN;
            final Protein p=getProtein();
            if (p==null) return NO_BOOLEAN;
            if (_isNt) {
                if (p.countNucleotides()==0) return NO_BOOLEAN;
                else {
                    getSelectedNucleotides();
                    return _selAAfromNT;
                }

            }
            final byte[] aa=p.getResidueType();
            final int mc=p.mc(ProteinMC.MC_RESIDUE_TYPE);
            if (_selRes==null || _aaText!=_patternText || _rtMC!=mc) {
                _rtMC=mc;
                p.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS, p);
                _dialog.setError("");
                final boolean selected[]=isRegex(_patternText) ?
                    getMatches(p.getResidueTypeAsString(),sPatterns, _selRes, _selResL, true) :
                    getMatches(aa,p.countResidues(),        _patterns, _selRes, _selResL, true);
                _selResL=_resultLength;
                _aaText=_patternText;
                _selRes=selected;

            }
            return _selRes;
        }
        private boolean isRegex(String ex) {
            for(int i=sze(ex); --i>=0;) {
                final char c=ex.charAt(i);
                if (c!='.' && c!=' ' && (c<'A' || c>'Z'))  return true;
            }
            return false;
        }

    }
}
