package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
public abstract class AbstractAlignerProxy extends AbstractProxy {
    /* ---------------------------------------- */
    /* >>> SequenceAligner >>> */
    private long options;
    public void setOptions(long flags) {
        this.options=flags;
        if (_po!=null) {
            final SequenceAligner sa=psa(); if (sa!=null) sa.setOptions(flags);
            final Superimpose3D su=su();    if (su!=null) su.setOptions(flags);
        }
    }
    public long getOptions() { final SequenceAligner psa=psa(); return psa!=null ? psa.getOptions() : this.options;  }

    private Object _po;
    private final Object _po() {
        if (_po==null) {
            _po=proxyObject();
            if (options!=0) {
                if (_po instanceof SequenceAligner) ((SequenceAligner)_po).setOptions(options);
                if (_po instanceof Superimpose3D) ((Superimpose3D)_po).setOptions(options);
            }
        }
        return _po;
    }

    private final SequenceAligner psa() { return deref(_po(), SequenceAligner.class); }
    private Superimpose3D su() {return deref(_po(), Superimpose3D.class); }

    public void setSequences(byte[]... ss){
        final SequenceAligner psa=psa();
        if (psa!=null) psa.setSequences(ss);
    }

    public byte[][] getAlignedSequences(){ final SequenceAligner psa=psa(); return psa!=null ? psa.getAlignedSequences() : null;}
    public byte[][] getSequences() { final SequenceAligner psa=psa(); return psa!=null ? psa.getSequences() : null;  }

    /* <<< SequenceAligner <<< */
    /* ---------------------------------------- */
    /* >>> Superimpose3D >>> */

    public void setProteins(Protein... pp) {
        final Superimpose3D su=su();
        if (su!=null) su.setProteins(pp);
    }
    public Protein[] getProteins() {
        final Superimpose3D su=su();
        return su==null?null: su.getProteins();
    }
    public void compute() {
        final Object o=proxyObject();
        if (o instanceof Superimpose3D) {
            ((Superimpose3D)o).compute();
        } else if (o instanceof SequenceAligner) ((SequenceAligner)o).compute();
    }
    public Superimpose3D.Result getResult() { final Superimpose3D su=su(); return su!=null ? su.getResult() : null;}

}
