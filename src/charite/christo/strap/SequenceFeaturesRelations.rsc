# Sequence features will be suppressed if there are at the same position features with more conclusive names.
# Here are two columns which are separated by tabulator.
# The sequence feature names in the left column are less informative than those in the right column.
# Wild card asterisk is only allowed in the second column at expression start and end.

Modified_residue	Post_translational_modification Glycosylation

Post_translational_modification	Glycosylation

Variant	Natural_variant Mutated_variant_site

Mutagenesis	Mutated_variant_site

Mature_protein_region	*_cytoplasmic_*

Initiator_methionine	Cleaved_initiator_methionine

Domain	*

Polypeptide_region	*

Binding_motif	Binding_site

Binding_site	*nucleotide

Nucleotide_binding	Flavin*

Metal_binding	*_ion


Cytoplasmic_region	nucleotide
