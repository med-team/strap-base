package charite.christo.strap;
import charite.christo.*;
import charite.christo.hotswap.ProxyClass;
import charite.christo.protein.*;
import charite.christo.blast.SequenceBlaster;
import charite.christo.strap.extensions.*;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class StrapPlugins implements java.awt.event.ActionListener, ChRunnable {
    private static StrapPlugins _inst;
    private static StrapPlugins instance() {if (_inst==null) _inst=new StrapPlugins(); return _inst;}

    private StrapPlugins(){}
    private static void loadClassesIntoVectors(int i) {
        final Class interf=interfaces()[i];
        UniqueList v=allClassesVV()[i];
        if (v==null) {
            allClassesVV()[i]=v=new UniqueList(Object.class).t(shrtClasNam(interf));
            TabItemTipIcon.setTiti(TabItemTipIcon.getTiti(interf),v);
            v.run(ChRunnable.RUN_SET_TREE_VALUE,interf);
        }
        builtInClassesV(builtInPlugins()[i],v);
        if (_dialogInit) {
            for(StrapPlugin p : StrapPlugin.getPlugins()) {
                final Class c=p.getClazz(false);
                if (c!=null && interf.isAssignableFrom(c)) v.add(c);
            }
            _mc++;
        }
        multiUpdate();
        updateAllNow(CHANGED_PLUGINS_ADDED_OR_REMOVED);
    }
    private static int _mc;
    public static void setChanged() {
        _mc++;
        multiUpdate();
    }
    /**
       Changes each time the plugins change;
       If only the built in plugins are activated, it returns 0;
    */
    public static int mc() { return _mc;}
    public static UniqueList allClassesV(Class clas) { return allClassesV(class2index(clas)); }
    private static UniqueList _empty;
    static UniqueList allClassesV(int interf) {
        if (interf<0 || interf>=interfaces().length) {
            if (_empty==null) _empty=new UniqueList(Object.class); else _empty.clear();
            return _empty;
        }
        if (allClassesVV()[interf]==null) loadClassesIntoVectors(interf);
        return allClassesVV()[interf];
    }
    private static void builtInClassesV(Class clas,List<Class> v) {
        for(Object[] plugins:builtInPlugins())
            if (plugins[0]==clas) builtInClassesV(plugins,v);
    }
    private static void builtInClassesV(Object plugins[],List<Class> v) {
        final Object[] cc=plugins;
        for(int j=1;j<cc.length;j++) {
            final Object o=cc[j];
            if (o instanceof Class) {  v.add((Class) o);continue;}
            try { v.add(Class.forName((String)o)); } catch(Exception e){}
        }
    }
    private static Object[][] _builtInPlugins;
    private static Object[][] builtInPlugins() {
        if (_builtInPlugins==null) {

            _builtInPlugins=new Object[][]{
                {ProteinParser.class,DSSP_Parser.class,PDB_Parser.class,SingleFastaParser.class,
                 NumberedSequence_Parser.class,XML_SequenceParser.class,SBD_Parser.class,StupidParser.class},
                {ProteinWriter.class,ProteinWriter1.class},
                {AlignmentWriter.class,ExportAlignment.class,CLASS_AlignmentWriterHSSP},
                {StrapExtension.class,CLASS_ConsensusSequence,CLASS_DemoAssociateObjectsToProteins},
                {ResidueSelection.class,CLASS_Distance3DToHetero,CLASS_Distance3DToCursor},
                {CompareTwoProteins.class, CLASS_SuperimposeTwoStructuresAndGetScore, CLASS_AlignTwoProteinsAndGetScore},

                {SequenceAlignmentScore2.class, CLASS_AlignmentScoreBlosume62, CLASS_AlignmentScoreCountIdentical},

                {ValueOfAlignPosition.class,CLASS_AlignmentEntropy},
                {ValueOfResidue.class, CLASS_Hydrophobicity, CLASS_ResidueSolventAccessibility,CLASS_ResidueValues_from_textfile /*,CLASS_ResidueHydrophobicity_TOPPRED*/},
                {ValueOfProtein.class, CLASS_CountResidues},
                {SequenceAligner.class,
                 Insecure.EXEC_ALLOWED ? Aligner3D.class : null,CLASS_MultipleAlignerClustalW,CLASS_MultipleAlignerAmap,CLASS_MultipleAlignerMafft,
                 CLASS_MultipleAlignerT_Coffee, CLASS_MultipleAlignerKalign, CLASS_MultipleAlignerMuscle,
                 isSystProprty(IS_WINDOWS) || isSystProprty(IS_LINUX386) ? CLASS_MultipleAlignerAlign_m : null,
                 CLASS_MultipleAlignerDialign,
                 CLASS_MultipleAlignerDialignT,
                 CLASS_MultipleAlignerProbcons2,
                 //CLASS_MultipleAligner_SPEM,
                 CLASS_PairAlignerNeoBioPROXY,
                 PairAlignerJAlignerPROXY.class,CLASS_Superimpose_TM_align,CLASS_Superimpose_native_CE, CLASS_Superimpose_CEPROXY,
                 CLASS_Aligner3D_Mapsci, CLASS_Aligner3D_Matt, CLASS_Aligner3D_Smolign, CLASS_Aligner3D_Mustang
                },

                {SequenceAlignerTakesProfile.class, CLASS_MultipleAlignerClustalW, CLASS_MultipleAlignerT_Coffee},

                {SequenceAligner3D.class,  CLASS_Aligner3D_Matt, CLASS_Aligner3D_Mapsci, CLASS_Aligner3D_Smolign,
                 CLASS_Superimpose_TM_align, CLASS_Superimpose_CEPROXY,CLASS_Superimpose_native_CE, CLASS_Aligner3D_Mustang},

                {Superimpose3D.class, CLASS_Superimpose_CEPROXY,CLASS_Superimpose_TM_align,CLASS_Superimpose_native_CE,CLASS_Superimpose_GoedePROXY, CLASS_Superimpose_GangstaPlus
                 /*, CLASS_Superimpose_LajollaProteinPROXY *//*, CLASS_Superimpose_LajollaRNA */},

                {ProteinsSorter.class},

                {TransmembraneHelix_Predictor.class,CLASS_TransmembraneHelix_Phobius, CLASS_TransmembraneHelix_HMMTOP,CLASS_TransmembraneHelix_TOPPRED,
                 CLASS_TransmembraneHelix_ORIENTM,CLASS_TransmembraneHelix_SPLIT4,CLASS_TransmembraneHelix_SPLIT3,
                 CLASS_TransmembraneHelix_SOSUI,CLASS_TransmembraneHelix_DAS,
                 CLASS_TransmembraneHelix_PRED_TMR,CLASS_TransmembraneHelix_TMHMM2,CLASS_TransmembraneHelix_TMAP,
                 CLASS_TransmembraneHelix_TMPRED,CLASS_TransmembraneHelix_MEMSAT,CLASS_TransmembraneHelix_WaveTM,
                 CLASS_TransmembraneHelix_THUMBUP,CLASS_TransmembraneHelix_TMAP_MULTI},
                {SecondaryStructure_Predictor.class, CLASS_SecondaryStructure_Sopma, CLASS_SecondaryStructure_Jnet, CLASS_SecondaryStructure_NNPREDICT},
                {CoiledCoil_Predictor.class,CLASS_CoiledCoil_PredictorRobinson},
                {PredictSubcellularLocation.class,SubcellularLocationHum_mPloc.class, SubcellularLocationSherLoc.class, SubcellularLocationPLOC.class},

                {PhylogeneticTree.class,                                                                       CLASS_PhylogeneticTree_TreeTop, CLASS_PhylogeneticTree_Archaeopteryx, CLASS_PhylogeneticTree_Jalview },
                {SequenceBlaster.class, CLASS_Blaster_SOAP_ebi, CLASS_Blaster_web_ncbi, CLASS_Blaster_local_Wu, CLASS_Blaster_local_NCBI /*, CLASS_Blaster_local_NCBI_gapped_PSI */},
                {ProteinViewer.class, CLASS_ChJmolPROXY, CLASS_Pymol, CLASS_ChAstexPROXY }
                //{SimilarPdbFinder.class, CLASS_SimilarPdbFinderBlast}
            };

            for(Object[] oo :  _builtInPlugins) {
                for(int i=sze(oo); --i>=0;) {
                    if (oo[i] instanceof String && isDeactivated((String)oo[i])) oo[i]=null;
                }
            }
        }
        return _builtInPlugins;
    }
    public static boolean isDeactivated(String s) {
        if (s!=null && !Insecure.CLASSLOADING_ALLOWED && s.endsWith("PROXY")
            || !Insecure.EXEC_ALLOWED && (s.indexOf("Superimpose")>=0||s.indexOf("Aligner")>0||s==CLASS_Pymol)
            ) {
            if (
                s!=CLASS_PairAlignerNeoBioPROXY &&
                s!=CLASS_Superimpose_CEPROXY &&
                s!=CLASS_ChAstexPROXY &&
                s!=CLASS_PairAlignerJAlignerPROXY
                ) return true;
        }
        return false;
    }
    private static UniqueList _allClassesVV[];
    private static UniqueList[] allClassesVV() {
         if (_allClassesVV==null) _allClassesVV=new UniqueList[builtInPlugins().length];
         return _allClassesVV;
     }
    static UniqueList[] getInterfacesAndClasses() {
        final UniqueList vv[]=allClassesVV();
        for(int i=interfaces().length; --i>=0;) {
            if (vv[i]==null) loadClassesIntoVectors(i);
        }
        return vv;
    }
    private static Class[] _interfaces;
    public static Class[] interfaces() {
        if (_interfaces==null) {
        Class cc[]=new Class[builtInPlugins().length];
        for(int i=cc.length;--i>=0;) cc[i]=(Class)builtInPlugins()[i][0];
        _interfaces=cc;
        }
        return _interfaces;
    }
    public static boolean isBuiltIn(Class c) {return idxOf(c,builtInPlugins())>=0;}
    private static int class2index(Class c) {
        for(int i=builtInPlugins().length;--i>=0;) if (c==interfaces()[i]) return i;
        return -1;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> List model two interfaces >>> */

    private static Class[][] _multiCC=new Class[99][];
    private static List[] _multiVV=new List[99];
    private static int[] _multiMC=new int[99];
    public static List for2Interfaces(Class cc[]) {
        int n=0;
        for(; n<99; n++) {
            if (_multiCC[n]==null) break;
            if (arraysEqul(cc,_multiCC[n])) return _multiVV[n];
        }
        _multiCC[n]=cc;
        multiUpdate();
        return _multiVV[n];
    }
    private static void multiUpdate() {
        for(int i=0;i<99;i++) {
            final Class cc[]=_multiCC[i];
            if (cc==null) break;
            int mc=0;
            for(Class c : cc) mc+=allClassesV(c).mc();
            List v=_multiVV[i];
            if (_multiMC[i]!=mc || v==null) {
                _multiMC[i]=mc;
                if (v==null) v=_multiVV[i]=new UniqueList(Object.class).rmOptions(UniqueList.UNIQUE);
                v.clear();
                for(Class c : cc) v.addAll(allClassesV(c));
            }
        }
    }
    /* <<< List model two interfaces <<< */
    /* ---------------------------------------- */
    /* >>> Standard plugin dialog  >>> */
    private static boolean _dialogInit;
    private static StrapPlugin _selectedPlugin;
    private static BA _log;
    private static BA log() {
        if (_log==null) _log=new BA(0);
        return _log;
    }
    private final static boolean _isLog=false;
    private static ChJList _jList, _jListMenus;
    private static List<StrapPlugin> _vPlugins;
    private static List<String> _vMenus;
    private static JFrame _frame;
    private static void update() {
        if (_isLog) log().aln("update()");
        final int idx=_jListMenus.getSelectedIndex();
        String menu=idx<0 || idx>_vMenus.size() ? "" : _vMenus.get(idx);
        _vMenus.clear();
        for(StrapPlugin p : StrapPlugin.getPlugins()) adUniq(p.getMenu(), _vMenus);
        if (_vMenus.size()>0) {
            sortArry(_vMenus);
            int iMenu=_vMenus.indexOf(menu);
            if (iMenu<0) menu=_vMenus.get(iMenu=0);
            _jListMenus.setSelI(iMenu);
        }
        if (menu!=null) {
            _vPlugins.clear();
            for(StrapPlugin p : StrapPlugin.getPlugins()) {
                if (menu.equals(p.getMenu())) adUniq(p, _vPlugins);
            }
        }
        _jList.clearSelection();
        ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,_jListMenus,1,null);
        ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,_jList,1,null);
        if (_frame!=null) _frame.toFront();
        for(int i=interfaces().length; --i>=0;)loadClassesIntoVectors(i);
    }
    static void dialogStaticPlugins() {
        _dialogInit=true;
        if (_frame==null) {

            _jList=new ChJList(_vPlugins=new Vector(),ChJTable.CLASS_RENDERER|ChJTable.SINGLE_SELECTION);
            _jListMenus=new ChJList(_vMenus=new Vector(), ChJTable.SINGLE_SELECTION);
            _jList.li(instance());
            _jListMenus.li(instance());
            final ChRenderer render=new ChRenderer();
            render.label().setHorizontalAlignment(SwingConstants.RIGHT);
            _jListMenus.setCellRenderer(render);
            final Object

                but=new ChButton("DOWNLOAD").t("Download/update plugins").li(instance()),
                pNorth=pnl(CNSEW,pnl(but),null,null,smallHelpBut(StrapPlugins.class)),
                pDetails=pnl(HBL,but(BUT_SOURCE), " directory=",fPathUnix(dirPlugins()), !_isLog?null: ChButton.doView(log()).t("Log")),
                pSouth=pnl(VBHB,
                           pnl(HBL, toggl().doCollapse(pDetails), "Details ",but(BUT_APPLY),but(BUT_DOCUMENTAION)),
                           pDetails);
            _frame=new ChFrame("Plugins").ad(pnl(CNSEW,scrllpn(_jList),pNorth,pSouth,null,_jListMenus)).size(666,333);
            addActLi(instance(),HtmlDoc.class);
        }
        update();
        _frame.show();
    }
    private static ChFrame _frameDownload;
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==_jListMenus) {_selectedPlugin=null; update();}
        if (q==_jList) {
            final StrapPlugin p=_selectedPlugin= get(_jList.getSelectedIndex(),_vPlugins,StrapPlugin.class);
            but(BUT_DOCUMENTAION).enabled(p!=null && p.getDocumentation()!=null);
            but(BUT_SOURCE).enabled(hasJavaSrc(p));
            but(BUT_APPLY).enabled(p!=null);
        }
        if (cmd==ACTION_DOWNLOAD_PLUGIN) update();

        if (cmd=="DOWNLOAD") {
            if (_frameDownload==null) {
                startThrd(thrdCR(this,"DOWNLOAD"));
                _frameDownload=new ChFrame("http://www.bioinformatics.nu/").size(666,333).ad(new JLabel("<html><body>contacting Wiki site.<br>Please wait</body></html>"));
            }
            _frameDownload.shw(0);
        }
        final StrapPlugin plugin=_selectedPlugin;
        final Class clazz=plugin!=null ? plugin.getClazz(true) : null;
        if (clazz!=null) {
            switch(idxOf(q,_bb)) {
            case BUT_APPLY:  applyPlugin(clazz); break;
            case BUT_DOCUMENTAION: new ChJTextPane(plugin.getDocumentation()).tools().showInFrame(plugin.getClassName()); break;
            case BUT_SOURCE: showJavaSource(nam(clazz),(String[])null); break;
            }
        }
    }
    public Object run(String id, Object arg) {
        if (id=="DOWNLOAD") {
            final String WIKI="http://www.bioinformatics.org/strap/wiki/Strap/Plugins/";
            final BA wiki=readBytes(inStreamT(99,WIKI, 0L));
            if (wiki==null)  error("Could not download "+WIKI+"<br>Are the Web proxy settings OK?");
            else {
                final int E=wiki.end();
                final byte T[]=wiki.bytes();
                int count=0;
                final BA txt=new BA(9999)
                    .a(
                            "This text contains URLs of WIKI:Jar-files with the suffix ?plugins\n"+
                            "To download/update a plugin, click the respective Jar-file.\n"+
                            "The clicked Jar-file will be downloaded to the directory ").a(fPathUnix(dirJars()))
                    .aln("\nThe plugin[s] provided by this Java archive appear in the list of installed plugins in the Plugins panel.\n");
                for(int pos=0; (pos=strstr("?plugins</a>", T, pos+1, E))>0;) {
                    txt.a(" * ")
                        .a(T, strchr(STRSTR_E|STRSTR_PREV, '>',T,pos,0)+1, pos+8)
                        .a("\n   ")
                        .a(T, pos+(4+8), strchr(STRSTR_E,'<', T, pos+(4+8),E));
                    count++;
                }
                txt.a("\nTo contribute your application please modify the wiki page\n")
                    .a(WIKI).aln("?action=edit   Enter passwd \"strap\"\n")
                    .a("Security warning:\n"+
                            "Plugins are potentially dangerous and may contain WIKI:Malware.\n"+
                            "You must not download plugins that you do not trust.\n"+
                            "Trusted URLs highlighted green\n"+
                            "If in doubt, do not run the plugin, or test it in a safe environment. \nSee ")
                    .a(URL_STRAP).a("security.html.");
                if (count==0)
                    txt.clr()
                        .a("Error in ").a(WIKI)
                        .a("\nMissing entries of the form http://..pluginfile.jar?plugins\nPlease send report christophgil@googlemail.com.");
                inEdtLaterCR(this,"SETTEXT",txt);
            }
        }
        if (id=="SETTEXT") {
            final ChJTextPane ta=new ChJTextPane((BA)arg);
            _frameDownload.getContentPane().removeAll();
            for(String url: TRUSTED_PLUGINS) ta.tools().highlightOccurrence(url ,null,null,0, C(0xff00,99));
            _frameDownload.ad(scrllpn(ta));
            addActLi(this,ta);
        }
        return null;
    }
    private final static ChButton[] _bb=new ChButton[4];
    private final static int BUT_APPLY=1, BUT_DOCUMENTAION=2, BUT_SOURCE=3;
    private static ChButton but(int type) {
        if (_bb[type]==null) _bb[type]=new ChButton(ChButton.DISABLED,null).li(instance());
        return _bb[type].t(type==BUT_APPLY ? ChButton.GO : type==BUT_DOCUMENTAION  ? "Documentation" : "Source code");
    }
    // ----------------------------------------
    private final static Object _dialog4if[][]={
        {ResidueSelection.class,  new Object[]{CLASS_DialogSelectionOfResiduesMain,CLASS_DialogCopyAnnotations}},
        {Superimpose3D.class,     new Object[]{CLASS_DialogSuperimpose3D,CLASS_DialogCompareProteins}},
        {CompareTwoProteins.class,new Object[]{CLASS_DialogCompareProteins,CLASS_DialogBarChart}},
        {SequenceAligner.class,                CLASS_DialogAlign},
        {ProteinViewer.class,                  CLASS_Dialog3DViewer},
        {AlignmentWriter.class,                CLASS_DialogExportAlignment},
        {ProteinWriter.class,                  CLASS_DialogExportProteins},
        {ValueOfProtein.class,                 CLASS_DialogBarChart},
        {SequenceBlaster.class,                CLASS_DialogBlast},
        {PhylogeneticTree.class,               CLASS_DialogPhylogeneticTree},
        {CoiledCoil_Predictor.class,           CLASS_DialogPredictCoiledCoil},
        {SecondaryStructure_Predictor.class,   CLASS_DialogPredictSecondaryStructures},
        {TransmembraneHelix_Predictor.class,   CLASS_DialogPredictTmHelices2}
    };
    static Object[] dialogsForPlugin(Class c) {
        if (c!=null) {
            for(Object[] oo:_dialog4if) if (((Class)oo[0]).isAssignableFrom(c)) return oo(oo[1]);
        }
        return NO_STRING;
    }
    public static void applyPlugin(Object proxyOrClass) {
        final Class clas;
        if (proxyOrClass instanceof ProxyClass) clas=((ProxyClass)proxyOrClass).getClassInstance();
        else clas=(Class)proxyOrClass;
        if (clas==null) { if (proxyOrClass!=null) assrt(); return; }
        try {
            if (StrapExtension.class.isAssignableFrom(clas)) {
                final StrapExtension ext=mkInstance(proxyOrClass,StrapExtension.class,true);
                StrapAlign.addListener(ext);
                if (ext!=null) ext.run();
            }
            if (ProteinsSorter.class.isAssignableFrom(clas)) SPUtils.showSortDialog();
            for(Object sDialog : dialogsForPlugin(clas)) {
                if (null!=StrapAlign.addDialogSetClass(sDialog,proxyOrClass)) break;
            }
            if (ValueOfResidue.class.isAssignableFrom(clas)) StrapAlign.addDialogSetClass(CLASS_DialogPlot, proxyOrClass);

            if (SequenceAlignmentScore2.class.isAssignableFrom(clas) || CompareTwoProteins.class.isAssignableFrom(clas)) {
                StrapAlign.addDialogSetClass(CLASS_DialogCompareProteins, proxyOrClass);
            }
        } catch(Exception e){ putln(RED_CAUGHT_IN+"StrapPlugins.applyPlugin ",proxyOrClass); stckTrc(e);}
    }

    public static SequenceAligner instanceSA(Object o,int numProteins, boolean error) {
        if (o==null) return null;
        final SequenceAligner sa= mkInstance(o,SequenceAligner.class,error);
        if (sa==null) { assrt(); return null; }
        if (sa instanceof SequenceAligner3D)  return new Aligner3D().setClass3D(sa.getClass());
        if (numProteins>2 && (sa.getPropertyFlags()&SequenceAligner.PROPERTY_ONLY_TWO_SEQUENCES)!=0) return new MultiFromPairAligner(sa.getClass());
        return sa;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Class Alias >>> */
    private static Map<String,String> _mapS2L, _mapL2S=new HashMap();
    public static String mapS2L(String k) { _mapS2L(); return applyDict(k,_mapS2L); }
    public static String mapL2S(String k) { _mapS2L(); return applyDict(k,_mapL2S); }

    static void _mapS2L() {
        synchronized(_mapL2S) {
            if (_mapS2L==null) {
                final String[] PFX={null,"TransmembraneHelix_","PhylogeneticTree_","MultipleAligner","Aligner3D_","PairAligner","Superimpose_"};
                final Map m=_mapS2L=new HashMap();
                for(List v : getInterfacesAndClasses()) {
                    for (int iV=sze(v); --iV>=0;) {
                        final Object o=v.get(iV);
                        final String n=o instanceof Class ? nam(o) : toStrgIntrn(o);
                        if (n==null) continue;
                        for(int mode=(1<<3); --mode>=-1; ) {
                            String s=n, a;
                            final boolean rev=mode<0;
                            if (rev || 0!=(mode&1)) {
                                a=delToLstChr(s, '.');
                                if (s==a && !rev) continue; else s=a;
                            }
                            if (rev || 0!=(mode&2)) {
                                a=delSfx("PROXY",s);
                                if (s==a && !rev) continue; else s=a;
                            }
                            if (rev || 0!=(mode&4)) {
                                a=is(UPPR,s,2) ? delPfx("Ch",s) : s;
                                if (s==a && !rev) continue; else s=a;
                            }
                            if (rev) {
                                _mapL2S.put(n, toStrgIntrn(s));
                                continue;
                            }

                            for(String pfx : PFX) {
                                a=pfx==null ? s : delPfx(pfx,s);
                                if (pfx==null || a!=s) {
                                    m.put(a,n);
                                    //if (debug && pfx==null) putln("n="+n+" s="+s0+" a="+a);
                                    final String lc=a.toLowerCase();
                                    if (a!=lc) m.put(lc,n);
                                }
                            }
                        }
                    }
                }
                // for(Map.Entry e : entryArry(_mapL2S)) putln(e);debugExit("");
            }
        }
    }
}
