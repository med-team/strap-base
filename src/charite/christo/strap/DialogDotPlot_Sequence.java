package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class DialogDotPlot_Sequence extends JPanel implements java.awt.event.ActionListener,Runnable {
    private final DialogDotPlot _dotplot;
    private final char _xOrY;
    private final JComponent _pnl;
    private int _charH, _charW, last_sbPos;
    private final static Point ORIGIN=new Point();
    private final DrawGappedSequence dgs=new DrawGappedSequence();
    void move(int d){ _dotplot.setAlignmentPos(_xOrY, _dotplot.alignmentPos(_xOrY)+d); run(); }

    private int fontSize;
    Font setFontSize(int s,boolean isAbs) {
        int size=s+(isAbs?0:this.fontSize);
        if (size<4) size=4;
        final Font font=getFnt(this.fontSize=size,true,0);
        setFont(font);
        final Rectangle cb=chrBnds(font);
        _charW=cb.width; _charH=cb.height;
        return font;
    }
    //    JScrollBar sb() { return getSP(this).getHorizontalScrollBar();}
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="<") move(-1);
        if (cmd==">") move(+1);
    }
    public JComponent getPanel() { return _pnl;}
    public DialogDotPlot_Sequence(DialogDotPlot dotplot, char X_or_Y) {
        _xOrY=X_or_Y;
        _dotplot=dotplot;
        setDragScrolls('B',this,scrllpn(SCRLLPN_NO_VSB|SCRLLPN_NO_HSB, this));
        _pnl=pnl(CNSEW,getSP(this),null,null,
                 new ChButton(ChButton.MAC_TYPE_ICON,">").li(this),
                 new ChButton(ChButton.MAC_TYPE_ICON,"<").li(this));
        setFontSize(24,true);
        setPreferredSize(dim(Short.MAX_VALUE,_charH+1));

        pcp(KEY_SCROLL_EVEN_IF_INVISIBLE,"", getSB('H',this));
        pcp(KOPT_TRACKS_VIEWPORT_HEIGHT,"",this);
    }
    public void run() {
        final int pos=_dotplot.alignmentPos(_xOrY)*_charW;
        final JScrollBar sb=pos<0?null:getSB('H',this);
        if (sb!=null) sb.setValue(pos);
    }
    @Override public Dimension getPreferredSize() {
        final byte[] seq=_dotplot.getSeq(_xOrY);
        return dim(seq!=null ? seq.length*_charW+getOffset()*2 : 1,_charH+2);
    }
    private int getOffset() { return wdth(getSP(this).getViewport())/2; }

    @Override public void paintComponent(Graphics g) {
        super.paintComponent(g);
        final Color[] shadingColors=ShadingAA.colors(false);
        final Protein p=_dotplot.protein(_xOrY);
        if (p==null) return;
        g.setColor(C(isWhiteBG() ? 0xFFffFF : 0));
        g.fillRect(0,0, Short.MAX_VALUE,_charH+9);
        final byte sequence[]=_dotplot.getSeq(_xOrY);
        final char t=_dotplot.sequenceType(_xOrY);
        final int mode=
            (ShadingAA.i()==ShadingAA.SECSTRU ? dgs.SHADE_SEC_STRU:0) |
            (t=='B' ? DrawGappedSequence.AA2NT : t=='N' ? dgs.NUCLEOTIDES : 0);
        dgs.draw(this,g, setXY(getOffset(),0,ORIGIN),0,sequence,0,MAX_INT,p,shadingColors,mode);

        final int sbPos=getSB('H',this).getValue();
        if (last_sbPos!=sbPos) {
            last_sbPos=sbPos;
            _dotplot.setAlignmentPos(_xOrY,sbPos/_charW);
        }
    }

}

