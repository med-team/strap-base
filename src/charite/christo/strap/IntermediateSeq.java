package charite.christo.strap;
import charite.christo.*;
import charite.christo.blast.*;
import charite.christo.protein.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import javax.swing.*;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

Identifying Blast hits that are common to two sequences helps to compare remote homologs.

The analysis requires two steps.

<ol>
<li>One  protein is selected in one, the other in the other protein list. </li>
<li>For each sequence a BLAST search is performed against a non-redundant set of sequences such as uniref50

</li>
<li>For two sequences the BLAST hits are compared and all common segments presented to the user by pressing the button <i>LABEL:ChButton#BUTTON_GO</i>.
</li>

</ol>

Alignment programs perform better when sequences are added that share similarity with the proteins
to be aligned. Using two additional steps one can improve multiple sequence alignments:

<ol>
<li>Choose hits that are similar to either sequence by activating the respective check-boxes.</li>
<li>Protein objects are created for each activated Blast segment. At this stage overlapping segments with the same sequence ID but different start and end positions are joined.</li>
<li>Finally a multiple sequence alignment is computed <i>BUTTON:DialogAlign!</i>.</li>
</ol>

<br>
<b>Related publications:</b> PUBMED:11159329 PUBMED:9367767
<br>
   @author Christoph Gille
*/
public class IntermediateSeq extends AbstractDialogJTabbedPane implements ProcessEv, Disposable,HasControlPanel,StrapListener,ChRunnable {
    private final Map<Protein,Object> _mapBlastResult=new WeakHashMap();
    private final List<BlastAlignment> _vAli=new Vector();
    private final List<String> _vDB=new Vector();
    private final EvAdapter LI=evAdapt(this);
    private final ChPanel _progress=prgrssBar();
    private final ChCombo
        _comboDB=new ChCombo(0L, _vDB).li(LI),
        _comboClass=SPUtils.classChoice(SequenceBlaster.class).li(LI);
    private final Object
        _butBlast=new ChButton(ChButton.GO).li(LI).tt("Invokes comparison of BLAST hits."),
        _bLoad=new ChButton(ChButton.DISABLED,"Load all selected").li(LI);
    private final BA _log=new BA(999), _logNoHits=new BA(999).aln("protein pairs without  common blast hits\n");
    private final ProteinList _listProt1, _listProt2;
    private Object _ctrl;
    private void updateDB() {
        final SequenceBlaster b=_comboClass.instanceOfSelectedClass(SequenceBlaster.class);
        if (b!=null) {
            _vDB.clear();
            final String[] dd=b.getAvailableDatabases(0);
            adAll(dd, _vDB);
            for(int i=sze(dd); --i>=0;) {
                if (strstr(STRSTR_IC, "uniref50",dd[i])>=0) _comboDB.s(i);
            }
            if (sze(_vDB)==0) _vDB.add("Error: no Database");
        }
    }
    public void processEv(AWTEvent ev) {
        StrapAlign.errorMsg("");
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final int id=ev.getID();
        if (id==MOUSE_CLICKED && q==_progress) ChMsg.runShowResult().run();
        if (cmd!=null) {
            if (q==_comboClass || q==_comboDB) _mapBlastResult.clear();
            if (q==_comboClass) updateDB();
            if (q==_butBlast) {
                setPrefSze(true,20*EX, prefH(q), _progress);
                setEnabld(false,_bLoad);
                setEnabld(false,_butBlast);
                startThrd(thrdCR(this,"GO"));
            }
            if (q==_bLoad) {
                _vAli.clear();
                for(IntermediateSeqPair sp: childsR(this,IntermediateSeqPair.class))
                    for(BlastHitIntersection a: sp.vAllAlignments.asArray())
                        if (a.SELECTED[0]) {
                            final BlastAlignment ba=new BlastAlignment();
                            ba.setMatchStartEnd(a.getMatchStart(),a.getMatchEnd());
                            ba.setMatchSeq(filtrBB(0L,LETTR,a.getSeq('M',0)),0, MAX_INT);
                            ba.setBlastHit(a.getAlignment(0).getBlastHit());
                            _vAli.add(ba);
                        }
                union();
                final List<Protein> vProteins=new Vector();
                for(int i=0; i<sze(_vAli); i++) {
                    final BlastAlignment a=_vAli.get(i);
                    final String pn="xx"+a.getID()+'-'+a.getMatchStart()+'-'+a.getMatchEnd();
                    if (SPUtils.proteinWithName(pn,null)==null){
                        final Protein p=new Protein(StrapAlign.getInstance());
                        p.setName(pn);
                        p.setResidueType(a.getMatchSeq());
                        p.setHeader(toStrg(a.getBlastHit().getDescription()));
                        adUniq(p, vProteins);
                    }
                }
                if (sze(vProteins)==0) StrapAlign.errorMsg("You must select blast hits in the result tabs");
                final StrapView v=StrapAlign.alignmentPanel();
                if (v!=null) v.setCursor(StrapView.CURSOR_COLUMN,v.proteinInRow(v.countRows()-1),1);
                StrapAlign.addProteins(-1, toArry(vProteins, Protein.class));
            }
        }
    }
    @Override public Object getControlPanel(boolean real) {
        if (!real) return CP_EXISTS;
        if (_ctrl==null) {
            _ctrl=pnl(ChButton.doView(_log).t("Log")," ",ChButton.doView(_logNoHits).t("Protein pairs without common blast hits"));
        }
        return _ctrl;
    }
    // ----------------------------------------
    public IntermediateSeq() {
        _listProt1=new ProteinList(0L);
        _listProt1.setSelI(0);
        _listProt1.li(LI);
        _listProt2=new ProteinList(0L);
        _listProt2.setSelI(1);
        _listProt2.li(LI);
        getPanel();
        updateDB();
    }
    // ----------------------------------------
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="GO") {

            Protein[] pp0=_listProt1.selectedProteins(),pp1=_listProt2.selectedProteins();
            if(pp0.length==0) pp0=pp1;
            if(pp1.length==0) pp1=pp0;
            if (pp0.length==0) {
                StrapAlign.errorMsg("Select at least one protein in the top and button protein list");
                return null;
            }
            final BlastResult[][] results=new BlastResult[2][];
            for(int i2=0, count=0; i2<2; i2++) {
                final Protein[] pp=i2==0 ? pp0 : pp1;
                results[i2]=new BlastResult[pp.length];
                for(int iP=0; iP<pp.length; iP++) {
                    final Protein p=pp[iP];
                    _progress.setProgress( ++count*100/(1+pp0.length+pp1.length), toStrg(p)).repaint();
                    BlastResult result=gcp(p,_mapBlastResult,BlastResult.class);
                    if (result==null) {

                        final SequenceBlaster blaster=mkInstance(_comboClass,SequenceBlaster.class,true);
                        if (blaster==null)  { _log.aln("Error: could not instantiate blast service");  return null; }
                        blaster.setAAQuerySequence(p.getResidueTypeAsString());
                        blaster.setDatabase(toStrg(_comboDB));
                        blaster.setNumberOfAlignments(MAX_INT);
                        blaster.setSensitivity(10);
                        result=BlastResult.newInstance(blaster, "Intermed Seq "+p,null);
                    }
                    if (result!=null) {
                        _mapBlastResult.put(p,newSoftRef(result));
                        results[i2][iP]=result;
                    }
                }
            }
            _progress.setProgress(0, "Blast done").repaint();

            inEdtLaterCR(this,"COMPUTE",new Object[]{pp0,pp1,results[0],results[1]});
            setEnabld(true,_bLoad);
            setEnabld(true,_butBlast);
        }
        if (id=="COMPUTE") {
            compute((Protein[])argv[0], (Protein[])argv[1], (BlastResult[])argv[2], (BlastResult[])argv[3]);
        }
        return null;
    }
    private void compute(Protein[] pp1,Protein[] pp2, BlastResult[] rr1, BlastResult[] rr2) {
        boolean success=false;
        for(int i=0; i<pp1.length; i++) {
            for(int j=0; j<pp2.length; j++) {
                final Protein p1=pp1[i], p2=pp2[j];
                if (p1==p2)  {
                    shwTxtInW(_logNoHits.a("skip ").a(p1).aln(" with itself"));
                    continue;
                }
                if (get(i,rr1)==null) { _logNoHits.a("Error ").a(p1).aln(": No Blast Result"); continue; }
                if (get(j,rr1)==null) { _logNoHits.a("Error ").a(p2).aln(": No Blast Result"); continue; }
                final IntermediateSeqPair r=new IntermediateSeqPair(p1,p2,rr1[i],rr2[j]);

                // debugExit("rr1="+rr1[i].getHits().length+"  rr2="+rr2[j].getHits().length);
                if (sze(r._vHitViewShown)==0) {
                    shwTxtInW(_logNoHits.a(p1).a(" and ").aln(p2));
                    r.dispose();
                } else {
                    success=true;
                    adTab(DISPOSE_CtrlW, ProteinUtils.tabText2(p1,p2), r, this);
                }
                //StrapAlign.drawProgress((i*pp1.length+j)*100/pp1.length/pp2.length);
            }
        }
        if (success) setSelectedIndex(getTabCount()-1);
    }
    private Object getPanel() {
        final Object
            pStep23=pnl(VB,
                       pnl(VBHB,"#ETBem",
                            " ",
                            "1st step: Load those blast hits that are<br>activated with the check-boxes \"accept\".",
                            _bLoad,
                            " ",
                            "2nd step: Align the sequences together with the loaded blast hits.",
                            StrapAlign.b(CLASS_DialogAlign)
                            )
                       ),
            pDetails=pnl(VBHB,
                          "Method:",
                          _comboClass.panel(),
                          "Database:",
                          _comboDB,
                          ChMsg.butShowComputedResults().t("Log blast")
                          ),
            pStep1=pnl(VB,KOPT_TRACKS_VIEWPORT_WIDTH,
                      pnl(VBPNL,"#ETBem",
                           "Find common blast hits.",
                          pnl(_butBlast, _progress),
                          pnlTogglOpts(pDetails),
                          pDetails
                          ),
                      " ",
                       pnl(HBL, toggl().doCollapse(pStep23), "Use result to improve multiple sequence alignment:"),
                      pStep23
                      ),

            pCenter=pnl(new java.awt.GridLayout(1,2),scrllpn(pStep1),
                        pnl(new java.awt.GridLayout(2,1),_listProt1.scrollPane(),_listProt2.scrollPane())),
            pMain=pnl(CNSEW,pCenter,dialogHead(this), pnl(REMAINING_VSPC1));
        setPrefSze(false,0,0,_progress);
        LI.addTo("m",_progress);
        _progress.setToolTipText("Progress of blast runs");
        setPrefSze(false, MIN_INT, prefH(_butBlast), _comboClass);
        adMainTab(pMain, this,null);
        return this;
    }
    // ----------------------------------------
    /* Keep
    static java.io.File getFileOfHits(Protein pp[],String suffix) {
        final String ss[]=new String[pp.length];
        for(int i=pp.length;--i>=0;) ss[i]=pp[i].toString();
        sortArry(ss);
        final java.io.File dirTmp=file(dirTmp()+"/X-blast");
        mkdrs(dirTmp);
        return file(dirTmp, new BA(0).join(ss,"+").a(suffix).toString());
    }
    */
    private void union() {
        for(int i0=sze(_vAli);--i0>=0;) {
            BlastAlignment a0=_vAli.get(i0);
            if (a0==null) continue;
            for(int i1=sze(_vAli);--i1>=0;) {
                final BlastAlignment a1=_vAli.get(i1);
                if (a1==null || a1==a0) continue;
                final BlastAlignment u=BlastHitIntersection.union(a0,a1);
                if (u!=null) {
                    _vAli.set(i0,a0=u);
                    _vAli.set(i1,null);
                }
            }
        }
        while(_vAli.remove(null));
    }
    public @Override void dispose() {
        for(IntermediateSeqPair p: childsR(this,IntermediateSeqPair.class)) p.dispose();
        _mapBlastResult.clear();
        super.dispose();
    }
    public void handleEvent(StrapEvent ev) {
        final int type=ev.getType();
        if (type==StrapEvent.AA_SHADING_CHANGED) repaint();
    }
}
// http://bibiserv.techfak.uni-bielefeld.de/altavist/
//http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=16076885&itool=iconabstr&query_hl=8
