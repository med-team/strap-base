package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
class TexshadeSecStructure {
    private final ChButton _cbSkip, _toggle;
    private final ChCombo _choiceSheet;
    private final Object _where, _tfHelix1, _tfSheet1, _tfHelix2, _tfSheet2;
    private Object _panel;
    private ProteinCombo _choiceP;
    TexshadeSecStructure(String where) {
        _where=where;
        final String save=Texshade.SAVE+where+" ";
        _tfHelix1=new ChTextField().cols(10,true,true).saveInFile(save+"tf_Helix1");
        _tfSheet1=new ChTextField().cols(10,true,true).saveInFile(save+"tf_Sheet1");
        _tfHelix2=new ChTextField().cols(10,true,true).saveInFile(save+"tf_Helix2");
        _tfSheet2=new ChTextField().cols(10,true,true).saveInFile(save+"tf_Sheet2");
        _cbSkip=toggl("skip sequence").save(TexshadeSecStructure.class,"skip_"+where)
            .tt("You may not want to show the protein<br>which texshade retrieves the <br>secondary structure information from.");
        _toggle=toggl().s(where=="top").save(TexshadeSecStructure.class, "enable_"+where);
        _choiceSheet=new ChCombo("box","---","===","-->","'->","<-|","<=>",",-,","|=|","helix").s("---").save(TexshadeSecStructure.class,"_choiceSheet_"+where);
    }
    Object panel() {
        if (_panel==null) {
            _choiceP=new ProteinCombo(ProteinList.SECSTRU);
            _choiceP.save(TexshadeSecStructure.class,"choiceProtein_"+_where);
            final Object
                pan0=pnl(HBL,_choiceP, " ",_cbSkip.cb()),
                pan1=pnl(HBL,"\\feature{",_where,"}{ ... }{helix[RED]",_tfHelix1,"}{",_tfHelix2,"}"),
                pan2=pnl(HBL,"\\feature{",_where,"}{ ... }{",_choiceSheet,"[YELLOW]:",_tfSheet1,"}{",_tfSheet2,"}");
            _panel=pnl(VBHB,pnl(HBL,_toggle,"Location : ",_where,pan0),  pan1, pan2);
            _toggle.doCollapse(new Object[]{pan1,pan2,_choiceP, pan0});
        }
        return _panel;
    }
    public void makeTex(BA sb, int colFrom, int colTo) {
        sb.aln("% secondary structure");
        Protein p=_choiceP.getProtein();
        if (!_toggle.s() || p==null) return;
        if (p.getResidueSecStrType()==null) {
            for(Protein pSS : Texshade.proteins()) {
                if (pSS.getResidueSecStrType()!=null) {
                    _choiceP.s(p=pSS);
                    break;
                }
            }
        }
        final int i=idxOf(p,Texshade.proteins());
        final byte ss[]=p==null?null:p.getResidueSecStrType();
        if (i<0) error("Secondary structure of "+p+" will not be included, <br>because the protein is not selected.");
        else if (sze(ss)==0) sb.a("% TexshadeSecStructure: ").a(p).aln(" has no secondary structures");
        else {
            final int idx0=Protein.firstResIdx(p);
            final int iaFrom=maxi(0,p.column2nextIndexZ(colFrom));
            final int iaTo  =mini(ss.length, p.countResidues(), p.column2thisOrPreviousIndex(colTo-1)+1);
            for(int iType=2; --iType>=0; ) {
                final int type=iType==0 ? 'h' : 'e';
                final boolean[] bb=new boolean[ss.length];
                for(int ia=iaFrom; ia<iaTo; ia++) bb[ia]=(ss[ia]|32)==type;
                if (fstTrue(bb)>=0) {
                    sb.a("\\feature{").a(_where).a("}{").a(i+1).a("}{").a(ResSelUtils.texshadeRange(bb,idx0,p)).a("}{");
                    if (type=='h') sb.a("helix[Red]:").a(_tfHelix1).a("}{").a(_tfHelix2);
                    if (type=='e') sb.a(_choiceSheet).a("[Yellow]:").a(_tfSheet1).a("}{").a(_tfSheet2);
                    sb.a("}  % ").aln(p);
                }
            }
            if (_cbSkip.s()) sb.a("\\hideseq{").a(i+1).a("} % ").aln(p);
        }
    }

}
