package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.io.File;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.StrapAlign.drawMessage;
import static java.awt.event.KeyEvent.*;
/**HELP

      The keyboard is used to move the alignment cursor and to introduced/removed alignment gaps.<br>

    <div class="floatR">
      <table border="1" >
        <caption>Cursor Movements</caption>
        <tr><th>&larr;     </th><td>Left             </td></tr>
        <tr><th>&rarr;     </th><td>Right            </td></tr>
        <tr><th>Ctrl+&larr;</th><td>To first residue </td></tr>
        <tr><th>Ctrl+&rarr;</th><td>To last residue  </td></tr>
        <tr><th>Alt+&rarr; </th><td>To next gap      </td></tr>
        <tr><th>Alt+&larr; </th><td>To previous gap  </td></tr>
        <tr><th>Home       </th><td>To first row     </td></tr>
        <tr><th>End        </th><td>To last row      </td></tr>
        <tr><th>Ctrl+Home  </th><td>To first residue </td></tr>
        <tr><th>Ctrl+End   </th><td>to last residue  </td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Movement of the view-port</caption>
        <tr><th>PgDown     </th><td>Scroll down</td></tr>
        <tr><th>PgUp       </th><td>Scroll up  </td></tr>
        <tr><th>Shift+PgUp </th><td>Scroll left</td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Deleting gaps</caption>
        <tr><th>Delete              </th><td>Delete gap right from or under cursor     </td></tr>
        <tr><th>Backspace           </th><td>Delete gap left from cursor               </td></tr>
        <tr><th>Ctrl+Delete         </th><td>Delete next gap right from the cursor     </td></tr>
        <tr><th>Ctrl+Backspace      </th><td>Delete next gap left from cursor          </td></tr>
        <tr><th>Ctrl+Shift+Delete   </th><td>Delete entire white space under cursor    </td></tr>
        <tr><th>Ctrl+Shift+Backspace</th><td>Delete entire white space left from cursor</td></tr>
      </table>
      <br>
    </div>

    <div class="floatR" >
      <table border=1>
        <caption>Inserting gaps</caption>
        <tr><th>Space    </th><td>Insert gap under cursor                                   </td></tr>
        <tr><th>Insert   </th><td>Insert gaps until next residue in above row is reached.   </td></tr>
        <tr><th>Ctrl+Ins </th><td>Remove gaps until previous residue in above row is reached</td></tr>
        <tr><th>Shift+Ins</th><td>Insert gaps until next residue in below row is reached    </td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Repetition number. Performing an operation n times</caption>
        <tr><th>4  2 Space </th><td>Insert 42 gaps</td></tr>
        <tr><th>4  2 Delete</th><td>Delete 42 gaps</td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Moving a sequence blocks:  A group of consecutive residues that are not interrupted by gaps is moved left or right.</caption>
        <tr><th>&gt;</th><td>Move right</td></tr>
        <tr><th>&lt;</th><td>Move left </td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Window</caption>
        <tr><th>Ctrl+W</th><td>Close alignment panel</td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Miscellaneous</caption>
        <tr><th>Ctrl+K</th><td>Close protein                    </td></tr>
        <tr><th>Ctrl+*</th><td>Display letters in better quality</td></tr>
        <tr><th>U     </th><td>Upper Case                       </td></tr>
        <tr><th>L     </th><td>Lower Case                       </td></tr>
      </table>
      <br>
    </div>

    <div class="floatR">
      <table border=1>
        <caption>Font Size</caption>
        <tr><th>Ctrl++</th><td>Zoom in </td></tr>
        <tr><th>Ctrl+-</th><td>Zoom out</td></tr>
      </table>
      <br>
    </div>

    <div class="floatR"><table border=1>
        <caption>Go to position</caption>
        <tr><th>4 2 i</th><td>Move cursor to residue index 42</td></tr>
        <tr><th>4 r  </th><td>Move cursor to 4th row         </td></tr>
        <tr><th>4 2 c</th><td>Move cursor to column 42       </td></tr>
        <tr><th>4 2 n</th><td>Move cursor pdb-number 42      </td></tr>
      </table>
      <br>
    </div>

    <div class="floatR"><table border=1>
        <caption>Save / restore position</caption>
        <tr><th>S</th><td>Save current cursor position</td></tr>
        <tr><th>s</th><td>Revert cursor position previously saved</td></tr>
        <tr><th>4  S</th><td>Save current cursor position to "4"</td></tr>
        <tr><th>4  s</th><td>Restore cursor position previously saved in "4"</td></tr>
      </table>
      <br>
    </div>

    <br>
    <div class="clearall"></div>
    <br>The mouse actions follow general conventions:
    <ol class="clearall">
      <li>
        Sequence alignment
        <ul>
          <li>Left-click sets the cursor.</li>
          <li>Middle-click sets the focus without changing the cursor.</li>
          <li>Right-click opens a context menu either for the alignment pane, for a residue selection or the rubber band selection.</li>
          <li>Dragging creates a mouse selection.</li>
          <li>Dragging over more than one rows creates a rectangular region.
            Residue selections and annotations within the rectangle are selected.
            Shift-key for union-set and Ctrl-key for cut-set similar to the program gimp or the MS-Window desktop. </li>
          <li>Alt+Drag scrolls two-dimensionally. Under Unix/Linux also hold Shift-key</li>
        </ul>
      </li>

      <li>
        Row-header
        <ul>
          <li>Dragging up/down changes the order of proteins.</li>
          <li>Ctrl+click selects or deselects the protein.</li>
          <li>Right-click opens a context menu for the protein.</li>
        </ul>
      </li>

      <li>
        Scroll-bar or alignment pane
        <ul>
          <li>Wheel: Scrolls.</li>
          <li>Shift+Wheel: Scrolls vertically</li>
          <li>Ctrl+Wheel: Zoom.</li>
        </ul>
      </li>
    </ol>

    <span class="clearall"></span>
    <div  class="floatR" >
      <table  border=1>
        <caption>Sequence groups</caption>
        <tr><th>G&nbsp;x</th><td>Define all currently selected proteins as group "x". </td></tr>
        <tr><th>g&nbsp;x</th><td>Load and select proteins of sequence group "x" if not already loaded. Select these proteins. </td></tr>
        <tr><th>h&nbsp;x</th><td>Hide all proteins of sequence group "x".   </td></tr>
        <tr><th>u&nbsp;x</th><td>Unselect all proteins of sequence group "x".</td></tr>
      </table>
      <br>
    </div>
    <b>Sequence Groups:</b><br>
    Sequences can be grouped. Groups are designated by a digit or letter.
    The names of all proteins of a group are listed in a file in ./annotations/sequenceGroups/.

<i>SEE_CLASS:StrapView</i>
<i>SEE_CLASS:Gaps2Columns</i>
@author Christoph Gille
*/
public class StrapKeyStroke {
    private final static int BL='B'+'L', BR='B'+'R';
    private static boolean _clrMsg, _shift;
    private final StrapView _view;
    private int _multiStroke, _shiftStart, _prefCol, _action;

    public StrapKeyStroke(StrapView v) { _view=v;}

    public void pe(AWTEvent ev) {
        final StrapView v=_view;
        final int id=ev.getID();
        if (isSimplClick(ev)) _prefCol=v.cursorColumn();
        if (id==MouseEvent.MOUSE_PRESSED) _shift=false;
        if (id!=KEY_PRESSED) return;
        final char ch=keyChar(ev), act=(char)_action;
        final int kcode=keyCode(ev),  modi=modifrs(ev);
        final boolean shift=0!=(modi&SHIFT_MASK), ctrl=0!=(modi&CTRL_MASK),  alt=0!=(modi&ALT_MASK), isDigit=!ctrl && !shift && is(DIGT,ch);
        if (ctrl) setNextProcessHasLogPanel(System.currentTimeMillis());
        if (kcode==VK_SHIFT || kcode==VK_CONTROL) return;
        final Protein ppSel[]=StrapAlign.selectedProteins();
        try {
            if (act=='0' && !isDigit) drawMessage("");
            if (!alt && !ctrl && group(ch)) {
            } else if (isDigit) {
                drawMessage(ANSI_W_ON_B+"Apply next keystroke  "+ (_multiStroke=_multiStroke*10+ch-'0')+" times");
                _action='0';
            } else { /* From here Cursor movements or Edits */
                _action=0;
                StrapAlign.setAlignmentPanelWithFocus(v);
                final int row0=v.cursorRow(), col0=v.cursorColumn(), iA0=v.cursorAminoAcid(), times=_multiStroke>1 ? _multiStroke : 1;

                final Protein p=v.cursorProtein();
                if (p==null) return;
                /* <<< Sequence groups <<< */
                /* ---------------------------------------- */
                /* >>> Cursor movement >>> */
                final int nR=p.countResidues(), maxCol=p.getMaxColumnZ();
                if (shift && !_shift) _shiftStart=col0;
                _shift=shift;
                int iA=MIN_INT, row=MIN_INT, col=MIN_INT;
                if (!ctrl && (kcode==VK_U || kcode==VK_L) && p!=null && p.countNucleotides()==0) {
                    final byte aa[]=p.getResidueTypeFullLength();
                    final int i0=p.getResidueSubsetB1();
                    for(int i=mini(nR,i0+iA0+times); --i>=i0+iA0;) {
                        if (0<=i && i<aa.length) aa[i]=(byte) (kcode==VK_U ? aa[i]&~32 : aa[i]|32);
                    }
                    iA=iA0+times;
                    p.setResidueType(aa);
                }

                final int num=times;
                if (!ctrl)
                    switch (kcode) {
                    case VK_LEFT:  iA=iA0-num;  break;
                    case VK_RIGHT: iA=iA0+num;  break;
                    case VK_DOWN:  row=row0+num;break;
                    case VK_UP:    row=row0-num;break;
                    case VK_HOME:  row=0;       break;
                    case VK_END:   row=MAX_INT; break;
                    case VK_PAGE_UP:  v.scrollRel('v',-num,true); break;
                    case VK_PAGE_DOWN:v.scrollRel('v', num,true); break;
                    }
                if (!ctrl && _multiStroke>0) {
                    switch(kcode) {
                    case VK_I: iA=num-1+p.getResidueIndexOffset();  break;
                    case VK_N: iA=p.resNumAndChain2resIdxZ(false, num,(char)0,(char)0);  break;
                    case VK_C: col=num;     break;
                    case VK_R: row=num-1;   break;
                    }
                }
                if (ctrl)
                    switch (kcode) {
                    case VK_PAGE_UP:    v.scrollRel('h', num*10, true); break;
                    case VK_PAGE_DOWN:  v.scrollRel('h',-num*10, true); break;
                    case VK_HOME:
                    case VK_LEFT:        iA=0; break;
                    case VK_END:
                    case VK_RIGHT:       iA=MAX_INT; break;
                    case VK_DOWN:       row=MAX_INT; break;
                    case VK_UP:         row=0; break;
                    default:
                    }

                final int resCol[]=p.getResidueColumn();
                if (!ctrl && alt && resCol!=null && (kcode==VK_LEFT || kcode==VK_RIGHT)) {
                    final int d=(kcode==VK_RIGHT ? 1 : -1);
                    iA=iA0+d;
                    while(iA>0 && iA<resCol.length-1) {
                        final int diff=resCol[iA+d]-resCol[iA];
                        if (diff!=1 && diff!=-1) break;
                        iA+= d;
                    }
                }

                if (0==(modi&(CTRL_MASK|ALT_MASK)) && kcode==VK_S) v.saveRestore(shift,intObjct(_multiStroke));
                if (row!=MIN_INT) v.setCursor(StrapView.CURSOR_COLUMN, v.proteinInRow(mini(v.countRows()-1, maxi(0,row))),  _prefCol>=0?_prefCol:col0);
                else if (col!=MIN_INT) v.setCursor(StrapView.CURSOR_COLUMN, p,  col);
                else if (iA!=MIN_INT) v.setCursor(0,p, mini(nR-1,maxi(0,iA)));
                if (iA!=MIN_INT || col!=MIN_INT) _prefCol=v.cursorColumn();
                if (shift && (row==MIN_INT && (iA!=MIN_INT||col!=MIN_INT) || kcode==VK_UP || kcode==VK_DOWN)) {
                    final int beyond=(iA>=nR || col>maxCol) ? 1 : 0;
                    StrapView.selectResidues(true, true, v.cursorProtein(), _shiftStart,  v.cursorColumn()+beyond, null, v);
                }

                if (iA!=MIN_INT||col!=MIN_INT||row!=MIN_INT) {
                    if (_clrMsg) StrapAlign.drawMessage(null);
                    _clrMsg=false;
                    v.scrollCursorToVisible();
                    if (ev instanceof KeyEvent) ((KeyEvent)ev).consume();
                    return;
                }
                /* <<< Cursor movements <<< */
                /* ---------------------------------------- */
                /* >>> Editing  >>> */
                final Protein
                    ppRow[]=v.proteinsInRow(v.cursorRow()),
                    pp[]= (ppRow.length>0 && cntains(ppRow[0],ppSel)) ? ppSel : ppRow;
                if (pp.length==0) return;
                boolean isChanged=false;
                switch (ch) {
                case ' ':
                    isChanged=
                        ctrl ? insertDelete(BL,pp,col0,times) :
                    shift ? insertDelete(BR,pp,col0,times) :
                    insertDelete('L',pp,col0,times);
                    break;
                case '>':
                    isChanged=insertDelete(BR,pp,col0,-times);
                    if (isChanged) insertDelete(BL,pp,v.cursorColumn(),times);
                    break;
                case '<':
                    isChanged|=insertDelete(BL,pp,col0,-times);
                    if (isChanged) insertDelete(BR,pp,v.cursorColumn(),times);
                    break;
                case 'u':
                case 'l': isChanged|=!ctrl;break;
                }
                switch (kcode) {
                case VK_F1: StrapAlign.allMenus(true,true,null); break;
                case VK_DELETE:
                    isChanged=
                        ctrl&&!shift ? insertDelete(BR,pp,col0,-times) :
                    ctrl&&shift ? deleteTotal('R',pp,col0) :
                    insertDelete('R',pp,col0,-times);
                    break;
                case VK_BACK_SPACE:
                    isChanged=
                        ctrl&&!shift ? insertDelete(BL,pp,col0,-times) :
                    ctrl&&shift ? deleteTotal('L',pp,col0) :
                    insertDelete('L',pp,col0,-times);
                    break;
                case VK_INSERT:
                    final Protein p2=v.proteinInRow(shift ? row0+1:row0-1);
                    if (p2==null) break;
                    isChanged= ctrl?
                        leftAlign(pp,col0,p,p2):
                        rightAlign(pp,col0,p,p2);
                    break;
                case VK_W: if (ctrl && !shift) StrapAlign.closeAlignmentPanel(v); break;
                default:
                }
                if (ppSel.length==StrapAlign.visibleProteins().length && isChanged) {
                    StrapAlign.drawMessage(ANSI_W_ON_R+"Warning: Inserting/deleting gaps does not have a visible effect if all proteins are selected.");
                    _clrMsg=true;
                }
                if (isChanged) {
                    StrapEvent.dispatch(StrapEvent.ALIGNMENT_CHANGED);
                    v.scrollCursorToVisible();
                }
            }
        } finally{
            if (!isDigit) _multiStroke=0;
        }
    }

    /* ---  The following Methods insert or delete a gap in various ways --- */
    private final boolean deleteTotal(int op, Protein[] pp, int col) {
        final int offset=op=='R' ? 1 : 0;
        int gapMin=MAX_INT;
        final int iAa[]=column2idx('R',pp,col,op=='R');
        if (iAa==null) return false;
        for(int iP=0;iP<pp.length;iP++) {
            final Protein p=pp[iP];
            final int gap=p==null?-1 : p.getResidueGap(iAa[iP]+offset);
            if (gap>=0) gapMin=mini(gapMin,gap);
        }
        for(int iP=0;iP<pp.length;iP++) {
            final Protein p=pp[iP];
            if (p==null) continue;
            final int iA=iAa[iP]+offset;
            final int g=p.getResidueGap(iA);
            if (g>=0) p.setResidueGap(iA,maxi(0,g-gapMin));
        }
        return true;
    }

    private int[] column2idx(int op,Protein[] prot, int col, boolean thisOrPrev) {
        final int iAa[]=new int[prot.length];
        final int protectN=_view.getProtectFromColumn('N');
        for(int iP=0;iP<prot.length;iP++) {
            final Protein p=prot[iP];
            if (p==null) continue;
            final int gap[]=p.getResidueGap(), nR=p.countResidues();
            int ia=thisOrPrev ? p.column2thisOrPreviousIndex(col) : p.column2nextIndexZ(col);
            while (op==BL && ia>0 && gap[ia]==0) ia--;
            while (op==BR && ia+1<nR-1 && gap[ia+1]==0) ia++;
            if (ia>=nR) ia=-1;
            if (op==BR && protectN>0 && col<protectN && p.getResidueColumnZ(ia)>=protectN) {
                ia=p.column2thisOrPreviousIndex(protectN-1);
            }
            if (ia<0) return null;
            iAa[iP]=ia;
        }
        return iAa;
    }
    private final boolean insertDelete(int op, Protein[] pp, int col, int num) {
        final int
            n2w[]=Protein.isWide() ? null : StrapAlign.g2c().narrow2wideColumn(),
            countColumns=StrapAlign.g2c().countColumns(),
            iA[]=column2idx(op,pp,col,false),
            protectW=_view.getProtectFromColumn('W'),
            protectN=_view.getProtectFromColumn('N'),
            n;
        final boolean delRight=op=='R' || op==BR;
        /* Are there positions before protectN */
        boolean protect=false;
        if (protectW>0) {
            for(int iP=pp.length; --iP>=0;) {
                protect=protect || pp[iP]!=null && pp[iP].getResidueColumnZ(iA[iP])<protectN;
            }
        }
        /* What is the max number that can be inserted/deleted */
        if (num>0 && protect && protectN<countColumns) {
            int max=MAX_INT;
            for(int iP=0;iP<pp.length;iP++) {
                if (pp[iP]==null) continue;
                final int i1=iA[iP], i2=pp[iP].column2thisOrPreviousIndex(protectN-1);
                final int c1=pp[iP].getResidueColumnZ(i1);
                final int c2=pp[iP].getResidueColumnZ(i2);
                if (0<=c1 && i1<=i2 && c1<=c2 && (n2w==null||c2<n2w.length) && c2<protectN) {
                    final int g=protectW-(n2w==null?c1:n2w[c1]) -(i2-i1)-1;
                    if (g<max)max=g;
                }
            }
            n=mini(num,max);
        } else if (num<0) {
            int max=MAX_INT;
            for(int iP=0;iP<pp.length;iP++) {
                if (pp[iP]!=null) {
                    max=mini(max, pp[iP].getResidueGap(iA[iP]+(delRight?1:0)));
                }
            }
            n=-mini(-num,max);
        } else n=num;
        if (iA==null && n!=0) return false;
        final int[][] gaps=new int[pp.length][];
        /* Do insertions/deletions */
        for(int iP=0;iP<pp.length;iP++) {
            final Protein p=pp[iP];
            final int nCol=p.getMaxColumnZ();
            final int nR=p.countResidues();
            final int gg[]=gaps[iP]=p.getResidueGap();
            final int i1=iA[iP];
            final int iChange= delRight ?  i1+1 : i1;
            if (protect && p.getResidueColumnZ(iChange)>=protectN) continue;
            if (i1>=sze(gg)) continue;
            gg[iChange]=maxi(0,n+gg[iChange]);
            if (protect && /*(op!=BR && op!=BL) &&*/ protectN<nCol && protectN>p.getResidueColumnZ(i1)) {
                final int i2=p.column2thisOrPreviousIndex(protectN-1);
                if (0<=i2 && i2+1<mini(nR, gg.length) && (n2w==null||i2+1<n2w.length)) {
                    if (n>0) {
                        int gapBias=n;
                        final int c2=p.getResidueColumnZ(i2);
                        if (0<=c2 && (n2w==null||c2<n2w.length)) {
                            final int space=protectW-(n2w==null?c2:n2w[c2])-n;
                            for(int i=i2+2; --i>=i1 && gapBias>0;) {
                                final int g0=gg[i];
                                gg[i]=maxi(0,gg[i]- (i==i2+1?mini(space,gapBias):gapBias) );
                                gapBias-=g0-gg[i];
                            }
                        }
                    } else  gg[i2+1]-=n;
                }
            }
        }
        for(int iP=pp.length; --iP>=0;)  pp[iP].setResidueGap(gaps[iP]);
        StrapAlign.setNotSaved();
        return true;
    }
    // ---
    private final boolean rightAlign(Protein prot[], int pos, Protein p1, Protein p2) {
        final int
            ia1=p1.column2nextIndexZ(pos),
            ia2=p2.column2nextIndexZ(pos),
            h1=p1.getResidueColumnZ(ia1),
            h2=p2.getResidueColumnZ(ia2);
        if (h1<pos || h2<pos) return false;
        if (h2>h1) return insertDelete('L',prot,p1.getResidueColumnZ(ia1),sumGaps(p2,ia2)-sumGaps(p1,ia1));
        else {
            final int nR2=p2.countResidues();
            int ia=ia2;
            while( ia<nR2-1 && p2.getResidueGap(ia+1)==0) ia++;
            _view.setCursor(StrapView.CURSOR_COLUMN,p2, p2.getResidueColumnZ(ia)+1);
        }
        return true;
    }
    // ---
    private boolean leftAlign(Protein[] prot, int pos, Protein p1, Protein p2) {
        final int
            ia1=p1.column2nextIndexZ(pos),
            ia2=p2.column2nextIndexZ(pos),
            h1=p1.getResidueColumnZ(ia1),
            h2=p2.getResidueColumnZ(ia2);
        if (h1<pos || h2<pos || ia2<1) return false;
        if (h1!=h2) {
            if (ia1>p1.countResidues()-2) return false;
            final int count=sumGaps(p1,ia1)-sumGaps(p2,ia2);
            for(int i=0;i<count;i++) {
                if (!insertDelete(BL,prot,p1.getResidueColumnZ(ia1),-1)||
                    !insertDelete('L',prot,p1.getResidueColumnZ(ia1+1),1)) return true;
            }
            return true;
        } else {
            int ia=ia2;
            while( ia>0 && p2.getResidueGap(ia)==0) ia--;
            if (p2.getResidueGap(ia)==0) return false;
            _view.setCursor(StrapView.CURSOR_COLUMN, p2, p2.getResidueColumnZ(ia)-1);
        }
        return true;
    }
    public boolean group(char ch) {
        final char act=(char)_action;
        final Protein ppSel[]=StrapAlign.selectedProteins();
        final File fGroup=(act|32)=='g' || act=='h' || act=='u' || act=='d' ? file(DIR_ANNOTATIONS+"/sequenceGroups/"+ch) : null;
        String nttl=null;
        try {
            if (act=='0' && !is(DIGT,ch)) drawMessage("");
            if (act=='G') {
                wrte(fGroup,new BA(0).join(ppSel," "));
                drawMessage(ANSI_GREEN+"Defining group \""+ch+"\" with "+ppSel.length+" proteins");
                _action=0;
            } else if (fGroup!=null) {
                final BA ba=readBytes(fGroup);
                Protein pp[]=null;
                if (!is(LETTR_DIGT,ch)) drawMessage(ANSI_W_ON_R+"canceled ");
                else {
                    if (ba!=null) {
                        StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED, ba);
                        final String ss[]=splitTokns(ba);
                        pp=new Protein[ss.length];
                        for(int i=ss.length; --i>=0; ) pp[i]=SPUtils.proteinWithName(ss[i], StrapAlign.proteins());
                        pp=rmNullA(pp,Protein.class);
                    }
                }
                if (act=='g') StrapAlign.setIsInAlignment(false, 0, StrapAlign.proteins());
                if (sze(pp)>0) {
                    if (act=='g' || act=='d') {
                        adAll(pp,  StrapAlign.selectedObjectsV());
                        drawMessage(ANSI_GREEN+"Displayed "+pp.length+" sequences of group \""+ch+"\"");
                        StrapAlign.setIsInAlignment(true, -1, pp);
                        new StrapEvent(this,StrapEvent.PROTEINS_SHOWN).setParameters(new Object[]{pp, intObjct(StrapAlign.cursorRow())}).run();
                        StrapEvent.dispatch(StrapEvent.OBJECTS_SELECTED);
                    }
                    if (act=='h') {
                        StrapAlign.setIsInAlignment(false, 0, pp);
                        drawMessage(ANSI_GREEN+pp.length+" proteins hidden");
                    }
                    if (act=='u') {
                        rmAll(pp,  StrapAlign.selectedObjectsV());
                        StrapEvent.dispatch(StrapEvent.OBJECTS_SELECTED);
                        drawMessage(ANSI_GREEN+pp.length+" proteins unselected");
                    }
                } else error(RED_ERROR+"No such sequence group \""+ch+"\""+ (sze(fGroup)==0 ? ".\nNo such file: "+fGroup : ""));
                _action=0;
            } else if (!is(DIGT,ch) && (ch=='G' || ch=='g' || ch=='d' || ch=='h' || ch=='U')) {
                if (ch=='G' && ppSel.length==0) drawMessage(ANSI_W_ON_R+"Error defining sequence group:  Select proteins first!");
                else {
                    _action=ch;
                    nttl=
                        ch=='g' || ch=='d' ? "Displaying sequences of a group." :
                        ch=='h' ? "Hiding sequences." :
                        ch=='U' ? "Unselecting sequences." :
                        ch=='G' ? "Defining sequence group for "+ppSel.length+" proteins." :
                        null;
                }
            } else return false;
            return true;
        } finally{
            if (nttl!=null) drawMessage(ANSI_W_ON_B+nttl+" Now type the letter designating the group!");
        }
    }

    private static int sumGaps(Protein p, int ias) {
        if (ias<0) return 0;
        final int gg[]=p.getResidueGap(), N=mini(gg.length-1, ias, p.countResidues()-1)+1;
        int g=-1;
        for(int i=N; --i>=0; ) g+=gg[i];
        return g+N;
    }

}
