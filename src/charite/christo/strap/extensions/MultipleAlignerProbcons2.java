package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import charite.christo.protein.*;
import charite.christo.*;
import charite.christo.strap.*;

/**HELP
Author: Mahathi Mahabhashyam
<br>
Publication: PUBMED:15687296
*/
// probcons  -v --consistency 2  --iterative-refinement 1000     input_file
public final class MultipleAlignerProbcons2 extends AbstractAligner implements HasPrgParas, SequenceAlignerSorting {
    {
        final  String PARAMERS=
            "-c=\t Use 0<=REPS<=5 (default: 2)<br>passes of consistency transformation:\n"+
            "-ir=\t Use 0<=REPS<=1000 (default: 100)<br>passes of iterative refinement.:";
        defineParameters(FURTHER_PARAMETERS, PARAMERS);
    }

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        final String n="probcons_v1_12",
            instScript=
                "cd probcons;\n"+
                "make;\n"+
            "cp probcons ../"+n+".exe",
            control=n+"\n/usr/bin/probcons"+
            PFX_DPKG+"probcons"+
            BasicExecutable.PFX_FIX_INCLUDE+"probcons/ProjectPairwise.cc probcons/CompareToRef.cc probcons/Main.cc ";

        final BasicExecutable e=initX(control, null, instScript);
        final ChExec ex=
            e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.LOG|ChExec.CYGWIN_DLL)
            .setCommandLineV(e.fileExecutable(), getPrgParas(), mfaInFile(seqs))
            .dir(dirTemp());
        ex.run();
        return parse(seqs.length, ex.getStdoutAsBA(), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);
    }
}
