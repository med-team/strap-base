package charite.christo.strap.extensions;
import charite.christo.strap.*;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import java.util.Arrays;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
Author: Ivaylo Ilinkin and Ravi Janardan
Publication:  PUBMED:20122279
*/
public class Aligner3D_Smolign extends AbstractAligner implements SequenceAligner3D {
    public Superimpose3D.Result computeResult(byte[][]sequ, Protein[] pp) {
        final File fJar=InteractiveDownload.downloadFileIfNewer(ChConstants.URL_STRAP_JARS+"/Smolign.jar");
        final String ERR=RED_WARNING+"Aligner3D_Smolign ";
        if (sze(fJar)==0) {
            putln(ERR,"Failed downloading Smolign.jar");
            return null;
        }
        final int n=sze(pp);
        if (777==777) {
            BA sb=null;
            for(int i=0; i<n; i++) {
                final File f=pdbFile(pp[i],  i, (boolean[])null, ProteinWriter.IDX_FOR_RESNUM|ProteinWriter.PDB|ProteinWriter.ATOM_LINES|ProteinWriter.MET_INSTEAD_OF_MSE);
                if (f==null) continue;
                if (sb==null) sb=new BA(333); else sb.a(';');
                sb.a(f.getName());
            }
            final BasicExecutable aex=getNativeExec(true);
            final ChExec ex=aex.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.SHOW_STREAMS).dir(dirTemp());
            ex.setCommandLineV(file(systProprty(SYSP_BIN_JAVA)),"-Xmx500M","-jar", fJar, "-f", sb);
            ex.run();
        }
        final String path=dirTemp()+"/output/flxAlignment/Alignment.txt";
        final BA ba=readBytes(path);
        if (ba==null) {
            putln(ERR, "Could not read ",path);
            return null;
        }
        shwTxtInW("Smolign",ba);
        final int[] ends=ba.eol();
        final byte T[]=ba.bytes();
        final int[][] num=new int[n][ends.length];

        for(int i=0; i<n; i++) Arrays.fill(num[i], -1);
        final ChTokenizer TOK=new ChTokenizer();
        int iL_header=-1;
        for(int iL=1; iL<ends.length; iL++) {
             final int b=ends[iL-1]+1, e=ends[iL];
             if (e-b>5 && T[b]=='s' && is(DIGT,T,b+1) && strstr(".pdb", T, b,e)>0 ){
                 iL_header=iL;
                 break;
             }
        }
        if (iL_header<0) {
            assrt();
            return null;
        }
        TOK.setText(T,ends[iL_header-1]+1, ends[iL_header]);
        final int col2iP[]=new int[n];
        for(int iP=0; TOK.nextToken(); iP++) {
            final int from=TOK.from();
            if (T[from]!='s' || !is(DIGT,T,from+1)) {
                assrt();
                return null;
            }
            col2iP[iP]=atoi(T,from+1,TOK.to());
        }
        for(int i : col2iP) {
            if (i<0||i>=n) {
                assrt();
                return null;
            }
        }
        for(int iL=iL_header+1; iL<ends.length; iL++) {
            final int b=ends[iL-1]+1, e=ends[iL];
            TOK.setText(T,b,e);
            for(int iP=0; iP<n; iP++) {
                TOK.nextToken();
                TOK.nextToken();
                num[col2iP[iP]][iL-8]=TOK.asInt()-1;
            }
        }
        final byte[] gg[]=AlignUtils.applyAlignPositions(num, sequ);
        final Superimpose3D.Result result=new Superimpose3D.Result(n);
        result.setGappedSequences(gg);
        return result;
    }

}
