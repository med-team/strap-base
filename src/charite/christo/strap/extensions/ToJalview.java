package charite.christo.strap.extensions;
import charite.christo.strap.*;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
//import java.net.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

Jalview is an excellent well-known alignment viewer.<br>
*/

public class ToJalview {
    public final static int SECSTRU=1;

    /*
      description	sequenceId	sequenceIndex	start	end	featureType	score
      http://www.jalview.org/help/html/features/featuresFormat.html
    */

    private static BA ff(int opts, Protein pp[]) {
        final BA sb=new BA(9999), sb1=new BA(9999).aln("helix\tFF0000\nbeta_strand\taaAA00");
        int countA=0;
        final Collection<String> vFeature=new HashSet();
        for(Protein p : pp) {
            if (p==null) continue;
            final byte[] ss=p.getResidueSecStrType();
            if ( (opts&SECSTRU)!=0 && ss!=null) {
                for(int  iType=2; --iType>=0;) {
                    int count=0;
                    final char type=iType==0 ? 'h' : 'e';
                    for(int i=0; i<ss.length; i++) {
                        if ((ss[i]|32)!=type) continue;
                        sb
                            .a(type=='h' ? "Helix " : "Beta strand ").tab(++count)
                            .tab((nam(p))).tab(-1)
                            .tab(i+1);
                        for(; i<ss.length && (ss[i]|32)==type; i++);
                        sb.tab(i).aln(type=='h' ? "helix" : "beta_strand");
                    }
                }
            }
            final BA sbTmp=new BA(333);
            for(ResidueAnnotation a : p.residueAnnotations()) {
                final String fn=a.featureName();
                final Object color=orO(a.getColor(), "00FF00");
                final int offset=ResSelUtils.selAminoOffsetZ(a);
                final boolean bb[]=a.getSelectedAminoacids();
                if ( (a.getVisibleWhere()&VisibleIn123.JALVIEW)==0 || !a.isEnabled() || countTrue(bb)==0) continue;
                sbTmp.clr().a(nam(a));
                for(int j=2; --j>=0;) {
                    final CharSequence txt=rmHtmlHeadBody(a.value(j==0?"Remark":ResidueAnnotation.BALLOON));
                    if (txt==null) continue;
                    sbTmp.a("<br>");
                    if (containsHtmlTags(txt)) sbTmp.a(txt);
                    else sbTmp.filter(FILTER_HTML_DECODE,0,txt);
                }
                if (fn==null) sb1.a("annotation").tab(++countA).aln(color);
                for(int i=0; i<bb.length; i++) {
                    if (!bb[i]) continue;
                    sb.tab(toBA(addHtmlTags(sbTmp)).replaceChar('\t',' ').replaceChar('\r',' ').replaceChar('\n',' '))
                        .tab((nam(p))).tab(-1)
                        .tab(i+1+offset);
                    for(; i<bb.length && bb[i]; i++);
                    sb.tab(i+offset);
                    if (fn==null) sb.a("annotation").aln(countA);
                    else {
                        sb.aln(fn);
                        if (vFeature.add(fn)) sb1.tab(fn).aln(color);
                    }

                }
            }
            final String txt=p.balloonText(true);
            if (sze(txt)>0) {
                sb.tab(toBA(addHtmlTags(txt)).replaceChar('\t',' ').replaceChar('\r',' ').replaceChar('\n',' '))
                    .tab((nam(p)))
                    .tab(-1)
                    .tab(0).tab(0)
                    .aln("annotation");
            }

        }
        sb1.a('\n').aln(sb);
        //  debugExit("\n"+ANSI_CYAN+sb1+ANSI_RESET);
        return sb1;
    }

    public void launchJalview(int opt, Protein pp[], File fPh) {
        if (pp==null) return;
        final File fFeature=file(STRAPOUT+"/jalview/features.gff"), fAlign=file(STRAPOUT+"/jalview/align.msf");
        delFile(fFeature);
        delFile(fAlign);
        wrte(fFeature, ff(opt,pp));
        final BA sb=new BA(9999);
        final ExportAlignment w=new ExportAlignment();
        w.setProteins(pp);
        w.getText(ExportAlignment.MSF|ExportAlignment.FILE_SUFFIX, sb);

        wrte(fAlign,sb);
        sb.clr().a("http://www.jalview.org/services/launchApp?open=").filter(FILTER_URL_ENCODE,0,fAlign)
            .a("&features=").filter(FILTER_URL_ENCODE,0,fFeature);
        if (sze(fPh)>0) sb.a("&tree=").filter(FILTER_URL_ENCODE,0,fPh);
        visitURL(sb.a(VISIT_URL_JAVAWS),0);
    }
}
// http://www.compbio.dundee.ac.uk/~ws-dev1/jalview/latest/webstart/jalview.jnlp
// http://www.compbio.dundee.ac.uk/~ws-dev1/jalview/release/jalview-source.tar.gz
