package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import charite.christo.strap.*;
import charite.christo.protein.*;
import charite.christo.*;
/**HELP
This class reads protein files like http://www.bioinformatics.org/strap/data/fileFormats/example.hssp.txt

*/
public class AlignmentWriterHSSP implements AlignmentWriter {
    private String[] _names;
    private byte _ali[][], _aliTranspon[][];
    private Protein _pp[];
    private int _fstCol,_lstCol=9999999;
    public void setProteins(Protein... pp) { _pp=pp;}
    public void setNames(String[] names) { _names=names;}
    public void setColumnRange(int first, int last){ _fstCol=first; _lstCol=last;}
    public void setResiduesPerLine(int resPerLine) {}
    public void setCharForGapAndTerminalBlanks(char m,char n, char c) {}

    public String getFileExtension() { return "hssp";}
    public void getText(long options, BA sb){
        sb.aln("HSSP       HOMOLOGY DERIVED SECONDARY STRUCTURE OF PROTEINS ");
        final Protein withSecStr=StrapAlign.cursorProtein();
        if (withSecStr==null) {
            sb.aln("\nERROR: "+
                   "The HSSP-format requires a reference protein with 3D coordinates.\n"+
                   "Please indicate the reference protein by placing the alignment cursor on the protein");
            return;
        }

        int firstAA=withSecStr.column2thisOrPreviousIndex(_fstCol);
        final int lastAA=withSecStr.column2thisOrPreviousIndex(_lstCol);
        sb.aln("# the structure file is the one of the alignment cursor")
            .a("# structure file=").a(withSecStr.getName()).a(" fromIdx=").a(firstAA).a(" toIndex=").a(lastAA);
        if (_pp==null) return;
        if (idxOf(withSecStr,_pp)<0) {
            final int n=_pp.length;
            final Protein pp[]=new Protein[n+1];
            pp[n]=withSecStr;
            System.arraycopy(_pp,0,pp,0,n);
            _pp=pp;
            _names=chSze(_names,n+1);
            _names[n]=withSecStr.getName();
        }
        sb.a("\n# NUMBER OF PROTEINS: ").a(_pp.length).a('\n').a("# PROTEINS: ").join(_names," ").a('\n');
        if (firstAA<0) firstAA=0;
        transpose();
        int iBlock=0;
        while(exportHSSP(sb,iBlock++,withSecStr,firstAA,lastAA));
        sb.aln("## ");
    }
    // ----------------------------------------
    private boolean exportHSSP(BA sb,int iBlock,Protein withSecStr, int firstAA,int lastAA) {
        final int nProt=_ali.length;
        if (nProt<1) return false;
        int i,j;
        boolean anOtherBlock=true;
        final int colMin=iBlock*70;
        int colMax=(iBlock+1)*70;
        if (nProt<=colMax) {colMax=nProt; anOtherBlock=false;}
        if (iBlock==0) {
            sb.a("SEQLENGTH   ").a(lastAA-firstAA)
                .a("\nNALIGN       ").a(nProt)
                .aln("\n  NR.    ID         STRID   %IDE %WSIM IFIR ILAS JFIR JLAS LALI NGAP LGAP LSEQ2 ACCNUM     PROTEIN");
        }
        sb.a("## ALIGNMENTS    ").a(colMin+1).a("  -  ").a(colMax)
            .a("\n SeqNo  PDBNo AA STRUCTURE BP1 BP2  ACC NOCC  VAR  ");
        // ....:....1....:....2....:....3....:....4....:....5....
        for(i=0;i<nProt;i++) {
            char c='.';
            if (i%10==4) c=':';
            if (i%10==9) c= (char)(((i+1)/10)%10+'0');
            sb.a(c);
        }
        sb.a('\n');
        int pdbNo[]=withSecStr.getResidueNumber();
        if (pdbNo==null) {
            pdbNo=new int[withSecStr.countResidues()];
            for(int c=0; c<pdbNo.length; c++) pdbNo[c]=c+1;
        }
        final byte resType[]=withSecStr.getResidueType(), secStr[]=withSecStr.getResidueSecStrType();
        final float accessibility[]=withSecStr.getResidueSolventAccessibility();
        for(j=firstAA;j<lastAA && j<pdbNo.length;j++) {
            final int num=pdbNo.length>j && pdbNo[j]!=MIN_INT ? (pdbNo[j]) : (0);
            sb
                .a(j+1-firstAA,6)
                .a(num,5)
                .a(' ',3)
                .a((char)resType[j])
                .a(' ',2).a((char)(maxi(' ',get(j,secStr)))).a("           0   0")
                .a((int)get(j,accessibility),5).a(1,10).a(' ',2).a(_aliTranspon[j],colMin,colMax).a('\n');
        }
        return anOtherBlock;
    }

    private void transpose() {
        final Protein pp[]=_pp;
        final int n=pp.length;
        final int hMax=SPUtils.maxColumn(pp);
        _ali=new byte[n][hMax];_aliTranspon=new byte[hMax][n];
        for(int j=0;j<hMax;j++) {
            for(int i=0;i<n;i++)_ali[i][j]=_aliTranspon[j][i]=32;
        }
        for(int i=0;i<n;i++) {
            final Protein p=pp[i];
            final byte aa[]=p.getResidueType();
            final int resPos[]=p.getResidueColumn(), nA=p.countResidues();
            for(int j=0;j<nA;j++) {
                final byte b=(byte)(aa[j]&~32);
                if (b<'A' || b>'Z') continue;
                final int pos=resPos[j];
                if (pos>=hMax) continue;
                _ali[i][pos]=_aliTranspon[pos][i]=b;
            }
        }
    }
}
