package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
/**HELP
   Mafft is an  alignment program published in PUBMED:15661851
*/
public class MultipleAlignerMafft extends charite.christo.strap.AbstractAligner implements HasPrgParas, SequenceAlignerSorting  {
    {
        defineParameters(FURTHER_PARAMETERS, null);
    }
    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        if (!Insecure.EXEC_ALLOWED) return null;
        final String
            n="mafft-6.860-without-extensions",
            instScript=
            "cd "+n+"/core\n"+
            "make;\n"+
            "cp ../scripts/mafft ../../"+n+".exe";
        final BasicExecutable e=initX(n+"\n/usr/bin/mafft"+PFX_DPKG+"mafft", null, instScript);
        final ChExec ex=e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.LOG)
            .setCommandLineV(ChExec.CYGWINSH, e.fileExecutable(),getPrgParas(),mfaInFile(seqs) )
            .dir(dirTmp());
        if (strEquls(e.dirBinaries().toString(),e.fileExecutable())) ex.addToEnvironement("MAFFT_BINARIES="+file(e.dirBinaries()+("/"+n+"/core")));
        ex.run();
        return parse(seqs.length, ex.getStdoutAsBA(), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);
    }

}
