package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.*;
import charite.christo.strap.*;
import java.io.File;
import static charite.christo.ChUtils.*;
/**HELP

Home: <i>HOME_PAGE:MultipleAlignerDialignT</i>
<br>
Author: Amarendran R. Subramanian.
<br>
Publication PUBMED:15784139
@author Christoph Gille
*/
//  gcc -lm *.c
public final class MultipleAlignerDialignT extends AbstractAligner implements HasPrgParas,  SequenceAligner {
    {
        defineParameters(FURTHER_PARAMETERS, "dialignT.rsc");
    }

    private static String conf;
    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        final String
            n="DIALIGN-TX_1.0.2",
            instScript="cd "+n+"/source\n"+
            "make\n"+
            "cp dialign-tx"+RPLC_WIN_DOT_EXE+" ../../"+n+".exe";
        final BasicExecutable e=initX(n+"\n/usr/bin/dialign-tx"+PFX_DPKG+"dialign-t", null, instScript);
        final File dirTmp=dirTemp(), fileOut=file(dirTmp,"output.msf"), mfaInfile=mfaInFile(seqs), fileExe=e.fileExecutable(), dirBin=e.dirBinaries();
        if (strEquls("/usr/bin",fileExe)) conf="/usr/share/dialign-tx/";
        if (conf==null) {
            for(String s : lstDir(dirBin)) {
                if (s.startsWith("DIALIGN") && !s.endsWith(".exe")) {
                    conf=(dirBin+"/"+s+"/conf").replace('\\','/');
                    break;
                }
            }
        }
        e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL|ChExec.LOG)
            .setCommandLineV(fileExe, getPrgParas(),conf,mfaInfile,fileOut)
            .dir(dirTmp)

            .run();
        return parse(seqs.length, readBytes(fileOut), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);

    }
}
