package charite.christo.strap.extensions;
import charite.christo.strap.*;
import charite.christo.protein.*;
import charite.christo.*;
//import static charite.christo.ChUtils.*;

/**HELP
   CE/CL ported to Java by Andreas Prlic
*/
public class Superimpose_CEPROXY extends AbstractAlignerProxy implements HasScore, Superimpose3D, SequenceAligner3D {
    public final static int MIN_CALPHA=16;
    public long getPropertyFlags() {return PROPERTY_ONLY_TWO_SEQUENCES|PROPERTY_LOCAL_ALIGNMENT; }
    @Override public String getRequiredJars(){ return jarFile(BIOJAVA_3D); }

}
