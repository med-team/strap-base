package charite.christo.strap.extensions;
/**HELP

The C-alpha positions of two proteins are superimposed.  The
connectivity and the order of atoms and the type of amino acids are
not used for the superposition.
<br>
<b>Author:</b> Andrean Goede
<br>
<b>Ported from Pascal:</b>  Matthias Dunckel.

*/
public final class Superimpose_GoedePROXY extends charite.christo.strap.AbstractAlignerProxy implements charite.christo.protein.Superimpose3D {
    @Override public String getRequiredJars() { return "andreanOVL.jar";}
}
