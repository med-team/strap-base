package charite.christo.strap.extensions;
import charite.christo.strap.*;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import static charite.christo.ChUtils.*;
/*
Author: Matthew Menke
Publication:  PUBMED:18193941
Home: http://groups.csail.mit.edu/cb/matt/
*/
public class Aligner3D_Matt extends AbstractAligner implements SequenceAligner3D {
    public Superimpose3D.Result computeResult(byte[][]seqs, Protein[] pp) {
        final String
            n="Matt",
            inst=
            "cd Matt-src-1.00\n"+
            "make\n"+
            "cp bin/"+n+RPLC_WIN_DOT_EXE+" ../"+n+".exe\n";
        final BasicExecutable aex=initX(n, null, inst);
        final File dir=aex.dirTemp();
        final Object ff[]=new Object[pp.length];
        for(int i=0; i<pp.length; i++) {
            ff[i]=nam(pdbFile(pp[i], i, (boolean[])null, ProteinWriter.CHAIN_SPACE));
        }
        //    aex.setSourceInstallationScript(inst);
        //   aex.initExecutable();
        aex.exec(ChExec.SHOW_STREAMS|ChExec.CYGWIN_DLL)
            .dir(dir)
            .setCommandLineV(aex.fileExecutable(), "-b","-o", "alignment", ff)
            .run();
        final BA txt=readBytes(file(dirTemp(),"alignment_bent.fasta"));
        return parse(seqs.length, txt, null, Superimpose3D.PARSE_MSA_START_S);
    }

}
