package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;

import static charite.christo.ChUtils.*;
/**HELP
<b>Server:</b> http://alexander.compbio.ucsf.edu/cgi-bin/nnpredict.pl
<br>
<b>Author:</b> Donald Kneller, Server: Nomi Harris
   @author Christoph Gille
*/
public class SecondaryStructure_NNPREDICT extends AbstractPredictionFromAminoacidSequence implements SecondaryStructure_Predictor,HasControlPanel, NeedsInternet  {
  /* --- Control panels --- */
    /* new ChCombo("none","all-alpha","all-beta","alpha/beta"); */

    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final BA data=getResultText(sequ,"http://alexander.compbio.ucsf.edu/cgi-bin/nnpredict.pl",new Object[][]{
        //final BA data=getResultText(sequ,"http://www.cmpharm.ucsf.edu/~nomi/nnpredict.pl",new Object[][]{
                                           {"text",sequ},
                                           {"option","none"}
            }, false);
        if (data==null) return null;
        final byte bb[]=data.bytes();
        final int begin=strstr(STRSTR_IC,"<br></b><tt>",data);
        final int end=strstr(STRSTR_IC,"</tt>",data);
        char[] cc=null;
        while(true) {
            int count=0;
            boolean isTag=false;
            for(int i=begin; i<end; i++) {
                final byte c=bb[i];
                if (c=='>') isTag=false;
                else if (c=='<') isTag=true;
                else if (!isTag) {
                    if (cc!=null) cc[count]= c=='-' ? ' ' : (char)c;
                    count++;
                }
            }
            if (cc==null) cc=new char[count]; else break;
        }
        return cc;
    }

}
