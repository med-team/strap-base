package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.*;

import static charite.christo.ChUtils.*;
/**HELP
   <b>GangstaPlus*</b>
   <br><b>Authors:</b> Aysam Guerler,  Bjoern Kolbeck,  Connie Wang, Patrick May, Thomas Steinke, Tobais Schmidt-Goenner, Group Leader:  Ernst-Walter Knapp
   <br><b>Publication:</b> PUBMED:18583523  PUBMED:17118190
   @author Christoph Gille
*/
public class Superimpose_GangstaPlus extends AbstractAligner implements HasScore, Superimpose3D {
    
    @Override public Result computeResult(byte[][] ssNotNeeded, Protein[] proteins) {
        if (proteins[0]==null || proteins[1]==null) return null;
        final String
            n="gangstaPlus2",
            instScript="c++ -O gplus.cpp -o "+n+".exe\n";
        final BasicExecutable aex=initX(n,null,instScript);
        final long writeOpts=ProteinWriter.PDB|ProteinWriter.ATOM_LINES|ProteinWriter.SIDE_CHAIN_ATOMS;
        final java.io.File fR=proteins[0].writePdbFile(writeOpts), fM=proteins[1].writePdbFile(writeOpts);
        final ChExec ex=aex.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL).setCommandLineV( aex.fileExecutable().toString(), fM, fR);
        ex.run();
        final BA txt=ex.getStdoutAsBA();
        return parse(2, txt,  null, PARSE_MATRIX|PARSE_SCORE);
    }

   /*
     aysam.guerler@googlemail.com
   */
}
