package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;

/**HELP
   Server site:   <i>HOME_PAGE:TransmembraneHelix_DAS</i>
   <br>
   Author: Miklos Cserzo
   @author Christoph Gille
*/
public class TransmembraneHelix_DAS extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,HasSharedControlPanel,NeedsInternet {
    private Object _ctrl, _comboLibSize, _comboEval;
    public Object getSharedControlPanel() {
        if (_ctrl==null) {
            _ctrl=pnl(VB,
                      pnl("Evaluation: [u]nconditional, [t]rusted:",_comboEval=new ChCombo("-t","-u")),
                      pnl("TM-library size:", _comboLibSize=new ChCombo("08","16","24","32"))
                      );
        }
        return _ctrl;
    }
    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final TransmembraneHelix_DAS si=(TransmembraneHelix_DAS)orO(getSharedInstance(),this);
        final int n=sequ.length();
        final char pred[]=new char[n];
        final BA fromServer=getResultText(sequ,"http://mendel.imp.ac.at/sat/DAS//cgi-bin/das.cgi",new Object[][]{
            {"LEN","-s"},
            {"CON", orO( toStrg(si._comboEval),   "-t")},
            {"LIB", orO( toStrg(si._comboLibSize),"08")},
            {"SEQ",sequ},
            {"submit","submit"}
            },false);
        if (fromServer==null) return null;
        //int idx=-1;
        for(String line:splitLnes(fromServer)){
            if (chrAt(0,line)=='@') {
                final StringTokenizer st=new StringTokenizer(line);
                int nT=st.countTokens();
                while(nT>0 && !st.nextToken().equals("core:")) nT--;
                if (nT<3) continue;
                final int from=atoi(st.nextToken())-1;
                st.nextToken();
                final int to=atoi(st.nextToken())-1;
                if (from>0 && to<=n)
                    for(int i=from-1; i<to && i<pred.length;i++)
                        pred[i]='H';
            }
        }
        return pred;
    }
}
