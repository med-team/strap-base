package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import charite.christo.protein.*;
import charite.christo.*;
/**HELP
<b>Author:</b> A.S. Schwartz, E.W. Myers and L. Pachter
<br>
WIKI:AMAP
*/
public final class MultipleAlignerAmap extends charite.christo.strap.AbstractAligner implements SequenceAlignerSorting, HasPrgParas {
    {
        defineParameters(FURTHER_PARAMETERS,"amap.rsc");
    }

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        if (!Insecure.EXEC_ALLOWED) return null;
        final String n="amap_2_2",
            D=" amap-align/align/",
            inst=
            "cd "+D+"\n"+
            "C_PLUS_PLUS_COMPILER -O3  -W -Wall -pedantic -DNDEBUG -DNumInsertStates=1 -DVERSION='\"AMAP.2.1\"' -funroll-loops -lm -o ../../"+n+".exe Amap.cc",
            control=n+"\n/usr/bin/amap-align"+
            PFX_DPKG+"amap-align"+
            BasicExecutable.PFX_FIX_INCLUDE+D+"Amap.cc"+D+"Defaults.h"+D+"EvolutionaryTree.h"+D+"FileBuffer.h"+D+"MultiSequence.h"+D+"Sequence.h"+
            BasicExecutable.PFX_ADD_INCLUDE_LIMITS+D+"Amap.cc";
        final BasicExecutable e=initX(control,null, inst);

        final ChExec ex=e.exec(ChExec.STDOUT|ChExec.STDERR)
            .setCommandLineV(e.fileExecutable(), getPrgParas() ,"-a", mfaInFile(seqs))
            .dir(dirTemp());
        ex.run();
        return parse(seqs.length, ex.getStdoutAsBA(), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);
    }
}
