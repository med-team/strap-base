package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.strap.*;
import charite.christo.protein.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
/**HELP
A profile of the sequence variability of the multiple sequence alignment is displayed.
The variability is calculated with the formula by Shannon.
Gaps are treated as a 21th amino acid.
*/
public class AlignmentEntropy implements ValueOfAlignPosition,StrapListener,HasSharedControlPanel,ActionListener, HasMC{
    public final static int SKIP_GAPS=0, GAP_IS_21TH_AMINO_ACID=1, GAP_IS_A_DIFFERENT_AMINO_ACID=2;
    private static ChCombo _combo;
    private static Object _ctrl;
    private Protein _pp[];
    private double[] _v;
    private int _wieOft[], _mc;

    public void actionPerformed(ActionEvent ev) {
        StrapEvent.dispatchLater(StrapEvent.VALUE_OF_ALIGN_POSITION_CHANGED,111);
    }

    public void setProteins(Protein... proteins) { _pp=proteins; _v=null;   }
    public Protein[] getProteins() {return _pp;}
    public void handleEvent(StrapEvent ev) {
        if (ev.getType()==StrapEvent.ALIGNMENT_CHANGED)
            StrapEvent.dispatchLater(StrapEvent.VALUE_OF_ALIGN_POSITION_CHANGED,111);
    }
    /**
       Avoid calculating by caching the value;
    */
    public int mc() { return _mc;}

    public double[] getValues() {
        final ChCombo c=_shared!=null?_shared._combo : _combo;
        return getValues(c==null ? 0 : c.i());
    }
    public double[] getValues(int mode) {
        final Protein pp[]=_pp;
        if (sze(pp)==0) return null;
        int mc=(_combo!=null ? _combo.mc() : 0);
        for(Protein p : pp) {
            if (p!=null)  mc+=p.mc(ProteinMC.MC_GAPPED_SEQUENCE);
        }

        if (_v==null || _mc!=mc) {
            _mc=mc;
            final int posMax=SPUtils.maxColumn(pp);
            final double
                entropyMax=Math.log(pp.length)*pp.length,
                entropy[]=new double[posMax+1];
            if (_wieOft==null) _wieOft=new int[255]; else java.util.Arrays.fill(_wieOft,0);
            double sumEntropy=0;
            for(int iPos=0;iPos<posMax;iPos++) {
                java.util.Arrays.fill(_wieOft,0);
                for(int iP=0;iP<pp.length;iP++) {
                    final Protein p=pp[iP];
                    final int iA=p.column2indexZ(iPos);
                    final byte c= iA<0 ? 32 : (byte)(p.getResidueType(iA)&~32);
                    ++_wieOft[c];
                }
                int insgesamt=0;
                for(int c='A'; c<='Z'; c++) insgesamt+=_wieOft[c];
                double entro=0;
                if (insgesamt>0) {
                    for(int c='A'; c<='Z'; c++)
                        if (_wieOft[c]>0 && (mode==GAP_IS_21TH_AMINO_ACID || c!=' '))  {
                            final double p=_wieOft[c]/(double)insgesamt;
                            entro-=p*Math.log(p);
                        }
                    /* A gap is like a new amino acid occurring once */
                    if (mode==GAP_IS_A_DIFFERENT_AMINO_ACID && _wieOft[' ']>0) {
                        final double p=1f/insgesamt;
                        entro-=_wieOft[' ']*p*Math.log(p);
                    }
                }
                if (mode==SKIP_GAPS && _wieOft[' ']>0) entropy[iPos]=Double.NaN;
                else {
                    entropy[iPos]=entro/entropyMax;
                    sumEntropy+=entro;
                }
            }

            _v=entropy;
        }
        return _v;
    }
    public Object getSharedControlPanel(){
        if (_ctrl==null) {
            _combo=new ChCombo("Skip alignment columns with gaps","A gap is the 21st amino acid","A gap is a different amino acid").li(this);
            _ctrl=pnl(HBL, "How should gaps be treated ?",_combo);
        }
        return _ctrl;
    }
    private AlignmentEntropy _shared;
    public void setSharedInstance(Object shared) { _shared=(AlignmentEntropy)shared;}
    public Object getSharedInstance() { return _shared; }

}
