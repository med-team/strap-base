package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.strap.*;
import charite.christo.*;
import java.io.File;
import static charite.christo.ChUtils.*;
/**HELP

SPEM aligns multiple sequences using pre-processed sequence profiles
and predicted secondary structures for pairwise alignment,
consistency-based scoring for refinement of the pairwise alignment and
a progressive algorithm for final multiple alignment.

<br>
<b>Installation:</b> http://sparks.informatics.iupui.edu/.
<br>
<b>Publication:</b> PUBMED:16020471
<br>
<b>Authors:</b>Hongyi Zhou  and Yaoqi Zhou
*/
public final class MultipleAligner_SPEM extends AbstractAligner implements  SequenceAligner {
    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        final String
            n="spem",
            instScript=null;
        final BasicExecutable e=initX(n, null, instScript);
        final File mfaInfile=mfaInFile(seqs), fileOut=file(delDotSfx(mfaInfile)+".aln");
        e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.LOG|ChExec.CYGWIN_DLL|ChExec.PLEASE_INSTALL_MANUALLY)
            .setCommandLineV("scan_spem_alone.job",mfaInfile)
            .dir(dirTemp())
            .run();

        return parse(seqs.length, readBytes(fileOut), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);

    }
}
