package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;

public abstract class AbstractSubcellularLocation implements PredictSubcellularLocation {
    private byte _sequence[];
    private Prediction[] _prediction;
    public void setResidueType(byte[] aa) { _sequence=aa;}
    public byte[] getResidueType() { return _sequence;}

    public Prediction[] getCompartments() {
        if (_prediction==null) {
            String cacheKey=null, cacheSection=null;
            BA data=null;
            if (CacheResult.isEnabled()) {
                final BA sb=new BA(333);
                sb.join(getPrgParas().asStringArray(), "_");
                cacheKey=sb.a(_sequence).toString();
                sb.clr();
                CacheResult.sectionForSequence(_sequence,sb);
                cacheSection=sb.toString();
                data=CacheResult.getValue(0, getClass(), cacheSection, cacheKey, (BA) null);
            }
            if (data==null) {
                data=computeOutput();
                if (data!=null && cacheKey!=null) CacheResult.putValue(0, getClass(),cacheSection,cacheKey,data);
            }
            if (data!=null) {
                _prediction=Prediction.text2prediction(data.bytes(),0,data.end());
            } else _prediction=ERROR;
        }
        return _prediction;
    }
    public abstract BA computeOutput();
    public void setOrganism(int organism){}

    /* ---------------------------------------- */
    /* >>> Command line options >>> */
    private String _parameters;
    private long _parameterOpt;
    private Object _shared;
    private PrgParas _prgPara;
    public final PrgParas getPrgParas() {
        final AbstractSubcellularLocation si= ((AbstractSubcellularLocation)orO(getSharedInstance(), this));
        if (si._prgPara==null) {
            si._prgPara=new PrgParas();
            if (si._parameters==null) si._prgPara.defineGUI(si._parameterOpt, si._parameters);
        }
        return si._prgPara;
    }
    public final void setSharedInstance(Object shared) { _shared=shared;}
    public final Object getSharedInstance() { return _shared; }
    public void defineParameters(long opt, String s) { _parameterOpt=opt; _parameters=s;}

    /* <<<  Command line options <<< */
    /* ---------------------------------------- */

}

/*

http://202.120.37.186/bioinf/hum-add/Hum-mPLoc-upload.asp

*/
