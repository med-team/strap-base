package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;


import static charite.christo.ChUtils.*;
/**HELP
   Web site:   <i>HOME_PAGE:TransmembraneHelix_HMMTOP</i>
   <br>
   Publication: PUBMED:11590105
   <br>
   Authors:  G.E. Tusnady
   <br>
   @author Christoph Gille
*/
public class TransmembraneHelix_HMMTOP extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,Disposable,HasControlPanel,NeedsInternet {
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final BA serverResult=getResultText(sequ,"http://www.enzim.hu/hmmtop/server/hmmtop.cgi",new Object[][]{
            {"if",sequ},
            {"submit","submit"}
            },false);
        if (serverResult==null) return null;
        final char preds[][]={new char[sequ.length()]};
        parse(splitLnes(serverResult),preds);
        return preds[0];
    }
    private void parse(String[] sResult,char[][] pred) {
        if (sResult==null) return;
        final int nP=pred.length;
        int iP=0;
        BA sb[]=new BA[nP];
        for(String l:sResult){
            if (l.startsWith("Protein: s")) iP=atoi(l,10);
            if (l.startsWith("     pred ")) {
                if (sb[iP]==null) sb[iP]=new BA(99);
                sb[iP].a(l, 9, 99999);
            }
        }
        for(int i=0;i<nP; i++) {
            final BA b=sb[i];
            if (b==null) continue;
            final int l=b.length();
            int iA=0;
            final char cc[]=pred[i];
            boolean isOuwards=false;
            for(int j=0;j<l && iA<cc.length;j++) {
                final int c=b.charAt(j)|32;
                if ('a'<=c&&c<='z') {
                    if (c=='o') isOuwards=false;
                    if (c=='i') isOuwards=true;
                    if (c=='h') cc[iA]= isOuwards ? 'H' : 'h';
                    iA++;
                }
            }
        }
    }
}
