package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.io.File;
/**HELP
ClustalW is a widely applied program to generate multiple sequence
alignments automatically.
<br>
<b>Publication:</b> PUBMED:18792934 <br>
<b>Home: </b> http://www.clustal.org/ <br>
<br><br><b>NT-sequence:</b>
ClustalW treats the letters as nucleotides if 85% or more of the characters in the sequence are A,C,G,T,U or N.
See UBIC:clustalw

<h2>As Protein sorter </h2>

*/
public final  class MultipleAlignerClustalW extends AbstractAligner implements HasPrgParas, SequenceAlignerTakesProfile, SequenceAlignerSorting, HasScore {
    public final static String TREE_TYPES[]={"nj","phylip","dist", "nexus"};

    public MultipleAlignerClustalW() {
        final String PARAMERS=
            "-GAPOPEN=\t Gap opening penalty\n"+
            "-GAPEXT=\t Gap extension penalty\n"+
            "-MATRIX=\t Protein weight matrix=BLOSUM, PAM, GONNET, ID or filename";
        defineParameters(FURTHER_PARAMETERS, PARAMERS);
    }
    /* ---------------------------------------- */
    /* >>> Evolutionary Tree, used by  >>> */

    private String _outputTree;   /** option -OUTPUTTREE=nj */

    public File computePhFile(byte[][] alignment, String[] names, boolean ext, String tree) {
        if (alignment==null) return null;
        _outputTree=tree;
        computeResult(alignment, null);
        final String fn=MFA_FILE_BASE+(tree==null?".dnd":".ph");
        final File fPh=file(dirTemp(),fn), fPh2=file(dirTemp(),"named_"+fn);
        if (names==null) return fPh;
        final BA sb=new BA(readBytes(fPh));
        for(int i=names.length; --i>=0;) {
            final String n=ext ?  names[i] : delDotSfx(names[i]);
            sb.replace(STRPLC_FILL_RIGHT,"s"+i+":",n+":");
        }
        rmAllChars((char)0, sb, 0,MAX_INT);
        wrte(fPh2, sb);
        return fPh2;
    }
    //public File getPhFile() { return file(MFA_FILE_BASE+".ph");}
    /* <<< Evolutionary <<< */
    /* ---------------------------------------- */
    /* >>> Compute >>> */

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        if (!Insecure.EXEC_ALLOWED) return null;
        final String n="clustalw_2.1",
            instScript=
            "sh configure\n"+
            "make\n"+
            "cp src/clustalw2"+RPLC_WIN_DOT_EXE+" "+n+".exe";
        final BasicExecutable e=getNativeExec(true);
        e.setBinaryPackageURLs(BasicExecutable.DEFAULT_BINARY);
        initX(n+"\n/usr/bin/clustalw"+PFX_DPKG+"clustalw", null, instScript);
        final File
            fileExe=e.fileExecutable(),
            fileOut=file(dirTemp(),"output.msf"), mfaInfile=mfaInFile(seqs);
        if (_outputTree!=null) wrte(mfaInfile, AlignUtils.toGappedFasta(seqs));
        final long opt=((getOptions()&OPTION_NOT_TO_SECURITY_LIST)!=0) ? ChExec.NOT_TO_DENY_LIST : 0;
        final ChExec ex=e.exec(ChExec.LOG|ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL| opt);
        final int offset=seqs.length;
        final byte[][][] profiles=getProfiles();
        if (profiles!=null && profiles.length>0 && profiles[0]!=null) {
            wrte(file(dirTemp(),"profile1"),AlignUtils.toGappedFasta(profiles[0],"p",offset,null));
            ex.setCommandLineV(fileExe, getPrgParas(), "-PROFILE1=profile1","-sequences","-PROFILE2="+MFA_FILE,"-OUTFILE=output.msf");
        }
        else if (_outputTree!=null) ex.setCommandLineV(fileExe,"-tree","-INFILE="+MFA_FILE,"-OUTPUTTREE="+_outputTree );
        else {
            ex.setCommandLineV(fileExe,getPrgParas(),"-INFILE="+MFA_FILE,"-OUTFILE=output.msf" );
        }

        ex.dir(dirTemp());
        if (e.isStopped()) return null;
        ex.run();
        if (_outputTree!=null) return null;
        final Superimpose3D.Result result=parse(seqs.length,ex.getStdoutAsBA(), null, Superimpose3D.PARSE_SCORE);

        parse(seqs.length, readBytes(fileOut), result, Superimpose3D.PARSE_MSA_START_S);

        return result;
    }

    @Override public int getMaxNumberOfProfiles() { return 1;}

}
/*
EBI (apc) (SUP#552193)
 */
