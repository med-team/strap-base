package charite.christo.strap.extensions;
import charite.christo.*;
import static charite.christo.ChUtils.*;
/**HELP

   http://www.genome.jp/SIT/plocdir/
   <b>Publication</b> PUBMED:12967962 <br>
   <b>Author:</b> Keun-Joon Park and  Minoru  Kanehisa (2003) <br>

*/
public class SubcellularLocationPLOC extends AbstractSubcellularLocation implements HasPrgParas {
    {
        defineParameters(FURTHER_PARAMETERS, "kingdom\t{animal,plant,yeast}");
    }

    @Override public BA computeOutput() {
       final java.util.Map m=getPrgParas().getMapVariables();
       return Web.getServerResponse(0L, "http://www.genome.jp//sit-bin/plocdir/ploc.cgi",new Object[][]{
                {"kingdom", m==null ? "animal" : m.get("kingdom")},
                {"seq",getResidueType()},
            });
    }
}
