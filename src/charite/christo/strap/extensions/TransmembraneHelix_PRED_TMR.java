package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP
   Home page: <i>HOME_PAGE:TransmembraneHelix_PRED_TMR</i>
   <br>
   <b>Author:</b> Claude Pasquier <br>
   @author Christoph Gille
*/
public class TransmembraneHelix_PRED_TMR extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,HasSharedControlPanel,NeedsInternet {
    private final static String URL0="http://o2.biol.uoa.gr/cgi-bin/PRED-TMR/PRED-TMR";
    private  Object _comboServer;
    /* --- Control panels --- */
    private static Object _ctrl;
    public Object getSharedControlPanel() {
        if (_ctrl==null){
            _ctrl=pnl(VBHB,"Server 1 or 2 ?",_comboServer=new ChCombo(URL0,"http://o2.biol.uoa.gr/cgi-bin/PRED-TMR2/PRED-TMR"));
        }
        return _ctrl;
    }
    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        final TransmembraneHelix_PRED_TMR si=(TransmembraneHelix_PRED_TMR)orO(getSharedInstance(),this);
        if (sequ==null) return null;
        final int n=sequ.length();
        final char result[]=new char[n];
        final String server=orS(toStrg(si._comboServer),URL0);
        final BA data=getResultText(sequ,server,new Object[][]{
                {"seq_data",sequ},
                {"show_obsTM",""},
            },false);
        if (data==null) return null;
        boolean started=false;
        for(String d:splitLnes(data)) {
            String line=rplcToStrg("<B>","",d);
            line=rplcToStrg("</B>","",line);
            if (line.startsWith("#TM")) {started=true;continue;}
            if (!started) continue;
            final String[] tt=splitTokns(line);
            if (tt.length<3) continue;
            final int from=atoi(tt[1]);
            final int to=atoi(tt[2]);
            if (0<from && to<=n)
                for(int i=from-1; i<to; i++) result[i]='H';
        }
        return result;
    }

}
