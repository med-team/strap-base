package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.strap.*;
import charite.christo.*;
import java.io.File;
import static charite.christo.ChUtils.*;

/**HELP
Align-m is an accurate and highly versatile multiple alignment program.
<br>
Author: Ivo Van Walle
<br>
http://bioinformatics.vub.ac.be/software/software.html
*/
public final class MultipleAlignerAlign_m extends AbstractAligner implements SequenceAligner, HasPrgParas {

    {
        defineParameters(FURTHER_PARAMETERS, "align_m.rsc");
    }

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        final String n="align_m";
        final BasicExecutable e=initX(n,null,null);
        final File dirTmp=dirTemp(), mfaInfile=mfaInFile(seqs), fExe=e.fileExecutable();
        final byte[][] sequences=getSequences();
        if (sequences.length<3) { error("At least 3 sequences required for Align-m"); return null;}      
        e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.LOG|ChExec.PLEASE_INSTALL_MANUALLY)
            .setCommandLineV(fExe, getPrgParas(), "-i", mfaInfile, "-o", "output.fa")
            .dir(dirTmp)
            .run();
        return parse(seqs.length, readBytes(file(dirTmp,"output.fa")), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);
    }
}
