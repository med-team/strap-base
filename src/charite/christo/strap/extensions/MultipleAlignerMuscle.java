package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import charite.christo.protein.*;
import charite.christo.*;
import charite.christo.strap.*;
import java.io.File;

/**HELP

HOME: <i>HOME:MultipleAlignerMuscle</i>
<br>
Windows (cygwin):
Building the program fails
<br>
Publication: PUBMED:15318951
<br>
<b>MS_Windows:</b> To build the package successfully please change the file intmath.cpp:
Move the definition of the method log2(double) to the top and rename it e.g. to Log2 (upper case).
*/
// intmath.cpp: log2(double) => Log2(double)

public final class MultipleAlignerMuscle extends AbstractAligner implements HasPrgParas, SequenceAlignerSorting {
    {
        final String PARAMERS=
            "-maxiters\t Maximum number of iterations (integer, default 16)\n"+
            "-maxhours\t Maximum time to iterate in hours (default no limit)\n"+
            "-maxmb\t Maximum memory to allocate in Mb (default 80% of RAM)";
        defineParameters(FURTHER_PARAMETERS, PARAMERS);
    }

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        final String
            n="muscle3.8.31",
            instScript="cd "+n+"/src\n make\n cp muscle"+RPLC_WIN_DOT_EXE+"  ../../"+n+".exe",
            control=n+"\n/usr/bin/muscle"+PFX_DPKG+"muscle";
        final BasicExecutable e=initX(control,null, instScript);
        final File fileOut=file(dirTemp(),"output.fa");
        e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL)
            .setCommandLineV(e.fileExecutable(), getPrgParas(), "-in", mfaInFile(seqs), "-out", fileOut)
            .dir(dirTemp())
            .run();
        return parse(seqs.length, readBytes(fileOut), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);    }
}
