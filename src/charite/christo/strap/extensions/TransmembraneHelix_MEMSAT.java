package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;

import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP
Web site: <i>HOME_PAGE:TransmembraneHelix_MEMSAT</i>

<br>Authors:  David T. Jones's group

   @author Christoph Gille
*/
public class TransmembraneHelix_MEMSAT extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {
    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final char result[]=new char[n];
        final BA data=getResultText(sequ,"http://saier-144-37.ucsd.edu/cgi-bin/memsat.cgi",new Object[][]{
            {"name",""},
            {"email",""},
            {"Sequence",">protein\n"+sequ},
            }, false);
        if (data==null)  return null;
        boolean started=false;
        for(String line:splitLnes(data)) {
            if (line.startsWith("FINAL PREDICTION")) {
                started=true;
                continue;
            }
            if (!started) continue;
            if ("".equals(line.trim())) break;
            if (line.indexOf(':')<0) continue;
            final String tt[]=splitTokns(line);
            for(String t : tt) {
                final int idx=t.indexOf('-');
                if (idx<0) continue;
                final int from=atoi(t);
                final int to=atoi(t,idx+1);
                if (0<from && to<=n)
                    for(int i=from-1; i<to; i++) result[i]='H';
            }
        }
        return result;
    }

}
