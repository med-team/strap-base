package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP

Server: <i>HOME_PAGE:TransmembraneHelix_TMHMM2</i>
<br>
 Maintained by  Anders Krogh  and  Kristoffer Rapacki
   @author Christoph Gille
*/
// rapacki@cbs.dtu.dk,   krogh@binf.ku.dk
public class TransmembraneHelix_TMHMM2 extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {

    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final BA data=getResultText(sequ, "http://www.cbs.dtu.dk/cgi-bin/nph-webface",new Object[][]{
                {"configfile","/usr/opt/www/pub/CBS/services/TMHMM-2.0/TMHMM2.cf"},
                {"SEQ",sequ},
                {"outform","-short"}
            }, true);
        if (data==null) return null;
        final char pred[]=new char[n];
        for(String line:splitLnes(data)){
            final int posToplogoy=line.indexOf("Topology=");
            if (posToplogoy<0 || !line.startsWith("Sequence")) continue;
            final String expr=line.substring(posToplogoy+"Topology=".length()).replaceAll("o"," o").replaceAll("i"," i");
            for(String t : splitTokns(expr)) {
                final String tt[]=splitTokns(t, chrClas("oi-"));
                if (tt.length<2) continue;
                final int from=atoi(tt[0]), to=atoi(tt[1]);
                if (from>0 && to<=n)
                    for(int i=from-1; i<to; i++)
                        pred[i]='H';
            }
        }
        return pred;
    }
}
//http://www.cbs.dtu.dk/cgi-bin/nph-webface?jobid=TMHMM2,449524A50296E34D&opt=wait
