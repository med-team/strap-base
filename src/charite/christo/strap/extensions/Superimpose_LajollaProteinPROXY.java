package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.strap.*;

/**HELP
   Home: http://lajolla.sf.net
   <br>

   <br>
   Written in Java by Raphael Bauer
*/
public class Superimpose_LajollaProteinPROXY extends AbstractAlignerProxy implements Superimpose3D {
    final static int OPTION_PROTEIN=1;
    { setOptions(OPTION_PROTEIN); }

    @Override public String getRequiredJars(){
        return "http://master.dl.sourceforge.net/project/lajolla/lajolla/lajolla-2.1/lajolla-2.1.jar "+jarFile(BIOJAVA_3D);
    }

}
