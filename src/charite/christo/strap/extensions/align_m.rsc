-v	[x]Verbose comments during execution
-s2p_guided_aln	[x]Substitution matrix name. For protein sequences
-s2p_m	Width of ungapped segment to average over. Default: BLOSUM62
-s2p_w	nsigma max. Maximum number of sigma that a new set of columns may score on average below the first one. default: 15
-s2p_minscore	minscore. Minimum score that a new set of columns may score on average (supersedes -s2p nsigma max) .default: 0.0
-s2p_go	Gap opening penalty for guided alignments Gap extension penalty for guided. default: 12.0



