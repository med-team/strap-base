package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.*;
/**HELP
JAligner: Java-implementation of the Smith Waterman algorithm for  <b>local</b> sequence alignments.
<br>
<b>Author:</b> Ahmed Moustafa.
<br>
<b>Home:</b> http://jaligner.sourceforge.net/
*/
public class PairAlignerJAlignerPROXY extends charite.christo.strap.AbstractAlignerProxy  implements SequenceAligner,HasSharedControlPanel,HasScore {
    @Override public String getRequiredJars() {  return "jaligner.jar"; }
    public final long getPropertyFlags() { return PROPERTY_ONLY_TWO_SEQUENCES|PROPERTY_LOCAL_ALIGNMENT;}
}
