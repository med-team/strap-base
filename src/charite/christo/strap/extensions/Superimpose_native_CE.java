package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

<br><b>Publication:</b> PUBMED:11125099 , PUBMED:9796821
@author Christoph Gille
*/

public final class Superimpose_native_CE extends AbstractAligner implements HasScore, HasControlPanel, SequenceAligner3D, Superimpose3D {
    private final Object _refEx[]=new Object[3];

    public Superimpose_native_CE() {
        setPropertyFlags(PROPERTY_ONLY_TWO_SEQUENCES);
    }

    @Override public Result computeResult(byte[][] ssNotNeeded, Protein[] proteins) {
        final Protein pR=proteins[0], pM=proteins[1];
        if (pR==null || pM==null) return null;
        final String
            instScript=
            "C_PLUS_PLUS_COMPILER  -DREAD_WRITE -DSGI pom/ipdb_df.C pom/ipdb.C pom/pom.C pom/miscutil.C pom/bnastats.C pom/derive.C pom/calc.C pom/jdate.C pom/pdbutil.C pom/linkedid.C -o mkDB.exe\n"+
            "C_PLUS_PLUS_COMPILER  pom/pom.C pom/miscutil.C ce/cmp_util.C ce/ce_align.C ce/ce.C -o ce.exe",
            dirScratch="scratch_"+System.currentTimeMillis();
        final BasicExecutable aex=initX("ce",null, instScript);
        final long
            fileOpt=ProteinWriter.SEQRES_IF_COORDINATES,
            execOpt=ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL;
        final java.io.File
            dir=dirTemp(),
            // fR=pR.writePdbFile(fileOpt), /* geht nicht */
             // fM=pM.writePdbFile(fileOpt),
            fR=pdbFile(pR, 0, (boolean[])null, fileOpt),
            fM=pdbFile(pM, 1, (boolean[])null, fileOpt),
            scratchDir=file(dir,dirScratch),
            bin_mkDB=file(aex.dirBinaries(),"mkDB.exe"),
            fExec=aex.fileExecutable();
        if (sze(fExec)==0) return null;
        mkdrs(scratchDir);
        delFileOnExit(scratchDir);
        final ChExec
            exPom1=new ChExec(execOpt).setCommandLineV(bin_mkDB,"scratch",dirScratch,fR).dir(dir),
            exPom2=new ChExec(execOpt).setCommandLineV(bin_mkDB,"scratch",dirScratch,fM,"USR2","add").dir(dir),
            ex=aex.exec(execOpt).setCommandLineV(fExec, "-", fR.getName(), "A", fM.getName(), "A", dirScratch).dir(dir);
        _refEx[0]=newSoftRef(exPom1);
        _refEx[1]=newSoftRef(exPom2);
        _refEx[2]=newSoftRef(ex);
        exPom1.run();
        exPom2.run();
        ex.run();
        final BA txt=ex.getStdoutAsBA();
        //     X2 = ( 0.018530)*X1 + (-0.955898)*Y1 + ( 0.293113)*Z1 + (  176.273087)
        //     Y2 = (-0.038094)*X1 + ( 0.292275)*Y1 + ( 0.955575)*Z1 + (  -37.874550)
        //     Z2 = (-0.999102)*X1 + (-0.028872)*Y1 + (-0.030998)*Z1 + (   59.276119)
        //Chain 1:    4 VPPSTALRELIEELVNITQNQKAPL---------CNGSMVWSINLTA--GMYCAALESLINVSGC-----
        //Chain 2:    2 SSTKKTQLQLEHLLLDLQMILNGINNYKNPKLTRMLTFKFYMPKKATELKHLQCLEEELKPLEEVLNLAQ
        final byte[] T=txt!=null ? txt.bytes() : null;
        if (T==null) return null;
        final int E=txt.end(), ends[]=txt.eol();
        final int iX2=strstr("X2",T,0,E);
        final String FAILED=RED_ERROR+" parsing CE output: ";
        if (iX2<0 || strstr(" Z2 = ",T,0, E)<0) { putln(FAILED); return null;}
        final int parOpen[]=new int[12];
        for(int i=iX2,count=0; i< E; i++) if (T[i]=='(') parOpen[count++]=i+1;
        if (T[11]==0)  { putln(FAILED,"Missing parenthesis"); return null;}
        final double[] t=new double[3], r[]=new double[3][3];
        for(int row=0, count=0; row<3; row++) {
            for(int col=0;col<3; col++) r[row][col]=atof(T,parOpen[count++],E);
            t[row]=atof(T,parOpen[count++],E);
        }
        final Result result=new Result(2);
        result.setMatrix(new Matrix3D().setTranslation(t).setRotation(r));
        parse(2, txt, result, PARSE_SCORE);
        final BA ba=new BA(999);
        //putln(txt);
        final int[] alnBegin={-1,-1};
        byte[] ggR=null, ggM=null;
        for(int iChain=2; --iChain>=0;) {
            ba.clr();
            boolean foundSeq=false;
            for(int iL=1; iL<ends.length; iL++) {
                final int b=ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                if (e-b>9 && strEquls("Chain ",T,b) && T[b+6]==iChain+'1' && T[b+7]==':' && T[e-1]!=')') {
                    foundSeq=true;
                    ba.a(T, b+14, e);
                    if (alnBegin[iChain]<0) alnBegin[iChain]=atoi(T,b+8,E);
                }
            }
            if (!foundSeq) putln(FAILED+"Missing parenthesis\n",txt);
            final byte gg[]=ba.subSequenceAsBytes(0,MAX_INT);
            if (iChain==0) ggR=gg; else ggM=gg;
        }
        if (!(alnBegin[0]>0 && alnBegin[1]>0)) assrt();
        //putln(" alnBegin="+alnBegin[0]+" "+alnBegin[1]);
        final byte[][] gapped=insertWhereNoCoordinates(alnBegin[0],ggR,alnBegin[1],ggM, pR, pM);
        result.setGappedSequences(gapped);
        return result;
    }
    private Object _ctrl;
    @Override public Object getControlPanel(boolean real) {
        final ChExec xx[]=derefArray(_refEx, ChExec.class);
        if (_ctrl==null && countNotNull(xx)>0) _ctrl=pnl(VB,ctrlPnl(xx[0]),ctrlPnl(xx[1]), ctrlPnl(xx[2]));
        return _ctrl;
    }

}
