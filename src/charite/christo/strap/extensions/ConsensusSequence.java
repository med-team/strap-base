package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.strap.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
/**HELP
   All identical residues of  aligned protein sequences are determined.
*/
public class ConsensusSequence implements StrapExtension {
    public void run() {
        Protein pp[]= (Protein[])runCR(_align,Protein.RUN_ALIGNMENT_GET_PROTEINS);
        if (pp==null) pp=StrapAlign.proteins();
        if (pp==null) return;
        final BA sb=new BA("# selected Proteins: ").a(pp.length).a('\n').join(pp);
        for(int pos=0;;pos++) {
            boolean consensus=true;
            byte consAA=0;
            boolean finished=true;
            for(Protein p:pp) {
                if (p.getMaxColumnZ()>=pos) finished=false;
                final int col=p.column2indexZ(pos);
                if (col==-1) { consensus=false; break; }
                final byte aa= p.getResidueType(col);
                if (consAA==0) consAA=aa;
                if (aa!=consAA) { consensus=false; break; }
            }
            sb.a(consensus ? (char) consAA : '.');
            if (finished) break;
        }
        shwTxtInW("Consensus sequence",sb);
    }
    private ChRunnable _align;
    public final void handleEvent(StrapEvent ev) {
        if (_align==null) {
            _align=ev.getProteinAlignment();
            StrapAlign.rmListener(this);
        }
    }
}
