package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
/**HELP
   <b>Web:</b> http://www.genebee.msu.ru/services/phtree_reduced.html
   <br>
   <b>Publication:</b> PUBMED:7578577
   @author Christoph Gille
*/
public class PhylogeneticTree_TreeTop implements PhylogeneticTree {

    private String _names[];
    private byte[][] _alignment;
    @Override public void setAlignment(String[] names, byte[][] aa, Object[] pp) {_names=names; _alignment=aa;  }

    public void drawTree() {
        final String[] nn=_names;
        final byte[][] aa=_alignment;
        if (nn==null||aa==null) return;
         int maxLen=0;
         for(String n:nn) {
             if (maxLen<n.length()) maxLen=n.length();
         }
         final BA sb=new BA(9999);
         final String msg=
             "The data will be sent to the Tree-Top Web service.<br>"+
             "Please wait for the result to appear in the Web Browser.<br><br>"+
             "Start computation?";
         if (ChMsg.yesNo(msg)) {
             sb.a("http://www.genebee.msu.ru/cgi-bin/nph-ptree.pl??"+
                  "HOM=Yes&"+
                  "SCALE=Random&"+
                  "ALG=Both Cluster and Topological&"+
                  "START=1&"+
                  "END=99999&"+
                  "GAP=1&"+
                  "WIDTH=800&"+
                  "HEIGHT=600&"+
                  "MMFACTOR_C=8&"+
                  "MMFACTOR_T=8&"+
                  "TREETYPE=PHYLIP&"+
                  "PICTURE=SLANT&"+
                  "BOOTSRAP=NO&"+
                  "ALIGN=");
             for(int i=0; i<nn.length && i<aa.length; i++) {
                 sb.a(nn[i]).a(' ',maxLen+1-nn[i].length()).aln(aa[i]);
             }
             visitURL(sb, 0);
         }
    }
}
