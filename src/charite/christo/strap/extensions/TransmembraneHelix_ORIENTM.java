package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
/**HELP
   Web site:
   <i>HOME_PAGE:TransmembraneHelix_ORIENTM</i>
   <br>
   Publication: PUBMED:11477216
   <br>
   Authors: Theodore D. Liakopoulos, Claude Pasquier and Stavros J. Hamodrakas
   <br>
   OrienTM uses  PRED-TMR
   @author Christoph Gille
*/
public class TransmembraneHelix_ORIENTM extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final char pred[]=new char[n];
        final BA data=getResultText(sequ,"http://athina.biol.uoa.gr/cgi-bin/orienTM/orienTM.pl" ,new Object[][]{ // "http://o2.biol.uoa.gr/cgi-bin/orienTM/orienTM.pl"
                                           {"seq",sequ},
                                           {"submit","PREDTMR2 + orienTM"}
            },false);
        boolean started=false;
        if (data==null) return null;
        for(String line:splitLnes(data)){
            if (line.startsWith("<TD>Transmembrane part</TD>")) {
                started=true;
                continue;
            }
            if (!started) continue;
            if ((line.startsWith("</TABLE"))) break;
            final int idx=line.indexOf('-');
            if (!line.startsWith("<TD>") || idx<0) continue;
            final int from=atoi(line,4);
            final int to=atoi(line,idx+1);
            if (0<from && to<=n)
                for(int i=from-1; i<to && i<pred.length;i++)
                    pred[i]='H';
        }
        return pred;
    }
}
