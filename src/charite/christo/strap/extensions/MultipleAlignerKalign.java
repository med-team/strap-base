package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.*;
import charite.christo.strap.*;
import java.io.File;
import static charite.christo.ChUtils.*;
/**HELP
Home page <i>HOME_PAGE:MultipleAlignerKalign</i>
Author: Timo Lassmann
Publication: PUBMED:16343337
*/
// <timolassmann@gmail.com> lukas.kall@ki.se
public final class MultipleAlignerKalign extends AbstractAligner implements HasPrgParas, SequenceAlignerSorting {

 {
     final String PARAMERS=
         "--gapopen=\t Gap open penalty\n"+
         "--gapextension=\t Gap extension penalty\n"+
         " --terminal_gap_extension_penalty=\t Terminal gap penalties\n"+
         "--matrix_bonus=\t A constant added to the substitution matrix.";
        defineParameters(FURTHER_PARAMETERS, PARAMERS);
    }

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        if (!Insecure.EXEC_ALLOWED) return null;
        final String n="kalign2",
            instScript=
            "sh configure\n"+
            "make\n"+
            "ln kalign"+RPLC_WIN_DOT_EXE+" "+n+".exe";
        final BasicExecutable e=initX(n+"\n/usr/bin/kalign"+PFX_DPKG+"kalign",null, instScript);
        final File dirTmp=dirTemp(), fileOut=file(dirTmp,"output.fa"), mfaInfile=mfaInFile(seqs);
        e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.LOG|ChExec.CYGWIN_DLL)
            .setCommandLineV(
                             e.fileExecutable(), getPrgParas(),
                             "-sort","tree","--quiet",
                             "--infile",mfaInfile, "-o", fileOut)
            .dir(e.dirTemp()).run();
        return parse(seqs.length, readBytes(fileOut), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);
    }
}
