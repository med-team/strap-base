package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;

import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP
Home: <i>HOME_PAGE:TransmembraneHelix_WaveTM</i>
<br>
Publication: PUBMED:15107018
<br>
Maintainer: Zoi Litou
   @author Christoph Gille
*/
public class TransmembraneHelix_WaveTM extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor, HasControlPanel, Disposable, NeedsInternet {
    @Override public String serverRoot() { return "http://athina.biol.uoa.gr/bioinformatics"; } // !!compansation for an error on the WaveTM web site.
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final char pred[]=new char[n];
        final BA data=getResultText(sequ, "http://athina.biol.uoa.gr/cgi-bin/WAVELET/wave.pl", new Object[][]{
            {"seq",">test\n"+sequ+"\n"},
            {"submit","Execute"}
            }, true);
        if (data==null) return null;
        for(String line:splitLnes(data)){
            if (line==null || line.indexOf("Prediction of transmembrane")<0) continue;
            line=line.replaceAll("^.*center>Begin","")
                .replaceAll("<[^>]+>"," ")
                .replaceAll("[^0-9 ]","");
            final String tt[]=splitTokns(line);
            for(int iT=tt.length/3;--iT>=0;) {
                final int from=atoi(tt[iT*3+1]);
                final int to=atoi(tt[iT*3]+2);
                if (0<from && to<=n)
                    for(int i=from-1; i<to && i<pred.length;i++)
                        pred[i]='H';
            }
        }
        return pred;
    }

}
