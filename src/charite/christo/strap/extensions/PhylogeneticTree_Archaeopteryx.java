package charite.christo.strap.extensions;
import java.io.File;
import java.net.URL;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

Archaeopteryx takes a multiple sequence alignment and displays a phylogenetic tree.
<br><b>Homepages:</b> http://www.phylosoft.org/archaeopteryx/
<br><b>Publication:</b> PUBMED:11301314
<br><b>License:</b> LGPL
<br><b>Author</b> Christian Zmasek
*/
// cmzmasek@yahoo.com
public class PhylogeneticTree_Archaeopteryx implements PhylogeneticTree, HasControlPanel, HasSharedControlPanel {
    private String _names[];
    private byte[][] _alignment;
    private MultipleAlignerClustalW _clustalW;
    private static Object _ctrl;
    private Object _shared, _cbExtension, _cbATV, _comboTree;

    public void setSharedInstance(Object shared) { _shared=shared;}
    public Object getSharedInstance() { return _shared; }

    @Override public void setAlignment(String[] names, byte[][] aa, Object[] proteins ){
        _names=names;
        _alignment=aa;
    }
    @Override public Object getControlPanel(boolean real) { return _clustalW;}

    @Override public Object getSharedControlPanel() {
        if (_ctrl==null) {
            _ctrl=pnl(VBPNL,
                         _cbExtension=cbox("show file extension"),
                         _cbATV=cbox("Old program version (ATV)"),
                         pnl("output tree ",_comboTree=new ChCombo(MultipleAlignerClustalW.TREE_TYPES))
                         );
        }
        return _ctrl;
    }

    public void drawTree() {
        final PhylogeneticTree_Archaeopteryx si=(PhylogeneticTree_Archaeopteryx)orO(getSharedInstance(),this);
        final boolean ext=isSelctd(si._cbExtension);
        final String treeType=orS(toStrg(si._comboTree), "nj");
        final File fPh2=(_clustalW=new MultipleAlignerClustalW()).computePhFile(_alignment, _names, ext, treeType);
        if (sze(fPh2)==0) {
            error("PhylogeneticTree_Archaeopteryx: Could not compute ph-file");
            shwCtrlPnl(CLOSE_CtrlW, "PhylogeneticTree_ATV",_clustalW);
            return;
        }
        final String fn= isSelctd(si._cbATV) ? "forester_400a4.jar" : "forester.jar";
        File fJar=installdFile(fn);
        final URL url=url(URL_STRAP_JARS+fn+".pack.gz");
        if (sze(fJar)==0 && ChMsg.askPermissionInstall(url, null) && InteractiveDownload.downloadFilesAndUnzip(new URL[]{url},dirJars())) {
            fJar=file(dirJars()+"/"+fn);
            ChZip.unpack200(dirJars(),ChZip.PACK_IF_NEWER);
        }
        if (sze(fJar)>0) startThrd(new ChExec(ChExec.WITHOUT_ASKING).setCommandLineV(file(systProprty(SYSP_BIN_JAVA)),"-jar", fJar, fPh2));
        else error("Cannot find "+fJar);
    }
}
