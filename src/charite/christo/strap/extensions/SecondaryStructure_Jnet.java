package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import charite.christo.*;
import charite.christo.protein.*;



/**

<b>Web:</b> http://www.compbio.dundee.ac.uk/~www-jpred/</i>
<br>
<b>Author:</b> James Cuff and Geoff Barton
<br>
<b>Publication:</b> PUBMED:10861942
<br>
<b>Download:</b> http://www.compbio.dundee.ac.uk/~www-jpred/jnet/download.html
<br>
The following modification was necessary for Windows:
<ul>
<li>jnet.h "#define MAXSEQNUM 600" changed to "#define MAXSEQNUM 60";</li>
</ul>

   @author Christoph Gille
*/
public class SecondaryStructure_Jnet extends BasicExecutable  implements SecondaryStructure_Predictor  {
    {
        setSourceInstallationScript("cc  -lm -O2 jnetsrc/src/*.c  -o jnet.exe");
        setName("jnet");
    }

    /* --- Computation --- */
    public String sequences[];
    public void setGappedSequences(String ss[]) {sequences=ss; }
    private char[][] prediction;
    public char[][] getPrediction(){ return prediction;}
    public void compute() {
        if (sequences==null) return;
         prediction=new char[sequences.length][];
         initExecutable();
         final long opts=ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL;
         for(int i=0;i<sequences.length;i++) {
             wrte(fileIn(),">t\n"+sequences[i].replaceAll("[^A-Za-z]",""));
             final ChExec ex= i==0 ? exec(opts) : new ChExec(opts);
             ex.setCommandLineV(fileExecutable(),"-z",fileIn()).run();
             final byte[] result=ex.getStdout();
             if (result!=null){
                 final int ends[]=new BA(result).eol();
                 prediction[i]=new char[ends.length];
                 int count=0;
                 for(int l=1; l<ends.length; l++) {
                     final int begin=ends[l-1]+1;
                     if (ends[l]-begin>3 && result[begin+4]=='|')
                         prediction[i][count++]=(char)result[begin+2];
                 }
             }
         }

    }
}

// Jpred Admin" <www-jpred@compbio.dundee.ac.uk
