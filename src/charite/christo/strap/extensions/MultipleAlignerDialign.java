package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import charite.christo.protein.*;
import charite.christo.*;
import charite.christo.strap.*;
import java.io.File;
/**HELP
Home-page <i>http://dialign.gobics.de/</i>
<br>
Author: Burkhard Morgenstern and Said Abdeddaim
<br>
Publication: PUBMED:10222408
<br>
*/
public final class MultipleAlignerDialign extends AbstractAligner implements HasPrgParas, SequenceAligner {

    {
        defineParameters(FURTHER_PARAMETERS, "dialign.rsc");
    }

    @Override public Superimpose3D.Result computeResult(byte[][] seqs, Protein[] proteinsNotNeeded) {
        if (!Insecure.EXEC_ALLOWED) return null;
        final String n=
            "dialign2-2",
            instScript=
            "cd dialign_package/src\n"+
            "make\n"+
            "cp dialign2-2"+RPLC_WIN_DOT_EXE+" ../../"+n+".exe";
        final BasicExecutable e=initX(n+"\n/usr/bin/dialign2-2"+PFX_DPKG+"dialign", null, instScript);
        final File dirTmp=dirTemp(),  mfaInfile=mfaInFile(seqs), fileExe=e.fileExecutable();
        final ChExec ex=e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL|ChExec.LOG)
            .setCommandLineV(fileExe, getPrgParas(), "-fa","-cw","-fn","output",mfaInfile)
            .dir(dirTmp);
        if (!strEquls("/usr",e.fileExecutable())) ex.addToEnvironement("DIALIGN2_DIR="+e.dirBinaries()+"/dialign_package/dialign2_dir/");
        ex.run();
        return parse(seqs.length, readBytes(file(dirTmp,"output.fa")), null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);
    }
}
