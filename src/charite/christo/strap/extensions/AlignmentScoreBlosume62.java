package charite.christo.strap.extensions;
import charite.christo.protein.*;
/**HELP
   This class computes the similarity of two aligned sequences using
   the Blosum62 similarity matrix and a gap creation and extension penalty
*/
public class AlignmentScoreBlosume62 extends AbstractSequenceAlignmentScore2 {

    @Override public double computeScore(byte[] s1, byte[] s2, int fromCol, int toCol) {
        return Blosum.pairAignScore(-10, -1, s1, s2,fromCol, toCol, -Blosum.BLOSUM62_MIN);
    }
    @Override public boolean isDistanceScore() { return false;}

}
