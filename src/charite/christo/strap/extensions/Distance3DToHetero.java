package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
A white space separated list of hetero-groups is needed.

Hetero-groups can be specified:
<ol>
<li>The name E.G. FAD, FMN, HEM. </li>
<li>The number as found in the pdb file. </li>
<li>In case of DNA/RNA  a capital letter which designates the chain letter </li>
</ol>

<br><b>Limitations:</b> Currently only c-alpha atoms.
<i>SEE_CLASS:Distance3DToCursor</i>
*/
public class Distance3DToHetero extends BasicResidueSelection implements StrapListener,IsEnabled,HasSharedControlPanel,java.awt.event.ActionListener, ChRunnable, HasRenderer {
    public final static Object KEY_DEFAULT_HETERO=new Object();
    private final static int MAX_VALUE=3200;
    /* Begin shared fields */
    private ChSlider _maxSlider;
    private Object _ctrl, _tf;
    private ChButton _togSideChain, _labMax;
    private int _maxValue=MAX_VALUE;
    /* End shared fields */
    private int _mc;
    private boolean _selected[], _triedSideChain;
    private String _toStrg="Distance3DToHetero ", _txt;
    public Distance3DToHetero() { super(OFFSET_IS_1ST_IDX); }
    public Object getRenderer(long options, long rendOptions[]) {return _toStrg;}
    public boolean isEnabled(Object o) { return o instanceof Protein  && ((Protein)o).getResidueCalpha()!=null;}

    public Object slider() {
        if (_maxSlider==null) {
            final ChSlider sl=_maxSlider=new ChSlider("",0,MAX_VALUE,_maxValue);
            addActLi(this, sl);
            sl.setMajorTickSpacing(100);
            sl.setMajorTickSpacing(200);
            sl.setPaintLabels(false);
            sl.setPaintTicks(true);
        }
        return _maxSlider;
    }
    public Object getSharedControlPanel() {
        if (_ctrl==null) {
            _labMax=labl();
            pcp(KEY_IF_EMPTY,"Enter list of hetero group names or hetero residue numbers here",_tf=new ChTextField("").saveInFile("Distance3DToHetero_hetero").li(this));
            label(MAX_VALUE/2);
            final Object explainTF=pnl(HBL,
                "<br>Either enter residue name like \"FMN\" or the residue number. <br>"+
                "Optionally append colon and chain-letter such as FMN:A or 250:A<br>"+
                                 "Specify DNA/RNA by a single capital letter denoting the chain or type \"DNA\" or \"RNA\".<br>"),
                noteAdvanced=pnl(HBL,
                                 "<br>Usually, the distance measurements are performed for hetero compounds and cAlpha positions of the same protein.<br>"+
                                 "But if the hetero compound is written in the form \"protein|hetero\", distance is measured to cAlpha positions of all proteins.<br>"+
                                 "For example peptide chain F of PDB:1gd2 has a RNA with chain letter \"A\". "+
                                 "The DNA chain is then written in the form \"1gd2_F.pdb|A\" "),
                pan=pnl(VBHB,
                        pnl(HBL,"Enter heterogroups separated by space. ", pnlTogglOpts("Explain",explainTF)),
                        explainTF,
                        _tf,
                        pnl(CNSEW,slider()),
                        pnl(HBL,"Highlight residues closer than ",monospc(_labMax),"to the selected heterogroup"),
                        " ",
                        (_togSideChain=toggl("Consider not only C-Alpha atoms but also side chain atoms").li(this)).cb(),
                        pnlTogglOpts("Explain measurements across proteins.",noteAdvanced),
                        noteAdvanced
                        );

            _ctrl=pan;
        }
        return _ctrl;
    }
    // ----------------------------------------
     private String label(int l) {
         final String s=toStrg(new BA(99).a(l/100f,5,3).a("\u00c5 "));
        if (_labMax!=null) _labMax.t(s);
        return s;
    }
    public final void handleEvent(StrapEvent ev) {
        if ((ev.getType()&StrapEvent.FLAG_ATOM_COORDINATES_CHANGED)!=0) {
            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS,null);
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        }
    }
    @Override public void setSelectedAminoacids(boolean bb[],int offset) {}

    public void setText(String txt) { _txt=txt; _mc++; }
    private static ChTokenizer _tok;
    @Override public boolean[] getSelectedAminoacids() {
        final Distance3DToHetero si=(Distance3DToHetero)orO(getSharedInstance(),this);
        if (si._txt==null && si._tf!=null) si._txt=toStrg(si._tf);
        final int maxValue=si._maxValue;
        final Object txt=orS(_txt, si._txt), togSS=si._togSideChain;
        final ChTokenizer TOK= _tok==null ? _tok=new ChTokenizer() : _tok;
        final Protein p=getProtein();
        final float[] xyzCA=p==null?null : p.getResidueCalpha();
        if (sze(txt)==0 || xyzCA==null) return null;
        final BA ba=toBA(txt);
        final byte T[]=ba.bytes();
        final float max=maxValue*0.01f;
        float[] xyzSS=null;

        if (isSelctd(togSS)) {
            xyzSS=p.getAtomCoordinates();
            if (xyzSS==null &&  !_triedSideChain) {
                _triedSideChain=true;
                p.loadSideChainAtoms(null);
                xyzSS=p.getAtomCoordinates();
            }
            if (xyzSS==null) StrapAlign.errorMsg(" No side chain for protein "+p);
        }
        final int mc=p.mc(ProteinMC.MC_ATOM_COORD)+si._mc;
        if (_selected==null || _mc!=mc) {
            _mc=mc;
            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS, p);
            final int nR=mini(xyzCA.length/3, p.countResidues());
            boolean[] selected=null;
            TOK.setText(T,0,ba.end());
            final BA sbTip=new BA(99).a(max).a("\u00c5 ");
            while(TOK.nextToken()) {
                final int from0=TOK.from(), to=TOK.to();
                final int pipe=strchr('|',T,from0,to);
                final int from=pipe>0?pipe+1:from0;
                final Protein pHet=pipe<0 ? p : SPUtils.proteinWithName(bytes2strg(T,from0,pipe),null);
                if (pHet==null) continue;
                final int colon=strchr(':', T, from,to);
                final int end=colon>0?colon:to;
                final int L=end-from;
                final int name32=
                    L==3 ? charsTo32bit( T[from], T[from+1], T[from+2], ' ') :
                    L==2 ? charsTo32bit( ' ',  T[from], T[from+1], ' ') :
                    L==1 ? charsTo32bit( ' ',  ' ', T[from], ' ') :
                    0;

                char chain=colon+2==to ? (char)T[colon+1] : colon+1==to ? ' ' : 0;
                int no=cntainsOnly(DIGT, T, from, end) ? atoi(T,from,to) : MIN_INT;
                if (to-from==1 && T[from]=='!') {
                    final String s=gcps(KEY_DEFAULT_HETERO,p);
                    if (s!=null) {
                        final int col=s.indexOf(':');
                        chain=col<0?0:chrAt(col+1, s);
                        no=atoi(s);
                    }
                }
                final Matrix3D m3d=pHet.getRotationAndTranslation();
                final float max2=max*max;
                for(HeteroCompound h : pHet.getHeteroCompounds('*')) {
                    if (!h.isNucleotideChain()) {
                        if(no!=MIN_INT && no!=h.getCompoundNumber()) continue;
                        if (no==MIN_INT && name32!=h.getCompoundName32()) continue;
                        if (chain!=0 && h.getCompoundChain()!=chain) continue;
                    } else {
                        if ((L!=1 || T[from]!=h.getCompoundChain()) && !strEquls(h.isDNA() ? "DNA" : "RNA",T,from)  ) continue;
                    }
                    sbTip.a(" h=").a(h).a(' ',2);
                nextAmino:
                    for(int iA=0;iA<nR;iA++) {
                        final boolean ss=xyzSS!=null;
                        if (selected==null) selected=new boolean[nR];
                        selected[iA]=selected[iA]||h.squareDistanceToNucleotidechain(ss?xyzSS:xyzCA, ss?pHet.getResidueFirstAtomIdx(iA):iA, ss?pHet.getResidueLastAtomIdx(iA):iA+1,  m3d, 0)<max2;
                    }
                }
            }
            _toStrg=toStrg(sbTip);
            _selected=selected;
        }
        return _selected;
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==_maxSlider) {
            _maxValue=_maxSlider.getValue();
            setName(" distance of C-alpha Atom to hetero group distance < "+label(_maxValue));
            TabItemTipIcon.set(null,null,"distance <"+_maxValue,null,this);
        }

        if (q==_maxSlider || q==_tf && (cmd==ACTION_FOCUS_LOST || cmd==ACTION_ENTER ) ||q==_togSideChain) {
            setText(toStrg(_tf));
            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS, null);
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        }
    }
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_GET_TIP_TEXT) {
            //final int modi=modifrs(arg);
            return _toStrg;
        }
        return null;
    }
}

