package charite.christo.strap.extensions;
import charite.christo.strap.*;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import static charite.christo.ChUtils.*;
/*
Author: Ivaylo Ilinkin and Ravi Janardan
Publication:  PUBMED:20122279

*/
public class Aligner3D_Mapsci extends AbstractAligner implements SequenceAligner3D, HasControlPanel {
    public Superimpose3D.Result computeResult(byte[][]seqs, Protein[] pp) {
        final String
            mapsci="mapsci-1.0",
            inst=
            "cd "+mapsci+"/build\n"+
            "make\n"+
            "cp mapsci"+RPLC_WIN_DOT_EXE+" ../../"+mapsci+".exe\n";
        final BasicExecutable aex=initX(mapsci, null, inst);
        final File
            dir=aex.dirTemp(),
            fAli=file(dir+"/alignment.pir"),
            fList=file(dir+"/input");
        final BA list=new BA(99);
        for(int i=0; i<pp.length; i++) {
            list.aln(pdbFile(pp[i], i, (boolean[])null,  0L).getName());
        }
        aex.setSourceInstallationScript(inst);
        aex.initExecutable();
        wrte(fList,list.toString());
        aex.exec(ChExec.SHOW_STREAMS|ChExec.CYGWIN_DLL)
            .dir(dir)
            .setCommandLineV(aex.fileExecutable(), fList, "center")
            .run();
        final BA output=readBytes(fAli);
        return parse(seqs.length, output, null, Superimpose3D.PARSE_MSA_START_S);
    }

}
