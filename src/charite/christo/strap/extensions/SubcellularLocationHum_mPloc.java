package charite.christo.strap.extensions;
import charite.christo.*;


/**HELP
   http://chou.med.harvard.edu/bioinf/hum-multi/
   <br>
   <b>Author:</b>Hong-Bin Shen and Kuo-Chen Chou<br>
   <b>Publication</b> PUBMED:17346678
*/
public class SubcellularLocationHum_mPloc extends AbstractSubcellularLocation {

    @Override public BA computeOutput() {
        return Web.getServerResponse(0L, "http://www.csbio.sjtu.edu.cn/cgi-bin/HummPLoc2.cgi",new Object[][]{
                {"S1=",">http_3d_alignment.eu\n"+new String(getResidueType())},
                {"B1=","Submit"},
                {"mode=","string"}
            });
    }
}

/*
  http://202.120.37.186/bioinf/hum-add/Hum-mPLoc-upload.asp
  http://chou.med.harvard.edu/cgi-bin/HummPLoc.cgi
*/
