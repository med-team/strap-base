package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
/**HELP
Web site: <i>HOME_PAGE:TransmembraneHelix_Phobius</i>
<br>
Publication:  PUBMED:15111065
<br>
Author: Lukas Kall

@author Christoph Gille
*/
public class TransmembraneHelix_Phobius extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final char result[]=new char[n];
        final BA data=getResultText(sequ, "http://phobius.sbc.su.se/cgi-bin/predict.pl",new Object[][]{
                {"protseq",">STRAPSEQUENCE\n"+sequ+"\n"},
                {"format","short"},
                {"submit","submit"}
            }, false);

        //putln("---------------------------");
        puts(">>>Phobius#compute  ");
        //putln(data);

        if (data==null)  return null;
        for(String line:splitLnes(data)){
            if (line.indexOf("STRAPSEQUENCE")>=0) {
                for(int inOut=0;inOut<2;inOut++) {
                    int pos=0;
                    while( (pos=line.indexOf(inOut==0 ? "o" : "i",pos+1))>0) {
                        final int iDash=line.indexOf("-",pos);
                        if (iDash<0) break;
                        final int from=atoi(line,pos+1);
                        final int to=atoi(line,iDash+1);
                        if (0<from && to<=n)
                            for(int i=from-1; i<to; i++)
                                result[i]= (inOut==0 ? 'H':'h');
                            //putln(" from "+from+" to "+to);
                        }
                    }
                }
                /* Signal */
                final int iFrom=line.indexOf(" n"),iDash=line.indexOf("-",iFrom+1), iTo=line.indexOf("c",iFrom+1);
                //putln(" i "+iFrom+" "+iTo+" "+iDash);
                if (iFrom>0 && iTo>iFrom && iDash>iFrom && iDash<iTo) {
                    final int from=atoi(line,iFrom+1);
                    final int to=atoi(line,iDash+1);
                    if (from>0 && to<=n)
                        for(int i=from-1; i<to; i++)
                            result[i]='s';
                }
            }
        //sleep(99);
        puts("Phobius#compute<<< ");
        return result;
    }
    /*
      Y n2-10c15/16o86-109i121-149o161-181i202-224o244-266i334-358o37
    */
}
