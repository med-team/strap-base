
-c	<val>    Use <val> certain cut-off. ( Default value: 1 )
-d	<val>    Use <val> as critical distance between 2 transmembrane segments. ( Default value: 2 )
-e	         For use with Eucaryotes.
-H	{CYTEXT-scale,GES-scale,GVH-scale,KD-scale} <file>   Use Hydrophobycitie values from <file>. 
-n	<val>    Use <val> as core window length.  ( Default value: 11 )
-p	<val>    Use <val> as putative cut-off. ( Default value: 0.6 )
-q	<val>    Use <val> as wedge window length. ( Default value: 5 )
-s	<val>    Use <val> as critical loop length.  ( Default value: 60 )

