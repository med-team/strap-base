package charite.christo.strap.extensions;
import charite.christo.strap.*;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import static charite.christo.ChUtils.*;
/**
Authors: Arun S. Konagurthu, James C. Whisstock, Peter J. Stuckey, Arthur M. Lesk
Publication:  PUBMED:16736488
Home: http://www.csse.monash.edu.au/~karun/Site/mustang.html
*/
public class Aligner3D_Mustang extends AbstractAligner implements SequenceAligner3D {
    public Superimpose3D.Result computeResult(byte[][]seqs, Protein[] pp) {
        final String
            n="mustang_v3_2_1",
            inst=
            "cd MUSTANG_v3.2.1\n"+
            "make\n"+
            "cp bin/mustang-3.2.1"+RPLC_WIN_DOT_EXE+" ../"+n+".exe";
        final BasicExecutable aex=initX(n+"\n/usr/bin/mustang"+PFX_DPKG+"mustang", null, inst);
        final File dir=aex.dirTemp();
        final Object ff[]=new Object[pp.length];
        for(int i=0; i<pp.length; i++) {
            ff[i]=nam(pdbFile(pp[i], i, (boolean[])null, ProteinWriter.CHAIN_SPACE));
        }
        aex.exec(ChExec.SHOW_STREAMS|ChExec.CYGWIN_DLL)
            .dir(dir)
            .setCommandLineV(aex.fileExecutable(), "-i", ff,"-o","output","-F","fasta")
            .run();
        final BA txt=readBytes(file(dirTemp(),"output.afasta"));
        return parse(seqs.length, txt, null, Superimpose3D.PARSE_MSA_START_S);
    }

}
