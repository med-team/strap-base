package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
/**HELP
   Server: <i>HOME_PAGE:TransmembraneHelix_TMAP_MULTI</i>
   <br>
   It takes a multiple sequence alignment as input and predicts the transmembrane helices from the alignment.
   <br>
   Authors:By  B Persson and P Argos
   <br>
   Publication: PUBMED:9246628
   @author Christoph Gille
*/
public class TransmembraneHelix_TMAP_MULTI implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {
    private Object _ctrl;
    private String _sequences[];
    public void setGappedSequences(String ss[]) {
        if (ss!=null) {
            _sequences=new String[ss.length];
            for(int i=ss.length; --i>=0; ) _sequences[i]=filtrS(0L,LETTR,ss[i]);
        }
    }
    private final ChTextView LOG=new ChTextView("");
    private char[][] _prediction;
    public char[][] getPrediction(){ return _prediction;}
    public Object getControlPanel(boolean real) {
        if (_ctrl==null) _ctrl=pnl(CNSEW, scrllpn(LOG),dHomePage(this));
        return _ctrl;
    }

    public void compute(){
        final int n=_sequences.length;
        if (n<2) { error("TransmembraneHelix_TMAP_MULTI requires  at least two aligned sequences");return;}
        _prediction=new char[n][0];
        if (n>0) _prediction[n-1]=TransmembraneHelix_TMAP.parse(getResults(_sequences));
    }
    public BA getResults(String sequences[]) {
        final String SERVER="http://bioinfo.limbo.ifm.liu.se/tmap/tmap1.pl";
        final BA sbAli=AlignUtils.toGappedMSF(strgs2bytes(sequences), "s",0,null);
        final BA data=Web.getServerResponse(0L,SERVER,new Object[][]{{"sequence",sbAli} });
        LOG.a("contacting ").a(SERVER).a(data).a("\n");
        if (data==null) LOG.a("error: could not load\n");
        return data;
    }
}
