package charite.christo.strap.extensions;
import charite.christo.protein.*;

/**HELP
   This class counts the amino acids in a protein.
*/
public class CountResidues implements ValueOfProtein {
    private Protein _p;
    public void setProtein(Protein p) { _p=p;}
    public void compute() {}
    public double getValue() {
        final Protein p=_p;
        return p==null ? Double.NaN : p.countResidues();
    }
}
