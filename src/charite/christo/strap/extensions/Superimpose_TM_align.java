package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   <br><b>Publication:</b> PUBMED:15849316
   <br><b>Home page:</b> http://zhang.bioinformatics.ku.edu/TM-align/
   <br><b>Authors:</b> Yang Zhang and Jeffrey Skolnick
   @author Christoph Gille

   http://www.apple.com/downloads/macosx/development_tools/intelfortrancompiler,professionaledition,formacos.html

   Versager: Superimposed s6stdout -load="PDB:1G51_B PDB:1J5W_A"
*/
public final class Superimpose_TM_align extends AbstractAligner implements HasScore, SequenceAligner3D, Superimpose3D {
    private final static javax.swing.AbstractButton cbCommutative=cbox("improve speed: reduce # of computations by commutativity"); static { cbCommutative.setSelected(true);}
    public Superimpose_TM_align() {
        setPropertyFlags(PROPERTY_ONLY_TWO_SEQUENCES);
    }
    @Override public Result computeResult(byte[][] ssNotNeeded, Protein[] proteins) {
        final Protein pR=proteins[0], pM=proteins[1];
        if (pR==null || pM==null) return null;
        final String n=ChExec.EXE_TM_ALIGN,
            inst="FORTRAN_COMPILER TMalign.f -o "+n+".exe \n"+
            "test -f "+n+".exe || f77  -O2 TMalign.f -o "+n+".exe \n"+
            "test -f "+n+".exe || f95  -O2 TMalign.f -o "+n+".exe \n";
        final BasicExecutable e=initX(n+"\n/usr/bin/tm-align"+PFX_DPKG+"tm-align", null, inst);
        final File
            dir=dirTemp(),
            fR=pR.writePdbFile(0L),
            fM=pM.writePdbFile(0L);
        if (e.exec(false)!=null || fM==null || fR==null) return null;
        final ChExec exec=e.exec(ChExec.STDOUT|ChExec.LOG|ChExec.DEBUG);
        exec.setCommandLineV(e.fileExecutable(),fM,fR).dir(dir);

        exec.run();
        final BA output=new BA(toByts(exec.getStdoutAsBA()));

        final byte[] T=output!=null ? output.bytes() : null;
        String search, error=null;
        if (T==null) { error="No program output ";  return null; }
        final int L=output.end();
        final Result result=new Result(2);
        final ChTokenizer TOKEN=new ChTokenizer();
        final double[] r[]=new double[3][3], t=new double[3];
        try {
            final int headerMatrix=strstr(STRSTR_AFTER, search="-------- Rotation matrix to rotate ",T,0,L);
            if (headerMatrix<0) { error="Missing "+search; return null; }
            int cr=strchr('\n', T, headerMatrix,L);
            if (!strEquls(" m ",T, cr+1)) {  error="Line after "+search+" should start with ' m '";  return null;  }
            cr=strchr('\n', T, cr+1,L);
            if (!strEquls(" 1 ",T, cr+1)) {  error="2nd Line after "+search+" should start with ' 1 '";  return null;  }
            TOKEN.setText(T,cr,L);
            for(int i=0;i<3;i++) {
                TOKEN.nextToken();
                TOKEN.nextToken();
                t[i]=TOKEN.asFloat();
                for(int j=0;j<3;j++) {
                    if (!TOKEN.nextToken()) { error="Failed to read superposition matrix"; return null; }
                    r[i][j]=TOKEN.asFloat();
                }
            }

            result.setMatrix(new Matrix3D().setRotation(r).setTranslation(t));

            AbstractAligner.parse(2, output,result, Superimpose3D.PARSE_SCORE);

            final int pos=strstr("denotes other aligned residues)",T,0,L)+1;
            if (pos<0) { error="Missing "+pos;}
            else {
                final int start0=1+strchr('\n', T,pos,L), end0=strchr('\n', T,start0,L);
                final int start1=1+strchr('\n', T,end0+1,L);
                final int end1=strchr('\n', T,start1+1, L);
                if (start0>0 && start0<end0 && start1>0 && start1<end1 && end0<start1) {
                    final byte[] ggM=new byte[end0-start0]; System.arraycopy(T,start0,ggM,0,ggM.length);
                    final byte[] ggR=new byte[end1-start1]; System.arraycopy(T,start1,ggR,0,ggM.length);
                    final byte[][] gapped=insertWhereNoCoordinates(0,ggR,0,ggM, pR, pM);
                    result.setGappedSequences(gapped);
                } else {
                    error=" Could not read alignment \n ggM="+start0+"-"+end0+"n ggR="+start1+"-"+end1;
                }
            }
            return result;
        } finally {
            if (error!=null) {
                putln(RED_ERROR+" TMalign: ");
                putln(error);
                putln(output);
            }
        }
    }
}
