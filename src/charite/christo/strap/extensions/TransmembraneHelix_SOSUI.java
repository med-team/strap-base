package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;


import static charite.christo.ChUtils.*;
/**HELP
   Web site: <i>HOME_PAGE:TransmembraneHelix_SOSUI</i>
   <br>
   Developed at Mitaku Group, Department of Biotechnology Tokyo University of Agriculture and Technology<br>
   @author Christoph Gille
*/
public class TransmembraneHelix_SOSUI extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final BA data=getResultText(sequ,"http://sosui.proteome.bio.tuat.ac.jp/cgi-bin/adv_sosui.cgi",new Object[][]{{"query_seq",sequ}}, false);
        if (data==null) return null;
        final char[] pred=new char[n];
        final int ends[]=data.eol();
        final byte txt[]=data.bytes();
        for(int iL=0;iL<ends.length;iL++) {
            final int b= iL==0 ? 0 : ends[iL-1]+1, e=ends[iL];
            if (e-b<5) continue;
            int from=0;
            for(int pos=b+4,col=0; pos<e; pos++) {
                if (txt[pos-3]=='e'&&txt[pos-2]=='r'&&txt[pos-1]=='>') {
                    if ('0'<=txt[pos] && txt[pos]<='9') {
                        final int num=atoi(txt,pos,e);
                        if (col==1) from=num;
                        else if (col==3) {
                            final int to=atoi(txt,pos,e);
                            if (0<from && to<=n)
                                for(int i=from-1; i<to; i++)
                                    if (0<=i && i<n) pred[i]='H';
                        }
                    }
                    col++;
                }
            }
        }
        return pred;
    }
}
