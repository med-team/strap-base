package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP
   Web site: <i>HOME_PAGE:TransmembraneHelix_TMPRED</i>
   <br>
   Author:  K Hofmann and  W Stoffel

   @author Christoph Gille
*/
public class TransmembraneHelix_TMPRED extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,HasSharedControlPanel,NeedsInternet {
    /* --- Control panels --- */
    private  ChTextField _tfMin, _tfMax;
    private static Object _sharedCtrl;
    public Object getSharedControlPanel() {
        if (_sharedCtrl==null) {
            _tfMin=new ChTextField("17").cols(6,true,true);
            _tfMax=new ChTextField("33").cols(6,true,true);
            _sharedCtrl=
                pnl(VB,
                    pnl(HB,"minimal helix lenght [14..26]",_tfMin.ct(Integer.class),"#"),
                    pnl(HB,"maximal helix lenght [23..41]",_tfMax.ct(Integer.class),"#")
                    );
        }
        return _sharedCtrl;
    }
    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        final TransmembraneHelix_TMPRED si=(TransmembraneHelix_TMPRED)orO(getSharedInstance(),this);
        if (sequ==null) return null;
        final int n=sequ.length();
        final char pred[]=new char[n];
        final BA data=getResultText(sequ, "http://www.ch.embnet.org/cgi-bin/TMPRED_form_parser",new Object[][]{
                {"outmode","ascii"},
                {"min", orO(si._tfMin, "17")},
                {"max", orO(si._tfMin, "33")},
                {"comm","title"},
                {"format","plain_text"},
                {"seq",sequ},
            }, false);
        if (data==null) return null;
        for(String line:splitLnes(data)){
            if (line.indexOf(" ( ")<0)continue;
            final String tt[]=splitTokns(line);
            if (tt.length<4) continue;
            final int from=atoi(tt[0]);
            final int to=atoi(tt[3]);
            if (from>0 && to<=n)
                for(int i=from-1; i<to; i++)
                    pred[i]='H';
        }
        return pred;
    }
}
