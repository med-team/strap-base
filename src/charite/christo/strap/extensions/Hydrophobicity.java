package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP
   This was translated from HydrophobicityProfiler.pl into java at
   http://gchelpdesk.ualberta.ca/servers/HydrophobicityProfiler/HydrophobicityProfiler.php
<br>
See MOL_TOOLKIT:hydropathy

<br>
<b>Related links:</b> Description of hydrophobicity scales: http://www.clcbio.com/index.php?id=43
*/

// http://www.biologie.uni-hamburg.de/b-online/library/genomeweb/GenomeWeb/prot-transmembrane.html

public class Hydrophobicity extends AbstractValueOfResidue implements HasSharedControlPanel, java.awt.event.ActionListener {
    public float[] _scale=KYTE_DOOLITTLE, _lastScale;
    private int _window=9,_lastWindow,_rtMC;
    private double _v[];
    private JComponent _ctrl;
    public double[] getValues() {
          final Hydrophobicity si=(Hydrophobicity)orO(getSharedInstance(), this);
        final int window=si._window, lWindow=si._lastWindow;
        final float scale[]=si._scale, lScale[]=si._lastScale;
        final Protein p=getProtein();
        if (p==null || scale==null) return null;
        final int nR=p.countResidues();
        final byte[] seq=p.getResidueType();
        final int modi=p.mc(ProteinMC.MC_RESIDUE_TYPE);
        if (_v==null || _rtMC!=modi || lScale!=scale || lWindow!=window) {
            _mc++;
            _rtMC=modi;
            si._lastWindow=window;
            si._lastScale=scale;
            final double dd[]=new double[nR];
            final int w2=(window+1)/2;
            for(int i=nR; --i>=0;) {
                float sum=0;
                for(int j=i-w2; j<=i+w2; j++) {
                    if (j>=0 && j<nR) {
                        final int ch=seq[j]|32;
                        if ('a'<=ch &&  ch<='z') sum+=scale[ch-'a'];
                    }
                }
                dd[i]=sum/(w2*2);
            }
            _v=dd;
        }
        return _v;
    }

    @Override public Object getSharedControlPanel() {
        if (_ctrl==null) {
            final ChSlider sl=new ChSlider("WINDOW",1,66,9);
            addActLi(this,sl);
            sl.setPaintLabels(true);
            sl.setPaintTicks(true);
            sl.setMajorTickSpacing(10);
            sl.setMinorTickSpacing(1);
            sl.setPreferredSize(dim(EM*60, prefH(sl)));
            sl.setSnapToTicks(true);
            _ctrl=pnl(VB,
                         "What Window size (default is 9) ?",
                         sl,
                         " ",
                         "What hydrophobicity scale ? ",
                         new ChCombo(SCALE_NAMES).li(this)
                         );

        }
        return _ctrl;
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        if (q instanceof JComboBox) {
            setScale(SCALE_NAMES[((JComboBox)q).getSelectedIndex()]);
        }
        if (q instanceof JSlider) {
            setWindowSize(((JSlider)q).getValue());
        }
        StrapEvent.dispatchLater(StrapEvent.VALUE_OF_RESIDUE_CHANGED,111);
    }

    public void setWindowSize(int wSize) { _window=wSize;}
    public void setScale(String sScale) {
        if (sScale=="KYTE_DOOLITTLE") _scale=KYTE_DOOLITTLE;
        else if (sScale=="HOPP_WOODS") _scale=HOPP_WOODS;
        else if (sScale=="JANIN") _scale=JANIN;
        else if (sScale=="ROSE") _scale=ROSE;
        else if (sScale=="EISENBERG") _scale=EISENBERG;
        else if (sScale=="CORNETTE") _scale=CORNETTE;
        else if (sScale=="ENGELMAN") _scale=ENGELMAN;

    }
    public final static String SCALE_NAMES[]={"KYTE_DOOLITTLE","HOPP_WOODS","JANIN","ROSE","EISENBERG","CORNETTE","ENGELMAN"};
    public final static float[]
        KYTE_DOOLITTLE={
            +1.8f,//A
            0,   // B
            +2.5f,//C
            -3.5f,//D
            -3.5f,//E
            +2.8f,//F
            -0.4f,//G
            -3.2f,//H
            +4.5f,//I
            0,    //J
            -3.9f,//K
            +3.8f,//L
            +1.9f,//M
            -3.5f,//N
            0,    //O
            -1.6f,//P
            -3.5f,//Q
            -4.5f,//R
            -0.8f,//S
            -0.7f,//T
            0,    //U
            +4.2f, //V
            -0.9f,//W
            0,    //X
            -1.3f,//Y
            0     //Z
        },
        HOPP_WOODS={
		          -0.5f,//A
            0,    //B
		          -1.0f,//C
            +3.0f,//D
            +3.0f,//E
		          -2.5f,//F
            +0.0f,//G
		          -0.5f,//H
		          -1.8f,//I
            0,    //J
            +3.0f,//K
		          -1.8f,//L
		          -1.3f,//M
            +0.2f,//N
            0,    //O
            +0.0f,//P
            +0.2f,//Q
            +3.0f,//R
            +0.3f,//S
		          -0.4f,//T
            0,    //U
		          -1.5f,//V
		          -3.4f,//W
            0,    //X
		          -2.3f,//Y
            0     //Z
        },
            JANIN={
                +0.3f,//A
                0,    //B
                +0.9f,//C
                -0.6f,//D
                -0.7f,//E
                +0.5f,//F
                +0.3f,//G
                -0.1f,//H
                +0.7f,//I
                0,    //J
                -1.8f,//K
                +0.5f,//L
                +0.4f,//M
                -0.5f,//N
                0,    //O
                -0.3f,//P
                -0.7f,//Q
                -1.4f,//R
                -0.1f,//S
                -0.2f,//T
                0,    //U
                +0.6f,//V
                +0.3f,//W
                0,    //X
                -0.4f,//Y
                0     //Z
            },
                ROSE={
                    0.74f,//A
                    0,    //B
                    0.91f,//C
                    0.62f,//D
                    0.62f,//E
                    0.88f,//F
                    0.72f,//G
                    0.78f,//H
                    0.88f,//I
                    0,    //J
                    0.52f,//K
                    0.85f,//L
                    0.85f,//M
                    0.63f,//N
                    0,    //O
                    0.64f,//P
                    0.62f,//Q
                    0.64f,//R
                    0.66f,//S
                    0.70f,//T
                    0,    //U
                    0.86f,//V
                    0.85f,//W
                    0,    //X
                    0.76f,//Y
                    0    // Z
                },
                    EISENBERG={
                        +0.62f,//A
                        0,     //B
                        +0.29f,//C
                        -0.90f,//D
                        -0.74f,//E
                        +1.19f,//F
                        +0.48f,//G
                        -0.40f,//H
                        +1.38f,//I
                        0,     //J
                        -1.50f,//K
                        +1.06f,//L
                        +0.64f,//M
                        -0.78f,//N
                        0,     //O
                        +0.12f,//P
                        -0.85f,//Q
                        -2.53f,//R
                        -0.18f,//S
                        -0.05f,//T
                        0,     //U
                        +1.08f,//V
                        +0.81f,//W
                        0,     //X
                        +0.26f,//Y
                        0      //Z
                    },
                        ENGELMAN={
                            +1.6f,//A
                            0,    //B
                            +2.0f,//C
                            -9.2f,//D
                            -8.2f,//E
                            +3.7f,//F
                            +1.0f,//G
                            -3.0f,//H
                            +3.1f,//I
                            0,    //J
                            -8.8f,//K
                            +2.8f,//L
                            +3.4f,//M
                            -4.8f,//N
                            0,    //O
                            -0.2f,//P
                            -4.1f,//Q
                            -12.3f,//R
                            +0.6f,//S
                            +1.2f,//T
                            0,    //U
                            +2.6f,//V
                            +1.9f,//W
                            0,    //X
                            -0.7f,//Y
                            0     //Z
                        },
                            CORNETTE={
                                +0.2f,//A
                                0,    //B
                                +4.1f,//C
                                -3.1f,//D
                                -1.8f,//E
                                +4.4f,//F
                                +0.0f,//G
                                +0.5f,//H
                                +4.8f,//I
                                0,    //J
                                -3.1f,//K
                                +5.7f,//L
                                +4.2f,//M
                                -0.5f,//N
                                0,    //O
                                -2.2f,//P
                                -2.8f,//Q
                                +1.4f,//R
                                -0.5f,//S
                                -1.9f,//T
                                0,    //U
                                +4.7f,//V
                                +1.0f,//W
                                0,    //X
                                +3.2f,//Y
                                0     //Z
                            };

}
