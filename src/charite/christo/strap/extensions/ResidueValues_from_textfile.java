package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

A plain text file containing floating point numbers separated by space or comma must be provided by the user.
Strap reads these numbers and assigns them to the residues of the protein.

The decimal point is a dot.  Undefined values
should be coded as NaN ("Not A Number").

*/
public final class ResidueValues_from_textfile implements ValueOfResidue, HasControlPanel, Disposable, java.awt.event.ActionListener, Runnable, HasMC {
    private final ChTextView tfFile=new ChTextView("");
    private final ChCombo comboDataset=new ChCombo("0","1","2","3","4","5","6","7","8","9","10","11","12").li(this);
    private boolean _running=true, _started;
    private File _file;
    private Protein _lastProtF;
    private int _lastDataset;
    private Protein _protein;
    private double[] _values;
    public ResidueValues_from_textfile() {
        setSettings(this, Customize.fileBrowsers, Customize.textEditors);
    }
    public void dispose() { _running=false; }
    /* <<< Instance  <<< */
    /* ---------------------------------------- */
    /* >>> Protein >>> */
    public void setProtein(Protein p) {
        //putln(ANSI_GREEN+"ResidueValues_from_textfile"+ANSI_RESET+" setProtein "+p);
        if (_protein!=p) {
            _values=null;
            _protein=p;
            updateTextFields();
            if (!_started) {
                _started=true;
                startThrd(this);
            }
        }
    }
    public Protein getProtein() { return _protein;}
    /* <<< Protein <<< */
    /* ---------------------------------------- */
    /* >>>  File >>> */
    private void updateTextFields() {
        final File f=f();
        tfFile.t(f==null?"":f+(f.exists() ? "  "+sze(f)+" Byte" : "  not existing"));
    }
    private File f() {
        final Protein p=getProtein();
        final int d=comboDataset.i();
        if (p==null) return null;
        if (_file==null || _lastDataset!=d || _lastProtF!=p) {
            _file=file("residueData/"+p+"."+d);
            _lastProtF=p;
            _lastDataset=d;
            mkParentDrs(_file);
        }
        return _file;
    }
    /* <<< File <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==comboDataset){
            _values=null;
            new StrapEvent(this,StrapEvent.VALUE_OF_RESIDUE_CHANGED).run();
            updateTextFields();
        }
        if (cmd=="EDIT" && f()!=null) edFile(-1, f(), modifrs(ev));
    }

    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> ControlPanel >>> */
    private Object _ctrl;
    public Object getControlPanel(boolean real) {
        if (_ctrl==null) {
            _ctrl=pnl(VBHB,
                      pnl(HBL,"dataset: ",comboDataset),
                      pnl(CNSEW,tfFile,null,null, new ChButton("EDIT").t("Edit data file").li(this), "data file: ")
                      );
        }
        return _ctrl;
    }
    /* <<< ControlPanel <<< */
    /* ---------------------------------------- */
    /* >>> getValues >>> */
    public int mc() { return _mc;}
    private int _mc;

    public double[] getValues() {
        final Protein p=getProtein();
        if (p==null) return null;
        final File fData=f();
        if (fData==null || !fData.exists()) {
            //error("file "+fData+" does not exist.\n<br>This file should contain a float value for each residue.\n");
            return null;
        }
        final long lastModi=fData.lastModified();
        if (_values==null || lastModifiedFile!=lastModi) {
            _mc++;
            lastModifiedFile=lastModi;
            _values=readValues(fData,p.countResidues());
        }
        return _values;
    }
    private double[] readValues(File fData,int nResidues) {
        double vv[]=new double[nResidues];
        for(int i=vv.length;--i>=0;) vv[i]=Double.NaN;
        InputStream is=null;
        try {
            is=new FileInputStream(fData);
            final StreamTokenizer tokenizer=new StreamTokenizer(is);
            for(int type,iAa=0; (type=tokenizer.nextToken())!=StreamTokenizer.TT_EOF && iAa<nResidues;) {
                if (type==StreamTokenizer.TT_NUMBER) {
                    vv[iAa++]=tokenizer.nval;
                }
            }
        } catch(IOException e){ return null;}
        closeStrm(is);
        return vv;
    }
    /* <<< getValues <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    private long lastModifiedFile;
    public void run() {
        while(_running) {
            ChUtils.sleep(111);
            final File f=f();
            if (f==null) continue;
            final long lastModi=f.lastModified();
            if (lastModifiedFile!=lastModi) {
                lastModifiedFile=lastModi;
                _values=null;
                new StrapEvent(this,StrapEvent.VALUE_OF_RESIDUE_CHANGED).run();
                updateTextFields();
            }
        }
    }
    /* <<< Threading <<< */
}
