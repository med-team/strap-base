package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.*;
/**HELP
   This class counts identical residues of two aligned sequences.
   Gaps are ignored.
*/
public class AlignmentScoreCountIdentical extends AbstractSequenceAlignmentScore2 {

    public double computeScore(byte[] s1, byte[] s2, int fromCol, int toCol) {
        final Ratio r=AlignUtils.getIdentity(s1,s2, fromCol, toCol);
        final int count= r==null ? 0 : r.getDenominator();
        return count==0 ? Double.NaN : r.getNumerator()/(double)count;
    }
    @Override public boolean isDistanceScore() { return false;}
}
