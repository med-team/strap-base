package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP
Web site: <i>HOME_PAGE:TransmembraneHelix_THUMBUP</i>,
<br>Maintainer:  Hongyi Zhou. Author  Hongyi Zhou and Yaoqi Zhou
<br>Publication: PUBMED:12824500
@author Christoph Gille
*/
public class TransmembraneHelix_THUMBUP extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,NeedsInternet {
    /* --- Computation --- */
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final int n=sequ.length();
        final char result[]=new char[n];
        final BA fromServer=getResultText(sequ,"http://sparks.informatics.iupui.edu/cgi-bin/caltmtop.cgi",new Object[][]{{"SEQ",sequ}},false);
        if (fromServer==null) return null;
        for(String s: splitLnes(fromServer)) {
            if (s.startsWith(" Helix ")) {
                final String tt[]=splitTokns(s);
                if (tt.length<5) continue;
                final int from=atoi(tt[3]);
                final int to=atoi(tt[4]);
                if (0<from && to<=n)
                    for(int i=from-1; i<to; i++)
                        result[i]='H';
            }
        }
        return result;
    }
}

//"http://www.smbs.buffalo.edu/phys_bio/cgi-bin/caltmtop.cgi"
