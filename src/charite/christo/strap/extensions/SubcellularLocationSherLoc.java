package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

<b>Publication:</b>: PUBMED:17392328 PUBMED:17094224
*/
public class SubcellularLocationSherLoc implements PredictSubcellularLocation, HasControlPanel, HasSharedControlPanel {
    private byte _seq[];
    private Prediction[] _res;
    private Object _e;
    private static Object _organism;
    public void setOrganism(int organism){}

    @Override public void setSharedInstance(Object shared) {}
    @Override public Object getSharedInstance() { return null; }
    @Override public Object getSharedControlPanel() {
        if (_organism==null) _organism=new ChCombo("animal", "plant", "fungal");
        return _organism;
    }

    private boolean parseOutput(BA ba) {
        if (ba==null) return false;
        final int ends[]=ba.eol(), E=ba.end();
        final byte bb[]=ba.bytes();
        if (ends.length<3) return false;
        final int from=ends[1], end=ends[2];
        final Prediction[] pp=Prediction.text2prediction(bb,from,end);
        for(Prediction p : pp) {
            int pos=p.getTextPosition();
            while(pos<end && (bb[pos]<'0' || bb[pos]>'9')) pos++;
            if (pos<end) p.setScore((int)(100*atof(bb,pos,E)));
        }
        _res=pp;
        return true;
    }

    @Override public Object getControlPanel(boolean real) { return _e!=null?_e:CP_EXISTS; }
    public Prediction[] getCompartments() {
        if (_res==null) {
            final BasicExecutable e=new BasicExecutable();
            _e=e;
            e.setName("sherloc");
            final File dirBin=e.dirBinaries(), exe=e.fileExecutable();
            final String
                sDir=toCygwinPath(dirBin+"/SherLoc"),
                svmDir=toCygwinPath(dirBin+"/libsvm-2.85/"),
                S="> s.sed\n",
                instScript=
                "test -d SherLoc/python_orig || mv SherLoc/python SherLoc/python_orig \n"+
                "mkdir SherLoc/python \n"+
                "echo 's|cmd = \"blastall|return result  #   cmd = \"blastall|1' "+S+
                "echo 's|/share/usr/blum/SherLoc|"+sDir+"|g' >"+S+
                "echo 's|/nfs/wsi/bs/share/opt/bin/python|/usr/bin/python|1'  >"+S+
                "echo 's|/share/usr/blum/libsvm-2.85|"+svmDir+"|g' >"+S+
                "for i in "+sDir+"/python_orig/*; do \n"+
                "  cat $i | sed -f s.sed > "+sDir+"/python/${i##*/} \n"+
                "done \n"+
                "cd libsvm-2.85; make; cd .. \n"+
                "cd SherLoc/cpp; make; cd ../.. \n"+
                "echo 'python "+dirBin+"/SherLoc/python/sherloc_prediction.py $@' > "+exe+"\n";
            e.setSourcePackageURLs(URL_STRAP_SRC+"libsvm-2.85.tar.gz"+" "+URL_STRAP_SRC+"sherloc.zip.gz");
            e.setSourceInstallationScript(instScript);
            e.initExecutable();

            final File dirTmp=e.dirTemp(), f=file(dirTmp,"s.fasta");
            final BA sb=new BA(_seq.length+99).a(">s\n").aln(_seq);
            wrte(f,sb);
            final ChExec ex=e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.LOG);
            if (ex!=null) {
                ex.setCommandLineV(ChExec.CYGWINSH,  exe, f, orO(_organism, "animal")).run();
                parseOutput(ex.getStdoutAsBA());
            } else _res=new Prediction[0];
        }
        return _res;
    }

    public void setResidueType(byte[] aa) { _seq=aa;}

}

/*
auf blast kann man auch verzichten. man verliert dadurch ein paar features.
das tool entspricht dann im wesentlichen unserer vorgangerversion MultiLoc.

phone  +49-7071-29--70462

room    torsten.blum@uni-tuebingen.de
http://www-bs2.informatik.uni-tuebingen.de/services/blum/SherLoc/
libsvm-2.85.tar.gz
sherloc.tar.gz

python python/sherloc_prediction.py test.fasta animal

*/
