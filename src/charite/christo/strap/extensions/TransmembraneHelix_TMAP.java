package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP
<b>Server</b> <i>HOME_PAGE:TransmembraneHelix_TMAP</i>
<br>
<b>Authors:</b>  B Persson and P Argos
<br>
Publication: PUBMED:9246628

   @author Christoph Gille
*/
public class TransmembraneHelix_TMAP extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,NeedsInternet, HasControlPanel {
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final BA ba=getResultText(sequ, "http://bioweb.pasteur.fr/cgi-bin/seqanal/tmap.pl",
                                       new Object[][]{
                                           {"sequences_data",sequ},
                                           {"Title","TMAP"},
                                           {"Cmd","tmap"},
                                           {"email",getPrpty(ChUtils.class,KEY_EMAIL,"")}
                                   }, true);
        return parse(ba);
    }

    static char[] parse(BA ba) {
        if (ba==null) return null;
        char pred[]=null;
        while(true) {
            int count=0;
            boolean started=false;
            for(String line : splitLnes(ba)) {
                if (!started) {
                    if (line.indexOf("Start ")>0) started=true;
                    else continue;
                }
                if (line.length()>0 && line.charAt(0)=='#') break;
                final int from=atoi(line);
                if (from<1) continue;
                final int to=atoi(line,8);
                if (pred!=null) {  for(int i=from-1; i<to; i++) pred[i]='H'; }
                else { if (count<to) count=to; }
            }
            if (pred==null) pred=new char[count]; else break;
        }
        return pred;
    }
    @Override public String findUrlInText(String txt) {
        if (txt==null) return null;
        final int i=txt.indexOf("tmap.out");
        if (i<0) return null;
        final int posFrom=txt.lastIndexOf("http://",i);
        if (i<0) return null;
        return txt.substring(posFrom,i+8);
    }
}
