package charite.christo.strap.extensions;
import static charite.christo.ChUtils.*;
import charite.christo.*;
import java.util.*;

/**
   @author Christoph Gille
*/
public class Toppred extends BasicExecutable implements HasPrgParas  {
    {
        setName("toppred");
        setSourceInstallationScript("sh configure \n make \n mv  src/toppred toppred.exe");
        defineParameters(FURTHER_PARAMETERS, "toppred.rsc");
    }

    private String sequence, seqName="test";
    public void setSequence(String name,String s) {seqName=name; sequence=filtrS(0L,LETTR,s); }
    private char[] prediction;
    private double[] hydrophobicity;
    public char[] getTM_Helices(){ return prediction;}
    public double[] getHydrophobicity(){  return hydrophobicity; }
    // java charite.christo.strap.extensions.TransmembraneHelix_TOPPRED
    /* --- Computation --- */

    private String fnPrediction,fnSequence;
    private static boolean isInitializing;
    public void compute() {
        if (isInitializing) return;
        isInitializing=true;
        initExecutable();
        isInitializing=false;
        if (sequence==null) return;
        final java.io.File
            dirTemp=dirTemp(),
            fSequence=file(dirTemp,fnSequence=seqName+".fa"),
            fGraph=file(dirTemp,seqName+".ps"),
            fPrediction=file(dirTemp,fnPrediction=seqName+".pred"),
            fHydro=file(dirTemp,seqName+".hydro"),
            dirData=file(dirBinaries(),"data");
        delFile(fSequence);
        delFile(fGraph);
        delFile(fPrediction);
        delFile(fHydro);
        wrte(fSequence,">"+seqName+"\n"+sequence+"\n");
        final ChExec ex=exec(ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL|ChExec.LOG)
            .setCommandLineV(fileExecutable(), getPrgParas(), "-g", "ps", "-o", fnPrediction, fnSequence)
            .dir(dirTemp)
            .addToEnvironement(("TOPPREDDATA="+dirData).replace('\\','/'));
        ex.run();
        /* --- Transmembrane --- */
         String[] ss=readLines(fPrediction);
        final int L=sze(sequence);

        if (ss!=null) {
            boolean started=false;
            final char pred[]=new char[L];
            Arrays.fill(pred,' ');
            for(String s:ss){
                if (started) {
                    final String[] tt=splitTokns(s,chrClas(" \t\n\r-"));
                    if (tt.length==0) break;
                    if (tt.length<3) continue;
                    final int from=atoi(tt[1])-1;
                    final int to=atoi(tt[2])-1;
                    if (from<0|| from>=L || to<0|| to>=L) {
                        putln("error in Toppred: from="+from+" to="+to);
                    }
                    else Arrays.fill(pred,from,to,'H');
                }
                started=started||s.startsWith(" Helix Begin - End   Score Certainity");
            }
            prediction=pred;
        }
        /* --- Hydrophobicity  --- */
        ss=readLines(fHydro);
        if (ss!=null) {
            final double[] ff=new double[L];
            int iAa=0;
            for(int i=0; i<ss.length && iAa<L; i++) {
                final String s=ss[i];
                final int iSpace=s.indexOf('\t');
                if (iSpace<0 || s.charAt(0)=='#') continue;
                ff[iAa++]=atof(s,iSpace,99999);
            }
            final int start=(L-iAa)/2;
            System.arraycopy(ff,0, ff, start,iAa);
            Arrays.fill(ff,0,start,Double.NaN);
            Arrays.fill(ff,start+iAa,L,Double.NaN);
            hydrophobicity=ff;
        }
    }
}
