package charite.christo.strap.extensions;
import charite.christo.protein.*;
import charite.christo.*;
/**HELP
Home: <i> http://neobio.sourceforge.net/ </i>
<br>
Depending on the settings it produces local or global sequence alignments.
<br>
Written in Java
*/
public class PairAlignerNeoBioPROXY extends charite.christo.strap.AbstractAlignerProxy implements SequenceAligner,HasScore {
    public final static String
        JARF="neobio_001.jar",
        PARA_ALGORITHM="Algorithm",
        PARA_MATCH="Match",
        PARA_MISMATCH="Mismatch",
        PARA_GAP="Gap",
        GLOBAL_NEEDLEMANWUNSCH="Global NeedlemanWunsch",
        LOCAL_SMITHWATERMAN="Local SmithWaterman",
        GLOBAL_CROCHEMORELANDAUZIVUKELSON="Global CrochemoreLandauZivUkelson",
        LOCAL_CROCHEMORELANDAUZIVUKELSON="Local CrochemoreLandauZivUkelsony";

    @Override public String getRequiredJars(){
        return JARF;
        //return PFX_INSTALLED+"neobio|neobio_001.jar";
    }
    public final long getPropertyFlags() { return PROPERTY_ONLY_TWO_SEQUENCES|PROPERTY_LOCAL_ALIGNMENT;}

}
