
-afc	[x]Creates additional output file <i>*.afc</i> containing data of all fragments considered for alignment. WARNING: this file can be HUGE !
-afc_v	[x]like <i>"-afc"</i> but verbose: fragments are explicitly printed. WARNING: this file can be EVEN BIGGER !
-ff	[x]Creates file <i>*.frg</i> containing information about all fragments that are part of the respective optimal pairwise alignments plus information about consistency in the multiple alignment
-fop	[x]Creates file <i>*.fop</i> containing coordinates of all fragments
-fsm	[x]Creates file <i>*.fsm</i> containing coordinates of all fragments that are part of the final alignment
-iw	[x]overlap weights switched off.<br> Higher quality, less speed
-lo	[x](Long Output) Additional file <i>*.log</i> with more information
-mat	[x]Creates file *mat with substitution counts.
-mat_thr	[x]Like <i>-mat</i> but only fragments with weight score
-max_link	[x]<i>maximum linkage</i> clustering used to construct sequence tree instead of UPGMA
-min_link	[x]minimum linkage
-mot	[x]motif option
-msf	[x]separate output file in MSF format
-o	[x]fast version, resulting alignments may be slightly different
-ow	[x]overlap weights enforced. <br>Less quality, high speed
-thr=	Threshold T = x

