package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP 

The solvent accessibility of a protein is currently loaded
only from dssp-files.

*/
public class ResidueSolventAccessibility implements ValueOfResidue, IsEnabled, HasMC {
    private Protein _p;
    private double[] _v;
    private int _mc;
    public int mc() { return _mc;}
    public void setProtein(Protein p) { if (_p!=p) _v=null; _p=p; }
    public Protein getProtein() { return _p;}

    public double[]  getValues() {
        final Protein p=_p;
        final float[] ac=p!=null ? p.getResidueSolventAccessibility() : null;
        if (ac==null) return null;
        final int n=p.countResidues();
        final int mc=p.mc(ProteinMC.MC_SOLVENT_ACCESSIBILITY);
        if (_v==null || _mc!=mc) {
            _mc=mc;
            double[] vv=redim(_v,n,99);
            for(int i=n; --i>=0; ) vv[i]= i<ac.length ? ac[i] : Double.NaN;
            _v=vv;
        }
        return _v;
    }
    public boolean isEnabled(Object p) {
        return p instanceof Protein &&  ((Protein)p).getResidueSolventAccessibility()!=null;
    }
}
