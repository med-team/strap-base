package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP
<i>INCLUDE_DOC:TransmembraneHelix_SPLIT3</i>
*/
public class TransmembraneHelix_SPLIT4 extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,Disposable,NeedsInternet {
    @Override public String findUrlInText(String sResponse) {
        return super.findUrlInText(rplcToStrg("/results/results","/out/out",sResponse));
    }
    private final JComponent panButResult=new ChPanel();
    @Override public char[] compute(String sequence) {
        //http://split.pmfst.hr/split/4/results/results820.html
        //http://split.pmfst.hr/split/4/out/out820.html#
    final BA data=getResultText(sequence, "http://split.pmfst.hr/cgi-bin/split/4/tamburr",new Object[][]{
            {"SEQUENCE",sequence},
            {"PROTEIN","test"},
            {"HOMEPAGE",URL_STRAP},
            {"EXTRASCALE","n"},
            }, true);
        if (data==null) return null;
        final String urlResult=getUrlOfResult();
        final String urlGraph=urlResult!=null ? rplcToStrg("/out/out","/results/results",urlResult) : null;
        if (urlResult!=null) ChDelay.toContainer(urlGraph, panButResult,null,33);
        boolean started=false;
        final int n=sequence.length();
        final char pred[]=new char[n];
        int i=0;
        for(String s:splitLnes(data)){
            if (s.startsWith(" AA#")) {
                if (started) break;
                started=true;
            }
            if (!started || s.length()<69) continue;
            final double f=atof(s,68,MAX_INT);
            if (i<n) pred[i++]=(f>0 ? 'H' : ' ');
        }
        return pred;
    }
}
