package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.*;
import java.io.File;

import static charite.christo.ChUtils.*;
/**HELP

<br>Publication: PUBMED:10964570

<br>Author: Cedric Notredame
<br>

*/
public final class MultipleAlignerT_Coffee extends AbstractAligner implements HasPrgParas, SequenceAlignerTakesProfile, SequenceAlignerSorting, HasScore, NeedsProteins {
    {
        final String PARAMERS=
            "-gapopen=\t D [0]\n"+
            "-gapext=\t D [0]\n"+
            "-fgapopen=\t D [0]\n"+
            "-fgapext=\t D [0]";
        defineParameters(FURTHER_PARAMETERS, PARAMERS);
    }
    @Override public Superimpose3D.Result computeResult(byte[][] seq, Protein[] proteins) {
        final String
            n="t_coffee_9.02.r1228",
            instScript=
            "cd T-COFFEE_distribution_Version_9.02.r1228\n"+
            "chmod +x install\n"+
            "./install  t_coffee \n"+
            "cp bin/binaries/*/t_coffee"+RPLC_WIN_DOT_EXE+" ../"+n+".exe\n";
        final BasicExecutable e=initX(n+"\n/usr/bin/t_coffee"+PFX_DPKG+"t-coffee", null, instScript);
        final File dirTmp=dirTemp();
         //final byte ss[][]=_ssWithoutFixed!=null ? _ssWithoutFixed : getSequences();
         final BA baTemplate=new BA(99);
         if ((OPTION_USE_SECONDARY_STRUCTURE&getOptions())!=0 && proteins!=null) { /* Prepare secondary structure */
             for(int iP=proteins.length; --iP>=0;) {
                 final byte ss[]=proteins[iP].getResidueSecStrType();
                 final int nR=proteins[iP].countResidues();
                 if (ss!=null) {
                     final BA ba=new BA(nR+10).a(">s").a(iP).a('\n');
                     for(int i=0;i<nR && i<ss.length; i++) {
                         final char c=(char)(ss[i]&~32);
                         ba.a(c=='H' || c=='E' ? c : 'C');
                     }
                     wrte(file(dirTmp+"/s"+iP+".ssp"), ba.a('\n'));
                     baTemplate.a(">s").a(iP).a(" _E_ s").a(iP).a(".ssp\n");
                 }
             }

         }
         File fTemplate=null;
         if (sze(baTemplate)>0) wrte(fTemplate=file(dirTmp+"/template_file"),baTemplate);
         final ChExec ex=e.exec(ChExec.STDOUT|ChExec.STDERR|ChExec.CYGWIN_DLL);
         if (isWin()) ex.addToEnvironement("HOME="+dirSettings());
         ex.setCommandLineV(
                            e.fileExecutable(),  mfaInFile(seq), mayAddProfile(), fTemplate!=null ? new Object[] {"-template_file" , "template_file",  "-method_evaluate_mode", "ssp", "-method", "lalign_id_pair", "slow_pair"} : null,
                            //  fileExecutable()/*, mayAddFixed()*/, mfaInfile().getName(), mayAddProfile(),
                            "-outfile=output.msf",  getPrgParas(),
                            "-email=no@mail", "-proxy=noProxy"
                            )
             .dir(dirTmp).run();
         final BA ba=readBytes(file(dirTmp,"output.msf"));
         return parse(seq.length, ba,null, Superimpose3D.PARSE_SCORE|Superimpose3D.PARSE_MSA_START_S);

    }
    //~/testTC $ t_coffee  Aa1b1hs_3d.clusta Ra1b1hs_3d.clustalW  sequencesWithout.fasta   -outfile=out
    /*
private byte[][] _fixed;
    private String mayAddFixed() {
        if (_fixed==null) return null;
        final BA sb=new BA("CLUSTAL\n\n");
        AlignUtils.toGappedMSF(_fixed,"s",_ssWithoutFixed.length, sb);
        wrte(file(getNativeExec(false).dirTemp(),"fixed.clustalW"),sb);
        return "Afixed.clustalW";
    }
    */
    /* --- Keep these aligned --- */
    private byte[][] _ssWithoutFixed;
    public void setSequencesWithoutFixed(byte ss[][]) {_ssWithoutFixed=ss;  }
    private Object mayAddProfile() {
        final byte[] profiles[][]=getProfiles();
        if (profiles==null) return null;
        BA sb=null;
        for(int iProfile=profiles.length; --iProfile>=0;) {
            final String fn="profile"+iProfile+".mfa";
            wrte(file(getNativeExec(false).dirTemp(),fn),AlignUtils.toGappedFasta(profiles[iProfile],"p",0, null));
            if (sb==null) sb=new BA(99).a("-profile="); else sb.a(',');
            sb.a(fn);
        }
        return sb;
    }
}
