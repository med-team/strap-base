package charite.christo.strap.extensions;
import java.io.File;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

<b>Homepage</b>http://www.jalview.org
<br>
The phylogenetic dendrogram is shown in WIKI:Jalview.

*/
// cmzmasek@yahoo.com
public class PhylogeneticTree_Jalview implements PhylogeneticTree, HasControlPanel, HasSharedControlPanel {
    private String _names[];
    private byte[][] _alignment;
    private Protein[] _pp;
    private MultipleAlignerClustalW _clustalW;
    private static Object _ctrl;
    private Object _shared, _comboTree;

    public void setSharedInstance(Object shared) { _shared=shared;}
    public Object getSharedInstance() { return _shared; }

    @Override public void setAlignment(String[] names, byte[][] aa, Object[] pp){
        _names=names;
        _alignment=aa;
        _pp=(Protein[])pp;
    }
    @Override public Object getControlPanel(boolean real) { return _clustalW;}

    @Override public Object getSharedControlPanel() {
        if (_ctrl==null) {
            _ctrl=pnl(VBPNL, pnl("output tree ",_comboTree=new ChCombo(MultipleAlignerClustalW.TREE_TYPES)) );
        }
        return _ctrl;
    }

   @Override public void drawTree() {

       final File fPh=(_clustalW=new MultipleAlignerClustalW()).computePhFile(_alignment, _names, false, toStrg(_comboTree));
       if (sze(fPh)==0) {
            error("Jalview: Could not compute ph-file");
            shwCtrlPnl(CLOSE_CtrlW, "PhylogeneticTree_ATV",_clustalW);
            return;
        }
        putln(YELLOW_DEBUG+" fPh='", fPh, "'");
        new ToJalview().launchJalview(0, _pp,  fPh);
    }
}
