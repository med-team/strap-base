package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP

<b>Authors</b>: Davor Juretic, Croatia.
<br>
<b>Server program written by</b> Damir Zucic
<br>
<b>Home page:</b> <i>HOME_PAGE:TransmembraneHelix_SPLIT3</i>
<br>
<b>Publications:</b> PUBMED:8485300 PUBMED:12086524
<br>
<b>Benchmarks:</b> http://cubic.bioc.columbia.edu/service/tmh_benchmark/

@author Christoph Gille
*/
public class TransmembraneHelix_SPLIT3 extends AbstractPredictionFromAminoacidSequence implements TransmembraneHelix_Predictor,HasControlPanel,HasSharedControlPanel,NeedsInternet {
    private Object _comboMOMSCALE,_comboAASCALE1;
    private static Object _cbExtraScale, _ctrl;
    public Object getSharedControlPanel() {
        if (_ctrl==null){
            _comboMOMSCALE=new ChCombo("","26 EISEN","17 PONG1","69 MATPO","27 PRIFT","79 MARTI","43 KUHLE","66 CHOU6","15 CIDA+","44 DEBER","52 EDE25","41 ZAMYA","03 PONNU","51 EDE31","32 SWEET","07 GUY-M","22 WOLFE","42 MIJER","06 JONES","11 LEVIT","31 GUYFE","16 CIDAB","39 MEIRO","85 OSMP1","53 EDE21","35 NNEIG","56 FASMB","21 ROSEM","71 GRANT","20 KIDER","12 GIBRA","09 VHEBL","45 WERSC","70 WOESE","02 FAUPL","28 HOPPW","54 EDE15","59 JURET","83 MODKD","84 MKD4","30 ROSEF","86 OSMP","80 MDK0","87 JACWH2","01 KYTDO","04 ENGEL","05 JANIN","08 KRIGK","10 EIMCL","13 CIDAA","14 CIDBB","18 PONG2","19 PONG3","23 CHOTA","24 ROSEB","25 ROSEA","29 CHOTH","33 KUNTZ","34 BULDG","36 COHEN","37 KIMBE","38 MINKI","40 JACWH","46 NEILD","47 SCHER","48 JURPE","49 HEIJN","50 EDEWH","55 FASMA","57 FASMT","58 RICRI","60 RICH1","61 CHOU1","62 CHOU2","63 CHOU3","64 CHOU4","65 CHOU5","67 MEEKR","68 KARPL","72 ZIMMP","73 MCMER","74 FAUCH","75 KUBOT","76 URRY1","77 URRY2","78 CASSI","81 MDK1","82 MKD2","88 CPREF");
            _comboAASCALE1=new ChCombo("","01 KYTDO","83 MODKD","52 EDE25","53 EDE21","51 EDE31","54 EDE15","04 ENGEL","26 EISEN","09 VHEBL","88 CPREF","86 OSMP","84 MKD4","82 MKD2","81 MDK1","80 MDK0","02 FAUPL","29 CHOTH","79 MARTI","49 HEIJN","44 DEBER","43 KUHLE","27 PRIFT","35 NNEIG","05 JANIN","03 PONNU","07 GUY-M","30 ROSEF","31 GUYFE","32 SWEET","33 KUNTZ","12 GIBRA","85 OSMP1","13 CIDAA","16 CIDAB","14 CIDBB","15 CIDA+","17 PONG1","19 PONG3","20 KIDER","21 ROSEM","40 JACWH","87 JACWH2","45 WERSC","48 JURPE","66 CHOU6","62 CHOU2","41 ZAMYA","42 MIJER","69 MATPO","70 WOESE","71 GRANT","72 ZIMMP","22 WOLFE","50 EDEWH","57 FASMT","59 JURET","78 CASSI","10 EIMCL","08 KRIGK","28 HOPPW","11 LEVIT","39 MEIRO","18 PONG2","76 URRY1","77 URRY2","68 KARPL","67 MEEKR","56 FASMB","34 BULDG","36 COHEN","06 JONES","47 SCHER","65 CHOU5","74 FAUCH","24 ROSEB","63 CHOU3","37 KIMBE","38 MINKI","23 CHOTA","25 ROSEA","46 NEILD","55 FASMA","58 RICRI","60 RICH1","61 CHOU1","64 CHOU4","75 KUBOT","73 MCMER");
            _cbExtraScale=cbox("Use extra scale");
            _ctrl=pnl(VB,
                      pnl("conformational preferences",_comboAASCALE1),
                      pnl("hydrophobic moments",_comboMOMSCALE),
                      pnl(_cbExtraScale)
                      );
        }
        return _ctrl;
    }
    @Override public String findUrlInText(String sResponse) {
        return super.findUrlInText(rplcToStrg("/results/results","/out/out",sResponse));
    }
    @Override public char[] compute(String sequence) {
        //http://split.pmfst.hr/split/4/results/results820.html
        //http://split.pmfst.hr/split/4/out/out820.html#
        final TransmembraneHelix_SPLIT3 si=(TransmembraneHelix_SPLIT3)orO(getSharedInstance(),this);
        final String
            aascale1=substrg(toStrg(si._comboAASCALE1),0,2),
            moments= substrg(toStrg(si._comboMOMSCALE),0,2);
        final BA data=getResultText(sequence, "http://split.pmfst.hr/cgi-bin/split/tambur_cgi.script",new Object[][] {
            {"SEQUENCE",sequence},
            {"PROTEIN","test"},
            {"HOMEPAGE",URL_STRAP},
            {"VERSION","split35"},
            {"AASCALE1",aascale1},
            {"MOMSCALE",moments},
            {"EXTRASCALE", isSelctd(si._cbExtraScale) ? "y" : "n"},
            }, true);
        if (data==null) return null;

        boolean started=false;

        final int n=sequence.length();
        final char pred[]=new char[n];
        int i=0;
        for(String s:splitLnes(data)){
            if (s.startsWith(" AA#")) {
                if (started) break;
                started=true;
            }
            if (!started || s.length()<68) continue;
            final double f=atof(s,69,MAX_INT);
            if (i<n) pred[i++]=(f>0 ? 'H' : ' ');
        }
        return pred;
    }

}
/*
echo $VARS | lynx  -source -post_data -mime_header $SERVER_TMHMM
*/
