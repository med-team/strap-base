package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**HELP
Publication: PUBMED:7704669
<br>
<i>SEE_CLASS:TransmembraneHelix_TOPPRED</i>
*/
public class ResidueHydrophobicity_TOPPRED extends AbstractValueOfResidue implements HasNativeExec {
    static {
        TabItemTipIcon.set("hydro","hydrophobicity profile with TOPPRED",
                           "The hydrophobicity is calculated locally with the program TOPPRED",
                           iicon(IC_UMBRELLA),ResidueHydrophobicity_TOPPRED.class);
    }

    public double[] getValues() {
        final Protein p=getProtein();
        if (p==null) return null;
        final int mc=p.mc(ProteinMC.MC_RESIDUE_TYPE);
        if (_v==null || _mc!=mc) {
            _mc=mc;
            toppred.setSequence(p.getName().replaceAll("[^0-9A-Za-z]","_"), p.getResidueTypeAsString());
            toppred.compute();
            _v=toppred.getHydrophobicity();
        }
        return _v;
    }

    private final Toppred toppred=new Toppred();
    public BasicExecutable getNativeExec(boolean createNew) { return toppred;}

}
