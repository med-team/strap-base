package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;



/**HELP
Web site: <i>HOME_PAGE:Toppred</i>",
<br>
Publication: PUBMED:7704669
<br>
Authors: von Heijne, Claros, Deveaud and Schuerer
<br>
<i>SEE_CLASS:ResidueHydrophobicity_TOPPRED</i>
   @author Christoph Gille
*/
public class TransmembraneHelix_TOPPRED implements TransmembraneHelix_Predictor, HasNativeExec, Disposable {
    public String sequences[];
    public void setGappedSequences(String ss[]) {sequences=ss; }
    private char[][] prediction;
    public char[][] getPrediction(){ return prediction;}

    public final Toppred toppred=new Toppred();
    public BasicExecutable getNativeExec(boolean createNew) { return toppred;}
    /* --- Computation --- */
    private boolean disposed;public void dispose() {disposed=true;}
    public void compute() {
        final int n=sequences.length;
        prediction=new char[n][];
        for(int i=0;i<n&&!disposed;i++) {            
            toppred.setSequence("s"+i,sequences[i]);
            toppred.compute();
            prediction[i]=toppred.getTM_Helices();
        }
    }
}
