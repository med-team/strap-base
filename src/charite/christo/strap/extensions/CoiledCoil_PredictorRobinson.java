package charite.christo.strap.extensions;
/**HELP
 Ported from Perl by Nick Robinson
*/

public class CoiledCoil_PredictorRobinson implements charite.christo.protein.CoiledCoil_Predictor {
    private String _sequences[];
    private char _prediction[][];
    private final static int WINDOW=21,M_CC=0,SD_CC=1,M_G=2,SD_G=3,SCALING=4;
    private final static double
        min_P=0.5,
        UNWEIGHTED[]={1.79,0.24,0.92,0.22,25.0},
        WEIGHTED[]={1.74,0.23,0.86,0.21,25.0},
        HEPTAD_WEIGHT[][] =new double[256][7];
    static {
        HEPTAD_WEIGHT['L']=new double[]{2.998, 0.269, 0.367, 3.852, 0.510, 0.514, 0.562};
        HEPTAD_WEIGHT['I']=new double[]{2.408, 0.261, 0.345, 0.931, 0.402, 0.440, 0.289};
        HEPTAD_WEIGHT['V']=new double[]{1.525, 0.479, 0.350, 0.887, 0.286, 0.350, 0.362};
        HEPTAD_WEIGHT['M']=new double[]{2.161, 0.605, 0.442, 1.441, 0.607, 0.457, 0.570};
        HEPTAD_WEIGHT['F']=new double[]{0.490, 0.075, 0.391, 0.639, 0.125, 0.081, 0.038};
        HEPTAD_WEIGHT['Y']=new double[]{1.319, 0.064, 0.081, 1.526, 0.204, 0.118, 0.096};
        HEPTAD_WEIGHT['G']=new double[]{0.084, 0.215, 0.432, 0.111, 0.153, 0.367, 0.125};
        HEPTAD_WEIGHT['A']=new double[]{1.283, 1.364, 1.077, 2.219, 0.490, 1.265, 0.903};
        HEPTAD_WEIGHT['K']=new double[]{1.233, 2.194, 1.817, 0.611, 2.095, 1.686, 2.027};
        HEPTAD_WEIGHT['R']=new double[]{1.014, 1.476, 1.771, 0.114, 1.667, 2.006, 1.844};
        HEPTAD_WEIGHT['H']=new double[]{0.590, 0.646, 0.584, 0.842, 0.307, 0.611, 0.396};
        HEPTAD_WEIGHT['E']=new double[]{0.281, 3.351, 2.998, 0.789, 4.868, 2.735, 3.812};
        HEPTAD_WEIGHT['D']=new double[]{0.068, 2.103, 1.646, 0.182, 0.664, 1.581, 1.401};
        HEPTAD_WEIGHT['Q']=new double[]{0.311, 2.290, 2.330, 0.811, 2.596, 2.155, 2.585};
        HEPTAD_WEIGHT['N']=new double[]{1.231, 1.683, 2.157, 0.197, 1.653, 2.430, 2.065};
        HEPTAD_WEIGHT['S']=new double[]{0.332, 0.753, 0.930, 0.424, 0.734, 0.801, 0.518};
        HEPTAD_WEIGHT['T']=new double[]{0.197, 0.543, 0.647, 0.680, 0.905, 0.643, 0.808};
        HEPTAD_WEIGHT['C']=new double[]{0.918, 0.002, 0.385, 0.440, 0.138, 0.432, 0.079};
        HEPTAD_WEIGHT['W']=new double[]{0.066, 0.064, 0.065, 0.747, 0.006, 0.115, 0.014};
        HEPTAD_WEIGHT['P']=new double[]{0.004, 0.108, 0.018, 0.006, 0.010, 0.004, 0.007};
        for(char i='A';i<='Z';i++) HEPTAD_WEIGHT[i|32]=HEPTAD_WEIGHT[i];
    }

    @Override public void setGappedSequences(String ss[]){
        _sequences=ss;
    }
    @Override public void compute(){
        _prediction=new char[_sequences.length][];
        for(int i=0;i<_sequences.length;i++) {
            final boolean bb[]=pred_coils(_sequences[i].toUpperCase().replaceAll("[^A-Z]","").getBytes());
            _prediction[i]=new char[bb.length];
            for(int j=0;j<bb.length;j++) _prediction[i][j]= bb[j] ? 'X' : ' ';
        }
    }
    @Override public char[][] getPrediction(){ return _prediction;}
    /** Window length assumed to be 21 amino acids. Data taken from new.mat.
        Data are arranged as [0] m_cc (mean for coiled coils) [1] sd_cc,
        [2] m_g (mean for globular), [3] sd_g, [4] scaling factor,
        and are entered separately for weighted and unweighter*/
/**
     * pre_coils() implements the coil _prediction algorithm
     * on the protein sequence seq
*/
    private static boolean[]  pred_coils(byte seq[]){
        final int L=seq.length;
        final boolean weighted=false; /* default UNWEIGHTED */
        final boolean selRes[]=new boolean[L];
        final double[] score=new double[L], P=new double[L];
         for(int i=0; i<(L-(WINDOW-1)); ++i) { /* Iterate over each win-sized substring */
            double this_score=1;
            double actual_win=0;
            for(int j=0; (j<WINDOW && i+j<L); ++j) {
                final int aa_pt=seq[i+j];
                if (aa_pt != '_') {
                    final int pos=j%7;
                    final double power= weighted && (pos==0 || pos==3) ? 2.5 :1;
                    actual_win+=power;
                    this_score *= Math.pow(HEPTAD_WEIGHT[aa_pt][pos],power);
                }
            }
            this_score= actual_win>0 ? Math.pow(this_score,(1.0/actual_win)) : 0;
            for(int j=0; (j<WINDOW && i+j<L); ++j) {
                final int aa_pt=seq[i+j];
                if(aa_pt != '_' )  {
                    if(this_score>score[i+j]) {
                        score[i+j]=this_score;
                    }
                }
            }
        }
        for(int i=0; i<L; ++i) {
            final double weight[]=weighted ? WEIGHTED : UNWEIGHTED;
            double
                t1=1/weight[SD_CC],
                t2=(score[i]-weight[M_CC])/weight[SD_CC],
                t3=Math.abs(t2),
                t4=t3*t3;
            final double Gcc=t1 * Math.exp(-0.5*t4);
            t1=1/weight[SD_G];
            t2=(score[i]-weight[M_G]) / weight[SD_G];
            t3=Math.abs(t2);
            t4=t3*t3;
            final double Gg=t1 * Math.exp(-0.5*t4);
            P[i]=Gcc/(weight[SCALING]*Gg+Gcc);
            if(P[i]>=min_P) selRes[i]=true;
        }
        return selRes;
    }
}
