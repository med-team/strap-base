package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.strap.*;
import charite.christo.protein.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
This demo shows how own objects can be attached, retrieved and removed
from proteins.  <br> This will become important to you when you
develop Strap extensions that require a storage place for information.
<i>SEE_CLASS:Protein</i>
*/
public class DemoAssociateObjectsToProteins extends JPanel implements ActionListener,StrapExtension,Disposable,HasRenderer {
    public void run() {
        add(getPanel());
        StrapAlign.addDialog(DemoAssociateObjectsToProteins.this);
    }
    {
        final String tip="Demo associating user objects";
        TabItemTipIcon.set(null,null,tip,IC_SUPERIMPOSE,DemoAssociateObjectsToProteins.class);
    }
    public Object getRenderer(long options, long rendOptions[]) { return "this is returned by labelText()";}
    // ----------------------------------------
    private JComponent _p;
    private JComponent getPanel() {
        if (_p==null) {
            _p=Box.createVerticalBox();
            _p.add(pnl(HB,"This demo shows how to add and remove user objects",new ChButton().doClose(0,this)));
            _p.add(pCombo=new ProteinCombo(0));
            _p.add(pnl(HB,butAdd,butCount,butRemove));
        }
        return _p;
    }
    // ----------------------------------------
    public final void handleEvent(StrapEvent ev) {
        StrapAlign.rmListener(this);
    }
    // ----------------------------------------
    private UniqueList<Object> vObjects;

    private ProteinCombo pCombo;
    private final ChButton
        butAdd=new ChButton("add one object").li(this),
        butRemove=new ChButton("remove  object").li(this),
        butCount=new ChButton("count objects").li(this);
    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final Protein p=pCombo.getProtein();
        if (p==null) return;
        if (q==butAdd){
            if (p!=null) {
                if (vObjects==null) {
                    vObjects=new UniqueList(Object.class).t("Demo Objects");
                    p.putClientProperty(DemoAssociateObjectsToProteins.class,vObjects);
                }
                vObjects.add(new MyObject());
            }
        }
        if (q==butRemove){
            if (p!=null && vObjects!=null) vObjects.clear();
        }
        if (q==butCount){
            ChMsg.msgDialog(0L, "I counted "+(vObjects==null ? 0 : vObjects.size()) +" Objects in "+p);
        }
    }
    private static class MyObject  {
        String date;
        public MyObject() { date=new java.text.SimpleDateFormat("yyyy MM:dd:hh mm").format(new Date()); }
        @Override public String toString() { return date;}
    }
    public void dispose() {StrapAlign.rmListener(this); }
}
