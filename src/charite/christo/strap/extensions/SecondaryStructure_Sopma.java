package charite.christo.strap.extensions;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
/**HELP

<b>Authors:</b>  C Geourjon  and  G Deleage. <br>
<b>Web site:</b> http://npsa-pbil.ibcp.fr/cgi-bin/npsa_automat.pl?page=npsa_sopma.html <br>
<b>Publication:</b> PUBMED:8808585 PUBMED:10694887 <br>
   @author Christoph Gille
*/
public class SecondaryStructure_Sopma extends AbstractPredictionFromAminoacidSequence implements SecondaryStructure_Predictor,HasControlPanel,HasSharedControlPanel,NeedsInternet  {
    /* --- Control panels --- */
    public Object getSharedControlPanel() { return null;}
    /* --- Computation --- */
    private final static Object SYNC=new Object();
    @Override public char[] compute(String sequ) {
        if (sequ==null) return null;
        final BA data;
        synchronized(SYNC) {
            data=getResultText(sequ,"http://npsa-pbil.ibcp.fr/cgi-bin/secpred_sopma.pl",new Object[][]{
                               {"notice",sequ},
                               {"threshold","8"},
                               {"width","17"},
                               {"states","3"},
                               {"ali_width","99999"}
                }, false);
        }
        if (data!=null) {
            final BA sb=new BA(300);
            final byte T[]=data.bytes();
            for(int pos=strstr("<CODE>",data); (pos=strstr("</FONT",data,pos+1, Integer.MAX_VALUE))>0;) {
                sb.a((char)T[pos-1]);
            }
            return sb.toString().toCharArray();
        }
        return null;
    }
}
