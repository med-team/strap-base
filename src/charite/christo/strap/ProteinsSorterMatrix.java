package charite.christo.strap;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;


/**
 @author Christoph Gille
*/
public class ProteinsSorterMatrix implements ProteinsSorter {
    private int _dist;
    private Object _vop;
    private boolean _run[];
    public ProteinsSorter setCompareTwoProteins(Object vop) {
        _vop=vop;
        return this;
    }
    public Protein[] sortProteins(Protein[] pp, boolean[] running) {
        _run=running;
        final int N=pp.length;
        if (N<2) return pp;
        final float[][] m=matrix(pp);

        {
            //putln(YELLOW_DEBUG+" representant="+pp[representant(m, _dist==CTRUE)]);

        }

        final int ii[]=sort(m, _dist==CTRUE);
        final Protein sorted[]=new Protein[N];
        for(int i=0; i<N; i++) sorted[i]=pp[ii[i]];
        return running!=null && !running[0] ? null : sorted;
    }
    public static int[] sort(float[][] matrix, boolean distance) {
        final int N=matrix.length;
        final int sorted[]=new int[N];
        final boolean[] taken=new boolean[N];
        taken[sorted[0]=representant(matrix, distance)]=true;
        for(int i=1; i<N;i++) {
            double bestScore=0;
            int best=-1;
            for(int j=N; --j>=0;) {
                if (taken[j]) continue;
                double sum=0;
                for(int k=0; k<i; k++) {
                    if (j==sorted[k]) putln(RED_ERROR+"j==sorted[k]");
                    if (matrix[j][sorted[k]]!=matrix[sorted[k]][j]) putln(RED_ERROR+"symmetrie");
                    sum+=(1+k*k)*matrix[j][sorted[k]];
                }
                if (best<0 || (distance ? sum<bestScore : sum>bestScore)) { bestScore=sum; best=j; }
            }
            taken[sorted[i]=best]=true;
        }
        return sorted;
    }
    private float[][] matrix(Protein pp[]) {
        final boolean[] run=_run;
        final Object hasScore=_vop!=null?_vop : new charite.christo.strap.extensions.AlignmentScoreBlosume62();
        final float m[][]=new float[pp.length][pp.length];
        //Set debug=new HashSet();
        for(int i=pp.length; --i>=0;) {
            for(int j=i; --j>=0;) {
                if (run!=null && !run[0]) break;
                m[i][j]=m[j][i]=(float)SPUtils.computeValue(0, hasScore, pp[i], pp[j], 0, MAX_INT);
                if (_dist==0) _dist=SPUtils.isDistanceScore(hasScore) ? CTRUE : CFALSE;
                /*
                {
                    String test=i+"_"+j;
                    if (!debug.add(test)) debugExit(test);
                }
                */
            }
        }
        return m;
    }
    private static int representant(float[][] m, boolean dist) {
        double bestScore=0;
        int best=-1;
        for(int i=m.length; --i>=0;) {
            double sum=0;
            for(int j=m.length; --j>=0;)  if (i!=j) sum+=m[i][j];
            if (best<0 || (dist ? sum<bestScore: sum>bestScore)) { best=i; bestScore=sum;}
        }
        return best;

    }

}
