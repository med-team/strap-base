package charite.christo.strap;
import charite.christo.*;
import charite.christo.blast.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class IntermediateSeqPair extends JPanel implements Disposable,Runnable,HasControlPanel,
                                                           ActionListener, MouseWheelListener, PaintHook, PreferredSize {
    private final static Point ORIGIN=new Point();
    private final Protein[] _p;
    private int _blosums[][], _gapOpen=8, _gapExt=2, _fontSize=14;
    private Font _font=getFnt(14,true,0);
    private JComponent _ctrl;
    private final DrawGappedSequence dgs=new DrawGappedSequence();
    final UniqueList<BlastHitIntersection> vAllAlignments=new UniqueList(BlastHitIntersection.class);

    private final DialogDotPlot _plot;
    private final BA LOG=new BA(0);
    private final ChJTable _jt;
    private final BlastResult _blastResult[];
    private final ChTextField _tfExp,_tfNum,
        _labNum=new ChTextField().cols(8,true,true),
        _tfGapOpen=new ChTextField("8").cols(4,true,true),
        _tfGapExt=new ChTextField("2").cols(4,true,true);
    private final JScrollBar _vSB;
    private final UniqueList<HitView> _vHitView=new UniqueList(HitView.class);
    final UniqueList<HitView> _vHitViewShown=new UniqueList(HitView.class);

    public IntermediateSeqPair( Protein p0,Protein p1,BlastResult res0, BlastResult res1) {
        final ChTableModel model=new ChTableModel(0L,new String[]{"x"}).setData(_vHitViewShown).editable(true);
        final ChJTable t=_jt=new ChJTable(model,ChJTable.DEFAULT_RENDERER);
        t.addMouseWheelListener(this);
        final ChJScrollPane sp=scrllpn(t);
        noBrdr(sp);
        sp.getViewport().setBackground(C(DEFAULT_BACKGROUND));
        _plot=new DialogDotPlot(DialogDotPlot.BL2SEQ|DialogDotPlot.CB_HITS,p0,p1);
        _blastResult=new BlastResult[]{ res0,res1};
        _p=new Protein[]{p0,p1};
        _tfExp=new ChTextField("1").cols(9,true,true).li(this);

        _tfNum=new ChTextField("55").cols(4,true,true).li(this);

        final JComponent
            pNorth=pnl(CNSEW,pnl(new ProteinLabel(0L,p0),new ProteinLabel(0L,p1),ChButton.doPrint(this)),null,null,
                                   new ChButton().doClose(CLOSE_RM_CHILDS, this),
                            ChButton.doCtrl(this).rover(IC_CONTROLPANEL)),
            pSouth=pnl(VB, pnl( new ChButton("LOG").t("Log").li(this),"  e-value<",_tfExp," show",_tfNum, _labNum));
        for(int i=0;i<2;i++) {
            final JComponent pnl=pnl(dim(9999,EX));
            addPaintHook(this,pnl);
            pcp("ID", i==0 ? "ALL0":"ALL1",pnl);
            pSouth.add(pnl);
        }
        t.setRowSelectionAllowed(false);
        t.setColumnSelectionAllowed(false);
        _vSB=sp.getVerticalScrollBar();
        _vSB.setUnitIncrement(3*EX);
        final int h=StrapAlign.dialogPanel().getHeight()/2;
        _plot.setPreferredSize(dim(100,h));
        setMinSze(0,0, _plot);
        final JSplitPane pSplit=new JSplitPane(JSplitPane.VERTICAL_SPLIT,_plot,sp);
        pSplit.setOneTouchExpandable(true);
        pnl(this,CNSEW, pSplit,pNorth,pSouth);
        run();
        {
            final String name=_p[0]+"-"+_p[1];
            pcp(KEY_IMAGE_FILE_NAME,"intermedSequence-dotplot-"+name,_plot);
            pcp(KEY_IMAGE_FILE_NAME,"intermedSequence-"+name,_jt);
            pcp(KEY_PRINTABLE_COMPONENTS,new Object[]{_plot,_jt},this);
        }
    }

    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final boolean isSelected=isSelctd(q);
        BA showLog=null;
        if ((q==_tfNum || q==_tfExp) && (cmd==ACTION_ENTER || cmd==ACTION_FOCUS_LOST)) {
            run();
            SwingUtilities.updateComponentTreeUI(this);
        }
        if (cmd=="LOG") showLog=LOG;
        final HitView v=(HitView)gcp(HitView.class,q);
        if (v!=null) {
            if (cmd=="CB") {
                v._selected[0]=isSelected;
                allBlosums(true);
                final int fromIdx,toIndex;
                final HitView vv[]=_vHitViewShown.asArray();
                if (isShift(ev)) {
                    fromIdx=0;
                    toIndex=vv.length;
                    for(HitView hv:vv) hv.cb.setSelected(hv._selected[0]=isSelected);
                } else {
                    int i=0;
                    while(i<vv.length && vv[i]!=v) i++;
                    fromIdx=toIndex=i;
                }
                final ListSelectionModel lsm=_jt.getSelectionModel();
                if (isSelected) lsm.addSelectionInterval(fromIdx, toIndex);
                else lsm.removeIndexInterval(fromIdx, toIndex);
                repaint();
                toDotplot();
            }
            if (cmd=="L") showLog=new BA(v.logMsg);
            if (cmd=="V") {
                final BA sb=new BA(999);
                int i=0;
                for(Protein p: _p) {
                    sb.aln(p).a("score=").a(v.scores[i]).a(" X-score=").a(v.score).a('\n',2);
                    v.intersection.getAlignment(i).toText(0,sb,120);
                    i++;
                }
                final ChTextView ta=new ChTextView(sb);
                ta.tools().underlineRefs(0);
                pcp(ACTION_DATABASE_CLICKED,"Load",ta);
                ta.tools().showInFrame("blast result");
            }
        }
        shwTxtInW("Log", showLog);
    }
    private static void removeIfMatchedSequenceToShort(List<BlastAlignment>v) {
        for(int i=v.size(); --i>=0;) {
            final BlastAlignment a=v.get(i);
            if (a.getMatchSeq().length<20)
                v.remove(a);
        }
    }
    private static void removeIfMatchedSequencesAreIdentical(UniqueList<BlastAlignment>v) {
        final int size=v.size();
        for(int i=size; --i>=0;) {
            final BlastAlignment a0=v.get(i);
            final int a0Hash=a0.getMatchSeqHashCode();
            boolean del=false;
            for(BlastAlignment a1:v.asArray())
                if (a0!=a1 && a0Hash==a1.getMatchSeqHashCode() && compareAlphabetically(a0.getMatchSeq(), a1.getMatchSeq(),true )==0) {
                    del=true;
                    break;
                }
            if (del) v.remove(a0);
        }
        //StrapAlign.drawProgress(0);
    }
    private void listIntersections() {
        vAllAlignments.clear();
        final UniqueList<BlastAlignment> vBlastAli0=new UniqueList(BlastAlignment.class),  vBlastAli1=new UniqueList(BlastAlignment.class);
        for(BlastHit h0:_blastResult[0].getHits()) adAll(h0.getAlignments(),vBlastAli0);
        for(BlastHit h1:_blastResult[1].getHits()) adAll(h1.getAlignments(),vBlastAli1);
        removeIfMatchedSequenceToShort(vBlastAli0);
        removeIfMatchedSequenceToShort(vBlastAli1);
        removeIfMatchedSequencesAreIdentical(vBlastAli0);
        removeIfMatchedSequencesAreIdentical(vBlastAli1);
        final int size=vBlastAli0.size();
        puts(RED_WARNING+"BlastHitIntersection.intersect["+size+"]: ");

        for(BlastAlignment a0:vBlastAli0.asArray()) {
            //if (size>0 && count%10==0) StrapAlign.drawProgress(count++*100/size);
            for(BlastAlignment a1:vBlastAli1.asArray()) {
                adNotNull(BlastHitIntersection.intersect(a0,a1), vAllAlignments);

            }
        }
        LOG.a(" The proteins have ").a(vAllAlignments.size()).aln(" BLAST hits in common\n");
        if (vAllAlignments.size()>333) {
            final int[]  hist=BlastHitIntersection.expectValueHistogramm(vAllAlignments.asArray());
            LOG.aln("\nE-value histogram");
            for(int h:hist) LOG.a(h).a(' ');
            final float eValueCutoff=BlastHitIntersection.expectValueCutoff(hist,333);
            LOG.a("\nI want to have less than #333 and take an E-value cutoff of ").a(eValueCutoff).a('\n');
            for(int i=vAllAlignments.size(); --i>=0;) {
                final BlastHitIntersection sect=vAllAlignments.get(i);
                if (sect.getExpect()>eValueCutoff) vAllAlignments.remove(sect);
            }
            LOG.a("The number vAllAlignments.size() is now ").a(vAllAlignments.size()).a('\n');
        }
    }
    public void run() {
        if (vAllAlignments.size()==0) listIntersections();
        final List<BlastHitIntersection> vAlignments=new UniqueList(BlastHitIntersection.class);
        vAlignments.addAll(vAllAlignments);

        _gapOpen=atoi(_tfGapOpen);
        _gapExt=atoi(_tfGapExt);
        final float expVal=(float)atof(_tfExp);
        if (!Float.isNaN(expVal)) {
            final int size=vAlignments.size();
            for(int i=size;--i>=0;) {
                puts("+");
                final BlastHitIntersection sect=vAlignments.get(i);
                if (sect.getExpect()>expVal) vAlignments.remove(sect);
            }
            LOG.a(" apply text field e-value ").a(expVal).a(" vAlignments.size()=").a(size).a(" ==> ").a(vAlignments.size()).a('\n',2);
        }

        final int size=vAlignments.size();
        final HitView[] aHitView=new HitView[size];
        //puts(" Creating HitView ["+size+"]: ");
        for(int i=vAlignments.size();--i>=0;) {
            //if (i%30==0) StrapAlign.drawProgress((size-i-1)*100/size);
            aHitView[i]=newHitView(vAlignments.get(i));
        }
        //StrapAlign.drawProgress(0);
        sortArry(aHitView);
        _vHitView.clear();
        adAll(aHitView, _vHitView);
        JComponent firstComponent=null;
        final int countDisplayedHits=mini(_vHitView.size(),atoi(_tfNum));
        _labNum.setText("of "+vAlignments.size());
        _labNum.setEditable(false);
        _vHitViewShown.clear();
        for(int i=0;i<countDisplayedHits;i++) {
            final HitView view=_vHitView.get(i);
            initView(view);
            if (firstComponent==null) firstComponent=view;
            _vHitViewShown.add(view);
        }
        if (firstComponent!=null) {
            firstComponent. scrollRectToVisible(new Rectangle(0,0,1,1));
            setFont(_fontSize,false);
        }
    }
    public void dispose() {
        rmAllChilds(this);
        vAllAlignments.clear();
        _vHitView.clear();
    }
    private int[][] allBlosums(boolean always) {
        int all[][]=_blosums;
        if (all==null) _blosums=new int[][]{new int[_p[0].countResidues()],new int[_p[1].countResidues()]};
        if (always||all==null) {
            all=_blosums;
            final byte[][] BLOS=Blosum.BLOSUM62;
            for(int iP=0;iP<2;iP++) {
                final int[] blos=all[iP];
                Arrays.fill(blos,0);
                for(HitView v: _vHitViewShown.asArray()) {
                    if (!v._selected[0])  continue;
                    final byte q[]=v.gapped[iP];
                    final byte m[]=v.match;
                    for(int col=0, iAa=0; col<q.length && col<m.length; col++) {
                        final int c=q[col]|32;
                        if ('a'<=c && c<='z') {
                            final int b=BLOS[c&127][m[col]&127];
                            if (b>Byte.MIN_VALUE) blos[iAa]+=b;
                            iAa++;
                        }
                    }
                }
                for(int i=blos.length; --i>=0;)
                    if (blos[i]<Blosum.BLOSUM62_MIN) blos[i]=Blosum.BLOSUM62_MIN;
                    else if (blos[i]>Blosum.BLOSUM62_MAX) blos[i]=Blosum.BLOSUM62_MAX;

            }
        }
        return all;
    }

    public boolean paintHook(JComponent component,Graphics g, boolean after) {
        if (!after) return true;
        else {
            final boolean isWhiteBG=isWhiteBG();
            final Color fg=C(isWhiteBG ? 0:0xFFffFF);
            final Color bg=C(isWhiteBG ? 0xFFffFF:0);
            g.setColor(bg);
            final Font fnt=_font;
            final int w=component.getWidth();
            final int h=component.getHeight(), h2=h/2;
            final Rectangle cb=chrBnds(fnt), clip=clipBnds(g,w,h);
            final int clipX1=x(clip),  clipX2=x2(clip);

            final Object ID=gcp("ID",component);
            final int all01= ID=="ALL0" ? 0 : ID=="ALL1" ? 1 : -1;
            final int charH=cb.height, charW=cb.width;
            if (all01>=0) {
                final int blosums[]=allBlosums(false)[all01], n=blosums.length, rgb[]=sharedRGB(n);
                Arrays.fill(rgb,0,n,0);
                for(int i=n;--i>=0;) {
                    final int bl0=blosums[i];
                    if (bl0<0) continue;
                    final int bl=((bl0<<8)/Blosum.BLOSUM62_MAX)&255;
                    final int white=bl>>>1;
                    rgb[i]= all01==0 ? (white<<16) +  (bl<<8) + white :  (bl<<16) +  (white<<8)  + white;
                    if (isWhiteBG) rgb[i]=~rgb[i];
                }
                final java.awt.image.BufferedImage image=sharedImage('H',n);
                image.setRGB(0,0,n,1, rgb,0,n);
                g.drawImage(image,0,1,w,h,   0,0,n,1,this);
            }
            final HitView v=(HitView)gcp(HitView.class,component);
            if (v!=null) {
                if (ID=="CENTER") {
                    initView(v);
                    final byte[]  q0=v.gapped[0], q1=v.gapped[1], m=v.match;
                    g.setColor(bg); g.fillRect(0,0,w,h);
                    setFont(fnt);
                    g.setFont(fnt);
                    final int mode=ShadingAA.i()==ShadingAA.SECSTRU ? dgs.SHADE_SEC_STRU : 0;
                    final Color[] shadingColors=ShadingAA.colors(false);
                    setXY(0,-charH/2, ORIGIN);
                    final int fromCol=maxi(0,clipX1/charW-1);
                    final int toCol=clipX2/charW+1;
                    g.setColor(fg);dgs.drawBlastMidline(q0,q1,fromCol,toCol, null, g,0, ORIGIN.y-1);
                    ORIGIN.y+=charH;
                    dgs.draw(this,g,ORIGIN, 0,q0, fromCol, toCol, null, shadingColors,mode);
                    ORIGIN.y+=charH;
                    g.setColor(fg);dgs.drawBlastMidline(q0,m,0,MAX_INT, null, g,0,ORIGIN.y);
                    ORIGIN.y+=charH;
                    dgs.draw(this,g,ORIGIN, 0, m, fromCol, toCol, null, shadingColors,mode);
                    ORIGIN.y+=charH;
                    g.setColor(fg);dgs.drawBlastMidline(m,q1,0,MAX_INT, null, g,0,ORIGIN.y);
                    ORIGIN.y+=charH;
                    dgs.draw(this,g,ORIGIN, 0, q1, fromCol, toCol, null, shadingColors,mode);
                    ORIGIN.y+=charH;
                    g.setColor(fg);dgs.drawBlastMidline(q1,q0,0,MAX_INT, null, g,0,ORIGIN.y+1);
                    return true;
                }
                final int oneOrTwo=ID=="SP0"?0 : ID=="SP1" ? 1 : -1;
                if (oneOrTwo>=0) {
                    final ChScrollBar sb=(ChScrollBar) component;
                    final Rectangle track=sb.getTrack();
                    final int dx1=x(track), dx2=x2(track);
                    g.setColor(bg);
                    g.fillRect(0,h2, 1,1);
                    g.fillRect(0,0,dx1,h);
                    g.fillRect(dx1,0,w,h);
                    final byte[] seq=v.gapped[oneOrTwo], match=v.match;
                    final int nRes=v.countLetters[oneOrTwo];
                    final int[] rgb=sharedRGB(nRes);
                    final byte[][]M=Blosum.BLOSUM62;
                    final java.awt.image.BufferedImage image=sharedImage('H',nRes);
                    final int allBll[][]=allBlosums(false);
                    final int allBl[]=allBll[oneOrTwo];
                    final int colF=sb.getValue()/charW;
                    final int colT=(sb.getValue()+sb.getVisibleAmount())/charW;
                    int iaF=0, iaT=0;
                    for(int isDiff=0; isDiff<2; isDiff++) {
                        Arrays.fill(rgb,0,nRes,0);
                        for(int col=0, iAa=0; iAa<nRes && col<seq.length; col++) {
                            final int c=seq[col]|32;
                            if ('a'<=c && c<='z') {
                                final int bl0=col<match.length ? M[c&127][match[col]&127] : -1;
                                final int bl1=isDiff>0 ? bl0-allBl[iAa]:bl0;
                                if (bl0>=0 && bl1>0) {
                                    final int bl= (bl1 * 256 / Blosum.BLOSUM62_MAX)&255;
                                    final int white=bl/2;
                                    rgb[iAa]=  (oneOrTwo==0 ? (white<<16)|(bl<<8) : (bl<<16)|(white<<8) )  + white;
                                    if (isWhiteBG) rgb[iAa]=~rgb[iAa];
                                }
                                if (iaF==0 && col>colF) iaF=iAa;
                                iAa++;
                                if (col<=colT) iaT=iAa;
                            }
                        }
                        image.setRGB(0,0,nRes,1, rgb,0,nRes);
                        final int dy1=isDiff>0 ? h2+1: 1;
                        final int dy2=isDiff>0 ? h: h2;
                        g.drawImage(image,  dx1,dy1, dx2,dy2,    0,0,nRes,1, this);
                    }
                    if (nRes!=0) {
                        final int span=w-2*dx1;
                        final int x0=iaF*span/nRes;
                        final int x1=iaT*span/nRes;
                        g.setColor(C(0x8080ff,0x60));
                        g.fillRect(x0+dx1,0, x1-x0,h);
                        if (colF!=atoi(gcp("LV",sb))) { pcp("LF",intObjct(colF),sb); ChDelay.repaint(sb,333); }
                    }

                }
            }
        }
        return true;
    }
    @Override public Object getControlPanel(boolean real) {
        if (_ctrl==null) _ctrl=pnl(" gapOpen=",_tfGapOpen," gapExt=",_tfGapExt);
        return _ctrl;
    }

    private void setFont(int dFont,boolean isRelative) {
        _fontSize=maxi(4,((isRelative ? _fontSize : 0)+dFont));
        _font=getFnt(_fontSize,true,0);
        if (_vHitViewShown.size()>0) {
            _jt.setRowHeight(prefH(_vHitViewShown.get(0))+ChJTable.COLUMN_EXTRA_WIDTH);
            _jt.revalidate();
        }
    }

    public void mouseWheelMoved(MouseWheelEvent mev) {
        final int i=mev.getWheelRotation();
        if (isCtrl(mev)) setFont(i,true);
        else if (!isShift((mev))) _vSB.setValue(_vSB.getValue()+i*EX);
    }

    @Override public Dimension preferredSize(Object component) {
        final Object ID=gcp("ID",component);
        final HitView v=(HitView)gcp(HitView.class,component);
        if (ID=="CENTER" && v!=null) {
            initView(v);
            final Rectangle cb=chrBnds(_font);
            final int w=cb.width*(maxi(v.gapped[0].length, v.gapped[1].length)+1);
            return dim(w+3*cb.width, cb.height*6);
        }
        return dim(0,0);
    }

    public void initView(HitView v) {
        if (v.cb!=null) return;
        final Object refHitView=wref(v);
        v.setLayout(new BoxLayout(v,BoxLayout.Y_AXIS));
        final JComponent ali=pnl();
        setPrefSze(this,ali);
        addPaintHook(this,ali);
        monospc(ali);
        final BlastHitIntersection intersect=v.intersection;
        final String db=intersect.getAlignment(0).getDatabase();
        v.id=db+":"+intersect.getAlignment(0).getID();
        final ChButton tog=toggl("CB").t(null).tt("With Shift-key: select all").li(this).s(v._selected[0]);
        pcp(HitView.class,refHitView,tog);
        v.cb=tog.cb();
        final ChTextField tf=new ChTextField(v.id+"  score "+(int)v.blosumScore+" "+toStrg(intersect.getAlignment(0).getBlastHit().getDescription()));
        try { ((JTextField)tf).setCaretPosition(0); } catch(Exception e){}
        pcp(KOPT_TRACKS_VIEWPORT_HEIGHT,"",ali);
        final ChJScrollPane sp=scrllpn(SCRLLPN_NO_VSB|SCRLLPN_NO_HSB,ali);
        tf.setPreferredSize(dim(999999,EX+4));
        pcp(ACTION_DATABASE_CLICKED,"Load",tf);
        tf.tools().underlineRefs(0);
        final Object butLog=new ChButton("L").t("Log").li(this).cp(HitView.class,refHitView);
        final Object butView=new ChButton("V").t("view").li(this).cp(HitView.class,refHitView);
        final ChJScrollPane sp2=scrllpn(tf);
        sp2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        sp2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        v.add(pnl(HB,v.cb, sp2,butLog,butView));
        for(int j=0; j<2; j++) {
            final ChScrollBar sb=new ChScrollBar(JScrollBar.HORIZONTAL);
            addPaintHook(this,sb);
            sb.setUnitIncrement(2*EM);
            if (j==0) sp.setHorizontalScrollBar(sb);
            v.add(sb);
            if (j==0) {
                v.add(sp);
                setDragScrolls('H',ali, sp);
                setDragScrolls('V',ali, scrllpn(_jt));
            } else sb.setModel(sp.getHorizontalScrollBar().getModel());
            pcp(HitView.class,refHitView,sb);
            pcp("ID",j==0?"SP0":"SP1",sb);
        }

        pcp("ID","CENTER",ali);
        pcp(HitView.class,refHitView,ali);
        pcp(ChRenderer.KEY_RENDERER_COMPONENT,refHitView,v);

    }

    private HitView newHitView(BlastHitIntersection a) {
        final HitView v=new HitView();
        v.intersection=a;
        v._selected=a.SELECTED;
        byte[] q0=null,q1=null, m0=null, m1=null;
        final BA log=new BA(999);

        for(int i=0;i<2;i++) {
            final byte[] querySeq=_p[i].getResidueType();
            final byte[] matchSeq=a.getSeq('M',i);
            final int nSpc=a.getQueryStart(i);
            final byte[] spc_matchSeq=new byte[nSpc+matchSeq.length];
            Arrays.fill(spc_matchSeq,0,nSpc,(byte)32);
            System.arraycopy(matchSeq,0,spc_matchSeq,nSpc,matchSeq.length);
            final byte[] q=concat(querySeq,0,a.getQueryStart(i),   a.getSeq('Q',i),0,MAX_INT,   querySeq, a.getQueryEnd(i), MAX_INT,null);
            v.countLetters[i]=countLettrs(q);

            if (i==0) { q0=q; m0=spc_matchSeq;} else { q1=q; m1=spc_matchSeq; }
            if (log!=null) {
                log.a("i=").a(i).a('\n').aln(q).aln(spc_matchSeq).a('\n')
                    .a("getSeq(M,0)=").aln(a.getSeq('M',0))
                    .a("getSeq(M,1)=").aln(a.getSeq('M',1)).a('\n');
            }
            v.scores[i]=a.getAlignment(i).getScore();
        }
        final byte[][] l3=BlastHitIntersection.mergeAlignments(q0,q0.length, m0,m0.length, q1,q1.length, m1,m1.length);
        v.match=l3[1];
        v.gapped=new byte[][]{l3[0],l3[2]};
        v.score=v.scores[0]*v.scores[1];
        {
            final byte[] m=l3[1];
            int im=0;
            while(im<m.length && m[im]==' ') im++;
            v.blosumScore=Blosum.blosumScore3(_gapOpen,_gapExt,m, l3[0], l3[2], im, m.length)/(float)(m.length-im);
        }
        v.logMsg=toByts(log);
        return v;
    }

    //card4_human q7rtr2_human

    private static class HitView extends ChPanel implements Comparable {
        final int[] countLetters={0,0};
        final float scores[]=new float[2];
        String id;
        byte[] logMsg, gapped[], match;
        float blosumScore=Float.NaN,score;
        BlastHitIntersection intersection;
        boolean _selected[]={false};
        AbstractButton cb;
        public int compareTo(Object o) {  return o instanceof HitView  && ((HitView)o).blosumScore > blosumScore ? 1 : -1;}
    }

    private void toDotplot() {
        if (_plot==null) return;
        final int[][] vDots=new int[_p[1].countResidues()][];
        for(HitView v : _vHitView.asArray())  {
            if (!v._selected[0]) continue;
            final BlastHitIntersection is=v.intersection;
            _plot.addPoints(is.getSeq('Q',0), 0, false, is.getQueryStart(0),
                           is.getSeq('Q',1), 0, false, is.getQueryStart(1),   v.blosumScore, vDots);
        }
        _plot.setBl2seq(vDots,null);
        _plot.repaint();
    }
}
