package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.Protein;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.StrapScriptInterpreter.*;
/*
 */
public class StrapScriptCreator {
    public final static int ICONS=1<<1, NO_SELECTIONS=1<<2, SELECTED_SELECTIONS=1<<4;
    public static void makeScript(int opt, Protein pp[], BA sb) {
        boolean foundAccession=false;
        for(Protein p : pp) sb.a(SCRIPT_aa_sequence).aRplc(' ','-',p.getGappedSequenceExactLength()).a(", ").aln(p);
        sb.a('\n');
        for(int i=3; --i>=0;) {
            for(Protein p : pp) {
                final String a=p.getAccessionID(), u=i==0?a:i==1?p.getUniprotID() : p.getPdbID();
                if (u==null) continue;
                if (i==0) foundAccession=true;
                else if (u.equals(a)) continue;
                sb.a(i==0?SCRIPT_accession_id:SCRIPT_add_xref).a(u).a(", ").aln(p);
            }
        }
        sb.a('\n');

        final ResidueAnnotation selAA[]=(ResidueAnnotation[]) StrapAlign.coSelected().residueSelections('A');
        for(Protein p : pp) {
            if (0!=(opt&NO_SELECTIONS)) continue;
            for(ResidueAnnotation a:p.residueAnnotations()) {
                final String an=a.getName();
                if (((opt|SELECTED_SELECTIONS)==0 || cntains(a,selAA)) && a.featureName()==null) {
                    sb.a(SCRIPT_new_aminoacid_selection).a(' ').a(p).a('/').a(an).a(", ").a(a.value(ResidueAnnotation.GROUP)).a(", ").aln(ResSelUtils.optimizedPositions(a, false));
                    sb.a(SCRIPT_set_annotation).a(p).a('/').a(an).a(", Color ,#").a(a.getColor(),0,6).a('\n');
                    for(ResidueAnnotation.Entry e : a.entries()) {
                        final String k=e.key(), v=e.value();
                        if (k==ResidueAnnotation.POS || k==ResidueAnnotation.GROUP || k==ResidueAnnotation.NAME || !e.isEnabled()) continue;
                        if (v.indexOf(Hyperrefs.PAIR_ALIGNMENT)>=0 || v.indexOf(SequenceFeatures.ALI_SCORE)>=0) continue;
                        sb.a(SCRIPT_add_annotation).a(p).a('/').a(an).a(", ").or(StrapPlugins.mapL2S(k),k).a(", ").aln(v);
                    }
                    sb.a('\n');
                }
            }
        }
        sb.a('\n');
        for(Protein p : pp) {
            final charite.christo.protein.Matrix3D m3d=p.getRotationAndTranslation();
            if (m3d!=null && !m3d.isUnit()) {
                sb.a(SCRIPT_rotate_translate);
                m3d.toText(0,null,sb).a(", ").aln(p);
            }
        }
        for(Protein p : pp) {
            for(String ref:p.getSequenceRefs()) sb.a(SCRIPT_add_xref).a(ref).a(", ").aln(p);
            final String inf3d=p.getInferredPdbID();
            if (inf3d!=null) sb.a(SCRIPT_infer_xyz).a(p.getInferredPdbID()).a(" ").aln(p);
        }
        if (0!=(ICONS&opt)) {
            for(Protein p : pp) {
                final BA iconUrl=readBytes(p.iconLink());
                if (sze(iconUrl)>0 && !strEquls(URL_RCSB_IMAGES,iconUrl,0)) sb.a(SCRIPT_icon ).a(p).a(", ").aln(iconUrl);
            }
            sb.a('\n');
        }
        if (foundAccession) sb.a(SCRIPT_download_files).join(pp," ").a('\n');
    }
}
