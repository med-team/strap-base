package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.Protein;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
A choice menu containing for selecting a protein.
   @author Christoph Gille
*/
public class ProteinComboModel implements ComboBoxModel, HasMC {
    private final long _filter;
    private Object _selectedItem;
    public ProteinComboModel(long filter) {_filter=filter;}
    public int indexOf(Protein p) {
        final StrapView panel=StrapAlign.alignmentPanel();
        final Protein pp1[]=panel!=null ? panel.pp() : StrapAlign.ppAliV().asArray();
        final Protein pp2[]=StrapAlign.ppNotAliV().asArray();
        int idx=idxOf(p,pp1);
        if (idx>=0) return idx;
        idx=idxOf(p,pp2);
        if (idx>=0) return pp1.length+idx;
        return -1;
    }
    public void addListDataListener(javax.swing.event.ListDataListener l){/*vListDataListener.add(l);*/}
    public void removeListDataListener(javax.swing.event.ListDataListener l){/*vListDataListener.remove(l);*/}
    public Object getElementAt(int i) {
        final StrapView v=StrapAlign.alignmentPanel();
        final Protein pp1[]=v!=null ? v.pp() : StrapAlign.ppAliV().asArray();
        final Protein pp2[]=StrapAlign.ppNotAliV().asArray();
        if (i<0 || i>=pp1.length+pp2.length) return null;
        return i<pp1.length ? pp1[i] : pp2[i-pp1.length];
    }
    public int mc() {
        final StrapView v=StrapAlign.alignmentPanel();
        return Protein.MC_GLOBAL[charite.christo.protein.ProteinMC.MCA_PROTEINS_V]+(v!=null ? v.mc()  : 0);
    }
    public int getSize() {
        final StrapView panel=StrapAlign.alignmentPanel();
        final Protein[]
            pp1=panel!=null ? panel.pp() : StrapAlign.ppAliV().asArray(),
            pp2=StrapAlign.ppNotAliV().asArray();
        return pp1.length+pp2.length;
    }

    public void setSelectedItem(Object o) { _selectedItem=o;}
    public Object getSelectedItem() {
        final Object i=_selectedItem;
        if (i instanceof Protein && indexOf((Protein)i)>=0) return i;
        final long f=_filter;
        final StrapView panel=StrapAlign.alignmentPanel();
        final Protein[]
            pp1=panel!=null ? panel.pp() : StrapAlign.ppAliV().asArray(),
            pp2=StrapAlign.ppNotAliV().asArray();
        for(int i10=2; --i10>=0;) {
            for(Protein p:i10==1? pp1:pp2) {
                if (ProteinList.isEnabled(p,f)) return _selectedItem=p;
            }
        }
        return null;
    }
}
