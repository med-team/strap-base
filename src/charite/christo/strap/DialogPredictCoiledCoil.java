package charite.christo.strap;
/**HELP

WIKI:Coiled_coil regions are predicted from amino acid sequences.

   @author Christoph Gille
*/
public class DialogPredictCoiledCoil  extends AbstractDialogPredict {
    {
        init(this, charite.christo.protein.CoiledCoil_Predictor.class,  "coiled coil");
    }
}
