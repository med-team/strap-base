package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import javax.swing.JSlider;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

In <i>DIALOG:DialogSelectionOfResiduesMain</i> the method   <i>COMBO:Distance3DToCursor</i> is selected.
<br>
The Euclidean distances between the cursor and  all C-alpha atom coordinates are permanently monitored
when the cursor is moved.

This way amino acids equivalent to the amino acid at the cursor position show up in all proteins.

Before, the proteins should be superimposed.
<br>
<i>SEE_DIALOG:DialogSelectionOfResiduesMain</i>
<i>SEE_DIALOG:DialogSuperimpose3D</i>
   @author Christoph Gille
*/
public class Distance3DToCursor extends BasicResidueSelection implements StrapListener, IsEnabled, HasSharedControlPanel, HasImage, javax.swing.event.ChangeListener {
    private float _lastMax,_lastMin;
    private long _calphaMC;
    private boolean _selected[];
    private final static int MAX_VALUE=800;
    private int _maxV=MAX_VALUE, _minV=0;
    private JSlider _maxSlider,_minSlider;
    private Object _ctrl;
    private Distance3DToCursor _shared;
    public Distance3DToCursor() {
        super(OFFSET_IS_1ST_IDX);
        setImage(img(IC_MEASURE));
        setStyle(VisibleIn123.STYLE_IMAGE);
    }
    public Object getSharedControlPanel() {
        if (_ctrl==null) {
            setBG(DEFAULT_BACKGROUND,_labMin=new ChTextField().cols(8,true,true));
            setBG(DEFAULT_BACKGROUND,_labMax=new ChTextField().cols(8,true,true));
            monospc(_labMax);
            monospc(_labMin);
            set(_labMin,0);
            set(_labMax, MAX_VALUE);
            _maxSlider= slider();
            _minSlider= slider();
            _maxSlider.setValue(MAX_VALUE);
            final Object panNorth=pnl( "underline residues closer than ",_labMax,"and more distant than",_labMin,"to cursor");
            _ctrl=pnl(VB,_minSlider,_maxSlider,panNorth);
        }
        return _ctrl;
    }
    public void setSharedInstance(Object shared) { _shared=(Distance3DToCursor)shared;}
    public Object getSharedInstance() { return _shared; }

    private JSlider slider()  {
        final JSlider sl=new JSlider(0,1600,0);
        sl.setMajorTickSpacing(200);
        sl.setMajorTickSpacing(100);
        sl.setPaintTicks(true);
        sl.addChangeListener(this);
        return sl;
    }
    // ----------------------------------------
    private static ChTextField _labMin,_labMax;
    private static String set(ChTextField lab,long l) {
        final String s=new BA(99).a(l/100f,5,3)+"\u00c5 ";
        lab.t(s);
        return s;
    }
    // ----------------------------------------
    private static float _calpha[];
    private float _calphaPrev[];
    private void calpha() {
        final StrapView sp=StrapAlign.alignmentPanel();

        final Protein pCursor=sp==null?null:sp.cursorProtein();
        if (pCursor==null) return;
        final float xyz[]=pCursor.getResidueCalpha();
        final int i=sp.cursorAminoAcid();
        _calpha=new float[]{get(3*i,xyz),get(3*i+1,xyz),get(3*i+2,xyz)};
    }
    // ----------------------------------------

    @Override public String toString() { return "3D-Distance to Cursor"; }

    public boolean isEnabled(Object o){ return o instanceof Protein &&  ((Protein)o).getResidueCalpha()!=null; }

    public final void handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        if (t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN || t==StrapEvent.CURSOR_CHANGED_PROTEIN || (t&StrapEvent.FLAG_ATOM_COORDINATES_CHANGED)!=0) {
            calpha();
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        }
    }

    @Override public boolean[] getSelectedAminoacids() {
        final Protein p=getProtein();
        if (_calpha==null) calpha();
        final float[] calpha=_calpha;
        if (calpha==null) return null;
        final Distance3DToCursor shared=(Distance3DToCursor)getSharedInstance();
        final float max=(shared!=null?shared._maxV : _maxV)*0.01f;
        final float min=(shared!=null?shared._minV : _minV)*0.01f;
        if (_calphaMC==p.mc(ProteinMC.MC_ATOM_COORD) && _calphaPrev==calpha && _lastMin==min && _lastMax==max) {
            return _selected;
        }

        final float calphas[]=p.getResidueCalpha();
        if (calphas==null) return null;
        final int ca3=calphas.length/3, nR=p.countResidues(), n=ca3<nR ? ca3:nR;
        final float x=calpha[0],y=calpha[1],z=calpha[2];
        final boolean selected[];
        if (_selected==null || _selected.length<n) selected=new boolean[n]; else Arrays.fill( selected=_selected,false);
        final float min2=min*min,max2=max*max;
        for(int i=0,i3=0; i<n; i++,i3+=3) {
            final float xx=x-calphas[i3],yy=y-calphas[i3+1],zz=z-calphas[i3+2],dd=xx*xx+yy*yy+zz*zz;
            selected[i]=dd<max2 && dd>min2;
        }
        _calphaMC=p.mc(ProteinMC.MC_ATOM_COORD);
        _calphaPrev=calpha;
        _lastMax=min;
        _lastMax=max;
        Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS, p);
        return _selected=selected;
    }
    public void stateChanged(javax.swing.event.ChangeEvent ev) {
        final Object q=ev.getSource();
        if (q==_maxSlider) {
            _maxV=_maxSlider.getValue();
            if(_maxV<_minV) {_minSlider.setValue(_minV=_maxV);_minSlider.repaint();}
            setName("distance of C-alpha Atom to cursor "+_labMin+"<distance<"+set(_labMax,_maxV));
        }
        if (q==_minSlider) {
            _minV=_minSlider.getValue();
            if(_minV>_maxV) {_maxSlider.setValue(_maxV=_minV);_maxSlider.repaint();}
            set(_labMin,_minV);
        }
         if (q==_minSlider || q==_maxSlider) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
    }
}
