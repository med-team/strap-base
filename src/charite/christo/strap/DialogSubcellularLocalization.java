package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;

import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.KeyEvent.*;
import static charite.christo.protein.PredictSubcellularLocation.*;
/**HELP

The cellular compartment is predicted from the amino acid sequence.
@author Christoph Gille
*/
public class DialogSubcellularLocalization extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener,ChRunnable, StrapListener {
    private final ChJList classList;
    private final ProteinList proteinList;
    private final JComboBox comboOrgansims;
    private final ChTextView LOG=new ChTextView("");
    public DialogSubcellularLocalization() {
        final UniqueList l=StrapPlugins.allClassesV(PredictSubcellularLocation.class);
        classList=new ChJList(l,ChJTable.CLASS_RENDERER);
        classList.setSelII(new int[]{0,1,2,3,4,5,6,7,8,9});
        comboOrgansims=new JComboBox(Prediction.getOrganisms());
        final JComponent
            sp=scrllpn(classList).addMenuItem(smallHelpBut(l.toArray())).addMenuItem(pnl(VB,"Right-click for context-menu")),
            pCenter=pnl(new GridLayout(1,2),sp, scrllpn(proteinList=new ProteinList(0L))),
            pButtons=pnl(comboOrgansims," ",new ChButton("GO").t(ChButton.GO).li(this)),
            pMain=pnl(CNSEW,pCenter,dialogHead(this),pButtons);
        adMainTab(remainSpcS(pMain), this,null);
        LOG.tools().underlineRefs(0);
    }

    public void process(Protein[] pp) {
        final Object cc[]=classList.getSelectedValues();
        final Object[][] tableData=new Object[pp.length][];
        for(int iP=pp.length;--iP>=0;) {
            tableData[iP]=new Object[cc.length+1];
            tableData[iP][0]=pp[iP];
            for(int iC=cc.length; --iC>=0;) {
                tableData[iP][iC+1]=iicon(IC_HOURGLASS);
            }
        }
        final String colNames[]=new String[cc.length+1];
        for(int i=cc.length; --i>=0;) colNames[i+1]=delPfx("SubcellularLocation",shrtClasNam(cc[i]));
        colNames[0]="protein";
        final ChTableModel tm=new ChTableModel(0L, colNames).setData(tableData);
        final JComponent
            jTable=new ChJTable(tm,ChJTable.ICON_ROW_HEIGHT|ChJTable.DEFAULT_RENDERER),
            butLog=ChButton.doView(LOG).t("Log"),
            pnl=pnl(CNSEW,scrllpnT(jTable),null,pnl(butLog, new ChButton("Debug").li(this)));
        adTab(CLOSE_CtrlW, toStrg(++_countTab),pnl,  this);
        for(int iC=0; iC<cc.length; iC++) {
            startThrd(thrdCR(this,"GO",new Object[]{tableData,pp,cc[iC],intObjct(iC),jTable,pnl}));
        }
    }

    private int _countTab;
    private PredictSubcellularLocation lastPredictor;
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="GO") {
            final Protein pp[]=proteinList.selectedOrAllProteins();
            process(pp);
        }
        if (cmd=="Debug") {
            final String scn=shrtClasNam(lastPredictor);
            if (scn==null) error("Error: No prediction performed!\nThis button shows the control-panel of the last prediction process.\n");
            else if (!(isAssignblFrm(HasControlPanel.class, lastPredictor))) error("Error: "+scn+" does not have a control panel.\n");
            else shwCtrlPnl(0, scn, lastPredictor);
        }
    }
    public Object run(String id, Object arg) {
        final Object[] argv= arg instanceof Object[] ? (Object[])arg:null;
        if (id=="GO") {
            final Object[][] td=(Object[][])argv[0];
            final Protein pp[]=(Protein[])argv[1];
            final int iC=atoi(argv[3]);
            final JTable jTable=(JTable)argv[4];
            final JComponent tab=(JComponent)argv[4];
            final BA sbF=new BA(33), sbP=new BA(33);
            for(int iP=0; iP<pp.length && !isDisposed() && SwingUtilities.getRootPane(tab)!=null;  iP++) {
                sbF.clr();
                sbP.clr();
                final byte[] rt=pp[iP].getResidueTypeExactLength();
                final PredictSubcellularLocation predictor=mkInstance(argv[2],PredictSubcellularLocation.class,false);
                if (predictor==null) continue;
                lastPredictor=predictor;

                predictor.setResidueType(rt);
                predictor.setOrganism(comboOrgansims.getSelectedIndex());
                sbF.a(STRAPTMP).a("/where/").a(niceShrtClassNam(predictor)).a('_');
                CacheResult.sectionForSequence(rt,sbF);
                final PredictSubcellularLocation.Prediction[] predictions=predictor.getCompartments();

                if (predictions==PredictSubcellularLocation.ERROR || predictions==null) td[iP][iC+1]=iicon(IC_UNHAPPY);
                else {
                    for(PredictSubcellularLocation.Prediction pred : predictions) {

                        sbP.a( get(pred.getCompartment(),  finalStaticInts(PredictSubcellularLocation.class, PredictSubcellularLocation.MAX_VARIABLE)));
                        if (pred.getScore()!=Integer.MIN_VALUE) sbP.a('(').a(pred.getScore()).a(')');
                        sbP.a(' ');
                    }
                    final String appnd=pp[iP]+" "+sbP+"\n";

                    if ( strstr(appnd,toBA(LOG))<0) LOG.a(appnd);
                    td[iP][iC+1]=sbP.length()==0 ? "unknown" : sbP.toString();
                }
                ChDelay.repaint(jTable,333);
            }
        }
        return null;
    }
    public void handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        if ( (t&StrapEvent.FLAG_ROW_HEADER_CHANGED)!=0) ChDelay.repaint(this,333);
    }
}
