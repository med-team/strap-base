package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.ProteinViewer.*;
/**HELP

   Annotations can be added to residue selections.

   <i>PACKAGE:charite.christo.protein.</i>

   The type of entry such as
   <i>BUTTON:"Remark"</i> or <i>CLASSICON_CLASSNAME:Texshade</i>
   is chosen. Then free text or Texshade code or 3D-commands is entered.

   <i>SEE_CLASS:ResidueAnnotation</i>
   <i>SEE_CLASS:ResidueAnnotationView</i>
   <i>SEE_CLASS:ActivateDeactivate</i>
   <i>SEE_DIALOG:DialogResidueAnnotationChanges</i>

   @author Christoph Gille
*/
public final class AddAnnotation extends JPanel implements ActionListener, ListCellRenderer {
    public final JComponent
        _cbVar=toggl(ResidueAnnotationView.BUT_LAB_VARIABLES).li(this).cb(),
        _cbSame,
        _butIns=new ChButton(ChButton.DISABLED,"Insert").li(this).tt("Add this annotation");
    private static ChCombo _choiceV3D;
    private static Customize _custKeys;
    private final static Map<String,Customize> _examples=new HashMap();
    private final ChButton LABEL=labl();
    private final ChTextField _jtf=new ChTextField().li(this);
    private final Object _refAA[];
    private final ChJList _jlValues, _comboKeys;
    private final List _vValues=new Vector();
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    public AddAnnotation(ResidueAnnotation... ss) {
        _refAA=new Object[sze(ss)];
        _cbSame=toggl("Allow identical entries to occur several times").s(sze(ss)==1).cb();
        for(int i=sze(ss); --i>=0;) _refAA[i]=wref(ss[i]);
        _comboKeys=new ChJList(new ComboKeys(false),ChJTable.CLASS_RENDERER|ChJTable.ICON_ROW_HEIGHT|ChJTable.SINGLE_SELECTION).li(this);
        pcp(KEY_IF_EMPTY, "Annotation text to be added", _jtf);
        (_jlValues=new ChJList(_vValues,ChJTable.SINGLE_SELECTION)).li(this).setCellRenderer(this);
        final Object
            msg=pnl(HB, plrl(ss.length,
                             "This dialog acts on %N residue selection%S,<br>"+
                             "First select an annotation type at the left.<br>"+
                             "Then select an example text line to be added to the residue selection%S.<br>"+
                             "Finally press the insert-button.")),

            pTop=pnl(CNSEW, "<h2>Add Annotations</h2>", null,null,  panHS(this), new ChButton(IC_CUSTOM).li(this).rover(IC_CUSTOM)),
            pAdvanced=pnl(VB, _cbVar, _cbSame),
            pTF=pnl(CNSEW, _jtf, null, null, _butIns),
            pNorth=pnl(VB, pTop, pnlTogglOpts("Explain",msg), msg),
            pSouth=pnl(VB,pnlTogglOpts(pAdvanced), pAdvanced, pTF),
            scrollValues=scrllpn(_jlValues).addMenuItem(new ChButton("ED").t("Edit this list").li(this));
        pnl(this,CNSEW,scrollValues,pNorth,pSouth,null,_comboKeys);
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Keys/Values >>> */
    public static Customize customizeKeys() {
        if (_custKeys==null) _custKeys=new Customize("keys",new String[]{"Remark","Note",ResidueAnnotation.BALLOON},"Here you can define new keys for your set entries.",Customize.LIST);
        return _custKeys;
    }

    /* <<< Keys/Values <<< */
    /* ---------------------------------------- */
    /* >>> Rendering >>> */
    public java.awt.Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        CharSequence str=toStrg(value);
        if (isSelctd(_cbVar)) {
            for(Object r:_refAA) {
                final ResidueAnnotation a=(ResidueAnnotation)deref(r);
                if (a!=null) {
                    str=ResSelUtils.replaceVariable(str,a);
                    break;
                }
            }
        }
        return LABEL.t(str).fg(isSelected ? 0xFF : 0);
    }
    /* <<< Rendering  <<< */
    /* ---------------------------------------- */
    /* >>> Insert  >>> */
    public static String toKey(Object c) {
        Object k=c instanceof ChJList ? ((ChJList)c).getSelectedValue() : c instanceof ChCombo ? ((ChCombo)c).getSelectedItem() : null;
        k=k==ResidueAnnotation.SPECIFIC_VIEW3D && _choiceV3D!=null ? _choiceV3D.getSelectedItem() : k;
        return k instanceof Class ? nam(k) : toStrg(k);
    }

    private void insertThis(Object value) {
        if (sze(toStrg(value))==0) { StrapAlign.errorMsg("Enter a text into the text field"); return;}
        final String key=toKey(_comboKeys);
        if (key==null) { StrapAlign.errorMsg("You must select a key"); return;}
        nextA:
        for(ResidueAnnotation a: derefArray(_refAA, ResidueAnnotation.class)) {
            if (!isSelctd(_cbSame)) {
                for(ResidueAnnotation.Entry e : a.entries()) {
                    if (key.equals(e.key()) && value.equals(e.value())) continue nextA;
                }
            }
            a.addE(ResidueAnnotation.E_EDITED|ResidueAnnotation.E_NOT_UNIQUE, key, toStrg(value));
            if (key==ResidueAnnotation.STYLE) StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_CHANGED);
        }
        ResidueAnnotationView.changed(false);
    }
    /* <<< Insert <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private String _v;
    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource(), key0=_comboKeys.getSelectedValue();
        if (q==_jlValues) {
            if (cmd==ACTION_CLICKED_AFTER_SUPER) {
                final String v=toStrg(q);
                if (sze(v)>0){
                    if (eqNz(_v,v)) insertThis(v);
                    else {
                        _jtf.t(v);
                        setBG(0xFF00,_butIns);
                        setEnabld(true, _butIns);
                        ChDelay.fgBg(true,_butIns,  null ,2222);
                        ChDelay.revalidate(_butIns,99);
                    }
                }
                _v=v;
            }
        }
        if (cmd==IC_CUSTOM) {
            Customize.addDialog(customizeKeys());
            final ComboKeys c=new ComboKeys(false);
            for(int i=c.getSize(); --i>=0;) Customize.addDialog(examples(c.getElementAt(i)));
        }
        if (q==_butIns) insertThis(toStrg(_jtf));
        if (cmd=="ED") Customize.addDialog(examples(toKey(_comboKeys)));
        if ( (q==_jtf || q==_comboKeys) && key0!=null) {
            setEnabld(nxt(-SPC, toBA(_jtf))>=0, _butIns);
            if (cmd==ACTION_ENTER) _butIns.requestFocus();
        }
        if (q==_comboKeys && cmd==ACTION_CLICKED_AFTER_SUPER) {
            _vValues.clear();
            final Object key;
            if (key0==ResidueAnnotation.SPECIFIC_VIEW3D) {
                if (_choiceV3D==null) {
                    _choiceV3D=SPUtils.classChoice(ProteinViewer.class);
                    final Object pnl=pnl(VBHB,"Please choose a specific 3D-viewer to add an entry for",
                                        _choiceV3D,
                                        "Note: it is recommended, not to use specific 3D-viewer commands",
                                        "If possible, use generic 3D-viewer commands which are valid for any viewer implementation.",
                                        "These are stored in entries of the type \""+ResidueAnnotation.VIEW3D+"\".");
                    pcp("pnl",pnl,_choiceV3D);
                }
                key=ChMsg.yesNo(gcp("pnl",_choiceV3D)) ? _choiceV3D.getSelectedItem() : null;
            } else key=key0;
            if (key==ResidueAnnotation.BG_IMAGE) _vValues.add(0,URL_STRAP+"images/windows.png");
            else if (key==ResidueAnnotation.STYLE) {
                for(String s : ResSelUtils.styles()) _vValues.add(0,s);
            } else {
                final Customize c=examples(key);
                if (c!=null) {
                    for(String s:c.getSettings()) _vValues.add(0,s);
                    setEnabld(true, _cbVar);
                }
            }
            repaint();
        }
        if (q==_cbVar) repaint();
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Examples_ >>> */
    public static Customize examples(Object key) {
        Customize cust=null;
        final Class c=key instanceof Class ? (Class)key : findClas(toStrg(key),STRAP_PACKAGES);
        if (c!=null && c!=String.class) cust=Customize.getCustomizableForClass(c, Customize.CLASS_Example);
        else {
            final String k=toStrgIntrn(key);
            cust=_examples.get(k);
            if (cust==null) {
                final String
                    examples3D=COMMANDspheres+"\n"+COMMANDribbons+"\n"+COMMANDsticks+"\n"+COMMANDlines+"\n"+COMMANDsurface+"\n"+COMMANDcolor+" #FF0000\n"+COMMANDcolor+" #FF00AA\n",
                    lines=
                    "Note"==k ?  "Experimantal confirmation awaited\n"+"Is a cleaved\n"+
                    "Is modified\n"+"Is important for catalysis\n"+"PUBMED:10547838 PDB:1RYP EMBL:X59417" :
                    "Remark"==k ? "Important note\n"+"Experimantal confirmation awaited" :
                    ResidueAnnotation.BALLOON==k ? "A Balloon text that appears when the mouse is over" :
                    ResidueAnnotation.ATOMS==k ? ".CA\n.CA.CB\n.C*\n.S*.N*" :
                    ResidueAnnotation.VIEW3D==k ? examples3D :
                    "Some text";
                cust=new Customize("Examples_"+k,splitStrg(lines,'\n') , "Examples", Customize.LIST);
                _examples.put(k,cust);
            }
        }
        return cust;
    }
    /* <<< Examples <<< */
    /* ---------------------------------------- */
    /* >>> Html  >>> */
    public static Object docuWithPymol() {
        final AddAnnotation al=new AddAnnotation(new ResidueAnnotation[0]);
        final Object model=al._comboKeys.getModel();
        for(int i=sze(model); --i>=0;) {
            if ("Texshade".equals(get(i,model))) al._comboKeys.setSelI(i);
        }
        adAll(splitLnes(Texshade.getCustomizableExamples()) , al._vValues);
        return al;
    }
}
