package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   Makes TeXshade commands to display the predicted structure.
   Text lines starting with a percent sign are ignored since "%" preceds comments in WIKI:LaTeX.
   <br><br>
   The line valid for helices is preceded by "Hh" and the line valid for extended (i.e. beta sheet) is preceded by "Ee".

   @author Christoph Gille
*/
public class DialogPredictTexshade implements java.awt.event.ActionListener {
    private final Protein _proteins[];
    private final char _prediction[][];
    private final String _name;
    private ChTextArea _ta;
    private JComponent _panel;
    private Object _tog;
    private final static List _vInst=new ArrayList();

    public DialogPredictTexshade(char prediction[][], Protein pp[], String name) {
        _proteins=pp;
        _name=name;
        _prediction=prediction;
        rmNull(_vInst);
        _vInst.add(wref(this));
    }
    public JComponent panel() {
        if (!isEDT()) inEDT(thrdM("panel",this));
        else {
            _ta=new ChTextArea(5,80);
            final Object
                //butTex=new ChButton("TEX").t("Test PDF for one protein").li(this).i(IC_PDF).tt("For the first protein the PDF will be generated and displayed"),
                butTexshade=StrapAlign.b(CLASS_Texshade).t("Open PDF Dialog"),
                panDetails=pnl(butTexshade,smallHelpBut(this)),
                pnl=pnl(CNSEW,(_ta), panDetails,null,null,"#TB TeXshade -- generating PDF -- only the first selected protein");
            setDefaultText();
            _tog=toggl("Add to PDF ").doCollapse(pnl).cb();
            _panel=pnl(CNSEW,null, pnl(HBL,_tog),pnl);
        }
        return _panel;
    }
    private void setDefaultText() {
        final String
            S0="\\shaderegion{$PROTEIN_NO}{$TEXSHADE_RESIDUES}{",
            T=
            "\\allmatchresidues{Black}{White}{upper}{}\n"+
            "\\nomatchresidues{Black}{White}{upper}{}\n"+
            "\\conservedresidues{Black}{White}{upper}{}\n"+
            "\\similarresidues{Black}{White}{upper}{}\n"+
            "\\shadingmode{none}\n"+
            "Hh "+S0+"White}{Red}\n"+
            "Ee "+S0+"Black}{Yellow}\n"+
            "%X  \\feature{bbottom}{$TEX_PROTEIN}{$TEXSHADE_RESIDUES}{fill:X}{}\n";
        _ta.t(T);
    }
    private void tex(BA sb, Protein pp[], int iTab) {
        if (_prediction==null) return;
        int maxLen=0;
        for(char[] cc : _prediction) if (maxLen<sze(cc)) maxLen=cc.length;
        final BA ba=toBA(_ta).a('\n');
        final byte T[]=ba.bytes();
        final int ends[]=ba.eol();
        final boolean[] selA=new boolean[maxLen];
        sb.a("%   ").aln(_name);
        for(int iP=-1; iP<_proteins.length; iP++) {
            if (iP>=0 && !cntains(_proteins[iP],pp)) continue;
            for(int iL=0; iL<ends.length; iL++) {
                final int E=ends[iL], B=nxtE(-SPC, T, iL==0?0:ends[iL-1], E);
                if (E-B<4 || T[B]=='%') continue;
                if (strstr(STRSTR_w_R,"$PROTEIN_NO",T,B,E)>0) {
                    final int spc=nxt(SPC,T,B,E);
                    if (iP<0 || spc<0) continue;
                    for(int iA=selA.length; --iA>=0;) selA[iA]= iA<_prediction[iP].length && strchr(_prediction[iP][iA], T,B,spc)>=0;
                    if (countTrue(selA)>0) sb.a(ResSelUtils.replaceVariableBB(ba.newString(spc,E+1), selA, Protein.firstResIdx(_proteins[iP]), _name, _proteins[iP], pp));
                } else  if (iP==0) {
                    final String ins=ba.newString(B,E+1); /* INEFFICIENT */
                    if (iTab==0 || strstr(ins,sb)<0) sb.a(ins);
                }
            }
        }
    }
    public static void getTexts(BA sb, Protein proteins[]) {
        for(int i=0; i<sze(_vInst); i++) {
            final DialogPredictTexshade d=getRmNull(i,_vInst,DialogPredictTexshade.class);
            if (isSelctd(d)) d.tex(sb,proteins,i);
        }
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="TEX")  {
            final BA sb=new BA(999);
            Protein p=_proteins[0];
            for(int i=_proteins.length; --i>=0;) {
                if (_prediction[i]==null) continue;
                for(char c:_prediction[i]) {
                    if (c!=' ' && c!=0) p=_proteins[i];
                }
            }
            tex(sb, new Protein[]{p}, 0);
            ResSelUtils.texshade(sb,p);
        }
        if (cmd=="RST" && (sze(_ta.toString())==0 || ChMsg.yesNo("Really discard text and set the default text?"))) setDefaultText();
    }
}
