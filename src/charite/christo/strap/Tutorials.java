package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.net.URL;
import javax.swing.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
public class Tutorials implements ChRunnable, ProcessEv {
    private Tutorials(){}
    private static Tutorials _inst;
    private static Object _oMouse;
    private static Tutorials instance() {
        if (_inst==null) {
            _inst=new Tutorials();
            HtmlDoc.setCreateComponentHook(_inst);
            HtmlDoc.setButtonForClass(_inst);
        }
        return _inst;
    }

    private static Object li(Object o) {
        if (o instanceof AbstractButton) evAdapt(instance()).addTo("am",o);
        if (o instanceof Icon) o= new JLabel(((Icon)o));
        return o;
    }

    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final int id=ev.getID();
        final String cmd=actionCommand(ev);
        if (id==MOUSE_ENTERED) _oMouse=q;
        if (id==MOUSE_EXITED) _oMouse=null;
        if (cmd!=null) {
            StrapAlign.errorMsg("");
            if (cmd=="COPY_PROTEINS") {
                final ArrayList<URL> v=new ArrayList(),vGif=new ArrayList();
                final BA sbProteinNames=new BA(99);
                for(String t : splitTokns(gcps("COPY_PROTEINS",q))) {
                    if (t.endsWith(".gif")) vGif.add(url(URL_STRAP_DATAFILES+t));
                    else {
                        v.add(url(URL_STRAP_DATAFILES+t+".gz"));
                        sbProteinNames.a(t).a(' ');
                    }
                }
                InteractiveDownload.downloadFilesAndUnzip(toArry(v,URL.class),dirWorking());
                InteractiveDownload.downloadFilesAndUnzip(toArry(vGif, URL.class),  Protein.strapFile(Protein.mANNO,null,null));
                StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED, sbProteinNames);
            }

            if (q==_oMouse && StrapAlign.button(StrapAlign.TOG_TUTORIAL_SIMUL_MENUS).s() && 0==(Protein.optionsG()&Protein.G_OPT_VIEWER)) openMenus(getEquivalentMenuButton(q,null));
            openTutorial(cmd);
        }
    }
    static void openTutorial(String tutorial) {
        final String key="Tutorial_"+tutorial, tab=orS(dTab(key),""), title=dTitle(key);
        if (title==null) return;
        final BA sb=readBytes(rscAsStream(0L,_CCS, key+".html"));
        if (sze(sb)<6) {
            if (ChMsg.yesNo("Tutorials are not included in this Web-version<br>Launch full Strap-application?:")) visitURL(URL_STRAP_JNLP, 0);
            return;
        }
        for (String p : STRAP_PACKAGES) sb.a("<i>PACKAGE:").a(p).aln("</i>");
        if (!strEquls("<html>", sb,0)) sb.insert(0,"<h1>"+title+"</h1>");
        final ChJTextPane tp=new ChJTextPane(sb);
        StrapAlign.newDropTarget(tp,false);
        pcp(ACTION_DATABASE_CLICKED,"Load", tp);
        updateOn(CHANGED_SELECTED_OBJECTS,tp);
        TabItemTipIcon.set(tab, title, "" , IC_TUTORIAL,  tp);
        pcp(ChTabPane.KEY_UNIQUE_ID,_CCS+tab, scrllpn(SCRLLPN_TOP, tp));
        pcp(ChTabPane.KEY_SORT,"0T", scrllpn(tp));
        StrapAlign.addDialog(scrllpn(tp));

    }

    private static void openMenus(AbstractButton mi) {
        if (mi==null) return;
        boolean inMenubar=false;
        final Component aa[]=ancestrs(mi);
        for(Component c:aa) if (c instanceof JMenuBar) { inMenubar=true; break;}
        ChDelay.highlightButton(mi,9999);
        for(Component c : aa) {
            if (!inMenubar && c instanceof JPopupMenu) {
                ((JPopupMenu)c).show(StrapAlign.frame(),7,7);
                break;
            }
            ChDelay.highlightButton(c, 9999);
            final JPopupMenu popup=c instanceof JMenu ? ((JMenu)c).getPopupMenu() : null;
            if (popup!=null) {
                popup.show(c, prefW(c),prefH(c)/2);
                vPopupCloseOnClick.add(popup);
            }
        }
    }
    public static ChButton b(String tutorial) {
        final String key="Tutorial_"+tutorial;
        final ChButton b=new ChButton(tutorial)
            .t(dItem(key))
            .li(evAdapt(instance()));
        return b;
    }

    public static JComponent bExampleFiles(String id) {
        final String
            PROTEA_3D=
            "hs_EscherichiaColi.pdb a1_SaccharomycesCerevisiae.pdb  b1_SaccharomycesCerevisiae.pdb "+
            "hs_EscherichiaColi.pdb.gif ",
            PROTEA_HS_A2="hs_BacillusSubtilis.swiss hs_EscherichiaColi.pdb "+
            "hs_HelicobacterPylori.swiss hs_SalmonellaTyphi.swiss a2_ArabidopsisThaliana.swiss "+
            "a2_CaenorhabditisElegans.swiss a2_CarassiusAuratus.swiss a2_DrosophilaMelanogaster.swiss "+
            "a2_HomoSapiens.swiss a2_MusMusculus.swiss a2_RattusNorvegicus.swiss "+
            "a2_XenopusLaevis.swiss ",
            t="Get sample files",
            l=
            "MUT".equals(id) ? "1alm_2mys.ent  " :
            "3".equals(id) ? PROTEA_3D :
            "13".equals(id) ? PROTEA_3D+PROTEA_HS_A2 :
            "AA".equals(id) ? PROTEA_HS_A2 :
            null;

        if (l!=null) return new ChButton("COPY_PROTEINS").t(t).li(evAdapt(instance())).cp("COPY_PROTEINS",l);
        else { assrt(); return new JLabel(id); }
    }
    public Object run(String id,Object arg) {
        if (id==HtmlDoc.ID_LAST_COMPONENT) {
            final JComponent last=(JComponent)arg;
            noBrdr(last);
            if (last instanceof AbstractButton) li((AbstractButton)last);
        }
        if (id==HtmlDoc.ID_BUTTON_FOR_CLASS) {
            final Object oo[]=(Object[])arg;
            final Class c=(Class)oo[0];
            if (c==null) oo[0]=new ChButton("ERROR ");
            else {
                final String cn=c.getName();
                li(oo[0]=StrapAlign.b(CLASS_DialogCopyAnnotations.equals(cn) ? cn : c));
            }
        }
        return null;
    }

}
