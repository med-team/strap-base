package charite.christo.strap;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class AcceptAlignment2 {

    private final Row _rows[];

    private final int[] _countInserts, _startIdx;

    public static int eliminateCommonGaps(Protein pp[], int startCol, int endCol, byte[][] alignment, int wide2narrow[]) {
       int c1=startCol, c2=endCol;
       if (alignment!=null) {
           c1=MAX_INT;
           c2=0;
           for(int r=0; r<pp.length; r++) {
               c1=mini(c1, startCol);
               c2=maxi(c2, startCol+countLettrs(alignment[r]));
           }
       }
       final int c2W=idxOf(c2, wide2narrow);
       final int c1W=maxi(idxOf(c1, wide2narrow),0);
       final int deleted=Gaps2Columns.eliminateCommonGaps(pp, c1W,  c2W>c1W?c2W:MAX_INT);
       return deleted;
   }

    public static void delGaps(Protein pp[], int startCol, int endCol, byte[][] alignment) {
        final Object KEY=new Object();
        for(int iP=pp.length; --iP>=0;) {
            final Protein p=pp[iP];
            final int iA1=p.column2nextIndexZ(startCol);
            if (iA1<0) continue;
            final int iA2=alignment!=null?iA1+countLettrs(alignment[iP]) :  p.column2nextIndexZ(endCol);
            //putln("AcceptAlignment2 delGaps "+p+" iA1="+iA1+" iA2="+iA2+"   ",   alignment!=null?alignment[iP] : "null"  );
            pcp(KEY, new int[]{iA1,iA2>=0?iA2:p.countResidues()},p);
        }
        for(Protein p:pp) {
            final int ii[]=gcp(KEY, p, int[].class), iA1=get(0,ii), iA2=get(1,ii);
            //putln("AcceptAlignment2 delGaps "+p+" iA1="+iA1+" iA2="+iA2);
            int count=0;
            for(int iA=iA1+1; iA<iA2; iA++) {
                count+=p.getResidueGap(iA);
                p.setResidueGap(iA, 0);
            }
            if (count>0) p.addResidueGap(iA2,count);
        }
        for(Protein p:pp) pcp(KEY, null, p);
    }

    public AcceptAlignment2(Protein pp[], int startCol, byte[][] alignment) {
        delGaps(pp,startCol, MAX_INT, alignment);
        final int nP=sze(pp);
        _rows=new Row[nP];
        _startIdx=new int[nP];
        _countInserts=new int[nP];

        if (alignment==null) return;
        for(int r=0; r<nP; r++) {
            final byte gg[]=gappedSequenceWide(pp[r]);
            final int col=idxOfLetters(alignment[r], gg, startCol, MAX_INT);
            _startIdx[r]=countLettrs(gg,0, col);
            Protein[] ppRow=SPUtils.getProteinsSameRow(pp[r]).clone();
            if (ppRow.length==0) ppRow=new Protein[]{pp[r]};
            for(int i=ppRow.length; --i>=0;) ppRow[i]=newP(ppRow[i]);
            _rows[r]=new Row(newP(pp[r]), ppRow,alignment[r]);

        }
        accept();
        for (Row r : _rows) {
            for(Protein p : r._all) {
                gcp(KEYorig,p,Protein.class).setResidueGap(p.getResidueGap());
            }
        }

        StrapAlign.setNotSaved();
    }

    static byte[] gappedSequenceWide(Protein p) {
        final int gg[]=p.getResidueGap(), nR=p.countResidues();
        final byte aa[]=p.getResidueType(), gapped[];
        int col=nR;
        for(int i=nR; --i>=0;) col+=gg.length>i?gg[i]:0;
        gapped=new byte[col];
        java.util.Arrays.fill(gapped,(byte)0);
        col=0;
        for(int i=0; i<nR; i++) {
            col+=gg.length>i?gg[i]:0;
            gapped[col++]=aa[i];
        }
        return gapped;
    }

    public int[] getCountInserts() { return _countInserts;}

    private void accept() {
        final Row[] stacks=_rows;
        final byte[][] aa=new byte[stacks.length][], ali1d=new byte[stacks.length][];
        for(int r=0; r<stacks.length; r++)  {
            aa[r]=stacks[r]._gapped;
            ali1d[r]=stacks[r]._top.getGappedSequenceExactLength();
        }
        final MergeAlignments merge=new MergeAlignments(_startIdx, aa, ali1d);
        final int inserts[][]=merge.getInsertsAtCol();
        final Object KEY=new Object();
        /*
        for(int r=0; r<stacks.length; r++) {
            for(Protein p : stacks[r].all) {
                addGapAtColumn(r, inserts[r], p.getResidueGap(),  p.columns2nextIndices(),p.getMaxColumnZ());
            }
        }
        */

        for(int r=0; r<stacks.length; r++) {
            for(Protein p : stacks[r]._all) {
                final int gg[]=p.getResidueGap().clone();
                pcp(KEY,gg,p);
                addGapAtColumn(r, inserts[r], gg,  p.columns2nextIndices(),p.getMaxColumnZ());
            }
        }
        for(int r=0; r<stacks.length; r++) {
            for(Protein p : stacks[r]._all) {
                p.setResidueGap(gcp(KEY,p, int[].class));
                pcp(KEY,null,p);
            }
        }

    }
    private void addGapAtColumn(final int row, int insert[], int resGap[], int column2nextIndex[],int maxCol) {
        for(int iCol=mini(mini(maxCol,column2nextIndex.length-1),insert.length);--iCol>=0;) {
            final int ias=column2nextIndex[iCol];
            if (ias>=resGap.length || insert[iCol]==0) continue;
            resGap[ias]+=insert[iCol];
            _countInserts[row]+=insert[iCol];
            //lastChangedA[row]=maxi(ias,lastChangedA[row]);
        }
    }

    private final static Object KEYorig=new Object();
    private static Protein newP(Protein p) {
        final Protein neu=new Protein();
        //neu.setName("x"+p.getName());
        neu.setResidueType(p.getResidueTypeExactLength());
        neu.setResidueGap(p.getResidueGap());
        pcp(KEYorig,p,neu);
        return neu;
    }

    /* --- Instance --- */
    private static class Row {
        final Protein _top, _all[];
        final byte _gapped[];
        public Row(Protein top,Protein[] all, byte[] gapped) {
            _top=top;
            _all=all;
            _gapped=gapped;
        }
    }

}

