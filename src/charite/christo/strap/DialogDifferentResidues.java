package charite.christo.strap;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import charite.christo.protein.*;
import charite.christo.*;
import javax.swing.*;

/**HELP
   Based on the current sequence alignment two aligned proteins are
   compared residue by residue and all different residues are collected
   in a table.

   The table can be used in different ways:

   <ol>
   <li>
   Homology modeling: Some protein viewers such as the Swiss-viewer
   (http://www.expasy.org/spdbv/  can substitute residues in a
   3D-structure.

   With this dialog the commands to replace
   the residues of the 3D-structure by the residues of a raw
   sequence can be generated.
   Alignment gaps are not considered.

   <br>
   <b>Customization: </b> These commands can be modified  using the customize dialog.

   The contents of the n-th column is referred to by backslash n.
   </li>
   <li>
   Differing positions may be used to generate residue annotations.
   </li>
   </ol>

   <i>SEE_CLASS:ResidueAnnotation</i>
   <i>SEE_CLASS:ResidueAnnotationView</i>
   @author Christoph Gille

*/
public class DialogDifferentResidues extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener  {
    private JComboBox _comboReplace;
    public final static String SWISS_VIEWER="SwissViewer";
    private final ProteinCombo _choiceProt3D, _choiceProt;

    public DialogDifferentResidues() {
        setSettings(this, replaceResiduesLine());
        final Object pCenter=pnl(VBPNL,
                                      pnl("#TB Select target protein (e.g. a pdb-file)",_choiceProt3D=new ProteinCombo(0)),
                                      pnl("#TB Select source protein e.g. a sequence file)",_choiceProt=new ProteinCombo(0)),
                                      new ChButton("GO").li(this).t(ChButton.GO)
                                      ),
            pMain=pnl(CNSEW,pCenter,dialogHead(this));
        if (!isMac()) setTabPlacement(LEFT);
        adMainTab(pMain,this,null);
    }
    private static Customize _rplc;
    private static Customize replaceResiduesLine() {
        if (_rplc==null) {
            _rplc=new Customize("replaceResiduesLine",new String[] {
                                                  SWISS_VIEWER+" $X = select in 0 num \\10; mutate $X to \"\\05\";",
                                                  SWISS_VIEWER+" $X = select in 0 pos \\02+1; mutate $X to \"\\05\";",
                                                  "Pymol select strapMut, ( ///\\12/\\10 ); mutate PROTEIN, strapMut, \\07; delete strapMut;"
                                              },
                                              "Generate a text from the table rows.\nThe contents of the table cells are addressed by backslash followed by to digits indicating the row  index",Customize.LIST);
        }
        return _rplc;
    }
    // ----------------------------------------
    private static String formatChain(byte b) {final char c=(char)b; return is(LETTR_DIGT,c) ? toStrg(c) : ""; }
    private class Result extends JPanel implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent ev) {
            final String cmd=ev.getActionCommand();
            if (cmd=="SAVE") {
                final java.io.File f=file(STRAPOUT+"/replaceResidues");
                wrte(f, toBA(_tpReplace));
                edFile(-1,f,modifrs(ev));
            }
            if (cmd=="RPLC") replacementCommands();
            if (cmd=="SEL") {
                final int ii[]=_table.getRowCount()==1 ? new int[]{0} : ChJTable.selectedRowsConverted(_table);
                if (ii.length==0) { StrapAlign.errorMsg("Error: No table rows selected");return;}
                final ResidueAnnotation sors[]=new ResidueAnnotation[ii.length];
                final String group="compared to "+_pSeq;
                for(int i=0;i<ii.length;i++) {
                    final int row=ii[i];
                    final ResidueAnnotation s=sors[i]=new ResidueAnnotation(_pPdb);
                    s.addE(0,ResidueAnnotation.NAME,_table.getValueAt(row,8)+"=>"+_table.getValueAt(row,7));
                    s.addE(0,ResidueAnnotation.GROUP,group);
                    s.addE(0,ResidueAnnotation.POS, toStrg(atoi(_table.getValueAt(row,2))+1));
                    s.addE(0,ResidueAnnotation.COLOR,"255,255,128");
                    _pPdb.addResidueSelection(s);
                }
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED,111);
            }
        }
        private final ChJTable _table;
        private final Protein _pSeq, _pPdb;
        private ChTextArea _tpReplace;
        public Result(Protein pSeq, Protein pPdb) {
            _pSeq=pSeq; _pPdb=pPdb;
            final int n=pSeq.countResidues();
            Object[][] data=null;
            while(true){
                int iRow=0;
                for(int iA=0,count=0;iA<n;iA++) {
                    final int
                        posFrom=pSeq.getResidueColumnZ(iA),
                        iATo=pPdb.column2nextIndexZ(posFrom),
                        posTo=pPdb.getResidueColumnZ(iATo);
                    if (posFrom<0 || posTo<0) continue;
                    if (iATo>=0 && iA<=pSeq.countResidues() && posFrom==posTo && pSeq.getResidueType(iA)!=pPdb.getResidueType(iATo)) {
                        final char asFrom=(char)pSeq.getResidueType(iA);
                        final char asTo=(char)pPdb.getResidueType(iATo);
                        final int rn=pSeq.getResidueNumber(iA), rnTo=pPdb.getResidueNumber(iATo);
                        final Object[] row={
                            intObjct(count++),
                            intObjct(iA),
                            intObjct(iATo),
                            intObjct(posFrom),
                            intObjct(posTo),
                            toStrg(asFrom),
                            toStrg(asTo),
                            Protein.toThreeLetterCode(asFrom),
                            Protein.toThreeLetterCode(asTo),
                            rn==MIN_INT?"NaN":intObjct(rn),
                            rnTo==MIN_INT?"NaN":intObjct(rnTo),
                            formatChain(pSeq.getResidueChain(iA)),
                            formatChain(pPdb.getResidueChain(iATo))};
                        if (iRow<sze(data)) data[iRow]=row;
                        iRow++;
                    }
                }
                if (data==null) data=new Object[iRow][]; else break;
            }
            final String SP=" of the source protein", TP=" of the target protein",
                C1="One letter code ", C3="Three letter code ", RI="Residue index ", AC="Alignment column ", RN="residue number ", RC="residue chain ";
            _table=new ChJTable(new ChTableModel(0L, "Idx","Idx from","Idx to","Col from","Col to","AA from","AA to","AA from","AA to","Num from","Num to","Chain from","Chain to").setData(data),0)
                .headerTip("Index of the line", RI+SP, RI+TP, AC+SP, AC+TP, C1+SP, C1+TP, C3+SP, C3+TP, RN+SP, RN+TP, RC+SP, RC+TP);
            _comboReplace=replaceResiduesLine().newComboBox();

            final Object
                butSubst=new ChButton("RPLC").t("Generate replacement commands").li(this),
                butResidueSelection=new ChButton("SEL").t("Create residue selections").li(this).tt("create  annotations for protein "+pPdb+"<br>for each different position"),
                panSubst=ChUtils.pnl(VB,
                                    "#ETB",
                                    enlargeFont(_comboReplace,0.8),
                                     pnl(butSubst, " ", Customize.newButton(replaceResiduesLine()))
                                    ),
                panSor=pnl("#ETB",butResidueSelection,pPdb),
                pSouth=pnl(VB,panSubst,panSor),
                pPdbp=pnl(HB,smallHelpBut(DialogDifferentResidues.this),"#","Different Residues","#",new ChButton().doClose(CLOSE_RM_CHILDS, this));
            pnl(this,CNSEW,scrllpn(_table),pPdbp,pSouth);
        }

        public void replacementCommands() {
            final BA sb=new BA(999);
            int rows[]=ChJTable.selectedRowsConverted(_table);
            if (rows.length==0) rows=count01234(_table.getRowCount());
            final int nCol=_table.getColumnCount();
            final String line=(String)_comboReplace.getSelectedItem();
            final boolean isSwissViewer=line.startsWith(SWISS_VIEWER);
            if (isSwissViewer) sb.a("please do \nopen from \"disk\" \"").a(_pPdb.getFile()).aln("\";");
            for(int iRow=0;iRow<rows.length;iRow++) {
                if (line==null || line.indexOf(' ')<0) return;
                String l=rplcToStrg("\\\\","@",line.substring(line.indexOf(' ')));
                for(int iCol=0;iCol<nCol;iCol++) {
                    l= rplcToStrg("\\"+iCol/10+iCol%10, ""+_table.getValueAt(rows[iRow],iCol),l);
                }
                sb.aln(rplcToStrg("@","\\\\",l));
            }
            if (isSwissViewer) sb.a("\nthank you");
            _tpReplace=new ChTextArea(sb);

            final Object bSave=new ChButton("SAVE").t("save as file \"replaceResidues\"").li(this);
            new ChFrame("replaceResidues").ad(pnl(CNSEW,scrllpn(_tpReplace),null,pnl(bSave))).show();
        }
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Protein pSeq=_choiceProt.getProtein(),pPdb=_choiceProt3D.getProtein();
        if (pSeq==null || pPdb==null || pSeq==pPdb) {StrapAlign.errorMsg("Select two different proteins "); return;}
        else StrapAlign.errorMsg("");
        adTab(DISPOSE_CtrlW, pSeq+" "+pPdb, new Result(pSeq,pPdb), this);
    }

}
