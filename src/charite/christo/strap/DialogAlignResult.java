package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import javax.swing.*;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public final class DialogAlignResult extends ChPanel implements Runnable, Disposable, PaintHook, ProcessEv {
    public final static String BUT_LAB_Apply="Approve alignment";
    final static int USE_THREADS=1<<1;
    private final EvAdapter LI=new EvAdapter(this);
    private final DrawGappedSequence dgs=new DrawGappedSequence();
    private final JComponent
        _togSort=toggl("Apply order of aligned proteins").li(LI),
        _bApply=new ChButton(BUT_LAB_Apply).li(LI),
        _bUndo=new ChButton(ChButton.DISABLED|ChButton.HIDE_IF_DISABLED,"Undo").t("Undo").li(LI),
        _rowHeader=pnl(C(DEFAULT_BACKGROUND), KOPT_TRACKS_VIEWPORT_WIDTH),
        _pAli=pnl(C(DEFAULT_BACKGROUND));
    private final Thread _thread=new Thread(this);
    private final SequenceAligner _a;
    private final ChTableLayout _tLayout;
    private static Object _lastA;
    private Protein _pp[];
    private int _iiSorting[];
    private byte[][] _aligned;
    private float _rhAdjust;
    private final int _startIdx;
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    DialogAlignResult(int opt, SequenceAligner aligner,Protein pp[], int startIdx) {
        _a=aligner;
        _pp=pp;
        _startIdx=startIdx;
        LI.addTo("kmw",_pAli).addTo("w",_rowHeader);
        scrollByWheel(_pAli);
        final ChJScrollPane sp=scrllpn(SCRLLPN_INHERIT_SIZE, _pAli), spH=scrllpn(_rowHeader);

        (_tLayout=new ChTableLayout(ChJTable.NO_REORDER).addCol(0,spH).addCol(0,sp))
            .t().setW(24*EM, EM,2222, 0).setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        sp.setVerticalScrollBar(spH.getVerticalScrollBar());
        _thread.setPriority(Thread.MIN_PRIORITY);
        if ( (opt&USE_THREADS)!=0) startThrd(_thread); else _thread.run();
        final Object
            cbSort=isAssignblFrm(SequenceAlignerSorting.class,aligner)?((ChButton)_togSort).cb():null,
            pButtons=pnl(_bApply,_bUndo," ",cbSort, ChButton.doCtrl(aligner), REMAINING_VSPC1);
        pnl(this,CNSEW, _tLayout,null, pButtons);
        _tLayout.t().setColWidth(true,0, 10*EM);
        setDragScrolls('B',sp.getViewport(),sp);
        setDragScrolls('B',_pAli,sp);
        setPrefSize();
        addActLi(LI,aligner);
        addPaintHook(this,_rowHeader);
        addPaintHook(this,_pAli);
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> PaintHook >>> */
    private final static int _lineSkip=5;
    private final static Point _origin=new Point();
    private int _localAliStartsAt[];
    private short _freqAA[][];
    public boolean paintHook(JComponent jc,Graphics g, boolean after) {
        if (!after) return true;
        final Protein[] pp=_pp;
        if (pp==null) return true;
        final int W=jc.getWidth(), H=jc.getHeight();
        final boolean isSorting=isSelctd(_togSort) && sze(_iiSorting)>=pp.length;
        final Font font=font();
        g.setFont(font);
        final Rectangle cb=chrBnds(font), clip=clipBnds(g);
        final int clipY1=y(clip), clipY2=y2(clip), lineH=cb.height+_lineSkip;
        final byte[] aligned[]=_aligned;
        if (!_isReady) { paintProgress(g); return true; }
        if (jc==_rowHeader || jc==_pAli) {
            g.setColor(C(0));
            final int h=pp.length*lineH;
            fillBigRect(g, 0,0,W, h);
            g.setColor(C(DEFAULT_BACKGROUND));
            if (H>h) fillBigRect(g, 0,h,W, H-h);
            if (jc==_pAli) {
                if (msgShownPreview==1) {
                    g.setColor(C(0));
                    g.fillRect(0,0,999,999);
                    g.setColor(C(0xFFffFF));
                    final String[] msg={
                        "Note: This alignment preview is not editable.",
                        "By pressing the button ["+BUT_LAB_Apply+"]",
                        "The alignment from the preview is infered to the working alignment panel below.",
                        "",
                        StrapAlign.alignmentPanels().length>0 ? null: "Warning: There is no align panel. Open one using the menu \"View\".",
                        "Click to dismiss this message"
                    };
                    drawMsg(0L,msg, _pAli, g, 0);
                    return false;
                }

                if (sze(aligned)!=pp.length) { g.drawString("error   ",0,y(cb));return true;}
                if (_freqAA==null) FrequenciesAA.atColumn(aligned, _freqAA=new short[maxSze(aligned)][],0,MAX_INT, false);
                g.setColor(C(0));
                g.drawString("Info: This is the alignment result preview.  Press the button <"+BUT_LAB_Apply+">",0, h+y(cb));
            }
            final boolean isShadeSecStru=ShadingAA.i()==ShadingAA.SECSTRU;
            final int[] startsAt=_localAliStartsAt;
            final Color shadingColors[]=ShadingAA.colors(false);
            for(int iP=0;iP<pp.length;iP++) {
                final Protein p=pp[iP];
                final int y=lineH*(isSorting ? _iiSorting[iP] :iP);
                if (p==null || clipY1>y+lineH || clipY2<y) continue;
                if (jc==_rowHeader) {
                    final Image im=p.getIconImage();
                    final int
                        imW0=im==null?0: im.getWidth(jc),
                        imH=im==null?0:im.getHeight(jc),
                        imW=imW0*imH==0?0:imW0*cb.height/imH,
                        labelW=imW+strgWidth(font,p.getName()),
                        x=(int) ((W-labelW)*_rhAdjust);
                    if (im!=null) g.drawImage(im,x,y,imW,cb.height,jc);
                    g.setColor(C(0xffFFff));
                    g.drawString(p.getName(),x+imW,y+y(cb));
                }
                if (jc==_pAli) {
                    final byte[] s=(aligned!=null && aligned.length>iP && aligned[iP]!=null) ? aligned[iP] : "error".getBytes();
                    setXY(0,y,_origin);
                    dgs.setConservation(_freqAA, StrapView.sliderSimilarity().getValue());
                    final int firstAmino=startsAt!=null ? startsAt[iP] : 0;
                    final long options=isShadeSecStru && firstAmino>=0 ? DrawGappedSequence.SHADE_SEC_STRU : 0L;
                    dgs.draw(_pAli,g,_origin, maxi(0,firstAmino) ,s,0,MAX_INT,p,shadingColors, options);
                    dgs.setConservation(null,0);
                }
            }
        }
        return true;
    }
    /* <<< Paint  <<< */
    /* ---------------------------------------- */
    /* >>> PreferredSize >>> */
    private int _fontSize=12, _fontSizePrev;
    private Font _font;
    private Font font() {
        if (_font==null || _fontSizePrev!=_fontSize) {
            _font=getFnt(_fontSizePrev=_fontSize, true, Font.PLAIN);
        }
        return _font;
    }
    public void setPrefSize() {
        final Protein[] pp=_pp;
        final Font font=font();
        final Rectangle cb=chrBnds(font);
        final int N=sze(pp), h=(cb.height+_lineSkip)*(1+N);
        final byte ali[][]=_aligned;
        final HasScore hs=deref(_a,HasScore.class);
        final Object clazz=_a instanceof MultiFromPairAligner ? ((MultiFromPairAligner)_a).getClasses()[0]:_a;
        _tLayout
            .setTitle(0, ali!=null && hs!=null ? "Score="+hs.getScore() : "Protein names")
            .setTitle(1,"preview of "+delSfx("PROXY",shrtClasNam(clazz)));
        setEnabld(ali!=null, new Object[]{_togSort,_bApply});
        if (ali!=null) {
            final int startsAt[]=new int[N];
            for(int i=mini(sze(_aligned), N); --i>=0;) {
                startsAt[i]=idxOfLetters(_aligned[i], pp[i].getResidueTypeExactLength(),0,pp[i].countResidues());
            }
            _localAliStartsAt=startsAt;
        }
        int w=0;
        for(int i=0; i<N;i++) w=maxi(w, cb.height*2+strgWidth(font,pp[i].getName()));
        _rowHeader.setPreferredSize(dim(w,h));
        _pAli.setPreferredSize(dim( maxSze(ali)*cb.width, h));
        _pAli.revalidate();
    }
    /* <<< PreferredSize <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    public void dispose() { // war InvokeAndWait warum ?
        try {
            _thread.stop();
        } catch(Throwable td){}
        dispos(_a);
        dispos(_lastA);
        rmAllChilds(this);
        rmFromParent(this);
        _pp=null;
    }
    /* <<< Disposable <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    private boolean _isReady;
    public void run() {
        final long time=System.currentTimeMillis();
        _a.compute();
        _lastA=wref(_a);
        final SequenceAlignerSorting sorting=deref(_a,SequenceAlignerSorting.class);
        if (sorting!=null) _iiSorting=sorting.getIndicesOfSequences();
        _isReady=true;
        _aligned=AlignUtils.trimAlignment(2,_a.getAlignedSequences());
        inEdtLater(thrdM("setPrefSize",this));
        if (System.currentTimeMillis()-time>4444) {
            ChMsg.speakBG(_aligned!=null ? "Alignment finished" : "Alignment failed", _aligned!=null ? ChMsg.SOUND_SUCCESS : ChMsg.SOUND_FAILURE);
        }

        repaint();
    }
    private String _progress;
    private void paintProgress(Graphics gr) {
        final String progr=_progress;
        if (progr==null) return;
        try {
            final Graphics g=gr!=null ? gr : _pAli.getGraphics();
            g.setColor(C(0));
            fillBigRect(g,0,0,EM*100,2*EX);
            g.setColor(C(0x00FF00));
            g.drawString(progr,10,EX);
        } catch(Exception ex){}
    }
    /* <<< Threading  <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private static int msgShownPreview;
    public void processEv(AWTEvent ev) {
        final int id=ev.getID();
        final int scaleUpDown=scaleUpDown(ev);
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);

        final int wheel=wheelRotation(ev);
        if (scaleUpDown!=0) {
            if ((_fontSize+=(scaleUpDown>0?1:-1))<4) _fontSize=4;
            setPrefSize();
            repaint();
        } else if (wheel!=0 && q==_rowHeader) {
            final float f=_rhAdjust+wheel*.1f;
            _rhAdjust=f<0?0:f>1?1:f;
            _rowHeader.repaint();
        }
        if (id==MOUSE_CLICKED && q==_pAli && msgShownPreview++<2) {
            if (msgShownPreview==1) {
                _pAli.setPreferredSize(dim(EM*80,EX*9));
                _pAli.scrollRectToVisible(new Rectangle(0,0,EM*80,EX*9));
            } else setPrefSize();
            repaint();
        }

        if (cmd!=null && cmd.startsWith(KEY_PROGRESS)) {
            _progress=delPfx(KEY_PROGRESS,cmd);
            paintProgress(null);
        }
        if (q==_togSort) repaint();
        if (q==_bApply || q==_bUndo) {
            final Protein pp[]=_pp;
            if (pp!=null) {
                final StrapView panel=StrapAlign.alignmentPanel();
                final Protein ppVis[]=panel!=null ? panel.pp() : null;
                for(Protein p : ppVis!=null ? ppVis : pp) {
                    final int gg[]=gcp(_bApply, p, int[].class);
                    if (q==_bApply && gg==null) pcp(_bApply, p.getResidueGap().clone(),p);
                    if (q==_bUndo  && gg!=null) p.setResidueGap(gg.clone());
                }
                if (q==_bApply) {
                    final byte[] aligned[]=_aligned;
                    if (panel==null || sze(aligned)<pp.length || pp.length<2) return;
                    int midColumn=0;
                    final int lastIdx[]=new int[pp.length];
                    for(int r=pp.length; --r>=0;) {
                        final int col=idxOfLetters(aligned[r],pp[r].getGappedSequenceExactLength());
                        final int nL=countLettrs(aligned[r]);
                        lastIdx[r]=pp[r].column2nextIndexZ(col)+nL;
                        midColumn+=col+nL/2;
                    }
                    midColumn/=pp.length;
                    //AcceptAlignment2.delGaps(pp, _startIdx, MAX_INT, aligned);
                    final AcceptAlignment2 accept=new AcceptAlignment2(pp, _startIdx, aligned);
                    final int[] inserts=accept.getCountInserts();

                    final int MAX_SQUEEZE=5;
                    boolean canCompensate=true;
                    for(int r=pp.length; --r>=0;) {
                        final int a0=lastIdx[r]+1, nR=pp[r].countResidues();
                        if (a0+MAX_SQUEEZE>=nR) continue;
                        int bias=inserts[r];
                        for(int a=a0; bias>0 && a<nR && a<a0+MAX_SQUEEZE; a++)  bias-=pp[r].getResidueGap(a);
                        if (bias>0) { canCompensate=false; break; }
                    }
                    final int addToBias= canCompensate ? 0 : -maxi(inserts);
                     {
                    for(int r=pp.length; --r>=0;) {
                        final int a0=lastIdx[r]+1, nR=pp[r].countResidues();
                        int bias=inserts[r]+addToBias;
                        for(int a=a0; bias!=0 && a<nR; a++) {
                            bias+=pp[r].addResidueGap(a,-bias);
                        }
                    }
                    if (addToBias!=0 && ppVis!=null) {
                        for(Protein p : ppVis) {
                            if (cntains(p,pp)) continue;
                            p.addResidueGap(p.column2nextIndexZ(midColumn), -addToBias);
                        }
                    }
                    }
                    new StrapEvent(this,StrapEvent.ALIGNMENT_CHANGED).run();
                    if (isSelctd(_togSort) && sze(_iiSorting)==pp.length) {
                        for(int i=_iiSorting.length; --i>=0;) pp[i].setPreferedOrder(_iiSorting[i]);
                        StrapAlign.inferOrderOfProteins(StrapAlign.SORT_PREFORDER, pp);
                    }
                    for(Protein p:pp) pcp("ALIGNED_BY_METHOD",_a.getClass(),p);
                }
                setEnabld(q==_bApply, _bUndo);
                StrapEvent.dispatchLater(StrapEvent.ALIGNMENT_CHANGED,1);
            }
        }
    }
}
