package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;

/**HELP
This operation creates residue selections at all beginnings of  continuous blocks of translated nucleotides (WIKI:Exon, WIKI:Intron).

   @author Christoph Gille
*/
public class DnaExonStart {
    public final static void mark(Protein pp[]) {
        StrapAlign.errorMsg("");
        if (pp==null || pp.length==0) { StrapAlign.errorMsg("Select proteins"); return;}
        int count=0;
        for(int i=0;i<pp.length;i++) {
            final Protein p=pp[i];
            final byte bb[]=p.getNucleotides();
            final boolean isTr[]=p.isCoding();
            if (bb==null || isTr==null) continue;
            mark(p,isTr);
            count++;
        }
        if (count==0)  { StrapAlign.errorMsg("No exons and introns in proteins"); return;}
    }

    private final static void killResidueAnnotations(Protein p) {
        for(ResidueAnnotation s: p.residueAnnotations()) {
            if ("start_of_exons".equals(s.value(ResidueAnnotation.GROUP)) &&
                s.value(ResidueAnnotation.NAME).startsWith("exon")) p.removeResidueSelection(s);
        }
    }
    private final static void mark(Protein p, boolean isTr[]) {
        killResidueAnnotations(p);

        boolean lastIsTr=false;
        final UniqueList<ResidueAnnotation> v=new UniqueList(ResidueAnnotation.class);
        final boolean isReverse=0!=(p.getCodingStrand()&Protein.REVERSE);
        for(int i=0;i<isTr.length;i++) {
            if (!lastIsTr && isTr[i]) {
                final ResidueAnnotation s=new ResidueAnnotation(p);
                s.setValue(0,ResidueAnnotation.NAME, "exon"+ (1+v.size()));
                final int idx=isReverse ? isTr.length-i : i+1;
                s.setValue(ResidueAnnotation.E_NT,ResidueAnnotation.POS,ChUtils.toStrg(idx));
                s.setValue(0,ResidueAnnotation.GROUP,"start_of_exons");
                s.setColor(Color.PINK);
                p.addResidueSelection(s);
                v.add(s);
            }
            lastIsTr=isTr[i];
        }
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED,111);
    }
}
