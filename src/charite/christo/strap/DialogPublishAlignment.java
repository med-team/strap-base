package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.Protein;
import java.awt.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

<div class="figure">

    <table style="caption-side: bottom">
        <CAPTION><b>Figure: </b>
        Generation of a Html link. The html code is generated automatically and can be tested in the web browser.
        </CAPTION>
        <tr><td> <i>JCOMPONENT:new DialogPublishAlignment("")</i></td></tr>

      </table>

</div>

With this dialog a Web-Link can be formed which loads the proteins from the public databases into Strap and displays an alignment.
     The only requirement for the client machine is Java.

      Depending on the amount of information two different types are
      available:

    <h3>1. Single web address</h3>

    The web address can not only be included in web-pages, but also in e-mails and Office-documents.

    The generation is conducted in two steps:
    <ol>

      <li> <i>BUTTON:DialogPublishAlignment#BUT_LAB_P</i> The
        parameter String is written into the first text field and can
        be modified by the user.  Not the sequence but the database
        accession id is stored.  For technical details see
        http://www.bioinformatics.org/strap/createStrapLinks.html.
        </li>

      <li> <i>BUTTON:DialogPublishAlignment#BUT_LAB_U</i> From the
        text in the first text field the web address will be generated
        using WIKI:Url_encoding and written into the 2nd text field.
        </li>

        <li><i>BUTTON:DialogPublishAlignment#BUT_LAB_B</i> The
        generated web address can be tested by clicking. A new Strap
        session will be opened in web-mode and an alignment will be
        loaded with the specified information.  </li>

    </ol>

   <h3>2. Web form</h3>

    Since the web form has no  size limitation,  the entire information for the alignment can be included.
    The draw-back is that it can only be included in web-pages, but not in office documents or e-mails.

   <ol>
     <li><i>BUTTON:DialogPublishAlignment#BUT_LAB_F</i>
       A html-page including the web link for the selected proteins is generated.</li>

     <li><i>BUTTON:DialogPublishAlignment#BUT_LAB_B</i>
       The link can be tested in the web browser.
       From this html code the text between the opening and closing and &lt;body&gt; tags can be used in any html-page.
     </li>

   </ol>

    @author Christoph Gille
*/
public class DialogPublishAlignment extends AbstractDialogJPanel implements java.awt.event.ActionListener {
    public final static String
        BUT_LAB_B="Test web page in Web Browser",
        BUT_LAB_F="\u2193\u2193\u2193 Generate Web Form \u2193\u2193\u2193",
        BUT_LAB_P="\u2193\u2193\u2193 Generate parameter String  \u2193\u2193\u2193",
        BUT_LAB_U="\u2193\u2193\u2193 Form URL \u2193\u2193\u2193";

    private final ChTextArea _taPARA=new ChTextArea(""), _taENCODED=new ChTextArea(""), _taForm=new ChTextArea("");
    private final ChButton _toggles[]=new ChButton['z'];
    private Object[] _radioAnno;
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String php="http://www.bioinformatics.org/strap/strap.php";
        final String cmd=ev.getActionCommand();
        Protein[] pp=StrapAlign.selectedProteinsInVisibleOrder();
        if (sze(pp)==0) pp=StrapAlign.visibleProteins();
        final BA sb=new BA(333);
        final int iRadioAnnotations=radioGrpIdx(_radioAnno);
        final ResidueAnnotation selectedAnnotations[]=(ResidueAnnotation[]) StrapAlign.coSelected().residueSelections('A');
        if (cmd==BUT_LAB_P) {
            for(Protein p : pp) {
                final String acc=p.getAccessionID(), nam=p.getName(), pdb=p.getResidueCalpha()==null?null:p.getPdbID();
                String pName=null;
                if (pdb!=null) {
                    final String ch=p.getChainsAsString();
                    pName= (sze(ch)>0 ? pdb+ "_"+ch : pdb)+".pdb";
                    sb.a(pdb);
                    if (sze(p.getChainsAsString())>0) sb.a('_').a(p.getChainsAsString());
                } else if (acc!=null)  {
                    pName=acc; /* KEGG */
                    sb.a(acc);
                } else {
                    if (p.getURL()==null) continue;
                    sb.a(p.getURL());
                }
                sb.a('|');
                if (pName!=null && !pName.substring(pName.indexOf(':')+1).replace(':','_').equals(nam)) sb.a(nam);
                if (cmd==BUT_LAB_P) {
                    sb.a('|');  /* ---  Icon --- */
                    if (_toggles['i'].s()) {
                        final BA iconUrl=readBytes(p.iconLink());
                        if (sze(iconUrl)>0 && !strEquls(URL_RCSB_IMAGES,iconUrl,0)) sb.a(iconUrl.delBlanksL().delBlanksR());
                    }
                    sb.a('|'); /* ---  ResidueAnnotation --- */
                    if (iRadioAnnotations!=2) {
                        for(ResidueAnnotation a:p.residueAnnotations()) {
                            if ((iRadioAnnotations==0 || cntains(a,selectedAnnotations)) && a.featureName()==null) {
                                final Color color=a.getColor();
                                if (color!=null && a.isEnabled() && a.featureName()==null) {
                                    sb.a('#').a(color,0,6).a(',').a(ResSelUtils.optimizedPositions(a, false));
                                }
                            }
                        }
                    }
                    sb.a('|'); /* --- CDS --- */
                    final boolean translated[]=p.isCoding();
                    if (sze(translated)>0 && ( !p.cdsExpressionApplied() || sze(p.getCDS())>1) ) {
                        if (0!=(p.getCodingStrand()&Protein.REVERSE)) sb.a("complement,");
                        sb.boolToText(translated,0, ",", "..");
                    }
                    sb.a('|');
                }
                _taPARA.t(sb.a('\n').replace(0L, "|\n","\n")).tools().underlineRefs(ULREFS_WEB_COLORS);
            }
        }
        if (cmd==BUT_LAB_F) {
            sb.a("<form action=\"").a(php).aln("?separateInstance=t\" method=\"POST\"> <input type=\"hidden\" name=\"script\" value=\"\n");
            final int opt=
                (_toggles['i'].s()?StrapScriptCreator.ICONS:0) |
                (iRadioAnnotations==2 ? StrapScriptCreator.NO_SELECTIONS:0) |
                (iRadioAnnotations==1 ? StrapScriptCreator.SELECTED_SELECTIONS:0);
            StrapScriptCreator.makeScript(opt, pp, sb);
            sb.aln("\n\"/> <input type=\"SUBMIT\" name=\"SUBMIT\" value=\"This is the Web-Button to start Strap.\"/></form>");
            _taForm.t(sb).tools().underlineRefs(ULREFS_WEB_COLORS|ULREFS_NOT_CLICKABLE|ULREFS_NO_ICON|ULREFS_GO)
                .cp(HelpCommands.class,HelpCommands.getInstance(Strap.SCRIPT_COMMANDS))
                .tt(null);
        }
        if (cmd==BUT_LAB_U) {
            final BA ba1=new BA(333).a(php).a("?align=").filter(FILTER_URL_ENCODE, _taPARA.toString().replaceAll("\\s+"," ").replaceAll("\\|+ "," "));
            _taENCODED.t(ba1).tools().underlineRefs(ULREFS_NEVER);
        }
        if (cmd=="B"){
            final java.io.File fHtml=newTmpFile(".html");
            wrte(fHtml,"<html><body>\n"+_taForm+"\n</body></html>\n");
            visitURL(fHtml, modifrs(ev));
        }
    }

    public DialogPublishAlignment() {  this((String)null);}
    public DialogPublishAlignment(String isHtmlDocumentation) {
        _taENCODED.setLineWrap(true);
        _taPARA.setLineWrap(true);
        _taPARA.tools().underlineRefs(ULREFS_WEB_COLORS|ULREFS_NEVER|ULREFS_NOT_CLICKABLE);
        for(ChTextArea ta : new ChTextArea[]{_taPARA, _taENCODED, _taForm}) {
            ta.tools().enableWordCompletion(new Object[]{dirWorking(),StrapScriptInterpreter.allScriptCommands()}).enableUndo(true);
        }
        _toggles['i']=toggl("Include protein icons").s(true).tt("Protein images dragged from Web pages or file browsers onto proteins");
        _radioAnno=radioGrp(new String[]{"All","Only selected", "None"}, 0, this);
         final javax.swing.JTabbedPane tabbed=new javax.swing.JTabbedPane();
         final Component pNorth=pnl(CNSEW, null,dialogHead(this),null,_toggles['i'].cb(),pnl("#ETB","  Included residue annotations: ",_radioAnno));
         {
             final Object
                 butP=new ChButton(BUT_LAB_P).li(this),
                 butE=new ChButton(BUT_LAB_U).li(this);
             tabbed.add("Compact web address", pnl(new GridLayout(2,1),pnl(CNSEW,scrllpn(_taPARA),pnl(butP)), pnl(CNSEW, _taENCODED, pnl(butE))));
        }
        {
            final Object
                but=new ChButton(BUT_LAB_F).li(this),
                pNorth2=pnl(but),
                panRelated=StrapAlign.b(DialogExportWord.class),
                pSouth=pnl(HB,new ChButton("B").li(this).t(BUT_LAB_B), "#",pnlTogglOpts("Related dialog: ",panRelated),panRelated,"#");
            tabbed.add("Comprehensive Web form", pnl(CNSEW,scrllpn(_taForm),pNorth2,pSouth));
        }
        if (isHtmlDocumentation!=null) tabbed.setSelectedIndex(1);
        pnl(this,CNSEW,  tabbed, pNorth);
    }

}
