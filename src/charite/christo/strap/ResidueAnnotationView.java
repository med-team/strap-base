package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.strap.ResidueAnnotation.*;
import static java.awt.event.MouseEvent.*;
/**HELP
   <i>PACKAGE:charite.christo.strap.extensions.</i>
   <i>PACKAGE:charite.christo.protein.</i>

   To edit a <i>CLASS_REF:ResidueAnnotation</i>residue annotation the menu
   item <i>BUTTON:ResidueSelectionPopup#ACTION_edit</i> of the
   context menu must be clicked. Alternatively the residue underlining in the alignment panel can be double clicked.

   The annotation data is shown as a table. The rows contain the name, the group name, the residue positions and additional annotation.
   <div class="figure">
   <table style="caption-side: bottom">
   <CAPTION><b>Figure: </b>
   The left column gives the type  and the right column the annotation text.
   Each row has a toggle button to activate and inactivate the entry.</CAPTION>
   <tr><td>  <i>JCOMPONENT:ResidueAnnotationView#docuView("")</i> </td></tr>
   </table>
   </div>
   <ul>
   <li>
   The most important entry is <b>Positions</b> which defines the the selected sequence positions.
   Here are some examples of valid entries
   <ol>
   <li>
   The expression "1,3,4,6,101-103,110-112"
   selects the residue indices  1,3,4,6,101,102,103,110,111,112.
   Instead of the commas, spaces can by used.
   </li>
   <li>
   The expression
   "+2 1 3 4"
   selects the residues 3,5, and 6 because the preceding +2 adds an offset of 2 to all positions. The "+"-sign is the first character in this expression.
   For the user interface  the first residue has the index "1" whereas internally Strap starts counting at zero.
   A negative offset of "2" is achieved by a leading  "+-2".
   </li>
   <li>
   The expression
   "1:G-3:G 5:G"
   selects the residues with the pdb-numbers 1,2, 3 and 5 of the chain G.
   If the protein has only one chain the chain identifier can be omitted like
   "1:-3: 5:"

   </li>
   <li>
   Referring to nucleotide positions:
   When the toggle button is pressed (default state) the indices refer to <b>amino acid</b> positions.
   Otherwise the positions indicate <b>nucleotides</b> in case the protein is translated from a nucleotide sequence.
   </li>

   <li>
   To any group an atom specification can be appended.
   This allows to select certain atoms before changing their style in the 3D-viewers.
   Even though protein 3D viewers usually have a specific language for atom selection,
   the atom expression used here is defined by Strap and works for all 3D-viewers.
   <br>
   for example "10:-20:.CA.CB" narrows the selection to the atoms CA and CB for the residues 10 to 20.
   A later command  such as "3D_spheres" would affect only c-alpha and c-beta atoms.
   There may be more than one "Atoms"-entries. Each "Atoms" specification take effect only on the following 3D-style
   commands but not on the previous.

   Asterisk can be used as a wild card like "*.CA" or "10:-20:.C".
   For the proper atom identifiers have a look at the PDB file.

   </li>
   </ol>
   </li>
   <li><b><i>STRING:ResidueAnnotation#ATOMS</i></b>
   Certain Atoms can be specified by an expression like ".CB.CA" which means only C&alpha; and C&beta; atoms.
   The next 3D-command will act on these rather than on all atoms of the amino acid.
   When sending the commands to a 3D-program, the rows are processed sequentially.
   If there comes yet another <i>STRING:ResidueAnnotation#ATOMS</i>-row,
   the previous atom-specification is replaced by the new one.
   </li>
   <li><b><i>STRING:ResidueAnnotation#NAME</i></b> Each selection has a name. Un-checking the check box deactivates the residue annotation.</li>
   <li><b><i>STRING:ResidueAnnotation#GROUP</i></b> Several selections may be bundled in one group e.g. "active site"</li>
   <li><i>CLASSICON_CLASSNAME:Texshade</i>: TeXshade commands. See menu Export in the file menu.</li>
   <li><b>Note, Remark: </b> Free text. URLs are clickable. Supports cross-links like PDB:1ryp, PUB<span></span>MED:0815. The list of databases can be changed by Ctrl-click the cross-link.</li>
   <li><b>Balloon: </b> The balloon text appears when the mouse is over the highlighted residues.</li>
   </ul>

   <br><b>Drag and drop: </b> Rows can be reordered with the mouse or can be dropped on other residue selections.

   <i>SEE_DIALOG:DialogResidueAnnotationChanges</i>
   <i>SEE_DIALOG:DialogResidueAnnotationList</i>
   @author Christoph Gille
*/
public final class ResidueAnnotationView extends DefaultTableModel
    implements HasPanel, ProcessEv, Disposable, StrapListener, ListCellRenderer, TableCellEditor,TableCellRenderer, ChRunnable {

    public final static String
        FILE_EXT_DND=".annotationRow",
        POS_AA=addHtmlTagsAsStrg("<font size=2>Indices of <br>amino acids</font>"),
        POS_NUCL=addHtmlTagsAsStrg("<font size=2>Indices of <br>nucleotides</font>"),
        BUT_INS_VAR="Insert variable...",
        BUT_LAB_VARIABLES="View current replacement for variables such as \"TEX_PROTEIN\" or \"First_Aaa\"";
    private final static int COL_KEY=1,COL_VALUE=2,COL_CB=0,COL_DELETE=3;
    private final EvAdapter LI=new EvAdapter(this);

    private final ChButton
        RENDERER=labl(),
        TOG_RENDERER=toggl(ChButton.ICON_SIZE|ChButton.PAINT_IN_TABLE, null),
        BUT_RENDERER=new ChButton(ChButton.PAINT_IN_TABLE,null),
        BUT_DEL=new ChButton(ChButton.ICON_SIZE|ChButton.PAINT_IN_TABLE,null).li(LI).i(IC_KILL),
        BUT_OPT=new ChButton(ChButton.MAC_TYPE_ICON|ChButton.PAINT_IN_TABLE,null).li(LI).t("*"),
        BUT=new ChButton(ChButton.PAINT_IN_TABLE,null).li(LI),
        TOG=toggl(ChButton.ICON_SIZE|ChButton.PAINT_IN_TABLE,null).li(LI),
        CB_VARIABLES=toggl(BUT_LAB_VARIABLES).li(LI);
    { monospc(RENDERER); }
    private ChJList _jlVar;
    private int _rowEd=-1, _colEd=-1, _rowTf=-1, _mc, _rowCount;
    private boolean _disposed;
    private final JComponent _pNorth, _panel;
    private final ChJTable _jt=new ChJTable(this,ChJTable.ICON_ROW_HEIGHT|ChJTable.NO_REORDER);
    private final ResidueAnnotation _a;
    private Color _color;
    private static String _wcTexshade[];
    /* ---------------------------------------- */
    /* >>> Instance >>> */
    public ResidueAnnotationView(ResidueAnnotation a) {
        _a=a;
        pcp(KEY_MODEL, wref(a),this);

        setDataVector(new Object[0][4],"\nType\nValue\n ".split("\n"));
        final ChJTable jt=_jt;
        jt.renderer(this).setDefaultEditor(Object.class,this);
        jt.setRowSelectionAllowed(false);
        jt.setCellSelectionEnabled(false);
        jt.setColumnSelectionAllowed(false);
        pcp(ChJTable.KEY_FILE_EXT_DND, FILE_EXT_DND, jt);
        final String tips[]="\nType of entry\nContent of Entry which can be modified by the user".split("\n");
        rtt(jt.headerTip(tips));
        scrollByWheel(jt);
        final Object
            oo[]={this, ResidueAnnotation.class},
            butVar=new ChButton(BUT_INS_VAR).li(LI).tt("Variables contain text that is set dynamically"),
            butCursor=new ChButton("C").li(LI).t("Add cursor position").tt("Add alignment cursor position."),
            pVar=pnl(HB,  butCursor, " ", butVar, CB_VARIABLES.cb()),
            pNorth=pnl(CNSEW,
                       pnl(HB,
                           new ChButton("A").li(LI).t("Add entry").tt("Add a new entry consisting of <br>a key and a value"),
                           new ChButton(ChButton.NO_FILL|ChButton.NO_BORDER,"M").t("Menu \u25bc").li(LI),
                           (a.featureName()!=null ? Customize.newButton(Customize.seqFeatureColors).rover(IC_COLOR).t(null) : new ButColor(0,C(0xFF00FF),new Object[]{a,LI}).li(LI)).setOptions(ChButton.NO_FILL|ChButton.ICON_SIZE),
                           " ",
                           new ProteinLabel(ProteinLabel.ALWAYS_CONTEXT_MENU, SPUtils.sp(a))
                           ),
                       null,null,
                       pnl(new ChButton(ChButton.NO_FILL|ChButton.NO_BORDER|ChButton.ICON_SIZE,null).doClose(CLOSE_DISPOSE,this).tt("Close this view.<br>The residue annotation is not deleted.")),
                       pnl(smallHelpBut(oo),smallSourceBut(oo), toggl().doCollapse(new Object[]{butCursor,pVar}).tt("Such as \"$FIRST_AAA\" "), "Options")
                       );
        _pNorth=pnl(CNSEW, pVar, pNorth);
        _panel=pnl(CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE,_jt), _pNorth, pnl(REMAINING_VSPC1));
        setMinSze(1,1, _panel);
        TabItemTipIcon.set(dTab(a),null,null,null,  _panel);

        jt.setColWidth(false, COL_CB,ICON_HEIGHT);
        jt.setColumnWidthC(false, COL_DELETE, new Object[]{BUT_OPT, BUT_DEL});
        jt.setColWidth(true, COL_KEY, EM*20);
        jt.setColWidth(true, COL_VALUE, EM*60);
        pcp(KEY_DROP_TARGET_REDIRECT,wref(a),this);
        pcp(KEY_DROP_TARGET_REDIRECT,wref(a),jt);
        setColor(a.getColor());

        StrapAlign.addListener(this);
        StrapAlign.newDropTarget(_panel,false);
        StrapAlign.newDropTarget(jt,false);
        jt.enableDragRows(this, new int[]{1,2});
        pcp(KEY_DRAG_SRC_ID, intObjct(a.hashCode()), jt);

        pcp(KEY_GET_VIEW, wref(this), a);
        pcp(KEY_MODEL,wref(a), _panel);
    }
    public Object getPanel(int mode) { return _panel;}
    public void dispose(){
        final ResidueAnnotation a=_a;
        if (!_disposed) {
            _disposed=true;
            StrapAlign.rmListener(this);
            rmFromParent(this);
            rmAllChilds(_panel);
            pcp(KEY_GET_VIEW, null, a);
        }
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> TableModel >>> */
    public int getRowCount() {
        final ResidueAnnotation a=_a;
        return a==null ? 0 : a.entries().length;
    }
    public Object getValueAt(int row,int col) {
        final ResidueAnnotation a=_a;
        final Entry e=a==null?null:get(row, a.entries(), Entry.class);
        return e==null?null:col==1?e.key():col==2?e.value():col==3 && e.isEnabled()?"true" : "";
    }
    public boolean isCellEditable(int row,int col) {
        final Entry e=entryAt(row);
        final String k=e!=null ? e.key() : null;
        return col!=COL_KEY || isButton(k);
    }
    public void setValueAt(Object o,int row,int col) { changed(false); }
    static void changed(boolean color) {
        StrapEvent.dispatchLater(color?StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR : StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        StrapAlign.setNotSaved();
    }
    /* <<< TableModel  <<< */
    /* ---------------------------------------- */
    /* >>> CellRenderer >>> */
    public Component getListCellRendererComponent(JList l,Object o, int idx, boolean isSelected, boolean hasFocus) {
        final String txt=o.toString();
        return RENDERER.t(txt+" ===> "+ResSelUtils.replaceVariable(txt,_a)).fg(isSelected ? 0xff : 0).bg(0xFFffFF);
    }
    private void stopEditingTextField(int row) {
        final ResidueAnnotation a=_a;
        final Entry e=entryAt(row);
        if (a!=null && e!=null && e.key()==BG_IMAGE) a.findIconImage();
    }
    public Object getCellEditorValue() { return null; }
    public boolean isCellEditable(EventObject ev) { return true;}
    public boolean shouldSelectCell(EventObject ev) { return true;}
    public boolean stopCellEditing() {
        fireEditing('S');
        _rowTf=-1;
        return true;
    }
    @Override public void cancelCellEditing() { fireEditing('C');}
    public void addCellEditorListener(CellEditorListener li) {_listeners.add(li);}
    public void removeCellEditorListener(CellEditorListener li) { _listeners.remove(li);};
    private final List<CellEditorListener> _listeners=new ArrayList();
    private void fireEditing(char cancelOrStop) {
        final ChangeEvent ev=new ChangeEvent(this);
        for(int i=_listeners.size();--i>=0;) {
            final CellEditorListener l=(CellEditorListener)get(i,_listeners);
            if (l==null) continue;
            if (cancelOrStop=='C') l.editingCanceled(ev);
            else l.editingStopped(ev);
        }
        if (cancelOrStop=='S') _rowTf=-1;
    }
    private ChTextField textField(Entry e, boolean createInstance, boolean sync) {
        if (e==null) return null;
        final String v=e.value();
        ChTextField tf=(ChTextField)get(0,e.TF);
        if (tf==null) {
            if (!createInstance) return null;
            e.TF[0]=wref(tf=new ChTextField(v));
            tf.tools().enableUndo(true).underlineRefs(ULREFS_WEB_COLORS).cp(Entry.class,e). cp(ACTION_DATABASE_CLICKED,"Load");
            final String k=e.key();
            Object wc=k==POS ? "REFERENCE=UNIPROT:" : cntains(k,MAIN_KEYS) ? null : ResSelUtils.variables(0);
            if (k==VIEW3D) wc=new Object[]{Protein3dUtils.allCommands(),wc};
            if (k==TEXSHADE) {
                if (_wcTexshade==null) _wcTexshade=ChUtils.wrdsIntTxt(readBytes(rscAsStream(0L,_CCS, Texshade.EXAMPLES_RSC)), chrClas(-LETTR_DIGT_US));
                wc=new Object[]{wc,_wcTexshade};
            }
            if (k==STYLE) wc=ResSelUtils.styles();
            if (wc!=null) tf.tools().enableWordCompletion(wc);
            addActLi(LI,tf);
        }
        if (sync && !v.equals(toStrg(tf))) {
            tf.tools().underlineRefs(ULREFS_WEB_COLORS).setTextTS(v);
        }
        return tf;
    }
    public Component getTableCellRendererComponent(JTable table,Object value,boolean isSelected,boolean hasFocus,int row,int column) {
        final JComponent c=component(row,column, false);
        setTip(toolTip(row,column), c);
        return c;
    }
    public Component getTableCellEditorComponent(JTable table,Object value,boolean hasFocus, int row,int column) {
        _rowEd=row;
        _colEd=column;
        final JComponent c=component(row,column, true);
        setTip(toolTip(row,column), c);
        return c;
    }
    private JComponent component(int row,int column, boolean isEditor) {
        final ChButton
            lab=RENDERER.tt(null).i(null).t("").fg(0).cp(KEY_BACKGROUND, C(column==2 ? 0xFFffFF : DEFAULT_BACKGROUND)),
            toggle=  (isEditor ? TOG : TOG_RENDERER).tt(null),
            button=  (isEditor ? BUT : BUT_RENDERER).tt(null);
        lab.setEnabled(true);
        final ResidueAnnotation a=_a;
        final Entry e=entryAt(row);
        if (e==null||a==null) return lab.t("error entry==null");
        final Protein p=SPUtils.sp(a);
        final String value= e.value().trim(), key=e.key();
        final boolean empty=sze(value)==0;
        switch(column) {
        case COL_DELETE:
            return
                key==POS ? (a.canSimplify()?BUT_OPT:lab) :
            idxOf(key,MAIN_KEYS)>=0 ? lab :
            isEditor ? BUT_DEL : button.i(IC_KILL);
        case COL_KEY:
            if (key==POS) return lab.t(e.isEnabled() ? POS_AA:POS_NUCL);
            final String t=delSfx("2StrapPROXY",key.substring(key.lastIndexOf('.')+1));
            if (isButton(key)) {
                button.setHorizontalAlignment(SwingConstants.LEFT);
                return button.i(dIcon(key)).t(t).tt(dTip(key));
            }
            return lab.i(orO(dIcon(key), IC_BLANK)).t(t);
        case COL_VALUE:
            if (isEditor) {
                if (_rowTf>=0) { stopEditingTextField(_rowTf);_rowTf=row; }
                _rowTf=row;
                return textField(e,true,true);
            }
            if (key==POS && !isEditor) {
                if (empty) return lab.i(null).t("Enter sequ. posit. e.g.24-26,29,31. !   For DNA unselect checkbox !").fg(0xff0000);
                final BA sb=new BA(99).a(value);
                if (!e.isEnabled() && p!=null && p.getNucleotides()==null) return lab.t("Error: the Protein does not have nucleotides. Set the toggle to aminos !").fg(0xff0000);
                if (strchr(':',value ,0, strstr(STRSTR_E,"REFERENCE=",value))>=0 && p!=null && p.getResidueNumber()==null) {
                    return lab.t("Error:  Colon is only used to refer to the PDB residue chain and number!").fg(0xff0000);
                }
                ResSelUtils.selectedAsString(a,sb.a(" ==> "));
                return lab.t(sb);
            }
            if (key==NAME) return empty ? lab.t("enter a name for the residue selection e.g. 'active site'").fg(0xff0000) : lab.t(a.getName());
            if (key==BG_IMAGE && !empty && value.equals(a.lastImage())) return lab.i(a.getIcon()).t(value);
            if (key==GROUP && empty) return lab.i(null).t("enter a name for the group e.g. 'polymorphisms'").fg(0xff0000);
            return lab.t(CB_VARIABLES.s() ? ResSelUtils.replaceVariable(value,a) : value);
        case COL_CB:
            toggle.s(e.isEnabled());
            if (key==GROUP ||  key==COLOR) return lab.i(null).t("");
            if (key==POS) return  toggle.i(new Object[]{IC_DNA,IC_3D});
            return toggle.i(new Object[]{IC_HIDE,IC_SHOW});
        }
        return lab.t("ERROR");
    }
    /* <<< Rendering <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    private String toolTip(int row, int col) {
        final Entry e=entryAt(row);
        final ResidueAnnotation a=_a;
        if (e==null || a==null) return null;
        final String k=e.key();
        if(k==POS) {
            return
                col==COL_KEY ?   "Sequence positions<br>E.g. 42, 100-200,202<br>negtive or pos offset: <br> first char '+' or '-' followed by a number<br>Referring to pdb-nuber and chain like 42:A " :
                col==COL_VALUE ? "Sequence position" :
                col==COL_CB ?    "Toggle nucleotides or amino acids" :
                col==COL_DELETE && a.canSimplify() ? "Simplify. For example \"4-5,6\" yields \"4-6\"<br>Hold shift-key to use PDB numbering." :
                null;
        }
        if (k==NAME) {
            return
                col==COL_KEY || col==COL_VALUE ?  "Name of the residue selection" :
                col==COL_CB ?  "Activate/deactivate the entire selection." :
                null;
        }
        if (col==COL_CB) return  "Activate / deactivate row";
        if (k==TEXSHADE) {
            return
                col==COL_KEY ?  "Click this button to generate and view PDF." :
                col==COL_VALUE ?"TeXshade command." :
                null;
        }
        if (k!=null && k.startsWith(_CCP)) {
            return
                col==COL_KEY ?  "Click this button to send the 3D command to a 3D view." :
                col==COL_VALUE ?"Command for protein 3D visualization" :
                null;
        }
        return
            k==GROUP ? "Residue selections are bundled in groups" :
            (k=="Note" || k=="Remark") && col==COL_VALUE ? "Free text" :
            col==COL_DELETE ? "Delete row" :
            null;
    }
    /* <<< Tooltip <<< */
    /* ---------------------------------------- */
    /* >>> StrapListener >>> */
    public void handleEvent(StrapEvent ev) {
        final ResidueAnnotation a=_a;
        final int t=ev.getType(), mc=a.mc(), nRows=getRowCount();

        if (_rowCount!=nRows) {
            _rowCount=nRows;
            revalAndRepaintC(_jt);
        } else if (_mc!=mc) {
            _mc=mc;
            _jt.repaint();
        }
        if ( (t&StrapEvent.FLAG_ROW_HEADER_CHANGED)!=0) revalAndRepaintC(_pNorth);
        if (t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR) setColor(a.getColor());
        if (t==StrapEvent.RESIDUE_SELECTION_DELETED) {
            final Protein p=SPUtils.sp(a);
            if (p==null || !cntains(a,p.residueAnnotations())) dispose();
        }
    }

    private void setColor(Color col) {
        if (col!=null && col!=_color) {
            _color=col;
            for(Component c: childsR(_pNorth, JPanel.class)) {
                if (c instanceof JPanel || c instanceof JLabel) c.setBackground(col);
            }
        }
    }
    /* <<< StrapListener <<< */
    /* ---------------------------------------- */
    /* >>> ActionListener >>> */
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_GET_TAB_TEXT || id==ChRunnable.RUN_GET_ITEM_TEXT || id==ChRunnable.RUN_GET_TIP_TEXT || id==ChRunnable.RUN_GET_ICON) {
            final ResidueAnnotation a=_a;
            if (a!=null) return a.run(id,arg);
        }
        return null;
    }
    private AddAnnotation _aa;
    public void processEv(AWTEvent ev) {
        final String cmd=actionCommand(ev);
        final Object q=ev.getSource();
        StrapAlign.errorMsg("");
        final ResidueAnnotation a=_a;
        if (a==null) return;
        final Protein p=SPUtils.sp(a);
        if (isPopupTrggr(false,ev) || cmd=="M") {
            if (isPopupTrggr(true,ev) || cmd=="M") ResidueSelectionPopup.showContextMenu(a);
            return;
        }
        if (cmd!=null) {
            if (cmd==ACTION_COLOR_CHANGED) changed(true);
            if (cmd=="A") {
                if (_aa==null) _aa=new AddAnnotation(a);
                ChFrame.frame(txtForTitle(q),_aa,CLOSE_CtrlW_ESC|ChFrame.PACK).shw();
            }
            if (cmd==BUT_INS_VAR) {
                if (_jlVar==null) {
                    (_jlVar=new ChJList(ResSelUtils.variables(0),ChJTable.SINGLE_SELECTION).li(LI)).setCellRenderer(this);
                    pcp(KEY_NORTH_PANEL,"Choose a variable to be inserted at the<br>current table cell and caret position",_jlVar);
                }
                ChFrame.frame(txtForTitle(q), _jlVar, ChFrame.SCROLLPANE|CLOSE_CtrlW_ESC).shw();
            }
            if (cmd=="C") {
                final int iA=StrapAlign.indexOfAminoAcidAtCursorZ(p);
                if (iA<0) StrapAlign.errorMsg("Inserting Cursor-Position: First place the cursor in the alignment pane");
                else {
                    final String s=" "+Protein.selectedPositionsToText(new boolean[]{true}, iA, p)+" ";
                    final Entry e=a.entryWithKey(POS);
                    final ChTextField tf=textField(e,false,false);
                    if (tf!=null && tf.hasFocus()) ChTextComponents.insertAtCaret(0L, s, tf);
                    else if (!ResSelUtils.isSelectedAAZ(iA,a)) {
                        a.setValueE(E_NO_UPDATE_UI|E_EDITED, a.value(POS)+s, e);
                        _jt.repaint();
                    }
                    changed(false);
                }
            }
            if (cmd==ACTION_CLICKED && q==_jlVar) {
                final Entry e=entryAt(_rowTf);
                if (e==null) return;
                if (idxOf(e.key(),MAIN_KEYS)>=0) {
                    error(new BA(99).a("A variable cannot be inserted into an annotation of type ").join(MAIN_KEYS,", "));
                    return;
                }
                final ChTextField tf= textField(e,true,true);
                if (tf==null) StrapAlign.errorMsg("Error: Click into table cell where you want the variable to be inserted");
                else {
                    ChTextComponents.insertAtCaret(0L," "+_jlVar+" ",tf);
                    changed(false);
                }
            }
            final int row=_rowEd, col=_colEd;
            final Entry entry=entryAt(row);
            if (cmd==BUT_LAB_VARIABLES) { _jt.repaint();}
            if (entry!=null && p!=null) {
                final String key=StrapPlugins.mapS2L(entry.key()), value=entry.value();
                if (_rowEd<0) return;
                if (q==BUT_DEL) {
                    final Entry e=entryAt(_rowEd);
                    if (e!=null) {
                        a.removeEntry(e);
                        changed(e.key()==STYLE);
                        if (a.featureName()!=null && !a.isEdited()) delFile(a.featureFile(dirStrapAnno()));
                    }
                } else if (q==BUT_OPT) {
                    _rowTf=-1;
                    a.setValue(0,ResidueAnnotation.POS, ResSelUtils.optimizedPositions(a, isShift(ev)));
                    fireEditing('S');
                } else if (q==BUT) {
                    final Class keyC=name2class(key);
                    if (key==TEXSHADE) {
                        ResSelUtils.texshade(ResSelUtils.replaceVariable(strplc(STRSTR_w_R,"$PROTEIN_NO","1",value),a),p);
                    } else if (key==VIEW3D || isAssignblFrm(ProteinViewer.class, keyC)) {
                        final String w0=fstTkn(value);
                        boolean success=false;
                        final BA sbErr=new BA(99);
                        ProteinViewer vv[]=p.getProteinViewers().clone();
                        for(int i=vv.length; --i>=0;) {
                            if (keyC!=null ? !keyC.equals(clazz(vv[i])) : !Protein3dUtils.supports(w0, vv[i])) vv[i]=null;
                        }
                        if (countNotNull(vv)==0) {
                            vv=V3dUtils.vvOfProteinAsk(p, keyC!=null?keyC:name2class(StrapAlign.defaultClass(ProteinViewer.class)));
                        }
                        for(ProteinViewer v:vv) {
                            if (v==null) continue;
                            a.setOnlyEntry(entry);
                            if (idxOfStrg(w0,Protein3dUtils.allCommands())>=0 && !Protein3dUtils.supports(w0,v)) sbErr.a("Command \"").a(w0).a("\" is not yet supported by ").aln(shrtClasNam(v));
                            else {
                                success=true;
                                V3dUtils.residueSelectionsTo3D(V3dUtils.RSto3D_COMMANDS|V3dUtils.RSto3D_SET_FOCUSED_PV, new ResidueAnnotation[]{a},  new ProteinViewer[]{v});
                            }
                            a.setOnlyEntry(null);
                            V3dUtils.viewerToFront(V3dUtils.OPEN_ADD_TAB, v);
                        }
                        if (!success && sze(sbErr)>0) error(sbErr);
                    }
                } else if (q==TOG) {
                    a.setEnabledE(TOG.s(), entry);
                    fireEditing('S');
                    revalAndRepaintC(_jt);
                    _jt.editCellAt(row,col);
                }
                if (key==BG_IMAGE) {
                    a.findIconImage();
                    changed(true);
                }
            }
            if (cmd==ACTION_FOCUS_LOST || cmd==ACTION_ENTER) {
                a.setValueE(E_NO_UPDATE_UI|E_EDITED, toStrg(q), gcp(Entry.class, q, Entry.class));
            }
        }
    }
    /* <<< ActionListener <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */
    private static boolean isButton(String k) {
        return
            isAssignblFrm(CommandInterpreter.class,StrapPlugins.mapS2L(k)) ||
            k.equals(TEXSHADE) ||
            k==COLOR ||
            k==VIEW3D;
    }
    public Entry entryAt(int row) {
        final ResidueAnnotation a=_a;
        final Entry ee[]=a==null?null:a.entries();
        return 0<=row && row<sze(ee)? ee[row] : null;
    }
    /* <<< Utils <<< */
    /* ---------------------------------------- */
    /* >>> Html >>> */
    public static Object docuView(String id) {
        final Protein p=new Protein();
        p.setName("test_protein");
        final ResidueAnnotation s=new ResidueAnnotation(p);
        p.addResidueSelection(s);
        s.setValue(0, GROUP,"A_group_name");
        s.setValue(0, POS,"6-8");
        if (strchr('B',id)>=0) s.addE(0,BALLOON,"This ballon text is shown if the mouse pointer is over the selection");
        if (strchr('3',id)>=0) s.addE(0,VIEW3D,ProteinViewer.COMMANDspheres);
        if (strchr('T',id)>=0) s.addE(0,TEXSHADE,"\\feature{bottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\\uparrow$}{active site}");
        final ResidueAnnotationView v=new ResidueAnnotationView(s);
        final Component pan=v._panel;
        pcp("",v,pan);
        setPrefSze(false, 555, 222, pan);
        return pan;
    }
}
