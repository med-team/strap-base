package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import java.util.*;
import static charite.christo.ChUtils.*;
/**HELP

This Dialog allows to translate coding nucleotide sequences into amino acid sequences.
Three nucleotides are called a triplet and are translated into one amino acid (WIKI:Genetic_code).

The user specifies the WIKI:Reading_frame and can select translated and untranslated sequence regions.
The coding nucleotides (Exons) are drawn bright white (WIKI:Coding_sequence), while
non-coding nucleotides are shown in gray (WIKI:Introns,  WIKI:Untranslated_region UTRs).

The horizontal scroll-bar outlines the intron-exon structure of DNA.

<br><br>

<b>Change coding/non-coding:</b> By pressing the Insert-key or
Space-bar, nucleotides are turned into coding nucleotides. Those are
drawn bright white.  Backspace and Delete, however, turns translation
int amino acids off and creates or prolongs introns and UTRs. Those
nucleotides are drawn gray.

<br><br>
<b>Navigation:</b>
The keys &larr;, &rarr;, Home, End, PgUp, PgDown and the mouse allow navigation within the DNA sequence.
The Shift-key in conjunction with these keys creates a block selection.

<b>Alt+&rarr;</b> and <b>Alt+&larr;</b> jumps to the next interesting sequence
position. These positions are intron/exon boundaries and selected
nucleotides or selected amino acids.  Typing letter <b>c</b> jumps to
the nucleotide position corresponding to the alignment-cursor
position.  Typing a number followed by letter <b>e</b> goes to the
n-th exon. Typing a number followed by letter 'i' or 'r' goes to the
i-th nucleotide position counted from left or right.

<br><br><b>Num-prefix</b> Some key-stroke can be applied n times.  For
example typing a number followed by Insert will turn the next n
nucleotides into coding nucleotides.

<br><br><b>Zoom:</b>
Ctrl+<b>+</b> and Ctrl+<b>-</b> or Ctrl+Wheel

<br><br><b>Saving/restoring cursor and scroll position:</b> Type a
number followed by upper letter <b>s</b> to store the current position. For
reverting to a saved position type the number followed by
lower letter<b>s</b>.

<br><br><b>Mouse actions:</b>
Left-click an amino acid moves the alignment cursor to the clicked residue.

<i>SEE_DIALOG:DialogGenbank</i>
<i>SEE_DIALOG:DnaExonStart</i>
   @author Christoph Gille
*/
public class EditDna extends AbstractDialogJPanel implements java.awt.event.ActionListener {
    private final JComponent _pEditors=pnl(VB,Box.createVerticalGlue(),pnl());
    private static Object _focus;
    private final Map<Protein,EditDnaTable> _tables=new WeakHashMap();

    void setFocused(EditDnaView e, boolean always) {
        if (deref(_focus)==null || always) _focus=wref(e);
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final EditDnaView e=(EditDnaView)orO(deref(_focus),get(0,ee()));
        final Protein p=e==null ? null:e.p();
        if (p!=null) {
            if (cmd=="O") askOrientation(p);
            if (cmd=="T") {
                EditDnaTable t=_tables.get(p);
                if (t==null) {
                    StrapAlign.addListener(t=new EditDnaTable(p));
                    _tables.put(p,t);
                }
                ChFrame.frame("EditDnaTable", t.getPanel(), 0).shw();
            }
        }
    }
    public EditDna() {
        final Object
            b1=new ChButton("T").li(this).t("Table of exons").tt("start and end of exons as a table"),
            b2=new ChButton("O").li(this).t("Set forward or reverse-complement"),
            pNorth=pnl(HB,b1,b2,"#",smallHelpBut(this));
        pnl(this,CNSEW,_pEditors,pNorth,pnl(REMAINING_VSPC1));
    }
    private EditDnaView[] ee() { return childsR(_pEditors,EditDnaView.class); }
    public void addEditor(Protein p, boolean newInstance) {
        if (p==null) return;
        EditDnaView e=null;
        if (!newInstance) {
            for(EditDnaView ed: ee()) {
                if (ed.p()==p) e=ed;
            }
        }
        if (e==null) {
            if (p.getNucleotides()==null) {
                p.selectCodingStrand(Protein.FORWARD);
                StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
            }
            e=new EditDnaView(p,this);
            _pEditors.add(e.getPanel());
        }
        _focus=wref(e);
        revalAndRepaintC(_pEditors);
    }
    private void askOrientation(Protein p) {
        final ChCombo combo=new ChCombo(Protein.TRANSLATIONS_AS_STRING).s(maxi(0,p.getCodingStrand()));
        final String
            TRANS="Translate nucleotide sequence",
            NO="Dispose existing translation",
            GB="Use CDS expression in sequence file",
            option[]= sze(p.getCDS())>0 ? new String[]{TRANS,NO,GB} : new String[]{TRANS,NO},
            title="Reading frame of "+p;
        final int iChoice=JOptionPane.showOptionDialog(this,pnl("Translation direction: ",combo),title,JOptionPane.DEFAULT_OPTION,JOptionPane.QUESTION_MESSAGE,null,option,null);
        if (iChoice>=0) {
            final String choice=option[iChoice];
            if (choice==NO) p.selectCodingStrand(Protein.NO_TRANSLATE);
            else if (choice==GB)  {
                runR(SPUtils.dialogSetProteins(new Protein[]{p}, StrapAlign.addDialogC(DialogGenbank.class)));
            } else {
                p.selectCodingStrand(combo.i());
                addEditor(p,false);
            }
            StrapEvent.dispatch(StrapEvent.NUCL_TRANSLATION_CHANGED);
        }
    }
}
