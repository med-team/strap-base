java(-jar)                         Mai 2012                         java(-jar)



NNAAMMEE
       java  -jar  alignment2html.jar - Reads amino acid sequences and creates
       an alignment view for web browsers.


SSYYNNOOPPSSIISS
       java -jar alignment2html.jar [_p_r_o_t_e_i_n _o_r _a_l_i_g_n_m_e_n_t _f_i_l_e_s]

       java -jar alignment2html.jar [_p_r_o_t_e_i_n _o_r  _a_l_i_g_n_m_e_n_t  _f_i_l_e_s]  -o=_o_u_t_p_u_t_-
       _f_i_l_e

       java    -jar    alignment2html.jar   [_p_r_o_t_e_i_n   _o_r   _a_l_i_g_n_m_e_n_t   _f_i_l_e_s]
       -script="align *"




DDEESSCCRRIIPPTTIIOONN
       Amino acid sequences are loaded from input files or are  defined  using
       the  scripting  languages.   A html file with the rendered alignment is
       written. It can be viewed in a web browser.




IINNPPUUTT FFIILLEESS
       The amino acid sequences are extracted from alignment files and protein
       files.   A  large number of different formats are supported: PDB, Swis‐
       sprot, Embl, Genbank, GenPept,  Fasta,  multiple-Fast,  Pir,  ClustalW,
       MSF, Nexus, Prodom, Pfam, Stockholm and HSSP.


   FFiillee CCoommpprreessssiioonn
       Supported are .gz, .bz2 and .Z.


   PPaarrttss ooff pprrootteeiinnss
       To  load  only a subset of a proteins, a suffix is appended to the name
       of the protein input file.  Residue subsets are specified by an  excla‐
       mation  mark  and  a subset expression after the protein file. Example:
       "hslv_ecoli.swiss!20-30,50-66". Residue numbering starts with  one.  To
       refer  to  pdb-residue  numbers  use  the  Rasmol/Jmol  syntax  _P_d_b_R_e_s_‐
       _N_u_m:_C_h_a_i_n_L_e_t_t_e_r. Example: "pdb1ryp.ent!50:A-66:A".


OOPPTTIIOONNSS
       --oo=_o_u_t_p_u_t_-_f_i_l_e Specifies an output file

   AAlliiggnnmmeenntt ffoorrmmaatt
       Comma separated display options can be appended  directly  (!no  space)
       after the output file name.

        w=_r_e_s_i_d_u_e_s _p_e_r _l_i_n_e  Setting output width

        r=_c_o_l_u_m_n _r_a_n_g_e  Example: 20-30

        r=_r_e_s_i_d_u_e _p_o_s_i_t_i_o_n _r_a_n_g_e  Example: aProtein/10-20



       --ssccrriipptt=_s_c_r_i_p_t_-_l_i_n_e --ssccrriipptt=_f_i_l_e or --ssccrriipptt=_n_a_m_e_d___p_i_p_e --ssccrriipptt=_U_R_L Each
       script line starts with a strap command and may be  followed  by  argu‐
       ments.  The script is run line by line.  For example the line "align *"
       aligns all proteins using  the  current  alignment  backends  (Default:
       ClustalW,  TM-align).   Instead  of computing the alignment, the gapped
       sequences  can  be   directly   defined   with   the   script   command
       "aa_sequence".  In this case no sequence files need to be given and the
       align command is not required.  In HTML output various information  can
       be  specified: Important positions can be highlighted, an icon for each
       protein and the secondary structure to be displayed.  This  information
       is  provided  with  script  commands.  A list of all script commands is
       printed with the program parameter -help=script.  See http://www.bioin‐
       formatics.org/strap/alignment-to-html.html for details.

       If  computations  are  performed within the script and if more than one
       Strap instances are running at the same time,  then  the  default  file
       based  cache is not applicable and the  SQL database based cache should
       be   used.     This    is    described    in    http://www.bioinformat‐
       ics.org/strap/cache.html.




   RReessiidduuee aannnnoottaattiioonnss iinn HHTTMMLL aalliiggnnmmeenntt oouuttppuutt..
       Residue  positions  to  be  highlighted  in  the html output need to be
       defined with  script  commands.  See  option  --ssccrriipptt==_S_r_i_p_t_-_f_i_l_e.   The
       script  command  _n_e_w___a_m_i_n_o_a_c_i_d___s_e_l_e_c_t_i_o_n is used to create a new selec‐
       tion object and the commands _a_d_d___a_n_n_o_t_a_t_i_o_n or _s_e_t___a_n_n_o_t_a_t_i_o_n are  used
       to  attach  information.   The  script  may  also be used to define the
       [gapped] amino acid sequences.



       --ss33dd=_J_a_v_a_C_l_a_s_s

       Sets the Java class for superimposing proteins.

       The short name or the  full  Java  class  name  can  be  used  such  as
       "tm_align" for "Superimpose_TM_align".

       Example: -s3d=tm_align or -s3d=ce or -s3d=gangstaplus

       --aa33dd=_J_a_v_a_C_l_a_s_s

       Sets the Java class to compute structure based sequence alignments.

       Examples: -a3d=tm_align or -a3d=mapsci or -a3d=matt or  -a3d=mustang


       --aalliiggnneerr22=_J_a_v_a_C_l_a_s_s

       Sets  the  Java class for aligning two amino acid sequences. Preferably
       an implementation in Java because native  executables  take  longer  to
       start.  Default value is PairAlignerNeoBioPROXY.

       Example: -aligner2=MultipleAlignerClustalW


       --aalliiggnneerrPP=_J_a_v_a_C_l_a_s_s

       Sets the Java class for aligning sequences and structures.  The default
       is clustalW.

       Example: -alignerP=t_coffee


PPaarraammeetteerrss ffoorr tthhee JJaavvaa vviirrttuuaall mmaacchhiinnee
       If the java process is started from a script file,  parameters  can  be
       passed directly to the Java command.  Those parameters need to be  pre‐
       fixed with "JVM_".  This does not (yet) work for MS-Windows.


       Examples:

        JJVVMM__--XXmmxx220000MM   Setting the heap size to 200 Megabyte.
        JJVVMM__--DDaappppllee..llaaff..uusseeSSccrreeeennMMeennuuBBaarr==ttrruuee   Screen menu bar for MacOS
        JJVVMM__--DDpprrooxxyyHHoosstt==rreeaallpprrooxxyy..cchhaarriittee..ddee   JJVVMM__--DDpprrooxxyyPPoorrtt==888888     Setting
       the web proxy
        JJVVMM__--eeaa   Enable assertions







HHOOMMEE--PPAAGGEE
       http://3d-alignment.eu/

       http://www.bioinformatics.org/strap/



CCOOPPYYRRIIGGHHTT
       Christoph Gille © 1999-2012

       License GPL




1                             alignment2html.jar                    java(-jar)
