package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.SPUtils.sp;
/**
   @author Christoph Gille
 */
public class ResidueSelectionPopup implements ActionListener, IsEnabled {
    public final static String
        ACTION_edit="^Edit annotations ...",
        ACTION_MK_RESANO="Annotate, make persistent residue selection ...";
    private final ContextObjects _objects;
    private final Component LAB_TITLE[]=new JComponent['z'];
    final ChButton COLOR_BUTTON=new ButColor(0,C(0xffFF00),this).li(this).t("^Color ...");

    public ResidueSelectionPopup(ContextObjects hs) { _objects=hs;}
    private JPopupMenu _popup[]=new JPopupMenu['z'];
    private String _title;

    public JPopupMenu menu(char mID) {
        final int n=_objects.residueSelections(mID).length;
        _title=mID=='A' ? "Annotation" : mID=='F' ? "Sequence feature" :  "Residue-selection";
        if (LAB_TITLE[mID]==null) {
            LAB_TITLE[mID]=menuInfo(0,"");
            _popup[mID]=jPopupMenu(0,_title+"-contextmenu", getMenuObjects(mID));
        }
        setTxt(plrl(n,"Menu for %N "+_title+"%S"), LAB_TITLE[mID]);
        return _popup[mID];
    }
    public final static int WIKI_D=5, BUT_EXEC=6, WIKI_3D=7;
    private final static int HIDE_MASK=1<<16, SHOW_MASK=1<<17,
        BUT_ACTIVATE=1, BUT_HIDDEN=2, BUT_AD_CURS=3, BUT_RM_CURS=4,
        PAN_STYLE=11,
        MENU_CURSOR=23, MENU_COPY=24, MENU_APPEARANCE=25, MENU_3D=26, MENU_SELECT=27, MENU_APPLY=28;

    private static ChButton _sBb[]=new ChButton[99];
    protected static ChButton staticBut(int t) {
        ChButton b=_sBb[t];
        if (b==null) {
            if (t==WIKI_3D || t==WIKI_D) {
                final String
                    toCopy="to copy the residue selection to",
                    usingDnD=" using Drag-and-Drop ...";
                final Object
                    wikiDnD[]={"You can use","WIKI:Drag_and_drop"},
                    msgInfoDrag3D=pnl(VBPNL,pnl(wikiDnD),toCopy," a 3D-View",WATCH_MOVIE+MOVIE_Sequence_Features_in_3D),
                        msgInfoDrag=pnl(VBPNL,pnl(wikiDnD),toCopy," another protein", WATCH_MOVIE+MOVIE_Sequence_Features_in_3D);
                    if (t==WIKI_3D) b=ChButton.doView(msgInfoDrag3D).t("Copy to 3D-Viewer"+usingDnD).i(IC_COPY);
                    if (t==WIKI_D) b=ChButton.doView(msgInfoDrag).t("Copy"+usingDnD).i(IC_DND);
            } else if (t==BUT_HIDDEN) {
                final String msg=
                    "Note: Invisible selections can be accessed in the object tree.<br>"+
                    "The object tree is beyond the vertical divider at the left.";
                b=ChButton.doView(msg).t("Access hidden selections");
            } else assrt();
            _sBb[t]=b;
        }
        return b;
    }

    private Object _bb[]=new Object[99];
    public ChButton b(int t) { return (ChButton)jc(t);}
    private Object jc(int t) {
        final boolean hide=0!=(t&HIDE_MASK);
        if (0!=((HIDE_MASK|SHOW_MASK)&t)) {
            final int h=t&~(HIDE_MASK|SHOW_MASK);
            final String where=
                h==VisibleIn123.SB ? " in scroll-bar" :
                h==VisibleIn123.SEQUENCE ?" in alignment panel":
                h==VisibleIn123.STRUCTURE ? " in 3D backbone" :
                h==VisibleIn123.HTML ? " in HTML export" :
                " everywhere";
            return new ChButton(t+IC_SHOW).i(hide?null:IC_SHOW).t((hide ?"Hide ":"Show ")+where).li(this).cp(KEY_ENABLED,this);
        }

        Object b=_bb[t];
        if (b==null) {
            if (t==PAN_STYLE) {
                final ChButton but=new ChButton("STYLE_APPLY").t("Apply").li(this).cp(KEY_ENABLED,this);
                updateOn(CHANGED_SELECTED_OBJECTS,but);
                b=pnl(VBHB, "Style of residue annotations",pnl(HBL, AbstractVisibleIn123.newComboStyle(),but));
            } else if (t==BUT_ACTIVATE) {
                final ActivateDeactivate actDeact=new ActivateDeactivate(_objects);
                b=b("", "Activate / ^Deactivate single entries of residue annotations ... ").li(actDeact).cp(KOPT_HIDE_IF_DISABLED,"").cp(actDeact,actDeact);
            } else if (t==BUT_EXEC) {
                b=b("", "... on residue selections ...").i(IC_BATCH)
                    .tt("A program is executed with the amino acid sequences as the command line arguments.<br>The selected positions are upper case, the remaining are lower case.");
            } else if (t==BUT_AD_CURS) {
                b=b("","^Add cursor position").i(IC_TEXT_CURSOR);
            } else if (t==BUT_RM_CURS) {
                b=b("","^Remove cursor position").i(IC_TEXT_CURSOR);
            } else if (t==MENU_SELECT) {
                final String ttAct="In the table view the first toggle button indicates whether the selection is activated or deactivated.";
                b=new Object[] {
                    "^Select", "i", IC_SELECT,
                    b("SEL","Select").i(IC_SELECT),
                    b("DES","Deselect").i(IC_SELECT+ChIcon.SFX_CROSS_OUT),
                    StrapAlign.button(StrapAlign.BUT_SELECT_BY_NAME),
                    "Hint: Also try to Ctrl+left-click underlined residues in the alignment pane.",
                    "Selected residue selections are indicated by marching ants.",
                    "-",
                    new Object[]{
                        "i",IC_SHOW,
                        "Show / Hide",
                        b("ACTIV","Activate").i(IC_SHOW).tt(ttAct).cp(KOPT_HIDE_IF_DISABLED,""),
                        " ",
                        jc(SHOW_MASK|VisibleIn123.SEQUENCE),
                        jc(SHOW_MASK|VisibleIn123.SB),
                        jc(SHOW_MASK|VisibleIn123.STRUCTURE),
                        jc(SHOW_MASK|VisibleIn123.HTML),
                        jc(SHOW_MASK),
                        "-",
                        "Hide",
                        b("DEACT","^Deactivate").tt(ttAct).cp(KOPT_HIDE_IF_DISABLED,""),
                        " ",
                        jc(HIDE_MASK|VisibleIn123.SEQUENCE),
                        jc(HIDE_MASK|VisibleIn123.SB),
                        jc(HIDE_MASK|VisibleIn123.STRUCTURE),
                        jc(HIDE_MASK|VisibleIn123.HTML),
                        b(HIDE_MASK),
                        "-",
                        b("AH","Access ^hidden")
                    },
                    " ",
                    jc(BUT_ACTIVATE),
                };
            } else if (t==MENU_APPEARANCE) {
               b=new Object[] {
                    "^Appearance",
                    "i",IC_SHOW,
                    b("STYLE","^Style like \"Underline\" or \"Fill background\" ...").i(IC_UNDERLINE),
                    COLOR_BUTTON,
                    b("UPPER","To ^upper case"),
                    b("LOWER","To ^lower case"),
               };
            } else if (t==MENU_CURSOR) {
                b=new  Object[]{
                    "Alignment c^ursor", "i", IC_TEXT_CURSOR,
                    b("GO1","Alignment cursor to ^1st selected residue").i(IC_TO_START),
                    b("GO9","Alignment cursor to ^last selected residue").i(IC_TO_END),
                    "-",
                    jc(BUT_AD_CURS),
                    jc(BUT_RM_CURS),
                };
            } else if (t==MENU_3D) {
                b=new Object[] {
                    "i", IC_3D,
                    "^3D",
                     b("TO3D","Amino acid selections to all ^3D-views of that protein ...").i(IC_JMOL)
                    .tt("In case the respective protein is viewed three-dimensionally<br>the residues are selected in that 3D-viewer."),
                    staticBut(WIKI_3D),
                    "-",
                    "^",CLASS_DialogSelectionOfResiduesMain,
                    "-",
                    b("PDB","Make ^PDB-file of selected c-Alpha positions").i(IC_3D),
                };
            } else if (t==MENU_COPY) {
                b=new Object[]{
                    "Copy",
                    "i",IC_COPY,
                    b("CN",null),
                    b("CS",null),
                    b("CP",null),
                    b("CR",null),
                    b("COPY","Copy ^annotation[s] to selected protein[s]").i(IC_COPY),
                    staticBut(WIKI_D)
                };
            } else if (t==MENU_APPLY) {
                    b=new Object[] {
                    "^Apply / Export ", "i", IC_EXPORT,
                    jc(MENU_CURSOR),
                    jc(MENU_3D),
                    jc(MENU_COPY),
                    b("AS_STRING","Report selected positions").tt("The amino acids of a residue selection as a string.").i(IC_INFO),
                    b("AS_RANGE","Report first and last selected ^positions").i(IC_1).i(IC_INFO),
                    b("CROP", "Crop amino acid sequence ... ").i(IC_SCISSOR),
                    !Insecure.EXEC_ALLOWED?null: new Object[]{"i",IC_BATCH,"Execute external program ", b(BUT_EXEC)}
                };
            } else assrt();
            _bb[t]=b;
        }
        return b instanceof Object[] ? jMenu(0, (Object[])b, "ResSelPopup") : b;
    }
    private ChButton b(String id, String label) {
        String t=label, ic=null;
        if (id=="CS") {t="^Copy selected sequence"; ic=IC_CLIPBOARD;}
        if (id=="CN") {t="Copy ^name"; ic=IC_CLIPBOARD;}
        if (id=="CP") {t="Copy selected residue numbers"; ic=IC_CLIPBOARD;}
        if (id=="CR") {t="Copy residue range (first to last position)"; ic=IC_CLIPBOARD;}
        return new ChButton(orS(id,t)).t(t).i(ic).li(this).cp(KEY_ENABLED,this);
    }

    private final Object[] _menuObjects[]=new Object['z'][];
    public Object[] getMenuObjects(char mID) {
        if (_menuObjects[mID]==null) {
            final Object
                top= mID==' ' ? null : pnl(CNSEW,pnl(LAB_TITLE[mID]),null, null,null, smallHelpBut(mID=='s' ? ResidueSelection.class : ResidueAnnotation.class)),
                bL= b("L",ProteinPopup.ACTION_showInList).i(IC_LIST);
            if (mID=='S') {
                _menuObjects[mID]=new Object[] {
                    top,
                    bL,
                    "^",
                    b(null,ACTION_MK_RESANO).i(IC_ANNO),
                    jc(MENU_SELECT),
                    jc(MENU_COPY),
                    jc(MENU_APPEARANCE),
                    jc(MENU_APPLY),
                    " ",
                    jc(MENU_CURSOR),
                    "-",
                    b(IC_CLOSE,"^Dispose residue selection[s]").i(IC_KILL)
                };
            }
            if (mID=='A' || mID=='F' || mID=='s') {
                _menuObjects[mID]=new Object[] {
                    "? context-menu for residue selections",null,
                    top,
                    bL,
                    "-",
                    mID=='s' ? b(null,ACTION_MK_RESANO).i(IC_ANNO) : null,
                    new Object[]{
                        "^Edit","i",IC_EDIT,
                        COLOR_BUTTON,
                        "-",
                        "Alignment cursor",
                       jc(BUT_AD_CURS),
                       jc(BUT_RM_CURS),
                        "-",
                        "Edit Annotations",
                        b(null,ACTION_edit).i(IC_ANNO),
                        b("+ROW","Add a^nnotations ...").i(IC_ANNO),
                        jc(BUT_ACTIVATE),
                        "Advanced",
                        CLASS_DialogResidueAnnotationChanges
                    },
                    jc(MENU_COPY),
                    jc(MENU_APPEARANCE),
                    jc(MENU_SELECT),
                    "-",
                    jc(MENU_APPLY),
                    jc(MENU_3D),
                    new Object[]{
                        "Sequence feature", "i", IC_UNDERLINE,
                        b("INC","Report related features that are not displayed").i(IC_INFO),
                        KOPT_DISABLE_IF_CHILDS_DISABLED
                    },
                    "-",
                    b(IC_KILL,"^Delete ...").i(IC_KILL).tt("Kill the annotation[s].<br>After saving the project<br>it/they will be erased permanently<br>because all annotations of a protein are stored in a single file")
                };
            }
            StrapAlign.replaceClassByButton(_menuObjects[mID],null);
        }
        return _menuObjects[mID];
    }
    public boolean isEnabled(Object o) {
        final ResidueSelection ss[]=_objects==null?null: _objects.residueSelections('s');
        boolean hasA=false, hasF=false, hasS=false, hasBS=false, hasInc=false;
        for(int i=sze(ss); --i>=0;) {
            hasBS=hasBS||ss[i] instanceof BasicResidueSelection;
            final char type=ResSelUtils.type(ss[i]);
            hasA|=type=='A';
            hasF|=type=='F';
            hasS|=type!='A' && type!='F';
            hasInc|=type=='F'&& sze(((ResidueAnnotation)ss[i]).includedFeatures())>0;
        }
        if (o==COLOR_BUTTON) return hasA||hasS;
        if (o instanceof ChButton) {
            final String cmd=((ChButton)o).getActionCommand();
            if (cmd==ACTION_MK_RESANO) return hasS;
            if (
                o==_bb[BUT_ACTIVATE] ||
                cmd=="ACTIV" || cmd=="DEACT" || cmd=="ACTIV" || cmd=="DEACT" ||
                cmd==IC_KILL || cmd=="+ROW" || cmd==ACTION_edit) {
                return hasA||hasF;
            }

            if (o==_bb[BUT_RM_CURS] || o==_bb[BUT_RM_CURS]) return hasF || hasA || hasBS;

            if (cmd=="STYLE_APPLY") return hasA || idxOfInst(0L,  ResidueSelection.class, getCntxtObj())>=0;
            if (cmd=="STYLE") return idxOfInst(0L, VisibleIn123.class, ss)>=0;
            if (cmd=="INC") return hasInc;
        }
        return true;
    }
    private static int _countJList;
    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        final List vSel=StrapAlign.selectedObjectsV();
        final int vSelMC=modic(vSel), iBut=q==null?-1:idxOf(q,_bb);
        if (_objects==null) {
            if (cmd=="OCM") {
                final List v=new ArrayList();
                final boolean types[]=new boolean['S'+1];
                for(Object s :  _vSel.toArray()) {
                    final char t=ResSelUtils.type(s);
                    if (isSelctd(get(ResSelUtils.type(s), _cbTyp))) {
                        v.add(s);
                        types[t]=true;
                    }
                }
                closeWithCtxtMenu(parentWndw(q));
                StrapAlign.showContextMenu(countTrue(types)>1?'s' : (char)fstTrue(types), v.toArray());
            }
            return;
        }
        ResidueSelection[] ss=_objects.residueSelections('s');
        if (ss.length==0 && (q==COLOR_BUTTON || cmd=="STYLE_APPLY")) ss=derefArray( getCntxtObj(), ResidueSelection.class);
        boolean changed=false, featureColor=false;
        Color changedColor=null;
        if (cmd=="SEL") adAllUniq(ss,vSel);
        if (cmd=="DES") rmAll(ss,vSel);
        if (cmd=="AH") StrapAlign.viewTree(null);
        if (cmd=="GO9" || cmd=="GO1") {
            final boolean bb[]=ss[0].getSelectedAminoacids();
            final Protein p=sp(ss[0]);
            final int iA=ss[0].getSelectedAminoacidsOffset()+ (cmd=="GO1" ? fstTrue(bb) : lstTrue(bb));
            StrapAlign.setCursor(p,iA);
            StrapAlign.animatePositionZ(StrapAlign.ANIM_SET_CURS_SCROLL_TO_VISIBLE, p, iA);
        }
        if (cmd=="L")  {
            StrapAlign.showInJList(0L,null, ss,_title,null).showInFrame(ChFrame.AT_CLICK|ChFrame.ALWAYS_ON_TOP, _title+ " "+ ++_countJList);
            for(JPopupMenu m : _popup) setVisblC(false,m,0);
        }
        if (cmd=="TO3D") V3dUtils.residueSelectionsTo3D_I(V3dUtils.RSto3D_SAME_PROTEIN|V3dUtils.RSto3D_NO_WIRE,ss, (ProteinViewer[])null);
        for (ResidueSelection s0 : ss) {
            if (!(s0 instanceof VisibleIn123)) continue;
            final VisibleIn123 s= (VisibleIn123)s0;
            if (endWith(IC_SHOW,cmd)) {
                final int t=atoi(cmd);
                final int h=t & ~(HIDE_MASK|SHOW_MASK);
                final int visible=s.getVisibleWhere();
                final int visibleNew=0!=(t&HIDE_MASK) ? (visible&~h) : h==0?0xFFffFF : (visible|h);
                if (visibleNew!=visible) {
                    changed=true;
                    s.setVisibleWhere(visibleNew);
                }
            }
            if (s instanceof ResidueAnnotation && (cmd=="ACTIV" || cmd=="DEACT")) {
                changed=true;
                ((ResidueAnnotation)s).setEnabled(cmd=="ACTIV");
            }
        }
        if (cmd=="INC") {
            final BA sb=new BA(9999);
            int count=0;
            for(ResidueSelection s : ss) {
                final ResidueAnnotation a=deref(s,ResidueAnnotation.class);
                final String inc[]=ResSelUtils.type(a)!='F' ? null: strgArry(a.includedFeatures());
                if (sze(inc)==0) continue;
                if (0<count++) sb.a('\n',2);
                sb.a(ANSI_W_ON_B).a(a.getRenderer(HasRenderer.JLIST,null)).aln(ANSI_RESET).join(inc,"\n");
            }
            shwTxtInW("Included sequence features",sb);
        }
        if (cmd=="STYLE") ChFrame.frame("Style", jc(PAN_STYLE), ChFrame.PACK|ChFrame.ALWAYS_ON_TOP).shw(ChFrame.AT_CLICK|ChFrame.DRAG);
        if (cmd=="STYLE_APPLY") {
            final int i=child( (JComponent)jc(PAN_STYLE),ChCombo.class).i();
            for(ResidueSelection s : ss) {
                if (s instanceof VisibleIn123 && i!=((VisibleIn123)s).getStyle()) {
                    ((VisibleIn123)s).setStyle(i);
                    changedColor=C(0);
                }
            }

        }

        else if (cmd=="COPY") {
            final Protein pp[]=StrapAlign.selectedProteins();
            if (!ChMsg.noProteinsSelected(pp)) ResSelUtils.copyResidueSelections(ResSelUtils.COPY_ADD_TO_PROTEIN|ResSelUtils.COPY_REPORT, ss, pp);
        }
        if (cmd==IC_KILL || cmd=="COPY" ||cmd=="+ROW" || cmd==ACTION_edit) {
            final ResidueAnnotation aa[]=derefArray(ss,ResidueAnnotation.class);
            if (aa.length>0) {
                if (cmd==IC_KILL) ResSelUtils.killResidueAnnotation(aa);
                else if (cmd=="+ROW") ChFrame.frame("add row",new AddAnnotation(aa),ChFrame.CENTER).shw(0);
                else if (cmd==ACTION_edit) {
                    if (aa.length>3 && !ChMsg.yesNo("Really edit "+aa.length+" residue annotations?")) return;
                    for(ResidueAnnotation a : aa) StrapAlign.editAnnotation(a);
                }
            }
        }
        /* --- selections --- */
        if (cmd=="PDB") {
            final List<Protein> vP=new ArrayList();
            for(ResidueSelection s:ss) adUniq(sp(s), vP);
            for(Protein p: toArry(vP, Protein.class)) {
                final boolean selected[]=new boolean[p.countResidues()];
                final BA sb=new BA(99999).a("REMARK protein=").aln(p);
                for(ResidueSelection s:ss) {
                    if (p!=sp(s)) continue;
                    if (p==null || p.getResidueCalpha()==null) { sb.a("REMARK ").a(p).aln(" no C-alpha - skipped"); continue;}
                    p.loadSideChainAtoms(null);
                    final boolean bb[]=s.getSelectedAminoacids();
                    final int offset=s.getSelectedAminoacidsOffset();
                    for(int i=mini(selected.length,bb.length+offset); --i>=0;)
                        if (i-offset>=0 && bb[i-offset]) selected[i]=true;
                }
                final long mode=ProteinWriter.PDB | ProteinWriter.ATOM_LINES | (p.getAtomCoordinates()!=null ? ProteinWriter.SIDE_CHAIN_ATOMS : 0) | ProteinWriter.SEQRES | ProteinWriter.HELIX_SHEET;
                final ProteinWriter1 pw=new ProteinWriter1();
                pw.selectResidues(selected);
                final File f=file(STRAPOUT+"/selectedAsPdb/"+p+".pdb");
                sb.a("REMARK ").aln(f);
                pw.toText(p, new Matrix3D[]{p.getRotationAndTranslation()}, mode,sb);
                wrte(f,sb);
                new ChTextView(f).tools().showInFrame("Written cAlpha coordinats");
            }
        }
        if (iBut==BUT_EXEC) {
            final String para[]=new String[ss.length];
            for(int j=ss.length; --j>=0;) para[j]=ResSelUtils.selectedToUpperCase(ss[j], ANSI_FG_RED, ANSI_RESET);
            new DialogExecuteOnFile(Customize.customize(Customize.execResidueSelection)).show(para);
        }
        if (cmd=="CS" || cmd=="CN" || cmd=="CP" || cmd=="CR") {
            final BA sb=new BA(333);
            for(ResidueSelection s:ss) {
                final Protein p=sp(s);
                if (p==null) continue;
                if (cmd=="CS") ResSelUtils.selectedAsString(s,sb);
                else {
                    sb.a(p).a('/');
                    if (cmd=="CN" && sze(nam(s))>0) sb.a(nam(s));
                    else if (cmd=="CR") sb.a(ResSelUtils.firstAmino(s)).a('-').a(ResSelUtils.lastAmino(s));
                    else sb.boolToText(s.getSelectedAminoacids(), 1+Protein.firstResIdx(p)+s.getSelectedAminoacidsOffset(),",","-");

                }
                sb.a('\n');
            }
            toClipbd(sb);
        }
        if (cmd=="CROP") ResSelUtils.cropSequences(0, ss);
        if (cmd=="AS_RANGE") {
            final BA sb=ResSelUtils.listFromTo(ss, 0, new BA(999));
            if (sze(sb)>0) shwTxtInW(txtForTitle(q), sb);
        }
        if (cmd=="AS_STRING")  {
            final BA sb=new BA(999);
            for(ResidueSelection s:ss) {
                sb.aln("<pre class=\"data\">");
                final Protein p=sp(s);
                if (p==null) continue;
                sb.a(p).a('\t').or(nam(s),s).a('\t');
                ResSelUtils.selectedAsString(s,sb).aln("\n</pre>");
                for(int i012=3; --i012>=0;) {
                    final int nn[]=p.getResidueNumber();
                    if (i012==2) {
                        if (nn==null) continue;
                        sb.a("<b>PDB-numbers: </b>");
                    } else sb.a("<b>Indices start counting with ").a(i012).a(": </b> ");
                    final boolean bb[]=s.getSelectedAminoacids();
                    final int offset=ResSelUtils.selAminoOffsetZ(s);
                    for(int iB=0;iB<bb.length;iB++)
                        if (bb[iB]) {
                            final int i=iB+offset;
                            if (i012==2) {  if (i>=0 && i<nn.length && nn[i]!=MIN_INT) sb.a(nn[i]).a(':').a((char)p.getResidueChain(i)).a(", ");}
                            else sb.a(i+i012).a(',');
                        }
                    sb.aln("<br>");
                }
                sb.aln("<br>");
            }
            new ChJTextPane(sb).tools().showInFrame("Selected residues");
        }
        if (cmd==ACTION_MK_RESANO && sze(ss)>0) {
            final ResidueSelection ss2[]=ss.clone();
            for(int i=sze(ss2); --i>=0; ) {
                if (ResSelUtils.type(ss2[i])=='A') ss2[i]=null;
                //nam(ss2[i])==ResidueSelection.NAME_STANDARD ||
            }
            ResSelUtils.toResidueAnnotation(countNotNull(ss2)<5, ss2);
        }
        for(ResidueSelection s:ss) {
            final Protein p=sp(s);
            if (p==null) continue;
            final boolean bb[]=s.getSelectedAminoacids();
            final int offset=s.getSelectedAminoacidsOffset();
            if (cmd==IC_CLOSE) {
                p.removeResidueSelection(s);
                changed=true;
                dispos(s);
            }
            if (iBut==BUT_AD_CURS || iBut==BUT_RM_CURS)  {
                changed=BasicResidueSelection.setAminoSelected(iBut==BUT_AD_CURS, p.column2indexZ(StrapAlign.cursorColumn())+Protein.firstResIdx(p), s);
            }
            if (q==COLOR_BUTTON) {
                if (cmd==ACTION_COLOR_CHANGED) {
                    if (s instanceof Colored) ((Colored)s).setColor(changedColor=colr(COLOR_BUTTON));
                } else if (ResSelUtils.type(s)=='F') featureColor=true;
            }
            if (cmd=="UPPER" || cmd=="LOWER") {
                final byte aa[]=p.getResidueTypeFullLength();
                for(int i=p.countResidues(); --i>=0;) {
                    if (get(i-offset,bb)) aa[i]=(byte) (cmd=="UPPER" ? aa[i]&~32 : aa[i]|32);
                }
                p.setResidueType(aa);
                changed=true;
            }
        }
        if (featureColor) Customize.addDialog(Customize.customize(Customize.seqFeatureColors));
        if (changed) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        if (changedColor!=null) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR,111);
        if (vSelMC!=modic(vSel)) StrapEvent.dispatch(StrapEvent.OBJECTS_SELECTED);
    }
    /* <<< actionPerformed <<< */
    /* ---------------------------------------- */
    /* >>> Ask for type of Context Menu >>> */
    private static JComponent _pnlType;
    private final static ChButton _cbTyp[]=new ChButton['S'+1];
    private static ActionListener _actLi;
    private final static List<ResidueSelection> _vSel=new ArrayList();
    public static void showContextMenu(ResidueSelection...ss) {
        if (countNotNull(ss)==0) return;
        if (_pnlType==null) {
            _pnlType=pnl(
                         new GridLayout(4,1),
                         (_cbTyp['S']=toggl(ChButton.PAINT_IF_ENABLED,"Residue selections")).cb(),
                         (_cbTyp['A']=toggl(ChButton.PAINT_IF_ENABLED,"Annotated residue selections")).cb(),
                         (_cbTyp['F']=toggl(ChButton.PAINT_IF_ENABLED,"Sequence features")).cb(),
                         pnl(new ChButton("OCM").li(_actLi=new ResidueSelectionPopup(null)).t("Open Context Menu"))
                         );
        }
        final boolean typ[]=new boolean[_cbTyp.length];
        for(int i=ss.length; --i>=0;) typ[ContextObjects.menuID(ss[i])]=true;
        _vSel.clear();
        adAllUniq(ss,_vSel);
        if(countTrue(typ)==1) StrapAlign.showContextMenu((char)fstTrue(typ), _vSel.toArray());
        else {
            for(int i=_cbTyp.length; --i>=0;) if (_cbTyp[i]!=null) _cbTyp[i].s(typ[i]).setEnabled(typ[i]);
            ChFrame.frame("Context Menu",_pnlType,ChFrame.PACK).shw(ChFrame.AT_CLICK|ChFrame.ALWAYS_ON_TOP);
        }
    }
}
