package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.extensions.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import javax.swing.*;
/**HELP
   Combining sequence alignment methods and 3D-superposition.
   If at least for two of the sequences to be aligned have 3D-coordinates and not all sequences have
   attached 3D-structures,
   this class combines a sequence alignment method such as ClustalW or T_coffee with
   a 3D-alignment method such as  TM_align or CE.

   Otherwise only the standard sequence alignment method or 3D-method is run.
   <br><b>Default setting: </b> ClustalW + CE
   <br><b>Program line options to set t_coffee and TM_align</b>: -alignerP=t_coffee -a3d=TM_align

*/
public class Aligner3D implements CanBeStopped, NeedsProteins, SequenceAlignerSorting, HasControlPanel, HasSharedControlPanel, java.awt.event.ActionListener {
    private long _opt;
    private byte[][] _sequences, _aligned;
    private SequenceAlignerTakesProfile _clustalW;
    private SequenceAligner _align3D;
    private Protein _pp[];
    public void setOptions(long flags){ _opt=flags;}
    public long getOptions(){ return _opt;}
    public long getPropertyFlags(){ return 0L;}
    private JComponent _ctrl;
    private int _ctrlInit;
    @Override public Object getControlPanel(boolean real) {
        if (_ctrl==null) _ctrl=pnl(new java.awt.GridLayout(2,1));
        for(int i=2; --i>=0;) {
            final Object a=i==0?_clustalW:_align3D;
            if (a!=null && (_ctrlInit&(1<<i))==0) {
                _ctrlInit|=(1<<i);
                final Object
                    cached=gcp(KEY_CACHE_TEXT,a),
                    info=(i==0?"Multiple sequence alignment: "+niceShrtClassNam(a)+" ":"3D-alignment "+niceShrtClassNam(_class3D)+" "),
                    but=sze(cached)>0 ? ChButton.doView(cached).t("In cache") : ChButton.doCtrl(a);
                _ctrl.add(pnl(HBL,info, but));
            }
        }
        return _ctrl;
    }
    public float getScore() {
        return _clustalW instanceof HasScore ? ((HasScore)_clustalW).getScore() :
            _align3D instanceof Superimpose_CEPROXY ? ((Superimpose_CEPROXY)_align3D).getScore() :Float.NaN;
    }
    {vALIGNMENTS.add(wref(this));}
    private boolean stopped;
    public void stop() {
        stopped=true;
        if (_clustalW instanceof CanBeStopped) ((CanBeStopped)_clustalW).stop();
        if (_align3D instanceof CanBeStopped) ((CanBeStopped)_align3D).stop();
    }
    public Aligner3D setClass3D(Class c) {
        if (c!=null) _class3D=c;
        return this;
    }
    private Class _class3D=name2class(StrapAlign.defaultClass(SequenceAligner3D.class));
    public void compute() {
        final Protein[] pp=_pp;
        Protein[] pp3D=null;
        for(int iP=sze(pp); --iP>=0;)  {
            if (!trueOrEmpty(gcp(SequenceAligner.KOPT_NOT_USE_STRUCTURE,pp[iP])) && pp[iP].countCalpha()>=Superimpose_CEPROXY.MIN_CALPHA) {
                if (pp3D==null) pp3D=new Protein[pp.length];
                pp3D[iP]=pp[iP];
            }
        }
        if (pp3D!=null) pp3D=rmNullA(pp3D, Protein.class);
        byte[][] profile=null;
        if (sze(pp3D)>=2) {
            //if (stopped) { putln("Aligner3D stopped"); return; }
            //putln(ANSI_GREEN+"Aligner3D  "+ANSI_RESET+"   clazz="+_clazz+" class3D="+ class3D);

            final SequenceAligner inst3D=(SequenceAligner)mkInstance(_class3D);
            if (inst3D!=null) {
                inst3D.setOptions(_opt);
                _align3D=
                    pp3D.length==2 || 0==(inst3D.getPropertyFlags()&PROPERTY_ONLY_TWO_SEQUENCES) ? inst3D :
                    new MultiFromPairAligner(_class3D);
                addActLi(this,_align3D);
                SPUtils.setSeqs(_align3D, pp3D);
                if (stopped) return;
                _align3D.compute();
                profile=_align3D.getAlignedSequences();
                if (pp3D.length==pp.length) {
                    _aligned=profile;
                    return;
                }
            }
        }
        if (stopped) return;
        final Class c=name2class(StrapAlign.defaultClass(SequenceAlignerTakesProfile.class));
        try {
            _clustalW=(SequenceAlignerTakesProfile)c.newInstance();
        } catch(Exception ex){}
        if (_clustalW==null)  _clustalW=new MultipleAlignerClustalW();
        _clustalW.setOptions(_opt);
        SPUtils.setSeqs(_clustalW, pp);
        _clustalW.addProfile(profile);
        _clustalW.compute();
        _aligned=_clustalW.getAlignedSequences();
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        if (ev.getActionCommand().startsWith(KEY_PROGRESS)) handleEvt(this,ev);
    }
    public void setProteins(Protein... proteins) { _pp=proteins;}

    public int[] getIndicesOfSequences() {
        return _clustalW instanceof SequenceAlignerSorting ? ((SequenceAlignerSorting)_clustalW).getIndicesOfSequences() :
            _align3D instanceof SequenceAlignerSorting ? ((SequenceAlignerSorting)_align3D).getIndicesOfSequences() : count01234(_pp.length);
    }

    public byte[][] getSequences() { return _sequences;}
    public byte[][] getAlignedSequences() { return _aligned;}
    public void setSequences(byte[]...ss) { _sequences=ss;}

    private Object _ctrlS;
    public Object getSharedControlPanel() {
        final Aligner3D inst=(Aligner3D)orO(_shared,this);
        if (inst._ctrlS==null) {
            final JMenuBar mb=new JMenuBar();
            mb.add(jMenu(0,StrapAlign.menu(SequenceAligner3D.class),""));
            inst._ctrlS=mb;
        }
        return inst._ctrlS;
    }
    private Object _shared;
    public Object getSharedInstance() { return _shared;}
    public void setSharedInstance(Object o) { _shared=o;}
}
