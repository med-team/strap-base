package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import javax.swing.JPopupMenu;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class HeteroPopup implements java.awt.event.ActionListener, IsEnabled  {
    public final static String TITLE=" Nucleotide and Hetero 3D-Structure";
    private HeteroCompound[] _hh;
    private final ContextObjects _objects;
    private JPopupMenu _popup;
    private static int _iJList;

    private final Object LAB_TITLE=menuInfo(MI_HEADLINE,"");
    public HeteroPopup(ContextObjects o) { this._objects=o;}
    public boolean isEnabled(Object buttton) {
        final HeteroCompound hh[]=_hh;
        final String cmd=gcps(KEY_ACTION_COMMAND,buttton);
        boolean e=false;
        for(int iH=sze(hh); --iH>=0;) {
            final HeteroCompound h=hh[iH];
            if (h!=null) {
                if (cmd=="L") e=true;
                if (cmd=="RM" && h.getBelongsToProtein()!=null) e=true;
                if ((cmd=="K" || cmd=="PDB") && h.getFile()!=null) e=true;
            }
        }
        return e;
    }

    private ChButton b(String s) {
        final ChButton b=new ChButton(s).li(this).cp(KEY_ENABLED,this);
        rtt(b);
        return b;
    }
    // ----------------------------------------
    public JPopupMenu menu() {
        _hh=_objects.heteroCompounds();
        setTxt("Menu For "+_hh.length+TITLE+(_hh.length==1 ? "" : "s"), LAB_TITLE);
        if (_popup==null) {
            final String tt1="Only if the compound is coded in a file of its own, instead of being defined in one file together with the peptide chain.";
            final Object oo[]={
                LAB_TITLE,
                b("L").t(ProteinPopup.ACTION_showInList).i(IC_LIST),
                b("PDB").i(IC_3D).t("View 3D-structure file").tt(tt1),
                b("RM").i(IC_CLOSE).t("Remove from protein[s]"),
                "-",
                helpBut(HeteroCompound.class).t("Info").i(IC_INFO),
                "-",
                b("K").i(IC_KILL).t("Move file to ./trash").tt(tt1),
            };
            _popup=jPopupMenu(0, "Context-menu for "+TITLE, oo);
        }
        return _popup;
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final HeteroCompound[] hh=_hh;
        if (sze(hh)==0) return;
        final Protein[] ppSel=StrapAlign.selectedProteins(), pp=StrapAlign.proteins();
        if (cmd=="RM" && sze(ppSel)==0 && !ChMsg.yesNo("Remove from proteins:<br>No proteins selected. Remove the "+hh.length+TITLE+" from any protein")) return;
        for(HeteroCompound h :hh) {
            if (cmd=="PDB") {
                for(File f : (File[])h.run(PROVIDE_DND_FILES,null)) {
                    if (sze(f)>0) {
                        new ChTextView(f).tools().showInFrame(ChFrame.STAGGER|ChFrame.SCROLLPANE, fPathUnix(f));
                    }
                }
            }
            if (cmd=="L") {
                StrapAlign.showInJList(0L, null, hh,TITLE,null).showInFrame(ChFrame.AT_CLICK,TITLE+"  "+ ++_iJList);
            }
            if (cmd=="RM" || cmd=="K") {
                int count=0;
                for(Protein p:sze(ppSel)>0?ppSel:pp) {
                    if (cntains(h,p.getHeteroCompounds('*'))) {
                        p.removeHeteroCompound(h);
                        count++;
                    }
                }
                if (count>0)  StrapEvent.dispatchLater(StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED, 33);
            }
            if (cmd=="K") {
                final File f=h.getFile();
                if (f!=null) renamFile(h.getFile(), "./trash/"+f.getName());
            }
        }
        _hh=null;
    }

}
