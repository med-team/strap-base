package charite.christo.strap;
import charite.christo.*;
import charite.christo.blast.*;
import charite.christo.protein.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

Strap allows to search public and private sequence databases to
identify sequences that are similar to a query sequences.

Depending on the type of sequences, there are four search methods:
<ol>
<li>The query is an amino acid (aa) sequence and the database contains nucleotide (nt) sequences.</li>
<li>  ...   nt   ...  nt. ...  </li>
<li>  ...   aa   ...  aa  ...  </li>
<li>  ...   nt   ...  aa  ...  </li>
</ol>

By default the computation is performed on the EBI Blast server.

Strap as a front end provides advanced features compared to the web user interfaces of public services:

<ul>

<li>Many jobs can be run one after another.</li>

<li>The Blast result is saved in the cache. If the same query is used a
second time the cached result appears without delay.
</li>

<li>
Blast alert:

The sequence databases are permanently growing.

With the cache disabled Strap conducts a new Blast
and compares the new result with the previous result stored in the cache.

It reports if there are new hits since last run and  highlights all new hits.
</li>

<li>Several patterns can be highlighted.
Strap identifies patterns even if they are disrupted by a gap.
</li>

<li>Sequences from the hit-list can be loaded by mouse click.</li>
</ul>
   See UBIC:blastall UBIC:blat UBIC:wu_blast
   @author Christoph Gille
*/
public class DialogBlast extends AbstractDialogJTabbedPane implements ActionListener,ChRunnable  {
    private final ProteinList _pList;
    private final List[] _vDB={new Vector(999),new Vector(22000)};
    private final ChButton
        _togDetail=toggl(),
        _togAmino=toggl("Database of Amino acid sequences").s(true).li(this),
        _butDB=new ChButton("Download full list of databases").li(this),
        _butGo=new ChButton("GO").t(ChButton.GO).li(this).tt("Invokes BLAST computation."),
        _labDB=labl(), _labFst=labl(), _labLst=labl();

    private final ChCombo
        _comboClass=SPUtils.classChoice(SequenceBlaster.class).li(this),
        _comboNT=new ChCombo("Aminos or nucleotides: Strap's guess","Query is nucl sequ with introns","Query is the coding nucleotide sequence").li(this),
        _comboDB[]={ new ChCombo(0L,_vDB[0]), new ChCombo(0L,_vDB[1])};

    private final JComponent _pnlCount=pnl();
    private SequenceBlaster _lastRunning;
    private final Object
        _tfFrom=new ChTextField("1").cols(6,true,true),
        _tfTo=new ChTextField("9999").cols(6,true,true),
        _tfSkip=new ChTextField("ensembl").li(this).saveInFile("DialogBlast_skip"),
        _tfSkipNot=new ChTextField("gardnerella").li(this).saveInFile("DialogBlast_skipNot"),
        _radioDB[]=radioGrp(new String[]{"Proteins with 3D-structure", "All proteins", "Non-redundant set of proteins"},0,this);

    public DialogBlast() {
        (_pList=new ProteinList(0L)).li(this);
        _comboClass.setPreferredSize(dim(prefW(_comboClass), prefH(_butGo)));
        updateDatabases(0);
        final JComponent
            pAllDB=pnl(VBHB, "#ETB",
                       pnl(CNSEW,_labDB, null,null,null,_butDB),
                       "Facilitate browsing long lists of databases by hiding entries.",
                       pnl(HB, "   Hide those that contain one of the strings: ",_tfSkip),
                       pnl(HB, "   But do not hide those, that contain: ",_tfSkipNot)
                       ),

            pSimple=pnl(VBHB,
                        "Searching for similar sequences in:",
                        pnl(HBL," "," ",pnl(VB,_radioDB)),
                        " ",
                        buttn(TOG_CACHE).cb(),
                        " "
                        ),
            pRange=pnl(HBL,_labFst, _tfFrom, _labLst ,_tfTo),
            pSettings=pnl(VBHB,
                          pRange,
                          " ",
                          pnl(HBL,"Database:  ",  _togAmino.doCollapse(_comboDB[1] ).doCollapse(true, _comboDB[0]).cb()),
                          pnl(HBL, toggl().doCollapse(pAllDB),"Load full list of available databases"),
                          pnl(HB," ",pAllDB," "),
                          _comboDB[1],
                          _comboDB[0],
                          " ",
                          "Blast service:",
                          pnl(HBL," ",_comboClass.panel()),
                          pnl(HBL," ",_comboNT)
                          ),
            pSide=pnl(VBPNL,
                      KOPT_TRACKS_VIEWPORT_WIDTH,
                      pSettings,
                      pSimple,
                      pnl(HBL,"Options ",_togDetail.doCollapse(new Object[]{pSettings, pRange}).doCollapse(true, pSimple),"#",pnl(_butGo))
                      ),
            pLeftRight=pnl(new java.awt.GridLayout(1,2),_pList.scrollPane(),pnl(CNSEW,null,pSide)),
            pMain=pnl(CNSEW,pLeftRight,dialogHead(this), pnl(REMAINING_VSPC1));
        adMainTab(pMain,this, DialogBlast.class);
    }

    private void updateDatabases(int dbOpts) {
        final SequenceBlaster b=mkInstance(_comboClass,SequenceBlaster.class,true);
        if (b==null) return;
        for(int isAmino=2; --isAmino>=0;) {
            _vDB[isAmino].clear();
            skipDatabases(b, _tfSkip, _tfSkipNot);

            final String[] dd=b.getAvailableDatabases( dbOpts|(isAmino!=0?0:SequenceBlaster.DATABASES_NUCLEOTIDES));
            if (sze(dd)>0) {
                final String db=toStrg(_comboDB[isAmino]);
                adAll(dd, _vDB[isAmino]);
                _comboDB[isAmino].s(maxi(0,idxOfStrg(db,dd)));
            }
        }
        final String s=_comboNT.i()==0 ? " residue " : " nucleotide ";
        _labFst.t("First "+s);
        _labLst.t(" Last "+s);
        _pList.repaint();
    }
    private Runnable _todo[];
    private void compute() {
        Protein pp[]=_pList.selectedProteins();
        if (pp.length==0 && StrapAlign.visibleProteins().length<4) pp=StrapAlign.visibleProteins();
        if (pp.length==0) { StrapAlign.errorMsg("Select a protein!"); return; }
        int count=0;
        _todo=new Runnable[pp.length];
        final int mode=_comboNT.i();
        for(Protein p : pp) {
            if (!_pList.isEnabled(p)) continue;
            final SequenceBlaster blaster=_lastRunning=mkInstance(_comboClass,SequenceBlaster.class,true);
            if (blaster==null)  { StrapAlign.errorMsg("Could not instantiate"+_comboClass);return;}
            final Object seq;
            if (mode==0) seq=p.getResidueTypeExactLength();
            else {
                final byte nts[]=p.getNucleotidesCodingStrand();
                if (nts==null && p.containsOnlyACTGNX()) seq=p.getResidueTypeExactLength();
                else if(nts!=null) {
                    seq=mode==1 ? nts:DNA_Util.onlyTranslated(nts,p.isCoding());
                } else seq=null;
            }
            if (seq==null) return;
            final int iDB=radioGrpIdx(_radioDB);
            final int idx0=Protein.firstResIdx(p);
            final String query=substrg(seq, atoi(_tfFrom)-idx0-1, atoi(_tfTo)-idx0);
            if (mode==0 && !(p.countResidues()>50 && p.containsOnlyACTGNX())) blaster.setAAQuerySequence(query);
            else blaster.setNTQuerySequence(query);
            blaster.setDatabase(_togDetail.s() ? toStrg(_comboDB[_togAmino.s()?1:0]) : iDB==0 ? "pdb" : iDB==1 ? "uniprotkb" : "uniref90");
            final Result r=new Result(blaster,p,mode==0,query);
            _todo[count++]=thrdCR(r,"RUN");
            adTab(DISPOSE_CtrlW, delDotSfx(r._p)+" ...", r, this);
        }
        startThrd(thrdCR(this,"RUN"));
    }
    public Object run(final String id,final  Object arg) {
        if (id=="AFTER_DB") _labDB.t("Done").fg(0xff00);
        if (id=="RUN") {
            for(int i=0; i<sze(_todo); i++) {
                final Runnable r=_todo[i];
                if (r!=null && !isDisposed()) {
                    final long time=System.currentTimeMillis();
                    r.run();
                    if (System.currentTimeMillis()-time>4444) ChMsg.speakBG("Blast finished",ChMsg.SOUND_SUCCESS);
                }
            }
        }
        return null;
    }
    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final int nP=_pList.selectedProteins().length;
        if (q==_comboClass) {
            setEnabld(true,_butDB);
            _labDB.t(null);
        }
        if (q==_butDB) {
            setEnabld(false,q);
            final SequenceBlaster b=mkInstance(_comboClass,SequenceBlaster.class,true);
            startThrd(thread_setAvailableDB(b, _vDB, _comboDB, _tfSkip, _tfSkipNot, thrdCR(this,"AFTER_DB")));
            revalAndRepaintCs(_labDB.t("Loading ...").fg(0));
        }
        if (nP>0) StrapAlign.errorMsg("");
        if (q==_comboNT || q==_comboClass || (cmd==ACTION_FOCUS_LOST || cmd==ACTION_ENTER) && (q==_tfSkip || q==_tfSkipNot)) {
            final int mode=_comboNT.i();
            _pList.setFilter(mode==1 ? ProteinList.NT_or_ACTGN : mode==2 ? ProteinList.NT : 0);
            _pList.repaint();
            updateDatabases(0);
        }
        if (q==_butGo) compute();
        if (q==_pList) {
            setEnabld(nP==1, _tfFrom);
            setEnabld(nP==1,_tfTo);
        }
    }
    private class Result extends ChPanel implements Disposable,ChRunnable, ActionListener {
        private final SequenceBlaster _blaster;
        private final ChTextView _tp=new ChTextView((File)null);
        private final Protein _p;
        private final JComponent
            _tfFormat=new ChTextField("999999").cols(7,true,true).li(this),
            _butFormat=new ChButton(ChButton.DISABLED,"Apply").li(this);
        private final String _query;
        private String _db;
        private boolean _computed;
        public void dispose() {
            dispos(_blaster);
            dispos(_lastRunning);
        }
        Result(SequenceBlaster bl,Protein p, boolean isAA, String query) {
            _query=query;
            _p=p;
            _blaster=bl;
            final BA txt=new BA(222)
                .a("query=").aln(query)
                .a("program=").aln(_blaster.getClass().getName())
                .a("database=").aln(_blaster.getDatabase())
                .aln("Calculating please wait ...");

            _tp.setText(txt);
            pcp(KOPT_CONTAINS_AA,"",_tp);
            pcp(ACTION_DATABASE_CLICKED,"load", _tp);
            pcp(KEY_TITLE, "Blast "+p, _tp);
            final String msgFetch=
                "<h2>Downloading proteins from the Blast result</h2>"+
                "<i>PACKAGE:charite.christo.strap.</i>"+
                "Entries in the blast-result start with a greater than sign "+
                "followed by a database and a sequence identifier. <br>"+
                "Consider the the following example and watch the mouse cursor while moving over the identifiers:"+
                "<pre>\n  71&gt; UNIPROTKB:P56817\n</pre>"+
                "Clicking before the colon it acts as a Web-hyperlink while after the colon it loads the protein into Strap."+
                "<br><br>"+
                "<i>SEE_DIALOG:DialogFetchSRS</i>"+
                "<i>SEE_DIALOG:DialogFetchPdb</i>",
                infoCreateSel=
                "The selected proteins (I.e. the proteins marked in alignment row header) are analyzed.<br>"+
                "The sequences of the blast hits are considered (I. e. the lines starting with the letter \"H\").<br>"+
                "If a hit sequence is identical or part of a protein sequence,<br>"+
                "then a residue selection for this protein is created.";
            final Object
                pFormat=pnl(VBHB,
                            "The maximum width of the Blast output can be specified.<br>"+
                            "Longer Blast alignments will be folded.<br>"+
                            "Note that sequence searching with Ctrl+F will not work at line breaks.<br>",
                            pnl(HB,"Max output width ", _tfFormat),
                            _butFormat
                            ),
                pTools=pnl(VBHB,
                           ChButton.doView(msgFetch).t("Download proteins...").cp(ACTION_DATABASE_CLICKED,"load"),
                           pnl(HBL,new ChButton("RES_SEL").t("Create residue selection of matched regions").li(this),smallHelpBut(infoCreateSel)),
                           ChButton.doView(pFormat).t("Change Output format"),
                           ChButton.doCtrl(_blaster),
                           " ",
                           pnl(HBL,"Options for result-text ", scrllpn(ChJTextPane.KEYBINDINGS))
                           ),
                pNorth=pnl(VB,
                           pnl(HBL, pnlTogglOpts("Tools ", pTools), _pnlCount),
                           pTools
                           ),
                pNorth2=pnl(CNSEW,pNorth,null,null,new ChButton().doClose(CLOSE_RM_CHILDS, this));
            pnl(this,CNSEW, scrllpn(_tp), pNorth2);
        }
        public Object run(String id, Object arg) {
            if (id=="RUN") {
                _db=delToLstChr(toStrg(_comboDB[_togAmino.s()?1:0]),' ');
                BA xml=CacheResult.isEnabled() || arg!=null ? _blaster.getResultXml() : null;
                if (xml==null) {
                    _blaster.compute();
                    _computed=true;
                    xml=_blaster.getResultXml();
                }
                if (xml!=null) inEdtLaterCR(this,"RUN_EDT",xml);
            }
            if (id=="RUN_EDT") {
                final BA xml=(BA)arg;
                final File fResult=newTmpFile(".blast");
                final OutputStream fOut;
                try { fOut=fileOutStrm(fResult); } catch(IOException e){ putln(RED_CAUGHT_IN+"DialogBlast ",e); return null;}
                final AbstractBlaster abstrB=deref(_blaster, AbstractBlaster.class);
                final String whenSection=abstrB==null?null:"/day/"+abstrB.cacheSK('S');
                BA when=abstrB==null?null: CacheResult.getValue(0, abstrB.getClass(), whenSection, abstrB.cacheSK('K'), null);
                if (when==null) when=new BA(0);
                final BlastParser parser=new BlastParser();
                parser.setDay(when, _computed);
                try {
                    fOut.write((fResult+"\n"+shrtClasNam(_blaster)+" "+_db+"\nQuery="+_query+"\n").getBytes());
                } catch(IOException ex){}

                parser.parseXML(xml,maxi(10,atoi(_tfFormat)),fOut);
                closeStrm(fOut);
                xml.set(null,0,0);
                final int count=parser.countHits(), countN=parser.countNovelHits();
                setTtleAt(this, _p+(" #"+count+(countN>0?"*":"")), DialogBlast.this);
                if (abstrB!=null) CacheResult.putValue(CacheResult.OVERWRITE, abstrB.getClass(), whenSection, abstrB.cacheSK('K'), when);
                _tp.setFile(fResult);
                pcp(ChTextView.KOPT_SEARCH_SKIP_BLANKS,"",_tp);
                _pnlCount.removeAll();
                if (countN>0 && countN<count) _pnlCount.add(pnl(countN+" Novel Blast hits.",toggl("(NEW)").t("Highlight").li(this).cb()));
                else _pnlCount.add(pnl("#hits = "+count));
                setEnabld(true,_butFormat);
                revalAndRepaintCs(_tp);
            }
            return null;
        }

        private final List<ResidueAnnotation> _vSelections=new Vector(999);
        private TextMatches _highlightNew;
        public void actionPerformed(ActionEvent ev) {
            final String cmd=ev.getActionCommand();
            final Object q=ev.getSource();
            if (q==_butFormat || q==_tfFormat && cmd==ACTION_ENTER) run("RUN", "Cached");
            if (cmd=="(NEW)") {
                if (isSelctd(q)) {
                    if (_highlightNew==null) _highlightNew=_tp.tools().highlightOccurrence("(NEW)",null,null,HIGHLIGHT_REPAINT, C(0xffafaf));
                } else {
                    _tp.tools().removeHighlight(_highlightNew);
                    _highlightNew=null;
                    _tp.repaint();
                }
            }
            if (cmd=="RES_SEL") {
                final String SELECTION_NAME="blast_match";
                Protein pp[]=StrapAlign.selectedProteins();
                if (sze(pp)==0) pp=StrapAlign.visibleProteins();
                final BA xml=_blaster.getResultXml();
                if (sze(xml)==0) { error("Blast not finished yet"); }
                else {

                    final BlastResult bResult=new BlastResult(xml);
                    final List<ResidueAnnotation> v=_vSelections;
                    v.clear();
                    final BA baTmp=new BA(99);
                    for(Protein p : pp) {
                        final boolean[] selected=bResult.getMatchingAminoacids(p.getResidueTypeExactLength());
                        if (countTrue(selected)>0) {
                            ResidueAnnotation a=null;
                            for(ResidueAnnotation ra : p.residueAnnotations()) if (SELECTION_NAME.equals(ra.getName())) a=ra;
                            if (a==null) a=new ResidueAnnotation(p);
                            a.setValue(0,ResidueAnnotation.POS, baTmp.clr().boolToText(selected,1+Protein.firstResIdx(p),",","-"));
                            a.setValue(0,ResidueAnnotation.NAME,SELECTION_NAME);
                            a.setValue(0,ResidueAnnotation.GROUP, "Blast-match");
                            p.addResidueSelection(a);
                            v.add(a);
                        }
                    }
                    if (sze(v)>0)  {
                        final String T="Residue selections created by Blast";
                        final Object
                            pApply=pnl(VBPNL,
                                         "#TB Apply created residue selections",
                                         pnl(HBL,"Compute multiple sequence alignment using only these matching parts ",new ChButton("REALIGN").t("Realign ...").li(this))
                                         ),
                            pNorth=pnl(VBPNL,T,pnl(HB," ",pApply," "));
                        StrapAlign.showInJList(0L,null, v.toArray(), pNorth, null).showInFrame(ChFrame.AT_CLICK,T);
                        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,9);
                    } else ChMsg.msgDialog(0L, "I analyzed "+pp.length+" proteins but could not create residue selections from blast result");
                }
            }
            if (cmd=="REALIGN") ResSelUtils.openDialogRealign(toArry(_vSelections,ResidueAnnotation.class));
        }
    }

    public static void skipDatabases(SequenceBlaster b, Object tfSkip, Object tfSkipNot) {
        final AbstractBlaster ab=deref(b,AbstractBlaster.class);
        if (ab!=null) ab.skipDatabases(splitTokns(toStrg(tfSkip)), splitTokns(toStrg(tfSkipNot)));
    }
    private static String[] _prefDb;
    private static Runnable thread_setAvailableDB(SequenceBlaster b, List v[], ChCombo jc[], Object tfSkip, Object tfSkipNot, Runnable finaly) {
        return thrdM("setAvailableDB", DialogBlast.class, new Object[]{b,v,jc, tfSkip,tfSkipNot,finaly});
    }
    public static void setAvailableDB(SequenceBlaster b, List v[], ChCombo jc[], Object tfSkip, Object tfSkipNot, Runnable finaly) {
        if (_prefDb==null) _prefDb=splitTokns("pdb uniprotkb uniref100 uniref90 uniref50 em_rel emnew emcds emblcds em_rel_hum em_rel_mam");
        for(int isAmino=2; --isAmino>=0;) {
            skipDatabases(b, tfSkip, tfSkipNot);
            final String ss[]=b.getAvailableDatabases( (isAmino!=0?0:SequenceBlaster.DATABASES_NUCLEOTIDES)|SequenceBlaster.DATABASES_FULL_LIST);
            if (sze(ss)>0 && get(isAmino,v)!=null) {
                final String sel=lstTkn(toStrg(jc[isAmino]));
                final String dd[]=new SortBlastDatabases(ss,_prefDb).sorted();
                v[isAmino].clear();
                adAll(dd,v[isAmino]);
                int idx=-1;
                for(int i=sze(dd); --i>=0;) {
                    if (endWith(STRSTR_w|STRSTR_IC, sel, dd[i])) idx=i;
                }
                if (idx>=0) jc[isAmino].s(idx);
                else revalAndRepaintCs(jc[isAmino]);
            }
        }
        runR(finaly);
    }
}
