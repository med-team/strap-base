package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.util.*;
import javax.swing.*;
import javax.swing.text.StyleConstants;
import javax.swing.text.SimpleAttributeSet;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class DialogNeighborsResult2 extends AbstractDialogJPanel implements ChRunnable {
    private final boolean _interrupt[]={false};
    private final Map<String,AbstractButton> _mapId2cb=new HashMap();
    private final Protein _p;
    private final ChJTextPane _tPane=new ChJTextPane(ChJTextPane.KEYBINDINGS);
    private final ChButton _butLoad=new ChButton(ChButton.GO).enabled(false).r(thrdCR(this,"LOAD"));
    private String _pdbId;
    private char _chain;
    private int _countServices;

    public DialogNeighborsResult2(Protein p) { _p=p; }
    @Override public void dispose() { _interrupt[0]=true; _mapId2cb.clear();}
    private void init() {
        final String id=_p.getAnyPdbID();
        char chain=pdbChain(id);
        if (!is(LETTR_DIGT,chain)) chain=chrAt(0, _p.getChainsAsString());
        _chain=is(LETTR_DIGT,chain)?chain : '_';
        _pdbId=pdbID(id);
        _tPane.tools().cp(KOPT_TRACKS_VIEWPORT_WIDTH,null).underlineRefs(0);
        final String sChain=id!=null?_pdbId+":"+_chain : "Error, PDB ID not known";
        pnl(this,CNSEW,
            id==null?null:scrllpn(monospc(_tPane)),
            pnl(CNSEW,pnl(new ProteinLabel(0,_p)," ",sChain),null,null, pnl(smallHelpBut(this), ChButton.doClose15(DISPOSE_CtrlW,this))),
            id==null?null:pnl("To load structures, select checkboxes and press: ", _butLoad)
            );

        final List v=new Vector();
        if (id!=null) {
            final SimpleAttributeSet attUB=new SimpleAttributeSet(), attB=new SimpleAttributeSet();
            StyleConstants.setBold(attUB,true);
            StyleConstants.setBold(attB,true);
            StyleConstants.setUnderline(attUB,true);
            v.add(attUB);
            v.add("\nQuery: ");
            v.add("PDB:"+_pdbId+ (is(LETTR_DIGT,_chain)?":"+_chain:"")+"\n\n");
            run("V",v);
        }
    }
    /* <<< instance <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */

    public Object run(String id, Object arg) {
        final Object[] argv=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="V")  {
            final List v=(List)arg;
            final ChJTextPane tPane=_tPane;
            pcp(ACTION_DATABASE_CLICKED,"Load",tPane);
            final javax.swing.text.Document doc=tPane.getDocument();
            SimpleAttributeSet attr=null;
            Color bg=null;
            final int N=sze(v);
            for(int i=0; i<N; i++) {
                final Object o=get(i,v);
                if (o instanceof SimpleAttributeSet) { attr=(SimpleAttributeSet)o; continue;}
                if (o instanceof Color) { bg=(Color)o; continue;}
                try {
                    JComponent jc=derefJC(o);
                    if (o instanceof CharSequence) {
                        final String txt=toStrg(o);
                        final boolean isPDB=strEquls("PDB:",txt,0);

                        if (isPDB) {
                            final int space=strchr(' ',txt);
                            final String pdb=space>0 ? txt.substring(0,space) : txt;
                            final AbstractButton cb=cbox(null);
                            cb.setOpaque(true);
                            if (bg!=null) cb.setBackground(bg);
                            cb.setCursor(cursr('D'));
                            cb.setPreferredSize(dim(2*EX,EX+EX/2));
                            pcp("PDB_ID", pdb,cb);
                            _mapId2cb.put(pdb,cb);
                            tPane.setCaretPosition(doc.getLength());
                              tPane.insertComponent(cb);
                        }
                        doc.insertString(doc.getLength(),txt,attr);

                    } else if (o instanceof URL || o instanceof File) {
                        jc=ChButton.doOpenURL(o).t(null).setOptions(ChButton.NO_FILL|ChButton.NO_BORDER).i(o instanceof URL ? IC_WWW16 : IC_DIRECTORY+"16x16");
                    }
                    if (jc!=null) {
                        tPane.setCaretPosition(doc.getLength());
                        tPane.insertComponent(jc);
                    }
                } catch(Exception ex){stckTrc(ex);}
                attr=null;
            }
        }
        if (id=="RUN") {
            if (isEDT()) {
                init();
                final JTabbedPane tp=(JTabbedPane)arg;
                adTab(DISPOSE_CtrlW, nam(_p), this,tp);
                StrapAlign.addDialog(tp);
                startThrd(thrdCR(this,id));
            } else {
                for(String db : new String[]{"N_Dali","N_GangstaPlus"}) {
                    if (!_interrupt[0]) startThrd(thrdCR(this,db,null));
                }
            }
        }
        if (id.startsWith("N_")) {
            if (_pdbId==null) return null;

            final List<String> vEntries=new Vector();
            final String sChain=is(LETTR_DIGT,_chain)?toStrg(_chain):"";
            String subTitle="", url=null;
            Color bgColor=C(0xFFff00);
            if (id=="N_GangstaPlus") {
               url="http://agknapp.chemie.fu-berlin.de/gplus/addons/getinfo.php?id="+_pdbId+sChain+"&.txt";
               final File fHtml=read(url,1);
               final String lines[]=readLines(fHtml);
               if (lines!=null) {
                   nextLine:
                   for(int iL=0; iL<lines.length; iL++) {
                       final String l=lines[iL]+"\t";
                       if (chrAt(0,l)=='<') continue;
                       final int tab=l.indexOf('\t');
                       if (iL==0) subTitle="GangstaPlus representative which is an entry of SCOP: "+l.substring(0,tab)+"\n";
                       if (strEquls(STRSTR_IC, _pdbId,l,0)) continue;
                       if (tab<4) continue;
                       final String id_blank=(tab>4 ? l.substring(0,4)+":"+ uCase(chrAt(4,l))+" " : l)+"  ";
                       adUniq(id_blank+simplifyTitle(l.substring(tab+1).replace('\t',' ')), vEntries);
                   }
               }
            }
            final ChTokenizer TOK=new ChTokenizer();
            if (id=="N_Dali") {
                /*
                  500:  2dbz-A  3.2  3.8  111   333    9   MOLECULE: GLYOXYLATE REDUCTASE;
                  3: 1ryp-A 2fak-G   186 - 242 <=>  186 - 242   (ASN  195  - GLN  251  <=> ASN  184  - GLN  239 )
                */
                bgColor=C(0xff00ff);
                url="http://ekhidna.biocenter.helsinki.fi/dali/daliquery_txt?pdbid="+_pdbId+"&chainid="+sChain;
                final File fHtml=read(url,1);
                final BA txt=readBytes(fHtml);
                if (txt!=null) {
                    final int ends[]=txt.eol();
                    final byte[] T=txt.bytes();
                    for(int iL=1; iL<ends.length; iL++) {
                        final int b=ends[iL-1]+1, e=ends[iL];
                        final int iMolecule=strstr(STRSTR_AFTER,"MOLECULE: ",T,b,e);
                        TOK.setText(T,b,e);
                        if (iMolecule<0 || !TOK.nextToken() || !TOK.nextToken()) continue;
                        final String idColonChain=TOK.asString().replace('-',':'), score=TOK.nextAsString();
                        vEntries.add(idColonChain+" Z-score="+score+" "+simplifyTitle(new BA(T,iMolecule,e)));
                    }
                }
            }
            final String dbName=id.substring(id.indexOf('_')+1);
            final List v=new Vector();
            if (_countServices++>0) v.add("--------------------------------------------------------------\n");
            final SimpleAttributeSet att=new SimpleAttributeSet();
            StyleConstants.setBold(att,true);
            StyleConstants.setUnderline(att,true);
            v.add(url(url));
            v.add(file(url));
            v.add(att);
            v.add("Similar structures from the "+dbName+"*\n\n");
            if (sze(vEntries)==0) v.add("No result from "+dbName+"*\n\n");
            else if (subTitle!=null) v.add(subTitle+"\n");
            v.add(bgColor);
            if (sze(vEntries)>0) {
                for(int i=0; i<sze(vEntries); i++) {
                    v.add("PDB:"+get(i,vEntries));
                    v.add("\n");
                }
                _butLoad.enabled(true);
            }
            inEdtLater(thrdCR(this,"V",v));
        }
        if (id=="FINISHED") {
            StrapAlign.drawMessage("@1 "+ANSI_GREEN+" finished computation");
            ChMsg.speakBG("Search for similar structures finished", ChMsg.SOUND_SUCCESS);
        }
        if (id=="LOAD") {
            StrapEvent.dispatchLater(StrapEvent.COMPUTATION_STARTED,333);
            final List<String> vCheckboxText=new Vector();

            for(Object o : _mapId2cb.values().toArray()) {
                final AbstractButton cb=(JCheckBox)o;
                if (cb.isSelected()) vCheckboxText.add(gcps("PDB_ID",cb));
            }
            if (sze(vCheckboxText)>0) {
                vCheckboxText.add(_pdbId+":"+_chain);
                final List<Protein> vP=new Vector();
                final boolean stop[]=new boolean[1];
                CanBeStopped.vALIGNMENTS.add(stop);
                startThrd(thrdRRR(
                                  SPUtils.threadLoopSuperimpose(SPUtils.SUPERIMP_EVENT, vP, stop),
                                  SPUtils.threadAlignProteins(0, null,vP,CAN_BE_STOPPED),
                                  thrdCR(this,"FINISHED")
                                  ));

                Protein3d p3d=null;
                for(int i=0; i<sze(vCheckboxText); i++) {
                    final String txt=vCheckboxText.get(i);
                    final String pdbId=pdbID(txt);
                    final char ch=pdbChain(txt);
                    final String chain=is(LETTR_DIGT,ch) ? "_"+ch : "";
                    final Protein p=get(0, StrapAlign.downloadProteinID(0, "PDB:"+pdbId+":"+chain, new Object[]{StrapAlign.class}, null ), Protein.class);
                    if (p==null || p.getResidueCalpha()==null) continue;
                    if (p3d==null) { p3d=StrapAlign.new3dBackbone(0, new Protein[]{p});}
                    if (p3d!=null) p3d.addProteins(p);
                    vP.add(p);
                    StrapAlign.setIsInAlignment(true, 100, p);
                }
                stop[0]=true;
                startThrd(SPUtils.threadSuperimposeProteins(SPUtils.SUPERIMP_EVENT, vP,CAN_BE_STOPPED, null));
                if (p3d!=null) p3d.eachChainOneColor(false,false);
                SPUtils.setProteinIcon(toArry(vP, Protein.class),false,'A');
            } else if (sze(_mapId2cb)>1) error("No checkbox selected!");

        }
        return null;
    }
    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */
    private static File read(String url, int days) { return urlGet(url(url), CacheResult.isEnabled()?days:0); }
    private final static String
        delPrefix[]="_ID: 1; MOLECULE: /The /Nmr /X-Ray /Crystal /Solution /Structure /Struture /Of /The /Structural Analysis Of /Asymmetric Structure Of /Conformation Of The /Solution Structure Of /And Function Of /Structural Basis Of /Structural Basis For /Structural Genomics/,".split("/");
    public static String simplifyTitle(CharSequence t) {
        CharSequence s=toStrg(t).replaceAll("  +"," ").replaceAll("; CHAIN:.*","");
        s=strplc(STRSTR_IC, "Crystal Structure ", "", s);
        s=strplc(STRSTR_IC, "MOLID: 1; ", "", s);
        s=strplc(STRSTR_IC, "MOLECULE: ", "", s);
        for(String d : delPrefix)  if (strEquls(d,s,0)) s=s.subSequence(sze(d), sze(s));
        return toStrgTrim(s);
    }

    void highlight() {
        final ChJTextPane tp=_tPane;
        final ChTextComponents tools=tp.tools();
        final List<String> vIDs=new Vector();
        for(Protein p: StrapAlign.proteins()) {
            final String id=pdbID(p.getAnyPdbID()), chains=p.getChainsAsString();
            adUniq(id,vIDs);
            if (id==null) continue;
            for(int i=sze(chains); --i>=0;) {
                final char c=chrAt(i,chains);
                adUniq(id+":"+(c==' ' ? '_' : c),vIDs);
            }
        }
        for(int i=sze(vIDs); --i>=0;) {
            tools.highlightOccurrence(vIDs.get(i), null,null, HIGHLIGHT_IGNORE_CASE|HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(0xffff,00));
        }
        tools.repaintLater(999);
    }

}
/*

Problems 2hpl not yet.

"Christopher Wilton" <chris.wilton@helsinki.fi> Dali

   GangstaPlus 1fnt not available

*/

