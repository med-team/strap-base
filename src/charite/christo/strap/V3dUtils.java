package charite.christo.strap;
import charite.christo.protein.*;
import charite.christo.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import javax.swing.*;
import charite.christo.strap.extensions.AlignmentEntropy;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.ProteinViewer.*;
import static charite.christo.protein.Protein3dUtils.*;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.KeyEvent.*;
import static charite.christo.strap.SPUtils.*;
/**HELP
   <br><br><b>Menu-bar: </b>

   If the user clicks inside the 3D-view, the menu-bar of the 3D-view and the 3D-tool bar are shown.

   In addition to this <b>generic menu</b> bar,
   3D-programs may or may not have their own <b>native menu bar</b> which can be activated.

   <br><br><b>Undock menu items: </b>

   Menu items like "spheres on" or "sticks on" in the style menu might be frequently used.
   For convenience, menu items can be dragged to the desktop  with the mouse and are easier accessible.

   <br><br><b>Undock the viewer pane: </b>

   3D-views inside the Strap frame can be moved outside (undocking) which is most useful if there is a  2nd screen.
   For this purpose the head bar needs to be dragged with the mouse.

   Like all Strap windows, the undoked view  can be set "Always floating on top of all other windows". Type Ctrl+T.

   <br><br><b>Picking an amino acid in the 3D-View: </b>

   When atoms are clicked in the 3D-view, a toolbar with buttons that
   act upon the clicked amino acid appear.

   <br><br><b>Picking several amino acids: </b>

   By picking an atom with the mouse without Ctrl or Shift key, exactly one amino acid is selected.
   With the modifier keys Shift and Ctrl, more than one amino acid can be selected.
   This corresponds to the notion of selecting more than one files in a file browser.
   Unfortunately, Shift+Click has already a function in OpenAstex viewer and the modifier keys for amino acid selection differ from the general convention - see table below.
  <br>
   <table border="1">
   <tr><th>3D-Viewer</th><th>Continuous selection</th><th>Discontinuous selection</th></tr>
   <tr><td>OpenAstex</td><td>Ctrl+Shift+Click</td><td>Ctrl+Click</td></tr>
   <tr><td>JMol     </td><td>Shift+Click</td><td>Ctrl+Click</td></tr>
   </table>

   <br><br>

   <br><br><b>Selecting consecutive amino acids with the keyboard: </b>
   One amino acid can be picked and adjacent amino acids can be selected by  typing <b>Shift+&larr;</b> or <b>Shift+&larl;</b>.
   This is similar to selecting list elements in graphical user interfaces or text in word processors using the Shift key with the arrow keys.

   <br><br><b>Selecting amino acids in the alignment panel: </b>
   Using the mouse or using Arrow-Left/ Arrow-Right keys together with Shift, a number of amino acids can be selected
   in Strap. This selection is passed to 3D.
   Alternatively, a residue selection can be dragged with the mouse into the 3D-view.

   <br><br><b>Adding proteins: </b>

   Proteins can be added by WIKI:Drag_and_drop.
   See <i>STRING:ChConstants#MOVIE_Drag_to_another_STRAP</i>

   <br><br><b>Displaying Sequence Features: </b>

   Sequence Features can highlighted in the 3D-view using  WIKI:Drag_and_drop.
   See <i>STRING:ChConstants#MOVIE_Sequence_Features_in_3D</i>

   <br><br><b>3D-Superposition: </b>
   3D-Superposition allows to compare two or more protein backbones.
   Two options are available:
   <ol>
   <li>All peptide chains in the view: One medium structure will be used as a reference.
   All other proteins are overlaid upon this reference peptide chain. </li>
   <li>All multi-chain complexes in the view:
   Different chains of the same PDB entry form a complex and their relative position is sustained.

   For superimposing two multi-chain complexes, the best structurally matching pair of chains is determined.
   I.E. for superimposing two trimeric G-proteins, the beta propeller structures may be selected.

   The superposition is computed for this pair of peptide chains and the transformation is applied to the entire
   complexes.

   Complexes might also be defined by the user or by a script - see context menu of proteins.
   </li>
   </ol>
   @author Christoph Gille
*/
public class V3dUtils implements ChRunnable, ProcessEv, TooltipProvider, PaintHook {
    public final static int OPEN_VERBOSE=1, OPEN_ASK=1<<1, OPEN_SEND_ALL_ANNOTATIONS=1<<2, OPEN_NOT_ADD_PANEL=1<<3,
        OPEN_NARROW_OTHERS=1<<4, OPEN_ADD_TAB=1<<5, OPEN_INIT_SCRIPT=1<<6, OPEN_EACH_PROTEIN_OWN_COLOR=1<<7;
    public final static String
        DIA="DIA",
        TOG_CURSOR_SELECT="Select alignment cursor position in 3D",
        BUT_DEFAULT_VIEWER="   \u25baFull featured 3D-view",
        BUT_NATIV_MENUS="PVP$$NM",
        BUT_SURF_OBJ="SURF_OBJ",
        BUT_COLOR="COLOR",
        KEY_WHICH_MENU="PVU$$TN",
        KEY_PROTEIN_VIEWER="PV$$KPV",
        KOPT_IS_PREVIEW="PV$$KIP",
        KEY_PV_CMD="PV$$cmd";
    private final static String
        KEY_s3d_click="V3DU$$lc",
        KEY_s3d_shift="V3DU$$lcs",
        ACTION_SET_FOCUS="V3D$$appv",
        TT_ATOMS=
        "Example one single atom: <pre class=\"data\"> .CA</pre>"+
        "Example main chain atoms: <pre class=\"data\"> .C.CA.CB.N.O</pre>"+
        "Example all nitrogen using asterisk: <pre class=\"data\"> .N*</pre>";

    final static List vStyleButton=new ArrayList();
    private static int _shiftRange;
    private final static ProteinViewer[] VIEWERS={null,null, null}, FOCUSED={null};
    private static Object _colorDia, _sfcDia, _selectedInStrap, _msgDnD;
    private static ChButton _cbSurfaceColors, _cbColorEntire, _colorBut, _labIcon;
    private static BA _scriptList;
    /* ---------------------------------------- */
    /* >>> Instance, Constructor >>> */
    static EvAdapter li() { return evAdapt(instance()); }
    private final ContextObjects contextObjects=new ContextObjects(false,FOCUSED, "V3d");
    private static V3dUtils _inst;
    static V3dUtils instance() {  if (_inst==null) _inst=new V3dUtils();  return _inst;  }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> open3dViewer >>> */
    public static Runnable thread_open3dViewer(long opt, Protein pp[],Object clazzOrViewer, ProteinViewer[] ret) {
        return thrdMR("open3dViewer", V3dUtils.class, new Object[]{longObjct(opt), pp, clazzOrViewer},ret);
    }
    public static ProteinViewer open3dViewer(long opt, Protein pp[],Object clazzOrViewer) {
        if (!isEDT()) {
            final ProteinViewer ret[]={null};
            inEDT(thread_open3dViewer(opt,pp,clazzOrViewer,ret));
            return ret[0];
        }
        final Class clazz=clas(clazzOrViewer);
        final ProteinViewer inSameView=deref(clazzOrViewer,ProteinViewer.class);
        if (clazz==null || sze(pp)==0 ||  (opt&OPEN_ASK)!=0 && !ChMsg.yesNo("Load "+ (pp.length==1 ? "the protein "+pp[0] : pp.length+" proteins ")+"\n into the protein viewer?")) return null;
        ProteinViewer oneViewer=null;
        nextProtein:
        for(int iP=0; iP<pp.length; iP++) {
            final Protein p=pp[iP];
            if (p==null || (p.getResidueCalpha()==null && p.getHeteroCompounds('*').length==0)) continue;
            if (viewForProtein(p, inSameView)!=null) continue;
            final ProteinViewer pv=mkInstance(clazz,ProteinViewer.class,true);
            if (pv==null) {
                error("Error in V3dUtils: cannot instantiate "+clazz);
                return null;
            }
            final long properties=flags(pv), opts=(opt&OPEN_VERBOSE)!=0 ? SET_PROTEIN_VERBOSE :0;
            if ((properties&PROPERTY_EXTERNAL_PROCESS)!=0) StrapAlign.drawMessage("To see details of the external process,\nhold ctrl when pressing the button to start the 3D viewer.");
            final boolean isWire=clazz==Protein3d.PView.class;
            if (pv.setProtein(opts, p, oneViewer!=null ? oneViewer:inSameView)) {
                transform3D(p.getRotationAndTranslation(), pv);
                if (!isWire) {
                    if (0!=(opt&OPEN_EACH_PROTEIN_OWN_COLOR) && !isWire) colorAllAminos(Protein3d.chain2color(iP),pv);
                    if (0!=(opt&OPEN_INIT_SCRIPT) && !isWire) {
                        sendCommand(0L, COMMANDselect+" $FIRST-$LAST", pv);
                        for(String s : custSettings(clazz, Customize.CLASS_InitCommands)) sendCommand(0L, s,pv);
                    }

                    if (0!=(opt&OPEN_SEND_ALL_ANNOTATIONS)) residueSelectionsTo3D(RSto3D_COMMANDS|RSto3D_COLOR,p.residueAnnotations(), new ProteinViewer[]{pv});
                    StrapEvent.dispatch(StrapEvent.PROTEIN_VIEWER_LAUNCHED);
                }
                if (isWire || p.getFileMayBeWithSideChains()!=null)  p.addProteinViewer(pv);
                if (oneViewer==null) oneViewer=pv;
            }
            final Object pan=viewerCanvas(pv);
            addCP(KEY_CLOSE_HOOKS, pv, pan);
            final Object msg=pnl(CNSEW,
                                      "If you came here because you intended to drag a protein with the mouse:<br>"+
                                      "This is not the right way to drag a protein.<br>"+
                                      "You can start dragging a protein  for example from the respective row header of the sequence alignment.<br>"+
                                      "The row-header displays the protein names. Here you can initiater Drag-and-Drop.",
                                      null,pnl(WATCH_MOVIE+MOVIE_Export_Proteins));
            pcp(KEY_UNDOCK_MESSAGE, msg, pv);
            pcp(KEY_UNDOCK_MESSAGE, msg, pan);
        }
        if (oneViewer!=null) {
            setPV(PV_FOCUSED, oneViewer);
            if (0!=(opt&OPEN_INIT_SCRIPT)) {
                for(String s : custSettings(clazz, Customize.CLASS_LateCommands)) sendCommand(0L, s,oneViewer);
            }
            if (supports(COMMANDcenter_amino, oneViewer)) sendCommand(0L, COMMANDcenter_amino, oneViewer);
            final Component panel=viewerCanvas(oneViewer);
            if (panel!=null) {
                pcp(KOPT_UNDOCKABLE, "", panel);
                final Object ref=wref(oneViewer);
                closeOnKey(CLOSE_CtrlW, panel,vvSharingView(oneViewer));
                pcp(KEY_PROTEIN_VIEWER, ref, panel);
                for(Component child : childsR(panel, Component.class)) {
                    pcp(KEY_PROTEIN_VIEWER, ref, child);
                    StrapAlign.newDropTarget(child,false);
                    li().addTo("Mmk",child);
                    ProteinViewer.mapViewer.put(child,ref);
                    closeOnKey(CLOSE_CtrlW, child,vvSharingView(oneViewer));
                }
                li().addTo("Mwk",panel);
                evLstnr(KLI_SHIFT_TIP).addTo("k", panel);
                pcp(ChRunnable.RUN_GET_COLUMN_TITLE, ref, panel);
                if ((opt&OPEN_NOT_ADD_PANEL)==0) viewerToFront(opt|OPEN_ADD_TAB, oneViewer);
                evLstnr(KLI_ALWAYS_ON_TOP).addTo("k", panel);
            } else {
                final Map m=sharedHashMapV3D(oneViewer);
                if (m.get(ACTION_SET_FOCUS)==null) {
                    final ChButton b=new ChButton(ACTION_SET_FOCUS).li(li())
                        .t(niceShrtClassNam(oneViewer)+" - Activate menus")
                        .i(orO(dIcon(oneViewer),IC_3D))
                        .li(li())
                        .cp(KEY_PROTEIN_VIEWER, oneViewer.getViewersSharingViewV(true));
                    rtt(b);
                    addPaintHook(instance(), b);
                    setTip(instance(), b);
                    m.put(ACTION_SET_FOCUS,b);
                    undockAdd(b);
                }
                setWndwStateT('F', -1,-1,(String)oneViewer.getProperty(GET_FRAME_TITLE));
            }
            maySetFrameTitle(oneViewer);
        }
        return oneViewer;
    }
    /* <<<  open3dViewer <<< */
    /* ---------------------------------------- */
    /* >>> get or open new >>> */
    static ProteinViewer[] vvOfProteinAsk(Protein p, Class clazz) {
        if (p==null) return ProteinViewer.NONE;
        final ProteinViewer vv0[]=p.getProteinViewers();
        ProteinViewer vv[]=vv0;
        for(int i=sze(vv);--i>=0;) {
            if (clazz==null && vv[i] instanceof Protein3d.PView  || !isAssignblFrm(clazz,vv[i])) {
                if (vv==vv0) vv=vv0.clone();
                vv[i]=null;
            }
        }
        vv=rmNullA(vv,ProteinViewer.class);
        if (vv.length==0) {
            final ProteinViewer v=open3dViewer(OPEN_ASK|OPEN_SEND_ALL_ANNOTATIONS, new Protein[]{p},clazz);
            if (v!=null) vv=new ProteinViewer[]{v};
        }
        return vv;
    }
    /* <<< get or open new <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    private final static Map _mPV=new HashMap();
    static Map<String, Collection<? extends ProteinViewer>> mapProteinViewers() { return _mPV;}
    static void disposeV(ProteinViewer...vv) {
        final String key="V3D$$dis";
        for(ProteinViewer pv : vv) {
            final Protein p=p(pv);
            if (pv==null || gcp(key,pv)!=null) continue;
            pcp(key,"", pv);
            if (AbstractProxy.getProxy(pv,ProteinViewer.class)!=null) assrt();
            final Object panel=viewerCanvas(pv), sameView[]=vvSharingView(pv);
            if (sameView.length==0 || (sameView.length==1 && sameView[0]==pv)) {
                rmFromParent(panel);
                undockRemove(sharedHashMapV3D(pv).get(ACTION_SET_FOCUS));
            }
            for(Object surface : oo(pv.getProperty(GET_SURFACEOBJECTS))) {
                sendCommand(0L, COMMANDobject_delete+" "+surface, pv);
            }
            dispos(pv);
            if (p!=null) p.removeProteinViewer(pv);
            maySetFrameTitle(pv);
        }
        StrapAlign.setMenuBar(null,0,0);
        setPV(PV_FOCUSED,null);
        for(Map.Entry e : entryArry(mapProteinViewers())) if (sze(e.getValue())==0) mapProteinViewers().remove(e.getKey());
    }
    /* <<< Disposable  <<< */
    /* ---------------------------------------- */
    /* >>> ChRunnable >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id=="SAVE_TB") {
            final int type=atoi(arg);
            wrte(fileTB(type), new BA(999).join(TEXT_TF[type]));
        }
        if (id=="viewerToFront") viewerToFront(atol(argv[1]), (ProteinViewer)argv[0] );
        if (id==ChScriptPanel.RUN_SCRIPT) sendCommand(0L, toStrg(argv[1]), getPV(PV_FOCUSED));
        return null;
    }
    private static ChScriptPanel _scriptPnl;
    static void showScriptPanel() {
        if (_scriptPnl==null) {
            final Object
                wc[]={
                allCommands(),
                dirWorking(),
                StrapAlign.vProteins(),
                //splitTokns("color red orange brown yellow green blue cyan magenta purple white lightgrey grey darkgrey black "),
            };
            final Object butList=new ChButton("ALL_CMDS").li(li()).rover(IC_LIST).tt("Alphabetical list of all commands");
            _scriptPnl=new ChScriptPanel("3D view", wc, butList, V3dUtils.instance());
            pcp(HelpCommands.class, HelpCommands.getInstance(Strap.SCRIPT_COMMANDS), rtt(_scriptPnl.textPane()));
        }
        _scriptPnl.showFrame("3D-Script-panel ");
    }
    /* <<< ChRunnable <<< */
    /* ---------------------------------------- */
    /* >>> MenuBar >>> */
    private static void viewMenuBar(ProteinViewer pv) {
        if (pv==null) return;
        final Object panel=viewerCanvas(pv);
        final ChFrame f=deref(parentWndw(panel),ChFrame.class);
        final boolean mainFrame=f==null || f==StrapAlign.frame();
        final boolean isNative=gcp(KEY_WHICH_MENU, panel)==BUT_NATIV_MENUS;
        if (gcp(KEY_ADD_TO_NATIVE_MENU, pv)==null)  pcp(KEY_ADD_TO_NATIVE_MENU, jMenu(pvp(pv).m('b')), panel);
        final Component[] mm=
            isNative ? nativeMenus(pv) :
            (mainFrame ? pvp() : pvp(pv)).genericMenus(false);
        final int fg=isNative?0xFF00 : 0xFFffFF;
        if (mainFrame) {
            if (_labIcon==null) _labIcon=labl();
            setEnabld(true, _labIcon.i(orO(dIcon(pv),IC_3D)));
            pcp(KEY_BEFORE_MENU_ITEM, _labIcon, get(0,mm));
            StrapAlign.setMenuBar(mm, fg,0);
            setPV(PV_MENUBAR,pv);
        } else {
            ChJMenuBar mb=deref(f.getJMenuBar(), ChJMenuBar.class);
            if (mb==null) f.setJMenuBar(mb=new ChJMenuBar());
            if (!arraysEqul(JMenu.class, mm,mb.getComponents())) {
                mb.removeAll();
                toMenuBar(0,mm,mb);
                revalAndRepaintC(mm[0]);
                noMenuIconsOnMac(mb);
            }
            setFgBgMb(mb,fg,0);
            ChDelay.revalidate(mb,333);
        }
    }
    static ProteinViewerPopup pvp() { return instance().contextObjects.proteinViewerMenu(); }
    private static ProteinViewerPopup pvp(ProteinViewer pv) {
        if (pv==null) return null;
        ProteinViewerPopup pvp=null;
        final ProteinViewer vv[]=vvSharingView(pv);
        for(int i=sze(vv); pvp==null && --i>=-1;) {
            pvp=gcp(KEY_GET_MENUBAR, i<0?pv:vv[i], ProteinViewerPopup.class);
        }
        if (pvp==null) pvp=new ContextObjects(true, oo(wref(pv)), "V3dUtils pvp").proteinViewerMenu();
        for(int i=sze(vv); --i>=-2;) pcp(KEY_GET_MENUBAR, pvp, i<-1?viewerCanvas(pv): i<0?pv : vv[i]);
        return pvp;
    }
    /* <<< MenuBar <<< */
    /* ---------------------------------------- */
    /* >>> Set Title >>> */
    private static void maySetFrameTitle(ProteinViewer v) {
        final JFrame f=deref(parentWndw( viewerCanvas(v)), JFrame.class);
        if (f!=null && f!=StrapAlign.frame()) setTitl(titleFor3dViewers(ppInV3D(v)), f);
    }
    private static void maySetTitleStyleMenu(ProteinViewer pv) {
        final Protein p=p(pv);
        if (p==null) return;

        if (FOCUSED[PV_FOCUSED]!=null && !pv.getViewersSharingViewV(true).contains(FOCUSED[PV_FOCUSED])) return;
        final boolean isP3d=pv instanceof Protein3d.PView;
        Selection3D atoms[]=getSelection3D(pv);
        if (atoms==null) atoms=getSelection3D(pv);
        final String pn=delSfx(".pdb",delSfx(".ent",toStrg(p)));
        setEnabld(isP3d, pvp(pv).b(BUT_DEFAULT_VIEWER));
        setEnabld(isP3d, pvp().b(BUT_DEFAULT_VIEWER));
        for(int i=sze(vStyleButton); --i>=0;) {
            final AbstractButton b=getRmNull(i,vStyleButton,AbstractButton.class);
            if (b!=null) {
                final boolean style="Style"==gcp(KEY_TITLE,b);
                if (style) setTxtAndReval(isP3d ? "" : "Style", b, 99);
                else if (sze(atoms)==0)  setTxtAndReval("", setTip("",b),99);
                else {
                    final String
                        S="    Selection=",
                        sel=selection3dToText(GENERIC, atoms),
                        tt=S+pn+"  "+sel,
                        t=sze(sel)<20 ? tt : S+pn+"  #"+countTrue(selection3dToBoolZ(atoms, p))+" amino acids";
                    setTxtAndReval(t, setTip(tt,b), 99);
                }
                setEnabld(!isP3d, b);
            }
        }
        setTitl(p+" @ "+niceShrtClassNam(pv), parentWndw(_scriptPnl));
    }
    /* <<< Set Title <<< */
    /* ---------------------------------------- */
    /* >>> Viewer with focus >>> */

    public final static int PV_FOCUSED=0, PV_MENUBAR=1,    PV_FOCUSED_NOT_MB=1000;
    static ProteinViewer getPV(int type) {
        final ProteinViewer pv=VIEWERS[type];
        return isActive(pv) ? pv : null;
    }
    static void setPV(int type0, ProteinViewer v0) {
        final int type=type0==PV_FOCUSED_NOT_MB ? PV_FOCUSED : type0;
        final ProteinViewer v=isActive(v0)?v0:null;
        final boolean changed=VIEWERS[type]!=v;
        VIEWERS[type]=v;
        if (type==PV_FOCUSED) {
            FOCUSED[PV_FOCUSED]=v;
            enableDisable();
            if (v==null) StrapAlign.setToolpane(null);
            else {
                final Component cc[]=childs(_toolpane);
                for(int i=-1; i<cc.length; i++) pcp(KEY_DROP_TARGET_REDIRECT,wref(v),i<0?_toolpane:cc[i]);
                pcp(ChRunnable.RUN_GET_COLUMN_TITLE, wref(v), viewerCanvas(v));
                maySetTitleStyleMenu(v);
                for(ProteinViewer v2 :vvSharingView(v)) {
                    pcp(KEY_COLOR, gcp(KEY_COLOR,v2),  p(v2));
                }
                StrapEvent.dispatchLater(StrapEvent.PROTEIN_ICON_CHANGED,33);
            }
            if (type0!=PV_FOCUSED_NOT_MB) viewMenuBar(v);
            if (changed) setSelection3D(0L, gcp(KEY_SEL_ATOM, v, Selection3D[].class),"",v);
            repaintC(jlUndock(false));
        }
    }
    public static boolean setFocusedP(boolean menuBar, Protein p) {
        final ProteinViewer pvF=getPV(menuBar ? PV_MENUBAR : PV_FOCUSED);
        if (p==null || pvF==null) return false;
        if (sp(pvF)==p) return true;
        for(ProteinViewer pv :vvSharingView(pvF)) {
            if (sp(pv)==p) {
                setPV(PV_FOCUSED, pv);
                return true;
            }
        }
        return false;
    }

    public boolean paintHook(JComponent c, Graphics g, boolean after) {
        if (gcp(KEY_ACTION_COMMAND, c)==ACTION_SET_FOCUS && !after) {
            ((ChButton)c).setTabSelected(g, cntains(VIEWERS[PV_FOCUSED], gcp(KEY_PROTEIN_VIEWER, c, Collection.class)));
        }
        return true;
    }

  /* <<< Viewer with focus <<< */
    /* ---------------------------------------- */
    /* >>> Picked atoms >>> */

    private static ProteinLabel _labPick;
    private static Component _toolpane, _labPickAA;
    private static Selection3D _aaPick;
    private static Component toolpane() {
        if (_toolpane==null) {
            setFG(0xFFffFF, _labPick=new ProteinLabel(ProteinLabel.ALWAYS_CONTEXT_MENU,null));
            setFG(0xFFffFF, _labPickAA=labl());
            final Object[] bb={null,null,null};
            for(int i=3; --i>=0;) {
                final ChButton b=new ChButton(ChButton.DISABLED, i==0?"SET_C":i==1?"ADD_S":"RM_S").t(i==0?"Set alignment cursor":i==1?"Add to selection":"Remove from selection");
                vEnableDisable.add(bb[i]=b);
                li().addTo("M", setUndockble(b.li(li())));
            }
            final Dimension d=StrapView.toolbars().getPreferredSize();
            _toolpane=pnl(FLOWLEFT,KOPT_NOT_INDICATE_TOO_SMALL, d.height>1 ? d : null, C(0),
                          new ChButton("STP").t("\u25ba Other tool bar").li(li()), " ",
                          _labPick,
                          _labPickAA,
                          bb[0], bb[1], bb[2]);
            StrapAlign.newDropTarget(_toolpane,true);
        }
        return _toolpane;
    }
    private static void setSelection3dPicked(Selection3D at, ProteinViewer pv0, int awtModi) {
        _shiftRange=0;
        toolpane(); /* define _labPickAA */
        final ProteinViewer
            proxy=AbstractProxy.getProxy(pv0, ProteinViewer.class),
            pv=proxy!=null ? proxy : pv0;
        final Protein p=p(pv);
        if (p==null || at==null) return;
        final int
            iA=selection3dToFirstLastIdxZ(false, false, at, p),
            maskCont=atoi(pv.getProperty(GET_AWTMASK_CONT_SELECTION)),
            maskDisc=atoi(pv.getProperty(GET_AWTMASK_DISC_SELECTION));
        Selection3D[] atoms={_aaPick=at};
        final Selection3D lastSel=gcp(KEY_s3d_click, pv, Selection3D.class);
        pcp(KEY_s3d_click, at, pv);
        if ((maskCont|maskDisc)!=0 && lastSel!=null && at.getChain()==lastSel.getChain()) {
            final int frst=at.getFirst(), last=at.getLast();
            if (maskCont==(awtModi&MASK_KEYMODIFIERS)) {
                final int f=mini(frst, lastSel.getFirst()), t=maxi(last, lastSel.getLast());
                atoms=addOrRemove('+', getSelection3D(pv), f, t, at.getInsertion(), at.getChain());
            }
             if (maskDisc==(awtModi&MASK_KEYMODIFIERS)) {
                atoms=addOrRemove('!', getSelection3D(pv), frst, last, at.getInsertion(), at.getChain());
            }
        }
        setSelection3D(0L, atoms, SELECTION_PICKED, pv);
        {
            _labPick.setProtein(p);
            setTxt(partDotDotDot(selection3dToText(GENERIC,atoms),30), _labPickAA);
            ((JComponent)_labPickAA).setOpaque(true);
            setBG(0xFFffFF,_labPickAA);
            setFG(0,_labPickAA);
            ChDelay.fgBg(true,_labPickAA,C(0),444);
            ChDelay.fgBg(false,_labPickAA,C(0xFFffFF),444);
            StrapAlign.animatePositionZ(0, p, iA);
            setPV(PV_FOCUSED,pv);
            StrapAlign.setToolpane(toolpane());
            final AWTEvent ev=_canvasEv;
            final StrapView view=StrapAlign.alignmentPanel();
            final Component
                canvas=deref(pv.getProperty(GET_CANVAS), Component.class),
                pCenter=view==null ? null:view.alignmentPane();
            if (pCenter!=null && ev!=null && pv.getProperty(GET_CANVAS)==ev.getSource()) {
                final int
                    row=view.findRow(p),
                    col=p.getResidueColumnZ(iA);
                final float speed=(float)atof(toStrg(comp(GET_ANIM_SPEED)));
                if (row>=0 && col>=0 && speed>1E-8) {
                    final Point
                        pSrc= new Point(x(ev),y(ev)),
                        pDest=new Point((view.col2x(col)+view.col2x(col+1))/2,  (view.row2y(row)+view.row2y(row+1))/2);
                    final String s=new BA(9).aSomeBytes(p.getResidueName32(iA),4).toString();
                    startThrd(new ChSprite(ChSprite.SPEED_SINUS, canvas, pSrc, view.alignmentPane(), pDest, s, speed));
                }
            }
        }
    }
    /* <<< Picked atoms <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    final static List vEnableDisable=new ArrayList();
    public static void enableDisable() {
        ProteinViewer pv=getPV(PV_FOCUSED);
        if (!isActive(pv)) pv=null;
        final Object refPV=wref(pv);
        for(int i=sze(vEnableDisable); --i>=0;) {
            final Object c=get(i,vEnableDisable);
            final String cmd=gcps(KEY_ACTION_COMMAND,c);
            if (cntains(cmd, Protein3dUtils.allCommands())) setEnabld(supports((String)cmd,pv), pv);
            final Object sze=
                cmd=="UNDEL_O" ? V3dListModel.list(V3dListModel.DELETED_OBJECTS, pv, false) :
                cmd=="UPDATE"|| cmd==COMMANDchange_object_color || cmd=="CH_COLOR" || cmd==COMMANDobject_delete ?
                V3dListModel.jList(V3dListModel.SURFACE_OBJECTS, false,0) :
                cmd=="UNDEL_OBJ" ? V3dListModel.jList(V3dListModel.DELETED_OBJECTS, true,0) :
                "";
            if (sze!="") setEnabld(sze(sze instanceof ChJList ? ((ChJList)sze).getSelectedValues() : sze)>0,c);
            if (cmd=="SET_C" || cmd=="ADD_S" || cmd=="RM_S") {
                final int iA=selection3dToFirstLastIdxZ(false, false, _aaPick, p(getPV(PV_FOCUSED)));
                final boolean e=
                    iA<0 ? false :
                    cmd=="SET_C" ? true :
                    (cmd=="ADD_S")!=ResSelUtils.isSelectedAAZ(iA, residueSelectionsF(false));
                setEnabld(e,c);
            }
            if (c instanceof ChJList) pcp(KEY_PROTEIN_VIEWER, refPV, c);
            revalAndRepaintCs(c);
        }
    }
    private static boolean checkResiduesSelected(ProteinViewer pv, boolean setTrue) {
        final Protein p=p(pv);
        if (p==null) return false;
        final Selection3D atoms[]=getSelection3D(pv);
        if (sze(atoms)>0) return true;
        final String MSG_NO_RES=
            "Warning: No residue selected.<br>"+
            "Residues can be selected by clicking into the 3D-model or "+
            "by selecting positions in the alignment pane.<br><br>"+
            "Continue to process all amino acids of the protein?";
        if (ChMsg.yesNo(MSG_NO_RES)) {
            if (setTrue) setSelection3D(0L, rangeToSelection3D(false,0,MAX_INT,p), "",pv);
            return true;
        }
        return false;
    }

    private final static BasicResidueSelection residueSelectionsF(boolean create) {
        final ProteinViewer pvF=getPV(PV_FOCUSED);
        final String name=pvF==null ? null : pvF instanceof Protein3d.PView ? "3D-Backbone" :  niceShrtClassNam(pvF);
        final Protein pF=p(pvF);
        if (name==null) return null;
        BasicResidueSelection s=pF==null?null:findWithName(name, pF.residueSelections(),BasicResidueSelection.class);
        if (s==null && create) {
            s=new BasicResidueSelection(0);
            s.setName(name);
            s.setColor(C(0xFF0000));
            s.setProtein(pF);
            pF.addResidueSelection(s);
            s.setVisibleWhere(0xffffffff);
        }
        return s;
    }

    private static AWTEvent _canvasEv;
    private static ProteinViewer vForP(Protein p) {
        if (p==null) return null;
        final ProteinViewer pvF=getPV(PV_FOCUSED);
        if (p(pvF)==p) return pvF;
        for(ProteinViewer pv :vvSharingView(pvF)) {
            if (p(pv)==p) return pv;
        }
        return get(0, p.getProteinViewers(), ProteinViewer.class);
    }
    public void processEv(AWTEvent ev) {
        final int id=ev.getID(), kcode=keyCode(ev), modi=modifrs(ev);
        final Object q=ev.getSource();
        if (q==null) return;
        final String cmd=actionCommand(ev);
        final boolean ctrl=0!=(modi&CTRL_MASK), shift=0!=(modi&SHIFT_MASK);

        setAotEv(ev);
        if (0==(modi&BUTTON3_MASK)) mayClosePopupMenu(ev);
        if (cmd==BUT_COLOR) openColorDialog();
        if (cmd=="STP") StrapAlign.setToolpane(null);
        if (cmd=="ALL_CMDS") {
            if (_scriptList==null) _scriptList=HelpCommands.getInstance(Strap.SCRIPT_COMMANDS).alphabetically(Protein3dUtils.allCommands());
            shwTxtInW(ChFrame.SCROLLPANE, getTxt(q),_scriptList);
        }
        final ProteinViewer pvF=getPV(PV_FOCUSED);
        if (  (id==MOUSE_ENTERED || id==MOUSE_EXITED) && vStyleButton.contains(q)) {
            sendCommand(0L,COMMANDhighlight_selected_atoms+" 999", pvF);
        }
        if (q instanceof ChJList && id==MOUSE_PRESSED) {
            final List<Selection3D> v=new ArrayList();
            Protein aProtein=null;
            for(Object o : getSelValues(q)) {
                if (o instanceof Protein) aProtein=(Protein)o;
                final HeteroCompound het=deref(o,HeteroCompound.class);
                if (het!=null) {
                    final char chain= het.getCompoundChain();
                    if (het.isNucleotideChain()) {
                        final int rn[]=het.getNucleotideNumber();
                        if (sze(rn)>0) v.add(new Selection3D(0L, rn[0], rn[rn.length-1], (char)0,  chain, null));
                    } else {
                        final int rn=het.getCompoundNumber();
                        v.add(new Selection3D(0L, rn, rn, (char)0,  chain, null));
                    }
                }
                adAll(toSelectionS(deref(o,ResidueSelection.class)), v);
            }
            if (aProtein!=null) {
                final ProteinViewer pv1=vForP(aProtein);
                setPV(PV_FOCUSED, pv1);
                //setSelection3D(0L,getSelection3D(pv1) , gcps(KEY_SEL_ID, pv1), pv1);
                sendCommand(0L, ProteinViewer.COMMANDhighlight_selected_atoms, pv1);
            } else {
                setPV(PV_FOCUSED, pvF);
                setSelection3D(0L, toArry(v,Selection3D.class), "", pvF);
            }
        }
        if (id==KEY_PRESSED && 0==(modi&ALT_MASK) && (kcode==VK_LEFT||kcode==VK_RIGHT)) {
            final Selection3D sel0=gcp(KEY_s3d_click, pvF, Selection3D.class);
            if (sel0!=null) {
                if (ctrl) _shiftRange=(kcode==VK_LEFT?MIN_INT/2:MAX_INT/2);
                else _shiftRange+=(kcode==VK_LEFT ? -1 :  1);

                final Selection3D ssNeu[]=Protein3dUtils.selection3dRange(shift, sel0, _shiftRange, sp(pvF) );
                setSelection3D(0L, ssNeu, SELECTION_PICKED, pvF);
                if (!shift) {
                    pcp(KEY_s3d_click, get(0,ssNeu), pvF);
                    _shiftRange=0;
                }
            }
        }

        final Protein pF=p(pvF);
        if (cmd==ACTION_SELECTION_CHANGED) enableDisable();
        if (q==viewerCanvas(gcp(KEY_PROTEIN_VIEWER,q, ProteinViewer.class))) {
            if (id==MOUSE_PRESSED || id==MOUSE_MOVED || id==MOUSE_DRAGGED) _canvasEv=ev;

        }
        if (pF!=null && isActive(pvF)) {
            if (cmd=="ENTROPY_COLOR" && pF!=null && pvF!=null) {
                final Selection3D[] atoms=getSelection3D(pvF);
                final AlignmentEntropy entropy=new AlignmentEntropy();
                final Protein ppSel[]=StrapAlign.selectedProteins();
                entropy.setProteins(sze(ppSel)>1 ? ppSel : StrapAlign.proteins());
                final double vv[]=entropy.getValues(AlignmentEntropy.GAP_IS_21TH_AMINO_ACID);
                if (sze(vv)==0) return;
                double maxV=Double.MIN_VALUE, minV=Double.MIN_VALUE;
                for (double v : vv) {
                    if (maxV<v) maxV=v;
                    if (minV>v) minV=v;
                }
                if (maxV<=minV) error("Entropy: minV="+minV+" maxV="+maxV);
                final boolean bb[]=selection3dToBoolZ( getSelection3D(pvF),  pF);
                for(int i=0;i<bb.length && i<vv.length; i++) {
                    if (!bb[i] || Double.isNaN(vv[i])) continue;
                    setSelection3D(0L,  rangeToSelection3D(false,i,i+1, pF), "", pvF);
                    sendCommand(0L, COMMANDcolor+" #"+rgbHex(float2blueRed( (vv[i]-minV)/(maxV-minV)), false), pvF);
                }
                if (isSelctd(_cbSurfaceColors)) updateSurfacesP(pvF,true);
            }
            if (id==KEY_PRESSED) {
                if (kcode==VK_W && ctrl) disposeV(vvSharingView(pvF));
                if (kcode==VK_V && shift && ctrl) {
                    if (atoi(gcp(KEY_SEL_DIALOG_TYPE,q))==SELECT_AMINO && q instanceof ChTextField) {
                        if (kcode==VK_W && ctrl) disposeV(vvSharingView(pvF));
                        final ProteinViewer pv=pvF;
                        Selection3D[] sel=getSelection3D(pv);
                        if (sel!=null) {
                            changeAtoms(true, sel=sel.clone(), "");
                            String ins=" "+selection3dToText(GENERIC, sel)+" ";
                            final String pn=toStrg(sp(pv));
                            if (strstr(STRSTR_w, pn,toStrg(q))<0) ins=pn+ins;
                            ChTextComponents.insertAtCaret(0L, ins, (Component)q);
                        }
                    }
                    return;
                }
            }
            final Color color=cmd==ACTION_COLOR_CHANGED ? colr(q) : null;
            final String hex=rgbHex(color,true);
            if (color!=null) {
                if (q!=_colorBut) {
                    final String
                        script=gcps(KEY_PV_CMD,q),
                        w0=wordAt(0,script),
                        lines=rplcToStrg("COLOR", hex, script);
                    if (script!=null && ( idxOfStrg(w0, STYLE_COMMANDS)<0 || checkResiduesSelected(pvF, true))) {
                        boolean success=false;
                        for(String line : splitLnes(lines)) success|=sendCommand(0L, line, pvF);
                        if (success) ChDelay.repaint(viewerCanvas(pvF),99);
                    }
                } else {
                    final String cmdColor=COMMANDcolor+" #"+hex;
                    final ChJList jlProteins=V3dListModel.jList(V3dListModel.PROTEINS, false,0);
                    final Object entire[]=oo(isSelctd(_cbColorEntire) ? jlProteins.getSelectedValues() : null);
                    for(Object p : entire) {
                        final ProteinViewer pv=viewForProtein((Protein)p, pvF);
                        if (pv==null) continue;
                        setSelection3D(0L, boolToSelection3D(false,(boolean[])null,0,(Protein)p,""), "", pv);
                        sendCommand(0L, cmdColor, pv);
                        if (0!=(PROPERTY_RIBBON_COLOR_NO_CHANGE&flags(pv)) && isSelctd(_cbSurfaceColors)) updateSurfacesP(pv,true);
                    }
                    if (entire.length==0) {
                        if (checkResiduesSelected(pvF,true)) {
                            sendCommand(0L, cmdColor, pvF);
                            if (isSelctd(_cbSurfaceColors)) updateSurfacesP(pvF, false);
                        }
                    }
                    for(Object o : entire) {
                        final Protein p=deref(o, Protein.class);
                        final ProteinViewer pv=viewForProtein(p, pvF);
                        colorAllAminos(color,pv);
                        if (isSelctd(_cbSurfaceColors)) updateSurfacesP(pv, false);
                    }
                    return;
                }
            }
            /* Surface Objects */
            final ChJList jlSurf=V3dListModel.jList(V3dListModel.SURFACE_OBJECTS, false,0);
            final Object[] surfOO=jlSurf==null?null:jlSurf.getSelectedValues();
            Object toSelection= q==jlSurf && id==MOUSE_PRESSED ? get(0,surfOO) : null;
            if (sze(surfOO)>0) {
                if (gcp(KEY_ACTION_COMMAND,q)=="CH_COLOR" && color!=null) {
                    for(Object o : surfOO) sendCommand(0L, COMMANDchange_object_color+" "+o+" #"+hex, pvF);
                }
                if (cmd==COMMANDobject_delete || cmd=="UPDATE" || cmd==COMMANDchange_object_color) {
                    for(Object obj : surfOO) {
                        final String name=toStrg(obj);
                        if (cmd==COMMANDobject_delete) {
                            sendCommand(0L, COMMANDobject_delete+" "+name, pvF);
                            adUniq(name,   V3dListModel.list(V3dListModel.DELETED_OBJECTS, pvF, true));
                            enableDisable();
                        }
                        if (cmd==COMMANDchange_object_color) updateSurface(name, pvF, true, false);
                        if (cmd=="UPDATE") updateSurface(name, pvF, false, false);
                        toSelection=name;
                    }
                    revalAndRepaintCs(jlSurf);
                }
                if (sze(toSelection)>0) updateSurface(toStrg(toSelection), pvF, false, true);
            }
            if (cmd=="UNDEL_O")  undelObj(true, pvF);
            if (cmd=="UNDEL_OBJ")undelObj(false,pvF);
            if (cmd==BUT_SURF_OBJ) ChFrame.frame("", pnlSurfaceObjects(), ChFrame.ALWAYS_ON_TOP|ChFrame.PACK|CLOSE_CtrlW_ESC).shw();
        }/* pvF */
        /* Atom selection */
        if (cmd!=null && cmd.startsWith(DIA)) openDialogSelect(cmd);
        if (cmd=="TF" || gcp(KEYS_DIALOG_SEL,q)!=null && cmd==ACTION_ENTER) {
            final Object tf=gcp(KEYS_DIALOG_SEL,q);
            final int t=atoi(gcp(KEY_SEL_DIALOG_TYPE,tf));
            final String txt=toStrg(tf), atoms=isEnabld(gcp("ATOM",tf))?toStrgTrim(gcp("ATOM",tf)):null;
            ProteinViewer pvS=pvF;
            if (t==SELECT_ATOMS_G || t==SELECT_ATOMS) setSelAtomType(SELECT_ATOMS_G==t, txt, pvF);
            if (t==SELECT_AMINO) {
                final String tt[]=splitTokns(txt, chrClas("/ "));
                Protein pAlready=null;
                for(int i=tt.length; --i>=0;) {
                    Protein p0=SPUtils.proteinWithName(tt[i], StrapAlign.proteins());
                    if (p0!=null && pAlready!=p0) {
                        if (pAlready!=null) error("You can select residues only in one protein");
                        pAlready=p0;
                    }
                    final ProteinViewer pv0=viewForProtein(p0,pvF);
                    if (pv0!=null) {
                        setPV(PV_FOCUSED, pvS=pv0);
                        tt[i]=null;
                    }
                }
                if (pvS!=null) setSelection3D(0L, textToSelection3D(new BA(0).join(tt," "), sp(pvS)), "Saved", pvS);
            }
            final Selection3D[] orig=getSelection3D(pvF),save=new Selection3D[sze(orig)];
            String saveID=null;
            if (sze(atoms)>0) {
                if (t==SELECT_LABEL) {
                    for(int i=save.length; --i>=0;) save[i]=new Selection3D(orig[i]);
                    saveID=gcps(KEY_SEL_ID,  pvF);
                }
                setSelAtomType(true, atoms, pvF);
            }

            if (t==SELECT_LABEL) {
                sendCommand(0L, COMMANDlabel+" "+(sze(txt)==0?"off":txt), pvF);
                if (sze(atoms)>0) setSelection3D(0L, save, saveID, pvF);
            }
            sendCommand(0L, COMMANDhighlight_selected_atoms+" 999", pvS);
            sendCommand(0L, COMMANDhighlight_selected_amino_acids+" 999", pvS);
        }
        if (q instanceof ChTextField && gcp(KEYS_DIALOG_SEL, q)!=null) {
            newRow(atoi(gcp(KEY_SEL_DIALOG_TYPE,q)),  parentC(parentC(q)));
        }
        if (cmd==ACTION_SET_FOCUS) {
            final ProteinViewer pv=get(0,gcp(KEY_PROTEIN_VIEWER,q),ProteinViewer.class);
            if (pv!=null) {
                setPV(PV_FOCUSED,pv);
                inEDTms(thread_setWndwStateT('F', -1, -1, deref(pv.getProperty(GET_FRAME_TITLE), String.class)),333);
            }
        }
        if (id==MOUSE_ENTERED && q instanceof ChJList && gcp(ProteinViewer.class,q)!=null) {
            final ChJList v=(ChJList)q;
            final Protein pp[]=ppInV3D(gcp(ProteinViewer.class,q,ProteinViewer.class));
            if (sze(v)!=pp.length || !cntainsAll(v,pp)) {
                v.setData(pp);
                revalAndRepaintC(v);
            }
        }
        if ( (id==MOUSE_PRESSED || id==MOUSE_EXITED || id==MOUSE_ENTERED || id==MOUSE_MOVED) && q!=null && !(q instanceof ChJList)) {
            final ProteinViewer pvRep=gcp(KEY_PROTEIN_VIEWER, q, ProteinViewer.class);
            if (pvRep!=null) {
                final ProteinViewer pv=cntains(pvRep,vvSharingView(pvF)) ? pvF : pvRep;
                if (isActive(pv) && id==MOUSE_PRESSED) {
                    setPV( pv.getProperty(GET_CANVAS)==q ? PV_FOCUSED : PV_FOCUSED_NOT_MB, pv);
                    StrapAlign.setToolpane(toolpane());
                    return;
                }
            }
        }
        {
            final int iA=selection3dToFirstLastIdxZ(false, false, _aaPick, pF);
            if (iA>=0 && (cmd=="ADD_S" || cmd=="RM_S" || cmd=="SET_C" || isPopupTrggr(false,ev))) {
                final BasicResidueSelection s=residueSelectionsF(cmd=="ADD_S");
                if (isPopupTrggr(false,ev)) {
                    if (q instanceof ChButton && isPopupTrggr(true,ev)) StrapAlign.showContextMenu('S',oo(s));
                    return;
                }
                if (cmd=="SET_C") StrapAlign.setCursor(pF, iA);
                else if (s!=null) {
                    BasicResidueSelection.setAminoSelected(cmd=="ADD_S", iA+Protein.firstResIdx(pF), s);
                    final boolean bb[]=s.getSelectedAminoacids();
                    if (fstTrue(bb)<0) pF.removeResidueSelection(s);
                    setSelection3D(0L, toSelectionS(s), "", pvF);
                    setPV(PV_FOCUSED,pvF);
                    pF.removeResidueSelection(findWithName(ResidueSelection.NAME_CURSOR, pF.residueSelections(),BasicResidueSelection.class));
                    StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,10);
                }
                StrapAlign.animatePositionZ(StrapAlign.ANIM_SET_CURS_SCROLL_TO_VISIBLE, pF, iA);
                enableDisable();
            }
        }
        if (isActive(pvF)) {
            final int n=scaleUpDown(ev);
            if (n!=0) sendCommand(0L, COMMANDzoom+" "+(100-n*10), pvF);
            else {
                final int rotZ=3*wheelRotation(ev);
                if (rotZ!=0)sendCommand(0L, COMMANDrotate+" z"+" "+rotZ, pvF);
            }
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> StrapEvent >>> */

    static void handleEvent(StrapAlign align, StrapEvent ev) {
        final int id=ev.getType();
        final ProteinViewer pvF=getPV(PV_FOCUSED);
        if (id==StrapEvent.OBJECTS_SELECTED) {
            final Protein pp[]=StrapAlign.selectedProteins();
            if (pp.length==1 && wref(pp[0])!=_selectedInStrap) {
                _selectedInStrap=wref(pp[0]);
                setFocusedP(true, pp[0]);
            }
        }
        if (id==StrapEvent.PROTEIN_VIEWER_CLOSED) disposeV(deref(ev.getSource(), ProteinViewer.class));
        if (id==StrapEvent.PROTEIN_VIEWER_PICKED) setSelection3dPicked((Selection3D)ev.parameters()[0] , (ProteinViewer)ev.getSource(), atoi(ev.parameters()[1]));
        if (id==StrapEvent.BACKGROUND_CHANGED) {
            final String cmd=isWhiteBG() ? COMMANDbackground+" #FFffFF" : COMMANDbackground+" #000000";
            for(Protein p : align.getProteins()) {
                for(ProteinViewer pv : p.getProteinViewers()) sendCommand(0L, cmd, pv);
            }
        }
        if (id==StrapEvent.PROTEIN_VIEWER_SURFACES_CHANGED) {
            revalAndRepaintCs(V3dListModel.jList(V3dListModel.SURFACE_OBJECTS, false, 0));
        }
        if (id==StrapEvent.PROTEIN_3D_MOVED) {
            for(Protein p : align.getProteins()) {
                Matrix3D m3d=p.getRotationAndTranslation();
                if (m3d!=null && m3d.isUnit()) m3d=null;
                final int mc=p.mc(ProteinMC.MC_MATRIX3D);
                for(ProteinViewer v : p.getProteinViewers()) {
                    if (gcp(KOPT_IS_PREVIEW,v)==null && mc!=atoi(gcp(KEY_MARIX_MC, v))) transform3D(m3d,v);
                }
            }
        }
        if (id==StrapEvent.CURSOR_CHANGED_PROTEIN || id==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN_DELAYED || id==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN || id==StrapEvent.CURSOR_CLICKED) {
            if (isSelctd(pvp().b(TOG_CURSOR_SELECT))) {
                final Protein pCursor=StrapAlign.cursorProtein();
                final int iA=StrapAlign.indexOfAminoAcidAtCursorZ(pCursor);
                final ProteinViewer[] vvFocused=vvSharingView(pvF);
                int isSetMB=0;
                if (pCursor!=null && iA>=0) {
                    for(ProteinViewer pv : pCursor.getProteinViewers()) {
                        final boolean delay= 0!=(flags(pv) & PROPERTY_SEQUENCE_CURSOR_DELAYED);
                        if (id==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN_DELAYED && !delay) continue;
                        if (id==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN && delay) continue;
                        setSelAminosI(0L, iA, SELECTION_CURSOR, pv);
                        sendCommand(0L, COMMANDhighlight_selected_amino_acids,pv);
                         if (0==isSetMB++ && StrapAlign.isOtherMenuBar() && pv!=pvF ) maySetTitleStyleMenu(pv);
                    }
                }
            }
        }
        if (id==StrapEvent.RESIDUE_SELECTION_CHANGED || id==StrapEvent.RESIDUE_SELECTION_ADDED || id==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR ||
            (id&StrapEvent.FLAG_ATOM_COORDINATES_CHANGED)!=0 || id==StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED /*|| id==StrapEvent.BIOMOLECULES_SPECIFIED*/) {
            for(Protein p : align.getProteins()) {
                for(ProteinViewer pv : p.getProteinViewers()) {
                    p.selAminos();
                    /* TODO nur die geaenderten */
                    if (pv instanceof StrapListener) ((StrapListener)pv).handleEvent(ev);
                    if (pv instanceof Protein3d.PView) {
                        final Protein3d p3d=(Protein3d)viewerCanvas(pv);
                        if (id==StrapEvent.RESIDUE_SELECTION_CHANGED)  { p3d.enableDisable(); ChDelay.repaint(p3d,200);}
                        if (id==StrapEvent.RESIDUE_SELECTION_ADDED || id==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR ||
                            (id&StrapEvent.FLAG_ATOM_COORDINATES_CHANGED)!=0 || id==StrapEvent.HETERO_OR_NUCLEOTIDE_STRUCTURE_ADDED_OR_REMOVED)
                            ChDelay.repaint(p3d,11);
                    }
                }
            }
            for(int vers01=2; --vers01>=0;) {
                for(int type : V3dListModel.TYPES_SEL) {
                    revalAndRepaintCs(V3dListModel.jList(type,false,vers01));
                }
            }

        }
    }
    /* <<< StrapEvent <<< */
    /* ---------------------------------------- */
    /* >>> Selection >>> */
    private final static Object KEY_SEL_ATOM=new Object(), KEY_SEL_ID=new Object();
    public static Selection3D[] getSelection3D(ProteinViewer pv) {
        Selection3D[] ss=gcp(KEY_SEL_ATOM, pv, Selection3D[].class);
        if (ss==null) ss=gcp(KEY_SEL_ATOM, sharedHashMapV3D(pv), Selection3D[].class);
        return ss==null?Selection3D.NONE:ss;
    }
    private static void setSelAminosI(long opt, int iA, String id, ProteinViewer pv) {
        final Protein p=p(pv);
        if (p==null) return;
        if (pv instanceof Protein3d.PView && id==ProteinViewer.SELECTION_CURSOR) {
            final Protein3d p3d=(Protein3d)pv.getProperty(GET_CANVAS);
            if (p3d!=null) p3d.highlightAminoAcidZ(p,iA);
        } else setSelection3D(opt, rangeToSelection3D(false,iA,iA, p), id,  pv);
    }
    /*
      Not using sendCommand(...) because sendCommand(...) is delegating here!
     */
    static void setSelection3D(long opt, Selection3D atoms[], String id, ProteinViewer pv) {
        final Protein p=p(pv);
        if (atoms==null || p==null) return;
        final Protein3d.PView p3d=deref(pv, Protein3d.PView.class);
        if (p3d!=null) {
            p3d.setSelectedResidues(selection3dToBoolZ(atoms, p), Protein.firstResIdx(p));
        }
        else if (!supports(COMMANDselect,pv)) return;
        if (sze(id)>0 && supports(COMMANDselection_name, pv)) pv.interpret(0L, COMMANDselection_name+" "+id);
        pv.interpret(opt, COMMANDselect+" "+selection3dToText(GENERIC, atoms));
        pcp(KEY_SEL_ID, id,  pv);
        pcp(KEY_SEL_ATOM, atoms,  pv);
        maySetTitleStyleMenu(pv);
    }
    private static void setSelAtomType(boolean isGeneric, String atomType, ProteinViewer pv) {
        if (p(pv)!=null) {
            final Selection3D[] atoms=getSelection3D(pv);
            if (countNotNull(atoms)==0) return;
            changeAtoms(isGeneric, atoms,atomType);
            setSelection3D(0L, atoms, gcps(KEY_SEL_ID,pv), pv);
        }
    }
    static boolean[] selectedAAto3D_single(long opt, ResidueSelection ss[], ProteinViewer viewer, String selName) {
        final Protein p=p(viewer);
        final boolean selected[]=ResSelUtils.mapPositionsZ(ss,p);
        if (fstTrue(selected)<0) return null;
        final float[] xyz=p.getResidueCalpha();
        final int countTrue=countTrue(selected);
        int count=0;
        for(int i=mini(sze(xyz)/3, sze(selected)); --i>=0;) {
            if (selected[i] && !Float.isNaN(xyz[i*3])) count++;
        }
        if (countTrue>0) {
            final ProteinViewer vv[]=viewer!=null ? new ProteinViewer[]{viewer} : vvOfProteinAsk(p, (Class)null);
            if (vv.length>0) {
                boolean needsName=false;
                for(ProteinViewer v : vv) needsName|= 0!=(PROPERTY_NAMED_SELECTIONS&flags(v));
                String name=null;
                if (needsName) {
                    final Object
                        msg=pnl(VBHB,"Protein "+p+"<br>Selected amino acids: ",
                                 scrllpn(0, monospc(Protein.selectedPositionsToText(selected,0,p)), dim(1,3*EX)),
                                 "What should the selection be named in the protein viewer?",
                                 (count==0 ? "<br>Problem: None of the selected amino acids has coordinates.<br>You may not see the selection.":null)
                                 );
                    name=
                        selName!=null ? selName :
                        0!=(opt&RSto3D_GENERIC_NAME) ? toStrg(new BA(99).boolToText(selected,1,"__","_")) :
                        0!=(PROPERTY_NAMED_SELECTIONS&flags(viewer)|(opt&RSto3D_ASK_NAME)) ? ChMsg.input(msg,20,"my_selection") :
                        "Unnamed";
                }
                if (!needsName || sze(name)>=0) {
                    for(ProteinViewer v : vv) {
                        setSelection3D(0L, boolToSelection3D(false, selected, 0, p, ""), name,v);
                        if (sze(p.getProteinViewers())==1) setPV(PV_FOCUSED,v);
                    }
                }
            }
        }
        return selected;
    }
    public final static long
        RSto3D_COMMANDS=1L<<11, RSto3D_COLOR=1L<<2, RSto3D_AS_ONE_SINGLE=1L<<3,
        RSto3D_SAME_PROTEIN=1L<<4,
        RSto3D_ASK_NAME=1L<<5, RSto3D_REPORT_ERROR=1L<<6, RSto3D_NOT_SAME_WIRE=1L<<7,
        RSto3D_NO_WIRE=1L<<8,  RSto3D_GENERIC_NAME=1L<<9,
        RSto3D_SET_FOCUSED_PV=1<<11;
    public static boolean residueSelectionsTo3D(long opt, ResidueSelection ss[], ProteinViewer[] vv) {
        int count=0;
        String error=null;
        if (0==(opt&RSto3D_AS_ONE_SINGLE)) {
            for(ResidueSelection s:ss) {
                final Protein prot=sp(s);
                if (prot==null) continue;
                final ResidueAnnotation a=deref(s,ResidueAnnotation.class);
                Selection3D atoms[]=null;
                ProteinViewer pvSel=null;
                for(ProteinViewer v : vv!=null ? vv : prot.getProteinViewers()) {
                    final Protein p=p(v);
                    if (0!=(RSto3D_SAME_PROTEIN&opt) && prot!=p) continue;
                    final String id=s instanceof ResidueAnnotation ?  ((ResidueAnnotation)s).getID() :
                        new BA(99).filter(FILTER_NO_MATCH_TO|'_', LETTR_DIGT,s).toString();
                    final boolean bb[]=ResSelUtils.mapPositionsZ(new ResidueSelection[]{s}, p);
                    if (fstTrue(bb)<0) { error="The selected residues do not map to the protein "+p; continue;}
                    final String pos=a==null?null:a.value(ResidueAnnotation.POS);
                    if (atoms==null && pos!=null && pos.indexOf('.')>0) atoms=textToSelection3D(true, pos,p);
                    if (atoms==null) atoms=boolToSelection3D(false,bb, 0, p,"");
                    setSelection3D(0L, atoms, id, v);
                    sendCommand(0L, COMMANDhighlight_selected_amino_acids,v);
                    pvSel=v;
                    if (v instanceof Protein3d.PView) continue;
                    if ( (RSto3D_COMMANDS&opt)!=0 && a!=null) {
                        /* Color of a */
                        boolean hasColorCmd=false, hasViewerCommand=false;
                        for(ResidueAnnotation.Entry e : a.entries()) {
                            final String k=e.key(), val=e.value();
                            hasColorCmd= hasColorCmd || k==ResidueAnnotation.VIEW3D && val.startsWith(COMMANDcolor);
                            hasViewerCommand|=k==ResidueAnnotation.VIEW3D;
                        }
                        if (!hasColorCmd && 0!=(RSto3D_COLOR&opt)) {
                            final String hex=rgbHex(colr(s),true);
                            for(int iC=2; --iC>=0 && hex!=null;) {
                                sendCommand(INTERPRET_NO_MSG_DIALOGS, (iC==1?COMMANDcolor : COMMANDsurface_color) + " #"+hex, v);
                            }
                        }
                        /* Command */
                        final String cn=clasNam(v), scn=shrtClasNam(v);
                        for(ResidueAnnotation.Entry e : a.entries()) {
                            final String k=e.key(), val=e.value();
                            if (!e.isEnabled() || sze(val)==0) continue;
                            if (k==COMMANDselect && supports(COMMANDselect,v)) {
                                if (chrAt(0,val)=='.') changeAtoms(true, atoms, val); else atoms=textToSelection3D(true, val, p);
                            }
                            if (k==ResidueAnnotation.ATOMS) changeAtoms(true, atoms, val);
                            boolean doSend=cn.equals(StrapPlugins.mapS2L(k));
                            if (ResidueAnnotation.VIEW3D==k && (supports(val,v) || k.equals(cn) || k.equals(scn))) {
                                setSelection3D(0L, atoms, id, v);
                                doSend=true;
                            }
                            if (doSend) sendCommand(INTERPRET_NO_MSG_DIALOGS, ResSelUtils.replaceVariable(val,a) , v);
                        }
                    }
                    count++;
                }
                if (pvSel!=null && 0!=(opt&RSto3D_SET_FOCUSED_PV)) setPV(PV_FOCUSED, pvSel);
            }
        }
        if (count!=1) {
            for (ProteinViewer v  : vv!=null ? vv : views3D(SPUtils.spp(ss),null)) {
                final boolean isP3d=v instanceof Protein3d.PView;
                if (!isActive(v) || isP3d && 0!=(opt&RSto3D_NO_WIRE)) continue;
                ResidueSelection ss2[]=ss;
                if (0!=(opt&RSto3D_NOT_SAME_WIRE) && isP3d) {
                    for(int i=sze(ss); --i>=0;) {
                        final ResidueSelection s=ss[i];
                        if (sp(s)==sp(v)) {
                            (ss2!=ss ? ss2=ss.clone() : ss2 )[i]=null;
                            if (s instanceof VisibleIn123) {
                                final int
                                    w0=((VisibleIn123)s).getVisibleWhere(),
                                    w1=w0|VisibleIn123.STRUCTURE;
                                if (w0!=w1) {
                                    ((VisibleIn123)s).setVisibleWhere(w1);
                                    StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,99);
                                }
                            }
                        }
                    }
                }
                final String name=0!=(opt&RSto3D_AS_ONE_SINGLE)? null:"Unnamed";
                count+=countTrue(selectedAAto3D_single(opt, rmNullA(ss2,ResidueSelection.class), v, name));
            }
        }
        if (count==0 && 0!=(opt&RSto3D_REPORT_ERROR)) error(error!=null?error:"No residue selection copied to any 3D-viewer.");
        return count>0;
    }
    public static void residueSelectionsTo3D_I(long option, ResidueSelection ss[], ProteinViewer vv[]) {
        AbstractButton cbOne=null, cbCmd=null, cbCol=null;
        boolean advancedPV=false;
        int hasPv=0;
        for(ResidueSelection s : ss) {
            final Protein p=sp(s);
            if (p==null) continue;
            for(ProteinViewer v : vv!=null ? vv : p.getProteinViewers()) {
                if (0!=(option&RSto3D_NO_WIRE) && v instanceof Protein3d.PView) continue;
                hasPv++;
                if (v instanceof Protein3d.PView) continue;
                advancedPV=true;
                if (cbCol==null) cbCol=cbox("Infer the color of the amino acid selection");
                if (s instanceof ResidueAnnotation) {
                    final String cn=clasNam(v), scn=shrtClasNam(v);
                    for(ResidueAnnotation.Entry e : ((ResidueAnnotation)s).entries()) {
                        if (!e.isEnabled()) continue;
                        final String k=e.key(), val=e.value();
                        if (cbCmd==null && (k==ResidueAnnotation.VIEW3D && supports(val,v) || k.equals(cn) || k.equals(scn)))
                            cbCmd=cbox("Send 3D-Viewer  commands contained in residue annotation");
                    }
                }
            }
        }
        if (hasPv==0) return;
        cbOne=ss.length>1 && advancedPV ? cbox("Combine all "+ss.length+" amino acid selection  to one single amino acid selection to be passed to the 3D-View") : null;
        if (cbOne!=null && cbCol!=null) radioGrp(0, cbOne,cbCol);
        if (cbOne!=null && cbCmd!=null) radioGrp(0, cbOne,cbCmd);
        if ( !ChMsg.yesNo(pnl(VB,"Sending "+ss.length+" residue selections to 3D-View",cbOne,cbCmd, cbCol))) return;
        final long opt=option
            | RSto3D_REPORT_ERROR
            | RSto3D_SET_FOCUSED_PV
            | (isSelctd(cbCmd) ? RSto3D_COMMANDS : 0)
            | (isSelctd(cbOne) ? RSto3D_AS_ONE_SINGLE|RSto3D_ASK_NAME : 0)
            | (isSelctd(cbCol) ? RSto3D_COLOR : 0);
        if (!residueSelectionsTo3D(opt,ss, vv)) {
            if (_msgDnD==null) {
                _msgDnD=pnl(VB,
                          "Conveniently, Drag-and-drop can be used to get a residue-selection into a 3D-viewer.",
                          "This works for all Java-based 3D-Viewers like Astex, but not for native ones like Pymol.",
                          " ",
                          "The protein of the residue-selection may be different from the one in the 3D-viewer.",
                          " ",
                          WATCH_MOVIE+MOVIE_Sequence_Features_in_3D
                          );
                ChMsg.msgDialog(0L, _msgDnD);
            }
        }
    }
    /* <<< ResidueSelection <<< */
    /* ---------------------------------------- */
    /* >>> Atom Selection >>> */
    final static int SELECT_ATOMS_G=1, SELECT_ATOMS=2, SELECT_AMINO=3, SELECT_LABEL=4;
    private final static Object KEY_SEL_DIALOG_TYPE=new Object(), KEYS_DIALOG_SEL[]={null, new Object(),new Object(),new Object(), new Object()};
    private static ChTextField newRow(int t, Component parent) {
      for(ChTextField tf : childsR(parent, ChTextField.class)) if (sze(toStrg(tf))==0) return tf;
        final ChTextField tf=new ChTextField().cols(40,true,false).li(li());
        StrapAlign.newDropTarget(tf,true);
        pcp(KEY_SEL_DIALOG_TYPE, intObjct(t), tf);
        final Component
            bClear=tf.tools().newClearButton().t("C").tt("Clear"),
            b=new ChButton("TF").cp(KEYS_DIALOG_SEL, wref(tf)).t(t==SELECT_LABEL?"Set label": "Apply").li(li()),
            row=pnl(CNSEW,tf, null,null,b, bClear);
        if (t==SELECT_AMINO) {
            li().addTo("k",tf);
            StrapAlign.highlightProteins("AP", tf);
        }
        TEXT_TF[t]=adNotNullNew(tf, TEXT_TF[t]);
        pcp(KEYS_DIALOG_SEL, wref(tf), tf);
        pcp("ATOM", wref(gcp("ATOM",parent)), tf);
        ((Container)parent).add(row);
        closeOnKey(CLOSE_CHILDS|CLOSE_CtrlW, row, REP_PARENT_WINDOW);
        packW(parent);
        return tf;
    }
    private final static Component[] TEXT_DIALOGS=new Component[5];
    private final static Collection<ChTextField> TEXT_TF[]=new Collection[5];
    private static void openDialogSelect(String cmd) {
        final int t=atoi(cmd,sze(DIA));
        Component dialog=TEXT_DIALOGS[t];
        if (dialog==null) {
            final BA sb=new BA(" Specify atom types for the current amino acid selection using the ");
            if (t==SELECT_ATOMS) {
                sb.a("specific syntax of the particular 3D-viewer. <ul>");
                for(Object o :  StrapPlugins.allClassesV(ProteinViewer.class).asArray()) {
                    final ProteinViewer v=mkInstance(o, ProteinViewer.class, false);
                    final Object example=v==null?null:v.getProperty(GET_ATOM_SELECTION_EXAMPLE);
                    if (example!=null) sb.a("<li>").a(niceShrtClassNam(v)).a("<pre>").a(example).a("</pre></li>");
                }
                sb.a("</ul>");
            }
            final Object
                info=
                t==SELECT_LABEL ? "A label text attached to the selected atoms." :
                t==SELECT_AMINO ?
                "An amino acid / atom selection like \"10-20,44\".<br>"+
                "Example giving the peptide chain:<pre class=\"data\">  30:C</pre>"+
                "Example giving the insertion code:<pre class=\"data\">  30A:C</pre>"+
                "Example for atoms C-Alpha and C-Beta:<pre class=\"data\">  30:C.CA.CB</pre>"+
                "Example for any carbon atom:<pre class=\"data\">  30:C.C*</pre>"+
                "Example of selecting residues by name:<pre class=\"data\">  \"GLY\"</pre>"+
                "Example for hetero compounds:<pre class=\"data\">  \"FAD\"</pre>"+
                "Example for water molecules:<pre class=\"data\">  \"HOH\"</pre>"+
                "<br>To refere to a specific protein just type the protein name flanked by white space.<br>"+
                "For convenience use tab-key completion for protein names.<br>"+
                "<br>Inserting the current selection: Type Ctrl+Shift+V"
                :
                t==SELECT_ATOMS ? sb : sb.a("generic (same for all 3D-programs) syntax.<br>").a(TT_ATOMS);
            final Component
                tfAtoms=t==SELECT_ATOMS || t==SELECT_ATOMS_G ? null : new ChTextField(t==SELECT_LABEL?".CA":".C.CA.CB.N.O").tt(TT_ATOMS),
                cbAtoms=toggl("Optional atom type: ").s(t==SELECT_LABEL).doEnable(tfAtoms).tt(TT_ATOMS).cb(),
                pSouth=
                t==SELECT_ATOMS_G ? pnl("Alternatively use native expressions of this ", new ChButton(DIA+SELECT_ATOMS).li(li()).t("specific 3D-viewer")) :
                tfAtoms==null?null:  pnl(CNSEW, tfAtoms,null,null, null,cbAtoms),

                pCenter=pnl(VB),
                panInfo=pnl(info+"<br>"+hintAOT(true)),
                pTop=pnl(VBHB,
                          pnl(HBL,t==SELECT_ATOMS?C(0x88FF88): t==SELECT_ATOMS_G?C(0x00FF00):t==SELECT_AMINO?C(0xaaAA00):null, pnlTogglOpts("*Info",panInfo)),
                          panInfo
                          );
            pcp(KOPT_NOT_PAINTED_IF_DISABLED,"",tfAtoms);
            pcp("ATOM", wref(tfAtoms), pCenter);
            TEXT_DIALOGS[t]=dialog=pnl(CNSEW,pCenter, pTop,pSouth);
            final String ss[]=readLines(fileTB(t));
            if (ss!=null) for(String s : ss) if (nxt(-SPC,s)>=0) newRow(t,pCenter).t(s);
            addShutdownHook1(thrdCR(instance(),"SAVE_TB", intObjct(t)));
            newRow(t, pCenter);
        }
        ChFrame.frame(t==SELECT_LABEL ? "Label text" : t==SELECT_AMINO?"Aminos ":"Atoms ", dialog, ChFrame.ALWAYS_ON_TOP|CLOSE_CtrlW_ESC).shw(ChFrame.AT_CLICK|ChFrame.PACK);
    }
    final static File fileTB(int t) { return file(STRAPTMP+"/3D_sel_textbox"+t+".txt"); }
    /* <<< Atom Selection <<< */
    /* ---------------------------------------- */
    /* >>> View >>> */
    private static Map<ProteinViewer,Runnable> mapV2F=new WeakHashMap();
    static Runnable runViewerToFront(ProteinViewer pv) {
        Runnable r=mapV2F.get(pv);
        if (r==null) mapV2F.put(pv,r=thrdCR(instance(), "viewerToFront",new Object[]{pv,longObjct(OPEN_ADD_TAB)}));
        return r;
    }
    public static void viewerToFront(long opt, ProteinViewer pv) {
        if (pv==null) return;
        if (!isEDT()) inEdtLater(runViewerToFront(pv));
        else {
            final String cn=clasNam(pv);
            final Component panel=viewerCanvas(pv);
            if (panel!=null) {
                final ChTableLayout tl=StrapAlign.tPanel(cn);
                final Window w=parentWndw(panel);
                if (tl!=null && cntains(panel,tl.getAllComponents())) StrapAlign.addDialog(tl);
                else if (gcp(KEY_detach_component,w)!=null) setWndwState('F',w);
                else if ((opt&OPEN_ADD_TAB)!=0) tl.addCol(CLOSE_DISPOSE, panel);
                if ((opt&OPEN_NARROW_OTHERS)!=0 && tl!=null) tl.narrowAllOthers(panel, ICON_HEIGHT);
            }
        }
    }

    private final static Component[] _jc=new Component[3];
    final static int GET_ANIM_SPEED=1;
    static Component comp(int id) {
        Component c=_jc[id];
        if (c==null) {
            if (id==GET_ANIM_SPEED) c=new ChTextField("  6.0");
            _jc[id]=c;
        }
        return c;
    }
    /* <<< View <<< */
    /* ---------------------------------------- */
    /* >>> Script >>> */

    public static void colorAllAminos(Color c, ProteinViewer pv) {
        if (c!=null && pv!=null) {
            pcp(KEY_COLOR,c,pv); /* For ChRenderer */
            pcp(KEY_COLOR,c,p(pv));
            StrapEvent.dispatchLater(StrapEvent.PROTEIN_ICON_CHANGED,33);
            sendCommand(0L, COMMANDselect+" $AMINOACID", pv);
            sendCommand(0L, COMMANDcolor+" #"  +rgbHex(c,false), pv);
        }
    }

    private final static long SEND_NO_STORE=1L<<32;
    public static boolean sendCommand(long opt, CharSequence command0, ProteinViewer pv) {
        final Protein p=p(pv);
        String command=toStrgTrim(command0);
        if (p==null || sze(command)==0) return false;
        if (command.indexOf('\n')>0) {
            boolean succ=true;
            for(String line:splitTokns(SPLIT_TRIM, command,0,MAX_INT, chrClas1('\n'))) succ&=sendCommand(opt, line, pv);
            return succ;
        }
        final boolean sup=supports(command,pv);
        final String w0=toStrgIntrn(wordAt(nxt(-SPC,command),command));
        if (w0==COMMANDselect) { /* !! setSelAminos is not calling sendCommand */
            setSelection3D(0L, textToSelection3D(delPfx(w0,command), p(pv)), "", pv);
            return true;
        }
        if (w0==COMMAND_HIDE_EVERYTHING) {
            for(String sc : STYLE_COMMANDS) if (supports(sc,pv)) sendCommand(opt,sc+" off",pv);
            return true;
        }
        if (idxOf(w0,allCommands())<0 || sup) {
            final Selection3D atoms[]=getSelection3D(pv);
            if (w0==COMMANDlabel) {
                final boolean bb[]=selection3dToBoolZ(atoms,p);
                command=toStrg(ResSelUtils.replaceVariableBB(command, bb, Protein.firstResIdx(p), gcps(KEY_SEL_ID,pv), p, null));
            }
            pv.interpret(opt,command);
            final String objectName=(String)pv.getProperty(GET_LAST_CREATED_OBJECT);
            final Map map=sharedHashMapV3D(pv);
            final boolean store=0==(opt&SEND_NO_STORE);
            if (objectName!=null) {
                final Color color=gcp(KEY_SURF_COLOR, pv,Color.class);
                if (store) {
                    pcp2(false, KEY_SURF_ATOMS,  objectName, atoms, map);
                    pcp2(false, KEY_SURF_CMD,    objectName, command,     map);
                }
                pcp2(true, KEY_SURF_COLOR, objectName, color, map);
                pcp(KEY_COLOR, color, objectName);
                pcp(KEY_SURF_COLOR, pv, null);
            }
            if (sup) {
                final long rgb=rgbInScript(command);
                final Color color=rgb==-1 ? null : new Color((int)rgb,true);
                if (w0==COMMANDchange_object_color) {
                    final String thisObj=objectIdInScript(command);
                    pcp(KEY_COLOR, color, thisObj);
                    pcp2(false, KEY_SURF_COLOR, thisObj, color, map);
                    ChDelay.repaint(V3dListModel.jList(V3dListModel.SURFACE_OBJECTS,false,0),222);
                    ChDelay.repaint(V3dListModel.jList(V3dListModel.DELETED_OBJECTS,false,0),222);
                }
                if (w0==COMMANDsurface_color) pcp(KEY_SURF_COLOR, color, pv);
                if (w0==COMMANDcolor && 0!=(PROPERTY_RIBBON_COLOR_NO_CHANGE&flags(pv))
                    && color!=null && isSelctd(_cbSurfaceColors)) updateSurfacesP(pv, false);
            }
            return true;
        }
        return false;
    }
    /* <<< Script <<< */
    /* ---------------------------------------- */
    /* >>> Surfaces >>> */
    private final static Object KEY_SURF_CMD=new Object(), KEY_SURF_COLOR=new Object(), KEY_SURF_ATOMS=new Object();
    private static void updateSurfacesP(ProteinViewer pv, boolean updateColor) {
        final String selID=gcps(KEY_SEL_ID, pv);
        final Selection3D[] atomType=getSelection3D(pv);
        final List v=(List)pv.getProperty(GET_SURFACEOBJECTS);
        int count=0;
        for(int i=sze(v); --i>=0;) {
            updateSurface(getS(i,v), pv, updateColor, false);
            count++;
        }
        if (count>0) setSelection3D(0L, atomType , selID,  pv);
    }
    private static void updateSurface(String obj, ProteinViewer pv, boolean updateColor, boolean selectOnly) {
        final List vObjects=pv==null ? null : (List) pv.getProperty(GET_SURFACEOBJECTS);
        if (vObjects==null || obj==null) return;
        final Map map=sharedHashMapV3D(pv);
        final String script=gcp2(KEY_SURF_CMD, obj, map, String.class);
        final Selection3D[] atoms=gcp2(KEY_SURF_ATOMS, obj, map, Selection3D[].class);
        final Color color=gcp2(KEY_SURF_COLOR, obj, map, Color.class);
        if (selectOnly) setSelection3D(0L, atoms, "",  pv);
        else if (script!=null) {
            sendCommand(0L, COMMANDobject_delete+" "+obj,  pv);
            setSelection3D(0L, atoms, "",  pv);
            sendCommand(0L, script, pv );
            if (color!=null && updateColor) sendCommand(0L, COMMANDchange_object_color+" "+obj+" #"+rgbHex(color,true),  pv);
        }
    }
    public static void openColorDialog() {
        if (_colorDia==null) {
            final ChJList jl=V3dListModel.jList(V3dListModel.PROTEINS, true,0);
            if (_cbSurfaceColors==null) _cbSurfaceColors=toggl("Also update colors of ribbons").s(true);
            final Object
                pNorth=pnl(HBL,"To change the color of the entire protein you need to select a protein:"),
                pEntireProt=pnl(CNSEW,scrllpn(0,jl, dim(EX,4*ICON_HEIGHT)),pNorth),
                pEntropy=pnl(VBHB,"<h2>Set color according to sequence variation</h2>",
                             pnl(HB, "First select some aligned proteins. ", new ChButton("ENTROPY_COLOR").t(ChButton.GO).li(li()))
                             ),
                pSouth=pnl(VBHB,
                           _cbSurfaceColors.cb(),
                           " ",
                           pEntireProt,
                           pnl(HBL, _cbColorEntire=toggl().doCollapse(new Object[]{jl, pEntireProt}),"Change color of the entire protein"),
                           pEntireProt,
                           pnl(HBL,toggl().doCollapse(new Object[]{jl, pEntropy}),"Colorize according to sequence variation"),
                           pEntropy
                           );
            (_colorBut=new ButColor(0, C(0xFF00), li())).li(li());
            _colorDia=pnl(CNSEW,getPnl(_colorBut),null,pSouth);
            enableDisable();
        }
        ChFrame.frame("Color", _colorDia, ChFrame.PACK|CLOSE_CtrlW_ESC).shw();
    }
    public static Object pnlSurfaceObjects() {
        if (_sfcDia==null) {
            final Object
                ttU="The surface object is disposed and created de-nove.<br>"+
                "This might be necessary if the protein has moved in 3D or to adjust the color",
                bb[]=new Object[10],
                pSouth=pnl(VBHB,
                            pnl(HBL,
                                bb[0]=new ChButton("UPDATE").li(li()).t("Update ribbon or surface").i(IC_BLANK).tt(ttU),
                                bb[1]=new ChButton(COMMANDchange_object_color).li(li()).t("Update and set color").i(IC_BLANK).tt(ttU)
                                ),
                            bb[2]=new ButColor(ButColor.TRANSPARENCY_CHANGE_EVT, C(0xFF00), li()).li(li())
                            .cp(KEY_ACTION_COMMAND, "CH_COLOR").t("Change color ...")
                            .cp(KEY_FRAME_TITLE, "Surface color "),
                            pnl(HBL,
                                bb[3]=new ChButton(COMMANDobject_delete).li(li()).t("Delete ribbon or surface").i(IC_KILL),
                                bb[4]=new ChButton("UNDEL_O").t("Rescue deleted").li(li()).i(IC_RESCUE).cp(KOPT_HIDE_IF_DISABLED,"")
                                )
                            );
            adAll(bb,vEnableDisable);
            _sfcDia=pnl(CNSEW,scrllpn(0,V3dListModel.jList(V3dListModel.SURFACE_OBJECTS,true,0), dim(222,99)),"This is a list of ribbons and surfaces",pSouth);
            enableDisable();
        }
        return _sfcDia;
    }
    private final static Object KEY_MARIX_MC=new Object();
    public static void transform3D(Matrix3D m3d, ProteinViewer pv) {
        final Protein p=p(pv);
        if (supports(COMMANDset_rotation_translation,pv) && p!=null) {
            sendCommand(0L, COMMANDset_rotation_translation+" "+(m3d==null?null:m3d.toText(0,"",null)), pv);
            pcp(KEY_MARIX_MC, intObjct(p.mc(ProteinMC.MC_MATRIX3D)), pv);
            final long flags=flags(pv);
            if (0!=(flags&PROPERTY_NEEDS_UPDATE_SURFACES_WHEN_ROTATED)) updateSurfacesP(pv,true);
            if (supports(COMMANDcenter_amino,pv)) sendCommand(0L,COMMANDcenter_amino, pv);
        }
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> ToolTip >>> */
    public String provideTip(Object objOrEv) {
        final AWTEvent ev=deref(objOrEv, AWTEvent.class);
        final Object c=evtSrc(objOrEv);
        final ProteinViewer pvF=getPV(PV_FOCUSED);
        final Map mapF=sharedHashMapV3D(pvF);
        if (c instanceof ChJList && mapF!=null) {
            final Object obj=objectAt(null,ev);
            if (obj instanceof String) {
                return
                    "<pre>"+
                    "\n  Cmd: "+gcp2(KEY_SURF_CMD, obj, mapF, String.class)+
                    "\natoms: "+selection3dToText(GENERIC, gcp2(KEY_SURF_ATOMS, obj, mapF, Selection3D[].class))+
                    "\nColor: "+rgbHex(gcp2(KEY_SURF_COLOR, obj, mapF, Color.class), true)+
                    "</pre>";
            }
        }
        if (c instanceof ChButton && gcp(KEY_ACTION_COMMAND,c)==ACTION_SET_FOCUS) {
            final BA sb=baTip().a("<br>Sets this as the current 3D-view.<br>Proteins in this 3D-view:<br>");
            for(Object pv :  oo(gcp(KEY_PROTEIN_VIEWER,c)))sb.and(" * ",sp(pv),"<br>");
            return toStrg(sb);
        }
        return null;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Undelete >>> */
    private static Component _undelObj;
    private static void undelObj(boolean showOnly, ProteinViewer pv) {
        final Map map=sharedHashMapV3D(pv);
        if (map==null) return;
        final ChJList jl=V3dListModel.jList(V3dListModel.DELETED_OBJECTS,true,0);
        if (showOnly) {
            if (_undelObj==null) {
                final Object
                    b=new ChButton("UNDEL_OBJ").t("Undelete").li(li()),
                    sp=scrllpn(0, jl, dim(99,99));
                _undelObj=pnl(CNSEW,sp,null,pnl(b));
                vEnableDisable.add(b);
            }
            ChFrame.frame("Deleted",_undelObj, CLOSE_CtrlW_ESC).shw();
        } else {
            for(Object o : oo(jl.getSelectedValues())) {
                V3dListModel.list(V3dListModel.DELETED_OBJECTS, pv, true).remove(o);
                updateSurface(toStrg(o), pv, true, false);
            }
            enableDisable();
        }
    }
}
