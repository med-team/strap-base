package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**HELP

   This dialog  allows to perform string replacements in many residue annotations.

   <br><br>

   The dialog acts on all or on all selected residue annotations.
   First the annotation type like "Remark" or "Note" needs to be specified in the choice menu.
   All residue selections with annotations of the selected type appear
   in the preview table.

   <br><br>
   The user can enter a text pattern and optionally a replacement text. The effect is observe in the preview table.

   <br><br>
   The changes are committed by pressing one of three buttons.
   <i>BUTTON:"Replace"</i>
   <i>BUTTON:"Add"</i>
   <i>BUTTON:"Kill"</i>.

   <br><br>

   Advanced WIKI:Regular_expressions: Groups are captured with parenthesis (No backslash).
   The captured groups can be references by dollar-sign-number.

   <i>SEE_DIALOG:AddAnnotation,A</i>
   <i>SEE_DIALOG:ActivateDeactivate,A</i>

   @author Christoph Gille

*/
public class DialogResidueAnnotationChanges extends AbstractDialogJPanel implements StrapListener,ProcessEv,Runnable {
    private final static int MAX_NUM=5, COL_REPLACED=3;
    private final String[][] _tableData1=new String[2][1+MAX_NUM];
    {
        _tableData1[0][0]="Search pattern";
        _tableData1[1][0]="Replace by";
    }
    private final List _tableData2=new ArrayList();
    private final ChTableModel
        _tm1=new ChTableModel(0L).setData(_tableData1).editable(setTrue(1, MAX_NUM,null)),
        _tm2=new ChTableModel(0L).setData(_tableData2);
    private final ChJTable
        _jTable1=new ChJTable(_tm1,ChJTable.DEFAULT_RENDERER),
        _jTable2=new ChJTable(_tm2,ChJTable.DEFAULT_RENDERER);
    private final ChCombo
        _comboKeys=new ChCombo(ChJTable.CLASS_RENDERER, new ComboKeys(true)).s(ResidueAnnotation.NAME).li(evAdapt(this)),
        _comboNum;

    private final ChButton
        _cbRegex=toggl("Regular expression"),  _labInfo=labl().fg(0xFF0000),
        butKill=new ChButton("Kill").tt("Delete the selected key-value pairs from the residue selections"),
        butAdd=new ChButton("Add").tt("Add new key value pairs to the sets of residues");
    private final Object cached[]={null,null};
    private int _num;
    private void setNum(int num) {
        _num=num;
        final String[] h1=new String[num+1], h2=new String[num+COL_REPLACED];
        h2[0]="Residue Selection";
        h2[1]="Annotation type";
        h2[2]="Original String";
        for(int i=num; --i>=0;) {
            h1[i+1]="Replacement No "+(i+1);
            h2[i+COL_REPLACED]="After Replacement "+(i+1);
        }
        _tm1.setColumnNames(h1);
        _tm2.setColumnNames(h2);
        _jTable1.createDefaultColumnsFromModel();
        _jTable2.createDefaultColumnsFromModel();
        _jTable1.setColWidth(false,0, EM*15);
        _jTable2.setColWidth(true, 0, EM*15);
        _jTable2.setColWidth(true, 1, EM*15);

        revalAndRepaintC(_jTable1);
        revalAndRepaintC(_jTable2);
    }
    public DialogResidueAnnotationChanges() {
        setNum(1);
        _jTable1.setRowSelectionAllowed(false);
        final EvAdapter li=evAdapt(this);
        addActLi(li,_jTable1);
        final String num[]=new String[MAX_NUM];
        for(int i=MAX_NUM;--i>=0;) num[i]=plrl(i+1,"%N Replacement%S");
        _comboNum=new ChCombo(0L, num).li(li);
        final Object
            pKey=pnl(HB,"Enter the annotation type:  ",_comboKeys.li(li)),
            pNorth=pnl(VB,
                     dialogHead(this),pKey,
                      _labInfo,
                      " ",
                      pnl(HBL,"Enter the string pattern and replacement text: (For ", "WIKI:Word_completion hit the tab-key or alt-/)"),
                      _jTable1.getTableHeader(), _jTable1,
                      " ",
                      pnl(HBL,"Preview of replacement")
                      ),
            pAdvanced=pnl(VBHB,
                          pnl(HBL,"Several subsequent replacements: ",_comboNum),_cbRegex.li(li).cb(),
                          pnl(HBL,"Select rows in the preview table by string match ", ChButton.dialogStringMatch(DialogStringMatch.FROM_RENDERER_COMPONENT, new Object[]{wref(_jTable2)}, "resCh"))
                          ),
            pButtons=pnl(
                         "Act on all or on all selected rows of the preview table: ",
                         new ChButton("RPLC").t("Replace").li(li)
                         .tt("<b>Caution</b><br>replace the entries in the  residue selections<br> by the table entries in the right column"),
                         " ", butAdd.li(li),
                         " ", butKill.li(li)
                         ),
            pSouth=pnl(VB,
                      pButtons,
                      " ",
                      pnlTogglOpts(pAdvanced),
                      pAdvanced
                      ),
            pan=pnl(CNSEW,scrllpn(_jTable2),pNorth,pSouth);
        remainSpcS(this,pan);
        li.addTo("m",_jTable2);
        run();
        enableDisable();
    }

    private boolean _needsUpdate;
    public void run() {
        _needsUpdate=false;
        final String key=AddAnnotation.toKey(_comboKeys);
        final int num=_num;
        final HashMap mapSplit=fromSoftRef(1,cached, HashMap.class);
        if (sze(key)>0) {
            final List vWC=alSoftClr(0,cached);
            final HashSet setWC=new HashSet();
            ResidueAnnotation aa[]=(ResidueAnnotation[])StrapAlign.coSelected().residueSelections('A');
            if (aa.length==0) {
                final List v=new ArrayList(1000);
                for(Protein p: StrapAlign.proteins()) adAll(p.residueAnnotations(),v);
                aa=toArry(v,ResidueAnnotation.NONE);
            }
            _tableData2.clear();
            final boolean regex=_cbRegex.s();
            int countKey=0;
            for(ResidueAnnotation s:aa) {
                nextEntry:
                for(ResidueAnnotation.Entry e : s.entries()) {
                    if (!key.equals(e.key())) continue;
                    countKey++;
                    final String v=e.value();
                    if (sze(v)==0) continue;

                    String tt[]=(String[]) mapSplit.get(v);
                    if (tt==null) mapSplit.put(v, tt=splitTokns(v));
                    for(String t : tt) if (is(LETTR_DIGT,t,0)) setWC.add(t);

                    final Object[] row=new Object[COL_REPLACED+num];
                    row[0]=s;
                    row[1]=e;
                    row[2]=v;

                    String haystack=v;
                    boolean match=false, noNeedle=true;
                    for(int r=0; r<num; r++) {
                        final String needle=_tableData1[0][1+r];
                        final String replacement=_tableData1[1][1+r];
                        String result=null;
                        if (sze(needle)>0) {
                            noNeedle=false;
                            if (regex) {
                                try {
                                    if (ChRegex.pattern(needle).matcher(haystack).find()) {
                                        result=toStrg(ChRegex.replace(ChRegex.pattern(needle),replacement,haystack));
                                        match=true;
                                    }
                                } catch(Exception ex) {_labInfo.t("Error on processing regexpr. "+needle+"  "+ex);}
                            } else {
                                if (strstr(needle,haystack)>=0) {
                                    if (sze(replacement)>0) result=rplcToStrg(needle,replacement,haystack);
                                    match=true;
                                }
                            }
                        }
                        if (result==null) break;
                        row[r+COL_REPLACED]=haystack=result;
                    }
                    if (match || noNeedle) _tableData2.add(row);
                }
            }
            _labInfo.t(
                       aa.length==0 ? "Error: no residue annotations found!" :
                       countKey==0  ? "Error: none of the "+aa.length+" residue annotations has an entry  "+key+" " :
                       "");
            revalAndRepaintC(_jTable2);
            vWC.addAll(setWC);
            setWC.clear();
            _jTable1.getChRenderer().textField().tools().enableWordCompletion(vWC);
        }
    }

    private void enableDisable() {
        final boolean main=idxOfStrg(AddAnnotation.toKey(_comboKeys), ResidueAnnotation.MAIN_KEYS)>=0;
        setEnabld(!main,butKill);
        setEnabld(!main,butAdd);
    }
    public void processEv(java.awt.AWTEvent ev) {
        final Object q=ev.getSource();

        final String cmd=actionCommand(ev);
        if (q==_comboNum) {
            setNum(_comboNum.i()+1);
        }
        if (q==_cbRegex || q==_comboKeys || q==_jTable1 && cmd==ACTION_TABLE_CHANGED) {
            run();
            enableDisable();
        }
        if (cmd=="RPLC" || q==butAdd || q==butKill) {
            final int num=_num, ii[]=_jTable2.getSelectedRows();
            final Object data[][]=toArry(_tableData2,Object[].class);
            boolean changed=false;
            for(int row=0; row<data.length; row++) {
                if (sze(ii)>0 && idxOf(row,ii)<0) continue;
                final Object oo[]=data[row];
                final ResidueAnnotation a=(ResidueAnnotation)oo[0];
                final ResidueAnnotation.Entry e=(ResidueAnnotation.Entry)oo[1];
                if (q==butKill) {
                    a.removeEntry(e);
                    changed=true;
                } else {
                    String newValue=null;
                    for(int i=mini(num, oo.length-COL_REPLACED); sze(newValue)==0 && --i>=0;) newValue=(String)oo[i+COL_REPLACED];
                    if (newValue!=null && !newValue.equals(e.value())) {
                        if (q==butAdd) a.addE(ResidueAnnotation.E_EDITED,e.key(),newValue);
                        if (cmd=="RPLC") a.setValueE(ResidueAnnotation.E_EDITED, newValue, e);
                        changed=true;
                    }
                }
            }
            if (changed) StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_CHANGED);

        }
        if (isPopupTrggr(true,ev)) {
            final Object o=objectAt(null,ev);
            final ResidueAnnotation.Entry e=deref(o,ResidueAnnotation.Entry.class);
            final ResidueAnnotation a=deref(e!=null?e._a : o,ResidueAnnotation.class);
            ResidueSelectionPopup.showContextMenu(a);
        }
    }

    @Override public void paintComponent(java.awt.Graphics g) {
        if (_needsUpdate) run();
        super.paintComponent(g);
    }
    @Override public final void handleEvent(StrapEvent ev) {
        final int type=ev.getType();
        if (type==StrapEvent.OBJECTS_SELECTED || type==StrapEvent.RESIDUE_SELECTION_CHANGED) {
            _needsUpdate=true;
            ChDelay.repaint(this,444);
        }
    }

}
