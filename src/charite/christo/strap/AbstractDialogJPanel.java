package charite.christo.strap;
import charite.christo.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class AbstractDialogJPanel extends ChPanel implements Disposable {
    public AbstractDialogJPanel() {
        setOpaque(true);
        setBG(ChConstants.DEFAULT_BACKGROUND,this);
        setMinSze(0,0,this);
    }

    private boolean _disposed;
    public boolean isDisposed() { return _disposed;}
    public void dispose() {
        _disposed=true;
        StrapAlign.rmListener(this);
    }

}
