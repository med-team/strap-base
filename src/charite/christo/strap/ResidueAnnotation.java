package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.ProteinMC.*;

/**HELP

Annotated residue selections, allow the assignment of information to
specific amino acids or nucleotides of proteins.  A residue annotation
has a list of entries each having a type and a modifiable text.  The user
can change these entries and add new entries. See
<i>ITEM:ResidueAnnotationView</i> Annotated residue selections can be
created in different ways:

<ul>

<li>Selecting residues with the mouse. Right-click opens the context menu. Click the menu item
<i>BUTTON:ResidueSelectionPopup#ACTION_MK_RESANO</i>.</li>

<li>Dragging any residue selection to the same or another protein.</li>

</ul>

Residue selections can be selected in the alignment pane in two different ways: By Ctrl-click or by dragging a rectangular box.
Selected residue selections have WIKI:Marching_ants.

<h2>Background Image</h2>

Image files and images in the browser can be dragged onto an annotated residue selection. See Drop_Web_Link*

*/
public final class ResidueAnnotation implements HasName, ResidueSelection, SelectorOfNucleotides, VisibleIn123, Disposable, HasImage, HasMap, HasWeakRef, HasMC, HasRendererMC, ChRunnable  {
    public final static String
        NAME="Name",POS="Positions",GROUP="Group",ATOMS="Atoms", STYLE="Style", TEXSHADE=mapKey(CLASS_Texshade), HYPERREFS=mapKey(CLASS_Hyperrefs),
        COLOR="Color", BG_IMAGE="BG_IMAGE", VIEW3D="3D_view", SPECIFIC_VIEW3D="Specific_3D_view",
        DISABLED="Disabled",
        BALLOON="Balloon",
        MAIN_KEYS[]={NAME,GROUP,POS},
        OTHER_KEYS[]={BG_IMAGE, STYLE, TEXSHADE, VIEW3D, SPECIFIC_VIEW3D, ATOMS};
    public final static ResidueAnnotation NONE[]={};

    private static int _allEnabled;
    private static boolean _rendererInit;
    private static String _aaNames[];
    final static List vAll=new ArrayList(999);
    private final long _created=System.currentTimeMillis();
    private final Object _wr=newWeakRef(this);
    private int _mc, _ntOffset, _aaOffset, _endsAA, _canSimplify, _aaMC;
    private boolean _doSave=true, _parseIsAmino;
    private String  _imageId, _lastImage, _tt, _renTxt;

    public Object wRef() { return _wr;}
    public int mc() { return _mc;}
    private Map _cp;
    public Map map(boolean create) {return _cp==null&&create?_cp=new HashMap() : _cp;}

    public ResidueAnnotation(Protein p) {
        _p=wref(p);
        addE(0,NAME, "new_annotation");
        addE(0,GROUP,"new_group");
        vAll.add(_wr);
    }

    public long whenCreated() { return _created;}
    public void dispose() {
        final Protein protein=getProtein();
        if (protein!=null) protein.removeResidueSelection(this);
        _renTxt=" Discarded: "+getName();
        vAll.remove(_wr);
    }
    /**  ResidueAnnotation-Objects belong to a certain protein */
    @Override public Protein getProtein() { return (Protein)deref(_p); }
    private Object _p;
    public void setProtein(Protein p) { _p=wref(p);   }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Name and ID >>> */
    /**  A ResidueAnnotation-object has a name. */
    public String getName() {return value(NAME);}
    private final static Object _ID[]={null};
    public String getID() {
        final BA ba=baClr(_ID);
        synchronized(ba) {
        return ba.filter(FILTER_NO_MATCH_TO|'_', LETTR_DIGT, getProtein()).a('_').filter(FILTER_NO_MATCH_TO|'_', LETTR_DIGT, getName()).toString();
        }
    }
    /* <<< Name and ID  <<< */
    /* ---------------------------------------- */
    /* >>> Selected Nucleotides and Aminoacids >>> */
    private final static int IS_SEL_NT=0, IS_SEL=1,  REND_TEXT=2, REND_TEXT_JLIST=3;
    final static int IS_SEL_V=4;
    private final long cacheMC[]=new long[6];
    private transient Object _rCached;
    Object[] cached() {
        Object[] r=(Object[])deref(_rCached);
        if (r==null) _rCached=newSoftRef(r=new Object[6]);
        return r;
    }
    public void setSelectedAminoacids(boolean bb[],int offset) {
        final Protein p= getProtein();
        final boolean pdbNum=strchr(':', value(POS))>0;
        setValue(0, POS, Protein.selectedPositionsToText(pdbNum, bb,offset,p));
        setEnabledE(true, entryWithKey(ResidueAnnotation.POS));
    }
    /** e.g. s.setSelectedAminoacids("3-5"); */
    public void setSelectedAminoacids(String selection) { setValue(0,    POS,selection);}
    public void setSelectedNucleotides(String selection){ setValue(E_NT, POS,selection);}

    public int getSelectedAminoacidsOffset() {
        getSelectedAminoacids();
        return _aaOffset;
    }
    public boolean[] getSelectedAminoacids() {
        final Object[] cached=cached();
        mayParse(cached);
        final boolean bb[]= (boolean[])cached[IS_SEL];
        return bb!=null ? bb : NO_BOOLEAN;
    }

    public int getSelectedNucleotidesOffset() {
        final Object[] cached=cached();
        mayParse(cached);
        return _ntOffset;
    }
    public boolean[] getSelectedNucleotides() {
        final Object[] cached=cached();
        mayParse(cached);
        return (boolean[])cached[IS_SEL_NT];
    }

    /* <<< Selected Nucleotides and Aminoacids <<< */
    /* ---------------------------------------- */
    /* >>> Rendering >>> */
    private int _style=-1;
    public int getStyle() {
        int s=_style;
        if (s==-1) {
            final String v=value(STYLE);
            final int i=v==null ? STYLE_LOWER_HALF_BACKGROUND : idxOfStrg(v, ResSelUtils.styles());
            if (i>=0) s=_style=i;
        }
        return
            _img==null && (s<0 || s==STYLE_IMAGE || s==STYLE_IMAGE_LUCID) ? STYLE_LOWER_HALF_BACKGROUND :
            _img==null && s<0  ? STYLE_LOWER_HALF_BACKGROUND :
            _img!=null ? STYLE_IMAGE_LUCID :
            s;
    }
    public void setStyle(int i) {
        setValue(0, STYLE, (String)get(i,ResSelUtils.styles()));
        _style=i;
    }
    public void setColor(Color c) {
        if (c!=null && c!=_color) {
            _color=c;
            setChanged(COLOR);
        }
    }
    public Color getColor() { return _color; }
    void setFgForWhiteBG(Color c) { _paper=c; }
    Color fgForWhiteBG() { return _paper!=null ? _paper : _color; }
    private Color _color=Color.PINK, _paper;
    private int _where=0xffffffff;
    public int getVisibleWhere() { return _style!=VisibleIn123.STYLE_HIDDEN && isEnabled() ? _where : 0;}
    public void setVisibleWhere(int where) { _where=where;}
    /* <<< Rendering  <<< */
    /* ---------------------------------------- */
    /* >>> Image >>> */
    private Image _img;
    public Image getImage() { return _img; }

    private final static Object[] _provideS={null,null}, _provideWC={null,null};
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_IS_DISABLED) return isEnabled() ? null:"";
        final Object argv[]= arg instanceof Object[] ? (Object[])arg : null;
        if (id==ChRunnable.RUN_GET_TIP_TEXT) {
            final boolean shift=0!=(java.awt.event.MouseEvent.SHIFT_MASK&modifrs(arg));
            final long mc=mc()+(shift?1L<<63:0);
            if (_tt==null || _ttMC!=mc) {
                _ttMC=mc;
                final boolean[] sel=getSelectedAminoacids();
                final int offset=getSelectedAminoacidsOffset();
                final String f=featureName();
                final BA sb=new BA(222).colorBar(rgb(fgForWhiteBG())&0xFFffFF)
                    .a(f!=null?"Feature ":"Annotation ").or(f,getName()).a("<small>")
                    .a(' ',2).a(value(POS));
                for(Entry e : entries()) {
                    final String k=e.key();
                    if (k==BALLOON || shift && (k=="Remark" || k=="Note")) {
                        final String v=e.value();
                        if (v!=null) sb.a("<br>").filter(FILTER_HTML_ENCODE,v);
                    }
                }
                if (_simRegion!=null && shift) _simRegion.getLocalAlignment(offset+fstTrue(sel)-5, offset+lstTrue(sel)+5, sb.a("<pre>")).a("\n</pre>");
                if (cached()[IS_SEL_V]!=null) sb.a("<br>Positions ").a(value(POS));
                _tt=sb.a("</small>").toString();
            }
            return _tt;
        }
        if (id==ChRunnable.RUN_GET_ICON) return getIcon();
        if (id==ChRunnable.RUN_GET_TAB_TEXT) return new BA(99).a("<sub>").a(getProtein(), 0,10).a("<br>").a(getName(),0,10).a("</sub>");
        if (id==DialogStringMatch.PROVIDE_STRINGS) {
            _provideS[0]=getName();
            _provideS[1]=_vE;
            return _provideS;
        }
        if (id==ChRunnable.RUN_SET_ICON_IMAGE) {
            final Image im=(Image)arg;
            if (_img!=im) {
                _img=im;
                _icon=null;
                setChanged(BG_IMAGE);
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR,444);
            }
        }

        if (id==PROVIDE_WORD_COMPLETION_LIST){
            _provideWC[0]=getRenderer(HasRenderer.JLIST,null);
            _provideWC[1]=getName();
            return _provideWC;
        }
        return null;
    }
    String lastImage() { return _lastImage;}
    void findIconImage() {
        _lastImage=lastValue(BG_IMAGE);
        final String newImageId=_lastImage;
        if (cntainsOnly(LETTR_DIGT_US,newImageId)) {
            final javax.swing.ImageIcon ic=iicon(newImageId);
            _img=ic==null?null:ic.getImage();
            return;
        }
        if (_imageId!=newImageId) {
            _imageId=newImageId;
            _icon=null;
            _img=null;
            ChIcon.requestImage(_imageId,this);
        }
    }
    private javax.swing.ImageIcon _icon;
    public javax.swing.ImageIcon getIcon() {
        if (ResSelUtils.type(this)=='F' && isEdited()) return iicon(IC_EDIT);
        final Image im=_img;
        if (im==null) _icon=null;
        else if (_icon==null) {
            final Image imSmall=Toolkit.getDefaultToolkit().createImage(new java.awt.image.FilteredImageSource(im.getSource(),  new java.awt.image.ReplicateScaleFilter(ICON_HEIGHT, ICON_HEIGHT)));
            if (imSmall!=null) _icon=new javax.swing.ImageIcon(imSmall);
        }
        return _icon;
    }
    /* <<< Image <<< */
    /* ---------------------------------------- */
    /* >>> Tooltip  >>> */
    private long _ttMC;
    public void setRendererText(String s) { _renTxt=s; }
    public Object getRenderer(long options, long rendOptions[]) {
        final Object[] cached=cached();
        final boolean jList=(options&HasRenderer.JLIST)!=0;
        final int cacheID=jList ? REND_TEXT_JLIST : REND_TEXT;
        final Protein p=getProtein();
        long hc=hashCd(_renTxt)+mc();
        if (p!=null) hc+=p.mcSum(MC_PROTEIN_LABELS, MC_RESIDUE_SELECTIONS);
        String s=(String)cached[cacheID];
        if (s==null || cacheMC[cacheID]!=hc) {
            cacheMC[cacheID]=hc;
            cached[cacheID]=s=
                jList ? getProtein()+"/"+getName()+"/"+value(POS) :
                getName()+"/"+value(POS);

        }
        if (rendOptions!=null) {
            rendOptions[0]=p==null || !cntains(this, p.allResidueSelections()) ? HasRenderer.STRIKE_THROUGH : 0L;
        }
        return s;
    }
    {
        if (!_rendererInit) {
            _rendererInit=true;
            TabItemTipIcon.set("",VIEW3D,"Script lines, for protein viewers",IC_3D,VIEW3D);
            TabItemTipIcon.set("",SPECIFIC_VIEW3D,"Script lines for one particular protein viewers",IC_3D,SPECIFIC_VIEW3D);
            TabItemTipIcon.set(null,null,"Atom types like \".CA\" means C-alpha", IC_3D, ATOMS);
            TabItemTipIcon.setTiti(TabItemTipIcon.getTiti(CLASS_Texshade),"Texshade");
            TabItemTipIcon.set(null,null,"Balloon message", IC_TT, BALLOON);
            TabItemTipIcon.set(null,null,"Free text", IC_EDIT, "Remark");
            TabItemTipIcon.setTiti(TabItemTipIcon.getTiti("Remark"), "Note");

        }
    }
    private final long _hc32=((long)hashCode())<<32;
    public long getRendererMC() {
      final Protein p=getProtein();
      long mc=_hc32+_mc;
      if (p!=null) { p.residueAnnotations(); mc+=p.mc(MC_RESIDUE_ANNOTATIONS_V)<<16; }
      return mc;
    }

    /* <<< Tooltip <<< */
    /* ---------------------------------------- */
    /* >>>  Get Entry >>> */
    String lastValue(String key) {
        final String k=mapKey(key);
        final Entry ee[]=entries();
        for(int i=ee.length; --i>=0;) {
            if (ee[i].isEnabled() && k==ee[i].key()) {
                final String v=ee[i].value();
                if (v.length()>0) return v;
            }
        }
        return null;
    }
   /**  Return the 1st value with the key. */
    public String value(String key) {
        final Entry e=key!=null ? entryWithKey(key) : null;
        return e!=null ? e.value(): null;
    }

    public boolean isAmino() { return isEnabled(ResidueAnnotation.POS);}

     /** Returns the status of the i-th key-value pair or null. */
    boolean isEnabled(String key) {
        final String k=mapKey(key);
        for(Entry e:_vE.asArray()) {
            if (k==e.key() && e.isEnabled()) return true;
        }
        return false;
    }
    /* <<< Get Value <<< */
    /* ---------------------------------------- */
    /* >>> Set Value >>> */
    /** Adds a new key-value-pair */
    public void setValue(int opt, String key, CharSequence value) {
        if (key==null) return;
        final String k=mapKey(key);
        boolean found=false;
        for(Entry e:entries()){
            if (k==e.key()) {
                setValueE(opt, value,e);
                found=true;
            }
        }
        if (!found && value!=null) addE(opt,k,value);
    }

    public boolean setValueE(int opt, CharSequence value0, Entry e) {
        CharSequence value=value0;
        if (value==null || e==null) {
            removeEntry(e);
            return false;
        }
        final String k=e.key();
        if ((k==NAME || k==GROUP) && nxt(-FILENM,value)>=0) value=new BA(sze(value)).filter('_'|FILTER_NO_MATCH_TO|FILTER_TRIM, FILENM, value);
        final String v=toStrgTrim(value);
        if (v.equals(e.value())) return false;
        if (k==DISABLED) setEnabled(false);
        if (k==POS) _canSimplify=0;
        e._v=v;
        e._opt=opt;
        e._kv=null;
        clr(_vKV);
        final ChTextField tf=0!=(opt&E_NO_UPDATE_UI) ? null : deref(e.TF[0],ChTextField.class);
        if (tf!=null) tf.t(v);
        setChanged(k);
        if (BG_IMAGE==k) findIconImage();
        return true;
    }

    boolean isEdited() {
        for(Entry e : entries()) if (0!=(e._opt&E_EDITED)) return true;
        return false;
    }

    private void setChanged(String key) {
        final Protein p=getProtein();
        Protein.incrementMC(MC_RESIDUE_SELECTIONS,p);
        if (key==NAME || key==COLOR) Protein.incrementMC(MC_RESIDUE_SELECTION_LABELS, p);
        if (key==GROUP) Protein.incrementMC(MC_RESIDUE_ANNOTATION_GROUPS, p);
        if (key==BG_IMAGE) findIconImage();
        if (key==STYLE) _style=-1;
        _mc++;
        _doSave=true;
    }

    public void setEnabled(boolean b) { setEnabledE(b, entryWithKey(NAME)); }
    public void setEnabledE(boolean b, Entry e) {
        if (e!=null && e.isEnabled()!=b) {
            if (b) e._opt&=~E_DISABLED;
            else e._opt|=E_DISABLED;
            setChanged(null);
        }
    }
    /* <<< Set Value <<< */
    /* ---------------------------------------- */
    /* >>> Get Entry Objects >>> */
    public final static int E_EDITED=1<<12, E_NT=1<<13, E_DISABLED=E_NT, E_ROW=1<<14, E_NO_UPDATE_UI=1<<15, E_NO_SAVE=1<<16, E_NOT_UNIQUE=1<<17;
    private Entry _onlyE;
    void setOnlyEntry(Entry e) {_onlyE=e;}
    private final UniqueList<Entry> _vE=new UniqueList(Entry.class);
    public Entry[] entries() {
        final Entry oe=_onlyE;
        return oe!=null ? new Entry[]{oe}:_vE.asArray();
    }
    public void removeEntry(Entry e) { if (_vE.remove(e) && e!=null) setChanged(e.key()); }

    void addE(int opt, String key, CharSequence value0) {
        if (key==null || value0==null) return;
        final String k=mapKey(key), value=toStrgTrim(value0);
        final boolean main=cntains(k,MAIN_KEYS);
        for(Entry e : entries()) {
            if (k!=e.key()) continue;
            if (main || k==STYLE || k==BG_IMAGE) {
                setValueE(opt, value, e);
                return;
            }
            if (0==(opt&E_NOT_UNIQUE) && value.equals(e.value())) return;
        }
        final Entry e=new Entry(_wr);
        e._k=mapKey(k);
        setValueE(opt,value,e);
        final int L=sze(_vE), row=0!=(opt&E_ROW) ? (opt&0xFF) : MAX_INT;
        _vE.add(row<0? L : mini(L,row), e);
        if (0!=(opt&E_NO_SAVE) && key==NAME) _doSave=false;

    }
    Entry entryWithKey(String key) {
        final String k=mapKey(key);
        for(Entry e:entries()) if (k==e.key()) return e;
        return null;
    }
    /* <<< Entry List  <<< */
    /* ---------------------------------------- */
    /* >>> Parse >>> */
    /**
       Example
       texshade=  X\feature...
       ^
       i1234
       value is substring(i+4);
       X/x  is substring(i+3);
    */

    void parse(BA txt, int fromLine, int toLine) {
        if (txt==null) return;
        final byte T[]=txt.bytes();
        final int eol[]=txt.eol();
        for(int iL=fromLine; iL<toLine && iL<eol.length; iL++) {
            final int b=iL==0?0:eol[iL-1]+1, e=eol[iL], eq=strchr('=', T,b,e);
            if (eq<b+2 || e<eq+4) continue;
            final String k=mapKey(toStrg(T,b,eq)), v=toStrg(T, eq+4, e);
            if (k==COLOR) {
                final Color c=str2color(v);
                if (c!=null) _color=c;
            } else addE(E_NOT_UNIQUE| (T[eq+3]=='X'?0:E_DISABLED)|(T[eq+2]=='X'?E_EDITED:0), k,v);
        }
    }

    static void enableAllFeatures(int b) {
        if (_allEnabled!=b) {
            _allEnabled=b;
            Protein.incrementMC(MC_RESIDUE_SELECTIONS, null);
            Protein.incrementMC(MC_RESIDUE_ANNOTATION_GROUPS, null);
        }
    }
    public boolean isEnabled() {
        return featureName()==null || _allEnabled==0 ? isEnabled(NAME) :  _allEnabled==CTRUE;
    }

    private void parseResidueAnnotationNT(String sSet, Object[] cached) {
        final Protein protein=getProtein();
        if (protein==null) {
            cached[IS_SEL_NT]=NO_BOOLEAN;
            _ntOffset=0;
        } else {
            final int[] returnMinIdx={0};
            cached[IS_SEL_NT]=parseSet(sSet,protein.countNucleotides(),returnMinIdx);
            _ntOffset=returnMinIdx[0]-1;
        }
    }
    /* <<< Parse <<< */
    /* ---------------------------------------- */
    /* >>> Parse >>> */
    private void mayParse(Object[] cached) {
        final String txt=value(ResidueAnnotation.POS);
        final Protein p=getProtein();
        final int maxUL=ResSelUtils.maxUnderlining();
        if (txt!=null && p!=null) {
            final int mc=p.mcSum(MC_RESIDUE_TYPE,MC_RESIDUE_SELECTION_REFERENCE,MC_1ST_RES_IDX)+_mc+(maxUL<<10);
            final boolean isA=isAmino();
            if (cached[IS_SEL]==null || isA!=_parseIsAmino || _aaMC!=mc) {
                _parseIsAmino=isA;
                _aaMC=mc;
                if (isA) {
                    cached[IS_SEL]=parseResidueAnnotationAA(txt);
                    _ntOffset=0;
                } else {
                    parseResidueAnnotationNT(txt,cached);
                    final int returnMinIdx[]={0};
                    cached[IS_SEL]=p.ntPositions2aa( (boolean[])cached[IS_SEL_NT], _ntOffset, returnMinIdx);
                    _aaOffset=returnMinIdx[0];
                }
                Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS,p);
            }

        }
    }

    private boolean[] parseResidueAnnotationAA(String expr) {
        final Protein p=getProtein();
        final ReferenceSequence refS;
        final String rr;
        {
            final String REF="REFERENCE=";
            final int ref=strstr(STRSTR_w_L,REF,expr);
            if (ref>=0) {
                final String uniprot=wordAt(ref+sze(REF),expr);
                refS=p.getReferenceSequence(uniprot,(BA)null, true, (Runnable)null);
                rr=expr.substring(0,ref)+substrg(expr,  nxt(-SPC,expr,ref) ,MAX_INT);
            } else {
                refS=null;
                rr=expr;
            }
        }
        final int returnMinIdx[]={0};
        final Protein refP=refS==null ? null : refS.getReferenceProtein();
        final boolean bb[];
        if (refS==null) bb=p.residueSubsetAsBool(rr,returnMinIdx);
        else if (refP!=null) {
            final boolean bbRef[]=refP.residueSubsetAsBool(rr,returnMinIdx);
            final int offset=returnMinIdx[0];
            bb=refS.uniprotIdx2Idx(bbRef, offset, returnMinIdx);
        } else bb=NO_BOOLEAN;

        final int fstTrue=fstTrue(bb);
        if (fstTrue>0) assrt();
        _aaOffset=returnMinIdx[0]-1;
        final String fn=featureName();
        final Object[] cached=cached();
        if (fstTrue>=0 && fn!=null && bb.length>ResSelUtils.maxUnderlining() && countTrue(bb)==bb.length) {
            final boolean lastResIsSelected=bb.length+_aaOffset>=p.countResidues();
            final boolean sel[]=new boolean[lastResIsSelected ? 2 : bb.length];
            sel[0]=sel[1]=sel[0]=sel[sel.length-1]=sel[sel.length-2]=true;
            cached[IS_SEL_V]=sel;
        } else cached[IS_SEL_V]=null;
        return bb;
    }

    public static void loadResidueAnnotation(Protein p, File file) {
        if (sze(file)==0) return;
        final BA txt=readBytes(file);
        if (txt==null) return;
        final byte T[]=txt.bytes();
        final int eol[]=txt.eol();
        int start=-1;
        for(int iL=0;iL<=eol.length;iL++) {
            final int b=iL==0?0:eol[iL-1]+1;
            if (iL==eol.length || eol[iL]-b>6 && T[b]=='N' && T[b+1]=='a' && T[b+2]=='m' && T[b+3]=='e' && T[b+4]=='=') {
                if (start>=0) {
                    final ResidueAnnotation s=new ResidueAnnotation(p);
                    s.parse(txt, start, iL);
                    p.addResidueSelection(s);
                }
                start=iL;
            }
        }
    }

    /* <<< Parse <<< */
    /* ---------------------------------------- */
    /* >>> Save >>> */

    private static void saveE(Entry e, BA sb) { if (e!=null) saveE(e._opt, e.key(), e.value(), sb); }
    private static void saveE(int opt, String k, CharSequence v, BA sb) { sb.a(k).a('=').a(' ').a(0!=(opt&E_EDITED)?'X':' ').a(0==(opt&E_DISABLED)?'X':'x').aln(v); }
    public static File save(Protein p, File dir) {
        final ResidueAnnotation[] aa=p.residueAnnotations();
        final BA sb=new BA(aa.length*999);
        for(ResidueAnnotation a:aa) delFile(ResSelUtils.type(a)!='F'?null: a.featureFile(dir));
        for(ResidueAnnotation a:aa) {
            if (ResSelUtils.type(a)!='F' || !a._doSave) continue;
            sb.clr();
            for(Entry e: a.entries()) if (0!=(e._opt&E_EDITED)) saveE(e,sb);
            if (sze(sb)>0) wrte(a.featureFile(dir),sb);
        }
        sb.clr();
        for(ResidueSelection s : p.allResidueSelections()) {
            if (nam(s)==NAME_STANDARD || ResSelUtils.type(s)=='A') saveS(false, s, sb);
        }
        final File f=Protein.strapFile(Protein.mANNO, p, dir);
        if (sze(sb)==0)  { delFile(f); return null;}
        return wrte(f,sb) ? null : f;
    }

    public static void saveS(boolean dnd, ResidueSelection s, BA sb) {
        final ResidueAnnotation a=s instanceof ResidueAnnotation ? (ResidueAnnotation)s : null;
        if (s==null || a!=null && !a._doSave) return;
        if (a!=null) {
            saveE(a.entryWithKey(NAME),sb);
            for(Entry e: a.entries()) {
                final String k=e.key();
                if (k!=COLOR && k!=NAME) ResidueAnnotation.saveE(e,sb);
            }
        } else {
            ResidueAnnotation.saveE(0, NAME, nam(s), sb);
            ResidueAnnotation.saveE(0, GROUP, dnd?"Drag-Drop":"Selection", sb);
            ResidueAnnotation.saveE(0, POS,  new BA(33).boolToText(s.getSelectedAminoacids(), 1+s.getSelectedAminoacidsOffset(), " ","-"), sb);
        }
        final Color c=colr(s);
        if (c!=null && (dnd || ResSelUtils.type(a)!='F')) ResidueAnnotation.saveE(0,COLOR,  "#"+rgbHex(c,true), sb);
        if (dnd) saveE(0,ResSelUtils.DND_SELECTED_UC, ResSelUtils.selectedToUpperCase(s,null,null), sb);

    }

    /* <<< Save <<< */
    /* ---------------------------------------- */
    /* >>> Entry Instance >>> */
    public final static class Entry implements ChRunnable, HasRenderer, Comparable {
        private int _opt;
        private String _k, _v, _kv;
        final Object _a, TF[]={null};
        private Entry(Object a){_a=a;}
        @Override public Object getRenderer(long opt, long rendOptions[]) { return _k; }
        public String value() { return toStrgN(_v);}
        public String key() { return _k;}
        public boolean isEnabled() { return 0==(_opt&E_DISABLED);}

        public Object run(String id, Object arg) {
            if (id==DialogStringMatch.PROVIDE_STRINGS || id==PROVIDE_WORD_COMPLETION_LIST) return value();
            return null;
        }
        public int compareTo(Object o) {
            final Entry e= o instanceof Entry ? (Entry)o : null;
            if (e==null||e._k==null||e._v==null) return -1;
            if (_k==null || _v==null) return 1;
            int c=_k.compareTo(e._k);
            if (c==0) c=_v.compareTo(e._v);
            return c;
        }
    }
    /* <<<  Entry Class <<< */
    /* ---------------------------------------- */
    /* >>> Sequence Features >>> */

    private String _featureName, _featureSrcDb="";
    private int _featureSrc;
    File featureFile(File dir) {
        final Protein p=getProtein();
        if (p==null) return null;
        final String acc=p.getAccessionID();
        final BA sb=new BA(99).a(dir!=null?dir : dirStrapAnno()).a("/"+DIR_ANNOTATIONS+"/");
        if (acc!=null) sb.filter(FILTER_NO_MATCH_TO|'_', LETTR_DIGT_US,acc).and("_",p.getChainsAsString());
        else sb.a(p);
        final boolean bb[]=getSelectedAminoacids();
        final int offset=getSelectedAminoacidsOffset(), i0=Protein.firstResIdx(p);
        final int hc=hashCd(p.getResidueType(),  maxi(0,offset+fstTrue(bb)-i0), mini(offset+lstTrue(bb)+1-i0, p.countResidues()),true);
        sb.a('_',2).a(_featureName).a('_',2).boolToText(bb,offset+1,"_","-").a('_',2).a(hc);
        return file(sb.a(".fAnno"));
    }

    public void setFeatureName(String name, int featureSrc) {
        _featureName=mapFeatName(name);
        _featureSrc=featureSrc;
    }
    public String featureName() { return _featureName;}
    public int whereFeatureLoadedFrom() { return _featureSrc;}

    void setFeatureSrc(String db, String seqName) { _featureSrcDb=db; }
    public String featureSrcDb() { return _featureSrcDb;}

    private ReferenceSequence _simRegion;
    private float _simRegionScore=Float.NaN;
    float featureAligScore() { return _simRegionScore; }
    void setReferenceSequence(ReferenceSequence s, int start, int end) {
        if (s!=null) _simRegionScore=s.getAlignmentScore();
        _simRegion=s;
    }
    private CharSequence _srcText;
    public CharSequence srcText() { return _srcText;}
    public void setSrcText(CharSequence t) {  _srcText=t;}

    public boolean featureEndsWithAminoName() {
        if (_endsAA==0) _endsAA=ProteinUtils.textHasAminoAcid(STRSTR_EOL|STRSTR_IC, featureName())>0?CTRUE:CFALSE;
        return _endsAA>0;
    }
 /* <<< Sequence Features <<< */
    /* ---------------------------------------- */
    /* >>> Included >>> */

    private Collection _vIncl;
    void includeFeature(ResidueAnnotation a) {
        if (a==null) return;
        Collections.sort((List)a._vE);
        includedFeatures().add(a.report(true));
        includedFeatures().addAll(a.includedFeatures());
    }
    Collection<String> includedFeatures() {
        if (_vIncl==null) _vIncl=new HashSet();
        return _vIncl;
    }

    String report(boolean skipMain) {
        final int indent=3;
        final BA sb=new BA(2222);
        sb.a(' ',indent).a(ANSI_UL).a(getRenderer(HasRenderer.JLIST,null));
        if (!isEnabled()) sb.a(" (disabled) ");
        sb.aln(ANSI_RESET);
        int keyLen=0;
        for(ResidueAnnotation.Entry e : entries()) {
            final String k=e.key();
            keyLen=maxi(keyLen, sze(k)-k.lastIndexOf('.')+1);
        }
        for(ResidueAnnotation.Entry e : entries()) {
            final String k=e.key();
            if (skipMain && (k==ResidueAnnotation.NAME || k==ResidueAnnotation.COLOR || k==ResidueAnnotation.POS || k==ResidueAnnotation.GROUP)) continue;
            final int dot=k.lastIndexOf('.');
            sb.a(' ',2*indent+keyLen-sze(k)+dot).a(ANSI_FG_BLUE).a(k,dot+1, MAX_INT).a(ANSI_RESET).a(' ',3).aln(e.value());
        }
        return sb.toString();
    }

   /* <<< Included <<< */
    /* ---------------------------------------- */
    /* >>> Compare entries >>> */

    private static String kTabV(Entry e) {
        String kv=e._kv;
        if (kv==null) kv=e._kv=e._k+"\t"+e._v;
        return kv;
    }
    private Collection _vKV;
    private Collection vKV() {
        Collection v=_vKV;
        if (v==null) v=_vKV=new HashSet();
        if (v.size()==0) for(Entry e : entries()) v.add(kTabV(e));
        return v;
    }

    public boolean containsAllEntriesOf(ResidueAnnotation a) {
        final Collection v=vKV();
        for(Entry e : a.entries()) if (!v.contains(kTabV(e))) return false;
        return true;
    }

    /* <<< Compare entries <<< */
    /* ---------------------------------------- */
    /* >>> DnD >>> */
    void dropRows(BA data, final int row) {
        boolean success=false;
        final Entry ee[]=entries();
        for(String line : splitLnes(data)) {
            final String fields[]=splitStrg(line,'\t');
            if (fields.length<3+2) continue;
            final String k=fields[1], v=fields[2], bool=fields[3];
            final int hc=atoi(fields,4), srcRow=atoi(fields,5);
            success=true;
            int mvE=-1;
            if (hc==hashCode()) {
                for(int iE=ee.length; --iE>=0;) {
                    if (k.equals(ee[iE].key()) && v.equals(ee[iE].value())) {
                        if ((mvE=iE)==srcRow) break;
                    }
                }
            }
            if (mvE>=0)  adUniqR(_vE.get(mvE), row,_vE);
            else addE((isTrue(bool)?0:E_DISABLED) | (E_ROW|(row&0xFF)), k, v);
            _mc++;
        }
        if (success) {
            revalAndRepaintCs(getPnl(gcp(KEY_GET_VIEW, this)));
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED, 111);
        }
    }
   /* <<< DnD  <<< */
    /* ---------------------------------------- */
    /* >>> Edit >>> */

    boolean canSimplify() {
        if (_canSimplify==0) {
            final Protein p=getProtein();
            _canSimplify=
                p!=null && p.getResidueNumber()!=null ||
                !strEqulsWithFilter(0, chrClas(DIGT), value(POS), MAX_INT, ResSelUtils.optimizedPositions(this,false) , MAX_INT) ? CTRUE:CFALSE;
        }
        return _canSimplify==CTRUE;
    }
    /* <<< Edit <<< */
    /* ---------------------------------------- */
    /* >>> static utils >>> */
    private static Map<String,String> _mapKeys, _mapFeatName;
    public static String mapKey(String k) {
        if (k==null) return null;
        if (_mapKeys==null) _mapKeys=new HashMap();
        String s=_mapKeys.get(k);
        if (s==null) _mapKeys.put(k,s=toStrgIntrn(k.substring(k.lastIndexOf('.')+1)));
        return s;
    }
    public static String mapFeatName(String n) {
        if (n==null) return null;
        if (_mapFeatName==null) _mapFeatName=new HashMap();
        String s=_mapKeys.get(n);
        if (s==null) _mapKeys.put(n,s=toStrgIntrn(capitalize(n.replace(' ','_'))));
        return s;
    }
}

