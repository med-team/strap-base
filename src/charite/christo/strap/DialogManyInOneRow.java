package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.Protein;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

For manipulating large numbers of homologous sequences
simultaneously, more than one protein may be stacked in one single
line by pressing either button:
<ul>
<li> <i>JCOMPONENT:DialogManyInOneRow#getButton("A")!</i> or<br> </li>
<li> <i>JCOMPONENT:DialogManyInOneRow#getButton("X")!</i> </li>
</ul>

Only one representative is shown and the others are hidden.
But when gaps are inserted or deleted manually or by alignment computations, all
proteins in that row are changed.

<br><br>

<b>Separating the stacked proteins:</b>
Select the row and subsequently press one of the two buttons.

 @author Christoph Gille
*/
public class DialogManyInOneRow  implements ActionListener {
    private static DialogManyInOneRow li;
    public static ChButton getButton(String id) {
        final boolean cAlpha="A".equals(id);
        if (li==null) li=new DialogManyInOneRow();
        final String T="Stack selected proteins";
        return new ChButton(cAlpha?"A":"X").t(cAlpha?T+". Take one with 3D-coordinates as representative.":T).li(li);
    }

    public void actionPerformed(ActionEvent ev) {
        StrapAlign.errorMsg("");
        final boolean takeOneWithCalpha= ev.getActionCommand()=="A";
        final StrapView ap=StrapAlign.alignmentPanel();
        final Protein pp[]=StrapAlign.selectedProteins();

        Protein pVisible=null;
        if (ChMsg.noProteinsSelected(pp)) return;
        if (!takeOneWithCalpha) pVisible=pp[0];
        else if (pp!=null) {
            for(Protein p: pp) if (p!=null && p.getResidueCalpha()!=null) pVisible=p;
            if (pVisible==null) {
                StrapAlign.errorMsg("No protein with C-alpha coordinate among the selected proteins");
                return;
            }
        }
        if (pVisible!=null && ap!=null) ap.oneLineManyProteins(pVisible,pp);
        StrapAlign.tree().clearSelection();
    }
}
