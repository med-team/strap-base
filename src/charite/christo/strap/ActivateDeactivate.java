package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   Annotation entries in residue selections can be activated or deactivated or deleted.
   @author Christoph Gille
*/
public class ActivateDeactivate extends ChPanel implements ActionListener {
    private final ChCombo
        comboKeys=new ChCombo(ChJTable.CLASS_RENDERER, new ComboKeys(false)).li(this),
        choice=new ChCombo("Activate","Deactivate","Delete");
    private final ChButton togFilter=toggl("Process only entries that contain a string ");
    private final ChTextField tfFilter=new ChTextField();
    private final ContextObjects hasResidueAnnotations;

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="GO") {
            for(ResidueSelection sel: hasResidueAnnotations.residueSelections('s')) {
                final ResidueAnnotation a=deref(sel,ResidueAnnotation.class);
                if (a==null) continue;
                final String sKey;
                {
                    final Object k=comboKeys.getSelectedItem();
                    sKey=k instanceof Class ? nam(k) : toStrg(k);
                    if (sze(k)==0) continue;
                }
                final String filter=togFilter.s() ? toStrg(tfFilter) : null;
                for(ResidueAnnotation.Entry e : a.entries()) {
                    if (!sKey.equals(e.key())) continue;
                    if (sze(filter)>0 && e.value().indexOf(filter)<0) continue;
                    final int i=choice.i();
                    if (i<2) a.setEnabledE(i==0,e);
                    else a.removeEntry(e);
                }
            }
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        } else ChFrame.frame("",this,ChFrame.CENTER).shw(ChFrame.PACK);
    }
    public ActivateDeactivate(ContextObjects annotations) {
        hasResidueAnnotations=annotations;
        tfFilter.tools().highlightOccurrence(" ",null,null,HIGHLIGHT_UPDATE_IF_TEXT_CHANGES,C(DEFAULT_BACKGROUND));
        pcp(KOPT_HIDE_IF_DISABLED, "" ,tfFilter);
        final java.awt.Component
            panTF=pnl(HB,tfFilter),
            vB=pnl(VBPNL,
                     pnl(HB,smallHelpBut(this),"#"," Type of Entries: ",comboKeys),
                     " ",
                     pnl(HB,choice,pnl(new ChButton("GO").li(this).tt("Activate/deacivate the key -value pairs in the residue annotations").t(ChButton.GO))),
                     " ",
                     togFilter.doCollapse(panTF).cb(),
                     panTF,
                     " "
                     );
        add(vB);
    }
}
