package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class TexshadeGraph extends Box implements java.awt.event.ActionListener,IsEnabled {
    private final Object _vor[]={null},/* _needsReval[],  */_tfMin, _tfMax;
    private final String _where;
    private final ProteinCombo _choiceP;
    private final ChCombo _choiceClass, _choiceColor;
    private final ChButton _toggle, _butP;
    private final Class _interf;
    private ProteinList _ppl;
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
   TexshadeGraph(String where,Class interf) {
       super(BoxLayout.Y_AXIS);
       _where=where;
        _interf=interf;
        final boolean isBottom="bottom"==where && interf!=ValueOfResidue.class;
        final String save=Texshade.SAVE+where+" ";
        final String ttMiMa="By giving a max and min value you can scale  the plot<br>Keep field blank to take entire range of values";
        _tfMin=new ChTextField("auto").tt(ttMiMa).saveInFile(save+"_tfMin");
        _tfMax=new ChTextField("auto").tt(ttMiMa).saveInFile(save+"_tfMax");
        _choiceColor=new ChCombo("ColdHot","BlueRed","GreenRed", "Blue","Red","Green","Brown","Black","Yellow","Orange","Pink").save(TexshadeGraph.class,"solor_"+where);
        _toggle=toggl().s(isBottom).save(TexshadeGraph.class,"enabled_"+where);
        _choiceClass=SPUtils.classChoice(interf).li(this).classRenderer(interf).save(TexshadeGraph.class,"class_"+where);
        if (interf==ValueOfResidue.class) {
            _choiceP=new ProteinCombo(0L);
            pcp(KEY_ENABLED, this, _choiceP.li(this));
            _butP=null;
        } else {
            _butP=new ChButton("Choose other proteins").li(this);
            _choiceP=null;
        }
        final JComponent
            //            bCtrlS=ChButton.doSharedCtrl(_vor),
            //            bCtrl=ChButton.doCtrl(_vor),
            pan1=pnl(_choiceColor," min=",_tfMin," max=",_tfMax),
            pan2=pnl(HBL,_choiceP,_butP, _choiceClass.panel()/*, bCtrlS, bCtrl,panHS(_choiceClass)*/);

        add(pnl(HBL,_toggle,"Location: ",where, pan1));
        add(pan2);
        _toggle.doCollapse(new Object[]{pan1,pan2});
        replaceInstance();
        replaceProteins();
        //_needsReval=new Object[]{bCtrl, bCtrlS};
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Replace >>> */
    private void replaceInstance() {
        dispos(_vor[0]);
        _vor[0]=mkInstance(_choiceClass, _interf, true);
        replaceProteins();
    }
    private void replaceProteins() {
        final ValueOfResidue vor=deref(_vor[0], ValueOfResidue.class);
        final ValueOfAlignPosition vap=deref(_vor[0], ValueOfAlignPosition.class);
        if (vor!=null) vor.setProtein(_choiceP.getProtein());
        if (vap!=null && _ppl!=null) vap.setProteins(_ppl.selectedProteins());
    }
   public final boolean isEnabled(Object p) {
       final Object ie=_vor[0];
       return p==null ? false : ie instanceof IsEnabled ? ((IsEnabled)ie).isEnabled(p) : true;
    }
    BA makeTex(BA sb, int colFrom, int colTo) {
        final String errorPrefix="% TexshadeGraph "+_where+" "+shrtClasNam(_interf);
        final Protein pp[]=Texshade.proteins();
        if (!isSelctd(_toggle) || !_choiceClass.isEnabled()) return sb;
        if (pp==null) return sb.a(errorPrefix).aln(" pp=null");
        final String color=toStrg(_choiceColor), barOrColor=nxt(UPPR, color, 1,MAX_INT)>0 ? "color":"bar";
        double min=atof(_tfMin), max=atof(_tfMax);
        final ValueOfResidue vor=_interf==ValueOfResidue.class ? deref(_vor[0], ValueOfResidue.class) : null;
        final ValueOfAlignPosition vap=_interf==ValueOfAlignPosition.class ? deref(_vor[0], ValueOfAlignPosition.class) : null;
        final double[] values;
        int valuesFrom=0, valuesTo=0;
        String sRange=null,protNum=null;
        boolean[] takeValue=null;
        if (vor!=null) {
            final Protein pVoR=_choiceP.getProtein();
            if ( pVoR==null) return sb.a(errorPrefix).aln(" p=null");
            final int idx0=Protein.firstResIdx(pVoR), iP=idxOf(pVoR,pp);
            if (iP<0) return sb;
            protNum=toStrg(iP+1);
            vor.setProtein(pVoR);
            values=vor.getValues();
            valuesFrom=maxi(0,pVoR.column2nextIndexZ(colFrom));
            valuesTo=mini(sze(values) , pVoR.column2thisOrPreviousIndex(colTo-1)+1);
            sRange=(idx0+valuesFrom+1)+".."+(idx0+valuesTo);
            for(int i=0; i<sze(values) && i<10;i++) puts(values[i]+" ");
        } else if (vap!=null) {
            final Protein[] ppVoAP=_ppl!=null ? _ppl.selectedProteins(): Texshade.proteins();
            if (sze(ppVoAP)==0) return sb.a(errorPrefix).aln(" ppVoAP=null");
            else {
                final byte gappedSeq[][]=new byte[ppVoAP.length][];
                for(int iP=ppVoAP.length; --iP>=0;) gappedSeq[iP]=ppVoAP[iP].getGappedSequence();
                vap.setProteins(ppVoAP);
                values=vap.getValues();
                if (values!=null) {
                    valuesFrom=0;
                    valuesTo=values.length;
                    takeValue=new boolean[valuesTo];
                    int count=0;
                    for(int iC=colFrom; iC<=colTo && iC<values.length; iC++) {
                        boolean isGap=true;
                        for(int iP=ppVoAP.length; --iP>=0;) {
                            if (is(LETTR, gappedSeq[iP], iC)) { isGap=false; break; }
                        }
                        if (!isGap) { count++; takeValue[iC]=true;}
                    }
                    sRange="1.."+count;
                    protNum="consensus";
                }
            }
        } else return sb.a(errorPrefix).a("Could not initialize ").aln(_choiceClass);
        if (values==null) return sb.a(errorPrefix).aln(" noValues ");
        final BA sbData=new BA(999);
        final boolean minAuto=Double.isNaN(min), maxAuto=Double.isNaN(max);
        if (minAuto) min=Double.MAX_VALUE;
        if (maxAuto) max=Double.MIN_VALUE;
        for(int write=0; write<2; write++) {
            for(int i=valuesFrom; i<=valuesTo && i<values.length; i++) {
                if (takeValue!=null && !get(i,takeValue)) continue;
                if (write!=0) {
                    final double v=(1000*(values[i]-min)/(max-min));
                    if (Double.isNaN(v)) sbData.aln("NaN");
                    else sbData.a((int)v).a('\n');
                } else {
                    if (minAuto && min>values[i]) min=values[i];
                    if (maxAuto && max<values[i]) max=values[i];
                }
            }
            if (min==max || Double.isNaN(min) || Double.isNaN(min)) return sb.a(errorPrefix).a(" min=").a(min).a(" max=").a(max).a('\n');
        }
        final String fileName=shrtClasNam(_interf)+"_"+_where+".dat";
        wrte(file(Texshade.DIR+fileName),sbData);
       return sb.a("\\feature{").a(_where).a("}{").a(protNum).a("}{").a(sRange).a("}{").a(barOrColor).a("[0,1000]:").a(fileName).a('[').a(color).aln("]}{}");
    }
    /* <<< make Tex <<< */
    /* ---------------------------------------- */
    /* >>> ActionListener  >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        if (q==_choiceP || q==_ppl) replaceProteins();
        if (q==_choiceClass) replaceInstance();
        if (q==_butP) {
            if (_ppl==null) (_ppl=new ProteinList(0L)).li(this);
            _ppl.showInFrame(ChFrame.AT_CLICK, "Proteins");
        }

        //revalidateC(_needsReval);
    }
}
