package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.ArrayList;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP
Multiple sequence files (<i>WIKI:FASTA_format</i>, MSF, BIOWIKI:NexusFormat, WIKI:Stockholm_format ) can be directly dragged into the
alignment panel.

But the import dialog provides more control for the import of these files.

The alignment text is pasted into the text-area
and <i>LABEL:ChButton#BUTTON_GO</i> is pressed.

The result is a table of proteins.

The  column <i>Protein name</i> must contain the names for the new
protein files.

<br><br><b>Setting the protein names</b>:

Often the header consists of several fields separated by a vertical bar and the first or
secondary field can be used as protein name.
More general but also more complicated are regular expressions to extract parts of the header text (WIKI:Regular_expression).

@author Christoph Gille
*/
public class DialogImportMFA  extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener {

    private final static int HEADER=0, NAME=1, SEQUENCE=2, OVERWRITE=3;

    private final ChTextArea _ta=new ChTextArea(3,3);
    private final Object[] _togOverwrite=radioGrp(new String[]{"Skip","Overwrite protein"},0,null);
    private ChFileChooser _fs;
    private ChFrame _frame;
    private final ChTextField _tfSeparator=new ChTextField("|").cols(3,true,true).li(this);
    //========================================

    public DialogImportMFA() {
        final Object pSouth=pnl(
                          new ChButton("FF").t("Load from file").li(this),
                          new ChButton("EX").t("Show example").li(this), "  ",
                          new ChButton("GO").t(ChButton.GO).li(this)
                          );
        _ta.tools().enableUndo(true).enableWordCompletion(dirWorking()).cp(KEY_IF_EMPTY,"Enter an alignment in multiple fasta format");
        adMainTab( remainSpcS(pnl(CNSEW,scrllpn(_ta),dialogHead(this),pSouth)), this,null);
    }
    public void setText(CharSequence s) { _ta.setText(toStrg(s));}
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Event  >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        StrapAlign.errorMsg("");
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (cmd=="EX") _ta.t("\n>homo_sapiens \nMY-MISS--ISAH-IPPIE\n>mus_musculus \nI--MISS-MISSISSIPPI\n");
        if (cmd=="FF")  {
            if (_fs==null) {
                _fs=new ChFileChooser(ChFileChooser.CLOSE, "DialogImportMFA").addFilter("-iz .fasta .fa .mfa # Fasta format");
                addActLi(this, _fs);

                _frame=new ChFrame("DialogImportMFA").size(400,400).ad(_fs);
            }
            _frame.shw(ChFrame.AT_CLICK);
        }
        if (cmd==JFileChooser.APPROVE_SELECTION) _ta.t(readBytes(_fs.getSelectedFile()));
        if (cmd=="GO") {
            final Parse c=new Parse(new MultipleSequenceParser());
            if (c.isSuccess()) adTab(DISPOSE_CtrlW, "Parsed",c, this);
        }
        if (q==_tfSeparator && (cmd==ACTION_ENTER || cmd==ACTION_FOCUS_LOST)) _tfSeparator.t((_tfSeparator+"|").substring(0,1));
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Class >>> */
    private final class Parse extends JPanel implements java.awt.event.ActionListener, javax.swing.table.TableCellRenderer {
        /* --- Table Renderer --- */
        private final ChButton LABEL=labl();
        private final ChJTable _table;
        private final ChTextField
            tfExpr=new ChTextField("([^|]*)\\|([^|]*)\\|([^|]*)\\|([^|]*).*").saveInFile("DialogImportMFA_expression"),
            tfRepl=new ChTextField("HELLO $1 WORLD $2 ").tt("the replacement expression").saveInFile("DialogImportMFA tfRepl");
        private final Object[][] _data;
        private boolean _success=true, _veryLongName;
        private int _countNoName;
        private ArrayList<Protein> _vProt, _vExists;

        public Component getTableCellRendererComponent(JTable t,Object v,boolean isSel,boolean hasFocus,int row,int col) {
            if (v==null) v="Error";
            LABEL.cp(KOPT_IS_SELECTED,isSel?"":null).t(toStrg(v)).fg(0).setEnabled(true);
            monospc(LABEL);
            switch(col) {
            case NAME:
                // if (v!=null && v.equals(ChRegex.NO_MATCH)) {
                //     LABEL.setEnabled(false);
                //     return LABEL.t(toStrg(v));
                // }
                if (nxt(-SPC,v)<0) {
                    LABEL.setEnabled(false);
                    return LABEL.t("Enter a name for the protein");
                }
                break;
            case OVERWRITE:
                final String name=sze(_data)>row ? toStrg(_data[row][NAME]) : null;
                final boolean exists=sze(name)>0 && null!=SPUtils.proteinWithName(name, StrapAlign.proteins());
                return LABEL
                    .t(exists ? "exists" : "").fg(0xFF0000)
                    .tt(exists ? "A protein with this name already exists." : "A protein with this name does not yet exist");
            }
            return LABEL;
        }
        /* --- EndTable Renderer --- */
        public void actionPerformed(java.awt.event.ActionEvent ev) {
            StrapAlign.errorMsg("");
            final Object q=ev.getSource();
            final String cmd=ev.getActionCommand();
            if (cmd=="RE") {
                final String replacement=toStrg(tfRepl);
                for(Object[] d: _data) {
                    final String header=toStrgTrim(d[HEADER]);
                    String s=toStrg(ChRegex.replace(ChRegex.pattern(toStrgTrim(tfExpr)), replacement, header));
                    if (s==header) s=null;
                    if (s!=null) s=s.replaceAll("\\W","_");
                    d[NAME]=s;
                }
                _table.repaint();
            }
            if (cmd=="FIELD") {
                final char sep=chrAt(0,toStrgTrim(_tfSeparator));
                final int col=atoi(getTxt(q));
                for(Object[] dd: _data) dd[NAME]=get(col-1, splitStrg(dd[HEADER],sep!=0?sep:'|'));
                _table.repaint();
            }
            if (cmd=="CLR") {
                for(Object[] dd: _data) dd[NAME]="";
                _table.repaint();
            }
            if (cmd=="GO") {
                final int nR=_table.getRowCount();
                _vProt=new ArrayList();
                _vExists=new ArrayList();
                if (nR==0) StrapAlign.errorMsg("Cannot create protein objects: no table rows");
                _veryLongName=false;
                _countNoName=0;
                final int selected[]=ChJTable.selectedRowsConverted(_table);
                if (sze(selected)==0) {
                    for(int i=0;i<nR;i++) processRow(i);
                } else {
                    for(int s:selected) processRow(s);
                }
                final String msgLong="At least one of the protein names is unusal long.\nDo you want to review the protein names before creating the protein objects ?";
                if (_veryLongName && ChMsg.yesNo(msgLong)) return;
                final BA sb=new BA(333);
                if (sze(_vProt)>0) sb.a("The following proteins were created:\n").join(_vProt);
                if (_countNoName>0) sb.a('\n',2).a(_countNoName).a(" table rows were skipped because the field in the column 'Protein name' was empty");
                if (sze(_vExists)>0) {
                    sb.a("\n\nThe following proteins were not created because proteins with the given name already exist: \n")
                        .join(_vExists);
                }
                shwTxtInW("Created",sb);
                StrapAlign.addProteins(-1, toArry(_vProt,Protein.class));
                _table.repaint();
            }
        }
         private void processRow(int i) {
            final String name=getNameAt(i);
            if (sze(name)==0) { _countNoName++; return;}
            final String seq=toStrgTrim(_table.getValueAt(i,SEQUENCE));
            final String header=toStrgTrim(_table.getValueAt(i,HEADER));
            // already known ?
            Protein p=SPUtils.proteinWithName(name,null);
            if (p!=null) {
                if (radioGrpIdx(_togOverwrite)==1) StrapAlign.rmProteins(false,p);
                else {
                    _vExists.add(p);
                    return;
                }
            }
            (p=new Protein(StrapAlign.getInstance())).setName(name);
            p.setProteinParserClass(MultipleSequenceParser.class);
            if (sze(name)>30) _veryLongName=true;
            final byte bb[]=seq.getBytes();
            p.setResidueType(filtrBB(0L,LETTR,bb));
            p.inferGapsFromGappedSequence(bb);
            _vProt.add(p);
            final java.io.File f=file(name);
            p.setFile(f);
            wrte(f,">"+header+"\n"+seq);
            StrapAlign.newAlignmentPanel(false);
        }

        boolean isSuccess() { return _success;}
        private String getNameAt(int i) { return rplcToStrg(" ","_",toStrgTrim(_table.getValueAt(i,NAME))); }

        public Parse(MultipleSequenceParser parser) {
            final BA ba=toBA(_ta).delBlanksL().delBlanksR().a('\n');
            if (sze(ba)==0) {StrapAlign.errorMsg("Enter multiple sequence text"); _success=false; _data=null; _table=null; return;}
            parser.setText(ba);
            final byte seq[][]=parser.gappedSequences();
            String headers[]=toStrgArray(parser.getFastaHeader());
            if (headers==null) headers=parser.getSequenceNames();
            if (sze(headers)==0) {StrapAlign.errorMsg("Error parsing the text: No protein names found"); _success=false; _data=null; _table=null; return;}
            _data=new Object[headers.length][];
            for(int i=0;i<headers.length;i++) {
                _data[i]=new Object[] {
                    headers[i],
                    nxt(-FILENM,headers[i])<0 ? headers[i] : "",
                    seq[i]!=null ? bytes2strg(seq[i]) : "",
                    ""
                };
            }

            _table=new ChJTable(new ChTableModel(0L, new String[]{"Header","Protein name","Sequence",""}).setData(_data), ChJTable.NO_REORDER|ChJTable.DEFAULT_RENDERER);
            _table.renderer(this).setColumnWidthC(false, OVERWRITE, LABEL.t("exists"));
            final Container pFill=pnl(HB, new ChButton("CLR").t("Clear protein name").li(this), "Take column in protein header ");
            for(int i=7;--i>=0;) {
                pFill.add(new ChButton("FIELD").t(toStrg(7-i)).li(this));
                if (i==1) pFill.add(labl(" or "));
            }
            final Object
                expl= "Short explanation: \n"+
                "The headers in fasta files often consist of fields that are separated by vertical bar.\n",
                explanation= expl+"For example press the button \"3\" to use the 3rd field of the fasta header as the protein name.",
                explanationRG=
                expl+
                "The fasta header can be used to form protein names.\nSee WIKI:Regular_expression.";
            pFill.add(labl("  Seperator:"));
            pFill.add(_tfSeparator);
            pFill.add(smallHelpBut(explanation),"#");
            final Object
                pReg=pnl(
                         new GridLayout(3,1),
                         pnl(CNSEW,tfExpr,null,null,null,"Expression for fasta header:"),
                         pnl(CNSEW,tfRepl,null,null,null,"Replacement for protein name:"),
                         pnl(HBL,
                             new ChButton("RE").t("Fill the column \"protein name\" using regular expressions").li(this).tt("Find protein names  applying the regular <br>expressions on fasta header"),
                             " ", smallHelpBut(explanationRG)
                             )
                         ),
                pSouth=pnl(VBPNL,
                           pnl(new ChButton("GO").li(this).t(ChButton.GO),"If a protein file already exists:",pnl(_togOverwrite)),
                           pnl(VBPNL,"#TBem Protein header infers  protein name",
                               pFill,
                               pnlTogglOpts("Advanced: regular expressions",pReg),
                               pReg
                               )
                           );
            remainSpcS(this, pnl(CNSEW,scrllpn(_table),pnl(HB,"Preview: A protein file could be created for each selected table row  ","#", ChButton.doClose15(CLOSE_RM_CHILDS, this)), pSouth));
        }
    }

}

