package charite.christo.strap;
/**HELP

<i>PACKAGE:charite.christo.</i>
Transmembrane Helices are predicted from the amino acid sequences.
The predicted helical regions are highlighted in the alignment.

<br><br>
Example: UNIPROT:P32302

<br>
Related: TRAMPLE (PUBMED:15980454)
   @author Christoph Gille
*/
public class DialogPredictTmHelices2 extends AbstractDialogPredict {
    {
        init(this, charite.christo.protein.TransmembraneHelix_Predictor.class, "transmembrane helices");
    }
}
