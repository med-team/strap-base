package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import charite.christo.strap.extensions.*;
import java.awt.*;
import java.net.URL;
import java.io.*;
import java.util.*;
import java.util.List;
import javax.swing.JFrame;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.strap.StrapAlign.*;
import static charite.christo.strap.SPUtils.*;
/*
  @author Christoph Gille

  sleep?
  mseconds number
  H Sleeps the amount of time

  3D_select

  Siehe TestScriptDocu
*/
public class StrapScriptInterpreter implements ChRunnable {
 public final static String
     SCRIPT_echo="echo ",
     SCRIPT_print_proteins="print_proteins ",
     SCRIPT_print_selections="print_residue_selections ",
     SCRIPT_print_alignment="print_alignment ",
     SCRIPT_print_3D_transformation="print_3D_transformation ",
     SCRIPT_tell_3D="3D_native ",
     SCRIPT_set_residue_index_offset="set_residue_index_offset ",
     SCRIPT_set_ruler_secondary_structure="set_ruler_secondary_structure ",
     SCRIPT_set_conservation_threshold="set_conservation_threshold ",
     SCRIPT_set_export_columns="set_export_columns ",
     SCRIPT_set_characters_per_line="set_characters_per_line ",
     SCRIPT_set_color_mode="set_color_mode ",
     SCRIPT_put_property="put_property ",
     SCRIPT_find_uniprot_id="find_uniprot_id ",
     SCRIPT_balloon_text="balloon_text ",
     SCRIPT_find_uniprot_id_fast="find_uniprot_id_fast ",
     SCRIPT_download_files="download_protein_files ",
     SCRIPT_define_complex="define_protein_complex ",
     SCRIPT_rotate_translate_complex="rotate_translate_protein_complex ",
     SCRIPT_superimpose_complex="superimpose_protein_complexes ",
     SCRIPT_use_3D_viewer="use_3D_viewer ",
     SCRIPT_select_3D=Protein3dUtils.SCRIPT_select_3D,
     SCRIPT_open_3D="open_3D ",
     SCRIPT_close_3D="close_3D ",
     SCRIPT_to_front_3D="to_front_3D ",
     SCRIPT_aa_sequence="aa_sequence ",
     SCRIPT_accession_id="accession_id ",
     SCRIPT_add_annotation="add_annotation ",
     SCRIPT_add_xref="add_xref ",
     SCRIPT_align="align ",
     SCRIPT_below_row="below_row ",
     SCRIPT_box="box ",
     SCRIPT_ants="ants ",
     SCRIPT_cds="cds ",
     SCRIPT_close="close ",
     SCRIPT_delete="delete ",
     SCRIPT_wire="wire ",
     SCRIPT_close_wire="close_wire ",
     SCRIPT_cursor="cursor ",
     SCRIPT_deiconify="deiconify",
     SCRIPT_gaps="gaps ",
     SCRIPT_hide="hide ",
     SCRIPT_icon="icon ",
     SCRIPT_iconify="iconify",
     SCRIPT_load="load ",
     SCRIPT_sleep="sleep ",
     SCRIPT_secondary_structure="secondary_structure ",
     SCRIPT_new_nucleotide_selection="new_nucleotide_selection ",
     SCRIPT_new_aminoacid_selection="new_selection ",
     SCRIPT_plugin="plugin ",
     SCRIPT_remove_xref="remove_xref ",
     SCRIPT_rotate_translate="rotate_translate ",
     SCRIPT_scroll_to="scroll_to ",
     SCRIPT_select="select ",
     SCRIPT_seqvista="seqvista ",
     SCRIPT_jalview="jalview ",
     SCRIPT_spice="spice ",
     SCRIPT_set_annotation="set_annotation ",
     SCRIPT_superimpose="superimpose ",
     SCRIPT_to_row="to_row ",
     SCRIPT_tree="tree ",
     SCRIPT_unhide="unhide ",
     SCRIPT_unselect="unselect ",
     SCRIPT_infer_xyz="project_coordinates ",
     SCRIPT_das="DAS_features ",
     SCRIPT_gff_expasy="GFF_expasy_features ",
     SCRIPT_toFront="STRAP_to_front",
     SCRIPT_src="source_code ",
     SCRIPT_msec="mseconds",
     SCRIPT_exit="exit",
     SCRIPT_wait_for="wait_for ",
     ANY_VIEWER="$ANY_VIEWER", ANY_WIRE="$ANY_WIRE";
    public final static Map<String,Object> VALUES=new HashMap();
    private String _view3dID;
    private final Collection<String> _vJobs=new HashSet(), _vJobsKeep=new HashSet();
    private Protein[] _view3D_pp;
    private static String _sView3dID;
    private static Protein[] _sview3Dpp;
    private static BA _ipt;
    public static BA interpretedText() {  if (_ipt==null) _ipt=new BA(9999); return _ipt;}
    private Runnable thread_interpretScript(BA script) {
        return thrdM("interpretScript", this, new Object[]{script});
    }

    public void interpretScript(BA script) {
        if (sze(script)==0) return;
        interpretedText().aln(script);
        script.replace(' '|STRPLC_FILL_RIGHT, "new_aminoacid_selection ",SCRIPT_new_aminoacid_selection);
        if (!isEDT()) {
            inEDT(thread_interpretScript(script));
            return;
        }
        final boolean gui=withGui();
        if (gui) newAlignmentPanel(false);
        final int ends[]=script.eol();
        final byte[] T=script.bytes();
        boolean deiconify=!prgOptT("-iconify");
        for(int iL=0; iL<ends.length; iL++) {
            final int B=nxtE(-SPC,T,iL==0 ? script.begin() : ends[iL-1]+1,ends[iL]), E=ends[iL];
            if (E-B>6 && strEquls(STRSTR_w, SCRIPT_iconify,T,B)) deiconify=false;
        }
        if (deiconify && gui) frame().setExtendedState(JFrame.NORMAL);

        final ChTokenizer TOK=new ChTokenizer();
        final StrapView view=alignmentPanel();
        final String WNA="Error: wrong number of arguments ";
        final ChRunnable logR=StrapAlign.getInstance();
        final BA print=new BA(99);
        for(int iL=0; iL<ends.length; iL++) {
            print.clr();
            final Protein ppAll[]=proteins(), ppVis[]=gui ? visibleProteins() : ppAll;
            final int B, B0=nxtE(-SPC,T,iL==0 ? script.begin() : ends[iL-1]+1,ends[iL]), E=prev(-SPC,T, ends[iL]-1,B0-1)+1;
            if (E-B0<2 || T[B0]=='#') continue;
            Object error=null, warning=null;
            CharSequence objects=null;
            boolean asterisk=false;
            final String jobId, cmd;
            final boolean is3DCmd;
            int evt=-1, evtLater=-1;
            Runnable threadBG=null;
            if (E-B0>1 && T[B0]=='b' && T[B0+1]=='g' && !is(-SPC,T,B0+2)) {
                B=nxt(-SPC,T,B0+2,E);
                jobId=!gui?null:wordAt(B,T);
                if (B<0) error="Missing command after \"bg\"";
            } else {
                B=B0;
                jobId=null;
            }
            try {
                if (error!=null) continue;
                {
                    String c=null;
                    boolean is3D=false;
                    for(String s : allScriptCommands()) {
                        final int len=s.length()-(lstChar(s)==' '?1:0);
                        if (B+len<=E && strEquls(STRSTR_w, s,0, len,T,B)) {
                            c=s;
                            break;
                        }
                    }
                    for(String s : Protein3dUtils.allCommands()) {
                        if (B+s.length()<=E && strEquls(s,T,B)) { c=s; is3D=true; break; }
                    }
                    is3DCmd=is3D;
                    cmd=c;
                }
                if (cmd==SCRIPT_exit) break;

                if (!gui && (cmd==SCRIPT_use_3D_viewer || cmd==SCRIPT_tell_3D || cmd=="to_3D "|| cmd==SCRIPT_select_3D || cmd==SCRIPT_open_3D  || cmd==SCRIPT_close_3D ||
                             cmd==SCRIPT_close_3D || cmd==SCRIPT_to_front_3D || cmd==SCRIPT_below_row || cmd==SCRIPT_box ||
                             cmd==SCRIPT_wire || cmd==SCRIPT_close_wire || cmd==SCRIPT_cursor || cmd==SCRIPT_deiconify || cmd==SCRIPT_sleep ||
                             cmd==SCRIPT_scroll_to || cmd==SCRIPT_select || cmd==SCRIPT_seqvista || cmd==SCRIPT_jalview || cmd==SCRIPT_spice || cmd==SCRIPT_to_row ||
                             cmd==SCRIPT_tree || cmd==SCRIPT_toFront ||
                             cmd==SCRIPT_src || cmd==SCRIPT_msec || cmd==SCRIPT_exit || cmd==SCRIPT_wait_for)) continue;
                final int spc=strchr(STRSTR_E,' ',T,B,E);
                final int noSpc=spc<0 ? E : nxtE(-SPC, T,spc,E);
                TOK.setText(T,noSpc,E).setDelimiters(chrClas1(','));

                final boolean
                    parameter_pp=
                    cmd==SCRIPT_set_residue_index_offset || cmd==SCRIPT_put_property ||
                    cmd==SCRIPT_infer_xyz || cmd==SCRIPT_gaps || cmd==SCRIPT_cds || cmd==SCRIPT_to_row ||
                    cmd==SCRIPT_below_row ||
                    cmd==SCRIPT_rotate_translate || cmd==SCRIPT_rotate_translate_complex ||
                    cmd==SCRIPT_das ||
                    cmd==SCRIPT_add_xref || cmd==SCRIPT_accession_id || cmd==SCRIPT_remove_xref ||
                    cmd==SCRIPT_secondary_structure,
                    only_pp=
                    cmd==SCRIPT_print_selections || cmd==SCRIPT_print_3D_transformation ||
                    cmd==SCRIPT_set_ruler_secondary_structure ||
                    cmd==SCRIPT_gff_expasy ||
                    cmd==SCRIPT_find_uniprot_id || cmd==SCRIPT_find_uniprot_id_fast || cmd==SCRIPT_download_files ||
                    cmd==SCRIPT_define_complex ||
                    cmd==SCRIPT_wire || cmd==SCRIPT_close_wire || cmd==SCRIPT_hide || cmd==SCRIPT_unhide || cmd==SCRIPT_tree ||
                    cmd==SCRIPT_align  || cmd==SCRIPT_superimpose ||cmd==SCRIPT_superimpose_complex ||
                    cmd==SCRIPT_seqvista || cmd==SCRIPT_jalview || cmd==SCRIPT_spice,
                    parameter_pp_aa= cmd==SCRIPT_icon || cmd==SCRIPT_balloon_text,
                    only_pp_aa= cmd==SCRIPT_close || cmd==SCRIPT_delete  || cmd==SCRIPT_select || cmd==SCRIPT_unselect ||
                    cmd==SCRIPT_scroll_to || cmd==SCRIPT_box||cmd==SCRIPT_ants || cmd==SCRIPT_cursor,
                    parameter_p_or_new=cmd==SCRIPT_aa_sequence;
                final String para;
                if (parameter_pp_aa || parameter_pp || parameter_p_or_new) {
                    final int lastKomma=strchr(STRSTR_PREV,',',T,E,noSpc-1);
                    if (lastKomma<0)  error="Komma missing";
                    para=toStrg(T,noSpc,lastKomma).trim();
                    objects=new BA(T,lastKomma+1, E);
                } else if (only_pp_aa || only_pp) {
                    objects=new BA(T, noSpc, E);
                    para=null;
                } else para=toStrg(T, noSpc, E);
                asterisk=sze(objects)==1 && '*'==chrAt(0,objects);
                final boolean paraNull=sze(para)==0 || "NULL".equals(para);
                final String paraOrNull=paraNull ? null:para;
                /*
                //putln(ANSI_YELLOW+"cmd="+ANSI_RESET+cmd+"<\n"+ANSI_YELLOW+"para="+ANSI_RESET+para+"\n"+ANSI_YELLOW+"objects="+ANSI_RESET+objects);
                */
                if (cmd==SCRIPT_sleep || cmd==SCRIPT_set_characters_per_line  || cmd==SCRIPT_set_conservation_threshold) {
                    if (!cntainsOnly(DIGT,T,noSpc,E)) error="Expect number after "+cmd;
                }
                if (cmd==SCRIPT_set_residue_index_offset) {
                    if (!cntainsOnly(DIGT,para))  error="Expect number after "+cmd;
                }
                if (cmd==SCRIPT_toFront || cmd==SCRIPT_iconify || cmd==SCRIPT_deiconify) {
                    if (sze(para)>0) error="Not expecting a parameter ";
                }
                if (error!=null) {
                }
                else if (cmd==SCRIPT_print_proteins) print.a(' ').join(ppAll," ");
                else if (cmd==SCRIPT_echo) print.a(T,noSpc-1,E);
                else if (cmd==SCRIPT_wait_for) {
                    final String[] waitFor=splitTokns(para);
                    threadBG=thrdCR(this,"WAIT_JOB", waitFor);
                    if (waitFor.length==0) error="Expected command name";
                    else for (String s : waitFor) if (!_vJobsKeep.contains(s))  warning="No such background process \""+s+"\"";
                } else if (cmd==SCRIPT_set_characters_per_line || cmd==SCRIPT_set_export_columns) {
                    VALUES.put(cmd,para);
                } else if (cmd==SCRIPT_set_conservation_threshold) {
                    VALUES.put(cmd,para);
                    if (gui) StrapView.sliderSimilarity().setValue(atoi(para));
                } else if (cmd==SCRIPT_sleep) { threadBG=thrdSleep(atoi(para));
                } else if (cmd==SCRIPT_set_color_mode) {
                    if (ShadingAA.set(para)) StrapEvent.dispatch(StrapEvent.AA_SHADING_CHANGED);
                    else error=new BA("Expect color mode like ").join(ShadingAA.TYPES,", ").a(" after ").a(SCRIPT_set_color_mode);
                } else if (cmd==SCRIPT_use_3D_viewer) { if (!setDefaultC(ProteinViewer.class, StrapPlugins.mapS2L(para))) error="Unknown 3D viewer "+para;
                } else if (cmd==SCRIPT_iconify || cmd==SCRIPT_deiconify) {
                    if (gui) frame().setExtendedState(cmd==SCRIPT_deiconify ? JFrame.NORMAL : JFrame.ICONIFIED);
                } else if (cmd==SCRIPT_toFront) {
                    if (gui) {
                        setWndwStateT('F', -1, -1, frame().getTitle());
                        frame().shw(0).setExtendedState(JFrame.NORMAL);
                        if (view!=null) view.alignmentPane().requestFocus();
                    }
                } else if (cmd==SCRIPT_msec && is(DIGT, T, noSpc)) {
                    final int ms=atoi(T,noSpc,E);
                    final BA command=new BA(T,nxtE(SPC,T,noSpc,E),E);
                    if (ms>0 && sze(command)>0) inEDTms(thread_interpretScript(command), ms);
                    else error="Expected number followed by script command ";
                } else if (T[B]=='*') new StrapEvent(StrapAlign.class,StrapEvent.SCRIPT_LINE).setParameters(new String[]{script.newString(B,E)}).run();
                else if (cmd==SCRIPT_load) {
                    final String[] ss=splitTokns(0L, T, noSpc,E, chrClas(SPC));
                    if (ss.length==0) error="Expected list of proteins (DB-references, protein files, URLs)";
                    else threadBG=thread_downloadProteins(OPTION_PROCESS_LOADED_PROTS, ss, oo(StrapView.KEY_AP),(List)null,(List)null);
                } else if (parameter_p_or_new) {
                    final Protein pp[]=proteinsWithRegex(objects,ppAll);
                    if (cmd==SCRIPT_aa_sequence) {
                        final String gapped=toStrgTrim(para);
                        if (sze(gapped)>0 && sze(objects)>0) {
                            final Protein prots[];
                            if (sze(pp)>0) prots=pp;
                            else {
                                prots=new Protein[]{new Protein(StrapAlign.getInstance())};
                                prots[0].setName(filtrS('_'|FILTER_NO_MATCH_TO, FILENM, toStrgTrim(objects)));
                            }
                            setIsInAlignment(true, 0, prots);
                            for(Protein p : prots) {
                                p.setResidueType(filtrS(0L, LETTR,gapped));
                                p.selectCodingStrand(Protein.NO_TRANSLATE);
                                if (gapped.indexOf('-')>=0) p.inferGapsFromGappedSequence(gapped.getBytes());
                                evt=StrapEvent.ALIGNMENT_CHANGED;
                            }
                        } else error=WNA;
                    }
                } else if (parameter_pp_aa || only_pp_aa) {
                    final Collection vSel=selectedObjectsV();
                    final int mc=modic(vSel);
                    if (sze(objects)==0) error="Expected list of proteins or residue selections";
                    else if (parameter_pp_aa && sze(para)==0) error="Expected parameter";
                    else if (asterisk && cmd==SCRIPT_unselect) vSel.clear();
                    else {
                        final char alsoAnonymSel=cmd==SCRIPT_scroll_to || cmd==SCRIPT_box||cmd==SCRIPT_ants || cmd==SCRIPT_cursor ?'A':'a';
                        final Object[] pp_aa=proteinsAndAnnotationsWithRegex(objects,ppVis, alsoAnonymSel);
                        for(Object o : pp_aa) {
                            final ResidueAnnotation a=deref(o, ResidueAnnotation.class);
                            final Protein p=deref(o, Protein.class),  prot=p!=null ? p : a!=null ? a.getProtein() : null;
                            if (cmd==SCRIPT_select) vSel.add(o);
                            else if (cmd==SCRIPT_unselect) vSel.remove(o);
                            else if (cmd==SCRIPT_close || cmd==SCRIPT_delete) {
                                if (p!=null) {
                                    rmProteins(false,p);
                                    evt=StrapEvent.PROTEINS_KILLED;
                                } else if (a!=null) {
                                    a.dispose();
                                    evt=StrapEvent.RESIDUE_SELECTION_CHANGED;
                                }
                            }  else if (cmd==SCRIPT_balloon_text) {
                                if (p!=null) { p.setBalloonText(paraOrNull); evt=StrapEvent.PROTEIN_INFO_CHANGED; }
                                if (a!=null) a.setValue(0,ResidueAnnotation.BALLOON,para);
                            }  else if (cmd==SCRIPT_icon) {
                                if (p!=null) {
                                    p.setIconImage(paraOrNull);
                                    evt=StrapEvent.PROTEIN_RENAMED;
                                } if (a!=null) {
                                    a.setValue(0,ResidueAnnotation.BG_IMAGE,paraOrNull);
                                    evtLater=StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR;
                                }
                            } else if (cmd==SCRIPT_cursor && a!=null && view!=null) {
                                view.setCursor(0, sp(orO(p,a)),fstTrue(a.getSelectedAminoacids())+ResSelUtils.selAminoOffsetZ(a));
                                view.scrollCursorToVisible();
                            } else if (cmd==SCRIPT_scroll_to || cmd==SCRIPT_box||cmd==SCRIPT_ants) {
                                final Rectangle r=ResSelUtils.enclosingRectangle(pp_aa);
                                if (r!=null && view!=null) {
                                    if (cmd==SCRIPT_scroll_to) view.scrRectToRowCol(true, r);
                                    else view.setRectRubberBand(x(r), y(r), wdth(r), hght(r));
                                }
                            }
                        }
                    }
                    if (modic(vSel)!=mc) evt=StrapEvent.OBJECTS_SELECTED;
                } else if (cmd==SCRIPT_new_aminoacid_selection || cmd==SCRIPT_new_nucleotide_selection) {
                    final String
                        aName_slash_protein=TOK.nextAsStringTrim(),
                        protein=delFromChar(aName_slash_protein,'/'),
                        aName=lstPathCmpnt(aName_slash_protein),
                        grp=TOK.nextAsStringTrim(),
                        resi=TOK.nextToken() ? script.newString(TOK.from(),E).trim() : null;
                    if (resi!=null && grp!=null && aName_slash_protein!=aName && aName.length()>0) {
                        for(Object o : proteinsAndAnnotationsWithRegex(protein,ppVis, 'p')) {
                            final Protein p=deref(o,Protein.class);
                            if (p==null) continue;
                            ResidueAnnotation a=null;
                            for(ResidueAnnotation s : p.residueAnnotations()) {
                                if (aName.equals(s.getName()) && s.value(ResidueAnnotation.POS).equals(resi) ) {
                                    a=s;
                                    break;
                                }
                            }
                            if (a==null) a=new ResidueAnnotation(p);
                            a.addE(cmd==SCRIPT_new_aminoacid_selection?0: ResidueAnnotation.E_NT, ResidueAnnotation.POS, resi);
                            a.addE(0,ResidueAnnotation.NAME, aName);
                            a.addE(0,ResidueAnnotation.GROUP, grp);
                            p.addResidueSelection(a);
                            evt=StrapEvent.RESIDUE_SELECTION_ADDED;
                        }
                    } else error=WNA;
                } else if (parameter_pp || only_pp) {
                    final Protein[] pp=proteinsWithRegex(objects,ppVis);
                    if (parameter_pp && sze(para)==0) error="Expected parameter";
                    else if (sze(objects)==0) error="Expected list of proteins";
                    else if (pp.length==0) error="P";
                    else {
                        for(Protein p : pp) {
                            if (cmd==SCRIPT_set_residue_index_offset) p.setResidueIndexOffset(atoi(para));
                            else if (cmd==SCRIPT_put_property) {
                                final int eq=para.indexOf('=');
                                final String val=para.substring(eq+1).trim();
                                pcp((eq>0?para.substring(0,eq):para).trim(), "NULL".equals(val)?null:val, p);
                            } else if (cmd==SCRIPT_secondary_structure) {
                                p.setResidueSecStrType(para.getBytes());
                                evtLater=StrapEvent.AA_SHADING_CHANGED;
                            } else if (cmd==SCRIPT_gaps) {
                                p.inferGapsFromGappedSequence(para.getBytes());
                                evtLater=StrapEvent.ALIGNMENT_CHANGED;
                            } else if (cmd==SCRIPT_cds) {
                                p.parseCDS(cntainsOnly(DIGT,para) ? get(atoi(para)-1,p.getCDS()) : para);
                                evt=StrapEvent.NUCL_TRANSLATION_CHANGED;
                            } else if ((cmd==SCRIPT_to_row || cmd==SCRIPT_below_row)&& view!=null) {
                                final int src=idxOf(p,ppVis);
                                final Object pDest=proteinsWithRegex(para,ppVis);
                                int dst=cntainsOnly(DIGT,para) ? atoi(para)-1 : idxOf(get(0,pDest), ppVis);
                                if (pDest!=null && dst>src) dst--;
                                if (dst>=0) {
                                    if (cmd==SCRIPT_below_row) dst++;
                                    if (src>=0 && dst>=0 && src!=dst) view.moveLines(src, mini(view.countRows()-1, dst), 1);
                                    evt=StrapEvent.ORDER_OF_PROTEINS_CHANGED;
                                } else error="Wrong destination row";
                            } else if (cmd==SCRIPT_rotate_translate || cmd==SCRIPT_rotate_translate_complex) {
                                final Protein[] ppMatrix=proteinsWithRegex(para, ppAll);
                                final Matrix3D m3d=paraNull ? null : ppMatrix.length==1 ? ppMatrix[0].getRotationAndTranslation() : new Matrix3D();
                                if (paraNull || ppMatrix.length==1 || m3d.parsePlain(para.getBytes(),0, MAX_INT)) {
                                    p.setRotationAndTranslation(m3d);
                                    if (cmd==SCRIPT_rotate_translate_complex) {
                                        for(Protein p2 : p.getProteinsSameComplex()) p2.setRotationAndTranslation(m3d);
                                    }
                                    evt=StrapEvent.PROTEIN_3D_MOVED;
                                } else error="Error parsing matrix ";
                            } else if (cmd==SCRIPT_spice) { SequenceFeatures.toSpiceViewer(p);
                            } else if (cmd==SCRIPT_close_wire) {
                                V3dUtils.disposeV( derefArray(p.getProteinViewers(),Protein3d.PView.class));
                            } else if (cmd==SCRIPT_add_xref || cmd==SCRIPT_accession_id || cmd==SCRIPT_remove_xref ) {
                                if (cmd==SCRIPT_accession_id) p.setAccessionID(para);
                                else if (cmd==SCRIPT_add_xref) p.addSequenceRef(para);
                                else if (cmd==SCRIPT_remove_xref) p.removeSequenceRef(para);
                                evtLater=StrapEvent.PROTEIN_XREF_CHANGED;
                                if (para.indexOf(':')<0) error="Expected database reference like UNIPROT:P29590";
                            } else if (cmd==SCRIPT_seqvista) {
                                Hyperrefs.toSeqvista(new File[]{p.getFile()});
                            } else if (cmd==SCRIPT_define_complex) {
                                p.setProteinsSameComplex(pp);
                            } else if (cmd==SCRIPT_print_selections) {
                                print.tab(p);
                                for(ResidueSelection s : p.allResidueSelections()) {
                                    print.a(nam(s)).a('=').boolToText(s.getSelectedAminoacids(),s.getSelectedAminoacidsOffset()+1, ",","-").a(' ');
                                }
                                print.a('\n');
                            } else if (cmd==SCRIPT_print_3D_transformation) {
                                final Matrix3D m3d=p.getRotationAndTranslation();
                                print.tab(p);
                                if (m3d==null) print.aln("NULL");
                                else m3d.toText(Matrix3D.FORMAT_PLAIN, "", print).a('\n');
                            }
                        }/*for p*/
                        if (cmd==SCRIPT_find_uniprot_id || cmd==SCRIPT_find_uniprot_id_fast) {
                            threadBG=FindUniprot.thread_load(FindUniprot.FETCH_AFTER|(cmd==SCRIPT_find_uniprot_id?FindUniprot.BLAST_EBI : 0), pp, logR);
                        } else if (cmd==SCRIPT_infer_xyz) {
                            if (paraNull) {
                                for(Protein p : pp) p.detach3DStructure();
                                evtLater=StrapEvent.RESIDUE_SELECTION_DELETED;
                            } else if (pp.length>0) {
                                final String ids[]="AUTO".equals(para) ? INFER3D_AUTO : strgArry(para);
                                threadBG=thrdRRR(threadInferCoordinates(INFER3D_IF_NOT_ALREADY, pp, ids)); /* EBI_NCBI */
                            }
                        } else if (cmd==SCRIPT_das) { threadBG=StrapDAS.thread_load(0, splitTokns(para), pp,  logR);
                        } else if (cmd==SCRIPT_set_ruler_secondary_structure) {
                            Object r="";
                            for(Protein p : pp) {
                                if (p.getResidueSecStrType()!=null) {
                                    if (view!=null) view.setSecStruP(p);
                                    r=wref(p);
                                    break;
                                }
                            }
                            VALUES.put(cmd, r);
                        } else if (cmd==SCRIPT_gff_expasy) { threadBG=thrdCR(this,"GFF", new Object[]{pp,logR});
                        } else if (cmd==SCRIPT_wire) { webAlignment3D(pp, null,true);
                        } else if (cmd==SCRIPT_hide) { setIsInAlignment(false,  0, pp);
                        } else if (cmd==SCRIPT_unhide) { setIsInAlignment(true, 0, pp);
                        } else if (cmd==SCRIPT_align) { threadBG=threadAlignProteins(0, (SequenceAligner)null,Arrays.asList(pp),CAN_BE_STOPPED);
                        } else if (cmd==SCRIPT_download_files) { threadBG=thread_fetchIdsAndDownloadOriginalProteins(false, pp);
                        } else if (cmd==SCRIPT_jalview) {  new ToJalview().launchJalview(ToJalview.SECSTRU, pp, null);
                        } else if (cmd==SCRIPT_superimpose || cmd==SCRIPT_superimpose_complex) {
                            threadBG=threadSuperimposeProteins(SUPERIMP_EVENT|(cmd==SCRIPT_superimpose_complex?SUPERIMP_COMPLEX_BEST:0), Arrays.asList(pp), CAN_BE_STOPPED, null);
                        } else if (cmd==SCRIPT_tree) {
                            final Protein ppT[]=noDuplicateNames(pp,names(pp,true,true));
                            if (ppT.length>2) {
                                final String nn[]=names(ppT,true,true);
                                final byte[][] gg=gappedSequences(pp, 0, MAX_INT, '-');
                                final PhylogeneticTree tree=mkInstance(defaultClass(PhylogeneticTree.class), PhylogeneticTree.class);
                                if (tree!=null) {
                                    tree.setAlignment(rmNullS(nn), rmNullA(gg, byte[].class), null);
                                    tree.drawTree();
                                }
                                setWndwStateLaterT('F',444, "Phylogenetic Tree");
                            } else error="Phylogenetic tree:s At least 3 proteins are required";
                        }
                    }
                } else if (cmd==SCRIPT_close_3D || cmd==SCRIPT_open_3D || cmd==SCRIPT_select_3D || cmd==SCRIPT_to_front_3D) {
                    final int komma;
                    {
                        int i, countQ=0;
                        for( i=noSpc; i<E; i++) {
                            if (T[i]=='"') countQ++;
                            if (countQ%2==0 && T[i]==',') break;
                        }
                        komma=i;
                    }
                    final String script_vID=toStrg(T,noSpc,komma).trim();
                    final String pp_aa_ss=script.newString(komma+1, E);
                    final Class c=name2class(defaultClass(ProteinViewer.class));
                    final int lastSpc=prev(SPC, script_vID, MAX_INT,-1);
                    final String viewId=toStrgIntrn(script_vID.substring(lastSpc+1));
                    if (sze(viewId)==0) error="3D-Viewer-ID not defined";
                    else if (c==null) error="No such 3D-viewer Java class  "+defaultClass(ProteinViewer.class);
                    else {
                        final Object pp_aa[]=proteinsAndAnnotationsWithRegex(pp_aa_ss,ppVis,'A');
                        final ResidueAnnotation aa[]=derefArray(pp_aa,ResidueAnnotation.class);
                        final ProteinViewer
                            pv=get(0, V3dUtils.mapProteinViewers().get(viewId), ProteinViewer.class),
                            vv[]=Protein3dUtils.vvSharingView(pv);
                        final Protein[] pp;
                        if (sze(pp_aa_ss)==0) pp=Protein3dUtils.ppInV3D(vv);
                        else {
                            final Collection vP=new Vector();
                            for (Object o : pp_aa) adUniq( o instanceof Protein ? o : o instanceof ResidueSelection ? ((ResidueSelection)o).getProtein() : null, vP);
                            pp=spp(vP);
                        }
                        if (cmd==SCRIPT_close_3D) {
                            for(ProteinViewer v : Protein3dUtils.vvSharingView(pv)) {
                                if (sze(pp_aa_ss)==0 || cntains(v.getProtein(),pp)) V3dUtils.disposeV(v);
                            }
                        } else if (cmd==SCRIPT_open_3D) {
                            if (countNotNull(pp)>0) {
                                final ProteinViewer pvNew=V3dUtils.open3dViewer(0, pp, pv!=null?pv:c);
                                if (pv==null) {
                                    if (pvNew!=null) V3dUtils.mapProteinViewers().put(viewId, pvNew.getViewersSharingViewV(true));
                                    else error="Could not instantiate protein viewer "+c;
                                }
                                for(ProteinViewer v : Protein3dUtils.vvSharingView(pv)) if (!cntains(v.getProtein(),pp)) V3dUtils.disposeV(v);
                            }
                        }
                        if (cmd==SCRIPT_select_3D || cmd==SCRIPT_open_3D) {
                            _sview3Dpp=_view3D_pp=pp;
                            _sView3dID=_view3dID=viewId;
                            if (sze(aa)>0) {
                                for (ProteinViewer v : vv) V3dUtils.selectedAAto3D_single(0, aa, v, "unnamed");
                            }
                        }
                        if (cmd!=SCRIPT_close_3D) inEDTms(V3dUtils.runViewerToFront(pv),99);
                    }
                } else if (is3DCmd || cmd==SCRIPT_tell_3D || cmd=="to_3D ") {
                    Protein pp[]=_view3D_pp;
                    if (pp==null) pp=_sview3Dpp;
                    String viewId=_view3dID;
                    if (viewId==null) viewId=_sView3dID;
                    final ProteinViewer
                        pv=get(0, V3dUtils.mapProteinViewers().get(viewId),ProteinViewer.class),
                        vv[]=Protein3dUtils.vvSharingView(pv);

                    final String pvNames=cmd==SCRIPT_tell_3D || cmd=="to_3D " ? TOK.nextAsStringTrim() : null;
                    final String txt=cmd==SCRIPT_tell_3D || cmd=="to_3D " ? TOK.remainingAsString(true) : script.newString(B,E);

                    for(int iP=0; iP<sze(pp); iP++) {
                        if (pp[iP]==null) continue;
                        boolean sent=false;
                        for(ProteinViewer v : pp[iP].getProteinViewers()) {
                            if ((ANY_WIRE==viewId || "ANY_WIRE"==viewId) && v instanceof Protein3d.PView ||
                                (ANY_VIEWER==viewId || "*"==viewId || "ANY_VIEWER"==viewId) && !(v instanceof Protein3d.PView) ||
                                viewId!=null && viewId.equalsIgnoreCase(niceShrtClassNam(v)) ||
                                cntains(v,vv)
                                ) {
                                sent=true;
                                if (pvNames!=null && strstr(STRSTR_IC|STRSTR_w, niceShrtClassNam(v),pvNames)<0) continue;
                                V3dUtils.sendCommand(ProteinViewer.INTERPRET_NO_MSG_DIALOGS, txt, v);
                            }
                        }
                        if (!sent) error="No 3D-viewer with id \""+viewId+"\"";
                    }
                } else if (cmd==SCRIPT_add_annotation || cmd==SCRIPT_set_annotation) {
                    final boolean edit=cmd==SCRIPT_add_annotation || cmd==SCRIPT_set_annotation;
                    if (TOK.nextToken()) {
                        final ChTokenizer tok=new ChTokenizer().setText(T,TOK.from(),TOK.to());
                        final String
                            key=edit ? StrapPlugins.mapS2L(TOK.nextAsStringTrim()) : null,
                            value=TOK.nextToken() ? script.newString(TOK.trim().from(),E) : null;
                        while(tok.nextToken()) {
                            for(Object o : proteinsAndAnnotationsWithRegex(tok.asString(),ppVis,'a')) {
                                if (!(o instanceof ResidueAnnotation)) { objects=tok.asString();  error="A"; continue; }
                                final ResidueAnnotation a=(ResidueAnnotation)o;
                                if (edit) {
                                    if (key==null || value==null) error=WNA;
                                    if (ResidueAnnotation.COLOR.equals(key)) a.setColor(str2color(value));
                                    else {
                                        if (cmd==SCRIPT_add_annotation) a.addE(0,key,value);
                                        else a.setValue(0,key,"NULL".equals(value)?null:value);
                                    }
                                    evt=StrapEvent.RESIDUE_SELECTION_CHANGED;
                                }
                            }
                        }
                    } else error=WNA;
                } else if (cmd==SCRIPT_plugin) {
                    if (gui) {
                        frame().setExtendedState(JFrame.NORMAL);
                        frame().toFront();
                    }
                    final String className=TOK.nextAsStringTrim(), sUrl=TOK.nextAsString(), delete[]=splitTokns(TOK.nextAsString());
                    final URL u=url(sUrl);
                    if (u==null) error="No URL: "+sUrl;
                    if (sze(className)>0 && u!=null) {
                        for(int i=sze(delete); --i>=0;) delFile(StrapPlugin.jarFile(delete[i]));
                        final File f=StrapPlugin.jarFile(u);
                        InteractiveDownload.downloadFiles(new URL[]{u}, new File[]{f});
                        startThrd(thrdCR(StrapAlign.getInstance(),StrapAlign.RUN_CHECK_PLUGIN_UPTODATE,new URL[]{u}));
                        java.util.zip.ZipFile zf=null;
                        try {zf=new java.util.zip.ZipFile(f);} catch(Exception ex){}
                        if (zf==null) error="Unable to open Jar-file "+f;
                        else {
                            final StrapPlugin plugin=new StrapPlugin(className, "", zf);
                            final Class c=plugin.getClazz(true);
                            if (c!=null) StrapPlugins.applyPlugin(c);
                            else error="Unable to load class :"+className;
                        }
                    }
                } else if (cmd==SCRIPT_src) {
                    if (TOK.nextToken()) {
                        final String cn=TOK.trim().asString();
                        try { Class.forName(cn); } catch(Exception ex) { error=ex;}
                        showJavaSource(cn,NO_STRING);
                    }
                } else if (T[0]!='#' && nxt(LETTR,T,B,E)>=0) error="Unknown script command";
                if (sze(print)>0) print(cmd,print);
            } finally {
                if (evt>=0) StrapEvent.dispatch(evt);
                if (evtLater>=0) StrapEvent.dispatchLater(evtLater,111);
                final int end=log().end();
                if (error=="P" || error=="A") {
                    if (!asterisk) log().a(T,B,E).a(" "+RED_WARNING).a("No such ").a(error=="P" ? "protein \"" : "residue selection \"").a(objects).a('"').a(T,B,E);
                } else if (error!=null || warning!=null) {
                    log().a('\n').a(error!=null?RED_ERROR:RED_WARNING).a(error).a(warning).a(": ").a(T,B,E).a('\n');
                }
                if (log().end()!=end) log().send();
                if (threadBG!=null) {
                    if (sze(jobId)>0) {
                        _vJobs.add(jobId);
                        _vJobsKeep.add(jobId);
                        startThrd(thrdRRR(threadBG, thrdM("remove", _vJobs, new Object[]{jobId})));
                    } else if (!gui) threadBG.run();
                    else {
                        startThrd(thrdRRR(threadBG,thread_interpretScript(new BA(T,E+1,script.end()))));
                        return;
                    }
                }
            }
            if (gui) setWndwState('F',frame());
        }
    }
    /* <<< interpretScript <<< */
    /* ---------------------------------------- */
    /* >>> Log >>> */
    private static BA _log;
    public static BA log() {
        if (_log==null) {
            _log=new BA(9999);
            if (withGui()) toLogMenu(_log, "Script interpreter", IC_CONSOLE);
            else _log.setSendTo(BA.SEND_TO_STDOUT);
        }
        return _log;
    }
    /* <<< Log <<< */
    /* ---------------------------------------- */
    /* >>> Runnable >>> */
    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id=="GFF") ExpasyGff.load(0, (Protein[])argv[0], (ChRunnable)argv[1]);
        if (id=="WAIT_JOB") {
            while(sze(_vJobs)>0) {
                sleep(111);
                boolean running=false;
                for(Object s : argv) if (_vJobs.contains(s)) { running=true; break;}
                if (!running) break;
            }
        }

        if (id=="OBSERVE_FILE") {
            if (_filesSkip>=0 && _filesSkip++%5!=0) return null;
            for(int i=_filesN; --i>=0;) {
                final File f=_filesF[i];
                final int s=sze(f);
                if (s<_filesS[i]) continue;
                RandomAccessFile fp=null;
                try {
                    fp=new RandomAccessFile(f,"r");
                    fp.seek(_filesS[i]);
                    final byte bb[]=new byte[s-_filesS[i]];
                    fp.readFully(bb,0,bb.length);
                    final int end=strchr(STRSTR_PREV, '\n', bb, bb.length,-1);
                    if (end<0) continue;
                    inEDT(thread_interpretScript(new BA(bb,0,end+1)));
                    _filesS[i]+=end+1;
                    _filesSkip=-1;/* from now on observe file more often */
                } catch(IOException iox){stckTrc(iox);}
                try { if (fp!=null) fp.close(); } catch(IOException iox){}
            }
        }

        return null;
    }
    /* <<< Runnable <<< */
    /* ---------------------------------------- */
    /* >>> static Utils >>> */
    private static int[] _filesS=new int[10];
    private static File[] _filesF=new File[10];
    private static int _filesN, _filesSkip;

    public void namedPipe(File f) {
        InputStream is=null;
        try {
            final ChInStream chis=new ChInStream(is=new FileInputStream(f),9999);
            putln(ANSI_GREEN+"Reading script from named pipe: "+ANSI_RESET,f);
            final BA txt=new BA(3333);
            while(true) {
                while(chis.readLines(txt.clr())) inEDT(thread_interpretScript(txt));
                sleep(333);
            }
        } catch(IOException ioex) { putln(RED_CAUGHT_IN+"StrapScriptInterpreter.namedPipe ",ioex);}
    }
    private static String[] _allCmds;
    public static String[] allScriptCommands() {
        if (_allCmds==null) _allCmds=finalStaticStrings(true, "SCRIPT_", StrapScriptInterpreter.class);
        return _allCmds;
    }
    private static BA concat(CharSequence[] scriptOrUrl, boolean showHtml) {
        final int N=sze(scriptOrUrl);
        final BA[] su=new BA[N];
        int len=N;

        for(int i=0; i<N; i++) {
            BA s=toBA(scriptOrUrl[i]);
            if (sze(s)>0) {
                s.rmFirstLastChar('"','"').trim();
                File f=null;
                String u=null;
                boolean pipe=false;

                if (looks(LIKE_EXTURL,s) && (u=toStrg(url(s)))!=null) s=readBytes(inStreamT(99,u,0L));
                else if (nxt(SPC,s)<0 && (looks(LIKE_FILEPATH,s) || cntainsOnly(FILENM,s)) && fExists(f=file(s))) {
                    if (withGui() && isNamedPipe(f)) startThrd(thrdM("namedPipe", new StrapScriptInterpreter(), new Object[]{f}));
                    else {
                        s=readBytes(f);
                        if (_filesN<_filesF.length) { _filesF[_filesN]=f; _filesS[_filesN++]=sze(f); }
                    }
                }
                if (u!=null||f!=null) log().a("Read ").or(u,f).a(' ').formatSize(sze(s)).a('\n');
            }
            su[i]=s;
            len+=sze(s);
        }
        if (_filesN>0) ChThread.callEvery(0, 999, thrdCR(new StrapScriptInterpreter(), "OBSERVE_FILE"), "OBSERVE_FILE");
        for(int i=N; --i>=0;) {
            final CharSequence url=scriptOrUrl[i];
            final BA html=su[i];
            if (looks(LIKE_HTML,html)) {
                final String preScript=MAP_ARGV.get("-preScript");
                if (showHtml && preScript==null) {
                    if (strstr(STRSTR_w_R, "<script",html)>0 || strstr(STRSTR_w,"stylesheet",html)>0) {
                        final File fHtml=newTmpFile(".html");
                        wrte(fHtml,html);
                        visitURL(fHtml,0);
                    } else new ChFrame("strap_script").ad(scrllpn(SCRLLPN_TOP, new ChJTextPane(html))).shw(CLOSE_CtrlW_ESC|ChFrame.CENTER|ChFrame.ALWAYS_ON_TOP);
                }
                (su[i]=new BA(sze(html)).filter(FILTER_HTML_DECODE,html)).insidePreTags(orS(preScript,"strap_script"));
                su[i].trimSize();
            }
        }
        final BA script=N==1 ? su[0] : new BA(len);
        for(int i=0; N!=1 && i<N; i++) script.aln(su[i]);
        return script;
    }
    public Runnable thread_runScripts(CharSequence[] scriptOrUrl) { return thrdM("runScripts", this, new Object[]{scriptOrUrl});}
    public void runScripts(CharSequence[] scriptOrUrl) { interpretScript(concat(scriptOrUrl,true)); }
    public static void showScriptText(List<BA> v, boolean showError) {
        if (isEDT()) startThrd(thrdM("showScriptText", StrapScriptInterpreter.class, new Object[]{v,boolObjct(showError)}));
        else {
            BA txt=concat(toArry(v,BA.class), false);
            if (sze(txt)==0) {
                if (!showError) return;
                txt=new BA("No script at startup");
            }
            new ChTextView(txt.trimSize()).tools()
                .showInFrame(ChFrame.STAGGER|ChFrame.SCROLLPANE|ChFrame.TO_FRONT,"Scripts")
                .underlineRefs(ULREFS_NO_ICON)
                .cp(HelpCommands.class, HelpCommands.getInstance(Strap.SCRIPT_COMMANDS))
                .tt(null);
        }
    }

    private static File _fOut;
    private static void print(String cmd, BA s) {
        if (_fOut==null) {
            _fOut=file(FILE_NO_ERROR, MAP_ARGV.get("-scriptOutput"));
            if (_fOut==null) {
                _fOut=ERROR_FILE;
                putln(RED_WARNING+" Output file not specified with the command line parameter -scriptOutput=.... ");
            }
        }
        if (_fOut!=ERROR_FILE) appndToFile(new BA(sze(s)+99).a("#BEGIN ").a(cmd).trim().a('\n').a(s).a1('\n').a("#END ").a(cmd).delBlanksR().a('\n',2), _fOut);
        else putln("\n"+ANSI_GREEN+"Script output "+ANSI_RESET,s,"\n");
    }

}
