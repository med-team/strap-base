package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

public class AbstractAlignAndCompare implements CompareTwoProteins, HasSharedControlPanel, HasControlPanel, Disposable, IsEnabled {
    private final static int VALUE_SCORE=0, VALUE_IDENT=1;
    private final char _mode;
    private Object _aligner;
    private double _value=Double.NaN;
    private int  _from=0, _to=MAX_INT, _scoreMethod;
    private boolean _dist, _done;
    private Protein _p0, _p1;
    private ChCombo _comboAligner, _comboScore;
    public AbstractAlignAndCompare(char m) {
        _mode=m;
    }
    public void dispose() { dispos(_aligner); }

  /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Get/Set >>> */
    public ChCombo comboClass() {
        if (_shared!=null) return _shared.comboClass();
        if (_comboAligner==null) {
            _comboAligner=SPUtils.classChoice(_mode=='3' ? Superimpose3D.class : SequenceAligner.class);
            if (_mode=='S') _comboAligner.s(CLASS_MultipleAlignerClustalW).classRenderer(SequenceAligner.class);
            if (_mode=='3') _comboAligner.classRenderer(Superimpose3D.class);
        }
        return _comboAligner;
    }
    public Object getOtherClass() { return comboClass().getSelectedItem(); }   // ResultCompareProteinPairs.java

    public Object getCalculator() { return _aligner;}
    /* <<< Get/Set <<< */
    /* ---------------------------------------- */
    /* >>> Ctrl  >>> */
    private AbstractAlignAndCompare _shared;
    public void setSharedInstance(Object i) {
        final AbstractAlignAndCompare a=(AbstractAlignAndCompare)i;
        _shared=a;
        _aligner=mkInstance(comboClass());
        //putln(YELLOW_DEBUG, "setSharedInstance ",_aligner);
        _scoreMethod=a==null || a._comboScore==null ? 0 : a._comboScore.i();
    }
    public Object getSharedInstance() { return _shared;}

    private Object _ctrl;
    public Object getSharedControlPanel() {
        if (_shared!=null) return _shared.getSharedControlPanel();
        if (_ctrl==null) {
            _comboScore=new ChCombo(_mode=='3' ? new String[]{"Score", "RMSD"} : new String[]{"Alignment Score","Sequence identity"});
            _ctrl=pnl(VB,
                      pnl( _mode=='3' ? "Superposition":"Alignment", " method ? ", comboClass().panel()),
                      pnl( "Returned value: ", _comboScore)
                      );
        }
        return _ctrl;
    }

    @Override public Object getControlPanel(boolean real) {
        if (!real) return hasCtrlPnl(_aligner) ? CP_EXISTS : null;
        return ctrlPnl(_aligner);
    }

    /* <<< Ctrl <<< */
    /* ---------------------------------------- */
    /* >>> Compute  >>> */

    @Override public void setProteins(Protein prot0, Protein prot1, int fromCol, int toCol) {
        _p0=prot0;
        _p1=prot1;
        _from=fromCol;
        _to=toCol;
    }
    @Override public void compute() {
        final Protein prot0=_p0, prot1=_p1;
        final int fromCol=_from, toCol=_to;
        if (_done || prot0==null||prot1==null) return;
        _done=true;

        final Protein
            p0=fromCol==0 && toCol>prot0.getMaxColumnZ() ? prot0 : SPUtils.newProteinColumnFromTo(prot0, _from,_to),
            p1=fromCol==0 && toCol>prot0.getMaxColumnZ() ? prot1 : SPUtils.newProteinColumnFromTo(prot1, _from,_to);
        if (_aligner==null) _aligner=mkInstance(_mode=='S' ? CLASS_MultipleAlignerClustalW : CLASS_Superimpose_CEPROXY);

        if (_aligner==null) { putln(RED_ERROR+"AbstractAlignAndCompare: cannot instantiate "); return; }
        final HasScore hasScore=deref(_aligner,HasScore.class);
        if (_mode=='S') {
            final SequenceAligner sa=deref(_aligner, SequenceAligner.class);
            //putln(YELLOW_DEBUG+"AbstractAlignAndCompare aligner=",sa);
            final byte aligned[][];
            if (sa==null) aligned=null;
            else {
                SPUtils.setSeqs(sa,p0,p1);
                sa.compute();
                aligned=sa.getAlignedSequences();
            }
            if (sze(aligned)!=2) _value=Double.NaN;
            else if (_scoreMethod==VALUE_IDENT) {
                final Ratio r=AlignUtils.getIdentity(aligned[0],aligned[1], 0, MAX_INT);
                final int count=r==null ? 0 : r.getDenominator();
                _value=count==0?Double.NaN : 1- r.getNumerator()/(double)count;
            }
            else if (hasScore!=null) _value=hasScore.getScore();
            else _value= Blosum.pairAignScore(-10, -1, aligned[0], aligned[1], 0,MAX_INT, -Blosum.BLOSUM62_MIN);
        }
        if (_mode=='3') {
            final Superimpose3D.Result r=SPUtils.superimposeTwoProteinsM(_aligner,p0,p1);
            _value=
                r==null ? Double.NaN :
                _scoreMethod==VALUE_SCORE && hasScore!=null ? hasScore.getScore() :
                r.getRmsd();
        }
        _dist=SPUtils.isDistanceScore(_aligner);
    }
    public double getValue() {  return _value; }
    public boolean isDistanceScore() { return _dist; }
    /* <<< Compute <<< */
    /* ---------------------------------------- */
    /* >>> Event  >>> */

    public boolean isEnabled(Object p) {
        return _mode!='3' || p instanceof Protein &&  ((Protein)p).getResidueCalpha()!=null;
    }

}
