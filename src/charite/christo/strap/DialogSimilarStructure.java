package charite.christo.strap;
import charite.christo.protein.*;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   Proteins loaded from sequence files like Uniprot or Genpept files lack 3D-information and cannot be visualized three-dimensionally.

   With this dialog  similar structures for given sequences can be identified.

   Only if a suitable 3D model exist, the 3D-coordinates can be assigned and the protein can be visualized in 3D.

   <br><br>

   The search result is presented in a table.
   Each table row represents one protein. It shows candidates of matching PDB-IDs.

   The location of the hit relative to the query sequence is shown graphically as a gray bar.

   <br>

   To infer the proposed 3D-coordinates
   the user needs to press the button <i>BUTTON:"Apply"</i> in the last table column.

   Those amino acids in the amino acid sequence that inherited xyz-3D-coordinates will be written as an upper case
   letter. Those, that are not assigned 3D-coordinates are shown as lower case letter.

   Amino acid positions where the residue type of the 3D model differs are marked.

   <br><br>

   The associated 3D-structure can be dissociated using the context menu of the protein.

   <i>SEE_DIALOG:DialogBlast</i>

   @author Christoph Gille
*/
public class DialogSimilarStructure extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener, StrapListener, Runnable {
    private final ProteinList _pList=new ProteinList(ProteinList.NO_3D_FILE);

    private int _count;
    public DialogSimilarStructure() {
        final Object
            pDetails=pnl(VBHB,
                         buttn(TOG_CACHE).cb(),
                         ChMsg.butShowComputedResults().t("Log messages of Blast"),
                         " ",
                         new ChButton("M").t("Mannually assign structures").li(this)
                         ),
            pLeft=pnl(VBHB,
                      pnl(FLOWLEFT, "Search homologous 3D-structures ", new ChButton("GO").t(ChButton.GO).li(this)),
                      pnlTogglOpts(pDetails),
                      pDetails
                      ),

            pMain=pnl(CNSEW, scrllpn(SCRLLPN_INHERIT_SIZE, _pList), dialogHead(this), pnl(REMAINING_VSPC1),  null, pnl(CNSEW,null,pLeft));
        adMainTab(pMain,this,null);
    }

    public void run() { actP("GO");}
    public void actionPerformed(java.awt.event.ActionEvent ev) { actP(ev.getActionCommand()); }
    private void actP(String cmd) {
        if (cmd=="M" || cmd=="GO") {
            Protein pp[]=_pList.selectedProteins().clone();
            for(int iP=pp.length; --iP>=0;) if (!_pList.isEnabled(pp[iP])) pp[iP]=null;
            pp=rmNullA(pp, Protein.class);
            if (pp.length==0) {
                error("Select at least one protein that has not been loaded from a 3D-file!");
                return;
            }
            if (cmd=="GO") {
                if (pp.length>30 && !ChMsg.yesNo("Are you sure to start the search for similar structures for that many proteins?")) return;
            }
            if (cmd!="M") {
                final int options=atoi(gcp(KEY_OPTIONS,this));
                pcp(KEY_OPTIONS,null,this);
                adTab(DISPOSE_CtrlW, "#"+ ++_count+"   ",  new DialogSimilarStructureResult(options,pp,this),  this);
            }
        }
    }
    public void handleEvent(StrapEvent e) {
        final int id=e.getType();
        if (0!=(id|StrapEvent.FLAG_ROW_HEADER_CHANGED)) ChDelay.repaint(this,99);
    }

   public static Object docuView() {
        final Protein p=new Protein(null);
        p.setName("HSLV_BACSU");
        final DialogSimilarStructureResult d=new DialogSimilarStructureResult(0, new Protein[]{p}, null);
        d.setPreferredSize(dim(700,150));
        return d;
    }
}
