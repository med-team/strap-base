package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.SPUtils.sp;

public class DialogAlignOneToAllResult extends javax.swing.table.DefaultTableModel implements Runnable, Disposable, ProcessEv {
    private final Protein _p, _pp[];
    private final ChPanel _progress=prgrssBar();
    private final Object _clazz;
    private boolean _disposed;
    private final EvAdapter LI=evAdapt(this);
    final ChButton
        _cbSortScore=toggl("sort score").li(LI),
        _cbSelect=toggl("Select matching residues in the alignment pane").li(LI),
        _cbShadeStru=toggl("Structure").li(LI).tt("Color residues according to secondary structure");
    final UniqueList<DialogAlignOneToAllAlignment> vALignments=new UniqueList(DialogAlignOneToAllAlignment.class);
    final ChJTable _jt=new ChJTable(this, ChJTable.DEFAULT_RENDERER);
    final float minMaxScore[]={Float.NaN,Float.NaN};
    private SequenceAligner _aligner;
    private int _font=14, _countSelect;
    Object _sharedInst;

    final DrawGappedSequence dgs=new DrawGappedSequence();
    private final ChPanel _panel=new ChPanel();
    public DialogAlignOneToAllResult(Protein pp[], Protein protein, ChCombo comboClass) {
        super(0,2);
        _panel.setFont(getFnt(_font,true,0));
        _pp=pp;
        _p=protein;
        updateOn(CHANGED_PROTEIN_AT_CURSOR,_jt);
        _jt.setShowVerticalLines(false);
        _jt.setRowHeight(99);
        _clazz=comboClass.getSelectedItem();
        _sharedInst=comboClass.instanceOfSelectedClass(null);
        /*
        boolean anyWithSecStr=protein.getResidueSecStrType()!=null;
        for(Protein p:pp) if (p.getResidueSecStrType()!=null) { anyWithSecStr=true; break;}
        */
        final Object
            sliderMatch=new ChSlider("SLIDER",-100,100,0).endLabels("All","High matching"),
            pSelection=pnl(
                           "#TBem Select high matching regions",
                           FLOWLEFT, sliderMatch,
                           new ChButton("NARROW").t("Narrow proteins to matching regions ...").li(LI),
                           new ChButton("REALIGN").t("Realign matching regions ...").li(LI)
                           ),
            pTop1=pnl(FLOWLEFT, _cbSortScore.cb(), " ",_cbSelect.doCollapse(pSelection).cb()),
            pTop=pnl(CNSEW, pTop1, null, pSelection, ChButton.doClose15(CLOSE_RM_CHILDS, this));

        pnl(_panel,CNSEW,scrllpn(_jt), pTop, _progress);
        startThrd(this,"DialogAlignOneToAllResult");
        pcp(KEY_PANEL, _panel, this);
        setW();

        addActLi(LI,sliderMatch);
        addActLi(LI, _jt);
        LI.addTo("w",_jt);
    }
    private void setW(){
        _jt.setColWidth(false, 0, _cbSelect.s()?ICON_HEIGHT:0);
        _jt.setColWidth(false, 0, _cbSelect.s()?ICON_HEIGHT:0);
        _jt.revalidate();
    }

    /* <<< Constructor  <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent  >>> */
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        final int id=ev.getID(), scaleUpDown=scaleUpDown(ev);
       if (scaleUpDown!=0) {
           if ((_font+=(scaleUpDown>0?1:-1))<4) _font=4;
           _panel.setFont(getFnt(_font,true,0));
           ChDelay.revalidate(_jt, 99);
       }
       if (id==ActionEvent.ACTION_PERFORMED) {
           boolean de=false;
           if (q==_cbSelect) {
               if (0==_countSelect++) _jt.addRowSelectionInterval(0, getRowCount()-1);
               if (_cbSelect.s()) addResidueSelections(true);
               else removeAllResidueSelections();
               de=true;
               setW();
           }
           if (q==_cbSortScore) {
               sortArry((List)vALignments);
               _jt.repaint();
           }
           if (cmd==ACTION_SELECTION_CHANGED) { addResidueSelections(false); de=true; }
           if (q==_cbShadeStru) _jt.repaint();
           if (cmd=="NARROW")  ResSelUtils.cropSequences(ResSelUtils.NARROW_IS_ALIGNMENT_MATCH, getSelectedAlignments());
           if (cmd=="REALIGN") ResSelUtils.openDialogRealign(vALignments.asArray());
           if (cmd=="SLIDER") {
               final int v=((ChSlider)q).getValue();
               final Object intObjct=intObjct(v);
               for(DialogAlignOneToAllAlignment a: vALignments.asArray()) {
                   a.run(a.RUN_SLIDER, intObjct);
                   de=true;
               }
           }
           if (de) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
       }
    }
    public DialogAlignOneToAllAlignment[] getSelectedAlignments() {
        final int ii[]=_jt.getSelectedRows();
        final DialogAlignOneToAllAlignment[] sel=new DialogAlignOneToAllAlignment[sze(ii)];
        for(int i=0; i<sze(ii);i++) {
            sel[i]=vALignments.get(ii[i]);
            if (sp(sel[i])==_p) sel[i]=null;
        }
        return rmNullA(sel,DialogAlignOneToAllAlignment.class);
    }
    private void removeAllResidueSelections() {
        for(DialogAlignOneToAllAlignment a:vALignments.asArray()) {
            final Protein p=sp(a);
            if (p!=null) p.removeResidueSelection(a);
        }
    }
    private void addResidueSelections(boolean all) {
        final DialogAlignOneToAllAlignment[] aa=vALignments.asArray(), aaSel=getSelectedAlignments();
        for(DialogAlignOneToAllAlignment a:aa) {
            final Protein p=sp(a);
            if (all || cntains(a,aaSel))  p.addResidueSelection(a);
            else p.removeResidueSelection(a);
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> ChRunnable >>> */
    public void run() {
        final Class c=clas(_clazz);
        if (c==null) return;

        minMaxScore[0]=Float.MAX_VALUE;
        minMaxScore[1]=Float.MIN_VALUE;
        final Object shared=StrapPlugins.instanceSA(_clazz,2,true);
        if (isInstncOf(SequenceAligner.class,shared)) {
            for(int i=0;i< _pp.length && !_disposed;i++) {
                _aligner= StrapPlugins.instanceSA(c,2,true);
                setSharedParas(shared,_aligner);
                final DialogAlignOneToAllAlignment a=new DialogAlignOneToAllAlignment(new Protein[]{_p, _pp[i]}, _aligner, sze(vALignments)+1, this);
                vALignments.add(a);
                inEDT(a);
                inEDTms(thrdCR(a, DialogAlignOneToAllAlignment.RUN_ENABLE),444);
                _progress.setProgress(i*100/_pp.length,_pp[i].getName()).repaint();
                ChDelay.revalidate(_jt, 99);
                sleep(5);
            }
            ChDelay.repaint(this,333);
            rmFromParent(_progress);
        }
    }
    /* <<< ChRunnable <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    @Override public void dispose() {
        removeAllResidueSelections();
        _disposed=true;
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,1);
    }
    /* <<< Disposable <<< */
    /* ---------------------------------------- */
    /* >>> TableModel >>> */
    @Override public String getColumnName(int c) {return "";}
    @Override public boolean isCellEditable(int row,int column) { return column>0;}
    @Override public void setValueAt(Object o,int row,int col) {}
    @Override public int getRowCount() { return sze(vALignments);}
    @Override public Object getValueAt(int row,int col) {
        final ResidueSelection a=vALignments.get(row);
        return a==null || col==1 ? a : sp(a)==_p ? "" :  iicon(IC_SHOW);
    }

}
