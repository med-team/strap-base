package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;

/**HELP
<h2>Sequence Features</h2>

Sequence features are annotations of sequence positions indicating for example signal peptides, disulfide bonds,
glycosylated or phosphorylated amino acids.  In
Strap they are loaded from the protein file, from BioDAS* servers, from Expasy or from the
Catalytic Site Atlas at the EBI.  They are underlined in the alignment
panel and in the alignment output.  They have a context menu
(right-click), a balloon message and they can be dragged to other proteins or 3D-views.

<h3>Downloading sequence features</h3>

The button "Load and underline sequence features"  loads a selection of sequence features.
For more control expand  "Options"  and open "BioDas".

<h3>The Table</h3>

The table lists the sequence features of all or of all selected proteins.  Each
table row gives the color, the type such as "Metal binding", and the number of features.

With check-boxes the user can control where the sequence feature is displayed.
The following explains the table columns in detail:

<ul>

  <li><b>Feature: </b> This column tells the type of sequence feature. Left click
    selects all features of this type.  In the alignment panel the
    selected sequence features will be indicated by WIKI:Marching_ants. Right
    click opens the context menu for the sequence features. Drag-and-drop copies all sequence features of this type to
    the target such as a 3D-structure.
  </li>

  <li><b>#: </b> The number of entries found. </li>

  <li><b>Navigation: </b> Jump to the first | next | previous | last selected residue in the alignment panel. </li>

</ul>

<h3>Colors</h3>

Each feature type has a color for black background (screen), and one for white background (paper).
These colors can be customized in the context menu.

<h3>Mouse-control</h3>

The following mouse actions  apply to the sequence features in the sequence feature table and in the alignment panel.
<ul>

  <li>Right click opens a context menu for the feature.</li>

  <li>Ctrl+left-click selects and deselects the sequence features.</li>

  <li>Dragging the mouse copies sequence features to other proteins or to 3D-views (See  WIKI:Drag_and_drop).</li>

  <li>Double clicking a feature opens a table with detailed data. The user can add annotation entries which are persistent.</li>

  <li>Moving the mouse provides a balloon text with information on the sequence feature under the mouse.</li>

</ul>

<h3>Deactivated Features</h3>

Deactivated features are usually hidden.
Reasons why features are deactivated:

<ul>
  <li>Poor alignment with the homologous sequence, where the feature is indirectly inferred from.</li>

  <li>The Sequence positions are outside the shown sequence fragment. This is often the case for
    N-terminal signal sequences in Pfam alignments showing only the conserved part.</li>

  <li>Secondary structure when the structure information is already obtained from the protein
    file.</li>

  <li>Phospho-tyrosine falling on a non-'Y' residue. This is controlled by the third column of the
    color table which contains a set of allowed letters or a dash if all letters are allowed. This table appears when
    opening the menu item Edit &gt; Color of the context menu.

</ul>

<h3>Examples</h3>
The following Swissprot entries have many sequence features and can be used for testing/demonstration:
SWISS:Q9Y4K3 &nbsp; SWISS:Q9H9D4 &nbsp;  SWISS:Q05516 &nbsp;  SWISS:P28825 &nbsp;  SWISS:P08045 &nbsp;  SWISS:Q13105
<br>
However, for many other protein files no sequence features is recorded yet.
@author Christoph Gille

ftp://ftp.psb.ugent.be/pub/ssb/so.rdf.gz

*/
public class SequenceFeatures extends javax.swing.table.DefaultTableModel implements Comparator,ChRunnable,ProcessEv, TooltipProvider {

    final static int
        MAX_DAYS=33,
        MASK_SRC=15<<8, SRC_CSA=1<<8, SRC_PROTEIN_FILE=2<<8, SRC_GFF_EXPASY=3<<8, SRC_DAS=4<<8,
        OPT_STOP_ON_CLOSED_FRAME=1<<15,
        OPT_ID_BY_HOMOLOGY=1<<16,
        OPT_ONLY_FIRST_UNIPROT=1<<17,
        OPT_BG=1<<18;
    static { assert 0==(MASK_SRC&FindUniprot.MASK); }
    final static String ALI_SCORE=" Alignment-Score=";
    private static String[]
        _featureNames=NO_STRING,
        _headers=("\n"+
                  "Feature\n"+
                  "#\n"+
                  ChButton.PFX_VERTICAL+"Sequence\n"+
                  ChButton.PFX_VERTICAL+"3D-backbone\n"+
                  ChButton.PFX_VERTICAL+"MS-Word, HTML\n"+
                  ChButton.PFX_VERTICAL+"Jalview-Export\n"+
                  ChButton.PFX_VERTICAL+"Web link\n"+
                  "Navigation").split("\n");
    private static BA _log, _logGeneralization;
    /* ---------------------------------------- */
    /* >>> Instance  >>> */

    private static SequenceFeatures _inst;
    public static SequenceFeatures instance() {if (_inst==null) _inst=new SequenceFeatures(); return _inst;}
    static ChButton getButton() { return _inst!=null ? _inst._butDefault : null; }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Table >>> */
    public SequenceFeatures() {
        super(_headers,0);
    }
    @Override public boolean isCellEditable(int row, int col){ return col>=COL_V0; }
    private ChButton _butWeb, _butWebEditor, _butExpasy, _butDefault;
    private static ChButton _butUP;
    static JComponent butFindUniprot() {
        if (_butUP==null) {
            _butUP=new ChButton()
                .li(evAdapt(instance())).t("Find UniProt IDs")
                .tt("Find UniProt identifiers of selected proteins by sequence identity or by PDB ID");
        }
        return _butUP;

    }
    private Protein[] proteins() { return onlySelected() ? StrapAlign.selectedProteins() : StrapAlign.visibleProteins(); }
    @Override public Object getValueAt(int row, int col){
        final String ft[]=_featureNames, f=ft.length>row ? ft[row] : "error";
        if (col==0) return new Object[]{ChRenderer.KEY_TOGGLE_SELECTED,screenColor(featureName2color(f))};
        if (col==1) return f;
        if (col==COL_WEB) {
            for(Protein p : proteins()) {
                for(ResidueAnnotation a :p.residueAnnotations()) {
                    if (a.featureName()!=null && a.lastValue(ResidueAnnotation.HYPERREFS)!=null) return _butWeb;
                }
            }
            return null;
        }

        if (col==COL_NAVI) {
            Color color=paperColor(featureName2color(f));
            if (color==null || color==C(0xFFffFF)) color=C(0);
            for(Component b : _pNavi.getComponents()) b.setForeground(color);
            return _pNavi;
        }
        final int iB=col-COL_V0;
        int count=0;
        for(Protein p : proteins()) {
            for(ResidueAnnotation a :p.residueAnnotations()) {
                if (a.featureName()!=f) continue;
                count++;
                if (col>=COL_V0) {
                    if ((ib2vw(iB) &  a.getVisibleWhere())!=0) return boolObjct(true);
                }
            }
        }
        return col==2 ? intObjct(count) :  boolObjct(false);
    }
    private final static int ib2vw(int F) {
        return
            F==WIRE   ? VisibleIn123.STRUCTURE :
            F==HTML   ? VisibleIn123.HTML :
            F==SEQ    ? VisibleIn123.SEQUENCE :
            F==JALVIEW? VisibleIn123.JALVIEW: 0;
    }
    private void featuresAtRow(int rows[], List v) {
        final String ft[]=_featureNames;
        for(Protein p : proteins()) {
            for(ResidueAnnotation a :p.residueAnnotations()) {
                final String fn=a.featureName();
                if (fn==null) continue;
                for(int r : rows) {
                    if (ft.length>r && r>=0 && fn==ft[r]) adNotNull(a,v);
                }
            }
        }
    }

    @Override public void setValueAt(Object o, int row, int col){
        final String ft[]=_featureNames;
        if (COL_V0<col && col<COL_V9 &&  row<ft.length) {
            final Protein pp[]=proteins();
            if (pp.length==0) StrapAlign.errorMsg("SequenceFeatures Error: no protein");
            final boolean on=isTrue(o);
            for(Protein p : pp) {
                for(ResidueAnnotation a : p.residueAnnotations()) {
                    if (ft[row]!=a.featureName()) continue;
                    if (on) a.setEnabled(true);
                    final int vw0=ib2vw(COL_V0-col);
                    for(int i=0;i<2;i++) {
                        final int vw=i==0 ?  vw0  : vw0==VisibleIn123.SEQUENCE ? VisibleIn123.SB : 0;
                        if (vw!=0) a.setVisibleWhere( on ? (a.getVisibleWhere()|vw) : (a.getVisibleWhere() &~ vw));
                    }
                }
            }
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR,111);
            if (col==SEQ+COL_V0) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,1111);
        }
    }
    static void updateTable(int delay) {
        final ChJTable t=_inst!=null ? _inst._jt : null;
        if (t!=null) {
            if (_inst.getRowCount()>0) setEnabld(true,t);
            ChDelay.revalidate(t,delay);
            ChDelay.repaint(t,delay);
        }
    }
    static void fChanged(boolean selected) {
        if (!selected || isSelctd(_inst==null?null:_inst._cbNotAll)) {
            Protein.incrementMC(ProteinMC.MCA_SEQUENCE_FEATURES_V,null);
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        }

    }

    private int _ttRow;
    private String _tt;
    public String provideTip(Object objOrEv) {
        final AWTEvent ev=deref(objOrEv, AWTEvent.class);
        final Point point=point(ev);
        if (point==null) return null;
        if (_jt.columnAtPoint(point)>1) return null;
        final int row=_jt.rowAtPoint(point);
        if (_ttRow!=row || _tt==null) {
            _ttRow=row;
            final BA sb=new BA(999);
            final Set setRemarks=new HashSet();
            final String[] nn=_featureNames;
            if (row>=0 && row<nn.length) {
                for(Protein p:proteins()) {
                    int isFirstA=-1;
                    setRemarks.clear();
                    for (ResidueAnnotation a:p.residueAnnotations()) {
                        if (a.featureName()!=nn[row] || !a.isEnabled() || a.getVisibleWhere()==0) continue;
                        if (isFirstA<0) { isFirstA=sze(sb); sb.a("<br>").a(p).a(' ',2); }
                        final int from=a.getSelectedAminoacidsOffset();
                        final int to=from+a.getSelectedAminoacids().length;
                        final String attribute=a.value("Remark");
                        sb.a(from);
                        if (to-from>1) sb.a('-').a(to-1);
                        if (sze(sb)-isFirstA>110) { sb.a(" ...."); break;}
                        if (sze(attribute)>3 && !setRemarks.contains(attribute)) {
                            adNotNull(attribute,setRemarks);
                            sb.a("<small>(").filter(FILTER_HTML_ENCODE,attribute).a(")</small>");
                        }
                        sb.a(' ',3);
                    }
                }
            }
            _tt=addHtmlTagsAsStrg(sb);
        }
        return _tt;
    }
    /* <<< Table <<< */
    /* ---------------------------------------- */
    /* >>> GUI >>> */
    private final static int
        ROW_HEIGHT=3*EX/2,
        SEQ=0, WIRE=1, HTML=2, JALVIEW=3,
        COL_V0=3, COL_V9=COL_V0+4, COL_WEB=COL_V9, COL_NAVI=COL_WEB+1;
    private JComponent _labMsg, _pnl, _pNavi;
    private ChJTable _jt;
    private ChFrame _frame;
    private ChTextArea _taWeb;
    private ChButton _cbVisibility, _cbNotAll, _cbArrows;
    private ChCombo _comboAll;
    private final ChPanel _prgBars[]=new ChPanel[9];
    private final ChRenderer _renderer=new ChRenderer();
    static boolean onlySelected() { return _inst!=null && isSelctd(_inst._cbNotAll);}
    public void showPanel() {
        if (_pnl==null) {
            _labMsg=new JLabel(" ");
            for(int i=9; --i>=0;) _prgBars[i]=prgrssBar();
            final EvAdapter LI=evAdapt(this);
            _cbVisibility=toggl("Specify where features of a certain type are shown / exported to").li(LI);
            _cbNotAll=toggl("Act not on all but only on selected proteins  ")
                .tt("Regarding only the selected proteins, when changing the check-boxes or navigating to the alignment positions with the  \"&gt;\"- and \"&lt;\" buttons in the table.<br><br>"+
                    "Select proteins by left-click the protein-label in the alignment.").li(LI);
            _cbArrows=toggl("Show only arrow heads for  long underlinings").s(true).li(LI);
            _pNavi=ChButton.navigationPreviousNext(LI,"Go to * selected residue");
            _butWebEditor=new ChButton(ChButton.NO_FILL|ChButton.NO_BORDER,null).i(IC_WWW16).li(LI);
            _butWeb=new ChButton(ChButton.NO_FILL|ChButton.NO_BORDER|ChButton.ICON_SIZE,null).i(IC_WWW16).cp(ChRenderer.KEY_EDITOR_COMPONENT,_butWebEditor);

            final String
                tipS="Left-Click to select the sequence features.<br>Selected sequence features are indicated in the alignment pane by marching ants.<br>Right-click for context menu.",
                tips[]=
                    ("The color of sequence features with the respective name<br>"+tipS+"\n"+
                 "Feature name<br>"+tipS+"\n"+
                 "Number of sequence features with the respective feature name\n"+
                 "Enable underlining in alingment pane.\n"+
                 "Enable highlighting in 3D-Backbone\n"+
                 "Enable export to MS-Word and Html\n"+
                 "Enable export to Jalview\n"+
                 "List of Web addresses where the information has been loaded from\n"+
                 "Jump to first,previous, next or last amino acid").split("\n");

            _jt=new ChJTable(this, ChJTable.NO_REORDER|ChJTable.EXX_ROW_HEIGHT).headerTip(tips);
            if (getRowCount()==0) {
                setEnabld(false, _jt);
                pcp(KOPT_NOT_PAINTED_IF_DISABLED,"",_jt);
            }

            final Comparator[] comparators=new Comparator[COL_WEB+1];
            for(int i=comparators.length; --i>=0;) comparators[i]=_jt;
               ChJTable.setRowSorter(comparators,_jt);

            _jt.setDefaultRenderer(Object.class,_renderer);
            _jt.getTableHeader().setDefaultRenderer(_renderer);
            _jt.setDefaultEditor(Object.class,_renderer);
            _comboAll=new ChCombo("Show those and only those features that are activated","Show all features, including those with poor alignment","Hide all features").li(LI)
                .tt("Features with poor alignment to the reference sequence are deactivated but can be activated by the user");
            _butExpasy=new ChButton("Expasy").li(LI)
                .tt("GFF files are loaded from Expasy. These files contain lines of the form"+
                    "<pre class=\"data\">P25787	UniProtKB	Modified residue	24	24	.	.	.	Note=Phosphotyrosine</pre>"+
                    "As long as one job is runnting the button remains deactivated.<br>"+
                    "The process can be interrupt by closing this frame."
                    );
            _butDefault=new ChButton("Load and underline sequence features of "+StrapAlign.LABEL_ALL_OR_SELECTED_P).i(IC_UNDERLINE).li(LI)
                .tt("The sequence features such as \"Phosphorylated Tyrosine\" will be loaded from servers and underlined in the alignment panel.");
            StrapAlign.setNumOfProteins(_butDefault);
            final Object
                hb=pnl(Customize.newButton(Customize.seqFeatureColors).setOptions(ChButton.NO_FILL|ChButton.NO_BORDER|ChButton.ICON_SIZE).rover(IC_CUSTOM), panHS(SequenceFeatures.class)),
                panAboveT=pnl(KOPT_NOT_PAINTED_IF_DISABLED, _cbNotAll.cb(), _comboAll),
                pTop=pnl(CNSEW,pnl("<h2>Sequence Features</h2>"),null, panAboveT, hb),
                b1=new ChButton("FROM_FILE").li(LI).t("Protein files")
                .tt("Some protein files contain sequence features lines like <pre class=\"data\">FT   METAL       141    141       Copper.\n\nSITE     1 CAT  3 ASP A  32  HIS A  64  SER A 221\n\nMODRES 1IVO ASN A   32  ASN  GLYCOSYLATION</pre>"),
                b3=new ChButton("FROM_DAS").li(LI).t("BioDAS..."),
                pOtherViews=pnl(VB,
                                pnl(monospc("Spice-Browser* on selected protein"),new ChButton("SPICE").li(LI).t(ChButton.GO)),
                                pnl(monospc("Dasty on selected protein"),new ChButton("DASTY").li(LI).t(ChButton.GO)),
                                WATCH_MOVIE+MOVIE_Sequence_Features_in_3D
                                ),
                pLoadFrom=pnl(VBHB,pnl(FLOWLEFT, "Load from ",b1, _butExpasy, b3),
                              " ",
                              "What if a sequence feature goes over many residues: ",
                              _cbArrows.cb(),
                              " ",
                              "What if there is e.g. \"Modified_residue\" and \"Phosphotyrosine\" at the same position: ",
                              buttn(TOG_COMBINE_SEQ_FEATURES).cb(),
                              " ",
                              _cbVisibility.cb()
                              ),
                pAdvanced=pnl(VBHB,
                              "#JS",
                              pLoadFrom,
                              pOtherViews,
                              "#JS",
                              pnl(HBL,toggl().doCollapse(pOtherViews).doCollapse(true,pLoadFrom), "Other feature views")
                             ),
                bCust=Customize.newButton(Customize.preferedDasSources).rover(IC_CUSTOM),
                pSouth0=pnl(VBHB, pnl(HB, _butDefault,"  ", bCust, "#", toggl().doCollapse(pAdvanced),"Options"), pAdvanced),
                pSouth=pnl(CNSEW,pSouth0, pnl(VB,_prgBars), enlargeFont(_labMsg, 0.7));
            for(JComponent c : _prgBars) setEnabld(false, c);
            pcp(KEY_ENABLED_IF_ENABLED,wref(_jt), panAboveT);
            _pnl=pnl(CNSEW,scrllpn(0,_jt, dim(EX,14*EX)),pTop,pSouth);
            setW();
            addMoli(MOLI_REPAINT_ON_ENTER_AND_EXIT,_labMsg);
            setSettings(instance(), Customize.customize(Customize.seqFeatureColors));
            addActLi(LI, Customize.customize(Customize.seqFeatureColors));
            addActLi(LI, _jt);
            LI.addTo("Mkm",_jt);
            rtt(_jt);
            _frame=ChFrame.frame("Sequence Features",_pnl, ChFrame.ALWAYS_ON_TOP|ChFrame.PACK);
            startThrd(thrdCR(this,"REG"));
        }
        _frame.shw(ChFrame.CENTER|CLOSE_CtrlW);
    }

    private void setW() {
        final boolean details=_cbVisibility.s();
        int headerH=EX;
        for(int col=COL_NAVI+1;--col>=0;) {
            if (col==1) continue;
            final int w=
                col>=COL_V0 && col<COL_V9 || col==COL_WEB ? (details ? EX*2 : 0) :
                col==2 ? 5*EM :
                col==COL_NAVI ? prefW(_pNavi) :
                EX*2;
            for(int j=2; --j>=0;) _jt.setColWidth(false, col, w);
            if (details) headerH=maxi(headerH,prefH(labl(_headers[col])));
        }
        _jt.getColumnModel().setColumnMargin(details?1:0);
        _jt.getTableHeader().setPreferredSize(dim(1,headerH+EX));
    }

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Sort >>> */
    public int compare(Object o1, Object o2) {
        if (o1 instanceof ResidueAnnotation && o2 instanceof ResidueAnnotation) {
            final ResidueAnnotation a1=(ResidueAnnotation)o1, a2=(ResidueAnnotation)o2;
            final String f1=a1.featureName(), f2=a2.featureName();
            if (f1!=null && f2!=null) {
                final int diffLen=a1.getSelectedAminoacids().length-a2.getSelectedAminoacids().length;
                if (diffLen!=0) return -diffLen;
                final int diffOff=a1.getSelectedAminoacidsOffset()-a2.getSelectedAminoacidsOffset();
                if (diffOff!=0) return -diffOff;
                final int diffName=f1.compareTo(f2);
                if (diffName!=0) return diffName;
            }
        }
        return 0;
    }
    /* <<< Sort <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    private int _row;
    private final Map<String,NextAndPrevSelection> mapNextPrev=new HashMap();
    private final static Object PROGRESS[]={null};
    public void processEv(AWTEvent ev) {
        final int id=ev.getID(), modi=modifrs(ev);
        final String cmd=actionCommand(ev);
        final Object q=ev.getSource();
        final Protein ppSel[]=StrapAlign.selectedProteins();
        final StrapView view=StrapAlign.alignmentPanel();
        final List<ResidueAnnotation> vA=vClr(PROGRESS);
        if (q==_cbVisibility) setW();

        if (q==_butUP) {
            setEnabld(false,_butUP);
            startThrd(thrdCR(this,"FIND_UP", ppSel.length>0?ppSel:StrapAlign.proteins()));
        }
        if (cmd=="<" || cmd==">" || cmd=="|<" || cmd==">|") {
            final int row=ChJTable.rowIndexToModel(_renderer.editedRow(),_jt);
            final String name=getS(row,_featureNames);
            if (name!=null) {
                NextAndPrevSelection nextPrev=mapNextPrev.get(name);
                if (nextPrev==null) mapNextPrev.put(name,nextPrev=new NextAndPrevSelection());
                vA.clear();
                featuresAtRow(new int[]{row}, vA);
                final ResidueSelection[]
                    ss=toArry(vA,ResidueAnnotation.class),
                    ss2=ResSelUtils.filter(VisibleIn123.SEQUENCE,ss, _cbNotAll.s()?ppSel : null);
                nextPrev.goNextMatch(cmd,ss2);
            }
        }
        if (q!=null && q==_jt && cmd==null) {
            final int row=y(ev)/ROW_HEIGHT;
            if (isPopupTrggr(false,ev)) {
                if (isPopupTrggr(true,ev) && 0<=row && row<getRowCount()) {
                    final int selected[]=ChJTable.selectedRowsConverted(_jt);
                    featuresAtRow(idxOf(row, selected,0,MAX_INT)>=0 ? selected : new int[]{row} ,(List)vA);
                    StrapAlign.showContextMenu('F', toArry(vA,ResidueAnnotation.class));
                }
                return;
            }
            if (id==MOUSE_MOVED && view!=null && _row!=row) {
                featuresAtRow(new int[]{row},vA);
                final ResidueAnnotation ss[]=toArry(vA,ResidueAnnotation.class), s0=get(0,ss,ResidueAnnotation.class);
                view.highlightProteins(ss, s0==null?null: s0.getColor());
                _row=row;
            }
            if (id==MOUSE_DRAGGED && !isCellEditable(row, _jt.columnAtPoint(point(ev)))) {
                featuresAtRow(new int[]{row},dndV(true,_jt));
                exportDrg(_jt, ev, TransferHandler.COPY);
            }
        }
        if (id==MOUSE_ENTERED || id==MOUSE_EXITED) {
            _row=-1;
            if (view!=null) view.highlightProteins(null,null);
        }
        if (cmd!=null) {
            StrapAlign.errorMsg("");
            if (cmd=="SPICE" || cmd=="DASTY") {
                if (ppSel.length!=1) error("For Spice-Browser* or Dusty please select exactly one protein");
                else startThrd(thrdCR(this,"DASTY",new Object[]{cmd,ppSel[0],intObjct(modi)}));
            }
            if (q==_jt && cmd==ACTION_SELECTION_CHANGED) {
                StrapAlign.selectedObjectsV().clear();
                featuresAtRow(ChJTable.selectedRowsConverted(_jt), StrapAlign.selectedObjectsV());
                StrapEvent.dispatchLater(StrapEvent.OBJECTS_SELECTED,11);
            }
            if (q==_frame && cmd==ACTION_WINDOW_CLOSING) setEnabld(true,_butExpasy);
            if (q==_cbNotAll || q==_comboAll || q==_cbArrows) {
                if (q==_cbArrows) ResSelUtils.setMaxUnderlining(_cbArrows.s() ?6:99999);
                if (q==_comboAll) {
                    final int activate=_comboAll.i(), ALL=1, NONE=2;
                    for(Protein p : StrapAlign.proteins()) {
                        ResidueAnnotation.enableAllFeatures(activate==ALL ? 1 : activate==NONE ? -1 : 0);
                    }
                }
                fChanged(false);
                //Protein.incrementMC(ProteinMC.MCA_SEQUENCE_FEATURES_V,null);
                //StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
            }
            final boolean fromFile=cmd=="FROM_FILE" || q==_butDefault;
            if (fromFile || q==_butExpasy || q==_butDefault) {
                final Protein pp[]=ppSel.length>0 ? ppSel: StrapAlign.visibleProteins();
                final String[] dasTitle=null;
                setEnabld(false,q);
                if (q==_butExpasy  || q==_butDefault) {
                    for(int blaster=FindUniprot.MAX_BLASTER+1; --blaster>=0;) multiPass(OPT_BG|SRC_GFF_EXPASY|blaster, dasTitle,  pp);
                }
                if (q==_butDefault)  {
                    multiPass(OPT_BG|SRC_CSA, dasTitle,  pp);
                    for(int blaster=FindUniprot.MAX_BLASTER+1; --blaster>=0;) multiPass(OPT_BG|SRC_DAS|blaster, dasTitle,  pp);
                }
                if (fromFile) startThrd(thrdCR(this,"VAR", ExpasyGff.readFeaturesInProteinFile(null,MAX_INT, pp) ));
            }
            if (cmd=="FROM_DAS") {
                setAOT(false,_frame);
                StrapDAS.show(true);
            }
            if (fromFile || q==_butExpasy || q==_butDefault || cmd=="FROM_DAS") {
                setEnabld(true,_jt);
                _pnl.repaint();
            }
            if (q==Customize.customize(Customize.seqFeatureColors)) {
                mapFeatureColors.clear();
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR,999);
                ChDelay.repaint(this,999);
            }
            if (q==_butWebEditor) {
                setAOT(false,_frame);
                final int row=ChJTable.rowIndexToModel(_renderer.editedRow(),_jt);
                final String[] nn=_featureNames;
                if (row<0 || row>=nn.length) return;
                if (_taWeb==null) _taWeb=new ChTextArea("");
                final Set vURLs=new HashSet();
                final BA sb=new BA(999).aln(nn[row]);
                final Protein pp[]=proteins();
                final int max=sze(toStrg(longestName(pp)));
                for(Protein p:pp) {
                    nextFeature:
                    for (ResidueAnnotation a:p.residueAnnotations()) {
                        if (a.featureName()!=nn[row] || !a.isEnabled() || a.getVisibleWhere()==0) continue;
                        final int L=sze(p.getName());
                        int count=0;
                        for(ResidueAnnotation.Entry e : a.entries()) {
                            if (ResidueAnnotation.HYPERREFS!=e.key()) continue;
                            final String url=e.value();
                            if (0==count++) {
                                if (vURLs.contains(url)) continue nextFeature;
                                adNotNull(url,vURLs);
                                sb.a(' ',max-L+1).a(SPUtils.sp(a)).a(' ');
                            }
                            sb.a(' ').a(url);
                        }
                        if (count>0) sb.a('\n');
                    }
                }
                _taWeb.t(sb).tools().showInFrame(ChFrame.AT_CLICK|ChFrame.PACK|ChFrame.SCROLLPANE,"Feature URLs").underlineRefs(0);

            }
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Color >>> */
    private final static Map<String,FeatureColor> mapFeatureColors=new HashMap();
    static class FeatureColor {
        String name,nameUS,  screen, paper, rule="-";
        Color cS, cP;
        FeatureColor(String n) { name=n;}
    }
    private static Color screenColor(FeatureColor fc) {
        if (fc.cS==null) {
            final long rgb=hexToInt(fc.screen);
            fc.cS=rgb>=0 ? new Color((int)rgb) : C(0xFFffFF);
        }
        return fc.cS;
    }
    private static Color paperColor(FeatureColor fc) {
        if (fc.cP==null) {
            final long rgb=hexToInt(fc.paper);
            fc.cP=rgb>=0 ? new Color((int)rgb) : C(0x808080);
        }
        return fc.cP;
    }
    private static String nameUS(FeatureColor fc) {
        if (fc.nameUS==null && fc.name!=null) fc.nameUS=fc.name.replace(' ','_').replace('-','_');
        return fc.nameUS;
    }
    public static FeatureColor featureName2color(String f) {
        parseColors();
        FeatureColor fc=mapFeatureColors.get(f);
        if (fc==null) {
            mapFeatureColors.put(f,fc=new FeatureColor(f));
            mapFeatureColors.put(nameUS(fc),fc);
        }
        return fc;
    }
    private static void parseColors() {
        if (sze(mapFeatureColors)==0) {
            for(String s : custSettings(Customize.seqFeatureColors)) {
                final String tt[]=splitTokns(s);
                if (tt.length<3) continue;
                final String fn=tt[0].replace('_',' ');
                if (chrAt(0, fn)=='#') continue;
                final String screen=tt[1];
                if (chrAt(0,screen)!='#') continue;
                final FeatureColor fc=new FeatureColor(fn);
                fc.screen=screen;
                fc.paper=tt[2];
                if (chrAt(0,fc.paper)!='#') continue;
                fc.rule=orS(toStrgIntrn(get(3,tt)), "-");
                mapFeatureColors.put(fn,fc);
                mapFeatureColors.put(nameUS(fc),fc);
            }
        }
    }
    static void setFN(String n, int src, ResidueAnnotation a) {
        if (a!=null && n!=null) {
            a.setFeatureName(n,src);
            final FeatureColor featureC=featureName2color(n);
            a.setColor(screenColor(featureC));
            a.setFgForWhiteBG(paperColor(featureC));
            a.addE(0,ResidueAnnotation.NAME,n);
        }
    }
    /* <<< Color <<< */
    /* ---------------------------------------- */

    private final static Set<String> _vNames=new HashSet();
    private static int _mcNames;
    public static void updateNames() {
        final int mc=Protein.MC_GLOBAL[ProteinMC.MCA_SEQUENCE_FEATURES_V];
        if (_mcNames!=mc) {
            _mcNames=mc;
            _vNames.clear();
            for(Protein p : StrapAlign.visibleProteins()) {
                final ResidueSelection[] ss=p.allResidueSelections();
                for(ResidueSelection s : ss) {
                    if (ResSelUtils.type(s)=='F' && ResSelUtils.isSelVisible(VisibleIn123.SEQUENCE,s)) {
                        adNotNull(((ResidueAnnotation)s).featureName(), _vNames);
                    }
                }
            }
            _featureNames=strgArry(_vNames);
            instance().setNumRows(_featureNames.length);
            updateTable(33);

        }
    }

    /* <<< Layout Underlining <<< */
    /* ---------------------------------------- */
    /* >>> Threads  >>> */
    private static int _countThread;
    public static void multiPass(int opts, String[] dasTitle, Protein pp0[]) {
        if (0!=(opts&OPT_BG)) {
            startThrd(thrdM("multiPass", SequenceFeatures.class, new Object[]{intObjct(opts&~OPT_BG), dasTitle, pp0}));
            return;
        }
        _countThread++;
        final int type=opts&MASK_SRC;
        if (type==SRC_CSA) {
            CsaWeb.load( pp0, instance());
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED, 555);
        } else {
            for(int onlyFirst=2; --onlyFirst>=0;) {
                final Protein[] pp=pp0.clone();
                shuffl(pp);
                final int options=opts|OPT_ID_BY_HOMOLOGY|(onlyFirst!=0 ? OPT_ONLY_FIRST_UNIPROT : 0);
                if (type==SRC_DAS) StrapDAS.load(options, dasTitle!=null?dasTitle: custSettings(Customize.preferedDasSources), pp, instance());
                if (type==SRC_GFF_EXPASY) ExpasyGff.load(options, pp, instance());
                StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_ADDED, 555);
            }
        }
        if (0==--_countThread) {
            setEnabld(true, instance()._butDefault);
            setEnabld(true, instance()._butExpasy);
        }
    }
    public static BA progressText(int opt, BA sb) {
        final int blaster=opt&FindUniprot.BLASTER_MASK;
        sb.clr();
        if (blaster!=0) sb.a("Use ").a(blaster==FindUniprot.BLAST_EBI ? "EBI":"NCBI").a(" blast to get ID. ");
        final int src=opt & MASK_SRC;
        sb.a(src==SRC_DAS ? " DAS " : src==SRC_GFF_EXPASY ? " GFF expasy ": "");
        return sb;
    }
    /* <<< Threads <<< */
    /* ---------------------------------------- */
    /* >>> Threading  >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id=="REG") DAS.readRegistry((ChRunnable)this);
        if (id=="DASTY") {
            final String cmd=(String)argv[0];
            final Protein p=(Protein)argv[1];
            final int modi=atoi(argv[2]);
            if (cmd=="SPICE") toSpiceViewer(p);
            if (cmd=="DASTY") {
                String uniprot=p.getUniprotID();
                if (uniprot==null) uniprot=getS(0,FindUniprot.load1(0, p,logR()));
                if (uniprot==null) error("No uniprot ID associated with "+p);
                else visitURL(Hyperrefs.toUrlString("DASTY:"+uniprot.substring(uniprot.indexOf(':')+1),Hyperrefs.WEB_LINK), modi);
            }
        }
        if (id=="FIND_UP") {
            final Protein pp[]=((Protein[])arg).clone();
            FindUniprot.load(FindUniprot.FETCH_AFTER, pp, logR());
            final String title="Assigned uniprot IDs";
            final BA sb=new BA(999).aln(title);
            for(Protein p:pp) {
                if (p!=null) sb.a(p).a("  acc=").a(p.getUniprotID()).a("  by-sequence=").join(FindUniprot.load1(0, p,logR())," ").a('\n');
            }
            shwTxtInW(title,sb);
            setEnabld(true,_butUP);
        }

        if (id==ChRunnable.RUN_SET_PROGRESS && _pnl!=null) {
            final Object src=argv[PRGRSS_SRC], txt=argv[PRGRSS_TXT];
            final int blast=strstr(STRSTR_w,"blast",txt)<0 ? 0 : strstr(STRSTR_w,"EBI",txt)>=0 ? 1 : 2 ;
            final int iB=
                src==CsaWeb.class ? 0 :
                src==ExpasyGff.class ? 1+blast :
                src==StrapDAS.class ? 4+blast :
                -1;
            if (iB>=0) {
                prgrss2bar(_prgBars[iB],argv);
                setEnabld(true, _prgBars[iB]);
            }
        }
        if (id==ChRunnable.RUN_APPEND) drawMsg(toStrg(arg), _labMsg);
        if (id=="VAR") {
            BA ba=null;
            final ResidueAnnotation aa[]=(ResidueAnnotation[])arg;
            for(ResidueAnnotation a : aa ) {
                for(ResidueAnnotation.Entry e : a.entries()) {
                    if ("Remark"!=e.key() || !e.value().startsWith("FTId=VAR")) continue;
                    final String u=Hyperrefs.toUrlString(e.value(),Hyperrefs.WEB_LINK);
                    final java.io.File f=urlGet(url(u), CacheResult.isEnabled()?MAX_DAYS:0);
                    ba=readBytes(f,ba);
                    if (ba!=null) {
                        int i=strstr("<td>Disease",ba);
                        if (i<0) i=strstr("<td>Polymorphism",ba);
                        if (i<0) i=strstr("<td>Unclassified",ba);
                        if (i>0) {
                            final String type=ChUtils.wordAt(i+4, (CharSequence)ba, LETTR).toUpperCase();
                            final char c=chrAt(0,type);
                            setFN(c=='U' ? type+"_"+type : type, SRC_PROTEIN_FILE, a);
                            if (idxOfStrg(type,_featureNames)<0) _featureNames=adToStrgs(type,_featureNames,0);
                            updateTable(999);
                        }
                        final int from=strstr(STRSTR_AFTER,"<td><b>From ",ba);
                        if (from>0) {
                            final int colon=strchr(',',ba,from, mini(ba.length(),from+30));
                            if (colon>0) a.setValue(0,ResidueAnnotation.NAME,wordAt(colon+2,ba,LETTR_DIGT));
                        }
                    }
                }
            }
        }
        return null;
    }

    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>>  Score >>> */
    static void keepFeaturesWithBestScore(Protein p) {
        final ResidueAnnotation[] aa=p.residueAnnotations();
        for(int i0=aa.length; --i0>=0;) {
            final ResidueAnnotation a0=aa[i0];
            final String pos0=a0==null?null:a0.value(ResidueAnnotation.POS);
            if (pos0==null) continue;
            final float score0=a0.featureAligScore();
            final String name0=a0.featureName();
            int from0=MIN_INT, to0=MIN_INT;
            final int where0=a0.whereFeatureLoadedFrom();
            for(int i=aa.length; --i>=0;) {
                final ResidueAnnotation a=aa[i];
                if (a==null || a==a0 || pos0==null || a.featureName()!=name0 ) continue;
                final int where=a.whereFeatureLoadedFrom();
                final float score=a.featureAligScore();
                if (where0==where && score0<=score) continue;
                final String pos=a.value(ResidueAnnotation.POS);
                final boolean posEquals=pos0.equals(pos);
                boolean overlaps;
                if (posEquals) overlaps=true;
                else {
                    final int from=a.getSelectedAminoacidsOffset(), to=from+lstTrue(a.getSelectedAminoacids());
                    if (from0==MIN_INT) {
                        from0=a0.getSelectedAminoacidsOffset();
                        to0=from0+lstTrue(a0.getSelectedAminoacids());
                    }
                    overlaps=
                        from==from0 && to==to0 ||
                        from0<=from && from<to0 || from0<=to && to<=to0 ||  from<=from0 && from0<=to   || from<=to0 && to0<=to;
                }
                if (overlaps) {
                    log().a("   DELETE: ").a(name0).a(' ').a(score).a("  [").a(pos).a("]   BECAUSE  OF ").a(score0).a("  [").a(pos0).aln(']');
                    p.removeResidueSelection(a);
                    adNotNull(a.srcText(), p.vInvalidFeatureLine());
                    aa[i]=null;
                }
            }
        }
        log().send();
    }
    /* <<< Score <<< */
    /* ---------------------------------------- */
    /* >>> Translations >>> */
    static void addFeature(ResidueAnnotation a, Protein p, String why) {
        if (a==null || p==null) return;
        final String fn=a.featureName();
        if (fn=="Sequence_conflict" || fn=="mature_protein_region") why="Unspecific";
        if (p.getResidueSecStrType()!=null && ("Turn"==fn || "Helix"==fn || "Beta_strand"==fn)) why="Secondary structure already known";
        if (countTrue(a.getSelectedAminoacids())==0) why="No aminos selected";
        if (why!=null) a.addE(0,ResidueAnnotation.DISABLED, "Why disabled: "+why);
        java.io.File fAnno=a.featureFile(dirWorking());
        synchronized(mkIdObjct("SF$$af",p)) {
            a.parse(readBytes(sze(fAnno)>0?fAnno:a.featureFile(null)),0,MAX_INT);
            final ResidueAnnotation s=(ResidueAnnotation) orO(isSelctd(BUTTN[TOG_COMBINE_SEQ_FEATURES]) ? mergeSimilar(a,p) : a, a);
            if (s!=null) {
                if (countTrue(s.getSelectedAminoacids())>5) s.setVisibleWhere(s.getVisibleWhere()&~VisibleIn123.STRUCTURE);
                p.addResidueSelection(s);
            }
        }
    }

    private static BA _baRelations;
    private static BA txtRelations() {
        BA ba=_baRelations;
        if (ba==null) {
            ba=readBytes(rscAsStream(MAP_VALUES_AS_INTERN, _CCS,"SequenceFeaturesRelations.rsc"));
            if (ba==null) { ba=new BA(0); assrt();}
            _baRelations=ba;
        }
        return ba;

    }

    private static Map<String,String[]> _mapRelations;
    public static ResidueAnnotation mergeSimilar(ResidueAnnotation a, Protein p) {
        final String fn=a==null?null:a.featureName();
        if (fn==null||p==null) return null;
        Map<String,String[]> map=_mapRelations;
        if (map==null) mapKey2Array(MAP_VALUES_AS_INTERN, txtRelations(), map=_mapRelations=new HashMap());
        final String pos=a.value(ResidueAnnotation.POS);
        for(ResidueSelection s2 : p.allResidueSelections()) {
            final ResidueAnnotation a2=s2 instanceof ResidueAnnotation ? (ResidueAnnotation)s2 : null;
            final String fn2=a2==null?null:a2.featureName();
            if (fn2==null) continue;
            final String pos2=a2.value(ResidueAnnotation.POS);
            final boolean sameColon= (pos.indexOf(':')>0)==(pos2.indexOf(':')>0);
            if (!(sameColon ? pos.equals(a2.value(ResidueAnnotation.POS)) : ResSelUtils.equalAminoPositions(a,a2))) continue;
            if (a2.containsAllEntriesOf(a)) return a2;
            if (a.containsAllEntriesOf(a2)) {
                p.removeResidueSelection(a2);
                return a;
            }
            if (fn2==fn || isGeneralizationOf(a.featureName(), a2)) {
                a2.includeFeature(a);
                return a2;
            }
            if (isGeneralizationOf(fn2, a)) {
                a.includeFeature(a2);
                p.removeResidueSelection(a2);
                return a;
            }
            if (a2.isEnabled() && a.isEnabled() && reportG(fn) && reportG(fn2)) {
                final boolean lt=fn.compareTo(fn2)<0;
                adNotNull(new StrgArray(lt?fn:fn2, lt?fn2:fn), _vNoGeneralization);
            }
        }
        return null;
    }

    private static boolean isGeneralizationOf(String fnGeneral,  ResidueAnnotation aSpecific) {
        final String fnSpecific=aSpecific.featureName();
        if (fnSpecific.length()>fnGeneral.length() && strstr(STRSTR_IC, fnGeneral,fnSpecific)>=0) return true;
        Map<String,String[]> map=_mapRelations;
        if (map==null) mapKey2Array(MAP_VALUES_AS_INTERN, txtRelations(), map=_mapRelations=new HashMap());
        final String ssSpecific[]=map.get(fnGeneral);
        boolean ret=false;
        if (ssSpecific!=null) {
            for(String s : ssSpecific) {
                final int L=s==null?0:s.length();
                if (s==null) continue;
                if (s==fnSpecific) { ret=true; break; }
                final char c0=s.charAt(0), c9=s.charAt(L-1);
                if (c0=='*' && c9=='*') {
                    if (L==1) { ret=true; break; }
                    if (strstr(0, s,1,L-1,  fnSpecific,0,MAX_INT)>=0) { ret=true; break; }
                } else if (c0=='*') {
                    if (strEquls(0, s, 1, L, fnSpecific, fnSpecific.length()-L+1)) { ret=true; break; }
                } else if (c9=='*') {
                    if (strEquls(0, s, 0, L-1, fnSpecific, 0)) { ret=true; break; }
                }
            }
        }
        ret=ret || (fnGeneral=="Post_translational_modification" || fnGeneral=="Modified_residue") && aSpecific.featureEndsWithAminoName();
        if (ret && adNotNull(new StrgArray(fnGeneral, fnSpecific), _vGeneralization)) {
            if (_logGeneralization==null) toLogMenu(_logGeneralization=new BA(9999), "Similar Sequence Features", IC_UNDERLINE);
            _logGeneralization.a("Drop ").a(fnGeneral).a(" but keep ").aln(fnSpecific);
        }
        return ret;
    }

    private static boolean reportG(String s) {
        return s!=null && s!="Turn" && s!="Helix" && s!="Beta_strand" && s!="Active_site";
    }

    final static Collection<StrgArray> _vGeneralization=new HashSet(), _vNoGeneralization=new HashSet();
    public final static Map<String,String> _lCase=new HashMap();
    private static Map<String,String> _mapSyn;
    public static String mapSynonyms(String fn, boolean found[]) {
        if (_mapSyn==null) {
            final BA ba=readBytes(rscAsStream(MAP_VALUES_AS_INTERN, _CCS,"SequenceFeaturesSyn.rsc"));
            mapColumns012345to9(MAP_KEYS_LETTR_DIGT_LC|MAP_VALUES_AS_INTERN,ba, _mapSyn=new HashMap());
            if (sze(ba)<99) assrt();
            final BA txt=txtRelations();
            final ChTokenizer tok=new ChTokenizer().setText(txt);
            while(tok.nextToken()) {
                final String s=tok.asString();
                _mapSyn.put(lCaseOnlyLettersDigits(s, _lCase),s);
            }
        }
        final String lc=lCaseOnlyLettersDigits(fn,_lCase);
        String ret=null, aa;
        if (lc.indexOf("phosphorylated")>=0 && (lc.indexOf(aa="tyrosine")>=0 || lc.indexOf(aa="threonine")>=0 || lc.indexOf(aa="serine")>=0)) {
            ret="Phospho"+aa;
        }
        if (ret==null && (lc.indexOf("glycosylat")>=0 || lc.indexOf("glycosolat")>=0) ) ret="Glycosylation";
        if (ret==null) ret=_mapSyn.get(lc);
        if (ret!=null && found!=null) found[0]=true;
        return orS(ret,fn);
    }

    /* <<< Translations <<< */
    /* ---------------------------------------- */
    /* >>> Spice >>> */
    public static void toSpiceViewer(Protein p) {
        if (p==null) return;
        final BA sb=new BA(99).a("http://www.dasregistry.org/runspice.jsp?");
        final String pdb=p.getPdbID();
        String uniprot=p.getUniprotID();
        if (uniprot==null) uniprot=getS(0,FindUniprot.load1(0, p, instance()));
        if (pdb!=null) sb.a("pdb=").a(pdb,pdb.indexOf(':')+1,MAX_INT);
        else if (uniprot!=null) sb.a("uniprot=").a(uniprot,uniprot.indexOf(':')+1,1);
        else {
            error("Error: cannot run Spice on "+p+"\nbecause it has neither a PDB ID nor a Uniprot ID");
            return;
        }
        visitURL(sb.a(VISIT_URL_JAVAWS),0);
    }
    /* <<< Spice <<< */
    /* ---------------------------------------- */
    public static ChRunnable logR() { return StrapDAS.logR(); }
    public static BA log() {
        if (_log==null) {
            _log=new BA(99);
            if (withGui()) _log.sendToLogger(0, "Sequence Features",IC_UNDERLINE, 99*1000);
            else _log.setSendTo(BA.SEND_TO_STDOUT);
        }
        return _log;
    }
}
