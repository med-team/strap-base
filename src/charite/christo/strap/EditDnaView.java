package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.ProteinMC.*;
import static java.awt.event.MouseEvent.*;
import static java.awt.event.KeyEvent.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
   Error: not finalized // !!!!!!
*/
class EditDnaView extends ChPanel implements PaintHook, StrapListener, ChRunnable, Disposable{
    private final Rectangle RECT=new Rectangle(), RECT2[]={new Rectangle(),new Rectangle()}, _saveR[]=new Rectangle[100];
    private final int _saveC[]=new int[100];
    private final static byte[] BUF=new byte[12];
    private final EditDna _parent;
    private final Protein _p;
    private final ChScrollBar _hSB=new ChScrollBar(JScrollBar.HORIZONTAL);
    private final Object[] _img=new Object[2];
    private ResidueSelection _ss[];
    private JComponent _pnl, _lab;
    private Rectangle _cb, _cbSmall;
    private boolean _selecting, _cursorBright=true;

    private int _font=18, _mouseAA=-1, _caret, _selStart=-1, _selEnd=-1, _mc, _multi, _dragged[], _yLines, _yNt, _yPos;
    private long _imgMC=-1;
    Protein  p() { return _p;}
    public EditDnaView(Protein p, EditDna pan) {
        _p=p;
        _parent=pan;
        setToolTipText("");
        enableEvents(MOUSE_EVENT_MASK|MOUSE_WHEEL_EVENT_MASK|FOCUS_EVENT_MASK|FOCUS_EVENT_MASK|KEY_EVENT_MASK);
        StrapAlign.addListener(this);
        setAutoscr(this);
        addPaintHook(this,this);
        addPaintHook(this,_hSB);
        scrollByWheel(this);
        pcp(KOPT_TRACKS_VIEWPORT_HEIGHT, "", this);
        StrapAlign.repaintCursor(true,this);
    }
    @Override public void dispose() {
        StrapAlign.repaintCursor(false,this);
    }
    @Override public void processEvent(AWTEvent ev) {
        final Protein p=p();
        final int id=ev.getID(), modi=modifrs(ev), kcode=keyCode(ev), scale=scaleUpDown(ev), N=p==null?0 : p.countNucleotides();
        if (N==0 || _cb==null) return;
        if (isPopupTrggr(false,ev)) {
            if (isPopupTrggr(true,ev)) StrapAlign.showContextMenu('S',resSels(ev));
        } else {
            final byte nucleotides[]=p.getNucleotidesCodingStrand();
            final int
                chW=wdth(_cb), chH=hght(_cb), chH2=hght(_cbSmall), w=wdth(this),
                lastSelection=_selEnd, x=x(ev), y=y(ev);
            int caret=_caret;
            boolean toClipbd=false;
            Rectangle scroll=null;
            final boolean shift=isShift(ev), ctrl=isCtrl(ev), inSequ=_yNt<=y && y<=_yNt+chH;
            if (scale!=0) changeFont(scale<0);
            if (id==MOUSE_PRESSED) {
                requestFocus();
                if (inSequ) {
                    _selStart=_selEnd=-1;
                    caret=x/chW;
                }
            }
            if (id==MOUSE_DRAGGED) {
                if (_dragged!=null) {
                    final int yD=get(1,_dragged);
                    if (_yNt<=yD && yD<=_yNt+chH) _selEnd=x/chW;
                } else if (inSequ) {
                    _dragged=new int[]{x,y};
                    _selStart=_selEnd=x/chW;
                }
            }
            if (id==MOUSE_MOVED) {
                _dragged=null;
                if (y>-3 && y<_yLines)  {
                    final int
                        nt=x/chW,
                        tNt=get(nt, p.toCodingPositions()),
                        ia=_mouseAA=(tNt-tNt%3+1)/3;
                    if (ia>=0) Protein.mouseOver(p,ia+p.getResidueIndexOffset());
                    setCursor(cursr(ia>=0 ? 'H':'D'));
                }
            }
            if (id==MOUSE_RELEASED) {
                repaint();
                if (_dragged!=null) toClipbd=true;
            }
            if (id==MOUSE_PRESSED && _mouseAA>=0 && y<=_yLines) {
                final StrapView v=StrapAlign.alignmentPanel();
                if (v!=null) {
                    v.setCursor(0,p,_mouseAA-Protein.firstResIdx(p)+p.getResidueIndexOffset());
                    v.scrollCursorToVisible();
                }
            }

            if (id==KEY_PRESSED) {
                final boolean reverse=0!=(p.getCodingStrand()&Protein.REVERSE), coding[]=p.isCoding();
                final byte[] ssN=p.selNuc(), ssA=p.selAminos();
                final int times=maxi(1,_multi), step=kcode==VK_LEFT ? -1 : kcode==VK_RIGHT ? 1 : 0;
                int go=0;
                if ((modi&ALT_MASK)!=0 && step!=0) {
                    final int skippedA=Protein.firstResIdx(p)-p.getResidueIndexOffset();
                    int pos=caret, feature=-1, gone=0;
                    while(pos>=0 && pos<N) {
                        final int
                            nRev=reverse? N-pos-1 : pos,
                            ia=get(pos,p.toCodingPositions())/3-skippedA,
                            f=
                            (get(pos,coding)  ? (1<<1) : 0) |
                            (0!=get(nRev,ssN) ? (1<<2) : 0) |
                            (0!=get(  ia,ssA) ? (1<<3) : 0);
                        if (gone++>3 && feature>=0 && feature!=f) break;
                        feature=f;
                        pos+=step;
                    }
                    caret=maxi(0,mini(N-1,pos));
                } else {
                    switch(kcode) {
                    case VK_C: toClipbd=true; break;
                    case VK_W: if (ctrl) rmFromParent(_pnl); break;
                    case VK_A: if (ctrl) { _selStart=0; _selEnd=MAX_INT; } break;
                    case VK_E: if (ctrl) go=MAX_INT/2; break;
                    case VK_END:  go= MAX_INT/2; break;
                    case VK_HOME: go=-MAX_INT/2; break;
                    case VK_LEFT: go= ctrl ? -MAX_INT/2 : -times; break;
                    case VK_RIGHT:go= ctrl ?  MAX_INT/2 :  times; break;
                    case VK_PAGE_DOWN:  go=100; break;
                    case VK_PAGE_UP:  go=-100; break;
                    case VK_DOWN:  go=10; break;
                    case VK_UP: go=-10; break;
                    }
                }

                if (0==(modi&(CTRL_MASK|ALT_MASK)) && kcode==VK_S) {
                    final int i=times%100;
                    if (shift) {
                        _saveC[i]=caret;
                        _saveR[i]=scrllpn(this).getViewport().getViewRect();
                    } else if (_saveR[i]!=null) {
                        caret=_saveC[i];
                        scroll=_saveR[i];
                    }
                }
                if (0==(modi&(CTRL_MASK|SHIFT_MASK|ALT_MASK))) {
                    if (kcode=='C' && aliCursor2n(p)>0) caret=aliCursor2n(p);

                    if (_multi!=MIN_INT) {
                        if (kcode=='I') caret=_multi-1;
                        if (kcode=='R') caret=N-_multi;
                        if (kcode=='E') {
                            int exon=0;
                            for(caret=0; exon<_multi && caret<N; caret++) {
                                if (get(caret,coding) && !get(exon-1,coding)) exon++;
                            }
                        }
                    }
                }
                if (go!=0) {
                    if (shift && !_selecting) _selStart=caret;
                    caret=maxi(0,mini(N-1, caret+go));
                    if (shift) _selEnd=caret;
                    _selecting=shift;
                } else if (!shift && (kcode==VK_INSERT||kcode==VK_SPACE||kcode==VK_DELETE||kcode==VK_BACK_SPACE)) {
                    final int f,t;
                    if (_selEnd!=_selStart) {
                        f=mini(_selStart,_selEnd);
                        t=maxi(_selStart,_selEnd)+1;
                    } else {
                        caret+=kcode==VK_INSERT || kcode==VK_DELETE ? times : -times;
                        if (_caret<caret) {f=_caret; t=caret;}
                        else { f=caret+1; t=_caret+1;}
                    }
                    boolean changed=false;
                    final boolean on=kcode==VK_INSERT || kcode==VK_SPACE;
                    for(int j=f;  j<t; j++) {
                        final int i=kcode==VK_BACK_SPACE ? j-1:j;
                        if (i>=0 && i<N && i<sze(coding) && coding[i] != on) {
                            changed=true;
                            coding[i]=on;
                        }
                    }
                    if (changed) {
                        Protein.incrementMC(MC_CODING,p);
                        StrapEvent.dispatchLater(StrapEvent.NUCL_TRANSLATION_CHANGED,111);
                        repaint(chW*f,0,chW*(t-f),9999);
                        scroll=new Rectangle(chW*(f-2), 0, chW*(t-f+4), 1);
                    }

                }
            }
            if (_selEnd>N) _selEnd=N;
            if (lastSelection!=_selEnd) repaint(0, _yNt,w,chH2);
            caret=mini(N-1,maxi(0,caret));
            if (caret!=_caret) {
                final int min=mini(caret,_caret), diff=caret-_caret;
                _caret=caret;
                repaint( (min-1)*chW, _yNt, ((diff>0?diff:-diff)+2)*chW, chH);
                if (scroll==null) setRect( (caret-3)*chW, 1, chW*6, 1, scroll=RECT);
            }
            if (scroll!=null) scrollRectToVisible(scroll);

            if (id==MOUSE_PRESSED || kcode!=0) _parent.setFocused(this,true);
        if (toClipbd) {
            final String s=bytes2strg(nucleotides,mini(_selStart,_selEnd),  maxi(_selEnd,_selStart)+1);
            if (sze(s)>0) toClipbd(s);
        }

        }
        _multi=typedNumber(ev);

        if (kcode==0 && scale==0) super.processEvent(ev);
    }
    public boolean paintHook(JComponent c, Graphics g2d, boolean after) {
        if (!after) return true;
        if (c==null) return false;
        final Graphics2D g=(Graphics2D)g2d;
        final int W=wdth(c), H=hght(c);
        final Rectangle clip=clipBnds(g,W,H);
        final Protein p=p();
        final int
            chW=wdth(_cb), chW2=chW/2,  chH=hght(_cb),  chWs=wdth(_cbSmall), chHs=hght(_cbSmall),
            clipX=x(clip),
            clipY=y(clip),
            clipX2=x2(clip),
            N=p==null?0 : p.countNucleotides();
        if (N==0 || W==0) return true;
        final Font font=getFnt(_font,true,0), fontSmall=getFnt(_font/2+1,true,0);
        final boolean reverse=0!=(p.getCodingStrand()&Protein.REVERSE), coding[]=p.isCoding();
        final ResidueSelection[] selections=p.allResidueSelections();
        final byte[] selectedNT=p.selNuc();
        final Color fg=C(isWhiteBG()?0:0xFFffFF), bg=C(isWhiteBG()?0xFFffFF:0), lineColor=C(0x808080);
        if (c==_hSB) {
            final Rectangle track=_hSB.getTrack();
            final int width=maxi(1, wdth(track));
            g.setColor(bg);
            fillBigRect(g,0,0,W,H);
            if (width<=0) return true;
            BufferedImage image0=(BufferedImage)deref(_img[0]), image1=(BufferedImage)deref(_img[1]);
            {
                final int rgbExon=isWhiteBG() ? 0xFFffFFff : 0xFF000000;
                if (image0!=null && image0.getWidth()<width) image0=null;
                if (image1!=null && image1.getWidth()<width) image1=null;
                final long mc=p.mcSum(ProteinMC.MC_CODING, ProteinMC.MC_RESIDUE_SELECTIONS)+ (((long)width)<<32);
                if (image0==null || _imgMC!=mc) {

                    if (image0==null)  _img[0]=newSoftRef(image0=new BufferedImage(width,1, BufferedImage.TYPE_INT_ARGB));
                    final int rgbs[]=sharedRGB(width);
                    for(int i=width; --i>=0;) {
                        final int iN=(int)(i*(long)N/width), iRev=reverse ? N-iN-1 : iN;
                        rgbs[i]=iRev>=0 && iRev<coding.length && coding[iRev]  ? rgbExon : 0xffc8c8c8;
                    }
                    image0.setRGB(0,0,width,1,rgbs,0,width);
                }
                if (image1==null || _imgMC!=mc) {
                    if (image1==null)  _img[1]=newSoftRef(image1=new BufferedImage(width,1, BufferedImage.TYPE_INT_ARGB));
                    final int rgbs[]=sharedRGB(width);
                    Arrays.fill(rgbs,0);
                    final int MINPIXEL=5;
                    for(ResidueSelection s:selections) {
                        if (s==this) continue;
                        Color col=C(0xFF00);
                        if (s instanceof VisibleIn123) {
                            final VisibleIn123 v123=(VisibleIn123)s;
                            if ((v123.getVisibleWhere()&VisibleIn123.SB)==0) continue;
                            col=v123.getColor();
                        }
                        final int rgb=col==null ? 0 : col.getRGB();
                        final SelectorOfNucleotides selNt=deref(s, SelectorOfNucleotides.class);
                        final boolean nt[]= selNt!=null ? selNt.getSelectedNucleotides() : null;
                        if (nt!=null && nt.length>0) {
                            final int ntOffset=selNt.getSelectedNucleotidesOffset();
                            for(int iN=mini(nt.length+ntOffset,N);--iN>=ntOffset;) {
                                if (nt[iN-ntOffset]) {
                                    final long iRev=reverse ? N-iN-1 : iN;
                                    final int
                                        idx1= (int) (iRev*width/N),
                                        idx2= mini(width,  maxi(idx1+MINPIXEL, (int) ((iRev+1)*width/N))+1);
                                    for(int i=idx1; i<idx2; i++) if (i>0) rgbs[i]=rgb;
                                }
                            }
                        } else {
                            final boolean selAA[]=s.getSelectedAminoacids();
                            if (selAA!=null) {
                                final int selOffset=s.getSelectedAminoacidsOffset()+p.getResidueIndexOffset();
                                final int iMax=mini(selAA.length+selOffset, p.countResidues());
                                for(int ia=maxi(0,selOffset); ia<iMax; ia++) {
                                    if (selAA[ia-selOffset]) {
                                        for(int i3=0, iAa3=3*ia; i3<3; i3++) {
                                            final int iN=get(iAa3+i3, p.coding2allPositions());
                                            if (iN<0) continue;
                                            final long iRgb=reverse ? N-iN-1 : iN;
                                            final int
                                                idx1= (int) (iRgb*width/N),
                                                idx2= mini(width, maxi(idx1+MINPIXEL,(int) ((iRgb+1)*width/N))+1);
                                            for(int i=idx1; i<idx2; i++) rgbs[i]=rgb;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    image1.setRGB(0,0,width,1,rgbs,0,width);
                }
                _imgMC=mc;
            }

            {
                final int x1=x(track), x2=x(track)+width;
                final int dest1=reverse?x2:x1, dest2=reverse?x1:x2;
                int y=0;
                if (image0!=null) g.drawImage(image0,dest1, y, dest2, y+3,  0,0,width,1,_hSB); y+=5;
                if (image1!=null) g.drawImage(image1,dest1, y, dest2, y+3,  0,0,width,1,_hSB);
            }
            {
                g.setColor(C(0x8080ff,100));
                final Rectangle t=_hSB.getThumb();
                fillBigRect(g, x(t), y(t), wdth(t), hght(t));
                g.draw(t);
                final Rectangle r=aliCursor(_hSB);
                if (r!=null && _cursorBright) {
                    g.setColor(C(0xFFffFF));
                    g.fill(r);
                }
            }
        } /* _hSB */
        if (c==this) {
            final BA format=new BA(16);
            g.setColor(bg);
            fillBigRect(g, 0,0,W,H);
            aliCursor(this);
            final byte nucleotides[]=p.getNucleotidesCodingStrand();
            if (nucleotides==null) return true;
            final int
                ntTo=mini( N-1, clipX2/chW, nucleotides.length-1),
                ii[]=p.coding2allPositions(),
                kk[]=p.toCodingPositions();
            g.setColor(bg);
            final int ntFrom, tNtFrom;
            {
                final int nt=clipX/chW;
                if (nt>=kk.length) return true;
                final int tNt=kk[nt];
                ntFrom=nt-nt%10;
                tNtFrom=tNt-tNt%3;
            }
            if (clipY<chHs+2*chH) { /* --- aminos --- */
                g.setColor(fg);
                for(int i=tNtFrom-3;i<=ntTo;i+=3) {
                    if (i<0) continue;
                    final int i3=i/3;
                    if (ii.length<=i+2 || N<=i+2) break;
                    final int i0=ii[i], i1=ii[i+1], i2=ii[i+2];
                    if (nucleotides.length<=i2 || i0<0 || i1<0 || i2<0 ) break;
                    g.setColor(lineColor);
                    final int x=i1*chW;
                    for(int j=3;--j>=0;) g.drawLine(x+chW2, _yLines,chW2+ii[j+i]*chW,_yNt-2);
                    if (x+chW<clipX || x>clipX2) continue;
                    BUF[0]=DNA_Util.triplet2aa(nucleotides[i0],nucleotides[i1],nucleotides[i2]);
                    g.setFont(font);
                    setRect(x-1,0,chW*2, chH*2, RECT);
                    final int i3Z=i3+p.getResidueIndexOffset()-Protein.firstResIdx(p);
                    ResSelUtils.drawResidueSelection(Protein.AMINO_ACIDS, BUF[0], p, selections, i3Z, g,RECT,_cb);
                    g.setColor(fg);
                    g.drawBytes(BUF,0,1,x,y(_cb));
                    g.setFont(fontSmall);
                    if (i3%5==0) {
                        format.clr().format10(i3+1+p.getResidueIndexOffset(),0);
                        g.drawBytes(format.bytes(),0, sze(format), x+chW,y(_cb));
                    }
                }
            }
            if (clipY<_yNt) { /* --- ruler for all nucleotides --- */
                g.setFont(fontSmall);
                for(int i=ntFrom-4;i<=ntTo+4;i++) {
                    if (i<0 || i>ntTo || i%10!=0) continue;
                    format.clr().format10(i+1,0).a(" /").format10(N-i,0);
                    final int x=i*chW+chW2, len=sze(format);
                    g.setColor(fg);
                    g.drawBytes(format.bytes(),0,len, maxi(0,x-len*chWs/2),  _yPos+y(_cbSmall));
                    g.setColor(lineColor);
                    g.drawLine(x,_yNt+chH, x, _yPos-2);
                }
            }
            final Font bold=getFnt(_font,true,Font.BOLD);
            for(int i=maxi(0,ntFrom);i<=ntTo; i++) {
                BUF[0]=nucleotides[i];
                final int x=i*chW+chW2;
                boolean isBG=false;
                if (_selStart!=_selEnd && (_selStart<=i && i<=_selEnd || _selEnd<=i && i<=_selStart)) {
                    g.setColor(C(0xFF));
                    fillBigRect(g, x-chW2, _yNt,chW, chH);
                    isBG=true;
                }
                if(i==_caret) {
                    g.setColor(C(0xFF0000));
                    fillBigRect(g, x-chW2, _yNt+2,chW, chH);
                    isBG=true;
                }
                final int iRev=reverse ? N-i-1 : i;
                if (get(iRev,selectedNT)!=0) {
                    g.setColor(C(0xFFAFAF));
                    setRect(x-chW2,_yNt-2,chW+1,chH+6, RECT);
                    ResSelUtils.drawResidueSelection(Protein.NUCLEOTIDES,(byte)0,p,selections,iRev,g,RECT,_cb);
                    isBG=true;
                }
                if (isBG) {
                    g.setColor(bg);
                    g.setFont(bold);
                    g.drawBytes(BUF,0,1,x-chW2-1,_yNt+y(_cb));
                    g.drawBytes(BUF,0,1,x-chW2+1,_yNt+y(_cb)+2);
                }
                g.setColor(i<coding.length && coding[i] ? fg : C(0x808080));
                g.setFont(font);
                g.drawBytes(BUF,0,1,x-chW2, _yNt+y(_cb)+1);
            }
        } /* this */
        return true;
    }
    @Override public String getToolTipText(java.awt.event.MouseEvent ev) {
        final ResidueSelection[] v=resSels(ev);
        if (v.length==0) return null;
        final BA sb=baTip();
        int i=0;
        for(ResidueSelection s:v) {
            if (s==null || s==Protein.getMouseOverSelection()) continue;
            if (i++>0) sb.a("<br>");
            sb.or(dTip(s),s);
        }
        return addHtmlTagsAsStrg(sb);
    }
     private ResidueSelection[] resSels(AWTEvent ev) {
        if (_cb==null) paintComponent(null);
        final Protein p=p();
        if (p==null) return ResidueSelection.NONE;
        final boolean reverse=0!=(p.getCodingStrand()&Protein.REVERSE);
        final int x=x(ev), y=y(ev), iN=(reverse ? getWidth()-x : x)/wdth(_cb);
        if (iN!=0 || _ss==null) {
            final Object[] oo;
            if (y>_yNt) oo=ResSelUtils.selOfNuclAt(p,iN);
            else if (y<_yLines) {
                final int ia=_mouseAA-Protein.firstResIdx(p)+p.getResidueIndexOffset();
                oo=p.residueSelectionsAt(0L,ia,ia+1,VisibleIn123.SEQUENCE);
            } else oo=null;
            final int N=sze(oo);
            final ResidueSelection ss[]=N==0 ? ResidueSelection.NONE : new ResidueSelection[N];
            for(int i=0; i<N; i++) {
                ss[i]=get(i,oo,ResidueSelection.class);
                if (ss[i]==Protein.getMouseOverSelection()) ss[i]=null;
            }
            _ss=rmNullA(ss,ResidueSelection.class);
        }
        return _ss;
    }
    public JComponent getPanel() {
        if (_pnl==null) {
            final Color bg=C(isWhiteBG()?0xFFffFF:0);
            final ChJScrollPane sp=scrllpn(this);
            noBrdr(sp);
            _pnl=pnl(CNSEW,sp,null,"#JS",bg);
            (_lab=new ProteinLabel(0,p())).setForeground(C(0xFFffFF));
            final JComponent pNorth=pnl(CNSEW, _lab, bg, null, new ChButton().doClose(0,_pnl), dim(1,24));
            _pnl.add(pNorth,BorderLayout.NORTH);
            changeFont(false);
            addMoli(MOLI_REFRESH_TIP,_hSB);
            sp.setHorizontalScrollBar(_hSB);
            noBrdr(sp);
            setDragScrolls('B',this, sp);
            pcp(ChRunnable.RUN_IS_DRAG4XY, wref(this), this);
        }
        return _pnl;
    }
    private void changeFont(boolean enlarge) {
        _font=mini(33,maxi(3,_font + (enlarge ? -1 : 1)));
        _hSB.setUnitIncrement(3*_font);
        _hSB.setBlockIncrement(30*_font);
        _cbSmall=chrBnds(getFnt(_font/2+1,true,0));
        _cb=chrBnds(getFnt(_font,true,Font.BOLD));

        final int
            width=wdth(_cb)*p().countNucleotides(),
            h2=hght(_cbSmall),
            h=hght(_cb);
        _yLines=h+2;
        _yNt=_yLines+1*h;
        _yPos=_yNt+8+h;
        final int height=_yPos+h2;
        setPreferredSize(dim(width, height+EX));
        setMaxSze(width, height+EX, _pnl);
        revalAndRepaintC(this);
        revalAndRepaintC(_parent);
    }
      public final void handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        boolean repaint=false;
        if (t==StrapEvent.CURSOR_CHANGED_PROTEIN || t==StrapEvent.CURSOR_MOVED_WITHIN_PROTEIN) {
            final Rectangle rSB=aliCursor(_hSB), r=aliCursor(this);
            if (rSB!=null) _hSB.repaint(x(rSB),y(rSB), wdth(rSB), hght(rSB));
            if (r!=null) repaint(x(r),y(r), wdth(r), hght(r));
        }
        else if (t==StrapEvent.PROTEIN_RENAMED) revalAndRepaintC(_lab);
        else if (t==StrapEvent.RESIDUE_SELECTION_CHANGED ||
                 t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR ||
                 t==StrapEvent.RESIDUE_SELECTION_ADDED ||
                 (t&StrapEvent.FLAG_RESIDUE_TYPES_CHANGED)!=0) {
            final Protein p=p();
            if (p==null || p.countNucleotides()==0) rmFromParent(getPanel());
            final int mc=p==null?0:p.mcSum(MC_RESIDUE_SELECTIONS,MC_RESIDUE_TYPE,MC_NUCLEOTIDE_STRAND);
            repaint=_mc!=mc;
            _mc=mc;
        }
        if (repaint) getPanel().repaint();
    }
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_IS_DRAG4XY && _cb!=null) {
            final int y=y(arg);
            return boolObjct( y>=_yNt &&  y<=_yNt+hght(_cb));
        }
        if (id==ChRunnable.RUN_REPAINT_CURSOR) {
            _cursorBright=arg!=null;
            if (isVisbl(this)) {
                Rectangle r;
                if ((r=aliCursor(_hSB))!=null) _hSB.repaint(x(r), y(r), wdth(r),hght(r));
                if ((r=aliCursor(this))!=null) this.repaint(x(r), y(r), wdth(r),hght(r));
            }
        }
        return null;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Cursor >>> */
    private int aliCursor2n(Protein p) {
        final int
            i0=StrapAlign.indexOfAminoAcidAtCursorZ(p),
            ia=i0<0?-1:i0+Protein.firstResIdx(p)-p.getResidueIndexOffset();
        return get(3*ia+1, p.coding2allPositions());
    }
    private Rectangle aliCursor(JComponent c) {
        final Rectangle R=RECT2[c==_hSB?0:1];
        final Protein p=p();
        final int N=p==null?0:p.countNucleotides();
        if (N<3) return null;
        final long iN=aliCursor2n(p);
        if (c==_hSB) {
            if (iN<0) return null;
            final Rectangle track=_hSB.getTrack();
            final int h=hght(track), w=wdth(track), width=maxi(3,w*3/N);
            setRectAndRepaint( (int)(w*iN/N + x(track)-width/2), h-3, width, 3,  R, c);
        }
        if (c==this) {
            final int w=wdth(_cb);
            setRectAndRepaint((int)(iN*w-w/2), 0, 3*w, hght(_cb), R, c);
        }
        return R;
    }
   /* <<< Cursor <<< */
}
