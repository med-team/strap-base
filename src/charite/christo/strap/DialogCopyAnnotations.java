package charite.christo.strap;
import static charite.christo.ChUtils.*;
import charite.christo.*;
import charite.christo.protein.*;
/**HELP

Residue selections are  underlined in the alignment panel.
Those that are selected have WIKI:marching_ants.

By pressing the button [Go], the residue selections are copied to the
target protein.

<i>SEE_DIALOG:StrapTree,H</i>

@author Christoph Gille
*/
public class DialogCopyAnnotations extends AbstractDialogJPanel implements Runnable {
    private final ProteinCombo comboProt=new ProteinCombo(0);
    private final ChTextField tfGroup=new ChTextField("copied from protein PROTEIN");
    public void run() {
        StrapAlign.errorMsg("");
        final BA sbError=new BA(33);
        final ResidueSelection ss[]=StrapAlign.coSelected().residueSelections('A');
        if (ss.length==0) error("Error, no residue selections are selected.");
        else {

            ResSelUtils.copyResidueSelections(ResSelUtils.COPY_ADD_TO_PROTEIN|ResSelUtils.COPY_REPORT, ss,new Protein[]{SPUtils.sp(comboProt)},toStrg(tfGroup), sbError);
            if (sze(sbError)>0) error(sbError);
            StrapEvent.dispatch(StrapEvent.RESIDUE_SELECTION_CHANGED);
        }
    }
    public DialogCopyAnnotations() {
        add(
            pnl(VBHB,
               dialogHead(this),
                " ",
                pnl(HBL,"Target protein ",comboProt),
                " ",
                pnl(HBL,"Name of group (short free text): ", tfGroup),
                "<sup>(\"PROTEIN\" will be replaced by the name of the source protein)</sup>",
                " ",
                pnl(HBL,"Select residue selections. Then push button ", new ChButton("GO").r(this).t(ChButton.GO))
                )
            );
    }
}
