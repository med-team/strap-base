package charite.christo.strap;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**
   @author Christoph Gille
   http://forum.javacore.de/viewtopic.php?t=2902
   See MetalTabbedPaneUI
*/
public class AbstractDialogJTabbedPane  extends javax.swing.JTabbedPane implements Disposable {
    public AbstractDialogJTabbedPane() {
        pcp(KOPT_UNDOCKABLE,"",this);
        setOpaque(true);
        setBG(DEFAULT_BACKGROUND,this);
        pcpAddOpt(KEY_CLOSE_OPT, CLOSE_DISPOSE_TABS,this);
    }

    private boolean _disposed;
    public void dispose() {
        if (!_disposed) {
            _disposed=true;
        }
        StrapAlign.rmListener(this);
    }
    public boolean isDisposed() { return _disposed; }
    @Override public void fireStateChanged() {
        try {
            /*
              IllegalArgumentException: Comparison method violates its general contract!
                SwingUtilities2.tabbedPaneChangeFocusTo(newComp);
            */
            super.fireStateChanged();
        } catch(Throwable t) {
            putln(RED_CAUGHT_IN+"AbstractDialogJTabbedPane.fireStateChanged() ", t);
        }
    }
}
