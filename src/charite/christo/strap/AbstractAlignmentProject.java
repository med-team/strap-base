package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.Protein;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public abstract class AbstractAlignmentProject extends AbstractDialogJPanel implements java.awt.event.ActionListener {
    public final static String KEY_ADD_TO_PREPARE_PANEL="AAP$$AP";
    public final static int FILES=0,NOTE=1,DEF=2,TITLE=3,EC=4,NAME=5,CHART_DATA=6;
    public final static String DIR_PROTEINS="proteins/";
    private static boolean _haveAddedPath;
    private static String _listFile="";
    private JScrollPane _scrollPane;
    private URL _urls[];
    private final UniqueList<Alignment> _vAlignments=new UniqueList(Alignment.class);
    private static Collection<String> _vOneFromEach, _vUnsorted;
    public void run() {
        if (_scrollPane!=null) return;
        if (!_haveAddedPath){
            //Customize.customize(Customize.file_directories).addHiddenEntries(false, DIR_PROTEINS);
            _haveAddedPath=true;
        }
        final JComponent
            pan=pnl(VBPNL,
                    "<h3>Data Preparation</h3>"+
                    "By loading this project, many files will be created. It is recommended, to make a new project directory.<br>"+
                    "Two large zip files need to be loaded from the web.<br>",
                    new ChButton("DOWNLOAD").t("download").li(this).tt("Hold shift to load only the alignment but not the proteins"),
                    gcp(KEY_ADD_TO_PREPARE_PANEL,this)
                    ),
            butPrepare=ChButton.doView(pan).i(null).t("Prepare project"),
            pNorth=pnl(VBPNL,dialogHead(this),  pnl(HB,"The project files need to be loaded",butPrepare)),
            butCloseAll=new ChButton("CLOSE_ALL").t("Close all proteins").li(this)
            .tt("Loaded proteins in the alignment are closed.<br>Unsaved alignment data is lost"),
            pSouth=pnl(  pnl(radioGrp(new String[]{"Files","Comment","Definition","Title","EC"},0,this)), pnl(butCloseAll));
        _scrollPane=new JScrollPane(getPanelButtons(),JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pcp(KEY_SOUTH_PANEL, wref(pSouth),this);
        pnl(this,CNSEW,_scrollPane,pNorth,pSouth);
        revalidate();
    }

    public void setURLs(URL alignments, URL proteinsCalpha) {
        _urls=new URL[]{alignments,proteinsCalpha};
    }
    {
        if (_vUnsorted==null) _vUnsorted=new UniqueList(String.class);
        adAll(splitTokns(readBytes(file("ALL_IDs"))), _vUnsorted);
    }
    public Alignment[] getAlignments() { return _vAlignments.asArray();}
    private void downloadAndExtract(URL url, File destDir) {
        mkdrsErr(destDir);
        final String path=url.getPath();
        final File f=file(delSfx(".gz", path.substring(path.lastIndexOf('/')+1)));
        delFile(f);
        InteractiveDownload.downloadFiles(new URL[]{url},new File[]{f});
        ChZip.extractAll(ChZip.REPORT_ERROR|ChZip.SUGGEST_DEL,f,destDir);
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (cmd=="DOWNLOAD") {
            downloadAndExtract(_urls[0],dirWorking());
            _scrollPane.getViewport().removeAll();
            _scrollPane.getViewport().add(getPanelButtons());
            if ( !isShift(ev)) downloadAndExtract(_urls[1],file(DIR_PROTEINS));
        }
        if (cmd=="CLOSE_ALL") {
            wrte(file("sorted/"+_listFile),new BA(999).join(StrapAlign.visibleProteins()));
StrapAlign.rmProteins(false,StrapAlign.proteins());
        }
        final int iRadio=radioGrpIdx(q);
        if (iRadio>=0) {
            for(Alignment a:getAlignments()) a._tf.t(a.getInfo(iRadio)).setEditable(false);
        }
    }

    private JComponent getPanelButtons() {
        final JComponent pnl=pnl(VB, KOPT_TRACKS_VIEWPORT_WIDTH);
        final BA sb=new BA(999);
        for(String suffix: new String[]{".LIST"}) {
            final String files[]=lstDir(file("."));
            sortArry(files);
            for(String l:files) {
                if (l.endsWith(suffix)) {
                    boolean each=false;
                    final Alignment a=new Alignment(l,pnl);
                    _vAlignments.add(a);
                    for(String s : a.getProteinNames()) {
                        if (!each) {
                            if (_vOneFromEach==null) _vOneFromEach=new UniqueList(String.class);
                            _vOneFromEach.add(s);
                            each=true;
                        }
                        if (s.length()>6) sb.a(s,0,6).a(".ent ");
                    }
                    sb.a('\n');
                    pnl.add(new JSeparator());
                }
            }
        }
        wrte(file("ll"),sb);
        final String[] unsort=strgArry(_vUnsorted), each=strgArry(_vOneFromEach);
        for(int i=2; --i>=0;) {
            pnl.add(new JSeparator());
            final String fn= i==0 ? "OneFromEach" : "Unsorted";
            wrte(file(fn), new BA(0).join(i==0?each:unsort," "));
            _vAlignments.add(new Alignment(fn,pnl));
        }
        return pnl;
    }

    public Protein[] loadProteins(String name,String proteinNames[]) {
        assrtEDT();
        final BA sb=new BA(999);
        for(String fn:proteinNames) sb.aln(proteinName2file(fn));
        final Protein ppLoaded[][]=StrapAlign.loadTheProteinsInList(StrapAlign.OPTION_PROCESS_LOADED_PROTS|StrapAlign.OPTION_EVENT_PROTEINS_ADDED, sb);
        return ppLoaded==null ? Protein.NONE : ppLoaded[0];
    }

    public File proteinName2file(String name) { return file(DIR_PROTEINS+name); }
    private static void addProtsInList(String fn, final List l) {
        final ChTokenizer t1=new ChTokenizer().setText(readBytes(file(fn)));
        while(t1.nextToken()) {
            final String s1=t1.asString();
            if (s1.startsWith("uuu")|| s1.startsWith("xxx")) continue;
            if (!s1.endsWith(".L")) {
                l.add(s1);
                _vUnsorted.remove(s1);

            } else addProtsInList(s1,l);
        }
    }
    public void alignmentInstantiatedHook(Alignment a){}

    public final class Alignment implements java.awt.event.ActionListener {
        private final String _base, _name;
        private final ChTextArea _tf=new ChTextArea("");
        private JComponent _addComponent[]=new JComponent[9];
        public void setAdditionalComponent(JComponent c, int where) {
            _addComponent[where]=c;
        }
        public String[] getProteinNames() {
            final List<String> l=new ArrayList();
            addProtsInList(_name,l);
            final String sorted[]=readLines(file("sorted/"+_name));
            for(int i=sze(sorted); --i>=0;) {
                final String s=sorted[i];
                if (l.remove(s)) l.add(0,s);
            }
            return strgArry(l);
        }
        public String getName() { return _name;}
        public BA getInfo(int i) {
            final String ext=i==NOTE?".NOTE" : i==DEF?".DEF" : i==TITLE?".TITLE" : i==EC?".EC" : null;
            BA cs=null;
            if (i==FILES) cs=readBytes(_name);
            else if (i==NAME) cs=toBA(_base);
            else if (i==CHART_DATA) {
                final String sd[]=readLines(fChartData());
                if (sd!=null) {
                    cs=new BA(99);
                    for(String s:sd) if (strstr("NaN",s)>0) cs.aln(s);
                }
            }
            return ext!=null ? readBytes("info/"+_base+ext) : cs;
        }
        public void actionPerformed(java.awt.event.ActionEvent ev) {
            final String cmd=ev.getActionCommand();
            final Object q=ev.getSource();
            if (q instanceof JComponent) ((JComponent)q).setBackground(((JComponent)q).getBackground().darker());
            if (cmd=="1D"){
                if (_name.endsWith(".LIST")) Strap.setListFile(_name);
                if (isCtrl(ev)) edFile(-1, file("info/"+_base+".NOTE"), 0);
                else if (isShift(ev)) edFile(-1, file("info/"+_base+".TITLE"), 0);
                else {
                    loadProteins(_name,getProteinNames());
                    final ProteinList pl=child(derefC(StrapAlign.MAP_DIALOGS.get(DialogSelectionOfResidues.class)), ProteinList.class);
                    if (pl!=null) pl.selectAll(0);
                }
            }
            if (cmd=="3D") {
                if (isCtrl(ev)) edFile(-1, file(_name), 0);
                else {
                    final String nn[]=getProteinNames();
                    final Protein pp[]=new Protein[nn.length];
                    for(int i=nn.length; --i>=0;) {
                        Protein p=SPUtils.proteinWithName(nn[i],null);
                        if (p==null) {
                            p=Protein.newInstance(proteinName2file(nn[i]));
                            if (p!=null) {
                                p.setAlignment(StrapAlign.getInstance());
                                p.setName(nn[i]);
                                StrapAlign.readAttributes("3",p,(File)null);
                            }
                        }
                        pp[i]=p;
                    }
                    StrapAlign.new3dBackbone(0,pp);
                }
            }
        }
        private Alignment(String name, JComponent panel) {
            _name=name;
            _base=delDotSfx(name);
            alignmentInstantiatedHook(this);
            final Object
                but1d=new ChButton("1D").li(this).bg(0xFFffFF).fg(0xFF)
                .tt("<ul><li>Load all proteins => Strap alignment</li><li>Ctrl-key: edit note</li><li>Shift-key: edit title</li></ul>"),
                but3d=new ChButton("3D").li(this).bg(0xFFffFF).fg(0xFF)
                .tt("<ul><li>Withtout Shift-key without Ctrl-key: all proteins in one 3d</li><li>Ctrl-key: edit List file </li><li>Shift-key: grep </li>"),
                pButtons=pnl(HB,pnl(VBPNL, name+" #"+getProteinNames().length,  pnl(HB, cbox(""),but3d,but1d,_addComponent[SwingConstants.WEST])),_addComponent[SwingConstants.EAST]);
            panel.add(pnl(CNSEW, scrllpn(_tf),null,null,null,pButtons));
        }

        public File fChartData() { return file("chartData/"+_base+".data");}
    }
}
