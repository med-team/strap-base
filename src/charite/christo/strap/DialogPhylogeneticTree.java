package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   This dialog is used to draw phylogenetic dendrograms of aligned
   protein sequences.

   @author Christoph Gille
*/
public class DialogPhylogeneticTree extends AbstractDialogJPanel implements ChRunnable,ActionListener {
    private final ChCombo comboClass=SPUtils.classChoice(PhylogeneticTree.class);

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if ( cmd==ChButton.GO){
            setNextProcessHasLogPanel(isCtrl(ev));
            final Protein pp[]=StrapView.ppInRectangleForAlignment(3);
            if (pp.length<3) StrapAlign.errorMsg("For phylogenetic trees at least 3 proteins are required");
            else {
                StrapAlign.errorMsg("");
                final Rectangle r=StrapView.rectangle(null);
                final byte[][] gapped=SPUtils.gappedSequences(pp, x(r), r!=null?x2(r):MAX_INT,'-');
                final String[] names=SPUtils.names(pp,false, false);
                startThrd(thrdCR(this,"GO",new Object[]{gapped, names, pp, intObjct(modifrs(ev))}));
            }
        }
    }
    public DialogPhylogeneticTree() {
        final JComponent
            pWest=pnl(VBHB,
             " ",
             "The proteins must be correctly aligned.",
             "Select some aligned proteins or select a rectangular alignment region with the mouse.",
             " ",
             comboClass.panel(),
             StrapView.button(StrapView.BUT_WARN_MARCHING_ANTS),
             pnl(new ChButton(ChButton.GO).li(this))
             );
        add(pWest);
    }
    public Object run(String id, Object arg) {
        if (id=="GO") {
            final Object[] argv=(Object[])arg;
            final byte[][] gapped=(byte[][])argv[0];
            final String[] names=(String[])argv[1];
            final Protein[] proteins=(Protein[])argv[2];
            final boolean ctrl=0!=(ActionEvent.CTRL_MASK&atoi(argv[3]));

            final PhylogeneticTree phy= mkInstance(comboClass,PhylogeneticTree.class,true);
            if (phy==null) StrapAlign.errorMsg("Unable to instantiate "+comboClass);
            else {
                phy.setAlignment(names,gapped, proteins);
                phy.drawTree();
                if (ctrl) shwCtrlPnl(ChFrame.AT_CLICK, shrtClasNam(phy), phy);
            }
        }
        return null;
    }
}
/*
  http://mesquiteproject.org/mesquite/mesquite.html
*/
