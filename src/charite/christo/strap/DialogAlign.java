package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.util.ArrayList;
import java.awt.event.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   The Dialog is used to align a
   number of selected proteins (see WIKI:Sequence_alignment).

   The user selects an alignment method <i>COMBO:charite.christo.strap.extensions.MultipleAlignerClustalW</i>.

   By pressing the button <i>LABEL:ChButton#BUTTON_GO</i> the alignment
   computation is started. It takes some seconds to minutes, before the
   computation the result is shown in a preview panel.

   To accept the alignment in the preview and to infer the alignment into
   the working alignment panel
   <i>BUTTON:DialogAlignResult#BUT_LAB_Apply</i> needs to be pressed to.

   <b>3D-alignment</b> programs consider the 3D-structure.
   If not all proteins have 3D-coordinates, then the Strap uses a combination of
   3D- and sequence alignment.

   <i>SEE_DIALOG:DialogAlignOneToAll</i>

   @author Christoph Gille
*/
public final class DialogAlign extends AbstractDialogJTabbedPane implements StrapListener, ActionListener, ChRunnable {
    public final static String RADIO_REGION="Rubber band selection", RADIO_SELECTION="Text list of protein names";
    private final AbstractButton
        _cbSecStru=cbox("Use secondary structure"),
        _radio[]=radioGrp(new String[]{"Selecting proteins", RADIO_REGION , RADIO_SELECTION},0,this);
    private final ChButton togProfile[]=new ChButton[3], _butInsertRB=new ChButton("Insert rubber band selection").li(this);
    private final JComponent _pC[]=new JComponent[3], _pMain;
    private final ChTextArea _taResSel=new ChTextArea(""), _taProfile[]=new ChTextArea[3];
    private final ProteinList _pList=new ProteinList(0L);
    private final ChCombo _comboClass=SPUtils.classChoice(SequenceAligner.class).save(DialogAlign.class,"SequenceAligner").li(this);
    private int _count=1;
    /* <<< Fields <<< */
    /* ---------------------------------------- */
    /* >>> Constructor  >>> */

    public DialogAlign() {
        pcp(KEY_ENABLED,_comboClass,_pList);
        _pC[0]=_pList.scrollPane();
        _pC[1]=pnl(VBHB,"The residues within the rubber band are taken.",
                        "The rubber band is a red-white  \"Marching ants\" rectangle in the alignment pane.",
                        "It looks like:"
                        );
        addMarchingAnt(ANTS_SEARCH, wref(_pC[1]), new Rectangle(10*EM,2*EX, 5*EM, EX), C(0xffFFff),C(0xFF0000));
        final Object
            msgExplain=pnl(VBHB,
                           "The tab key completes protein names.<br>"+
                           "Residue ranges are written in the form \"myProtein/100-200\".<br>"+
                           "If the range goes from the first or to the last position, it can be written like \"myProtein/-200\" or  \"myProtein/100-\", respectively.<br>"+
                           "With the tool button below, residue ranges of residue selections can be copied.<br>"+
                           "The following dialogs can generate residue selections by computation:",
                           StrapAlign.b(DialogSelectionOfResiduesMain.class),
                           StrapAlign.b(DialogBlast.class),
                           StrapAlign.b(DialogAlignOneToAll.class)
                           ),
            msgList=pnl(VBHB,
                        pnl(HBL, "Enter a list of proteins or residue ranges.",pnlTogglOpts(" Explain:",msgExplain)),
                        msgExplain),

            butInsert=new ChButton("INC_S").li(this).t("Insert selected residue selections").tt(ResSelUtils.ttSelect());
        addActLi(this,_taResSel);
        StrapAlign.newDropTarget(_taResSel,false);
        _pC[2]=pnl(CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE, _taResSel),msgList,pnl(butInsert," ",_butInsertRB));
        _taResSel.tools().saveInFile("DialogAlign_namesResSel").underlineRefs(ULREFS_NOT_CLICKABLE);
        StrapAlign.highlightProteins("PA",_taResSel);
        final JComponent
            panProfile=pnl(new GridLayout(1,togProfile.length)),
            panDetails0=pnl(VBHB,"#ETBem",
                            pnl(HBL,"Alignment Method:  Note: 3D-methods are indicated by icon",iicon(IC_SUPERIMPOSE)),
                            _comboClass.panel(),
                            buttn(TOG_CACHE).cb(),
                            buttn(TOG_ALI_NOT_COMMUT).cb(),
                            pnl(HBL,"Use some pre-aligned sequences as a profile.", panProfile),
                            pnl(HBL,"Currently only for T-coffee:", _cbSecStru),
                            (isSystProprty(IS_LINUX386) || isSystProprty(IS_WINDOWS)) ? null :  "Note: some alignment methods require certain packages to be installed."
                            ),
            panDetails=pnl(CNSEW,null,panDetails0),
            pEast=pnl(VBPNL,
                      pnl(HBL," ",pnl(HB,"#TB The proteins to be aligned can be specified in three ways:",_radio)," "),
                      pnl(HBL, pnlTogglOpts(panDetails), "#",new ChButton("GO").t(ChButton.GO).li(this), "#"),
                      panDetails
                      );
        _pMain=pnl(CNSEW,null, dialogHead(this),pnl(REMAINING_VSPC1) ,null,pEast);
        setCenter();
        for(int i=0; i<togProfile.length; i++) panProfile.add((togProfile[i]=toggl("profile "+(i+1)).li(this)).cb());
        adMainTab(_pMain,this, DialogAlign.class);
        enableDisable();
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Profiles >>> */
    private static boolean takesProfile(Object a, boolean msg) {
        if (!isAssignblFrm(SequenceAlignerTakesProfile.class,a)) {
            if (msg) {
                final BA s=new BA(shrtClasNam(a)+" does not take profiles.<br>Only the following methods can work with alignment profiles:<ul>");
                for(Object o : derefArray(StrapPlugins.allClassesV(SequenceAligner.class).asArray(), SequenceAlignerTakesProfile.class))
                    s.a("<li>").a(shrtClasNam(o)).a("</li>");
                error(s.a("</ul>"));
            }
            return false;
        }
        return true;
    }
    public void addProfiles(SequenceAligner aligner) {
        BA sbIgnore=null, sbError=null;
        final Color COLOR_IGNORE=C(0xFF0000,66),  COLOR_OK=C(0x00ff00,66);
        int count=0;
        for(int i=0; i<togProfile.length; i++) {
            if (!isSelctd(togProfile[i]) || !takesProfile(aligner,true)) continue;
            final SequenceAlignerTakesProfile alignerP=(SequenceAlignerTakesProfile)aligner;
            final int maxProfile=alignerP.getMaxNumberOfProfiles();
            final ChTextArea ta=_taProfile[i];
            final BA ba=toBA(ta);
            final int ends[]=ba.eol();
            final byte[] T=ba.bytes();
            wrte(file(DIR_ANNOTATIONS+"/profile"+i),T,0,ba.end());
            final ArrayList<byte[]> vGapped=new ArrayList();
            int blockBegin=0;
            for(int iL=0; iL<ends.length;  iL++) {
                final int b= iL==0 ? 0 : ends[iL-1]+1;
                final int e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                int countLetters=0;
                Color color=null;
                int start=b;
                if (e-b>5 && strEquls("pdb",T,b)) { /* Skip protein name */
                    final int spc=nxt(SPC,T,b,e);
                    if (spc>0) start=nxt(-SPC, T,spc,e);
                }
                for(int iT=start; iT<e; iT++) {
                    final byte c=T[iT];
                    if (is(LETTR,c)) countLetters++;
                    else if (c!=' ' && c!='.' && c!='-') {
                        countLetters=-1;
                        if (sbIgnore==null) sbIgnore=new BA(99).a("The following lines are ignored: "); else sbIgnore.a(", ");
                        color=COLOR_IGNORE;
                        sbIgnore.a(iL);
                        break;
                    }
                }
                if (countLetters>0) {
                    final byte seq[]=new byte[e-start];
                    for(int iT=start; iT<e;iT++) seq[iT-start]=is(LETTR,T,iT) ? T[iT] : (byte)'-';
                    if (sze(vGapped)==0) blockBegin=start;
                    //putln(seq);
                    vGapped.add(seq);
                }
                if (countLetters==0 || iL==ends.length-1) {
                    final byte[][] alignment=toArry(vGapped,byte[].class);
                    if (alignment.length==1) {
                        (sbError==null ? sbError=new BA(99) : sbError).a("Error line ").a(iL).a(" profile has only one sequence.\n");
                    }
                    if (alignment.length>1) {
                        if (++count>maxProfile) StrapAlign.errorMsg(shrtClasNam(aligner)+" takes not more than "+maxProfile+" profiles.");
                        else {
                            alignerP.addProfile(alignment);
                            color=COLOR_OK;
                        }
                    }
                    vGapped.clear();
                }
                if (color!=null) ChJHighlighter.addColoredBg(color==COLOR_OK ? blockBegin : b,e, color, ta);
            }
        }
        if (sbIgnore!=null) (sbError==null ? sbError=new BA(99) : sbError).a('\n').a(sbIgnore);
        if (sbError!=null) error(sbError);
    }

    /* <<< Profiles <<< */
    /* ---------------------------------------- */
    /* >>> ActionEvent >>> */
    @Override public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final boolean isSelctd=isSelctd(q);
        if (q==_comboClass) repaintC(_pList);
        for(int i=togProfile.length; --i>=0;) {
            if (q==togProfile[i] && isSelctd) {
                if (_taProfile[i]==null) {
                    final ChTextArea ta=_taProfile[i]=new ChTextArea(readBytes(DIR_ANNOTATIONS+"/profile"+i));
                    final Object msg=pnl(HBL,
                        "Enter a sequence alignment without protein name. This alignments will be used as a profile.<br>"+
                        "More than one profile can be given by separating the alignments by a blank line.<br>"+
                        "A rectangular region (\"walking ants\") in the alignment pane can be inserted as a profile: <br>"+
                        "Open a rectangular region by dragging the mouse. right-click the box and select \"Copy\"  in the context menu. Type Ctrl+V to insert."
                                                    ),
                        c=pnl(CNSEW,scrllpn(ta), pnl(VBHB,pnlTogglOpts("Explain",msg),msg));
                    addActLi(this,ta);
                    ta.tools().saveInFile("DialogAlign_profile"+i).cp("MSG",c);
                }
                ChFrame.frame(getTxt(q), gcp("MSG",_taProfile[i]),CLOSE_CtrlW).shw();
            }
        }
        Object append=null;
        if (cmd=="INC_S") {
            final BA sb=ResSelUtils.listFromTo(StrapAlign.coSelected().residueSelections('s'), 0, new BA(999));
            if (sze(sb)>0) append=sb;
            else error("Error: No reside selection specified.<br><br>"+ResSelUtils.ttSelect());
        }
        if (q==_butInsertRB) append=StrapView.proteinsInRectangleToText(StrapAlign.alignmentPanel());
        if (append!=null) _taResSel.t("\n"+append);
        if (cmd=="GO") tryAlignment();
        setCenter();
    }

    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Compute >>> */
    private void tryAlignment() {
        final Protein[] pp,ppLocal;
        int startIdx=0;
        String error2="Error: Required at least 2 proteins.";
        switch(radioGrpIdx(_radio)) {
        case 1:
            final Rectangle rect=StrapView.rectangle(null);
            pp=rect==null?null : StrapView.ppInRectangle(null);
            ppLocal=new Protein[sze(pp)];
            for(int iP=ppLocal.length; --iP>=0;) {
                ppLocal[iP]=SPUtils.newProteinColumnFromTo(pp[iP],x(rect), x2(rect));
                startIdx=x(rect);
            }
            error2="Error: Please mark a rectangular area in the alignment with the mouse.";
            break;
        case 2:
            final Object[] pp_ss=StrapAlign.proteinsAndAnnotationsWithRegex(toStrg(_taResSel),StrapAlign.proteins(),'A');
            final ResidueSelection ss[]=derefArray(pp_ss,ResidueSelection.class);
            final ArrayList vP=new ArrayList(pp_ss.length);
            for(ResidueSelection s : ss) adUniq(s.getProtein(), vP);
            adAllUniq( derefArray(pp_ss,Protein.class),vP);
            pp=toArry(vP, Protein.class);
            ppLocal=new Protein[pp.length];
            for(int iP=pp.length; --iP>=0;) {
                final Protein pL=SPUtils.newProteinNarrowToResidueSelection(pp[iP],ss);
                ppLocal[iP]=pL!=null ? pL : pp[iP];
            }
            break;
        default:
            pp=ppLocal=_pList.selectedProteinsAtLeast(2);
            break;
        }

        if (sze(pp)<2) { error(error2); return; }
        final SequenceAligner aligner=StrapPlugins.instanceSA(_comboClass, sze(pp),true);
        if (aligner==null)  { error("Could not instantiate the alignment class.");return;}
        aligner.setOptions(SequenceAligner.OPTION_NOT_TO_SECURITY_LIST);
        SPUtils.setSeqs(aligner, ppLocal);
        addProfiles(aligner);
        aligner.setOptions(isSelctd(_cbSecStru)?SequenceAligner.OPTION_USE_SECONDARY_STRUCTURE:0L);
        adTab(CLOSE_CtrlW|CLOSE_CHILDS, "preview "+_count++, new DialogAlignResult(DialogAlignResult.USE_THREADS, aligner,pp, startIdx), this);
    }
    /* <<< Compute <<< */
    /* ---------------------------------------- */
    /* >>> StrapEvent >>> */
    public void handleEvent(StrapEvent e) {
        final int t=e.getType();
        if (t==StrapEvent.RESIDUE_SELECTION_CHANGED || t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR || t==StrapEvent.AA_SHADING_CHANGED) {
            repaintC(getSelectedComponent());
        }
        if (t==StrapEvent.RUBBER_BAND_CREATED_OR_REMOVED) enableDisable();
    }
    private void enableDisable() {
        final StrapView view=StrapAlign.alignmentPanel();
        _butInsertRB.enabled( view!=null&& wdth(view.rectRubberBand())>0);
    }
    /* <<< StrapEvent <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        final int iRadio=id==RADIO_REGION ? 1 : id==RADIO_SELECTION ? 2 : -1;
        if (iRadio>=0) {
            radioGrpSet(iRadio,_radio);
            if (id==RADIO_SELECTION) _taResSel.t(toStrg(arg));
            setCenter();
        }
        return null;
    }
    private JComponent _pCnow;
    private void setCenter() {
        final JComponent c=_pC[maxi(0,radioGrpIdx(_radio))];
        if (_pCnow!=c) {
            rmFromParent(_pCnow);
            _pMain.add(_pCnow=c, BorderLayout.CENTER);
        }
    }

}
