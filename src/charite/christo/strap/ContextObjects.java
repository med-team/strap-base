package charite.christo.strap;
import javax.swing.JPopupMenu;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public final class ContextObjects {
    public final static Object KEY=new Object();
    private final Object _vObjects;
    private final String _name;
    private final boolean _immutable;
    private int _mc=-1;
    private HeteroPopup _hMenu;
    private ProteinPopup _pMenu;
    private ProteinViewerPopup _pvMenu;
    private ResidueSelectionPopup _sMenu;
    private Object _lastV;   /* <<< Fields <<< */
    /* ---------------------------------------- */
    /* >>> Constructor  >>> */
    public ContextObjects(boolean immutable, Object v, String name) {
        _name=name;
        _immutable=immutable;
        _vObjects=v;
    }
    private final UniqueList _vResSel[]=new UniqueList['z'+1];
    private UniqueList vResSel(char c) {
        if (_vResSel[c]==null) _vResSel[c]=new UniqueList(id2class(c));
        return _vResSel[c];
    }
    public String toString() {return super.toString()+"_"+_name;}
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
   public ResidueSelection[] residueSelections(char A_or_S) {
        update();
        return vResSel(A_or_S)==null ? ResidueSelection.NONE : (ResidueSelection[])vResSel(A_or_S).asArray();
    }
    public Protein[]          proteins() { update(); return   (Protein[])vResSel('P').asArray(); }
    public HeteroCompound[] heteroCompounds() { update(); return (HeteroCompound[])vResSel('H').asArray();}
    public ProteinViewer[]   proteinViewers() { update(); return  (ProteinViewer[])vResSel('3').asArray(); }
    public ProteinPopup proteinMenu() {if (_pMenu==null) _pMenu=new ProteinPopup(this); return _pMenu;}
    public HeteroPopup heteroCompoundMenu() {
        if (_hMenu==null) _hMenu=new HeteroPopup(this);
        return _hMenu;
    }
    public ResidueSelectionPopup selectionMenu() {
        if (_sMenu==null) _sMenu=new ResidueSelectionPopup(this);
        return _sMenu;
    }
    public ProteinViewerPopup proteinViewerMenu() { if (_pvMenu==null) _pvMenu=new ProteinViewerPopup(_immutable,this); return _pvMenu;}

     JPopupMenu menuForType(char id) {
        return
            id=='F' || id=='A' || id=='S' || id=='s'? selectionMenu().menu(id) :
            id=='P' ? proteinMenu().menu() :
            id=='3' ? proteinViewerMenu().menu() :
            id=='H' ? heteroCompoundMenu().menu() :
            null;
    }
    private void update() {
        final Object v=_vObjects==CNTXT_OBJCTS ? getCntxtObj() : _vObjects;
        int mc=modic(v);
        if (v instanceof Object[] || v instanceof List) {

            for(int i=sze(v); --i>=0;) {
                final Object o=get(i,v);
                if (o!=null) mc+=o.hashCode();
            }
        } else if (v!=null) mc+=v.hashCode();
        if (_mc!=mc  || _lastV!=v || mc<0) {
            final Object oo[]=oo(getSelValues(v));
            _mc=mc;
            _lastV=v;
            for(UniqueList vS : _vResSel) clr(vS);
            for(int i=0; i<oo.length; i++) {
                final Object o=get(i,oo);
                if (o instanceof UniqueList) for(Object el : ((UniqueList)o).asArray()) myAdd(el);
                myAdd(o);
            }
        }
    }
    private void myAdd(Object o) {
        final char id=menuID(o);
        if (id!=0) {
            if (id=='F' || id=='A' || id=='S') vResSel('s').add(o);
            vResSel(id).add(o);
        }
    }
    /* <<< Update  <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    public static char menuID(Object o) {
        return
            o instanceof HeteroCompound ? 'H' :
            o instanceof ResidueSelection ? ResSelUtils.type(o) :
            o instanceof Protein ?  'P' :
            o instanceof UniqueList &&  ((UniqueList)o).getClazz()==ResidueAnnotation.class ? 'G' :
            isInstncOf(ProteinViewer.class,o) ?  '3' :
            (char)0;
    }
    public static boolean isMenuID(char type, Object o) {
        return
            o==null ? false :
            type=='s'? isAssignblFrm(ResidueSelection.class, o) :
            menuID(o)==type;
    }
    public static Class id2class(char c) {
        final Class clazz=
            c=='A' || c=='F' ? ResidueAnnotation.class :
            c=='S' || c=='s' ? ResidueSelection.class :
            c=='G' ? UniqueList.class :
            c=='P' ? Protein.class :
            c=='3' ? ProteinViewer.class :
            c=='H' ? HeteroCompound.class :
            null;
        if (clazz==null) assrt();
        return clazz;
    }
    // --------------------------------------------------
    private static ContextObjects _inst;
    public static ContextObjects defaultInstance() {
        if (_inst==null) _inst=new ContextObjects(false,CNTXT_OBJCTS, "Context");
        return _inst;
    }

    public static JPopupMenu popupMenu(char menuType, Object[] ooMouse, Object[] ooSelected) {
        if (sze(ooMouse)==0) return null;
        if (null==id2class(menuType)) assrt();
        final java.awt.AWTEvent mev=lstMouseEvt();
        final Object q=mev==null?null:mev.getSource();
        if (q!=null) undockSetTarget(gcp(KEY_UNDOCK_TARGET,q, ChJList.class));
        final Set v=new HashSet();
        for(Object o : ooMouse) if (isMenuID(menuType,o)) v.add(o);
        setCntxtObj(v);
        if (cntainsAtLeastOne(ooMouse,ooSelected)) {
            for(Object o : ooSelected) if (isMenuID(menuType,o)) v.add(o);
            setCntxtObj(v);
        }
        return defaultInstance().menuForType(menuType);
    }

    public static void openMenu(char menuType, Object[] ooMouse, Object[] ooSelected) {
        shwPopupMenu(popupMenu(menuType,ooMouse,ooSelected));
    }

}
