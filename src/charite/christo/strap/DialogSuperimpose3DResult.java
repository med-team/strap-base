package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class DialogSuperimpose3DResult implements Disposable,StrapListener,Runnable,ActionListener,PaintHook  {
    private final Superimpose3D _s3d;
    private final Protein _pp[]={null,null};
    private final DialogSuperimpose3D _parent;
    private String _msg=" still calculating ...";
    private ChTextArea _ta;
    private JComponent _pan;
    private Protein3d _p3D;
    private Matrix3D _m3d;
    private Object  _butApply, _butUndo, _pSouth, _butDetails;
    DialogSuperimpose3DResult(Superimpose3D s3d, DialogSuperimpose3D parent) {
        _s3d=s3d;
        _parent=parent;
        inEDT(thrdM("init",this));
        parent.vRESULT.add(this);
    }
    public void init() {
        StrapAlign.addListener(this);
        for(int i=2; --i>=0;) {
            final Protein
                pp[]=_s3d.getProteins(),
                p=sze(pp)>i?pp[i]:null,
                p0=gcp(KEY_CLONED_FROM,p,Protein.class);
            _pp[i]= p0!=null ? p0 : p;
        }
    }
    public JComponent getPanel() {
        if (_pan==null) {
            _butApply=new ChButton(ChButton.DISABLED,DialogSuperimpose3D.BUT_LAB_Apply).li(this).tt("Accept the transformation as shown in the preview.");
            _butUndo=new ChButton(ChButton.DISABLED,"Undo").li(this);
            _butDetails=ChButton.doCtrl(_s3d);
            _ta=new ChTextArea(_msg);

            final Object
                butDis=new ChButton("D").li(this).t("Distance in alignment..."),
                butAll=new ChButton("A").li(this).t("Approve results from all tabs ..."),
                butHelp=smallHelpBut(new Object[]{_s3d, DialogSuperimpose3D.class}),
                t1=toggl().doCollapse(new Object[]{_ta, _butDetails}),
                t2=toggl().doCollapse(new Object[]{butAll, butDis});
            _pSouth=pnl(HB, "#", _butApply," ",_butUndo,"#", t1,"Info  ", _butDetails, t2,"Options  ", butAll, butDis, " ", butHelp);
            addPaintHook(this, _p3D=StrapAlign.new3dBackbone(V3dUtils.OPEN_NOT_ADD_PANEL,_pp));
            addCP(KEY_CLOSE_HOOKS,wref(this),_pan=pnl(CNSEW,_p3D,_ta,_pSouth));
            addCP(KEY_CLOSE_HOOKS,wref(this), _p3D);
        }
        return _pan;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    @Override public void dispose() {
        StrapAlign.rmListener(this);
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        rmFromParent(_pan);
        dispos(_s3d);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  AWTEvent >>> */
    public void handleEvent(StrapEvent ev) {
        final int t=ev.getType();
        if (_p3D!=null) {
            if (t==StrapEvent.RESIDUE_SELECTION_CHANGED || t==StrapEvent.RESIDUE_SELECTION_CHANGED_COLOR) _p3D.repaint();
            if (t==StrapEvent.PROTEIN_VIEWER_CLOSED && ev.getSource()==_p3D) {
                dispose();
                StrapAlign.addDialogC(DialogSuperimpose3D.class);
            }
        }
    }
    public void actionPerformed(ActionEvent ev) {
        final Protein pp[]=_pp;
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==_butApply  || q==_butUndo || cmd=="A") {
            if (q==_butApply) apply();
            else if (cmd=="A" && ChMsg.yesNo("Really accept results in all result tabs?")) {
                for(Object result : _parent.vRESULT.toArray()) {
                    if (result!=null) ((DialogSuperimpose3DResult)result).apply();
                }
            } else if (q==_butUndo) {
                pp[1].setRotationAndTranslation(gcp(_butApply, pp[1],Matrix3D.class));
                StrapEvent.dispatch(StrapEvent.PROTEIN_3D_MOVED);
                setEnabld(false, _butUndo);
            }
            StrapAlign.drawMessage("@1 3D Coordinates of "+ANSI_GREEN+pp[1]+(q==_butApply?" changed ":" reset ")+" coordinates");
        }
        if (cmd=="D" && pp[1]!=null) {
            if (ChMsg.yesNo(
                            "<b>3D-distance of C-alpha atoms in the alignment: </b><br><br>"+
                            "All residues within a certain range around the cursor position are highlighted.<br><br>"+
                            "This makes sense only after superimposing all proteins to the same 3D-model<br>"+
                            "by pressing the button [Approve results from all tabs]")) {
                StrapAlign.addDialogSetClass(CLASS_DialogSelectionOfResiduesMain, Distance3DToCursor.class);
            }
        }
    }
    private void apply(){
        final Protein pp[]=_pp;
        if (gcp(_butApply,pp[1])==null) {
            final Matrix3D m3d=pp[1].getRotationAndTranslation();
            if (m3d!=null) pcp(_butApply, new Matrix3D(m3d),pp[1]);
        }
        final Matrix3D m=new Matrix3D().mult(_m3d);
        final Protein pR=(Protein)get(0,_s3d.getProteins());
        if (pR==null) stckTrc();
        else m.mult(pR.getRotationAndTranslation());
        pp[1].setRotationAndTranslation(m);
        StrapEvent.dispatch(StrapEvent.PROTEIN_3D_MOVED);
        setEnabld(true, _butUndo);
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Graphics >>> */
    public boolean paintHook(JComponent c, Graphics g, boolean after) {
        if (after && gcp("COMPUTING",this)!=null) {
            g.setColor(Color.PINK);
            g.setFont(getFnt(ICON_HEIGHT,true,Font.BOLD));
            g.drawString(_msg,ICON_HEIGHT,3*ICON_HEIGHT);
        }
        return true;
    }
    /* <<< Graphics <<< */
    /* ---------------------------------------- */
    /* >>> Computation  >>> */
    private boolean superimpose() {
        if (_p3D!=null) _p3D.eachChainOneColor(false,false);
        _s3d.compute();
        revalAndRepaintC(_butDetails);
        final Superimpose3D.Result result=_s3d.getResult();
        if (result!=null) {
            _m3d=result.getMatrix();
            result.getScore();
        }
        return result!=null;
    }
    public Superimpose3D.Result getResult() {
        final Superimpose3D s3d=_s3d;
        return s3d==null?null:s3d.getResult();
    }
    /* <<< Computation <<< */
    /* ---------------------------------------- */
    /* >>> Thread  >>> */
    public void run(){
        final Protein pp[]=_pp;
        try {
            final Superimpose3D s3d=_s3d;
            pcp("COMPUTING","",this);
            if (!superimpose()) {
                _msg="calculation terminated with an error! ";
                _ta.t(_msg); return;
            }
            final Superimpose3D.Result result=s3d.getResult();
            final BA sb=new BA(999).a("Superimposed ").a(pp[1]).a(" upon ").a(pp[0]);
            sb.a("\nMethod: ").aln(shrtClasNam(s3d));
            for(int i=0;i<2; i++) {
                final int n= gcp(KEY_CLONED_FROM, pp[i])==null ? 0 : countTrue(pp[i].getResidueSubsetB());
                if (n>0) sb.a(pp[i]).a(": ").a(n).a(" c-Alpha-Atoms selected for superposition\n");
            }
            if (result!=null) result.toText(sb.a('\n'), Superimpose3D.PRINT_HUMAN_READABLE);
            _ta.t(sb);
            setEnabld(true, _butApply);
            final Protein3d.PView pvM= _p3D.getView(pp[1]);
            if (pvM!=null) {
                pvM.transformPreview(_m3d);
                ChDelay.repaint(_p3D,333);
            }
        }
        finally {
            pcp("COMPUTING",null,this);
            revalAndRepaintC(_pSouth);
            if (_p3D!=null) _p3D.repaint();
        }
    }
}
