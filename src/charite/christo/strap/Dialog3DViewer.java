package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**HELP
    <i>PACKAGE:charite.christo.strap.extensions.</i>
    <div class="floatR">
      <table border="1" summary="3D-viewers">
        <caption>Comparing different 3D-viewers</caption>
        <tr>
          <th></th>
          <th><i>CLASS_REF:ChJmolPROXY</i>JMOL</th>
          <th><i>CLASS_REF:Pymol</i>PYMOL</th>
          <th><i>CLASS_REF:ChAstexPROXY</i>OpenAstex</th>
        </tr>
        <tr>
          <th>Installed by Strap</th>
          <td>always</td>
          <td>Linux</td>
          <td>Linux, Windows, Mac</td>
          <td>always</td>
        </tr>
        <tr>
          <th>Operation system</th>
          <td>all</td>
          <td>X-Windows</td>
          <td>all</td>
          <td>all</td>
        </tr>
        <tr>
          <th>Performance</th>
          <td>medium</td>
          <td>high</td>
          <td>high with accelerated video cards</td>
          <td>high</td>
        </tr>
        <tr>
          <th>Memory consumption</th>
          <td>high</td>
          <td>low</td>
          <td>medium</td>
          <td>medium</td>
        </tr>
        <tr>
          <th># models per view</th>
          <td>many</td>
          <td>1</td>
          <td>many</td>
          <td>many</td>
        </tr>

        <tr>
          <th>Shows superimposed proteins</th>
          <td>yes</td>
          <td>no</td>
          <td>yes</td>
          <td>yes</td>
        </tr>
      </table>
      <br>
    </div>

    
    The alignment pane and the 3D-view are
    interoperable.

    Related links:  http://biodesign.asu.edu/labs/assets/lindsay/molvis/MOL.html
    <i>SEE_DIALOG:V3dUtils,H</i>
    @author Christoph Gille

*/
public class Dialog3DViewer extends AbstractDialogJPanel implements  java.awt.event.ActionListener{
    private final ChCombo _choiceV, _choiceP;

    public Dialog3DViewer() {
        _choiceV=SPUtils.classChoice(ProteinViewer.class).save(Dialog3DViewer.class,"ProteinViewer");
        _choiceP=new ProteinCombo(ProteinList.CALPHA);
        final String send[]=ProteinViewerPopup.TOG_SEND;
        final JComponent
            pMovie=pnl(HBL, WATCH_MOVIE+MOVIE_Drag_protein_to_3D),
            pCenter=pnl(VBHB,dialogHead(this), " ",
                        pnl(HBL,"Protein:", _choiceP)," ",
                        _choiceV.panel(),
                        pnl(new ChButton("GO").t(ChButton.GO).li(this)),
                        V3dUtils.pvp().b(send[0]).cb(),
                        V3dUtils.pvp().b(send[1]).cb(),
                        pnlTogglOpts("Also see",pMovie),
                        pMovie
                        );
        add(pCenter);
    }
    public void actionPerformed(java.awt.event.ActionEvent ev)  {
        if ("GO"==ev.getActionCommand()) {
            final Protein p=SPUtils.sp(_choiceP);
            if (p==null) return;
            if (p.getResidueCalpha()==null) {
                if (ChMsg.yesNo("The protein "+p+" does not have 3D-coordinates.<br>Find suitable PDB file?")) {
                    StrapAlign.addDialogC(CLASS_DialogSimilarStructure);
                }
            } else {
                if (p.getName().endsWith(".dssp") && !ChMsg.yesNo("The dssp file format is usually not supported.<br>Continue anyway?")) return;
                final String send[]=ProteinViewerPopup.TOG_SEND;
                final int opt=
                    (isCtrl(ev)?V3dUtils.OPEN_VERBOSE:0)
                    | (V3dUtils.pvp().b(send[1]).s()?V3dUtils.OPEN_SEND_ALL_ANNOTATIONS:0)
                    | (V3dUtils.pvp().b(send[0]).s()?V3dUtils.OPEN_INIT_SCRIPT:0);

                V3dUtils.open3dViewer(opt, new Protein[]{p},_choiceV);
                setNextProcessHasLogPanel(false);
            }
        }
    }

}
/* NOC */
