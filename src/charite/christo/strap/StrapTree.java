package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.ProteinMC.*;
import static charite.christo.ChUtils.*;
import static java.awt.event.MouseEvent.*;

/**HELP

<b>Context menus in Strap:</b>

The term WIKI:Context_menu is commonly used for menus which pop up when right
clicking (<b>right mouse button</b>) an item in a graphical user interface,
offering a list of options which vary depending on the item selected.
Watch movie <i>STRING:ChConstants#MOVIE_Context_Menu</i>
<br><br>
<i>OS:M, On Macintosh computers the right mouse button is often deactivated.
It can be activated: Image:Cust_mac_mouse*.
Alternatively, the right mouse button can be simulated by  Alt+Ctrl+left-click.
</i>
<br><br>
Context menus are available for example for proteins, for residue selections, for the alignment panel and for the rubber band selection.

<br><br><b>Selecting single items:</b>
List items are selected simply by left-click the respective node.
Single residue selections in the alignment panel can be selected by clicking with the Ctrl-key.

<br><br><b>Selecting more than one item:</b>
Selecting more than one list item requires the <b>Shift</b> and <b>Ctrl</b> keys.
The Ctrl-key is located at the lower left of the key board and is sometimes labeled  <b>Strg</b> and
the Shift-key is sometimes termed "Umschalt".

<br><br><b>Selecting residue selections:</b>
Residue selections which are highlighted in the alignment panel can be selected by Ctrl+left-click.
Selected residue selections are indicated by marching ants.

By dragging a rectangular region in the alignment, all contained
residue selections are selected. With the Shift or Ctrl-key the
union-set or cut-set is formed between already selected items and those inside the
rectangle.

<br><br><b>Frequently used menu items</b> can be dragged out the menu and placed on the desktop.
Instead of clicking a menu-item the user drags the menu-item to the desktop.
Being on the desktop, it can now be accessed much easier.
When the menu-item on the desktop is not needed any more, it can be
deleted.

<br><br><b>The tree view</b> is located at the left of the application.
It is usually  hidden and can be opened by dragging the vertical divider bar.
The tree contains all loaded proteins and their child objects.

@author Christoph Gille
*/
public class StrapTree extends ChJTree implements ChRunnable, ProcessEv  {
    /* >>> Instance >>> */
    public final static String ACTION_SELECT="S";
    private boolean _painted;
    public StrapTree() {
        super(ChJTable.DRAG_ENABLED|ChJTable.DEFAULT_RENDERER,null);
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Toolbar  >>> */
    private static AbstractButton[] _radioGrp;
    private Set _vMatching;
    private static int _radioIdx;
    public static int idxRadioSearch() { return _radioIdx;}
    public void setMatchingItems(Object[] oo) {
        adAllUniq(oo, StrapAlign.selectedObjectsV());
        StrapEvent.dispatchLater(StrapEvent.OBJECTS_SELECTED,99);
    }

    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Annotation Group >>> */
    private void renameGroupOfAnnotations(UniqueList<ResidueAnnotation> l) {
        final String oldName=l.getText();
        final String str=ChMsg.input("Please enter the new name for this group of annotations",30,oldName);
        if (str!=null) {
            final String newName=str.trim();
            if (!"".equals(newName)) {
                for(ResidueAnnotation s: l.asArray()) {
                    if (oldName.equals(s.value(ResidueAnnotation.GROUP)))
                        s.setValue(0,ResidueAnnotation.GROUP,newName);
                }
                l.t(newName);
            }
        }
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
    }
    /* <<< Annotation Group  <<< */
    /* ---------------------------------------- */
    /* >>> Repaint  >>> */
    public void paintComponent(Graphics g1) {
        if (!_painted) {
            _painted=true;
            inEdtLaterCR(this,"I",null);
        }
        try { super.paintComponent(g1); } catch(Throwable e){ stckTrc(e);}
    }
    private static int _updateMC,_repaintMC ;

    void maybeUpdate() {
        final int modi[]=Protein.MC_GLOBAL;
        if(!_painted) return;
        {
            final int mc=modi[MC_RESIDUE_ANNOTATION_GROUPS] + modi[MCA_PROTEINS_V] + modi[MC_RESIDUE_SELECTIONS] +
                modi[MC_PROTEIN_USER_OBJECTS_V] + modi[MC_PROTEIN_VIEWERS_V];
            if (_updateMC!=mc) {
                _updateMC=mc;
                updateKeepStateLater(0L, StrapAlign.getProteins().length/10);
            }
        }
        {
            final int mc= modi[MC_PROTEIN_LABELS]+ modi[MC_RESIDUE_SELECTION_LABELS];
            if (_repaintMC!=mc) {
                _repaintMC=mc;
                repaint();
            }
        }
    }
    /* <<< Repaint  <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent  >>> */

    private static DialogStringMatch _dsm;
    public void selectDialog() {
        DialogStringMatch d=_dsm;
        if (d==null) {
            _dsm=d=new DialogStringMatch(0, this, _vMatching=new HashSet(), "StrapTree");
            _radioGrp=radioGrp("Protein names;Protein header;Annotations;Sequence".split(";"),0, null);
            pcp(KEY_NORTH_PANEL, pnl("Search in ", _radioGrp),d);
            addActLi(LI,d);
        }
        d.showInFrame();
    }

    final EvAdapter LI=new EvAdapter(this);
    public void processEv(AWTEvent ev) {
        final Object q=ev.getSource();
        final JComponent qjc=deref(q,JComponent.class);
        final String cmd=actionCommand(ev);
        final List vSel=StrapAlign.selectedObjectsV();
        final StrapView view=StrapAlign.alignmentPanel();
        final int mc=modic(vSel), id=ev.getID(), kcode=keyCode(ev);
        if (isPopupTrggr(false,ev)) {
            if (isPopupTrggr(true,ev)) {
                final Object oMouse=objectAt(null,ev);
                if (oMouse instanceof UniqueList && ((UniqueList)oMouse).getClazz()==ResidueAnnotation.class) renameGroupOfAnnotations((UniqueList)oMouse);
                else {
                    final char menuType=ContextObjects.menuID(oMouse);
                    if (menuType!=0) ContextObjects.openMenu(menuType, oo(oMouse), getSelValues(q));
                }
            }
            return;
        }
        if (cmd==ACTION_SELECT) selectDialog();
        if (cmd==DialogStringMatch.ACTION_BEFORE) _radioIdx=radioGrpIdx(_radioGrp);

        if (cmd==DialogStringMatch.ACTION_FINISHED && _vMatching!=null) {
            vSel.addAll(_vMatching);
            _radioIdx=-1;
        }
        if (id==MOUSE_ENTERED || id==MOUSE_EXITED) {
            maybeUpdate();
        }

        if ( (id==MOUSE_MOVED || id==MOUSE_EXITED) && view!=null) {
            final Protein p=id==MOUSE_EXITED ? null : SPUtils.sp(objectAt(null,ev));
            view.highlightProteins(oo(p), (Color) orO(colr(p),Color.RED));

        }
        if (doubleClck(ev)) {
            final Object o=objectAt(null,ev);
            StrapAlign.editAnnotation(deref(o,ResidueAnnotation.class));
            if (o instanceof UniqueList && ((UniqueList)o).getClazz()==ResidueAnnotation.class) {
                renameGroupOfAnnotations((UniqueList<ResidueAnnotation>)o);
            }
            if (qjc!=null) qjc.requestFocus();
        }
        if (qjc!=null && gcp(StrapAlign.KOPT_DELETE_DISABLED,qjc)==null && (id==KeyEvent.KEY_PRESSED && (kcode==KeyEvent.VK_BACK_SPACE || kcode==KeyEvent.VK_DELETE))) {
            boolean selResidues=false;
            final Class cc[]={Protein.class, UniqueList.class, ResidueAnnotation.class, ResidueSelection.class, HeteroCompound.class};
            final ChJList jList=deref(q,ChJList.class);
            final Object[] ooJL=getSelValues(jList);
            Collection vDelProt=null;
            if (gcp(StrapAlign.OBJECT_JLIST,jList)!=null && 0==(jList.options()&ChJTable.CHJTABLE_BACKSPACE_DEACTIVATED)) {
                if (ChMsg.yesNo("Remove the selected items from this list?")) {
                    for(int i=sze(ooJL); --i>=0;) jList.getList().remove(ooJL[i]);
                    jList.repaint();
                }
            } else {
                nxtClass:
                for(Class c : cc) {
                    for(boolean del : FALSE_TRUE) {
                        int count=0;
                        final Object[] ooSel=jList!=null ? ooJL : oo(vSel);
                        if (sze(ooSel)==0) break;
                        for(Object o : ooSel) {
                            for(Class c2 : cc) if (!isAssignblFrm(c,o) || c!=c2 && isAssignblFrm(c,c2) && isAssignblFrm(c2,o)) continue nxtClass;
                            final Protein prot=deref(o, Protein.class);
                            final ResidueSelection selection=deref(o, ResidueSelection.class);
                            final HeteroCompound hetero=deref(o, HeteroCompound.class);
                            final UniqueList<ResidueSelection> g=deref(o,UniqueList.class);
                            if (g!=null && !isAssignblFrm(ResidueSelection.class, g.getClazz())) continue;
                            count++;
                            if (del) {
                                if (hetero!=null) {
                                    for(Protein p : StrapAlign.getProteins()) p.removeHeteroCompound(hetero);

                                }
                                if (prot!=null) {
                                    StrapAlign.rmProteins(false,prot);
                                    for(ResidueSelection  s: prot.allResidueSelections()) vSel.remove(s);
                                    vDelProt=adNotNullNew(prot,vDelProt);
                                }
                                final ResidueSelection[] ss= selection!=null ? new ResidueSelection[]{selection} : g!=null ? g.asArray() : null;
                                if (sze(ss)>0) {
                                    for(ResidueSelection s : ss) {
                                        final Protein p=s.getProtein();
                                        if (p!=null) p.removeResidueSelection(s);
                                        dispos(s);
                                        vSel.remove(s);
                                    }
                                }
                                vSel.remove(o);
                                selResidues=true;
                            }
                        }
                        final String n= c==HeteroCompound.class ? "hetero compounds/nucleotides" : c==Protein.class ? "protein" :  c==ResidueAnnotation.class ? "residue annotation" :  c==ResidueSelection.class ? "residue selection" : c==UniqueList.class?"residue selection group" :  "";
                        if (count>0 && !del && !ChMsg.yesNo(plrl(count,"Delete %N "+n+"%S"))) break;
                    }
                }
                ChDelay.repaint(qjc,333);
            }
            if (jList!=null) jList.setSelOO(ooJL);
            if (sze(vDelProt)>0) StrapEvent.dispatch(StrapEvent.PROTEINS_KILLED);
            if (selResidues) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,1);
        }
        if (cmd==ACTION_SELECTION_CHANGED) {
            if (gcp(KEY_IGNORE_SELECTION_EVT,this)==null && StrapView.anyPainted()) {
                vSel.clear();
                ((UniqueList)vSel).rmOptions(UniqueList.UNIQUE);
                adAll(getSelValues(this),vSel);
                ((UniqueList)vSel).addOptions(UniqueList.UNIQUE);
            }
        }
        if (mc!=modic(vSel)) StrapEvent.dispatchLater(StrapEvent.OBJECTS_SELECTED,1);
    }

    /* <<< Repaint <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    private final Object _root2[]={null,null};
    public Object run(String id, Object arg) {
        if (id==PROVIDE_JTREE_CHILDS) {
            final StrapView pan=StrapAlign.alignmentPanel();
            if (pan==null) return StrapAlign.vProteins();
            final List vHidden=StrapAlign.ppNotAliV();
            if (sze(vHidden)==0) return pan.visibleProteins();
            _root2[0]=StrapAlign.ppAliV();
            _root2[1]=vHidden;
            return _root2;
        }
        if (id==DialogStringMatch.PROVIDE_HAYSTACK || id==PROVIDE_WORD_COMPLETION_LIST) return StrapAlign.getProteins();
        if (id=="I") {
            setRowHeight(ICON_HEIGHT);
            setModel(new ChTreeModel(0L,this));
            setRootVisible(false);
            setShowsRootHandles(true);
            rtt(this);
            addMoli(MOLI_REFRESH_TIP,this);
            LI.addTo("kmM",this);
            addActLi(LI,this);
            pcp(KEY_SCROLL_INCR_V,intObjct(32),this);
            scrllpn(this)
                .addMenuItem(new ChButton(ACTION_SELECT).t("Search").i(IC_SEARCH).li(LI))
                .addMenuItem(new int[]{ChJScrollPane.AS_TXT, ChJScrollPane.EXPAND_NODES, ChJScrollPane.UPDATE});
        }
        return null;
    }
    /* <<< Thread <<< */

}
