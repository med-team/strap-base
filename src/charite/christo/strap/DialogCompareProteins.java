package charite.christo.strap;
import charite.christo.*;
import java.awt.*;
import javax.swing.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

This dialog allows the pairwise comparison of a set of proteins.
<i>PACKAGE:charite.christo.strap.extensions.</i>

Numeric (float) values are computed for pairs of proteins using the method selected by the user.

As an example  "sequence dissimilarity" yields the proportion of amino acids that are different
in both sequences.

<br>
These values can be visualized as:
<ul>
<li>A table with each row and column representing a protein.</li>

<li>A graph of proteins. The  proteins are the graph nodes.
The length of the  graph edges which connect  the  protein nodes   represent the  distance.</li>

</ul>

<i>SEE_DIALOG:DialogBarChart</i>

@author Christoph Gille

*/
public class DialogCompareProteins extends AbstractDialogJTabbedPane implements java.awt.event.ActionListener {
    private final ChTextField _tfFrom, _tfTo;
    private final ProteinList _selOfProtRow, _selOfProtCol;
    private final ChCombo _comboClass;
    private final JComponent _pMain,_listProtRow,_listProtCol, _pBoth=pnl(new GridLayout(1,2));
    private void layoutPCenter() {
        _pMain.remove(_listProtRow);
        _pMain.remove(_pBoth);
        _pBoth.removeAll();
        switch(RADIO_GRAPH_TABLE.i()) {
        case 0: _pMain.add(_listProtRow,BorderLayout.CENTER); break;
        case 1:
            _pBoth.add(_listProtRow);
            _pBoth.add(_listProtCol);
            _pMain.add(_pBoth,BorderLayout.CENTER);
            break;
        case 2:
            _pMain.add(_pBoth,BorderLayout.CENTER);
            break;
        }
        revalAndRepaintC(_pMain);
    }

    private final ChCombo RADIO_GRAPH_TABLE=new ChCombo("graph","table (n x m matrix)","list of protein pairs").li(this).save(getClass(),"mode");

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        StrapAlign.errorMsg("");
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        if (q==RADIO_GRAPH_TABLE) { revalAndRepaintC(_comboClass); layoutPCenter();}
        if (q==_comboClass) { _selOfProtRow.repaint(); _selOfProtCol.repaint();}

        if (cmd=="GO") {
           Protein
                ppRow[]=_selOfProtRow.selectedProteins(),
                ppCol[]=_selOfProtCol.selectedProteins();
            int from=atoi(_tfFrom)-1, to=atoi(_tfTo);
            if (to==99999) to=MAX_INT;
            if (from<0) from=0;
            Container result=null;
            String tab=null;
            if (ppRow.length==0) ppRow=StrapAlign.proteins();
            final Object sharedInst=_comboClass.instanceOfSelectedClass(null);
            switch(RADIO_GRAPH_TABLE.i()) {
            case 0:
                if (ppRow.length>2) {
                    result=new DialogCompareProteinsResult(_comboClass,sharedInst,ppRow,null,from,to).getPanel();
                    tab="graph representation";
                }
                else StrapAlign.errorMsg("Select at least 3 proteins");
                break;
            case 1:
                if (ppCol.length==0) {
                    ppCol=ppRow.length<2 ? StrapAlign.proteins() :  ppRow;
                }
                tab="table";
                result=new DialogCompareProteinsResult(_comboClass,sharedInst,ppRow,ppCol,from,to).getPanel();
                break;
            case 2:
                tab="pairs";

                result=new ResultCompareProteinPairs(_comboClass,sharedInst,from,to);
                break;
            }
            adTab(DISPOSE_CtrlW, tab,result, this);
        }
    }
    public DialogCompareProteins() {
        _selOfProtRow=new ProteinList(0L).selectAll(0L);
        _selOfProtCol=new ProteinList(0L).selectAll(0L);
        _listProtRow=_selOfProtRow.scrollPane();
        _listProtCol=_selOfProtCol.scrollPane();
        _tfFrom=new ChTextField("1").cols(6,true,true);
        _tfTo=new ChTextField("99999").cols(6,true,true);
        _comboClass=SPUtils.classChoice2(CompareTwoProteins.class, SequenceAlignmentScore2.class).save(DialogCompareProteins.class,"clazz");
        pcp(KEY_ENABLED,_comboClass,_selOfProtCol);
        pcp(KEY_ENABLED,_comboClass,_selOfProtRow);
        _comboClass.li(this);
        final Object
            pDetails=pnl(VBHB,
                         pnl(FLOWLEFT,"Alignment range  from: ",_tfFrom,"  to: ",_tfTo,"(inclusive) "),
                         buttn(TOG_CACHE).cb(),
                         buttn(TOG_ALI_NOT_COMMUT).cb()

                         ),
            bGo=new ChButton("GO").t(ChButton.GO).li(this).tt("show as table or graph"),
            pSouth=pnl(VB,
                       _comboClass.panel(),
                       pnl(HBL, pnlTogglOpts(pDetails), "#", "View option: ",RADIO_GRAPH_TABLE," ",bGo, "#"),
                       pDetails
                       );
        _pMain=pnl(CNSEW,null,dialogHead(this), pSouth);
        layoutPCenter();
        adMainTab(remainSpcS(_pMain),this, getClass());
    }

}
