package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import java.awt.Component;
import java.util.*;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.Protein3dUtils.*;
import static charite.christo.strap.V3dUtils.*;
import static charite.christo.protein.ProteinViewer.*;
import static java.awt.event.MouseEvent.*;
/**
   The menu for protein viewers (Pymol, Jmol ... ).
   @author Christoph Gille
*/
public class ProteinViewerPopup implements java.awt.event.ActionListener, IsEnabled, ChRunnable {
    private final static String
HINT_CLICKING_S="Clicking a list item selects the corresponding amino acids in 3D",
        BUT_GEN_MENUS="PVU$$KGM", MENU_OTHER_BAR="   \u25baOther-menubar",  KEY_WORD0="PVP$$w0",
        SELECT_N="-99-$NTERM", SELECT_C="$CTERM-9999", SELECT_NC="-99-$NTERM,$CTERM-9999",
        HIDE_NC=COMMANDselect+" "+SELECT_NC+"\n"+COMMAND_HIDE_EVERYTHING;

    final static String TOG_SEND[]={"Send init script","Send 3D-commands attached to residue selections"};
    private final ContextObjects _objects;
    private final ChButton
        _labTitle=labl().cp(KOPT_NOT_FOR_MENUBAR,""),
        _labCtxt=labl("Note: This 3D-View has a context menu (Right click the 3D-pane)")
        .cp(KOPT_HIDE_IF_DISABLED,"")
        .cp(KEY_ENABLED,this)
        .fg(0xAA);
    private final boolean _immutable;
    public ProteinViewerPopup(boolean immutable, ContextObjects o) {
        _immutable=immutable;
        _objects=o;
    }
    /* ---------------------------------------- */
    /* >>> ActionListener  >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        if (q==null) return;
        final int modi=modifrs(ev);
        final boolean shift=0!=(modi&SHIFT_MASK), ctrl=0!=(modi&CTRL_MASK);
        Protein3d p3d=null;
        final List<Protein> vAllPP=new Vector();
        for(ProteinViewer pv : _objects.proteinViewers()) {
            final Protein p=p(pv);
            if (p==null) continue;
            final boolean isBackbone=pv instanceof Protein3d.PView;
            final Class clazz=pv.getClass();
            final Component panel=viewerCanvas(pv);
            if (panel instanceof Protein3d) p3d=(Protein3d)panel;
            final Protein pp[]=ppInV3D(pv);
            adAllUniq(pp,vAllPP);
            if (cmd=="AOT" || cmd=="AOToff") setWndwStateT(cmd=="AOT" ? 'T':'t', -1,-1,(String)pv.getProperty(GET_FRAME_TITLE));
            if (cmd=="SUPER_GO" || cmd=="SUPER_COMPLEXES_GO") {
                StrapEvent.dispatchLater(StrapEvent.COMPUTATION_STARTED,333);
                final String msg="It seems that a superposition job is already running.<br>Start new job anyway?";
                if (sze(pp)<2) error("Superposition requires at least \n"+
                                     "two proteins in the same 3D-view.\n\n"+
                                     "But only "+sze(pp)+" protein  is in this view\n\n"+
                                     (viewerCanvas(pv)==null?"":
                                      "Use WIKI:Drag_and_drop to load\n"+
                                      "at least one more protein.\n\n"+
                                      "Watch "+MOVIE_Drag_protein_to_3D));
                else if (!SPUtils.SUPERIMPOSING[0] || ChMsg.yesNo(msg)) {
                    final List<Protein> v=new Vector();
                    for(int i=pp.length; --i>=0;) {
                        final Matrix3D m3d=pp[i].getRotationAndTranslation();
                        pcp("SUPER_UNDO",m3d!=null ? m3d : new Matrix3D(),pp[i]);
                        if (pp[i].getResidueCalpha()!=null) v.add(pp[i]);
                    }
                    if (cmd=="SUPER_COMPLEXES_GO") {
                        final List vRemove=new Vector();
                        for(int i=sze(v); --i>=0;) {
                            final Protein protein=get(i,v,Protein.class);
                            if (protein==null) continue;
                            final Protein ppRemove[]=protein.getProteinsSameComplex();
                            if (ppRemove.length>0) {
                                for(Protein pR : ppRemove) v.remove(pR);
                            }
                        }
                        v.removeAll(vRemove);
                    }
                    final int options=cmd=="SUPER_COMPLEXES_GO" ? SPUtils.SUPERIMP_COMPLEX_BEST:0;
                    startThrd(SPUtils.threadSuperimposeProteins(options|SPUtils.SUPERIMP_EVENT, v,CAN_BE_STOPPED, null));
                }
            }
            for(int i=pp.length; --i>=0;) {
                if (cmd=="SUPER_UNDO" || cmd=="SUPER_HOME") {
                    pp[i].setRotationAndTranslation(cmd=="SUPER_HOME" ? null : gcp("SUPER_UNDO",pp[i],Matrix3D.class));
                    StrapEvent.dispatchLater(StrapEvent.PROTEIN_3D_MOVED, 111);
                }
            }
            if (cmd=="SUPER_STOP") stopAllAlignments();
            if (cmd==IC_CAMERA) {
                final File f=file(STRAPOUT+"/images/"+niceShrtClassNam(pv)+".png");
                writePng(0,panel,f);
                visitURL(f, modi);
            }
            if (cmd=="INFO") showHelp(isBackbone ? Protein3d.class : clazz);
            if (cmd=="H") showHelp(V3dUtils.class);
            if (cmd==IC_JAVA) showJavaSource(clasNam(pv),(String[])null);
            if (cmd=="CTRL") shwCtrlPnl(CLOSE_CtrlW_ESC|ChFrame.AT_CLICK, "Ctrl "+pv.getProtein()+" @ "+niceShrtClassNam(pv), pv);
            if (cmd=="CUST") Customize.addDialog(Customize.cc(pv));
            if (cmd=="LOG" || cmd=="LOGg" || cmd=="SAVE") showLogPanel(cmd!="LOG", pv);
            if (cmd=="LOAD") loadSessionI();
            if (cmd==JFileChooser.APPROVE_SELECTION) loadSession(SESSION_LOAD_PROTS, pv, _fs.getSelectedFile());
            if (cmd=="EXPORT") {
                Object msg=gcp(cmd,this);
                if (msg==null) {
                    msg=pnl(VB,
                           "<br><b><u>Export using Drag-and-Drop</u></b><br>"+
                           "To export proteins use Drag-and-Drop:<br>"+
                           "Start dragging the mouse on a protein label and drop the protein on the desktop.<br>"+
                           "Protein labels contain the protein name and maybe a protein icon.<br>"+
                           "They are found in the alignment panel or with above menu item \"List proteins\".",
                           WATCH_MOVIE+MOVIE_Export_Proteins,
                           "<br><b><u>Export using the context menu</u></b><br>"+
                           "Alternatively, you can use the export-function in the context menu.",
                           StrapAlign.b(CLASS_DialogExportProteins),
                           WATCH_MOVIE+MOVIE_Context_Menu);
                    pcp(cmd,msg,this);
                }
                ChFrame.frame(cmd, msg, CLOSE_CtrlW_ESC).shw(ChFrame.AT_CLICK|ChFrame.PACK);
            }
            final int iList=idxOf(cmd,V3dListModel.TITLES);
            if (iList>=0) {
                final ChJList jl=V3dListModel.jList(iList,true,1);
                Component pSouth=gcp(KEY_SOUTH_PANEL, jl, Component.class);
                if (pSouth==null) {
                    if (iList==V3dListModel.HET || iList==V3dListModel.NUCL || idxOf(iList, V3dListModel.TYPES_SEL)>=0) {
                        pSouth=labl(HINT_CLICKING_S);
                    }
                    if (iList==V3dListModel.PROTEINS) {
                        final String msg=
                            "<h2>Drag'n Drop for proteins and Residue selections</h2>"+
                            "Dropping proteins here, will display these proteins in the current 3D-view. "+
                            MOVIE_Drag_protein_to_3D+"<br>"+
                            "This is essential for those 3D-views such as Pymol where the 3D-pane itself is not a drop target for proteins. "+
                            "<br>"+
                            "By dropping residue selections (underlined residues in the alignment panel) "+
                            "the respective amino acids will be selected in the 3D-view."+
                            "<h2>Keep this window on top of all other windows</h2>"+
                            "If his window loses the focus and becomes covert by another window, it is not possible to drop proteins anymore.<br>"+
                            "This can be avoided: Typing Ctrl+T, toggles floating on top on/off. "+
                            "<h2>Miscellaneous</h2>"+
                            "Proteins in this list can be copied anywhere with Drag'n Drop.<br> "+
                            "Right click opens the context menu of the respective protein.";

                        final Object
                            b=new ChButton("RM").li(this).cp("RM",wref(jl)).enabled(false).t("Remove selected proteins from view"),
                            pInfo=new ChJTextPane(msg);
                        pSouth=pnl(VBPNL, b,
                                   pnl(HBL,"Hint:  Proteins or residue selections can be dragged here. ", pnlTogglOpts("*Explain",pInfo)),
                                   pInfo);
                        addCP(ChJList.KEY_ENABLE_IF_SELECTED, wref(b), jl);
                        for(JComponent c : childsR(pSouth,JComponent.class)) {
                            StrapAlign.newDropTarget(c,false);
                            pcp(KEY_DROP_TARGET_REDIRECT, jl,c);
                        }
                    }
                    pcp(KEY_SOUTH_PANEL,pSouth,jl);

                }
                jl.showInFrame(ChFrame.AT_CLICK|CLOSE_CtrlW_ESC,cmd);
                enableDisable();
            }
            if (cmd=="STRAP_MB") { StrapAlign.setMenuBar(null, 0,0); StrapAlign.setToolpane(null); }
            if (cmd==BUT_GEN_MENUS  || cmd==BUT_NATIV_MENUS) {
                pcp(KEY_WHICH_MENU,cmd, viewerCanvas(pv));
                setPV(PV_FOCUSED, pv);
            }
            if (cmd=="RM") {
                final ProteinViewer vv[]=vvSharingView(pv), vvDel[]=new ProteinViewer[vv.length];
                ProteinViewer focused=null;
                int iDel=0;
                final ChJList jl=gcp("RM",q,ChJList.class);
                final Object ppDel[]=jl==null?null:jl.getSelectedValues();
                for(ProteinViewer v : vv) {
                    if (cntains(p(v),ppDel)) vvDel[iDel++]=v;
                    focused=v;
                }
                if (iDel>0) disposeV(vvDel);
                if (focused!=null) setPV(PV_FOCUSED, focused);
            }
            if (cmd=="PV" || cmd==BUT_DEFAULT_VIEWER) {
                final Class c3d=clas(cmd==BUT_DEFAULT_VIEWER ? StrapAlign.defaultClass(ProteinViewer.class) :  gcp(ProteinViewer.class,q));
                if (sze(pp)>5 && !ChMsg.yesNo("Sure you want to open "+sze(pp)+" chains in "+niceShrtClassNam(c3d)+" ?")) continue;

                final boolean isPreview=p3d!=null && p3d.isPreview();
                final long options=
                    (ctrl?OPEN_VERBOSE:0L)|
                    (b(TOG_SEND[1]).s() ? OPEN_SEND_ALL_ANNOTATIONS:0) |
                    (b(TOG_SEND[0]).s() ? OPEN_INIT_SCRIPT:0) |
                    (isPreview ? OPEN_EACH_PROTEIN_OWN_COLOR : 0);

                final ProteinViewer pvNew=c3d==null ? null : open3dViewer(options, pp, c3d);

                if (isPreview) {
                    final ProteinViewer vvNew[]=vvSharingView(pvNew);
                    for(ProteinViewer v0 : vvSharingView(pv)) {
                        for(ProteinViewer v : vvNew) {
                            if (p(v)==p(v0)) {
                                V3dUtils.transform3D(((Protein3d.PView)v0).getTransformationPreview(),v);
                                pcp(KOPT_IS_PREVIEW,"",v);
                            }
                        }
                    }
                }
                if (pvNew!=null && !isBackbone) {
                    final Runnable run=thrdCR(this,"CPY_SESSION", new Object[]{ log3D(true, pv,""), wref(pvNew)});
                    final Object
                        b1=new ChButton("Apply?").r(run).doClose(0,REP_PARENT_WINDOW),
                        b2=new ChButton("Cancel").doClose(0,REP_PARENT_WINDOW),
                        pnl=pnl(VBHB,
                                "You opened a new 3D-view<br>"+
                                "Also apply the rendering styles<br>"+
                                "from the previous 3D-view?",
                                pnl(b1,b2));
                    new ChFrame("Yes-No").ad(pnl).shw(ChFrame.PACK|ChFrame.AT_CLICK|ChFrame.ALWAYS_ON_TOP|CLOSE_CtrlW_ESC);
                }
            }
            if (cmd=="LOG" || cmd=="LOGg") {
                final BA ba=log3D(cmd=="LOGg", pv, "");
                final ChLogger logger=ba==null?null:(ChLogger)ba.getSendTo();
                if (logger!=null) ChFrame.frame(gcps(KEY_TITLE,logger), logger.getPanel(0), CLOSE_CtrlW_ESC).shw(ChFrame.AT_CLICK);
            }

            if (cmd=="SCRIPT") showScriptPanel();
            { /* --- Selections --- */
                Object select=chrAt(0,cmd)=='$' || chrAt(0,cmd)=='-'&&chrAt(1,cmd)=='9' ? cmd : null;
                if (cmd=="SEL_I") {
                    final boolean bb[]=selection3dToBoolZ(getSelection3D(pv), p);
                    for(int i=sze(bb); --i>=0;) bb[i]=!bb[i];
                    select=bb;
                }
                if (cmd=="SEL_H" ||  cmd=="SEL_E" ||  cmd=="SEL_HE") {
                    final byte ss[]=p.getResidueSecStrType();
                    if (sze(ss)>0) {
                        final boolean bb[]=new boolean[ss.length];
                        for(int i=ss.length; --i>=0;) bb[i]=strchr(0L, (char)ss[i], cmd, 4, MAX_INT)>=0;
                        select=bb;
                    }
                }
                final Selection3D ss3D[]=
                    select instanceof boolean[] ? boolToSelection3D(false, (boolean[])select,0,p,"") :
                    select instanceof String ? textToSelection3D((String)select, p):
                    null;
                if (ss3D!=null) {
                    setPV(PV_FOCUSED,pv);
                    setSelection3D(0L, ss3D, "",  pv);
                }
            }
            final String word0=gcps(KEY_WORD0, q);
            if (word0!=null && word0!=COMMANDcolor) {
                sendCommand(0L,  shift&&strEquls(word0,cmd,0) ? rplcToStrg(" on", " off", cmd) : cmd, pv);
            }
        }/* for(ProteinViewer pv */
        if (cmd=="CUST_INIT") {
            for(Object o : StrapPlugins.allClassesV(ProteinViewer.class).asArray()) {
                Customize.addDialog(Customize.getCustomizableForClass(clas(o), Customize.CLASS_InitCommands));
            }
        }
        if (cmd==ACTION_BECOME_INVISIBLE) { /* Otherwise the menus are not reacting */
            inEDTms(thrdCR(this, "SET_PV"), 99);
        }
        if (cmd=="WIRE_HETEROS") {
            final String s="This can be done in the tool-panel.<br>"+
                "It is opened with the \"List proteins\"-toggle in the lower right corner.";
            ChMsg.msgDialog(0L, s);
        }
        if (cmd=="MBT") {
            final List vID=new Vector();
            for(Protein p : toArry(vAllPP,Protein.class)) adUniq(delLstCmpnt(delPfx("PDB:",p.getPdbID()),'_'),vID);
            if (sze(vAllPP)==0) error("No PDB ID");
            else {
                final ChCombo comboPdbID=new ChCombo(0L,vID);
                final Object msg=pnl(VBHB,"Starting MBT viewer. see",
                                      "http://mbt.sdsc.edu/",
                                      "This requires a modern computer and fast internet.",
                                      "PCs with older video cards may crash.",
                                      "Very large proteins may cause problems.",
                                      pnl(HB,"PDB ID ", comboPdbID));
                final int choice=ChMsg.option(msg,new String[]{"Simple Viewer", "Protein Workshop","Cancel"});
                final String pdbID=toStrg(comboPdbID), view=choice==0?"SV":choice==1?"PW":null;
                if (view!=null) visitURL("http://www.pdb.org/pdb/Viewers/RCSBViewers/view.jsp?viewerType="+view+"&structureId="+pdbID+"&structIdFromStrutsAction="+pdbID,0);
            }
        }

        if (p3d!=null) {
            if (cmd=="WIRE_DIFF_COLORc" || cmd=="WIRE_DIFF_COLOR") {
                p3d.eachChainOneColor(cmd=="WIRE_DIFF_COLORc",false);
                repaintC(p3d);
                p3d.enableDisable();
            }
            if (cmd=="WIRE_HIDE_DIST_HET") p3d.setHideDistantDNS(isSelctd(q));
        }
    }
    /* <<< ActionEvent <<< */
    /* ---------------------------------------- */
    /* >>> Menu >>> */
    Object[] m(char id) {
        final boolean mac=isScreenMenuBar();
        if((id|32)=='b') {
            return new Object[]{
                MENU_OTHER_BAR,
                mac?null : b("STRAP_MB").t("Menu-bar of the Strap program (Or simply click into the scroll-bar of the alignment)"),
                b(id=='b'?BUT_GEN_MENUS:BUT_NATIV_MENUS),
                _labCtxt,
                mac?null:"-",
                mac?null:StrapAlign.button(StrapAlign.TOG_MULTI_MBARS),
                KOPT_DISABLE_IF_CHILDS_DISABLED
            };
        }
        if (id=='S') {
            return
                new Object[]{
                "Superimpose",
                b("SUPER_GO"),
                b("SUPER_COMPLEXES_GO").t("Superimpose all protein complexes in this view").i(IC_SUPERIMPOSE),
                b("SUPER_STOP").t("Stop all superposition processes").i(IC_STOP),
                b("SUPER_UNDO").t("Reset coordinates to what it was before superposition"),
                b("SUPER_HOME").t("Reset coordinates").i(IC_HOME).tt("Reset coordinates to home the original coordinates as recorded in the protein file"),
                KOPT_DISABLE_IF_CHILDS_DISABLED,
                "-",
                StrapAlign.menu(Superimpose3D.class)
            };
        }
        if (id=='T') {
            return new Object[]{
                "Tools",
                new Object[] {
                    "Session",
                    b("SAVE").t("Save session").i(IC_DIRECTORY),
                    b("LOAD").t("Load session").i(IC_DIRECTORY),
                    "-",
                    new Object[]{ "Log", b("LOGg").t("Generic commands "), b("LOG").t("Native commands ")}
                },
                b(COMMANDcenter).i(IC_CENTER),
                b(IC_CAMERA).t("Capture image (print)").i(IC_CAMERA),
                butColor(ButColor.NO_TRANSPARENCY, COMMANDbackground +" #COLOR").t("Background color ..."),
                "-",
                b("CTRL").t("Control panel").i(IC_CONTROLPANEL),
                b("SCRIPT").t("Generic script panel").i(IC_CONSOLE),
                b(COMMANDshowScriptPanel),
                b("CUST").t("Customize").i(IC_CUSTOM),

                "-",
                menuForAccelerators(),
            };
        }
        if (id=='P') return new Object[]{"Protein", setIcn(iicon(IC_ADD),b(V3dListModel.TITLES[V3dListModel.PROTEINS]).mi(null)), b("EXPORT")};
        if (id=='H') return new Object[]{" Help  ", b("INFO"),b("H").t("3D visualization in Strap")};
        if (id=='v') {
            final Object vv[]=StrapPlugins.allClassesV(ProteinViewer.class).asArray(),
                ii[]=new Object[vv.length+9];
            int j=0;
            if (vv.length>0) {
                ii[j++]="Alternative  viewer type";
                ii[j++]="i";
                ii[j++]=IC_JMOL;
                for(int i=vv.length; --i>=0;) {
                    final Object c=vv[i];
                    if (c==null) continue;
                    ii[j++]=new ChButton("PV").li(this)
                        .t("Open in "+niceShrtClassNam(c)).i(dIIcon(c))
                        .cp(ProteinViewer.class,c);
                }
                if (!Insecure.EXEC_ALLOWED) ii[j++]=b("MBT").t("   \u25baMBT ...");
                ii[j++]=new Object[]{
                    "Options for start",
                    b("CUST_INIT"),
                    b(TOG_SEND[0]),
                    b(TOG_SEND[1])
                };
            }
            return ii;
        }
        return null;
    }
    /* <<< Menu <<< */
    /* ---------------------------------------- */
    /* >>> Button >>> */
    private final Map<String,ChButton> _mapButtons=new HashMap();
    ChButton b(String id) {
        ChButton b=_mapButtons.get(id);
        if (b==null) {
            String t=null, tt=null, clazz3D=null, word0=null;
            Object i=null;
            boolean toggle=false, select=false;
            final int iList=idxOf(id, V3dListModel.TITLES);
            if (id==DIA+SELECT_ATOMS_G)  { t="Select atom types ...";  }
            else if (id==DIA+SELECT_AMINO)    { t="Select amino acids ..."; }
            else if (id==BUT_NATIV_MENUS)t="Native menu-bar of this particular 3D-viewer";
            else if (id==BUT_GEN_MENUS)  t="Generic menu-bar for 3D-viewers in Strap";
            else if (id=="INFO")         t="x";
            else if (id==IC_JAVA)      { t="Java source"; i=IC_JAVA; }
            else if (iList>=0)  {
                t=id;
                i=iList==V3dListModel.HET ? IC_FLAVIN : iList==V3dListModel.NUCL? IC_DNA : null;
            }
            else if (id=="EXPORT") { t="Export proteins"; i=IC_DND; }
            else if (id=="SUPER_GO") {
                t="Superimpose all protein chains in this view";
                tt="All structures will be superimposed upon the optimal structure (OS)<br>"+
                    "The OS is the one where the sum of scores to the others is maximal.<br><br>"+
                    "To superimposed only fewer proteins,  open another 3D-view with only the proteins under consideration.<br><br>"+
                    "The Computation is interupted when the view is closed.<br>"+
                    "An 'Undo' button will appear";
                i=IC_SUPERIMPOSE;
            }
            else if (id=="WIRE_HETEROS") t="Show/Hide single hetero compounds";
            else if (id=="WIRE_DIFF_COLOR") {
                t="Each protein in a different color";
                tt="Each protein color according to the chain ID using Jmol colors.<br>Shift-key: Each in a different color indipendent of the chain ID.";
            }
            else if (id=="WIRE_DIFF_COLORc") t="Each chain in a color according to Jmol convention";
            else if (id=="CUST_INIT") { t="Customize init scripts"; i=IC_CUSTOM; }

            else if (id==BUT_DEFAULT_VIEWER) clazz3D=CLASS_ChAstexPROXY;
            else if (id==TOG_SEND[0] || id==TOG_SEND[1] || id==TOG_CURSOR_SELECT) { t=id; select=toggle=true; }
            else if (id=="WIRE_HIDE_DIST_HET") {
                t="Hide nucleotide chains and heteros that are distant from current peptide chain";
                toggle=true;
            } else {
                for(String cmd : allCommands()) {
                    if (strEquls(STRSTR_w_R, cmd, id, 0)) {
                        t=id;
                        word0=cmd;
                        break;
                    }
                }
            }
            if (id==COMMANDcenter) { t="Center"; tt="Center view to current selection";}
            if (id==COMMANDshowScriptPanel) { t="Native script-panel"; tt="The script panel originally provided by this viewer."; i=IC_BATCH; }
            if (clazz3D!=null) {
                final String nice=niceShrtClassNam(clazz3D);
                t=id==BUT_DEFAULT_VIEWER ? id : "View in "+nice;
                i=dIIcon(clazz3D);
                tt=nice+" is an advanced 3D-viewer";
            }
            b=(toggle ? toggl(id).s(select) : new ChButton(id)).t(t).tt(tt).i(i).li(this).cp(KEY_ENABLED,this);
            if (strEquls(DIA,id)) b.li(V3dUtils.li());
            if (word0!=null
                || id=="SAVE" || id=="LOG" || id=="LOAD"
                || id==BUT_DEFAULT_VIEWER || id==BUT_NATIV_MENUS || id=="SCRIPT"
                || id=="CTRL" || id=="CUST" || id=="STRAP_MB"
                || id==DIA+SELECT_ATOMS_G
                || id=="SEL_H" || id=="SEL_E" || id=="SEL_HE"
                || id=="SUPER_STOP" || id=="SUPER_GO"  || id=="SUPER_COMPLEXES_GO" || id=="SUPER_UNDO" || id=="SUPER_HOME"
                ) {
                b.cp(KOPT_HIDE_IF_DISABLED,"");
                if (word0!=null) b.cp(KEY_WORD0, word0);
            }
            if (clazz3D!=null) pcp(ProteinViewer.class, clazz3D, b);
            if (iList==V3dListModel.SEL || iList==V3dListModel.ANNO || iList==V3dListModel.SFEATURE) b.i(IC_LIST);
            _mapButtons.put(id, b);
        }
        return b;
    }
    public boolean isEnabled(Object o) {
        final boolean cpOnlyWire=null!=gcp("ONLY_WIRE",o);
        final String s=gcps(KEY_ACTION_COMMAND,o);
        boolean e=false, c=false;
        for(ProteinViewer v : _objects.proteinViewers()) {
            final int E=isEnabledPV(s,v);
            if (E!=0) return E==CTRUE;
            final long flags=flags(v);
            final Component panel=viewerCanvas(v);
            final Protein3d p3d=deref(panel, Protein3d.class);
            final ProteinViewer vv[]=vvSharingView(v);
            if (s=="INFO") {
                final String label= p3d!=null ? "Using the backbone 3D view" : "Using "+niceShrtClassNam(v);
                final Object ii=gcp(ChButton.KEY_MENU_ITEMS,o);
                for(int i=sze(ii); --i>=0;) {
                    final AbstractButton mi=get(i,ii,AbstractButton.class);
                    if (mi!=null && !label.equals(mi.getText())) mi.setText(label);
                }
            }
            for(int iV=sze(vv); --iV>=-1;) {
                final ProteinViewer v2=iV<0?v:vv[iV];
                final Protein p=p(v2);
                if (p==null) continue;
                final boolean subs[]=p.getResidueSubsetB();
                final boolean isPreview=p3d!=null && p3d.isPreview() || gcp(KOPT_IS_PREVIEW,v2)!=null;
                final boolean hiddenN=p3d==null && (p.getResidueSubsetB1()>0 || p.getInferredPdbID()!=null);
                final boolean hiddenC=p3d==null && (subs!=null && lstTrue(subs)+1<sze(p.getResidueTypeFullLength()) || p.getInferredPdbID()!=null);
                final int iList=idxOf(s,V3dListModel.TITLES);
                if (iList>=0) { c=true; e=0<sze(V3dListModel.model(iList).items()); }
                else if (s=="SAVE" ||  s=="LOG" || s=="LOAD" || s=="$ALL") { c=true; e=p3d==null;}
                else if (s=="SEL_E" || s=="SEL_H" || s=="SEL_HE") { c=true; e=p.getResidueSecStrType()!=null; }
                else if (s==SELECT_N) { c=true; e=hiddenN; }
                else if (s==SELECT_C) { c=true; e=hiddenC; }
                else if (s==SELECT_NC || s==HIDE_NC) { c=true; e=hiddenN||hiddenC; }
                else if (s=="SUPER_UNDO") { c=true; e=!isPreview && gcp("SUPER_UNDO",p)!=null; }
                else if (s=="SUPER_HOME") {
                    c=true;
                    final Matrix3D m3d=p.getRotationAndTranslation();
                    e=!isPreview && m3d!=null && !m3d.isUnit();
                }
                else if (s=="WIRE_HIDE_DIST_HET" || s=="WIRE_HETEROS") {c=true; e=p3d!=null && p.getHeteroCompounds('N').length>0; }
                else if (s=="SUPER_STOP" || s=="SUPER_GO" || s=="SUPER_COMPLEXES_GO") { c=true; e=!isPreview; }
                if (e) break;
            }
            if (o==_labCtxt) { c=true; e=0!=(flags&PROPERTY_HAS_CONTEXTMENU); }
            else if (s=="STRAP_MB")   { c=true; e=!(StrapAlign.button(StrapAlign.TOG_MULTI_MBARS).s() || isMac()); }
            else if (s==BUT_SURF_OBJ) { c=true; e=v.getProperty(GET_SURFACEOBJECTS)!=null;}
            else if (s=="WIRE_DIFF_COLOR" || s=="WIRE_DIFF_COLORc") {
                c=true;
                e=p3d!=null && sze(vv)>1;
            }
            else if (cpOnlyWire) {
                c=true;
                e=p3d!=null;
            }
            final String word0=gcps(KEY_WORD0,o);
            if (word0!=null && cntains(word0, allCommands())) { c=true; e=supports(word0,v); }
            if (e) break;
        }
        return !c || e;
    }
    private int isEnabledPV(String s, ProteinViewer v) {
        if (v==null) return 0;
        final long flags=flags(v);
        final boolean isWire=v instanceof Protein3d.PView, hasPanel=null!=viewerCanvas(v);
        boolean c=false, e=false;
        if (s==BUT_NATIV_MENUS) { c=true; e=jMenuBar(v)!=null; }
        else if (s==BUT_DEFAULT_VIEWER) {  c=true; e=isWire; }
        else if (s=="CTRL") { c=true; e=ctrlPnl(v)!=null; }
        else if (s=="AOToff" || s=="AOT") { c=true; e=v.getProperty(GET_FRAME_TITLE)!=null; }
        else if (s==DIA+SELECT_ATOMS_G) { c=true; e=!isWire; }
        else if (s=="CUST") { c=true; e=sze(Customize.cc(v))>0; }
        else if (s==IC_CAMERA) { c=true; e=  hasPanel && 0==(flags&PROPERTY_OPENGL); }
        return !c?0:e?CTRUE:CFALSE;
    }
    /* <<< Button <<< */
    /* ---------------------------------------- */
    /* >>> MenuBar 3D >>> */
    private JPopupMenu _popup;
    public JPopupMenu menu() {
        final ProteinViewer[] vv=_objects.proteinViewers();
        ProteinViewer oneViewer=null;
        for(int i=sze(vv); --i>=0;) {
            if (vv[i]!=null) {
                if (oneViewer==null || !(vv[i] instanceof Protein3d.PView)) oneViewer=vv[i];
            }
        }
        if (oneViewer==null) return null;
        _labTitle.setText("Context menu for "+vv.length+" protein viewer"+(vv.length>1 ? "s ":" ")+(vv.length==1?niceShrtClassNam(vv[0]):""));
        final Object mm[]=genericMenus(false).clone();
        for(int i=mm.length; --i>=0;) {
            if (mm[i]==b("MBT") || mm[i]==b(BUT_DEFAULT_VIEWER) || getTxt(mm[i])==MENU_OTHER_BAR) mm[i]=null;
        }
        if (_popup==null) (_popup=jPopupMenu(0, "Contextmenu for ProteinViewer", mm)).setLightWeightPopupEnabled(false);
        return _popup;
    }
    private JComponent[][] _genMenus={null,null};
    JComponent[] genericMenus(boolean isBar) {
        if (_genMenus[isBar?0:1]==null) {
            final ProteinViewer pv=!_immutable?null:get(0,_objects.proteinViewers(),ProteinViewer.class);
            final JMenu styleMenuTellSelection= styleMenu(pv);
            setMinSze(EX,EX, styleMenuTellSelection);
            final boolean notP3d=!(pv instanceof Protein3d.PView);
            final long flags=flags(pv);
            final Object acpMenus[]={ARRAY_CP, "ONLY_WIRE","",  KOPT_HIDE_IF_DISABLED,"", KEY_ENABLED,this};
            final Object acpWire[]={ARRAY_CP, "ONLY_WIRE",""};
            final Object mView=new Object[]{
                "View",
                m('v'),
                new Object[]{
                    "Application frame", KOPT_DISABLE_IF_CHILDS_DISABLED,
                    b("AOT").t("Always on top of other windows"),
                    b("AOToff").t("Always on top of other windows off")
                },
                new Object[]{
                    "Settings", "i",IC_CUSTOM,
                    new Object[]{
                        "Delay animation when picking amino acid. Off=0  Fast=1  Medium=10  Slow=30 ",
                        new ChTextField(((ChTextField)comp(GET_ANIM_SPEED)).getDocument(), 0L).cols(9,true,true).ct(Float.class)
                    }
                },
                !notP3d?null: new Object[] {
                    "i", IC_COLOR,
                    "Color",
                    b("WIRE_DIFF_COLOR"),
                    b("WIRE_DIFF_COLORc"),
                    KOPT_DISABLE_IF_CHILDS_DISABLED
                },
                !notP3d?null: new Object[] {
                    "i", IC_FLAVIN,
                    "Hetero compounds",
                    b("WIRE_HIDE_DIST_HET"),
                    b("WIRE_HETEROS"),
                    acpMenus,
                    acpWire
                },
                b(COMMANDreset).t("Revert original orientation").i(IC_HOME)
            };
            final Object mSelect=new Object[] {
                "Select",
                b(V3dListModel.TITLES[V3dListModel.PROTEINS]).mi("Select proteins"),
                b(DIA+SELECT_AMINO),
                !notP3d ? null : b(DIA+SELECT_ATOMS_G),
                b(V3dListModel.TITLES[V3dListModel.HET]).mi("Select hetero compoundes"),
                b(V3dListModel.TITLES[V3dListModel.NUCL]).mi("Select DNA or RNA"),
                "-",
                new Object[] {
                    "By secondary structure", "i", IC_HELIX,
                    b("SEL_H").t("Alpha-helices").i(IC_HELIX),
                    b("SEL_E").t("Beta-sheets").i(IC_SHEET),
                    b("SEL_HE").t("Both, alpha-helices and beta-sheets")
                },
                new Object[] {
                    "Infer residue selections from alignment panel", "i", IC_UNDERLINE,
                    "Note: Residue selections are underlined residues in alignment pane.",
                    ResidueSelectionPopup.staticBut(ResidueSelectionPopup.WIKI_3D).t("Via Drag'n Drop ..."),
                    "-",
                    HINT_CLICKING_S,
                    b(V3dListModel.TITLES[V3dListModel.SEL]),
                    b(V3dListModel.TITLES[V3dListModel.SFEATURE]),
                    b(V3dListModel.TITLES[V3dListModel.ANNO])
                },
                "-",
                new Object[] {
                    "Select all",
                    b("$FIRST-$LAST").t("All amino acids"),
                    b("$ALL").t("Everything"),
                    b(COMMANDselect+" HOH WAT SOL OHH HHO H2O OH2").t("All water molecules"),
                },
                b("SEL_I").t("Invert selection"),
                new Object[]{
                    "Termini not shown in the alignment",
                    "i", IC_SCISSOR,
                    b(SELECT_N).t("Hidden N-terminus"),
                    b(SELECT_C).t("Hidden C-terminus"),
                    b(SELECT_NC).t("Hidden N- and C-terminus"),
                    "-",
                    b(HIDE_NC).t("Hide"),
                    KOPT_DISABLE_IF_CHILDS_DISABLED
                },
                "-",
                b(TOG_CURSOR_SELECT)
            };
            final Object
                ooo[]={
                _labTitle,
                m('P'),
                mView,
                m('S'),
                mSelect,
                pcpReturn(KEY_TITLE, "Style", styleMenu(pv)),
                m('T'),
                m('H'),
                !notP3d ? null : pcpReturn(KEY_BEFORE_MENU_ITEM, labl("    "), b(BUT_DEFAULT_VIEWER) ),
                pv!=null&&jMenuBar(pv)==null && 0==(flags&PROPERTY_HAS_CONTEXTMENU)?null:m('B'),
                pcpReturn(KEY_BEFORE_MENU_ITEM, Box.createHorizontalGlue(), styleMenuTellSelection)
            };
            for(Object o : ooo) {
                if (o instanceof ChButton)  ((ChButton)o).setOptions(ChButton.NO_FILL|ChButton.NO_BORDER).i(null);
            }
            final JComponent mm[]=_genMenus[isBar?0:1]= derefArray(StrapAlign.makeMenus(ooo, null), JComponent.class);
            for(JComponent menu : mm) {
                final JMenu m=deref(menu,JMenu.class);
                final String s=getTxt(m);
                if (s==null) continue;
                if (s!=MENU_OTHER_BAR) pcp( ACTION_BECOME_INVISIBLE, oo(this), m.getPopupMenu());
            }
            if (isBar) undockSaved(_genMenus[0]);
        }
        return _genMenus[isBar?0:1];
    }
    private JMenu styleMenu(ProteinViewer pv) {
        final JMenu m=new ChJMenu(""),
            mOff=ChUtils.jMenu(0, new Object[]{"Off", "Alternatively use the  \"On\"-", "menu while holding Shift-key"}, "Off"),
            mOn=ChUtils.jMenu(0, new Object[]{"On", "For repeated use,", "menu items can be undocked"}, "On");
        for(String cmd : STYLE_COMMANDS) {
            if (pv!=null&&!supports(cmd,pv)) continue;
            if (strstr("surface",cmd)>0) {
                m.add(butColor(0, COMMANDsurface_color+" #COLOR\n"+cmd+" off\n"+ cmd+" on\n").cp(KEY_WORD0,cmd).i(null).mi(cmd));
            } else mOn.add(b(cmd+" on").mi(null));
            mOff.add(b(cmd+" off").mi(null));
        }
        mOn.add(menuInfo(0,"Click with Shift-key sets \"off\""));
        if (pv==null||null!=pv.getProperty(GET_SURFACEOBJECTS)) {
            m.add(new ChButton(BUT_SURF_OBJ).li(V3dUtils.li()).cp(KEY_ENABLED,this).mi("Surface (and ribbon) objects"));
            m.addSeparator();
        }
        if ((pv==null||supports(COMMANDcolor,pv))) {
            m.add(b(BUT_COLOR).li(V3dUtils.li()).i(IC_COLOR).cp(KEY_WORD0, COMMANDcolor).mi("Color"));
        }
        m.add(mOn);
        m.add(mOff);
        if (pv==null||supports(COMMANDlabel, pv)) {
            final Object oo[]={
                "Text labels", "i", IC_ANNO,
                new ChButton(DIA+SELECT_LABEL).li(V3dUtils.li()).t("Type free label text ...").i(IC_EDIT),
                " ",
                b(COMMANDlabel+" $FIRST_Aaa $FIRST_NUMBER"),
                b(COMMANDlabel+" $FIRST_Aaa"),
                b(COMMANDlabel+" $FIRST_A"),
                " ",
                b(COMMANDlabel+" off"),
                butColor(ButColor.NO_TRANSPARENCY, COMMANDlabel_color +" #COLOR").t("Label color"),
            };
            m.add(ChUtils.jMenu(0, oo, "Label"));
        }
        mOff.add(b(COMMAND_HIDE_EVERYTHING).mi(null));
        vStyleButton.add(m);
        m.addMouseListener(V3dUtils.li());
        return m;
    }

    /* <<< Menu bar <<< */
    /* ---------------------------------------- */
    /* >>> Accelerator  >>> */
    private static ChButton[] _withAccel;
    private static void withAccelerator(Object object, List<ChButton> v, Set<KeyStroke> setKS) {
        final Object oo[]=
            object instanceof JMenu ? childs((Component)object) :
            object instanceof Object[] ? (Object[])object:
            null;
        if (oo!=null) for(Object o : oo) withAccelerator(o,v,setKS);
        else {
            final JMenuItem mi=deref(object, JMenuItem.class);
            final KeyStroke ks=mi==null?null:mi.getAccelerator();
            if (ks!=null && setKS.add(ks)) adNotNull(gcp(KEY_CLONED_FROM,mi,ChButton.class), v);
        }
    }
    private static JMenu menuForAccelerators() {
        if (_withAccel==null) {
            final List<ChButton> v=new Vector(99);
            final Set<KeyStroke> setKS=new HashSet();
            withAccelerator(StrapAlign.strapMenus(), v, setKS);
            _withAccel=toArry(v,ChButton.class);
        }
        final ChJMenu m=new ChJMenu(KEY_ACCELERATOR);
        for(ChButton b : _withAccel) m.add(b.mi(null));
        return m;
    }
    /* <<< Accelerator <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    private JMenuBar _mbGeneric;
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id=="SET_PV") setPV(PV_FOCUSED, getPV(PV_FOCUSED));
        if (id=="CPY_SESSION") {
            final BA log=(BA)get(0,argv);
            final ProteinViewer  pvNew=(ProteinViewer)get(1,argv);
            final ChLogger logger=log==null?null:(ChLogger)log.getSendTo();
            if (logger!=null) {
                final File f=newTmpFile(".txt");
                logger.save(0L, f);
                loadSession(SESSION_NO_ASK, pvNew,f);
            }
        }
        if (id==KEY_GET_MENUBAR) {
            final ProteinViewer vv[]=_objects.proteinViewers(), pv=vv.length==1 ? vv[0] : null;
            if (!isActive(pv)) return null;
            JMenuBar mb=_mbGeneric;
            if (mb==null) mb=_mbGeneric=toMenuBar(0, genericMenus(true), new ChJMenuBar());
            return mb;
        }
        return null;
    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> Session >>> */
    private static ChFileChooser _fs;
    public final static long SESSION_LOAD_PROTS=1<<1, SESSION_NO_ASK=1<<2;
    private void loadSessionI() {
        if (_fs==null) {
            final File f=file(LOG_FILE);
            final String fn=f.getName();
            addActLi(this, _fs=new ChFileChooser(ChFileChooser.CLOSE, "ProteinViewerPopup"));
            _fs.setCurrentDirectory(mkParentDrs(f));
            _fs.addFilter(fn.substring(fn.indexOf('.')+1));
        }
        ChFrame.frame("Choose a script input file",_fs,ChFrame.ALWAYS_ON_TOP|ChFrame.PACK|CLOSE_CtrlW_ESC).shw(ChFrame.CENTER);
    }
    private static void loadSession(long options, ProteinViewer pv, File f) {
        if (pv==null || f==null) return;
        final Protein pp[]=ppInV3D(pv);
        if (sze(f)==0) { error("File "+f+" has size 0"); return; }
        final BA baLine=new BA(99);
        final BA baNotFound=new BA(999);
        final List<Protein> vNeedsLoading=new Vector();
        InputStream is=null;
        if (0!=(options&SESSION_LOAD_PROTS)) {
            try {
                is=new FileInputStream(f);
                final ChInStream in=new ChInStream(is,1024);
                final Set<String> set=new HashSet();
                while(in.readLine(baLine.clr())) {
                    if (strEquls(LOG_PROTEIN_PFX, baLine)) {
                        final String pn=baLine.newString(LOG_PROTEIN_PFX.length(), baLine.end());
                        if (set.contains(pn)) continue;
                        set.add(pn);
                        final Protein p=SPUtils.proteinWithName(pn, StrapAlign.proteins());
                        if (p==null) baNotFound.aln(pn);
                        else if (!cntains(p,pp)) vNeedsLoading.add(p);
                    }
                }
            } catch(IOException iox) { error(iox, "");}
            closeStrm(is);
        }
        if (0==(options&SESSION_NO_ASK)) {
            final JComponent
                pAlready=sze(vNeedsLoading)==0?null:
                pnl(VBHB,
                     " ",
                     "The following proteins will be loaded:",
                    scrllpn(0, new BA(0).join(vNeedsLoading),dim(99,99))
                     ),
                pNotFound=sze(baNotFound)==0?null:
                pnl(VBHB,
                     " ",
                     "The following proteins could not be found and will be ignored:",
                     scrllpn(0, baNotFound, dim(99,99))
                     ),
                msg=pnl(VBHB,
                         "<h2>Running script</h2>",
                        pnl(HB,new ChJList(oo(f),ChJList.OPTIONS_FILES), " ", new BA(22).formatSize(f)),
                         pAlready,
                         pNotFound,
                         " ",
                         "Continue?"
                         );
            if (!ChMsg.yesNo(msg)) return;
        }
        open3dViewer(0L, toArry(vNeedsLoading,Protein.class),pv);
        final Map<String,ProteinViewer> mapName2V=new HashMap();
        for(ProteinViewer v : vvSharingView(pv)) {
            final String n=nam(p(v));
            if (n!=null) mapName2V.put(n,v);
        }
        ProteinViewer v=pv;
        try {
            is=new FileInputStream(f);
            final ChInStream in=new ChInStream(is,1024);
            while(in.readLine(baLine.clr())) {
                if (strEquls(LOG_PROTEIN_PFX, baLine)) {
                    v=mapName2V.get(baLine.newString(LOG_PROTEIN_PFX.length(), baLine.end()));
                } else sendCommand(0L, baLine, v);
                //v.interpret(0L,
            }
            sendCommand(0L, COMMANDcenter_amino,v);
        } catch(IOException iox) { error(iox, "");}
        closeStrm(is);
    }
    /* <<< Session <<< */
    /* ---------------------------------------- */
    /* >>> Color >>> */
    private static Map<String,ChButton> _mapBC=new HashMap();
    static ChButton butColor(int opt, String command) {
        ChButton bc=_mapBC.get(command);
        if (bc==null) {
            final String w0=toStrgIntrn(wordAt(0,command));
            bc=new ButColor(opt,C(0xFFffFF),V3dUtils.li())
                .cp(KEY_PV_CMD, command)
                .cp(KOPT_HIDE_IF_DISABLED,"")
                .cp(KEY_FRAME_TITLE, w0)
                .cp(KEY_WORD0, w0)
                .cp(KEY_ENABLED, pvp());
            _mapBC.put(command,bc);
        }
        return bc;
    }
    /* <<< Color <<< */

}
