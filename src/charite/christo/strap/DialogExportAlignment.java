package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.protein.AlignmentWriter.*;
/**HELP
The sequence alignment can be exported in various formats:
<ol>
<li>WIKI:Fasta_format</li>
<li>HSSP-format is not frequently used: http://www.predictprotein.org/Dexa/out_hssp.html</li>
<li>MSF-format:  http://bmerc-www.bu.edu/examples/output/seqlist.msf.html</li>
<li>ClustalW-format: http://www.molecularevolution.org/mbl/resources/fileformats/</li>
</ol>
<i>SEE_DIALOG:Texshade</i>
<i>SEE_DIALOG:DialogImportMFA</i>
<i>SEE_DIALOG:DialogExportProteins</i>

@author Christoph Gille
*/
public class DialogExportAlignment  extends AbstractDialogJTabbedPane implements Runnable {
    private final ProteinList proteinList=new ProteinList(0L).selectAll(0L);
    private final ChCombo
        comboClass=SPUtils.classChoice(AlignmentWriter.class),
        comboFormat=new ChCombo("ClustalW","MSF","Fasta"),
        comboDNA=new ChCombo("amino acid sequence","nucleotide sequence");
    private final ChTextField
        _tfCols=new ChTextField(" 50").cols(6,true,true),
        _tfFrom=new ChTextField("1").cols(7,true,true),
        _tfTo=new ChTextField("99999").cols(7,true,true);
    private final ChButton
        _cbExt=toggl("Suppress file suffices such as  \".pdb\"").s(true),
        _cbSpc=toggl("Extra space in MSF and ClustalW format");
    private int _count;
    public void run() {
        final Protein pp[]=proteinList.selectedOrAllProteins();
        if (pp.length==0) return;
        final AlignmentWriter alWr=mkInstance(comboClass,AlignmentWriter.class,true);
        if (alWr==null) return;
        final String names[]=new String[pp.length];
        for(int iP=0; iP<pp.length; iP++) {
            final String s=pp[iP].getName();
            names[iP]=_cbExt.s() ? delDotSfx(s) : s;
        }
        alWr.setProteins(pp);
        alWr.setNames(names);
        alWr.setColumnRange(maxi(atoi(_tfFrom)-1,0), maxi(atoi(_tfTo)-1,0) );
        alWr.setResiduesPerLine(maxi(atoi(_tfCols),1));
        final int iF=comboFormat.i();
        final long options=
            (comboDNA.i()>0 ? NUCLEOTIDE_TRIPLET: 0) |
            (_cbSpc.s() ? EXTRA_SPACE :0) |
            (iF==0?CLUSTALW:iF==1?MSF:iF==2?FASTA:0);
        final BA sb=new BA(99999);
        if (alWr instanceof ExportAlignment) ((ExportAlignment)alWr).setProperty(ExportAlignment.PROPERTY_IDENT_THRESHOLD, intObjct(StrapView.sliderSimilarity().getValue()));
        alWr.getText(options,sb);
        final String ext="."+alWr.getFileExtension(), txt=sb.toString();
        final File f=file(STRAPOUT+"/alignments/"+ ++_count+ext), f2=file(STRAPOUT+"/alignments/current"+ext);
        wrte(f,txt);
        wrte(f2,txt);
        final Object
            ta=(options&HTML)!=0 && alWr instanceof ExportAlignment ? new ChJTextPane(txt) : new ChTextView(f),
            pnl=pnl(CNSEW, scrllpn(SCRLLPN_INHERIT_SIZE, ta),null, pnl(f,f2));
        adTab(DISPOSE_CtrlW, shrtClasNam(clas(comboClass.getSelectedItem())), pnl, this);
    }

    public DialogExportAlignment() {
        final Object
            panRelated=StrapAlign.b(DialogExportWord.class),
            pOptions=pnl(VBHB,
                         comboClass.panel(),
                         pnl(FLOWLEFT,"Format ",comboFormat," ",comboDNA),
                         _cbExt.cb(),
                         pnl(FLOWLEFT,"Alignment from column ",_tfFrom," to column ",_tfTo),
                         _cbSpc.cb()                         ),
            pLeft=pnl(VBHB, KOPT_TRACKS_VIEWPORT_WIDTH,
                      pnl(FLOWLEFT,"Residues per line ",_tfCols),
                      pnl(FLOWLEFT,"Export Alignment: ",new ChButton("GO").t(ChButton.GO).r(this)),
                      pnlTogglOpts(pOptions),
                      pOptions,
                      pnlTogglOpts("Related",panRelated),
                      panRelated

                      ),
            pMain=pnl(CNSEW,  scrllpn(SCRLLPN_INHERIT_SIZE,proteinList), dialogHead(this),  pnl(REMAINING_VSPC2), null, pnl(CNSEW,null,pLeft));
        adMainTab(pMain, this,null);
    }

}
