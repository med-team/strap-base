package charite.christo.strap;
import charite.christo.protein.*;
import java.util.*;
import java.awt.Rectangle;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

public class NextAndPrevSelection {
    private int _col;
    private Object _p;
    private long _when;

    private static boolean t(int pos, boolean bb[]) { return pos>=0 && pos<bb.length && bb[pos]; }

  public void goNextMatch(String direction, ResidueSelection[] selections) {
    final boolean bb[]=toAlignmentPositionsZ(direction, selections);
    final Protein pp[]=ppAtColZ(_col,selections);
    Protein p=deref(_p,Protein.class);
    final int iP=idxOf(p,pp);
    int col=_col;

    if (_when<StrapAlign.whenMovedCursor()) {
        col=StrapAlign.cursorColumn();
        p=StrapAlign.cursorProtein();
    }
    final int col0=col;
    _when=System.currentTimeMillis();
    if ("<"==direction) {
        while(col>=0 && !t(col,bb)) col--;
        p=col!=col0 && 0<=idxOf(p,ppAtColZ(col,selections)) ? p :  t(col,bb) ? get(iP-1,pp,Protein.class) : null;
        if (p==null) {
            while(col>=0 && !(t(--col,bb)));
            p=lstEl(ppAtColZ(col,selections),Protein.class);
        }
    }
    if (">"==direction) {
        while(col<bb.length && !t(col,bb)) col++;
        p= col!=col0 && 0<=idxOf(p,ppAtColZ(col,selections)) ? p : t(col,bb) ?  get(iP+1,pp, Protein.class) : null;
        if (p==null) {
            while(col<bb.length && !t(++col,bb));
            p=get(0,ppAtColZ(col,selections),Protein.class);
        }
    }

    final boolean success=p!=null && p.column2indexZ(col)>=0;
    if ("|<"==direction || "<"==direction && !success) {
        p=get(0,ppAtColZ(col=fstTrue(bb),selections),Protein.class);
    }

    if (">|"==direction || ">"==direction && !success) {
        p=lstEl(ppAtColZ(col=lstTrue(bb),selections),Protein.class);
    }
    final int iA=p!=null ? p.column2indexZ(col) : -1;
    if (iA>=0) {
        _p=wref(p);
        _col=col;
        StrapAlign.animatePositionZ(StrapAlign.ANIM_SET_CURS_SCROLL_TO_VISIBLE, p, iA);
        final StrapView view=StrapAlign.alignmentPanel();
        final ResidueSelection s=get(0, p.residueSelectionsAt(0L,iA,iA+1,VisibleIn123.ANYWHERE), ResidueSelection.class);
        if (view!=null && s!=null) {
            final int fst, lst;
            if (s instanceof HighlightPattern.Selector) {
                fst=iA;
                lst=mini(p.countResidues(), fst+get(iA,((HighlightPattern.Selector)s).getMatchLenghts()))-1;
            } else {
                final int offset=s.getSelectedAminoacidsOffset()-Protein.firstResIdx(p);
                if (s instanceof ResidueAnnotation && !((ResidueAnnotation)s).isEnabled()) putln(RED_WARNING+"NextAndPrevSelection !s.isEnabled() ",s);
                fst=offset+fstTrue(s.getSelectedAminoacids());
                lst=offset+lstTrue(s.getSelectedAminoacids());
            }
            final int row=view.findRow(p);
            if (fst>=0 && lst>=fst && row>=0) {
                final int x1=view.col2x(p.getResidueColumnZ(fst));
                final int x2=view.col2x(p.getResidueColumnZ(lst)+1);
                final int y=view.row2y(row);

                view.scrRectToVis(new Rectangle(view.col2x(col)-16, y-16, 32, 32),false);
                if (lst-fst<10) {
                    setMarchingAnt(ANTS_SEARCH,  view.alignmentPane(), new Rectangle(x1, y, x2-x1, view.charB().height), C(0xFFffFF),C(0xFF0000));
                }
            } else setMarchingAnt(ANTS_SEARCH, view.alignmentPane(), null, null, null );
        }
    }
  }

    private static Protein[] ppAtColZ(int col, ResidueSelection ss[]) {
        final List v=new ArrayList(sze(ss));
        for(int iS=sze(ss); --iS>=0;) {
            final Protein p=SPUtils.sp(ss[iS]);
            if (p!=null && ResSelUtils.isSelectedAAZ(p.column2indexZ(col),ss[iS])) adUniq(p,v);
        }
        final Protein pp[]=toArry(v,Protein.NONE);

        SPUtils.sortVisibleOrder(pp);
        return pp;
    }

    private static boolean[] buff=new boolean[999];
    public static boolean[] toAlignmentPositionsZ(String direction, ResidueSelection[] ss) {
        boolean bb[]=buff;
        Arrays.fill(buff,false);
        for(int iS=sze(ss); --iS>=0;) {
            final ResidueSelection s=ss[iS];
            final Protein p=SPUtils.sp(s);
            if (p==null) continue;
            final boolean[] selRes=s.getSelectedAminoacids();

            for(int i=lstTrue(selRes)+1; --i>=0;) {
                if (selRes[i]) {
                    if (direction=="<" && i+1<selRes.length && selRes[i+1]) continue;
                    if (direction==">" && i>0               && selRes[i-1]) continue;
                    final int col=p.getResidueColumn(i+s.getSelectedAminoacidsOffset());
                    if (col>=0) (sze(bb)<=col ? bb=chSze(bb,col+333) : bb)[col]=true;
                }
            }
        }
        return buff=bb;
    }

}
