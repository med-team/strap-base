package charite.christo.strap;
import charite.christo.*;
import charite.christo.strap.extensions.*;
import java.io.File;
import java.awt.event.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.strap.ExportAlignment.*;
/*
  @author Christoph Gille
*/
public class DialogExportWord extends AbstractDialogJTabbedPane implements ActionListener {
    private final ChTextField
        _tfFile=new ChTextField(" "),
        _tfCharsInLine=new ChTextField("50").cols(5,true,true);

    private final ChCombo _comboTarget=new ChCombo("Web-browser","Word-processor","clustalW-format","MSF-format").li(this);
    private final ProteinCombo _comboSecStru=new ProteinCombo(ProteinList.SECSTRU);
    private final ChButton
        _bbCust[]={
        Customize.newButton(Customize.customize(Customize.cssAlignBrowser), !Insecure.EXEC_ALLOWED?null: Customize.customize(Customize.webBrowser)),
        Customize.newButton(Customize.cssAlignBrowser, !Insecure.EXEC_ALLOWED?0 : Customize.htmlEditors)
    },

        _bGo=new ChButton("GO").t(ChButton.GO).li(this).tt("Create alignment file.<br>Hold Ctrl-key to see details"),
        _cbFileExt=toggl("Include file name suffices like \".pdb\""),
        _cbPdbNum=toggl("Use the residue numbers in pdb-files if available"),
        _cbUL=toggl("Underline all annotations").s(true),
        _cbProtIcons=toggl("Show protein icons in web browser").s(true),
        _cbJalviewSS=toggl("Include PDB-secondary structure as feature").s(true),
        _cbSecStru1=toggl("Take 1st protein with secondary structure information").s(true),
        _cbSecStru=toggl("Choose protein with secondary structure").doEnable(_comboSecStru),
        _cbSecStruBG=toggl("Secondary structure for each individual protein as background color");
    private final Object
        _panBrowsers=!isWin() ? null :
        pnl(HB," Or try one of those web browsers: ", new ChButton("B").t("Firefox").li(this),new ChButton("B").t("Chrome").li(this),new ChButton("B").t("Opera").li(this),new ChButton("B").t("iexplore").li(this)),
        _msgWysiwyg=
        isWin() ? pnl(HBL, KOPT_HIDE_IF_DISABLED, "The alignment is edited in Microsoft Word.") :
        pnl(VBHB,
            KOPT_HIDE_IF_DISABLED,
            "Suggestion: The following programs can be installed/used to edit alignments:",
            isMac() ? pnl(HBL,"http://www.kompozer.net/") :
            pnl(HBL, "gwrite   ", monospc(isSystProprty(IS_KNOW_PACKAGE_MANAGER) && sze(file("/usr/bin/gwrite"))==0 ? installCmdForPckgs("gwrite"):null))
            );

    private int _count;
    public DialogExportWord() {
        _tfFile.setOpaque(false);
        noBrdr(_tfFile);
        for(ChButton b : _bbCust) b.t("").setOptions(ChButton.ICON_SIZE).cp(KOPT_HIDE_IF_DISABLED,"");

        final Object
            pOpt=pnl(VBHB,
                     pnl(HBL, _cbFileExt.cb(), _cbPdbNum.cb()),
                     pnl(VBHB,"#TBem Options for web browser and Word-processor",
                         _cbUL.cb(),
                         _cbProtIcons.cb(),
                         " ",
                         _cbSecStruBG.cb(),
                         _cbSecStru1.doUnselect(_cbSecStru).cb(),
                         pnl(HBL, _cbSecStru.doUnselect(_cbSecStru1).cb(),  _comboSecStru)
                         )
                     ),

            pMain=pnl(VBHB,
                      StrapView.button(StrapView.BUT_WARN_MARCHING_ANTS),
                      pnl(HBL,"Send  Alignment to ",_comboTarget," ", _bbCust, "#", "  Fold lines after how many characters? ",_tfCharsInLine),
                      pnl(HBL, _bGo, _panBrowsers),
                      _msgWysiwyg,

                      pnlTogglOpts(pOpt),
                      pOpt,
                      pnl(CNSEW,null,_tfFile)
            );
        adTab(0, "Standard options", pnl(CNSEW, pMain,dialogHead(this)), this);
        final ChButton butJalview=new ChButton("JALVIEW").t("Export to Jalview ...").li(this)
            .tt("Export to Jalview ...")
            .cp(KEY_REQUEST_IMAGE_MAX_WIDTH,intObjct(333))
            .cp(KEY_REQUEST_IMAGE_OPTIONS, longObjct(REQUEST_IMAGE_REMOVE_TEXT));
        ChIcon.requestImage("http://jalview.aegir.lifesci.dundee.ac.uk/sites/jalview.aegir.lifesci.dundee.ac.uk/files/styles/adaptive/adaptive-image/public/jalviewHome_0.jpg", butJalview);
        final Object
            advanced=pnl(VBHB,
                          "A Web-link for the alignment can be included into<ul>"+
                          "<li> E-mails</li><li>Office documents</li><li> Web-pages</li></ul>",
                         StrapAlign.b(CLASS_DialogPublishAlignment),
                          " ",
                          "#JS",
                          "Jalview is another powerful alignment program written in Java",
                          butJalview,
                          " ",
                          "#JS",
                          "PDF output offers advanced features:"+
                          "<ul><li>Plotting residue hydrophobicity or conservation</li>"+
                          "<li>Indicating residues by annotated arrows</li>"+
                          "<li>Secondary structure graph above the alignment</li>"+
                          "<li>Sequence logos</li>"+
                          "<li>Alignment fingerprint</li>"+
                          "</ul>",
                         StrapAlign.b(CLASS_Texshade),
                          "##"
                          );
        adTab(0, "More options", advanced, this);
        enableDisable();
    }

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        final int modi=modifrs(ev);
        final boolean isJalview=cmd=="JALVIEW";
        StrapView view=StrapAlign.alignmentPanel();
        if (view==null) view=get(0,StrapAlign.alignmentPanels(),StrapView.class);
        if (view==null) return;
                if (q==_comboTarget) enableDisable();

        if (isJalview || cmd=="GO" || cmd=="B") {
            if (isJalview) {
                Protein[] pp=StrapAlign.selectedProteinsInVisibleOrder();
                if (pp.length==0) pp=view.visibleProteins();
                Object cb=null;
                for(Protein p:pp) if (p.getResidueSecStrType()!=null) cb=_cbJalviewSS.cb();
                final CharSequence s=getHlp(ToJalview.class);
                if (ChMsg.yesNo(pnl(VBPNL,s," ",cb,pnl(HBL,"<br>Loading the alignment in Jalview?")))) {
                    final int mode= _cbJalviewSS.s() ? ToJalview.SECSTRU : 0;
                    new ToJalview().launchJalview(mode,pp,null);
                }
            } else {
                final Protein[] pp=view.ppInRectangleForAlignment(2);
                final int iTarget=_comboTarget.i();
                final long iType;
                final String ext;
                int size=SPUtils.maxColumn(pp)*pp.length;
                final BA sb=new BA(size);
                switch(iTarget) {
                case 2: iType=CLUSTALW; ext=".clustalW"; break;
                case 3: iType=MSF;      ext=".msf"; break;
                default:
                    iType=HTML;     ext=".html";
                    sb.a("<!DOCTYPE HTML>\n<!-- saved from url=(0023)http://3d-alignment.eu/ -->\n");
                    size*=7;
                }
                sb.ensureCapacity(size);
                final java.awt.Rectangle r=view.rectRubberBand();
                final ExportAlignment w=new ExportAlignment();
                w.setProteins(pp);

                if (wdth(r)>0) w.setColumnRange(x(r), x2(r));
                w.setResiduesPerLine( maxi(5,atoi(_tfCharsInLine)));
                final BA script=new BA(9999);
                StrapScriptCreator.makeScript(0,pp,script);
                w.setProperty(ExportAlignment.PROPERTY_SCRIPT, script.trimSize());
                w.setProperty(ExportAlignment.PROPERTY_IDENT_THRESHOLD,intObjct(StrapView.sliderSimilarity().getValue()));
                w.setProperty(ExportAlignment.PROPERTY_COLORING, toStrg(ShadingAA.choice()));
                if (iType==HTML) {
                    Protein p2=_cbSecStru.s() ? SPUtils.sp(_comboSecStru) : null;
                    if (_cbSecStru1.s()) {
                        for(Protein p : pp) if (p.getResidueSecStrType()!=null) {p2=p; break;}
                    }
                    w.setRulerSecStru(p2);
                }
                w.getText(iType | HTML_HEAD_BODY| BALLOON_MESSAGE |
                          (_cbSecStruBG.s() ? SECONDARY_STRUCTURE:0) |
                          ((_cbFileExt.s())?FILE_SUFFIX:0) |
                          ((_cbPdbNum.s())?PDB_RESIDUE_NUMBER:0) |
                          (_cbUL.s() ? UNDERLINE_ANNOTATIONS : 0) |
                          (_cbProtIcons.s() ? PROTEIN_ICONS : 0) |
                          (iTarget==0 ? CSS_BROWSER : 0), sb);
                final File fOut=file(STRAPOUT+"/align"+ ++_count+ext);
                wrte(fOut,sb);
                _tfFile.t(" "+fOut).tools().underlineRefs(0);
                _bGo.addDnD(fOut);
                if (cmd=="B") {
                    final Object ex[]="Opera"==getTxt(q) ?
                        new Object[]{"SH","%ProgramFiles%\\Opera\\Opera.exe "+fOut} :
                    new Object[]{"cmd.exe", "/c" ,"start "+getTxt(q)+" "+fOut};
                    new Thread(new ChExec((modi&InputEvent.CTRL_MASK)==0?0: ChExec.LOG|ChExec.SHOW_STREAMS|ChExec.STDOUT_IN_TEXT_BOX).setCommandLineV(ex)).start();
                } else if (iTarget<2) {
                    if (!Insecure.EXEC_ALLOWED) {
                        if (iTarget==0) visitURL(url(fOut),modi);
                        else {
                            final String msg="Two methods for opening the alignment in the word processor (e.g. MS-Word):"+
                                "<ul><li>Drag the following file path with the mouse onto the word processor-icon. Then drop the file-path over the word-icon.</li>"+
                                "<li>Or open the file in the word processor</li></ul>";
                            ChMsg.msgDialog(0L, pnl(VBHB,msg,fOut));
                        }
                    } else {
                        htmlTidy(fOut);
                        final ChExec ex=new ChExec(0).setCommandsAndArgument(custSettings(iTarget==1 ? Customize.htmlEditors : Customize.webBrowser),fOut);
                        if (0!=(modi&InputEvent.CTRL_MASK)) ex.setCustomize(Customize.customize(Customize.htmlEditors));
                        startThrd(ex);
                    }
                } else new ChTextView(fOut).tools().showInFrame("");
            }
        }
    }
    private void enableDisable() {
        setEnabld(_comboTarget.i()==1,_msgWysiwyg);
        for(int i=sze(_bbCust); --i>=0; ) setEnabld(_comboTarget.i()==i, _bbCust[i]);
    }

}
