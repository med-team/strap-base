package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.io.*;
import java.util.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.ProteinWriter.*;
/**HELP

<h3>Using the export dialog (file-menu)</h3>
Single protein files can be exported in various output formats.

<h3>Dragging proteins with the mouse</h3>

Proteins can be dragged from the alignment to the Desktop or the file browser.

This requires dragging a protein label such as in the row header of
the alignment with the mouse.

Proteins can be dropped in other Desktop applications that support  WIKI:Drag_and_drop.
They may also be dropped into another Strap instance.

<br><br>
By default, the original file that the protein was loaded from, is directly transferred even if the 3D orientation
has been changed in Strap or the N-terminus or C-terminus have been cleaved off.
This behavior can be changed with an option pane. The button to open this option pane appears in the Strap tool pane when the proteins are dragged.

   @author Christoph Gille
*/
public class DialogExportProteins extends AbstractDialogJPanel implements java.awt.event.ActionListener {
    private final ProteinList _pList=new ProteinList(0L);
    private static List<File>_vFiles;
    private static ChJList _jList;

    private final ChCombo
        _comboClass=SPUtils.classChoice(ProteinWriter.class),
        _comboFormat=new ChCombo("PDB-format", "PDB-format of C-alpha atoms", "Fasta-format", "Fasta-format of coding nucleotides");

    private final ChButton _togRubber=toggl("Take proteins from rubber band selection").li(this);
    public DialogExportProteins() {
        final Object
            pDnD=pnl("Export via drag'drop", WATCH_MOVIE+MOVIE_Export_Proteins),
            pWest=pnl(VBHB,KOPT_TRACKS_VIEWPORT_WIDTH,
                      pnl(_comboClass.panel()),
                      pnl(_comboFormat),
                      _togRubber.cb(),
                      pnl(new ChButton("GO").t(ChButton.GO).li(this).tt("For the  selected proteins output files are written.")),
                      pnl(HBL,"Target directory: ", file(STRAPOUT)),
                      pnlTogglOpts("Also see",pDnD),
                      pDnD

                      ),
            pCenter=pnl(new java.awt.GridLayout(1,2),_pList.scrollPane(), pWest);
        pnl(this,CNSEW, pCenter,dialogHead(this),pnl(REMAINING_VSPC1));
    }
    public void actionPerformed(java.awt.event.ActionEvent ev){
        final String cmd=actionCommand(ev);
        final Object q=ev.getSource();
        final boolean isRubber=isSelctd(_togRubber);
        if (q==_togRubber)  setEnabld(!isRubber, _pList);

        if (cmd=="GO") {
            final File DIR=file(STRAPOUT);
            StrapAlign.errorMsg("");
            int fromCol=0, toCol=MAX_INT;
            final Protein pp[];
            if (isRubber) {
                final StrapView v=StrapAlign.alignmentPanel();
                if (v==null) { error("Error export proteins: no alignment panel is focused"); return; }
                pp=StrapView.ppInRectangle(v);
                if (sze(pp)==0) { error("No protein marked by rubber band"); return; }
                fromCol=x(v.rectRubberBand());
                toCol=x2(v.rectRubberBand());

            } else {
                pp=_pList.selectedOrAllProteins();
            }
            final int format=_comboFormat.i();
            final boolean
                pdbSS=format==0,
                pdbCA=format==1,
                amino=format==2,
                nucle=format==3;
            final BA sbFailed=new BA(99);
            if (_jList==null) {
                _jList=new ChJList(_vFiles=new ArrayList(), ChJList.OPTIONS_FILES|ChJTable.DEFAULT_RENDERER);
                pcp(KEY_PANEL, pnl(CNSEW, scrllpn(_jList), null,TTContextAndDnD), _jList);
                pcp(DialogStringMatch.KEY_SAVE, "Proteins",_jList);
            }
            final BA sb=new BA(999);
            final long options=(
                                pdbCA||pdbSS? PDB|ATOM_LINES|HETEROS|NUCLEOTIDE_STRUCTURE | (pdbSS?SIDE_CHAIN_ATOMS:0) :
                                amino?FASTA:
                                nucle?FASTA|NUCLEOTIDES:0) |
                SEQRES | HELIX_SHEET;
            for(Protein p:pp) {
                final ProteinWriter pw=mkInstance(_comboClass,ProteinWriter.class,true);
                if (pw==null) { StrapAlign.errorMsg("Could not instantiate "+_comboClass); return;}
                sb.clr();
                if (pdbSS ) p.loadSideChainAtoms(null);

                if (isRubber) {
                    final int f=p.column2nextIndexZ(fromCol);
                    final int t=p.column2thisOrPreviousIndex(toCol);
                    if (f>=t) continue;
                    pw.setResidueRange(f,t);
                }

                final String err=
                    pdbCA && p.getResidueCalpha()==null ? "no C-alpha coordinates" :
                    pdbSS && p.getAtomCoordinates()==null ? "no coordinates" :
                    nucle && p.getNucleotides()==null ? "no nucleotides" :
                    !pw.toText(p,new Matrix3D[]{p.getRotationAndTranslation()},options,sb) || sb.end()==0 ? "" : null;
                if (err!=null) sbFailed.a("Failed: ").a(err).a(" in ").aln(p);
                else {
                    final File f=file(DIR,p+pw.getFileSuffix());
                    wrte(f,sb);
                    adUniq(DIR,_vFiles);
                    adUniq(f,_vFiles);
                }
                ChDelay.afterMS(EDT,ChDelay.CONTENTS_CHANGED,_jList,111,null);
            }
           if (sze(_vFiles)>0) _jList.showInFrame(ChFrame.AT_CLICK, "Exported proteins");
            shwTxtInW("Error export proteins",sbFailed);
        }
    }
}
