package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   This dialog reorders proteins.
   @author Christoph Gille
*/
public class DialogSortProteins extends ChPanel implements ChRunnable {
    private Protein[] _prev;
    private final ChCombo _choice=SPUtils.classChoice2(CompareTwoProteins.class, SequenceAlignmentScore2.class, ProteinsSorter.class).s(CLASS_AlignmentScoreBlosume62);
    private final Object
        _bUndo=new ChButton("Undo").r(thrdCR(this,"U")),
        _bStop=new ChButton("Stop computation").r(thrdCR(this,"S")),
        _bRunA=new ChButton("Sort all").r(thrdCR(this,"SA")),
        _bRunS=new ChButton("Sort selected").r(thrdCR(this,"SS"));
    private final boolean _run[]={true};
    public DialogSortProteins() {
        setEnabld(false, _bUndo);
        setEnabld(false, _bStop);
        final Object pOpt=pnl(CNSEW, _choice.panel(), "Chose the sorting method:");
        pnl(this, CNSEW,
            pOpt,
            pnlTogglOpts("*Options",pOpt),
            pnl(FLOWLEFT, _bRunA, _bRunS, _bStop, _bUndo),
            "#ETBem"
            );
    }

    public Object run(String id, Object arg) {
        if (id=="SA") startThrd(thrdCR(this, "R", StrapAlign.visibleProteins()));
        if (id=="SS") {
            final Protein pp[]=StrapAlign.selectedProteins();
            if (pp.length<2) error("Select at least 3 proteins");
            else startThrd(thrdCR(this, "R", pp));
        }
        if (id=="R") {
            setEnabld(true, _bStop);
            setEnabld(false,_bRunS);
            setEnabld(false,_bRunA);
            final Protein pp[]=(Protein[])arg;
            ProteinsSorter sorter=mkInstance( _choice, ProteinsSorter.class, false);
            if (sorter==null) {
                final Object score=mkInstance( _choice, Object.class, true);
                if (score==null) return null;
                sorter=new ProteinsSorterMatrix().setCompareTwoProteins(score);
            }
            if (sorter!=null) {
                _prev=StrapAlign.visibleProteins().clone();
                _run[0]=true;
                final Protein sorted[]=sorter.sortProteins(pp, _run);
                StrapAlign.inferOrderOfProteins(0, sorted);
                setEnabld(true, _bUndo);
            }
            setEnabld(false, _bStop);
            setEnabld(true,  _bRunS);
            setEnabld(true,  _bRunA);
        }
        if (id=="U") {
            StrapAlign.inferOrderOfProteins(0, _prev);
            setEnabld(false, _bUndo);
        }
        if (id=="S") _run[0]=false;
        return null;
    }

}
