package charite.christo.strap;
import charite.christo.*;
import java.awt.event.*;

import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   This dialog aims at loading many PDB-files from file servers.

   The user enters a text containing
   pdb-codes such as <I>1prp</I>, <I>pdb1ryp</I> or <I>1SBC.</I>

   The optional chain identifier must be preceded by colon or underscore.

   @author Christoph Gille
*/
public class DialogFetchPdb extends AbstractDialogJTabbedPane implements ActionListener {
    private final ChCombo _comboPdbSite=Customize.customize(Customize.pdbSite).newComboBox().save(DialogFetchPdb.class,"ftp-site");
    private final ChTextArea _ta=new ChTextArea(4,4);
    public DialogFetchPdb() {
        setSettings(this,Customize.pdbSite);
        hl();
        addActLi(this,_ta);
        _ta.tools()
            .cp(KEY_IF_EMPTY, "Enter here a text containing  PDB IDs")
            .saveInFile("DialogFetchPdb")
            .enableUndo(true).enableWordCompletion(dirWorking());
        final Object
            pButtons=pnl(
                         new ChButton("GO").t(ChButton.GO).li(this), " ",
                         new ChButton("EX").t("For newbies: show example").li(this)
                         ),
            pAlsoSee=pnl(VBHB,
                          StrapAlign.b(CLASS_DialogFetchSRS),
                          WATCH_MOVIE+MOVIE_Load_Proteins+" "+MOVIE_Load_Proteins_web_pdb
                          ),

            pSouth=pnl(VBHB,_comboPdbSite,pButtons,pnlTogglOpts("Also see ",pAlsoSee), pAlsoSee),
            pMain=pnl(CNSEW, scrllpn(_ta),dialogHead(this),pSouth);
        adMainTab(remainSpcS(pMain),this,null);

    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        final Object q=ev.getSource();
        final ChTextComponents tools=_ta.tools();
        if (q==_ta && cmd==ACTION_TEXT_CHANGED || cmd=="GO") hl();
        if (cmd=="GO") {
            final BA ba=tools.byteArray();
            final int
                selStart=_ta.getSelectionStart(),
                selEnd=_ta.getSelectionEnd(),
                fromTo[]=ids(ba,selStart<selEnd?selStart:0, selStart<selEnd?selEnd:MAX_INT),
                N=fromTo.length/2;
            if (N==0) StrapAlign.errorMsg(ANSI_W_ON_R+"No PDB codes found in the text!");
            else {
                StrapAlign.errorMsg("");
                final String pdbBase=toStrg(_comboPdbSite), urls[]=new String[N], fileNames[]=new String[N];
                final byte T[]=ba.bytes();
                final BA bFN=new BA(9);
                for(int i=N; --i>=0; ) {
                    final int f=fromTo[2*i],t=fromTo[2*i+1];
                    if (t-f!=4 || t>=T.length) continue;
                    final String code4=bytes2strg(T,f, t).toLowerCase();
                    bFN.clr().a("pdb").a(code4);
                    if ( (T[t]==':' || T[t]=='_' || T[t]=='|') && is(LETTR_DIGT_US,T,t+1) && !is(LETTR_DIGT_US,T,t+2)) {
                        bFN.a('_').a((char)T[t+1]);
                    }
                    fileNames[i]=toStrg(bFN.a(".ent"));
                    urls[i]=rplcToStrg("??",code4.substring(1,3),rplcToStrg("????",code4,pdbBase));
                }
                adTab(DISPOSE_CtrlW, "Fetched ",new FilesFetchedFromServer(urls, fileNames), this);
            }
        }

        if (cmd=="EX") {
            _ta.setText(
                       "Words consisting of four letters or digits are assumed to be PDB identifiers.\n"+
                       "They can be typed in different ways: \n"+
                       "  1sbc 1sbt  or \n"+
                       "  or pdb1sbc pdb1sbt \n"+
                       "  or with chain pdb1sbc_A or pdb1sbc:A  or  1ryp:L \n"+
                       "They are download with the \"GO\" button.\n\n"+
                       _ta);
        }
    }

    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Highlight >>> */
    private TextMatches _highlight;
    private void hl() {
        final ChTextComponents t=_ta.tools();
        final BA ba=t.byteArray();
        t.removeHighlight(_highlight);
        t.addHighlight(_highlight=new TextMatches(0L, ids(ba, 0, MAX_INT), MAX_INT, C(0xFF4400,80)));
    }

    private int[] _ft={};
    private int[] ids(BA ba, int selBeg, int selEnd) {
        final byte T[]=ba.bytes();
        final int E=mini(ba.end(),selEnd);
        int count=0;
        for(int i=selBeg; i<E; i++) {
            if (is(LETTR_DIGT_US, T,i-1)  ?
                ('b'!= (T[i-1]|32) || !strEquls(STRSTR_w_L,"pdb",T,i-3)&& !strEquls(STRSTR_w_L,"PDB",T,i-3)) :
                !is(DIGT,T,i)) continue;

            if (!is(LETTR_DIGT,T,i+4) && (cntainsOnly(LOWR_DIGT,T, i,i+4) || cntainsOnly(UPPR_DIGT,T, i,i+4))) {
                if (_ft.length<=count+1) _ft=chSze(_ft,count+99);
                _ft[count++]=i;
                _ft[count++]=i+4;
            }
        }
        if (_ft.length>count) _ft=chSze(_ft,count);
        return _ft;
    }
}
