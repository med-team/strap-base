package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import javax.swing.*;
import charite.christo.blast.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
The query sequence for which a similar 3D-structure is searched.|

The search results ordered by score.<br>
A protein chain in the result list is designated by a four-letter/digits-PDB-ID and a chain ID.<br>
The gray bar represents the matching region.<br>
The residue identity between the query and the hit is printed.|

Show the sequence alignment of the query sequence and the Blast hit.|

Open the web page of the hit.|

Display the 3D Backbone of the hit without side effects.|

Display the detailed information of the blast search.<br>Mainly for debugging in case of problems.|
Associate the 3D-coordinates of the hit to the query sequence.

   @author Christoph Gille
*/
public class DialogSimilarStructureResult extends ChPanel implements java.awt.event.ActionListener, Disposable {
    private static final int COL_P=0, COL_R=1, COL_ALI=2, COL_WWW=3, COL_3D=4, COL_DETAILS=5, COL_APPLY=6, COLS=7;
    public final static int VIEW3D_ON_PRESSING_APPLY=1<<0, MANUAL=1<<1;
    private final static Object COMPUTING="Computing ...";
    private final ChButton _buttons[]=new ChButton[COLS], _infoWait=labl("Please wait"),  _cbDetails=toggl("Details ").li(this);
    private final ChPanel _progress[]={prgrssBar(),prgrssBar()};
    private boolean _disposed;
    private final int _opt;
    private ChTextArea _taByHand;
    private final ChJTable _jt;
    private final Object _tData[][], _main;

    private void setW() {
        for(int col=COLS; --col>=0;) {
            if (_buttons[col]==null) continue;
            if (!_cbDetails.s() && (col==COL_ALI || col==COL_WWW || col==COL_3D || col==COL_DETAILS)) {
                _jt.setColWidth(false, col, 0);
                _jt.setColWidth(false, col, 0);
            } else {
                _jt.setColumnWidthC(col<2, col, _buttons[col]);
            }
        }
    }
    public DialogSimilarStructureResult(int options, Protein pp[], Object main) {
        _main=wref(main);
        _opt=options;
        if (0!=(options&MANUAL)) {
            final BA sb=new BA(999);
            for(Protein p : pp) {
                sb.a(p).a(' ',4).aln(p.getInferredPdbID());
            }
            final Object wc=StrapAlign.vProteins();
            (_taByHand=new ChTextArea(sb))
                .tools().enableUndo(false)
                .enableWordCompletion(wc)
                .highlightOccurrence(wc, chrClas(SPC), chrClas(SPC), HIGHLIGHT_UPDATE_IF_TEXT_CHANGES, C(0xFFff14,90));
            final Object
                pNorth="Type a pdb-ID after each protein name. Optionally append underscore chain like \"1IRU_D\".",
                pCenter=scrllpn(_taByHand),
                pButtons=pnl(HBL,pnl(new ChButton("M").t("Apply").li(this)));
            pnl(this,CNSEW,pCenter,pNorth,pButtons);
        }

        final Object data[][]=_tData=new Object[pp.length][COLS];
        for(int iP=pp.length; --iP>=0;) {
            final List v=new ArrayList();
            v.add(COMPUTING);
            data[iP][COL_P]=pp[iP];
            data[iP][COL_R]=new ChCombo(ChJTable.DEFAULT_RENDERER, v);
        }
        _jt=new ChJTable(null, ChJTable.DEFAULT_RENDERER|ChJTable.ICON_ROW_HEIGHT|ChJTable.NO_REORDER);
        final String[]
            titles="Protein;Blast search Hit in the database of protein structures (PDB);Alignment;Web;3D;Details;Apply".split(";"),
            tips=splitTokns(getHlp(this),chrClas1('|'));
        _jt.headerTip(tips).setModel(new ChTableModel(0L, titles).setData(_tData));
        for(int i=COLS; --i>=0;) {
            final String
                apply=0==(_opt&VIEW3D_ON_PRESSING_APPLY)?"Apply" : "Apply & View 3D",
                t=
                i==COL_ALI ? "Preview" :
                i==COL_WWW ? "WWW" :
                i==COL_3D ? "Preview" :
                i==COL_DETAILS ? "Details" :
                i==COL_APPLY ? apply :
                null;
            if (t!=null) {
                _buttons[i]=new ChButton(t).li(this);
                _buttons[i].cp(ChRenderer.KEY_EDITOR_COMPONENT, _buttons[i].cln());
            }

        }
        setW();
        final Object
            pOptions=pnl(
                         "Act on selected table rows: ",
                         new ChButton("GO").t("Associate Structure").li(this),
                         new ChButton("RM").li(this).t("Remove structure")
                         ),
            pSouth=pnl(VB,   pnl(CNSEW,pnl(new java.awt.GridLayout(2,1), _progress),null,null,pnlTogglOpts(pOptions),_cbDetails.cb()), " ",pOptions),
            pan=pnl(CNSEW, scrllpn(_jt), _infoWait, pSouth);
        remainSpcS(this,pan);
        startThrd(thrdM("compute",this,new Object[]{Blaster_SOAP_ebi.class, pp,count01234(pp.length)}));
        startThrd(thrdM("compute",this,new Object[]{Blaster_web_ncbi.class, pp,null                 }));
    }

    public void compute(Object clazz, Protein pp[], int[] order) {
        final Object NOT_FOUND="No 3D-structure found";
        final int type=order==null?0:1;
        final ChPanel progress=_progress[type];
        nextP:
        for(int i=0; i<pp.length; i++) {
            final int iP=order==null?pp.length-i-1:order[i];
            if (_disposed) return;
            if (pp[iP]==null) continue;
            final String msg=niceShrtClassNam(clazz)+": Searching similar structure for "+pp[iP]+". Please wait!";
            progress.setProgress(1+iP*100/(1+pp.length), msg).repaint();
            final Object row[]=_tData[iP];
            final ChCombo combo=(ChCombo)row[COL_R];
            final List v=(List)combo.data();
            for(int k=sze(v); --k>=0;) if (get(k,v)!=null && get(k,v)!=COMPUTING) continue nextP;
            final SimilarPdbFinderBlast sim=new SimilarPdbFinderBlast();
            sim.setBlaster(clazz);
            sim.setProtein(pp[iP]);
            final SimilarPdbFinder.Result[] rr=sim.getSimilarStructures();

            v.remove(COMPUTING);
            v.remove(NOT_FOUND);
            adAll(rr, v);
            if (sze(v)==0) v.add(NOT_FOUND);
            else {
                for(int col=COLS; --col>=0;) {
                    if (_buttons[col]!=null) row[col]=_buttons[col];
                }
                pcp(Protein.KEY_SUGGEST_PDB_ID, rr[0].getPdbID(), rr[0].getProtein());
            }
            revalAndRepaintC(combo);
            ChDelay.repaint(_jt, 33);
        }
        rmFromParent(_infoWait);
        if (type==0) progress.setProgress(0, "Computation finished. Choose a PDB-Chain (2nd column) and click \"Apply\" (last table column)" ).repaint();
        else rmFromParent(progress);
        runR(gcp(KEY_RUN_AFTER,deref(_main),Runnable.class));
    }
    public void dispose() {
        _disposed=true;
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final int iButton=idxOf(q,_buttons);
        if (q==_cbDetails) setW();
        if (iButton>=0) {
            final int iRow=_jt.getEditingRow();
            final SimilarPdbFinder.Result r=result(iRow);
            if (r==null) return;
            final Protein p=r.getProtein();
            final String id_chain=delPfx("PDB:",r.getPdbID()), id=delLstCmpnt(id_chain,'_');
            if (iButton==COL_ALI) {
                final int iChain=id_chain.lastIndexOf('_');
                final char chain=iChain<0?0:chrAt(iChain+1,id_chain);
                final BA ba=new BA(3333)
                    .a("This alignment will show how well the associated structure file fits the sequence under consideration.\nDownloading ")
                    .a(id).a(" chain=").a(chain).a(", please wait ... ");
                final ChTextView view=new ChTextView(ba);
                view.tools().showInFrame(p+" ./. "+id_chain);
                startThrd(thread_showAli(p, id,  chain,  ba, view));
            }
            if (iButton==COL_WWW) ChTextComponents.showFrameWeblinks(Customize.pdbLinks, new BA(0).join(ChTextComponents.webLinks(id, custSettings(Customize.pdbLinks))), "B");
            if (iButton==COL_3D) {
                final Protein3d p3d=new Protein3d();
                pcpAddOpt(KEY_CLOSE_OPT,CLOSE_DISPOSE,p3d);
                new ChFrame(id_chain).ad(p3d).shw(ChFrame.AT_CLICK|ChFrame.DISPOSE_ON_CLOSE|CLOSE_CtrlW_ESC);
                startThrd(Protein3dUtils.thread_addPdbId(id_chain,p3d, Protein.class));
            }
            if (iButton==COL_DETAILS) shwCtrlPnl(ChFrame.AT_CLICK|ChFrame.PACK, "Control-panel "+id_chain, r._simPdbFinder);
            if (iButton==COL_APPLY && p!=null) {
                final Protein pp[]={p};
                final Runnable after=0==(_opt&VIEW3D_ON_PRESSING_APPLY) ? null : StrapAlign.thread_new3dBackbone(0L, pp, null);
                startThrd(thrdRRR(SPUtils.threadInferCoordinates(0, pp, new String[]{id_chain})[0], after));
            }
        }
        if (cmd=="GO" || cmd=="RM") {
            final int N=sze(_tData), ii[]=ChJTable.selectedRowsConverted(_jt);
            final boolean all=sze(ii)==0 && (cmd=="RM" || ChMsg.yesNo(plrl(N,"Set 3D-coordinates for %N protein%S?")));
            final String ids[]=new String[_tData.length];
            final Protein ppInf[]=new Protein[ids.length];
            for(int i=all?N:sze(ii); --i>=0;) {
                final int iR=i<sze(ii) ? ii[i] : i;
                final SimilarPdbFinder.Result r=result(iR);
                final Protein p=r==null?null:r.getProtein();
                if (p==null) continue;
                if (cmd=="GO") { ppInf[iR]=p; ids[iR]=r.getPdbID(); }
                if (cmd=="RM" && p.getInferredPdbID()!=null) ppInf[iR]=p;
            }
            if (countNotNull(ppInf)!=0) {
                if (cmd=="RM" && ChMsg.yesNo(plrl(countNotNull(ppInf), "Remove associated 3D-structures from %N protein%S?"))) {
                    for(Protein p : ppInf) if (p!=null) p.detach3DStructure();
                    StrapEvent.dispatchLater(StrapEvent.ATOM_COORDINATES_CHANGED,1);
                }
                if (cmd=="GO") for(Runnable r : SPUtils.threadInferCoordinates(0, ppInf, ids)) startThrd(r);
            }
        }
        if (cmd=="M") {
            final BA ba=toBA(_taByHand);
            final byte T[]=ba.bytes();
            final int[] ends=ba.eol();
            final ChTokenizer TOK=new ChTokenizer();
            final Protein all[]=StrapAlign.proteins(), pp[]=new Protein[ends.length];
            final String ids[]=new String[ends.length];
            for(int iL=0; iL<ends.length; iL++) {
                final int b=iL==0?0:ends[iL-1]+1,  e=ends[iL];
                TOK.setText(T,b,e);
                final String prot=TOK.nextAsString(), pdb=TOK.nextAsString();
                final Protein p=SPUtils.proteinWithName(prot, all);
                if (p!=null) {
                    pp[iL]=p;
                    ids[iL]=pdb;
                }
            }
            for(Runnable r : SPUtils.threadInferCoordinates(0, pp, ids)) startThrd(r);
        }
    }
    private SimilarPdbFinder.Result result(int row) {
        final ChCombo combo=row<0?null:(ChCombo)_tData[row][COL_R];
        return combo==null?null: deref(combo.getSelectedItem(), SimilarPdbFinder.Result.class);
    }
    private static Runnable thread_showAli(Protein p, String pdbId, char chain, BA ba, JComponent ta) {
        return thrdM("showAli", DialogSimilarStructureResult.class, new Object[]{p,pdbId,charObjct(chain),ba,ta});
    }
    public static void showAli(Protein p, String pdbId, char chain, BA ba, JComponent ta) {
        final byte seq[]=SequenceForPdbID.getSequence(pdbId, chain);
        final byte rt[]=p==null?null:p.getResidueTypeExactLength();
        final SequenceAligner aligner=AlignUtils.newDefaultAligner2();
        aligner.setSequences(new byte[][]{seq,rt});
        aligner.compute();
        final byte[][] aligned=aligner.getAlignedSequences();
        if (sze(aligned)==2) {
            AlignUtils.aliWithMidline(AlignUtils.PRINT_ALI_TRUNCATE, aligned[0], 1, pdbId+(chain!=0?"_"+chain:""), aligned[1], 1, toStrg(p), 60, ba.a('\n'));
            ChDelay.repaint(ta,22);
            packW(ta);

        }
    }

}
