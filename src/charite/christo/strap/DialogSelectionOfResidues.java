package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class DialogSelectionOfResidues extends AbstractDialogJPanel implements java.awt.event.ActionListener, IsEnabled {
    private final ResidueSelection _sharedI;
    private final ChCombo _style=AbstractVisibleIn123.newComboStyle().s(VisibleIn123.STYLE_CIRCLE).li(this);
    private final ProteinList _ppl=new ProteinList(0);

    private final Object
        _class,
        _bColor=new ButColor(0, C(0xffFFAFAF), this).setOptions(ChButton.NO_FILL|ChButton.NO_BORDER|ChButton.ICON_SIZE),
        _cb1D=cbox(true, "1D", "Mark selected residues in alignment",this),
        _cb3D=cbox(true, "3D", "Mark selected residues in 3d-wire representation", this),
        _cbSP=cbox(true, StrapAlign.VIEW_SB, "mark selected residues in horizontal scroll-bar", this),
        _pleaseSelect=pnl("You need to select proteins from the list",KOPT_HIDE_IF_DISABLED),
        _pStyle=pnl(KOPT_HIDE_IF_DISABLED,
                    _cb1D, _cb3D, _cbSP, _bColor,
                    _style);

      private final List<ResidueSelection> _v=new ArrayList();

    public DialogSelectionOfResidues(ResidueSelection sharedInstance, Object classSel) {
        _sharedI=sharedInstance;
        _class=classSel;
        pcp(KEY_ENABLED, wref(this), _ppl.li(this));

        final Object
            pListHet=sharedInstance instanceof charite.christo.strap.extensions.Distance3DToHetero ? pnl("To obtain a complete list of all hetero groups press", new ChButton("ALL_HETEROS").li(this).t("List all heteros")) : null,
            ctrl=sharedCtrlPnl(sharedInstance, true),
            pStyle=pnl(KOPT_HIDE_IF_DISABLED, _pleaseSelect, _pStyle),
            pNorth=javaVsn()>15?null:pnl(CNSEW,shrtClasNam(sharedInstance),null,null, ChButton.doClose15(CLOSE_DISPOSE,this)),
            pEast=ctrl==null?null:pnl(CNSEW, scrllpn(ctrl), pListHet),
            pCenter=pnl(new java.awt.GridLayout(1,2), pnl(CNSEW, _ppl.scrollPane(),"Select proteins"),  pEast),
            pan=pnl(CNSEW,pCenter,pNorth, pStyle);
        remainSpcS(this,pan);
        pcp(KOPT_TRACKS_VIEWPORT_WIDTH,"",ctrl);
        StrapAlign.addListener(deref(sharedInstance,StrapListener.class));
        enableDisable();
    }
    private void enableDisable() {
        final int N=sze(_ppl.selectedProteins());
        setEnabld(N>0, _pStyle);
        setEnabld(N==0, _pleaseSelect);
    }

    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        boolean dispatch=false;
        if (q==_ppl) {
            myUpdate();
            dispatch=true;
            enableDisable();
        }
        if (q==_cb1D || q==_cb3D || q==_cbSP || q==_bColor || q==_style)  {
            if (q==_style || q==_bColor) setSelctd(true,_cb1D);
            myUpdate();
            dispatch=true;
            if (q==_cbSP) StrapAlign.strapSB();
        }
        if (dispatch) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        if (cmd=="ALL_HETEROS") {
            final Protein pp[]= _ppl.selectedProteins();
            SPUtils.showChildObjects(HeteroCompound.class, 0, pp.length>0?pp:StrapAlign.proteins());
        }
    }
    public boolean isEnabled(Object p) {
        return _sharedI instanceof IsEnabled ? ((IsEnabled)_sharedI).isEnabled(p) : true;
    }

    private void myUpdate() {
        removeAllResidueSelections();
        for(Protein p:_ppl.selectedProteins()){
            if (p==null || !isEnabled(p)) continue;
            final ResidueSelection s=mkInstance(_class,ResidueSelection.class,true);
            if (s==null) continue;
            setSharedParas(_sharedI,s);
            s.setProtein(p);
            final ResidueSelection sel=s instanceof VisibleIn123 ? s : new BasicResidueSelection(s);
            _v.add(sel);
            p.addResidueSelection(sel);
        }
        final int m=(isSelctd(_cb1D) ? VisibleIn123.SEQUENCE : 0) +  (isSelctd(_cb3D) ? VisibleIn123.STRUCTURE : 0) +   (isSelctd(_cbSP) ? VisibleIn123.SB : 0);
        final int style=_style.i();
        for(int i=sze(_v); --i>=0;) {
            final VisibleIn123 v = get(i,_v, VisibleIn123.class);
            if (v==null) continue;
            v.setVisibleWhere(m);
            v.setColor(colr(_bColor));
            v.setStyle(style);
        }
    }
    private void removeAllResidueSelections() {
        for(int i=sze(_v); --i>=0;) {
            final ResidueSelection s = get(i,_v, ResidueSelection.class);
            final Protein p=s==null?null:s.getProtein();
            if (p!=null) p.removeResidueSelection(s);
        }
        _v.clear();
    }
    @Override public final void dispose() {
        removeAllResidueSelections();
        StrapAlign.rmListener(_sharedI);
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
    }

}
