package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.protein.StrapEvent.*;
import static charite.christo.strap.SPUtils.sp;
/**HELP

With this dialog numeric data is plotted along the sequence/alignment.
Computational methods may compute numbers for residue positions of a single protein or for alignment positions.

<i>SEE_DIALOG:DialogPlot</i>

@author Christoph Gille
*/
public class DialogPlot extends AbstractDialogJTabbedPane implements ValueOfResidue, ValueOfAlignPosition, ResidueSelection, java.awt.event.ActionListener,IsEnabled, VisibleIn123, StrapListener, HasMC  {
    /* --- Fields for Plot ---  */
    private final HasMC _hasValues;
    private final ValueOfResidue _vor;
    private final ValueOfAlignPosition _vap;
    private ProteinList _listP;

    /* --- Fields for ResidueSelection ---  */

    private boolean[] _selectedAA;
    private int _selectedOffset;
    private ChSlider _sliderL, _sliderU;
    /* --- Fields for both, ResidueSelection and Plot ---  */
    private Object _cbShow;
    private final JComponent _panel;
    private final ChButton
        CB[]={
        toggl("1D").li(this).tt("Show in alignment pane").s(true),
        toggl(StrapAlign.VIEW_SB).li(this).tt("Show in scroll-bar").s(true).li(StrapAlign.li()),
        toggl("3D").li(this).tt("Show in 3D-backbones")
    };

    private final ProteinCombo _comboProt;
    private Color _color=Color.GREEN;
    private int _mc=-1;
    private JTabbedPane _tabbed;

    /* --- Fields  and Methods for parent ---  */
    private final ChCombo
        _choiceClass=SPUtils.classChoice2(ValueOfResidue.class, ValueOfAlignPosition.class),
        _comboStyle=AbstractVisibleIn123.newComboStyle().li(this);

    public DialogPlot() {
        _hasValues=null;
        _vap=null;
        _vor=null;
        _comboProt=null;
        _panel=null;
        final JComponent
            pCenter=pnl(CNSEW, pnl("Chose method: ",_choiceClass.panel()),
                        dialogHead(this),
                        pnl(new ChButton("GO").t("Plot").li(this))
                        );

        for(ChButton b : childsR(pCenter,ChButton.class)) {
            if (gcp(ChButton.KEY_SHARED_CTRL,b)!=null) rmFromParent(b);
        }

        adMainTab(scrllpn(pCenter), this,null);

    }
    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    private DialogPlot(HasMC v, Object sharedCtrlPanel, JTabbedPane tabbed) {
        _hasValues=v;
        _tabbed=tabbed;
        TabItemTipIcon.set(dTab(v),dItem(v),dTip(v),dIcon(v),this);
        if (v instanceof ValueOfAlignPosition) {_vor=null; _vap=(ValueOfAlignPosition)v;} else { _vor=(ValueOfResidue)v; _vap=null; }
        final boolean isVor=_vor!=null;
        StrapAlign.addListener(deref(v, StrapListener.class));
        _comboProt=new ProteinCombo(0);
        _listP=!isVor ? new ProteinList(0L) : null;
        _cbShow=isVor?cbox(true, "Show",null,this) : null;
        if (_comboProt!=null) pcp(KEY_ENABLED, wref(this),_comboProt.li(this));
        if (_listP!=null) pcp(KEY_ENABLED, wref(this),_listP.li(this));

        final Object
            pNorth=pnl(CNSEW, dialogHead(v),null,null,  ChButton.doClose15(0,this)),
            ctrlPanel=ctrlPnl(v),
            pCenter=_listP!=null ? pnl(CNSEW,_listP.scrollPane(), pnl(VBHB, sharedCtrlPanel,"Select proteins:"),ctrlPanel) :
            pnl(CNSEW,sharedCtrlPanel,_comboProt, ctrlPanel),
            pSouth=pnl(
                       _cbShow,
                       new ChButton("SEL").t("Select residues").li(this),
                       new ChButton("TABLE").t("As table").li(this),
                       new ButColor(0,Color.GREEN,this).setOptions(ChButton.NO_FILL|ChButton.NO_BORDER|ChButton.ICON_SIZE),
                       isVor ? StrapView.button(StrapView.TOG_PLOT_ENTIRE_PANEL).cb() : null,
                       REMAINING_VSPC1
                       );
        _panel=pnl(CNSEW,pCenter,pNorth,pSouth);
        addCP(KEY_CLOSE_HOOKS,wref(this),_panel);
        StrapView.getPlottersV().add(this);
        StrapAlign.addListener(this);
    }

    public Color getColor() { return _color; }
    public void setColor(Color c) {
        final int ev=CB!=null ? RESIDUE_SELECTION_CHANGED_COLOR : _vor!=null ? VALUE_OF_RESIDUE_CHANGED_COLOR : VALUE_OF_ALIGN_POSITION_CHANGED_COLOR;
        if (c!=_color) StrapEvent.dispatchLater(ev, 111);
        _color=c;
    }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final Protein p=getProtein(), pp[]=getProteins();
        if (cmd=="GO"){
            Object o=mkInstance(_choiceClass,  ValueOfAlignPosition.class, false);
            if (o==null) o=mkInstance(_choiceClass,  ValueOfResidue.class, false);

            adTab(CLOSE_DISPOSE, shrtClasNam(o), new DialogPlot((HasMC)o,sharedCtrlPnl(_choiceClass,true),this)._panel, this);
        }
        if (cmd=="SEL") {
            rmFromParent(q);
            addActLi(this,_sliderL=new ChSlider("sliderLower",0,100,  0).endLabels("Values that are higher than ...",null));
            addActLi(this,_sliderU=new ChSlider("sliderUpper",0,100,100).endLabels("Values that are lower than ...",null));

            _panel.add(
                       pnl(VB, "#TB Select Residues ", _sliderL, _sliderU, pnl(CB[0].cb(),CB[2].cb(),CB[1].cb()), pnl(_comboStyle),  _vap==null?null:pnl(_comboProt)),
                       BorderLayout.EAST);

            updateSelection();
        }
        if (cmd=="TABLE") {
            final BA sb=new BA(99);
            JTable table=null;
            final ChRenderer rend=new ChRenderer();
            monospc(rend.label());
            final double dd[]=_vap!=null ? _vap.getValues() : _vor!=null ? _vor.getValues() : null;
            if (p!=null && _vor!=null && _vap==null && dd!=null) {
                final Object data[][]=new Object[mini(dd.length,p.countResidues())][2];
                final int i0=Protein.firstResIdx(p);
                for(int i=0; i<data.length; i++) {
                    sb.clr();
                    final int resNum[]=p.getResidueNumber();
                    if (resNum!=null) {
                        sb.a(get(i,resNum));
                        final int ins=p.getResidueInsertionCode(i);
                        if (ins>0) sb.a((char)ins);
                        final byte chain=p.getResidueChain(i);
                        if (chain>0) sb.a(':').a((char)chain);
                    }
                    data[i]=new Object[]{intObjct(i+1+i0), toStrg((char)p.getResidueType(i)), toStrg(sb), toStrg(dd[i])};
                }
                table=new JTable(data,new Object[]{"Idx","Amino acid","residue Number","value"});
            }
            if (_vap!=null && dd!=null && sze(pp)>0) {
                {
                    final int maxCol=SPUtils.maxColumn(pp);
                    final Object data[][]=new Object[mini(maxCol,dd.length)][2];
                    for(int col=0; col< data.length; col++) {
                        sb.clr();
                        for(Protein prot:pp){
                            final int idx=prot.column2indexZ(col);
                            sb.a( idx<0 ? '.' : (char)prot.getResidueType(idx));
                        }
                        data[col]=new Object[]{intObjct(col+1),toStrg(sb), toStrg(dd[col])};
                    }
                    table=new JTable(data,new Object[]{"alignment column","residues","value"});
                }
                final Protein pCurs=StrapAlign.cursorProtein();
                if (pCurs!=null) {
                    final Object data[][]=new Object[pCurs.countResidues()][3];
                    final Object FLOAT_NaN=new Float(Float.NaN);
                    for(int i=0; i<data.length; i++) {
                        final int col=pCurs.getResidueColumnZ(i);
                        final Object val= (col>=0 && col<dd.length) ?  new Float(dd[col]) : FLOAT_NaN;
                        sb.clr();
                        final int resNum[]=pCurs.getResidueNumber();
                        if (resNum!=null) {
                          sb.a(get(i,resNum));
                            final int ins=p.getResidueInsertionCode(i);
                            if (ins>0) sb.a((char)ins);
                            final byte chain=pCurs.getResidueChain(i);
                            if (chain>0) sb.a(':').a((char)chain);
                        }
                        data[i]=new Object[]{intObjct(i+1), toStrg(((char)pCurs.getResidueType(i))), toStrg(sb), intObjct(col+1),val};
                    }
                    final JTable tab=new JTable(data,new Object[]{"idx","aa","pdb-no","alignment column","value"});
                    tab.setDefaultRenderer(Object.class,rend);
                    final Object pMain=pnl(CNSEW,scrllpn(tab),pCurs+"   (cursor)","use copy and paste Ctrl+A  Ctrl+C Ctrl+V");
                    new ChFrame("values from alignment positions mapped to protein at cursor").ad(pMain).shw(ChFrame.STAGGER);
                }
            }
            if (table!=null) {
                table.setDefaultRenderer(Object.class,rend);
                new ChFrame(_hasValues.getClass().getName())
                    .ad(pnl(CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE, table),null,"use copy and paste Ctrl+A Ctrl+C Ctrl+V"))
                    .shw();
            }
        }
        if (q instanceof ChSlider) {
            if (_sliderU.getValue()<_sliderL.getValue())
            if (q==_sliderU) _sliderL.setValue(_sliderU.getValue()); else _sliderU.setValue(_sliderL.getValue());
            CB[0].s(true);
            final double vv[]=getValues();
            if (vv!=null)  {
                double maxVal=Double.MIN_VALUE, minVal=Double.MAX_VALUE;
                for(double val:vv) { if (maxVal<val) maxVal=val; if (minVal>val) minVal=val; }
                final double
                    f=(maxVal-minVal)/100,
                    lower= minVal+_sliderL.getValue()*f,
                    upper= minVal+_sliderU.getValue()*f;
                if (q==_sliderU) _sliderU.endLabels("Values that are >"+(float)upper,null);
                else _sliderL.endLabels("Values that are <"+(float)lower,null);
            }
        }
        if (CB!=null) updateSelection();
        for(int isSelect=CB!=null ? 2 : 1; --isSelect>=0;) {
            int evType=0;
            if (q==_comboStyle) evType=RESIDUE_SELECTION_CHANGED_COLOR;
            if (q==_listP || q==_comboProt || q instanceof ChButton || q instanceof ChSlider) evType=isSelect>0 ? RESIDUE_SELECTION_CHANGED : VALUE_OF_RESIDUE_CHANGED;
            if (evType!=0)  StrapEvent.dispatchLater(evType, 111);
        }
    }
    public final boolean isEnabled(Object o) {
        final Object v=_hasValues;
        return o==null ? false : v instanceof IsEnabled ? ((IsEnabled)v).isEnabled(o) : true;
    }
    public Protein getProtein() { return  _vor!=null ? sp(_vor) : sp(_comboProt);}
    public Protein[] getProteins() { return _vap!=null ? _vap.getProteins() : null; }
    public void setProteins(Protein... proteins) {}
    public void setProtein(Protein p) { if (_comboProt!=null) _comboProt.s(p); }

    public double[] getValues() {
        if (notSelctd(_cbShow)) return null;
        if (_vor!=null) {
            if (_comboProt!=null) {
              final Protein pCombo=sp(_comboProt);
              if (sp(_vor)!=pCombo) _vor.setProtein(pCombo);
            }
            return _vor.getValues();
        } else if (_vap!=null) {
            Protein pp[]=_listP.selectedProteins();
            if (pp.length==0) pp=StrapAlign.visibleProteins();
            final int mc=modic(_listP);
            if ( (_mc!=mc) || sze(_vap.getProteins())!=pp.length) { _mc=mc; _vap.setProteins(pp); }
            return _vap.getValues();
        } else return null;
    }
    @Override public void dispose() {
        for(Protein p: StrapAlign.proteins()) p.removeResidueSelection(this);
        StrapAlign.rmListener(_hasValues);
        dispos(_hasValues);
        StrapEvent.dispatchLater(RESIDUE_SELECTION_CHANGED,111);
        StrapEvent.dispatchLater(VALUE_OF_RESIDUE_CHANGED,111);
        StrapEvent.dispatchLater(VALUE_OF_ALIGN_POSITION_CHANGED,111);
        StrapView.getPlottersV().remove(this);
        if (_tabbed!=null) _tabbed.remove(_panel);
        super.dispose();
    }

    /* --- Methods for ResidueSelection and Plotting  --- */
    public int mc() {
        getValues();
        return modic(_sliderL)+modic(_sliderU)+modic(_comboProt)+modic(_vor)+modic(_vap);
    }
    /* --- Methods for ResidueSelection --- */
    public void setSelectedAminoacids(boolean bb[], int offset) {}
    public int getSelectedAminoacidsOffset() {
        getSelectedAminoacids();
        return _selectedOffset;
    }
    private void updateSelection() {
        final Protein prot=sp(_comboProt);
        for(Protein p : StrapAlign.proteins()) {
            if (p!=prot) p.removeResidueSelection(this);
        }
        if (prot!=null) {
            prot.addResidueSelection(this);
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        }
    }
    private int _selMC;
    public boolean[] getSelectedAminoacids() {
        if (_sliderL==null) return null;
        final Protein p=sp(_comboProt);
        final double[] vv=getValues();
        //putln("getSelectedAminoacids "+p+"  "+vv);
        if (vv==null || p==null) return null;
        final int mc=mc();
        if (_selectedAA==null || _selMC!=mc) {
            _selMC=mc;
            Protein.incrementMC(ProteinMC.MC_RESIDUE_SELECTIONS, p);
            final int nR=p.countResidues();
            final boolean bb[]=redim(_selectedAA, nR, 99);
            double maxVal=Double.MIN_VALUE, minVal=Double.MAX_VALUE;
            for(double v:vv) { if (maxVal<v) maxVal=v; if (minVal>v) minVal=v; }
            final double
                f=(maxVal-minVal)/100,
                lower= minVal+_sliderL.getValue()*f,
                upper= minVal+_sliderU.getValue()*f;
            for(int iA=nR; --iA>=0; ) {
                final int i=_vor!=null ? iA : p.getResidueColumnZ(iA);
                final double v=i>=0 && i<vv.length ? vv[i] : Double.NaN;
                bb[iA]=!Double.isNaN(v) && v<=upper && v>=lower;
            }
            _selectedAA=bb;
            _selectedOffset=Protein.firstResIdx(p);
        }
        return _selectedAA;
    }

    public void setStyle(int s) { _comboStyle.s(s); }
    public int getStyle() {return _comboStyle.getSelectedIndex();}
    public int getVisibleWhere() {
        if (CB==null) return 0xFFffFFff;
        return
            (CB[0].s() ? VisibleIn123.SEQUENCE : 0) |
            (CB[1].s() ? VisibleIn123.SB : 0) |
            (CB[2].s() ? VisibleIn123.STRUCTURE : 0);
    }
    public void setVisibleWhere(int w){
        if (CB!=null) {
            CB[0].s( (w&VisibleIn123.SEQUENCE)!=0);
            CB[2].s( (w&VisibleIn123.STRUCTURE)!=0);
            CB[1].s( (w&VisibleIn123.SB)!=0);
        }
    }

    public final void handleEvent(StrapEvent ev) {
        if (CB!=null && ev.getType()==VALUE_OF_ALIGN_POSITION_CHANGED) {
            getValues();
            if (mc()!=_mc) StrapEvent.dispatchLater(RESIDUE_SELECTION_CHANGED,111);
        }
    }
}
