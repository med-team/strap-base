package charite.christo.strap;
import charite.christo.protein.*;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
/**HELP
   A set of proteins is generated from the selected proteins<br>
   where the sequence identity ratio is not higher than the given threshold.<br><br>

   Proteins are excluded from the set for which another very similar protein exists.<br>
   This procedure is rather primitive and more sophisticated programs for this task exist (E.g. nrdb90).<br><br>
   Because it is aligning each sequence against all others by ClustalW the time rises with the square of the number of sequences.<br><br>
   Enter the threshold ( 0 ... 1 ).<br>
   The larger the number, the more sequences  will be returned.

   @author Christoph Gille
   see also http://www.ebi.ac.uk/~holm/nrdb90/nrdb90.pl.txt

*/
public class DialogNonRedundantSequenceSet extends AbstractDialogJTabbedPane implements ActionListener  {

    private final ChButton _cbCurrent=toggl("Use current alignment").s(true);
    private final ChCombo _comboClass=SPUtils.classChoice(SequenceAligner.class).s(AlignUtils.defaultAligner2Class());
    private final ChTextField _tfThreshold=new ChTextField("0.8").cols(6,true,true);
    private final ProteinList _proteinList=new ProteinList(0L).selectAll(0);

    public DialogNonRedundantSequenceSet() {
        final Object
            pClass=pnl(HBL,"Method: ",_comboClass.panel()),
            pEast=pnl(VBHB,
                       _cbCurrent.doCollapse(true,pClass).cb(),
                       pClass,
                       pnl(HBL,"Sequence similarity threshold (0...1) ", _tfThreshold, " (Larger values, more proteins)"),
                       pnl(new ChButton("GO").t(ChButton.GO).li(this))
                       ),
            pWest=scrllpn(_proteinList),
            pMain=pnl(CNSEW,pnl(new java.awt.GridLayout(1,2), pWest,pEast),dialogHead(this), pnl(REMAINING_VSPC1));
        adMainTab(pMain,this, null);
    }

    public void actionPerformed(ActionEvent ev) {
        final String cmd=ev.getActionCommand();
        if (cmd=="GO") {
            final float th=(float)atof(_tfThreshold);
            final Protein pp[]=_proteinList.selectedProteins();
            final Class clazz=isSelctd(_cbCurrent)?null:clas(_comboClass);
            if (sze(pp)<3) error("Please select at least 3 proteins");
            else if (!(th>=0 && th<=1)) StrapAlign.errorMsg("The threshold value must be in the range of 0 to 1");
            else {

                adTab(DISPOSE_CtrlW, shrtClasNam(clazz)+" "+th, new Result(clazz, pp, th), this);
            }
        }
    }

    private static class Result extends ChPanel implements Runnable,ActionListener {
        private final float _threshold;
        private final Class _clazz;
        private final Protein[] _pp;
        private final ChButton labProgress=labl(), butHide=new ChButton("Show only these proteins; hide all others").enabled(false).li(this);
        private final ChJList jlProteins=StrapAlign.showInJList(0L, null, new Object[0], null, null);
        public Result(Class clazz, Protein pp[], float threshold) {
            _threshold=threshold;
            _clazz=clazz;
            _pp=pp;
            final Object
                pNorth=(_clazz==null ? "As aligned in Strap":"Compare proteins with "+shrtClasNam(_clazz))+"<br>Threshold="+_threshold,

                pan=pnl(CNSEW, scrllpn(SCRLLPN_INHERIT_SIZE,jlProteins),pNorth,pnl(VBHB,labProgress,butHide));
            remainSpcS(this,pan);
            startThrd(this);
        }
        public void actionPerformed(ActionEvent ev) {
            final Object q=ev.getSource();
            if (q==butHide) {
                final Protein pp[]=StrapAlign.proteins();
                final List v=jlProteins.getList(), vHide=new ArrayList(pp.length);
                for(Protein p : pp) {
                    if (!v.contains(p)) vHide.add(p);
                }
                StrapAlign.setIsInAlignment(true,  99, toArry(v,Protein.NONE));
                StrapAlign.setIsInAlignment(false, 99, toArry(vHide,Protein.class));
            }
        }

        public void run() {
            adAll(nonredundantSequenceSet(_pp,_threshold, _clazz, labProgress), jlProteins.getList());
            revalAndRepaintC(jlProteins);
            setEnabld(true, butHide);
        }
    }
    private static Protein[] nonredundantSequenceSet(Protein pp0[],float threshold, Class clazz, Object progress) {
        final Protein pp[]=pp0.clone();
        final boolean done[][]=new boolean[pp.length][pp.length];
        final byte[] ss[]=new byte[2][];
        final int N=pp.length*(pp.length-1)/2;
        int count=0;
        for(int i=0;i<pp.length;i++) {
            for(int j=0;j<pp.length;j++) {
                if (i==j || pp[i]==null||pp[j]==null || done[i][j] || done[j][i] ) continue;
                done[i][j]=true;
                ss[0]=pp[i].getResidueTypeExactLength();
                ss[1]=pp[j].getResidueTypeExactLength();
                final int maxLen=maxi(ss[0].length, ss[1].length);
                if (maxLen==0) continue;
                final SequenceAligner aligner=StrapPlugins.instanceSA(clazz,2,true);
                final byte aligned[][];
                final int maxCol;
                if (aligner!=null) {
                    aligner.setSequences(ss);
                    aligner.compute();
                    aligned=aligner.getAlignedSequences();
                    maxCol=MAX_INT;
                } else {
                    aligned=new byte[][]{ pp[i].getGappedSequence(), pp[j].getGappedSequence()};
                    maxCol=1+maxi(pp[i].getMaxColumnZ(), pp[j].getMaxColumnZ());
                }
                if (sze(aligned)<2) continue;
                final Ratio r=AlignUtils.getIdentity(aligned[0],aligned[1],0,maxCol);
                /*
                  putln(aligned[0]);
                  putln(aligned[1]);
                  putln(pp[i], pp[j]+" rrrrr="+r+"\n");
                */
                if (r.getNumerator()/((float)maxLen)>threshold) {
                    if (countLettrs(ss[0],0,MAX_INT) > countLettrs(ss[1],0,MAX_INT)) pp[j]=null; else pp[i]=null;
                }
            }
            if (progress instanceof JLabel) setTxt(" Progress: "+ ++count+" / "+N, progress);
        }
        final Protein[] selected=rmNullA(pp,Protein.class);
        if (progress instanceof JLabel) setTxt("Finished. Selected "+selected.length+"  of "+pp0.length+"  proteins.", progress);
        return selected;
    }

}
