package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**
   A choice menu containing for selecting a protein.
   @author Christoph Gille
*/
public class ProteinCombo extends ChCombo implements HasProtein {
    @Override public Dimension getPreferredSize() { return dim(super.getPreferredSize().width,ICON_HEIGHT);}

    @Override public boolean isEnabled(Object o) {
        return !(o instanceof Protein) ? false :  _filter!=0  ? ProteinList.isEnabled((Protein)o,_filter) : super.isEnabled(o);
    }
    private final long _filter;
    public ProteinCombo(long filter) {
        super(ChJTable.ICON_ROW_HEIGHT, new ProteinComboModel(filter));
        _filter=filter;
        setForeground(null);
        setBG(0xFFffFF,this);
        setRenderer(new ChRenderer().setEnabled(this));
        setLightWeightPopupEnabled(false);
        rtt(this);
        updateOn(CHANGED_PROTEIN_LABEL,this);
        updateOn(CHANGED_SELECTED_OBJECTS,this);
        updateOn(CHANGED_PROTEIN_AT_CURSOR,this);
        monospc(this);
        pcp(KEY_IF_EMPTY,ANSI_FG_BLUE+" No protein present",this);
        selNextEnabled();
    }
    @Override public void actionPerformed(java.awt.event.ActionEvent ev) {
        changed();
        setFG(getProtein()!=null&&isEnabled(getProtein())? 0:0x808080, this);
        super.actionPerformed(ev);
    }
    public void changed() {}
    public Protein getProtein() { return deref(getSelectedItem(), Protein.class);}
}
