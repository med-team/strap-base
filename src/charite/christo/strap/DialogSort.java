package charite.christo.strap;
import java.util.*;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP
   This dialog reorders proteins.
   @author Christoph Gille
*/
public class DialogSort extends ChPanel implements ChRunnable, Comparator {
    public final static int NUM=3, PROTEINS=1, RES_SEL=2;
    private final static Map<String,String> _m2l=new HashMap();
    private final int _opt;
    private Protein[] _prev;
    private final Object
        KEY_ROW=new Object(),
        _choice[]=new Object[NUM],
        _comboKeys[]=new Object[NUM],
        _list,
        _bRunS,
        _bUndo=new ChButton("Undo").r(thrdCR(this,"U")),
        _bRunA;
    private Object _save[];
    private final int[] _m=new int[NUM];

    public DialogSort(int opt, ChJList list) {
        _opt=opt;
        _list=list==null?null:wref(list);
        final String[] title=
            0!=(opt&PROTEINS) ? Protein.INFO_TITLE:
            0!=(opt&RES_SEL) ? ResSelUtils.INFO_TITLE:
        null;
        if (title==null) assrt();

        final Object pp[]=new Object[NUM];
        final Runnable enableKeys=list==null?null:thrdCR(this,"UK");
        for(int i=0; i<NUM; i++) {
            if (list!=null) setEnabld(false,_comboKeys[i]=new ChCombo(ChJTable.CLASS_RENDERER, new ComboKeys(true)));
            pcp(KOPT_HIDE_IF_DISABLED,"",_comboKeys[i]);
            _choice[i]=new ChCombo(title).r(enableKeys);
            final Object cb=toggl().doEnable(_choice[i]).cb();
            pp[i]=pnl(i==0?pnl(prefSze(cb)) : cb, _choice[i]," ",_comboKeys[i]);
            setEnabld(i==0,_choice[i]);
        }

        _bRunS=list!=null?null:new ChButton("Sort selected").r(thrdCR(this,"SS"));
        _bRunA=new ChButton(list!=null?"Sort":"Sort all").r(thrdCR(this,"SA"));
        pnl(this, CNSEW,
            pnl(new java.awt.GridLayout(NUM,1),pp),
            pnl(FLOWLEFT,"Sort by text comparison"),
            pnl(FLOWLEFT, _bRunA, _bRunS, _bUndo),
            "#ETBem"
            );
        setEnabld(false, _bUndo);
    }

    public Object run(String id, Object arg) {
        if (id=="SA") {
            for(int i=NUM; --i>=0;) _m[i]=!isEnabld(_choice[i]) ? -1: ((ChCombo)_choice[i]).i();
        }
        if (_list==null) {
            if (id=="SA") startThrd(thrdCR(this, "R", StrapAlign.visibleProteins()));
            if (id=="SS") {
                final Protein pp[]=StrapAlign.selectedProteinsInVisibleOrder();
                if (pp.length<2) error("Select at least 3 proteins");
                else startThrd(thrdCR(this, "R", pp.clone()));
            }
            if (id=="R") {
                setEnabld(false,_bRunS);
                setEnabld(false,_bRunA);
                final Protein pp[]=(Protein[])arg;
                _prev=StrapAlign.visibleProteins().clone();
                for(int i=pp.length; --i>=0;) pcp(KEY_ROW, intObjct(i), pp[i]);

                Arrays.sort(pp, this);
                StrapAlign.inferOrderOfProteins(0, pp);
                setEnabld(true, _bUndo);
                setEnabld(true, _bRunS);
                setEnabld(true, _bRunA);
            }
            if (id=="U") {
                StrapAlign.inferOrderOfProteins(0, _prev);
                setEnabld(false, _bUndo);
            }
        }
        final ChJList jl=deref(_list, ChJList.class);
        final List v=jl==null?null:jl.getList();
        if (v!=null) {
            if (id=="UK") {
                for(int i=NUM; --i>=0;) {
                    setEnabld( ((ChCombo)_choice[i]).i()==ResSelUtils.INFO_ANNOTATION, _comboKeys[i]);
                }
            }
            if (id=="SA") {
                final Object oo[]=v.toArray();
                _save=oo.clone();
                Arrays.sort(oo, this);
                v.clear();
                adAll(oo,v);
                setEnabld(true,_bUndo);
                jl.repaint();
            }
            if (id=="U" && _save!=null) {
                final List v2=new ArrayList();
                adAll(_save,v2);
                rmAll(_save,v);
                v2.addAll(v);
                v.clear();
                v.addAll(v2);
                setEnabld(false,_bUndo);
                jl.repaint();
            }
        }
        return null;
    }
    public int compare(Object o1, Object o2) {
        if (0!=(_opt&(RES_SEL|PROTEINS))) {
            final boolean isp=0!=(_opt&PROTEINS);
            final Protein p1=isp && o1 instanceof Protein?(Protein)o1 : null;
            final Protein p2=isp && o2 instanceof Protein?(Protein)o2 : null;
            final ResidueSelection s1=!isp && o1 instanceof ResidueSelection?(ResidueSelection)o1 : null;
            final ResidueSelection s2=!isp && o2 instanceof ResidueSelection?(ResidueSelection)o2 : null;

            if ((isp?p1:s1)==null) return 1;
            if ((isp?p2:s2)==null) return -1;
            for(int m=0; m<NUM;m++) {
                final int t=_m[m];
                if (t<0) continue;
                final String t1=isp?p1.getInfo(t) : ResSelUtils.info(t,s1, _comboKeys[m]);
                final String t2=isp?p2.getInfo(t) : ResSelUtils.info(t,s2, _comboKeys[m]);
                if (sze(t1)==0) {
                    if (sze(t2)==0) continue;
                    return 1;
                }
                if (sze(t2)==0) return -1;
                String lc1=_m2l.get(t1); if (lc1==null) _m2l.put(t1,lc1=t1.toLowerCase());
                String lc2=_m2l.get(t2); if (lc2==null) _m2l.put(t2,lc2=t2.toLowerCase());
                final int comp=lc1.compareTo(lc2);
                if (comp!=0) return comp;
            }
            if (_list==null) {
                if (isp) return atoi(gcp(KEY_ROW,p1))-atoi(gcp(KEY_ROW,p2));
            } else {
                final ChJList jl=deref(_list, ChJList.class);
                final List v=jl==null?null:jl.getList();
                final int i1=v==null?-1: v.indexOf(o1);
                final int i2=v==null?-1: v.indexOf(o2);
                return i1<0?1:i2<0?-1: i1-i2;
            }

        }
        return 0;
    }
}
