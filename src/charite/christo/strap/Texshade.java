package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/*
   @author Christoph Gille
   under Miktex
   usepackage{C:/StrapAlign/dataFiles/texshade120}

*/
public final class Texshade extends AbstractDialogJTabbedPane implements Disposable,NeedsProteins,ChRunnable, StrapListener, java.awt.event.ActionListener  {
    private final static String
        N_TT_T_B_BB[]="non ttop top bottom bbottom".split(" "),
        L_R_N[]="left right none".split(" "),
        N_T_B[]="non top bottom".split(" ");

    public final static String
        EXAMPLES_RSC="TexshadeExamples.rsc",
        SAVE="texshade/",
        DIR=STRAPTMP+"/texshade/",
        TEX_FILE=DIR+"alignm.tex", MSF_FILE=DIR+"alignm.msf", PDF_FILE=DIR+"alignm.pdf",
        DVI_FILE=DIR+"alignm.dvi",PS_FILE=DIR+"alignm.dvi.ps",PS_POSTER_FILE=DIR+"poster.ps",LOG_FILE=DIR+"alignm.log",AUX_FILE=DIR+"alignm.aux",
        CB_LAB_Postscript="Generate PostScript instead of PDF";
    private static Container _panProtNam;
    private static Protein _pp[];
    private ChFrame _wSrc;
    private final Object _instDetail=
        isMac() ? "Please install at least the BasicTeX subset of\nWIKI:Mactex http://www.tug.org/mactex/\n\n" :
        isWin() ? pnlNeedMiktex() :
        isSystProprty(IS_KNOW_PACKAGE_MANAGER) ? "Please install\n "+installCmdForPckgs("texlive")+"\n\n" :
        "Please install LaTeX (package texlive) \n\n",
        _pChoiceRuler;
    private final ChTextArea
        _taBefore=new ChTextArea("\n\n"), _taIn=new ChTextArea("\n\n"), _taSourceFiles=new ChTextArea("");
    private File _fPdf;
    private static int _fPdfCount=0;
    private ChExec _myExec;
    private ProteinList _subfamilyProtList;
    private static Customize[] _cust=new Customize[3];
    private final ChButton _togDetails=toggl();
    private final JComponent
        _panInst=pnl(VBHB, OPAQUE_FALSE, _instDetail,   new ChButton("").doOpenURL(URL_STRAP+"gallery/figAlignAlphasHydropathy.pdf").t("Example output")," "),
        _panPoster=pnl(HB),
        _panViewers,
        butDvi=new ChButton("dvi").addDnD(file(DVI_FILE)).doExecute(Customize.customize(Customize.dviViewer),file(DVI_FILE)).tt("View DVI file"),
        butDviPs=new ChButton("dvips").doExecute(Customize.customize(Customize.dviPsConverter),file(DVI_FILE)).tt("Convert dvi to postscript"),
        butPS=new ChButton("PS").addDnD(file(PS_FILE)).doViewFile(file(PS_FILE)),
        butPdf=new ChButton("PDF").addDnD(file(PDF_FILE)).tt("View PDF-File\nClick with Ctrl-key pressed for details of the external process."),
        butPosterViewPS=new ChButton("view PS-file").addDnD(file(PS_POSTER_FILE)).doExecute(Customize.customize(Customize.psViewer),file(PS_POSTER_FILE)),
        butRunTex=new ChButton("Make PDF").li(this).tt(
            "Processes the .tex file and creates the .pdf file.<br>"+
            "This button is active if "+TEX_FILE+" exists and no <br>other tex-process is running.<br>"+
            "LaTex is run twice unless the Shift-key is hold down."),
        butPoster=new ChButton("Run Poster on<br>"+PS_FILE).li(this),
        cbCustomSize=cb("Custom size"),
        cbLandscape=cbOn("Landscape",null),
        cbSeparationLine=cb("Additional space after alignment","cbSeparationLine"),
        cbUpperCase=cb("Use upper case only"),
        cbHideLegend=cb("Hide legend","cbHideLegend"),
        cbFingerprint=cb("Fingerprint","cbFingerprint"),
        cbHideConsensus=cb("Hide consensus","cbHideConsensus"),
        cbDoFreqCorrect=cb("\\dofrequencycorrection","cbDoFreqCorrect"),
        cbHideSeqs=cb("\\hideseqs");
    private final Object
        cbSubstituteNames=setTip("You may substitute the <br>names of the proteins <br>by any latex expression", cb("Apply substitutions of protein names","displayedProteinName")),
        cbPS=setTip("PDF-LaTeX produces PDF directly. <br>LaTeX generates a DVI <br>which can be converted to PostScript", cb(CB_LAB_Postscript)),
        cbNoExtension=setTip("protein1.swiss would be written as protein1", cbOn("Suppress file suffix in protein file names","noExtension"));
    private final ChCombo
        _choiceConsensus=new ChCombo("Gray BlueRed RedBlue GreenRed RedGreen ColdHot".split(" ")).save(Texshade.class,"choiceConsensus"),
        _choiceConsensusLocation=new ChCombo(N_TT_T_B_BB).save(Texshade.class,"choiceConsensusLocation"),
        _choicePosterSize=new ChCombo("same A0 A1 A2 A4".split(" ")).save(Texshade.class,"choicePosterSize"),
        _choiceShading=new ChCombo("none functional identical similar diverse".split(" ")).s(1).save(Texshade.class,"choiceShading"),
        _choiceShowNames=new ChCombo(L_R_N).save(Texshade.class,"choiceShowNames"),
        _choiceEmph=new ChCombo("bf md up it sl rm sf tt".split(" ")).save(Texshade.class,"choiceEmph"),
        _choiceNumbering=new ChCombo(L_R_N).save(Texshade.class,"choiceShowNumbering"),
        _choiceSeqLogo=new ChCombo(N_T_B).save(Texshade.class,"choiceSequenceLogo"),
        _choiceRuler=new ChCombo(N_TT_T_B_BB).save(Texshade.class,"choiceRuler"),
        _choiceSize=new ChCombo("A0 A1 A2 A3 A4 A5 A6 A7 A8 A9 B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 C0 C1 C2 C3 C4 C5 C6 C7 C8 C9 USletter USlegal USexecutive".split(" ")).s(4).save(Texshade.class,"paperFormat"),
        _choiceSubfamilyLocation=new ChCombo(N_T_B).li(this).save(Texshade.class,"choiceSubfamilyLocation");
    private final ChTextField
        _tfBargraphstretch=new ChTextField(" 1.0 ").ct(Float.class).saveInFile(SAVE+"bargraphstretch"),
        _tfVblockspace=new ChTextField("10").cols(7,true,true).ct(Float.class).saveInFile(SAVE+"vblockspace"),
        _tfMarginL=new ChTextField("0").cols(5,true,true).saveInFile(SAVE+"marginL"),
        _tfMarginR=new ChTextField("0").cols(5,true,true).ct(Integer.class).saveInFile(SAVE+"marginR"),
        _tfMarginT=new ChTextField("5").cols(5,true,true).saveInFile(SAVE+"marginT"),
        _tfMarginB=new ChTextField("0").cols(5,true,true).ct(Integer.class).saveInFile(SAVE+"marginB"),
        _tfResiduesperline=new ChTextField("auto").saveInFile(SAVE+"residuesperline"),
        _tfPaperheight=new ChTextField("auto").cols(6,true,true).ct(Integer.class).saveInFile(SAVE+"paperheight"),
        _tfCustomWidth=new ChTextField("10").cols(6,true,true).ct(Integer.class).saveInFile(SAVE+"customWidth"),
        _tfCustomHeight=new ChTextField("10").cols(6,true,true).ct(Integer.class).saveInFile(SAVE+"customHeight"),
        _tfRulerstep=new ChTextField("10").cols(5,true,true).ct(Float.class).saveInFile(SAVE+"rulerstep"),
        _tfSubfamilyName=new ChTextField().cols(30,true,false).saveInFile(SAVE+"subfamilyName");

    private static String _oneAnno;
    private final Runnable _runEnable=thrdCR(this,"ED");
    private final TexshadeGraph[] _resGraph=new TexshadeGraph[4], _consGraph=new TexshadeGraph[4];
    private final TexshadeSecStructure[] _secGraph=new TexshadeSecStructure[4];
    private final static ChFrame _frameExec=new ChFrame("Texshade messages");
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */

    public Texshade() {
        setEnabld(false,_panInst);
        pcp(KOPT_HIDE_IF_DISABLED,"",_panInst);
        setSettings(this,
                    customize(1),customize(2),
                    Customize.getCustomizableForClass(Texshade.class, Customize.CLASS_Example),
                    Customize.customize(Customize.latex),
                    Customize.customize(Customize.pdflatex),
                    Customize.customize(Customize.latexEditors),
                    Customize.customize(Customize.pdfViewer));
        mkdrsErr(file(DIR));
        for(int loc=4; --loc>=0;) {
            final String s=N_TT_T_B_BB[loc+1];
            _consGraph[loc]=new TexshadeGraph(s, ValueOfAlignPosition.class);
            _resGraph[loc]=new TexshadeGraph(s, ValueOfResidue.class);
            _secGraph[loc]=new TexshadeSecStructure(s);
        }

        _pChoiceRuler=new ProteinCombo(0);
        _panViewers=pnl(HBL,"View document ",butPdf);
        for(String s : new String[]{TEX_FILE,PDF_FILE,LOG_FILE,AUX_FILE,MSF_FILE,DVI_FILE,PS_FILE}) delFile(file(s));
        _pp=null;
        myDoLayout();
        ChThread.callEvery(EDT, 1111, _runEnable,"Texshade");
        inEdtLaterCR(this,"PROBE",null);
    }
    @Override public void dispose() {
        ChThread.callEvery(0, -1, _runEnable,"");
        final ChExec ex=_myExec;
        if (ex!=null) ex.dispose();
        super.dispose();
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Layout >>> */
    @Override public Dimension getMinimumSize() { return dim(0,0);}

    private void myDoLayout() {
        final String INFO_LOCATION="The locations \"top and \"ttop\" are above and \"bottom\" and \"bbottom\" below the alignment.";
        {
            final Object
                msgRestore=
                "RESTORING THE DEFAULT SETTINGS\n\n"+
                "\nAfter closing Strap, You can \n"+
                " - delete lines in the property file "+file(PROPERTY_FILE)+
                "\n\n - delete Files in the folder "+file(ChTextComponents.DIR_SAVED_TEXT+SAVE)+
                "\n\n - press the button [Default settings] in the settings dialogs.",
                pDetails=pnl(VBHB,
                             pnl(HBL," Restore default settings ",ChButton.doView(msgRestore).t("Restore")),
                             " ",
                                 cbPS,
                             " ",
                             new ChButton("SRC").t("Make LaTeX and Multiple Sequence file").li(this).tt("Generates the latex source and the multiple sequence file of the proteins."),
                             " ",
                             butRunTex
                             ),
                pMakePdf=pnl(VBHB,
                             "<b>How does it work: </b> The alignment is exported as PDF. The other tabs provide additional options.<br>"+
                             "If proteins are selected (white on blue in the alignment pane), only those are included. <br>"+
                             "By dragging the mouse in the alignment,  a rectangular region may be specified.",
                             new ChButton("MK").li(this).t("Generate PDF")
                             ),
                p=pnl(VBHB,
                      dialogHead(this),
                      _panInst,
                      pDetails,
                      pMakePdf,
                      " ",
                      _panViewers,
                      _panPoster,
                      " ",
                      " ",
                      pnl(HBL,"Details",_togDetails.doCollapse(true,pMakePdf).doCollapse(false,pDetails))
                      );
            adMainTab(pnl(p), this,null);
        }
        {
            final Object
                h="For PDB-files, helices are drawn <font bgcolor=#ff3333> red </font> and sheets <font bgcolor=#ffff00>yellow</font>. "+
                "The secondary structure information of one PDB-protein can be drawn over the alignment.",
                p=pnl(VBHB,
                       INFO_LOCATION,
                       pnl(HB,"<h2>Secondary structure</h2>","#",smallHelpBut(h)),
                       " ",
                       _secGraph[0].panel(), "#JS",
                       _secGraph[1].panel(), "#JS",
                       _secGraph[2].panel(), "#JS",
                       _secGraph[3].panel()
                       );

            adTab(0, "", TabItemTipIcon.set(null,null,null,iicon(IC_HELIX),pnl(p)), this);
        }
        {
            final Object
                h=
                "By default the file names of the proteins are written in the PDF-output. "+
                "Alternatively,  different names can be defined using  simple text or  complex LaTeX expressions.<br><br>"+
                "Please not that <b>underscore</b> \"_\" has a special meaning in latex and needs to be quoted.",
                p=pnl(VBHB,
                       pnl(HB,"<h2>Substitute protein name</h2>","#",smallHelpBut(h)),
                       cbSubstituteNames,
                       cbNoExtension
                       );
            adTab(0, "Row Header", pnl(p), this);
        }
        {
            final Object
                hBar="<i>PACKAGE:charite.christo.strap.</i>"+
                "Bar charts can be drawn at four different locations: ttop, top,bottom, bbottom<br>"+
                "The values are calculated by instances of the interface <i>ValueOfAlignPosition</i> / <i>ValueOfAlignPosition</i> "+
                "with the specified aligned proteins.<br>",
                p1=pnl(VBHB,
                        pnl(HB,"<h2>Plotting values of sequence positions</h2>","#",smallHelpBut(hBar)),
                        INFO_LOCATION,
                        pnl(HBL,"Bargraphstretch=",new ChTextField(_tfBargraphstretch.getDocument())),
                        " ",
                        _resGraph[0],"#JS",
                        _resGraph[1],"#JS",
                        _resGraph[2],"#JS",
                        _resGraph[3]
                        ),
                p2=pnl(VBHB,
                        pnl(HB,"<h2>Plotting values of alignment positions (\"consensus\")</h2>","#",smallHelpBut(hBar)),
                        INFO_LOCATION,
                        pnl(HBL,"Bargraphstretch=",_tfBargraphstretch),
                        " ",
                        _consGraph[0],"#JS",
                        _consGraph[1],"#JS",
                        _consGraph[2],"#JS",
                        _consGraph[3]
                        );

            adTab(0, "", TabItemTipIcon.set(null,null,null,iicon(IC_PLOT),pnl(p1)),  this);
            adTab(0, "", TabItemTipIcon.set(null,null,null,iicon(IC_PLOT),pnl(p2)),  this);
        }
        {
            final Object
                h=
                "The output is either PostScript or PDF.<br><br>"+
                "<b>Postscript:</b> In contrast to PDF viewers  PostScript viewers usually do not recognize page formats larger than A4."+
                "<h2>Printing over several sheets of paper</h2>"+
                "The PostScript utility \"poster\" allows to print the alignment horizontally over several sheets of paper<br>"+
                "Poster (http://www.geocities.com/SiliconValley/5682/poster.html) is a small utility for making a poster -- a large printed image -- from a PostScript document.<br>"+
                "Postscript output must be activated in the main tab (expand \"Details\") since poster does not work with PDF.<br>",
                p=pnl(VBHB,
                       pnl(HBL,"<h2>Page layout</h2>","#",smallHelpBut(h)),
                       pnl(HBL,"Paper format.",_choiceSize," ",cbLandscape),
                       "A4 is normal paper size in Europe, A2 has twice the  width and A0 is 4 times larger.",
                       pnl(HBL,"Left margin=",_tfMarginL,"mm"),
                       pnl(HBL,"Right margin=",_tfMarginR,"mm"),
                       pnl(HBL,"Top margin=",_tfMarginT,"mm"),
                       pnl(HBL,"Bottom margin=",_tfMarginB,"mm"),
                       pnl(HBL,"Residues per Line=",_tfResiduesperline),
                       pnl(HBL,cbCustomSize," Width=",_tfCustomWidth,"mm  Height=",_tfCustomHeight,"mm")
                       );
            adTab(0, "Page Layout",pnl(p), this);
        }
        {
            final Object
                p=pnl(VBHB,
                      "<h2>Appearance of the Alignment</h2>",
                      pnl(HBL,"Shading Mode",_choiceShading),
                      pnl(HBL,"Protein name",_choiceShowNames),
                      " ",
                      pnl(HBL,"Residue numbers",_choiceNumbering),
                      pnl(HBL,"Ruler: ",_choiceRuler," step: ",_tfRulerstep, _pChoiceRuler),
                      //cbNumAsPdb,
                      " ",

                      pnl(HBL,"Vertical block space ",_tfVblockspace,"mm"),
                      pnl(HBL,cbFingerprint, cbHideLegend),
                      pnl(HBL,"Consensus",_choiceConsensus,_choiceConsensusLocation,cbHideConsensus),
                      pnl(HBL,"Style for emphblock and emphregion: ",_choiceEmph),
                      cbSeparationLine,
                      cbUpperCase
                      );
            adTab(0, "Appearance",pnl(p), this);
        }
        {
            _subfamilyProtList=new ProteinList(0L);
            final Object
                h="A sequence logo visualizes the degree of conservation and the prevalence of amino acid letters at each alignment position.",
                p=pnl(VBHB,
                      pnl(HB,"<h2>Simple Sequence Logo</h2>","#",smallHelpBut(h)),
                      pnl("Location=",_choiceSeqLogo," " ,cbHideSeqs, " ", cbDoFreqCorrect),
                      " ","#JS",
                      "<h2>Subfamily Logo</h2>",
                      pnl("Location=", _choiceSubfamilyLocation, "  Name=",_tfSubfamilyName),
                      "##",
                      _subfamilyProtList.scrollPane()
                      );
            adTab(0, "Sequ Logo", remainSpcS(p), this);
        }
        final String empty="You can type addition LaTeX/TeXshade commands";
        _taBefore.tools().cp(KEY_IF_EMPTY,empty).saveInFile(SAVE+"before.txt");
        _taIn.tools().cp(KEY_IF_EMPTY,empty).saveInFile(SAVE+"within.txt");
        pcp(KEY_IF_EMPTY,"Enter the name of the logo",_tfSubfamilyName);

    }
    /* <<< Layout <<< */
    /* ---------------------------------------- */
    /* >>> Proteins >>> */
    public static Protein[] proteins(){ final Protein pp[]=_pp; return pp!=null?pp:Protein.NONE;}
    public void setProteins(Protein... pp) { _pp=pp;}

    /* <<< Proteins <<< */
    /* ---------------------------------------- */
    /* >>> Src and MSF >>> */
    private void makeMsfAndSource(Protein pp[], int colFrom, int colTo) {
        if (pp==null || pp.length==0) return;
        _pp=pp;
        for(String s : new String[]{MSF_FILE,TEX_FILE,PDF_FILE,LOG_FILE,AUX_FILE,DVI_FILE,PS_FILE}) delFile(file(s));

        final BA sb=new BA(9999);
        final ExportAlignment w=new ExportAlignment();
        w.setProteins(pp);
        w.setColumnRange(colFrom,colTo);
        w.getText(ExportAlignment.MSF| (isSelctd(cbNoExtension) ? ExportAlignment.FILE_SUFFIX : 0), sb);
        wrte(file(MSF_FILE), sb);

        ResSelUtils.useResNum(false);//isSelctd(cbNumAsPdb));
        final BA tex=new BA(9999).join(customize(0).getSettings()).a('\n');
        final int rPerL=atoi(_tfResiduesperline);
        if (isSelctd(cbCustomSize)) tex.a("\\setpapersize{custom}{").a(_tfCustomWidth).a("mm}{").a(_tfCustomHeight).aln("mm}");
        else tex.a("\\setpapersize").a(isSelctd(cbLandscape) ? "[landscape]" : "").a('{').a(_choiceSize).aln('}');
        tex.a("\\setmargrb{").a(_tfMarginL).a("mm}{").a(_tfMarginT).a("mm}{").a(_tfMarginR).a("mm}{").a(_tfMarginB).aln("mm}");
        final float height=(float)atof(_tfPaperheight);
        if (!Float.isNaN(height)) tex.a("\\setlength{\\paperheight}{").a(height).aln("mm}");

        for(String s : new String[]{TEX_FILE,PDF_FILE,LOG_FILE,AUX_FILE,PS_FILE,DVI_FILE}) delFile(file(s));
        if (isSelctd(cbPS)) tex.replace(0L,"\\documentclass[pdftex]","\\documentclass");
        /* --- setends --- */
        //final String setends;
        tex
            .aln("\\begin{document}")
            .aln(_taBefore)
            .join(customize(1).getSettings())
            .aln(_taIn)
            .aln((_choiceShowNames.i()!=2 ? "\n\\shownames{"+_choiceShowNames+"}" : "\\hidenames"))
            .a("\\emphdefault{").a(_choiceEmph).aln('}')
            .aln((_choiceNumbering.i()!=2 ? "\n\\shownumbering{"+_choiceNumbering+"}" : "\\hidenumbering"))
            .a("\\vblockspace{").a(_tfVblockspace).aln("mm}")
            .a("\\bargraphstretch{").a(_tfBargraphstretch).aln('}')
            .aln((isSelctd(cbHideLegend) ? "\\hidelegend" : "\\showlegend"));
        if (isSelctd(cbSeparationLine)) tex.a("\\separationline{").a(pp.length).aln('}');
        if (isSelctd(cbHideConsensus)) tex.aln("\\hideconsensus");
        if (_choiceConsensusLocation.i()>0) tex.a("\\defconsensus{{$\\bullet$}}{{$\\bullet$}}{{$\\bullet$}} \\showconsensus[").a(_choiceConsensus).a("]{").a(_choiceConsensusLocation).aln('}');
        if (isSelctd(cbFingerprint)) tex.a("\\fingerprint{").a(maxi(30,(colTo-colFrom+2))).aln('}');
        if (rPerL>0) tex.a("\\residuesperline*{").a(rPerL).aln('}');
        tex.aln(nameseq()).aln("\n% The setlength commands do not change the output, but may enhance speed");
        boolean[] bbCrop=null;
        for(int iP=0;iP<pp.length;iP++) {
            final Protein p=pp[iP];
            final int idx0=Protein.firstResIdx(p);
            final int aFrom=idx0+p.column2nextIndexZ(colFrom);
            final int i1=ResSelUtils.idxAmino(aFrom, p);
            if (i1!=1) tex.a("\\startnumber{").a(iP+1).a("}{").a(i1).aln('}');
            if (!isSelctd(cbUpperCase)) {
                final byte seq[]=p.getResidueType();
                boolean lc[]=null;
                for(int i=seq.length; --i>=0;) {
                    if (is(LOWR,seq,i)) (lc==null ? lc=new boolean[i+1] : lc)[i]=true;
                }
                if (lc!=null) tex.a("\\lowerregion{").a(iP+1).a("}{").a(ResSelUtils.texshadeRange(lc,idx0,p)).aln('}');
            }
            for(ResidueAnnotation s:p.residueAnnotations()) {
                final boolean bb[]=s.getSelectedAminoacids();
                final int lstTrue=lstTrue(bb);
                if (lstTrue<0) continue;
                final int offset=s.getSelectedAminoacidsOffset();
                final int iaFrom=maxi(offset, aFrom);
                final int iaTo=mini(lstTrue+offset+1, p.countResidues()+idx0, p.column2thisOrPreviousIndex(colTo-1)+idx0+1);
                Arrays.fill(bbCrop=redim(bbCrop, lstTrue+1, 33),false);
                boolean selected=false;
                for(int i=iaFrom; i<iaTo; i++) if (bb[i-offset]) bbCrop[i-offset]=selected=true;
                if (!selected) continue;
                for(ResidueAnnotation.Entry e: s.entries()) {
                    if (e.isEnabled() && ResidueAnnotation.TEXSHADE==e.key()) {
                        final String n=orS(s.getName(),"").replaceAll(REGEX_NO_FILE_NAME," ").replace('_',' ');
                        tex.a(ResSelUtils.replaceVariableBB(e.value(), bbCrop, offset,  n, p, _pp)).a(" % ").aln(p);
                    }
                }
            }
        }
        tex.a('\n');
        if (_oneAnno==null) {
            tex.a("\\threshold{").a(maxi(0,StrapView.sliderSimilarity().getValue())).aln('}');
            final String shade=lCase(toStrg(ShadingAA.choice()));
            final int i=_choiceShading.i();
            if (i==1 && strstr(STRSTR_w, shade, "charge hydropathy chemical")>=0) tex.a("\\shadingmode[").a(shade).aln("]{functional}");
            else if (i>0) tex.a("\\shadingmode{").a(_choiceShading).aln('}');
            if (_choiceSeqLogo.i()!=0 || _choiceSubfamilyLocation.i()!=0) {
                if (isSelctd(cbHideSeqs)) tex.aln("\\hideseqs");
                if (isSelctd(cbDoFreqCorrect)) tex.aln("\\dofrequencycorrection");
            }
            if (_choiceSeqLogo.i()!=0) tex.a("\\showsequencelogo{").a(_choiceSeqLogo).aln("} \\showlogoscale{leftright}");
            for(int loc=4;--loc>=0; ) {
                _resGraph[loc].makeTex(tex, colFrom, colTo);
                _consGraph[loc].makeTex(tex, colFrom, colTo);
                _secGraph[loc].makeTex(tex, colFrom, colTo);
            }
            DialogPredictTexshade.getTexts(tex,pp);
        }
        if (_choiceRuler.i()>0) {
            final int idx=idxOf(SPUtils.sp(_pChoiceRuler),pp);
            if (idx>=0) tex.a("\\showruler{").a(_choiceRuler).a("}{").a(idx+1).a("}\n\\rulersteps{").a(_tfRulerstep).aln('}');
        }
        subfamily_makeTex(tex);
        tex.a('\n').join(customize(2).getSettings()).aln("\n\\end{document}");
        wrte(file(TEX_FILE),tex);
        ResSelUtils.useResNum(false);
    }
    /* <<< Src and MSF <<< */
    /* ---------------------------------------- */
    /* >>> Error >>> */
    private static Object pnlNeedMiktex() {
        return
            pnl(VBHB,
                pnl(HBL,"Either ", Microsoft.newCygwinButton(Microsoft.CYGWIN_LATEX)),
                " ",
                pnl(HBL,"Or install Basic Miktex","http://www.miktex.org", " After Miktex installation logout and login again"),
                " "
                );
    }
    private static void errorWindows() {
        error("notLaunchLatex",
              pnl(VBHB,
                  "Cannot launch LaTeX. There are two possible reasons:<ol>"+
                  "<li>LaTex is not yet installed.</li>"+
                  "<li>The program path variable is not correctly set.</li></ol><br><br>"+
                  "Solution: ",
                  pnlNeedMiktex()
                  ));
    }
    /* <<< Error <<< */
    /* ---------------------------------------- */
    /* >>> Exec >>> */
    public void compute(){
        makeMsfAndSource(_pp,0,MAX_INT);
        thrdCR(this,"RUN").run();
        viewFile(_fPdf,0);
    }
    /* <<< Exec <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */

    public Object run(String id,Object arg)  {
        if (id=="ED") {
            final boolean finished=sze(_fPdf)>0;
            setEnabld(finished, butPdf);
            final ChExec ex=_myExec;
            setEnabld(sze(file(TEX_FILE))>0 && sze(file(MSF_FILE))>0 && (ex==null ||!ex.isRunning()), butRunTex);
            setEnabld(sze(file(DVI_FILE))>0, butDvi);
            setEnabld(sze(file(DVI_FILE))>0, butDviPs);
            setEnabld(sze(file(PS_FILE))>0, butPS);
            setEnabld(sze(file(PS_FILE))>0, butPoster);
            setEnabld(sze(file(PS_POSTER_FILE))>0, butPosterViewPS);
            revalAndRepaintC(butPdf);
            revalAndRepaintC(butPS);
            revalAndRepaintC(butDvi);

        }
        if (id=="PROBE") {
            final ChExec ex=new ChExec(ChExec.IGNORE_ERROR).setCommandsAndArgument(custSettings(Customize.pdflatex),"--version");
            ex.run();
            if (ex.exitValue()!=0) setEnabld(true,_panInst);
        }
        if (id=="RUN") {
            final int nPass=arg!=null ? 1 : 2;
            for (String s : new String[]{PDF_FILE,AUX_FILE,LOG_FILE,PS_FILE,DVI_FILE}) delFile(file(s));
            for(int pass=nPass; --pass>=0;) {
                final ChExec ex=_myExec=new ChExec(ChExec.STDOUT|ChExec.STDERR)
                    .showStdoutAndStderr(_frameExec).dir(file(DIR));
                if (pass==0) addActLi(this,ex);
                ex.setCommandsAndArgument(custSettings(isSelctd(cbPS) ? Customize.latex : Customize.pdflatex),file(TEX_FILE).getName());
                ex.run();
                if (!ex.couldNotLaunch()) setEnabld(false,_panInst);
            }
            copyPdf();
            if (0==sze(file(isSelctd(cbPS) ? DVI_FILE : PDF_FILE))) inEdtLaterCR(this,"ERROR",null);
        }
        if (id==ResSelUtils.RUN_TEX_ONE_PROTEIN) {
            _oneAnno=toStrg(arg);
            compute();
            _oneAnno=null;
        }
        if (id=="ERROR") {
            final BA sLog=readBytes(file(LOG_FILE));
            if (sLog==null) {
                if (isWin()) {errorWindows();}
                else {
                    error(null,
                          pnl(VBHB,
                              "<h2>Failed to run LaTeX</h2>",
                              ctrlPnl(_myExec),
                              "</pre>In case LaTeX is installed please check the binary path.<br>"+
                              "In case LaTeX is not yet installed install it now:",
                              monospc(_instDetail)
                              )
                          );
                }
            } else {
                if (strstr("capacity exceeded",sLog)<0) {
                    error("LaTeX no output",
                          pnl(VBHB,"<h2>No output written by LaTeX.</h2>\n\n",
                               isWin() ? Microsoft.newCygwinButton(Microsoft.CYGWIN_LATEX) : null)
                          );
                } else {
                    final JComponent
                        panBut=isWin() && sze(file(dirCygwin()+Insecure.TEXMF_DOT_CONF))>0 ?
                        new ChButton("INCREASE_MEM").t("Let Strap solve the problem").li(this) : null,
                        msg=pnl(VBHB,
                                  "LaTeX reports that the memory  capacity exceeded ",
                                  panBut,
                                  "<br>Please use less proteins or configure the  memory limits in the LaTeX configuration file "+
                                  "<pre> texmf.cnf </pre>"+
                                  "Keep a copy of the original file content.<br>"+
                                  "Increase (multiply by 10 or 100) all memory values that have two or more zero digits at the end. <br>"+
                                  "Then run as superuser "+
                                  "<pre class=\"terminal\">   fmtutil --byfmt=pdflatex </pre>"
                                  );
                    error("LaTeX ERROR",msg);
                }
            }
        }
        return null;
    }

    private void copyPdf() {
        ((ChButton) butPdf).doViewFile(_fPdf=file(STRAPOUT+"/align"+_fPdfCount++ +".pdf"));
        setEnabld(true, butPdf);
        cpy(file(PDF_FILE),_fPdf);
    }

    private String nameseq() {
        final Protein pp[]=_pp;
        if (pp==null) return "";
        final BA sb=new BA(999);
        for(int iP=0;iP<pp.length;iP++) {
            final CharSequence newName= !isSelctd(cbSubstituteNames) ? null : readBytes(DIR_ANNOTATIONS+"/"+pp[iP]+".texName");
            if (nxt(-SPC,newName)>=0) sb.a("\\nameseq{").a(iP+1).a("}{").a(newName).a("} % ").aln(pp[iP]);
        }
        return sb.toString();
    }
    /* <<< nameseq <<< */
    /* ---------------------------------------- */
    /* >>> Customize >>> */
    private static Customize customize(int i) {
        if (_cust[i]==null) {
            String n=null,t=null,
                h=i==0?"" : "The latex source is <br>built from <ul><li>a constant beginning part</li><li>a variable middle part</li><li>a constant end part</li><ul>";
            if (i==0) {
                n="latexBegin";
                final BA sb=new BA(999)
                    .aln("\\documentclass[pdftex]{letter}\n"+
                         "% NOTE: the above line is suitable for the command pdflatex but not for the command latex.\n"+
                         "% when 'latex' is run the  String '\\documentclass[pdftex,' is automatically replaced by '\\documentclass[' to get rid of the pdflatex-option which would cause an error");
                for(File sty : sty()) {
                    final String path=delSfx(".sty", toStrg(isWin() ? sty.getName() : sty)).replace('\\','/');
                    sb.a("\\usepackage{").a(path).aln('}');
                    if (isWin()) cpy(sty, file(file(DIR),sty.getName()));
                }
                t=sb.aln("\\usepackage{color}\n\\usepackage{hyperref}\n").toString();
                h+="\nThis is the beginning part.";
            }
            if (i==1) {
                n="texshadeBegin";
                t="\\pagestyle{empty}\n"+
                    "\\begin{texshade}{"+file(MSF_FILE).getName()+"}\n"+
                    "\\seqtype{P}\n"+
                    "\\gapchar{.}\n";
            }
            if (i==2) {
                n="texshadeEnd";
                t="\\end{texshade}\n";
                h+="\nThis is the end part.";
            }
            _cust[i]=new Customize(n,new String[]{t},h,Customize.LATEX);
        }
        return _cust[i];
    }
    public static String getCustomizableExamples() { return readBytes(rscAsStream(0L,_CCS,EXAMPLES_RSC)).toString(); }
    /* <<< Customize <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent  >>> */
    public void handleEvent(StrapEvent ev) { if (ev.getType()==StrapEvent.PROTEINS_KILLED) _pp=null; }
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        StrapAlign.errorMsg("");
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final boolean selected=isSelctd(q), ctrl=isCtrl(ev), shift=isShift(ev);

        if (q==_myExec && cmd==ChExec.ACTION_ERROR && isWin()) errorWindows();
        if (q==butPoster) {
            final Object size=_choiceSize.getSelectedItem();
            Object posterSize=_choicePosterSize.getSelectedItem();
            if (posterSize=="same") posterSize=size;
            final ChExec ex=new ChExec(ChExec.LOG).dir(file(DIR))
                .setCommandsAndArgument(custSettings(Customize.psPoster)," -i"+size+" -p"+posterSize+"  -o "+PS_POSTER_FILE+" "+file(PS_FILE).getName());
            if (ctrl) ex.setCustomize(Customize.customize(Customize.psPoster));
            startThrd(ex);
        }
        if (q==cbCustomSize) {
            _choiceSize.setEnabled(!selected);
            _tfCustomHeight.setEnabled(selected);
            _tfCustomWidth.setEnabled(selected);
        }
        if (q==cbPS) {
            _panPoster.removeAll();
            if (selected) {
                _panPoster.add(pnl(_choicePosterSize,butPoster, butPosterViewPS));
            }
            _panPoster.setBorder(selected ? new TitledBorder("Scale and tile alignment output to print on multiple pages:"):null);
            final JComponent p=_panViewers;
            p.remove(butDvi); p.remove(butDviPs); p.remove(butPS); p.remove(butPdf);
            if (selected) { p.add(butDvi,1); p.add(butDviPs,2); p.add(butPS,3); } else p.add(butPdf,1);
            _panViewers.revalidate();
            delFile(file(TEX_FILE));
            setTxt(selected ? "Make PS":"Make PDF", butRunTex);
        }
        if (cmd=="SRC" || cmd=="MK") {
            final Protein pp[]=StrapView.ppInRectangleForAlignment(1);
            if (sze(pp)==0) { StrapAlign.errorMsg("No proteins"); return;}
            final Rectangle r=StrapView.rectangle(null);
            makeMsfAndSource(pp, x(r), r!=null ? x2(r) : MAX_INT);
            _taSourceFiles.setText("Generated Multiple Sequence file: \n "+ fPathUnix(file(MSF_FILE))+"\n\n Genereated LaTeX-file:\n "+fPathUnix(file(TEX_FILE))+"\n");
            _taSourceFiles.setEditable(false);
            _taSourceFiles.tools().underlineRefs(0);
            if (cmd=="SRC") {
                if (_wSrc==null)  {
                    final Object
                        pC=pnl(VBHB,"#TBem Customize Source",
                               pnl(HB, Customize.newButton(customize(0)).t("preamble")," ", Customize.newButton(customize(1)).t("\\begin{texshade}texshade")," ", Customize.newButton(customize(2)).t("end")),
                                 " ", _taBefore,
                                 " ", _taIn
                                 ),
                        pnl=pnl(VBHB,
                                 pC,
                                 _taSourceFiles
                                 );
                    _wSrc=new ChFrame("Texshade").ad(pnl).size(444,444).shw(CLOSE_CtrlW);
                }
                _taBefore.setBorder(new TitledBorder("LaTeX code between \\begin{document} and \\begin{texshade}"));
                _taIn.setBorder(new TitledBorder("LaTeX code between  \\begin{texshade} and \\end{texshade}"));
                _taSourceFiles.setBorder(new TitledBorder("Generated files"));
                _wSrc.size(500,500).shw(0);
            }
        }
        if ((q==butRunTex || cmd=="MK") && sze(file(TEX_FILE))>0 && sze(file(MSF_FILE))>0) {
            final ChExec ex=_myExec;
            if ( (ctrl||isSelctd(_togDetails)) && ex!=null) ex.setCustomize(Customize.customize(isSelctd(cbPS) ? Customize.latex : Customize.pdflatex));
            _fPdf=null;
            startThrd(thrdCR(this,"RUN", shift?"":null));
        }
        if (q==cbSubstituteNames && selected)  showTexshadeNameseq();
        if (cmd=="INCREASE_MEM") {
            final File fTexmf=file(dirCygwin()+Insecure.TEXMF_DOT_CONF),fOri=file(fTexmf+".original");
            if (sze(fOri)==0) cpy(fTexmf,fOri);
            final String ss[]=readLines(fTexmf);
            if (ss==null) return;
            for(int i=ss.length; --i>=0;) {
                final String s=ss[i].trim();
                if (chrAt(0,s)=='%') continue;
                if (s.endsWith("00")) { ss[i]=s+"00"; continue;}
                int idx00=s.indexOf("00 ");
                if (idx00<0) idx00=s.indexOf("00\t");
                if (idx00<0) continue;
                ss[i]=s.substring(0,idx00+2)+"00"+s.substring(idx00+2);
            }
            wrte(fTexmf,new BA(0).join(ss).aln("\n\n Modified by Strap"));
            startThrd(new ChExec(ChExec.LOG).setCommandLine("CYGWINSH fmtutil --byfmt=pdflatex\nfmtutil --byfmt=latex"));
            final Object msg=pnl(VBPNL,pnl("I modified the file ",fOri," ==> ", fTexmf),"Please wait for the fmtutil to finish and then try again.");
            ChMsg.msgDialog(0L, pnl(VBPNL,"fmtutil",msg));
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> subfamilyLogo >>> */
    private BA subfamily_makeTex(BA sb) {
        if (_choiceSubfamilyLocation.i()==0) return sb;
        Protein pp[]=_subfamilyProtList.selectedProteins();
        if (sze(pp)==0) pp=proteins();
        String sep="";
        sb.a("\n\\setsubfamily{");
        for(Protein p : pp) {
            final int idx=idxOf(p,proteins());
            if (idx<0) continue;
            sb.a(sep).a(idx+1);
            sep=",";
        }
        sb.aln('}').a("\\showsubfamilylogo{")
            .a(_choiceSubfamilyLocation).aln('}')
            .aln("\\showlogoscale{leftright}")
            .a("\\namesubfamilylogo[others]{")
            .a(_tfSubfamilyName).aln('}');
        return sb;
    }

    /* --- NameSeq --- */

    private void showTexshadeNameseq() {
        Protein pp[]=StrapAlign.selectedProteins();
        if (pp.length==0) pp=StrapAlign.visibleProteins();
        if (pp.length==0) return;
        if (_panProtNam==null) _panProtNam=pnl(VBHB);
        else _panProtNam.removeAll();

        final String info="<h2>Change displayed protein names</h2>"+
            "The typed name must follow the syntax of LaTeX.<br>"+
            "E.g. an unquoted underscore '_' would lead to a LaTeX-error.<br>";
        _panProtNam.add(pnl(HBL,info));

        for(Protein p : pp) {
            final String key="TS$$PPN";
            Container pan=gcp(key,p,Container.class);
            if (pan==null) {
                final ChTextField tf=new ChTextField();
                tf.tools().saveInFile(file(DIR_ANNOTATIONS+"/"+p+".texName"));
                pan=pnl(HB,p.getName()," ", tf);
                pan.setPreferredSize(dim(80*EM,ICON_HEIGHT+2));
                pcp(key,pan,p);
            }
            _panProtNam.add(pan);
        }
        ChFrame.frame("Substitute names",_panProtNam,ChFrame.SCROLLPANE).shw();
    }
    /* <<< subfamilyLogo <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */
    private AbstractButton cb(String t) { return cb(false, t, null);}
    private AbstractButton cb(String t,   String saveInFile) { return cb(false, t, saveInFile);}
    private AbstractButton cbOn(String t, String saveInFile) { return cb(true,  t, saveInFile);}
    private AbstractButton cb(boolean set, String t, String saveInFile) {
        final ChButton tog=toggl(t).s(set);
        if (saveInFile!=null) tog.save(Texshade.class,saveInFile);
        final AbstractButton cb=tog.cb();
        cb.addActionListener(this);
        return cb;
    }

    private static File[] sty() {
        File ff[]={
            isSystProprty(IS_USE_INSTALLED_SOFTWARE) ? aFileNotZ("/usr/share/texmf-texlive/tex/latex/texshade/texshade.sty","/usr/share/strap-base/texshade.sty") : null,
            aFileNotZ("/usr/share/texmf-texlive/tex/latex/vmargin/vmargin.sty","/usr/share/strap-base/vmargin.sty")
        };
        BA sb=null;

        for(int i=2; --i>=0;) {
            final String sty=i==0?"texshade120" : "vmargin";

            if (sze(ff[i])==0) ff[i]=file(dirDataFiles()+"/"+sty+".sty");
            if (sze(ff[i])==0) (sb==null?sb=new BA(99):sb.a(' ')).a(sty).a(".sty.gz");

        }
        if (sb!=null) InteractiveDownload.downloadFilesAndUnzip(Web.urls(URL_STRAP_DATAFILES,toStrg(sb)),dirDataFiles());

        return ff;
    }

}
