package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**HELP
   BL2SEQ produces all local alignments of two given sequences  (UBIC:bl2seq). The
   high scoring alignments are displayed in a two-dimensional diagram similar to a WIKI:Dot_plot_(bioinformatics).
   @author Christoph Gille
*/
public class Dialog_bl2seq extends AbstractDialogJTabbedPane implements Runnable {

    private final ProteinCombo _comboX,_comboY;
    public Dialog_bl2seq() {

        final Object pMain=pnl(VBPNL,
                           pnl("Horizontal", _comboX=new ProteinCombo(0)),
                           pnl("Vertical",  (_comboY=new ProteinCombo(0)).s(1)),
                               " ",
                               isMac() ? "Warning:  bl2seq may crash on Mac " : null,
                               new ChButton().t(ChButton.GO).r(this),
                               " "
                               );

        adMainTab(pnl(CNSEW,pMain,dialogHead(this), pnl(REMAINING_VSPC1)), this, getClass());
    }

    public void run() {
        final Protein pX=_comboX.getProtein(),pY=_comboY.getProtein();
        if (pX==null || pY==null) return;
        final B2s bl2seq=new B2s(pX,pY);
        adTab(DISPOSE_CtrlW, ProteinUtils.tabText2(pX,pY), bl2seq, this);
        startThrd(bl2seq);
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Class B2s  >>> */
    private static class B2s extends DialogDotPlot implements Runnable {
        public B2s(Protein pX, Protein pY) {
            super(BL2SEQ,pX,pY);
            pcp(KEY_IMAGE_FILE_NAME,pX+"+"+pY+"-bl2seq.png",this);
        }
        public synchronized void run() {
            final byte[] sequenceX=getSeq('X'), sequenceY=getSeq('Y');
            final BA log=log().clr();
            log.a("x: ").aln(sequenceX).a("\ny: ").aln(sequenceY).a('\n');
            if (sequenceX==null || sequenceY==null) return;
            final boolean ntX=nxt(-ACTGUN,sequenceX)<0;
            final boolean ntY=nxt(-ACTGUN,sequenceY)<0;
            final String prg=ntX && ntY ? "blastn" : !ntX && ntY ? "tblastn" :  ntX && !ntY ? "blastx" : "blastp";
            log.a(ntX?"x Nucl  ":"x Amino ").aln(sequenceX).a("\ny: ").a(ntY?"y Nucl  ":"x Amino ").aln(sequenceY).a("Programm: ").aln(prg);
            final int[][] vHits=new int[sequenceY.length][];
            final Bl2seq bl2seq=new Bl2seq(Bl2seq.ALIGNMENT, sequenceX,sequenceY, prg, (BA)null);
            bl2seq.compute();
            final BA result=bl2seq.getResultText();
            if (result==null) log.aln("Local bl2seq failed\n");
            else if (strstr("No hits found",result)>=0) setBl2seq(null,"No hits found");
            else {
                final int ends[]=result.eol();

                final byte T[]=result.bytes();
                int fromX=-1, fromY=-1,toX=-1,toY=-1, seqX=-1, seqY=-1;
                double score=0;
                for(int iL=0; iL<ends.length; iL++) {
                    final int b=iL==0?0: ends[iL-1]+1;
                    final int e=ends[iL];
                    if (e-b<12) continue;
                    final byte c0=T[b], c1=T[b+1], c2=T[b+2], c3=T[b+3], c4=T[b+4], c5=T[b+5];
                    if (c1=='S' && c2=='c' && c3=='o'  && c4=='r' && c5=='e') score=atof(T,b+8,e);
                    final boolean
                        isX=c0=='Q' && c1=='u' && c2=='e' && c3=='r' && c4=='y',
                        isY=c0=='S' && c1=='b' && c2=='j' && c3=='c' && c4=='t';
                    if (isX || isY) {
                        int to=-1;
                        for(int i=b+12; i<e; i++) {
                            if (to==-1  && is(DIGT,T,i)) to=atoi(T,i,e);
                        }
                        final int from=atoi(T,b+6,e)-1;
                        final int seq=nxtE(LETTR, T, b+6, e);
                        if (isX) { fromX=from; toX=to; seqX=seq;}
                        if (isY) { fromY=from; toY=to; seqY=seq;}
                        if (isY && fromX>=0 && toX>=0 && fromY>=0 && toY>=0 ) {
                            super.addPoints(T,seqX, fromX>toX, fromX,
                                            T,seqY, fromY>toY, fromY,  score,vHits);
                        }
                    }
                }
            }
            setBl2seq(vHits, bl2seq);
            log.send();
        }

    }

}
