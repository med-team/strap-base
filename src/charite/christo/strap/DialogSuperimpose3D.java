package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import java.awt.*;
import java.awt.event.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

   The mobile protein and the reference protein[s] are selected by the
   user.

   Pressing <i>LABEL:ChButton#BUTTON_GO</i>  finds an optimal  rotation/translation that minimizes
   the square root mean distance (RMSD) between C-alpha  atoms of both proteins.
   The result is displayed in a preview panel.

   Only after pressing <i>BUTTON:DialogSuperimpose3D#BUT_LAB_Apply</i>
   this transformation is applied to the mobile protein whereas the
   target protein remains unchanged.

   <br><br><b>For Writing PDB-files in new coordinates:</b> See
   <i>DIALOG:DialogExportProteins</i>.

   @author Christoph Gille
*/
public class DialogSuperimpose3D  extends AbstractDialogJTabbedPane implements ActionListener,ChRunnable {
    public final static String BUT_LAB_Apply="Approve";
    private final ChTextField[] _tfSelection=new ChTextField['R'+1];
    private final ProteinCombo _choicePR=new ProteinCombo(ProteinList.CALPHA);
    private final ProteinList _choicePM=new ProteinList(ProteinList.CALPHA).selectAll(0L);
    private final ChCombo _choiceClass;
    private final Object _togOptions;
    final java.util.List vRESULT=new java.util.Vector();
    public DialogSuperimpose3D() {
        _choicePM.li(this);
        {
            final Protein pp[]=StrapAlign.proteins();
            int iP=0;
            while(iP<pp.length) if (pp[iP++].getResidueCalpha()!=null) { _choicePR.s(pp[iP-1]); break;}
            while(iP<pp.length) if (pp[iP++].getResidueCalpha()!=null) { _choicePM.setSelI(iP-1); break;}
        }

        final Object[] panSel=new Object['R'+1];
        for(char i='C'; i<'R'+1; i++) {
            final String
                ttE=".<br>If kept empty all c-Alpha-atoms are taken.<br>Example: \"10-100,120-199\" "+
                "<br><br>Residue selections can be copied via Drag-and-Drop.",
                ttSel="Insert alignment cursor position";
            final ChTextField tf=i=='C' || i=='M' || i=='R' ? new ChTextField() : null;
            if (tf==null) continue;
            _tfSelection[i]=tf;
            tf.tools().enableUndo(false).saveInFile("DialogSuperimpose3D_tf"+i);
            StrapAlign.newDropTarget(tf,false);
            panSel[i]=pnl(CNSEW,tf,null,null,new ChButton(ChButton.ICON_SIZE,"C"+i).li(this).i(IC_TEXT_CURSOR).t(null).tt(ttSel));
            if (i=='M' || i=='R') {
                pcp(KEY_IF_EMPTY,"Residue range like \"10-20\"",tf.tt("Selected amino acids"+ttE));
                pcp(ResidueSelection.class, wref(i=='M' ? _choicePM : _choicePR), tf);
            } else {
                pcp(ResidueSelection.KOPT_DND_COLUMNS, "",tf.tt("Selected alignment positions"+ttE));
                pcp(KEY_IF_EMPTY,"Alignment columns like \"10-100\"",tf);
            }
        }

        _choiceClass=SPUtils.classChoice(Superimpose3D.class).save(DialogSuperimpose3D.class,"Superimpose3D");
        final Object
            bGo=new ChButton("GO").t(ChButton.GO).li(this)
                .tt("<ul><li>Ctrl+Shift-key: worse protein as reference</li><li>Ctrl-key: optimal protein as reference</li><li>Shift-key: removes results with a  score below a threshhold</li></ul>"),

            panProteinM=pnl(CNSEW,_choicePM.scrollPane(),"<b>Mobile protein[s]</b>", pnl(VB,panSel['M'],panSel['C']),null,"#ETB"),
            pSettings=pnl(VB,_choiceClass.panel(), buttn(TOG_CACHE).cb(), buttn(TOG_ALI_NOT_COMMUT).cb()),
            pWestNorth=
            pnl(VBPNL,
                pnl(CNSEW,_choicePR,"<b>Reference protein</b>", panSel['R'],null,"#ETB"),
                pnl(HBL, _togOptions=toggl().doCollapse(new Object[]{pSettings, panSel, _tfSelection}), "Options" , "#", bGo,"#"),
                pSettings
                ),
            pWest=pnl(CNSEW,null,pWestNorth,null,null,KOPT_TRACKS_VIEWPORT_WIDTH),
            pCenter=pnl(new GridLayout(1,2), scrllpn(pWest),panProteinM),
            pMain=pnl(CNSEW,pCenter, dialogHead(this), pnl(REMAINING_VSPC1));
        adMainTab(pMain,this, null);
        if (!isMac()) setTabPlacement(LEFT);
    }

    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Thread >>> */
    public Object run(String id, Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg : null;
        if (id=="AT") {
            final DialogSuperimpose3DResult s=(DialogSuperimpose3DResult)argv[0];
            final String label=(String)argv[1];
            TabItemTipIcon.set(label,null,null,null,s.getPanel());
            final int idx=getSelectedIndex(), count=getTabCount();
            adTab(0, null, s.getPanel(), this);
            if (idx!=0 && idx!=count-1) setSelectedIndex(idx);
        }
        if (id=="COMPUTING") {
            try {
                pcp("COMPUTING","",this);
                final Protein pR=(Protein)get(0,arg),  ppM[]=(Protein[])get(1,arg);
                Protein lastMobile=null;
                if (ppM!=null) {
                    for(Protein p : ppM) {
                        if (p!=null && p.getResidueCalpha()!=null) lastMobile=p;
                    }
                }
                if (lastMobile==null) { StrapAlign.errorMsg("Select at least one mobile protein with C-alpha atoms");return null;}

                final String subset[]=new String['R'+1];
                for(int i=subset.length; --i>=0;) {
                    final Object jc=_tfSelection[i];
                    if (isEnabld(jc) && isSelctd(_togOptions)) {
                        final String s=jc.toString();
                        subset[i]=nxt(-SPC,s)>=0 ? s : null;
                    }
                }
                final Protein protR=subset['R']==null ? pR : SPUtils.newProteinSubset(pR,subset['R']);
                for(Protein pM:ppM) {
                    if(isDisposed()) break;
                    if (protR==null || pM==null || pM.getResidueCalpha()==null || ppM.length>1 && pM==pR) continue;
                    sleep(11);

                    final Superimpose3D s3d=mkInstance(_choiceClass,Superimpose3D.class,true);
                    if (s3d==null) StrapAlign.errorMsg("Could not instantiate superposition class ");
                    else {
                        final Protein protM;
                        if (subset['C']!=null) {
                            final int[] minIdx={0};
                            final boolean bb[]=pM.residueSubsetAsBool(subset['C'],  minIdx);
                            protM=SPUtils.newProteinColumnFromTo(pM, minIdx[0]+fstTrue(bb)-1,minIdx[0]+lstTrue(bb));
                        } else protM=subset['M']==null ? pM : SPUtils.newProteinSubset(pM,subset['M']);
                        if (protM==null || protM.countResidues()<4) continue;
                        s3d.setProteins(protR,  protM);
                        final DialogSuperimpose3DResult s=new DialogSuperimpose3DResult(s3d, this);
                        inEDT(thrdCR(this,"AT", new Object[]{s, ProteinUtils.tabText2(pR,pM)}));
                        s.run();
                    }
                }
                sleep(999);
            } finally{
                pcp("COMPUTING",null,this);
                revalidate();
            }
        }
        return null;
    }
    /* <<< Thread <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        StrapAlign.errorMsg("");
        if (q==_choicePM) setEnabledIncludingChildren(_tfSelection['M'].getParent(),_choicePM.selectedProteins().length==1);
        final Protein pR=_choicePR.getProtein(), ppM[]=_choicePM.selectedOrAllProteins();
        final char c1=chrAt(1,cmd);
        if (sze(cmd)==2 && chrAt(0,cmd)=='C' && (c1=='R'||c1=='M'||c1=='C')) {
            final ChTextField tf=_tfSelection[c1];
            if (!isEnabld(tf)) return;
            final Protein p=StrapAlign.cursorProtein();
            final int iA=StrapAlign.indexOfAminoAcidAtCursorZ(p);
            if (iA<0 || p==null) StrapAlign.errorMsg("Inserting Cursor-Position<: first place the cursor in the alignment pane");
            else {
                if (c1=='M' && ppM.length!=1 || c1=='R' && pR==null) return;

                final Protein pDest=c1=='R' ? pR : c1=='M' ? ppM[0] : null;
                final int col=p.getResidueColumnZ(iA);
                if (col>=0) {
                    final int ins=pDest!=null?pDest.column2nextIndexZ(col):col;
                    ChTextComponents.insertAtCaret(0L, " "+(ins+1)+" ",tf);
                }
            }
        }
        if (cmd=="GO") {

            if (isCtrl(ev)) setRepresentingProtein(isShift(ev));
            else if (isShift(ev)) removeAllBelowThreshold();
            else {
                if (pR==null) StrapAlign.errorMsg("Select a reference protein");
                else if(pR.getResidueCalpha()==null) StrapAlign.errorMsg("No c-alpha coordinates in the reference protein");
                else {

                    final Thread t=thrdCR(this,"COMPUTING", new Object[]{pR, ppM});
                    t.setPriority(Thread.MIN_PRIORITY);
                    startThrd(t);
                }
            }
        }
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Score >>> */
    private void removeAllBelowThreshold() {
        final float f=(float)atof(ChMsg.input("This removes all superposition resutls<br>with a score below a threshhold.<br>Enter threshhold",8,"3.1"));
        removeAllBelowThreshold(f);
    }

    private void removeAllBelowThreshold(double threshhold) {
        for(int i=getTabCount();--i>=0;) {
            final DialogSuperimpose3DResult s3d=deref(getComponentAt(i),DialogSuperimpose3DResult.class);
            final Superimpose3D.Result result=s3d==null?null:s3d.getResult();
            if (result!=null && result.getScore()<threshhold) s3d.dispose();
        }
    }
    /* <<< Score <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    @Override public void paintChildren(Graphics g){
        super.paintChildren(g);
        if (gcp("COMPUTING",this)==null || getSelectedIndex()>0) return;
        g.setFont(getFnt(ICON_HEIGHT,true,Font.BOLD));
        g.setColor(Color.PINK);
        g.drawString("Computing ...",1,getHeight()-3*ICON_HEIGHT);
        g.drawString("please wait",1,getHeight()-2*ICON_HEIGHT);

    }

    /* <<< Graphics  <<< */
    /* ---------------------------------------- */
    /* >>> Rep  >>> */
    private void  setRepresentingProtein(boolean isBest) {
        Protein pp[]=StrapAlign.selectedProteins();
        if (pp.length==0) pp=StrapAlign.proteins();
        final Protein pBestWorse[]=findRepresentingProtein3D(pp,_choiceClass);
        if (pBestWorse!=null) { _choicePR.s(pBestWorse[isBest ? 0 : 1]); _choicePR.revalidate();}
    }

    private static Protein[] findRepresentingProtein3D(Protein pp[], Object comboClass) {
        float highest=0,lowest=99999999;
        Protein best=null,worse=null;
        boolean rmsd=false;
        for(Protein pI:pp) {
            float scoreSum=0;
            for(Protein pJ:pp) {
                if (pI==pJ || pJ==null || pJ.getResidueCalpha()==null || pI==null || pI.getResidueCalpha()==null) continue;
                final Superimpose3D suim=mkInstance(comboClass,Superimpose3D.class,true);
                suim.setProteins(pJ,pI);
                suim.compute();
                final Superimpose3D.Result result=suim.getResult();
                if (result!=null) {
                    final HasScore hs=deref(suim, HasScore.class);
                    if (hs==null) rmsd|=true;
                    final float f=rmsd?result.getRmsd():hs.getScore();
                    scoreSum+=f*f;
                }
            }
            if (highest<scoreSum) { highest=scoreSum;  if (rmsd) worse=pI; else best=pI; }
            if (lowest>scoreSum)  { lowest=scoreSum;   if (rmsd) best=pI;  else worse=pI;}
        }
        if (Strap.listFile()!=null) {
            final String base=delDotSfx(Strap.listFile());
            if (best!=null)  wrte(file("info/"+base+".REP"),best+"\n");
            if (worse!=null) wrte(file("info/"+base+".EXTREME"),worse+"\n");
        }
        return new Protein[]{best, worse};
    }

}
// prosup www.came.sbg.ac.at
//PUBMED: 15609341 FAST: a novel protein structure alignment algorithm.
// dalilight FAST K2
//Aysam guerler@chemie.fu-berlin.de Prof Knapp

// http://bioinformatics.oxfordjournals.org/cgi/reprint/22/17/2166
//MATRAS: a program for protein 3D structure comparison
// qCOPS  TopMatch

// http://sourceforge.net/apps/mediawiki/lajolla/index.php?title=Main_Page http://lajolla.sf.net
