package charite.christo.strap;
import charite.christo.protein.*;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
import static charite.christo.strap.V3dUtils.*;

public class V3dListModel extends javax.swing.AbstractListModel {
    public final static int PROTEINS=1, SURFACE_OBJECTS=2, DELETED_OBJECTS=3,
        NUCL=4, HET=5,
        SEL=6, ANNO=7,  SFEATURE=8;
    public final static String

        TITLES[]={"", "Proteins: add, remove, select ...", "Surface objects", "Deleted objects",
                  "DNA/RNA ...", "Hetero compounds ...",
                  "List of all types of residue selections", "List of annotated residue selections", "List of sequence features"};
    private final int _type;
    public final static int[] TYPES_SEL={SEL,ANNO,SFEATURE};
    private final static V3dListModel MM[]=new V3dListModel[TITLES.length];
    private final static ChJList JL[][]={new ChJList[MM.length], new ChJList[MM.length]};
    static V3dListModel model(int type) {
        if (MM[type]==null) MM[type]=new V3dListModel(type);
        return MM[type];
    }
    private V3dListModel(int i) {_type=i;}

    public int getSize() { return sze(items()); }
    public Object items() {
        final int t=_type;
        final ProteinViewer pv=getPV(PV_FOCUSED);
        final Protein p=SPUtils.sp(pv);
        return
            p==null? null:
            t==DELETED_OBJECTS? list(DELETED_OBJECTS, pv, false) :
            t==PROTEINS ? Protein3dUtils.vvSharingView(pv) :
            t==SURFACE_OBJECTS ? pv.getProperty(ProteinViewer.GET_SURFACEOBJECTS) :
            t==NUCL ? p.getHeteroCompounds('N') :
            t==HET ?  p.getHeteroCompounds('H') :
            t==SEL ? p.allResidueSelections() :
            t==ANNO ? p.vSel(Protein.SELECTIONS_ANNO_NF) :
            t==SFEATURE ? p.vSel(Protein.SELECTIONS_FEATURES) :
            null;

    }

    @Override public Object getElementAt(int i) {
        final Object ret=get(i, items());
        return _type==PROTEINS ? SPUtils.sp((ProteinViewer)ret) : ret;
    }

    public static ChJList jList(int type, boolean create, int vers01) {
        if (JL[vers01][type]==null && create) JL[vers01][type]=newJList(type);
        return JL[vers01][type];
    }
    private static ChJList newJList(int type) {
        final boolean isDnD=type==PROTEINS||idxOf(type,TYPES_SEL)>=0;
        final long opts=ChJTable.DEFAULT_RENDERER|(isDnD?ChJTable.DRAG_ENABLED:0);
        final ChJList jl=new ChJList(V3dListModel.model(type),opts);

        pcp(KEY_IF_EMPTY,"No items for this 3D view",jl);
        pcp(StrapAlign.KOPT_DELETE_DISABLED, "No items for this 3D view",jl);
        if (isDnD) StrapAlign.newDropTarget(jl,false);
        jl.setFixedCellHeight(4+(isDnD?ICON_HEIGHT : EX));
        li().addTo("m", jl);
        adUniq(jl, vEnableDisable);
        setTip(V3dUtils.instance(),jl);
        StrapAlign.addAwtListeners(jl);
        setMinSze(10*EM, 2*EX, jl);
        if (type==PROTEINS) updateOn(CHANGED_PROTEIN_LABEL, jl);
        return jl;
    }

    public static List list(int type, ProteinViewer pv, boolean create) {
        final Map m=Protein3dUtils.sharedHashMapV3D(pv);
        List v=null;
        final String key= m==null?null : type==DELETED_OBJECTS ? "V3dlm$$ld" : null;
        if (key!=null) {
            v=(List)m.get(key);
            if (v==null && create) m.put(key,v=new ArrayList());
        }
        return v;
    }

}
