package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.JComponent;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

This dialog allows to create residue annotations from  lists of the  form
<pre class="data">
E815R  G4711A  C4444Q
</pre>
or
<pre class="data">
Glu815Arg  Gly4711Ala  Cys4444Gln
</pre>

These items may be mutations or SNPs or residue lists from crystallographic papers.

Each item should contain a number which is interpreted as a sequence position or a PDB-residue number.

<br>
<br>
<u>Related resources</u> http://www.mutdb.org http://gila.bioengr.uic.edu/snp/toposnp/
<br>
<i>SEE_CLASS:ResidueAnnotation</i>
<i>SEE_CLASS:AddAnnotation</i>
<i>SEE_DIALOG:DialogResidueAnnotationChanges</i>

   @author Christoph Gille
*/
public class DialogResidueAnnotationList extends JComponent implements ProcessEv {
    private final static String
        NT="Indices of nucleotide",
        AA="Indices of amino acids",
        RESNUM="Residue numbers as in the pdb file";
    private final ProteinCombo _comboP=new ProteinCombo(0);
    private final JComponent
        _comboIdx=new ChCombo(AA,NT,RESNUM),
        _tfOffset=new ChTextField("    0").cols(4,true,true),
        _tfGroup=new ChTextField("mutations").cols(30,false,true);
    private final ChTextArea _ta=new ChTextArea("\n\n\n");
    {pcp(KEY_IF_EMPTY,"Enter a list of mutations such as E33A Lys144Glu ...",_ta);}

    public DialogResidueAnnotationList() {
        final Object
            pSouth=pnl("#",
                       new ChButton("E").t("For newbies load example").li(evAdapt(this)),
                       " ",
                       new ChButton("GO").t(ChButton.GO).li(evAdapt(this)).tt("Creates a selection for each token in the text area"),
                       "#",
                       REMAINING_VSPC1
                       ),
            pNorth=pnl(VBHB,
                       dialogHead(this),
                       pnl(HB,"Protein:  ",_comboP,"  Residue numbering: ",_comboIdx),
                       pnl(HBL,"Index offset:  ",_tfOffset," (Negative values shift the residue selections to the left, positive values to the right.)"),
                       pnl(HBL,"Group (Short free text):  ",_tfGroup)
                       );
        pnl(this,CNSEW,scrllpn(SCRLLPN_INHERIT_SIZE,_ta),pNorth,pSouth);
        _ta.tools().enableUndo(true);
    }
    public void processEv(java.awt.AWTEvent ev) {
        final String cmd=actionCommand(ev);
        if (cmd!=null) {
            String err="";
            if (cmd=="E") _ta.t(new BA(999).aln("E33Q L44I M55C").a(_ta));
            if (cmd=="GO") {
                final Protein p=_comboP.getProtein();
                final String iType=toStrg(_comboIdx);
                if (p==null) err="No protein selected";
                else if (iType==RESNUM && p.getResidueNumber()==null) err="No PDB residue numbers found.";
                else if (iType==NT && p.getNucleotides()==null) err="No nucleotides found";
                else {
                    final String[] tokens=splitTokns(toStrg(_ta),chrClas(" ,"));
                    if (sze(tokens)==0) { StrapAlign.errorMsg("Enter some tokens like E33Q or L44I");  return;  }

                    final ResidueAnnotation[] vNewRA=new ResidueAnnotation[tokens.length];
                    int count=0;
                    for(String t : tokens) {
                        final ResidueAnnotation s=newA(t,p,iType);
                        if (s!=null) vNewRA[count++]=s;
                    }
                    final ResidueAnnotation aa[]=ResSelUtils.mayBeAddResidueAnnotations(vNewRA, this);
                    final BA sb=check(aa);
                    final Object pSouth=pnl(CNSEW,"Note: right click for context menu",null,null,sze(sb)>0?ChButton.doView(sb).t("Warnings"):null);
                    final String title="Created annotations";
                    StrapAlign.showInJList(0L,null, aa, title, pSouth).showInFrame(ChFrame.AT_CLICK, title);
                }
            }
            StrapAlign.errorMsg(err);

        }
    }
    private ResidueAnnotation newA(String txt,Protein p,String iType) {
        if (txt==null || p==null) return null;
        ResidueAnnotation s=null;
        final int dgt=nxt(DIGT,txt);
        if (dgt>=0) {
            final String idx=toStrg(atoi(txt,dgt)+atoi(_tfOffset));
            s=new ResidueAnnotation(p);
            s.addE(0,ResidueAnnotation.NAME,txt);
            s.addE(iType==NT?ResidueAnnotation.E_NT:0, ResidueAnnotation.POS, iType==RESNUM ? new BA(99).a(idx).a(':'):idx);
        }
        if (s==null) {
            final int open=txt.indexOf('['), slash=txt.indexOf('/'), close=txt.indexOf(']');
            if (open>0 && open+2==slash && slash<close) {
                final byte[] seq=new BA(999).a(txt,0,open).a(txt.charAt(open+1)).a(txt,close+1,MAX_INT).bytes();
                final int
                    iNT=strstr(STRSTR_IC,seq,p.getNucleotides()),
                    iTrans=iNT<0 ? strstr(STRSTR_IC,seq,DNA_Util.onlyTranslated(p.getNucleotidesCodingStrand(),p.isCoding())) : -1,
                    iAa=strstr(STRSTR_IC,seq,p.getResidueTypeExactLength());
                if (iNT>=0||iTrans>=0||iAa>=0) {
                    s=new ResidueAnnotation(p);
                    final int pos=iNT>=0 ? iNT : iTrans>=0 ? iTrans/3 : iAa;
                    s.addE(0,ResidueAnnotation.NAME, new BA(99).a(pos+1).a(txt,open+1,close));
                    s.addE(iType==NT?ResidueAnnotation.E_NT:0, ResidueAnnotation.POS, toStrg(pos+open+1));
                    s.addE(0,"Note",txt);
                }
            }
        }
        if (s!=null) {
            s.addE(0,ResidueAnnotation.GROUP, toStrg(_tfGroup));
            s.addE(0,ResidueAnnotation.TEXSHADE,"\\feature{bottom}{$TEX_PROTEIN}{$TEX_RESIDUES}{fill:$\\uparrow$}{$SELECTION_NAME}");
            s.addE(0,ResidueAnnotation.VIEW3D, ProteinViewer.COMMANDspheres);
        }
        return s;
    }
    // --------------------------------------
    private BA check(ResidueAnnotation[] vAdded) {
        final BA sb=new BA(99);
        int count=0;
        final String msg="There are possibly discrepancies between the residues in the sequence and the designations of the mutations.\nPlease check:\n";
        for(ResidueAnnotation s : vAdded) {
            final String name=s.getName();
            final Protein p=s.getProtein();
            final int iDigit=nxt(DIGT,name);
            if (NT!=toStrg(_comboIdx)) {
                final int iAa=fstTrue(s.getSelectedAminoacids())+s.getSelectedAminoacidsOffset()-Protein.firstResIdx(p);
                int aa=p.getResidueType(iAa);
                if (iDigit==1 && aa>0) {
                    aa&=~32;
                    final int c0=name.charAt(0)&~32;
                    if (aa!=c0) sb.a(count++==0 ? msg : null).a(name).a(": residue at ").a(1+iAa).a(" is ").a((char)aa).a('\n');
                }
                if (iDigit==3) {
                    final int c=Protein.toOneLetterCode(name.charAt(0),name.charAt(1),name.charAt(2));
                    if (c!=0 && c!='X' && c!=aa) {
                        if (count++==0) sb.a(msg);
                        sb.a(name).a(": residue at ").a(iAa).a(" is ").a((char)aa).a(' ').aln(Protein.toThreeLetterCode(aa));
                    }
                }
            } else {
                final int iNt=fstTrue(s.getSelectedNucleotides())+s.getSelectedNucleotidesOffset();
                int nt=get(iNt, p.getNucleotides());
                if (nt>0 && iDigit==1) {
                    nt&=~32;
                    final int c0=name.charAt(0)&~32;
                    if (c0!=nt) {
                        if (count++==0) sb.a(msg);
                        sb.a(name).a(": nucleotide at ").a(1+iNt).a(" is ").a((char)nt).a('\n');
                    }
                }
            }
        }
        return sb;
    }

}
