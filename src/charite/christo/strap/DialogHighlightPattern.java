package charite.christo.strap;
import charite.christo.*;
import charite.christo.protein.*;
import javax.swing.*;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChButton.PFX_VERTICAL;

/**HELP

   This dialog is used to underline all occurrences of sequence
   patterns.

   <br>
   <br>

   The search patterns are entered into the table.
   Several search patterns can be entered in one single table row and must be separated
   by white space.

   All matches are underlined. The highlights have a context menu.

   <br>
   <br>
   The search string may be a simple string of letters or a complex WIKI:Regular_expression.
   Syntax:

   <ul>
   <li>"FSP" is a simple search string identifying all
   occurrences of "FSP" in the amino acid sequence.</li>

   <li>A dot matches any letter. For example "F.P" would match F followed by any letter followed by P.</li>

   <li>Vertical bars separate alternatives. "IFSP|TFSP" is equivalent to "[IT]FSP" or "(IF|TF)SP" or "IFSP&nbsp;&nbsp;&nbsp;TFSP". </li>

   <li>"(.....)\1" selects all penta repeats. A backslash-number is a
   reference to a parenthesized group.</li>

   <li>Square brackets enclose groups of letters at one single
   sequence position. "[FYW][FYW]" selects adjacent aromatic amino
   acids. </li>

   <li>"[ke]{3,99}"highlights all occurrences of 3 to 99 continuous
   positions containing either Lys or Glu.</li>

   </ul>

   <i>SEE_CLASS:ResidueSelection</i>
   @author Christoph Gille
*/
public class DialogHighlightPattern extends AbstractDialogJPanel implements Disposable, StrapListener, javax.swing.table.TableModel, java.awt.event.ActionListener, Runnable {
    private String _error="";
    private final ChJTable _jt;
    private final UniqueList<HighlightPattern> _v=new UniqueList(HighlightPattern.class);
    private final Object _panNavi=ChButton.navigationPreviousNext(this, "Go to * match");

    private final ChButton

        _lab=labl(),
        _cbFwd=toggl("Fwd").li(this).s(true),
        _cbRev=toggl("Rev-Compl").li(this).s(true),
        _cbDetails,
        _cbSelectedPP=toggl("Search only in selected proteins").li(this);
    private final File _file=file(STRAPTMP+"/strap_sequence_patterns.dat");
    private final static String
        _headers[]=
        (PFX_VERTICAL+"Alignment pane/"+
         PFX_VERTICAL+"3D-Backbone/"+
         PFX_VERTICAL+"Scroll-bar/"+
         PFX_VERTICAL+"Nucleotide sequence/"+
         "Type a search pattern.  Separate patterns by white space./"+
         "/"+
         "Navigation").split("/");
    public DialogHighlightPattern() {
        _v.add(new HighlightPattern("",this));
        final String[] txt=readLines(_file);
        addShutdownHook1(new Thread(this));
        boolean nt=false;
        for(int i=0; i<sze(txt); i++) {
            final HighlightPattern hp=new HighlightPattern(txt[i],this);
            _v.add(hp);
            nt=nt||hp.isNT();
        }

        final String tips[]=
            ("Visible in alignment panel/"+
             "Visible in 3D-Backbone/"+
             "Visible in horizontal scrollbar/"+
             "Act on nucleotide sequence/"+
             "Small sequence of amino acids in (One-letter-code) to be searched for. A dot matches any amino acid./"+
             "Change color of hits/"+
             "Jump to first, previous, next and last hit").split("/");
        _jt=new ChJTable(this,ChJTable.EXX_ROW_HEIGHT|ChJTable.DEFAULT_RENDERER|ChJTable.NO_REORDER).headerTip(tips);
        _jt.setRowSelectionAllowed(false);
        _jt.getChRenderer().textField().li(this);
        _jt.getTableHeader().setDefaultRenderer(new ChRenderer());
        rtt(_jt);
        _jt.setCellSelectionEnabled(false);
        final JComponent
            sp=scrllpn(SCRLLPN_INHERIT_SIZE,_jt),
            pOptions=pnl("Search in nucleotides if check-box in 4th column is activated: ",_cbFwd.cb(),_cbRev.cb(),  "  ",_cbSelectedPP.cb(), "  ", AbstractVisibleIn123.newComboStyle().li(this)),
            pSouth=pnl(VB, REMAINING_VSPC2, pnl(HBL, "Details ", _cbDetails=toggl().s(nt).li(this).doCollapse(pOptions)),  pOptions, monospc(_lab),"##");
            pnl(this, CNSEW, sp,dialogHead(this), pSouth);
        addToProtein();
        pcp(KEY_CLOSE_OPT, intObjct(CLOSE_DISPOSE),this);
        noBrdr(sp);
        setMinSze(4*EX,4*EX,sp);
        setW();
    }
    @Override public final void dispose() {
        rmAll();
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
        clearAnts();
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final Object q=ev.getSource();
        final String cmd=ev.getActionCommand();
        final HighlightPattern hp=_v.get(_jt.getChRenderer().editedRow());
        boolean changed=false;
        if (q instanceof JComboBox) {
            for(HighlightPattern h : _v.asArray()) {
                h.setStyle(((JComboBox)q).getSelectedIndex());
            }
            changed=true;
        }
        if (q==_cbDetails) { setW(); revalidate(); }

        if (hp!=null) {
            if (q==_jt.getChRenderer().textField() && cmd==ACTION_TEXT_CHANGED) {
                final String s=q.toString();
                final boolean addRemove= (sze(s)==0)!=(sze(hp.getText())==0);
                hp.setText(s);
                if (addRemove) hp.addToProteins(getProteins());
                changed=true;
            }
            if (cmd=="<" || cmd==">" || cmd=="|<" || cmd==">|") {
                hp.nextAndPrevSelection.goNextMatch(cmd,hp.getResidueSelections(StrapAlign.proteins()));
            }
        }
        if (q==_cbFwd && _cbFwd.s()) _cbRev.s(false);
        if (q==_cbRev && _cbRev.s()) _cbFwd.s(false);
        if (q==_cbFwd || q==_cbRev) changed=true;
        if (q==_cbSelectedPP) {
            rmAll();
            addToProtein();
            clearAnts();
        }
        if (changed) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
    }
    public final void handleEvent(StrapEvent ev) {
        final int type=ev.getType();
        if (type==StrapEvent.PROTEINS_ADDED || type==StrapEvent.PROTEINS_KILLED) addToProtein();
        else if ( (type&StrapEvent.FLAG_RESIDUE_TYPES_CHANGED)!=0) StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,333);
        else if (type==StrapEvent.PROTEIN_SELECTED && _cbSelectedPP.s()) {
            rmAll();
            addToProtein();
            clearAnts();
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> TableModel >>> */
    public int getRowCount(){ return sze(_v);};
    public int getColumnCount(){ return 7; };
    public String getColumnName(int col) { return _headers[col]; }
    public Class getColumnClass(int col) { return Object.class; }
    public boolean isCellEditable(int row, int col){return true;}
    public Object getValueAt(int row, int col){
        final HighlightPattern hp=_v.get(row);
        if (hp==null) return "error";
        if (col==3) return boolObjct(hp.isNT());
        if (col==4) return hp.getText();
        if (col==5) return hp.butColor();
        if (col==6) return _panNavi;
        return boolObjct((hp.getVisibleWhere() & (col==0 ? VisibleIn123.SEQUENCE : col==1 ? VisibleIn123.STRUCTURE : VisibleIn123.SB))!=0);
    }
    public final void setValueAt(Object o, int row, int col){
        final HighlightPattern hp=_v.get(row);
        if (hp==null || o==null) return;
        final int visible0=hp.getVisibleWhere();
        int mask=0;
        boolean changed=false, addRemove=false;
        switch(col) {
        case 0:  mask=VisibleIn123.SEQUENCE; break;
        case 1:  mask=VisibleIn123.STRUCTURE; break;
        case 2:  mask=VisibleIn123.SB; break;
        case 3:  changed|=hp.setNT(o==Boolean.TRUE);
            setError(hp.isNT() && !ProteinList.isEnabled(getProteins(),ProteinList.NT)  ? "Error selecting \"nt\" : No nucleotide sequence present." : "");
            break;
        case 4:
            final String s=o.toString();
            addRemove|= (sze(s)==0)!=(sze(hp.getText())==0);
            changed|=hp.setText(s);
            newTableRow();
            _jt.repaint();
            break;
        default:
        }
        if (mask!=0) {
            final int visible= o==Boolean.TRUE ? (visible0|mask) :  (visible0&~mask);
            addRemove|= (visible0==0) != (visible!=0);
            if (visible!=visible0) {
                changed=true;
                hp.setVisibleWhere(visible);
            }
        }
        if (addRemove) {
            hp.addToProteins(getProteins());
            newTableRow();
        }
        if (changed) {
            StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,99);
            _jt.repaint();
        }
    }
    public final void addTableModelListener(javax.swing.event.TableModelListener li){}
    public final void removeTableModelListener(javax.swing.event.TableModelListener li){}
    private void setW() {
        final boolean details=_cbDetails.s();
        int headerH=EX;
        for(int col=getColumnCount();--col>=0;) {
            final int w=col<4 ? (details ? ICON_HEIGHT : 0) :
                col==5 ? ICON_HEIGHT :
                col==6 ? prefW(_panNavi) :
                -1;
            if (w>=0) for(int j=2; --j>=0;)  _jt.setColWidth(false, col, w);
            if (details) headerH=maxi(headerH,prefH(labl(_headers[col])));
        }
        _jt.getColumnModel().setColumnMargin(details?1:0);
        _jt.getTableHeader().setPreferredSize(dim(1,headerH+EX));

    }
    /* <<< TableModel <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    public void run() {
        final BA sb=new BA(999);
        for(HighlightPattern hp:_v.asArray()) hp.save(sb);
        wrte(_file,sb);

    }
    /* <<< Threading <<< */
    /* ---------------------------------------- */
    /* >>> Proteins >>> */
    private void addToProtein() {
        final Protein pp[]=getProteins();
        for(HighlightPattern hp:_v.asArray()) hp.addToProteins(pp);
        StrapEvent.dispatchLater(StrapEvent.RESIDUE_SELECTION_CHANGED,111);
    }
    private void newTableRow(){
        for(HighlightPattern p:_v.asArray()) {
            if (sze(p.getText())==0) return;
        }
        _v.add(new HighlightPattern("",this));
        _jt.revalidate();
    }
    private void rmAll() {
        for(Protein p: StrapAlign.proteins()) {
            p.removeResidueSelection(HighlightPattern.Selector.class);
        }
    }
    private Protein[] getProteins() {
        return _cbSelectedPP.s() ? StrapAlign.selectedProteins() : StrapAlign.visibleProteins();
    }
    /* <<< Proteins <<< */
    /* ---------------------------------------- */
    /* >>> Misc >>> */
    int orientations() {
        int r=0;
        if (_cbRev.s()) r|=2;
        if (_cbFwd.s()) r|=1;
        return r;
    }
    void setError(String msg) {
        if (msg!=_error) {
            _lab.t(msg).fg(0xFF0000);
            if ((sze(msg)==0) != (sze(_error)==0))  _lab.revalidate();
            _error=msg;
        }
    }
    private final void clearAnts() {
        final StrapView v=StrapAlign.alignmentPanel();
        if (v!=null) addMarchingAnt(ANTS_SEARCH, v.alignmentPane(), null,null, null );
    }
}
