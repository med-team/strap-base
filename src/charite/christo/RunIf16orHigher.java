package charite.christo;
import javax.swing.*;
import java.util.*;
import java.io.*;
import static charite.christo.ChUtils.*;
class RunIf16orHigher implements ChRunnable {

    public final static String
    RUN_setExecutable="R16$$sE",
        RUN_setRowSorter="R16$$srs",
        RUN_convertI2M="R16$$ci2m",
        RUN_convertI2V="R16$$ci2v";

    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[]) arg : null;
        if (id==RUN_setExecutable) {
            final File f=(File)arg;
            if (f!=null) f.setExecutable(true, false);
        }
        if (id==RUN_setRowSorter) {
            final JTable t=(JTable)(arg instanceof JTable ? arg : argv[0]);
            final javax.swing.table.TableRowSorter ts=new javax.swing.table.TableRowSorter(t.getModel());
            final Comparator[] cc=get(1,argv,Comparator[].class);
            for(int i=sze(cc); --i>=0;) {
                if (cc[i]!=null) ts.setComparator(i,cc[i]);
            }
            t.setRowSorter(ts);
        }
        final boolean cTm=id==RUN_convertI2M;
        if (cTm || id==RUN_convertI2V) {
            final int row[]=(int[])argv[1];
            final JTable t=(JTable)argv[0];
            row[0]=cTm ? t.convertRowIndexToModel(row[0]) : t.convertRowIndexToView(row[0]);
        }

        return null;
    }

}
