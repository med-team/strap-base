package charite.christo;
/**
   An Edge for a graph
   @author Christoph Gille
   @see GraphNode
   @see GraphSun
*/
public class GraphEdge  {

    private final GraphNode _from, _to;
    private final Object _data;
    private final ChRunnable _r;
    private boolean _dist;
    public GraphEdge(GraphNode from,GraphNode to, ChRunnable r, Object o) {
        _r=r; _data=o;
        _from=from;
        _to=to;
    }
    public boolean isDistanceScore() {return _dist;}
    public double len() {
        final double dd[]=_r==null ? null : (double[])_r.run(ChRunnable.RUN_GET_DISSIMILARITY_VALUE, this);
        if (dd!=null) _dist=dd[1]>=0;
        return dd==null?Double.NaN : dd[0];
    }
    public GraphNode from() { return _from; }
    public GraphNode to() { return _to; }
    public Object data() { return _data;}

}
