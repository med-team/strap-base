package charite.christo;
import javax.swing.text.*;
import java.awt.*;
import java.util.*;
import static charite.christo.ChUtils.*;

public class  ChJHighlighter extends DefaultHighlighter {
    private final Object _tools;
    // private boolean _isAfter;
    // public void setAfter(boolean b) { _isAfter=b;}
    public ChJHighlighter(ChTextComponents tools) {
        _tools=wref(tools);
    }
    /*
   @Override public void paint(Graphics g) {
        final ChTextComponents tools=deref(_tools,ChTextComponents.class);
        if (tools==null) return;
        final Rectangle clip=clipBnds(g);
        final int
            pos1=tools.boundsToPos(true,clip),
            pos2=tools.boundsToPos(true,clip);
        if (tools!=null) tools.paintHighlights(pos1-1, pos2, true, g, clip);
        super.paint(g);
   }
    */
    @Override public void paintLayeredHighlights(Graphics g, int p0, int p1, Shape viewBounds, JTextComponent editor, View view) {
        final ChTextComponents tools=deref(_tools,ChTextComponents.class);
        if (tools!=null) {
            tools.paintHighlights(p0, p1, false, g, deref(viewBounds,Rectangle.class));
            /* Ueberdeckt wenn p0-1 */
        }
        super.paintLayeredHighlights(g, p0, p1, viewBounds, editor, view);
    }

    private final static Map<Color, HighlightPainter> mapColor=new HashMap();
    public static Object addColoredBg(int from, int to, Color color, JTextComponent tc) {
        if (color==null) return null;
        HighlightPainter hp=mapColor.get(color);
        if (hp==null) mapColor.put(color, hp=new DefaultHighlighter.DefaultHighlightPainter(color));
        final Highlighter hl=tc==null?null:tc.getHighlighter();
        if (hl!=null) try { return hl.addHighlight(from,to,hp); } catch(Exception e){ stckTrc();}
        return null;
    }
}

