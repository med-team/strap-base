package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import java.io.*;
/*
  Similar to java.lang.StringBuffer but more efficient.
  Consumes only 8 bit and does not throw OutOfMemory as aften as StringBuffer.
  Contains an 8bit  byte array to store text.
  Line ends are returned as an array of text positions.
*/
public final class BA implements CharSequence, Comparable, ChRunnable, HasMC, HasMap {
    private final static Object SYNC_getLineEnds=new Object();
    public final static int OPTION_IS_FILTERED=1<<1, OPTION_CACHE_TO_STRG=1<<2;
    static { assert OPTION_CACHE_TO_STRG < FILTER_FIRST_OPTION; }
    private byte _bytes[];
    private int _begin, _end,  _mc, _reserve=99, _growPercent=133, _hc, _opt;
    private Object _cache, _send;
    private static Object _eol;
    public final static String KEY_SEND_TO="BA$$KST";
    /* <<< Fields <<< */
    /* ---------------------------------------- */
    /* >>> Constructor >>> */
    public BA(byte[] bb) { set(bb,0,sze(bb));}
    public BA(byte[] bb, int from, int to) { set(bb, from, to); }
    public BA(Object txt, int from, int to) {
        if (txt instanceof BA) {
            final BA sb=(BA)txt;
            set(sb.bytes(), from+sb.begin(), mini(sb.length()-sb.begin(),to)+sb.begin());
        } else set(toByts(txt), from,to);
    }
    public BA(int l) { _bytes=l>0 ? new byte[l] : NO_BYTE; }
    public BA(CharSequence cs) {
        if (cs instanceof BA) {
            final BA ba=(BA)cs;
            set(ba.bytes(), ba.begin(), ba.end());
        } else if (sze(cs)>0) {
            set(toByts(toStrg(cs)),0,cs.length());
        }
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Setters >>> */
    public BA setOptions(int o) { _opt=o; return this; }
    public BA setFilterOptions(int o) { _opt=o; return this; }
    public BA setBegin(int index){
        _begin=index;
        txtOrRangeChanged();
        return this;
    }
    private void txtOrRangeChanged() {
        _mc++; _cache=null; _hc=0;
    }
    public BA clr() {
        _esc=NO_LONG;
        _escL=_escEndText=0;
        return setBegin(0).setEnd(0);
    }
    public BA setEnd(int index){
        if (index==0) _opt&=~OPTION_IS_FILTERED;
        _end=maxi(0,index);
        txtOrRangeChanged();
        return this;
    }
    public BA set(byte[] bb, int b, int e) {
        if (!bytsEquls(bb, 0, mini(e,_end), _bytes, 0)) _escL=_escEndText=0;
        _bytes=bb;
        _begin=b;
        return setEnd(e);
    }

    public void setCharsAt(int b, int e, byte c) {
        final byte T[]=ensureCapacity(e);
        final int E=end();
        for(int i=b;i<e;i++) T[i]=c;
        if (E<e) setEnd(e);
        else txtOrRangeChanged();
    }
    /* <<< Setters <<< */
    /* ---------------------------------------- */
    /* >>> Basic Getters >>> */
    public int getOptions() {return _opt;}
    /** First text position +1 */
    public int begin(){ return _begin;}
    /** Last text position +1 */
    public int end() { return mini(bytes().length, _end);}
    /** The byte array used internally to store the text; */
    public byte[] bytes() { final byte bb[]=_bytes; return bb!=null ? bb : NO_BYTE; }
    /* <<< Basic Getters <<< */
    /* ---------------------------------------- */
    /* >>> Line ends >>> */
    /**
       Line ends.
       Indices of the '\n'.
       If trailing  '\n' is missing inlcude also text-length+1 .
    */
    public int[] eol(){
        final Object[] cached=cached();
        int ends[]=(int[]) cached[CACHE_ENDS];
        if (ends==null) {
            synchronized(SYNC_getLineEnds) {
                int[] buf=(int[])deref(_eol);
                if (buf==null) _eol=newSoftRef(buf=new int[32*1024]);
                final byte bb[]=bytes();
                final int e=mini(bb.length,end());
                int count=0;
                for(int i=begin(); i<e; i++) {
                    if (bb[i]=='\n') {
                        if (buf.length<=count+1) buf=chSze(buf,count+1+count/2);
                        buf[count++]=i;
                    }
                }
                if (e==0 || bb[e-1]!='\n') {
                    buf[count++]=e;
                }
                System.arraycopy(buf, 0, ends=new int[count], 0, count);
                cached[CACHE_ENDS]=ends;
            }
        }
        return ends;
    }
    /* <<< Line Ends <<< */
    /* ---------------------------------------- */
    /* >>> Ansi >>> */
    private long[] _esc=NO_LONG;
    private int _escL, _escEndText;
    public int countAnsiEscapes() {
        getAnsiEscapes();
        return _escL;
    }
    public long[] getAnsiEscapes() {
        if (end()>_escEndText) {
            final int oldL=_escL;
            findEscapes(_escEndText);
            rmAnsiEsc(oldL, _escL);
        }
        return _esc;
    }
    private long[] findEscapes(int from) {
        final byte[] T=bytes();
        final int E=mini(end(),sze(T));
        long ee[]=_esc;
        int count=_escL;
        for(int pos=from; pos+2<E; pos++) {
            if (T[pos]!='\u001B' || T[pos+1]!='[') continue;
            int begin=pos, i=pos+2;
            for(int end; (end=AnsiEscape.escapeEndsAt(i, T, E))>0; ) {
                if (ee==null || count>=ee.length) ee=chSze(ee,count+10+count/2);
                ee[count++]=AnsiEscape.newEscape(atoi(T,i,E), begin, (end-begin));
                i=begin=end;
            }
        }
        _escL=count;
        _escEndText=E;
        return _esc=ee;
    }
    private int rmAnsiEsc(int fromIdx, int toIdx) {
        final byte[] T=bytes();
        final long ee[]=_esc;
        final int E=mini(end(),sze(T));
        final int N=mini(toIdx,sze(ee));
        int sum=0;
        for(int i=fromIdx; i<N; i++) {
            long e=ee[i];
            final int len= (int)((e>>AnsiEscape.SHIFT_LEN)&255);
            if (len==0) continue;
            final int
                pos=(int)(e&AnsiEscape.MASK_POS),
                from=pos+len,
                to=i+1<N ? (int)(ee[i+1]&AnsiEscape.MASK_POS) : E,
                dest=pos-sum;
            try {
                System.arraycopy(T,from, T,dest, to-from);
            } catch (Exception ex) {
                stckTrc(ex);
                putln(new BA(99).a(RED_ERROR+"BA  from=").a(from).a(" dest=").a(dest).a(" N=").a(to-from));
                continue;
            }
            e&=~ (255L<<AnsiEscape.SHIFT_LEN);
            e=(e& AnsiEscape.MASK_POS_INV) |  (pos-sum);
            ee[i]=e;
            sum+=len;
        }
        if (sum>0) {
            _end=maxi(0,E-sum);
            _escEndText-=sum;
            _cache=null;
        }
        return sum;
    }
    /* <<< Ansi <<< */
    /* ---------------------------------------- */
    /* >>> pre-tags >>> */
    /*
    public BA rmOuterPreTags() {
        if (strEquls("<pre>",trim()) && endWith("</pre>",this)) setBegin(begin()+5).setEnd(end()-6);
        return this;
    }
    */
    public void insidePreTags(String cssClass) {
        delBlanksL();
        final int b=begin(), e=end();
        final byte[] T=bytes();
        if (e-b>20 &&   T[b]=='<' &&  (looks(LIKE_HTML, T,b) || strEquls(STRSTR_IC,"<?xml ",T,b) || strEquls("<p>Multiple sequence alignment ",T,b)  )) {
            /* Ausnahme: http://www.ensembl.org//Homo_sapiens/Component/Gene/Web/ComparaTree/Align?g=ENSG00000008853 */
            final int pre=strstr(STRSTR_IC|STRSTR_AFTER,"<pre",T,b+10,e);
            final int preGT=pre<0 ? -1 : strchr('>',T,pre,e);
            final boolean ok=preGT>=0 && (preGT==pre || T[pre]==' ') && (cssClass==null || strstr(STRSTR_w,cssClass,T,pre,preGT)>0);
            final int erp=ok ? strstr(STRSTR_IC,"</pre>",T,pre,e) : -1;
            if (erp>0) {
                setBegin(preGT+1);
                setEnd(erp);
                delBlanksL();
                { /* For files from Kegg */
                    while(strEquls("<!-- ",T,begin()) ) {
                        final int commentE= strstr(STRSTR_AFTER," -->",T,begin(),erp);
                        if (commentE<0) break;
                        setBegin(commentE);
                        delBlanksL();
                    }
                }
            }
        }
    }
    public BA delBlanksL() { return setBegin(nxtE(-SPC,bytes(),begin(),end())); }
    public BA delBlanksR() { return setEnd(prevE(-SPC,bytes(),end()-1,begin()-1)+1); }
    public BA trim() { delBlanksR(); return delBlanksL(); }
    public BA trimSize() {
        final byte[] T=toByts(this);
        return set(T,0,T.length);
    }
    /** returns the text between begin() and end() */
    public byte[] newBytes() { return newBytes(begin(),end()); }
    public byte[] newBytes(int b, int e) {
        final int B=maxi(0,b);
        final int E=mini(e,end());
        final byte[] T=bytes();
        if (E<=B) return NO_BYTE;
        final byte bb[]=new byte[E-B];
        System.arraycopy(T,B,bb,0,bb.length);
        return bb;
    }
    //   public String toStringIP(int from, int to) {  return toStrgIP(bytes(),from,to); }
    public String newString(int from, int to) { return getString(0L,from,to);}
    public String newStringTrim(int from, int to) { return getString(0L, nxtE(-SPC,bytes(),from,to),to); }
    public final static long  STRING_POOL=1L<<30, STRING_TRIM=1L<<31;
    /* Options can be combined with ChUtils.mapColumn1Column2(... ) */
    public String getString(long options, int from, int to) {
        final byte[] bb=bytes();
        int f=maxi(0,from), t=mini(sze(bb),to,bb.length);
        if ( (options&STRING_TRIM)!=0) {
            f=nxtE(-SPC,bb,f,t);
            t=prevE(-SPC,bb,t-1,f-1)+1;
        }
        return
            f>=t ?  "" :
            (options&STRING_POOL)!=0 ?  toStrgIP(bb,from,to) :
            bytes2strg(bb,f,t);
    }
    public int idxToRow(final int position){
        try {
            final int[] ends=eol();
            if (position<0) return -1;
            if (position>ends[ends.length-1]) return ends.length-1;
            int lastStep=-1, lastRow=-1, step=ends.length/2, row=step;
            for(;;) {
                if (lastStep==step && lastRow==row) { stckTrc(); return -1; } // should never happen
                lastStep=step;
                lastRow=row;
                if (row<0) row=0;
                if (row>=ends.length) row=ends.length-1;
                step=(step+1)/2;
                final int begin=row==0?0:ends[row-1]+1;
                if (begin<=position && position<=ends[row]) return row;
                row+=begin<position ? step : -step;
            }
        } catch(Exception e){ stckTrc(e);}
        return -1;
    }
    public int mc() { return _mc;}
    public void setTextSegment(javax.swing.text.Segment s) {
        if (s==null) return;
        final int sBegin=s.getBeginIndex();
        final int sEnd=s.getEndIndex();
        final byte[] bb=sze(_bytes)>sEnd-sBegin ? _bytes : new byte[sEnd-sBegin+101];
        final char cc[]=s.array;
        int iB=0;
        for(int iS=sBegin; iS<sEnd;iS++) {
            if (cc[iS]!='\r' && cc[iS]!=0) bb[iB++]=(byte)cc[iS];
        }
        set(bb,0,iB);
    }
    public boolean onlyWhiteSpace() {
        final byte bb[]=bytes();
        final int end=end();
        for(int i=begin(); i<end; i++) {
            final int c=bb[i];
            if (c!=' ' && c!='\n' && c!='\r' && c!='\t' && c!=0) return false;
        }
        return true;
    }
    public BA enlargeCapacityBy(int reserve, int growPercent) { _reserve=maxi(0,reserve); _growPercent=maxi(101, growPercent); return this;}
    public BA a(long num) { return a(num,0);}
    public BA a(char ch) {
        final int e=end();
        ensureCapacity(e+1)[e]=(byte)ch;
        return setEnd(e+1);
    }
   public BA a1(char ch) {
       if (charAt(end()-1)!=ch) a(ch);
       return this;
    }

    public BA insertChars(int pos0,char ch, int times) {
        _escL=0;
        final int pos=pos0+begin();
        final int e=end();
        if (pos<0 || pos>e) { assrt(); return this; }
        final byte[] T=ensureCapacity(e+times);
        System.arraycopy(T,pos,T,pos+times, e-pos);
        for(int i=times; --i>=0;) T[pos+i]=(byte)ch;
        return setEnd(e+times);
    }
    public BA a(char ch, int times) {
        if (times>0) {
            final int end=end();
            final byte T[]=ensureCapacity(end+times);
            for(int i=times;--i>=0;) T[end+i]=(byte)ch;
            setEnd(end+times);
        }
        return this;
    }
    public BA aSomeBytes(long n,int num) {
        final int e=end();
        final byte txt[]=ensureCapacity(e+num);
        setEnd(e+num);
        for(int i=num; --i>=0;) {
            final byte b=(byte) ((n>>>(i*8)) & 127);
            txt[e+i]=b==0?32:b;
        }
        return this;
    }
    private final static Object SB[]={null};
    public BA a(double f) {

        synchronized(SB) {
            return a(sbClr(SB).append(f));
        }
    }
    public BA a(float f) {
        final Object[] cached=cached();
        synchronized(cached) {
            return a(sbClr(SB).append(f));
        }
    }
    public BA aFloat(double f, int precision) {
        if (Double.isNaN(f)) return a("NaN");
        final long l=(long)f;
        final int L=stringSizeOfInt(l);
        return L>precision ? a(l) : a(f, L,precision-L-1);
    }
    public BA aBigFloat(double f, int precision) {
        if (Double.isNaN(f)) return a("NaN");
        if (precision>0 && javaVsn()>14) {
            final String s=forJavaVersion(15).run(RunIf15orHigher.RUN_BIG_DECI, new Object[]{Double.toString(f),intObjct(precision)}).toString();
            final int e=s.indexOf('E'), nulls=(e>0 ? e : s.length())-1;
            if (chrAt(nulls,s)=='0' && s.indexOf('.')>0) {
                int nul=nulls;
                while( chrAt(nul-1,s)=='0') nul--;
                if(chrAt(nul-1,s)=='.') nul--;
                return a(s,0,nul).a(s,nulls+1,MAX_INT);
            } else return a(s);
        } else {
            synchronized(SB) {
                return a( (f>0 ? f<1E-44 : -f<1E-44) ? sbClr(SB).append(f) : sbClr(SB).append((float)f));
            }
        }
    }

    public BA join(Object oo) {  return join(oo,"\n"); }
    public BA join(Object collectionOrArray, String glue) { return join2(null, collectionOrArray, glue); }
    public BA join2(String glueL, Object collectionOrArray, String glueR) {
        if (collectionOrArray==null) return this;
        final Object oo=collectionOrArray instanceof Set ? oo(collectionOrArray) : collectionOrArray;
        if (!(oo instanceof Object[] || oo instanceof List)) {
            return
                glueL!=null ? a(glueL).a(oo).a(glueR) :
                a(oo);
        }
        final int L=sze(oo);
        int count=0,len=L;
        for(int i=0;i<L;i++) {
            final Object o=get(i,oo);
            if (o==null) continue;
            len+=o instanceof char[] || o instanceof byte[] || o instanceof CharSequence ? sze(o) : 9;
        }
        ensureCapacity(end()+len+L*(sze(glueL)+sze(glueR)));
        for(int i=0;i<L;i++) {
            final Object o=get(i,oo);
            if (o==null) continue;
            if (glueL!=null) a(glueL);
            else if (count++>0) a(glueR);
            a(o);
            if (glueL!=null) a(glueR);
        }
        return this;
    }

    public BA joinBreakLines(Object[] ss,final String glue, int foldInitWidth, int foldWidth, String lineBreak) {
        boolean isFirst=true;
        int L=length()+foldInitWidth;
        for(int i=0; ss!=null && i<ss.length; i++) {
            if (ss[i]==null) continue;
            if (!isFirst && glue!=null) a(glue);
            isFirst=false;
            a(ss[i]);
            if (lineBreak!=null && length()-L>foldWidth) {
                a(lineBreak);
                L=sze(this);
            }
        }
        return this;
    }

    public BA foldText(Object text, int width, int initWidth) {
        final int L;
        final byte bb[];
        final CharSequence s;
        if (text instanceof byte[]) { bb=(byte[])text; s=null; L=bb.length; }
        else if (text instanceof CharSequence) { s=(CharSequence)text; bb=null; L=s.length(); }
        else { s=toStrg(text);  bb=null; L=sze(s); }
        int t=end();
        final byte[] T=ensureCapacity(t+L+L/width+2);

        for(int i=0, w=initWidth; i<L; i++) {
            final byte c=bb!=null?bb[i]:(byte)s.charAt(i);
            T[t++]=c;
            if (++w>=width) { T[t++]=(byte)'\n'; w=0; }
        }
        return setEnd(t);
    }
    public BA aln(Object s) {
        if (s!=null) {
            a(s).a('\n');
            if (length()>_sendIfExeeds) send();
        }
        return this;
    }
    public BA aln(int c) { return a(c).a('\n'); }
    public BA aln(char c) { return a(c).a('\n'); }
    public BA tab(int c) { return a(c).a('\t'); }
    public BA tab(char c) { return a(c).a('\t'); }
    public BA tab(Object s) { return a(s).a('\t'); }
    public BA a(Object s) {
        if (s!=null) {
            if (s==LOG_SHOW) shwTxtInW(this);
            else a(s,0,MAX_INT);
        }
        return this;
    }

    public BA a(boolean b) { return a(b?"true":"false"); }
    public BA a(Object o, int from, int to) {
        if (o instanceof java.awt.Color) return aHex(((java.awt.Color)o).getRGB(), to==MAX_INT?6:to);
        return filter(_opt, 0, o, from,to);
    }
    public BA colorBar(int rgb) { return a("<font color=\"#").aHex(rgb,6).a("\" style=\"background-color: #").aHex(rgb,6).a(";\">&nbsp;|</font>"); }
    public BA filter(long option, Object o) { return filter(option,0,o,0,MAX_INT); }
    public BA filter(long option, int charClass, Object o) { return filter(option,charClass,o,0,MAX_INT); }
    public BA filter(long option, int charClass, Object text, int fromPosition, int toPosition) {
        final byte bb[];
        final char cc[];
        final CharSequence cs;
        final String s;
        final Object o;
        final int L;
        if (text instanceof byte[]) { o=bb=(byte[])text; cc=null; cs=s=null; L=bb.length; }
        else if (text instanceof char[])  { o=cc=(char[])text; bb=null; cs=s=null; L=cc.length; }
        else if (text instanceof String) { o=s=(String)text; bb=null; cc=null; cs=null; L=s.length(); }
        else if (text instanceof CharSequence) { o=cs=(CharSequence)text; bb=null; cc=null; s=null; L=cs.length(); }
        else { o=s=toStrg(text); bb=null; cc=null; cs=null; L=sze(s); }
        int from=maxi(0,fromPosition), to=mini(L,toPosition);
        if (from>=to) return this;
        if (0!=(option&FILTER_NO_HTML_BODY)) {
            final int body=strstr(STRSTR_IC|STRSTR_w,"<body", o,from,to);
            final int ydob=body<0?-1:strstr(STRSTR_IC|STRSTR_w,"</body>", o,body,to);
            if (ydob>0) {
                from=strchr('>',o,body,to)+1;
                to=ydob;
            }
        }
        if (o instanceof BA) {
            final int b=((BA)o).begin();
            return filter(option, charClass,  ((BA)o).bytes(), from+b, to+b);
        }
        final byte noMatch=0==(option&FILTER_NO_MATCH_TO)?0:  (byte)(option&255);
        final int E=end();
        final boolean toU=0!=(option&FILTER_TO_UPPER), toL=0!=(option&FILTER_TO_LOWER);
        int dest=E;
        byte[] T=NO_BYTE;
        boolean changed=false;
        if ((option&~(FILTER_TO_LOWER|FILTER_TO_UPPER))==0 && charClass==0 && (bb!=null || s!=null)) {
            T=ensureCapacity(dest=E+to-from);
            if (bb!=null) System.arraycopy(bb, from, T, E, to-from);
            else for(int i=to-from; --i>=0;) T[E+i]=(byte)s.charAt(from+i);
        } else {
            final boolean
                oct=0!=(option&FILTER_DECODE_PSQL_OCTAL),
                u2d=0!=(option&FILTER_UNIX_TO_DOS),
                htm=0!=(option&FILTER_HTML_DECODE),
                HTM=0!=(option&FILTER_HTML_ENCODE),
                asc=0!=(option&FILTER_BACKSPACE),
                uen=0!=(option&FILTER_URL_ENCODE),
                fld=0!=(option&FILTER_FOLD),
                trm=0!=(option&FILTER_TRIM),
                q2h=0!=(option&FILTER_QUOTE_TO_HTML),
                tip=0!=(option&FILTER_LT_GT_4TT),
                urlenc[]=uen?chrClas(URLENCOD):null;
            boolean startRecording=!trm;
            final int reserve=u2d||fld?1:uen?3:tip||HTM?20:0;
            int column=0, trimEnd=E;
            boolean trimNoSpc=false;
            for(int i=from; i<to; i++) {
                final int c;
                if (cs!=null) try { c=cs.charAt(i); } catch(Exception ex){ break;}
                else c=s!=null ? s.charAt(i) : cc!=null ? cc[i] : bb[i];
                if (trm) {
                    if (trimNoSpc=(c!=' ' && c!='\t' && c!='\r' && c!='\n')) startRecording=true;
                    if (!startRecording) continue;
                }
                int append=c;
                String appendS=null;
                if (dest+reserve>=T.length) T=ensureCapacity(dest+to-i+reserve+
                                                             (fld ? (to-i)/(255&charClass)+2:0)+
                                                             (u2d||uen|tip ? (to-i)/10:
                                                              0));
                if (u2d && c=='\n' && chrAt(i-1,o)!='\r') {
                    changed=true;
                    T[dest++]=(byte)'\r';
                }

                if (htm && c=='&') {
                    final int c1=chrAt(i,o)!='&'?0:chrAt(i+1,o)|32;
                    if (c1=='a' || c1=='l' || c1=='g' || c1=='#' || c1=='q') {
                        final int c2=chrAt(i+2,o)|32, c3=chrAt(i+3,o)|32;
                        if (';'==c3 && c2=='t') { append=c1=='l'?'<' : c1=='g'?'>' : 0; i+=3; }
                        if (';'==chrAt(i+4,o)) {
                            if (c1=='a' && c2=='m' && c3=='p') { append='&'; i+=4; }
                            if (c1=='#') {
                                final int h2=HEX2INT[c2], h3=HEX2INT[c3];
                                if (h2>=0 && h3>=0) { append=16*h2+h3; i+=4; }
                            }
                        }
                        if (';'==chrAt(i+5,o)) {
                            final int c4=chrAt(i+4,o);
                            if (c1=='a' && c2=='p' && c3=='o' && c4=='s') { append='\''; i+=5; }
                            if (c1=='q' && c2=='u' && c3=='o' && c4=='t') { append='"';  i+=5; }
                        }
                        changed|= c!=append;
                    }
                }
                if (oct && c=='\\' && i+3<to) {
                    final int c1, c2, c3;
                    if (
                        ((c1=chrAt(i+1,o))=='0' || c1=='1') &&
                        ((c2=chrAt(i+2,o))<='0' || c2<='9') &&
                        ((c3=chrAt(i+3,o))<='0' || c3<='9')) {
                        append=(c3-'0')+8*(c2-'0')+8*8*(c1-'0');
                        i+=3;
                    }
                }
                if (q2h && c=='\'') {
                    appendS="&#27";
                    append=';';
                    changed=true;
                }
                if (tip && (c=='<' || c=='>')) {
                    appendS= c=='<' ? "'+LT+" : "'+GT+";
                    append='\'';
                    changed=true;
                }
                if (HTM && (c=='<' || c=='>' || c=='&' || c=='"' || c=='\'')) {
                    appendS= c=='<' ? "&lt" : c=='>' ? "&gt" : c=='&' ? "&amp" : c=='\'' ? "&apos" : c=='"' ? "&quot" :null;
                    append=';';
                    changed=true;
                }
                if (uen) {
                    if (c>0 && c<128 && urlenc[c]) append=c;
                    else {
                        changed=true;
                        if (c==' ') append='+';
                        else {
                            T[dest++]=(byte)'%';
                            T[dest++]=(byte) INT2HEX[(c>>>4)&15];
                            append=INT2HEX[c&15];
                        }
                    }
                }
                if (fld) {
                    T[dest++]=(byte)c;
                    if (c=='\n') column=0;
                    else if (++column>=charClass) { changed=true; T[dest++]=(byte)'\n'; column=0;}
                } else {
                    if (charClass==0 || is(charClass,c)) {
                        if (asc && append==0x08 && dest>0) dest--;
                        else {
                            if (appendS!=null) for(int k=0; k<appendS.length(); k++) T[dest++]=(byte)appendS.charAt(k);
                            T[dest++]=(byte)append;
                        }
                    } else {
                        changed=true;
                        if (noMatch!=0) T[dest++]=noMatch;
                    }
                }
                if (trimNoSpc) trimEnd=dest;
            }
            if (trm) dest=trimEnd;
        }
       setEnd(dest);
       T=bytes();
       if (toU) for(int i=dest; --i>=E;) if ( 'a'<=T[i] && T[i]<='z') { changed=true; T[i]&=~32; }
       if (toL) for(int i=dest; --i>=E;) if ( 'A'<=T[i] && T[i]<='Z') { changed=true; T[i]|=32; }
       if (changed) _opt|=OPTION_IS_FILTERED; else _opt&=~OPTION_IS_FILTERED;
       return this;
    }
    private static boolean notZeroNotEmpty(Object a) {
        return
            a!=null &&
            !(a instanceof CharSequence && ((CharSequence)a).length()==0) &&
            !(a instanceof byte[] && ((byte[])a).length==0);
    }
    public BA and(Object a, char c) {  return  notZeroNotEmpty(a) ?  a(a).a(c) : this; }
    public BA and(char c, Object a) {  return  notZeroNotEmpty(a) ?  a(c).a(a) : this; }
    public BA and(Object a1, Object a2) {  return  notZeroNotEmpty(a1)&&notZeroNotEmpty(a2) ?  a(a1).a(a2) : this; }
    public BA and(Object a1, Object a2, Object a3) {   return  notZeroNotEmpty(a1)&&notZeroNotEmpty(a2)&&notZeroNotEmpty(a3) ?  a(a1).a(a2).a(a3) : this;  }

    public BA aIfSze(int minSize, Object a1, Object a2) {  return  sze(a1)>=minSize && sze(a2)>=minSize  ?  a(a1).a(a2) : this; }

    public BA or(Object a1, Object a2) {  return  notZeroNotEmpty(a1) ?  a(a1) : a(a2); }
    public BA or(Object a1, Object a2, Object a3) {  return  notZeroNotEmpty(a1) ?  a(a1) : notZeroNotEmpty(a2) ? a(a2) : a(a3); }

    public BA a1(Object s) {
        final int L=sze(s);
        if (L>0 && !strEquls(s,bytes(), end()-L)) a(s);
        return this;
    }

    private final static byte[] FORMAT10=new byte[99];
    public BA format10(long num,int wInt) {
        synchronized(FORMAT10) {
            final int S=itoa(num,FORMAT10,0);
            final int commas=maxi(0,num>=0 ? S-1 : S-2)/3;
            a(' ',maxi(0, wInt-commas-S));
            for(int i=0; i<S; i++) {
                final char c=(char)FORMAT10[i];
                if (c!='-' &&  i!=0 && FORMAT10[i-1]!='-' && i!=S-1 && (S-i)%3==0) a(',');
                a(c);
            }
            return this;
        }
    }

    public BA formatSize(File f) { return f==null ? a(" null ") : formatSize(sze(f)); }
    public BA formatSize(int L)  { return L>5000 ?  format10(L/1024,10).a(" kByte") :  format10(L,10).a(" Byte");
    }

    public BA a(long num,int width) {
        final int strSize=stringSizeOfInt(num);
        final int size=width>strSize?width:strSize;
        final int end=end();
        final byte[] bb=ensureCapacity(end+size);
        for(int idx=end +size-strSize; --idx>=end;) bb[idx]=32;
        itoa(num,bb,end +size-strSize);
        return setEnd(end+size);
    }
    public BA a(double number,int wInt,int wFrac) {
        final int end=end();
        ensureCapacity(end+100);
        ftoa(number,wInt,wFrac,bytes(),end);
        return setEnd(end+wInt+wFrac+(wFrac>0?1:0));
    }
    public byte[] ensureCapacity(int n) {
        final byte bb[]=_bytes;
        return _bytes= bb==null? new byte[n] : n>bb.length ? chSze(bb, (int) (n*(long)_growPercent/100+_reserve)) : bb;
    }

    public BA aHex(long rgb, int digits) {
        final int E=end();
        final byte[] T=ensureCapacity(E+digits);
        int dest=E;
        for(int j=4*digits; (j-=4)>=0;)  T[dest++]=(byte)INT2HEX[(int)( (rgb>>j)&15)];
        return setEnd(dest);
    }
    public BA aQuoteIfNecessary(Object o) {
        final Object s=o instanceof File  || o instanceof java.net.URL ? toStrg(o) : o;
        if (o==null) return this;
        final boolean
            isW=isWin(),
            ok=
            nxt(-FILEP,s)<0 ||
            isW && nxt(-FILEP,s,2)<0 && chrAt(1,s)==':' && is(LETTR,s,0);
        if (!ok) {
            final char quote=strchr('"',s)<0||isW?'"' : '\'';
            a(quote).a(s).a(quote);
        } else a(s);
        return this;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    public BA replace(ChTextReplacement r) {
        if (r!=null) replace(0L, r.getNeedle(), r.getReplacement());
        return this;
    }

    public BA aRplc(char c, char replacement, Object o) {
        final int L=length();
        return a(o).replaceChar(c,replacement,L,MAX_INT);
    }

    public BA replaceChar(char c, char replacement) { return replaceChar(c,replacement,0,MAX_INT); }
    public BA replaceChar(char c, char replacement, int from, int to) {
        final byte[] T=bytes();
        final int B=begin();
        final int f=B+maxi(0,from);
        final int t=B+mini(to,end()-B);
        boolean changed=false;
        final byte rplc=(byte)replacement;
        for(int i=f; i<t; i++) {
            if (T[i]==c) {
                T[i]=rplc;
                changed=true;
            }
        }
        if (changed)_mc++;
        return this;
    }

    public BA replace(long options, Object pattern,  Object replacement) {
        return replace(options, pattern, replacement, 0, MAX_INT);
    }
    public BA replace(long options, Object pattern,  Object replacement, int from, int to) {
        final BA ba=toBA(strplc(options, pattern, null,null, replacement, this, from, to));
        if (this!=ba) set(ba.bytes(), ba.begin(), ba.end());
        return this;
    }
    public BA replaceRange(int from, int to0, Object replacement) {
        final int E=end(), to=mini(E,to0);
        if (from<0||from>to || replacement==null) return this;
        final byte bb[];
        final char cc[];
        final CharSequence cs;
        final int L;

        if (replacement instanceof byte[]) { bb=(byte[])replacement; cc=null; cs=null; L=bb.length; }
        else if (replacement instanceof char[]) { cc=(char[])replacement; bb=null; cs=null; L=cc.length; }
        else  { cs=(CharSequence)replacement; bb=null; cc=null; L=cs.length(); }
        final int increase=L- to+from, fromB=from+begin();
        final byte T[]=ensureCapacity(end()+increase);
        if (increase!=0) System.arraycopy(T,to,T,to+increase,E-to);
        if (L>0) {
            if (bb!=null) System.arraycopy(bb,0,T,fromB,L);
            else if (cs instanceof BA) {
                System.arraycopy( ((BA)cs).bytes(), ((BA)cs).begin(), T, fromB, L);
            } else if (cs instanceof String) {
                final String s=(String)cs;
                for(int i=L; --i>=0;) T[fromB+i]=(byte) s.charAt(i);
            } else if (cc!=null) {
                for(int i=L; --i>=0;) T[fromB+i]=(byte)cc[i];
            } else {
                for(int i=L; --i>=0;) T[fromB+i]=(byte) cs.charAt(i);
            }
        }
        _escL=0;
        return setEnd(E+increase);
    }
    public BA insert(int pos0, Object strg) { return replaceRange(pos0,pos0,strg);}

    public BA insertI(int pos, int num) {
        final int sze=stringSizeOfInt(num), E=end();
        final byte T[]=ensureCapacity(E+sze);
        System.arraycopy(T,pos,T,pos+sze,E-pos);
        itoa(num, T, pos);
        _escL=0;
        return setEnd(E+sze);
    }

    public BA rmFirstLastChar(char cFirst, char cLast) {
        final int b=begin(), e=end();
        final byte T[]=bytes();
        if (e-b>1 && T[b]==cFirst && T[e-1]==cLast) setBegin(b+1).setEnd(e-1);
        return this;
    }

    public BA aRplc(Object txt, Object replacement,  TextMatches matches) {
        final int fromTo[]=matches.fromTo(), count=matches.count(), tL=sze(txt), N=mini(count,sze(fromTo)/2);
        for(int i=0, lastTo=0; i<=N; i++) {
            final int from=i<N?fromTo[i*2]:tL;
            a(txt, lastTo, from);
            if (i<N) {
                a(replacement);
                lastTo=fromTo[i*2+1];
            }
        }
        return this;
    }
    private final static Object SYNC_STRPLC[]=new Object[256];
    public BA aRplc(long options, Object pattern, Object replacement,  CharSequence txt) {
        final TextMatches matches=fromSoftRef(Thread.currentThread().hashCode()&0xFF, SYNC_STRPLC, TextMatches.class);
        synchronized(matches) {
            strstr(options, pattern, 0,MAX_INT, null, null, txt, 0, MAX_INT, matches.clear());
            aRplc(txt,replacement, matches);
        }
        return this;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    public BA delSuffix(String s) {
        if (s!=null) {
            final int n=end()-sze(s);
            if (n>=begin() && strEquls(s,bytes(),n)) setEnd(n);
        }
        return this;
    }
    public void write(OutputStream os) throws IOException {
        final int b=begin(), e=end();
        if (e-b>0) os.write(bytes(), b, e-b);
    }
    @Override public String toString() {
        if (0==(getOptions()&OPTION_CACHE_TO_STRG)) return newString(begin(),end());
        final Object[] cached=cached();
        String s=(String)cached[CACHE_TO_STRG];
        if (s==null) cached[CACHE_TO_STRG]=s=toString();
        return s;
    }
    public char charAt(int i) {
        if (i<0) return (char)0;
        final int iT=i+begin();
        final byte[] T=bytes();
        return (char)(iT<end() && iT<T.length ? T[iT] : 0);
    }
    public int length() { return maxi(0,end()-begin());}
    public BA subSequence(int start, int end) {
        final int b=begin(); return new BA(bytes(), b+start, b+end);
    }
    public byte[] subSequenceAsBytes(int start, int end) {
        final int b=maxi(0,start), e=mini(end(),end);
        if (e-b<=0) return NO_BYTE;
        final byte[] T=bytes(), bb=new byte[e-b];
        System.arraycopy(T,b, bb,0, e-b);
        return bb;
    }
    @Override public boolean equals(Object o) {
        if (!(o instanceof CharSequence)) return false;
        final CharSequence cs=(CharSequence)o;
        final int L=cs.length();
        final byte[] T=bytes();
        final int b=maxi(0,begin()), e=mini(T.length, end());
        if (L!=e-b) return false;
        for(int i=L; --i>=0; ) {
            if (cs.charAt(i)!=T[b+i]) return false;
        }
        if (myComputer() && o instanceof BA && !(toString().equals(o.toString()))) assrt();
        return true;
    }
    public int compareTo(Object anotherString) {
        if (!(anotherString instanceof CharSequence)) return 0;
        final CharSequence anotherStr=(CharSequence)anotherString;
        final int len1=length(), len2=anotherStr.length();
        int n=mini(len1, len2);
        final byte v1[]=bytes(), v2[]=anotherStr instanceof BA ? ((BA)anotherStr).bytes() : null;
        int i=begin();
        int j=v2!=null ? ((BA)anotherStr).begin() : 0;
        if (i == j) {
            int k=i;
            final int lim=n + i;
            while (k < lim) {
                final byte c1=v1[k], c2=v2!=null ? v2[k] : (byte)anotherStr.charAt(k);

                if (c1 != c2) return c1 - c2;
                k++;
            }
        } else {
            while (n-- != 0) {
                final byte c1=v1[i++],  c2=v2!=null ? v2[j++] : (byte)anotherStr.charAt(j++);

                if (c1 != c2) return c1 - c2;
            }
        }
        return len1 - len2;
    }
    @Override public int hashCode() {
        if (_hc==0) _hc=hashCd(bytes(), begin(), end());
        return _hc;
    }
    public BA removeLeadingSpace() {
        final byte[] T=bytes();
        final int B=begin(), ends[]=eol();
        int indent=MAX_INT;
        for(int doIt=0; doIt<2; doIt++) {
            for(int iL=0; iL<ends.length; iL++) {
                final int b=iL==0 ? B : ends[iL-1]+1, e=prev(-SPC,T,ends[iL]-1,b-1)+1;
                if (e<=b) continue;
                if (doIt!=0) {
                    System.arraycopy(T,b+indent,T,b,e-b-indent);
                    for(int i=e-indent;i<e;i++) T[i]=(byte)' ';
                } else {
                    final int noSpc=strchr(STRCHR_NOT,' ',T,b,e);
                    if (indent>noSpc-b) indent=noSpc-b;
                    if (indent==0) return this;
                }
            }
        }
        return this;
    }
    /* <<< Space <<< */
    /* ---------------------------------------- */
    /* >>> Bool >>> */
    public BA boolToText(boolean bb[],int offset, String sep, String intervall) {
        if (bb==null) return this;
        boolean isFirst=true;
        for(int i=0; i<bb.length; i++) {
            if (!bb[i]) continue;
            if (isFirst)  isFirst=false; else a(sep);
            final int i0=i;
            while( i<bb.length && bb[i]) i++;
            a(i0+offset);
            if (i-i0>1) a(i-i0>2 ? intervall : sep).a(i+offset-1);
        }
        return this;
    }
    public BA boolToText(long bb,int offset, String intervall) {
        boolean isFirst=true;
        for(int i=0; i<64; i++) {
            if ( (bb & (1l<<i))==0) continue;
            if (isFirst)  isFirst=false; else a(',');
            final int i0=i;
            while( i<64 &&   (bb & (1l<<i))!=0) i++;
            a(i0+offset);
            if (i-i0>1) a(i-i0>2 ? intervall : ",").a(i+offset-1);
        }
        return this;
    }
    /* <<< Bool <<< */
    /* ---------------------------------------- */
    /* >>> Error >>> */
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_APPEND) a(arg).send();
        return null;
    }
    public final static int ERROR_PERMISSION_DENIED_BY_USER=1;
    private final static BA ERROR[]=new BA[9];
    public static BA error(int type) {
        if (ERROR[type]==null) {
            final String s=
                type==ERROR_PERMISSION_DENIED_BY_USER ? "Permission Denied by the User" : null;
            if (null==s) assrt();
            ERROR[type]=new BA(s);
        }
        return ERROR[type];
    }
    public static boolean isError(BA ba) {
        return ba!=null && idxOf(ba,ERROR)>=0;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> HasMap >>> */
    private Map _map;
    public Map map(boolean create) {
        if (create && _map==null) _map=new HashMap();
        return _map;
    }
    private final static int CACHE_TO_STRG=0, CACHE_ENDS=1;
    private Object[] cached() {
        Object oo[]=(Object[])deref(_cache);
        if (oo==null) _cache=newSoftRef(oo=new Object[5]);
        return oo;
    }
    /* <<< HasMap <<< */
    /* ---------------------------------------- */
    /* >>> Log >>> */
    private int _sendIfExeeds=MAX_INT;

    public BA sendToLogger(int option, String title, Object icon, int lenBytes) {
        if (withGui()) {
            setSendTo(new Object[]{ChLogger.class, intObjct(option), title, icon, intObjct(lenBytes)});
            _sendIfExeeds=lenBytes;
        }
        return this;
    }

    public final static String SEND_TO_STDOUT="BA$$STS", SEND_TO_NULL="BA$$ST0";
    public BA setSendTo(Object viewOrFile) {
        if (viewOrFile instanceof ChTextView && ((ChTextView)viewOrFile).byteArray()==this) {
            _send=adToArray(wref(viewOrFile), deref(_send, Object[].class), 2, Object.class);
        } else _send=viewOrFile;
        if (viewOrFile instanceof ChLogger) _sendIfExeeds=99*1000;

        if (! (viewOrFile instanceof File || viewOrFile instanceof String)) pcp(KEY_SEND_TO, wref(this), viewOrFile);

        return this;
    }
    public Object getSendTo() { return deref(_send); }

    public synchronized BA send() {
        Object v=deref(getSendTo());

        if (v instanceof Object[]) {
            final Object vv[]=(Object[])v;
            if (get(0, v)==ChLogger.class) {
                toLogMenu(v=new ChLogger(atoi(vv[1]), atoi(vv[4])), (String)vv[2], vv[3]);
                setSendTo(v);
            }
            else {
                for(int i=vv.length; --i>=0;) {
                    final ChTextView tv=getRmNull(i,vv,ChTextView.class);
                    if (tv!=null) tv.afterAppend();
                }
            }
        }
        if (length()>0) {
            if (v instanceof File) {
                appndToFile(this,(File)v);
                setEnd(0);
            } else if (v instanceof ChRunnable) {
                ((ChRunnable)v).run(ChRunnable.RUN_APPEND, this);
                setEnd(0);
            } else if (v==SEND_TO_STDOUT) {
                puts(this);
                setEnd(0);
            } else if (v==SEND_TO_NULL) setEnd(0);
            if (_tv!=null) _tv.afterAppend();
        }
        return this;
    }

    private Runnable _thread;
    public Runnable thread_send() {
        if (_thread==null) _thread=thrdM("send",this);
        return _thread;
    }
    public BA sendLater(int afterMS) {
        inEDTms(thread_send(),afterMS);
        return this;
    }

    private ChTextView _tv;
    public ChTextView textView(boolean create) {
        if (_tv==null && create)  _tv=new ChTextView(this);
        return _tv;
    }

}
