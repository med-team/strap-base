package charite.christo;
import java.awt.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class ChJMenuBar extends javax.swing.JMenuBar {

    @Override public void paintChildren(Graphics g) {
        final Color bg=getBackground();
        if (bg==C(0) || bg==C(ChConstants.DEFAULT_BACKGROUND)) {
            g.setColor(bg);
            g.fillRect(0,0,9999,99);
        }
        super.paintChildren(g);
    }

    // public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
    //     putln(YELLOW_DEBUG,ks);
    //     return super.processKeyBinding(ks,e,condition,pressed);
    // }

}
