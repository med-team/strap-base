package charite.christo;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.util.List;
import java.io.File;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class HtmlDoc implements javax.swing.event.HyperlinkListener {
    public final static Object KEY_DO_NOT_CLONE=new Object();
    private final static Object KEY_ORIGINAL_SIZE=new Object();
    private final static String
        OS="OS:",
        STRING="STRING:",HOME_PAGE="HOME_PAGE:",STRING_ARRAY="STRING_ARRAY:",TEXT_LINES="TEXT_LINES:",
        DIALOG="DIALOG:",ITEM="ITEM:", CLASS_REF="CLASS_REF:",
        INCLUDE_DOC="INCLUDE_DOC:", INCLUDE_DOC1="INCLUDE_DOC1:",
        INCLUDE_DOC2="INCLUDE_DOC2:",INCLUDE_DOC3="INCLUDE_DOC3:",INCLUDE_FILE="INCLUDE_FILE:",
        REGISTER_JAVADOC="REGISTER_JAVADOC:",JAVADOC="JAVADOC:",
        SEE_DIALOG="SEE_DIALOG:",JAVASOURCE="JAVASOURCE:",ABOUT="ABOUT:",SEE_CLASS="SEE_CLASS:",
        CONTROL="CONTROL:",CUSTOMIZE="CUSTOMIZE:",
        COMBO="COMBO:",TREENODE="TREENODE:",JCOMPONENT="JCOMPONENT:",CB="CB:",
        BUTTON="BUTTON:",LABEL="LABEL:", TEXTFIELD="TEXTFIELD:",ICON="ICON:", IMAGE="IMAGE:", CLASSICON_CLASSNAME="CLASSICON_CLASSNAME:";
    final static String
        PACKAGE="PACKAGE:",
        TAGS_FOR_EDITOR_KIT[]={TREENODE,JCOMPONENT,CLASSICON_CLASSNAME,CB,BUTTON,CONTROL,CUSTOMIZE,COMBO,LABEL,TEXTFIELD, PACKAGE,ICON, IMAGE};
    public final static String
        ID_LAST_COMPONENT="ID_LAST_COMPONENT",
        ID_BUTTON_FOR_CLASS="ID_BUTTON_FOR_CLASS",
        ACTION_VIEW_SOURCE="http://VIEW_SOURCE",
        ACTION_OPEN_DIALOG="http://OPEN_DIALOG",
        ACTION_OPEN_TUTORIAL="http://Tutorial_",
            SPECIAL_HTML_TAG[]={OS, PACKAGE,STRING_ARRAY,TEXT_LINES,STRING,HOME_PAGE,CLASSICON_CLASSNAME,REGISTER_JAVADOC,ABOUT,DIALOG,ITEM,CLASS_REF, TREENODE,
                            JCOMPONENT,INCLUDE_DOC,INCLUDE_FILE,CUSTOMIZE,
                            INCLUDE_DOC1,INCLUDE_DOC2,INCLUDE_DOC3,SEE_CLASS,SEE_DIALOG,JAVASOURCE,JAVADOC,BUTTON,COMBO,LABEL,TEXTFIELD, ICON, IMAGE,
                            CB,TREENODE,TREENODE};
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> JComponent >>> */
    static boolean jComponent(String id,HtmlDocElement f) {
        if (f==null || idxOf(id,TAGS_FOR_EDITOR_KIT)<0) return false;
        final Object fValue=f.getValue();
        final java.lang.reflect.Member member=f.getMember();
        final JComponent fValueJC=fValue instanceof JComponent ? (JComponent)fValue : null;
        final String fieldOrMethod=f.getFieldOrMethod();
        final boolean noField=sze(fieldOrMethod)==0;
        final Class clazz=f.getClazz();
        final Object likelyParameter=
            noField &&clazz!=null ?clazz :
            fValue!=null ? fValue :
            !noField ? fieldOrMethod : null;
        final String fPara=f.getParameter();
        JComponent jComponent=null;
        int isError=0;
        if (id==CONTROL) {
            final ChButton b=ChButton.doSharedCtrl(clazz);
            jComponent=b;
            b.setBorderPainted(true);
            b.setContentAreaFilled(true);
        } else if (id==CUSTOMIZE) {
            Customize cust=
                fValue instanceof Integer ? Customize.customize(atoi(fValue)) :
                fValue instanceof Customize ? (Customize)fValue :
                Customize.customize(idxOf(toStrgIntrn(fPara), finalStaticInts(Customize.class,Customize.MAX_VARIABLE)));
            if (cust==null) cust=Customize.mapInstances.get(fPara);
            if (cust!=null) jComponent=Customize.newButton(cust).i(null);
            else {
                final ChButton b=ChButton.doCustomize(clazz);
                if (b!=null) {
                    jComponent=b;
                    b.setBorderPainted(true);
                    b.setContentAreaFilled(true);
                }
            }
        } else if (id==COMBO) {
            if (fValue instanceof Object[]) jComponent=new ChCombo(ChJTable.CLASS_RENDERER, fValue);
            else if (likelyParameter!=null) {
                jComponent=new ChCombo(ChJTable.CLASS_RENDERER, new Object[]{likelyParameter,likelyParameter+"    "});
            } else isError=1;
        } else if (id==CLASSICON_CLASSNAME) {
            if (clazz!=null) jComponent=labl(delSfx("PROXY",shrtClasNam(clazz))).i(dIcon(clazz));
            else isError=2;
        } else if (id==BUTTON) {
            if (fValue instanceof ChButton) jComponent=((ChButton)fValue).cln();
            else if (fValue instanceof AbstractButton) jComponent=labl().like((AbstractButton)fValue);
            else if (fValue instanceof String) jComponent=labl((String)fValue);
            else if (fValue==null && clazz!=null) {
                if (_b4c==null) jComponent=new ChButton(shrtClasNam(clazz)+" Error: not specified HtmlDoc.setButtonForClass(...)");
                else {
                    final Object oo[]={clazz};
                    _b4c.run(ID_BUTTON_FOR_CLASS,oo);
                    jComponent=(JComponent)oo[0];
                }
            }
            else if (fValue instanceof Customize) jComponent=Customize.newButton((Customize)fValue);
            else if (fValue!=null) jComponent=labl(toStrg(fValue));
            else isError=3;
        } else if (id==CB) {
            if (fValue instanceof ChButton) jComponent= ((ChButton)fValue).cb();
            else {
                BA sb=fValue instanceof AbstractButton ?
                    toBA(rmHtmlHeadBody(new BA(999).a(getTxt(fValue)))) :
                    new BA(99).a(likelyParameter);
                if (sb!=null) {
                    for(String s:SOME_HTML_TAGS) sb=toBA(strplc(' '|STRPLC_FILL_RIGHT,s,"",sb));
                    for(String s:SOME_CLOSING_HTML_TAGS) sb=toBA(strplc(' '|STRPLC_FILL_RIGHT,s,"",sb));
                    if (sze(sb)>45) sb.replaceRange(45,MAX_INT,"...");
                    jComponent=labl( toStrg(sb.insert(0,"[X] ")));
                } else if (member==null) isError=4;
            }
        } else if (id==ICON || id==IMAGE) {
            String icon=null;
            if (fPara.startsWith("IC_")) try { icon=(String)ChConstants.class.getField(fPara).get(null); } catch(Exception e){ }
            if (icon==null) icon=toStrg(fValue);
            final Icon ic= icon!=null ? iicon(icon) : null;
            if (ic!=null) pcp(KEY_ORIGINAL_SIZE, id==IMAGE ? "":null, jComponent=new JLabel(ic));
            else if (member==null) isError=5;
        } else if (id==PACKAGE) {
            //jComponent=pnl(dim(0,0),WHITE);
            //pcp(KEY_PREF_SIZE,dim(0,0),jComponent);
        } else if (id==LABEL || id==TREENODE || id==TEXTFIELD) {
            if (fValue instanceof Icon) jComponent=new JLabel((Icon)fValue);
            else if (fValue instanceof String) jComponent=new JLabel(rmMnemon((String)fValue));
            else {
                String s= getTxt(fValueJC);
                if (s==null) s= toStrg(fValue);
                if (fValue instanceof JToggleButton || fValue instanceof ChButton && ((ChButton)fValue).isToggle() || fValue instanceof JCheckBoxMenuItem) s="[X] "+s;
                jComponent=labl().like(fValueJC).t(s).i("");
                if (id==TEXTFIELD) setBG(0xFFffFF, monospc(jComponent));
            }
        } else if (id==JCOMPONENT) {
            if (fValueJC!=null) {
                jComponent=fValueJC;
                if (gcp(KEY_DO_NOT_CLONE,jComponent)==null) {
                    final Object clone=invokeMthd("cln" ,jComponent);
                    if (clone instanceof JComponent) jComponent=(JComponent)clone;
                }
                pcp(KEY_ORIGINAL_SIZE,"",jComponent);
            } else if (member==null) isError=6;
        }
        if (!f.isActive()) {
            if (id==JCOMPONENT && jComponent!=null) {}
            else if (jComponent instanceof AbstractButton) jComponent=labl().like(jComponent);
            rmAllListnrs(true, jComponent);
        }
        if (jComponent!=null) {
            if (jComponent instanceof JLabel) jComponent.setBorder(BorderFactory.createEtchedBorder());
            if (_ccHook!=null) _ccHook.run(ID_LAST_COMPONENT,jComponent);
        }
        f.setJComponent(jComponent);
        if (isError!=0 && member==null) {
            putln(RED_ERROR+"HtmlDoc: "+isError+" "+f+"\n"+ANSI_GREEN+" member="+ANSI_RESET);
            assrt();
        }
        return jComponent!=null;
    }
    private static HtmlDocElement[] specialExpressions(String id,BA text,String[] myPackages,String documentId) {
        final List v=new ArrayList();
        final String key="<i>"+id;
        int pos=-1;
        while( (pos=strstr(key, text, pos+1, MAX_INT))>0) {
            if(pos<0) break;
            final int to=strstr("</i>",text,pos,MAX_INT);
            v.add( new HtmlDocElement( id, text.newString(pos+sze(key),to), new Range(pos,to+4), myPackages, documentId) );
        }
        return toArryClr(v, HtmlDocElement.class);
    }
    /* <<< JComponent <<< */
    /* ---------------------------------------- */
    /* >>> Settings >>> */
    private static ChRunnable _ccHook, _b4c;
    public static void setCreateComponentHook(ChRunnable r) {_ccHook=r;}
    public static void setButtonForClass(ChRunnable b4c) {_b4c=b4c;}
    /* <<< Settings <<< */
    /* ---------------------------------------- */
    /* >>> Log >>> */
    private static BA _log;
    public static BA log() {
        if (_log==null) _log=new BA(999);
        return _log;
    }
    /* <<< Log <<< */
    /* ---------------------------------------- */
    /* >>> Reflection >>> */
    static String[] getImportedPackages(final CharSequence txt, Object clazz) {
        Collection v=null;
        for (int i=-1;  (i=strstr(STRSTR_AFTER,"<i>"+PACKAGE,txt,i,MAX_INT))>=0;) {
            final int iClose=strstr("</i>",txt, i, MAX_INT);
            if (iClose<0) { assrt(); continue; }
            v=adUniqNew(txt.subSequence(i,iClose).toString(), v);
        }
        final String sClass=clazz instanceof String ? (String)clazz : clazz instanceof Class ? nam(clazz) : null;
        final int dot=sClass!=null ? sClass.lastIndexOf('.') : -1;
        if (dot>0)  (v==null ? v=new ArrayList() : v).add(sClass.substring(0,dot+1));
        return strgArry(v);
    }
    private static BA relatedDialogsAndClasses(Map<Class,String> map, boolean seeDialogs) {
        final BA SB=new BA(999);
        final Map.Entry[] ee=entryArry(map);
        if (ee.length==0) return SB;
        SB.a("<h3>Related ").a(seeDialogs ? "" :"Classes").aln("</h3><ul>");
        for(Map.Entry<Class,String> e : ee) {
            final Class c=e.getKey();
            final String cn=nam(c), parameter=e.getValue();
            SB.aln("<li><b>");
            if (seeDialogs) {
                SB.a(dTitle(c)).a("</b>: ");
                if (!tellContextMenu(parameter,SB)) SB.a(" &nbsp; ").a("<a href=\"").a(ACTION_OPEN_DIALOG).a(cn).aln("\">Open dialog</a>");
            } else {
                SB.a(shrtClasNam(c)).a("</b>: ").a(dTitle(c));
                if (isSelctd(BUTTN[TOG_JAVA_SRC])) SB.and("<a href=\"",ChJavadoc.javadocUrl(cn),"\">Javadoc</a> - \n");
            }
            if (hasHlp(c)) SB.a(" &nbsp; ").a("<a href=\"").a(cn).aln("\">Documentation</a>");
            SB.aln("</li>");
        }
        return SB.aln("</ul>");
    }
    private static boolean tellContextMenu(String parameter, BA sb) {
        final int comma=strchr(',',parameter);
        for(int i=3; comma>0 && --i>=0;) {
            final char m=i==0?'A':i==1?'P':i==2?'H':0;
            if (strchr(m,parameter,comma,MAX_INT)<0) continue;
            if (m=='H') sb.a("[Help menu] ");
            else sb.a(" [Context menu of ").a(m=='A' ? "residue annotations" : m=='P' ? "proteins" : null).a("] ");
            return true;
        }
        return false;
    }
    /* <<< Reflection <<< */
    /* ---------------------------------------- */
    /* >>> Menu >>> */
    static BA insertHeadline(String className,char level, BA sb,char docType) {
        if (sb==null || className==null) return null;
        final String headLine=dTitle(className);
        if (headLine==null) { sb.insert(0,"<div></div>");return sb;} /* Now it starts With an html tag */
        final BA SB=new BA("<a name=\""+delToLstChr(className,'.')+"\"></a><a name=\""+className+"\"></a>\n<h"+level+" class=\"withCounter\" >"+headLine+"</h"+level+">\n\n");
        final String menuPath[]=getMenuPaths(dTitle(className),null);
        if (menuPath.length>0) SB.a('[').join(menuPath,"&gt;").a("] ");
        final String hl=SB.toString();
        if (strstr(hl,sb)<0) sb.insert(maxi(strstr(STRSTR_IC,"<body>",sb),0) ,hl);
        return sb;
    }
    /* <<< Menu <<< */
    /* ---------------------------------------- */
    /* >>> Image >>> */
    private static boolean _renewI=true;
    private static String mayBeImageTag(HtmlDocElement sf) {
        if (sf.getJComponent()==null) return null;
        final String
            cssStyle=sf.getStyle()!=null ? " style=\""+sf.getStyle()+"\" " : gcp(KEY_ORIGINAL_SIZE,sf.getJComponent())!=null ? " style=\"vertical-align: bottom;\" " : " class=\"smallImage\"",
            alt=sf.getParameter()!=null ? sf.getParameter().replace('"',' ') : null,
            img="<img "+cssStyle+"  alt=\""+alt+"\"  src=\""+sf.urlImageFile()+"\">";
        final File imageFile=sf.imageFile();
        if (imageFile!=null && (sze(imageFile)==0  || _renewI && (System.currentTimeMillis()-imageFile.lastModified())>1000*60)) {
            writePng(PNG_NOT_NOW, sf.getJComponent(), imageFile);
        }
        return img;
    }
    /* <<< Image <<< */
    /* ---------------------------------------- */
    /* >>> Html >>> */

    public static BA replaceCrossRefs() {
        final BA sb=new BA(3333).aln("\n function replaceCrossRefs(s) {");
        final String[] ss[]=Hyperrefs.getDatabases(Hyperrefs.WEB_LINK|Hyperrefs.PLAIN_TEXT), kk=ss[0], uu=ss[1];
        for(int i=0; i<kk.length; i++) {
            if (kk[i]!=null && uu[i].indexOf('*')<0) sb.a("s=s.replace('").a(kk[i]).a("','").a(uu[i]).aln("');");
        }
        return sb.a("return s;\n}");
    }
    private final static String CSS[]=new String[2];
    public static String cssOrJS(char css_or_js) {
        final boolean isCss=(css_or_js|32)=='c', isJs=(css_or_js|32)=='j', addTags=css_or_js!=(css_or_js|32);
        if (!isCss && !isJs) assrt();
        final int idx=( isCss  ? 0:1);
        if (CSS[idx]==null) {
            final BA ba=readBytes(ChUtils.class.getResourceAsStream(isCss?"strap.css" : "HtmlDoc.js"));
            if (ba!=null) {
                if (isJs) ba.a(replaceCrossRefs());
                if (addTags) {
                    ba.insert(0, isCss ? "<style type=\"text/css\">\n" : "\n<script language=\"JavaScript\" type=\"text/javascript\">\n");
                    ba.aln(isCss ? "\n</style>\n" : "\n</script>\n<span id=\"toolTipLayer\"></span>");
                }
                CSS[idx]=ba.toString();
            } else { CSS[idx]=""; assrt(); }
        }
        return CSS[idx];
    }
    /* <<< Html <<< */
    /* ---------------------------------------- */
    /* >>>  >>> */
    static void process(BA sb,String documentId,char docType, String[] packages) {
        recursiveInclude(sb,documentId,docType,packages);
        stringTags(sb,documentId, docType, packages);
        if (docType==DOCTYPE_TUTORIAL) sb.replace(0L,"<CAPTION>","<CAPTION><BR>");
        if (docType!=DOCTYPE_HTML && strstr("arr;",sb)>0) {
            sb
                .replace(0, "&larr;", "&#8592;")
                .replace(0, "&uarr;", "&#8593;")
                .replace(0, "&rarr;", "&#8594;")
                .replace(0, "&darr;", "&#8595;");
        }
    }
    private static void stringTags(final BA sb,String documentId,char docType, String packages[]) {
        final BA SB=new BA(999), log=log();
        final Map<Class,String> vSeeClass=new HashMap(), vSeeDialog=new HashMap();
        for(String id: SPECIAL_HTML_TAG) {
            final HtmlDocElement vSpEl[]=specialExpressions(id,sb,packages,documentId);
            int isError=0;
            for(int iSpEl=sze(vSpEl);--iSpEl>=0;) {
                final HtmlDocElement f=vSpEl[iSpEl];
                boolean ok=false;
                SB.clr();
                final Class clazz=f.getClazz();
                final String parameter=f.getParameter();
                final String fieldOrMethod=f.getFieldOrMethod();
                final boolean noField=sze(fieldOrMethod)==0;
                if (id==STRING) {
                    final Object v=f.getValue();
                    SB.a(docType==DOCTYPE_HTML && v instanceof File ? fPathUnix((File)v) : v);
                } else if (id==SEE_CLASS) {
                    if (clazz!=null) {  ok=true;  vSeeClass.put(clazz,parameter);  } else isError=11;
                } else if (id==SEE_DIALOG) {
                    if (clazz!=null) {
                        ok=true;
                        vSeeDialog.put(clazz,parameter);
                    } else isError=12;
                } else if (id==INCLUDE_DOC || id==INCLUDE_DOC1|| id==INCLUDE_DOC2|| id==INCLUDE_DOC3 || id==PACKAGE) {
                    ok=true;
                } else if (id==JAVADOC) {
                    if (clazz!=null) {
                        final java.lang.reflect.Member member=ChJavadoc.findMember(clazz,fieldOrMethod, packages, (Object[])null);
                        final Object url=member!=null ? ChJavadoc.javadocUrl(member) : ChJavadoc.javadocUrl(nam(clazz));
                        SB.a("<a href=\"").a(url).a("\">").a(parameter).aln("</a>");
                    } else isError=13;
                } else if (id==OS) {
                    final int comma=parameter.indexOf(',');
                    final boolean w=isSystProprty(IS_WINDOWS), m=isSystProprty(IS_MAC), l=isSystProprty(IS_LINUX);
                    for(int i=comma;--i>=0;) {
                        final char c=parameter.charAt(i);
                        if (c=='M' && m || c=='W' && w || c=='L' && l || c=='O' && !w && !m && !l) {
                            SB.a(parameter, comma+1, MAX_INT);
                            break;
                        }
                        if ("MWLO".indexOf(c)<0) isError=14;
                    }
                    ok=true;
                } else if (id==HOME_PAGE && clazz!=null) {
                    SB.a(dHomePage(clazz));
                } else if (id==JAVASOURCE) {
                    SB.aln("\n<pre>");
                    if (clazz!=null) SB.aln(ChCodeViewer.highlightSyntax(ChCodeViewer.HTML, getJavaSrc(nam(clazz)),NO_STRING));
                    else SB.a(ChCodeViewer.highlightSyntax(ChCodeViewer.HTML, new BA(99).a(' ').a(parameter).a(' '),packages));
                    SB.aln("</pre>");
                }
                else if (id==ABOUT) {
                    SB.a("<b>About this document:</b> This text was processed with the tool ");
                    ChJavadoc.javadocLink(nam(HtmlDoc.class),SB);
                    SB.a('\n');
                } else if (id==REGISTER_JAVADOC) {
                    ok=true;
                    final String[] ss=splitTokns(parameter);
                    if (sze(ss)==2) {
                        if (url(ss[1])!=null) ChJavadoc.addJavadocUrl(ss[0],ss[1]);
                        else { log.a(RED_ERROR).a(parameter).a(ANSI_BLUE+" invalid url ").aln(ss[1]).a("\n"+ANSI_RESET); isError=16;}
                    } else {
                        log.a(RED_ERROR).a(ANSI_BLUE).aln(f).aln(" syntax: <i>"+ANSI_RESET+REGISTER_JAVADOC+"java.lang. http://docs.oracle.com/javase/1.5.0/docs/api/</i>\n\n"+ANSI_RESET);
                        isError=17;
                    }
                } else if (id==ITEM || id==DIALOG || id==CLASS_REF) {
                    final String item=noField ? rmMnemon(dItem(clazz)) : null;
                    String txt= id==CLASS_REF ? " <b>*</b> " : 	item;
                    if (txt==null && id==ITEM) {
                        final Object v=f.getValue();
                        txt=v instanceof CharSequence ? v.toString() : getTxt(v);
                    }
                    if (txt!=null) {
                        if (clazz!=null) {
                            final CharSequence ref=docType==DOCTYPE_JAVADOC ? ChJavadoc.javadocUrl(nam(clazz)) : getHlp(clazz)!=null ? nam(clazz)+".html" : null;
                            if (ref!=null) SB.a("<a href=\"").a(ref).a("\"><b><I>").a(txt).a("</I></b></a>");
                        } else isError=21; //SB.a("<i>LABEL:"+parameter+"</i>");
                        if (id==ITEM) {
                            final String ss[]=getMenuPaths(txt,null);
                            if (sze(ss)>0) SB.a('[').join(ss,"&gt;").a("] ");
                        }
                        if (id==ITEM || id==DIALOG) tellContextMenu(parameter,SB);
                    }
                } else if (id==STRING_ARRAY) {
                    int count=0;
                    for(Object o : oo(f.getValue())) {
                        if (count++>0) SB.a(',');
                        SB.a(o);
                    }
                } else if (id==TEXT_LINES) {
                    final Object v=f.getValue();
                    if (v instanceof Object[] || v instanceof List) SB.join(v);
                    else SB.a(v);
                } else if (id==INCLUDE_FILE) {
                    SB.a(readBytes(file(parameter)));
                } else if (docType!=DOCTYPE_TUTORIAL) {
                    if (jComponent(id,f)) {
                        final String img=mayBeImageTag(f);
                        if (img!=null) SB.a(mayBeImageTag(f));
                        else log.a(ANSI_RED+"\nNo png for "+ANSI_BLUE).a(documentId).a(' ').a(id).a(ANSI_RESET).aln(f);
                    }
                }
                if (sze(SB)>0 || ok) {
                    final Range range=f.getRange();
                    final String toBeREplaced=sb.newString(range.from(),range.to());
                    if (chrAt(0,toBeREplaced)!='<' || !toBeREplaced.endsWith(">")) log.a(RED_ERROR+": not ending with > Or not  starting with  < ").aln(toBeREplaced).a(ANSI_RESET);
                    sb.replaceRange(range.from(),range.to(),SB.toString());
                } else if (idxOf(id,TAGS_FOR_EDITOR_KIT)<0 && id!=PACKAGE) {
                    log.a("\n"+RED_ERROR+" Nothing todo for "+ANSI_CYAN+"docid=").a(documentId).a(ANSI_RESET+"  id=").aln(id).aln(f).a('\n');
                }
                if (isError!=0) {
                    putln(RED_ERROR+"HtmlDoc: "+isError+" "+documentId+"\n");
                    putln(f);
                    assrt();
                }
            }
            log.send();
        }
        if (docType==DOCTYPE_TUTORIAL) {
            final BA related=relatedDialogsAndClasses(vSeeClass,false).a(relatedDialogsAndClasses(vSeeDialog,true));

            int idxBody=strstr("<!-- ======== NESTED CLASS SUMMARY",sb);
            if (idxBody<0) idxBody=strstr(STRSTR_IC, "</body>", sb);
            if (idxBody<0) sb.a(related); else sb.insert(idxBody,(Object)related);
        }
    }
    private static void recursiveInclude(final BA sb,final String documentId,final char docType, String[] packages) {
        for(String id: SPECIAL_HTML_TAG) {
            final HtmlDocElement[] vSpEl=specialExpressions(id,sb,packages,documentId);
            for(int iSpEl=sze(vSpEl);--iSpEl>=0;) {
                final HtmlDocElement f=vSpEl[iSpEl];
                final Class clazz=f.getClazz();
                if (id==INCLUDE_DOC || id==INCLUDE_DOC1|| id==INCLUDE_DOC2|| id==INCLUDE_DOC3) {
                    if (clazz!=null) {
                        final BA include=new BA(getHlp(clazz));
                        if (include!=null) {
                            final char level=id==INCLUDE_DOC1 ? '1' : id==INCLUDE_DOC2 ? '2' : id==INCLUDE_DOC3 ? '3' : 0;
                            if (level!=0) insertHeadline(nam(clazz),level,include,docType);
                            final String title=clazz==HtmlDocElement.class ? "Manual" : shrtClasNam(clazz);
                            recursiveInclude(include, title, docType,packages);
                            sb.replaceRange(f.getRange().from(),f.getRange().to(),include.toString());
                        }
                    }
                }
            }
        }
    }
    static BA rplcWIKI(BA sb) {
        final boolean[] chrClassEndUrl=chrClas(" )<>;\n");
        for(int i=sb.length()-2; --i>=0; ) {
            final char c0=sb.charAt(i);
            if (c0=='W' || c0=='P' || c0=='h' || c0=='f' || c0=='<' || c0=='*') {
                final char b1=chrAt(i-1,sb);
                if (c0=='*' && is(LETTR_DIGT,b1) && sb.charAt(i+1)!='\'') { /* MolEvolFormats*, MOVIE:Drag_to_another_STRAP*  aper nicht s=s.replace('MolEvolFormats*' */
                    final int start=prev(-LETTR_DIGT_US_COLON,sb,i-1,-1);
                    final String word=i-start<3 ? null : sb.newString(start+1, i+1), url=Hyperrefs.toUrlString(word, Hyperrefs.WEB_LINK);
                    if (url!=null) {
                        final BA txt=new BA(222).a("<a href=\"javascript:shw('").a(url).a("');\">");
                        for(String s : splitTokns(delSfx('*',delPfx("MOVIE:",word)), chrClas1('_'))) txt.a(capitalize(s)).a(' ');
                        sb.replaceRange(start+1,i+1,txt.a("</a>"));
                        i-=word.length();
                    }
                }
                if (b1!='"' &&  b1!='\'' && !is(LETTR_DIGT_US,b1)) {
                    if (strEquls("WIKI:",sb,i)) {
                        int end=nxtE(-LETTR_DIGT_US,sb,i+5,i+20);
                        if (chrAt(end,sb)=='(') end=strchr(')',sb,end, MAX_INT)+1;
                        if (end>0) {
                            final String term=sb.newString(i+5,end);
                            sb.replaceRange(i,end,"<script>WIKI('"+term+"')</script>");
                        } else assrt();
                    } else if (strEquls("PUBMED:",sb,i)) {
                        final int end=nxt(-DIGT,sb,i+7,i+20);
                        if (end>i+7) {
                            final String term=sb.newString(i+7,end);
                            sb.replaceRange(i,end,"<script>pubmed('"+term+"')</script>"+term);
                        }
                    } else if (looks(LIKE_EXTURL,sb,i)) {
                        int end=nxt(chrClassEndUrl,sb,i+5,i+100);
                        if (end>0) {
                            if (sb.charAt(end-1)=='.') end--;
                            final String url=sb.newString(i,end);
                            sb.replaceRange(i,end,"<a href=\"javascript:shw('"+url+"');\">"+url+"</a>");
                        }
                    }
                    if (strEquls("<i>"+PACKAGE,sb,i)) {
                        final int end=strstr("</i>",sb, i, MAX_INT);
                        if (end>0) sb.setCharsAt(i, end+4, (byte)' '); else assrt();
                    }
                }
            }
        }
        return sb;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Dimension >>> */
    public static Dimension prefSize(Component c) {
        if (c==null) return dim(0,0);
        Dimension d=deref(gcpa(KEY_PREF_SIZE,c), Dimension.class);
        if (d==null)  d=c.getPreferredSize();
        if (gcp(KEY_ORIGINAL_SIZE,c)!=null) return d;
        if (c instanceof AbstractButton || c instanceof JLabel) {
            final Icon ic=getIcn(c);
            if (strstr("<br>",getTxt(c))<0) {
                d=dim(d.width, (ic==null || ic==iicon(IC_BLANK))  ? 16 : hght(ic));
            }
        }
        if (c instanceof JComboBox) d=dim(d.width,20);
        return d;
    }
    /* <<< Dimension <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    private HtmlDoc(){}
    private static HtmlDoc _inst;
    public static HtmlDoc instance() { return _inst==null ?  _inst=new HtmlDoc() : _inst;}

    @Override public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent ev) {
        if (ev.getEventType()==javax.swing.event.HyperlinkEvent.EventType.ACTIVATED) {
            final String sUrl=ev.getDescription();
            if (sUrl==null) return;
            if (strEquls(ACTION_OPEN_DIALOG,sUrl) || strEquls(ACTION_OPEN_TUTORIAL,sUrl)) handleActEvt(this,sUrl,0);
            else if (strEquls(ACTION_VIEW_SOURCE, sUrl)) showJavaSource(delPfx(ACTION_VIEW_SOURCE,sUrl), STRAP_PACKAGES);
            else {
                final Class c=findClas(delSfx(".html", delPfx(URL_STRAP_DOC,sUrl)), STRAP_PACKAGES);
                if (c!=null) showHelp(c);
                else visitURL(ev.getURL(),0);
            }
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> main >>> */
    // java charite.christo.HtmlDoc -log charite.christo.strap.Texshade
    public static void main(String... argv) {
        boolean exit=true;
        if (sze(TabItemTipIcon.MAP)==0) {
            TabItemTipIcon.ad(_CC,"tabItemTipIcon1.rsc");
            TabItemTipIcon.ad(_CC,"tabItemTipIcon2.rsc");
        }

        boolean view=false;
        for(String arg: argv) {
            arg=arg.intern();
            if (""==arg) continue;
            if ("-v"==arg) { exit=false; view=true; }
            else if ("-log"==arg) shwTxtInW(ChFrame.CENTER|CLOSE_CtrlW, null, log());
            else if (arg=="-h" || arg=="-help" || arg=="--help") { showHelp(HtmlDoc.class); return; }
            else if ("-noExit"==arg) exit=false;
            else if (chrAt(0,arg)=='-') { putln(RED_ERROR+"HtmlDoc: ","Unrecognized option: ",arg); continue;}
        }
        if (myComputer()) _renewI=false;
        if (argv.length==0) showHelp(HtmlDoc.class);
        for(String arg: argv) {
            if (arg!=null && chrAt(0,arg)!='-') {
                if (view && name2class(arg)!=null) showHelp(arg);
                else main1(arg);
            }
        }
        if (exit) {
            putln("HtmlDoc.exit");
            inEDTms(thrdRRR(threadWritePng(), !exit?null: thrdM("exit", System.class, new Object[]{intObjct(0)})), 999);
        }

    }
    static File main1(String arg) {
        if (arg==null || arg.indexOf('$')>0) return null;
        if (arg.endsWith(".class")) arg=delSfx(".class",delPfx("./",arg)).replace('/','.');
        final File fArg=file(arg);
        final Class c=name2class(arg);
        BA txt=null;
        File fOut=null;
        if (sze(fArg)>0) {
            if (arg.endsWith(".html") || arg.endsWith(".HTML") || arg.endsWith(".htm")) {
                txt=readBytes(fArg);
                final String fn=nam(fArg);
                process(txt,delDotSfx(fn),DOCTYPE_HTML, getImportedPackages(txt,(Class)null));
                fOut=file(dirDocumentation()+"/"+fn);
            }
        } else if (c!=null) {
            txt=new BA(getHlp(c));
            if (txt==null) error("HtmlDoc: no help for "+c);
            else {
                insertHeadline(nam(c),'2', txt,DOCTYPE_HTML);
                process(txt,shrtClasNam(c),DOCTYPE_HTML,getImportedPackages(txt,c));
                fOut=file(dirDocumentation(),nam(c)+".html");
            }
        }
        if (fOut!=null && txt!=null) {
            rplcWIKI(txt);
            if (!looks(LIKE_HTML,txt)) {
                txt.insert(0,"<!DOCTYPE HTML>\n<!-- saved from url=(0023)http://3d-alignment.eu/ -->\n<html><head>\n"+
                           cssOrJS('C')+"<title>"+delSfx(".html",nam(fOut))+"</title></head>\n<body>\n"/*+cssOrJS('J')*/)
                    .a("</body></html>");
            }
            putln("HtmlDoc: Written: "+fOut+" "+sze(txt)+" bytes");
            mkParentDrs(fOut);
            wrte(fOut,txt);
        }

        return fOut;
    }
}
