package charite.christo;
import static charite.christo.ChUtils.*;
import java.io.File;

/**
   J is a fast text editor written in Java and distributed under the GNU General Public License.
   It supports syntax highlighting for Java, C, LISP and HTML.
   It has a built-in LISP insterpreter.
   The author is Peter Graves <peter@armedbear.org.
   A main contributor is  Mikol Graves <mikol@thinbox.org>.
   Home page: http://armedbear-j.sourceforge.net/.
   @author Christoph Gille
*/
public final class J_JavaEditor implements FileEditor  {

    public boolean editFile(File f,int lineNumber){
        final String JAR="j-0.21.0_textEditor.jar";
        {
            final File fSrc=file(dirJars()+"/j-0.21.0_sourceCode.sh");
            if (sze(fSrc)==0) wrte(fSrc,"wget "+ChConstants.URL_STRAP_JARS+"j-0.21.0_src_textEditor.jar");
        }
        final File fJar=file(dirJars()+"/"+JAR);
        InteractiveDownload.downloadFiles(Web.urls(url(ChConstants.URL_STRAP_JARS),JAR),new File[]{fJar});
        if (sze(fJar)>0) {
            startThrd(new ChExec(0).setCommandLineV ( file(systProprty(SYSP_JAVA_HOME)+"/bin/java"),"-jar", fJar,f));
            return true;
        }
        return false;
    }
}
