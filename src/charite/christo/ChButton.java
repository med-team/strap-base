package charite.christo;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.io.File;
import java.net.URL;
import static java.awt.event.MouseEvent.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class ChButton extends JButton implements HasRenderer,ChRunnable, HasMC, ProcessEv {
    { if (!withGui()) assrt(); }
    public final static long CROSSING_OUT=1<<1, NO_FILL=1<<3, NO_BORDER=1<<4,ICON_SIZE=1<<6,
        NOT_OPAQUE=1<<7, PAINT_IN_TABLE=1<<8, DISABLED=1<<9, HEIGHT_I=1<<10, UNDOCKABLE=1<<11, HIDE_IF_DISABLED=1<<12,
        MAC_TYPE_ICON=1<<13, PAINT_IF_ENABLED=1<<14, ON_PRESS_GC=1<<16, PAINT_DIRECT=1<<18, ROLLOVER=1<<19, RENDERER_SELF=1<<20;
    public final static String
        FIRST_REVIOUS_NEXT_LAST="|< < > >|",
        BUTTON_GO="<html><body><font color=#007c00><b> GO </b></font></body></html>",
        RUN_PUSHED="CB$$RP",
        GO="CB$$1",
        KEY_COLOR_BOX_L="CB$$KCBl", KEY_COLOR_BOX_R="CB$$KCBr",
        KEY_MENU_ITEMS="CB$$mi",
        KEY_CLASS="CB$$kc",
        KEY_VIEW_MSG="CB$$M",
        KEY_DO_CLICK="CB$$DC",
        KEY_SHARED_CTRL="CB$$SC",
        KEY_JTabbedPane="CB$$KJTP",
        PFX_VERTICAL="CR$$V",
        KEY_DO_UNSEL="CR$$US";
    private boolean _contains, _handleEvt=true, _tabSelected, _superPaint;
    final char _typ;
    private int _modi, _height=-1, _mc, _fromTo[], _mnemonic;
    private long _opt;
    private final static Map _mapTV=new WeakHashMap();
    private final static Object
        KEY_DIA_STRG_MATCH=new Object(),
        KEY_WEB_SETTINGS=new Object(),
        DO_HIDE_PARENT_WINDOW=new Object(),
        DO_REPAINT=new Object(),
        KEY_EXECUTE_CUST=new Object(),KEY_EXECUTE_ARG=new Object(), KEY_VIEW_FILE=new Object(),
        KEY_EDIT_FILE=new Object(),KEY_PRINTABLE=new Object(),KEY_ChTabPane=new Object(),
        KEY_CTRL=new Object(), KEY_CUSTOMIZE=new Object(),
        KEY_HELP_OBJECTS=new Object(), KEY_HELP_SUFFIX=new Object(),
        KEY_pack=new Object(),
        KEY_OPEN_URL=new Object(),
        KEY_CLOSE=new Object(),
        KEY_ENABLE=new Object(),
        KEY_COLLAPSE[]={new Object(),new Object(), new Object()};
    final Map MAPCP=new java.util.HashMap();
    final Object WEAK_REF=newWeakRef(this);
    public ChButton cp(Object key, Object value) { pcp(key,value,this); return this;}
    // ----------------------------------------
    /* >>> Constructor >>> */

    public long getOptions() { return _opt;}
    public ChButton setOptions(long o) {
        if (0!=(o&RENDERER_SELF)) cp(ChRenderer.KEY_RENDERER_COMPONENT,wref(this));
        if (0!=(o&NO_FILL)) setContentAreaFilled(false);
        if (0!=(o&NO_BORDER)) setBorderPainted(false);
        if (0!=(o&HIDE_IF_DISABLED)) cp(KOPT_HIDE_IF_DISABLED,"");
        if (0!=(o&DISABLED)) setEnabld(false,this);
        if (0!=(o&UNDOCKABLE)) setUndockble(this);
        if (0!=(o&MAC_TYPE_ICON)) putClientProperty("JButton.buttonType","icon");
        if (0!=(o&ICON_SIZE)) {
            cp(KEY_PREF_SIZE,dim(ICON_HEIGHT,ICON_HEIGHT));
            setMargin(new Insets(0,0,0,0));
        }
        if (0!=(o&(NOT_OPAQUE|PAINT_IF_ENABLED|NO_FILL|PAINT_IF_ENABLED))) setOpaque(false);
        if (0!=(o&PAINT_IN_TABLE)) setRequestFocusEnabled(false);
        if (0!=(o&HEIGHT_I)) _height=ICON_HEIGHT;
        _opt=o;
        return this;
    }
    public ChButton(AbstractButton b) {
        this(b instanceof ChButton ? ((ChButton)b)._typ:'B',b.getText());
        setModel(b.getModel());
        cp(KEY_CLONED_FROM,b);
        if (myComputer() && !isEDT()) puts(ANSI_RED+" !EDT "+ANSI_RESET);
    }
    public ChButton() { this('B',null);}
    public ChButton(String s) { this('B',s); }
    public ChButton(long opt, String s) {
        this('B',s);
        setOptions(opt);
    }
    private final static ButtonModel _sharedModel=new DefaultButtonModel();
    public ChButton(char typ, String s) {
        super();
        if (!isEDT() && myComputer()) stckTrc();
        _typ=typ;
        if (s!=null) t(s);
        setAlignmentX(LEFT_ALIGNMENT);
        setAlignmentY(CENTER_ALIGNMENT);
        setDefaultCapable(false);
        if (typ=='C' || typ=='T') setModel(new JToggleButton.ToggleButtonModel());
        if (typ=='C') {
            setBorderPainted(false);
            setHorizontalAlignment(LEADING);
        }
        if (typ=='L')  {
            setModel(_sharedModel);
            setHorizontalAlignment(LEADING);
            setOpaque(false);
            setContentAreaFilled(false);
            //setBorderPainted(false);
            noBrdr(this);
        } else {
            cp(KEY_ACTION_COMMAND,s);
            setActionCommand(s);
            LI.addTo("a",getModel());
        }
        if (isMacLaf()) {
            setOpaque(false);
        }
    }
    /* <<< Constructor <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    private void cBox(Graphics g) {
        for(int left=2; --left>=0;) {
            final Color c=colr(gcp(left!=0 ? KEY_COLOR_BOX_L : KEY_COLOR_BOX_R,this));
            if (c!=null) {
                g.setColor(c);
                g.fillRect(left!=0?0:getWidth()-4, (getHeight()-8)/2, 4,8);
            }
        }
    }
    @Override public void paintComponent(Graphics g) {
        {
            ChButton cl=this;
            while(_oIcon==null &&  ((cl=gcp(KEY_CLONED_FROM,cl,ChButton.class))!=null)) _oIcon=cl._oIcon;
            _oIcon=ChButton.mkIcon(_oIcon, this);
        }
        if (g==null) return; /* See ChJMenu.paintComponent(null); */
        final long opt=_opt;
        final int w=getWidth(), h=getHeight();

        if (smallCollapse()) {
            g.setColor(C(0xFFffFF));
            g.fillRect(0,0,h,h);
            g.setColor(C(0));
            g.drawRect(0,0,h-1,h-1);
            g.drawLine(3,h/2,h-4,h/2);
            if (!isSelected()) g.drawLine(h/2,3,h/2, h-4);
            return;
        }

        final boolean isEnabled=isEnabled();
        if (!isEnabled && (
                           null!=gcp(KEY_HELP_OBJECTS,this) ||
                           null!=gcp(KEY_CUSTOMIZE,this) ||
                           null!=gcp(KEY_SHARED_CTRL,this) ||
                           0!=(opt&PAINT_IF_ENABLED))) return;
        antiAliasing(g);
        if (paintHooks(this, g, false)) {
            if (getModel().isEnabled()!=isEnabled()) super.setEnabled(isEnabled);
            if (isMacLaf()) setOpaque(false);
            if (!isOpaque()) cBox(g);
            final String txt=getText();
            final boolean
                vertical=strEquls(PFX_VERTICAL, txt),
                isTableHeader=gcp(KOPT_IS_IN_TABLE_HEADER,this)!=null;
            if (_typ=='L') {
                final boolean  isSelected=_fromTo==null && null!=gcp(KOPT_IS_SELECTED,this);
                final Color
                    fg=isSelected ? C(FG_SELECTED) : null,
                    gcpBG=gcp(KEY_BACKGROUND, this, Color.class),
                    bg=
                    isSelected  ? bgSelected() :
                    isTableHeader ? C(DEFAULT_BACKGROUND) :
                    gcpBG!=null?gcpBG :
                    !isOpaque() ?  null :
                    gcp(ChRenderer.KEY_NOT_EDITABLE_BG,this, Color.class);
                if (bg!=null) { g.setColor(bg);  g.fillRect(0,0, w,h); }
                if (fg!=null) setForeground(fg);
                if (_fromTo!=null) {
                    g.setColor(fg!=null?fg:C(0x808080));
                    g.drawRect(0,0,w-1,h-1);
                    g.fillRect(2+_fromTo[0]*(w-4)/100,h/4,   (_fromTo[1]-_fromTo[0])*(w-4)/100, h/2);
                }
            }
            final Font font=getFont();
            if (vertical) {
                g.setFont(font);
                g.setColor(getForeground());
                ((Graphics2D)g).rotate(-Math.PI/2, h/2f,h/2f);
                g.drawString(delPfx(PFX_VERTICAL,txt), 2, charA(font));
                ((Graphics2D)g).rotate(Math.PI/2, h/2f,h/2f);
            } else if (_fromTo==null) {
                _superPaint=true;
                try { super.paintComponent(g); } catch(Throwable e) { }
                _superPaint=false;
                if (0!=(_opt&PAINT_DIRECT) && sze(txt)>0) {
                    g.setColor(getForeground());
                    g.setFont(font);
                    final Insets margin=getMargin();
                    final int restX=w-strgWidth(font,txt)-x(margin)-margin.right;
                    final int restY=h-charH(font)-y(margin)-margin.bottom;
                    g.drawString(txt, x(margin)+restX/2, y(margin)+charA(this)+restY/2);
                }
            }
            if (isTableHeader) {
                g.setColor(C(0));
                g.drawLine(w-1,0, w-1,h);
                g.drawLine(0,h-1, w,h-1);
            }
            if (isOpaque()) cBox(g);
            if (0!=(_opt&ROLLOVER) && _contains) {
                g.setColor(C(0xFF));
                g.drawRect(0,0,w-1,h-1);
            }
            if (0!=(opt&CROSSING_OUT)) {
                g.setColor(C(0xFF0000));
                final int d=h/3;
                g.drawLine(0,d,w,h-d);
                g.drawLine(0,h-d,w,d);
            }
        }
        paintHooks(this, g, true);
    }
    public ChButton rover(String icon ) {
        setOptions(ICON_SIZE|NO_FILL|NO_BORDER|ROLLOVER);
        return i(icon);
    }
    /* ---------------------------------------- */
    /* >>> Icon >>> */
    private Object _oIcon;
    public final ChButton i(Object iconOrString) {
        final Object o=isToggle() && iconOrString instanceof String ?   new Object[]{iconOrString,iconOrString+ChIcon.SFX_PRESSED} : iconOrString;
        _oIcon=o;
        setIcn(o instanceof String || o instanceof String[] && get(0,o) instanceof String ? iicon(IC_BLANK) : deref(o,Icon.class), this);
        final ChButton[] bb=derefArray(gcp(KEY_CLONES,this), ChButton.class);
        if (bb!=null) for(ChButton b: bb) b.i(o);
        return this;
    }
    static Object mkIcon(Object object, final JComponent c) {
        Object o=object, oo[]=o instanceof Object[] ? (Object[])o:null;
        for(int i=sze(oo); --i>=0;) {
            if (oo[i] instanceof String) oo[i]=iicon((String)oo[i]);
        }
        if (o instanceof String && o!=ERROR_STRING) o=iicon((String)o);
        if (!isSelctd(buttn(TOG_MENUICONS)) || o==ERROR_STRING) return null;
        if (o==null) o=ERROR_STRING;
        final Icon ic=deref(oo!=null ? oo[isSelctd(c)?1:0] : o, Icon.class);
        if (ic!=null && ic!=getIcn(c)) setIcn(ic, c);
        return o;
    }
    /* <<< Icon <<< */
    /* ---------------------------------------- */
    /* >>> Label Text >>> */
    public final ChButton t(CharSequence t) {
        if (!isEDT()) inEdtLaterCR(this,"T",t);
        else if (t==GO) setFG(0x007c00, enlargeFont(t(" GO "),1.2));
        else {
            final String html=addHtmlTagsAsStrg(t);
            setText(rmMnemon(html));
            _mnemonic=mnemon(html);
        }
        return this;
    }

    int mnemonic() { return _mnemonic;}
    /* <<< Label Text <<< */
    /* ---------------------------------------- */
    /* >>> ToolTip >>> */
    private final String item(Object q, MouseEvent ev) {
        if (q==null || 0!=(ev.getModifiers()&SHIFT_MASK) || gcp(KEY_HELP_SUFFIX,this)==Boolean.FALSE) return clasNam(q);
        final String s=rmMnemon(dItem(q));
        return sze(s)>0?s : niceShrtClassNam(q);
    }
    @Override public String getToolTipText(MouseEvent ev) {
        if (!super.isVisible()) return null;
        final ChButton cloned=gcp(KEY_CLONED_FROM,this, ChButton.class);
        if (cloned!=null) return cloned.getToolTipText();
        final BA sb=appndTooltips(ev, super.getToolTipText(ev), baTip());
        Object o=gcp(KEY_HELP_OBJECTS,this);
        if (o!=null) {
            if (gcp(KEY_HELP_SUFFIX,this)==Boolean.FALSE) sb.a("Source code of<br>");
            for(Object q :oo(o)) {
                if (q instanceof Boolean || q==null) continue;
                sb.a(item(q,ev)).a(' ',2);
            }
            sb.aln("<br>");
        }
        if ((o=gcp(KEY_SHARED_CTRL,this))!=null || (o=gcp(KEY_CUSTOMIZE,this))!=null) {
            pcp(KOPT_HIDE_IF_DISABLED,"",this);
            sb.a("Settings for ").a(item(o,ev)).aln("<br>");
        }
        if ((o=gcp(KEY_EXECUTE_CUST,this))!=null) sb.a(o).a(' ',3).and(gcp(KEY_EXECUTE_ARG,this),"<br>");
        return sze(sb)==0 ? null : addHtmlTagsAsStrg(sb);
    }
    public final ChButton tt(Object tt) {
        setTip(tt,this);
        return this;
    }
    @Override public String toString() {
        return (isToggle() ? "ChToggle " : isChLabl(this) ? "ChLabel" : "ChButton ")+getText()+" / "+gcp(KEY_ACTION_COMMAND,this);
    }
    /* <<< Label Text <<< */
    /* ---------------------------------------- */
    /* >>> Appearance >>> */
    public final ChButton fg(int rgb) {
        setFG(rgb, this);
        return this;
    }
    public final ChButton bg(int rgb) {
        setBG(rgb,this);
        if (_typ!='L') setContentAreaFilled(true);
        return this;
    }
    public ChButton like(JComponent protoType) {
        if (protoType==null) return this;
        t(getTxt(protoType));
        setIcn(getIcn(protoType),this);
        setForeground(protoType.getForeground());
        setFont(protoType.getFont());
        tt(protoType.getToolTipText());
        setFont(protoType.getFont());
        if (protoType instanceof AbstractButton) {
            final Icon i= ((AbstractButton)protoType).getSelectedIcon();
            if (i!=null) setSelectedIcon(i);
            if (((AbstractButton)protoType).isSelected()) s(true);
        }
        final ChButton b=deref(protoType,ChButton.class);
        if (b!=null) _oIcon=b._oIcon;
        return this;
    }
    public ChButton tabItemTipIcon(Object c) {
        if (c!=null) cp(KEY_CLASS,c);
        final Icon icn=dIcon(c);
        return t(dItem(c)).tt(dTip(c)).i(icn!=null?icn : TabItemTipIcon.g(TabItemTipIcon.ICON,c,null));
    }
    @Override public Object getRenderer(long options, long rendOptions[]) { return this;}
    /* <<< Appearance <<< */
    /* ---------------------------------------- */
    /* >>> Size >>> */
    @Override public Dimension getMinimumSize() { try { return super.getMinimumSize();} catch(Throwable e){return dim(32,32);}}
    @Override public Dimension getMaximumSize() {
        if (prefSze0(this)) return dim(0,0);
        if (smallCollapse()) return dim(EX,EX);
        try { return super.getMaximumSize();} catch(Throwable e){return dim(32,32);}
    }
    final boolean smallCollapse() { return sze(getText())==0 && getIcon()==null && (gcp(KEY_COLLAPSE[0],this)!=null || gcp(KEY_COLLAPSE[1],this)!=null);}

    @Override public Dimension getPreferredSize() {
        if (smallCollapse()) return dim(EX,EX);
        Dimension ps=prefSze(this);
        if (ps==null) {
            final ChButton cloned=gcp(KEY_CLONED_FROM,this,ChButton.class);
            if (gcp(KOPT_HIDE_IF_DISABLED,cloned)!=null && !cloned.isEnabled()) {
                ps=dim(0,0);
            }
        }
        if (ps==null && strEquls(PFX_VERTICAL, getText())) {
            final Font f=getFont();
            ps=dim(charH(f), 4+strgWidth(f, delPfx(PFX_VERTICAL,getText())));
        }
        try {  /* NullPointer  at javax.swing.plaf.synth.SynthLookAndFeel.paintRegion(SynthLookAndFeel.java:352) */
            if (ps==null) ps=super.getPreferredSize();
        } catch(Throwable e) { ps=dim(EM*10,EX); }
        final int h=_height>0 ? _height : _oIcon instanceof String && sze(_oIcon)>0 ? ICON_HEIGHT : -1;
        if (h>=0) ps=dim(ps.width,h);
        if (tipDnd(this,null)>0) {
            return dim(ps.width+ICON_HEIGHT, ps.height);
        }
        return ps;
    }
    public ChButton ps(Dimension d) {  return cp(KEY_PREF_SIZE,d);}
    public ChButton setHeight(int h) { _height=h; return this;}
    /* <<< Size <<< */
    /* ---------------------------------------- */
    /* >>> Event >>> */
    public ChButton li(ActionListener l) { addActLi(l,this);  return this;}
    @Override public void processEvent(AWTEvent ev) {
        if (!shouldProcessEvt(ev)) return;
        final int id=ev.getID();
        if (  gcp(KEY_DND_OBJ,this)!=null && jListDoDnD(ev)) {
            _handleEvt=false;
            return;
        }
        if (id==MOUSE_RELEASED) _handleEvt=true;
        if (id==MOUSE_MOVED && !_contains || (id==MOUSE_RELEASED || id==MOUSE_EXITED) && _contains) {
            _contains=!_contains;
            repaint();
        }

        try { super.processEvent(ev); } catch(Throwable ex) { stckTrc(ex);}
    }
    private long _actPerfWhen;
    private final EvAdapter LI=new EvAdapter(this);
    public void processEv(AWTEvent ev) {
        final int modi=modifrs(ev), id=ev.getID();
        final Object q=ev.getSource();
        if (id==MOUSE_PRESSED && q instanceof JCheckBoxMenuItem) inEDTms(thrdCR(this,"CBMI",q),333);
        if (id==ActionEvent.ACTION_PERFORMED) {
            if (!(q instanceof MItem)) _modi=modi;
            if (getParent()==null && System.currentTimeMillis()-_actPerfWhen<111) return; /* Occasionally two events for buttons in a table */
            _actPerfWhen=System.currentTimeMillis();
            setPrpty(_propClass,_propId, s());
            _mc++;
            if (gcp(KEY_EXECUTE_CUST,this)!=null) run(RUN_PUSHED,null);
            else inEDTms(thrdCR(this, RUN_PUSHED), 9);
            _contains=false;
            Object para=null;
            if ((para=gcp(KEY_DIA_STRG_MATCH,this))!=null) {
                final Object pp[]=(Object[])para;
                final long options=atol(pp[0]);
                final Object c[]=(Object[])pp[1];
                final String fileName=(String)pp[2];
                DialogStringMatch d=(DialogStringMatch)pp[3];
                if (d==null) pp[3]=d=new DialogStringMatch(options, fileName).setListComponent(c);
                d.showInFrame();
            } else if ((para=gcp(DO_HIDE_PARENT_WINDOW,this))!=null) {
                setVisblC(false,parentWndw(this),0);
            } else if ((para=gcp(DO_REPAINT,this))!=null) {
                repaintC(para);
            } else if ((para=gcp(KEY_detach_component,this))!=null) {
                final ChFrame frame=gcp(KEY_detach_frame, para,ChFrame.class);
                if (frame!=null) pnlDock(para); else pnlUndock(para, -1, -1, true);
            } else if ((para=gcp(KEY_VIEW_MSG,this))!=null) {
                final Object db=gcp(ACTION_DATABASE_CLICKED,this);
                if (para instanceof BA) {
                    Object send=deref(((BA)para).getSendTo());
                    if (send instanceof Object[]) send=get(0,send);
                    if (send!=null) para=send;
                }
                if (para instanceof HasPanel) para=((HasPanel)para).getPanel(HasPanel.NEW_PANEL);
                if (para instanceof JPopupMenu) ((JPopupMenu)para).show(this,EM,EX);
                else if (para instanceof ChFrame) ((ChFrame)para).shw(ChFrame.AT_CLICK);
                else {
                    Object m=para instanceof File ? readBytes((File)para) : para;
                    final BA rsc=readBytes(tryRscAsStream(m));
                    if (sze(rsc)>0) m=rsc.trimSize();
                    if (m instanceof Map) {
                        final BA sb=new BA(999);
                        for(Map.Entry e : entryArry((Map)m)) if (e.getValue()!=null) sb.aln(e);
                        m=sb;
                    }
                    final Component jc=getPnl(m);
                    final String title=orS(gcps(KEY_TITLE,this), txtForTitle(this));
                    if (jc==null)  {
                        final CharSequence cs=addHtmlTags(m instanceof CharSequence ? (CharSequence)m : toStrg(m));
                        if (cs!=null) {
                            final boolean html=looks(LIKE_HTML,cs);
                            Object tv= !html && cs instanceof BA ? ((BA)cs).textView(true) : deref(_mapTV.get(cs));
                            if (tv==null) _mapTV.put(cs,wref(tv=html ? new ChJTextPane(cs) : new ChTextView(cs)));
                            ChTextComponents.tools(tv)
                                .cp(KEY_SOUTH_PANEL, wref(gcp(KEY_SOUTH_PANEL,this)))
                                .cp(KEY_NORTH_PANEL, wref(gcp(KEY_NORTH_PANEL,this)))
                                .cp(db==null?null:ACTION_DATABASE_CLICKED,db)
                                .showInFrame(title);

                        }
                    } else {
                        ChFrame.frame(title, jc, CLOSE_CtrlW_ESC|ChFrame.PACK_SMALLER_SCREEN).i(getIcon()).shw(ChFrame.AT_CLICK);
                        jc.invalidate();
                        ChDelay.afterMS(EDT,ChDelay.REVALIDATE,jc,333,null);
                    }
                }
            } else if ((para=gcp(KEY_CLOSE,this))!=null) {
                final Object toBeClosed=get(1,para);
                final int opt=atoi(get(0,para));
                clos(opt, toBeClosed==REP_PARENT_WINDOW ? parentWndw(this) : toBeClosed);
            }
            visitURL(gcp(KEY_OPEN_URL,this),modi);
        }
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> When pressed >>> */
    private Runnable _run;
    public final ChButton r(Runnable r) { _run=r; return this;};
    private static void revalParents(Object array_or_comp, boolean alsoParent) {
        final Object[] oo=deref(array_or_comp, Object[].class);
        final Component c=derefC(array_or_comp);
        if (oo!=null) for(Object o : oo) revalParents(o,alsoParent);
        if (c!=null) {
            revalidateC(c);
            if (alsoParent) revalParents(parentC(c), false);
        }
    }
    /* <<< When pressed <<< */
    /* ---------------------------------------- */
    /* >>> Clone >>> */
    public final ChButton cln() {
        final char typ=_typ;
        final boolean isCB=typ=='C';
        final ChButton b=new ChButton(typ, isCB?getText():null).like(this).cp(KEY_CLONED_FROM,this);
        b.setForeground(getForeground());
        if (typ==_typ) {
            b.cp(KEY_PREF_SIZE,gcp(KEY_PREF_SIZE,this));
        }
        addRef(KEY_CLONES,wref(b),this);
        b.setModel(getModel());
        b.setToolTipText(getToolTipText());
        return b;
    }
    private static AbstractButton _cbMi;
    public final AbstractButton mi(String txt0) {
        if (_typ=='L') return this;
        final String txt=txt0!=null ? rmMnemon(txt0) : getText();
        final int mnem=mnemon(txt0);
        if (mnem!=0) _mnemonic=mnem;
        final AbstractButton mi;
        if (isToggle()) {
            (mi=new JCheckBoxMenuItem(txt)).setModel(getModel());
            LI.addTo("m",mi);
        } else {
            mi=new MItem(txt);
            if (txt0!=" ") {
                if (_oIcon==null) setIcn(iicon(IC_BLANK),mi);
                final KeyStroke ks=gcp(KEY_ACCELERATOR,this, KeyStroke.class);
                if (ks!=null) ((JMenuItem)mi).setAccelerator(ks);
                pcp(KEY_CLASS,gcp(KEY_CLASS, this),mi);
            }
        }
        if (txt0!=" ") {
            ChUtils.setUndockble(mi);
        }
        pcp(KEY_CLONED_FROM, this, mi);
        pcp(KEY_MENU_ID, gcp(KEY_MENU_ID,this),mi);
        mi.setToolTipText(getToolTipText());
        addCP(KEY_MENU_ITEMS, wref(mi),  this);
        LI.addTo("a",mi);
        return mi;
    }

    public final AbstractButton mi(String txt, Icon ic) {
        final AbstractButton mi=mi(txt);
        mi.setIcon(ic);
        return mi;
    }
    private AbstractButton _radio1, _cb1;
    public AbstractButton radio1() { if (_radio1==null) _radio1=radio(); return _radio1;}
    public AbstractButton cb1() { if (_cb1==null) _cb1=cb(); return _cb1;}
    public AbstractButton cb() {
        if (!isToggle()) assrt();
        return (AbstractButton) pcpReturn(KEY_CLONED_FROM, this, new ChJCheckBox(this));
    }
    public AbstractButton radio() {
        if (!isToggle()) assrt();
        final JRadioButton cb=new JRadioButton(getText());
        cb.setOpaque(false);
        cb.setModel(getModel());
        cb.setToolTipText(getToolTipText());
        pcp(KEY_CLONED_FROM,this,cb);
        return cb;
    }
    /* <<< Clone <<< */
    /* ---------------------------------------- */
    /* >>> Enabled >>> */
    /* Bugfix, it does not notice isEnabled(); */
    public final ChButton enabled(boolean b) {
        setEnabld(b,this);
        return this;
    }
    @Override public boolean isEnabled() {
        Object o;
        if ( (o=gcp(KEY_WEB_SETTINGS,this))!=null) return isAssignblFrm(NeedsInternet.class,o);
        if (!Insecure.EXEC_ALLOWED && gcp(KEY_HIDE_IF_EXEC_FORBIDDEN,this)!=null) return false;
        boolean enabled=super.isEnabled();
        final ChButton cloned=gcp(KEY_CLONED_FROM,this,ChButton.class);
        if (cloned!=null) enabled= cloned.isEnabled();
        o=gcp(KEY_HELP_OBJECTS,this);
        if (get(0,o) instanceof URL) enabled=true;
        else if (o!=null)     {
            for(Object o2:oo(o)) {
                o2=deref(o2);
                if (o2==null) continue;
                if (gcp(KEY_HELP_SUFFIX,this)==Boolean.TRUE ? hasHlp(o2) :
                    isSelctd(BUTTN[TOG_JAVA_SRC]) && hasJavaSrc(o2)) return true;
            }
            return false;
        } else if ( (o=gcp(KEY_VIEW_FILE,this))!=null) {
            final File f=file(o);
            enabled=sze(f)>0 || isDir(f);
        } else if ( (o=gcp(KEY_CUSTOMIZE,this))!=null) enabled=Customize.cc(o).length>0;
        else if ( (o=gcp(KEY_ENABLED,this)) instanceof IsEnabled) enabled=((IsEnabled)o).isEnabled(this);
        else if ( (o=gcp(KEY_CTRL,this))!=null) enabled=hasCtrlPnl(o);
        else if ( (o=ChCombo.selItem(gcp(KEY_SHARED_CTRL,this)))!=null) enabled=hasSharedControlPanel(o);
        return enabled;
    }
    /* <<< Enabled <<< */
    /* ---------------------------------------- */
    /* >>> Specific Actions >>> */
    public ChButton setDrawFromTo(int from, int to) { _fromTo=new int[]{from,to}; return this; }
    public ChButton doClose(int opt, Object o) {
        if (sze(getText())==0) rover(IC_CLOSE).tt("Close");
        return cp(KEY_CLOSE,new Object[]{intObjct(opt), wref(o)});
    }
    public static ChButton doClose15(int opt, Object o) { return javaVsn()>15 ? null : new ChButton().doClose(opt,o); }

    public ChButton doRepaint(Object c) { return cp(DO_REPAINT, c); }
    public static ChButton doView(Object msg) { return new ChButton().cp(KEY_VIEW_MSG, msg); }
    public static ChButton doOpenURL(Object o) {
        final String url, s;
        if (o instanceof File) {
            s=fPathUnix(o);
            url=toStrg(url(o));
        } else url=Hyperrefs.toUrlString(s=toStrg(o),Hyperrefs.WEB_LINK|Hyperrefs.PLAIN_TEXT|Hyperrefs.PROTEIN_FILE);
        return new ChButton(lstChar(s)=='*'? delSfx('*',s).replace('_','_'):s).i(IC_WWW).tt(url).cp(KEY_OPEN_URL,url);
    }
    public static ChButton dialogStringMatch(long opt, Object jc[], String fileName) {
        return new ChButton("Select by string patterns").i(IC_SEARCH).cp(KEY_DIA_STRG_MATCH, new Object[]{longObjct(opt),jc,fileName, null});
    }
    public static ChButton doHelp(Object o) {  return doHelpOrJava(o,'H'); }
    static ChButton doHelpOrJava(Object o,char HJ) {
        final ChButton b=o instanceof CharSequence && strchr(' ',o)>0 ?
            doView(o) :
            new ChButton(HIDE_IF_DISABLED|PAINT_IF_ENABLED,null)
            .cp(KEY_HELP_SUFFIX, boolObjct(HJ=='H'))
            .cp(KEY_HELP_OBJECTS,oo(o))
            .cp(KEY_ChTabPane, PANELS_JH[HJ]);
        rtt(b);
        return b;
    }
   public static ChButton doWebSettings(Object o) {
       final ChButton b=new ChButton("WS").like(buttn(BUT_TEST_PROXY)).cln().rover(IC_WWW_SETTINGS);
        if (o instanceof ChCombo) updateOn(CHANGED_COMBOBOX,b);
        return b.cp(KEY_WEB_SETTINGS,wref(o)).cp(KOPT_HIDE_IF_DISABLED,"");
    }
    public static ChButton doSharedCtrl(Object o) {
        final ChButton b=new ChButton().cp(KEY_SHARED_CTRL,wref(o)).tt("Control panel").rover(IC_CONTROLPANEL);
        rtt(b);
        if (o instanceof ChCombo) updateOn(CHANGED_COMBOBOX,b);
        return b;
    }
    public static ChButton doCtrl(Object o) {
        return
            isInstncOf(HasControlPanel.class,o) || isInstncOf(HasNativeExec.class,o) ?
            new ChButton().t("Details").cp(KEY_CTRL,o) : null;
    }
    public static ChButton doCustomize(Object o) {
        if (!(o instanceof JComboBox || o instanceof JList || Customize.cc(o).length>0)) return null;
        final ChButton b=new ChButton(ChButton.NOT_OPAQUE|ChButton.ICON_SIZE,null);
        rtt(b);
        return b.i(IC_CUSTOM).cp(KEY_CUSTOMIZE,wref(o));
    }
    public ChButton doViewFile(File f) { return cp(KEY_VIEW_FILE,f); }
    public ChButton doEditFile(Object file) {
        if (!Insecure.EXEC_ALLOWED) cp(KEY_HIDE_IF_EXEC_FORBIDDEN,"");
        return cp(KEY_EDIT_FILE,file instanceof CharSequence ? file((CharSequence)file) : file);
    }
    public static ChButton doPrint(Object o) {
        return new ChButton().rover(IC_CAMERA)
            .tt("An image file is written to <pre>"+file(dirTmp()+"/images")+
                "</pre>and opened in the web browser.<br>In the browser it can be printed.<br>Ctrl-key: use light background")
            .cp(KEY_PRINTABLE,wref(o));
    }
    public ChButton doExecute(Customize cust,Object arg) {
        rtt(this);
        return cp(KEY_EXECUTE_ARG,arg).cp(KEY_EXECUTE_CUST,cust);
    }
    public ChButton doFileBrowser(Object oFile) {
        final Object arg= oFile instanceof File ? ((File)oFile).getAbsolutePath() : oFile;
        t("Open directory in "+systProprty(SYSP_NAME_FILE_BROWSER));
        i(systProprty(SYSP_ICON_FILE_BROWSER));
        doExecute(Customize.customize(Customize.fileBrowsers),arg);
        if (!Insecure.EXEC_ALLOWED) cp(KEY_HIDE_IF_EXEC_FORBIDDEN,"");
        return this;
    }
    public ChButton doEnable(Object c) {
        if (isToggle()) setEnabld(s(),c);
        return cp(KEY_ENABLE,c);
    }
    public ChButton doUnselect(Object otherBut) {  return cp(KEY_DO_UNSEL,otherBut); }
    public ChButton doCollapse(Object toolbar) { return doCollapse(false, toolbar);}
    public ChButton doCollapse(boolean b, Object toolbar) {
        if (!isToggle()) assrt();
        cp(KEY_COLLAPSE[b?0:1],toolbar);
        pcpTargetArray(KEY_TOGGLE_COLLAPSE[b?1:0], wref(this),toolbar);
        for(JMenu jm : derefArray(toolbar,JMenu.class)) {
            if (jm instanceof ChJMenu) continue;
            pcp(KEY_COLLAPSE, jm.getPreferredSize(),jm);
            jm.setPreferredSize(dim(0,0));
        }
        return this;
    }
    private static void pcpTargetArray(Object key, Object value, Object target) {
        if (target instanceof Object[]) {
            for(Object t : (Object[])target) pcpTargetArray(key,value, t);
        } else pcp(key,value,target);
    }
    public ChButton doPack() { return doPack(null);}
    public ChButton doPack(Object w) {  return cp(KEY_pack,w!=null?w:""); }
    public static JComponent navigationPreviousNext(ActionListener li, String tip) {
        final JComponent p=pnl(new GridLayout(1,4)), e=pnl(new GridLayout(1,4));
        final String t[]=intrn(FIRST_REVIOUS_NEXT_LAST.split(" "));
        for(int i=0; i<4; i++) {
            final CharSequence tt=strplc(0L, "*", i==0 ? "first": i==1 ? "previous" : i==2 ? "next" : "last", tip);
            e.add(new ChButton(ChButton.MAC_TYPE_ICON|ChButton.PAINT_DIRECT,t[i]).tt(tt).li(li));
            p.add(new ChButton(ChButton.MAC_TYPE_ICON|ChButton.PAINT_DIRECT,t[i]).tt(tt));
        }
        pcp(ChRenderer.KEY_EDITOR_COMPONENT,e,p);
        return p;
    }
    /* <<< Specific Actions <<< */
    /* ---------------------------------------- */
    /* >>> ChToggle >>> */
    public int mc() { return _mc;}
    public boolean isToggle() { return _typ=='T'; }
    public final ChButton s(boolean isSelected) {
        if (isToggle()) {
            _handleEvt=false; setSelected(isSelected); _handleEvt=true;
        }
        return this;
    }
    public boolean s() { return isSelected();}
    private Class _propClass;
    private String _propId;
    public ChButton save(Class propertyClass, String propertyId) {
        s(getPrpty(_propClass=propertyClass, _propId=propertyId.intern(), s()));
        return this;
    }
    /** Used in ChTabPane. Invoked in paint hook */
    public boolean setTabSelected(Graphics g, boolean b) {
        if (g!=null && b) {
            g.setColor(C(BG_SELECTED));
            g.fillRect(0,0,999,999);
        }
        if (_tabSelected!=b) {
            _tabSelected=b;
            fg(b?FG_SELECTED:0);
            setContentAreaFilled(!b);
            setBorderPainted(!b);
            return true;
        }
        return false;
    }
    /* <<< ChToggle <<< */
    /* ---------------------------------------- */
    /* >>> Threading >>> */
    @Override public Color getForeground() {
        if (_superPaint && 0!=(_opt&PAINT_DIRECT)) return C(0,0);
        return super.getForeground();
    }
    public Object run(String id, Object arg) {
        if (id==ChRunnable.RUN_SET_ICON_IMAGE && !isEDT()) {
            inEDT(thrdCR(this,id,arg));
            return null;
        }
        if (id=="CBMI") {
            final AbstractButton cbMi=(AbstractButton)arg;
            final Component par=parentC(cbMi);
            if (par!=null && !par.isVisible() && getUndockedButton(cbMi)==null) {
                if (_cbMi==null) pcp(KEY_SOUTH_PANEL, pnl(new ChButton("Ok").doClose(0,REP_PARENT_WINDOW)),_cbMi=new JCheckBox());
                if (_cbMi.getModel()!=cbMi.getModel()) {
                    _cbMi.setToolTipText(cbMi.getToolTipText());
                    _cbMi.setText(getTxt(cbMi));
                    _cbMi.setModel(cbMi.getModel());
                }
                ChFrame.frame("Check-box", _cbMi, ChFrame.ALWAYS_ON_TOP).shw(ChFrame.PACK|ChFrame.AT_CLICK|ChFrame.DRAG|CLOSE_CtrlW_ESC);
            }
        }
        if (id==RUN_PUSHED && _handleEvt) {
            if (0!=(_opt&ON_PRESS_GC)) { Runtime.getRuntime().gc(); System.gc(); System.runFinalization(); }
            final int modi=_modi;
            final boolean ctrl=(modi&CTRL_MASK)!=0, shift=(modi&SHIFT_MASK)!=0;
            final Component ctrlPnl=derefC(ctrlPnl(gcp(KEY_CTRL,this)));
            if (ctrlPnl!=null) {
                final String KEY="Frame for getControlPanel";
                ChFrame f=gcp(KEY,this,ChFrame.class);
                if (f==null) {
                    final Object cont= child(ctrlPnl,JScrollPane.class)==null ? scrllpn(ctrlPnl) : ctrlPnl;
                    f=new ChFrame("Details").ad(cont).size(mini(800,prefW(ctrlPnl))+40,mini(600,prefH(ctrlPnl))+40);
                    f.pack();
                    cp(KEY,wref(f));
                }
                setWndwState('F',f.shw(CLOSE_CtrlW_ESC|ChFrame.AT_CLICK));
            }
            Customize.addDialog(Customize.cc(gcp(KEY_CUSTOMIZE,this)));
            Object o=gcp(KEY_HELP_OBJECTS,this);
            if (o!=null) {
                final ChTabPane tp=parentWndw(this) instanceof JDialog ? null : gcp(KEY_ChTabPane,this,ChTabPane.class);
                boolean success=false;
                for(Object q: oo(o)){
                    q=deref(q);
                    if (q==null || q instanceof Boolean) continue;
                    final boolean isHelp=gcp(KEY_HELP_SUFFIX,this)==Boolean.TRUE;
                    if (isHelp && ctrl) ChUtils.MAP_HLP[0]=null;
                    if (shift) edFile(-1, getJavaSrcFile(clas(q)), 0);
                    else success|=  isHelp ? showHelp(q) : showJavaSource(clasNam(q),(String[])null);
                }
                if (tp!=null && success) {
                    pushDivider(tp,0.7);
                    pushDivider(parentC(tp),0.7);
                }
            }
            if ((o=gcp(KEY_DO_CLICK,this, AbstractButton.class))!=null)  ((AbstractButton)o).doClick();
            if ((o=gcp(KEY_SHARED_CTRL,this))!=null) {
                final Object cp=sharedCtrlPnl(o,true);
                if (cp!=null) {
                    final ChFrame f=ChFrame.frame("Control Panel for "+shrtClasNam(o),cp,ChFrame.SCROLLPANE|ChFrame.AT_CLICK)
                        .size(mini(800, prefW(cp))+40,mini(600,prefH(cp))+40)
                        .shw(0);
                    setWndwState('F',f);
                }
            }
            if ((o=gcp(KEY_PRINTABLE,this))!=null) {
                int i=0;
                for(Object c:oo(gcp(KEY_PRINTABLE_COMPONENTS, o))) {
                    if (!(c instanceof JComponent)) continue;
                    Object fn=gcp(KEY_IMAGE_FILE_NAME,c);
                    if (fn==null) fn=toStrg(i++);
                    final File f=file(STRAPOUT+"/images/"+fn+".png");
                    pcp(KEY_IS_PRINTING,"",c);
                    writePng(0, c,f);
                    pcp(KEY_IS_PRINTING,null,c);
                    visitURL(f, modi);
                }
                writePng(0,null,null);

                viewFile(file(STRAPOUT+"/images/"),modi);
            }
            if ((o=gcp(KEY_ENABLE,this))!=null) setEnabld(isToggle()?s():true, o);
            if ((o=gcp(KEY_VIEW_FILE,this))!=null) viewFile(file(o), modi);
            if ((o=gcp(KEY_EDIT_FILE,this))!=null) edFile(-1, o, modi);
            if ((o=gcp(KEY_EXECUTE_CUST,this))!=null) {
                final Customize cust=(Customize)o;
                final ChExec ex=new ChExec(0).setCommandsAndArgument(cust.getSettings(),gcp(KEY_EXECUTE_ARG,this));
                if (ctrl) ex.setCustomize(cust);
                startThrd(ex,"ChButton");
            }
            if ((o=gcp(KEY_DO_UNSEL,this)) instanceof AbstractButton && isSelected()) ((AbstractButton)o).setSelected(false);
            runR(_run);
            final String cmd=gcps(KEY_ACTION_COMMAND,this);
            handleActEvt(this, orS(cmd,getText()), modi);
            if (cmd=="WS") Web.showProxyInfo();
            _modi=0;
            for(Object key : KEY_COLLAPSE) {
                for(Object collapse : oo(gcp(key, this))) {
                    final JMenu jm=deref(collapse,JMenu.class);
                    final Dimension pfs=gcp(KEY_COLLAPSE,jm,Dimension.class);
                    if (pfs!=null) jm.setPreferredSize(s() ? pfs : dim(0,0));
                    revalParents(collapse,true);
                    adaptSmallSizeParent(collapse);

                }
            }
            if ((o=gcp(KEY_pack,this))!=null) {
                Window w=deref(o,Window.class);
                if (w==null) w=parentWndw(this);
                if (w==null) w=parentWndw(o);
                if (w!=null) inEDTms( thrdM("pack", w), 222);
            }
        }
        if (id=="T") t((CharSequence)arg);
        if (id=="DOCLICK" && System.currentTimeMillis()-_actPerfWhen>500) ((AbstractButton)arg).doClick();
        if (id==ChRunnable.RUN_SET_ICON_IMAGE) {
            Image im=(Image)arg;
            if (im!=null) {
                final long options=atol(gcp(KEY_REQUEST_IMAGE_OPTIONS,this));
                final int maxWidth=atoi(gcp(KEY_REQUEST_IMAGE_MAX_WIDTH,this));
                final int w=im.getWidth(this), h=im.getHeight(this);
                if (maxWidth>0 && w>maxWidth) {
                    im=ChIcon.getScaledInstance(im, maxWidth, h*maxWidth/w,null, C(0xFFffFF));
                }
                if (0!=(options&REQUEST_IMAGE_REMOVE_TEXT)) setText(null);
                if (0!=(options&REQUEST_IMAGE_REMOVE_BORDER)) { noBrdr(this);}
                if (0!=(options&REQUEST_IMAGE_PACK)) packW(this);
                final ImageIcon icon=new ImageIcon(im);
                setIcn(icon,this);
                for(Object o : oo(gcp(ChButton.KEY_MENU_ITEMS, this))) setIcn(icon, o);
            } else setIcn(null,this);
        }
        return null;
    }

    public class MItem extends JMenuItem {
        final Map MAPCP=new java.util.HashMap();
        public MItem(String s) { super(s); }
        @Override public void setBackground(Color bg) {}
        @Override public Dimension getPreferredSize() {
            return
                getText()==" "?dim(1,EX) :
                !ChButton.this.isEnabled() && null!=gcp(KOPT_HIDE_IF_DISABLED,ChButton.this) ? dim(0,0):
                super.getPreferredSize();
        }
        @Override public boolean isEnabled() { return ChButton.this.isEnabled(); }
        private boolean _init;
        @Override public void paintComponent(Graphics g) {
            if (g==null || !_init) { _init=true;  ChButton.this.paintComponent(null); }
            if (g==null || !isValid()) return;
            if (getText()==" ") {
                super.paintComponent(g);
            } else {
                setFG(isEnabled()?0:0x808080,this);
                final Color bg=gcp(KEY_BACKGROUND, this, Color.class);
                if (bg!=null && !isOpaque()) {
                    g.setColor(bg);
                    g.fillRect(0,0,3333,3333);
                }
                g.setColor(C(0)); /* sonst weiss auf weiss  MetalLAF in jlUndock */
                super.paintComponent(g);
                ChRenderer.drawSmallButtons(this,g);
            }
        }
        @Override public Icon getIcon() {
            if (gcp(KOPT_NO_ICON,this)!=null) return null;
            if (!isSelctd(buttn(TOG_MENUICONS))) return iicon(IC_BLANK);
            Icon c=super.getIcon();
            if (c==null) c=ChButton.this.getIcon();
            if (c==null) c=iicon(IC_BLANK);
            return c;
        }
        { enableEvents(ACTION_EVENT_MASK|MOUSE_EVENT_MASK); }
        @Override public final void processEvent(AWTEvent ev) {
            for(AWTEvent ev2 : ChJMenu.lockSubmenuOnClick(ev)) {
                if (shouldProcessEvt(ev2)) {
                    /* _modi Workaround: shift not in ActionEvent if JMenu.getPopupMenu().setLightWeightPopupEnabled(false); */
                    if (ev2.getID()==MOUSE_PRESSED) ChButton.this._modi=modifrs(ev);
                    super.processEvent(ev2);
                }
            }
        }
    }
    /* ---------------------------------------- */
    /* >>> DnD >>> */
    public Object getDndDateien() { return dndV(false,this);}
    public ChButton addDnD(Object o) {
        final boolean changed=(o!=null) != (gcp(KEY_DND_OBJ,this)!=null);
        if (o!=null) setHorizontalAlignment(LEFT);
        cp(KEY_DND_OBJ,o);
        if (changed) revalAndRepaintC(this);
        return this;
    }

    {

        //   if (!withGui()) stckTrc();
    }
}
