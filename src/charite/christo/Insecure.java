package charite.christo;
import java.io.File;
import java.io.IOException;
import java.util.*;
/**
   All potentially dangerous file operations such as file modification or running native
   code are chaneled through this class.
   This class contains all security checks.
   @author Christoph Gille
*/
public class Insecure {
    private final static boolean ALL_ALLOWED=true, WINDOWS=File.separatorChar=='\\';
    public final static boolean
        CLASSLOADING_ALLOWED=ALL_ALLOWED,
        EXEC_ALLOWED=ALL_ALLOWED,
       	ARBITRARY_WORKING_DIRECTORY_ALLOWED=ALL_ALLOWED,
        UNCONTROLLED_FILE_MODIFICATION_ALLOWED=ALL_ALLOWED;
    public final static String TEXMF_DOT_CONF="/usr/share/texmf/web2c/texmf.cnf";
    private Insecure(){}
    /* --------------------------------------- */
    private final static String ERROR="\u001B[45mError\u001B[0m in "+Insecure.class;

    private static String getPathLC(File f) {
        String path=null;
        try {
            path=f==null ? null : f.getCanonicalPath();
        } catch(IOException iox) { path=f.getAbsolutePath(); }

        return path==null?null:path.toLowerCase();
    }
    /* >>> Is file modification allowed ? >>> */
    public static String msgNotModify(Object f) {
        //ChUtils.stckTrc();
        return
            "SECURITY ERROR"+
            "\n  File: "+f+
            "\n  Working dir="+_allowedPath+
            ("\nStrap's security system prevents files from being modified\n"+
             "unless within the project directory or the file path contains the string \"StrapAlign\".\n\n"+
             "File modification control can be deactivated with the command line option -allowFileModification.\n"
             );
    }

    private static boolean canModifyThrowEx(File f) {
        if (f!=null) {
            if (canModify(f)) return true;
            final String path=getPathLC(f), txt=msgNotModify(f);
            System.out.println(ERROR+txt);
            if (VIOLATIONS.get(path)==null) {
                final String stckTrc=stckTrcAsStrg(new IllegalAccessException(txt));
                VIOLATIONS.put(path,stckTrc.intern());
                System.out.println(stckTrc);
            }
        }

        return false;
    }
    public static boolean canModify(File f) {
        if (f!=null) {
            if (UNCONTROLLED_FILE_MODIFICATION_ALLOWED && !_securityEnabled) return true;
            final String path=getPathLC(f), ap=_allowedPath;
            if (ARBITRARY_WORKING_DIRECTORY_ALLOWED && ap!=null && path.startsWith(ap)) {
                final int L=ap.length();
                if (path.length()==L) return true;
                final char slash=path.charAt(L);
                if (slash=='\\' || slash=='/') return true;
            }
            if (path.endsWith(".jnlp") && path.indexOf("strap")>=0) return true;
            if (path.indexOf("strapalign")>=0) return true;
            if (WINDOWS && path.indexOf("texmf")>0 && path.replace('\\','/').indexOf(TEXMF_DOT_CONF.toLowerCase())>0) return true;
        }
        return false;
    }
    private static String _allowedPath;
    public static void securitySetAllowedPath(File dir) {
        if (ARBITRARY_WORKING_DIRECTORY_ALLOWED) {
            if (dir==null || (dir.exists() && !dir.isDirectory())) {
                System.out.println("dir="+dir);
                assert false;
            }
            final String ap=getPathLC(dir);
            final int L=ap.length();
            boolean hasLetterOrDigit=false;
            for(int i=0; i<L; i++) {
                final char c=ap.charAt(i);
                if ('a'<=c && c<='z' || 'A'<=c && c<='Z' || '0'<=c && c<='9') hasLetterOrDigit=true;
            }
            if (!hasLetterOrDigit) {
                System.out.println("Error in securitySetAllowedPath "+dir+" has no letter or digit");
                return;
            }
            final char cLast=ap.charAt(L-1);
            Insecure._allowedPath= cLast=='/'||cLast=='\\' ? ap.substring(0,L-1) : ap;
        }
    }
    private static boolean _securityEnabled;
    public static void setFileModificationControl(boolean b) {_securityEnabled=b;}
    /* <<< Is file modification allowed ? <<< */
    /* ---------------------------------------- */
    /* >>> Report Violations >>> */
    public final static java.util.HashMap<String,String> VIOLATIONS=new java.util.HashMap();
    public static String stckTrcAsStrg(Throwable t) {
        final java.io.ByteArrayOutputStream os=new  java.io.ByteArrayOutputStream();
        if (t==null) t=new Throwable();
        t.printStackTrace(new java.io.PrintStream(os));
        return os.toString();
    }
    public static void tellNoPermission(char what, String pfx) {
        //ChUtils.stckTrc();
        final String
            w=what=='X'?"Execution of native code " : "",
            msg=
            " is disabled in Strap Lite.<br>\n"+
            "You can Launch_STRAP* with  full functionality by pressing the Web-start button on the Strap home page:<br>"+
            ChConstants.URL_STRAP+"<br><br>\n\n"+
            "You can exchange proteins between two Strap views with the mouse using Drag-and-Drop.<br>"+
            "Watch Movie "+ChConstants.MOVIE_Drag_to_another_STRAP+" <br>";
        ChUtils.error( pfx+w+msg);
    }
    /* >>> Report Violations >>> */
    /* ---------------------------------------- */
    /* >>> Native binaries  >>> */
    public static Process runtimeExec(String arg[], String[] env, File dir) throws IOException{
        return EXEC_ALLOWED ?  Runtime.getRuntime().exec(arg, env,dir) : null;
    }
    /* <<< Native binaries <<< */
    /* ---------------------------------------- */
    /* >>> File modification >>> */
    public static boolean renameToIgnoreSecurity(File src, File dest) {
        if (src!=null && dest!=null && src.renameTo(dest)) {
            touchParentDir(src);
            touchParentDir(dest);
            return true;
        }
        return false;
    }
    static boolean renameFile(File src, File dest) {
        if (canModifyThrowEx(src) && canModifyThrowEx(dest) && src.renameTo(dest)) {
            touchParentDir(src);
            touchParentDir(dest);
            return true;
        }
        return false;
    }
    static boolean delFile(File f) {
        if (canModifyThrowEx(f) && f.delete()) {
            touchParentDir(f);
            return true;
        }
        return false;
    }
    static java.io.OutputStream fileOutputStream(File f, boolean append) throws IOException {
        if (f==null) return null;
        if (canModifyThrowEx(f)) {
            touchParentDir(f);
            return new java.io.FileOutputStream(f,append);
        } else {
            System.out.println(ERROR+"new FileOutputStream failed for "+f);
            throw new IOException("Insecure.java: "+f);
        }

    }
    static java.io.RandomAccessFile randomAccessFile(File f, String mode) throws IOException {
        if (canModifyThrowEx(f)) return new java.io.RandomAccessFile(f, mode);
        else {
            System.out.println(ERROR+"new RandomAccessFile failed for "+f);
            throw new IOException("Insecure.java: "+f);
        }
    }
    /* <<< File modification <<< */
    /* ---------------------------------------- */
    /* >>> Directory >>> */
    static boolean makeDirectories(File f, boolean printErr) {
        if (f==null || f.isDirectory()) return false;
        if (canModifyThrowEx(f)) f.mkdirs();
        else {
            if (printErr){
                System.out.println(ERROR+"File.mkdirs failed for "+f);
            }
        }
        return f.isDirectory();
    }
    private final static Map<String,File> _mapF=new HashMap();
    public static void touchParentDir(File f) {
        if (!WINDOWS||f==null) return;
        final String path=f.getParent();
        File dir=_mapF.get(path);
        if (dir==null) _mapF.put(path, dir=new File(path));
        dir.setLastModified(System.currentTimeMillis());
    }
    /* <<< Directory <<< */
    /* ---------------------------------------- */
    /* >>> Reflection >>> */
    public static Class findClassByName(final String cn, File[] classpath) {
        if (cn==null) return null;
        try {
            if (CLASSLOADING_ALLOWED) {
                return ChClassLoader1.getInstance(false, classpath, (File)null).loadClass(cn);
            } else {
                return Class.forName(cn);
            }
        } catch(ClassNotFoundException ex) {
            if (!CLASSLOADING_ALLOWED)tellNoPermission(' ', "Insecure.java: Class.forName("+cn+") failed<br><br>Maybe that the requested operation ");
            //throw ex;
        }
        return null;

    }

}
