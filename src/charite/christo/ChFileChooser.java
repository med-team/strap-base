package charite.christo;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**
   @author Christoph Gille
*/
/*
   rescanCurrentDirectory()
   SynthFileChooserUIImpl  sun.swing.FilePane
   WindowsFileChooserUI
   MetalFileChooserUI
*/
public class ChFileChooser extends JFileChooser implements ProcessEv, java.beans.PropertyChangeListener, ChRunnable, Disposable {
    {  assert CLOSE_MASK<(1<<11); }
    public final static int CLOSE=1<<1, MULTI=1<<2, FOLDERS=1<<3, NOT_ALL=1<<4, BUT_PROJECT_FOLDER=1<<6;
    private final static int CACHE_SCROLL=0, CACHE_SELF=1;
    private final Map _maps[]=new Map[2];
    private final Object KEY=_maps;
    private final String _id;
    private final int _opt;
    private FilePopup _context;
    private Customize _filters;
    private final Set _vSelected=new HashSet(); /* Bugfix Windows LAF does not display setSelectedFiles(ff) */
    private DialogStringMatch _stringMatch;
    public final static String KEY_FPANE="CFC$$KFP";

    private final Runnable _r4sb=thrdCR(this,"S4SB"), _rHacks=thrdCR(this,"HACKS");
    public ChFileChooser(int options, String id) {
        _opt=options;
        _id=id;
        final File dir=id==null?dirWorking():file(getPrpty(ChFileChooser.class,id,null));
        if (isDir(dir)) setCurrentDirectory(dir);
        setDragEnabled(true);
        setMinSze(4*EM,4*EX, this);
        evAdapt(this).addTo("a", this);
        setDialogType(JFileChooser.OPEN_DIALOG);
        UIManager.put("FileChooser.fileNameLabelText", "File: (Tab-key to expand file) ");
        setControlButtonsAreShown(true);
        ChThread.callEvery(ChThread.WEAK_REFERENCE, 1000, thrdCR(this,"SCAN"), "rescanCurrentDirectory");
        setMultiSelectionEnabled(0!=(options&MULTI));
        if (0!=(options&FOLDERS)) setFileSelectionMode(DIRECTORIES_ONLY);
        setAcceptAllFileFilterUsed(0==(options&NOT_ALL));
        addPropertyChangeListener(this);

    }
    public void dispose() {
        _vSelected.clear();
        for(Map c : _maps) if (c!=null) c.clear();
        _context=null;
    }
    /* <<< Instance <<< */
    /* ---------------------------------------- */
    /* >>> Filters >>> */

    public ChFileChooser addFilter(String rules) {
        addChoosableFileFilter(strEquls(STRSTR_IC,"other",rules)?new ChFileFilter(getChoosableFileFilters()) : new ChFileFilter(rules));
        return this;
    }

    public ChFileChooser setFilters(Customize c) {
        _filters=c;
        addActLi(evAdapt(this),c);
        initCustFilters();
        return this;
    }
    private void initCustFilters() {
        if (_filters!=null) {
            resetChoosableFileFilters();
            for(ChFileFilter f : _filters.toFileFilters()) addChoosableFileFilter(f);
            setAcceptAllFileFilterUsed(true);
        }
    }

    /* <<< Filters <<< */
    /* ---------------------------------------- */
    /* >>> Keep selection >>> */
    private Map map(int i) {
        if (_maps[i]==null) _maps[i]=new HashMap();
        return _maps[i];
    }
    private String key(int t) {
        final FileFilter f=getFileFilter();
        return (f==null?null:f.getDescription())+(child(this,JTable.class)!=null ? "_t_":"_")+getCurrentDirectory();

    }
    public void propertyChange(java.beans.PropertyChangeEvent ev) {
        final String id=ev.getPropertyName();
        if (id==FILE_FILTER_CHANGED_PROPERTY || id==SELECTED_FILES_CHANGED_PROPERTY || id==DIRECTORY_CHANGED_PROPERTY) {
            Object arg=null;
            if (id==SELECTED_FILES_CHANGED_PROPERTY) {
                _vSelected.clear();
                File[] ff=deref(ev.getNewValue(), File[].class);
                if (ff==null) ff=getSelectedFiles();
                adAll(ff,_vSelected);
                arg=ff;
            }
            inEDTms(thrdCR(this,id,arg),222);
        }
    }

    public void saveSelection(File[] ff) {
        final int N=sze(ff);
        final BA ba=new BA(N*20);
        for(int i=0; i<N; i++) ba.aln(ff[i].getName());
        map(CACHE_SELF).put(key(CACHE_SELF), ba.trimSize());//newBytes(0,MAX_INT));
        repaintC(fPane());
    }
    private JComponent fPane() {
        final JComponent jt=child(this,JTable.class);
        return jt!=null?jt:child(this, JList.class);
    }
    public Object run(String id, Object arg) {

        if (id=="SCAN_EDT") rescanCurrentDirectory();
        if (id=="SCAN") {
            if (isVisbl(this)) {
                final File dir=getCurrentDirectory();
                if (dir!=null) {
                    final long lm=dir.lastModified();
                    if (lm!=_lm) {
                        _lm=lm;
                        inEdtLaterCR(this,"SCAN_EDT",null);
                    }
                }
            }
        }

        if (id=="S4SB") selectionToSb(fPane());
        if (id=="HACKS") {
            hacks();
            repaint();
        }
        if (id==SELECTED_FILES_CHANGED_PROPERTY) {
            final JComponent fPane=fPane();
            if (fPane!=null && fPane.hasFocus()) saveSelection(deref(arg,File[].class));
            if (_id!=null) setPrpty(ChFileChooser.class,_id, toStrg(getCurrentDirectory()));
            inEDTms(_r4sb,111);
        }
        if (id==FILE_FILTER_CHANGED_PROPERTY || id==DIRECTORY_CHANGED_PROPERTY) {
            final JViewport vp=child(this,JViewport.class);
            final Point p=(Point)map(CACHE_SCROLL).get(key(CACHE_SCROLL));
            if (vp!=null && p!=null) vp.setViewPosition(p);
            final String nn[]=splitTokns(map(CACHE_SELF).get(key(CACHE_SELF)),chrClas1('\n'));
            final File dir=getCurrentDirectory(), ff[]=new File[nn.length];
            for(int i=nn.length; --i>=0;) ff[i]=new File(dir,nn[i]);
            setSelectedFiles(ff);
        }
        // selectionToSb(fPane);

        return null;
    }
    /* <<< Keep selection <<< */
    /* ---------------------------------------- */
    /* >>> AWTEvent >>> */
    public void processEv(AWTEvent ev) {
        final int id=ev.getID(), kcode=keyCode(ev), modi=modifrs(ev);
        final Object q=ev.getSource();
        final String cmd=actionCommand(ev);
        setAotEv(ev);
        if ((id==MouseEvent.MOUSE_RELEASED && q instanceof JScrollBar || id==ComponentEvent.COMPONENT_RESIZED)) {

            final JViewport vp=child(this,JViewport.class);
            if (vp!=null) map(CACHE_SCROLL).put(key(CACHE_SCROLL),vp.getViewPosition());
        }
        selectNxtPrev(deref(q, JComboBox.class), wheelRotation(ev));
        if (id==MouseEvent.MOUSE_PRESSED && q instanceof AbstractButton) {
            inEDTms(_rHacks,999);
        }
        if (cmd==ACTION_CUSTOMIZE_CHANGED) initCustFilters();
        if (id==KeyEvent.KEY_PRESSED) {
            final ChTextComponents tools=gcp(KEY,ev.getSource(),ChTextComponents.class);
            if (tools!=null) tools.pEv(ev);
            if (kcode=='F' && isShrtCut(ev) && 0==(modi&MouseEvent.SHIFT_MASK)) {
                if (_stringMatch==null) _stringMatch=new DialogStringMatch(0L,"fileChooser");
                _stringMatch.setListComponent(new Object[]{this}).showInFrame();
            }
            if (kcode==KeyEvent.VK_ENTER) {
                final String t=toStrgTrim(getTxt(q));
                final boolean isAbs=looks(LIKE_FILEPATH,t);
                final File f=file(isAbs ? t : getCurrentDirectory()+"/"+t);
                if (isDir(f)) {
                    setCurrentDirectory(f);
                    if (!isAbs) setTxt("",q);
                } else if (fExists(f)) {
                    final File dir=file(f.getParent());
                    if (isDir(dir)) setCurrentDirectory(dir);
                    final javax.swing.filechooser.FileFilter fil=getFileFilter();
                    if (fil==null||fil.accept(f)) setSelectedFile(f);
                }

            }
        }
        if (chrAt(0,cmd)=='>') setCurrentDirectory(file(cmd.substring(1)));
        if ((CANCEL_SELECTION==cmd || APPROVE_SELECTION==cmd) && 0!=(CLOSE&_opt)) setVisblC(false, parentWndw(this), 0);
        if (APPROVE_SELECTION==cmd || CANCEL_SELECTION==cmd) handleActEvt(this,cmd, modi);
    }
    /* <<< AWTEvent <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    @Override public void paint(Graphics g) {
        try { super.paint(g); } catch(Throwable ex){}
        hacks();
    }
    private void hacks() {
        final EvAdapter li=evAdapt(this);
        final Action goHome=deref(invokeMthd("getGoHomeAction",getUI()), Action.class);

        for (JComponent c : childsR(this,JComponent.class)) {
            final JList jl=deref(c,JList.class);
            final JTable jt=deref(c,JTable.class);
            if (jt!=null && jt.getAutoResizeMode()==JTable.AUTO_RESIZE_OFF) jt.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            if ((jl!=null || jt!=null) && gcp(KEY_FPANE,this)!=c) {
                selectionToSb(c);
                pcp(KEY_FPANE,c,this);
                pcp(KOPT_SUPPORTS_CTRL_F,"",c);
                evLstnr(FOLI_FOCUS).addTo("f",c);
            }
            if (gcp(KEY,c)!=null) continue;
            pcp(KEY,"",c);
            li.addTo("mk",c);
            if (jl!=null || jt!=null) {
                final ChRenderer rr[]={null,null};
                if (jl!=null) jl.setCellRenderer(rr[0]=new ChRenderer().setOrigRenderer(jl.getCellRenderer(), _vSelected));
                else {
                    jt.setDefaultRenderer(Object.class,rr[0]=new ChRenderer().setOrigRenderer(jt.getDefaultRenderer(Object.class), _vSelected));
                    jt.setDefaultRenderer(  File.class,rr[1]=new ChRenderer().setOrigRenderer(jt.getDefaultRenderer(  File.class), _vSelected));
                }
                final String service=ChRunnable.RUN_MODIFY_RENDERER_COMPONENT;
                final List<ChRunnable> vR=listServicesR(false,this,service);
                for(int k=sze(vR); --k>=0;) {
                    final ChRunnable r=(ChRunnable)get(k,vR);
                    for(ChRenderer ren : rr) addServiceR(r, ren, service);
                }
                if (0!=(_opt&CLOSE_CtrlW)) closeOnKey(CLOSE_CtrlW, c, this);

                JComponent b1=null, b2=null;
                if (_context==null) {
                    _context=new FilePopup(0L);
                    _context.manageTT(c);
                    _context.JC_FOCUS[0]=wref(this);
                    b1=Customize.newButton(Customize.proteinFileExtensions).mi("Customize file filters");
                    b2=new ChButton(">"+dirWorking()).li(evAdapt(this)).i(IC_DIRECTORY).mi("Go to project folder");
                }
                final JPopupMenu jpm=(JPopupMenu)invokeMthd("getComponentPopupMenu",c);
                if (jpm==null) {
                    _context.register(c);
                    if (b1!=null) {
                        _context.popMenu().add(b1,0);
                        _context.popMenu().add(b2,0);
                    }
                } else if (b1!=null) {
                    jpm.addPopupMenuListener(_context);
                    jpm.add(b1);
                    jpm.add(b2);
                    adToMenu(0,jpm, _context.menuObjects(null,"-"),"");
                }
            }
            final JScrollPane sp=deref(c,JScrollPane.class);
            if (sp!=null && !(sp.getVerticalScrollBar() instanceof ChScrollBar))  {
                final ChScrollBar
                    hsb=new ChScrollBar(JScrollBar.HORIZONTAL),
                    vsb=new ChScrollBar(JScrollBar.VERTICAL);
                sp.setHorizontalScrollBar(hsb);
                sp.setVerticalScrollBar(vsb);
                li.addTo("m",vsb);
                li.addTo("m",hsb);
                li.addTo("c",sp);
            }
            final boolean goProject=0!=(_opt&BUT_PROJECT_FOLDER);
            ChButton bPrj=null;
            if (goProject && c.getClass().getName().endsWith("WindowsPlacesBar")) {
                bPrj=new ChButton(">"+dirWorking()).t("Project folder").i(IC_DIRECTORY);
                bPrj.setOpaque(false);
                bPrj.setHorizontalTextPosition(AbstractButton.CENTER);
                bPrj.setVerticalTextPosition(AbstractButton.BOTTOM);
                bPrj.setAlignmentX(CENTER_ALIGNMENT);
                bPrj.setFocusable(false);
                c.add(bPrj);

            }
            if (c instanceof AbstractButton) {
                final AbstractButton but=(AbstractButton)c;
                if (goProject) {
                    if (goHome!=null && goHome==but.getAction() || cntains(goHome, c.getListeners(ActionListener.class))) {
                        final Container par=c.getParent();
                        if (par.getLayout() instanceof BoxLayout || par.getLayout() instanceof FlowLayout) {
                            final Dimension d=c.getPreferredSize();
                            bPrj=new ChButton(">"+dirWorking()).t("./");
                            bPrj.setPreferredSize(d);
                            par.add(bPrj, idxOf(c,par.getComponents()));
                        }
                    }
                }
                li.addTo("m",c);
            }
            if (bPrj!=null) bPrj.li(evAdapt(this)).tt("Go to working directory<br>"+dirWorking()).revalidate();

            if (c instanceof Box || c instanceof Box.Filler || c instanceof JPanel || c instanceof JLabel) {
                setDragMovesWindow(false, c);
            }
            final JComboBox choice=deref(c,JComboBox.class);
            if (choice!=null) {
                if (sze(c.getListeners(MouseWheelListener.class))==0) li.addTo("w",c);
                choice.setMaximumRowCount(maxi(33,choice.getMaximumRowCount()));
            }

            if (c instanceof JTextField) {
                final ChTextComponents tools=new ChTextComponents(c).enableWordCompletion(ChTextComponents.COMPL_TAB|ChTextComponents.COMPL_ALLOW_SPC_IN_FILE,this, chrClas(SLASH),null);
                pcp(KEY,tools,c);
                li.addTo("k",c);
                Container p=c.getParent();
                if (p!=null) {
                    final Object cc[]=p.getComponents();
                    if (p.getLayout() instanceof FlowLayout) p.setLayout(new BoxLayout(p,BoxLayout.LINE_AXIS));
                    if (!(p.getLayout() instanceof BorderLayout)) {
                        p.add(new JLabel(addHtmlTagsAsStrg("<font size=\"2\"> (Tab-key for Auto-completion<br>Right click for context menu)</font>")),idxOf(c,cc)+1);
                    }
                    do { p.setVisible(true); } while( (p=p.getParent())!=null);
                }
            }
        }
    }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> rescanCurrentDirectory >>> */
    private long _lm;

    /* <<< rescanCurrentDirectory <<< */
}

