package charite.christo;
/**
   @author Christoph Gille
*/
public class Ratio {
    private int _n, _d;
    public Ratio(int numerator,int denominator) {
        _n=numerator;
        _d=denominator;
    }
    public int getNumerator() { return _n;}
    public int getDenominator() { return _d;}

    public void setNumerator(int n) {  _n=n;}
    public void setDenominator(int d) { _d=d;}

    @Override public String toString() { return _n+"/"+_d;}
    public float floatValue() {
        final int d=_d;
        return d==0 ? Float.NaN : _n/(float)d;
    }
}
