package charite.christo;
import static charite.christo.ChUtils.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;
public class PrgParasGUI implements ActionListener {
    final static String BLANK="PPGUI$$B";
    private final String[][] _names_descriptions;
    private final long _opt;
    private JComponent _panel, _jcc[], _tfFurtherOpts;
    private int _mc;
    /**
       {
       {option1,explanation},
       {option2,explanation}
       ...
       }
    */

    public PrgParasGUI(long options, String names_descriptions) {
        _opt=options;
        CharSequence txt=names_descriptions;
        if (txt!=null && nxt(SPC,txt)<0) {
            final CharSequence rsc=strchr('/',txt)>0?txt : ChConstants._CCSE+"/"+txt;
            txt=readBytes(tryRscAsStream(rsc));
            if (txt==null) assrt();
        }
        final String lines[][]=toDoubleArry(txt,chrClas1('\n'),SPLIT_SKIP_WHITE_SPC,chrClas1('\t'),SPLIT_SKIP_WHITE_SPC);
        _names_descriptions=lines;
        if (lines!=null) {
            for(String[] opt : lines) {
                if (sze(opt)>2) (_map==null ? _map=new HashMap() : _map).put(opt[0],opt[2]);
            }
        }
    }
    public int mc() { return _mc;}
    public JComponent getPanel() {
        final String[][] nd=_names_descriptions;
        if (_panel==null && (nd!=null || (_opt&HasPrgParas.FURTHER_PARAMETERS)!=0)) {
            _panel=pnl(VB);
            _jcc=new JComponent[sze(nd)];
            for(int i=0; i<sze(nd); i++) {
                final JComponent add, tf;
                final String descr=nd[i][1], name=nd[i][0], initValue=nd[i].length>2 ? nd[i][2]:"";
                final char d0=chrAt(0,descr),d1=chrAt(1,descr),d2=chrAt(2,descr);
                final int curly=d0=='{' ? descr.lastIndexOf('}') : -1;
                if (d0=='[' && (d1=='x'||d1=='X') && d2==']') {
                    tf=toggl(descr.substring(3)).s(d1=='X');
                    add=pnl(HBL,name, ((ChButton)tf).cb());
            } else if (curly>0) {
                    final ChCombo choice=new ChCombo(splitStrg(descr.substring(1, curly),','));
                    if (initValue!=null) choice.s(initValue);
                    add=pnl(HBL,name, tf=choice,descr.substring(1+curly));
                } else {
                    final Class contentType=d1!=' ' ? null : d0=='i' ? Integer.class : d0=='f' ? Float.class : d0=='F' ? File.class : null;
                    add=pnl(CNSEW,tf=new ChTextField(initValue).ct(contentType),null,null,contentType!=null ? descr.substring(2) : descr,name);
                }
                addActLi(this, _jcc[i]=tf);
                _panel.add(add);
            }

            if ( (_opt&HasPrgParas.FURTHER_PARAMETERS)!=0) {
                _panel.add(pnl(HB,"",_tfFurtherOpts=new ChTextField().cols(70,false,false).li(this),"Further<br>options"));
            }
        }
        return _panel;
    }

    public Map<String,String> getMap() { return _map;}
    private Map<String,String> _map;

    public String[] getFurtherOptions() { return splitTokns(toStrg(_tfFurtherOpts)); }

    public void actionPerformed(ActionEvent ev) {
        final Object q=ev.getSource();
        final int row=idxOf(q,_jcc);
        if (row>=0 && isEnabld(q)) {
            final String name=_names_descriptions[row][0],value;
            if (q instanceof ChButton) {
                if (lstChar(name)=='=') value=isSelctd(q) ? "true" : "false";
                else value=isSelctd(q) ? BLANK : null;
            } else value=toStrg(q);
            (_map==null ? _map=new HashMap() : _map).put(name,value);
            _mc++;
        }
        if (q==_tfFurtherOpts) _mc++;
    }

}
