package charite.christo;
import java.util.List;
import java.util.*;
import javax.swing.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
*/
public class ChTableModel extends javax.swing.table.AbstractTableModel {
    private final long _opt;
    private boolean _editable[];
    private String  _colNames[];
    private Object _rowData, _refTable;
    public void setColumnNames(String...header) { _colNames=header; }
    public ChTableModel(long opt, String...header) { _opt=opt; _colNames=header;}
    public ChTableModel setData(Object data[][]) { _rowData=data; return this; }
    public ChTableModel setData(List data)       { _rowData=data; return this;}
    @Override public String getColumnName(int col) { return toStrgN(get(col, _colNames)); }
    @Override public int getRowCount() { return sze(_rowData); }
    @Override public int getColumnCount() { return sze(_colNames); }

    @Override public Object getValueAt(int row, int col) {
        final Object oRow=get(row,_rowData);
        if (getColumnCount()==1 && !(oRow instanceof List || oRow instanceof Object[])) return oRow;
        return get(col, oRow);
    }
    public ChTableModel editable(boolean... bb) { _editable=bb; return this; }

    @Override public boolean isCellEditable(int row, int col) {
        final Object o=getValueAt(row,col) ;
        return o instanceof AbstractButton || o instanceof JComboBox && ((JComponent)o).isEnabled() || get(col,_editable);
    }
    @Override public void setValueAt(Object aValue, int row, int col) {
        if ( get(col,_editable)) {
            sv(aValue,row,col);
            handleActEvt(this,toStrgN(aValue) ,256*row+col);
            if ( (_opt&ChJTable.SET_VALUE_SELECTED_ROWS)!=0) {
                final JTable t=(JTable)deref(_refTable);
                final int ii[]=ChJTable.selectedRowsConverted(t);
                if (idxOf(row,ii,0,999999)>=0)
                    for(int i=sze(ii); --i>=0;) if (i!=row) sv(aValue,i,col);
            }
        }
        handleActEvt(deref(_refTable),ACTION_TABLE_CHANGED,0);
    }
    private void sv(Object aValue, int row, int col) {
        final Object data[]=get(row,_rowData,Object[].class);
        if (0<=col && col<sze(data) && !(data[col] instanceof AbstractButton || data[col] instanceof List)) data[col]=aValue;
    }
    void setJTable(JTable t) { _refTable=wref(t);}

}
