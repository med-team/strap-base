CC.ChJList;;Lists of objects
CC.ChStdout;;Stdout/Stderr
CC.ChTransferable;;Drag and Drop
CC.ChIcon;;Icons
CC.ChJTextPane;Text views;Text views
CC.ChTar;;Tar archives


CC.Customize;;Customizing ...;customizing strap<br>various settings/preferences;IC_CUSTOM
CC.DialogExecuteOnFile;;Executing shell command;shell escape
CC.ExecByRegex;;Executing shell command
CC.DialogStringMatch;;Selecting items by text patterns;multiple selections in list components using regular expressions;IC_SEARCH
CC.Hyperrefs;;Hyperlinks to Internet resources; Hyper-reference to Web resources e.g. <br>web pages<br>pubmed abstracts<br>database entries;IC_WWW
CC.InteractiveDownload;;Downloading files from the Internet;;IC_DOWNLOAD
CC.Microsoft;;Strap on Windows;;windows
CC.BasicExecutable;;External programs embedded in Strap
CC.PDB_separateChains;;Each peptide chain of the PDB-file in a separate file;;IC_SCISSOR
CC.NCBI_separateEntries;Split NCBI;Splitting a concatenated NCBI-file ...;;IC_SCISSOR





CCP.Pymol;;Protein viewer Pymol;Excellent 3D viewer by Warren Delano,  scriptable in Python;pymol;http://pymol.sourceforge.net/
CCP.Protein3d;;3D-Backbone view;IC_3D
# CCP.ChJvPROXY;;Protein viewer, under construction;jV is a protein 3d viewer;jV;http://ef-site.hgc.jp/wiki/jV/index.php/
CCP.ChAstexPROXY;;Protein viewer Astex;Astex View is a protein 3d viewer;astex;http://openastexviewer.net/web/
CCP.ChJmolPROXY;;3D-viewer Jmol; Jmol is a protein 3d viewer written in Java;IC_JMOL
CCP.Gaps2Columns;;Alignment Gaps


CCP.ProteinWriter1;;Write protein files in PDB- or Fasta-format;;IC_EDIT
# ******************
# *** Interfaces ***

CCP.CoiledCoil_Predictor;;;Predicts coiled coils from sequence;IC_COILEDCOIL
CCP.PredictSubcellularLocation;;;Predict location of a protein in mitochondrium, cytoplasm, ER ... from sequence;IC_MITOCHONDRION
CCP.SecondaryStructure_Predictor;;;Predicts secondary structure from sequence;IC_HELIX
CCP.TransmembraneHelix_Predictor;;;Predicts TM helices from sequence;IC_TM_HELIX

CCP.PhylogeneticTree;;;Draws a phylogenetic tree;IC_TREE
CCP.ProteinViewer;;;Protein 3D graphics;IC_3D

CCP.ProteinParser;;;Reading  protein file in a certain format


CCP.ProteinWriter;;;Writing protein files;IC_EDIT
CCP.ResidueSelection;;Residue Selections;;IC_UNDERLINE

CCP.SequenceAligner3D;;;Aligning sequences using 3D structure;IC_SUPERIMPOSE
CCP.SequenceAligner;;;Aligning sequences;IC_ALIGN
CCP.SequenceAlignerTakesProfile;;;Aligning sequences using sequence profile;IC_ALIGN

CCP.Superimpose3D;;;Superimposing two proteins;IC_SUPERIMPOSE
CCP.ValueOfProtein;;;A numeric value for each protein;IC_BARCHART
CCP.ValueOfResidue;;;A numeric value for each sequence position;IC_PLOT
CCS.AlignmentWriter;;;Writes protein alignments;IC_EDIT
CCP.SequenceAlignmentScore2;;;Compares two sequencs and returns a numeric value;IC_COMPARE
CCP.CompareTwoProteins;;;Compares two proteins and returns a numeric value;IC_COMPARE
CCS.StrapExtension;;;Its run method is started when the plugin is activated. It receives events because it extends StrapListener;IC_PLUGIN
CCS.ValueOfAlignPosition;;;A numeric value for each alignment position;IC_PLOT
CCP.ProteinsSorter;;;Changes the order of proteins;IC_SORT

CCB.SequenceBlaster;;;Search for similar sequences;IC_SEARCH
CCP.SimilarPdbFinder;;;Searches for a similar structure file from PDB;IC_3D

