package charite.christo;
/**
   This interface denotes objects that have  a numeric value which is a score.
    @author Christoph Gille
*/
public interface HasScore {
    float getScore();
}
