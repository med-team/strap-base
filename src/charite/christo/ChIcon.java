package charite.christo;
import java.awt.*;
import java.awt.image.*;
import java.net.URL;
import java.util.*;
import java.io.*;
import java.util.List;
import javax.swing.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;

/**HELP

<br><br><b>Drag'n Drop:</b>
   Image icons can be associated to proteins or annotations using Drag'n Drop. Click
 Drop_Web_Link* for more information.

<br><br><b>Species Icons: </b>
Organism names can be used to automatically select a suitable icon.
The following button acts on the selected proteins:
<i>BUTTON:charite.christo.strap.StrapAlign#button(CMD_SPECIES_ICONS)!</i>.

<br><br><b>Structure Icons: </b>
To associate the thumb-nail images from the PDB press this button:
<i>BUTTON:charite.christo.strap.StrapAlign#button(CMD_PDB_ICONS)!</i>.

<br><br><b>Remove: </b> The following button removes the icons of all selected proteins:
<i>BUTTON:charite.christo.strap.StrapAlign#button(CMD_DEL_ICONS)!</i>.

<br><br><b>Customize mapping of species and icons: </b> <i>CUSTOMIZE:speciesIcons!</i>

<br><br><b>List currently images: </b> <i>BUTTON:charite.christo.strap.StrapAlign#button(CMD_TABLE_ICONS)!</i>.

*/

public final class ChIcon extends RGBImageFilter implements ChRunnable {
    public final static String SFX_TRANSPARENT="@T", SFX_PRESSED="@P", SFX_CROSS_OUT="@CO", SFX_FLIPH="|", SFX_FLIPV="@FV";
    public final static int FILTER_GRAY=1, FILTER_BRIGHTER_4=2, FILTER_WHITE_TO_TRANSPARENT=3, FILTER_MONOCHROME=4;
    private static ChIcon _inst[]=new ChIcon[5];

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Filter  >>> */
    private int _filterType, _filterRGB=0xff;
    public static RGBImageFilter getFilter(int id) { return instance(id); }
    public static ChIcon instance(int id) {
        if (_inst[id]==null) (_inst[id]=new ChIcon())._filterType=id;
        return _inst[id];
    }
    public static RGBImageFilter newFilterMonochrome(int color) {
        final ChIcon i=new ChIcon();
        i._filterRGB=color;
        i._filterType=FILTER_MONOCHROME;
        return i;
    }
    @Override public int filterRGB(int x, int y, int rgb) {
        int
            b=(rgb>>0)  &255,
            g=(rgb>>8)  &255,
            r=(rgb>>16) &255;
        final int a=(rgb>>24) &255;
        if (_filterType==FILTER_WHITE_TO_TRANSPARENT) {
            return a>0xDE && g>0xDE && b>0xDE ? 0 : rgb;
         }
         if (_filterType==FILTER_GRAY) {
             final int v=(r+g+b)/3;
             return v + (v<<8) +(v<<16) +(v<<24);
         }
         if (_filterType==FILTER_BRIGHTER_4) {
             final float fBrighter=4;
             b=(int) (b*fBrighter); if (b>255) b=255;
             g=(int) (g*fBrighter); if (g>255) g=255;
             r=(int) (r*fBrighter); if (r>255) r=255;
             return b + (r<<8) +(r<<16) +(a<<24);
         }
         if (_filterType==FILTER_MONOCHROME) {
             final int s=(r+g+b)/3;
             final int f=_filterRGB;
             b=(s* ((f     )&255) )>>8;
             g=(s* ((f>>> 8)&255) )>>8;
             r=(s* ((f>>>16)&255) )>>8;
             return b + (g<<8) +(r<<16) +(a<<24);
         }
         return 0;
    }

    public static ImageIcon newFiltered(int filter, ImageIcon icn) { return newFiltered(getFilter(filter),icn);}
    public static ImageIcon newFiltered(RGBImageFilter filter, ImageIcon icn) {
        final Image i=icn==null ? null : newFiltered(filter,icn.getImage());
        return i!=null ? new ImageIcon(i) : null;
    }
    public static Image newFiltered(RGBImageFilter filter, Image i) {
        return i==null || dtkt()==null ? null : dtkt().createImage(new java.awt.image.FilteredImageSource(i.getSource(), filter));
    }
    /* <<< Filter  <<< */
    /* ---------------------------------------- */
    /* >>> Special Image >>> */
    public static Image text2image(String str, Font font, Color fg, Color bg) {
        final String ss[]=str.split("\n");
        final boolean isCursor=font==null;
        int w=0,h=0,fnSize=10;
        Font f=font;
        Dimension d=null;
        for(int iTry=4; --iTry>=0;) {
            f=font!=null ? font : getFnt(fnSize,true,Font.BOLD);
            w=8;
            for(String s: ss) w=maxi(w,4+strgWidth(f,s));
            h=charH(f)*ss.length+1;
            if (!isCursor) {
                final int hw=mini(ICON_HEIGHT, maxi(h,w));
                d=new Dimension(hw,hw);
                break;
            }
            d=dtkt()!=null ? dtkt().getBestCursorSize(w,h) : dim(16,16);
            if (w<=d.width && h<=d.height) break;
            else {
                final float scale1=d.width/(float)w, scale2=d.height/(float)h;
                fnSize=(int)(fnSize* (scale1<scale2 ? scale1:scale2));
            }
        }
        final int W=d.width, H=d.height;
        final int top= maxi(0,  (H-h)/(isCursor?1:2));
        final BufferedImage image=new BufferedImage(W,H,BufferedImage.TYPE_INT_ARGB);
        final Graphics g= image.getGraphics();

        g.setColor(C(0xff0000,0));
        if (bg!=null) {
            g.setColor(bg);
            if (str.length()==1) {
                g.fillOval(0 ,0, W,H);
            }
        }
        g.setFont(f);
        if (isCursor) {
            g.setColor(C(0));
            g.fillRoundRect(0,top,w,charH(f)*ss.length, 6,6);
            g.drawLine(0,0,3,8);
            g.drawLine(0,0,2,8);
        }
        g.setColor(fg);
        for(int i=ss.length; --i>=0;) g.drawString(ss[i],1,top+i*(charH(f)-1)+charA(f));
        return image;
    }

    private static Image imSmiley(String type, int width0, int height) {
        final String
            rmSfxPR=delSfx(SFX_PRESSED,type),
            rmSfxCO=delSfx(SFX_CROSS_OUT,type),
            rmSfxFH=delSfx(SFX_FLIPH,type),
            rmSfxFV=delSfx(SFX_FLIPV,type),
            id=
            IC_PYMOL.equals(type) ? IC_PYMOL :
            IC_UNIPROT.equals(type) ? IC_UNIPROT :
            IC_PFAM.equals(type) ? IC_PFAM :
            IC_CENTER.equals(type) ? IC_CENTER :
            IC_SEQUENCE.equals(type) ? IC_SEQUENCE :

            IC_DOTPLOT.equals(type) ? IC_DOTPLOT :
            IC_STOP.equals(type) ? IC_STOP :
            IC_TM_HELIX.equals(type) ? IC_TM_HELIX :
            IC_SUPERIMPOSE.equals(type) ? IC_SUPERIMPOSE :
            IC_COLOR.equals(type) ? IC_COLOR :
            IC_SORT.equals(type) ? IC_SORT :
            null;
        final int width=id==IC_PYMOL || id==IC_UNIPROT?64:width0;
        final Image img0=img(iicon(rmSfxPR!=type ? rmSfxPR :  rmSfxCO!=type ? rmSfxCO : rmSfxFH!=type ? rmSfxFH : rmSfxFV!=type ? rmSfxFV :
                                   id==IC_SUPERIMPOSE ? "stack_of_photos" : id==IC_TM_HELIX ? IC_HELIX : null));

        final int w0=wdth(img0), h0=hght(img0);
        if (img0!=null && (w0<=0 || h0<=0)) { assrt(); return null; }
        if (img0==null && id==null) return null;
        final BufferedImage image=new BufferedImage(img0!=null?w0:width, img0!=null?h0:height,BufferedImage.TYPE_INT_ARGB);
        final Graphics g=image.getGraphics();
        final Graphics2D g2=(Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(C(0));
        if (id==IC_PYMOL || id==IC_PFAM || id==IC_UNIPROT) {
            g.setFont( getFnt(id==IC_PFAM ? 22 : id==IC_UNIPROT ? 13 : 18, id!=IC_UNIPROT, Font.BOLD));
            final int y=(ICON_HEIGHT-charH(g))/2+charA(g);
            if (id==IC_UNIPROT) {
                g.setColor(C(0x4444FF));
                g.fillRoundRect(1,1,width-2,height-2,8,8);
                g.setColor(C(0xFFffFF));
                g.drawString("UniProt",7,y);
            } else {
            for(int j=2; --j>=0;){
                g.setColor(C(j==0?0x4444FF :0xFF4444));
                g.drawString(id==IC_PFAM ? "Pf": j==0?"Py":"PyMOL",0,y);
            }
            }
        } else if (id==IC_CENTER) {
            for(int i=4; --i>0;) {
                final int w=width*i/4, h=height*i/4;
                g.drawArc((width-w)/2, (height-h)/2, w, h, 0, 360);
            }
        } else if (id==IC_COLOR) {
            g.setColor(C(0xFF0000)); g.fillOval( 8, 2, 9,9);
            g.setColor(C(0x44FF44)); g.fillOval(18, 8, 9,9);
            g.setColor(C(0x4444FF)); g.fillOval(12,16, 9,9);
            g.setColor(C(0xAAAA00)); g.fillOval( 2,10, 9,9);
        } else if (id==IC_SORT) {
            g.setFont(new Font("",0, 8));
            final int h=(ICON_HEIGHT-4)/3,  w=(ICON_HEIGHT-8)/3;
            for(int i=0; i<3;i++) g.drawString((i+1)+".", 2+w*i, 4+charA(g)+i*h);
        } else if (id==IC_SEQUENCE) {
            for(float a=0; a<ICON_HEIGHT; a+=4) {
                final double fx=a, fy=20+ICON_HEIGHT/8*Math.cos(  3.14/ICON_HEIGHT*3/2  *a);
                final int x=(int)fx, y=(int)fy;
                g.setColor(C(0xAAaaAA));
                g.fillRect(x-1,y-1,3,3);
                g.setColor(C(0x555555));
                g.drawLine(x-1,y,x+1,y);
                g.drawLine(x,y-1,x,y+1);
                g.setColor(C(0));
                g.drawLine(x,y,x,y);
            }
        } else if (id==IC_STOP) {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            final int xx[]=new int[9], yy[]=new int[9];
            final int margin=2, w=width-2*margin, w3=w/3, w23=w*2/3;
            xx[8]=xx[0]=w3;  yy[8]=yy[0]=2;
            xx[1]=w23; yy[1]=2;
            xx[2]=w;   yy[2]=w3;
            xx[3]=w;   yy[3]=w23;
            xx[4]=w23; yy[4]=w-2;
            xx[5]=w3;  yy[5]=w-2;
            xx[6]=0;   yy[6]=w23;
            xx[7]=0;   yy[7]=w3;
            g.translate(margin,margin);
            g2.setStroke(new BasicStroke(2f));
            g.setFont(new Font("",Font.BOLD, 9));
            g.drawString("STOP", 3, (w-charH(g))/2+charA(g));
            g.drawPolygon(xx,yy,8);
            g.translate(-margin-1,-margin);
        } else if (id==IC_DOTPLOT) {
            final int ori=5, w=width-2*ori, step=w/3;
            g.drawRect(ori,ori,w, w);
            g.drawRect(ori+1,ori+1,w, w);
            for(int offset=-(w/step)*step, count=0; offset<=w; offset+=step) {
                for(int y=0; y<w; y++) {
                    final int x=y+offset;
                    if (x<0 || x>w) continue;
                    if (offset==0 ? (count++&1)==0 : ((0xF0730330>>(count++%32))&1)==0) continue;
                    g.drawLine(ori+x,ori+y,ori+x,ori+y);
                }
            }
        } else if (img0!=null) {
            if (rmSfxFV!=type) g.drawImage(img0,0,h0,w0,0,  0,0,w0,h0, imageObserver());
            else if (rmSfxFH!=type)  g.drawImage(img0,w0,0,0,h0,  0,0,w0,h0, imageObserver());
            else if (rmSfxPR!=type) {
                g.setColor(C(BG_TOGGLE_BUT));
                g.fillRect(0,0,w0,h0);
                g.setColor(C(0));
                g.drawLine(0,0,w0,0);
                g.drawLine(0,0,0,h0);
                g.setColor(C(0xFFffFF));
                g.drawLine(0,h0-1, w0,h0-1);
                g.drawLine(w0-1,h0-1,w0-1,0);
                g.drawImage(img0, (ICON_HEIGHT-w0)/2, (ICON_HEIGHT-h0)/2, imageObserver());
            } else {
                g.drawImage(img0, 0, 0, imageObserver());
                if (rmSfxCO!=type) {
                    g.setColor(C(0xFF0000));
                    for(int j=5; --j>=0;) {
                        g.drawLine(0+j,0,ICON_HEIGHT+j,ICON_HEIGHT);
                        g.drawLine(0+j,ICON_HEIGHT,ICON_HEIGHT+j,0);
                    }
                } else if (id==IC_SUPERIMPOSE) {
                    final ImageIcon i2=iicon("small_3d");
                    g.setColor(C(0xFFffFF));
                    g.fillRect(5,5,14,12);
                    if (i2!=null) g.drawImage(i2.getImage(),5,6,imageObserver());
                } else if (id==IC_TM_HELIX) {
                    g.setColor(C(0,30));
                    g.fillRect(4,0,24,32);
                }
            }
        }
        return image;
    }
    public static Image getImg(Icon ic) {
        if (ic instanceof ImageIcon) return ((ImageIcon)ic).getImage();
        if (ic==null||dtkt()==null) return null;
        if (_forIcon2image==null) _forIcon2image=new JComponent[]{new ChButton(),new JMenuItem()};
        final BufferedImage im=new BufferedImage(wdth(ic),hght(ic),  BufferedImage.TYPE_INT_ARGB);
        for(int i=0; i<_forIcon2image.length; i++) {
            try {
                ic.paintIcon(_forIcon2image[i],im.getGraphics(),0,0);
                break;
            } catch(Exception e){}
        }
        return im;
    }
    private static JComponent[] _forIcon2image;
    public static Image getScaledInstance(Image im, int w, int h, String fileExt,Color bg) {
        if (im==null) return null;
        final BufferedImage scaled=new BufferedImage(w,h, ".jpg".equalsIgnoreCase(fileExt) || ".jpeg".equalsIgnoreCase(fileExt) ?  BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB);
        final Graphics g=scaled.getGraphics();
        g.setColor(bg!=null ? bg: C(0,0));
        g.fillRect(0,0,w,h);
        g.drawImage(im, 0,0, w,h, 0,0,  wdth(im), hght(im), imageObserver());

        return scaled;
    }
    /* <<< Special Images <<< */
    /* ---------------------------------------- */
    /* >>> Image Factory >>> */

    static ImageIcon findIcon(String name) {
        final String s=delSfx(SFX_TRANSPARENT,name);
        if (sze(s)==0 || dtkt()==null) return null;
        {
            final Image im=imSmiley(s, ICON_HEIGHT, ICON_HEIGHT);
            if (im!=null) return new ImageIcon(im);
        }
        final int L=s.length();
        ImageIcon im=null;
        final boolean slash=s.indexOf('/')>0;
            final BA SB=new BA(99);
        for(int iExt=3; --iExt>=0;) {
            final String ext=iExt==2? ".png":iExt==1?".gif":".jpg";
            SB.clr();
            if (!slash) SB.a("charite/christo/myIcons/");
            SB.a(s).a(ext);
            final URL u=rscAsURL(0L, toStrg(SB));
            if (u==null) continue;
            try {im=new ImageIcon(u); } catch(Exception ex){ stckTrc(ex);}
            if (im!=null) break;
        }

        if (im==null) {
            final int iH= prev(-DIGT,s,L- 1 ,-1)+1;
            int width=-1,height=-1;
            String s0=s;
            if (chrAt(iH-1,s)=='x' && (height=atoi(s,iH))>0) {
                final int iW=prev(-DIGT,s,iH-2,-1)+1;
                s0=s.substring(0,iW);
                if(iW>=0) width=atoi(s,iW);
            }

            if (s0.length()==0) {
                im=width>0 && height>0?new ImageIcon(new BufferedImage(width ,height,BufferedImage.TYPE_INT_ARGB_PRE)):null;
            } else if (s0.length()<L) {
                final ImageIcon ic0=iicon(s0);
                if (ic0!=null) {
                    Image i0=ic0.getImage();
                    final boolean jpg=ic0.getDescription().endsWith(".jpg");
                    if (width>0 && height>0) {
                        i0=getScaledInstance(i0,width,height,jpg?".jpg":"",jpg?C(0xFFffFF):null);
                    }
                    im=new ImageIcon(i0);
                }
            }
        }
        if (s==IC_3D || s==IC_JMOL || name.endsWith(SFX_TRANSPARENT)) {
            im=newFiltered(getFilter(FILTER_WHITE_TO_TRANSPARENT),im);
        }
        return im;
    }
    public static void drawImage(Graphics g,Image im,int x, int y,int w,int h, AlphaComposite com,ImageObserver obs) {
        if (g==null) return;
        final Graphics2D g2d=(Graphics2D)g;
        final Composite originalComposite = g2d.getComposite();
        g2d.setComposite(com);
        g2d.drawImage(im,x,y,w,h,obs);
        g2d.setComposite(originalComposite);
    }
    /* <<< Image Factory <<< */
    /* ---------------------------------------- */
    /* >>> Cursor >>> */
    private static Image _grayBall;
    private static AlphaComposite _ac[];
    public static AlphaComposite getAlphaComposite(int percent) {
        if (_ac==null) _ac=new AlphaComposite[101];
        return _ac[percent]==null ?  _ac[percent]=AlphaComposite.getInstance(AlphaComposite.SRC_OVER, percent*.01f) : _ac[percent];
    }
    public static void drawCursor(Graphics g,int x, int y,int w, int h, ImageObserver obs) {
        final ImageIcon ic=iicon(IC_LED_GREEN16);
        if (ic!=null) drawImage(g,ic.getImage(),x,y,w,h, getAlphaComposite(70),obs);
    }
     public static Image coloredBall(Color color, Component observer) {
        if (_grayBall==null){
            final ImageIcon icCursor=iicon(IC_LED_GREEN16);
            if (icCursor==null) return null;
            _grayBall=newFiltered(FILTER_GRAY,icCursor).getImage();
        }
        final Component obs=observer!=null?observer:imageObserver();

        if (color==null || obs==null) return null;
        return waitFor(obs.createImage(new java.awt.image.FilteredImageSource(_grayBall.getSource(), newFilterMonochrome(color.getRGB()))), obs);
    }
    private static Map<String,Cursor> _mapCursor;
    public static Cursor getCursor(String str, Component obs) {
        if (str==null||dtkt()==null) return null;
        if (_mapCursor==null) _mapCursor=new java.util.HashMap();
        Cursor c=_mapCursor.get(str);
        if (c==null) {
            try{
                final ImageIcon icon=iicon(str);
                final Image image;
                final Graphics g;
                if (icon!=null) {
                    final int h=hght(icon), w=wdth(icon);
                    if (h<1 && w<1) return null;
                    final Dimension d=dtkt().getBestCursorSize(w,h);
                    image=new BufferedImage(d.width,d.height,BufferedImage.TYPE_INT_ARGB);
                    g=image.getGraphics();
                    g.drawImage(icon.getImage(),4,0,obs);
                } else {
                    image=text2image(str, null, C(0xFFffFF),C(0));
                }
                c=dtkt().createCustomCursor(image, str==IC_EDIT ? new Point(5,isWin() ? 20+8 : isWin() ? 20+8 : 20) :  new Point(0,0),"");
            } catch(Exception e){ c=cursr('H'); stckTrc(e); }
            _mapCursor.put(str,c);
        }
        return c;
    }
    /* <<< Cursor  <<< */
    /* ---------------------------------------- */
    /* >>> Utils >>> */
    private static Component _imObs;
    public static Component imageObserver() {
        if (_imObs==null) _imObs=dtkt()!=null ? new Frame() : new ChButton();
        return _imObs;
    }
    public static void setImageObserver(Component c) { _imObs=c; }

    private static Image waitFor(Image im,Component comp) {
        if (im!=null) {
            final MediaTracker mt=new MediaTracker(comp);
            mt.addImage(im,0);
            try { mt.waitForAll(); } catch(Exception e) {}
        }
        return im;
    }

   /* <<< Utils  <<< */
    /* ---------------------------------------- */
    /* >>> Download >>> */
   private static int _imgLoading;
    private static List<String> _imgScheduled;
    private static long _imgLoadTime;
    private final static Map<String, ChRunnable[]> _imgMapTargets=new HashMap();
    private final static Map<String,Object> _imgMap=new WeakHashMap();
    public static void requestImage(String fileOrURL, ChRunnable target) {
        if (sze(fileOrURL)==0) return;
        synchronized(_imgMap) {
            final Object imO=deref(_imgMap.get(fileOrURL));
            if (imO==ERROR_OBJECT) return;
            addCP(fileOrURL, target, _imgMapTargets);
            if (imO=="") { imgToTarget(fileOrURL); return; }
            final Image im=deref(imO,Image.class);
            final File f;
            if (im!=null && target!=null) target.run(ChRunnable.RUN_SET_ICON_IMAGE,im);
            else if (sze(f=file(fileOrURL))>0) imgLoad(fileOrURL, f);
            else {
                _imgMap.put(fileOrURL,"");
                if (_imgScheduled==null) {
                    _imgScheduled=new ArrayList();
                    ChThread.callEvery(0, 222,thrdCR(instance(0),"START_DOWNLOAD_IMAGE") ,"START_DOWNLOAD_IMAGE");
                }
                _imgScheduled.add(fileOrURL);
            }
        }
    }
    private static void imgLoad(String fileOrURL, File f) {
        Image im=null;
        if (sze(f)>0) {
            try {
                im=dtkt().getImage(toStrg(f));
            } catch(Throwable e){ putln(e," ChIcon ",f); }
        }
        synchronized(_imgMap) { _imgMap.put(fileOrURL, im==null?ERROR_OBJECT:wref(im)); }
        if (im!=null) imgToTarget(fileOrURL);
    }
    private static void imgToTarget(String s) {
        if (s==null) return;
        Image img=null;
        synchronized(_imgMap) {  img=deref(_imgMap.get(s), Image.class); }
        if (img!=null) {
            synchronized(_imgMap) {
                final Object rr[]=_imgMapTargets.get(s);
                _imgMapTargets.put(s,null);
                runCR(rr, ChRunnable.RUN_SET_ICON_IMAGE, img);
            }
        }
    }

    private final static Map<String,Object>_mapEx=new HashMap();
    public static File exeForApp(String application) {
        synchronized(_mapEx) {
            final Object o=_mapEx.get(application);
            if (o==ERROR_OBJECT) return null;
            File f=deref(o,File.class);
            if (f==null) {
                if (isWin() && Insecure.EXEC_ALLOWED) {
                    final ChExec ex=new ChExec(ChExec.STDOUT|ChExec.WITHOUT_ASKING)
                        .setCommandLineV("SH","REG QUERY", "\"HKEY_CLASSES_ROOT\\Applications\\"+application+".exe\\shell\\edit\\command\\\"");
                    ex.run();
                    final BA ba=ex.getStdoutAsBA();
                    final int quote0=strchr('"', ba), quote1=strchr('"', ba,quote0+1,MAX_INT);
                    if (quote1-quote0<=0) return null;
                    f= file(ba.newString(quote0+1, quote1));
                }
                _mapEx.put(application, orO(f,ERROR_OBJECT));
            }
        return f;
        }
    }

    public Object run(String id,Object arg) {
        final Object argv[]=arg instanceof Object[] ? (Object[])arg:null;
        if (id=="START_DOWNLOAD_IMAGE") {
            if (System.currentTimeMillis()-_imgLoadTime> 1000*30) _imgLoading=0;
            if (_imgLoading<=0) {
                synchronized(_imgMap) {
                    final String s=getS(0, _imgScheduled);
                    remov(0,_imgScheduled);
                    if (s!=null) startThrd(thrdCR(this,"DOWNLOAD_IMAGE", s));
                }
            }
        }
        if (id=="DOWNLOAD_IMAGE") {
            final String fileOrURL=(String)arg;
            final File f=file(fileOrURL);
            _imgLoadTime=System.currentTimeMillis();
            _imgLoading++;
            if (!looks(LIKE_EXTURL,fileOrURL) || getContentLen(url(fileOrURL))>0) downloadAndDecompress(fileOrURL,f);
            if (--_imgLoading<0) _imgLoading=0;
            imgLoad(fileOrURL,f);
        }
        return null;
    }
}

// --- Check ---
//   A=`cat $HOME/java/charite/christo/icon_java | fgrep public | fgrep -v fgrep | fgrep 'return gif("' | sed 's|^.*("||1;s|").*$|.gif|1'`
//
