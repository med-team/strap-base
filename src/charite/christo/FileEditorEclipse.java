package charite.christo;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.*;
import static charite.christo.ChUtils.*;
/**
Opens a file in Eclipse.  Interprocess communication via port.  Using
the Eclipse plugin written by Luca Barbieri.
*/
public class FileEditorEclipse implements ChRunnable ,FileEditor{
    //java charite.christo.FileEditorEclipse ~/java/test/t.java

    private static Map<String,OutputStream> mapStream=new HashMap();
    public boolean editFile(File file, int lineNumber) {
        final Set<String> vWorkspaces=new HashSet(),vAddresses=new HashSet();
        for(File f: lstDirF(file("~/.eclipse-server-workspaces"))) {
            final String ss[]=readLines(f);
            if (ss==null) continue;
            for(String s:ss) {
                if (s.startsWith("file:")) vWorkspaces.add(s);
                final String address=toStrg(readBytes(s+"/.metadata/.plugins/lb.server/.address"));
                if (address!=null && 0<sze(address.trim())) vAddresses.add(address);
            }
        }
        //putln(vWorkspaces);
        //putln(vAddresses);
        boolean success=false;
        for(String address : strgArry(vAddresses)) {
            OutputStream stream=mapStream.get(address);
            Thread thread=null;
            try{
                if (stream==null) {
                    final int colon=address.indexOf(":");
                    final Socket socket=new Socket(address.substring(0,colon), atoi(address,colon+1));
                    //putln("FileEditorEclipse created "+socket);
                    mapStream.put(address,stream=socket.getOutputStream());
                    startThrd(thread=thrdCR(this,"READ",socket));
                }
               final Object arg[][]={
                    {"listenClose","1"},
                    {"file",file},
                    {"line",intObjct(lineNumber>0 ? lineNumber :0)}
                };
                execute("lb.server.ui.open",arg,stream);
                execute("lb.server.ui.raise",null,stream);
                success=true;
            } catch(Exception e) {
               stckTrc(e);
                closeStrm(stream);
                if (thread!=null) thread.interrupt();
                mapStream.remove(address);
            }
        }
        if (!success && file.exists()) {
            final String
                wget=isMac() ? "curl -O  " : "wget -N ",
                msg=
                "Failed to connect to running Eclipse instance.<br> \n"+
                "If you do not use Eclipse then comment-out \"ECLIPSE\" in the customize dialog <i>CUSTOMIZE:javaSourceEditor!</i>. \n\n"+
                "<h2>Preparing Eclipse</h2>\n"+
                "Copy the two jar-files into the \"plugins\"-directory of Eclipse.\n\n"+
                "<pre class=\"terminal\">\n\n"+
                "  SRC="+ChConstants.URL_STRAP_JARS+"\n\n"+
                "# Specify the path to the eclipse plugins directory!\n"+
                "  DIR=/local/java/eclipse/plugins/\n\n "+
                wget+" $SRC/lb.server.ui_1.0.0.jar  -O $DIR/lb.server.ui_1.0.0.jar  \n\n "+
                wget+" $SRC/lb.server_1.0.0.jar -O $DIR/lb.server_1.0.0.jar\n\n"+
                "</pre>"+
                "Then restart Eclipse.<br>";
            new ChJTextPane(msg).tools().showInFrame("");
            error(msg);
        }
        return success;
    }

    public Object run(String id,Object arg) {
        if (id=="READ") {
            final Socket socket=(Socket)arg;
            InputStream is=null;
            try {
                //putln("going to open ");
                is=socket.getInputStream();
                //putln("going to open fertig");
                while(true) {
                    final int read=is.read();
                    //putln("read "+(char)read);
                }
            } catch(Exception e){ closeStrm(is);}
            //putln("READ_DONE");
        }
        return null;
    }

    public static void execute(String cmd, Object[][] args,OutputStream stream) throws IOException {
        final Charset UTF8 = Charset.forName("UTF-8");
        final ByteBuffer cmdb = UTF8.newEncoder().encode(CharBuffer.wrap(cmd));
        stream.write(cmdb.array(), cmdb.arrayOffset(), cmdb.limit());
        if(args != null) {
            for(Object[] arg : args) {
                //putln("arg={"+arg[0]+","+arg[1]+"}");
                final ByteBuffer valb = UTF8.newEncoder().encode(CharBuffer.wrap(arg[1].toString()));
                final ByteBuffer argb = UTF8.newEncoder().encode(CharBuffer.wrap(" " + arg[0] + " \\U" + Integer.toString(valb.limit(),16) + ":"));
                stream.write(argb.array(), argb.arrayOffset(), argb.limit());
                stream.write(valb.array(), valb.arrayOffset(), valb.limit());
            }
        }
        stream.write('\n');
        stream.flush();
    }
}

/*
/laura/people/christo/workspace2/.metadata/.plugins/lb.server/.address
cmd=lb.server.ui.open
args={listenClose=1, file=/laura/people/christo/java/test/T.java, line=0}
cmd=lb.server.ui.raise
*/
