package charite.christo;
import java.io.File;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
public class ChComparator  implements Comparator {
    private final int _type;
    public ChComparator(int type){ _type=type;}

    public int compare(Object o1,Object o2) {
        if (o1==null && o2==null) return 0;
        if (o1==null) return 1;
        if (o2==null) return -1;
        final File f1= o1 instanceof File ? (File)o1:null;
        final File f2= o2 instanceof File ? (File)o2:null;
        final int type=_type;
        if (f1!=null && f2!=null) {
            final boolean d1=f1.isDirectory(), d2=f2.isDirectory();
            if (d1!=d2) return  d1 ? -1 : 1;
        }
        if (type==COMPARE_WHEN_MODIFIED) {
            final long t1=o1 instanceof WhenCreatedWhenModified ? ((WhenCreatedWhenModified)o1).whenModified() : f1!=null ? f1.lastModified() : 0;
            final long t2=o2 instanceof WhenCreatedWhenModified ? ((WhenCreatedWhenModified)o2).whenModified() : f2!=null ? f2.lastModified() : 0;
            return t1<t2 ? -1 : t1>t2 ? 1 : 0;
        }
        if (type==COMPARE_WHEN_CREATED) {
            final long t1=o1 instanceof WhenCreatedWhenModified ? ((WhenCreatedWhenModified)o1).whenCreated() : 0;
            final long t2=o2 instanceof WhenCreatedWhenModified ? ((WhenCreatedWhenModified)o2).whenCreated() : 0;
            return t1<t2 ? -1 : t1>t2 ? 1 : 0;
        }
        if (type==COMPARE_SIZE) {
            final int  s1=sze(o1);
            final int  s2=sze(o2);
            return s1<s2 ? -1 : s1>s2 ? 1 : 0;
        }

        if (type==COMPARE_REVERT) {
            final int i1=atoi(gcp(KEY_DEFAULT_ORDER,o1));
            final int i2=atoi(gcp(KEY_DEFAULT_ORDER,o2));
            final int result=i1<i2?-1:i1>i2?1:0;
            if (result!=0) return result;
        }

        if (type==COMPARE_ALPHABET || type==COMPARE_ALPHABET_IC) {
            return compareAlphabetically(o1,o2, type==COMPARE_ALPHABET_IC );
        }

        if (type==COMPARE_NAME || type==COMPARE_ID || type==COMPARE_RENDERER_TEXT) {
            return compS(type, o1,o2);
        }
        return 0;
    }

    private static String getS(int type, Object o) {
        if (type==COMPARE_NAME) return nam(o);
        if (type==COMPARE_ID) return getID(o);
        if (type==COMPARE_RENDERER_TEXT) {
            CharSequence s=rendererTxt(o);
            if (s==null) s=dItem(o);
            if (s==null) s=nam(o);
            return toStrg(s);
        }
        return null;
    }

    private static int compS(int type, Object o1,Object o2) {
        return compS(getS(type, o1), getS(type, o2));

    }

    private static int compS(String s1,String s2) {
        if (s1==null && s2==null) return 0;
        if (s1==null) return -1;
        if (s2==null) return 1;
        return s1.compareTo(s2);
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    public final static String KEY_DEFAULT_ORDER="ChC$$KO";
    private static Set _vSortable;
    public static void setSortable(Object o) {
        if (_vSortable==null) _vSortable=new ChSet(ChSet.WEAK_REFERENCE, Object.class);
        if (o!=null) _vSortable.add(o);
    }
    public static void sortTreeNodes(Object o, int type) {
        if (o==null || _vSortable==null || !_vSortable.contains(o)) return;
        if (! (o instanceof List || o instanceof Object[])) return;

        for(int i=sze(o); --i>=0;) {
            final Object el=get(i,o);
            if (null==gcp(KEY_DEFAULT_ORDER,el)) pcp(KEY_DEFAULT_ORDER,intObjct(i), el);
        }
        sortArry(o, comparator(type));
    }

    static int compareAlphabetically(final Object object1, final Object object2, boolean ignoreCase) {
        Object o1=object1 instanceof ChRunnable ? ((ChRunnable)object1).run(PROVIDE_TEXT_FOR_SORTING,null) : object1 instanceof File ? toStrg(object1) : null;
        Object o2=object2 instanceof ChRunnable ? ((ChRunnable)object2).run(PROVIDE_TEXT_FOR_SORTING,null) : object2 instanceof File ? toStrg(object2) : null;

        if (o1==null) o1=object1;
        if (o2==null) o2=object2;

        if (o1 instanceof String && o2 instanceof String) {
            final String s1=(String)o1;
            final String s2=(String)o2;
            return ignoreCase ? String.CASE_INSENSITIVE_ORDER.compare(s1,s2) : s1.compareTo(s2);
        }
        final byte bb1[]=o1 instanceof byte[] ? (byte[])o1 : null;
        final byte bb2[]=o2 instanceof byte[] ? (byte[])o2 : null;
        final CharSequence cs1=o1 instanceof CharSequence ? (CharSequence)o1 : null;
        final CharSequence cs2=o2 instanceof CharSequence ? (CharSequence)o2 : null;

        final int L1=bb1!=null ? bb1.length : cs1!=null ? cs1.length() : 0;
        final int L2=bb2!=null ? bb2.length : cs2!=null ? cs2.length() : 0;
        if (L1==0) return -L2;
        if (L2==0) return 1;
        final int L=mini(L1,L2);
        for(int i=0; i<L; i++) {
            int c1=bb1!=null ? bb1[i]: cs1.charAt(i);
            int c2=bb2!=null ? bb2[i]: cs2.charAt(i);
            if (ignoreCase) {
                if ('A'<=c1 && c1<='Z') c1|=32;
                if ('A'<=c2 && c2<='Z') c2|=32;
            }
            if (c1==0 && c2==0) return 0;
            if (c1<c2) return -1;
            if (c1>c2) return 1;
        }
        return L1<L2 ? -1 : L1>L2 ? 1 : 0;
    }

}
