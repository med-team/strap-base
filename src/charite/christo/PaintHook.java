package charite.christo;
public interface PaintHook {

    /**
       @param after This method is called once before and once after  super.paintComponent.
       @return means do not call super.paintComponent
    */
    boolean paintHook(javax.swing.JComponent c,java.awt.Graphics g, boolean after);
}
