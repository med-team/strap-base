package charite.christo;
import javax.swing.*;
import java.util.*;
import java.util.List;
import javax.swing.tree.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import static java.awt.event.MouseEvent.*;
/**
   @author Christoph Gille
*/
public class ChJTree extends JTree implements HasWeakRef  {
    public final static String KEY_NUM_EXPAND="CT$$NE", KEY_EXPAND="CT$$KE";
    private boolean _painted;
    private final long _options;
    public ChJTree(long options, TreeModel m) {
        _options=options;
        if (m!=null) setModel(m);
        if ((ChJTable.DEFAULT_RENDERER&options)!=0) setCellRenderer(new ChRenderer());
        pcp(KOPT_TRACKS_VIEWPORT_WIDTH,"",this);
        adSelListener(this);
        if ( (options&ChJTable.DRAG_ENABLED)!=0) setDragEnabled(true);
    }

    @Override public boolean getScrollableTracksViewportWidth() {  return gcp(KOPT_TRACKS_VIEWPORT_WIDTH,this)!=null; }
    private final Object WEAK_REF=newWeakRef(this);
    public Object wRef() { return WEAK_REF; }

    /* ---------------------------------------- */
    /* >>> Event >>> */
    { this.enableEvents(ENABLE_EVT_MASK);}
    @Override public void processEvent(java.awt.AWTEvent ev) {
        final int id=ev.getID();
        if (isPopupTrggr(false,ev)) {
            if (isPopupTrggr(true,ev)) {
                final ChRunnable r=gcp(PROVIDE_JPopupMenu,this, ChRunnable.class);
                final JPopupMenu pop=r==null?null: deref(r.run(PROVIDE_JPopupMenu, ev), JPopupMenu.class);
                shwPopupMenu(pop);
            }
        } else {
            mayClosePopupMenu(ev);
            if (!shouldProcessEvt(ev)) return;
            if (0!=(_options&ChJTable.DRAG_ENABLED) && jListDoDnD(ev)) return;
            if (_needsUpdate || id==MOUSE_ENTERED) updateKeepState(0L);
        }
        try {super.processEvent(ev);} catch(Throwable e){}
    }
    /* <<< Event <<< */
    /* ---------------------------------------- */
    /* >>> Paint >>> */
    {pcp(KEY_NOT_YET_PAINTED,"",this);}
    @Override public void paintComponent(java.awt.Graphics g) {
        if (!_painted) {
            _painted=true;
            pcp(KEY_NOT_YET_PAINTED,null,this);
            for(int i=atoi(gcp(KEY_NUM_EXPAND,this)); --i>=0;) expandAllNodes(null,this);
        }

        if (_needsUpdate) updateKeepState(0L);
        if (paintHooks(this, g, false)) try {super.paintComponent(g);}catch(Throwable t){stckTrc(t);}
        paintHooks(this, g, true);
    }
    @Override public void updateUI() { try { super.updateUI();} catch(Exception e){stckTrc(e);}  }
    /* <<< Paint <<< */
    /* ---------------------------------------- */
    /* >>> Tip >>> */
    @Override public String getToolTipText(java.awt.event.MouseEvent ev) {
        return ballonMsg(ev, super.getToolTipText());
    }
    { rtt(this);}
    /* <<< Tip <<< */
    /* ---------------------------------------- */
    /* >>> Save State  >>> */
    private final Set<String> vExpanded=new HashSet();
    private TreePath[] selectionPaths;
    private void saveState() {
        vExpanded.clear();
        for(int row=getRowCount(); --row>=0;) {
            final TreePath path=getPathForRow(row);
            if (isExpanded(path)) vExpanded.add(path2String(path));
        }
        this.selectionPaths=getSelectionPaths();
    }
    private void applySavedState() {
        for(int depth=4; --depth>=0;) {
            boolean changed=false;
            for(int row=getRowCount(); --row>=0;) {
                final TreePath path=getPathForRow(row);
                if (isExpanded(path)) continue;
                if (vExpanded.contains(path2String(path))) {
                    changed=true;
                    setExpandedState(path,true);
                }
            }
            if (!changed)break;
        }
        if (sze(selectionPaths)>0) setSelectionPaths(selectionPaths);
    }
    private final WeakHashMap<Object,String> mapPath=new WeakHashMap();
    private BA _sb;
    private String path2String(TreePath path) {
        String o=mapPath.get(path);
        if (o==null) {
            if (_sb==null) _sb=new BA(99);
            path2String(path,_sb.clr());
            o=_sb.toString();
            mapPath.put(path,o);
        }
       return o;
    }
    private static void path2String(TreePath path, BA SB) {
        for(TreePath p=path; p!=null; p=p.getParentPath()) {
            final Object o=p.getLastPathComponent();
            final Object nodeId=runCR(o, PROVIDE_JTREE_NODE_ID,null);
            if (nodeId!=null) SB.a(nodeId);
            else SB.a(ChRenderer.hashC(o));
            SB.a('/');
        }
    }
    /* <<< Save State <<< */
    /* ---------------------------------------- */
    /* >>> Update JTree >>> */
    private boolean _needsUpdate;
    private Runnable _threadUpdate;
    public void updateKeepStateLater(long options, int ms) {
        _needsUpdate=true;
        inEDTms(thread_updateKeepState(options),ms);
    }
    public Runnable thread_updateKeepState(long options) {
        if (_threadUpdate==null) _threadUpdate=thrdM("updateKeepState", this, new Object[]{longObjct(options)});
        return _threadUpdate;
    }
    private long _sumChildsHC;

    public void updateKeepState(long options) {
        final long sumChildsHC=sumOfChildsHashValues();
        final boolean hashChanged=_sumChildsHC!=sumChildsHC;
        _sumChildsHC=sumChildsHC;
        if (hashChanged) {
            final TreeModel m=getModel();
            _needsUpdate=false;

            saveState();
            if (m instanceof ChTreeModel)  {
                ((ChTreeModel)m).dispatch(ChTreeModel.DISPATCH_CHANGED,new javax.swing.event.TreeModelEvent(m, new TreePath(new Object[]{m.getRoot()})));
            }
            revalidate();
            applySavedState();
        } else {
            if (ListOrTreeNeedsRepaint.treeNeedsRepaint(this)) {
                ChDelay.repaint(this,222);
            }
        }

        final Object toBeExpanded=gcp(KEY_EXPAND,this);
        if (toBeExpanded!=null) expandAllNodes(toBeExpanded,this);

        pcp(KEY_EXPAND,null,this);
        //putln("updateKeepState time="+(System.currentTimeMillis()-time));
    }
    public static void updateTreeLater(long options, JTree t, int ms) {
        if (t instanceof ChJTree) ((ChJTree)t).updateKeepStateLater(options, ms);
    }
    /* <<< update tree <<< */
    /* ---------------------------------------- */
    /* >>> Private long hash >>> */
    private long sumOfChildsHashValues() {
        long sum=0;
        int toLen=0, shift=0;
        final TreeModel m=getModel();
        boolean rootDone=false;
        for(int i=getRowCount(); --i>= -1;) {
            final TreePath tp=i>=0 ? getPathForRow(i) :  null;
            final Object parent;
            if (tp==null) {
                if (rootDone) continue;
                rootDone=true;
                parent=m.getRoot();
            } else parent=tp.getLastPathComponent();
            final int pathCount=tp==null ? 1 : tp.getPathCount();
            if (pathCount<toLen || rootDone) {
                final Object childs[]=parent instanceof UniqueList ? ((UniqueList)parent).asArray() : null;
                for(int iChild=childs!=null? childs.length : m.getChildCount(parent); --iChild>=0;) {
                    final Object child=childs!=null ? childs[iChild] : m.getChild(parent,iChild);
                    sum+=(ChRenderer.hashC(child)<<(shift++%32));
                }
            }
            toLen=pathCount;
        }
        return sum;
    }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Util >>> */
    public static void expandAllNodes(Object object, JTree t) {
        final TreeModel m=t==null?null:t.getModel();
        if (m==null) return;
        final Object objects[]= object instanceof Object[] ? (Object[])object:null;
        while(true) {
            final int rc= maxi(m.getChildCount(m.getRoot()), t.getRowCount());
            for(int i=rc; --i>=0;) {
                final TreePath tp=t.getPathForRow(i);
                if (tp==null || t.isExpanded(tp)) continue;
                final Object pc=object==null ? null : tp.getLastPathComponent();
                if (object==pc || objects!=null && cntains(pc,objects)) {
                    t.expandPath(tp);
                }
            }
            //if (!changed) break;
            break;
        }
    }
    public void sort(int type) {

        final int selRows[]=getSelectionRows();
        for(int onlySelected=2; --onlySelected>=0;) {
            int count=0;
            for(int i=getRowCount(); --i>=0;) {
                final TreePath tp=getPathForRow(i);
                if (!isExpanded(tp)) continue;
                if (onlySelected>0 && idxOf(i,selRows,0, MAX_INT)<0) continue;
                final Object o0=tp.getLastPathComponent();
                ChComparator.sortTreeNodes(ChTreeModel.rplc(o0), type);
                count++;
            }
            if (count>0) break;
        }
        updateKeepState(0L);
    }

    /* <<< util <<< */
    /* ---------------------------------------- */
    /* >>> DnD  >>> */
    public Object getDndDateien() { return dndV(false,this);}

}
