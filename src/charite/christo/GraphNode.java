package charite.christo;
import java.awt.*;
/**
   A node for a graph
   @author Christoph Gille
   @see GraphEdge
   @see GraphSun
*/
public final class GraphNode {
    private final Object _object;
    public GraphNode(Object o) {  _object=o;}
    public double x, y, dx, dy;
    public boolean isFixed() { return false;}
    public Image image() { return ChUtils.img(_object); }
    public Object component() { return _object;};
    public String text() { return _object!=null ?  _object.toString() : null;}
    @Override public String toString() { return ChUtils.toStrgN(_object);}
}
