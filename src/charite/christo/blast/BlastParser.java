package charite.christo.blast;
import java.util.*;
import java.io.*;
import charite.christo.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**

<BlastOutput_db>pdb</BlastOutput_db>
<Hit_id>gi|294979282|pdb|2KU1|A</Hit_id>
<Hit_accession>3UNB_A</Hit_accession>

<Hit_id>gi|297844574|ref|XP_002890168.1|</Hit_id>
<Hit_accession>XP_002890168</Hit_accession>

*/
public class BlastParser {
    private boolean _writeNew;
    private int _countNovelHits, _countHits;
    public int countNovelHits() { return _countNovelHits;}
    public int countHits() { return _countHits;}
    private BA _baWhen;
    public void setDay(BA ba,boolean isNew) { _baWhen=ba; _writeNew=isNew;}

    private static String s(byte txt[],int from,int endText,char term) {
        if (from<0) return "error";
        final int to=strchr(term,txt,from,endText);
        if (from<0 || from>=to || to>txt.length) return "error";
        return bytes2strg(txt,from,to);
    }

    public boolean parseXML(BA ba, int width, BlastResult blastResult) { return parseXML(ba,width,blastResult,null); }
    public boolean parseXML(BA ba, int width, OutputStream fOut) { return parseXML(ba,width, new BlastResult(null),fOut); }
    private boolean parseXML(BA ba, int width, BlastResult blastResult, OutputStream fOut) {
        final byte[] T=ba.bytes();
        final int B=ba.begin();
        final boolean isNcbi=strstr("BlastOutput_iterations",ba)>0;
        final int E=strstr(STRSTR_AFTER|STRSTR_E,isNcbi ? "</Iteration_hits" : "</hits",T, B, ba.end());
        int headerE=0;
        final String replaceDB;
        if (isNcbi) {
            final int prgS=strstr(STRSTR_AFTER,"<BlastOutput_program>",T, B, E);
            if (prgS>0) blastResult.setProgramName(s(T,prgS, E,'<'));
            final int dbS=strstr(STRSTR_AFTER,"<BlastOutput_db>",T, B, E);
            final String db=dbS>0 ? uCase(s(T,dbS, E,'<')) : "error";
            replaceDB=
                strstr(STRSTR_w,db,"NR REFSEQ_PROTEIN PAT")>=0 ||  endWith("_NR",db) ? "NCBI_AA" :
                strstr(STRSTR_w,db,"EST CHROMOSOME REFSEQ_RNA REFSEQ_DNA REFSEQ_GENOMIC")>=0 ? "NCBI_NT" :
                "SWISSPROT".equals(db) ? "UNIPROT" :
                db;

            blastResult.addDatabase(new BlastDatabase(replaceDB, ""));
        } else {
            final int headerS=strstr(STRSTR_AFTER,"<Header",T,B,E);
            headerE=strstr(STRSTR_AFTER,"</Header",T,headerS,E);
            final int prgS=strstr(STRSTR_AFTER,"<program ",T,headerS,headerE);
            blastResult.setProgramName(s(T,strstr(STRSTR_AFTER,"name=\"",T,prgS,headerE),headerE,'"'));
            blastResult.setProgramVersion(s(T,strstr(STRSTR_AFTER,"version=\"",T,prgS,headerE), headerE,'"'));
            final int dbS=strstr(STRSTR_AFTER,"<database ",T,headerS,headerE);
            final String db=s(T,strstr(STRSTR_AFTER,"name=\"",T,dbS,headerE) ,headerE,'"');
            final int created=strstr(STRSTR_AFTER,"created=\"",T,dbS,headerE);
            blastResult.addDatabase(new BlastDatabase(db, created<0?"":s(T,created,headerE,'"')));
            replaceDB=sze(db)>0 && 0>strstr(STRSTR_AFTER,"<database ",T,dbS+1,headerE) ? uCase(db) : null;
            if (replaceDB!=null) addToProteinDatabases(URL_EBI, new String[]{replaceDB});
        }

        int hitE=headerE;
        final BA sbOut=fOut!=null ? new BA("") : null;
        try {
            if (fOut!=null) blastResult.writeFile(fOut,width);
        } catch(IOException ioex) {stckTrc(ioex); }
        final int currentDay=(int)(System.currentTimeMillis()/(1000*24*60*60)), whenEnds[];
        final byte[] whenBytes;
        {
            final BA a=_baWhen;
            if (a!=null) {
                whenEnds=a.eol();
                whenBytes=a.bytes();
            } else { whenEnds=null; whenBytes=null;}
        }
        while(true) {
            final int hitS=strstr(STRSTR_AFTER,isNcbi ? "<Hit>": "<hit ",T,hitE,E);
            if (hitS<0) break;
            hitE=strstr(STRSTR_AFTER,isNcbi ? "</Hit>": "</hit>",T,hitS,E);
            if (hitE<0) break;
            final int descriptionS=strstr(STRSTR_AFTER,isNcbi ? "<Hit_def>" : "description=\"", T,hitS,hitE);
            final int descriptionE=strstr(isNcbi ? "</Hit_def>" : "\">",T,descriptionS+1, E);
            final int databaseS=strstr(STRSTR_AFTER, "database=\"", T,hitS,hitE); // !!!!!
            final BlastHit hit=new BlastHit();
            if (descriptionS>0 && descriptionE>descriptionS) {
                if (strchr('&',T,descriptionS,descriptionE)>=0) {
                    final BA desc=new BA(descriptionE-descriptionS).filter(FILTER_HTML_DECODE, 0, T, descriptionS, descriptionE);
                    hit.setDescription(desc.bytes(),0,sze(desc));
                } else hit.setDescription(T,descriptionS,descriptionE);
            }
            hit.setNumber(atoi(T,strstr(STRSTR_AFTER,isNcbi ? "<Hit_num>" : "number=\"",T,hitS,hitE),E));
            final String db=replaceDB!=null?replaceDB:uCase(s(T,databaseS,hitE,'"'));
            hit.setDatabase(db);

            int idS= strstr(STRSTR_AFTER,isNcbi ? "<Hit_accession>" : "id=\"",T,hitS,hitE);
            if ( strEquls(STRSTR_IC,db,T,idS)) idS+=db.length();
            if (T[idS]=='_') idS++;
            final String hitId=s(T,idS,hitE, isNcbi ? '<' : '"');
            final int hitIdL=sze(hitId);
            if (hitIdL==0) continue;

            hit.setId(hitId);
            final int hitId0=hitId.charAt(0);
            boolean isNovel=false;
            if (whenBytes!=null) {
                int day=0;
                for(int iL=0; iL<whenEnds.length; iL++) {
                    final int b= iL==0 ? 0 : whenEnds[iL-1]+1;
                    final int e=whenEnds[iL];
                    if (e-b<hitIdL+2 || hitId0!=whenBytes[b] || whenBytes[b+hitIdL]!='\t' || strstr(hitId,whenBytes,b,b+hitIdL)<0) continue;
                    day=(int)atol(whenBytes,b+hitIdL+1,e);
                    if (_writeNew) whenBytes[e-1]=' ';  else isNovel=whenBytes[e-1]=='N';
                    break;
                }
                if (day==0) {
                    isNovel=true;
                    _baWhen.a(hitId).a('\t').a(currentDay).a('N').a('\n');
                    _countNovelHits++;
                }
            }
            _countHits++;
            int alignmentE=hitS;
            while(true) {
                final int alignmentS=strstr(STRSTR_AFTER,isNcbi ? "<Hsp>" : "<alignment ", T,alignmentE,hitE);
                if (alignmentS<0) break;
                alignmentE=strstr(STRSTR_AFTER,isNcbi ? "</Hsp>" : "</alignment",T,alignmentS,hitE);
                if (alignmentE<0) break;
                final BlastAlignment alignment=new BlastAlignment();
                alignment.setIsNew(isNovel);
                for(int isHit=0; isHit<2; isHit++) {
                    final String pattern=isHit==0 ? (isNcbi ? "<Hsp_qseq>" : "<querySeq") : (isNcbi ? "<Hsp_hseq>" : "<matchSeq");
                    final int sequenceS=strstr(STRSTR_AFTER,pattern,T,alignmentS,alignmentE);
                    final int sequenceE=strchr('<',T,sequenceS+1,alignmentE);
                    final int start=atoi(T, isNcbi ? strstr(STRSTR_AFTER,isHit==0 ? "<Hsp_query-from>" : "<Hsp_hit-from>",T, alignmentS,alignmentE) : strstr(STRSTR_AFTER,"start=\"", T,sequenceS,sequenceE),E);
                    final int   end=atoi(T, isNcbi ? strstr(STRSTR_AFTER,isHit==0 ? "<Hsp_query-to>" :   "<Hsp_hit-to>",  T, alignmentS,alignmentE) : strstr(STRSTR_AFTER,"end=\"",   T,sequenceS,sequenceE),E);
                    final int seqS=isNcbi ? sequenceS : strstr(STRSTR_AFTER,"\">",T,sequenceS,sequenceE);
                    if (isHit==0) {
                        alignment.setQueryStartEnd(start-1,end);
                        alignment.setQuerySeq(T,seqS,sequenceE);
                    } else {
                        alignment.setMatchStartEnd(start-1,end);
                        alignment.setMatchSeq(T,seqS,sequenceE);
                    }
                }
                alignment.setScore(atoi(T,strstr(STRSTR_AFTER,isNcbi ? "<Hsp_score>" : "<score>",T,alignmentS,alignmentE),E));
                alignment.setExpect(atof(T,strstr(STRSTR_AFTER,isNcbi ? "<Hsp_evalue>" : "<expectation>",T,alignmentS,alignmentE),E));
                alignment.setBits(atof(T,strstr(STRSTR_AFTER,isNcbi ? "<Hsp_bit-score>" : "<bits>",T,alignmentS,alignmentE),E));
                alignment.setIdentity(atoi(T,strstr(STRSTR_AFTER,isNcbi ? "<Hsp_identity>" : "<identity>",T,alignmentS,alignmentE),E));
                alignment.setPositives(atoi(T,strstr(STRSTR_AFTER,isNcbi ? "Hsp_positive>" : "positives>",T,alignmentS,alignmentE),E));
                alignment.setBlastHit(hit);
                if (fOut!=null) {
                    sbOut.clr().a(_countHits);
                    alignment.toText(0L,sbOut,width);
                    try { wrte(fOut,sbOut);}catch(IOException ex){}
                } else hit.addAlignment(alignment);
            }
            if (fOut==null) blastResult.addHit(hit);
        }
        return true;
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Add to Customize >>> */
    public final static int URL_EBI=1;
    private final static Set<String> vAdded=new HashSet();
    public static void addToProteinDatabases(int type, String[] db) {
        final int N=sze(db);
        if (N==0) return;
        final String lines[]=new String[N];
        for(int i=N; --i>=0;) {
            final String s=delToLstChr(db[i],' '), su=uCase(s);
            final String line=su+": "+s+": http://www.ebi.ac.uk/cgi-bin/dbfetch?db="+su+"&style=raw&id=";
            if (!vAdded.contains(s)) {
                vAdded.add(s);
                if (type==URL_EBI) lines[i]=line;
            }
        }
        Customize.customize(Customize.proteinDatabases).addHiddenEntries(false, lines);
    }
}
