package charite.christo.blast;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille
   // Observe SocketOutputStream  SocketInputStream
   Download from ftp://ftp.ncbi.nlm.nih.gov/blast/web_services/
   java -jar bin/soap_client.jar -test_submission_lite

*/
public class Blaster_web_ncbi extends AbstractBlaster implements NeedsInternet {
    private static int _saidE;
    private final static String[][]
        DB={
        "Nucleotide collection nr/nt nr;Reference RNA sequences refseq_rna;Reference genomic sequences refseq_genomic;NCBI Genomes chromosome;Expressed sequence tags est".split(";"),
        "Non-redundant protein sequences nr;Reference proteins refseq_protein;UniProtKB/Swiss-Prot swissprot;Patented protein sequences pat;Protein Data Bank proteins pdb;Metagenomic proteins env_nr".split(";")
    };

    public String[] getAvailableDBs(int opt) {
        return DB[ (0==(opt&DATABASES_NUCLEOTIDES)) ? 1:0];
    }
    @Override public BA computeIt() {
        if (myComputer()) putln(YELLOW_DEBUG+"Blaster_web_ncbi");
        try {
            //final int num=mini(500,getNumberOfAlignments());
            final int sens=getSensitivity(), num=100, word=getWordSize();
            final String
                db0=getDatabase(),
                db=db0.substring(1+db0.lastIndexOf(' ')),
                query=getQuery();

            final boolean isPdb=strEquls(STRSTR_IC,"pdb",db);

            final Object[][] data={
                {"QUERY", query},
                {"DATABASE",db},
                {"HITLIST_SIZE",toStrg(num)},
                {"ALIGNMENTS",toStrg(num)},
                {"EXPECT", sens<=3 ? "1e-1" : "100"},
                {"FORMAT_TYPE","XML"},
                {"PROGRAM", isAAQuery() ? (isAaDb() ? "blastp" : "tblastn") : (isAaDb() ? "blastx" : "blastn") },
                {"CMD","Put"},
                {strEquls(STRSTR_IC,"swissprot",db) || isPdb ? null : "NCBI_GI","on"},
                {word<0?null:"WORD_SIZE", toStrg(word)}
            };

            final BA hasRid;
            synchronized(toStrgIntrn("BSncbi$$SYNC"+db)) { hasRid=Web.getServerResponse(0,"http://www.ncbi.nlm.nih.gov/blast/Blast.cgi",data); }
            final String rid=qbInfo("RID",hasRid);
            setUrlResultHtml("http://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastFormatting&RID="+rid);
            BA xml=null;
            if (sze(rid)>0) {
                for(int iTry=1000; --iTry>=0;) {
                    sleep(isPdb?3*000:10*000);
                    putln("#"+iTry+" Loop Blaster_web_ncbi rid="+rid);
                    xml=readBytes("http://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Get&FORMAT_TYPE=XML&RID="+rid);
                    if (!strEquls(STRSTR_IC, "waiting", qbInfo("Status",xml))) break;                }

            }
            if (strstr("<?xml",xml)>=0) return xml;
            else if (_saidE++==0) {
                error(new BA(sze(query)+sze(hasRid)+999).a(query).a('\n',2).a('=',60).a('\n',2).or(hasRid,"\nNo response from server" ));
                return xml;
            }
        } catch(Throwable ex){stckTrc(ex);}
        return null;
    }

    public static String qbInfo(String key, BA txt) {
        final int b=strstr("QBlastInfoBegin",txt), e=b<0?-1:strstr(0,"QBlastInfoEnd",txt,b,MAX_INT);
        final int i=e<0?-1:strstr(STRSTR_IC|STRSTR_w, key, txt,b,e);
        final int eq=i<0?-1:strchr('=',txt, i,e);
        return wordAt(nxt(-SPC, txt, eq+1,e),txt);
    }

}

