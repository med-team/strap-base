package charite.christo.blast;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;

public class SortBlastDatabases implements Comparator {

    private final String[] _ddDefault, _dd;
    public SortBlastDatabases(String[] ddOrig, String[] ddDefault) {
        final int D=sze(ddDefault);
        _ddDefault=new String[D];

        for(int i=D; --i>=0;) {
            _ddDefault[i]=lstTkn(ddDefault[i]);
        }
        Arrays.sort(_dd=ddOrig.clone(), this);
    }
    public String[] sorted() { return _dd; }

    public int compare(Object o1, Object o2) {
        final String s1=(String)o1, s2=(String)o2;
        if (s1==null) return s2==null ? 0 :1;
        if (s2==null) return -1;
        final int isDefault1=isDefault(s1);
        final int isDefault2=isDefault(s2);
        if (isDefault1!=isDefault2) return isDefault1 > isDefault2 ? -1 : 1;
        return s1.compareTo(s2);
    }
    private int isDefault(String s) {
        for(int i=_ddDefault.length; --i>=0; ) {
            if (endWith(STRSTR_IC|STRSTR_w, _ddDefault[i],s)) return _ddDefault.length-i;
        }
        return -1;
    }

}
