package charite.christo.blast;
import charite.christo.*;
import java.util.*;
public final class BlastDatabase{
    private final String _name, _created;
    public String getName(){return _name;}
    public String whenCreated(){return _created;}
    public BlastDatabase(String name, String whenCreated) {
        _name=name; _created=whenCreated;
    }
    @Override public String toString() { return _name+" "+_created;}
}
