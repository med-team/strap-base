package charite.christo.blast;
import charite.christo.*;
import java.io.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;

/**HELP

WU-BLAST can be downloaded from http://blast.wustl.edu/licensing/.
The WU-BLAST directory must be added to the PATH environment variable.
Further the environment variables  WUBLASTMAT and WUBLASTDB must be set.

<br>
<pre class="terminal">
export WUBLASTMAT=/local/bioinf/wu-blast/matrix
</pre>
<br>

WUBLASTDB points to the directory where the blast databases are
installed. By formatting a blast-database, files with the
endings ".xpd", ".xps" and ".xpt" are generated.

<pre class="terminal">
export WUBLASTDB=/local/bioinf/db/wu-blast
</pre>
<br>
The WUBLASTDB is also evaluated by Strap to determine the
available databases.
The databases are created from multiple fasta files using the command
<br>
<pre class="terminal">
xdformat -p  -C x multipleFastaFile
</pre>
<br>
Strap calls the  Blast-program with the method Runtime#exec(String[]).

A valid Blast command is for example:
<br>
 <pre class="terminal">
wu-blastall -p blastp -d uniref50.fasta -i ~/testBlast/t.fa -b 300 -m 7
</pre>

<i>SEE_CLASS:Blaster_local_NCBI</i>
@author Christoph Gille
*/
public class Blaster_local_Wu extends AbstractBlaster {
    private String _options, _envBLASTDB="WUBLASTDB", _prg="wu-blastall", _extDB=".xpt";
    private final static String _envBLASTMAT="WUBLASTMAT";
    private File _fOut, _fIn, _dir;
    private static int _countInstance;
    private ChExec _x;
    public String getCommandLine() {
        return new BA(99).a(isSystProprty(IS_LINUX) ? "nice " : "").a(_prg).a(" -p ").a(getBlastProgram())
            .a(" -d ").a(getDatabase()).a(" -i ").a(_fIn).a(" -o ").a(_fOut ).a(' ').a(_options).toString();
    }
    @Override public BA computeIt() {
        if (getDatabase()==null) return null;
        mkdrsErr(_dir=file(new BA(99).a(dirTmp()).a('/').a(_prg).a('/').a(_countInstance++)));
        delFile((_fOut=file(_dir,"output.xml")));
        wrte(_fIn=file(_dir,"input.fa"),new BA(333).aln(">query").aln(getQuerySequence()));
        delFileOnExit(_fIn);
        delFileOnExit(_fOut);
        delFileOnExit(_dir);
        _x=new ChExec(ChExec.STDOUT|ChExec.STDERR).setCommandLine(getCommandLine()).dir(_dir);
        _x.run();
        if (_x.exitValue()!=0) putln(RED_ERROR,getClass(),_x.getStdout());
        return readBytes(_fOut);

    }
    @Override public String[] getAvailableMatrices() {
        final String BLASTMAT=ChEnv.get(_envBLASTMAT);
        final File dirMat=file(new BA(99).a(BLASTMAT).a(isAAQuery() ? "/aa" : "/nt"));
        log().a("The environment variable ").a(_envBLASTMAT).a(" has the value").aln(BLASTMAT);
        if (!isDir(dirMat)) {
            log().a(RED_ERROR).a(" the environment variable ").a(_envBLASTMAT).aln(" does not point to a directory");
            return NO_STRING;
        }
        log().send();
        return lstDir(dirMat);
    }
    @Override public String[] getAvailableDBs(int opt) {
        final String BLASTDB=ChEnv.get(_envBLASTDB);
        final File dirDB=file(BLASTDB);
        final Object error=
            BLASTDB==null ? new BA(99).a("Shell variable $").a(_envBLASTDB).a(" is not set"):
            !isDir(dirDB) ? new BA(99).a(" No such directory:  $").a(_envBLASTDB).a('=').a(BLASTDB) :
            null;
        if (error!=null) {
            log().a(RED_ERROR).aln(error);
            putln(RED_ERROR,error);
            return NO_STRING;
        }
        log().a("The environment variable ").a(_envBLASTDB).a(" has the value").aln(BLASTDB);
        final List v=new ArrayList(99);
        final String ext=_extDB;
        for(String s: lstDir(dirDB)) {
            if (s.endsWith(ext)) v.add(delSfx(ext,s));
        }
        log().a("Databases found in ").a(BLASTDB).a(' ').aln(v);
        final String dd[]=strgArry(v);
        if (dd.length==0) { putln("Error: No database files with ending ",ext); putln("found in ", dirDB); }
        return dd;
    }
    /**
       The variable ..BLASTDB points to the directory where databases are installed.
       Strap reads this directory to provide a List of available databases.
    */
    public void setShellVariableBLASTDB(String db, String extensionOfDatabaseFiles) { _envBLASTDB=db; _extDB=extensionOfDatabaseFiles;}
    //    public String _envBLASTMAT() { return "WUBLASTMAT";}
    public void setProgramName(String prg) { _prg=prg;}
    public void setAdditionalOptions(String options) { _options=options;}
    public Blaster_local_Wu() {
        _options=new BA(99).a(" -b ").a(getNumberOfAlignments()).a(" -m 7  -W 2 -e 100 -Ff").toString();
    }

}
