package charite.christo.blast;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import charite.christo.protein.*;
/**
   @author Christoph Gille
   // See SocketOutputStream  SocketInputStream
*/
public class Blaster_SOAP_ebi extends AbstractBlaster implements NeedsInternet {
    private static int _saidE;

    private final static String[][]
        DB={
        "EMBL Release em_rel;EMBL Updates emnew;EMBL Coding Sequence emcds".split(";"),
         "*aminos_from_3D_structures pdb;aminoacid uniprotkb;*aminoacids uniref100;aminoacids uniref90;aminoacids uniref50".split(";")
    };
    public String[] getAvailableDBs(int opt) {
        final boolean isAmino=0==(opt&DATABASES_NUCLEOTIDES);
        return 0==(opt&DATABASES_FULL_LIST) ? DB[isAmino?1:0] : parseDatabases(isAmino, getParamDetail("database"));
    }
    @Override public String[] getAvailableMatrices() { return new String[]{ isAaDb() ?  "blosum65" : "identity.4.2"}; }
    @Override public BA computeIt() {
        if (myComputer()) putln(YELLOW_DEBUG+" Blaster_SOAP_ebi");
        try {
            final int num=mini(500,getNumberOfAlignments()), sens=getSensitivity();
            final String db0=getDatabase(),  db=db0.substring(1+db0.lastIndexOf(' '));
            final boolean isPdb=strEquls(STRSTR_IC,"pdb",db);
            String rid=null;
            BA answer=null, query=null;
            query=ChSoap.getRsc("soapWublastEbi");
            if (query!=null) {
                query
                    .replace(STRPLC_FILL_RIGHT, "SENSITIVITY", sens>=9?"high":sens>=7?"high":sens>=5?"normal":sens>=3?"low":"vlow")
                    .replace(STRPLC_FILL_RIGHT, "ALIGNMENTS", toStrg(num))
                    .replace(STRPLC_FILL_RIGHT, "SCORES", toStrg(num))
                    .replace(STRPLC_FILL_RIGHT, "EXP", sens<=3 ? "1e-1" : "100")         /*10*/
                    .replace(STRPLC_FILL_RIGHT, "PROGRAM", isAAQuery() ? (isAaDb() ? "blastp" : "tblastn") : (isAaDb() ? "blastx" : "blastn"))
                    .replace(STRPLC_FILL_RIGHT, "DATABASE", db)
                    .replace(STRPLC_FILL_RIGHT|STRPLC_FILL_RM, "SEQUENCE", getQuery());

                log().a(ANSI_GREEN).a("Socket Write").aln(ANSI_RESET).aln(query).send();
                synchronized(toStrgIntrn("BSEBI$$SYNC"+db+sens)) {
                    answer=ChSoap.getResponse(ChSoap.ASK_FOR_PERMISSION, query, new BA(1000));

                }
            }

            rid=answer==null||BA.isError(answer)?null : wordAt(strstr(STRSTR_AFTER,"<jobId>",answer), answer,-LT_GT);
            setUrlResultHtml("http://www.ebi.ac.uk/Tools/services/web/blastresult.ebi?tool=wublast&jobId="+rid);
            boolean error=sze(rid)==0;
            if (!error) {
                int status=0;
                for(int iTry=1000; --iTry>=0 && (status==0 || status==ChSoap.STATUS_RUNNING);) {
                    sleep( (isPdb?1000:3000)*sens);
                    status=ChSoap.getStatus(rid);
                    putln("#"+iTry+" Loop Blaster_SOAP_ebi status="+status);
                }
                error|= (status!=ChSoap.STATUS_FINISHED);
            }
            final BA rXml=error ? null : ChSoap.getResultDecode64(rid, "xml", new BA(num*1500));
            error |= sze(rXml)==0;
            if (!error) return rXml;
            else  if (_saidE++==0) {
                error(new BA(sze(query)+sze(answer)+999).a(query).a('\n',2).a('=',60).a('\n',2).aln(answer==null ? "\nNo response from server" : answer));
            }
        } catch(Throwable ex){stckTrc(ex);}
        return null;
    }
    public static BA getParamDetail(String paramDetail) {
        final BA query=ChSoap.getRsc("soapWublastParamDetailEBI");
        if (query==null) return null;
        query.replace(0L,"PARAMETERID",paramDetail);
        final BA ba=ChSoap.getResponse(0L, query, new BA(100*100));
        return ba==null||BA.isError(ba) ? null : ba;
    }

    public static String[] parseDatabases(boolean protein, BA xml) {
        if (xml==null) return null;
        final byte T[]=xml.bytes();
        final int E=xml.end();
        final List v=new ArrayList(protein ? 99: 22000);
        final BA sb=new BA(99);
        for(int value=0; 0<(value=strstr(STRSTR_AFTER,"<value>",T, value+1,E));) {
            final int end=strstr(STRSTR_AFTER, "</properties>",T, value,E);
            if (end<0) break;
            if ((0<strstr(">protein<",T, value,end)) == protein) {
                final String item, id=wordAt(strstr(STRSTR_AFTER, "<value>",xml, value,end), xml, -LT_GT);
                if (id.indexOf("ensemblgenomes")>=0) item=id;
                else {
                    final String label=wordAt(strstr(STRSTR_AFTER, "<label>",xml, value,end), xml, -LT_GT);
                    item=sb.clr().a(label).a("  ").a(id).toString();
                }
                v.add(item);
            }
            value=end;
        }

        return strgArry(v);
    }
}

