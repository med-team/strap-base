package charite.christo.blast;
import charite.christo.*;
import java.util.*;
import java.io.*;
/**
   A sequence blaster invokes a BLAST search with a given query sequence and
   returns the result as XML text.
   @author Christoph Gille
*/
public interface SequenceBlaster {
    /** set the amino acid sequence query */
    void setAAQuerySequence(String s);
    /** the nucleotide sequence  query sequence */
    void setNTQuerySequence(String s);
    /* On a scale between 1 and 10 */
    void setSensitivity(int n);
    /**
       Upper Limit of the number of alignments to be shown
    */
    void setWordSize(int n);


    void setNumberOfAlignments(int n);
    /** @param db db is the name of a database such as "uniprot", "pdb"  or "uniref50" */
    void setDatabase(String db) throws IllegalArgumentException ;
    /** Names of available databases depend on the type of query (AA or NT) */

    public final static int DATABASES_NUCLEOTIDES=1<<0, DATABASES_FULL_LIST=1<<1;
    String[] getAvailableDatabases(int opt);

    String getDatabase();
    /** @param matrix matrix is the name of a similarity matrix such as blosum62
        which contains a value for each pair of amino acids
    */
    void setMatrix(String matrix) throws IllegalArgumentException ;
    String[] getAvailableMatrices();
    String getMatrix();
    BA getResultXml();
    /**
       Computation is typically very time consuming
    */
    void compute();

   /**
       Use cached results only when new
    */
    void cachedResultsNotOlderThanSec(int time);

}
/*
  "Martin Jambon" <martin_jambon@emailuser.net>
  Feel free to add your suggestions to the BLAST page at Wikiomics:
  http://wikiomics.org/index.php?title=BLAST
  Martin
*/
