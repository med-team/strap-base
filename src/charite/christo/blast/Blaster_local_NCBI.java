package charite.christo.blast;
import static charite.christo.ChUtils.*;
import charite.christo.*;
/**HELP
NCBI-BLAST can be downloaded from ftp://ftp.ncbi.nih.gov/ or search GOOGLE{ ftp ncbi blast }.
Download and extract ncbi.tar.gz for example in /local/bioinf.

Install  WIKI:csh or WIKI:tcsh if not already installed.

Follow the installation instructions of ncbi/make/readme.unx.
<br><br>

<b>Environment variables: </b>
The  /ncbi/build/ directory which contains the blastall command
must be added to the PATH environment variable. Example:
<pre class="terminal">
export  PATH=$PATH:/local/bioinf/ncbi/build
</pre>

Further the environment variables: 
BLASTDB points to the directory  where the Blast databases are installed.
It is declared with a command like
<pre class="terminal">
export BLASTDB=/local/bioinf/blastdb/
</pre>

The variable BLASTDB is evaluated by Strap to obtain the list of available databases.
<br>
The databases are created from multiple fasta files using the command
<br>
<pre class="terminal">
formatdb -i  multipleFastaFile
</pre>
<br>
Strap invokes  Blast with a command like:
<br>
 <pre class="terminal">
blastall -p blastp -d pdb.fasta -i ~/testBlast/t.fa -b 300 -m 7
</pre>
   @author Christoph Gille
*/
public class Blaster_local_NCBI extends Blaster_local_Wu {

    public Blaster_local_NCBI() {
        super();
        setProgramName("blastall");
        setShellVariableBLASTDB("BLASTDB",".phr");
    }
    /*FIXME*/

    @Override public String[] getAvailableMatrices() {
      return isAAQuery() ? new String[]{"BLOSUM62","BLOSUM80","BLOSUM45","PAM30","PAM70"} : NO_STRING;
    }
}
