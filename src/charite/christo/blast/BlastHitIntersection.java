package charite.christo.blast;
import charite.christo.*;
import charite.christo.protein.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
/**
   @author Christoph Gille
*/
public class BlastHitIntersection {

    private final byte[][] matchSeqs=new byte[2][], querySeqs=new byte[2][];
    private int[] queryStarts=new int[2],queryEnds=new int[2];
    private final BlastAlignment[] alignments=new BlastAlignment[2];
    private int matchEnd, matchStart;

    public byte[] getSeq(char QorM,int i) { return (QorM=='Q' ? querySeqs:matchSeqs)[i];}
    public int getQueryStart(int i) { return queryStarts[i];}
    public int getQueryEnd(int i) { return  queryEnds[i];}

    public int getMatchStart() { return matchStart;}
    public int getMatchEnd() { return  matchEnd;}

    public BlastAlignment getAlignment(int i) { return alignments[i];}

    public final boolean SELECTED[]={false};
    public double getExpect() { return Math.max(alignments[0].getExpect(),alignments[1].getExpect());}

    public static BlastHitIntersection intersect(BlastAlignment a0,BlastAlignment a1) {
        //putln(" new BlastHitIntersection "+a0.getID(), a1.getID());
        if (!a0.getID().equals(a1.getID())) return null;
        if (!a0.getDatabase().equals(a1.getDatabase())) return null;
        final int matchS=maxi(a0.getMatchStart(),a1.getMatchStart());
        final int matchE=mini(a0.getMatchEnd(),a1.getMatchEnd());
        if (matchE-matchS<2) return null;

        final BlastHitIntersection a=new BlastHitIntersection();
        a.matchStart=matchS;
        a.matchEnd=matchE;
        for(int i=2;--i>=0;){
            final BlastAlignment aa=i==0 ? a0 : a1;
            final byte[] qSeq=aa.getQuerySeq();
            final byte[] mSeq=aa.getMatchSeq();
            final int startV=model2view(mSeq,matchS-aa.getMatchStart());
            final int endV=model2view(mSeq,matchE  -aa.getMatchStart());
            a.matchSeqs[i]=substrgB(mSeq,startV,endV);
            a.querySeqs[i]=substrgB(qSeq,startV,endV);
            a.queryStarts[i]=aa.getQueryStart()+AlignUtils.column2letterIdx(qSeq,startV);
            a.queryEnds[i]=aa.getQueryStart()+AlignUtils.column2letterIdx(qSeq,endV);
            a.alignments[i]=aa;
            if (a.queryStarts[i]<0) assrt();
        }

        if (myComputer() && hashCdOnlyLetters(a.matchSeqs[0],0,MAX_INT,true)!=hashCdOnlyLetters(a.matchSeqs[1],0,MAX_INT,true)) {
            putln("\n a.matchSeqs Not identical");
            putln(a.matchSeqs[0]);
            putln("\n");
            putln(a.matchSeqs[1]);
            putln("");
        }
        return a;
    }
    public static int[] expectValueHistogramm(BlastHitIntersection[] v) {
        final int N=400,hist[]=new int[N];
        for(BlastHitIntersection i : v) {
            final double expect=i.getExpect();
            final int exp=(int)-Math.round(10*Math.log(expect)/LOG10);
            final int idx=Math.min(Math.max(0,exp),N-1);
            hist[idx]++;
        }
        final int cumul[]=new int[N];
        for(int i=N;--i>=0;)
            for(int j=N;--j>=i;)
                cumul[i]+=hist[j];
        return cumul;
    }
    public static float expectValueCutoff(int cumulHist[],int num) {
        for(int i=0;i<cumulHist.length;i++) {
            if (cumulHist[i]<num) {
                return (float)Math.exp(-i/10.0*LOG10);

            }
        }
        return 10;
    }

   public static int model2view(byte[] seq,int fromAa) {
        int pos=0,count=0;
        final int l=seq.length;
        while(count<fromAa && pos<seq.length) if (is(LETTR,seq[pos++])) count++;
        while(pos<l && !is(LETTR,seq[pos])) pos++;
        return pos;
    }

    public static byte[][] mergeAlignments(byte[] q0, int Lq0, byte[] m0, int Lm0, byte[] q1, int Lq1, byte[] m1, int Lm1) {
        byte[] r0=null, r1=null, m=null;
        while(true) {
            int iq0=0, iq1=0, im0=0, im1=0, col=0;
            for(; iq0<Lq0 && iq1<Lq1 && im0<Lm0 && im1<Lm1; col++) {
                final boolean isL0=is(LETTR,m0[im0]), isL1=is(LETTR,m1[im1]);
                if (isL0 && !isL1) {
                    if (r0!=null) {
                        if (iq1<Lq1) r1[col]=q1[iq1];
                        r0[col]=m[col]=(byte)'-';
                    }
                    im1++;
                    iq1++;
                    continue;
                } else if (!isL0 && isL1) {
                    if (r0!=null) {
                        if (iq0<Lq0) r0[col]=q0[iq0];
                        r1[col]=m[col]=(byte)'-';
                    }
                    im0++;
                    iq0++;
                    continue;
                } else {
                    if (r0!=null) {
                        m[col]=m0[im0 ];
                        r0[col]=q0[iq0];
                        r1[col]=q1[iq1];
                    }
                    im0++;
                    iq0++;
                    im1++;
                    iq1++;
                }
            }
            if (r0==null) {
                r0=new byte[col];
                r1=new byte[col];
                m=new byte[col];
            } else break;
        }
        //putln(toStrg(r0).replace((char)0,'x')+"<");
        //putln(toStrg(m).replace((char)0,'x')+"<");
        //putln(toStrg(r1).replace((char)0,'x')+"<");
        return new byte[][]{r0,m,r1};
    }

    public static  BlastAlignment union(BlastAlignment a1,BlastAlignment a2) { //NEEDS_TUNING
        if (a1.getID()==null || !a1.getID().equals(a2.getID()) || a1.getDatabase()!=a2.getDatabase()) return null;
        final BlastAlignment aliR, aliL;
        if (a1.getMatchStart()<a2.getMatchStart()) { aliL=a1; aliR=a2;}
        else  { aliL=a2; aliR=a1;}

        final int startR=aliR.getMatchStart(),endR=aliR.getMatchEnd();
        final int startL=aliL.getMatchStart(),endL=aliL.getMatchEnd();

        if (endL>=endR) return aliL;
        /*
          lllllllllllllllllllllllllll
        */
        final BlastAlignment a=new BlastAlignment();
        a.setMatchStartEnd(mini(startL,startR),maxi(endL,endR));

        final int startOnlyR=1+endL-startR;
        if (startOnlyR<0 || startOnlyR>=aliR.getMatchUngapped().length) {
            putln("hier ist ein Fehler "+startOnlyR);
            putln(aliL);
            putln(aliR);
            stckTrc();
            return null;
        }
        final boolean debug=false;
        if (debug){
            final String sRight=toStrg(aliR.getMatchUngapped()).substring(startOnlyR);
            final String ms1=toStrg(aliL.getMatchUngapped())+sRight;
            final String ms2=toStrg(concat(aliL.getMatchUngapped(),0,MAX_INT,  aliR.getMatchUngapped(), startOnlyR, MAX_INT, null));
            putln("\n"+ms1+"\n"+ms2+"\n");
        }
        a.setMatchSeq(  concat(aliL.getMatchUngapped(),0,MAX_INT,  aliR.getMatchUngapped(), startOnlyR, MAX_INT, null), 0,MAX_INT);

        a.setBlastHit(aliL.getBlastHit());
        //putln("union performed\n"+toStrg(aliL.getMatchSeq())+" + "+toStrg(aliR.getMatchSeq())+" = \n"+toStrg(a.getMatchSeq())+"\n");
        return a;
    }

}
