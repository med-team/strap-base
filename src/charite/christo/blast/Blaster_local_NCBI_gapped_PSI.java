package charite.christo.blast;
import static charite.christo.ChUtils.*;
import charite.christo.*;
/**HELP

<i>SEE_CLASS:Blaster_local_NCBI</i>

   @author Christoph Gille
*/
public class Blaster_local_NCBI_gapped_PSI extends Blaster_local_NCBI  {

    @Override public String getCommandLine() {
        setAdditionalOptions(new BA(99).a(" -b ").a(getNumberOfAlignments()).a(" -m 7  -W 2 -e 100 ").join(getPrgParas().asStringArray()," ").toString());
        return super.getCommandLine();
    }

    public Blaster_local_NCBI_gapped_PSI() {
        super();
        setProgramName("blastpgp");
        defineParameters(0L, "-j\t Max number of rounds (default 1; i.e., regular BLAST) ");
    }
}
