package charite.christo.blast;
import charite.christo.*;
import static charite.christo.ChConstants.*;
import static charite.christo.ChUtils.*;
public class BlastAlignment {
    private byte[] _matchSeq,_querySeq,_matchUngapped;
    private int
        _queryStart, _queryEnd, _matchStart,_matchEnd, _matchHashcode,
        _identity, _positives, _score, _number,
        _querySeqFrom, _querySeqTo, _matchSeqFrom, _matchSeqTo;
    private double _expect, _bits;
    private BlastHit _blastHit;
    public void setMatchSeq(byte[] s,int from,int to) {_matchSeq=s; _matchSeqFrom=from; _matchSeqTo=mini(s.length,to); }
    public void setQuerySeq(byte[] s,int from,int to) {_querySeq=s; _querySeqFrom=from; _querySeqTo=mini(s.length,to); }
    public void setQueryStartEnd(int s,int e) {_queryStart=s;_queryEnd=e;}
    public void setMatchStartEnd(int s,int e) {_matchStart=s; _matchEnd=e;}
    public void setIdentity(int i) {_identity=i;}
    public void setPositives(int i) {_positives=i;}
    public void setScore(int i) {_score=i;}
    public void setNumber(int i) {_number=i;}
    public void setExpect(double d) {_expect=d;}
    public void setBits(double d) {_bits=d;}

    public byte[] getMatchSeq() {
        final int f=_matchSeqFrom, t=_matchSeqTo;
        if (_matchSeq!=null && (f>0 || t!=_matchSeq.length)) {
            final byte bb[]=new byte[t-f];
            System.arraycopy(_matchSeq,f,bb,0, t-f);
            _matchSeqFrom=0;
            _matchSeqTo=bb.length;
            _matchSeq=bb;
        }
        return _matchSeq;
    }

    public int getMatchSeqHashCode() {
        if (_matchHashcode==0) _matchHashcode=hashCd(getMatchSeq(),0,MAX_INT,true);
        return _matchHashcode;
    }

    public byte[] getQuerySeq() {
        final int f=_querySeqFrom, t=_querySeqTo;
        if (_querySeq!=null && (f!=0 || t!=_querySeq.length) ) {
            final byte bb[]=new byte[t-f];
            System.arraycopy(_querySeq,f,bb,0, t-f);
            _querySeqFrom=0;
            _querySeqTo=bb.length;
            _querySeq=bb;
        }
        return _querySeq;
    }

    public byte[] getMatchUngapped() {
        if (_matchUngapped==null) _matchUngapped=filtrBB(0L,LETTR,getMatchSeq());
        return _matchUngapped;
    }
    public int getQueryStart() { return _queryStart;}
    public int getQueryEnd() { return _queryEnd;}
    public int getMatchStart() { return _matchStart;}
    public int getMatchEnd() { return _matchEnd;}
    public int getIdentity() { return _identity;}
    public int getPositives() { return _positives;}
    public int getScore() { return _score;}
    public int getNumber() { return _number;}
    public double getExpect() { return _expect;}
    public double getBits() { return _bits;}
    public String getDatabase(){ return _blastHit.getDatabase();}
    public String getID(){ return _blastHit.getID();}
    public BlastHit getBlastHit() { if (null==_blastHit) assrt(); return _blastHit;}
    public void setBlastHit(BlastHit b) { _blastHit=b;}
    private boolean _isNovel;
    public void setIsNew(boolean b){_isNovel=b;}
    public boolean isNovel() { return _isNovel;}
    final int descrFromTo[]={0,0};
    private final static byte[][] STRG=new byte[99][];
    private static byte[] strg(int i, String s) {
        return STRG[i]==null ? STRG[i]=s.getBytes() : STRG[i];
    }
    public final void toText(long options, BA sb, int outputWidth) {
        final BlastHit bh=getBlastHit();
        final String db=bh.getDatabase(), id=bh.getID();
        sb.a("> ").a(db);
        if (strEquls("EM",db)) sb.a("EMCDS".equals(db) ? strg(1," EMBLCDS") : strg(2," EMBL"));
        sb.a(':').a(id).a(' ',2).a(bh.getDescription(descrFromTo), descrFromTo[0], descrFromTo[1]);
        if (0!=(options&BlastResult.WRITE_MARK_NOVEL) && isNovel()) sb.a(" (NEW)");
        sb
            .a(strg(3,"\n\n Score = ")).a(getScore()).a(' ').a('(').aBigFloat(getBits(), 6)
            .a(strg(4,"), Expect = ")).aBigFloat(getExpect(),6)
            .a(strg(5,"\nIdentities = ")).a(getIdentity()).a('/').a(_matchSeqTo-_matchSeqFrom)
            .a(strg(6,", Positives = ")).a(getPositives()).a('/').a(_matchSeqTo-_matchSeqFrom)
            .a('\n',2);

        final byte[] query=_querySeq, mSeq=_matchSeq;
        final int len=maxi(_matchSeqTo-_matchSeqFrom, _querySeqTo-_querySeqFrom, _matchSeqTo-_matchSeqFrom);
        for(int col=0, posQuery=getQueryStart(), posMatch=getMatchStart(); col<len; col+=outputWidth) {
            final int colE=mini(col+outputWidth,len);
            sb.a('Q').a(posQuery+1,8).a(' ')
                .a(query,col+_querySeqFrom,colE+_querySeqFrom).a(' ').a(posQuery+=countLettrs(query,col+_querySeqFrom,colE+_querySeqFrom)).a('\n')
                .a(' ',10);
            midline(query,col+_querySeqFrom,colE+_querySeqFrom,mSeq,col+_matchSeqFrom,colE+_matchSeqFrom,sb);
            sb.a("\nH").a(posMatch+1,8).a(' ')
                .a(mSeq,col+_matchSeqFrom,colE+_matchSeqFrom).a(' ').a(posMatch+=countLettrs(mSeq,col+_matchSeqFrom,colE+_matchSeqFrom))
                .a('\n',2);
        }

    }

    public static void midline(byte[] q,int qFrom, int qTo, byte[] m, int mFrom, int mTo, BA sb) {
        final byte[][] M=charite.christo.protein.Blosum.BLASTMIDLINE;
        for(int iQ=qFrom, iM=mFrom; iQ<qTo && iM<mTo; iQ++,iM++) {
            sb.a( (char) M[q[iQ]&127][m[iM]&127]);
        }
    }
    public final static BlastAlignment[] NONE={};
}
