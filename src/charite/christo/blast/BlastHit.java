package charite.christo.blast;
import charite.christo.*;
import java.util.*;
import static charite.christo.ChUtils.*;
final public class BlastHit {

    private BlastAlignment alignments[]=BlastAlignment.NONE;
    private BlastResult blastResult;
    public void setBlastResult(BlastResult br) {blastResult=br;}
    public BlastResult getBlastResult() {return blastResult;}
    private String id,database;
    public void setId(String s) {id=s;}
    public void setDatabase(String s) {database=s;}
    public String getID() { return id;}
    public String getDatabase() { return database;}
    private int number;
    public void setNumber(int i) {number=i;}
    public int getNumber() { return number;}
    public BlastAlignment[] getAlignments(){
        return alignments;
    }
    public void addAlignment(BlastAlignment a) {
        if (a==null) return;
        a.setBlastHit(this);
        alignments=adToArray(a, alignments, 0, BlastAlignment.class);
    }

    private byte[] descr, descrTrunc;
    private int descrF,descrT;
    public void setDescription(byte[] s,int from,int to) {
        descr=s;
        descrF=from;
        descrT=s.length<to?s.length:to;
    }
    public byte[] getDescription(int fromTo[]) {
        fromTo[0]=descrF;
        fromTo[1]=descrT;
        return this.descr;
    }

    public byte[] getDescription() {
        if (descr==null || descrF==0 && descrT>=descr.length) return descr;
        if (descrTrunc==null) descrTrunc=substrgB(descr,descrF,descrT);
        return descrTrunc;

    }

}
