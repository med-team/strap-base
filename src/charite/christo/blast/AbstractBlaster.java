package charite.christo.blast;
import static charite.christo.ChUtils.*;
import charite.christo.*;
import java.util.*;
import java.io.*;
import static charite.christo.ChConstants.*;
/**
   @author Christoph Gille InputStream
*/
public abstract class AbstractBlaster implements SequenceBlaster, HasControlPanel,Disposable,java.awt.event.ActionListener {
    /** This can be overridden */
    private boolean _isAAQ=true,  _hasResultXml, _disposed;

    private String _urlResultHtml, _query, _fromCache="found in cache", _matrix="blosum62", _database;
    private int _numOfAli=2000, _sensitivity=5, _word=-1;

    public void setWordSize(int n) { _word=n; }
    public void setSensitivity(int n) { _sensitivity=n; }
    public void setAAQuerySequence(String s) {_query=filtrS(FILTER_TO_UPPER, LETTR, s); _isAAQ=true;}
    public void setNTQuerySequence(String s) {_query=filtrS(FILTER_TO_UPPER, LETTR, s); _isAAQ=false;}
    public String getQuerySequence() { return _query;}
    public int getWordSize() { return _word;}
    public int getSensitivity() { return _sensitivity; }
    public int getNumberOfAlignments() { return _numOfAli;}
    public void setNumberOfAlignments(int n) {_numOfAli=n;}
    public boolean isAAQuery() {return _isAAQ;}
    public String getBlastProgram(){ return isAAQuery() ? "blastp" : "blastn";}
    public String getQuery() { return _query;}
    public BA getResultXml() {
        final String q=_query;
        if (q==null) return null;
        if (isAAQuery()) {
            if (q.indexOf('O')>=0 || q.indexOf('B')>=0 || q.indexOf('J')>=0) {
                putln(RED_ERROR,"Blast: Wrong residue ",q);
                if (myComputer()) stckTrc();
                return null;
            }
        }
        final String cacheS=cacheSK('S'), cacheK=cacheSK('K');
        {
            CacheResult.getLastModified(getClass(),cacheS,cacheK);
            final BA ba= CacheResult.getValue(0, getClass(),cacheS,cacheK,(BA)null);
            //if(debug) debugTime("AbstractBlaster CacheResult.getValue  time=", time);
            if (strstr("FATAL: ",ba)>0) {
                putln("AbstractBlaster Error: found \"FATAL: \"");
                return null;
            }
            if (ba!=null && ! _hasResultXml) {
                _hasResultXml=true;
                log().aln(_fromCache).send();
            }
            return ba;
        }
    }
    public void setUrlResultHtml(String url) {
        _urlResultHtml=url;
        enableDisable();
    }
    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Matrix  >>> */
    public void setMatrix(String mx) throws IllegalArgumentException {
        for(String m:getAvailableMatrices())
            if (mx.equals(m)) {_matrix=m; return; }
        throw(new IllegalArgumentException(mx+": no suXbch blast similarity _matrix"));
    }
    /**FIXME*/
    public String[] getAvailableMatrices() { return new String[]{"blosum64"}; }
    public String getMatrix() {return _matrix;}
    /* <<< Matrix <<< */
    /* ---------------------------------------- */
    /* >>> Databases >>> */
    public void setDatabase(String s) { _database=delToLstChr(s,' ');    }
    public String getDatabase(){return _database;}
    /* <<< Databases <<< */
    /* ---------------------------------------- */
    /* >>> log >>> */
    public final BA log() {
        if (_log==null) _log=new BA(999);
        return _log;
    }
    private BA _log;
    /* <<< log <<< */
    /* ---------------------------------------- */
    /* >>> GUI >>> */
    private Object _ctrl;
    @Override public Object getControlPanel(boolean real) {
        if (_ctrl==null) {
            if (!real) return CP_EXISTS;
            _ctrl=pnl(CNSEW,
                      scrllpn(0, log(), dim(99,99)),
                      dHomePage(this),
                      ChUtils.pnl(button(BUT_XML), button(BUT_XMLb)," ", button(BUT_TXT),button(BUT_TXTe), " ", button(BUT_IN_BROWSER)),
                      null,null
                      );
        }
        return _ctrl;
    }

    public final static int BUT_XML=1, BUT_XMLb=2, BUT_TXT=3, BUT_TXTe=4, BUT_IN_BROWSER=5;
    public final ChButton[] button=new ChButton[6];
    public ChButton button(int type) {
        ChButton b=button[type];
        if (b==null) {
            if (type==BUT_XMLb) b=new ChButton().li(this).t("XML in file browser");
            if (type==BUT_XML)  b=new ChButton().li(this).t("Result XML");
            if (type==BUT_TXT)  b=new ChButton().li(this).t("Result text");
            if (type==BUT_TXTe) b=new ChButton().li(this).t("Text in editor");
            if (type==BUT_IN_BROWSER) b=new ChButton().li(this).t("View in Browser");
            button[type]=b;
            enableDisable();
        }
        return b;
    }

    /* <<< GUI <<< */
    /* ---------------------------------------- */
    /* >>> Para >>> */
    private long _parameterOpt;
    private Object _shared;
    private String _parameters;
    private PrgParas _prgPara;
    public final PrgParas getPrgParas() {
        final AbstractBlaster si=((AbstractBlaster)orO(getSharedInstance(), this));
        if (si._prgPara==null) {
            si._prgPara=new PrgParas();
            if (si._parameters==null) si._prgPara.defineGUI(si._parameterOpt, si._parameters);
        }
        return si._prgPara;
    }
    public final void setSharedInstance(Object shared) { _shared=shared;}
    public final Object getSharedInstance() { return _shared; }
    public void defineParameters(long opt, String s) { _parameterOpt=opt; _parameters=s;}

     /* <<< Para <<< */
    /* ---------------------------------------- */
    /* >>> Cache >>> */

    private String _cacheS, _cacheK;
    private final static Object BA[]={null};
    public String cacheSK(char type) {
        final String q=_query;
        if (q==null) return null;
        synchronized(BA) {
            final BA ba=baClr(BA);
            if (type=='K') {
                if (_cacheK==null) _cacheK=ba.a(q).join(getPrgParas().asStringArray(), "_").toString();
                return _cacheK;
            } else {
                if (_cacheS==null) {
                    ba.a('/').a(getDatabase()).a('/');
                    if (_sensitivity!=5) ba.a(_sensitivity).a('/');
                    CacheResult.sectionForSequence(q,ba);
                    _cacheS=ba.toString();
                }
                return _cacheS;
            }
        }
    }
    private long maxAge=MAX_INT;
    @Override public void cachedResultsNotOlderThanSec(int time) { maxAge=time; }
    /* <<< Cache <<< */
    /* ---------------------------------------- */
    /* >>> Disposable >>> */
    public void dispose(){_disposed=true;}
    public boolean isDisposed(){return _disposed;}
    /* <<<  Disposable <<< */
    /* ---------------------------------------- */
    /* >>> Compute >>> */
    public abstract BA computeIt();
    public final void compute() {
        final String q=_query;
        if (sze(q)<5) {
            log().a(RED_ERROR).a(" in AbstractBlaster query sequence too short").aln(q).send();
            return;
        }
        if (_database==null) {
            log().a(RED_ERROR).aln(" in AbstractBlaster: _database=null").send();
            return;
        }
        final String sync=("AB$$SYNCc"+getClass()+" "+hashCd(_cacheS)+" "+hashCd(_cacheK)).intern();
        synchronized(sync) {
            final String cacheS=cacheSK('S'), cacheK=cacheSK('K');
            final long lastModified=maxAge==MAX_INT ? 0:CacheResult.getLastModified(getClass(), cacheS, cacheK);
            if (CacheResult.isEnabled() && maxAge*1000>System.currentTimeMillis()-lastModified) {
                final BA ba=getResultXml();
                if (ba!=null) {
                    log().aln(ANSI_GREEN+"Found in cache\n"+ANSI_RESET)
                        .a("Computation was performed on ").aln(lastModified>0 ? day(lastModified) : "")
                        .send();
                }
            }
            if (!_hasResultXml) {
                final BA result=computeIt();

                final Object msg=new BA(999).a(result==null?"Failed: ":GREEN_AVAILABLE).aln(shrtClasNam(this));
                if (onlyOnce(msg.toString())!=null) logUnavailable().a(msg);

                _fromCache=null;
                if (result!=null) {
                    CacheResult.putValue(CacheResult.OVERWRITE, getClass(),cacheS,cacheK,result);
                    _hasResultXml=true;
                }
            }
        }
        enableDisable();
    }
    public void enableDisable() {
        if (!isEDT())  inEdtLater(thrdM("enableDisable", this));
        else {
            for(int i=button.length;--i>=0;) {
                setEnabld(i==BUT_IN_BROWSER ? 0<sze(_urlResultHtml) : _hasResultXml, button[i]);
            }
        }
    }
    private File fTxt;
    public void actionPerformed(java.awt.event.ActionEvent ev) {
        final int modi=modifrs(ev), iBut=idxOf(ev.getSource(), button);
        if (iBut==BUT_XML) shwTxtInW("Blast XML", getResultXml());
        if (iBut==BUT_IN_BROWSER) visitURL(_urlResultHtml, modi);
        if (iBut==BUT_TXTe || iBut==BUT_TXT) {
            final BA xml=getResultXml();
            if (xml!=null) {
                if (sze(fTxt)==0) {
                    fTxt=newTmpFile(".txt");
                    try{
                        final OutputStream fOut=fileOutStrm(fTxt);
                        new BlastParser().parseXML(getResultXml(), 99999, fOut);
                        closeStrm(fOut);
                    } catch(IOException e){putln("Caught in AbstractBlaster ",e);}
                }
                if (sze(fTxt)>0) {
                    if (iBut==BUT_TXTe) viewFile(fTxt, modi);
                    if (iBut==BUT_TXT ) new ChTextView(fTxt).tools().showInFrame("Blast result");
                }
            }
        }
        if (iBut==BUT_XMLb) {
            final File fTmp=newTmpFile(".xml");
            final BA xml=getResultXml();
            if (xml!=null) { wrte(fTmp,xml); viewFile(fTmp, modi);}
        }
    }
    /* <<< Compute <<< */
    /* ---------------------------------------- */
    /* >>> Available Databases >>> */
    public void skipDatabases(String skip[], String notSkip[]) {
        strings()[SKIP]=skip;
        strings()[SKIP+1]=notSkip;
    }
    protected boolean isAaDb() {
        final String db=getDatabase();
        if (strEquls(STRSTR_IC, "pdb",db) || strEquls(STRSTR_IC, "uniprot",db)) return true;
        return 0<=idxOfStrg(STRSTR_END|STRSTR_IC|STRSTR_w_L, db, getAvailableDatabases(0));
    }

    public final static Map<Class,String[][]> _mapDB=new HashMap();
    private String[][] strings() {
        final Class c=getClass();
        String db[][]=_mapDB.get(c);
        if (db==null) _mapDB.put(c, db=new String[SKIP+2][]);
        return db;
    }
    public abstract String[] getAvailableDBs(int opt);
    private final static int SKIP=(DATABASES_FULL_LIST|DATABASES_NUCLEOTIDES)+1;
    public String[] getAvailableDatabases(int options) {
        synchronized(mkIdObjct("AB$$SYNC",getClass())) {
            final String db[][]=strings(), skip[]=db[SKIP], notSkip[]=db[SKIP+1];
            int opt=options&(DATABASES_FULL_LIST|DATABASES_NUCLEOTIDES);
            if (db[opt|DATABASES_FULL_LIST]!=null) opt|=DATABASES_FULL_LIST;

            String ss[]=db[opt];
            if (ss==null) db[opt]=ss=getAvailableDBs(opt);
            if (sze(ss)>0) {
                BlastParser.addToProteinDatabases(BlastParser.URL_EBI, db[opt]=ss);
                if (0!=(opt&DATABASES_FULL_LIST)) ss=filter(ss, skip, notSkip);
            }
            return ss;
        }
    }

    private static String[] filter(String dbs[], String skip[], String notSkip[]) {
        if (dbs==null || sze(skip)==0) return dbs;
        final String[] result=new String[dbs.length];
        int count=0;
        for(String db : dbs) {
            if (!containsPattern(db, skip) || containsPattern(db, notSkip)) {
                result[count++]=db;
            }
        }
        return rmNullS(result);
    }

    private static boolean containsPattern(String db, String patterns[]) {
        for(String p : patterns) if (db.indexOf(p)>=0) return true;
        return false;
    }
    /* <<<  Available Databases <<< */
}
