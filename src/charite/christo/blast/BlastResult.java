package charite.christo.blast;
import charite.christo.*;
import java.util.*;
import java.io.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**
   This class provids an object oriented structure of BLAST results.
   It can be initialized with an XML BLAST output.
   A human readable text can be created with the method toText().
   There is a significant difference between the numbering in BLAST output files and this API:
   In contrast to the
   output written by the BLAST program counting in this model starts
   at zero and not at one.  For example  queryStart 1 queryEnd 100 in the BLAST output would
   means 100  residues with indices 0 to index 99 here in this class.
   There is a difference between the XML format at NCBI and at ebi.ac.uk. This parser can cope with both formats.
*/
final public class BlastResult {
    /**
       The instance is created with the XML output of a blast run.
       This can either be in NCBI or in Wu-blast format
    */
    private String _query, _prgName, _prgVersion;
    private BlastHit[] _vHits={};
    private BlastDatabase _vDB[]={};
    public BlastResult(BA xml) {
        if (xml!=null)
        new BlastParser().parseXML(xml,60,this);
    }
    public void dispose() { _vHits=null; }

    public synchronized BlastHit[] getHits() { return _vHits=rmNullA(_vHits, BlastHit.class); }

    public void addDatabase(BlastDatabase database) {
        _vDB=adToArray(database, _vDB, 0, BlastDatabase.class);
    }
    public BlastDatabase[] getDatabases() {return _vDB;}

    public void setQuerySeq(String s) {_query=s.intern();}
    public String getQuerySeq(String s) {return _query;}
    public synchronized void addHit(BlastHit bh) {
        _vHits=adToArray(bh, _vHits, 501, BlastHit.class);
        bh.setBlastResult(this);
    }
    public void setProgramName(String s){_prgName=s;}
    public String getProgramName(){return _prgName;}

    public void setProgramVersion(String s){_prgVersion=s;}
    public String getProgramVersion(){return _prgVersion;}
    public void setMatrix(String s){}
    public void writeFile(File f, int outputWidth) throws IOException {
        OutputStream fOut=null;
        fOut=fileOutStrm(f);
        writeFile(fOut, outputWidth);
        closeStrm(fOut);
    }
    public final static long WRITE_MARK_NOVEL=1<<1;
    void writeFile(OutputStream fo,int outputWidth) throws IOException {
        if (fo==null)  return;
        int count=0;
        final BA sb=new BA(333)
            .a("Program=").a(getProgramName())
            .a(" Version=").a(getProgramVersion())
            .a(" Databases=").join(getDatabases(),", ")
            .a("\n\n");
        sb.write(fo);
        boolean allNovel=true;
        for(BlastHit bh:getHits()) {
            for(BlastAlignment bAli: bh.getAlignments()) {
                if (! (allNovel=bAli.isNovel())) break;
            }
        }
        for(BlastHit bh:getHits()) {
            for(BlastAlignment bAli: bh.getAlignments()) {
                sb.clr().a(++count);
                bAli.toText(allNovel?0:WRITE_MARK_NOVEL, sb,outputWidth);
                sb.write(fo);
            }
        }
    }
      /* ---------------------------------------- */
    /* >>> ResidueSelection >>> */
    public boolean[] getMatchingAminoacids(byte[] sequence) {
        boolean sel[]=null;
        for(BlastHit h : getHits()) {
            for (BlastAlignment a : h.getAlignments()) {
                final byte mSeq[]=a.getMatchSeq();
                final byte qSeq[]=a.getQuerySeq();
                final int pos=strstr(STRSTR_IC, filtrBB(0L,LETTR,mSeq), sequence);
                if (pos>=0) {
                    if (sel==null) sel=new boolean[sequence.length];
                    int iA=pos;
                    final byte[][] midline=charite.christo.protein.Blosum.BLASTMIDLINE;
                    for(int iCol=0; iA<sel.length && iCol<mSeq.length && iCol<qSeq.length; iCol++) {
                        if (midline[mSeq[iCol]&127][qSeq[iCol]&127]!=' ') sel[iA]=true;
                        iA++;
                    }
                }
            }
        }
        return sel;
    }
    /* <<< ResidueSelection <<< */
    /* ---------------------------------------- */
    /* >>> Static utils >>> */
    public static BlastResult newInstance(SequenceBlaster blaster, String label, String icon) {
        if (blaster==null) return null;
        pcp(ChMsg.KEY_RESULT_LABEL,label, blaster);
        pcp(ChMsg.KEY_RESULT_ICON, icon,  blaster);
        ChMsg.toListOfResults(blaster);
        blaster.compute();
        final BA xml=blaster.getResultXml();
        if (xml==null) putln(RED_ERROR, "BlastResult: computation failed", gcp(ChMsg.KEY_RESULT_LABEL,blaster));
        final BlastResult br=xml==null?null:new BlastResult(xml);
        ChMsg.computationDone(br==null?null:" #"+countNotNull(br._vHits), blaster);
        return br;
    }
}
