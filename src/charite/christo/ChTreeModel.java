package charite.christo;
import java.util.*;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
import javax.swing.tree.*;
import javax.swing.event.*;

public class ChTreeModel  implements TreeModel {
    static Object rplc(Object o0) {
        Object o=o0;
        if(o instanceof ChRunnable && !(o instanceof UniqueList)) {
            final Object o2=((ChRunnable)o).run(PROVIDE_JTREE_CHILDS, null);
            if (o2!=null) o=o2;
        }
        return o;
    }
    public int getIndexOfChild(Object parent0, Object child)  {
        final Object parent=rplc(parent0);
        return
            parent instanceof Object[] ? idxOf(child,(Object[])parent) :
            parent instanceof List ? ((List)parent).indexOf(child) :
            -1;
    }
    public Object getChild(Object parent0, int i)  {
        final Object parent=rplc(parent0);
        final Object child =parent instanceof Object[] || parent instanceof  List ? get(i,parent) : null;
        return child!=null ? child : "null";
    }

    private final UniqueList<TreeModelListener> _vTML=new UniqueList(TreeModelListener.class);
    public void valueForPathChanged(TreePath tp, Object o)  {}
    public void removeTreeModelListener(TreeModelListener li) { _vTML.remove(li);}
    public void addTreeModelListener(TreeModelListener li)  { _vTML.add(li);}

    public boolean isLeaf(Object parent0)  { return getChildCount(parent0)==0;}
    public int getChildCount(Object parent0)  {
        final Object parent=rplc(parent0);
        return parent instanceof Object[] || parent instanceof List ? sze(parent) : 0;
    }
    private final Object _root;
    public ChTreeModel(long opt, Object root) { _root=root; }
    public Object getRoot() {
        //runUpdateHooks(_root);
        return _root;
    }

    public final static int DISPATCH_INSERTED=1, DISPATCH_CHANGED=2;
    public void dispatch(int id,TreeModelEvent ev) {
        for(TreeModelListener l : _vTML.asArray()) {
            // if (id==DISPATCH_CHANGED) l.treeNodesChanged(ev);
            if (id==DISPATCH_CHANGED) l.treeStructureChanged(ev);
            if (id==DISPATCH_INSERTED) l.treeNodesInserted(ev);
        }
        ChDelay.repaint(this,555);
    }

}
