package charite.christo;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.swing.JComponent;
import static charite.christo.ChUtils.*;
import static charite.christo.ChConstants.*;
/**HELP

<br><br><b>Internet and Web-proxy</b><br>
<br>
In some institutions all Internet connections are going through a Web proxy (see WIKI:Proxy_server).

If the proxy is not set correctly, Strap is unable to download files from the Web.

The proxy cannot be set in the running application.

The way how the proxy is set depends on whether the application is
started via WIKI:Webstart (See WIKI:JNLP) or started directly.

<h3>Webstart</h3>

In Windows or Linux there is a program called Java control panel  which allows to set the proxy (Button "Network settings"):
<i>STRING:charite.christo.ChUtils#fileJcontrol()</i> located in
<i>STRING:ChUtils#dirJavaws()</i>.

<i>OS:M,Under Macintosh, however, the system wide proxy setting is also used in Java.</i>

<h3>Direct start without Webstart</h3>

The proxy is specified with command line arguments. Example:
<pre class="terminal">
java -Dhttp.proxyHost=realproxy.charite.de -Dhttp.proxyPort=888   ...
</pre>

If there are addresses that should bypass the proxy use vertical bar separated list of hosts as in the following example:
<pre class="terminal">
 ....  -Dhttp.nonProxyHosts="localhost|host.mydomain.com" ...
</pre>

Further http://docs.oracle.com/javase/6/docs/technotes/guides/net/proxies.html
   @author Christoph Gille

*/

public class Web implements ChRunnable {
    public final static long SHOW_EXCEPTION=1<<1, NO_PRINT_EXCEPTION=1<<2;
    private Web(){}
    private static Web _inst;
    private static Web instance() { if (_inst==null) _inst=new Web(); return _inst; }

    /* <<<  <<< */
    /* ---------------------------------------- */
    /* >>> Security >>> */

    public static boolean uploadPermission(long frameOpts, BA httpData, String url) {
        if (httpData==null) return false;
        if (strstr(DO_NOT_ASK, url)>0  || dtkt()==null) return true;
        final ChButton tog=buttn(TOG_NASK_UPLOAD);
        if (tog.s()) return true;

        final JComponent
            tp=new ChTextView(new BA(0).filter(FILTER_FOLD,90, httpData)),
            msg=pnl(CNSEW,scrllpn(0, tp, dim(EM*80,EX*10)), pnl(HB,"Going to transmit above data to ",url), tog.cb());
        return ChMsg.yesNoSync(true, tog, ChFrame.PACK|frameOpts, msg);
    }
    /* <<< Security <<< */
    /* ---------------------------------------- */
    /* >>> getServerResponse >>> */

    public static BA getServerResponse(long options, String url,Object variables[][]) {
        final InputStream is=getServerResponseAsStream(options, url,variables);
        final BA ba=readBytes(is);
        closeStrm(is);
        return ba;
    }
    public static InputStream getServerResponseAsStream(long options, String url,Object data[][]) {
        final BA httpData=new BA(99999), baTmp=new BA(999);
        int i=0;
        for(Object[] dd:data) {
            if (get(0,dd)==null||get(1,dd)==null) continue;
            if (i++>0) httpData.a('&');
            baTmp.clr().a(dd[1]);
            httpData.a(dd[0]).a('=').filter(FILTER_URL_ENCODE, baTmp);
        }
        return getServerResponseAsStream(options, url,httpData);
    }

    public static InputStream getServerResponseAsStream(long options, String url,BA httpData) {
        OutputStream os=null;
        InputStream is=null;
        java.net.URLConnection uc=null;
        try {
            if (!uploadPermission(ChFrame.CENTER, httpData, url)) return null;
            final URL u=url(delSfx("&"+DO_NOT_ASK, url));
            if (u!=null) {
                uc=u.openConnection();
                uc.setDoOutput(true);
                uc.setDoInput(true);
                uc.setUseCaches(false);
                os=new BufferedOutputStream(uc.getOutputStream());
                wrte(os,httpData);
                os.flush();
                is=uc.getInputStream();
                return is;
            } else assrt();
        } catch(IOException e) {
            closeStrm(is);
            handleEx(options, "getServerResponseAsStream",  e);
        }
        finally{ closeStrm(os); }
        return null;
    }

    private static void handleEx(long opt, String msg, Exception ex) {
        putln(RED_CAUGHT_IN,msg,ex);
        if (0==(NO_PRINT_EXCEPTION&opt)) stckTrc(ex);
        if (0!=(SHOW_EXCEPTION&opt)) {
            error(ex,msg);
        }
    }

    /* <<< getServerResponse <<< */
    /* ---------------------------------------- */
    /* >>> to URL >>> */
    public static URL[] urls(Object urlBase, String files) { if (files.indexOf('|')>=0) assrt(); return urls(urlBase, splitTokns(files)); }
    public static URL[] urls(Object urlBase, String tokens[]) {
        if (tokens==null) return null;
        final ArrayList<URL> v=new ArrayList();
        for(String jar : tokens) {
            adUniq(url(looks(LIKE_EXTURL,jar) || urlBase==null ? jar : delSfx('/',urlBase.toString())+"/"+delPfx('/',jar)),v);
        }
        return toArryClr(v,URL.class);
    }
    /* <<< to URL <<< */
    /* ---------------------------------------- */
    /* >>> Process HTML Page >>> */
    public static void insertBaseTag(URL url, File fDest, BA html) {
        if (html==null) return;
        final String sUrl=url.toString();
        final int
            slash=-1,//sUrl.lastIndexOf('/');
            eHead=strstr(STRSTR_IC,"</head",html);
        final String insert= (eHead<0?"<head>\n":"") + "<base href=\""+ (slash>9 ? sUrl.substring(0,slash) : sUrl)+"\"  /> \n"+(eHead<0?"</head>\n":"");
        if (eHead>0 && strstr(STRSTR_IC,"<base",html, eHead, MAX_INT)>0) return;
        final int bBody= eHead>0?eHead: strstr(STRSTR_IC,"<body",html);
        if (bBody>0) html.insert(bBody,insert);
        final byte[] fDestBB=toBytsCached(toStrg(url(fDest)));
        if (fDest!=null) { /*INEFFICIENT*/
            for(int i=html.end(); --i>=0; ) {
                if (strEquls(STRSTR_IC,"href=\"#",html,i) || strEquls("href=\'#",html,i)) {
                    html.insert(i+6,fDestBB);
                }
            }
        }
    }
    /* <<< Process HTML Page <<< */
    /* ---------------------------------------- */
    /* >>> Thread  >>> */
    public Object run(String id, Object arg) {
        final Object[] argv=arg instanceof Object[] ? (Object[])arg:null;

        if (id=="TEST_INTERNET") {
            final ChPanel pnl=(ChPanel)arg;
            while(_needProbeProxy) sleep(333);
            final BA sb=readBytes(URL_STRAP);
            final String msg=strstr("strap.js",sb)>0 ? null : gcps(sze(sb)==0?"1":"2",pnl);
            if (msg!=null) pnl.t(msg).revalidate();
        }
        if (id=="TEST_URL") {
            final String url=(String)argv[0];
            final ChTextView v=(ChTextView)argv[1];
            final BA ba=readBytes(inStreamT(20,url,STREAM_UNZIP));
            v.setText(ba!=null ? ba : new BA(99).a("Unable to connect to ").a(url));
            v.revalidate();
        }

        if (id=="CK_NEW_VERS") {
            while(_needProbeProxy) sleep(999);
            final File f=thisJarFile();
            final BA ba=new BA(99).a(URL_STRAP)
                .a("checkUpdates.php?j=").a(arg).a("_RC").a(ChFrame.startCount()).a('_').a(delSfx(".jar",nam(f))).a(delLstCmpnt(lstCmpnt(MAP_ARGV.get("script"),'/'),'.') )
                .a("&c=").filter(FILTER_URL_ENCODE, dateOfCompilation())
                .a("&s=").filter(FILTER_URL_ENCODE, readBytes(file(dirTmp()+"/last_"+appName()+".txt")))
                .a("&jv=").a(javaVsn()).a('_')
                .filter(FILTER_URL_ENCODE, systProprty(SYSP_OS_NAME)).a('_')
                .filter(FILTER_URL_ENCODE, systProprty(SYSP_OS_VERSION)).a('_');
          if (isSystProprty(IS_USE_INSTALLED_SOFTWARE)) ba.a("DEBIAN");
            if (isSystProprty(IS_WEBSTARTED)) ba.a("Ws");
            if (prgOptT("-update")) ba.a("Update");
            if (myComputer()) putln(YELLOW_DEBUG,id," ",ba);
            else {
                try {
                    final BA info=readBytes(url(ba).openStream());
                    if (0<=strstr(STRSTR_IC, "new version", info)) ChMsg.msgDialog(0L, info.toString());
                } catch(Throwable ex) {}
            }
        }
        return null;
    }
    private static String _title;
    public static void checkForNewVersion(String title) {
        String t=title;
        if (myComputer() && !withGui()) assrt();
        if (chrAt(0,title)=='<') {
            final BA ba=new BA(99);
            int count=0;
            for(int i='A'; i<='z';i++) {
                if (ChExec.SUCCESS[i]) {
                    if (count++==0) ba.a("EX=");
                    ba.a((char)i);
                }
            }
            ba.a(title).a('_').a((System.currentTimeMillis()-TIME_AT_START)/1000).a('_').a(_title);
            t=ba.toString();
        }
        else _title=title;
        startThrd(thrdCR(instance(), "CK_NEW_VERS", t));
    }
    /* <<< Thread  <<< */
    /* ---------------------------------------- */
    /* >>> Proxy >>> */
    /* returns the position of the colon or -1 */
    public static int proxyPort(Object text,  int from0, int to0) {
        final int from=nxt(-SPC,text,from0,to0);
        final int to=prev(-SPC, text,mini(to0,sze(text))-1,from-1)+1;
        final int f=strEquls("http://",text,from) ? from+7:from;
        final int colon=strchr(':', text,f,to);
        //putln(mini(to0,sze(text))+" colon="+colon+" from="+from+" to="+to+"  "+cntainsOnly(LETTR_DIGT_DOT,text,f,colon));
        if (colon<0 || !cntainsOnly(LETTR_DIGT_DOT,text,f,colon) || !cntainsOnly(DIGT,text,colon+1,to) || to<=colon) return -1;
        final int port=atoi(text,colon+1);
        return port>1 && port<0xFFff ? port : -1;

    }
    /*
      netsh diag show ieproxy
      netsh winhttp show proxy
      proxycfg.exe
    */
    static void proxycfgOutput(BA ba) {
        if (ba==null) return;
        final int end[]=ba.eol();
        final byte T[]=ba.bytes();
        final ChTokenizer tok=new ChTokenizer();
        for(int pass=0; pass<2; pass++) {
            for(int iL=0;iL<end.length; iL++) {
                 final int b=iL==0?ba.begin() : end[iL-1]+1, e=end[iL];
                 if (strstr(STRSTR_IC, "bypass list", T, b,e)>=0) continue;
                if (pass==0 && strstr(STRSTR_IC, "Proxyserver", T, b,e)<0) continue;
                tok.setText(T,b,e);
                while(tok.nextToken()) {
                    final int f=tok.from(), t=tok.to();
                    final int port=proxyPort(T,f,t);
                    if (port>0) {
                        final String host=delLstCmpnt(delPfx("http://", toStrg(T,f,t)), ':');
                        //putln(ANSI_GREEN+"Option -proxycfgOutput: "+ANSI_RESET+host+":"+port);
                        sysSetPrptrty("http.proxyHost", host);
                        sysSetPrptrty("http.proxyPort", toStrg(port));
                        return;
                    }
                }
            }
        }
    }
    static void testConnectionForURL(String url) {
        if (url(url)!=null) {
            final ChTextView v=new ChTextView("Connecting to "+url+" ...\n\n");
            v.tools().showInFrame("Test connection "+url);
            startThrd(thrdCR(instance(), "TEST_URL",new Object[]{url,v}));
        }
    }

    private static void msgP(String s) {
        final String msg=" Prg option \"-probeWebProxy\": ";
        if (_tfPP!=null) _tfPP.t(msg).a(s);
        putln(msg,s);
    }

    private static boolean _needProbeProxy;
    public static void runAfterProbeProxy(long options, Runnable r, int maxWaitMS) {
        if (r==null) return;
        if (_needProbeProxy) {
            if (isEDT()) {
                startThrd(thrdM("runAfterProbeProxy",Web.class,new Object[]{longObjct(options),r, intObjct(maxWaitMS)}));
                return;
            }
            puts("runAfterProbeProxy"); puts(" ... ");
            for(int i=maxWaitMS; (i-=100)>0 && _needProbeProxy;) {  puts("X"); sleep(100);}
            putln("runAfterProbeProxy", GREEN_DONE);
        }
        if (0!=(EDT&options)) inEdtLater(r);
        else r.run();
    }

    private static ChTextView _tfPP;
    public final static synchronized ChTextView tfAutoprobeProxy() { return _tfPP;}
    public final static Runnable thread_autoProbeProxy() {
        if (_tfPP==null) _tfPP=new ChTextView("").bg(null);
        return thrdM("autoProbeProxy", Web.class);
    }
    public final static void autoProbeProxy() {
        _needProbeProxy=true;
        try {
            final URL url=url("http://www.bioinformatics.org/strap/strap.jar");
            if (getContentLen(url)>99999) {
                msgP("Test connection to the internet: Success");
                return;
            }
            msgP("Going to test internet connection ...");
            final String env=delPfx("http://",ChEnv.get("http_proxy"));
            final int colon=strchr(':',env);
            if (colon>1) {
                sysSetPrptrty("http.proxyHost",env.substring(0,colon));
                sysSetPrptrty("http.proxyPort",env.substring(colon+1));
                if (getContentLen(url)>99999) {
                    msgP(GREEN_SUCCESS+" with $http_proxy="+env);
                    return;
                }
            }
            sysSetPrptrty("java.net.useSystemProxies", "true");
            msgP("Internet connection: "+(getContentLen(url)>99999?GREEN_SUCCESS:"Failure")+" with java.net.useSystemProxies=true");
        } finally {
            _needProbeProxy=false;
        }
    }

    final static Object KEY_TA_PROXY=new Object();
    private static JComponent _pnlProxy;
    public static void showProxyInfo() {
        ChFrame.frame("Internet settings",Web.pnlProxyInfo(), ChFrame.AT_CLICK).shw(ChFrame.PACK);
    }
    public static JComponent pnlProxyInfo() {
        if (_pnlProxy==null) {
            final Object
                ta=new ChTextView(""),
                pProxy=
                pnl(VBHB,
                    "<b>Proxy Setting</b>",

                    isMac() ? null: pnl(HBL, "The Web-Proxy cannot be changed inside Strap. It is selected in the Java Control Panel",  ChButton.doView(msgJcontrol()).t("Open ...")),
                    " ",
                    "To validate the current setting, the following shows proxies for a list of web addresses. Click addresses to probe connection.",
                    ta,
                    pnl(HBL,"Modify list of test addresses ",Customize.newButton(Customize.testProxySelector).i(null))
                    ),
                pParam=!Insecure.EXEC_ALLOWED?null:
                pnl(VB,
                    " ",
                    pnl(HBL,"The following shows command-line parameters which will be used when other Java-programs are started"),
                    monospc(new BA(0).join(commandLineParametersForProxy()))
                    ),
                pFtp=pnl(VB,
                         pnl(HBL,"EdtFTP may solve FTP problems: ",buttn(TOG_EDTFTP).cb(),smallHelpBut(AnonymousFTPPROXY.class))
                         );

            _pnlProxy=pnl(VBHB,
                          pnl(HBL, "<h2>Internet settings</h2>",  "#", smallHelpBut(Web.class)),
                          "If Strap fails to access the internet, this dialog helps to identify the problem.",
                          "It requires advanced computer knowledge and unexperienced users should seek help from a specialist.",
                          " ",
                          pnlTogglOpts("*Web proxy",pProxy),
                          pProxy,
                          " ",
                          !Insecure.EXEC_ALLOWED?null: pnlTogglOpts("*View java parameters ",pParam),
                          pParam,
                          " ",
                          pnlTogglOpts("*FTP ",pFtp),
                          pFtp
                          );

            addActLi(ChUtils. evLstnr(0),Customize.customize(Customize.testProxySelector));

            pcp(KEY_TA_PROXY, ta, _pnlProxy);
        }
        gcp(KEY_TA_PROXY, _pnlProxy,ChTextView.class).setText(testProxySelector()).tools().underlineRefs(0);
        return _pnlProxy;
    }

    /* <<< Proxy <<< */
    /* ---------------------------------------- */
    /* <<< Test connection <<< */
    public static JComponent labelTestConnection(String error1, String error2) {
        final ChPanel p=new ChPanel();
        p.setForeground(C(0xbc0000));
        pcp("1", orS(error1, "Warning: Failed to connect to the Internet"), p);
        pcp("2", error2, p);
        thrdCR(instance(), "TEST_INTERNET",p).start();
        return p;
    }

    /* >>> Test connection >>> */
    /* ---------------------------------------- */
    /* >>> Socket >>> */

    public static Socket newSocket(String host) throws IOException {
        final Object proxy= proxyForUrl(url("http://"+host));
        final Socket socket;
        if (proxy==null) {
            (socket=new Socket()).connect(new InetSocketAddress(host,80));
        } else {
            socket=new ChHttpClientPROXY().newSocket(host);
        }
        return socket;
    }
 // nc -l -p 1234

    public final static Map<String,String> MAP_VIA_PORT_MSG=new HashMap();
    public static Runnable thread_sendDataToPort(long options, int port, CharSequence data, String wordUnderMouse) {
        return thrdM("sendDataToPort", Web.class, new Object[]{longObjct(options), intObjct(port),data,wordUnderMouse});
    }

    public static boolean sendDataToPort(long options, int port, CharSequence data, String wordUnderMouse) {
        if (sze(wordUnderMouse)>0) {
            for(Map.Entry<String,String> e : entryArry(MAP_VIA_PORT_MSG)) {
                final String k=e.getKey(), v=e.getValue();
                if (sze(k)*sze(v)>0&& strEquls(k,wordUnderMouse)) {
                    shwTxtInW("Sent to "+port,toBA(v));
                    MAP_VIA_PORT_MSG.remove(k);
                }
            }
        }
        OutputStream os=null;
        try {
            final java.net.Socket s=new java.net.Socket("localhost", port);
            os=s.getOutputStream();
            wrte(os,toBA(data));
            return true;
        } catch(IOException iox){   handleEx(options, "sendDataToPort\nAlso see shell command\n <br><pre class=\"terminal\">\n netcat -l -p "+port+"\n</pre><br>\n", iox); }
        finally{ closeStrm(os); }
        return false;
    }

public static boolean localhost_sendData(String url, BA log) {
        final String u=toStrgTrim(url), pattern="http://localhost:";
        final int iQu=strchr('?',u);
        if (!(iQu>0&&strEquls(pattern, u))) return false;
        final String data= urlDecode(u.substring(iQu));
        final int port=atoi(u, sze(pattern));
        if (log!=null) log.a("Send data to localhost: port=").a(port).a(" data=").aln(data);
        startThrd(Web.thread_sendDataToPort(Web.SHOW_EXCEPTION, port, data,u));
        return true;
    }

}
