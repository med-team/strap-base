prefix =   $(DESTDIR)/usr
bindir =   $(prefix)/bin
man1dir =  $(prefix)/share/man/man1
sharedir =  $(prefix)/share/strap-base
etcdir =   $(DESTDIR)/etc
jar=strap-protein-alignment-1.jar

all: $(jar) 

$(jar):
	find src -name "*.java" | cut -c 5- > javaFiles.txt
	cd src; javac -source 1.5 -target 1.5 -cp @../classpath.txt  @../javaFiles.txt
	cd src; jar -cmf ../Manifest ../$(jar) *

clean:
	find . -name '*.class' -delete

install: all
	mkdir -p $(sharedir) $(man1dir) $(bindir) $(etcdir)/bash_completion.d
	install -m 0644 $(jar) $(sharedir)
	install -m 0644 man/*.1 $(man1dir)
	install -m 0644 strap_bash_completion.sh $(etcdir)/bash_completion.d/strap_base
	install sh/* $(bindir)
	cd $(bindir); ln -s strap_to_html alignment_to_html
	cd $(man1dir); ln -s strap_to_html.1 alignment_to_html.1

# (highlight-regexp "\t" 'hi-yellow)
