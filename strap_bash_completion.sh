# Author: Christoph Gille
# This is the text completion script for Strap, http://www.bioinformatics.org/strap/ 

# Usage:  Copy this script to to /etc/bash_completion.d/
#         At the command prompt type:  ".  /etc/bash_completion"
#         Then the tab key will complete options and parameters.

# Requires: bash version >= 4 and the package bash-completion



# The following methods are inspired from  __gitcomp and __gitcomp_1
# __strap_1 requires 2 arguments
export _strap_defined="x" 
__strap_1 () {
  local c IFS=' '$'\t'$'\n'
  for c in $1; do
    case "$c$2" in
      --*=*) printf %s$'\n' "$c$2" ;;
      *.)    printf %s$'\n' "$c$2" ;;
      *)     printf %s$'\n' "$c$2 " ;;
    esac
  done
}

# __strap accepts 1, 2, 3, or 4 arguments
# generates completion reply with compgen
__strap () {
  local cur
  if ! _get_comp_words_by_ref -n =: cur  &> /dev/null; then
    cur="${COMP_WORDS[COMP_CWORD]}"
  fi
  [ $# -gt 2 ] && cur="$3"
  
  case "$cur" in
	--*=)
          COMPREPLY=()
          ;;
	*)
       local IFS=$'\n'
       COMPREPLY=($(compgen -P "${2-}" \
        -W "$(__strap_1 "${1-}" "${4-}")" \
        -- "$cur"))
       ;;
  esac
}


_strap() {
  COMPREPLY=()
  local list="" cur="${COMP_WORDS[COMP_CWORD]}"
  _get_comp_words_by_ref -n = cur &> /dev/null
  case $cur in
    -c=*)
          COMP_WORDBREAKS=${COMP_WORDBREAKS//:}
          list="default http://strip/asdfasd  http://verbatim/4213412/fsda  whitespace" 
          ;;   


    JVM_*)            
           COMPREPLY=( $(compgen -W "JVM_-Xmx300M JVM_-ea  JVM_-DproxyHost= JVM_-DproxyPort= " -- ${cur}) )
           return 0
           ;;        


    -script=h*)
                COMP_WORDBREAKS=${COMP_WORDBREAKS//:}
                local STRAPURL=http://www.bioinformatics.org/strap 
                local s=$STRAPURL/scripts/HbA_alignment.txt" "$STRAPURL/scripts/HbA_with3D.txt
                s=$s" "$STRAPURL/PDF/2003/PMID12595256/1.txt" "$STRAPURL/PDF/2003/PMID12595256/6.txt 
                list=$s" "$STRAPURL/dataFiles/scriptExamples/toHTML1.txt
                ;;
    -script=*)
               cur="${cur##-script=}"              
               ;;

    -laf=*)
            list="de.muntjak.tinylookandfeel.TinyLookAndFeel com.sun.java.swing.plaf.gtk.GTKLookAndFeel com.sun.java.swing.plaf.motif.MotifLookAndFeel com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            ;;


    -dialog=*)
               list="DialogAlign DialogAlignOneToAll DialogBarChart DialogBlast DialogCompareProteins DialogDifferentResidues DialogDotPlot DialogExportAlignment DialogExportWord DialogFetchPdb DialogFetchSRS DialogGenbank DialogHighlightPattern DialogImportMFA DialogInferCDS DialogNeighbors DialogNewProtein DialogPhylogeneticTree DialogPlot DialogPredictCoiledCoil DialogPredictSecondaryStructures DialogPredictTexshade DialogPredictTmHelices2 DialogPublishAlignment DialogRenameProteins DialogResidueAnnotationChanges DialogResidueAnnotationList DialogSelectionOfResiduesMain DialogSuperimpose3 DialogExportProteins Dialog_bl2seq Texshade"               
               ;;

    -cp=*) list=$(ls   --color=never /usr/share/java/*laf.jar &> /dev/null)  ;;
    -geometry=*) list="1400x900+100+10  999x444+11+22" ;;
    -stdout=*|-stderr=*) list="true false" ;;
    -alignerP=*) list="clustalw t_coffee" ;;
    -aligner=*) list="clustalw t_coffee amap mafft kalign muscle align_m dialign dialignt probcons2 neobio jaligner" ;;
    -a3d=*) list="matt mapsci smolign tm_align ce native_ce mustang" ;;
    -s3d=*) list="tm_align ce native_ce gangstaplus" ;;
    -v3d=*) list="pymol astex jmol" ;;

    -*|-)
          
          local opts=$(echo -customizeAdd{C_compiler=,CplusPlus_compiler=,CssAlignBrowser=,Databases=,DviPsConverter=,DviViewer=,EcClassLinks=,Error=,ExecAminoSeq=,ExecResidueSelection=,FileBrowsers=,Fortran_compiler=,HtmlEditors=,JavaSourceEditor=,KeggOrganisms=,Laf=,Latex=,LatexEditors=,OpenFileWith=,PdbLinks=,PdbSite=,PdfViewer=,Pdflatex=,PreferedDasSources=,ProteinDatabases=,ProteinFileExtensions=,ProteinInfoRplc=,PsPoster=,PsViewer=,RefuseExe=,ScriptByRegex=,SeqFeatureColors=,SpeciesIcons=,StrapApps=,TestProxySelector=,TextEditors=,TrustExe=,UniprotLinks=,WatchLogFile=,WebBrowser=,WebLinks=,WebSearches=})

          opts=$opts" -allowFileModification -noCache -manual  -stdout= -noIdentical -noSeqres -probeWebProxy -script= -scriptOutput= "
          opts=$opts" -alignerP= -a3d= -s3d= -v3d= -aligner="

          local w0="${COMP_WORDS[0]}"

          if [[ $w0 == *_to_* ]]; then
            if [[ $w0 == *html* ]]; then 
              opts=$opts" -toWord -toWord= "; 
            fi
            opts=$opts" -o= "
            else
              opts=$opts" -align= -load= -pdb= -cp=  -dialog=  -laf= -edtftp  -log3d  -askExec -askUpload -noSound -geometry="
              opts=$opts" -toFasta -toMultipleFasta -toMSF -toClustal -toHTML -toWord  -toFasta= -toMultipleFasta= -toMSF= -toClustal= -toHTML= -toWord= "    
            fi
            
            COMPREPLY=( $(compgen  -o nospace -W "${opts}" -- ${cur}) )   
            return
            ;;
  esac

  if [ -n "$list" ]; then
    local optEq=${cur%%=*}"="
    __strap  "${list}" '' "${cur##$optEq}"
    return
  fi


  _filedir &> /dev/null || COMPREPLY=( $(compgen -f -- ${cur}) )  

} 


complete -F _strap  -o nospace  strap.sh strap_{protein_alignment,base} strap_to_{fasta,multiple_fasta,word,msf,clustal,clustal,html}

